/*
   ************************************************************
   * Project: ALL RISK REPORT
   * Module : ChineseDrywall.java
   * Creation History:
      Created By : Gowri on 1/16/2013
   * Modification History:
      Last Modified By : Gowri on 04/04/2013
   ************************************************************  
*/package idsoft.inspectiondepot.ARR;

import java.util.ArrayList;

import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.DrywallSummary.textwatcher;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.RelativeLayout.LayoutParams;

public class DrywallRoom extends Activity{
	CommonFunctions cf;
	boolean load_comment=true;
	Spinner rmnospin,rmnamespin,rmoutacessspin,rmoutevalspin,rmswiaccessspin,rmswievalspin,
	        rminvaspin,rmsulfspin,rmcopspin,rmmetcorspin,rmdrkspin,rmconfspin;
	ArrayAdapter rmnoadap,rmnameadap,rmoutletsadap,rmoutletsevaladap,rmswiadap,rmswievaladap,
	             rminvadap,rmsuladap,rmcopadap,rmmetcopadap,rmdrkadap,rmconfadap;
	String rmnum[] = {"--Select--","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25"};
	String rmname[] = {"--Select--","Kitchen","Dining Room","Breakfast Room","Living Room","Formal Room","Bathroom 1","Bathroom 2","Half Bathroom","Hall Bathroom",
			           "Master Bathroom","Bedroom 1","Bedroom 2","Bedroom 3","Bedroom 4","Bedroom 5","Bedroom 6","Bedroom 7","Bedroom 8","Bedroom 9","Bedroom 10","Hall Closet","Garage","Laundry","Other"};
	String rmout[] = {"--Select--","1","2","3","4","None"};
	String rminvas[] = {"--Select--","Yes","N/A-None"};
    String sulfr[] = {"--Select--","Yes","None Noted"};
    String cop[] = {"--Select--","Yes","None Noted","Not Confirmed"};
    static ArrayList<String> str_arrlst = new ArrayList<String>();
    static ArrayList<String> str_rmnumarrlst = new ArrayList<String>();
    String room_in_DB[];
    String rmnumstr="",getid="",rmnamestr="",rmoutacessstr="",rmoutevalstr="",rmswiaccessstr="",rmswievalstr="",rminvastr="",
		    rmsulfstr="",rmconfprestr="",rmcoprcorsstr="",rmmetcorstr="",rmdrkstainstr="";
	int vrint=0,roomCurrent_select_id;
	Cursor Drysum_save,Dryroom_save;
	TableLayout roomtblshow;
	TextView ntetxt;
	
	@Override
	   public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        cf=new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);
				
		 	}
	        setContentView(R.layout.drywallroom);
	        cf.getDeviceDimensions();
	        
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
			hdr_layout.addView(new HdrOnclickListener(this,1,"Chinese Drywall","Visual Assessment Room",6,0,cf));
	        
	        LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
	        mainmenu_layout.setMinimumWidth(cf.wd);
	        mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 6, 0,0,cf));
	        LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 62, 1,0,cf));
	        TableRow tblrw = (TableRow)findViewById(R.id.row2);
			tblrw.setMinimumHeight(cf.ht);
			cf.CreateARRTable(7);
			cf.CreateARRTable(74);
			declarations();
			setComments();
			Showroomvalue();
	}
	private void setComments() {
		// TODO Auto-generated method stub
		 try
			{
			   Cursor SCf_retrive=cf.SelectTablefunction(cf.DRY_sumcond, " where fld_srid='"+cf.selectedhomeid+"'");
			   if(SCf_retrive.getCount()>0)
			   {  
				   SCf_retrive.moveToFirst();
				   ((EditText)findViewById(R.id.roomcomment)).setText(cf.decode(SCf_retrive.getString(SCf_retrive.getColumnIndex("assesmentcomments"))));
			   }
			   
			}catch (Exception e) {
				// TODO: handle exception
			}
	}
	private void declarations() {
		// TODO Auto-generated method stub
		 cf.setupUI((ScrollView) findViewById(R.id.scr));
		((EditText)findViewById(R.id.roomcomment)).addTextChangedListener(new textwatcher(1));
		
		 rmnospin=(Spinner) findViewById(R.id.roomno_spin);
		 str_rmnumarrlst.clear();
		 for(int i=0;i<rmnum.length;i++)
		 {
			 str_rmnumarrlst.add(rmnum[i]);
		 }
		 
		 rmnoadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, str_rmnumarrlst);
		 rmnoadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 rmnospin.setAdapter(rmnoadap);
		 rmnospin.setOnItemSelectedListener(new  Spin_Selectedlistener(1));
		 
		 rmnamespin=(Spinner) findViewById(R.id.rmname_spin);
		 str_arrlst.clear();
		 for(int i=0;i<rmname.length;i++)
		 {
			  str_arrlst.add(rmname[i]);
		 }
		 
		 rmnameadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, str_arrlst);
		 rmnameadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 rmnamespin.setAdapter(rmnameadap);
		 rmnamespin.setOnItemSelectedListener(new  Spin_Selectedlistener(2));
		 
		 rmoutacessspin=(Spinner) findViewById(R.id.outacess_spin);
		 rmoutletsadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, rmout);
		 rmoutletsadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 rmoutacessspin.setAdapter(rmoutletsadap);
		 rmoutacessspin.setOnItemSelectedListener(new  Spin_Selectedlistener(3));
		 
		 rmoutevalspin=(Spinner) findViewById(R.id.outeval_spin);
		 rmoutletsevaladap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, rmout);
		 rmoutletsevaladap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 rmoutevalspin.setAdapter(rmoutletsevaladap);
		 rmoutevalspin.setOnItemSelectedListener(new  Spin_Selectedlistener(4));
		 
		 rmswiaccessspin=(Spinner) findViewById(R.id.switchacess_spin);
		 rmswiadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, rmout);
		 rmswiadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 rmswiaccessspin.setAdapter(rmswiadap);
		 rmswiaccessspin.setOnItemSelectedListener(new  Spin_Selectedlistener(5));
		 
		 rmswievalspin=(Spinner) findViewById(R.id.switchseval_spin);
		 rmswievaladap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, rmout);
		 rmswievaladap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 rmswievalspin.setAdapter(rmswievaladap);
		 rmswievalspin.setOnItemSelectedListener(new  Spin_Selectedlistener(6));
		 
		 rminvaspin=(Spinner) findViewById(R.id.invassive_spin);
		 rminvadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, rminvas);
		 rminvadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 rminvaspin.setAdapter(rminvadap);
		 rminvaspin.setOnItemSelectedListener(new  Spin_Selectedlistener(7));
		 rminvaspin.setSelection(2);
		 
		 rmsulfspin=(Spinner) findViewById(R.id.sulfur_spin);
		 rmsuladap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, sulfr);
		 rmsuladap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 rmsulfspin.setAdapter(rmsuladap);
		 rmsulfspin.setOnItemSelectedListener(new  Spin_Selectedlistener(8));
		 rmsulfspin.setSelection(2);
		 
		 rmcopspin=(Spinner) findViewById(R.id.copcor_spin);
		 rmcopadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, cop);
		 rmcopadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 rmcopspin.setAdapter(rmcopadap);
		 rmcopspin.setOnItemSelectedListener(new  Spin_Selectedlistener(9));
		 rmcopspin.setSelection(2);
		 
		 rmmetcorspin=(Spinner) findViewById(R.id.metcor_spin);
		 rmmetcopadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, cop);
		 rmmetcopadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 rmmetcorspin.setAdapter(rmmetcopadap);
		 rmmetcorspin.setOnItemSelectedListener(new  Spin_Selectedlistener(10));
		 rmmetcorspin.setSelection(2);
		 
		 rmdrkspin=(Spinner) findViewById(R.id.drkstain_spin);
		 rmdrkadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, cop);
		 rmdrkadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 rmdrkspin.setAdapter(rmdrkadap);
		 rmdrkspin.setOnItemSelectedListener(new  Spin_Selectedlistener(11));
		 rmdrkspin.setSelection(2);
	
		 rmconfspin=(Spinner) findViewById(R.id.confpres_spin);
		 rmconfadap= new ArrayAdapter(this,android.R.layout.simple_spinner_item, cop);
		 rmconfadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 rmconfspin.setAdapter(rmconfadap);
		 rmconfspin.setOnItemSelectedListener(new  Spin_Selectedlistener(12));
		 rmconfspin.setSelection(2);
		 
		 roomtblshow =(TableLayout)findViewById(R.id.roomvalue);
		 
	}
	class Spin_Selectedlistener implements OnItemSelectedListener
	{
		int i;
		public Spin_Selectedlistener(int i) {
			// TODO Auto-generated constructor stub
			this.i=i;
		}

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			if(i==1)
			{
			  rmnumstr = rmnospin.getSelectedItem().toString();
			  if(rmnumstr.equals("--Select--")){
				
			  }
			  else
			  {
				if(vrint==0){defaultroom();}
			  }
			}
			else if(i==2)
			{
				  rmnamestr = rmnamespin.getSelectedItem().toString();
				  /*if(!rmnamestr.equals("Other"))
				  {
					   try
					   {
						   Cursor rm_retrieve= cf.SelectTablefunction(cf.DRY_room, " Where fld_srid='"+cf.selectedhomeid+"' and roomname='"+rmnamestr+"' ");
						   System.out.println("rm_retrieve= "+rm_retrieve.getCount());
						   if(rm_retrieve.getCount()>0)
					       {
						      cf.ShowToast("Already Exists!!. Please select another Room Name.", 0); 
			               }
					   }
					   catch (Exception e) {
						// TODO: handle exception
						   System.out.println("e= "+e.getMessage());
					   }
				  }*/
			        /* boolean bb = false;
				if(rname!=null)
				{
					for(int i=0;i<rname.length;i++)
					{
						System.out.println("rname[i]= "+rname[i]+"\n"+rmnamestr);
						if((rname[i].equals(rmnamestr)))
						{
							bb=true;
						}
					}
				}
				if(!bb)
				{
					
				}
				else
				{
					cf.ShowToast("Already Exists!!. Please select another Room Name.", 0);
				}*/
				if(rmnamestr.equals("Other"))
				{
					((LinearLayout)findViewById(R.id.rmnamelin)).setVisibility(arg1.VISIBLE);
				}
				else
				{
					((LinearLayout)findViewById(R.id.rmnamelin)).setVisibility(arg1.GONE);
				}
			}
			else if(i==3)
			{
				rmoutacessstr = rmoutacessspin.getSelectedItem().toString();
			}
			else if(i==4)
			{
				rmoutevalstr = rmoutevalspin.getSelectedItem().toString();
			}
			else if(i==5)
			{
				rmswiaccessstr = rmswiaccessspin.getSelectedItem().toString();
			}
			else if(i==6)
			{
				rmswievalstr = rmswievalspin.getSelectedItem().toString();
			}
			else if(i==7)
			{
				rminvastr = rminvaspin.getSelectedItem().toString();
			}
			else if(i==8)
			{
				rmsulfstr = rmsulfspin.getSelectedItem().toString();
			}
			else if(i==9)
			{
				rmcoprcorsstr = rmcopspin.getSelectedItem().toString();
			}
			else if(i==10)
			{
				rmmetcorstr = rmmetcorspin.getSelectedItem().toString();
			}
			else if(i==11)
			{
				rmdrkstainstr = rmdrkspin.getSelectedItem().toString();
			}
			else if(i==12){
				rmconfprestr = rmconfspin.getSelectedItem().toString();
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	public void defaultroom() {
		// TODO Auto-generated method stub
		rmnamespin.setSelection(0);
		((EditText)findViewById(R.id.roomname_ettxt)).setText("");
		((LinearLayout)findViewById(R.id.rmnamelin)).setVisibility(cf.v1.GONE);
		rmoutacessspin.setSelection(0);
		rmoutevalspin.setSelection(0);
		rmswiaccessspin.setSelection(0);
		rmswievalspin.setSelection(0);
		rminvaspin.setSelection(2);
		rmsulfspin.setSelection(2);
		rmcopspin.setSelection(2);
		rmmetcorspin.setSelection(2);
		rmdrkspin.setSelection(2);
		rmconfspin.setSelection(2);
	}
	class textwatcher implements TextWatcher
    {
	         public int type;
	         textwatcher(int type)
	    	{
	    		this.type=type;
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
	    		if(this.type==1)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.room_tv_type1),500); 
	    			if(s.toString().startsWith(" "))
	    			{
	    				((EditText)findViewById(R.id.roomcomment)).setText("");
	    			}
	    		}
	    	
	         }

	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,int after) {
	    		// TODO Auto-generated method stub
	     	}

	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,int count) {
	    		// TODO Auto-generated method stub
	    	}
	}	
	public void clicker(View v)
	{
		switch (v.getId()) {
			case R.id.hme:
				cf.gohome();
				break;
			case R.id.dryroomclear:
				vrint=0;
			    defaultroom();rmnospin.setSelection(0);//rmnospin.setEnabled(true);
		        ((Button)findViewById(R.id.addroom)).setText("Save/Add More Assessment Room");
				break;	
			case R.id.loadcomments:/***Call for the comments***/
				int len=((EditText)findViewById(R.id.roomcomment)).getText().toString().length();
				if(load_comment )
				{
						load_comment=false;
						int loc[] = new int[2];
						v.getLocationOnScreen(loc);
						Intent in1 = cf.loadComments("Visual Assessment Room Comments",loc);
						in1.putExtra("length", len);
						in1.putExtra("max_length", 500);
						startActivityForResult(in1, cf.loadcomment_code);
				}
				break;
			case R.id.addroom:
				vrint=2;
				if(rmnumstr.equals("--Select--")){
					cf.ShowToast("Please select Room Number.", 0);
				}else{
					if(rmnamestr.equals("--Select--"))
					{
						cf.ShowToast("Please select Room Name.", 0);
					}
					else
					{
						if(rmnamestr.equals("Other"))
						{
							if(((EditText)findViewById(R.id.roomname_ettxt)).getText().toString().trim().equals(""))
							{
								cf.ShowToast("Please enter the Other text for Room Name.", 0);
								cf.setFocus(((EditText)findViewById(R.id.roomname_ettxt)));
							}
							else
							{
								next();
							}
						}
						else
						{
							next();
						}
					}
				}
				break;
			case R.id.save:
				chk_values();
				if(Dryroom_save.getCount()==0)
				{
					cf.ShowToast("Atleast one Visual Assessment entry need to be added and saved.", 0);
				}
				else
				{
					/*if(((EditText)findViewById(R.id.roomcomment)).getText().toString().trim().equals(""))
					{
						cf.ShowToast("Please enter Visual Assessment Room Comments.", 0);
						cf.setFocus(((EditText)findViewById(R.id.roomcomment)));
					}
					else
					{*/
						if(Drysum_save.getCount()>0)
						{
							try
							{
								cf.arr_db.execSQL("UPDATE "+cf.DRY_sumcond+ " set assesmentcomments='"+cf.encode(((EditText)findViewById(R.id.roomcomment)).getText().toString())+"' where fld_srid='"+cf.selectedhomeid+"'");
								cf.ShowToast("Visual Assessment Room saved successfully.", 1);
							}
							catch (Exception E)
							{
								String strerrorlog="Updating the Assessment - Drywall";
								cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
							}
						}
						else
						{
							try
							{
								cf.arr_db.execSQL("INSERT INTO "
										+ cf.DRY_sumcond
										+ " (fld_srid,presofsulfur,obscopper,presofdry,addcomments,drywallcomments,atticcomments,"+
										   "applianccomments,hvaccomments,assesmentcomments)"
										+ "VALUES('"+cf.selectedhomeid+"','','',"+
										"'','','',"+
										"'','','','"+cf.encode(((EditText)findViewById(R.id.roomcomment)).getText().toString())+"')");
								 cf.ShowToast("Visual Assessment Room saved successfully.", 1);
							}
							catch (Exception E)
							{
								String strerrorlog="Inserting the Assessment - Drywall";
								cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
							}
						}
						((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
					    cf.goclass(63);
					//}
				}
				
				break;
				
	   }
	}
	private void next() {
		// TODO Auto-generated method stub
		if(rmoutacessstr.equals("--Select--")){
		 	cf.ShowToast("Please select No.of Outlets (Total Accessible)", 0);
		    }else{
			if(rmoutevalstr.equals("--Select--")){
				cf.ShowToast("Please select No.of Outlets (Evaluated)", 0);
			}else{
				if(rmswiaccessstr.equals("--Select--")){
					cf.ShowToast("Please select No.of Swithches (Total Accessible)", 0);
				}else{
					if(rmswievalstr.equals("--Select--")){
						cf.ShowToast("Please select No.of Switches (Evaluated)", 0);
					}else{
						if(rminvastr.equals("--Select--")){
							cf.ShowToast("Please select Invasive Testing (Type)", 0);
						}else{
							if(rmsulfstr.equals("--Select--")){
								cf.ShowToast("Please select Sulfur-like odor or other unusual odors noted.", 0);
							}else{
								  if(rmcoprcorsstr.equals("--Select--")){
										cf.ShowToast("Please select Observed Copper Corrosion.", 0);
									}else{
										if(rmmetcorstr.equals("--Select--")){
											cf.ShowToast("Please select Observed Metal Corrosion.", 0);
										}else{
											if(rmdrkstainstr.equals("--Select--")){
												cf.ShowToast("Please select Darkening/Stains noted on Fixture/Fittings.", 0);
											}else{
												if(rmconfprestr.equals("--Select--")){
													cf.ShowToast("Please select Confirmed Presence of Chinese Drywall.", 0);
												}else{
                                                     try
											        {
													   if(((Button)findViewById(R.id.addroom)).getText().toString().equals("Update")){
				                                         cf.arr_db.execSQL("UPDATE  "+cf.DRY_room+" SET roomnum='"+rmnumstr+"',"+
													           "roomname='"+rmnamestr+"',"+
													           "rmnameotr='"+cf.encode(((EditText)findViewById(R.id.roomname_ettxt)).getText().toString())+"',"+
															   "outletsaccess='"+rmoutacessstr+"',outletseval='"+rmoutevalstr+"',switchaccess='"+rmswiaccessstr+"',"+
													           "switcheval='"+rmswievalstr+"',invasive='"+rminvastr+"',sulfur='"+rmsulfstr+"',confpresence='"+rmconfprestr+"',"+
															   "obscopcorrosion='"+rmcoprcorsstr+"',obsmetcorrosion='"+rmmetcorstr+"',"+
													           "darkstains='"+rmdrkstainstr+"' Where roomId='"+roomCurrent_select_id+"' ");
														defaultroom();
														rmnospin.setSelection(0);//rmnospin.setEnabled(true);
														cf.ShowToast("Visual Assessment Room updated successfully.", 0);((Button)findViewById(R.id.addroom)).setText("Save/Add More Assessment Room");
														vrint=0;
														Showroomvalue();
													}else{
														chk_values();
														boolean[] b3 = {false};
														if(Dryroom_save.getCount()==0){
															
														}else{
															/*if(room_in_DB!=null)
															{
																for(int i=0;i<room_in_DB.length;i++)
																{
																	if((room_in_DB[i].equals(rmnumstr)))
																	{
																		b3[0]=true;
																	}
																}
															}*/
														}
														/*if(!b3[0]){*/cf.arr_db.execSQL("INSERT INTO "
																	+ cf.DRY_room
																	+ " (fld_srid,roomnum,roomname,rmnameotr,outletsaccess,outletseval,switchaccess,switcheval,invasive,sulfur,"+
																	 "confpresence,obscopcorrosion,obsmetcorrosion,darkstains)"
																	+ "VALUES('"+cf.selectedhomeid+"','"+rmnumstr+"','"+rmnamestr+"','"+cf.encode(((EditText)findViewById(R.id.roomname_ettxt)).getText().toString())+"',"+
															        "'"+rmoutacessstr+"','"+rmoutevalstr+"','"+rmswiaccessstr+"','"+rmswievalstr+"','"+rminvastr+"','"+rmsulfstr+"','"+rmconfprestr+"',"+
															        "'"+rmcoprcorsstr+"','"+rmmetcorstr+"','"+rmdrkstainstr+"')");
															 cf.ShowToast("Visual Assessment Room added successfully.", 1);
															 defaultroom();      
															 rmnospin.setSelection(0);//rmnospin.setEnabled(true);
															/*}else{cf.ShowToast("Visual Assessment Room already exists. Please select another Visual Assessment Room.", 0);
															 rmnospin.setSelection(0);
															}*/
														    Showroomvalue();
															
													}
												}
												catch(Exception e){}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		 }
	}
	private void Showroomvalue() {
		// TODO Auto-generated method stub
		   Cursor rm_retrieve= cf.SelectTablefunction(cf.DRY_room, " Where fld_srid='"+cf.selectedhomeid+"' ");
	       System.out.println("rm_retrieve"+rm_retrieve.getCount());
		   if(rm_retrieve.getCount()>0)
			{
	    	   //((LinearLayout)findViewById(R.id.drylin)).setVisibility(cf.v1.VISIBLE);
	    	   if(!((EditText)findViewById(R.id.roomcomment)).getText().toString().trim().equals("")){((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);}
	    	   rm_retrieve.moveToFirst();
				try
				{
					roomtblshow.removeAllViews();
				}
				catch (Exception e) {
					// TODO: handle exception
				}
				LinearLayout h1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvac_listview_header, null); 
				TableRow thv = (TableRow)h1.findViewById(R.id.attic);thv.setVisibility(cf.v1.GONE);
				TableRow thvac = (TableRow)h1.findViewById(R.id.hvac);thvac.setVisibility(cf.v1.GONE);
				LinearLayout th= (LinearLayout)h1.findViewById(R.id.room);th.setVisibility(cf.v1.VISIBLE);
				TableLayout.LayoutParams lp = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
			 	lp.setMargins(2, 0, 2, 2);h1.removeAllViews(); 
			 	
			 	roomtblshow.addView(th,lp);
				room_in_DB=new String[rm_retrieve.getCount()];
				roomtblshow.setVisibility(View.VISIBLE); 
				System.out.println("asf");
				for(int i=0;i<rm_retrieve.getCount();i++)
				{System.out.println("asfinside");
					TextView no;
					final TextView rmnum;
					final TextView rmname;
					TextView rmoutacess, rmouteval, rmswiacess, rmswieval, rminv, rmsulf, rmconf, rmcop, rmmet, rmdrk;
					ImageView edit,delete,duplicate;
					String rmsp="",rmnamesp="",rmnamstr="",rmotacs="",rmotevl="",rmswiacs="",rmtswievl="",rminvs="",rmslf="",rmprs="",rmcc="",rmmc="",rmds="";
					rmsp=rm_retrieve.getString(2);
					rmnamesp=rm_retrieve.getString(3);
					rmnamstr=cf.decode(rm_retrieve.getString(4));
				    rmotacs=rm_retrieve.getString(5);
					rmotevl=rm_retrieve.getString(6);
					rmswiacs=rm_retrieve.getString(7);
					rmtswievl=rm_retrieve.getString(8);
					rminvs=rm_retrieve.getString(9);
					rmslf=rm_retrieve.getString(10);
					rmprs=rm_retrieve.getString(11);
					rmcc=rm_retrieve.getString(12);
					rmmc=rm_retrieve.getString(13);
					rmds=rm_retrieve.getString(14);
					System.out.println("rmds"+rmsp);
					room_in_DB[i]=rmsp;
					System.out.println("asfbfr");
					LinearLayout t1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvac_listview, null);
					LinearLayout t = (LinearLayout)t1.findViewById(R.id.room);t.setVisibility(cf.v1.VISIBLE);
					System.out.println("asfroom");
					TableRow at = (TableRow)t1.findViewById(R.id.attic);at.setVisibility(cf.v1.GONE);
					TableRow hv = (TableRow)t1.findViewById(R.id.hvac);hv.setVisibility(cf.v1.GONE);
					t.setId(44444+i);/// Set some id for further use
					System.out.println("asfsdfsdf");
				 	no= (TextView) t.findViewWithTag("No");
				 	no.setText(String.valueOf(i+1));
				 	rmnum= (TextView) t.findViewWithTag("RoomNum");
				 	rmnum.setText(rmsp);
				 	rmname= (TextView) t.findViewWithTag("RoomName");
				 	if(rmnamesp.equals("Other"))
				 	{
				 		rmname.setText(rmnamesp+"("+rmnamstr+")");
				 	}
				 	else
				 	{
				 		rmname.setText(rmnamesp);
				 	}
				 	rmoutacess= (TextView) t.findViewWithTag("OutletsAcess");
				 	rmoutacess.setText(rmotacs);
				 	rmouteval= (TextView) t.findViewWithTag("OutletsEval");
				 	rmouteval.setText(rmotevl);
				 	rmswiacess= (TextView) t.findViewWithTag("SwitchAcess");
				 	rmswiacess.setText(rmswiacs);
				 	rmswieval= (TextView) t.findViewWithTag("SwitchEval");
				 	rmswieval.setText(rmtswievl);
				 	rminv= (TextView) t.findViewWithTag("Invasive");
				 	rminv.setText(rminvs);
				 	rmsulf= (TextView) t.findViewWithTag("Sulfur");
					rmsulf.setText(rmslf);
					rmconf= (TextView) t.findViewWithTag("Presence");
					rmconf.setText(rmprs);
					rmcop= (TextView) t.findViewWithTag("Copper");
				 	rmcop.setText(rmcc);
				 	rmmet= (TextView) t.findViewWithTag("Metal");
				 	rmmet.setText(rmmc);
				 	rmdrk= (TextView) t.findViewWithTag("Darkening");
				 	rmdrk.setText(rmds);
				 	
				 	edit= (ImageView) t.findViewWithTag("edit");
				 	edit.setId(789456+i);
				 	edit.setTag(rm_retrieve.getString(0));
	                edit.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
						try
						{
						int i=Integer.parseInt(v.getTag().toString());
						
						  vrint=1;updateroom(i);
						}
						catch (Exception e) {
							// TODO: handle exception
						}
						}
	                });
						
				 	delete=(ImageView) t.findViewWithTag("del");
				 	delete.setTag(rm_retrieve.getString(0));
	                delete.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(final View v) {
							// TODO Auto-generated method stub
							
							AlertDialog.Builder builder = new AlertDialog.Builder(DrywallRoom.this);
							builder.setMessage("Do you want to delete the selected Visual Assessment Room Number details?")
									.setCancelable(false)
									.setPositiveButton("Yes",
											new DialogInterface.OnClickListener() {

												public void onClick(DialogInterface dialog,
														int id) {
													
													try
													{
													int i=Integer.parseInt(v.getTag().toString());
													if(i==roomCurrent_select_id)
													{
														roomCurrent_select_id=0;
													}
													cf.arr_db.execSQL("Delete from "+cf.DRY_room+" Where roomId='"+i+"'");
													cf.ShowToast("Visual Assessment Room deleted successfully.", 0);
													defaultroom();rmnospin.setSelection(0);//rmnospin.setEnabled(true);
											        ((Button)findViewById(R.id.addroom)).setText("Save/Add More Assessment Room"); 
											        Showroomvalue();vrint=0;
													}
													catch (Exception e) {
														// TODO: handle exception
													}
												}
											})
									.setNegativeButton("No",
											new DialogInterface.OnClickListener() {
												public void onClick(DialogInterface dialog,
														int id) {

													dialog.cancel();
												}
											});
							AlertDialog al=builder.create();
							al.setTitle("Confirmation");
							al.setIcon(R.drawable.alertmsg);
							al.setCancelable(false);
							al.show();
						
							
						}
					});
	                
	                duplicate= (ImageView) t.findViewWithTag("dup");
	                duplicate.setId(789456+i);
	                duplicate.setTag(rm_retrieve.getString(0));
	                duplicate.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
						try
						{
						final int i=Integer.parseInt(v.getTag().toString());
						String rmno="";
						try{
							Cursor rmret=cf.SelectTablefunction(cf.DRY_room, " Where roomId='"+i+"'");
							if(rmret.getCount()>0)
							{
								rmret.moveToFirst();
								rmno=rmret.getString(2);
								
							}
						}catch(Exception e){}
						AlertDialog.Builder builder = new AlertDialog.Builder(DrywallRoom.this);
						builder.setMessage("Do you want to duplicate the Room Number "+rmno+" ?")
								.setCancelable(false)
								.setPositiveButton("Yes",
										new DialogInterface.OnClickListener() {

											public void onClick(DialogInterface dialog,
													int id) {
												LayoutInflater factory = LayoutInflater.from(DrywallRoom.this);
                                                final View textEntryView = factory.inflate(R.layout.duplicatealert, null);
                                                final EditText input1 = (EditText) textEntryView.findViewById(R.id.dupid);
                                                
                                                InputFilter[] FilterArray = new InputFilter[1];  
                        						FilterArray[0] = new InputFilter.LengthFilter(1);  
                        						input1.setFilters(FilterArray);
                                                
												input1.setText("", TextView.BufferType.EDITABLE);
												
												final AlertDialog.Builder alert = new AlertDialog.Builder(DrywallRoom.this);
												alert.setCancelable(false);
												alert.setIcon(R.drawable.yell_alert_icon).setTitle(
												"Duplication:").setView(
												textEntryView).setPositiveButton("Duplicate",
												new DialogInterface.OnClickListener() {
												public void onClick(DialogInterface dialog,
												int whichButton) {
													 cf.hidekeyboard(input1);
													try{
														Cursor rmret=cf.SelectTablefunction(cf.DRY_room, " Where roomId='"+i+"'");
														if(rmret.getCount()>0)
														{
															rmret.moveToFirst();
															String rmsp="",rmnamesp="",rmnamstr="",rmotacs="",rmotevl="",rmswiacs="",rmswievl="",rminvs="",rmslf="",rmprs="",rmcc="",rmmc="",rmds="";
															rmsp=rmret.getString(2);
															rmnamesp=rmret.getString(3);
															System.out
																	.println("rmnamesp="+rmnamesp);
															/*if(rmnamesp.matches(".*\\d.*")){
																System.out
																		.println("insode");
															   // rmnamesp = rmnamesp.replaceAll("[0-9]", "").trim();
															    System.out
																.println("rmnamespreplace="+rmnamesp);
															} else{
															    
															}*/
															
															rmnamstr=cf.decode(rmret.getString(4));
															
															rmotacs=rmret.getString(5);
															rmotevl=rmret.getString(6);
															rmswiacs=rmret.getString(7);
															rmswievl=rmret.getString(8);
															rminvs=rmret.getString(9);
															rmslf=rmret.getString(10);
															rmprs=rmret.getString(11);
															rmcc=rmret.getString(12);
															rmmc=rmret.getString(13);
															rmds=rmret.getString(14);
															int j;
															
															for(j=0;j<Integer.parseInt(input1.getText().toString());)
															{
																Cursor cr = cf.arr_db.rawQuery("SELECT MAX(roomnum) as id FROM "+cf.DRY_room +" where fld_srid='"+cf.selectedhomeid+"'", null);
																if(cr.getCount()>0)
																{
																	cr.moveToFirst();
																	 getid = cr.getString(cr.getColumnIndex("id"));
																	System.out
																			.println("getid="+getid);
																	
																}
																System.out
																		.println("rmnamesp= "+rmnamesp+getid);
																cf.arr_db.execSQL("INSERT INTO "
																		+ cf.DRY_room
																		+ " (fld_srid,roomnum,roomname,rmnameotr,outletsaccess,outletseval,switchaccess,switcheval,invasive,sulfur,"+
																		 "confpresence,obscopcorrosion,obsmetcorrosion,darkstains)"
																		+ "VALUES('"+cf.selectedhomeid+"','"+getid+"','"+rmnamesp.trim()+"','"+cf.encode(rmnamstr)+"',"+
																        "'"+rmotacs+"','"+rmotevl+"','"+rmswiacs+"','"+rmswievl+"','"+rminvs+"','"+rmslf+"','"+rmprs+"',"+
																        "'"+rmcc+"','"+rmmc+"','"+rmds+"')");
																	j=j+1;
																	/*if(str_arrlst.contains((rmnamesp.trim()+" "+(Integer.parseInt(getid)+1))))
																	{
																          System.out
																				.println("contaoins");
																	}
																	else
																	{
																		System.out
																				.println("not contains");
																		str_arrlst.add(rmnamesp.trim()+" "+(Integer.parseInt(getid)+1));
																	}
																	if(str_rmnumarrlst.contains(Integer.parseInt(getid)+1))
																	{
																          System.out
																				.println("numcontains");
																	}
																	else
																	{
																		System.out
																				.println("num not contains");
																		str_rmnumarrlst.add(String.valueOf(Integer.parseInt(getid)+1));
																	}
																	rmnamespin.setAdapter(rmnameadap);
																	rmnospin.setAdapter(rmnoadap);*/
															}
															 
															 defaultroom();      
															 rmnospin.setSelection(0);//rmnospin.setEnabled(true);'
															 cf.ShowToast("Data Duplicated successfully.", 1);
															 Showroomvalue();
															
															 dialog.dismiss();
															
															
														}
													}catch(Exception e){}
													dialog.dismiss();
												
												}
												}).setNegativeButton("Cancel",
												new DialogInterface.OnClickListener() {
												public void onClick(DialogInterface dialog,
												int whichButton) {
													dialog.dismiss();
												}
												});
												alert.show();
											
											}
										})
								.setNegativeButton("No",
										new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog,
													int id) {

												dialog.cancel();
											}
										});
						AlertDialog al=builder.create();
						al.setTitle("Confirmation");
						al.setIcon(R.drawable.alertmsg);
						al.setCancelable(false);
						al.show();
						 
						}
						catch (Exception e) {
							// TODO: handle exception
						}
						}
	                });
	               
	                
	                t1.removeAllViews();
					roomtblshow.addView(t,lp);
					rm_retrieve.moveToNext();
				}
			}else{roomtblshow.setVisibility(View.GONE);
			//((LinearLayout)findViewById(R.id.drylin)).setVisibility(cf.v1.GONE);
			((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.GONE); }
	}
	protected void updateroom(int i) {
		// TODO Auto-generated method stub
		roomCurrent_select_id = i;//rmnospin.setEnabled(false);
		try{Cursor rmret=cf.SelectTablefunction(cf.DRY_room, " Where roomId='"+i+"'");
		if(rmret.getCount()>0)
		{
			rmret.moveToFirst();
			String rmsp="",rmnamesp="",rmnamstr="",rmotacs="",rmotevl="",rmswiacs="",rmswievl="",rminvs="",rmslf="",rmprs="",rmcc="",rmmc="",rmds="";
			rmsp=rmret.getString(2);
			rmnamesp=rmret.getString(3);
			rmnamstr=cf.decode(rmret.getString(4));
			rmotacs=rmret.getString(5);
			rmotevl=rmret.getString(6);
			rmswiacs=rmret.getString(7);
			rmswievl=rmret.getString(8);
			rminvs=rmret.getString(9);
			rmslf=rmret.getString(10);
			rmprs=rmret.getString(11);
			rmcc=rmret.getString(12);
			rmmc=rmret.getString(13);
			rmds=rmret.getString(14);
			
			int pos=rmnoadap.getPosition(rmsp);rmnospin.setSelection(pos);
			int nampos=rmnameadap.getPosition(rmnamesp);
			if(rmnamesp.equals("Other"))
			{
				((LinearLayout)findViewById(R.id.rmnamelin)).setVisibility(cf.v1.VISIBLE);
				((EditText)findViewById(R.id.roomname_ettxt)).setText(rmnamstr);
			}
			else
			{
				((LinearLayout)findViewById(R.id.rmnamelin)).setVisibility(cf.v1.GONE);
			}
			rmnamespin.setSelection(nampos);
			int oacspos=rmoutletsadap.getPosition(rmotacs);rmoutacessspin.setSelection(oacspos);
			int oevlpos=rmoutletsevaladap.getPosition(rmotevl);rmoutevalspin.setSelection(oevlpos);
			int sacspos=rmswiadap.getPosition(rmswiacs);rmswiaccessspin.setSelection(sacspos);
			int sevlpos=rmswievaladap.getPosition(rmswievl);rmswievalspin.setSelection(sevlpos);
			int invpos=rminvadap.getPosition(rminvs);rminvaspin.setSelection(invpos);
			int slfpos=rmsuladap.getPosition(rmslf);rmsulfspin.setSelection(slfpos);
			int ppos=rmconfadap.getPosition(rmprs);rmconfspin.setSelection(ppos);
			int copps=rmcopadap.getPosition(rmcc);rmcopspin.setSelection(copps);
			int metpos=rmmetcopadap.getPosition(rmmc);rmmetcorspin.setSelection(metpos);
			int drkpos=rmdrkadap.getPosition(rmds);rmdrkspin.setSelection(drkpos);
			
			((Button)findViewById(R.id.addroom)).setText("Update");
		}
		}catch(Exception e){}
	}
	private void chk_values() {
		// TODO Auto-generated method stub
		try
		{
			 Drysum_save = cf.SelectTablefunction(cf.DRY_sumcond, " where fld_srid='"+cf.selectedhomeid+"'");
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		
		try{
			Dryroom_save=cf.SelectTablefunction(cf.DRY_room, " where fld_srid='"+cf.selectedhomeid+"'");
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			cf.goclass(61);
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		  if(requestCode==cf.loadcomment_code)
		  {
			load_comment=true;
			if(resultCode==RESULT_OK)
			{
				 cf.ShowToast("Comments added successfully.", 0);
				 String atccomts = ((EditText)findViewById(R.id.roomcomment)).getText().toString();
				 ((EditText)findViewById(R.id.roomcomment)).setText((atccomts+" "+data.getExtras().getString("Comments")).trim());
				
			}
			
		}
    }
}
