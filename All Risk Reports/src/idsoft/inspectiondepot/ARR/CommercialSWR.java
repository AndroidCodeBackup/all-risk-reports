/*
************************************************************
* Project: ALL RISK REPORT
* Module : WindMitCommareas.java
* Creation History:
Created By : Nalini on 10/24/2012
* Modification History:
Last Modified By : Gowri on 04/04/2013
************************************************************ 
*/

package idsoft.inspectiondepot.ARR;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.CommercialRC.RCrdioclicker;
import idsoft.inspectiondepot.ARR.WindMitCommAuxbuildings.Touch_Listener;
import idsoft.inspectiondepot.ARR.WindMitCommAuxbuildings.textwatcher;
import idsoft.inspectiondepot.ARR.WindMitCommRestsupp.RestSupp_EditClick;
import idsoft.inspectiondepot.ARR.WindMitCommRestsupp.Spin_Selectedlistener;
import idsoft.inspectiondepot.ARR.WindMitCommRestsupp.checklistenetr;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class CommercialSWR extends Activity{
	CommonFunctions cf;
	String c1_q3[] = { "Underlayment","Foamed Adhesive","Not Applicable","Not Inspected","Unknown"};
	
	String swrvalue="",tmp="";
	RadioButton swrrdio[] = new RadioButton[5];
	int[] swrrdioid = {R.id.swr_chk1,R.id.swr_chk2,R.id.swr_chk3,R.id.swr_chk4,R.id.swr_chk5};
	boolean load_comment=true;
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cf=new CommonFunctions(this);	 
        Bundle extras = getIntent().getExtras();
  		if (extras != null) {
  			cf.getExtras(extras);				
  	 	}
  		  setContentView(R.layout.commwindswr);
          cf.getDeviceDimensions();
          
          if(cf.identityval==32){ cf.typeidentity=309;}
          else if(cf.identityval==33){cf.typeidentity=310;}
          else if(cf.identityval==34){cf.typeidentity=310;}
         
          LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
          if(cf.identityval==33)
          {
        	  hdr_layout.addView(new HdrOnclickListener(this,1,"Commercial Type II","Commercial Wind","SWR",3,1,cf));
          }
          else if(cf.identityval==34)
          {
        	  hdr_layout.addView(new HdrOnclickListener(this,1,"Commercial Type III","Commercial Wind","SWR",3,1,cf));
          }
          cf.mainmenu_layout = (LinearLayout) findViewById(R.id.header); //** HEADER MENU **//
          cf.mainmenu_layout.setMinimumWidth(cf.wd);
          cf.mainmenu_layout.addView(new MyOnclickListener(this, 3, 0,0, cf));
         
          cf.type_submenulayout =(LinearLayout) findViewById(R.id.typesubmenu); //** WINDMIT TYPE SUBMENU **//
          cf.type_submenulayout.addView(new MyOnclickListener(this, 300, 1,0, cf));
          cf.windmittypechk(cf.identityval,((RadioButton)findViewById(R.id.b1802_chk)),((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((TextView)findViewById(R.id.commtxt)),((ImageView)findViewById(R.id.editbno))); // To check the checkbox of choosen inspection type
          cf.uncheckcommercial(((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((RadioButton)findViewById(R.id.b1802_chk))); 
      	     
          System.out.println("cf.ide"+cf.identityval+"type"+cf.typeidentity);
          cf.submenu_layout = (LinearLayout) findViewById(R.id.submenu);//** WINDMIT TYPE SUBMENU **//
          cf.submenu_layout.addView(new MyOnclickListener(this,cf.identityval,1,314, cf));
          
          cf.innersubmenu_layout = (LinearLayout) findViewById(R.id.innersubmenu);//** WINDMIT TYPE SUBMENU **//
          cf.innersubmenu_layout.addView(new MyOnclickListener(this,cf.identityval,1,126, cf));
          
          cf.mainlayout = (LinearLayout)findViewById(R.id.bc_table);
          cf.save = (Button)findViewById(R.id.save);
          cf.tblrw = (TableRow)findViewById(R.id.row2);
          cf.tblrw.setMinimumHeight(cf.ht);			
          Declaration(); cf.CreateARRTable(40);setValue();
    }
	private void setValue() {
		// TODO Auto-generated method stub
		try
		{
			Cursor c1 = cf.arr_db.rawQuery("SELECT * FROM " + cf.Commercial_Wind + " WHERE fld_srid='"+ cf.selectedhomeid + "'  and insp_typeid='"+cf.identityval+"'", null);				
			if(c1.getCount()>0)
			{
				c1.moveToFirst();
				String swrvalue = c1.getString(c1.getColumnIndex("fld_swr"));
				if(!"".equals(swrvalue))
				{
					for(int i=0;i<swrrdio.length;i++)
					{
						if(String.valueOf(swrvalue).equals(swrrdio[i].getTag().toString()))
						{
							swrrdio[i].setChecked(true);
						
						}
					}
					   ((TextView)findViewById(R.id.savetxtswr)).setVisibility(cf.v1.VISIBLE);
				}
				else
				{
					   ((TextView)findViewById(R.id.savetxtswr)).setVisibility(cf.v1.GONE);
				}
				   String swrcomments = cf.decode(c1.getString(c1.getColumnIndex("fld_swrcomments")));
				   if(!swrcomments.equals(""))
				   {
					   ((EditText)findViewById(R.id.swrcomment)).setText(swrcomments);
				   }
				
			}
			else
			{
				((TextView)findViewById(R.id.savetxtswr)).setVisibility(cf.v1.GONE);	
			}
		}catch(Exception e)
		{
			System.out.println("catch 1"+e.getMessage());
		}
	}
	private void Declaration() {
		// TODO Auto-generated method stub
		cf.setupUI((ScrollView) findViewById(R.id.scr));
		for(int i=0;i<swrrdio.length;i++)
	   	 {
			swrrdio[i] = (RadioButton)findViewById(swrrdioid[i]); 
			swrrdio[i].setOnClickListener(new SWRrdioclicker());	
	   	 }	
		 ((EditText)findViewById(R.id.swrcomment)).addTextChangedListener(new textwatcher());
	}
	class textwatcher implements TextWatcher
    {
	         public int type;
	         textwatcher()
	    	{
	        	
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {


	    		// TODO Auto-generated method stub
	    		cf.showing_limit(s.toString(),(TextView)findViewById(R.id.swr_tv_type1),500);
	    		if(s.toString().startsWith(" "))
	    		{
	    			((EditText)findViewById(R.id.swrcomment)).setText("");
	    		}
	    		}

	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,

	    			int after) { 
	    		// TODO Auto-generated method stub
	     	}

	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,

	    			int count) {
	    		// TODO Auto-generated method stub
	    	}
	   }	
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.loadcomments:
			/***Call for the comments***/
			cf.findinspectionname(cf.identityval);SWR_suboption();
			int len=((EditText)findViewById(R.id.swrcomment)).getText().toString().length();			
			if(!tmp.equals(""))
			{
					if(load_comment)
					{
						load_comment=false;
						int loc1[] = new int[2];
						v.getLocationOnScreen(loc1);
						Intent in = cf.loadComments(tmp,loc1);
						in.putExtra("insp_name", cf.inspname);
						in.putExtra("insp_ques", "SWR");
						in.putExtra("length", len);
						in.putExtra("max_length", 500);
						startActivityForResult(in, cf.loadcomment_code);
					}
			}
			else
			{
				cf.ShowToast("Please select the option for SWR." , 0);
			}	
			break;
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.save:
			swrvalue=cf.getselecctedtag_radio(swrrdio);	
			if(swrvalue.equals(""))
			{
				cf.ShowToast("Please select the option for SWR." , 0);
			}
			else
			{
				InsertSWR();
			}
			break;
		}
	}
	private void SWR_suboption() {
		// TODO Auto-generated method stub
		 for(int i=0;i<swrrdio.length;i++)
    	 {
			 if(swrrdio[i].isChecked())
			 {
				tmp = c1_q3[i];
			 }
    	 }
	}
	class  SWRrdioclicker implements OnClickListener
	{
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			for(int i=0;i<swrrdio.length;i++)
			{
				if(v.getId()==swrrdio[i].getId())
				{
					swrrdio[i].setChecked(true);					
				}
				else
				{
					swrrdio[i].setChecked(false);
				}
			}
		}
	}
	private void InsertSWR() {
		// TODO Auto-generated method stub
		if(((EditText)findViewById(R.id.swrcomment)).getText().toString().trim().equals(""))
		{
			cf.ShowToast("Please enter the SWR Comments.",1);
			cf.setFocus(((EditText)findViewById(R.id.swrcomment)));
		}
		else
		{
			try		
			{
				Cursor c1 =cf.SelectTablefunction(cf.Commercial_Wind, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
				if(c1.getCount()==0)
				{
					cf.arr_db.execSQL("INSERT INTO "
							+ cf.Commercial_Wind
							+ " (fld_srid,insp_typeid,fld_swr,fld_swrcomments)"
							+ "VALUES ('"+cf.selectedhomeid+"','"+cf.identityval+"','"+swrvalue+"','"+cf.encode(((EditText)findViewById(R.id.swrcomment)).getText().toString().trim())+"')");
				}
				else
				{
					cf.arr_db.execSQL("UPDATE "
						+ cf.Commercial_Wind
						+ " SET fld_swr='" + swrvalue + "',fld_swrcomments='"+cf.encode(((EditText)findViewById(R.id.swrcomment)).getText().toString().trim())+"'"
						+ " WHERE fld_srid ='"+ cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'");			
				
				}
				cf.ShowToast("SWR saved successfully",1);
				cf.gotoclass(cf.identityval, CommercialOP.class);
			}
			catch(Exception e)
			{
				System.out.println("catch inside "+e.getMessage());
			}
		}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
 		// replaces the default 'Back' button action
 		if (keyCode == KeyEvent.KEYCODE_BACK) {
 			cf.gotoclass(cf.identityval, CommercialRD.class);
 			return true;
 		}
 		
 		return super.onKeyDown(keyCode, event);
 	}
	 /**on activity result for the load comments**/
	 @Override
	 		protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	 		try
	 		{
	 			if(requestCode==cf.loadcomment_code)
	 			{
	 				load_comment=true;
	 				if(resultCode==RESULT_OK)
	 				{
	 					String swrcomm = ((EditText)findViewById(R.id.swrcomment)).getText().toString();	 
	 					((EditText)findViewById(R.id.swrcomment)).setText(swrcomm +" "+data.getExtras().getString("Comments"));
		 			}
	 			}
	 		}
	 		catch (Exception e) {
	 			// TODO: handle exception
	 			System.out.println("the erro was "+e.getMessage());
	 		}
	 	}/**on activity result for the load comments ends**/
}