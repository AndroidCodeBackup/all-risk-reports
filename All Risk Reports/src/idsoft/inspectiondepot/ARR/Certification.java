/*
************************************************************
* Project: ALL RISK REPORT
* Module : WindMitCommareas.java
* Creation History:
Created By : Nalini on 10/24/2012
* Modification History:
Last Modified By : Gowri on 04/04/2013
************************************************************ 
*/

package idsoft.inspectiondepot.ARR;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.CommercialRD.RDrdioclicker;
import idsoft.inspectiondepot.ARR.WindMitCommAuxbuildings.Touch_Listener;
import idsoft.inspectiondepot.ARR.WindMitCommAuxbuildings.textwatcher;
import idsoft.inspectiondepot.ARR.WindMitCommRestsupp.RestSupp_EditClick;
import idsoft.inspectiondepot.ARR.WindMitCommRestsupp.Spin_Selectedlistener;
import idsoft.inspectiondepot.ARR.WindMitCommRestsupp.checklistenetr;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class Certification extends Activity{
	CommonFunctions cf;
	String certvalue="",s="";
	RadioButton certirdio[] = new RadioButton[6];
	int[] certid = {R.id.certrdgp1,R.id.certrdgp2,R.id.certrdgp3,R.id.certrdgp4,R.id.certrdgp5,R.id.certrdgp6};
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cf=new CommonFunctions(this);	 
        Bundle extras = getIntent().getExtras();
  		if (extras != null) {
  			cf.getExtras(extras);				
  	 	}
  		  setContentView(R.layout.commcertification);
          cf.getDeviceDimensions();
          
          if(cf.identityval==32){ cf.typeidentity=309;}
          else if(cf.identityval==33){cf.typeidentity=310;}
          else if(cf.identityval==34){cf.typeidentity=310;}         
          
          LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
          if(cf.identityval==33)
          {
            hdr_layout.addView(new HdrOnclickListener(this,1,"Commercial Type II","Commercial Wind","Certification",3,1,cf));
          }
          else if(cf.identityval==34)
          {
        	  hdr_layout.addView(new HdrOnclickListener(this,1,"Commercial Type III","Commercial Wind","Certification",3,1,cf));
          }
          
          cf.mainmenu_layout = (LinearLayout) findViewById(R.id.header); //** HEADER MENU **//
          cf.mainmenu_layout.setMinimumWidth(cf.wd);
          cf.mainmenu_layout.addView(new MyOnclickListener(this, 3, 0,0, cf));
         
          cf.type_submenulayout =(LinearLayout) findViewById(R.id.typesubmenu); //** WINDMIT TYPE SUBMENU **//
          cf.type_submenulayout.addView(new MyOnclickListener(this, 300, 1,0, cf));
          cf.windmittypechk(cf.identityval,((RadioButton)findViewById(R.id.b1802_chk)),((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((TextView)findViewById(R.id.commtxt)),((ImageView)findViewById(R.id.editbno))); // To check the checkbox of choosen inspection type
          cf.uncheckcommercial(((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((RadioButton)findViewById(R.id.b1802_chk))); 
      	    cf.submenu_layout = (LinearLayout) findViewById(R.id.submenu);//** WINDMIT TYPE SUBMENU **//
          cf.submenu_layout.addView(new MyOnclickListener(this,cf.identityval,1,314, cf));
          
          cf.innersubmenu_layout = (LinearLayout) findViewById(R.id.innersubmenu);//** WINDMIT TYPE SUBMENU **//
          cf.innersubmenu_layout.addView(new MyOnclickListener(this,cf.identityval,1,128, cf));   
          
          cf.mainlayout = (LinearLayout)findViewById(R.id.bc_table);
          cf.save = (Button)findViewById(R.id.save);
          cf.tblrw = (TableRow)findViewById(R.id.row2);
          cf.tblrw.setMinimumHeight(cf.ht);	
          cf.CreateARRTable(40);
          cf.CreateARRTable(3);
          Declaration();
          setValue();
          cf.getInspectorId();         
          display_insp_image();
    }
	private void display_insp_image() {
		// TODO Auto-generated method stub
		try
		{
			Cursor c = cf.SelectTablefunction(cf.inspectorlogin," where Fld_InspectorId='" + cf.Insp_id.toString() + "'");
			if(c.getCount()>0)
			{
				c.moveToFirst();
				String inspext = cf.decode(c.getString(c.getColumnIndex("Fld_signature")));
				File outputFile = new File(this.getFilesDir()+"/"+inspext);
				BitmapFactory.Options o = new BitmapFactory.Options();
				o.inJustDecodeBounds = true;
				BitmapFactory.decodeStream(new FileInputStream(outputFile),null, o);
				final int REQUIRED_SIZE = 200;
				int width_tmp = o.outWidth, height_tmp = o.outHeight;
				int scale = 1;
				while (true) 
				{
					if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE)
					break;
					width_tmp /= 2;
					height_tmp /= 2;
					scale *= 2;
				}
				BitmapFactory.Options o2 = new BitmapFactory.Options();
				o2.inSampleSize = scale;
				Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(
						outputFile), null, o2);
				BitmapDrawable bmd = new BitmapDrawable(bitmap);
				((ImageView)findViewById(R.id.headshotimg)).setImageDrawable(bmd);
			}
		}
		catch (IOException e)
		{
			System.err.println("ISOS"+e.getMessage());
			cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ Certification.this+" "+" in the stage of retrieving inspector image at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
	private void setValue() {
		// TODO Auto-generated method stub
		try
		{
			Cursor c1 = cf.arr_db.rawQuery("SELECT * FROM " + cf.Commercial_Wind + " WHERE fld_srid='"+ cf.selectedhomeid + "'  and insp_typeid='"+cf.identityval+"'", null);
			System.out.println("crecout"+c1.getCount());
			if(c1.getCount()>0)
			{
				c1.moveToFirst();
				String certvalue = cf.decode(c1.getString(c1.getColumnIndex("fld_certfication")));		
				System.out.println("cre="+certvalue);
				if(!certvalue.equals(""))
				{
					for(int i=0;i<certirdio.length;i++)
					{
						if(String.valueOf(certvalue).equals(certirdio[i].getTag().toString()))
						{
							certirdio[i].setChecked(true);						
						}
					}
					((TextView)findViewById(R.id.savetxtcert)).setVisibility(cf.v1.VISIBLE);
				}
				else
				{
					 ((TextView)findViewById(R.id.savetxtcert)).setVisibility(cf.v1.GONE);
					 licenceauto();
				}
			}
			else
			{
				 ((TextView)findViewById(R.id.savetxtcert)).setVisibility(cf.v1.GONE);
				 licenceauto();
				 
			}
		}catch(Exception e)
		{
			System.out.println("catch 1"+e.getMessage());
		}
	}
	private void licenceauto() {
		// TODO Auto-generated method stub
		 Cursor c2 = cf.arr_db.rawQuery("SELECT * FROM " + cf.inspectorlogin + " WHERE Fld_InspectorId='" + cf.encode(cf.Insp_id) + "'", null);
		  if(c2.getCount()>0)
		  {
			  c2.moveToFirst();
			  String licencetype = cf.decode(c2.getString(c2.getColumnIndex("Fld_licencetype")));
			  String[] l2 = getResources().getStringArray(R.array.licenced_contractor);		
			  String[] l5 = getResources().getStringArray(R.array.licenced_certified);		
			  
			  System.out.println("licencetype="+licencetype);
			  
			  System.out.println("licen11e="+getResources().getString(R.string.lic_type1));
			  System.out.println("licen33e="+getResources().getString(R.string.lic_type3));
			  System.out.println("licen44e="+getResources().getString(R.string.lic_type4));
			  
			  System.out.println("Arrays.asListl2"+Arrays.asList(l2));
			  System.out.println("Arrays.asListl5"+Arrays.asList(l5));
			  
			  if(getResources().getString(R.string.lic_type1).contains(licencetype)) 
			  {
				  certirdio[1].setChecked(true);
			  }
			  else if(Arrays.asList(l2).contains(licencetype)) 
			  {
				  certirdio[2].setChecked(true);
			  }
			  if(getResources().getString(R.string.lic_type3).contains(licencetype)) 
			  {
				  certirdio[3].setChecked(true);
			  }
			  else if(getResources().getString(R.string.lic_type4).contains(licencetype))
			  {
				  certirdio[4].setChecked(true);
			  }
			  else if(Arrays.asList(l5).contains(licencetype)) 
			  {
				  certirdio[5].setChecked(true);
			  }
			  else
			  {
				  System.out.println("inside last else");
			  }
		  }
	}
	private void Declaration() {
		// TODO Auto-generated method stub
		 for(int i=0;i<certirdio.length;i++)
	   	 {
			 certirdio[i] = (RadioButton)findViewById(certid[i]); 	
	   	 }	
		 ((TextView)findViewById(R.id.titletext)).setText(Html
					.fromHtml("<font color=black>I, "
							+ "</font>"
							+ "<b><font color=#bddb00>"
							+ cf.Insp_firstname.toUpperCase()+" "+cf.Insp_lastname.toUpperCase()
							+ "</font></b><font color=black> certify that I am (CHECK ONE OF THE FOLLOWING)."
							+ "</font>"));
		 ((TextView)findViewById(R.id.tv1)).setText(Html
					.fromHtml("<font color=black>I, "
							+ "</font>"
							+ "<b><font color=#bddb00>"
							+ cf.Insp_firstname.toUpperCase()+" "+cf.Insp_lastname.toUpperCase()
							+ "</font></b><font color=black> also certify that I personally inspected the premises at the Location Address listed above on the inspection date provided on this Affidavit. In my personal opinion, based on my knowledge, information and belief, I certify that the above statements are true and correct."
							+ "</font>"));
	}
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.save:
			certvalue=getselecctedtxt_radio(certirdio);	
			if(certvalue.equals(""))
			{
				cf.ShowToast("Please select the option for Certification." , 0);
			}
			else
			{
				Insertcertifcation();
			}
			break;
		}
	}
	public String getselecctedtxt_radio(RadioButton[] R) {
		// TODO Auto-generated method stub
		for(int i=0;i<R.length;i++)
		{
			if(R[i].isChecked())
			{
				s= R[i].getTag().toString();
			}
		}		
		return s;
	}
	private void Insertcertifcation() {
		// TODO Auto-generated method stub
		try
		{
			System.out.println("certvalue"+certvalue);
			Cursor c1 =cf.SelectTablefunction(cf.Commercial_Wind, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
			if(c1.getCount()==0)
			{
				cf.arr_db.execSQL("INSERT INTO "
						+ cf.Commercial_Wind
						+ " (fld_srid,insp_typeid,fld_certfication)"
						+ "VALUES ('"+cf.selectedhomeid+"','"+cf.identityval+"','"+ cf.encode(certvalue)+"')");
			}
			else
			{
				cf.arr_db.execSQL("UPDATE "
					+ cf.Commercial_Wind
					+ " SET fld_certfication='" + cf.encode(certvalue) + "'"
					+ " WHERE fld_srid ='"+ cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'");		
			}
			cf.ShowToast("Certification saved successfully",1);
			((TextView)findViewById(R.id.savetxtcert)).setVisibility(cf.v1.VISIBLE);
		}
		catch(Exception e)
		{
			System.out.println("catch inside "+e.getMessage());
		}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
 		// replaces the default 'Back' button action
 		if (keyCode == KeyEvent.KEYCODE_BACK) {
 			cf.gotoclass(cf.identityval, CommercialOP.class);
 			return true;
 		}
 		
 		return super.onKeyDown(keyCode, event);
 	}
}