package idsoft.inspectiondepot.ARR;


/******   Page Information
			Version :1.0
			VCode:1
			Created Date:19/10/2012
			Purpose:For Weather Condtion
			Created By:Rakki S
			Last Updated:01/08/2013
			Last Updated By: Rakki s *****/   
  


import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.Plumbing.textwatcher;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;

public class Weather extends Activity {
	CommonFunctions cf;
	String[] Weather_con=null,Weather_temp=null,val=null;
	CheckBox chk=null;
	String Wea_OutDoor="",WEA_NA="",Wea_InDoor="",Wea_Condition="",Wea_Recentweather="",Wea_R_OutTemp="",Wea_R_InTemp="",weatherna ="0";
	//EditText wea_ed[]=new EditText[4]; 
	ArrayAdapter adapter_insp,adapter_temp;
	Spinner sp1,sp2,spnoutdoor,spnindoor,spreloutdoor,sprelindoor;
	@Override
	   public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        cf=new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);
				
		 	}
			Weather_con=getResources().getStringArray(R.array.Weather_condtions);/**its available in the string xml**/
	        setContentView(R.layout.weather);
	        cf.CreateARRTable(8);
	        cf.getDeviceDimensions();
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
	     	hdr_layout.addView(new HdrOnclickListener(this,1,"General","Weather/Temperature",1,0,cf));
	     	LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
         	main_layout.setMinimumWidth(cf.wd);
         	main_layout.addView(new MyOnclickListener(getApplicationContext(), 1, 0,0,cf));
	        LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
	        submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 17, 1,0,cf));
			/*TableRow tblrw = (TableRow)findViewById(R.id.row2);
			tblrw.setMinimumHeight(cf.ht);*/
			sp1=(Spinner) findViewById(R.id.Wea_WC_sp);
			sp2=(Spinner) findViewById(R.id.Wea_RC_sp);
			
			spnoutdoor=(Spinner) findViewById(R.id.Wea_Outdoor_Temp);
			spnindoor=(Spinner) findViewById(R.id.Wea_Inddoor_Temp);
			spreloutdoor=(Spinner) findViewById(R.id.WeaRH_Outdoor_Temp);
			sprelindoor=(Spinner) findViewById(R.id.WeaRH_Indoor_Temp);
			
			Weather_temp=getResources().getStringArray(R.array.Weather_temp);/**its available in the string xml**/
			adapter_temp = new ArrayAdapter(Weather.this,android.R.layout.simple_spinner_item, Weather_temp);/**its available in the string_value xml**/
			adapter_temp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			
			 adapter_insp = new ArrayAdapter(Weather.this,android.R.layout.simple_spinner_item, Weather_con);
			 adapter_insp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			 sp1.setAdapter(adapter_insp);
			 sp2.setAdapter(adapter_insp);
			 
			 
			 spnoutdoor.setAdapter(adapter_temp);
			 spnindoor.setAdapter(adapter_temp);
			 spreloutdoor.setAdapter(adapter_temp);
			 sprelindoor.setAdapter(adapter_temp);
			 
			 chk=(CheckBox) findViewById(R.id.Wea_RH_na_chk);
			 chk.setOnClickListener(new event_driven());
			 cf.setupUI(((View)findViewById(R.id.scr)));

			 Weather_setValue();
	    }
	
	 private void Weather_setValue() {
		// TODO Auto-generated method stub
		 Cursor c= cf.SelectTablefunction(cf.WeatherCondition, " Where Wea_srid='"+cf.selectedhomeid+"'");
		 System.out.println("CCCCCCCCCCCCC="+c.getCount());
		 if(c.getCount()>0)
		 {
			 c.moveToFirst();
			 ((LinearLayout)findViewById(R.id.weatherlin)).setVisibility(cf.v1.VISIBLE);
				((TextView)findViewById(R.id.savetxtweather)).setVisibility(cf.v1.VISIBLE);
				WEA_NA= cf.decode(c.getString(c.getColumnIndex("Wea_NA")));
			 	
			 	
				 if(WEA_NA.equals("1"))
				 {
					 ((CheckBox)findViewById(R.id.weatherna)).setChecked(true);
						((LinearLayout)findViewById(R.id.weatherlin)).setVisibility(cf.v1.GONE);
						((TextView)findViewById(R.id.savetxtweather)).setVisibility(cf.v1.VISIBLE);
				 }
				 else
				 {
					 	Wea_OutDoor = cf.decode(c.getString(c.getColumnIndex("Wea_OutDoor")));
					 	Wea_InDoor = cf.decode(c.getString(c.getColumnIndex("Wea_InDoor")));
					 	
					 	Wea_Condition = cf.decode(c.getString(c.getColumnIndex("Wea_WeatherCondi")));
					 	Wea_Recentweather = cf.decode(c.getString(c.getColumnIndex("Wea_RecentCondi")));
					 	
					 	Wea_R_OutTemp  = c.getString(c.getColumnIndex("Wea_R_outTemp"));
					 	Wea_R_InTemp  = c.getString(c.getColumnIndex("Wea_R_inTemp"));
					 
					 if(!Wea_OutDoor.equals("") && !Wea_OutDoor.equals(""))
					 {
						 ((LinearLayout)findViewById(R.id.weatherlin)).setVisibility(cf.v1.VISIBLE);
						 ((TextView)findViewById(R.id.savetxtweather)).setVisibility(cf.v1.VISIBLE);
					 	 spnoutdoor.setSelection(adapter_temp.getPosition(Wea_OutDoor));
					 	 spnindoor.setSelection(adapter_temp.getPosition(Wea_InDoor));
					 	 sp1.setSelection(adapter_insp.getPosition(Wea_Condition));
					 	 sp2.setSelection(adapter_insp.getPosition(Wea_Recentweather));
					 	 
					 	if(Wea_R_OutTemp.equals("") && (Wea_R_InTemp.equals("")))
						 {
							chk.setChecked(true);
							findViewById(R.id.wea_out_tmp_rw).setVisibility(View.GONE);
							findViewById(R.id.wea_in_tmp_rw).setVisibility(View.GONE);								
						 }
						 else
						 {
							 spreloutdoor.setSelection(adapter_temp.getPosition(cf.decode(c.getString(c.getColumnIndex("Wea_R_outTemp")))));
							 sprelindoor.setSelection(adapter_temp.getPosition(cf.decode(c.getString(c.getColumnIndex("Wea_R_inTemp")))));
							 findViewById(R.id.wea_out_tmp_rw).setVisibility(View.VISIBLE);
							 findViewById(R.id.wea_in_tmp_rw).setVisibility(View.VISIBLE);
							 chk.setChecked(false);
						 }
					 	 
					 }
					 else
					 {
						    ((LinearLayout)findViewById(R.id.weatherlin)).setVisibility(cf.v1.GONE);
							((TextView)findViewById(R.id.savetxtweather)).setVisibility(cf.v1.GONE);
							((CheckBox)findViewById(R.id.weatherna)).setChecked(true);
					 }

						 
				 }
		 }
		 else
		 {
			 /**Set the value to the default**/
			 ((CheckBox)findViewById(R.id.weatherna)).setChecked(true);
				((LinearLayout)findViewById(R.id.weatherlin)).setVisibility(cf.v1.GONE);
			 
			 	chk.setChecked(true);
				findViewById(R.id.wea_out_tmp_rw).setVisibility(View.GONE);
				findViewById(R.id.wea_in_tmp_rw).setVisibility(View.GONE);
			 /***Set the default value **/
		 }
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
			if (keyCode == KeyEvent.KEYCODE_BACK) {
				
				cf.goclass(451);
				return true;
			}
			return super.onKeyDown(keyCode, event);
		}
	 public void clicker(View v)
	 {
		switch (v.getId()) {
		case R.id.weatherna:			
			if(((CheckBox)findViewById(R.id.weatherna)).isChecked())
			{
				weatherna="1";
			
				/*if(!WEA_NA.equals("") || )
				{
					((TextView)findViewById(R.id.savetxtweather)).setVisibility(cf.v1.VISIBLE);
					((LinearLayout)findViewById(R.id.weatherlin)).setVisibility(cf.v1.VISIBLE);
				}
				else
				{*/
					((TextView)findViewById(R.id.savetxtweather)).setVisibility(cf.v1.GONE);
					((LinearLayout)findViewById(R.id.weatherlin)).setVisibility(cf.v1.GONE);
				//}			
			}
			else
			{
				weatherna="0";
				if(Wea_R_OutTemp.equals(""))
				{
					chk.setChecked(true);
				}
				((LinearLayout)findViewById(R.id.weatherlin)).setVisibility(cf.v1.VISIBLE);
				((TextView)findViewById(R.id.savetxtweather)).setVisibility(cf.v1.GONE);
			}
			break;
		case R.id.save:
			
			if(((CheckBox)findViewById(R.id.weatherna)).isChecked())
			{
				 try
				 {
					 Cursor c=cf.SelectTablefunction(cf.WeatherCondition, " Where Wea_srid='"+cf.selectedhomeid+"'");
					 if(c.getCount()>0)
					 {
						 cf.arr_db.execSQL("Update "+cf.WeatherCondition+" SET Wea_NA='1',Wea_date=datetime() Where Wea_srid='"+cf.selectedhomeid+"'");
					 }
					 else
					 {
						 cf.arr_db.execSQL("INSERT INTO "+cf.WeatherCondition+" (Wea_srid ,Wea_NA) VALUES" +
						 		"('"+cf.selectedhomeid+"','1')");
					 }
					 cf.ShowToast("Weather/Temperature information saved successfully", 1);
					 Weather_setValue();
				 }catch (Exception e) {
					// TODO: handle exception
					 System.out.println("error occure"+e.getMessage());
				}
			}
			else
			{
				validation_weather();				 
			}
		break;
		case R.id.hme:
		cf.gohome();	
		break;
		default:
			break;
		} 
	 }	 
	 private void validation_weather() {
		// TODO Auto-generated method stub
		  val=new String[6];
		 val[0]=spnoutdoor.getSelectedItem().toString();
		 val[1]=spnindoor.getSelectedItem().toString();
		 val[2]=sp1.getSelectedItem().toString();
		 val[3]=sp2.getSelectedItem().toString();
		 if(chk.isChecked())
		 {
			 val[4]=val[5]="";
		 }else
		 {
			 val[4]=spreloutdoor.getSelectedItem().toString();
			 val[5]=sprelindoor.getSelectedItem().toString();
		 }
		 String tit[]=getResources().getStringArray(R.array.Weather_question);
		/* for(int i=0;i<tit.length;i++)
		 { String sub=""; 
		 System.out.println("vali eler if"+val[i]);
			 if(val[i].equals("") )
			 {
				 if(i>3 )
				 {System.out.println("5555555 eler if"+val[i]);
					 sub=getResources().getString(R.string.wea_relati);
					 
					 if( !chk.isChecked())
					 {
						 cf.ShowToast("Please enter text for "+tit[i]+" "+sub, 0);
						 return;
					 }
					 
				 }
				 else if(i>=2)
				 {
					 System.out.println("gretaetr than 2222");
					 cf.ShowToast("Please enter text for "+tit[i], 0);
					 return;
					 
				 }
				 
			 }
			 else if(val[i].equals("-Select-"))
			 {
				 if(i>3)
				 {
					 sub=getResources().getString(R.string.wea_relati);
				 }
				 
				 if(i==0 || i==1)
				 {
					 cf.ShowToast("Please Select option for "+tit[i]+" "+sub, 0);
					 return;
				 }
				 System.out.println("iwhat is 1="+i);
				 if(i!=2 && i!=3)				 
				 {
					 cf.ShowToast("Please Select option for "+tit[i]+" "+sub, 0);
				 }
				 return;
			 }
				 
		 }*/
		 try
		 {
			 Cursor c=cf.SelectTablefunction(cf.WeatherCondition, " Where Wea_srid='"+cf.selectedhomeid+"'");
			 if(c.getCount()>0)
			 {
				 cf.arr_db.execSQL("Update "+cf.WeatherCondition+" SET Wea_OutDoor='"+cf.encode(val[0])+"',Wea_InDoor='"+cf.encode(val[1])+"',Wea_WeatherCondi='"+cf.encode(val[2])+"',Wea_RecentCondi='"+cf.encode(val[3])+"',Wea_R_outTemp='"+cf.encode(val[4])+"',Wea_R_inTemp='"+cf.encode(val[5])+"',Wea_NA='"+weatherna+"',Wea_date=datetime() Where Wea_srid='"+cf.selectedhomeid+"'");
			 }
			 else
			 {
				 cf.arr_db.execSQL("INSERT INTO "+cf.WeatherCondition+" (Wea_srid ,Wea_OutDoor,Wea_InDoor,Wea_WeatherCondi,Wea_RecentCondi,Wea_R_outTemp,Wea_R_inTemp,Wea_date,Wea_NA) VALUES" +
				 		"('"+cf.selectedhomeid+"','"+cf.encode(val[0])+"','"+cf.encode(val[1])+"','"+cf.encode(val[2])+"','"+cf.encode(val[3])+"','"+cf.encode(val[4])+"','"+cf.encode(val[5])+"',datetime(),'"+weatherna+"')");
			 }
			 cf.ShowToast("Weather/Temperature information saved successfully", 1);
			 Weather_setValue();
		 }catch (Exception e) {
			// TODO: handle exception
			 System.out.println("error occure"+e.getMessage());
		}
	}
	
	class event_driven implements OnClickListener
	 {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch(v.getId())
			{
			case R.id.Wea_RH_na_chk:
				if(((CheckBox)v).isChecked())
				{
					findViewById(R.id.wea_out_tmp_rw).setVisibility(View.GONE);
					findViewById(R.id.wea_in_tmp_rw).setVisibility(View.GONE);
					spreloutdoor.setSelection(0);
					sprelindoor.setSelection(0);
				}
				else
				{
					findViewById(R.id.wea_out_tmp_rw).setVisibility(View.VISIBLE);
					findViewById(R.id.wea_in_tmp_rw).setVisibility(View.VISIBLE);
					
					
				}
			break;
			}
		}
		 
	 }
}