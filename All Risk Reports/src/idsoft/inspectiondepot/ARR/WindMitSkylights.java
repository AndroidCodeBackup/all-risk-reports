package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.BIGeneralData.Spin_Selectedlistener;
import idsoft.inspectiondepot.ARR.BIGeneralData.textwatcher;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

public class WindMitSkylights extends Activity {
	CommonFunctions cf;
	Cursor c1,c2;
	Spinner spnelevskylights,spnskylightqnty,spnskylightsprot;
	 public String spnofqunatity[] = { "--Select--", "1","2","3","4","5","6","7","8","9","10",};
	ArrayAdapter elevsskylightsadap,spnskylightqntyadap,spnskylightsprotadap;
	String BI_Skylights="",skylightsnotappl="",SKY_NA="",elevspinnertxt="";
	int skylightopeningid,rwsskylights;
	String[] arrskylights,arrskylightelev,arrskylightsid,arrelevothertxt;
	LinearLayout subpnlshwskylights;TableLayout dynviewskylights;
	public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	       
	        cf = new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);				
		 	}
	        setContentView(R.layout.skylights);
	        cf.getDeviceDimensions();
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
	        if(cf.identityval==31)
	        {
	          hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","B1 1802(Rev.01/12)","Skylights",3,1,cf));
	        }
	        else if(cf.identityval==32)
	        { 
	        	hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","Commercial Type I","Skylights",3,1,cf));
	        }
			LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
	     	main_layout.setMinimumWidth(cf.wd);
	     	main_layout.addView(new MyOnclickListener(getApplicationContext(), 3, 0,0,cf));
	       
	        cf.type_submenulayout =(LinearLayout) findViewById(R.id.typesubmenu); //** WINDMIT TYPE SUBMENU **//
	        cf.type_submenulayout.addView(new MyOnclickListener(this, 300, 1,0, cf));
	        
	        cf.windmittypechk(cf.identityval,((RadioButton)findViewById(R.id.b1802_chk)),((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((TextView)findViewById(R.id.commtxt)),((ImageView)findViewById(R.id.editbno))); // To check the checkbox of choosen inspection type
	        cf.uncheckcommercial(((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((RadioButton)findViewById(R.id.b1802_chk))); 
	    	         
	        cf.submenu_layout = (LinearLayout) findViewById(R.id.submenu);
	        cf.submenu_layout.addView(new MyOnclickListener(this, cf.identityval, 1,317,cf));
	        
	        cf.mainlayout = (LinearLayout)findViewById(R.id.bc_table);	
	        
	        cf.tblrw = (TableRow)findViewById(R.id.row2);
	        cf. tblrw.setMinimumHeight(cf.ht); 	
	        
	        cf.CreateARRTable(312);
	        cf.CreateARRTable(64);
	        Declaration();	      
	        
	        Show_Skylights_value();
	        setvalue();
	    }
	private void setvalue() {
		// TODO Auto-generated method stub		
		selecttbl();
		if(SKY_NA.equals("") || SKY_NA.equals("0"))
		{
			((LinearLayout)findViewById(R.id.skylightslin)).setVisibility(cf.v1.VISIBLE);
			if(c2.getCount()>0)
			{
				((TextView)findViewById(R.id.savetxtsky)).setVisibility(cf.v1.VISIBLE);
				
			}
			else
			{
				((TextView)findViewById(R.id.savetxtsky)).setVisibility(cf.v1.GONE);
				//((LinearLayout)findViewById(R.id.skylightslin)).setVisibility(cf.v1.GONE);
			}
		}
		else
		{
			if(c2.getCount()>0)
			{
				((CheckBox)findViewById(R.id.skynotappl)).setChecked(false);	
				((LinearLayout)findViewById(R.id.skylightslin)).setVisibility(cf.v1.VISIBLE);
			}
			else
			{
				((CheckBox)findViewById(R.id.skynotappl)).setChecked(true);
				((LinearLayout)findViewById(R.id.skylightslin)).setVisibility(cf.v1.GONE);
			}
			((TextView)findViewById(R.id.savetxtsky)).setVisibility(cf.v1.VISIBLE);
		}
	}
	private void Declaration() {
		// TODO Auto-generated method stub
		cf.setupUI((ScrollView) findViewById(R.id.scr));
		spnelevskylights=(Spinner) findViewById(R.id.elevskylights);
		 elevsskylightsadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item,cf.spndoorelevation);
		 elevsskylightsadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 spnelevskylights.setAdapter(elevsskylightsadap);
		 spnelevskylights.setOnItemSelectedListener(new  Spin_Selectedlistener(1));		
		 
		 spnskylightqnty=(Spinner) findViewById(R.id.spnquanskylights);
		 spnskylightqntyadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item,spnofqunatity);
		 spnskylightqntyadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 spnskylightqnty.setAdapter(spnskylightqntyadap);
		 
		 spnskylightsprot=(Spinner) findViewById(R.id.spnprotectskylights);
		 spnskylightsprotadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item,cf.spnwindprottype);
		 spnskylightsprotadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 spnskylightsprot.setAdapter(spnskylightsprotadap);
		 dynviewskylights = (TableLayout)findViewById(R.id.dynamicskylights);
		 subpnlshwskylights = (LinearLayout)findViewById(R.id.subpnlshowsky);
		  ((EditText)findViewById(R.id.edtskylights)).addTextChangedListener(new textwatcher(1));
	}
	class Spin_Selectedlistener implements OnItemSelectedListener
	{
		int i;
		public Spin_Selectedlistener(int i) {
			// TODO Auto-generated constructor stub
			this.i=i;
		}

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
			// TODO Auto-generated method stub
			elevspinnertxt = spnelevskylights.getSelectedItem().toString();
			if(elevspinnertxt.equals("Other Elevation"))
			{
				((EditText)findViewById(R.id.edtelevother)).setVisibility(cf.v1.VISIBLE);
			}
			else
			{
				((EditText)findViewById(R.id.edtelevother)).setText("");
				((EditText)findViewById(R.id.edtelevother)).setVisibility(cf.v1.GONE);
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
	}
	class textwatcher implements TextWatcher
	 {
	       public int type;
	         textwatcher(int type)
	    	{
	    		this.type=type;
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {
	    		// TODO Auto-generated method stub
	    		if(this.type==1)
	    		{
	    			 if(!s.toString().equals(""))
	    			 {
		    			     if(Character.toString(s.toString().charAt(0)).equals("0"))
			    			 {
			    				 cf.ShowToast("Invalid Entry! Please enter the valid Daylight/Buck.", 0);
			    				 ((EditText)findViewById(R.id.edtskylights)).setText("");cf.hidekeyboard();
			    			 }		    			
	    			}
	    		}
	    	}
	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,

	    			int after) {
	    		// TODO Auto-generated method stub
	    	}
	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,int count) {
	    		// TODO Auto-generated method stub
	    	}	    	
	    }
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.mouseoverid:
			cf.ShowToast("Use only for Commercial Type I and Residential Wind Mitigation Inspections 1802 only.",0);
			break;
		case R.id.skysave:
			selecttbl();
			if(((CheckBox)findViewById(R.id.skynotappl)).isChecked()==false)
			{
				if(c2.getCount()==0)
				{
					cf.ShowToast("Please save Skylights.", 0);	
				}
				else
				{
					cf.ShowToast("Skylights saved successfully",0);
					((TextView)findViewById(R.id.savetxtsky)).setVisibility(cf.v1.VISIBLE);
					updatenotappl();
					cf.goclass(307);
				}
			}
			else
			{
				skylightsnotappl  ="1";
				try
				{
					cf.arr_db.execSQL("Delete from "+cf.BI_Skylights+" Where fld_srid='"+cf.selectedhomeid+"' and insptypeid='"+cf.identityval+"'");
				}
				catch(Exception e)
				{
					System.out.println("deleting error="+e.getMessage());
				}
				updatenotappl();
				cf.goclass(307);
			}
			break;
		case R.id.skylightscancel:
			clearskylights();			
			break;	
		case R.id.skylightssave:
			selecttbl();
			boolean b=((spnelevskylights.getSelectedItem().toString().equals("Other Elevation")) && ((EditText)findViewById(R.id.edtelevother)).getText().toString().trim().equals("")) ? false:true;
			if(((CheckBox)findViewById(R.id.skynotappl)).isChecked()==false)
			{
				if(!spnelevskylights.getSelectedItem().toString().equals("--Select--") &&  b==true)
				{
					if(!spnskylightqnty.getSelectedItem().toString().equals("--Select--"))
					{
						if(!spnskylightsprot.getSelectedItem().toString().equals("--Select--"))
						{
							if(!((EditText)findViewById(R.id.edtskylights)).getText().toString().trim().equals(""))
							{
								InsertSkylights();
							}
							else
							{
								cf.ShowToast("Please enter Daylight/Buck.", 0);	
								cf.setFocus(((EditText)findViewById(R.id.edtskylights)));
							}
						}
						else
						{
							cf.ShowToast("Please select the Protection Codes.", 0);	
						}
					}
					else
					{
						cf.ShowToast("Please select the No. of Quantity.", 0);	
					}
				}else
				{
					if(spnelevskylights.getSelectedItem().toString().equals("--Select--"))
					{
						cf.ShowToast("Please select the Elevation.", 0);
					}
					else if(b==false)
					{
						cf.ShowToast("Please select the other text for Elevation.", 0);	
						cf.setFocus(((EditText)findViewById(R.id.edtelevother)));
					}
				}
			}
			else
			{
				InsertSkylights();
			}
			break;
		case R.id.skynotappl:			
			if(((CheckBox)findViewById(R.id.skynotappl)).isChecked())
			{
				skylightsnotappl="1";
				if(rwsskylights>0)
				{
					AlertDialog.Builder builder = new AlertDialog.Builder(WindMitSkylights.this);
					builder.setTitle("Confirmation");
					builder.setIcon(R.drawable.alertmsg);
					builder.setMessage("Do you want to Clear the Skylight Information?").setCancelable(false)
							.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,int id1) 
										{
												//updatenotappl();
												clearskylights();
												
												subpnlshwskylights.setVisibility(cf.v1.GONE);
												((CheckBox)findViewById(R.id.skynotappl)).setChecked(true);
												((LinearLayout)findViewById(R.id.skylightslin)).setVisibility(cf.v1.GONE);
												((TextView)findViewById(R.id.savetxtsky)).setVisibility(cf.v1.GONE);
										}
									})
							.setNegativeButton("No",new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,	int id) {
											
											((CheckBox)findViewById(R.id.skynotappl)).setChecked(false);
											dialog.cancel();											
									}
							});
					builder.show();
				}
				else
				{
					//updatenotappl();
					((TextView)findViewById(R.id.savetxtsky)).setVisibility(cf.v1.GONE);
					((LinearLayout)findViewById(R.id.skylightslin)).setVisibility(cf.v1.GONE);
				}
			}
			else
			{
				skylightsnotappl="0";clearskylights();
				//updatenotappl();
				((LinearLayout)findViewById(R.id.skylightslin)).setVisibility(cf.v1.VISIBLE);
				((TextView)findViewById(R.id.savetxtsky)).setVisibility(cf.v1.GONE);
			}
			break;
		}		
	}
	private void clearskylights() {
		// TODO Auto-generated method stub		
		spnelevskylights.setSelection(0);
		spnskylightqnty.setSelection(0);
		spnskylightsprot.setSelection(0);
		((EditText)findViewById(R.id.edtskylights)).setText("");
		((Button)findViewById(R.id.skylightssave)).setText("Save");
		((EditText)findViewById(R.id.edtskylights)).setFocusable(false);
		spnelevskylights.setEnabled(true);
	}
	private void selecttbl() {
		try
		{
			c1= cf.SelectTablefunction(cf.WindDoorSky_NA, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
			if(c1.getCount()>0)
			{
				c1.moveToFirst();
				SKY_NA = c1.getString(c1.getColumnIndex("fld_skylightsNA"));
			}
		}
		catch(Exception e)
		{
			
		}
		try
		{
			c2 =  cf.SelectTablefunction(cf.BI_Skylights, " where fld_srid='"+cf.selectedhomeid+"' and insptypeid='"+cf.identityval+"'");
		}
		catch(Exception e)
		{
			
		}
	}
	protected void updatenotappl() {
		// TODO Auto-generated method stub
		try
		{
			selecttbl();
			if(c1.getCount()==0)
			{
				cf.arr_db.execSQL("INSERT INTO " + cf.WindDoorSky_NA + " (fld_srid,insp_typeid,fld_skylightsNA)"
						+ "VALUES ('"+cf.selectedhomeid+"','"+cf.identityval+"','"+skylightsnotappl+"')");
			}
			else
			{
				cf.arr_db.execSQL("UPDATE "	+ cf.WindDoorSky_NA	+ " SET fld_skylightsNA='"+skylightsnotappl+"' WHERE fld_srid ='"+ cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'");	
			}			
		}
		catch(Exception e)
		{
			System.out.println("sdfdfds"+e.getMessage());
		}
	}
	private void InsertSkylights() {
		// TODO Auto-generated method stub
		String elevation =spnelevskylights.getSelectedItem().toString();
		String quantity =spnskylightqnty.getSelectedItem().toString();
		String protection =spnskylightsprot.getSelectedItem().toString();
		String daylight =((EditText)findViewById(R.id.edtskylights)).getText().toString().trim();	
		
		if(!quantity.equals("--Select--") && !protection.equals("--Select--") && !daylight.equals(""))
		{
			BI_Skylights=quantity+"&#39;"+protection+"&#39;"+daylight;
		}
		
		try
		{
			if(((Button)findViewById(R.id.skylightssave)).getText().toString().equals("Update")){
				cf.arr_db.execSQL("UPDATE "
						+ cf.BI_Skylights
						+ " SET BI_Skylights='"+cf.encode(BI_Skylights)+"',fld_elevation_othertxt='"+cf.encode(((EditText)findViewById(R.id.edtelevother)).getText().toString())+"'"
						+ " WHERE fld_srid ='"+ cf.selectedhomeid + "' and fld_elevation='"+elevation+"' and BI_SkyId='"+skylightopeningid+"' and insptypeid='"+cf.identityval+"'");
				((Button)findViewById(R.id.skylightssave)).setText("Save/Add More Skylights");
				
			}else{
				Cursor c1=cf.SelectTablefunction(cf.BI_Skylights, " where fld_srid='"+cf.selectedhomeid+"' and insptypeid='"+cf.identityval+"'");
				 cf.arr_db.execSQL("INSERT INTO "
							+ cf.BI_Skylights
							+ " (fld_srid,insptypeid,fld_elevation,BI_Skylights,fld_elevation_othertxt)"
							+ "VALUES ('"+cf.selectedhomeid+"','"+cf.identityval+"','"+elevation+"','"+cf.encode(BI_Skylights)+"','"+cf.encode(((EditText)findViewById(R.id.edtelevother)).getText().toString())+"')");
				 
			}
			
			cf.ShowToast("Skylights saved successfully",0);
			
			((TextView)findViewById(R.id.savetxtsky)).setVisibility(cf.v1.VISIBLE);
			clearskylights();spnelevskylights.setEnabled(true);((Button)findViewById(R.id.skylightssave)).setText("Save/Add More Skylights");
			Show_Skylights_value();
		}
		catch(Exception e)
		{
			System.out.println("ins "+e.getMessage());
		}
	}
	private void Show_Skylights_value() {
		// TODO Auto-generated method stub
		
		Cursor c1=cf.SelectTablefunction(cf.BI_Skylights, " where fld_srid='"+cf.selectedhomeid+"' and insptypeid='"+cf.identityval+"'");
		System.out.println("C!!"+c1.getCount());
		
		if(c1.getCount()!=0)
		{
			((TextView)findViewById(R.id.savetxtsky)).setVisibility(cf.v1.VISIBLE);
			rwsskylights = c1.getCount();
			arrskylights=new String[rwsskylights];
			arrskylightelev = new String[rwsskylights];
			arrskylightsid = new String[rwsskylights];
			arrelevothertxt = new String[rwsskylights];
			c1.moveToFirst();
				 int i=0;
				 do {
					 arrskylightsid[i] = c1.getString(c1.getColumnIndex("BI_SkyId"));
					 arrskylights[i] = cf.decode(c1.getString(c1.getColumnIndex("BI_Skylights")));
					 arrskylightelev[i] = c1.getString(c1.getColumnIndex("fld_elevation"));		
					 arrelevothertxt[i] = cf.decode(c1.getString(c1.getColumnIndex("fld_elevation_othertxt")));
					 
						i++;
					} while (c1.moveToNext());	
				 subpnlshwskylights.setVisibility(cf.v1.VISIBLE);
				 Disp_skylights_value();			
		}
		else
		{
			((TextView)findViewById(R.id.savetxtsky)).setVisibility(cf.v1.GONE);
			subpnlshwskylights.setVisibility(cf.v1.GONE);
		}
	}
	private void Disp_skylights_value() {
		// TODO Auto-generated method stub
		dynviewskylights.removeAllViews();
		LinearLayout h1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvac_listview_header, null);
		LinearLayout th= (LinearLayout)h1.findViewById(R.id.skylights_hdr);th.setVisibility(cf.v1.VISIBLE);
		TableLayout.LayoutParams lp = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
	 	lp.setMargins(2, 0, 2, 2); 
	 	h1.removeAllViews(); 
	 	
	 	dynviewskylights.addView(th,lp);	
	 	dynviewskylights.setVisibility(View.VISIBLE);
	 	for (int i = 0; i < rwsskylights; i++) 
		{	
			TextView No,txtelevation,txtnoofquantity,txtprotection,txtdaylight;
			ImageView edit,delete;
			
			LinearLayout t1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvac_listview, null);
			LinearLayout t = (LinearLayout)t1.findViewById(R.id.skyligts);t.setVisibility(cf.v1.VISIBLE);
			t.setId(44444+i);/// Set some id for further use
			
		 	No= (TextView) t.findViewWithTag("No");			
		 	txtelevation= (TextView) t.findViewWithTag("BI_SkyElev"); 	
		 	txtnoofquantity= (TextView) t.findViewWithTag("BI_Skyquan");
		 	txtprotection= (TextView) t.findViewWithTag("BI_SkyProtection");	
		 	txtdaylight= (TextView) t.findViewWithTag("BI_SkyDaylight");	System.out.println("BI_SkyProtection");
		 	
			No.setText(String.valueOf(i+1));		 	
			if(arrelevothertxt[i].equals(""))
			{
				txtelevation.setText(arrskylightelev[i]);
			}
			else
			{
				txtelevation.setText("Other Elevation("+arrelevothertxt[i]+")");	
			}
			System.out.println("arrskylights"+arrskylights[i]);
			String Skylights_split[] = arrskylights[i].split("&#39;"); 
			txtnoofquantity.setText(Skylights_split[0]);System.out.println("txtnoofquantity");
			txtprotection.setText(Skylights_split[1]);
			txtdaylight.setText(Skylights_split[2]);System.out.println("skylight");
			
		 	edit= (ImageView) t.findViewWithTag("Skylights_edit");		 
		 	edit.setTag(arrskylightsid[i]);System.out.println("edit");
		 	edit.setOnClickListener(new WindOpen_EditClick(1,arrskylightsid[i]));System.out.println("arrskylightsid");
		 	delete= (ImageView) t.findViewWithTag("Skylights_delete");
		 	delete.setTag(arrskylightsid[i]);System.out.println("delete");
		 	delete.setOnClickListener(new WindOpen_EditClick(2,arrskylightsid[i]));
		 	
		 	t1.removeAllViews();
		 	dynviewskylights.addView(t,lp);
		}
	}
	class WindOpen_EditClick implements OnClickListener
	{
		int type;
		int edit; 
		WindOpen_EditClick(int edit,String id1)
		{
			this.edit=edit;
			this.type=Integer.parseInt(id1);
		}
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			//type=Integer.parseInt(v.getTag().toString());
			if(edit==1)
			{				
				((Button)findViewById(R.id.skylightssave)).setText("Update");
				updateSkylights(type);
			}
			else if(edit==2)
			{
				AlertDialog.Builder builder = new AlertDialog.Builder(WindMitSkylights.this);
						builder.setMessage("Do you want to delete the selected Skylights?").setCancelable(false)
								.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog,int id) {												
														cf.arr_db.execSQL("Delete from "+cf.BI_Skylights+" Where BI_SkyId='"+type+"'");
														cf.ShowToast("Deleted successfully.", 0);														
														clearskylights();
														Show_Skylights_value();
													}
										})
								.setNegativeButton("No",new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog, int id) {
												dialog.cancel();
											}
										});
						builder.setTitle("Confirmation");
						builder.setIcon(R.drawable.alertmsg);
						builder.show();		
			}
		}
	}
	public void updateSkylights(int type) {
		// TODO Auto-generated method stub
		skylightopeningid=type;
		Cursor c1=cf.SelectTablefunction(cf.BI_Skylights, " where BI_SkyId='"+type+"'");
	
		if(c1.getCount()>0)
		{
			c1.moveToFirst();
			String Skylights_value = cf.decode(c1.getString(c1.getColumnIndex("BI_Skylights")));
			String Skylights_split[] = Skylights_value.split("&#39;"); 	
			
			int spinnerPosition1 = elevsskylightsadap.getPosition(c1.getString(c1.getColumnIndex("fld_elevation")));
			spnelevskylights.setSelection(spinnerPosition1);
			if(c1.getString(c1.getColumnIndex("fld_elevation")).equals("Other Elevation"))
			{
				((EditText)findViewById(R.id.edtelevother)).setVisibility(cf.v1.VISIBLE);
				((EditText)findViewById(R.id.edtelevother)).setText(cf.decode(c1.getString(c1.getColumnIndex("fld_elevation_othertxt"))));
			}
			else
			{
				((EditText)findViewById(R.id.edtelevother)).setVisibility(cf.v1.GONE);
				((EditText)findViewById(R.id.edtelevother)).setText("");
			}
			
			int spinnerPosition2 = spnskylightqntyadap.getPosition(Skylights_split[0]);
			spnskylightqnty.setSelection(spinnerPosition2);
			int spinnerPosition3 = spnskylightsprotadap.getPosition(Skylights_split[1]);
			spnskylightsprot.setSelection(spinnerPosition3);
			
			((EditText)findViewById(R.id.edtskylights)).setText(Skylights_split[2]);
			spnelevskylights.setEnabled(false);
			
			((Button)findViewById(R.id.skylightssave)).setText("Update");
		}
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			cf.gotoclass(cf.identityval, WindMitDooropenings.class);
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}
	
}