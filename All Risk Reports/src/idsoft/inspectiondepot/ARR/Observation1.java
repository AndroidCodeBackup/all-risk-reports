/*
   ************************************************************
   * Project: ALL RISK REPORT
   * Module : Observation1.java
   * Creation History:
      Created By : Gowri on 
   * Modification History:
      Last Modified By : Gowri on 04/04/2013
   ************************************************************  
*/
package idsoft.inspectiondepot.ARR;


import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.Observation2.Touch_Listener;
import idsoft.inspectiondepot.ARR.Observation2.checklistenetr;
import idsoft.inspectiondepot.ARR.Observation2.textwatcher;

import android.app.Activity;
import android.app.Dialog;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class Observation1 extends Activity{
	CommonFunctions cf;
	String obs1rdgtxt="",obs1chkloc_val="",obs1ardgtxt="",obs1achkloc_val="",obs2rdgtxt="",obs2chkloc_val="",
		   obs3rdgtxt="",obs3chkloc_val="",obs4rdgtxt="",obs4chkloc_val="",obs5rdgtxt="",obs5chkloc_val="",
		   obs6rdgtxt="",obs6chkloc_val="",obs6ardgtxt="",obs6achkloc_val="",obs6brdgtxt="",obs6bchkloc_val="",
		   obs6crdgtxt="",obs6cchkloc_val="",obs7rdgtxt="",obs7chkloc_val="",obs7ardgtxt="",obs7achkloc_val="",
		   obs7chkcrk_val="",edt="",obs7chkwidt_val="",obs7chkcond_val="",obs7chkcaus_val="",inspdata;
	public String[] data,countarr;
    public ScrollView sv;
	 public TextView tvstatus[];
	public LinearLayout dynlist;
	public Button deletebtn[],edtbtn[];
	int rws;
	RadioGroup obs1rdgval,obs1ardgval,obs2rdgval,obs3rdgval,obs4rdgval,obs5rdgval,obs6rdgval,obs6ardgval,
	obs6brdgval,obs6crdgval,obs7rdgval,obs7ardgval;
	public int[] chkbxobs1loc = {R.id.obs1loc1,R.id.obs1loc2,R.id.obs1loc3,R.id.obs1loc4,R.id.obs1loc5};
	public CheckBox obs1chkloc[] = new CheckBox[5];
	public int[] chkbxobs1aloc = {R.id.obs1aloc1,R.id.obs1aloc2,R.id.obs1aloc3,R.id.obs1aloc4,R.id.obs1aloc5};
	public CheckBox obs1achkloc[] = new CheckBox[5];
	public int[] chkbxobs2loc = {R.id.obs2loc1,R.id.obs2loc2,R.id.obs2loc3,R.id.obs2loc4,R.id.obs2loc5};
	public CheckBox obs2chkloc[] = new CheckBox[5];
	public int[] chkbxobs3loc = {R.id.obs3loc1,R.id.obs3loc2,R.id.obs3loc3,R.id.obs3loc4,R.id.obs3loc5};
	public CheckBox obs3chkloc[] = new CheckBox[5];
	public int[] chkbxobs4loc = {R.id.obs4loc1,R.id.obs4loc2,R.id.obs4loc3,R.id.obs4loc4,R.id.obs4loc5,
			                     R.id.obs4loc6,R.id.obs4loc7,R.id.obs4loc8};
	public CheckBox obs4chkloc[] = new CheckBox[8];
	public int[] chkbxobs5loc = {R.id.obs5loc1,R.id.obs5loc2,R.id.obs5loc3,R.id.obs5loc4,R.id.obs5loc5};
    public CheckBox obs5chkloc[] = new CheckBox[5];
    public int[] chkbxobs6loc = {R.id.obs6loc1,R.id.obs6loc2,R.id.obs6loc3,R.id.obs6loc4,R.id.obs6loc5};
    public CheckBox obs6chkloc[] = new CheckBox[5];
    public int[] chkbxobs6aloc = {R.id.obs6aloc1,R.id.obs6aloc2,R.id.obs6aloc3,R.id.obs6aloc4,R.id.obs6aloc5};
    public CheckBox obs6achkloc[] = new CheckBox[5];
    public int[] chkbxobs6bloc = {R.id.obs6bloc1,R.id.obs6bloc2,R.id.obs6bloc3,R.id.obs6bloc4,R.id.obs6bloc5};
    public CheckBox obs6bchkloc[] = new CheckBox[5];
    public int[] chkbxobs6cloc = {R.id.obs6cloc1,R.id.obs6cloc2,R.id.obs6cloc3,R.id.obs6cloc4,R.id.obs6cloc5};
    public CheckBox obs6cchkloc[] = new CheckBox[5];
    public int[] chkbx7crack = {R.id.obS7crk1,R.id.obS7crk2,R.id.obS7crk3,R.id.obS7crk4,R.id.obS7crk5};
	public int[] chkbx7loc = {R.id.obS7loc1,R.id.obS7loc2,R.id.obS7loc3,R.id.obS7loc4,R.id.obS7loc5};
	public int[] chkbx7wid = {R.id.obS7wid1,R.id.obS7wid2,R.id.obS7wid3,R.id.obS7wid4,R.id.obS7wid5};
	public int[] chkbx7cond = {R.id.obS7cond1,R.id.obS7cond2,R.id.obS7cond3,R.id.obS7cond4,R.id.obS7cond5,
			                    R.id.obS7cond6,R.id.obS7cond7,R.id.obS7cond8};
	public int[] chkbx7caus = {R.id.obS7caus1,R.id.obS7caus2,R.id.obS7caus3,R.id.obS7caus4,R.id.obS7caus5,
			R.id.obS7caus6,R.id.obS7caus7};
	public CheckBox obs7chkcrck[] = new CheckBox[5];
	public CheckBox obs7chkloc[] = new CheckBox[5];
	public CheckBox obs7chkwidt[] = new CheckBox[5];
	public CheckBox obs7chkcond[] = new CheckBox[8];
	public CheckBox obs7chkcaus[] = new CheckBox[7];
	public int[] chkbxobs7aloc = {R.id.obs7aloc1,R.id.obs7aloc2,R.id.obs7aloc3,R.id.obs7aloc4};
    public CheckBox obs7achkloc[] = new CheckBox[4];
	
	@Override
	   public void onCreate(Bundle savedInstanceState) {

	        super.onCreate(savedInstanceState);
	        cf=new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);
				
		 	}
	        setContentView(R.layout.sinkobservation1);
	        cf.getDeviceDimensions();
	        
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
			hdr_layout.addView(new HdrOnclickListener(this,1,"Sinkhole Inspection","Observations(1 - 7)",5,0,cf));
			
	        LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
	        mainmenu_layout.setMinimumWidth(cf.wd);
	        mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 5, 0,0,cf));
	        LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 52, 1,0,cf));
			TableRow tblrw = (TableRow)findViewById(R.id.row2);
			tblrw.setMinimumHeight(cf.ht);
			declarations();
			cf.CreateARRTable(52);
			cf.CreateARRTable(56);
			Obs1_Setvalue();
			
	}
	private void Obs1_Setvalue() {
		// TODO Auto-generated method stub
		 try
			{
			   Cursor Obs1_retrive=cf.SelectTablefunction(cf.SINK_Obs1tbl, " where fld_srid='"+cf.selectedhomeid+"'");
			   if(Obs1_retrive.getCount()>0)
			   {  
				   Obs1_retrive.moveToFirst();
				   ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
				   obs1rdgtxt=Obs1_retrive.getString(Obs1_retrive.getColumnIndex("IrregularLandSurface"));
				   ((RadioButton) obs1rdgval.findViewWithTag(obs1rdgtxt)).setChecked(true);
				   ((EditText)findViewById(R.id.obs1comment)).setText(cf.decode(Obs1_retrive.getString(Obs1_retrive.getColumnIndex("IrregularLandSurfaceComments"))));
				   if(obs1rdgtxt.equals("Yes")) {
				    	((LinearLayout)findViewById(R.id.obs1yes)).setVisibility(cf.v1.VISIBLE);
				    	 obs1chkloc_val=cf.decode(Obs1_retrive.getString(Obs1_retrive.getColumnIndex("IrregularLandSurfaceLocation")));
						 cf.setvaluechk1(obs1chkloc_val,obs1chkloc,((EditText)findViewById(R.id.obs1loc_etother)));
				   } else {
				    	((LinearLayout )findViewById(R.id.obs1yes)).setVisibility(cf.v1.GONE);}
				   
				   obs1ardgtxt=Obs1_retrive.getString(Obs1_retrive.getColumnIndex("VisibleBuriedDebris"));
				   ((RadioButton) obs1ardgval.findViewWithTag(obs1ardgtxt)).setChecked(true);
				   ((EditText)findViewById(R.id.obs1acomment)).setText(cf.decode(Obs1_retrive.getString(Obs1_retrive.getColumnIndex("VisibleBuriedDebrisComments"))));
				   if(obs1ardgtxt.equals("Yes")) {
				    	((LinearLayout)findViewById(R.id.obs1ayes)).setVisibility(cf.v1.VISIBLE);
				    	 obs1achkloc_val=cf.decode(Obs1_retrive.getString(Obs1_retrive.getColumnIndex("VisibleBuriedDebrisLocation")));
						 cf.setvaluechk1(obs1achkloc_val,obs1achkloc,((EditText)findViewById(R.id.obs1aloc_etother)));
				   } else {
				    	((LinearLayout )findViewById(R.id.obs1ayes)).setVisibility(cf.v1.GONE);}
				   
				   obs2rdgtxt=Obs1_retrive.getString(Obs1_retrive.getColumnIndex("IrregularLandSurfaceAdjacent"));
				   ((RadioButton) obs2rdgval.findViewWithTag(obs2rdgtxt)).setChecked(true);
				   ((EditText)findViewById(R.id.obs2comment)).setText(cf.decode(Obs1_retrive.getString(Obs1_retrive.getColumnIndex("IrregularLandSurfaceAdjacentComments"))));
				   if(obs2rdgtxt.equals("Yes")) {
				    	((LinearLayout)findViewById(R.id.obs2yes)).setVisibility(cf.v1.VISIBLE);
				    	 obs2chkloc_val=cf.decode(Obs1_retrive.getString(Obs1_retrive.getColumnIndex("IrregularLandSurfaceAdjacentLocation")));
						 cf.setvaluechk1(obs2chkloc_val,obs2chkloc,((EditText)findViewById(R.id.obs2loc_etother)));
				   } else {
				    	((LinearLayout )findViewById(R.id.obs2yes)).setVisibility(cf.v1.GONE);}
				   
				   obs3rdgtxt=Obs1_retrive.getString(Obs1_retrive.getColumnIndex("SoilCollapse"));
				   ((RadioButton) obs3rdgval.findViewWithTag(obs3rdgtxt)).setChecked(true);
				   ((EditText)findViewById(R.id.obs3comment)).setText(cf.decode(Obs1_retrive.getString(Obs1_retrive.getColumnIndex("SoilCollapseComments"))));
				   if(obs3rdgtxt.equals("Yes")) {
				    	((LinearLayout)findViewById(R.id.obs3yes)).setVisibility(cf.v1.VISIBLE);
				    	 obs3chkloc_val=cf.decode(Obs1_retrive.getString(Obs1_retrive.getColumnIndex("SoilCollapseLocation")));
						 cf.setvaluechk1(obs3chkloc_val,obs3chkloc,((EditText)findViewById(R.id.obs3loc_etother)));
				   } else {
				    	((LinearLayout )findViewById(R.id.obs3yes)).setVisibility(cf.v1.GONE);}
				   
				   obs4rdgtxt=Obs1_retrive.getString(Obs1_retrive.getColumnIndex("SoilErosionAroundFoundation"));
				   ((RadioButton) obs4rdgval.findViewWithTag(obs4rdgtxt)).setChecked(true);
				   ((EditText)findViewById(R.id.obs4comment)).setText(cf.decode(Obs1_retrive.getString(Obs1_retrive.getColumnIndex("SoilErosionAroundFoundationComments"))));
				   if(obs4rdgtxt.equals("Yes")) {
				    	((LinearLayout)findViewById(R.id.obs4yes)).setVisibility(cf.v1.VISIBLE);
				    	 obs4chkloc_val=cf.decode(Obs1_retrive.getString(Obs1_retrive.getColumnIndex("SoilErosionAroundFoundationOption")));
						 cf.setvaluechk1(obs4chkloc_val,obs4chkloc,((EditText)findViewById(R.id.obs4loc_etother)));
				   } else {
				    	((LinearLayout )findViewById(R.id.obs4yes)).setVisibility(cf.v1.GONE);}
				   
				   obs5rdgtxt=Obs1_retrive.getString(Obs1_retrive.getColumnIndex("DrivewaysCracksNoted"));
				   ((RadioButton) obs5rdgval.findViewWithTag(obs5rdgtxt)).setChecked(true);
				   ((EditText)findViewById(R.id.obs5comment)).setText(cf.decode(Obs1_retrive.getString(Obs1_retrive.getColumnIndex("DrivewaysCracksComments"))));
				   if(obs5rdgtxt.equals("Yes")) {
				    	((LinearLayout)findViewById(R.id.obs5yes)).setVisibility(cf.v1.VISIBLE);
				    	 obs5chkloc_val=cf.decode(Obs1_retrive.getString(Obs1_retrive.getColumnIndex("DrivewaysCracksOption")));
						 cf.setvaluechk1(obs5chkloc_val,obs5chkloc,((EditText)findViewById(R.id.obs5loc_etother)));
				   } else {
				    	((LinearLayout )findViewById(R.id.obs5yes)).setVisibility(cf.v1.GONE);}
				   
				   obs6rdgtxt=Obs1_retrive.getString(Obs1_retrive.getColumnIndex("UpliftDrivewaysSurface"));
				   ((RadioButton) obs6rdgval.findViewWithTag(obs6rdgtxt)).setChecked(true);
				   ((EditText)findViewById(R.id.obs6comment)).setText(cf.decode(Obs1_retrive.getString(Obs1_retrive.getColumnIndex("UpliftDrivewaysSurfaceComments"))));
				   if(obs6rdgtxt.equals("Yes")) {
				    	((LinearLayout)findViewById(R.id.obs6yes)).setVisibility(cf.v1.VISIBLE);
				    	 obs6chkloc_val=cf.decode(Obs1_retrive.getString(Obs1_retrive.getColumnIndex("UpliftDrivewaysSurfaceOption")));
						 cf.setvaluechk1(obs6chkloc_val,obs6chkloc,((EditText)findViewById(R.id.obs6loc_etother)));
				   } else {
				    	((LinearLayout )findViewById(R.id.obs6yes)).setVisibility(cf.v1.GONE);}
				   
				   obs6ardgtxt=Obs1_retrive.getString(Obs1_retrive.getColumnIndex("LargeTree"));
				   ((RadioButton) obs6ardgval.findViewWithTag(obs6ardgtxt)).setChecked(true);
				   ((EditText)findViewById(R.id.obs6acomment)).setText(cf.decode(Obs1_retrive.getString(Obs1_retrive.getColumnIndex("LargeTreeComments"))));
				   dbquery();
				   
				   obs6brdgtxt=Obs1_retrive.getString(Obs1_retrive.getColumnIndex("CypressTree"));
				   ((RadioButton) obs6brdgval.findViewWithTag(obs6brdgtxt)).setChecked(true);
				   ((EditText)findViewById(R.id.obs6bcomment)).setText(cf.decode(Obs1_retrive.getString(Obs1_retrive.getColumnIndex("CypressTreeComments"))));
				   if(obs6brdgtxt.equals("Yes")) {
				    	((LinearLayout)findViewById(R.id.obs6byes)).setVisibility(cf.v1.VISIBLE);
				    	obs6bchkloc_val=cf.decode(Obs1_retrive.getString(Obs1_retrive.getColumnIndex("CypressTreeLocation")));
						 cf.setvaluechk1(obs6bchkloc_val,obs6bchkloc,((EditText)findViewById(R.id.obs6bloc_etother)));
				   } else {
				    	((LinearLayout )findViewById(R.id.obs6byes)).setVisibility(cf.v1.GONE);}
				   
				   obs6crdgtxt=Obs1_retrive.getString(Obs1_retrive.getColumnIndex("LargeTreesRemoved"));
				   ((RadioButton) obs6crdgval.findViewWithTag(obs6crdgtxt)).setChecked(true);
				   ((EditText)findViewById(R.id.obs6ccomment)).setText(cf.decode(Obs1_retrive.getString(Obs1_retrive.getColumnIndex("LargeTreesRemovedComments"))));
				   if(obs6crdgtxt.equals("Yes")) {
				    	((LinearLayout)findViewById(R.id.obs6cyes)).setVisibility(cf.v1.VISIBLE);
				    	obs6cchkloc_val=cf.decode(Obs1_retrive.getString(Obs1_retrive.getColumnIndex("LargeTreesRemovedLocation")));
						 cf.setvaluechk1(obs6cchkloc_val,obs6cchkloc,((EditText)findViewById(R.id.obs6cloc_etother)));
				   } else {
				    	((LinearLayout )findViewById(R.id.obs6cyes)).setVisibility(cf.v1.GONE);}
				   
				   obs7rdgtxt=Obs1_retrive.getString(Obs1_retrive.getColumnIndex("FoundationCrackNoted"));
				    ((RadioButton) obs7rdgval.findViewWithTag(obs7rdgtxt)).setChecked(true);
				    if(obs7rdgtxt.equals("Yes"))
				    {
				    	((LinearLayout)findViewById(R.id.obS7yes)).setVisibility(cf.v1.VISIBLE);
				    	 obs7chkcrk_val=cf.decode(Obs1_retrive.getString(Obs1_retrive.getColumnIndex("FoundationTypeofCrack")));
						 cf.setvaluechk1(obs7chkcrk_val,obs7chkcrck,((EditText)findViewById(R.id.obS7crk_etother)));
						 obs7chkloc_val=cf.decode(Obs1_retrive.getString(Obs1_retrive.getColumnIndex("FoundationLocation")));
						 cf.setvaluechk1(obs7chkloc_val,obs7chkloc,((EditText)findViewById(R.id.obS7wid_etother)));
						 obs7chkwidt_val=cf.decode(Obs1_retrive.getString(Obs1_retrive.getColumnIndex("FoundationWidthofCrack")));
						 cf.setvaluechk1(obs7chkwidt_val,obs7chkwidt,((EditText)findViewById(R.id.obS7loc_etother)));
						 ((EditText)findViewById(R.id.obS7_etlen)).setText(cf.decode(Obs1_retrive.getString(Obs1_retrive.getColumnIndex("FoundationLengthofCrack"))));
						 obs7chkcond_val=cf.decode(Obs1_retrive.getString(Obs1_retrive.getColumnIndex("FoundationCleanliness")));
						 cf.setvaluechk1(obs7chkcond_val,obs7chkcond,((EditText)findViewById(R.id.obS7loc_etother)));
						 obs7chkcaus_val=cf.decode(Obs1_retrive.getString(Obs1_retrive.getColumnIndex("FoundationProbableCause")));
						 cf.setvaluechk1(obs7chkcaus_val,obs7chkcaus,((EditText)findViewById(R.id.obS7caus7_etother)));
				    }
				    else
				    {
				    	((LinearLayout)findViewById(R.id.obS7yes)).setVisibility(cf.v1.GONE);
				    }
				    ((EditText)findViewById(R.id.obs7comment)).setText(cf.decode(Obs1_retrive.getString(Obs1_retrive.getColumnIndex("FoundationComments"))));
				    
				    obs7ardgtxt=Obs1_retrive.getString(Obs1_retrive.getColumnIndex("RetainingWallsServiceable"));
					   ((RadioButton) obs7ardgval.findViewWithTag(obs7ardgtxt)).setChecked(true);
					   ((EditText)findViewById(R.id.obs7acomment)).setText(cf.decode(Obs1_retrive.getString(Obs1_retrive.getColumnIndex("RetainingWallsServiceComments"))));
					   if(obs7ardgtxt.equals("Yes")) {
					    	((LinearLayout)findViewById(R.id.obs7ayes)).setVisibility(cf.v1.VISIBLE);
					    	obs7achkloc_val=cf.decode(Obs1_retrive.getString(Obs1_retrive.getColumnIndex("RetainingWallsServiceOption")));
							 cf.setvaluechk1(obs7achkloc_val,obs7achkloc,((EditText)findViewById(R.id.obS7loc_etother)));
					   } else {
					    	((LinearLayout )findViewById(R.id.obs7ayes)).setVisibility(cf.v1.GONE);}
				    
			   }
			   else
			   {
				   ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.GONE);
			   }
			}
		    catch (Exception E)
			{
				String strerrorlog="Retrieving Observation1 - Sink";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			}
	}
	private void declarations() {
		// TODO Auto-generated method stub
		 obs1rdgval = (RadioGroup)findViewById(R.id.obs1_rdg);
	     obs1rdgval.setOnCheckedChangeListener(new checklistenetr(1));
	     
	     ((EditText)findViewById(R.id.obs1comment)).setOnTouchListener(new Touch_Listener(1));
	     ((EditText)findViewById(R.id.obs1comment)).addTextChangedListener(new textwatcher(1));
	     ((EditText)findViewById(R.id.obs1loc_etother)).setOnTouchListener(new Touch_Listener(2));
	     
	     obs1ardgval = (RadioGroup)findViewById(R.id.obs1a_rdg);
	     obs1ardgval.setOnCheckedChangeListener(new checklistenetr(2));
	     
	     ((EditText)findViewById(R.id.obs1acomment)).setOnTouchListener(new Touch_Listener(3));
	     ((EditText)findViewById(R.id.obs1acomment)).addTextChangedListener(new textwatcher(3));
	     ((EditText)findViewById(R.id.obs1aloc_etother)).setOnTouchListener(new Touch_Listener(4));
	     
	     obs2rdgval = (RadioGroup)findViewById(R.id.obs2_rdg);
	     obs2rdgval.setOnCheckedChangeListener(new checklistenetr(3));
	     
	     ((EditText)findViewById(R.id.obs2comment)).setOnTouchListener(new Touch_Listener(5));
	     ((EditText)findViewById(R.id.obs2comment)).addTextChangedListener(new textwatcher(5));
	     ((EditText)findViewById(R.id.obs2loc_etother)).setOnTouchListener(new Touch_Listener(6));
	     
	     obs3rdgval = (RadioGroup)findViewById(R.id.obs3_rdg);
	     obs3rdgval.setOnCheckedChangeListener(new checklistenetr(4));
	     
	     ((EditText)findViewById(R.id.obs3comment)).setOnTouchListener(new Touch_Listener(7));
	     ((EditText)findViewById(R.id.obs3comment)).addTextChangedListener(new textwatcher(7));
	     ((EditText)findViewById(R.id.obs3loc_etother)).setOnTouchListener(new Touch_Listener(8));
	     
	     obs4rdgval = (RadioGroup)findViewById(R.id.obs4_rdg);
	     obs4rdgval.setOnCheckedChangeListener(new checklistenetr(5));
	     
	     ((EditText)findViewById(R.id.obs4comment)).setOnTouchListener(new Touch_Listener(9));
	     ((EditText)findViewById(R.id.obs4comment)).addTextChangedListener(new textwatcher(9));
	     ((EditText)findViewById(R.id.obs4loc_etother)).setOnTouchListener(new Touch_Listener(10));
	     
	     obs5rdgval = (RadioGroup)findViewById(R.id.obs5_rdg);
	     obs5rdgval.setOnCheckedChangeListener(new checklistenetr(6));
	     
	     ((EditText)findViewById(R.id.obs5comment)).setOnTouchListener(new Touch_Listener(11));
	     ((EditText)findViewById(R.id.obs5comment)).addTextChangedListener(new textwatcher(11));
	     ((EditText)findViewById(R.id.obs5loc_etother)).setOnTouchListener(new Touch_Listener(12));
	     
	     obs6rdgval = (RadioGroup)findViewById(R.id.obs6_rdg);
	     obs6rdgval.setOnCheckedChangeListener(new checklistenetr(7));
	     
	     ((EditText)findViewById(R.id.obs6comment)).setOnTouchListener(new Touch_Listener(13));
	     ((EditText)findViewById(R.id.obs6comment)).addTextChangedListener(new textwatcher(13));
	     ((EditText)findViewById(R.id.obs6loc_etother)).setOnTouchListener(new Touch_Listener(14));
	     
	     obs6ardgval = (RadioGroup)findViewById(R.id.obs6a_rdg);
	     obs6ardgval.setOnCheckedChangeListener(new checklistenetr(8));
	     
	     ((EditText)findViewById(R.id.obs6acomment)).setOnTouchListener(new Touch_Listener(15));
	     ((EditText)findViewById(R.id.obs6acomment)).addTextChangedListener(new textwatcher(15));
	     ((EditText)findViewById(R.id.obs6aloc_etother)).setOnTouchListener(new Touch_Listener(16));
	     ((EditText)findViewById(R.id.obs6a_etwidth)).setOnTouchListener(new Touch_Listener(17));
	     ((EditText)findViewById(R.id.obs6a_etwidth)).setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
	     ((EditText)findViewById(R.id.obs6a_etheight)).setOnTouchListener(new Touch_Listener(18));
	     ((EditText)findViewById(R.id.obs6a_etheight)).setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
	     dynlist = (LinearLayout)findViewById(R.id.obs6alayoutdyn);
	     obs6brdgval = (RadioGroup)findViewById(R.id.obs6b_rdg);
	     obs6brdgval.setOnCheckedChangeListener(new checklistenetr(9));
	     
	     ((EditText)findViewById(R.id.obs6bcomment)).setOnTouchListener(new Touch_Listener(19));
	     ((EditText)findViewById(R.id.obs6bcomment)).addTextChangedListener(new textwatcher(19));
	     ((EditText)findViewById(R.id.obs6bloc_etother)).setOnTouchListener(new Touch_Listener(20));
	     
	     obs6crdgval = (RadioGroup)findViewById(R.id.obs6c_rdg);
	     obs6crdgval.setOnCheckedChangeListener(new checklistenetr(10));
	     
	     ((EditText)findViewById(R.id.obs6ccomment)).setOnTouchListener(new Touch_Listener(21));
	     ((EditText)findViewById(R.id.obs6ccomment)).addTextChangedListener(new textwatcher(21));
	     ((EditText)findViewById(R.id.obs6cloc_etother)).setOnTouchListener(new Touch_Listener(22));
	     
	     obs7rdgval = (RadioGroup)findViewById(R.id.obs7_rdg);
	     obs7rdgval.setOnCheckedChangeListener(new checklistenetr(11));
	     
	     ((EditText)findViewById(R.id.obs7comment)).setOnTouchListener(new Touch_Listener(23));
	     ((EditText)findViewById(R.id.obs7comment)).addTextChangedListener(new textwatcher(23));
	     
	     ((EditText)findViewById(R.id.obS7crk_etother)).setOnTouchListener(new Touch_Listener(24));
	     ((EditText)findViewById(R.id.obS7loc_etother)).setOnTouchListener(new Touch_Listener(25));
	     ((EditText)findViewById(R.id.obS7wid_etother)).setOnTouchListener(new Touch_Listener(26));
	     ((EditText)findViewById(R.id.obS7_etlen)).setOnTouchListener(new Touch_Listener(27));
	     ((EditText)findViewById(R.id.obS7_etlen)).setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
	     ((EditText)findViewById(R.id.obS7caus7_etother)).setOnTouchListener(new Touch_Listener(28));
	     
	     obs7ardgval = (RadioGroup)findViewById(R.id.obs7a_rdg);
	     obs7ardgval.setOnCheckedChangeListener(new checklistenetr(12));
	     
	     ((EditText)findViewById(R.id.obs7acomment)).setOnTouchListener(new Touch_Listener(29));
	     ((EditText)findViewById(R.id.obs7acomment)).addTextChangedListener(new textwatcher(29));
	     
	     ((EditText)findViewById(R.id.obs6a_etwidth)).addTextChangedListener(new textwatcher(30));
	     ((EditText)findViewById(R.id.obs6a_etheight)).addTextChangedListener(new textwatcher(31));
	     ((EditText)findViewById(R.id.obS7_etlen)).addTextChangedListener(new textwatcher(32));
	     
	     for(int i=0;i<5;i++)
    	 {
    		try{
    			obs1chkloc[i] = (CheckBox)findViewById(chkbxobs1loc[i]);
    			obs1achkloc[i] = (CheckBox)findViewById(chkbxobs1aloc[i]);
    			obs2chkloc[i] = (CheckBox)findViewById(chkbxobs2loc[i]);
    			obs3chkloc[i] = (CheckBox)findViewById(chkbxobs3loc[i]);
    			obs5chkloc[i] = (CheckBox)findViewById(chkbxobs5loc[i]);
    			obs6chkloc[i] = (CheckBox)findViewById(chkbxobs6loc[i]);
    			obs6achkloc[i] = (CheckBox)findViewById(chkbxobs6aloc[i]);
    			obs6bchkloc[i] = (CheckBox)findViewById(chkbxobs6bloc[i]);
    			obs6cchkloc[i] = (CheckBox)findViewById(chkbxobs6cloc[i]);
    			obs7chkcrck[i] = (CheckBox)findViewById(chkbx7crack[i]);
    			obs7chkloc[i] = (CheckBox)findViewById(chkbx7loc[i]);
    			obs7chkwidt[i] = (CheckBox)findViewById(chkbx7wid[i]);
    		}
			catch(Exception e)
			{
				System.out.println("error= "+e.getMessage());
			}
    	 }
	     for(int i=0;i<8;i++)
    	 {
    		try{
    			obs4chkloc[i] = (CheckBox)findViewById(chkbxobs4loc[i]);
    			obs7chkcond[i] = (CheckBox)findViewById(chkbx7cond[i]);
    		    			
    		}
			catch(Exception e)
			{
				System.out.println("error= "+e.getMessage());
			}
    	 }
	     for(int i=0;i<7;i++)
    	 {
    		try{
    			obs7chkcaus[i] = (CheckBox)findViewById(chkbx7caus[i]);
    		    			
    		}
			catch(Exception e)
			{
				System.out.println("error= "+e.getMessage());
			}
    	 }
	     for(int i=0;i<4;i++)
    	 {
    		try{
    			obs7achkloc[i] = (CheckBox)findViewById(chkbxobs7aloc[i]);
    		    			
    		}
			catch(Exception e)
			{
				System.out.println("error= "+e.getMessage());
			}
    	 }
	}
	class checklistenetr implements OnCheckedChangeListener
	{
    	int i;
		public checklistenetr(int i) {
			// TODO Auto-generated constructor stub
		   this.i=i;
		}

		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			   RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
		        // This puts the value (true/false) into the variable
		        boolean isChecked = checkedRadioButton.isChecked();
		        // If the radiobutton that has changed in check state is now checked...
		        if (isChecked)
		        {
		          switch (i) {
					case 1:
						obs1rdgtxt= checkedRadioButton.getText().toString().trim();
						if(obs1rdgtxt.equals("No")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs1comment)).setText(R.string.novalueobs);
							cf.Set_UncheckBox(obs1chkloc, ((EditText)findViewById(R.id.obs1loc_etother)));
							((LinearLayout)findViewById(R.id.obs1yes)).setVisibility(cf.v1.GONE);}
						else if(obs1rdgtxt.equals("Not Determined")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs1comment)).setText("");
							cf.Set_UncheckBox(obs1chkloc, ((EditText)findViewById(R.id.obs1loc_etother)));
							((LinearLayout)findViewById(R.id.obs1yes)).setVisibility(cf.v1.GONE);}
						else if(obs1rdgtxt.equals("Yes")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs1comment)).setText("");
							((LinearLayout)findViewById(R.id.obs1yes)).setVisibility(cf.v1.VISIBLE);}
					 break;
					case 2:
						obs1ardgtxt= checkedRadioButton.getText().toString().trim();
						if(obs1ardgtxt.equals("No")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs1acomment)).setText(R.string.novalueobs);
							cf.Set_UncheckBox(obs1achkloc, ((EditText)findViewById(R.id.obs1aloc_etother)));
							((LinearLayout)findViewById(R.id.obs1ayes)).setVisibility(cf.v1.GONE);}
						else if(obs1ardgtxt.equals("Not Determined")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs1acomment)).setText("");
							cf.Set_UncheckBox(obs1achkloc, ((EditText)findViewById(R.id.obs1aloc_etother)));
							((LinearLayout)findViewById(R.id.obs1ayes)).setVisibility(cf.v1.GONE);}
						else if(obs1ardgtxt.equals("Yes")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs1acomment)).setText("");
							((LinearLayout)findViewById(R.id.obs1ayes)).setVisibility(cf.v1.VISIBLE);}
					 break;
					case 3:
						obs2rdgtxt= checkedRadioButton.getText().toString().trim();
						if(obs2rdgtxt.equals("No")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs2comment)).setText("None noted to the visible / accessible areas surveyed.");
							cf.Set_UncheckBox(obs2chkloc, ((EditText)findViewById(R.id.obs2loc_etother)));
							((LinearLayout)findViewById(R.id.obs2yes)).setVisibility(cf.v1.GONE);}
						else if(obs2rdgtxt.equals("Not Determined")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs2comment)).setText("Limited access to adjoining properties.");
							cf.Set_UncheckBox(obs2chkloc, ((EditText)findViewById(R.id.obs2loc_etother)));
							((LinearLayout)findViewById(R.id.obs2yes)).setVisibility(cf.v1.GONE);}
						else if(obs2rdgtxt.equals("Yes")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs2comment)).setText("");
							((LinearLayout)findViewById(R.id.obs2yes)).setVisibility(cf.v1.VISIBLE);}
					 break;
					case 4:
						obs3rdgtxt= checkedRadioButton.getText().toString().trim();
						if(obs3rdgtxt.equals("No")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs3comment)).setText("None noted to the visible / accessible areas surveyed.");
							cf.Set_UncheckBox(obs3chkloc, ((EditText)findViewById(R.id.obs3loc_etother)));
							((LinearLayout)findViewById(R.id.obs3yes)).setVisibility(cf.v1.GONE);}
						else if(obs3rdgtxt.equals("Not Determined")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs3comment)).setText("Limited access to adjoining properties.");
							cf.Set_UncheckBox(obs3chkloc, ((EditText)findViewById(R.id.obs3loc_etother)));
							((LinearLayout)findViewById(R.id.obs3yes)).setVisibility(cf.v1.GONE);}
						else if(obs3rdgtxt.equals("Yes")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs3comment)).setText("");
							((LinearLayout)findViewById(R.id.obs3yes)).setVisibility(cf.v1.VISIBLE);}
					 break;
					case 5:
						obs4rdgtxt= checkedRadioButton.getText().toString().trim();
						if(obs4rdgtxt.equals("No")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs4comment)).setText(R.string.novalueobs);
							cf.Set_UncheckBox(obs4chkloc, ((EditText)findViewById(R.id.obs4loc_etother)));
							((LinearLayout)findViewById(R.id.obs4yes)).setVisibility(cf.v1.GONE);}
						else if(obs4rdgtxt.equals("Not Determined")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs4comment)).setText("");
							cf.Set_UncheckBox(obs4chkloc, ((EditText)findViewById(R.id.obs4loc_etother)));
							((LinearLayout)findViewById(R.id.obs4yes)).setVisibility(cf.v1.GONE);}
						else if(obs4rdgtxt.equals("Yes")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs4comment)).setText("");
							((LinearLayout)findViewById(R.id.obs4yes)).setVisibility(cf.v1.VISIBLE);}
					 break;
					case 6:
						obs5rdgtxt= checkedRadioButton.getText().toString().trim();
						if(obs5rdgtxt.equals("No")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs5comment)).setText(R.string.novalueobs);
							cf.Set_UncheckBox(obs5chkloc, ((EditText)findViewById(R.id.obs5loc_etother)));
							((LinearLayout)findViewById(R.id.obs5yes)).setVisibility(cf.v1.GONE);}
						else if(obs5rdgtxt.equals("Not Determined")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs5comment)).setText("");
							cf.Set_UncheckBox(obs5chkloc, ((EditText)findViewById(R.id.obs5loc_etother)));
							((LinearLayout)findViewById(R.id.obs5yes)).setVisibility(cf.v1.GONE);}
						else if(obs5rdgtxt.equals("Yes")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs5comment)).setText("");
							((LinearLayout)findViewById(R.id.obs5yes)).setVisibility(cf.v1.VISIBLE);}
					 break;
					case 7:
						obs6rdgtxt= checkedRadioButton.getText().toString().trim();
						if(obs6rdgtxt.equals("No")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs6comment)).setText(R.string.novalueobs);
							cf.Set_UncheckBox(obs6chkloc, ((EditText)findViewById(R.id.obs6loc_etother)));
							((LinearLayout)findViewById(R.id.obs6yes)).setVisibility(cf.v1.GONE);}
						else if(obs6rdgtxt.equals("Not Determined")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs6comment)).setText("");
							cf.Set_UncheckBox(obs6chkloc, ((EditText)findViewById(R.id.obs6loc_etother)));
							((LinearLayout)findViewById(R.id.obs6yes)).setVisibility(cf.v1.GONE);}
						else if(obs6rdgtxt.equals("Yes")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs6comment)).setText("");
							((LinearLayout)findViewById(R.id.obs6yes)).setVisibility(cf.v1.VISIBLE);}
						break;
					case 8:
						obs6ardgtxt= checkedRadioButton.getText().toString().trim();
						if(obs6ardgtxt.equals("No")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs6acomment)).setText(R.string.novalueobs);
							((EditText)findViewById(R.id.obs6a_etwidth)).setText("");
							((EditText)findViewById(R.id.obs6a_etheight)).setText("");
							cf.Set_UncheckBox(obs6achkloc, ((EditText)findViewById(R.id.obs6aloc_etother)));
							((LinearLayout)findViewById(R.id.obs6ayes)).setVisibility(cf.v1.GONE);}
						else if(obs6ardgtxt.equals("Not Determined")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs6acomment)).setText("");
							((EditText)findViewById(R.id.obs6a_etwidth)).setText("");
							((EditText)findViewById(R.id.obs6a_etheight)).setText("");
							cf.Set_UncheckBox(obs6achkloc, ((EditText)findViewById(R.id.obs6aloc_etother)));
							((LinearLayout)findViewById(R.id.obs6ayes)).setVisibility(cf.v1.GONE);}
						else if(obs6ardgtxt.equals("Yes")){
							cf.hidekeyboard();
							dbquery();
							((EditText)findViewById(R.id.obs6acomment)).setText("");
							((LinearLayout)findViewById(R.id.obs6ayes)).setVisibility(cf.v1.VISIBLE);
							}
					 break;
					case 9:
						obs6brdgtxt= checkedRadioButton.getText().toString().trim();
						if(obs6brdgtxt.equals("No")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs6bcomment)).setText(R.string.novalueobs);
							cf.Set_UncheckBox(obs6bchkloc, ((EditText)findViewById(R.id.obs6bloc_etother)));
							((LinearLayout)findViewById(R.id.obs6byes)).setVisibility(cf.v1.GONE);}
						else if(obs6brdgtxt.equals("Not Determined")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs6bcomment)).setText("");
							cf.Set_UncheckBox(obs6bchkloc, ((EditText)findViewById(R.id.obs6bloc_etother)));
							((LinearLayout)findViewById(R.id.obs6byes)).setVisibility(cf.v1.GONE);}
						else if(obs6brdgtxt.equals("Yes")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs6bcomment)).setText("");
							((LinearLayout)findViewById(R.id.obs6byes)).setVisibility(cf.v1.VISIBLE);}
					 break;
					case 10:
						obs6crdgtxt= checkedRadioButton.getText().toString().trim();
						if(obs6crdgtxt.equals("No")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs6ccomment)).setText(R.string.novalueobs);
							cf.Set_UncheckBox(obs6cchkloc, ((EditText)findViewById(R.id.obs6cloc_etother)));
							((LinearLayout)findViewById(R.id.obs6cyes)).setVisibility(cf.v1.GONE);}
						else if(obs6crdgtxt.equals("Not Determined")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs6ccomment)).setText("");
							cf.Set_UncheckBox(obs6cchkloc, ((EditText)findViewById(R.id.obs6cloc_etother)));
							((LinearLayout)findViewById(R.id.obs6cyes)).setVisibility(cf.v1.GONE);}
						else if(obs6crdgtxt.equals("Yes")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs6ccomment)).setText("");
							((LinearLayout)findViewById(R.id.obs6cyes)).setVisibility(cf.v1.VISIBLE);}
					 break;
					case 11:
						obs7rdgtxt= checkedRadioButton.getText().toString().trim();
						if(obs7rdgtxt.equals("No")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs7comment)).setText(R.string.novalueobs);
							cf.Set_UncheckBox(obs7chkcrck, ((EditText)findViewById(R.id.obS7crk_etother)));
							cf.Set_UncheckBox(obs7chkloc, ((EditText)findViewById(R.id.obS7loc_etother)));
							cf.Set_UncheckBox(obs7chkwidt, ((EditText)findViewById(R.id.obS7wid_etother)));
							((EditText)findViewById(R.id.obS7_etlen)).setText("");
							cf.Set_UncheckBox(obs7chkcond, ((EditText)findViewById(R.id.obS7crk_etother)));
							cf.Set_UncheckBox(obs7chkcaus, ((EditText)findViewById(R.id.obS7caus7_etother)));
							((LinearLayout)findViewById(R.id.obS7yes)).setVisibility(cf.v1.GONE);}
						else if(obs7rdgtxt.equals("Not Determined")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs7comment)).setText("");
							cf.Set_UncheckBox(obs7chkcrck, ((EditText)findViewById(R.id.obS7crk_etother)));
							cf.Set_UncheckBox(obs7chkloc, ((EditText)findViewById(R.id.obS7loc_etother)));
							cf.Set_UncheckBox(obs7chkwidt, ((EditText)findViewById(R.id.obS7wid_etother)));
							((EditText)findViewById(R.id.obS7_etlen)).setText("");
							cf.Set_UncheckBox(obs7chkcond, ((EditText)findViewById(R.id.obS7crk_etother)));
							cf.Set_UncheckBox(obs7chkcaus, ((EditText)findViewById(R.id.obS7caus7_etother)));
							((LinearLayout)findViewById(R.id.obS7yes)).setVisibility(cf.v1.GONE);}
						else if(obs7rdgtxt.equals("Yes")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs7comment)).setText("");
							((LinearLayout)findViewById(R.id.obS7yes)).setVisibility(cf.v1.VISIBLE);}
					 break;
					case 12:
						obs7ardgtxt= checkedRadioButton.getText().toString().trim();
						if(obs7ardgtxt.equals("No")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs7acomment)).setText(R.string.novalueobs7a);
							cf.Set_UncheckBox(obs7achkloc, ((EditText)findViewById(R.id.obS7crk_etother)));
							((LinearLayout)findViewById(R.id.obs7ayes)).setVisibility(cf.v1.GONE);}
						else if(obs7ardgtxt.equals("Not Determined/Not Applicable")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs7acomment)).setText("");
							((EditText)findViewById(R.id.obs7acomment)).setText(R.string.notdetvalueobs7a);
							cf.Set_UncheckBox(obs7achkloc, ((EditText)findViewById(R.id.obS7crk_etother)));
							((LinearLayout)findViewById(R.id.obs7ayes)).setVisibility(cf.v1.GONE);}
						else if(obs7ardgtxt.equals("Yes")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs7acomment)).setText("");
							((LinearLayout)findViewById(R.id.obs7ayes)).setVisibility(cf.v1.VISIBLE);}
					 break;
					default:
						break;
					}
		        }
		     }
    }	
 class Touch_Listener implements OnTouchListener

	    {
	    	   public int type;
	    	   Touch_Listener(int type)
	    		{
	    			this.type=type;
	    			
	    		}
	    	    @Override
	    		public boolean onTouch(View v, MotionEvent event) {

	    			// TODO Auto-generated method stub
	    	    	((EditText) findViewById(v.getId())).setFocusableInTouchMode(true);
			    	((EditText) findViewById(v.getId())).requestFocus();
	    			return false;
	    	    }
	    }
	 class textwatcher implements TextWatcher

     {
	         public int type;
	         textwatcher(int type)
	    	{
	    		this.type=type;
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {


	    		// TODO Auto-generated method stub
	    		if(this.type==1)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs1_tv_type1),250); 
	    			if(s.toString().startsWith(" "))
	    			{
	    				((EditText)findViewById(R.id.obs1comment)).setText("");
	    			}
	    		}
	    		else if(this.type==3)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs1a_tv_type1),250);
	    			if(s.toString().startsWith(" "))
	    			{
	    				((EditText)findViewById(R.id.obs1acomment)).setText("");
	    			}
	    		}
	    		else if(this.type==5)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs2_tv_type1),250); 
	    			if(s.toString().startsWith(" "))
	    			{
	    				((EditText)findViewById(R.id.obs2comment)).setText("");
	    			}
	    		}
	    		else if(this.type==7)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs3_tv_type1),250); 
	    			if(s.toString().startsWith(" "))
	    			{
	    				((EditText)findViewById(R.id.obs3comment)).setText("");
	    			}
	    		}
	    		else if(this.type==9)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs4_tv_type1),250); 
	    			if(s.toString().startsWith(" "))
	    			{
	    				((EditText)findViewById(R.id.obs4comment)).setText("");
	    			}
	    		}
	    		else if(this.type==11)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs5_tv_type1),250);
	    			if(s.toString().startsWith(" "))
	    			{
	    				((EditText)findViewById(R.id.obs5comment)).setText("");
	    			}
	    		}
	    		else if(this.type==13)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs6_tv_type1),250);
	    			if(s.toString().startsWith(" "))
	    			{
	    				((EditText)findViewById(R.id.obs6comment)).setText("");
	    			}
	    		}
	    		else if(this.type==15)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs6a_tv_type1),250);
	    			if(s.toString().startsWith(" "))
	    			{
	    				((EditText)findViewById(R.id.obs6acomment)).setText("");
	    			}
	    		}
	    		else if(this.type==19)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs6b_tv_type1),250);
	    			if(s.toString().startsWith(" "))
	    			{
	    				((EditText)findViewById(R.id.obs6bcomment)).setText("");
	    			}
	    		}
	    		else if(this.type==21)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs6c_tv_type1),250);
	    			if(s.toString().startsWith(" "))
	    			{
	    				((EditText)findViewById(R.id.obs6ccomment)).setText("");
	    			}
	    		}
	    		else if(this.type==23)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs7_tv_type1),250);
	    			if(s.toString().startsWith(" "))
	    			{
	    				((EditText)findViewById(R.id.obs7comment)).setText("");
	    			}
	    		}
	    		else if(this.type==29)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs7a_tv_type1),250);
	    			if(s.toString().startsWith(" "))
	    			{
	    				((EditText)findViewById(R.id.obs7acomment)).setText("");
	    			}
	    		}
	    		else if(this.type==30)
	    		{
	    			 if(!s.toString().equals("")){
	    			     if(Character.toString(s.toString().charAt(0)).equals("0") || Character.toString(s.toString().charAt(0)).equals("."))
		    			 {
		    				 cf.ShowToast("Invalid Entry! Please enter valid Approximate width of tree.", 0);
		    				 ((EditText)findViewById(R.id.obs6a_etwidth)).setText("");
		    			 }
	    			 }
	    		}
	    		else if(this.type==31)
	    		{
	    			 if(!s.toString().equals("")){
	    			     if(Character.toString(s.toString().charAt(0)).equals("0") || Character.toString(s.toString().charAt(0)).equals("."))
		    			 {
		    				 cf.ShowToast("Invalid Entry! Please enter valid Approximate height of tree.", 0);
		    				 ((EditText)findViewById(R.id.obs6a_etheight)).setText("");
		    			 }
	    			 }
	    		}
	    		else if(this.type==32)
	    		{
	    			 if(!s.toString().equals("")){
	    			     if(Character.toString(s.toString().charAt(0)).equals("0") || Character.toString(s.toString().charAt(0)).equals("."))
		    			 {
		    				 cf.ShowToast("Invalid Entry! Please enter valid Approximate Length of cracks.", 0);
		    				 ((EditText)findViewById(R.id.obS7_etlen)).setText("");
		    			 }
	    			 }
	    		}
	    	}

	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,

	    			int after) {
	    		// TODO Auto-generated method stub
	     	}

	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,

	    			int count) {
	    		// TODO Auto-generated method stub
	    	}
	   }	

	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.shwobs1:
			  hidelayouts();
			  ((ImageView)findViewById(R.id.shwobs1)).setVisibility(cf.v1.GONE);
			  ((ImageView)findViewById(R.id.hdobs1)).setVisibility(cf.v1.VISIBLE);
			  ((LinearLayout)findViewById(R.id.obs1lin)).setVisibility(cf.v1.VISIBLE);
			  ((TextView)findViewById(R.id.fsttxt)).requestFocus();
			  break;
		case R.id.hdobs1:
			((ImageView)findViewById(R.id.shwobs1)).setVisibility(cf.v1.VISIBLE);
			((ImageView)findViewById(R.id.hdobs1)).setVisibility(cf.v1.GONE);
			((LinearLayout)findViewById(R.id.obs1lin)).setVisibility(cf.v1.GONE);
			break;
		case R.id.shwobs2:
			  hidelayouts();
			  ((ImageView)findViewById(R.id.shwobs2)).setVisibility(cf.v1.GONE);
			  ((ImageView)findViewById(R.id.hdobs2)).setVisibility(cf.v1.VISIBLE);
			  ((LinearLayout)findViewById(R.id.obs2lin)).setVisibility(cf.v1.VISIBLE);
			  ((TextView)findViewById(R.id.sectxt)).requestFocus();
			  break;
		case R.id.hdobs2:
			((ImageView)findViewById(R.id.shwobs2)).setVisibility(cf.v1.VISIBLE);
			((ImageView)findViewById(R.id.hdobs2)).setVisibility(cf.v1.GONE);
			((LinearLayout)findViewById(R.id.obs2lin)).setVisibility(cf.v1.GONE);
			break;
		case R.id.shwobs3:
			  hidelayouts();
			  ((ImageView)findViewById(R.id.shwobs3)).setVisibility(cf.v1.GONE);
			  ((ImageView)findViewById(R.id.hdobs3)).setVisibility(cf.v1.VISIBLE);
			  ((LinearLayout)findViewById(R.id.obs3lin)).setVisibility(cf.v1.VISIBLE);
			  ((TextView)findViewById(R.id.thdtxt)).requestFocus();
			  break;
		case R.id.hdobs3:
			((ImageView)findViewById(R.id.shwobs3)).setVisibility(cf.v1.VISIBLE);
			((ImageView)findViewById(R.id.hdobs3)).setVisibility(cf.v1.GONE);
			((LinearLayout)findViewById(R.id.obs3lin)).setVisibility(cf.v1.GONE);
			break;
		case R.id.shwobs4:
			  hidelayouts();
			  ((ImageView)findViewById(R.id.shwobs4)).setVisibility(cf.v1.GONE);
			  ((ImageView)findViewById(R.id.hdobs4)).setVisibility(cf.v1.VISIBLE);
			  ((LinearLayout)findViewById(R.id.obs4lin)).setVisibility(cf.v1.VISIBLE);
			  ((TextView)findViewById(R.id.fourtxt)).requestFocus();
			  break;
		case R.id.hdobs4:
			((ImageView)findViewById(R.id.shwobs4)).setVisibility(cf.v1.VISIBLE);
			((ImageView)findViewById(R.id.hdobs4)).setVisibility(cf.v1.GONE);
			((LinearLayout)findViewById(R.id.obs4lin)).setVisibility(cf.v1.GONE);
			break;
		case R.id.shwobs5:
			  hidelayouts();
			  ((ImageView)findViewById(R.id.shwobs5)).setVisibility(cf.v1.GONE);
			  ((ImageView)findViewById(R.id.hdobs5)).setVisibility(cf.v1.VISIBLE);
			  ((LinearLayout)findViewById(R.id.obs5lin)).setVisibility(cf.v1.VISIBLE);
			  ((TextView)findViewById(R.id.fivtxt)).requestFocus();
			  break;
		case R.id.hdobs5:
			((ImageView)findViewById(R.id.shwobs5)).setVisibility(cf.v1.VISIBLE);
			((ImageView)findViewById(R.id.hdobs5)).setVisibility(cf.v1.GONE);
			((LinearLayout)findViewById(R.id.obs5lin)).setVisibility(cf.v1.GONE);
			break;
		case R.id.shwobs6:
			  hidelayouts();
			  ((ImageView)findViewById(R.id.shwobs6)).setVisibility(cf.v1.GONE);
			  ((ImageView)findViewById(R.id.hdobs6)).setVisibility(cf.v1.VISIBLE);
			  ((LinearLayout)findViewById(R.id.obs6lin)).setVisibility(cf.v1.VISIBLE);
			  ((TextView)findViewById(R.id.sixtxt)).requestFocus();
			  break;
		case R.id.hdobs6:
			((ImageView)findViewById(R.id.shwobs6)).setVisibility(cf.v1.VISIBLE);
			((ImageView)findViewById(R.id.hdobs6)).setVisibility(cf.v1.GONE);
			((LinearLayout)findViewById(R.id.obs6lin)).setVisibility(cf.v1.GONE);
			break;
		case R.id.shwobs7:
			  hidelayouts();
			  ((ImageView)findViewById(R.id.shwobs7)).setVisibility(cf.v1.GONE);
			  ((ImageView)findViewById(R.id.hdobs7)).setVisibility(cf.v1.VISIBLE);
			  ((LinearLayout)findViewById(R.id.obs7lin)).setVisibility(cf.v1.VISIBLE);
			  ((TextView)findViewById(R.id.sevtxt)).requestFocus();
			  break;
		case R.id.hdobs7:
			((ImageView)findViewById(R.id.shwobs7)).setVisibility(cf.v1.VISIBLE);
			((ImageView)findViewById(R.id.hdobs7)).setVisibility(cf.v1.GONE);
			((LinearLayout)findViewById(R.id.obs7lin)).setVisibility(cf.v1.GONE);
			break;
		case R.id.obs1_help: /** HELP FOR OPTION1 **/
			  cf.alerttitle="HELP";
			  cf.alertcontent="<p>Inspectors need to pay "+
				"close attention to yard areas on every elevation of the insured "+
				"premises, looking for any undulations or other abnormal activity "+
				"possibly related to a potential sinkhole issue.  These undulations are "+
				"particularly important to sinkhole-prone areas.</p><p>A photographic "+
				"record of all the surrounding areas of the home or building must be "+
				"provided as part of the inspector\'s process which will be "+
				"examined by the Quality Assurance Department prior to release of "+
				"report.</p><p>Where any issues exist, inspectors must comment if any "+ 
				"issues exist relating to the regular land surfaces.</p><p>If the "+
				"Inspector is unable to determine the condition of the entire lot based "+ 
				"on size, complexity, extent of vegetation or wooded area, this must be "+
				"marked as \"Not Determined\" and comments/photographs  "+
				"must be provided.</p>";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
		case R.id.obs1a_help: /** HELP FOR OPTIONA **/
			  cf.alerttitle="HELP";
			  cf.alertcontent="<p>If there is any evidence "+
				"of buried debris or disclosure of any nature, this must be clearly "+
				"outlined on the inspection report with ample comments.  Clear "+
				"photographs must be provided of all yard and surrounding "+
				"areas.</p><p>Please note that this condition must be outlined, commented "+
				"and photographed within the report.</p>";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
		case R.id.obs2_help: /** HELP FOR OBSERVATION2 **/
			  cf.alerttitle="HELP";
			  cf.alertcontent="<p>Inspectors need to review "+
				"the adjoining property on both sides of the street or behind, if needed, "+
				"to determine if any evidence of previous sinkhole or settlement activity "+
				"is noted.  Pay close attention to previous repairs, potential major "+
				"settlement, in addition to any undulations or issues noted to the "+
				"surrounding lot-ground.  Clear photographs, documentation and "+
				"commentary must be provided.  Inspectors are not required to enter "+
				"adjoining premises and to report on any elevation not visible.</p>";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
		 case R.id.obs3_help: /** HELP FOR OBSERVATION3 **/
			  cf.alerttitle="HELP";
			  cf.alertcontent="<p>A \"collapse feature\" is an actual hole in the ground that " +
			  		"resembles a " +
			  		"potential sinkhole.   The Inspector should clearly comment, photograph " +
			  		"and outline any area that resembles a sinkhole or soil collapse found " +
			  		"within the insureds premises or adjoining property.</p>";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
		 case R.id.obs4_help: /** HELP FOR OBSERVATION4 **/
			  cf.alerttitle="HELP";
			  cf.alertcontent="<p>Soil Erosion/washout can "+						  
                 "be caused by many reasons, many of which are normal, typically caused by "+
				  "deferred maintenance.   Examples include broken sprinkler lines, "+
					"inadequate rainwater control, downspout terminations, to mention a few.  "+
					"Where soil erosion is noted, it should be properly identified, "+
					"photographed and commented in addition to whether settlement activity of "+
					"any nature is found or if the washout is severe enough to undermine the "+
					"supporting structure or require emergency repair procedures.</p><p>If "+
					"the washout or erosion is associated with potential soil collapse or "+
					"sinkhole issues, this should be clearly outlined in the "+
					"report.</p><p>Photographs should include both close-up and general views "+
					"of the issues and building lines to show auditors and underwriting, the "+
					"condition of the building lines from a uniform - level and plumb "+
					"perspective.  The close up pictures should have a photograph of a "+
					"4\" spirit level against the wall showing clearly the plumbness "+
					"of the wall next to the soil erosion areas.  Multiple photographs will "+
					"be required if various areas of soil erosion is found.</p>";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
		  case R.id.obs5_help: /** HELP FOR OBSERVATION5 **/
			  cf.alerttitle="HELP";
			  cf.alertcontent="<p>Cracking is common to " +
			  		"almost all concrete surfaces outside of a home/building because of the " +
			  		"nature of the curing process, exposure, surrounding features such as " +
			  		"large trees, washout from broken sprinkler lines, running water run-off, " +
			  		"etc.</p><p>All cracking must be documented and recorded if present and " +
			  		"must be photographed and provided within the inspection report.  The " +
			  		"exact cause of the cracking cannot be determined without specialist " +
			  		"testing techniques and monitoring.  Inspectors should be cautioned to " +
			  		"use terms such as \"Hairline cracking or cracking appears to be " +
			  		"the result of..\"</p><p>If cracking is caused by tree " +
			  		"root damage from adjoining trees or shrubs, or from broken sprinkler " +
			  		"lines, this should be clearly photographed and outlined.  Severe " +
			  		"cracking, sinking, trip hazards, etc. should all be commented and " +
			  		"photographed.</p><p>If cracks are hairline in nature only and typically " +
			  		"the result of inadequate compaction or poor construction practices, this " +
			  		"should be pointed out within the comment areas also.</p>";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
		  case R.id.obs6_help: /** HELP FOR OBSERVATION6 OPTION1 **/
			  cf.alerttitle="HELP";
			  cf.alertcontent="<p>If trip hazards are noted " +
			  		"to any concrete hard surface, these need to be clearly photographed and " +
			  		"commented with the most probable reason as to the cause if " +
			  		"available.</p>";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
		  case R.id.obs6a_help: /** HELP FOR OBSERVATION6 OPTIONA **/
			  cf.alerttitle="HELP";
			  cf.alertcontent="<p>All large trees, " +
			  		"approximately 12inches or larger must be documented on the inspection " +
			  		" report.  Photographs and commentary on the proximity, i.e. distance to " +
			  		" home, must be provided as part of the inspection report.  If large trees " +
			  		" from adjoining property are encroaching on the insured premises, this " +
			  		" needs to be outlined by using the \"Other\" selection with " +
			  		" comments and photograph captions.</p>";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
		  case R.id.obs6b_help: /** HELP FOR OBSERVATION6 OPTIONB **/
			  cf.alerttitle="HELP";
			  cf.alertcontent="<p>Cypress trees are " +
			  		"associated with wet/saturated soils.  Any cypress trees or knees located " +
			  		"on the property should be identified, photographed and commented with " +
			  		"the approximate distance from the home or building being inspected.  " +
			  		"Cypress trees on adjoining properties within close proximity should also " +
			  		"be outlined.</p>";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
		  case R.id.obs6c_help: /** HELP FOR OBSERVATION6 OPTIONC **/
			  cf.alerttitle="HELP";
			  cf.alertcontent="<p>Large trees more recently " +
			  		"removed may not be visible and it is for this reason that questions must " +
			  		"be raised to the policyholder relating to this requirement.  Uneven " +
			  		"ground surfaces should be questioned and recorded.  Such conditions may " +
			  		"affect the insured\'s ability to gain property insurance and full " +
			  		"disclaimer with policyholder should be made.</p><p>Large trees recently " +
			  		"removed within proximity of the building structure are concerning as " +
			  		"tree roots left in the ground may decay and cause subsidence or " +
			  		"settlement in these regionalized areas down line.</p><p>Inspectors " +
			  		"should pay attention to the adjoining property and neighborhood to " +
			  		"determine the general tree coverage and how it compares to the subject " +
			  		"property.</p><p>Clear photographs and comments are needed.</p>";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
		  case R.id.obs7_help: /** HELP FOR OBSERVATION7 OPTION1 **/
			  cf.alerttitle="HELP";
			  cf.alertcontent="<p>Foundation cracks, while " +
			  		"not uncommon are more serious in nature depending on the location, " +
			  		"extent and settlement observed.  Foundation cracks at building corners, " +
			  		"changes in direction, and more recent additions are more common and " +
			  		"typically related to differential settlement or differential materials. " +
			  		"This type of cracking will be hairline and typically vertical in " +
			  		"nature.</p><p>Foundation cracking that is diagonal or vertical in nature " +
			  		"where one end of the crack is wider than the other may be symptomatic to " +
			  		"a more serious issue.  Inspectors should measure and describe the " +
			  		"conditions noted, in addition to the surrounding conditions including " +
			  		"but not limited to building lines, elevations, providing photographs " +
			  		"using a 4&amp;#34; spirit level, both close up and from a " +
			  		"distance.</p><p>Where cracks are present and measurements are needed, " +
			  		"these should be accompanied by clear photographs of the measurements " +
			  		"taken and proper captions.</p><p>While diagnosing cracks, the conditions " +
			  		"of cracks are also indicative of the potential for future or ongoing " +
			  		"settlement.   Older cracks associated with initial settlement are " +
			  		"typically covered with dust, debris and cobwebs, whereas more serious " +
			  		"cracks, or cracks which have occurred more recently will have sharp " +
			  		"edges and will be clean.  Cracking through joints in block work or brick " +
			  		"work is also typically considered gradual verses cracking through the " +
			  		"block or brickwork which can be considered more sudden or " +
			  		"serious.</p><p>If any building finish such as paint or caulking is found " +
			  		"to be squeezed out, particularly if painting has occurred since original " +
			  		"construction, this is indicative of settlement that has occurred since " +
			  		"the alterations or remodeling has taken place.  Evidence of more recent " +
			  		"settlement is always a concern.</p><p>Full documentation of the " +
			  		"observations should be provided.</p>";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
		  case R.id.obs7a_help: /** HELP FOR OBSERVATION7-OPTIONA **/
			  cf.alerttitle="HELP";
			  cf.alertcontent="<p>Retaining walls are " +
			  		"typically not found on most homes however where homes are built on a " +
			  		"slope or a swimming pool is in close proximity to adjoining lakes and " +
			  		"ponds, retaining walls are used.</p><p>Inspectors should properly " +
			  		"examine retaining walls and look for any evidence of unevenness, " +
			  		"settlement, leaning, decay, erosion, water washout, and properly " +
			  		"document and comment any issues that were found with the retaining walls " +
			  		"noted.  Retaining wall features, depending on type and proximity to the " +
			  		"home, could undermine the home support system.</p><p>Inspectors should " +
			  		"also familiarize themselves with the various retaining walls used in " +
			  		"residential and commercial construction.</p>";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
		case R.id.obs1loc5: /** CHECK BOX - OTHER 1 **/
   		 if(obs1chkloc[4].isChecked())
			  {
   			  ((EditText)findViewById(R.id.obs1loc_etother)).setVisibility(v.VISIBLE);
   			  ((EditText)findViewById(R.id.obs1loc_etother)).requestFocus();
			  }
			  else
			  {
				  cf.clearother(((EditText)findViewById(R.id.obs1loc_etother)));
			  }
			break;
		case R.id.obs1aloc5: /** CHECK BOX - OTHER 1A **/
	   		 if(obs1achkloc[4].isChecked())
				  {
	   			  ((EditText)findViewById(R.id.obs1aloc_etother)).setVisibility(v.VISIBLE);
	   			  ((EditText)findViewById(R.id.obs1aloc_etother)).requestFocus();
				  }
				  else
				  {
					  cf.clearother(((EditText)findViewById(R.id.obs1aloc_etother)));
				  }
				break;
		case R.id.obs2loc5: /** CHECK BOX - OTHER 2 **/
	   		 if(obs2chkloc[4].isChecked())
				  {
	   			  ((EditText)findViewById(R.id.obs2loc_etother)).setVisibility(v.VISIBLE);
	   			  ((EditText)findViewById(R.id.obs2loc_etother)).requestFocus();
				  }
				  else
				  {
					  cf.clearother(((EditText)findViewById(R.id.obs2loc_etother)));
				  }
				break;
		case R.id.obs3loc5: /** CHECK BOX - OTHER 3 **/
	   		 if(obs3chkloc[4].isChecked())
				  {
	   			  ((EditText)findViewById(R.id.obs3loc_etother)).setVisibility(v.VISIBLE);
	   			  ((EditText)findViewById(R.id.obs3loc_etother)).requestFocus();
				  }
				  else
				  {
					  cf.clearother(((EditText)findViewById(R.id.obs3loc_etother)));
				  }
				break;
		case R.id.obs4loc8: /** CHECK BOX - OTHER 4 **/
	   		 if(obs4chkloc[7].isChecked())
				  {
	   			  ((EditText)findViewById(R.id.obs4loc_etother)).setVisibility(v.VISIBLE);
	   			  ((EditText)findViewById(R.id.obs4loc_etother)).requestFocus();
				  }
				  else
				  {
					  cf.clearother(((EditText)findViewById(R.id.obs4loc_etother)));
				  }
				break;
		case R.id.obs5loc5: /** CHECK BOX - OTHER 5 **/
	   		 if(obs5chkloc[4].isChecked())
				  {
	   			  ((EditText)findViewById(R.id.obs5loc_etother)).setVisibility(v.VISIBLE);
	   			  ((EditText)findViewById(R.id.obs5loc_etother)).requestFocus();
				  }
				  else
				  {
					  cf.clearother(((EditText)findViewById(R.id.obs5loc_etother)));
				  }
				break;
		case R.id.obs6loc5: /** CHECK BOX - OTHER 6 **/
	   		 if(obs6chkloc[4].isChecked())
				  {
	   			  ((EditText)findViewById(R.id.obs6loc_etother)).setVisibility(v.VISIBLE);
	   			  ((EditText)findViewById(R.id.obs6loc_etother)).requestFocus();
				  }
				  else
				  {
					  cf.clearother(((EditText)findViewById(R.id.obs6loc_etother)));
				  }
				break;
		case R.id.obs6aloc5: /** CHECK BOX - OTHER 6a **/
	   		 if(obs6achkloc[4].isChecked())
				  {
	   			  ((EditText)findViewById(R.id.obs6aloc_etother)).setVisibility(v.VISIBLE);
	   			  ((EditText)findViewById(R.id.obs6aloc_etother)).requestFocus();
				  }
				  else
				  {
					  cf.clearother(((EditText)findViewById(R.id.obs6aloc_etother)));
				  }
				break;
		case R.id.obs6bloc5: /** CHECK BOX - OTHER 6B **/
	   		 if(obs6bchkloc[4].isChecked())
				  {
	   			  ((EditText)findViewById(R.id.obs6bloc_etother)).setVisibility(v.VISIBLE);
	   			  ((EditText)findViewById(R.id.obs6bloc_etother)).requestFocus();
				  }
				  else
				  {
					  cf.clearother(((EditText)findViewById(R.id.obs6bloc_etother)));
				  }
				break;
		case R.id.obs6cloc5: /** CHECK BOX - OTHER 6C **/
	   		 if(obs6cchkloc[4].isChecked())
				  {
	   			  ((EditText)findViewById(R.id.obs6cloc_etother)).setVisibility(v.VISIBLE);
	   			  ((EditText)findViewById(R.id.obs6cloc_etother)).requestFocus();
				  }
				  else
				  {
					  cf.clearother(((EditText)findViewById(R.id.obs6cloc_etother)));
				  }
				break;
		case R.id.obS7crk5: /** CHECK BOX - TYPE OF CRACK 7 **/
		   		 if(obs7chkcrck[4].isChecked())
					  {
		   			  ((EditText)findViewById(R.id.obS7crk_etother)).setVisibility(v.VISIBLE);
		   			  ((EditText)findViewById(R.id.obS7crk_etother)).requestFocus();
					  }
					  else
					  {
						  cf.clearother(((EditText)findViewById(R.id.obS7crk_etother)));
					  }
					break;
		   	case R.id.obS7loc5: /** CHECK BOX - LOCATION OF CRACK 7 **/
		   		 if(obs7chkloc[4].isChecked())
					  {
		   			  ((EditText)findViewById(R.id.obS7loc_etother)).setVisibility(v.VISIBLE);
		   			  ((EditText)findViewById(R.id.obS7loc_etother)).requestFocus();
					  }
					  else
					  {
						  cf.clearother(((EditText)findViewById(R.id.obS7loc_etother)));
					  }
					break;
		   	case R.id.obS7wid5: /** CHECK BOX - WIDTH OF CRACK 7 **/
		   		 if(obs7chkwidt[4].isChecked())
					  {
		   			  ((EditText)findViewById(R.id.obS7wid_etother)).setVisibility(v.VISIBLE);
		   			  ((EditText)findViewById(R.id.obS7wid_etother)).requestFocus();
					  }
					  else
					  {
						  cf.clearother(((EditText)findViewById(R.id.obS7wid_etother)));
					  }
					break;
		   	case R.id.obS7caus7: /** CHECK BOX - POSSIBLE CAUSE  7 **/
		   		 if(obs7chkcaus[6].isChecked())
					  {
		   			  ((EditText)findViewById(R.id.obS7caus7_etother)).setVisibility(v.VISIBLE);
		   			  ((EditText)findViewById(R.id.obS7caus7_etother)).requestFocus();
					  }
					  else
					  {
						  cf.clearother(((EditText)findViewById(R.id.obS7caus7_etother)));
					  }
					break;
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.save:
			if(obs1rdgtxt.equals(""))
			{
				cf.ShowToast("Please select the option for Question No.1", 0);
			}
			else
			{
				if(obs1rdgtxt.equals("Yes"))
				{
					 obs1chkloc_val= cf.getselected_chk(obs1chkloc);
			    	 String chk_Value1locother=(obs1chkloc[obs1chkloc.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.obs1loc_etother)).getText().toString():""; // append the other text value in to the selected option
			    	 obs1chkloc_val+=chk_Value1locother;
			    	 if(obs1chkloc_val.equals(""))
					 {
						 cf.ShowToast("Please select Locations for Question No.1." , 0);
					 }
			    	 else
			    	 {
			    		 if(obs1chkloc[obs1chkloc.length-1].isChecked())
						 {
							 if(chk_Value1locother.trim().equals("&#40;"))
							 {
								 cf.ShowToast("Please enter the Other text for Locations under Question No.1.", 0);
								 cf.setFocus(((EditText)findViewById(R.id.obs1loc_etother)));
							 }
							 else
							 {
								obs1_comments();	
							 }
						 }
						 else
						 {
							 obs1_comments();
						 }
			    	 }
				}
				else
				{
					obs1_comments();
				}
			}
			break;
		 case R.id.save6a:
			 
			 	if(((EditText)findViewById(R.id.obs6a_etwidth)).getText().toString().equals(""))
				{
					cf.ShowToast("Please enter the Approximate width of tree for Question No.6(a)", 0);
			 		cf.setFocus(((EditText)findViewById(R.id.obs6a_etwidth)));
				}
				else
				{
					if(((EditText)findViewById(R.id.obs6a_etheight)).getText().toString().equals(""))
						{
							cf.ShowToast("Please enter the Approximate height of tree for Question No.6(a)", 0);
							cf.setFocus(((EditText)findViewById(R.id.obs6a_etheight)));
						}
						else
						{
							 
							 obs6achkloc_val= cf.getselected_chk(obs6achkloc);
							 System.out.println("obs6achkloc_valbfre="+obs6achkloc_val);
							 String chk_Valueobs6Aother =(obs6achkloc[obs6achkloc.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.obs6aloc_etother)).getText().toString():""; // append the other text value in to the selected option
							 System.out.println("chk_Valueobs6Aother="+chk_Valueobs6Aother);
							 if(chk_Valueobs6Aother.contains("&#40;")&& obs6achkloc_val.equals("")){
								 chk_Valueobs6Aother = chk_Valueobs6Aother.replace("&#40;", "");
							 }
							 obs6achkloc_val+=chk_Valueobs6Aother;
							 System.out.println("obs6achkloc_val="+obs6achkloc_val);
							 /*if(obs6achkloc_val.equals(""))
							 {
								 obs6achkloc_val+=chk_Valueobs6Aother;
							 }
							 else if(!chk_Valueobs6Aother.equals(""))
							 {
								 obs6achkloc_val+=","+chk_Valueobs6Aother;
							 }*/
							 
							 if(obs6achkloc_val.equals(""))
							 {
								 /*if(obs6achkloc[obs6achkloc.length-1].isChecked()){
									 cf.ShowToast("Please enter the Other text for Question No.6(a) Locations." , 0);
									 cf.setFocus(((EditText)findViewById(R.id.obs6aloc_etother)));
								 }
								 else{*/
								 cf.ShowToast("Please select Locations for Question No.6(a)." , 0);//}
							 }
							 else 
							 {
								 if(obs6achkloc[obs6achkloc.length-1].isChecked())
								 {
									 if(chk_Valueobs6Aother.trim().equals("&#40;"))
									 {
										 cf.ShowToast("Please enter the Other text for Question No.6(a) Locations.", 0);
										 cf.setFocus(((EditText)findViewById(R.id.obs6aloc_etother)));
									 }
									 else
									 {
										 if(((Button)findViewById(R.id.save6a)).getText().toString().equals("Save"))
										 {
										     Chk_OBS6A_Insert();	
										 }
										 else
										 {
											 Chk_OBS6A_Update();
										 }
									 }
								 }
								 else
								 {
									 if(((Button)findViewById(R.id.save6a)).getText().toString().equals("Save"))
									 {
										 Chk_OBS6A_Insert();
									 }
									 else
									 {
										 Chk_OBS6A_Update();
									 }
								 }
							 }
							
						}
				
				}
			 
				
			  break;
		 case R.id.clear6a:
			 cleartree();
			 break;
		default:
			break;
		}
	}
	private void Chk_OBS6A_Update() {
		// TODO Auto-generated method stub
		cf.arr_db.execSQL("UPDATE "+cf.SINK_Obstreetbl+ " set Width='"+cf.encode(((EditText)findViewById(R.id.obs6a_etwidth)).getText().toString())+"',"+
				   "Height='"+cf.encode(((EditText)findViewById(R.id.obs6a_etheight)).getText().toString())+"',"+
	               "Location='"+cf.encode(obs6achkloc_val)+"'where fld_srid='"+cf.selectedhomeid+"' and OB1TId='"+edt+"'");
		
		  cf.ShowToast("Tree has been updated successfully.",0);
		  dbquery();
		  cleartree();
		
	}
	private void cleartree()
	{
		 ((EditText)findViewById(R.id.obs6a_etwidth)).setText("");
	      ((EditText)findViewById(R.id.obs6a_etheight)).setText("");
	      cf.Set_UncheckBox(obs6achkloc,((EditText)findViewById(R.id.obs6aloc_etother)));
	      ((Button)findViewById(R.id.save6a)).setText("Save");
	}
	private void Chk_OBS6A_Insert() {
		// TODO Auto-generated method stub
		  cf.arr_db.execSQL("INSERT INTO "+cf.SINK_Obstreetbl+ "(fld_srid,Width,Height,Location)" +
					"VALUES ('"+cf.selectedhomeid+"','"+cf.encode(((EditText)findViewById(R.id.obs6a_etwidth)).getText().toString())+"',"+
		 			"'"+cf.encode(((EditText)findViewById(R.id.obs6a_etheight)).getText().toString())+"','"+cf.encode(obs6achkloc_val)+"')");
		  cf.ShowToast("Tree has been saved successfully.",0);
		  dbquery();
	        ((EditText)findViewById(R.id.obs6a_etwidth)).setText("");
	        ((EditText)findViewById(R.id.obs6a_etheight)).setText("");
	        cf.Set_UncheckBox(obs6achkloc,((EditText)findViewById(R.id.obs6aloc_etother)));
	}
	private void obs1_comments() {

		// TODO Auto-generated method stub
		if(((EditText)findViewById(R.id.obs1comment)).getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.1 */
		{
			cf.ShowToast("Please enter the Question No.1 Comments.", 0);
			cf.setFocus(((EditText)findViewById(R.id.obs1comment)));
		}
		else
		{
			chk_Obs1aValidation();
		}
	}
	private void chk_Obs1aValidation() {
		// TODO Auto-generated method stub
		if(obs1ardgtxt.equals(""))
		{
			cf.ShowToast("Please select the option for Question No.1(a).", 0);
		}
		else
		{
			if(obs1ardgtxt.equals("Yes"))
			{
				 obs1achkloc_val= cf.getselected_chk(obs1achkloc);
		    	 String chk_Value1alocother=(obs1achkloc[obs1achkloc.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.obs1aloc_etother)).getText().toString():""; // append the other text value in to the selected option
		    	 obs1achkloc_val+=chk_Value1alocother;
		    	 if(obs1achkloc_val.equals(""))
				 {
					 cf.ShowToast("Please select Locations for Question No.1(a)." , 0);
				 }
		    	 else
		    	 {
		    		 if(obs1achkloc[obs1achkloc.length-1].isChecked())
					 {
						 if(chk_Value1alocother.trim().equals("&#40;"))
						 {
							 cf.ShowToast("Please enter the Other text for Locations under Question No.1(a).", 0);
							 cf.setFocus(((EditText)findViewById(R.id.obs1aloc_etother)));
						 }
						 else
						 {
							 obs1a_comments();	
						 }
					 }
					 else
					 {
						 obs1a_comments();
					 }
		    	 }
			}
			else
			{
				obs1a_comments();
			}
		}
	}
	private void obs1a_comments() {
		// TODO Auto-generated method stub
		if(((EditText)findViewById(R.id.obs1acomment)).getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.1(a) */
		{
			cf.ShowToast("Please enter Question No.1(a) Comments.", 0);
			cf.setFocus(((EditText)findViewById(R.id.obs1acomment)));
		}
		else
		{
			chkobs2_validation();
		}
	}
	private void chkobs2_validation() {

		// TODO Auto-generated method stub
		if(obs2rdgtxt.equals(""))
		{
			cf.ShowToast("Please select the option for Question No.2.", 0);
		}
		else
		{
			if(obs2rdgtxt.equals("Yes"))
			{
				 obs2chkloc_val= cf.getselected_chk(obs2chkloc);
		    	 String chk_Value2locother=(obs2chkloc[obs2chkloc.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.obs2loc_etother)).getText().toString():""; // append the other text value in to the selected option
		    	 obs2chkloc_val+=chk_Value2locother;
		    	 if(obs2chkloc_val.equals(""))
				 {
					 cf.ShowToast("Please select Locations for Question No.2." , 0);
				 }
		    	 else
		    	 {
		    		 if(obs2chkloc[obs2chkloc.length-1].isChecked())
					 {
						 if(chk_Value2locother.trim().equals("&#40;"))
						 {
							 cf.ShowToast("Please enter the Other text for Locations under Question No.2.", 0);
							 cf.setFocus(((EditText)findViewById(R.id.obs2loc_etother)));
						 }
						 else
						 {
							 obs2_comments();	
						 }
					 }
					 else
					 {
						 obs2_comments();
					 }
		    	 }
			}
			else
			{
				obs2_comments();
			}
		}
	}
	private void obs2_comments() {
		// TODO Auto-generated method stub
		if(((EditText)findViewById(R.id.obs2comment)).getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.2 */
		{
			cf.ShowToast("Please enter Question No.2 Comments.", 0);
			cf.setFocus(((EditText)findViewById(R.id.obs2comment)));
		}
		else
		{
			chkobs3_validation();
		}
	}
	private void chkobs3_validation() {
		// TODO Auto-generated method stub
		if(obs3rdgtxt.equals(""))
		{
			cf.ShowToast("Please select the option for Question No.3.", 0);
		}
		else
		{
			if(obs3rdgtxt.equals("Yes"))
			{
				 obs3chkloc_val= cf.getselected_chk(obs3chkloc);
		    	 String chk_Value3locother=(obs3chkloc[obs3chkloc.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.obs3loc_etother)).getText().toString():""; // append the other text value in to the selected option
		    	 obs3chkloc_val+=chk_Value3locother;
		    	 if(obs3chkloc_val.equals(""))
				 {
					 cf.ShowToast("Please select Locations for Question No.3." , 0);
				 }
		    	 else
		    	 {
		    		 if(obs3chkloc[obs3chkloc.length-1].isChecked())
					 {
						 if(chk_Value3locother.trim().equals("&#40;"))
						 {
							 cf.ShowToast("Please enter the Other text for Locations under Question No.3.", 0);
							 cf.setFocus(((EditText)findViewById(R.id.obs3loc_etother)));
						 }
						 else
						 {
							obs3_comments();	
						 }
					 }
					 else
					 {
						 obs3_comments();
					 }
		    	 }
			}
			else
			{
				obs3_comments();
			}
		}
	}
	private void obs3_comments() {
		// TODO Auto-generated method stub
		if(((EditText)findViewById(R.id.obs3comment)).getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.3 */
		{
			cf.ShowToast("Please enter Question No.3 Comments.", 0);
			cf.setFocus(((EditText)findViewById(R.id.obs3comment)));
		}
		else
		{
			chk_obs4validation();
		}
	}
	private void chk_obs4validation() {
		// TODO Auto-generated method stub
		if(obs4rdgtxt.equals(""))
		{
			cf.ShowToast("Please select the option for Question No.4", 0);
		}
		else
		{
			if(obs4rdgtxt.equals("Yes"))
			{
				 obs4chkloc_val= cf.getselected_chk(obs4chkloc);
		    	 String chk_Value4locother=(obs4chkloc[obs4chkloc.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.obs4loc_etother)).getText().toString():""; // append the other text value in to the selected option
		    	 obs4chkloc_val+=chk_Value4locother;
		    	 if(obs4chkloc_val.equals(""))
				 {
					 cf.ShowToast("Please select Locations for Question No.4." , 0);
				 }
		    	 else
		    	 {
		    		 if(obs4chkloc[obs4chkloc.length-1].isChecked())
					 {
						 if(chk_Value4locother.trim().equals("&#40;"))
						 {
							 cf.ShowToast("Please enter the Other text for Locations under Question No.4.", 0);
							 cf.setFocus(((EditText)findViewById(R.id.obs4loc_etother)));
						 }
						 else
						 {
							obs4_comments();	
						 }
					 }
					 else
					 {
						 obs4_comments();
					 }
		    	 }
			}
			else
			{
				obs4_comments();
			}
		}
	}
	private void obs4_comments() {
		// TODO Auto-generated method stub
		if(((EditText)findViewById(R.id.obs4comment)).getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.4 */
		{
			cf.ShowToast("Please enter Question No.4 Comments.", 0);
			cf.setFocus(((EditText)findViewById(R.id.obs4comment)));
		}
		else
		{
			chk_obs5Validation();
		}
	}
	private void chk_obs5Validation() {
		// TODO Auto-generated method stub
		if(obs5rdgtxt.equals(""))
		{
			cf.ShowToast("Please select the option for Question No.5.", 0);
		}
		else
		{
			if(obs5rdgtxt.equals("Yes"))
			{
				 obs5chkloc_val= cf.getselected_chk(obs5chkloc);
		    	 String chk_Value5locother=(obs5chkloc[obs5chkloc.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.obs5loc_etother)).getText().toString():""; // append the other text value in to the selected option
		    	 obs5chkloc_val+=chk_Value5locother;
		    	 if(obs5chkloc_val.equals(""))
				 {
					 cf.ShowToast("Please select Locations for Question No.5." , 0);
				 }
		    	 else
		    	 {
		    		 if(obs5chkloc[obs5chkloc.length-1].isChecked())
					 {
						 if(chk_Value5locother.trim().equals("&#40;"))
						 {
							 cf.ShowToast("Please enter the Other text for Locations under Question No.5.", 0);
							 cf.setFocus(((EditText)findViewById(R.id.obs5loc_etother)));
						 }
						 else
						 {
							obs5_comments();	
						 }
					 }
					 else
					 {
						 obs5_comments();
					 }
		    	 }
			}
			else
			{
				obs5_comments();
			}
		}
	}
	private void obs5_comments() {
		// TODO Auto-generated method stub
		if(((EditText)findViewById(R.id.obs5comment)).getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.5 */
		{
			cf.ShowToast("Please enter Question No.5 Comments.", 0);
			cf.setFocus(((EditText)findViewById(R.id.obs5comment)));
		}
		else
		{
			chk_obs6Validation();
		}
	}
	private void chk_obs6Validation() {
		// TODO Auto-generated method stub
		if(obs6rdgtxt.equals(""))
		{
			cf.ShowToast("Please select the option for Question No.6.", 0);
		}
		else
		{
			if(obs6rdgtxt.equals("Yes"))
			{
				 obs6chkloc_val= cf.getselected_chk(obs6chkloc);
		    	 String chk_Value5locother=(obs6chkloc[obs6chkloc.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.obs6loc_etother)).getText().toString():""; // append the other text value in to the selected option
		    	 obs6chkloc_val+=chk_Value5locother;
		    	 if(obs6chkloc_val.equals(""))
				 {
					 cf.ShowToast("Please select Locations for Question No.6." , 0);
				 }
		    	 else
		    	 {
		    		 if(obs6chkloc[obs6chkloc.length-1].isChecked())
					 {
						 if(chk_Value5locother.trim().equals("&#40;"))
						 {
							 cf.ShowToast("Please enter the Other text for Locations under Question No.6.", 0);
							 cf.setFocus(((EditText)findViewById(R.id.obs6loc_etother)));
						 }
						 else
						 {
							 obs6_comments();	
						 }
					 }
					 else
					 {
						 obs6_comments();
					 }
		    	 }
			}
			else
			{
				obs6_comments();
			}
		}
	}
	private void obs6_comments() {
		// TODO Auto-generated method stub
		if(((EditText)findViewById(R.id.obs6comment)).getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.6 */
		{
			cf.ShowToast("Please enter Question No.6 Comments.", 0);
			cf.setFocus(((EditText)findViewById(R.id.obs6comment)));
		}
		else
		{
			chk_obs6aValidation();
		}
	}
	private void chk_obs6aValidation() {
		// TODO Auto-generated method stub
		if(obs6ardgtxt.equals(""))
		{
			cf.ShowToast("Please select the option for Question No.6(a).", 0);
		}
		else
		{
			if(((EditText)findViewById(R.id.obs6acomment)).getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.6(a) */
			{
				cf.ShowToast("Please enter Question No.6(a) Comments.", 0);
				cf.setFocus(((EditText)findViewById(R.id.obs6acomment)));
			}
			else
			{
				if(obs6ardgtxt.equals("Yes"))
				{
					TreeCheck();
				}
				else
				{
				  chk_obs6bValidation();
				}
			}
			
		}
	}
	private void chk_obs6bValidation() {
		// TODO Auto-generated method stub
		if(obs6brdgtxt.equals(""))
		{
			cf.ShowToast("Please select the option for Question No.6 (b).", 0);
		}
		else
		{
			if(obs6brdgtxt.equals("Yes"))
			{
				 obs6bchkloc_val= cf.getselected_chk(obs6bchkloc);
		    	 String chk_Value5locother=(obs6bchkloc[obs6bchkloc.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.obs6bloc_etother)).getText().toString():""; // append the other text value in to the selected option
		    	 obs6bchkloc_val+=chk_Value5locother;
		    	 if(obs6bchkloc_val.equals(""))
				 {
					 cf.ShowToast("Please select Locations for Question No.6(b)." , 0);
				 }
		    	 else
		    	 {
		    		 if(obs6bchkloc[obs6bchkloc.length-1].isChecked())
					 {
						 if(chk_Value5locother.trim().equals("&#40;"))
						 {
							 cf.ShowToast("Please enter the Other text for Locations under Question No.6 (b).", 0);
							 cf.setFocus(((EditText)findViewById(R.id.obs6bloc_etother)));
						 }
						 else
						 {
							obs6b_comments();	
						 }
					 }
					 else
					 {
						 obs6b_comments();
					 }
		    	 }
			}
			else
			{
				obs6b_comments();
			}
		}
	}
	private void obs6b_comments() {
		// TODO Auto-generated method stub
		if(((EditText)findViewById(R.id.obs6bcomment)).getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.6(b) */
		{
			cf.ShowToast("Please enter Question No.6(b) Comments.", 0);
			cf.setFocus(((EditText)findViewById(R.id.obs6bcomment)));
		}
		else
		{
			chk_obs6cValidation();
		}
	}
	private void chk_obs6cValidation() {
		// TODO Auto-generated method stub
		if(obs6crdgtxt.equals(""))
		{
			cf.ShowToast("Please select the option for Question No.6 (c).", 0);
		}
		else
		{
			if(obs6crdgtxt.equals("Yes"))
			{
				 obs6cchkloc_val= cf.getselected_chk(obs6cchkloc);
		    	 String chk_Value5locother=(obs6cchkloc[obs6cchkloc.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.obs6cloc_etother)).getText().toString():""; // append the other text value in to the selected option
		    	 obs6cchkloc_val+=chk_Value5locother;
		    	 if(obs6cchkloc_val.equals(""))
				 {
					 cf.ShowToast("Please select Locations for Question No.6(c)." , 0);
				 }
		    	 else
		    	 {
		    		 if(obs6cchkloc[obs6cchkloc.length-1].isChecked())
					 {
						 if(chk_Value5locother.trim().equals("&#40;"))
						 {
							 cf.ShowToast("Please enter the Other text for Locations under Question No.6(c).", 0);
							 cf.setFocus(((EditText)findViewById(R.id.obs6cloc_etother)));
						 }
						 else
						 {
							obs6c_comments();	
						 }
					 }
					 else
					 {
						 obs6c_comments();
					 }
		    	 }
			}
			else
			{
				obs6c_comments();
			}
		}
	}
	private void obs6c_comments() {
		// TODO Auto-generated method stub
		if(((EditText)findViewById(R.id.obs6ccomment)).getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.6(c) */
		{
			cf.ShowToast("Please enter Question No.6(c) Comments.", 0);
			cf.setFocus(((EditText)findViewById(R.id.obs6ccomment)));
		}
		else
		{
			chk_obs7Validation();
		}
	}
	private void chk_obs7Validation() {
		// TODO Auto-generated method stub
		if(obs7rdgtxt.equals(""))
		{
			cf.ShowToast("Please select the option for Question No.7.", 0);
		}
		else
		{
			if(obs7rdgtxt.equals("Yes"))
			{
				 obs7chkcrk_val= cf.getselected_chk(obs7chkcrck);
		    	 String chk_Value10crkother=(obs7chkcrck[obs7chkcrck.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.obS7crk_etother)).getText().toString():""; // append the other text value in to the selected option
		    	 obs7chkcrk_val+=chk_Value10crkother;
		    	 if(obs7chkcrk_val.equals(""))
				 {
					 cf.ShowToast("Please select Type of cracks noted for Question No.7." , 0);
				 }
		    	 else
		    	 {
		    		 if(obs7chkcrck[obs7chkcrck.length-1].isChecked())
					 {
						 if(chk_Value10crkother.trim().equals("&#40;"))
						 {
							 cf.ShowToast("Please enter the Other text for Type of cracks noted for Question No.7.", 0);
							 cf.setFocus(((EditText)findViewById(R.id.obS7crk_etother)));
						 }
						 else
						 {
							 chk_location7();	
						 }
					 }
					 else
					 {
						 chk_location7();
					 }
		    	 }
			}
			else
			{
				chk_obs7comments();
			}
		}
	}
	private void chk_obs7comments() {
		// TODO Auto-generated method stub
		if(((EditText)findViewById(R.id.obs7comment)).getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.7 */
		{
			cf.ShowToast("Please enter Question No.7 Comments.", 0);
			cf.setFocus(((EditText)findViewById(R.id.obs7comment)));
		}
		else
		{
			chk_obs7aValidation();
		}
	}
	private void chk_obs7aValidation() {
		// TODO Auto-generated method stub
		if(obs7ardgtxt.equals(""))
		{
			cf.ShowToast("Please select the option for Question No.7(a).", 0);
		}
		else
		{
			if(obs7ardgtxt.equals("Yes"))
			{
				 obs7achkloc_val= cf.getselected_chk(obs7achkloc);
		    	 if(obs7achkloc_val.equals(""))
				 {
					 cf.ShowToast("Please select Locations for Question No.7(a)." , 0);
				 }
		    	 else
		    	 {
		    		  obs7a_comments();
				 }
			}
			else
			{
				obs7a_comments();
			}
		}
	}
	private void obs7a_comments() {
		// TODO Auto-generated method stub
		if(((EditText)findViewById(R.id.obs7acomment)).getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.7(a) */
		{
			cf.ShowToast("Please enter Question No.7(a) Comments.", 0);
			cf.setFocus(((EditText)findViewById(R.id.obs7acomment)));
		}
		else
		{
			//TreeCheck();
			if(obs6ardgtxt.equals("Yes"))
			{
				obs6achkloc_val= cf.getselected_chk(obs6achkloc);
				String chk_Value6Aother1 =(obs6achkloc[obs6achkloc.length-1].isChecked())? ((EditText)findViewById(R.id.obs6aloc_etother)).getText().toString():""; // append the other text value in to the selected option
				if(obs6achkloc_val.equals(""))
				{
					obs6achkloc_val+=chk_Value6Aother1;
				}
				else if(!chk_Value6Aother1.equals(""))
				{
					 obs6achkloc_val+=","+chk_Value6Aother1;
				}
				if(!((EditText)findViewById(R.id.obs6a_etwidth)).getText().toString().equals("")||!((EditText)findViewById(R.id.obs6a_etheight)).getText().toString().equals("")||
						!obs6achkloc_val.equals(""))
				{
					if(((EditText)findViewById(R.id.obs6a_etwidth)).getText().toString().equals(""))
				    {
						cf.ShowToast("Please enter the Approximate width of tree for Question No.6(a).", 0);
						cf.setFocus(((EditText)findViewById(R.id.obs6a_etwidth)));
					}
					else
					{
						String dstext=((EditText)findViewById(R.id.obs6a_etwidth)).getText().toString();
						int fv = Character.getNumericValue(dstext.charAt(0));
						if(fv==0)
						{
							cf.ShowToast("Approximate width of tree - Invalid entry.", 0);
							cf.setFocus(((EditText)findViewById(R.id.obs6a_etwidth)));
						}
						else
						{
							if(((EditText)findViewById(R.id.obs6a_etheight)).getText().toString().equals(""))
							{
								cf.ShowToast("Please enter the Approximate height of tree for Question No.6(a).", 0);
								cf.setFocus(((EditText)findViewById(R.id.obs6a_etheight)));
							}
							else
							{
								String hgtext=((EditText)findViewById(R.id.obs6a_etheight)).getText().toString();
								int hgtfv = Character.getNumericValue(hgtext.charAt(0));
								if(hgtfv==0)
								{
									cf.ShowToast("Approximate height of tree - Invalid entry.", 0);
									cf.setFocus(((EditText)findViewById(R.id.obs6a_etheight)));
								}
								else
								{
									obs6achkloc_val= cf.getselected_chk(obs6achkloc);
									String chk_Valueobs6Aother =(obs6achkloc[obs6achkloc.length-1].isChecked())? ((EditText)findViewById(R.id.obs6aloc_etother)).getText().toString():""; // append the other text value in to the selected option
									if(obs6achkloc_val.equals(""))
									{
									  obs6achkloc_val+=chk_Valueobs6Aother;
									}
									else if(!chk_Valueobs6Aother.equals(""))
									{
									  obs6achkloc_val+=","+chk_Valueobs6Aother;
									}
									if(obs6achkloc_val.equals(""))
									{
										 if(obs6achkloc[obs6achkloc.length-1].isChecked()){
											 cf.ShowToast("Please enter the Other text for Question No.6(a) Locations." , 0);
											 cf.setFocus(((EditText)findViewById(R.id.obs6aloc_etother)));
										 }
										 else{
										 cf.ShowToast("Please select the option for Question No.6(a) Locations." , 0);}
									 }
									 else 
									 {
										 if(obs6achkloc[obs6achkloc.length-1].isChecked())
										 {
											 if(chk_Valueobs6Aother.trim().equals(""))
											 {
												 cf.ShowToast("Please enter the Other text for Question No.6(a) Locations.", 0);
												 cf.setFocus(((EditText)findViewById(R.id.obs6aloc_etother)));
											 }
											 else
											 {
												 treeinsert();
											 }
										 }
										 else
										 {
											 treeinsert();
										 }
									 }
								}
							}
							
						}
					}
				}
				else
				{
					Obs1_Insert();
				}
			}
			else
			{
				Obs1_Insert();
			}
			
		}
	}
	private void TreeCheck() {
		// TODO Auto-generated method stub
		Cursor OBSTree_save = null;
		try
		{
			OBSTree_save=cf.SelectTablefunction(cf.SINK_Obstreetbl, " where fld_srid='"+cf.selectedhomeid+"'");
			if(OBSTree_save.getCount()==0)
			{
				cf.ShowToast("Please save the required fields for Large trees." , 0);
			}
			else
			{
				chk_obs6bValidation();
			}
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		/*if(obs6ardgtxt.equals("Yes"))
		{
		    obs6achkloc_val= cf.getselected_chk(obs6achkloc);
			String chk_Value6Aother1 =(obs6achkloc[obs6achkloc.length-1].isChecked())? ((EditText)findViewById(R.id.obs6aloc_etother)).getText().toString():""; // append the other text value in to the selected option
			if(obs6achkloc_val.equals(""))
			 {
				obs6achkloc_val+=chk_Value6Aother1;
			 }
			 else if(!chk_Value6Aother1.equals(""))
			 {
				 obs6achkloc_val+=","+chk_Value6Aother1;
			 }
		   if(!((EditText)findViewById(R.id.obs6a_etwidth)).getText().toString().equals("")||!((EditText)findViewById(R.id.obs6a_etheight)).getText().toString().equals("")||
					!obs6achkloc_val.equals(""))
			{
				 if(((EditText)findViewById(R.id.obs6a_etwidth)).getText().toString().equals(""))
			       {
						cf.ShowToast("Please enter the Approximate width of the tree for Question No.6(a)", 0);
						cf.setFocus(((EditText)findViewById(R.id.obs6a_etwidth)));
					}
					else
					{
						String dstext=((EditText)findViewById(R.id.obs6a_etwidth)).getText().toString();
						int fv = Character.getNumericValue(dstext.charAt(0));
						if(fv==0)
						{
							cf.ShowToast("Approximate width of the tree - Invalid entry.", 0);
							cf.setFocus(((EditText)findViewById(R.id.obs6a_etwidth)));
						}
						else
						{
							if(((EditText)findViewById(R.id.obs6a_etheight)).getText().toString().equals(""))
							{
								cf.ShowToast("Please enter the Approximate height of the tree for Question No.6(a)", 0);
								cf.setFocus(((EditText)findViewById(R.id.obs6a_etheight)));
							}
							else
							{
								String hgtext=((EditText)findViewById(R.id.obs6a_etheight)).getText().toString();
								int hgtfv = Character.getNumericValue(hgtext.charAt(0));
								if(hgtfv==0)
								{
									cf.ShowToast("Approximate height of the tree - Invalid entry.", 0);
									cf.setFocus(((EditText)findViewById(R.id.obs6a_etheight)));
								}
								else
								{
									 obs6achkloc_val= cf.getselected_chk(obs6achkloc);
									 String chk_Valueobs6Aother =(obs6achkloc[obs6achkloc.length-1].isChecked())? ((EditText)findViewById(R.id.obs6aloc_etother)).getText().toString():""; // append the other text value in to the selected option
									 if(obs6achkloc_val.equals(""))
									 {
										 obs6achkloc_val+=chk_Valueobs6Aother;
									 }
									 else if(!chk_Valueobs6Aother.equals(""))
									 {
										 obs6achkloc_val+=","+chk_Valueobs6Aother;
									 }
									 
									 if(obs6achkloc_val.equals(""))
									 {
										 if(obs6achkloc[obs6achkloc.length-1].isChecked()){
											 cf.ShowToast("Please enter the other text for Question No.6(a) Locations." , 0);
											 cf.setFocus(((EditText)findViewById(R.id.obs6aloc_etother)));
										 }
										 else{
										 cf.ShowToast("Please select the option for Question No.6(a) Locations." , 0);}
									 }
									 else 
									 {
										 if(obs6achkloc[obs6achkloc.length-1].isChecked())
										 {
											 if(chk_Valueobs6Aother.trim().equals(""))
											 {
												 cf.ShowToast("Please enter the Other text for Question No.6(a) Locations.", 0);
												 cf.setFocus(((EditText)findViewById(R.id.obs6aloc_etother)));
											 }
											 else
											 {
												 treeinsert();
											 }
										 }
										 else
										 {
											 treeinsert();
										 }
									 }
								}
						}
						
						}
	
	             }*/
			/*}
			else
			{
				 
				 if(OBSTree_save.getCount()!=0)
				 {
					 Obs1_Insert();
				 }
				 else
				 {
					 cf.ShowToast("Please enter the required fields for Large tree." , 0);
				 }
				
			}
			
		}*/
		/*else
		{
			Obs1_Insert();
		}*/
	}
	private void treeinsert() {
		// TODO Auto-generated method stub
		cf.arr_db.execSQL("INSERT INTO "+cf.SINK_Obstreetbl+ "(fld_srid,Width,Height,Location)" +
				"VALUES ('"+cf.selectedhomeid+"','"+cf.encode(((EditText)findViewById(R.id.obs6a_etwidth)).getText().toString())+"',"+
	 			"'"+cf.encode(((EditText)findViewById(R.id.obs6a_etheight)).getText().toString())+"','"+cf.encode(obs6achkloc_val)+"')");
        cf.ShowToast("Tree has been saved successfully.",0);
		dbquery();
        ((EditText)findViewById(R.id.obs6a_etwidth)).setText("");
        ((EditText)findViewById(R.id.obs6a_etheight)).setText("");
		cf.Set_UncheckBox(obs6achkloc,((EditText)findViewById(R.id.obs6aloc_etother)));
		chk_obs6bValidation();
	}
	private void Obs1_Insert() {
		// TODO Auto-generated method stub
		Cursor OBS1_save=null;
		try
		{
			OBS1_save=cf.SelectTablefunction(cf.SINK_Obs1tbl, " where fld_srid='"+cf.selectedhomeid+"'");
			if(OBS1_save.getCount()>0)
			{
				try
				{
					cf.arr_db.execSQL("UPDATE "+cf.SINK_Obs1tbl+ " set IrregularLandSurface='"+obs1rdgtxt+"',IrregularLandSurfaceLocation='"+cf.encode(obs1chkloc_val)+"',"+
				                  "IrregularLandSurfaceComments='"+cf.encode(((EditText)findViewById(R.id.obs1comment)).getText().toString())+"',"+
							      "VisibleBuriedDebris='"+obs1ardgtxt+"',VisibleBuriedDebrisLocation='"+cf.encode(obs1achkloc_val)+"',"+
				                  "VisibleBuriedDebrisComments='"+cf.encode(((EditText)findViewById(R.id.obs1acomment)).getText().toString())+"',"+
							      "IrregularLandSurfaceAdjacent='"+obs2rdgtxt+"',IrregularLandSurfaceAdjacentLocation='"+cf.encode(obs2chkloc_val)+"',"+
				                  "IrregularLandSurfaceAdjacentComments='"+cf.encode(((EditText)findViewById(R.id.obs2comment)).getText().toString())+"',"+
							      "SoilCollapse='"+obs3rdgtxt+"',SoilCollapseLocation='"+cf.encode(obs3chkloc_val)+"',"+
				                  "SoilCollapseComments='"+cf.encode(((EditText)findViewById(R.id.obs3comment)).getText().toString())+"',"+
							      "SoilErosionAroundFoundation='"+obs4rdgtxt+"',SoilErosionAroundFoundationOption='"+cf.encode(obs4chkloc_val)+"',"+
				                  "SoilErosionAroundFoundationComments='"+cf.encode(((EditText)findViewById(R.id.obs4comment)).getText().toString())+"',"+
				                  "DrivewaysCracksNoted='"+obs5rdgtxt+"',DrivewaysCracksOption='"+cf.encode(obs5chkloc_val)+"',"+
				                  "DrivewaysCracksComments='"+cf.encode(((EditText)findViewById(R.id.obs5comment)).getText().toString())+"',"+
				                  "UpliftDrivewaysSurface='"+obs6rdgtxt+"',UpliftDrivewaysSurfaceOption='"+cf.encode(obs6chkloc_val)+"',"+
				                  "UpliftDrivewaysSurfaceComments='"+cf.encode(((EditText)findViewById(R.id.obs6comment)).getText().toString())+"',"+
				                  "LargeTree='"+obs6ardgtxt+"',LargeTreeComments='"+cf.encode(((EditText)findViewById(R.id.obs6acomment)).getText().toString())+"',"+
				                  "CypressTree='"+obs6brdgtxt+"',CypressTreeLocation='"+cf.encode(obs6bchkloc_val)+"',"+
				                  "CypressTreeComments='"+cf.encode(((EditText)findViewById(R.id.obs6bcomment)).getText().toString())+"',"+
				                  "LargeTreesRemoved='"+obs6crdgtxt+"',LargeTreesRemovedLocation='"+cf.encode(obs6cchkloc_val)+"',"+
				                  "LargeTreesRemovedComments='"+cf.encode(((EditText)findViewById(R.id.obs6ccomment)).getText().toString())+"',"+
				                  "FoundationCrackNoted='"+obs7rdgtxt+"',FoundationTypeofCrack='"+cf.encode(obs7chkcrk_val)+"',"+
				                  "FoundationLocation='"+cf.encode(obs7chkloc_val)+"',FoundationWidthofCrack='"+cf.encode(obs7chkwidt_val)+"',"+
				                  "FoundationLengthofCrack='"+cf.encode(((EditText)findViewById(R.id.obS7_etlen)).getText().toString())+"',"+
				                  "FoundationCleanliness='"+cf.encode(obs7chkcond_val)+"',FoundationProbableCause='"+cf.encode(obs7chkcaus_val)+"',"+
				                  "FoundationComments='"+cf.encode(((EditText)findViewById(R.id.obs7comment)).getText().toString())+"',"+
				                  "RetainingWallsServiceable='"+obs7ardgtxt+"',RetainingWallsServiceOption='"+cf.encode(obs7achkloc_val)+"',"+
				                  "RetainingWallsServiceComments='"+cf.encode(((EditText)findViewById(R.id.obs7acomment)).getText().toString())+"' where fld_srid='"+cf.selectedhomeid+"'");
					cf.ShowToast("Observations(1-7) updated successfully.", 1);
				    ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
				     cf.goclass(53);
				}
				catch (Exception E)
				{
					String strerrorlog="Updating the Observation1 - Sinkhole";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
			}
			else
			{
				try
				{
					cf.arr_db.execSQL("INSERT INTO "
							+ cf.SINK_Obs1tbl
							+ " (fld_srid,IrregularLandSurface,IrregularLandSurfaceLocation,IrregularLandSurfaceComments,VisibleBuriedDebris,VisibleBuriedDebrisLocation,"+
				                  "VisibleBuriedDebrisComments,IrregularLandSurfaceAdjacent,IrregularLandSurfaceAdjacentLocation,IrregularLandSurfaceAdjacentComments,"+
							      "SoilCollapse,SoilCollapseLocation,SoilCollapseComments,SoilErosionAroundFoundation,SoilErosionAroundFoundationOption,"+
				                  "SoilErosionAroundFoundationComments,DrivewaysCracksNoted,DrivewaysCracksOption,DrivewaysCracksComments,UpliftDrivewaysSurface,"+
							      "UpliftDrivewaysSurfaceOption,UpliftDrivewaysSurfaceComments,LargeTree,LargeTreeComments,CypressTree,CypressTreeLocation,"+
				                  "CypressTreeComments,LargeTreesRemoved,LargeTreesRemovedLocation,LargeTreesRemovedComments,"+
				                  "FoundationCrackNoted,FoundationTypeofCrack,FoundationLocation,FoundationWidthofCrack,FoundationLengthofCrack,"+
				                  "FoundationCleanliness,FoundationProbableCause,FoundationComments,RetainingWallsServiceable,RetainingWallsServiceOption,"+
				                  "RetainingWallsServiceComments)"
							+ "VALUES ('"+cf.selectedhomeid+"','"+obs1rdgtxt+"','"+cf.encode(obs1chkloc_val)+"','"+cf.encode(((EditText)findViewById(R.id.obs1comment)).getText().toString())+"',"+
							      "'"+obs1ardgtxt+"','"+cf.encode(obs1achkloc_val)+"','"+cf.encode(((EditText)findViewById(R.id.obs1acomment)).getText().toString())+"',"+
							      "'"+obs2rdgtxt+"','"+cf.encode(obs2chkloc_val)+"','"+cf.encode(((EditText)findViewById(R.id.obs2comment)).getText().toString())+"',"+
							      "'"+obs3rdgtxt+"','"+cf.encode(obs3chkloc_val)+"','"+cf.encode(((EditText)findViewById(R.id.obs3comment)).getText().toString())+"',"+
							      "'"+obs4rdgtxt+"','"+cf.encode(obs4chkloc_val)+"','"+cf.encode(((EditText)findViewById(R.id.obs4comment)).getText().toString())+"',"+
				                  "'"+obs5rdgtxt+"','"+cf.encode(obs5chkloc_val)+"','"+cf.encode(((EditText)findViewById(R.id.obs5comment)).getText().toString())+"',"+
				                  "'"+obs6rdgtxt+"','"+cf.encode(obs6chkloc_val)+"','"+cf.encode(((EditText)findViewById(R.id.obs6comment)).getText().toString())+"',"+
				                  "'"+obs6ardgtxt+"','"+cf.encode(((EditText)findViewById(R.id.obs6acomment)).getText().toString())+"','"+obs6brdgtxt+"',"+
				                  "'"+cf.encode(obs6bchkloc_val)+"','"+cf.encode(((EditText)findViewById(R.id.obs6bcomment)).getText().toString())+"',"+
				                  "'"+obs6crdgtxt+"','"+cf.encode(obs6cchkloc_val)+"','"+cf.encode(((EditText)findViewById(R.id.obs6ccomment)).getText().toString())+"',"+
				                  "'"+obs7rdgtxt+"','"+cf.encode(obs7chkcrk_val)+"','"+cf.encode(obs7chkloc_val)+"','"+cf.encode(obs7chkwidt_val)+"',"+
				                  "'"+cf.encode(((EditText)findViewById(R.id.obS7_etlen)).getText().toString())+"','"+cf.encode(obs7chkcond_val)+"',"+
				                  "'"+cf.encode(obs7chkcaus_val)+"','"+cf.encode(((EditText)findViewById(R.id.obs7comment)).getText().toString())+"',"+
				                  "'"+obs7ardgtxt+"','"+cf.encode(obs7achkloc_val)+"','"+cf.encode(((EditText)findViewById(R.id.obs7acomment)).getText().toString())+"')");

					 cf.ShowToast("Observations(1-7) saved successfully.", 1);
					 ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE); 
					 cf.goclass(53);
			 	}
				catch (Exception E)
				{
					String strerrorlog="Inserting the Observation1 - Sinkhole";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
			}
		}
		catch (Exception E)
		{
			String strerrorlog="Checking the rows inserted in the Observation1 - Sinkhole table.";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+"table at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
	private void dbquery() {
		// TODO Auto-generated method stub
		data = null;
		inspdata = "";
		countarr = null;
		Cursor cur = cf.arr_db.rawQuery("select * from " + cf.SINK_Obstreetbl
				+ " where fld_srid='"+cf.selectedhomeid+"'", null);
		rws = cur.getCount();
		data = new String[rws];
		countarr = new String[rws];
		int j = 0;
		if(rws>0)
		{
			cur.moveToFirst();
			for(int i=0;i<rws;i++)
			{
				inspdata += " "+ cf.decode(cur.getString(cur.getColumnIndex("Width")))
						+ " | ";
				inspdata += cf.decode(cur.getString(cur
						.getColumnIndex("Height"))) + " | ";
				inspdata += cf.decode(cur.getString(cur
						.getColumnIndex("Location")))
						+ "&#126;";
				countarr[i] = cur.getString(cur
						.getColumnIndex("OB1TId"));
				cur.moveToNext();display();
			}
		}
		else
		{
			((RelativeLayout)findViewById(R.id.obs6ayeslin)).setVisibility(cf.v1.GONE);
			dynlist.removeAllViews();
		}
		
		
	}
	
	private void display() {
		// TODO Auto-generated method stub
		((RelativeLayout)findViewById(R.id.obs6ayeslin)).setVisibility(cf.v1.VISIBLE);
        dynlist.removeAllViews();
		final LinearLayout l1 = new LinearLayout(this);
		l1.setOrientation(LinearLayout.VERTICAL);
		dynlist.addView(l1);
			this.data = inspdata.split("&#126;");
			for (int i = 0; i < data.length; i++) {
				tvstatus = new TextView[rws];
				deletebtn = new Button[rws];
				edtbtn = new Button[rws];
				LinearLayout l2 = new LinearLayout(this);
				LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(
						 ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
				paramschk.topMargin = 5;
				paramschk.leftMargin = 20;
				paramschk.bottomMargin = 5;
				l2.setOrientation(LinearLayout.HORIZONTAL);
				l1.addView(l2,paramschk);

				tvstatus[i] = new TextView(this);
				tvstatus[i].setMinimumWidth(380);
				tvstatus[i].setMaxWidth(380);
				tvstatus[i].setTag("textbtn" + i);
				if(data[i].contains("&#94;") || data[i].contains("&#40;"))
				{
					data[i]=data[i].replace( "&#94;",",");
					//data[i]=data[i].replace( "&#40;",",");
					if(data[i].contains("&#40;"))
					{
						
						data[i]=data[i].replace( "&#40;",",Other(");
						data[i]+=")";
					}
					
					
				}
				tvstatus[i].setText(data[i]);
				tvstatus[i].setTextColor(Color.BLACK);
				tvstatus[i].setTextSize(16);
				l2.addView(tvstatus[i],paramschk);
			
				edtbtn[i] = new Button(this);
				edtbtn[i].setBackgroundResource(R.drawable.roofedit);
				edtbtn[i].setTag("edtbtn" + i);
				edtbtn[i].setPadding(30, 0, 0, 0);
				l2.addView(edtbtn[i],paramschk);
				
				deletebtn[i] = new Button(this);
				deletebtn[i].setBackgroundResource(R.drawable.roofdelete);
				deletebtn[i].setTag("deletebtn" + i);
				deletebtn[i].setPadding(30, 0, 0, 0);
				l2.addView(deletebtn[i],paramschk);
				
				deletebtn[i].setOnClickListener(new View.OnClickListener() {
					public void onClick(final View v) {
						String getidofselbtn = v.getTag().toString();
						final String repidofselbtn = getidofselbtn.replace(
								"deletebtn", "");
						final int cvrtstr = Integer.parseInt(repidofselbtn);
						final String dt = countarr[cvrtstr];
						 cf.alerttitle="Delete";
						  cf.alertcontent="Are you sure want to delete?";
						    final Dialog dialog1 = new Dialog(Observation1.this,android.R.style.Theme_Translucent_NoTitleBar);
							dialog1.getWindow().setContentView(R.layout.alertsync);
							TextView txttitle = (TextView) dialog1.findViewById(R.id.txthelp);
							txttitle.setText( cf.alerttitle);
							TextView txt = (TextView) dialog1.findViewById(R.id.txtid);
							txt.setText(Html.fromHtml( cf.alertcontent));
							Button btn_yes = (Button) dialog1.findViewById(R.id.yes);
							Button btn_cancel = (Button) dialog1.findViewById(R.id.cancel);
							btn_yes.setOnClickListener(new OnClickListener()
							{
			                	@Override
								public void onClick(View arg0) {

									// TODO Auto-generated method stub
									dialog1.dismiss();
									try {
										cf.arr_db.execSQL("DELETE FROM "
												+ cf.SINK_Obstreetbl
												+ " WHERE OB1TId ='" + cf.encode(dt)
												+ "'");
										cf.ShowToast("Deleted successfully.", 1);
										((Button)findViewById(R.id.save6a)).setText("Save");
										dbquery();cleartree();
										
									} catch (Exception e) {

									}
									
								}
								
							});
							btn_cancel.setOnClickListener(new OnClickListener()
							{

								@Override
								public void onClick(View arg0) {
									// TODO Auto-generated method stub
									dialog1.dismiss();
									
								}
								
							});
							dialog1.setCancelable(false);
							dialog1.show();
						      	}
								});
				edtbtn[i].setOnClickListener(new View.OnClickListener() {
					public void onClick(final View v) {
						String getidofselbtn = v.getTag().toString();
						final String repidofselbtn = getidofselbtn.replace(
								"edtbtn", "");
						final int cvrtstr = Integer.parseInt(repidofselbtn);
						
						 edt = countarr[cvrtstr];
						((Button)findViewById(R.id.save6a)).setText("Update");
						update(edt);
						 
		      	}
				});
			}
					

	}
	protected void update(String dt) {
		// TODO Auto-generated method stub
		String locval="";
		try{
			Cursor treret=cf.SelectTablefunction(cf.SINK_Obstreetbl, " Where OB1TId='"+dt+"'");
			if(treret.getCount()>0)
		    {
				treret.moveToFirst();
				((EditText)findViewById(R.id.obs6a_etwidth)).setText(cf.decode(treret.getString(treret.getColumnIndex("Width"))));
				((EditText)findViewById(R.id.obs6a_etheight)).setText(cf.decode(treret.getString(treret.getColumnIndex("Height"))));
				locval=cf.decode(treret.getString(treret.getColumnIndex("Location")));
				cf.Set_UncheckBox(obs6achkloc, ((EditText)findViewById(R.id.obs6aloc_etother)));
				cf.setvaluechk1(locval,obs6achkloc,((EditText)findViewById(R.id.obs6aloc_etother)));
				
		    }
		}catch (Exception e) {
			// TODO: handle exception
		}
	}
	private void chk_location7() {
		// TODO Auto-generated method stub
		 obs7chkloc_val= cf.getselected_chk(obs7chkloc);
	   	 String chk_Value10locother=(obs7chkloc[obs7chkloc.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.obS7loc_etother)).getText().toString():""; // append the other text value in to the selected option
	   	 obs7chkloc_val+=chk_Value10locother;
	   	 if(obs7chkloc_val.equals(""))
			 {
				 cf.ShowToast("Please select Location of cracks for Question No.7." , 0);
			 }
	   	 else
	   	 {
	   		 if(obs7chkloc[obs7chkloc.length-1].isChecked())
				 {
					 if(chk_Value10locother.trim().equals("&#40;"))
					 {
						 cf.ShowToast("Please enter the Other text for Location of cracks for Question No.7.", 0);
						 cf.setFocus(((EditText)findViewById(R.id.obS7loc_etother)));
					 }
					 else
					 {
						chk_width7();	
					 }
				 }
				 else
				 {
					 chk_width7();
				 }
	   	 }
	}
	private void chk_width7() {
		// TODO Auto-generated method stub
		 obs7chkwidt_val= cf.getselected_chk(obs7chkwidt);
	   	 String chk_Value10widother=(obs7chkwidt[obs7chkwidt.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.obS7wid_etother)).getText().toString():""; // append the other text value in to the selected option
	   	obs7chkwidt_val+=chk_Value10widother;
	   	 if(obs7chkwidt_val.equals(""))
			 {
				 cf.ShowToast("Please select Approximate width of cracks for Question No.7." , 0);
			 }
	   	 else
	   	 {
	   		 if(obs7chkwidt[obs7chkwidt.length-1].isChecked())
				 {
					 if(chk_Value10widother.trim().equals("&#40;"))
					 {
						 cf.ShowToast("Please enter the Other text for Approximate width of cracks for Question No.7.", 0);
						 cf.setFocus(((EditText)findViewById(R.id.obS7wid_etother)));
					 }
					 else
					 {
						chk_len7();	
					 }
				 }
				 else
				 {
					 chk_len7();
				 }
	   	 }
	}
	private void chk_len7() {

		// TODO Auto-generated method stub
		if(((EditText)findViewById(R.id.obS7_etlen)).getText().toString().trim().equals("")) /*CHECK LENGTH QUESTION NO.7 */
		{
			cf.ShowToast("Please enter the Approximate Length of cracks for Question No.7.", 0);
			cf.setFocus(((EditText)findViewById(R.id.obS7_etlen)));
		}
		else
		{
			obs7chkcond_val= cf.getselected_chk(obs7chkcond);
			 if(obs7chkcond_val.equals(""))
			 {
				 cf.ShowToast("Please select Condition of cracks noted for Question No.7." , 0);
			 }
	   	    else
	   	     {
	   	    	chk_possiblecause7();
	   	     }
		}
	}
	private void chk_possiblecause7() {
		// TODO Auto-generated method stub
		obs7chkcaus_val= cf.getselected_chk(obs7chkcaus);
	   	 String chk_Value7causother=(obs7chkcaus[obs7chkcaus.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.obS7caus7_etother)).getText().toString():""; // append the other text value in to the selected option
	   	obs7chkcaus_val+=chk_Value7causother;
	   	 if(obs7chkcaus_val.equals(""))
			 {
				 cf.ShowToast("Please select Possible cause of settlement based on limited survey for Question No.7." , 0);
			 }
	   	 else
	   	 {
	   		 if(obs7chkcaus[obs7chkcaus.length-1].isChecked())
				 {
					 if(chk_Value7causother.trim().equals("&#40;"))
					 {
						 cf.ShowToast("Please enter the Other text for Possible cause of settlement based on limited survey for Question No.7.", 0);
						 cf.setFocus(((EditText)findViewById(R.id.obS7caus7_etother)));
					 }
					 else
					 {
						chk_obs7comments();	
					 }
				 }
				 else
				 {
					 chk_obs7comments();
				 }
	   	 }
	}
	private void hidelayouts() {
		// TODO Auto-generated method stub
		((LinearLayout)findViewById(R.id.obs1lin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.obs2lin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.obs3lin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.obs4lin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.obs5lin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.obs6lin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.obs7lin)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdobs1)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdobs2)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdobs3)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdobs4)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdobs5)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdobs6)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdobs7)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.shwobs1)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwobs2)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwobs3)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwobs4)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwobs5)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwobs6)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwobs7)).setVisibility(cf.v1.VISIBLE);
	}
	 public boolean onKeyDown(int keyCode, KeyEvent event) {
	 		// replaces the default 'Back' button action
	 		if (keyCode == KeyEvent.KEYCODE_BACK) {
	 			cf.goback(5);
				return true;
	 		}
	 		
	 		return super.onKeyDown(keyCode, event);
	 	}
}
