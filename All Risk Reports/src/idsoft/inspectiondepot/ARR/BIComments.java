package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.PolicyHolder.textwatcher;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class BIComments extends Activity {
	CommonFunctions cf;
	boolean load_comment=true;
	public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	       
	        cf = new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);				
		 	}
	        setContentView(R.layout.bicomments);
	        cf.getDeviceDimensions();
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
         	hdr_layout.addView(new HdrOnclickListener(this,1,"General","Building Info","Overall Building Comments",1,1,cf));
         	LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
         	main_layout.setMinimumWidth(cf.wd);
         	main_layout.addView(new MyOnclickListener(getApplicationContext(), 1, 0,0,cf));
            LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
            submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 16, 1,0,cf));
	        LinearLayout submenu2_layout = (LinearLayout) findViewById(R.id.submenu1); 
	        submenu2_layout.addView(new MyOnclickListener(getApplicationContext(), 461, 1,0,cf));
	        /*TableRow tblrw = (TableRow)findViewById(R.id.row2);
	        tblrw.setMinimumHeight(cf.ht); */	
	        cf.setupUI((ScrollView) findViewById(R.id.scr));
	       
	        cf.CreateARRTable(61);
			 
	        try
			{
			   Cursor c1=cf.SelectTablefunction(cf.BI_General, " where fld_srid='"+cf.selectedhomeid+"'");
			   if(c1.getCount()>0)
			   {  
				   c1.moveToFirst();
				   if(!cf.decode(c1.getString(c1.getColumnIndex("BI_BUILD_COMM"))).equals(""))
				   {
					   ((EditText)findViewById(R.id.buildingcomm)).setText(cf.decode(c1.getString(c1.getColumnIndex("BI_BUILD_COMM"))));
				   }
				   else
				   {
					   ((EditText)findViewById(R.id.buildingcomm)).setText("No Comments.");
				   }
			   }
			   else
			   {
				   ((EditText)findViewById(R.id.buildingcomm)).setText("No Comments.");
			   }
			}
		    catch (Exception E)
			{
				String strerrorlog="Retrieving COMMETS - Building";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			}
	        ((EditText)findViewById(R.id.buildingcomm)).addTextChangedListener(new textwatcher());
	        
	    }
	
	class textwatcher implements TextWatcher
    {
	        public int type;
	        textwatcher()
	    	{

	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {
	    		// TODO Auto-generated method stub
	    		System.out.println("test"+s.toString());
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.buiildcomm_tv_type1),500);	    		
	    	}

	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,int after) {
	    		// TODO Auto-generated method stub
	     	}

	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,int count) {
	    		// TODO Auto-generated method stub
	    	}
	   }
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.loadcomments:
			/***Call for the comments***/			
			 int len=((EditText)findViewById(R.id.buildingcomm)).getText().toString().length();
			if(load_comment )
				{
					load_comment=false;
					int loc[] = new int[2];
					v.getLocationOnScreen(loc);
					Intent in1 = cf.loadComments("Overall Building Comments",loc);
					in1.putExtra("insp_name", "General Information");
					in1.putExtra("length", len);
					in1.putExtra("max_length", 500);
					startActivityForResult(in1, cf.loadcomment_code);
				}
			 break;
		case R.id.commsave:
			if(((EditText)findViewById(R.id.buildingcomm)).getText().toString().trim().equals(""))
			{
				
				cf.ShowToast("Please enter the Overall Building Comments.", 0);
				cf.setFocus(((EditText)findViewById(R.id.buildingcomm)));
			}
			else
			{
				try
				{
					Cursor c1=cf.SelectTablefunction(cf.BI_General, " where fld_srid='"+cf.selectedhomeid+"'");
					if(c1.getCount()>0)
					{

							cf.arr_db.execSQL("UPDATE "+cf.BI_General+ " set BI_BUILD_COMM='"+cf.encode(((EditText)findViewById(R.id.buildingcomm)).getText().toString())+"' where fld_srid='"+cf.selectedhomeid+"'");
					}
					else
					{
						cf.arr_db.execSQL("INSERT INTO "
								+ cf.BI_General
								+ " (fld_srid,BI_BUILD_COMM)"
								+ "VALUES ('"+cf.selectedhomeid+"','"+cf.encode(((EditText)findViewById(R.id.buildingcomm)).getText().toString())+"')");
					} 
					cf.ShowToast("Overall Building Comments saved successfully.", 1);
					
				}
				catch (Exception E)
				{
					String strerrorlog="Checking the rows inserted in the Building Information Comments table.";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
			}
			
			break;
		}

	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		  if(requestCode==cf.loadcomment_code)
		  {
			load_comment=true;
			if(resultCode==RESULT_OK)
			{
				 cf.ShowToast("Comments added successfully.", 0);
				 String buildingcomts = ((EditText)findViewById(R.id.buildingcomm)).getText().toString();
				 ((EditText)findViewById(R.id.buildingcomm)).setText((buildingcomts+" "+data.getExtras().getString("Comments")).trim());
				
			}
			
		}

	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			//cf.goclass(457);
			cf.goclass(16);
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}
}
