package idsoft.inspectiondepot.ARR;

/***   	Page Information
Version :1.0
VCode:1
Created Date:19/10/2012
Purpose:For Agent Inspection 
Created By:Rakki S
Last Updated:25/10/2012   
Last Updatedby:Rakki s
***/

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
 
public class Agent extends Activity {
	CommonFunctions cf;
	TableLayout agenttbl;TextView noagentinfotxt;
	TextView[] agent = new TextView[16];
	TextView[] realtor = new TextView[14];	
	 public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        cf = new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.selectedhomeid = extras.getString("homeid");
				cf.onlinspectionid = extras.getString("InspectionType");
			    cf.onlstatus = extras.getString("status");
        	}
			setContentView(R.layout.agent);
			cf.getDeviceDimensions();
			LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
			hdr_layout.addView(new HdrOnclickListener(this,1,"General","Agent/Real Estate Info",1,0,cf));
         	LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
         	main_layout.setMinimumWidth(cf.wd);
         	main_layout.addView(new MyOnclickListener(getApplicationContext(), 1, 0,0,cf));
		    LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
		    submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 12, 1,0,cf));
		    LinearLayout submenu2_layout = (LinearLayout) findViewById(R.id.submenu1); 
		    submenu2_layout.addView(new MyOnclickListener(getApplicationContext(),57, 1,0,cf));
		    
		    cf.getInspectorId();
		    agent[3] = (TextView) findViewById(R.id.efirstname);
		    agent[4] = (TextView) findViewById(R.id.txtaddr1);
		    agent[5] = (TextView) findViewById(R.id.txtaddr2);
		    agent[6] = (TextView) findViewById(R.id.txtcity);
		    agent[7] = (TextView) findViewById(R.id.txtcountry);
		    agent[8] = (TextView) findViewById(R.id.econtactperson);
		    agent[9] = (TextView) findViewById(R.id.txtstate);
		    agent[10] = (TextView) findViewById(R.id.txtzip);
		    agent[11] = (TextView) findViewById(R.id.txtoffphn);
		    agent[12] = (TextView) findViewById(R.id.txtconphn);
		    agent[13] = (TextView) findViewById(R.id.txtfax);
		    agent[14] = (TextView) findViewById(R.id.txtmail);
		    agent[15] = (TextView) findViewById(R.id.txtweb);
		    agent[2] = (TextView) findViewById(R.id.txtagncyname);
		    agenttbl= (TableLayout) findViewById(R.id.agenttbl);
		    noagentinfotxt= (TextView) findViewById(R.id.noagentinfotext);
		    
		  
		    
		    realtor[3] = (TextView) findViewById(R.id.txtretcompname);
		    realtor[4] = (TextView) findViewById(R.id.txtrealname);
		    realtor[5] = (TextView) findViewById(R.id.txtretaddr1);
		    realtor[6] = (TextView) findViewById(R.id.txtretaddr2);
		    realtor[7] = (TextView) findViewById(R.id.txtretcity);
		    realtor[8] = (TextView) findViewById(R.id.txtretcountry);
		    realtor[10] = (TextView) findViewById(R.id.txtretstate);
		    realtor[11] = (TextView) findViewById(R.id.txtretzip);
		    realtor[12] = (TextView) findViewById(R.id.txtretoffphn);
		    realtor[13] = (TextView) findViewById(R.id.txtretmail);		    
		    realtor[9] = (TextView) findViewById(R.id.txtretrolesdesc);
		    
		    try {
		    	cf.CreateARRTable(5);
				Cursor cur = cf.SelectTablefunction(cf.Agent_tabble, " where ARR_AI_SRID='"+ cf.selectedhomeid + "'");
				System.out.println("coagent="+cur.getCount());
				if(cur.getCount()!=0)
				{
					//noagentinfotxt.setVisibility(cf.v1.GONE);
					//agenttbl.setVisibility(cf.v1.VISIBLE);
					cur.moveToFirst();
	                if (cur != null) {
	                	System.out.println("agentname="+cf.decode(cur.getString(cur.getColumnIndex("ARR_AI_AgentName")))+"test");
	                	if(cur.getString(cur.getColumnIndex("ARR_AI_AgentName")).trim().equals(""))
	                	{
	                		System.out.println("inside 123");

	                		((LinearLayout)findViewById(R.id.agencylin)).setVisibility(cf.v1.GONE);
	                		((ImageView)findViewById(R.id.shwagent)).setVisibility(cf.v1.VISIBLE);
	                		((ImageView)findViewById(R.id.hdwagent)).setVisibility(cf.v1.GONE);
	                		
	                	}
	                	else
	                	{
	                		
	                		((LinearLayout)findViewById(R.id.agencylin)).setVisibility(cf.v1.VISIBLE);
	                		((ImageView)findViewById(R.id.shwagent)).setVisibility(cf.v1.GONE);
	                		((ImageView)findViewById(R.id.hdwagent)).setVisibility(cf.v1.VISIBLE);
	                	}
	                	
	                	
						for (int i = 2; i <= 15; i++) {
							System.out.println("avalues="+cf.decode(cur.getString(i))+"i="+i);
							agent[i].setText(cf.decode(cur.getString(i)));
							if (cur.getString(i).trim().equals("anyType{}") || cur.getString(i).trim().equals("()-")
									|| cur.getString(i).trim().equals("")) {
								agent[i].setText("N/A");
							}
						}

					}while (cur.moveToNext());
																	
				}
				/*else
				{
					noagentinfotxt.setVisibility(cf.v1.VISIBLE);
					agenttbl.setVisibility(cf.v1.GONE);
				}*/
				
		    }
		    catch(Exception e)
		    {
		    	cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ Agent.this +" "+" in the stage of(catch) retreving  at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				
		    }
		    
		    try {
		    	cf.CreateARRTable(15);
				Cursor c1 = cf.SelectTablefunction(cf.Relator_table, " where ARR_RL_SRID='"+ cf.selectedhomeid + "'");
				System.out.println("corealtor="+c1.getCount());
				if(c1.getCount()!=0)
				{
					c1.moveToFirst();
					
					
	                if (c1 != null) {

	                	if(c1.getString(c1.getColumnIndex("ARR_RL_RealtorName")).trim().equals(""))
	                	{
	                		System.out.println("inside 123");

	                		((LinearLayout)findViewById(R.id.realtorlin)).setVisibility(cf.v1.GONE);
	                		((ImageView)findViewById(R.id.shwreal)).setVisibility(cf.v1.VISIBLE);
	                		((ImageView)findViewById(R.id.hdwreal)).setVisibility(cf.v1.GONE);
	                		
	                	}
	                	else
	                	{
	                		
	                		((LinearLayout)findViewById(R.id.realtorlin)).setVisibility(cf.v1.VISIBLE);
	                		((ImageView)findViewById(R.id.shwreal)).setVisibility(cf.v1.GONE);
	                		((ImageView)findViewById(R.id.hdwreal)).setVisibility(cf.v1.VISIBLE);
	                	}
	                	
	                	
	                	realtor[3].setText(c1.getString(2).equals("")?"N/A" : c1.getString(2));
	                	realtor[4].setText(c1.getString(3).equals("")?"N/A" : c1.getString(3));
	                	realtor[5].setText(c1.getString(4).equals("")?"N/A" : c1.getString(4));
	                	realtor[6].setText(c1.getString(5).equals("")?"N/A" : c1.getString(5));	                	
	                	realtor[7].setText(c1.getString(6).equals("")?"N/A" : c1.getString(6));
	                	realtor[8].setText(c1.getString(7).equals("")?"N/A" : c1.getString(7));	                	
	                	realtor[10].setText(c1.getString(9).equals("")?"N/A" : c1.getString(9));
	                	realtor[11].setText(c1.getString(10).equals("")?"N/A" : c1.getString(10));
	                	realtor[12].setText(c1.getString(11).equals("")?"N/A" : cf.decode(c1.getString(11)));
	                	realtor[13].setText(c1.getString(11).equals("")?"N/A" : c1.getString(13));
	                	realtor[9].setText(c1.getString(8).equals("")?"N/A" : c1.getString(8));
					}while (c1.moveToNext());
																	
				}	
		    }
		    catch(Exception e)
		    {
		    	System.out.println("fdsff"+e.getMessage());
		    	cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ Agent.this +" "+" in the stage of(catch) retreving  at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				
		    }
	 }
	 public void clicker(View v) {
			switch (v.getId()) {

			case R.id.home:
				cf.gohome();
			break;
			case R.id.shwagent:
		    	hideotherlayouts();
				((LinearLayout)findViewById(R.id.agencylin)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdwagent)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.shwagent)).setVisibility(cf.v1.GONE);
			break;
		    case R.id.hdwagent:
				((ImageView)findViewById(R.id.shwagent)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdwagent)).setVisibility(cf.v1.GONE);
				((LinearLayout)findViewById(R.id.agencylin)).setVisibility(cf.v1.GONE);
				break;
		    case R.id.shwreal:
		    	hideotherlayouts();
				((LinearLayout)findViewById(R.id.realtorlin)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdwreal)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.shwreal)).setVisibility(cf.v1.GONE);
			break;
		    case R.id.hdwreal:
				((ImageView)findViewById(R.id.shwreal)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdwreal)).setVisibility(cf.v1.GONE);
				((LinearLayout)findViewById(R.id.realtorlin)).setVisibility(cf.v1.GONE);
				break;
			}
		}
	 private void hideotherlayouts() {
			// TODO Auto-generated method stub
			((LinearLayout)findViewById(R.id.agencylin)).setVisibility(cf.v1.GONE);
			((LinearLayout)findViewById(R.id.realtorlin)).setVisibility(cf.v1.GONE);
			((ImageView)findViewById(R.id.shwreal)).setVisibility(cf.v1.VISIBLE);
			((ImageView)findViewById(R.id.shwagent)).setVisibility(cf.v1.VISIBLE);
			((ImageView)findViewById(R.id.hdwreal)).setVisibility(cf.v1.GONE);
			((ImageView)findViewById(R.id.hdwagent)).setVisibility(cf.v1.GONE);
		}
	 public boolean onKeyDown(int keyCode, KeyEvent event) {
			if (keyCode == KeyEvent.KEYCODE_BACK) {
				cf.goback(0);
				return true;
			}
			return super.onKeyDown(keyCode, event);
		}

}