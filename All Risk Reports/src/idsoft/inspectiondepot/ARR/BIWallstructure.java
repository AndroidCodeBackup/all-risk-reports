package idsoft.inspectiondepot.ARR;

import java.util.ArrayList;
import java.util.List;

import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.MultiSpinner.MultiSpinnerListener;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

public class BIWallstructure extends Activity {
	CommonFunctions cf;
	TextView tv[] = new TextView[101];
	String Wall_Split[],WSPer_Split[]=new String[9];
	public TextView tvwstrudetails[] = new TextView[9];
	public int[] wallstrudetailsid = {R.id.tvwsdetails1,R.id.tvwsdetails2,R.id.tvwsdetails3,R.id.tvwsdetails4,R.id.tvwsdetails5,R.id.tvwsdetails6,R.id.tvwsdetails7,R.id.tvwsdetails8,R.id.tvwsdetails9};
	public MultiSpinner spnwcstories[] = new MultiSpinner[9];
	public int[] spnwcstoriesid = {R.id.spnstories1,R.id.spnstories2,R.id.spnstories3,R.id.spnstories4,R.id.spnstories5,R.id.spnstories6,R.id.spnstories7,R.id.spnstories8,R.id.spnstories9};
	Cursor c1;
	private List<String> items= new ArrayList<String>();
	public TextView tvwstrutitle[] = new TextView[9];
	public int[] wallstrutitleid = {R.id.tvwstitle1,R.id.tvwstitle2,R.id.tvwstitle3,R.id.tvwstitle4,R.id.tvwstitle5,R.id.tvwstitle6,R.id.tvwstitle7,R.id.tvwstitle8,R.id.txtwc9};
	String spnwallstruc[] = new String[9];
	public TextView tvwstruothertxt[] = new TextView[9];
	public int[] wallstruothersid = {R.id.tvwsothertxt1,R.id.tvwsothertxt2,R.id.tvwsothertxt3,R.id.tvwsothertxt4,R.id.tvwsothertxt5,R.id.tvwsothertxt6,R.id.tvwsothertxt7,R.id.tvwsothertxt8,R.id.tvwsothertxt9};
	public TextView dummytext[] = new TextView[9];
	public int[] tvid = {R.id.tv1,R.id.tv2,R.id.tv3,R.id.tv4,R.id.tv5,R.id.tv6,R.id.tv7,R.id.tv8,R.id.tv9};
	public TextView tvstoriesdisplay[] = new TextView[9];
	public int[] tvstoriesid = {R.id.tvstor1,R.id.tvstor2,R.id.tvstor3,R.id.tvstor4,R.id.tvstor5,R.id.tvstor6,R.id.tvstor7,R.id.tvstor8,R.id.tvstor9};
	Dialog dialog1;
	String wallstru="",floorchoosen="",s1="",WS_NA="",ISO="",wsnotappl="0",totperc="",totstor="",wsothervalue="",s="", wsvalue="",spn1="",spn2="",spn3="",spn4="",spn5="",spn6="",spn7="",spn8="",spn9="",Wallstructure="",WallstructurePerc="";
	int h=0,per1 = 0,per2 = 0,per3 = 0,per4 = 0,per5 = 0,per6 = 0,per7 = 0,per8=0,per9=0,per10=0;
	TableLayout myTable;TableRow tr;
	String WS_Split[];
	String wallstruvalue[] = new String[15];
	public Spinner spnwcpercentage[] = new Spinner[9];
	public int[] spnwcpercentageid = {R.id.spnperc1,R.id.spnperc2,R.id.spnperc3,R.id.spnperc4,R.id.spnperc5,R.id.spnperc6,R.id.spnperc7,R.id.spnperc8,R.id.spnperc9};
	ArrayAdapter spnpercadap;
	public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	       
	        cf = new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);				
		 	}
	        setContentView(R.layout.wallstructure);
	        cf.getDeviceDimensions();
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
         	hdr_layout.addView(new HdrOnclickListener(this,1,"General","Building Info","Wall Structure",1,1,cf));
         	LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
         	main_layout.setMinimumWidth(cf.wd);
         	main_layout.addView(new MyOnclickListener(getApplicationContext(), 1, 0,0,cf));
            LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
            submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 16, 1,0,cf));
	        LinearLayout submenu2_layout = (LinearLayout) findViewById(R.id.submenu1); 
	        submenu2_layout.addView(new MyOnclickListener(getApplicationContext(), 455, 1,0,cf));
	        TableRow tblrw = (TableRow)findViewById(R.id.row2);
	        tblrw.setMinimumHeight(cf.ht); 
	        cf.CreateARRTable(3);
	        cf.CreateARRTable(61);
	        Declaration();
	        Wallstructurestories();
	        setValue();
	    }
	private void Wallstructurestories() {
		// TODO Auto-generated method stub
			try{
				selecttbl();
				if(!Wallstructure.equals(""))
				{
					wsvalue="";
					String Wall_Struc_Split[] = Wallstructure.split("\\^",-1);
					for(int k=0;k<Wall_Struc_Split.length;k++)
					{
			    		if(!Wall_Struc_Split[k].trim().equals(""))
			    		{
			    			String WS_split[] = Wall_Struc_Split[k].split(",");
			    			for(int j=0;j<WS_split.length;j++)
			    			{
			    				if(!items.contains(WS_split[j].trim()))
				    			{
			    						if(h==0)
					    				{
				    						wallstruvalue[k] = Wall_Struc_Split[k].replace(WS_split[j],"");						
											h++;
					    				}
					    				else
					    				{
					    					wallstruvalue[k] = wallstruvalue[k].replace(WS_split[j],"");				    					
					    				}
				    			}
				    			else
				    			{
				    					if(h==0){
											if(wallstruvalue[k]==null) {wallstruvalue[k]="";wallstruvalue[k]=Wall_Struc_Split[k];}
											else{wallstruvalue[k] =Wall_Struc_Split[k];}
										}
				    			}
			    			}h=0;	
			    		}		
			    		else
			    		{
			    			wallstruvalue[k]="";
			    		}
			    		
			    		if(!wallstruvalue[k].equals(""))
			    		{
			    		
			    			if(wallstruvalue[k].contains(",,"))
				    		{
			    				wallstruvalue[k]=wallstruvalue[k].replace(",,", "");
				    		}
			    			if(!wallstruvalue[k].equals(""))
			    			{
			    				String lastChar = wallstruvalue[k].substring(wallstruvalue[k].length()-1);
			    				if (lastChar.contains(",")) {
			    					wallstruvalue[k] = wallstruvalue[k].substring (0,wallstruvalue[k].length()-1);
			    				}
			    			}		    			
			    		}	    
			    		spnwcstories[k].setItems1(items,"--Select--",new multi(),wallstruvalue[k]);	    	
			    	
			    		wsvalue +=wallstruvalue[k]+"^";
					}
					
					if(wsvalue.substring(wsvalue.lastIndexOf("^")).equals("^"))
					{
						wsvalue= wsvalue.substring(0,wsvalue.lastIndexOf("^"));
					}
				}				
					if(c1.getCount()!=0)
					{
						cf.arr_db.execSQL("UPDATE "	+ cf.BI_General	+ " SET BI_WALLSTRU='" + wsvalue+ "' WHERE fld_srid ='"+ cf.selectedhomeid + "'");			
					}			
			}
		    catch(Exception e)
		    {
		    	System.out.println("BI_WALLSTRU updating"+e.getMessage());
		    }
	}
	class multi implements MultiSpinnerListener{

		public void onItemsSelected(boolean[] selected) {
			// TODO Auto-generated method stub
			try
			{
				for(int i=0;i<spnwcstories.length;i++)
				{
					if(!"You have selected --Select--".equals(spnwcstories[i].getSelectedtext().toString()))
					{
						if(spnwcstories[i].getSelectedtext().toString().contains(","))
						{
							floorchoosen = " to view the selected Floors";
						}
						else
						{
							floorchoosen = " to view the selected Floor";
						}
						
						if(!spnwcstories[i].getSelectedtext().toString().equals(""))
						{								
							tvstoriesdisplay[i].setText("You have selected " +spnwcstories[i].getSelectedtext().toString());
							tvwstrudetails[i].setVisibility(cf.v1.VISIBLE);
							tvwstruothertxt[i].setVisibility(cf.v1.VISIBLE);
							tvwstrudetails[i].setText(Html.fromHtml("<font color='#0000FF'><u>Click here</u></font>"));
							tvwstruothertxt[i].setText(Html.fromHtml("<font color='#D11C1C'>"+floorchoosen+"</font>"));
							
						}else
						{
							tvstoriesdisplay[i].setVisibility(cf.v1.GONE);
							tvwstrudetails[i].setVisibility(cf.v1.INVISIBLE);
							tvwstruothertxt[i].setVisibility(cf.v1.INVISIBLE);
						}
					}
				}
			}
			catch(Exception e)
			{
				System.out.println("E "+e.getMessage());
			}
		}
    }
	private void selecttbl() {
		c1= cf.SelectTablefunction(cf.BI_General, " where fld_srid='"+cf.selectedhomeid+"'");
		if(c1.getCount()>0)
		{
			c1.moveToFirst();
			Wallstructure = cf.decode(c1.getString(c1.getColumnIndex("BI_WALLSTRU")));
			WallstructurePerc = cf.decode(c1.getString(c1.getColumnIndex("BI_WALLSTRUPER")));
			wsothervalue = cf.decode(c1.getString(c1.getColumnIndex("BI_WALLSTRUC_OTHER")));
			WS_NA = c1.getString(c1.getColumnIndex("BI_WS_NA"));
			ISO = cf.decode(c1.getString(c1.getColumnIndex("BI_ISO")));
		}
	}
	private void Declaration() {
		// TODO Auto-generated method stub
		cf.setupUI((ScrollView) findViewById(R.id.scr));
		for(int i=0;i<tvwstrutitle.length;i++)
  	     {
	   		tvwstrutitle[i] = (TextView)findViewById(wallstrutitleid[i]);
  	     }
		for(int i=0;i<tvwstrudetails.length;i++)
	   	 {
			tvwstrudetails[i] = (TextView)findViewById(wallstrudetailsid[i]);
		   	tvwstrudetails[i].setOnClickListener(new OnClickListener1(i));
	   	 }
		 for(int i=0;i<tvwstruothertxt.length;i++)
		 {
		 	tvwstruothertxt[i] = (TextView)findViewById(wallstruothersid[i]);		   		
	   	 }
		 for(int i=0;i<dummytext.length;i++)
	   	 {
		   	dummytext[i] = (TextView)findViewById(tvid[i]);
		 }
	   	 for(int i=0;i<tvstoriesdisplay.length;i++)
   	     {
	   		tvstoriesdisplay[i] = (TextView)findViewById(tvstoriesid[i]);
   	     }
	   	items.clear();
	
		try
		{
			cf.getPolicyholderInformation(cf.selectedhomeid);
			for(int j=0;j<Integer.parseInt(cf.Stories);j++)
			{
				items.add(cf.spnstories[j]);
			}
		}
		catch(Exception e)
		{
			
		}
	   	for(int i=0;i<spnwcstories.length;i++)
   	    {
	   		spnwcstories[i] = (MultiSpinner)findViewById(spnwcstoriesid[i]);
	   		spnwcstories[i].setItems(items,"--Select--",new multi());	
	   		spnwcstories[i].setSelectedtext(dummytext[i]);	   		
   	    }
	    
	   	for(int i=0;i<spnwcpercentage.length;i++)
		{
			spnwcpercentage[i] = (Spinner)findViewById(spnwcpercentageid[i]);
			spnpercadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, cf.spnpercentage);
			spnpercadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spnwcpercentage[i].setAdapter(spnpercadap);
			spnwcstories[i].setEnabled(false);
			spnwcpercentage[i].setOnItemSelectedListener(new MyOnItemSelectedListenerpercent(spnwcpercentage[i].getId()));
		}
	   	((EditText)findViewById(R.id.edwallstructureothr)).addTextChangedListener(new CustomTextWatcherSpace(((EditText)findViewById(R.id.edwallstructureothr))));
	   	
	}
	class OnClickListener1 implements OnClickListener {
		   int id,iden;
		   OnClickListener1(int id1)
			{
				this.id=id1;
			}
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog1 = new Dialog(BIWallstructure.this,android.R.style.Theme_Translucent_NoTitleBar);
				dialog1.getWindow().setContentView(R.layout.alertwallstories);
				
				
				if(tvwstrutitle[id].getText().toString().equals("Other"))
				{
					((TextView)dialog1.findViewById(R.id.txtheading)).setText(tvwstrutitle[id].getText().toString()+" ("+((EditText)findViewById(R.id.edwallstructureothr)).getText().toString()+")");
				}
				else
				{
					((TextView)dialog1.findViewById(R.id.txtheading)).setText(tvwstrutitle[id].getText().toString());
				}	
				
				
				myTable = (TableLayout)dialog1.findViewById(R.id.mainLayout);
				alertwallstories(id);
				((ImageView)dialog1.findViewById(R.id.imagehelpclose)).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog1.setCancelable(true);
						dialog1.dismiss();
					}
				});		
				((Button)dialog1.findViewById(R.id.okbtn)).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog1.setCancelable(true);
						dialog1.dismiss();
					}
				});
				dialog1.show();
			}
	    }
	public class MyOnItemSelectedListenerpercent implements OnItemSelectedListener {
		int type1;
		public MyOnItemSelectedListenerpercent(int id) {
			// TODO Auto-generated constructor stub
			type1=id;
		}

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			String storiesvalue = ((Spinner)findViewById(type1)).getSelectedItem().toString();
			if (storiesvalue.equals("")) {
				storiesvalue = "0";
			}			
			switch(type1)
			{			
				case R.id.spnperc1:					
					try {
						if(!storiesvalue.equals("--Select--"))
						{
							per1 = Integer.parseInt(storiesvalue);	
							spnwcstories[0].setEnabled(true);
						}
						else
						{
							per1=0;
							uncheckfloors(0);
						}
					} catch (Exception e) {
						per1 = 0;
					}
					break;
				case R.id.spnperc2:
					try {
						if(!storiesvalue.equals("--Select--"))
						{
							per2 = Integer.parseInt(storiesvalue);
							spnwcstories[1].setEnabled(true);
						}
						else
						{
							per2=0;
							uncheckfloors(1);
						}
					} catch (Exception e) {
						per2 = 0;
					}
					break;
				case R.id.spnperc3:
					try {
						if(!storiesvalue.equals("--Select--"))
						{
							per3 = Integer.parseInt(storiesvalue);
							spnwcstories[2].setEnabled(true);
						}
						else
						{
							per3=0;
							uncheckfloors(2);
						}
					} catch (Exception e) {
						per3 = 0;
					}
					break;
				case R.id.spnperc4:
					try {
						if(!storiesvalue.equals("--Select--"))
						{
							per4 = Integer.parseInt(storiesvalue);
							spnwcstories[3].setEnabled(true);
						}
						else
						{
							per4=0;
							uncheckfloors(3);
						}
					} catch (Exception e) {
						per4 = 0;
					}
					break;
				case R.id.spnperc5:
					try {
						if(!storiesvalue.equals("--Select--"))
						{
							per5 = Integer.parseInt(storiesvalue);	
							spnwcstories[4].setEnabled(true);
						}
						else
						{
							per5=0;
							uncheckfloors(4);
						}
					} catch (Exception e) {
						per5 = 0;
					}
					break;
				case R.id.spnperc6:
					try {
						if(!storiesvalue.equals("--Select--"))
						{
							per6 = Integer.parseInt(storiesvalue);
							spnwcstories[5].setEnabled(true);
						}
						else
						{
							per6=0;
							uncheckfloors(5);
						}
					} catch (Exception e) {
						per6 = 0;
					}
					break;
				case R.id.spnperc7:
					try {
						if(!storiesvalue.equals("--Select--"))
						{
							per7 = Integer.parseInt(storiesvalue);	
							spnwcstories[6].setEnabled(true);
						}
						else
						{
							per7=0;
							uncheckfloors(6);
						}
					} catch (Exception e) {
						per7 = 0;
					}
					break;
				case R.id.spnperc8:
					try {
						if(!storiesvalue.equals("--Select--"))
						{
							per8 = Integer.parseInt(storiesvalue);
							spnwcstories[7].setEnabled(true);
						}
						else
						{
							per8=0;
							uncheckfloors(7);
						}
					} catch (Exception e) {
						per8 = 0;
					}
					break;
				case R.id.spnperc9:
					try {
						if(!storiesvalue.equals("--Select--"))
						{
							per9 = Integer.parseInt(storiesvalue);	
							spnwcstories[8].setEnabled(true);
						}
						else
						{
							per9=0;
							uncheckfloors(8);
						}
					} catch (Exception e) {
						per9 = 0;
					}
					break;
					
			}
			}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
	}
	
	private void setValue() {
		try
		{
			selecttbl();
			if(c1.getCount()==0)
			{
				((CheckBox)findViewById(R.id.wsnotappl)).setChecked(true);
				((LinearLayout)findViewById(R.id.wallstruclin)).setVisibility(cf.v1.GONE);
			}
			else
			{
				if(WS_NA.equals("0") || WS_NA.equals(""))
				{
					((LinearLayout)findViewById(R.id.wallstruclin)).setVisibility(cf.v1.VISIBLE);
					if(!WallstructurePerc.equals(""))
					{
						String Wall_Struc_Split[] = WallstructurePerc.split("\\^",-1);
						for(int k=0;k<Wall_Struc_Split.length;k++)
						{	
							int spinnerPosition = spnpercadap.getPosition(Wall_Struc_Split[k]);
							spnwcpercentage[k].setSelection(spinnerPosition);
							if(!Wall_Struc_Split[k].equals("0"))
							{
								spnwcstories[k].setEnabled(true);
							}
						}
						((TextView)findViewById(R.id.savewallstruct)).setVisibility(cf.v1.VISIBLE);
					}
					else
					{
						
						
						((LinearLayout)findViewById(R.id.wallstruclin)).setVisibility(cf.v1.GONE);
						((TextView)findViewById(R.id.savewallstruct)).setVisibility(cf.v1.GONE);
						((CheckBox)findViewById(R.id.wsnotappl)).setChecked(true);
					}
					
					
					
					
					
						if(!Wallstructure.equals(""))
						{
							String floorchoosen="";
							WSPer_Split = Wallstructure.split("\\^",-1);			
							for(int k=0;k<WSPer_Split.length;k++)
							{
									dummytext[k].setText("");
									spnwcstories[k].setItems1(items,"--Select--",new multi(),WSPer_Split[k]);
									spnwcstories[k].setSelectedtext(dummytext[k]);					
									if(spnwcstories[k].getSelectedtext().toString().equals("You have selected ")){				
										spnwallstruc[k] =  spnwcstories[k].getSelectedtext().toString().replace("You have selected ","");
									}else{
										spnwallstruc[k] =  spnwcstories[k].getSelectedtext().toString();
									}
									if(!spnwallstruc[k].equals(""))
									{
										
										if(spnwallstruc[k].contains(","))
										{
											floorchoosen = " to view the selected Floors";
										}
										else
										{
											floorchoosen = " to view the selected Floor";
										}
										
										tvwstrudetails[k].setVisibility(cf.v1.VISIBLE);
										tvwstruothertxt[k].setVisibility(cf.v1.VISIBLE);
										tvwstrudetails[k].setText(Html.fromHtml("<font color='#0000FF'><u>Click here</u></font>"));
										tvwstruothertxt[k].setText(Html.fromHtml("<font color='#D11C1C'>"+floorchoosen+"</font>"));
									}
									else
									{
										tvwstrudetails[k].setVisibility(cf.v1.INVISIBLE);
										tvwstruothertxt[k].setVisibility(cf.v1.INVISIBLE);
									}				
							 }
							 if(!wsothervalue.equals(""))
							 {
								((EditText)findViewById(R.id.edwallstructureothr)).setText(wsothervalue);
							 }
							 else
							 {
								 ((EditText)findViewById(R.id.edwallstructureothr)).setText("");}			
							 }
				}
				else
				{
					((CheckBox)findViewById(R.id.wsnotappl)).setChecked(true);
					((LinearLayout)findViewById(R.id.wallstruclin)).setVisibility(cf.v1.GONE);
					((TextView)findViewById(R.id.savewallstruct)).setVisibility(cf.v1.VISIBLE);
				}
			}
		}
		catch(Exception e)
		{
			
		}
	}
		
		
	public void uncheckfloors(int i) {
		// TODO Auto-generated method stub
		spnwcstories[i].setEnabled(false);
		dummytext[i].setText("");
		spnwcstories[i].setSelectedtext(dummytext[i]);	
		tvwstrudetails[i].setVisibility(cf.v1.GONE);
		tvwstruothertxt[i].setVisibility(cf.v1.GONE);
		spnwcstories[i].setItems1(items,"--Select--",new multi(),"");
	}
	protected void alertwallstories(int val) {
		// TODO Auto-generated method stub
		cf.getPolicyholderInformation(cf.selectedhomeid);
		if(!spnwcstories[val].getSelectedtext().toString().equals(""))
		{
			wallstru= spnwcstories[val].getSelectedtext().toString().replace("You have selected ","");			
			Wall_Split = wallstru.trim().split(",");
		}
		
		/*if(WSPer_Split[val]==null || WSPer_Split[val]=="")
		{
			wallstru= spnwcstories[val].getSelectedtext().toString().replace("You have selected ","");			
			Wall_Split = wallstru.trim().split(",");
		}
		else
		{
			Wall_Split = WSPer_Split[val].split(",");
		}*/
		
		
		int k=0;
			for(int i=0;i<10;i++)
	        {
				tr =new TableRow(this);
		        TableRow.LayoutParams params = new TableRow.LayoutParams(55,30);
		        params.setMargins(1, 1, 0, 1);
		        tr.setOrientation(tr.HORIZONTAL);
		        for (int j=1;j<=10;j++)
		        {
		        	 tv[j] = new TextView(this);
		        	 tv[j].setTypeface(null, Typeface.BOLD);
		        	 tv[j].setGravity(Gravity.CENTER);
		        	 tv[j].setLayoutParams(params);
		        	 tv[j].setTextColor(Color.BLACK);
		        	 tv[j].setBackgroundResource(R.color.Wall_Light);
		        	 String l = ((i*10)+j)+"";	 
		 	         if(Integer.parseInt(l)<=Integer.parseInt(cf.Stories))
		        	 {
		        		 tv[j].setText(l);
		        		 tv[j].setBackgroundResource(R.color.Wall_selected);		 
		        	 }
		        	 else
		        	 {
		        		 tv[j].setText(l); 
		        		 tv[j].setTextColor(Color.parseColor("#bbd3e8"));
		        	 }
		 	         if(k<Wall_Split.length){
		 	        	if(Wall_Split[k].trim().equals(l))
		 	        	 {
		 	        		 tv[j].setBackgroundResource(R.color.Wall_dark);	
		 	        		 tv[j].setTextColor(Color.WHITE);k++;
		 	        	 }
		 	         }
		 	         	
		 	        tr.addView(tv[j]);
		        }    
		        myTable.addView(tr); 
	        } 
	}
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.mouseoverid:
			cf.ShowToast("Use for 4 Point, Commercial Mitigation,Sinkhole, Replacement Cost Inspections.",0);
			break;
		case R.id.wssave:
			selecttbl();
			//if(WS_NA.equals("0") || WS_NA.equals(""))
			if(((CheckBox)findViewById(R.id.wsnotappl)).isChecked()==false)
			{
					int totp = per1 + per2 + per3 + per4 + per5 + per6 +per7 + per8 +per9;
					if(totp==0)
					{
						cf.ShowToast("Please select atleast one Wall Structure.",1);		
					}
					else if(totp < 100)
					{
						cf.ShowToast("Percentage field should sum to 100.",1);
					}
					else if (totp > 100)
					{
						cf.ShowToast("Percentage field exceeds 100% and  should sum to 100.",1);
					}
					else if (totp == 100) 
					{				
						if(per9!=0) 
						{
							s = ((EditText)findViewById(R.id.edwallstructureothr)).getText().toString().trim();	
							if(s.equals(""))
							{
								cf.ShowToast("Please enter the other text for Wall Structure.",1);
								cf.setFocus((EditText)findViewById(R.id.edwallstructureothr));
							}
							else
							{
								InsertWallStructure();
							}
						}
						else
						{
							InsertWallStructure();
						}					
					}
			}
			else
			{
				InsertWallStructure();
			}
			break;
		case R.id.wsnotappl:			
			if(((CheckBox)findViewById(R.id.wsnotappl)).isChecked())
			{
			wsnotappl="1";totperc="";totstor="";
				if(!WallstructurePerc.equals(""))
				{
					AlertDialog.Builder builder = new AlertDialog.Builder(BIWallstructure.this);
					builder.setTitle("Confirmation");
					builder.setIcon(R.drawable.alertmsg);
					builder.setMessage("Do you want to Clear the Wall Structure Information?").setCancelable(false)
							.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,int id1) 
										{
												wallstructureclear();
												((CheckBox)findViewById(R.id.wsnotappl)).setChecked(true);
												((LinearLayout)findViewById(R.id.wallstruclin)).setVisibility(cf.v1.GONE);
												((TextView)findViewById(R.id.savewallstruct)).setVisibility(cf.v1.GONE);
										}
									})
							.setNegativeButton("No",new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,	int id) {
											
											((CheckBox)findViewById(R.id.wsnotappl)).setChecked(false);
											dialog.cancel();											
									}
							});
					builder.show();
				}
				else
				{
					((TextView)findViewById(R.id.savewallstruct)).setVisibility(cf.v1.GONE);
					((LinearLayout)findViewById(R.id.wallstruclin)).setVisibility(cf.v1.GONE);
				}
				
			}
			else
			{
				wsnotappl="0";totperc="";totstor="";wallstructureclear();
				((LinearLayout)findViewById(R.id.wallstruclin)).setVisibility(cf.v1.VISIBLE);
				((TextView)findViewById(R.id.savewallstruct)).setVisibility(cf.v1.GONE);
			}
			break;
		}		
	}
	protected void wallstructureclear() {
		// TODO Auto-generated method stub
		for(int i=0;i<spnwcpercentage.length;i++)
		{
			spnwcpercentage[i].setSelection(0);
			tvwstrudetails[i].setVisibility(cf.v1.GONE);
			tvwstruothertxt[i].setVisibility(cf.v1.GONE);
			dummytext[i].setText("");
			spnwcstories[i].setItems1(items,"--Select--",new multi(),dummytext[i].getText().toString());
			spnwcstories[i].setSelectedtext(dummytext[i]);	
		}
		
		((EditText)findViewById(R.id.edwallstructureothr)).setText("");WallstructurePerc="";
		selecttbl();
		System.out.println("iso="+ISO);
		if(!ISO.equals(""))
		{
			String isoarr[] = ISO.split("~");
			
		}
	}
	private void InsertWallStructure() {
		// TODO Auto-generated method stub		
		spn1=spnwcstories[0].getSelectedtext().toString();
		spn2=spnwcstories[1].getSelectedtext().toString();
		spn3=spnwcstories[2].getSelectedtext().toString();
		spn4=spnwcstories[3].getSelectedtext().toString();
		spn5=spnwcstories[4].getSelectedtext().toString();
		spn6=spnwcstories[5].getSelectedtext().toString();
		spn7=spnwcstories[6].getSelectedtext().toString();
		spn8=spnwcstories[7].getSelectedtext().toString();
		spn9=spnwcstories[8].getSelectedtext().toString();
		
		if(!spn1.equals("")){spn1=spn1.replace("You have selected ", "");spn1=spn1.replace(" ", "");}else{spn1=" ";}
		if(!spn2.equals("")){spn2=spn2.replace("You have selected ", "");spn2=spn2.replace(" ", "");}else{spn2=" ";}
		if(!spn3.equals("")){spn3=spn3.replace("You have selected ", "");spn3=spn3.replace(" ", "");}else{spn3=" ";}
		if(!spn4.equals("")){spn4=spn4.replace("You have selected ", "");spn4=spn4.replace(" ", "");}else{spn4=" ";}
		if(!spn5.equals("")){spn5=spn5.replace("You have selected ", "");spn5=spn5.replace(" ", "");}else{spn5=" ";}
		if(!spn6.equals("")){spn6=spn6.replace("You have selected ", "");spn6=spn6.replace(" ", "");}else{spn6=" ";}
		if(!spn7.equals("")){spn7=spn7.replace("You have selected ", "");spn7=spn7.replace(" ", "");}else{spn7=" ";}
		if(!spn8.equals("")){spn8=spn8.replace("You have selected ", "");spn8=spn8.replace(" ", "");}else{spn8=" ";}
		if(!spn9.equals("")){spn9=spn9.replace("You have selected ", "");spn9=spn9.replace(" ", "");}else{spn9=" ";}
		
		
		 totperc = per1+"^"+per2+"^"+per3+"^"+per4+"^"+per5+"^"+per6+"^"+per7+"^"+per8+"^"+per9;
		 if(!spn1.equals("") || !spn2.equals("") || !spn3.equals("") || !spn4.equals("") || !spn5.equals("")|| !spn6.equals("")|| !spn7.equals("")|| !spn8.equals("") || !spn9.equals(""))
		 {
			 totstor=spn1+"^"+spn2+"^"+spn3+"^"+spn4+"^"+spn5+"^"+spn6+"^"+spn7+"^"+spn8+"^"+spn9;
		 }
		 else
		 {
			 totstor="";
		 }
		 if(((CheckBox)findViewById(R.id.wsnotappl)).isChecked()){wsnotappl="1";totperc="";totstor="";s="";}else{wsnotappl="0";}
		 updatenotappl();
		 
		cf.ShowToast("Wall Structure saved successfully",1);
		//((TextView)findViewById(R.id.savewallstruct)).setVisibility(cf.v1.VISIBLE);
		cf.goclass(456);		
	}
	private void updatenotappl()
	{
		if(per9==0 && spn9.trim().equals(""))
		{
			s1 = ""; 
		}
		else
		{
			s1=cf.encode(((EditText)findViewById(R.id.edwallstructureothr)).getText().toString().trim());
		}	
		try
		{
			selecttbl();
			if(c1.getCount()==0)
			{
				cf.arr_db.execSQL("INSERT INTO " + cf.BI_General + " (fld_srid,BI_WALLSTRU,BI_WALLSTRUPER,BI_WALLSTRUC_OTHER,BI_WS_NA)"
						+ "VALUES ('"+cf.selectedhomeid+"','"+totstor+"','"+totperc+"','"+s1+"','"+wsnotappl+"')");
			}
			else
			{
				cf.arr_db.execSQL("UPDATE "
						+ cf.BI_General
						+ " SET BI_WS_NA='"+wsnotappl+"', BI_WALLSTRU='"+totstor+"',BI_WALLSTRUPER='"+totperc+"',BI_WALLSTRUC_OTHER='"+s1+"'"
						+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
			}			
		}
		catch(Exception e)
		{
			System.out.println("wsnotappl"+e.getMessage());
		}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			cf.goclass(454);
			return true;
		}		
		return super.onKeyDown(keyCode, event);
	}
}
