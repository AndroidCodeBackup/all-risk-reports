/*
************************************************************
* Project: ALL RISK REPORT
* Module : WindMitRoofWall.java
* Creation History:
Created By : Nalini on 10/24/2012
* Modification History:
Last Modified By : Gowri on 11/28/2012
************************************************************ 
*/
package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.WindMitBuildCode.textwatcher;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.opengl.Visibility;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;

public class WindMitRoofWall extends Activity {
	CommonFunctions cf;
	boolean chk_status = true;
	boolean load_comment=true,clipschk=false,doublechk=false;
	String showstr="",roofwallothertxt="",clipsvalue="",doublewrapsvalue="",subvalue="0";
	RadioGroup toenailrdgrp,clipsrdgrp,doubwrapsrdgrp;
	int clipsminvalue1=0, clipsminvalue2=0, roofclipsminvalue=0, clipdoubleminvalue1=0,roofwall_vlaue,roofwallsub_vlaue,
	clipsingleminvalue1=0, clipsingleminvalue2=0, roofclipsdoubleminvalue=0,roofclipssingleminvalue=0,
	clipdoubleminvalue2=0,toerdgpID,clipsrdgrpID,doubwrapsrdgrpID;
	public int[] rwid = {R.id.RWoption1,R.id.RWoption2,R.id.RWoption3,R.id.RWoption4,R.id.RWoption5,R.id.RWoption6,R.id.RWoption7,R.id.RWoption8};
	public int[] rwlinid = {R.id.rwnrlytoptA,R.id.rwnrlytoptB,R.id.rwnrlytoptC,R.id.rwnrlytoptD};
	public RelativeLayout ln[] = new RelativeLayout[4];
	public RadioButton rwardion[] = new RadioButton[8];
	EditText edroofwallother,edrwcomments;
	String b1_q4[] = {"Toe Nail", "Clips", "Single Wraps","Double Wraps", "Structural", "Other", "Unknown", "No Attic" };
	String chkstatus = "true",updatecnt="",s="",rdiochk="",tmp="";
	private int[] RDoption;
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cf=new CommonFunctions(this);	     
        Bundle extras = getIntent().getExtras();
		if (extras != null) {
			cf.getExtras(extras);				
	 	}	
		setContentView(R.layout.windmitroofwall);
        cf.getDeviceDimensions();      
        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
        if(cf.identityval==31)
        {
           hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","B1 1802(Rev.01/12)","Roof Wall",3,1,cf));
        }
        else if(cf.identityval==32)
        { 
        	hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","Commercial Type I","Roof Wall",3,1,cf));
        }
		LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
     	main_layout.setMinimumWidth(cf.wd);
     	main_layout.addView(new MyOnclickListener(getApplicationContext(), 3, 0,0,cf));
       
        cf.type_submenulayout =(LinearLayout) findViewById(R.id.typesubmenu); //** WINDMIT TYPE SUBMENU **//
        cf.type_submenulayout.addView(new MyOnclickListener(this, 300, 1,0, cf));
        cf.windmittypechk(cf.identityval,((RadioButton)findViewById(R.id.b1802_chk)),((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((TextView)findViewById(R.id.commtxt)),((ImageView)findViewById(R.id.editbno))); // To check the checkbox of choosen inspection type
        cf.uncheckcommercial(((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((RadioButton)findViewById(R.id.b1802_chk))); 
    	        
        cf.submenu_layout = (LinearLayout) findViewById(R.id.submenu);//** WINDMIT TYPE SUBMENU **//	        
        cf.submenu_layout .addView(new MyOnclickListener(this,cf.identityval,1,304, cf));
        
        cf.mainlayout = (LinearLayout)findViewById(R.id.bc_table);
        cf.save = (Button)findViewById(R.id.save);
        cf.tblrw = (TableRow)findViewById(R.id.row2);
        cf.tblrw.setMinimumHeight(cf.ht);			
		cf.CreateARRTable(31);
		cf.CreateARRTable(32);
		Declarations();	
		RoofWall_setValue();
    }
	private void RoofWall_setValue() {
		try
		{
			Cursor c1=cf.SelectTablefunction(cf.Wind_Questiontbl, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
		   if(c1.getCount()>0)
		   {  
			   c1.moveToFirst();
			   roofwall_vlaue=c1.getInt(c1.getColumnIndex("fld_roofwall"));			  
			   rdiochk= String.valueOf(roofwall_vlaue);
			   roofwallsub_vlaue=c1.getInt(c1.getColumnIndex("fld_roofwallsub"));
			   subvalue=String.valueOf(roofwallsub_vlaue);			   
			   if(!String.valueOf(roofwall_vlaue).equals(""))
			   {
				   cf.setvalueradio(String.valueOf(roofwall_vlaue),rwardion);
				   set_visible1(String.valueOf(roofwall_vlaue));
				   roofclipsminvalue=c1.getInt(c1.getColumnIndex("fld_roofwallclipsminval"));
				   roofclipssingleminvalue=c1.getInt(c1.getColumnIndex("fld_roofwallclipssingminval"));
				   roofclipsdoubleminvalue =c1.getInt(c1.getColumnIndex("fld_roofwallclipsdoubminval"));
				   
				   if(c1.getInt(c1.getColumnIndex("fld_roofwallclipsminval"))==1)
				   {
					   ((CheckBox)findViewById(R.id.chkclipsmin1)).setChecked(true);
					   ((CheckBox)findViewById(R.id.chkclipsmin2)).setChecked(true);
				   }
				   else if(c1.getInt(c1.getColumnIndex("fld_roofwallclipssingminval"))==1)
				   {
					   ((CheckBox)findViewById(R.id.chksinglewrapsmincond1)).setChecked(true);
					   ((CheckBox)findViewById(R.id.chksinglewrapsmincond2)).setChecked(true);
				   }
				   else if(c1.getInt(c1.getColumnIndex("fld_roofwallclipsdoubminval"))==1)
				   { 
					   ((CheckBox)findViewById(R.id.chkdoublewrapmincond1)).setChecked(true);
					   ((CheckBox)findViewById(R.id.chkdoublewrapmincond2)).setChecked(true);
				   }
				   
				   if(roofwallsub_vlaue!=0){   ((RadioButton) toenailrdgrp.findViewWithTag(String.valueOf(roofwallsub_vlaue))).setChecked(true); }
				   if(roofwallsub_vlaue!=0){   ((RadioButton) clipsrdgrp.findViewWithTag(String.valueOf(roofwallsub_vlaue))).setChecked(true);   }
				   if(roofwallsub_vlaue!=0){   ((RadioButton) doubwrapsrdgrp.findViewWithTag(String.valueOf(roofwallsub_vlaue))).setChecked(true);}
				   
				 if(roofwall_vlaue==3){ ((RadioButton) findViewById(R.id.singlewrapsoption1)).setChecked(true); }
				 if(roofwall_vlaue==6)
				 { 
					 ((EditText) findViewById(R.id.txtroofwallother)).setVisibility(cf.v1.VISIBLE);
					 ((EditText) findViewById(R.id.txtroofwallother)).setText(cf.decode(c1.getString(c1.getColumnIndex("fld_rwothertext"))));
					 cf.setFocus(((EditText) findViewById(R.id.txtroofwallother))); 
				 } 
				 else
				 {
					 ((EditText) findViewById(R.id.txtroofwallother)).setVisibility(cf.v1.INVISIBLE);
				 }
				 
				 if(String.valueOf(roofwall_vlaue).equals("0"))
				 {
					 ((TextView)findViewById(R.id.savetxtrw)).setVisibility(cf.v1.GONE);
				 }
				 else
				 {
					 ((TextView)findViewById(R.id.savetxtrw)).setVisibility(cf.v1.VISIBLE);	 
				 }
				 
			   }
			   else
			   {
				   ((TextView)findViewById(R.id.savetxtrw)).setVisibility(cf.v1.GONE);
			   }
		 }
		   else
		   {
			   ((TextView)findViewById(R.id.savetxtrw)).setVisibility(cf.v1.GONE);
		   }
		   
		   Cursor c2=cf.SelectTablefunction(cf.Wind_QuesCommtbl, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
		   if(c2.getCount()>0)
		   {
			 c2.moveToFirst();
			   edrwcomments.setText(cf.decode(c2.getString(c2.getColumnIndex("fld_roofwallcomments"))));
		   }
		   
		}
	    catch (Exception E)
		{
	    	System.out.println("EE dsfdf"+E.getMessage());
			String strerrorlog="Retrieving RoofDeck - WINDMIT";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
	
	private void Declarations() {
		// TODO Auto-generated method stub
		cf.setupUI((ScrollView) findViewById(R.id.scr));
		((TextView)findViewById(R.id.rwheader)).setText(Html
				.fromHtml("What is the WEAKEST roof to wall connection? (Do not include attachment of hip/valley jacks within 5 feet of the inside or outside corner of the roof in determination of WEAKEST type)"));
		
		 for(int i=0;i<rwardion.length;i++)
    	 {
    		try
    		{
    			rwardion[i] = (RadioButton)findViewById(rwid[i]);   			
    		}
			catch(Exception e)
			{
				System.out.println("error= "+e.getMessage());
			}
    	 }
		 for(int i=0;i<4;i++)
    	 {
    		try
    		{
    			ln[i] = (RelativeLayout)findViewById(rwlinid[i]);   			
    		}
			catch(Exception e)
			{
				System.out.println("error= "+e.getMessage());
			}
    	 }
		 toenailrdgrp=(RadioGroup) findViewById(R.id.toenailrdgrp);
		 clipsrdgrp=(RadioGroup) findViewById(R.id.clipsrdgrp);
		 doubwrapsrdgrp=(RadioGroup) findViewById(R.id.doubwrapsrdgrp);
	    
		 
		 edrwcomments=(EditText) findViewById(R.id.rwcomment);
	    edrwcomments.addTextChangedListener(new textwatcher());
	    
	   TextView helptxt = (TextView) findViewById(R.id.help);
		helptxt.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if (rdiochk=="")
				{
					cf.alertcontent = "To see help comments, please select Roof Wall options.";
				}
				else if (rwardion[0].isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection A Toe Nail, Question 4 Roof to Wall Attachment.";
				} else if (rwardion[1].isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection B Clips, Question 4 Roof to Wall Attachment.";
				}  else if (rwardion[2].isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection C single wraps, Question 4 Roof to Wall Attachment.";
				} else if (rwardion[3].isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection D double wraps, Question 4 Roof to Wall Attachment.";
				}  else if (rwardion[4].isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection E, structural connection, Question 4 Roof to Wall Attachment.";
				}  else if (rwardion[5].isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection F, Question 4 Roof to Wall Attachment.";
				}  else if (rwardion[6].isChecked()) {
					cf.alertcontent = "We were unable to verify the weakest form or type of roof wall connection to this home.";
				} else if (rwardion[7].isChecked()) {
					cf.alertcontent = "We were unable to verify the weakest form or type of roof wall connection to this home as a result of no attic access.";
				} else {
					cf.alertcontent = "To see help comments, please select Roof Wall options.";
				}
				cf.showhelp("HELP",cf.alertcontent);

			}
		});
	}
	
	class textwatcher implements TextWatcher
    {
	         public int type;
	         textwatcher()
	    	{
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {
	    			// TODO Auto-generated method stub
	    		cf.showing_limit(s.toString(),(TextView)findViewById(R.id.rw_tv_type1),500); 
	    	}

	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,

	    			int after) {
	    		// TODO Auto-generated method stub
	     	}

	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,

	    			int count) {
	    		// TODO Auto-generated method stub
	    	}
	   }	
	public void set_visible1(String rdiochk) {
		// function for set cisible once the radio button clicked
		for (int i = 0; i <= 3; i++) {
			if ((i + 1) != (Integer.parseInt(rdiochk))) {
				ln[i].setVisibility(View.GONE);
			} else {
				ln[i].setVisibility(View.VISIBLE);
			}
		}
		// function for set cisible once the radio button clicked
	}
	public void set_visible() {
		// function for set cisible once the radio button clicked
		for (int i = 0; i <= 3; i++) {
			if ((i + 1) != (Integer.parseInt(rdiochk))) {
				ln[i].setVisibility(View.GONE);
			} else {
				ln[i].setVisibility(View.VISIBLE);
			}
		}
		// function for set cisible once the radio button clicked
	}
	public void hiderdiooption()
	{
		clipschk=true;doublechk=true;
		 for(int i=0;i<8;i++)
    	 {
    			rwardion[i].setChecked(false);			
    	 }
		 subvalue="0";
		 roofclipsminvalue = 0;
		 roofclipssingleminvalue = 0;
		 roofclipsdoubleminvalue = 0;
		((RadioButton)findViewById(R.id.toenailoption1)).setChecked(false);
		((RadioButton)findViewById(R.id.toenailoption2)).setChecked(false);
		((CheckBox)findViewById(R.id.chkclipsmin1)).setChecked(false);
		((CheckBox)findViewById(R.id.chkclipsmin2)).setChecked(false);
		
		try{if(clipschk){clipsrdgrp.clearCheck();}}catch(Exception e){}
		try{if(doublechk){doubwrapsrdgrp.clearCheck();}}catch(Exception e){}
		
		//((RadioButton)findViewById(R.id.clipsoption1)).setChecked(false);
		//((RadioButton)findViewById(R.id.clipsoption2)).setChecked(false);
		((CheckBox)findViewById(R.id.chksinglewrapsmincond1)).setChecked(false);
		((CheckBox)findViewById(R.id.chksinglewrapsmincond2)).setChecked(false);
		((RadioButton)findViewById(R.id.singlewrapsoption1)).setChecked(false);
		((CheckBox)findViewById(R.id.chkdoublewrapmincond1)).setChecked(false);
		((CheckBox)findViewById(R.id.chkdoublewrapmincond2)).setChecked(false);
		((RadioButton)findViewById(R.id.doublewrapsoption1)).setChecked(false);
		((RadioButton)findViewById(R.id.doublewrapsoption2)).setChecked(false);
		((EditText) findViewById(R.id.txtroofwallother)).setVisibility(cf.v1.INVISIBLE);
		((EditText) findViewById(R.id.txtroofwallother)).setText("");
	}
	public void clicker(View v) {
		switch(v.getId())
		{
		case R.id.loadcomments:
			/***Call for the comments***/
			cf.findinspectionname(cf.identityval);Roofsub_option();
			int len=((EditText)findViewById(R.id.rwcomment)).getText().toString().length();			
			if(!tmp.equals(""))
			{
				/*if(len<500)
				{*/
					if(load_comment)
					{
						load_comment=false;
						int loc1[] = new int[2];
						v.getLocationOnScreen(loc1);
						Intent in = cf.loadComments(tmp,loc1);
						in.putExtra("insp_name", cf.inspname);
						in.putExtra("insp_ques", "Roof Wall");
						in.putExtra("length", len);
						in.putExtra("max_length", 500);
						startActivityForResult(in, cf.loadcomment_code);
					}
				/*}
				else
				{
					cf.ShowToast("You have exceed the maximum length.", 0);
				}*/
			}
			else
			{
				cf.ShowToast("Please select the option for Roof Wall.",0);
			}	
			break;
		case R.id.RWoption1:
			edrwcomments.setText("This home was verified as meeting the requirements of the OIR B1- 1802, Selection A Toe Nail, Question 4 Roof to Wall Attachment.");
			rdiochk="1";hiderdiooption();set_visible();
			rwardion[0].setChecked(true);
			break;
		case R.id.RWoption2:
			edrwcomments.setText("This home was verified as meeting the requirements of the OIR B1- 1802, Selection B Clips, Question 4 Roof to Wall Attachment.");
			rdiochk="2";hiderdiooption();set_visible();
			rwardion[1].setChecked(true);
			break;
		case R.id.RWoption3:
			edrwcomments.setText("This home was verified as meeting the requirements of the OIR B1- 1802, Selection C single wraps, Question 4 Roof to Wall Attachment.");
			rdiochk="3";hiderdiooption();set_visible();
			rwardion[2].setChecked(true);
			break;
		case R.id.RWoption4:
			edrwcomments.setText("This home was verified as meeting the requirements of the OIR B1- 1802, Selection D double wraps, Question 4 Roof to Wall Attachment.");
			rdiochk="4";hiderdiooption();set_visible();
			rwardion[3].setChecked(true);
			break;
		case R.id.RWoption5:
			edrwcomments.setText("This home was verified as meeting the requirements of the OIR B1- 1802, Selection E, structural connection, Question 4 Roof to Wall Attachment.");
			rdiochk="5";hiderdiooption();set_visible();
			rwardion[4].setChecked(true);
			break;
		case R.id.RWoption6:
			edrwcomments.setText("This home was verified as meeting the requirements of the OIR B1- 1802, Selection F, Question 4 Roof to Wall Attachment.");
			hiderdiooption();
			((EditText) findViewById(R.id.txtroofwallother)).setVisibility(cf.v1.VISIBLE);
			cf.setFocus(((EditText) findViewById(R.id.txtroofwallother)));
			rdiochk="6";set_visible();
			rwardion[5].setChecked(true);
			break;
		case R.id.RWoption7:
			edrwcomments.setText("We were unable to verify the weakest form or type of roof wall connection to this home.");
			rdiochk="7";hiderdiooption();set_visible();
			rwardion[6].setChecked(true);
			break;
		case R.id.RWoption8:
			edrwcomments.setText("We were unable to verify the weakest form or type of roof wall connection to this home as a result of no attic access.");
			rdiochk="8";hiderdiooption();set_visible();
			rwardion[7].setChecked(true);
			break;
		case R.id.helpoptA: /** HELP FOR OPTION1 **/
			cf.alerttitle="A - Toe Nail";
			cf.alertcontent="Rafter/truss anchored to top plate of wall using nails driven at an angle through the rafter/truss and attached to the top plate of the wall.";
			cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		case R.id.helpoptB: /** HELP FOR OPTION2 **/
			 cf.alerttitle="B - Clips";
			 cf.alertcontent="Metal attachments on every rafter/truss that are nailed to one side (or both sides in the case of a diamond type clip) of the rafter/truss and attached to the top plate of the wall frame or embedded in the bond beam.";
			 cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;			  
		case R.id.helpoptC: /** HELP FOR OPTION3 **/
			cf.alerttitle="C - Single Wraps";
			  cf.alertcontent="Metal Straps must be secured to every rafter/truss with a minimum of 3 nails, wrapping over and securing to the opposite side of the rafter/truss with a minimum of 1 nail. The Strap must be attached to the top plate of the wall frame or embedded in the bond beam in at least one place.";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		case R.id.helpoptD:
			 cf.alerttitle="D - Double Wraps";
			  cf.alertcontent="Both Metal Straps must be secured to every rafter/truss with a minimum of 3 nails, wrapping over and securing to the opposite side of the rafter/truss with a minimum of 1 nail. Each Strap must be attached to the top plate of the wall frame or embedded in the bond beam in at least one place.";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;			
		case R.id.helpoptE:
			cf.alerttitle="E - Structural";
			  cf.alertcontent="Anchor bolts, structurally connected or reinforced concrete roof.";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;			
		case R.id.helpoptF:
			 cf.alerttitle="F - other";
			 cf.alertcontent="Other.";
			 cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;			
		case R.id.helpoptG:
			 cf.alerttitle="G - Unknown";
			 cf.alertcontent="Unknown or unidentified.";
			 cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		case R.id.helpoptH:
			cf.alerttitle="H - No Attic";
			cf.alertcontent="No Attic Access";
			cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		case R.id.hme:
    		cf.gohome();
    		break;
		
		case R.id.save:
			System.out.println("save="+rdiochk);
			if (rdiochk.equals("1") || rdiochk.equals("2") || rdiochk.equals("3") ||  rdiochk.equals("4"))
			{
		
				System.out.println("rdiochk="+rdiochk);
		switch (Integer.parseInt(rdiochk)) {
		case 1:
			System.out.println("111111="+rdiochk);
			toerdgpID=toenailrdgrp.getCheckedRadioButtonId();System.out.println("toerdgpID="+toerdgpID);
			subvalue = (toerdgpID==-1) ? "":((RadioButton) findViewById(toerdgpID)).getTag().toString();System.out.println("subvalue="+subvalue);
			if (((RadioButton)findViewById(R.id.toenailoption1)).isChecked()
					|| ((RadioButton)findViewById(R.id.toenailoption2)).isChecked()) {
				chk_status = true;

			} else {
				chk_status = false;
				showstr = "Please select option for Toenail";
			}
			break;
		case 2:
			clipsrdgrpID=clipsrdgrp.getCheckedRadioButtonId();
			subvalue = (clipsrdgrpID==-1) ? "":((RadioButton) findViewById(clipsrdgrpID)).getTag().toString();
			if (((CheckBox)findViewById(R.id.chkclipsmin1)).isChecked() && ((CheckBox)findViewById(R.id.chkclipsmin2)).isChecked()
					&& (((RadioButton)findViewById(R.id.clipsoption1)).isChecked() || ((RadioButton)findViewById(R.id.clipsoption2)).isChecked())) 
			{			
				chk_status = true;
				roofclipsminvalue = 1;
				roofclipssingleminvalue = 0;
				roofclipsdoubleminvalue = 0;
			} else if (((CheckBox)findViewById(R.id.chkclipsmin1)).isChecked() ^ ((CheckBox)findViewById(R.id.chkclipsmin2)).isChecked()) {
				chk_status = false;
				showstr = "Please select minimum option for Clips";

			} else if(((RadioButton)findViewById(R.id.clipsoption1)).isChecked()==false
					|| ((RadioButton)findViewById(R.id.clipsoption2)).isChecked()==false) 
			{
				chk_status = false;
				showstr = "Please select option for Clips";
			} else {
				chk_status = false;
				showstr = "Please select minimum option for Single Wraps  ";
			}

			break;
		case 3:			
			subvalue="1";
			if (((CheckBox)findViewById(R.id.chksinglewrapsmincond1)).isChecked() && ((CheckBox)findViewById(R.id.chksinglewrapsmincond2)).isChecked()
					&& (((RadioButton)findViewById(R.id.singlewrapsoption1)).isChecked())) 
			{
				chk_status = true;
				roofclipsminvalue = 0;
				roofclipssingleminvalue = 1;
				roofclipsdoubleminvalue = 0;
			}
		    else if (((CheckBox)findViewById(R.id.chksinglewrapsmincond1)).isChecked() ^ ((CheckBox)findViewById(R.id.chksinglewrapsmincond2)).isChecked())
			{
				chk_status = false;
				showstr = "Please select minimum option for Single Wraps";

			} else if (!((RadioButton)findViewById(R.id.singlewrapsoption1)).isChecked()) {
				chk_status = false;
				showstr = "Please select option for Single Wraps";
			} else {
				chk_status = false;
				showstr = "Please select minimum option for Single Wraps  ";
			}

			break;
		case 4:
			doubwrapsrdgrpID=doubwrapsrdgrp.getCheckedRadioButtonId();
			subvalue = (doubwrapsrdgrpID==-1) ? "":((RadioButton) findViewById(doubwrapsrdgrpID)).getTag().toString();
			
			if (((CheckBox)findViewById(R.id.chkdoublewrapmincond1)).isChecked() && ((CheckBox)findViewById(R.id.chkdoublewrapmincond2)).isChecked()
					&& (((RadioButton)findViewById(R.id.doublewrapsoption1)).isChecked()) || (((RadioButton)findViewById(R.id.doublewrapsoption2)).isChecked())) 
			{
				chk_status = true;
				roofclipsminvalue = 0;
				roofclipssingleminvalue = 0;
				roofclipsdoubleminvalue = 1;
			} else if (((CheckBox)findViewById(R.id.chkdoublewrapmincond1)).isChecked()
					^ ((CheckBox)findViewById(R.id.chkdoublewrapmincond2)).isChecked()) {
				chk_status = false;
				showstr = "Please select minimum option for Double Wraps";

			} else if (!(((RadioButton)findViewById(R.id.doublewrapsoption1)).isChecked() || ((RadioButton)findViewById(R.id.doublewrapsoption2)).isChecked())) {
				chk_status = false;
				showstr = "Please select option for Double Wraps";
			} else {
				chk_status = false;
				showstr = "Please select minimum option for Double Wraps";
			}

			break;
		
		}
	}System.out.println("rwardion"+rdiochk);
			if (rwardion[0].isChecked() || rwardion[1].isChecked() || rwardion[2].isChecked() || rwardion[3].isChecked() || rwardion[4].isChecked()
					|| rwardion[5].isChecked() || rwardion[6].isChecked() || rwardion[7].isChecked()) {
				System.out.println("oneisscc"+rdiochk);
				if (rwardion[5].isChecked()) {
					System.out.println("5ixchecked"+rdiochk);
					
					roofwallothertxt =((EditText) findViewById(R.id.txtroofwallother)).getText().toString().trim();
					if (roofwallothertxt.equals("")) {
						chk_status = false;
						showstr = "Please enter the text for Other Roof Wall.";
						cf.setFocus(((EditText) findViewById(R.id.txtroofwallother)));
					} else {
						chk_status = true;
					}
				}
				if (!chk_status) {
					System.out.println("chksss");
					cf.ShowToast(showstr,0);
					showstr = "";
					chk_status = true;
				}
				else if (edrwcomments.getText().toString().trim().equals("")) {
					cf.ShowToast("Please enter the Comments for Roof Wall.",0);
					cf.setFocus(edrwcomments);
				}
				else {
					insertdata();
					if(updatecnt.equals("1"))
					{
						cf.ShowToast("Roof Wall Details saved successfully.",0);
						cf.gotoclass(cf.identityval, WindMitRoofGeometry.class);
					}
				}
			}
			else
			{
				cf.ShowToast("Please select the option for Roof Wall",0);
			}
			break;
		}
	}
	private void Roofsub_option() {
		// TODO Auto-generated method stub
		 for(int i=0;i<8;i++)
    	 {
			 if(rwardion[i].isChecked())
			 {
				tmp = b1_q4[i];
			 }
    	 }
	}
	public String getslected_radio(RadioButton[] R) {
		// TODO Auto-generated method stub
		for(int i=0;i<R.length;i++)
		{
			if(R[i].isChecked())
			{
					s= R[i].getTag().toString();				
			}
		}
		
		return s;
	}
	private void insertdata() {
		// TODO Auto-generated method stub	
	try
	{
		Cursor c1=cf.SelectTablefunction(cf.Wind_Questiontbl, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
			 if(c1.getCount()==0)
			 {				 
				 cf.arr_db.execSQL("INSERT INTO "
							+ cf.Wind_Questiontbl
							+ " (fld_srid,insp_typeid,fld_bcyearbuilt,fld_bcpermitappdate,fld_buildcode,fld_roofdeck,fld_rdothertext," +
							"	fld_roofwall,fld_roofwallsub,fld_roofwallclipsminval,fld_roofwallclipssingminval,fld_roofwallclipsdoubminval,fld_rwothertext," +
							"	fld_roofgeometry ,fld_geomlength ,fld_roofgeomtotalarea ,fld_roofgeomothertext ,fld_swr,fld_optype,fld_openprotect,fld_opsubvalue,fld_oplevelchart," +
							"   fld_opGOwindentdoorval,fld_opGOgardoorval,fld_opGOskylightsval,fld_opGOglassblockval,fld_opNGOentrydoorval,fld_opNGOgaragedoorval, " +
							"	fld_wcvalue,fld_wcreinforcement,fld_quesmodifydate)"							
							+ "VALUES ('" + cf.selectedhomeid + "','"+cf.identityval+"','','','','" + 0 + "','','" + 
							rdiochk + "','" + subvalue + "','" + roofclipsminvalue + "','" + roofclipssingleminvalue + "','" + roofclipsdoubleminvalue+ "','"+cf.encode(((EditText) findViewById(R.id.txtroofwallother)).getText().toString().trim())+"','"+
							+ 0 + "','" + 0 + "','" + 0 + "','','" + 0 + "','"+ 0 +"','"+ 0 + "','" + 0 + "','" + 0 + "','" +
							+ 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','"+ 0 +"','" +
							+ 0 + "','" + 0 + "','"+ cf.datewithtime + "')");			 
			 }
			 else
			 {
				 cf.arr_db.execSQL("UPDATE " + cf.Wind_Questiontbl
							+ " SET fld_roofwall ='" + rdiochk
							+ "',fld_roofwallsub='" + subvalue
							+ "',fld_roofwallclipsminval='"
							+ roofclipsminvalue
							+ "',fld_roofwallclipssingminval='"
							+ roofclipssingleminvalue
							+ "',fld_roofwallclipsdoubminval='"
							+ roofclipsdoubleminvalue
							+ "',fld_rwothertext='"
							+ cf.encode(((EditText) findViewById(R.id.txtroofwallother)).getText().toString().trim()) + "'"
							+ " WHERE fld_srid ='" + cf.selectedhomeid
							+ "' and insp_typeid='"+cf.identityval+"'");
			 }
			 c1.close();
			 	Cursor c2 = cf.SelectTablefunction(cf.Wind_QuesCommtbl, "where fld_srid='"+ cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'");
				if (c2.getCount() == 0) 
				{
					cf.arr_db.execSQL("INSERT INTO "
							+ cf.Wind_QuesCommtbl
							+ " (fld_srid,insp_typeid,fld_buildcodecomments,fld_roofcovercomments,fld_roofdeckcomments,fld_roofwallcomments," +
							"fld_roofgeometrycomments,fld_swrcomments,fld_openprotectioncomments,fld_wallconscomments,fld_insoverallcomments )"
							+ "VALUES('"+cf.selectedhomeid+"','"+cf.identityval+"','','','','"+cf.encode(edrwcomments.getText().toString())+"','','','','','')");
				}
				else
				{
					cf.arr_db.execSQL("UPDATE " + cf.Wind_QuesCommtbl
							+ " SET fld_roofwallcomments='"+cf.encode(edrwcomments.getText().toString())+ "' WHERE fld_srid ='" + cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'");
				}
				updatecnt="1";
		 }
		 catch (Exception E)
		 {
			 updatecnt="0";
			 System.out.println("EE "+E.getMessage());
			String strerrorlog="Updating Questions Roof Wall - WIND MIT";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		 }
	}
	 public boolean onKeyDown(int keyCode, KeyEvent event) {
	 		// replaces the default 'Back' button action
	 		if (keyCode == KeyEvent.KEYCODE_BACK) {
	 			cf.gotoclass(cf.identityval, WindMitRoofDeck.class);
	 			return true;
	 		}
	 		return super.onKeyDown(keyCode, event);
	 	}
	 /**on activity result for the load comments**/
	 @Override
	 		protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	 			try
	 			{
	 				if(requestCode==cf.loadcomment_code)
	 				{
	 					load_comment=true;
	 					if(resultCode==RESULT_OK)
	 					{
	 						String rwcomm = edrwcomments.getText().toString();	 
	 						edrwcomments.setText(rwcomm +" "+data.getExtras().getString("Comments"));
	 					}
	 				}
	 			}
	 			catch (Exception e) {
	 			// TODO: handle exception
	 				System.out.println("the erro was "+e.getMessage());
	 			}
	 	}/**on activity result for the load comments ends**/
}
