/*
************************************************************
* Project: ALL RISK REPORT
* Module : WindMitRoofGeometry.java
* Creation History:
Created By : Nalini on 10/24/2012
* Modification History:
Last Modified By : Gowri on 11/28/2012
************************************************************ 
*/
package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.WindMitBuildCode.textwatcher;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;

public class WindMitRoofGeometry extends Activity{
	CommonFunctions cf;
	String b1_q5[] = {"Hip Roof", "Flat Roof","Other Roof Type" };
	String rdiochkvalue="",updatecnt="";
	EditText rgcomment;
	int rdvalue;
	boolean load_comment=true;
	String tmp="",totallength="",totalroofarea="",edoptionCothertetxva1="",edroofgeometryother="";
	RadioButton rdiooptionA,rdiooptionB,rdiooptionC;

	 public void onCreate(Bundle savedInstanceState) 
	 {
        super.onCreate(savedInstanceState);
		cf=new CommonFunctions(this);	
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			cf.getExtras(extras);				
	 	}
		setContentView(R.layout.windmitroofgeometry);
        cf.getDeviceDimensions();
        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
        if(cf.identityval==31)
        {
          hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","B1 1802(Rev.01/12)","Roof Geometry",3,1,cf));
        }
        else if(cf.identityval==32)
        { 
        	hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","Commercial Type I","Roof Geometry",3,1,cf));
        }
		LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
     	main_layout.setMinimumWidth(cf.wd);
     	main_layout.addView(new MyOnclickListener(getApplicationContext(), 3, 0,0,cf));
       
        cf.type_submenulayout =(LinearLayout) findViewById(R.id.typesubmenu); //** WINDMIT TYPE SUBMENU **//
        cf.type_submenulayout.addView(new MyOnclickListener(this, 300, 1,0, cf));
        cf.windmittypechk(cf.identityval,((RadioButton)findViewById(R.id.b1802_chk)),((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((TextView)findViewById(R.id.commtxt)),((ImageView)findViewById(R.id.editbno))); // To check the checkbox of choosen inspection type
        cf.uncheckcommercial(((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((RadioButton)findViewById(R.id.b1802_chk))); 
    	         
        cf.submenu_layout = (LinearLayout) findViewById(R.id.submenu);//** WINDMIT TYPE SUBMENU **//	        
        cf.submenu_layout .addView(new MyOnclickListener(this,cf.identityval,1,305, cf));
        
        cf.mainlayout = (LinearLayout)findViewById(R.id.bc_table);
        cf.save = (Button)findViewById(R.id.save);
        cf.tblrw = (TableRow)findViewById(R.id.row2);
        cf.tblrw.setMinimumHeight(cf.ht);			
		cf.CreateARRTable(31);
		cf.CreateARRTable(32);
		Declarations();			
		RoofGeometry_setValue();
			
	 }
	private void RoofGeometry_setValue() {
		// TODO Auto-generated method stub
		showhideoption();
		try{ 
		Cursor c1=cf.SelectTablefunction(cf.Wind_Questiontbl, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
		   if(c1.getCount()>0)
		   {  
			   c1.moveToFirst();
			   rdvalue=c1.getInt(c1.getColumnIndex("fld_roofgeometry"));
			   if(rdvalue==1 || rdvalue==2 || rdvalue==3)
			   {
					if (rdvalue==1) {
						
						rdiooptionA.setChecked(true);
						((EditText) findViewById(R.id.edoptioA1)).setEnabled(true);
						((EditText) findViewById(R.id.edoptioA2)).setEnabled(true);
						((EditText) findViewById(R.id.edoptioA1)).setText(c1.getString(c1.getColumnIndex("fld_geomlength")));
						((EditText) findViewById(R.id.edoptioA2)).setText(c1.getString(c1.getColumnIndex("fld_roofgeomtotalarea")));					
						rdiochkvalue="1";
					} 
					else if (rdvalue==2) {
						rdiooptionB.setChecked(true);
						((EditText) findViewById(R.id.edoptioB1)).setEnabled(true);
						((EditText) findViewById(R.id.edoptioB2)).setEnabled(true);
						((EditText) findViewById(R.id.edoptioB1)).setText(c1.getString(c1.getColumnIndex("fld_geomlength")));
						((EditText) findViewById(R.id.edoptioB2)).setText(c1.getString(c1.getColumnIndex("fld_roofgeomtotalarea")));					
						rdiochkvalue="2";
					} 
					else if (rdvalue==3) {
						rdiooptionC.setChecked(true);
						((EditText) findViewById(R.id.edoptioC1)).setEnabled(true);
						((EditText) findViewById(R.id.edoptioC2)).setEnabled(true);
						((EditText) findViewById(R.id.edoptioC1)).setText(c1.getString(c1.getColumnIndex("fld_geomlength")));
						((EditText) findViewById(R.id.edoptioC2)).setText(c1.getString(c1.getColumnIndex("fld_roofgeomtotalarea")));					
						((EditText) findViewById(R.id.edoptioCothertext)).setText(cf.decode(c1.getString(c1.getColumnIndex("fld_roofgeomothertext"))));
						((EditText) findViewById(R.id.edoptioCothertext)).setEnabled(true);
						rdiochkvalue="3";
					} 
					 ((TextView) findViewById(R.id.savetxtrg)).setVisibility(cf.v1.VISIBLE);
			   }
			   else
			   {
				   ((TextView) findViewById(R.id.savetxtrg)).setVisibility(cf.v1.GONE);
			   }				
		   }
		   else
		   {
			   ((TextView) findViewById(R.id.savetxtrg)).setVisibility(cf.v1.GONE);
		   }
		   Cursor c2=cf.SelectTablefunction(cf.Wind_QuesCommtbl, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
		   if(c2.getCount()>0)
		   {
			  c2.moveToFirst();
			  ((EditText) findViewById(R.id.rgcomment)).setText(cf.decode(c2.getString(c2.getColumnIndex("fld_roofgeometrycomments"))));
		   }
		   
		}
	    catch (Exception E)
		{
	    	String strerrorlog="Retrieving RoofDeck - WINDMIT";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
	private void Declarations() {
		// TODO Auto-generated method stub
		cf.setupUI((ScrollView) findViewById(R.id.scr));
		((TextView)findViewById(R.id.rg_desc)).setText(Html
				.fromHtml("What is the roof shape? (Do not consider roofs of porches or carports that are attached only to the fascia or wall of the host structure over unenclosed space in the determination of roof perimeter or roof area for roof geometry classification). "));

		rdiooptionA = (RadioButton) this.findViewById(R.id.RGoption1);rdiooptionA.setOnClickListener(OnClickListener);
		rdiooptionB = (RadioButton) this.findViewById(R.id.RGoption2);rdiooptionB.setOnClickListener(OnClickListener);
		rdiooptionC = (RadioButton) this.findViewById(R.id.RGoption3);rdiooptionC.setOnClickListener(OnClickListener);
		rgcomment = ((EditText)findViewById(R.id.rgcomment));
		rgcomment.addTextChangedListener(new textwatcher(1));
		/*
		((EditText)findViewById(R.id.edoptioA1)).addTextChangedListener(new textwatcher(2));
		((EditText)findViewById(R.id.edoptioA2)).addTextChangedListener(new textwatcher(3));
		((EditText)findViewById(R.id.edoptioB1)).addTextChangedListener(new textwatcher(4));
		((EditText)findViewById(R.id.edoptioB2)).addTextChangedListener(new textwatcher(5));
		((EditText)findViewById(R.id.edoptioC1)).addTextChangedListener(new textwatcher(6));
		((EditText)findViewById(R.id.edoptioC2)).addTextChangedListener(new textwatcher(7));*/
		
		TextView helptxt = (TextView) findViewById(R.id.help);
		helptxt.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if (rdiochkvalue=="")
				 {
					cf.alertcontent = "To see help comments, please select roof geometry options.";
				}
				else if (rdiochkvalue.equals("1") || rdiooptionA.isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of selection A, Hip Roof of the OIR B1 -1802 Question 5 Roof Shape";
				}else if (rdiochkvalue.equals("2")  || rdiooptionB.isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of selection B, Flat Roof of the OIR B1 -1802 Question 5 Roof Shape.";
				}else if (rdiochkvalue.equals("3")  || rdiooptionC.isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of selection C, Non Hip, of the OIR B1 -1802 Question 5 Roof Shape.";
				} else {
					cf.alertcontent = "To see help comments, please select roof geometry options.";
				}

				
				cf.showhelp("HELP",cf.alertcontent);
			}
		});
	}
	class textwatcher implements TextWatcher
    {
	         public int type;
	         textwatcher(int type)

	    	{
	    		this.type=type;
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {

	    		if(this.type==1)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.rg_tv_type1),500); 
	    		}
	    		/*else if(this.type==2)
	    		{
	    			 if(!s.toString().equals("")){
	    			     if(Character.toString(s.toString().charAt(0)).equals("0"))
		    			 {
		    				 cf.ShowToast("Invalid Entry! Please enter the valid Total length for Hip Roof.", 0);
		    				 ((EditText)findViewById(R.id.edoptioA1)).setText("");cf.hidekeyboard();
		    			 }
	    			 } 
	    		}
	    		else if(this.type==3)
	    		{
	    			 if(!s.toString().equals("")){
	    			     if(Character.toString(s.toString().charAt(0)).equals("0"))
		    			 {
		    				 cf.ShowToast("Invalid Entry! Please enter the valid Total roof system perimeter for Hip Roof.", 0);
		    				 ((EditText)findViewById(R.id.edoptioA2)).setText("");cf.hidekeyboard();
		    			 }
	    			 } 
	    		}
	    		else if(this.type==4)
	    		{
	    			 if(!s.toString().equals("")){
	    			     if(Character.toString(s.toString().charAt(0)).equals("0"))
		    			 {
		    				 cf.ShowToast("Invalid Entry! Please enter the valid Roof area for Flat Roof.", 0);
		    				 ((EditText)findViewById(R.id.edoptioB1)).setText("");cf.hidekeyboard();
		    			 }
	    			 } 
	    		}
	    		else if(this.type==5)
	    		{
	    			 if(!s.toString().equals("")){
	    			     if(Character.toString(s.toString().charAt(0)).equals("0"))
		    			 {
	    			    	 cf.ShowToast("Invalid Entry! Please enter the valid Total roof area for Flat Roof.", 0);
		    				 ((EditText)findViewById(R.id.edoptioB2)).setText("");cf.hidekeyboard();
		    			 }
	    			 } 
	    		}
	    		else if(this.type==6)
	    		{
	    			 if(!s.toString().equals("")){
	    			     if(Character.toString(s.toString().charAt(0)).equals("0"))
		    			 {
	    			    	 cf.ShowToast("Invalid Entry! Please enter the valid Total length for Other roof.", 0);
		    				 ((EditText)findViewById(R.id.edoptioC1)).setText("");cf.hidekeyboard();
		    			 }
	    			 } 
	    		}
	    		else if(this.type==7)
	    		{
	    			 if(!s.toString().equals("")){
	    			     if(Character.toString(s.toString().charAt(0)).equals("0"))
		    			 {
	    			    	 cf.ShowToast("Invalid Entry! Please enter the valid Total roof system perimeter for Other Roof.", 0);
		    				 ((EditText)findViewById(R.id.edoptioC2)).setText("");cf.hidekeyboard();
		    			 }
	    			 } 
	    		}*/
	    	}

	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,

	    			int after) {
	    		// TODO Auto-generated method stub
	     	}

	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,

	    			int count) {
	    		// TODO Auto-generated method stub
	    	}
	   }	
	RadioButton.OnClickListener OnClickListener = new RadioButton.OnClickListener() {
		public void onClick(View v) {
			try {
				switch (v.getId()) {
				case R.id.RGoption1:
					rdiochkvalue="1";
					rgcomment.setText("This home was verified as meeting the requirements of selection A, �Hip Roof� of the OIR B1 -1802 Question 5 Roof Shape.");
					showhideoption();
					rdiooptionA.setChecked(true);
					((EditText) findViewById(R.id.edoptioA1)).setEnabled(true);((EditText) findViewById(R.id.edoptioA1)).requestFocus();
					((EditText) findViewById(R.id.edoptioA2)).setEnabled(true);((EditText) findViewById(R.id.edoptioA2)).requestFocus();
					break;

				case R.id.RGoption2:
					rdiochkvalue="2";
					rgcomment.setText("This home was verified as meeting the requirements of selection B, �Flat Roof� of the OIR B1 -1802 Question 5 Roof Shape.");
					showhideoption();
					rdiooptionB.setChecked(true);
					((EditText) findViewById(R.id.edoptioB1)).setEnabled(true);((EditText) findViewById(R.id.edoptioB1)).requestFocus();
					((EditText) findViewById(R.id.edoptioB2)).setEnabled(true);((EditText) findViewById(R.id.edoptioB2)).requestFocus();
					break;

				case R.id.RGoption3:
					rdiochkvalue="3";
					rgcomment.setText("This home was verified as meeting the requirements of selection C, Non Hip, of the OIR B1 -1802 Question 5 Roof Shape.");
					showhideoption();
					rdiooptionC.setChecked(true);
					((EditText) findViewById(R.id.edoptioC1)).setEnabled(true);((EditText) findViewById(R.id.edoptioC1)).requestFocus();
					((EditText) findViewById(R.id.edoptioC2)).setEnabled(true);((EditText) findViewById(R.id.edoptioC2)).requestFocus();
					((EditText) findViewById(R.id.edoptioCothertext)).setEnabled(true);
					((EditText) findViewById(R.id.edoptioCothertext)).requestFocus();
					((EditText) findViewById(R.id.edoptioCothertext)).setFocusableInTouchMode(true);
					break;
				}
			} catch (Exception e) {
				System.out.println("erro in on click " + e);
			}
		}
	};
	public void showhideoption() /** TO HIDE CONTROLS WHEN CLICKED ON RADIO OPTION**/
	{
		rdiooptionA.setChecked(false);
		rdiooptionB.setChecked(false);
		rdiooptionC.setChecked(false);
		((EditText) findViewById(R.id.edoptioA1)).setEnabled(false);((EditText) findViewById(R.id.edoptioA1)).setText("");
		((EditText) findViewById(R.id.edoptioA2)).setEnabled(false);((EditText) findViewById(R.id.edoptioA2)).setText("");
		((EditText) findViewById(R.id.edoptioB1)).setEnabled(false);((EditText) findViewById(R.id.edoptioB1)).setText("");
		((EditText) findViewById(R.id.edoptioB2)).setEnabled(false);((EditText) findViewById(R.id.edoptioB2)).setText("");
		((EditText) findViewById(R.id.edoptioC1)).setEnabled(false);((EditText) findViewById(R.id.edoptioC1)).setText("");
		((EditText) findViewById(R.id.edoptioC2)).setEnabled(false);((EditText) findViewById(R.id.edoptioC2)).setText("");
		((EditText) findViewById(R.id.edoptioCothertext)).setEnabled(false);((EditText) findViewById(R.id.edoptioCothertext)).setText("");
	}
	public void clicker(View v) {
		switch(v.getId())
		{
		case R.id.loadcomments:
			/***Call for the comments***/
			cf.findinspectionname(cf.identityval);RG_suboption();
			int len=((EditText)findViewById(R.id.rgcomment)).getText().toString().length();			
			if(!tmp.equals(""))
			{
				if(load_comment)
				{
						load_comment=false;
						int loc1[] = new int[2];
						v.getLocationOnScreen(loc1);
						Intent in = cf.loadComments(tmp,loc1);
						in.putExtra("insp_name", cf.inspname);
						in.putExtra("insp_ques", "Roof Geometry");
						in.putExtra("length", len);
						in.putExtra("max_length", 500);
						startActivityForResult(in, cf.loadcomment_code);
				}
			}
			else
			{
				cf.ShowToast("Please select the option for Roof Geometry.",0);
			}	
			break;
		case R.id.helpoptA: /** HELP FOR OPTION1 **/
			cf.alerttitle="A - Hip Roof";
		    cf.alertcontent="Hip roof: hip roof with no other roof shapes greater than 10 percent of the total roof system perimeter Total length of non-hip features: ____________ feet; total roof system perimeter: ____________ feet.";
		    cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		case R.id.helpoptB: /** HELP FOR OPTION2 **/
			cf.alerttitle="B - Flat Roof";
		    cf.alertcontent="Flat roof: roof on a building with five or more units where at least 90 percent of the main roof area has a roof slope of less than 2:12. Roof area with slope less than 2:12 ____________ sq. ft.; total roof area ____________ sq. ft.";
		    cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;			  
		case R.id.helpoptC: /** HELP FOR OPTION3 **/
			cf.alerttitle="C - Other Roof Type";
		    cf.alertcontent="Other roof: any roof that does not qualify as either A or B above.";
		    cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		case R.id.hme:
    		cf.gohome();
    		break;
		
		case R.id.save:
			int optionval=0;
			boolean chkstatus=true;			
			if(rdiooptionA.isChecked() || rdiooptionB.isChecked() || rdiooptionC.isChecked())
			{
				if (rdiochkvalue.equals("1") || rdiooptionA.isChecked()) 
				{
					totallength = ((EditText) findViewById(R.id.edoptioA1)).getText().toString();
					totalroofarea =((EditText) findViewById(R.id.edoptioA2)).getText().toString();
					optionval = 1;
				} 
				else if (rdiochkvalue.equals("2")  || rdiooptionB.isChecked()) 
				{
					totallength = ((EditText) findViewById(R.id.edoptioB1)).getText().toString();
					totalroofarea =((EditText) findViewById(R.id.edoptioB2)).getText().toString();
					optionval = 2;
				}
				else if (rdiochkvalue.equals("3")  || rdiooptionC.isChecked()) 
				{
					totallength = ((EditText) findViewById(R.id.edoptioC1)).getText().toString();
					totalroofarea =((EditText) findViewById(R.id.edoptioC2)).getText().toString();
					optionval = 3;			
				}						
				
				if (rdiochkvalue.equals("1") || rdiooptionA.isChecked()) 
				{
					if (totallength.trim().equals(""))
					{
						cf.ShowToast("Please enter the Total Length of non-hip features.\n"+"Condition : Total non-hip should not be greater than 10% of Total roof system perimeter.",0);						
						cf.setFocus(((EditText) findViewById(R.id.edoptioA1)));
					}
					else if (validation(totallength, totalroofarea, optionval) != "true") 
					{
							cf.ShowToast("Total non-hip should not be greater than 10% of Total roof system perimeter.",0);
							cf.setFocus(((EditText) findViewById(R.id.edoptioA1)));						 
					}
					else
					{
						insertroofgeometry();
					}					
				}
				else if (rdiochkvalue.equals("2") || rdiooptionB.isChecked()) 
				{
					if (totallength.trim().equals(""))
					{
						cf.ShowToast("Please enter Roof Area for Flat Roof. \n"+"Please enter roof area with slope 2:12 or should be less than 90% of Total sq.ft.",0);		
						cf.setFocus(((EditText) findViewById(R.id.edoptioB1)));	
					}
					else if (totalroofarea.trim().equals(""))
					{
						cf.ShowToast("Please enter Total roof area for Flat Roof.",0);		
						cf.setFocus(((EditText) findViewById(R.id.edoptioB2)));	
					}
					else if (validation(totallength, totalroofarea, optionval) != "true") 
					{
						//cf.ShowToast("Please enter roof area with slope 2:12 or should be less than 90% of Total sq.ft.",0);
						cf.ShowToast("Please Enter roof area with slope 2:12 or less should be less than 99% of total SF", 0);
						cf.setFocus(((EditText) findViewById(R.id.edoptioB1)));
					}
					else
					{
						insertroofgeometry();
					}
				}
				else if (rdiochkvalue.equals("3") || rdiooptionC.isChecked()) 
				{
					if(((EditText) findViewById(R.id.edoptioCothertext)).getText().toString().trim().equals(""))
					{
						cf.ShowToast("Please enter the other text for Other Roof.",0);
						((EditText)findViewById(R.id.edoptioCothertext)).setEnabled(true);
						cf.setFocus(((EditText) findViewById(R.id.edoptioCothertext)));	
					}
					else
					{
						insertroofgeometry();
					}
				}
			}
			else
			{
				cf.ShowToast("Please select the Roof Geometry Option.",0);
			}				
			break;
		}
	}
	private void insertroofgeometry()
	{
		if (((EditText) findViewById(R.id.rgcomment)).getText().toString().trim().equals("")) 
		{
			cf.ShowToast("Please enter the Comments for Roof Geometry.",0);
			cf.setFocus(((EditText) findViewById(R.id.rgcomment)));
		}		
		else
		{
			try {
				 Cursor c1=cf.SelectTablefunction(cf.Wind_Questiontbl, " where fld_srid='"+cf.selectedhomeid+"'  and insp_typeid='"+cf.identityval+"'");
				 if(c1.getCount()==0)
				 	{
						 cf.arr_db.execSQL("INSERT INTO "
									+ cf.Wind_Questiontbl
									+ " (fld_srid,insp_typeid,fld_bcyearbuilt,fld_bcpermitappdate,fld_buildcode,fld_roofdeck,fld_rdothertext," +
									"	fld_roofwall,fld_roofwallsub,fld_roofwallclipsminval,fld_roofwallclipssingminval,fld_roofwallclipsdoubminval,fld_rwothertext," +
									"	fld_roofgeometry ,fld_geomlength ,fld_roofgeomtotalarea ,fld_roofgeomothertext ,fld_swr,fld_optype,fld_openprotect,fld_opsubvalue,fld_oplevelchart," +
									"   fld_opGOwindentdoorval,fld_opGOgardoorval,fld_opGOskylightsval,fld_opGOglassblockval,fld_opNGOentrydoorval,fld_opNGOgaragedoorval, " +
									"	fld_wcvalue,fld_wcreinforcement,fld_quesmodifydate)"
								
								+ "VALUES ('" + cf.selectedhomeid + "','"+cf.identityval+"','','','','" + 0 + "','','" +
								+ 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0	+ "','','"+
								 rdiochkvalue + "','" + totallength + "','" + totalroofarea + "','"+cf.encode(((EditText) findViewById(R.id.edoptioCothertext)).getText().toString())+"','" + 0 + "','"+ 0 +"','"+ 0 + "','" + 0 + "','" + 0 + "','" +
								+ 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','"+ 0 +"','" +
								+ 0 + "','" + 0 + "','"+ cf.datewithtime + "')");			
				 		
					 } 
					 else 
					 {
						 cf.arr_db.execSQL("UPDATE " + cf.Wind_Questiontbl
									+ " SET fld_roofgeometry='" + rdiochkvalue
									+ "',fld_geomlength ='" + totallength
									+ "',fld_roofgeomtotalarea='" + totalroofarea
									+ "',fld_roofgeomothertext='"
									+ cf.encode(((EditText) findViewById(R.id.edoptioCothertext)).getText().toString()) + "'"
									+ " WHERE fld_srid ='" + cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'");
					 }
				 	c1.close();
				 	Cursor c2 = cf.SelectTablefunction(cf.Wind_QuesCommtbl, "where fld_srid='"+ cf.selectedhomeid + "'  and insp_typeid='"+cf.identityval+"'");
				 	if (c2.getCount() == 0) 
					{
				 		cf.arr_db.execSQL("INSERT INTO "
								+ cf.Wind_QuesCommtbl
								+ " (fld_srid,insp_typeid,fld_buildcodecomments,fld_roofcovercomments,fld_roofdeckcomments,fld_roofwallcomments," +
								"fld_roofgeometrycomments,fld_swrcomments,fld_openprotectioncomments,fld_wallconscomments,fld_insoverallcomments )"
								+ "VALUES('"+cf.selectedhomeid+"','"+cf.identityval+"','','','','','"+cf.encode(rgcomment.getText().toString())+"','','','','')");
					}
					else
					{
						cf.arr_db.execSQL("UPDATE " + cf.Wind_QuesCommtbl
								+ " SET fld_roofgeometrycomments='"+cf.encode(rgcomment.getText().toString())+ "' WHERE fld_srid ='" + cf.selectedhomeid + "'  and insp_typeid='"+cf.identityval+"'");
					}
				 	cf.ShowToast("Roof Geometry saved successfully.",0);
					cf.gotoclass(cf.identityval, WindMitSWR.class);
			}catch (Exception E)
			 {
				String strerrorlog="Updating Questions RoofGeometry - WindMit";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			 }
		}
	}
	private void RG_suboption() {
		// TODO Auto-generated method stub
		if(rdiooptionA.isChecked()){tmp=b1_q5[0];}
		else if(rdiooptionB.isChecked()){tmp=b1_q5[1];}
		else if(rdiooptionC.isChecked()){tmp=b1_q5[2];}		
	}
	private String validation(String totallen, String totalroof, int option) {
	//	private String validation(String yearbuilt2, String permitdate3, int option1) {
		// TODO Auto-generated method stub
		System.out.println("totalle"+totallen+"roof"+totalroof+"opu"+option);
		String chk = "false";
		try {
			if (!totalroof.equals("")) {
				
				if (option == 1) 
				{
					int perimeter = Integer.parseInt(totalroof);
					int yrbt = Integer.parseInt(totallen);
					perimeter = perimeter / 10;
					if (yrbt <= perimeter) {
						chk = "true";

					} else {
						chk = "false";

					}
				} 
				else if (option == 2) 
				{
					float perimeter = Float.parseFloat(totalroof);
					float yrbt = Float.parseFloat(totallen);
					Double temp = ((98.00 / 100.00) * perimeter);

					if (temp >= yrbt) {
						chk = "true";
					} else {
						chk = "false";
					}
				}
				else 
				{
					chk = "true";
				}
				
			} else {
				chk = "true";
			}
		} catch (Exception e) {
			//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofGeometry.this +" problem in datatype conversion types on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
System.out.println("catch"+e.getMessage());
			chk = "false";
		}
		return chk;
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
 		// replaces the default 'Back' button action
 		if (keyCode == KeyEvent.KEYCODE_BACK) {
 			cf.gotoclass(cf.identityval, WindMitRoofWall.class);
 			return true;
 		}
 		
 		return super.onKeyDown(keyCode, event);
 	}
	/**on activity result for the load comments**/
	 @Override
	 		protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	 		try
	 		{
	 			if(requestCode==cf.loadcomment_code)
	 			{
	 				load_comment=true;
	 				if(resultCode==RESULT_OK)
	 				{
	 					String rgcomm = rgcomment.getText().toString();	 
	 					rgcomment.setText(rgcomm +" "+data.getExtras().getString("Comments"));
		 			}
	 			}
	 		}
	 		catch (Exception e) {
	 			// TODO: handle exception
	 			System.out.println("the erro was "+e.getMessage());
	 		}
	 	}/**on activity result for the load comments ends**/
}