/*
************************************************************
* Project: ALL RISK REPORT
* Module : WindMitOpenProt.java
* Creation History:
Created By : Nalini on 10/24/2012
* Modification History:
Last Modified By : Gowri on 11/28/2012
************************************************************ 
*/
package idsoft.inspectiondepot.ARR;

import java.util.LinkedHashMap;
import java.util.Map;


import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.WindMitRoofCover.clicker1;
import idsoft.inspectiondepot.ARR.WindMitRoofWall.textwatcher;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory.Options;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;

public class WindMitOpenProt extends Activity{
	
	CommonFunctions cf;
	String b1_q7[] = {"FBC Plywood","All Glazed Openings 4 � 8 lb Large Missile lb.","All Glazed Openings 4 - 8 lb Large Missile","Opening Protection Not Verified", "None / Some Not Protected" };
	int opsubvalue,opvalue,focus=0;
	boolean weblink=false,optA=false,optB=false,optC=false,optD=false;
	String chkstatus[] = { "true", "true", "true", "true"};
	public int[] rwoptid = {R.id.opnrlytoptA,R.id.opnrlytoptB,R.id.opnrlytoptC,R.id.opnrlytoptN};
	public RelativeLayout rellyout[] = new RelativeLayout[4];
	String rdiochk="",tmp="",Windowentrydoorsval="", Garagedoorsval="",s="",suboption="",commentsfill="",comm="",updatecnt="",
			Skylightsval="", Glassblocksval="", Entrydoorsval="", NGOGarageDoorsval="",count_value="",
			GOwindowval="",GOgaraval="",GOskylightsval="",GOglassblockval="",NGOentryval="",NGOgaragedoorval;
	int optArdgpID,optBrdgpID,optCrdgpID,optNrdgpID;
	
	public int[] rdiooptionid = {R.id.OpenProtoption1,R.id.OpenProtoption2,R.id.OpenProtoption3,R.id.OpenProtoption4,R.id.OpenProtoption5};
	public RadioButton rdiobtnoption[] = new RadioButton[5];
	
	public int[] firstcolid = {R.id.NAans1,R.id.rdioAans1,R.id.rdioBans1,R.id.rdioCans1,R.id.rdioDans1,R.id.rdioN1ans1,R.id.rdioN2ans1,R.id.rdioXans1};
	public RadioButton GOcol1rdio[] = new RadioButton[8];
	
	public int[] secondcolid = {R.id.NAans2,R.id.rdioAans2,R.id.rdioBans2,R.id.rdioCans2,R.id.rdioDans2,R.id.rdioN1ans2,R.id.rdioN2ans2,R.id.rdioXans2};
	public RadioButton GOcol2rdio[] = new RadioButton[8];
	
	public int[] thirdcolid = {R.id.NAans3,R.id.rdioAans3,R.id.rdioBans3,R.id.rdioCans3,R.id.rdioDans3,R.id.rdioN1ans3,R.id.rdioN2ans3,R.id.rdioXans3};
	public RadioButton GOcol3rdio[] = new RadioButton[8];
	
	public int[] fourthcolid = {R.id.NAans4,R.id.rdioAans4,R.id.rdioBans4,R.id.rdioCans4,R.id.rdioDans4,R.id.rdioN1ans4,R.id.rdioN2ans4,R.id.rdioXans4};
	public RadioButton GOcol4rdio[] = new RadioButton[8];
	
	public int[] fifthcolid = {R.id.NAans5,R.id.rdioAans5,R.id.rdioBans5,R.id.rdioCans5,R.id.rdioDans5,R.id.rdioN1ans5,R.id.rdioN2ans5,R.id.rdioXans5};
	public RadioButton NGOcol1rdio[] = new RadioButton[8];
	
	public int[] sixthcolid = {R.id.NAans6,R.id.rdioAans6,R.id.rdioBans6,R.id.rdioCans6,R.id.rdioDans6,R.id.rdioN1ans6,R.id.rdioN2ans6,R.id.rdioXans6};
	public RadioButton NGOcol2rdio[] = new RadioButton[8];
	boolean load_comment=true;
	RadioGroup optArdgroup,optBrdgroup,optCrdgroup,optNrdgroup;
	EditText edopcomments;
	Map<String, String[]> autoselection = new LinkedHashMap<String, String[]>();
	String auto_status[] = { "flase", "flase", "flase", "flase", "flase",
			"flase", "flase", "false" };
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cf=new CommonFunctions(this);	  
        Bundle extras = getIntent().getExtras();
		if (extras != null) {
			cf.getExtras(extras);				
	 	}
		setContentView(R.layout.windmitopenprot);
        cf.getDeviceDimensions();
        focus=1;
        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
        if(cf.identityval==31)
        {
          hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","B1 1802(Rev.01/12)","Opening Protection",3,1,cf));
        }
        else if(cf.identityval==32)
        { 
        	hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","Commercial Type I","Opening Protection",3,1,cf));
        }
		LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
     	main_layout.setMinimumWidth(cf.wd);
     	main_layout.addView(new MyOnclickListener(getApplicationContext(), 3, 0,0,cf));
       
        cf.type_submenulayout =(LinearLayout) findViewById(R.id.typesubmenu); //** WINDMIT TYPE SUBMENU **//
        cf.type_submenulayout.addView(new MyOnclickListener(this, 300, 1,0, cf));
        cf.windmittypechk(cf.identityval,((RadioButton)findViewById(R.id.b1802_chk)),((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((TextView)findViewById(R.id.commtxt)),((ImageView)findViewById(R.id.editbno))); // To check the checkbox of choosen inspection type
        cf.uncheckcommercial(((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((RadioButton)findViewById(R.id.b1802_chk))); 
    	      
        cf.submenu_layout = (LinearLayout) findViewById(R.id.submenu);//** WINDMIT TYPE SUBMENU **//	        
        cf.submenu_layout .addView(new MyOnclickListener(this,cf.identityval,1,307, cf));
        
        cf.mainlayout = (LinearLayout)findViewById(R.id.bc_table);
        cf.save = (Button)findViewById(R.id.save);
        cf.tblrw = (TableRow)findViewById(R.id.row2);
        cf.tblrw.setMinimumHeight(cf.ht);			
		cf.CreateARRTable(31);
		cf.CreateARRTable(32);
		Declarations();        
		OpenProt_setValue();
    }
	private void OpenProt_setValue() {
		// TODO Auto-generated method stub
		try
		{
			Cursor c1=cf.SelectTablefunction(cf.Wind_Questiontbl, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
			 if(c1.getCount()>0)
		   {  
			   c1.moveToFirst();
			   opvalue=c1.getInt(c1.getColumnIndex("fld_openprotect"));
			   rdiochk=	String.valueOf(opvalue);
			   System.out.println("opval="+opvalue);
			   if(String.valueOf(opvalue).equals("") || String.valueOf(opvalue).equals("0"))
			   {
				   ((TextView)findViewById(R.id.savetxtop)).setVisibility(cf.v1.GONE);
			   }
			   else
			   {
				   
				   ((TextView)findViewById(R.id.savetxtop)).setVisibility(cf.v1.VISIBLE);
					   opsubvalue=c1.getInt(c1.getColumnIndex("fld_opsubvalue"));
					   
					   suboption=String.valueOf(opsubvalue); 		
					   cf.setvalueradio(String.valueOf(opvalue),rdiobtnoption);		
					   set_visible1(String.valueOf(opvalue));
					  
					   if(opsubvalue!=0)
					   {
					   
					   ((RadioButton) optArdgroup.findViewWithTag(String.valueOf(opsubvalue))).setChecked(true);
					   ((RadioButton) optBrdgroup.findViewWithTag(String.valueOf(opsubvalue))).setChecked(true);
					   ((RadioButton) optCrdgroup.findViewWithTag(String.valueOf(opsubvalue))).setChecked(true);
					   ((RadioButton) optNrdgroup.findViewWithTag(String.valueOf(opsubvalue))).setChecked(true); 
					   }
					   GOwindowval =c1.getString(c1.getColumnIndex("fld_opGOwindentdoorval"));
					   GOgaraval=c1.getString(c1.getColumnIndex("fld_opGOgardoorval"));
					   GOskylightsval=c1.getString(c1.getColumnIndex("fld_opGOskylightsval"));
					   GOglassblockval=c1.getString(c1.getColumnIndex("fld_opGOglassblockval"));
					   NGOentryval=c1.getString(c1.getColumnIndex("fld_opNGOentrydoorval"));
					   NGOgaragedoorval=c1.getString(c1.getColumnIndex("fld_opNGOgaragedoorval"));
					   System.out.println("GOwindowval");
					   if(GOwindowval.equals("NA")){Windowentrydoorsval = GOwindowval;open_product_autoselct("answer1", 0);}
					   else if(GOwindowval.equals("A")){Windowentrydoorsval = GOwindowval;open_product_autoselct("answer1", 1);}
					   else if(GOwindowval.equals("B")){Windowentrydoorsval = GOwindowval;open_product_autoselct("answer1", 2);}
					   else if(GOwindowval.equals("C")){Windowentrydoorsval = GOwindowval;open_product_autoselct("answer1", 3);}
					   else if(GOwindowval.equals("D")){Windowentrydoorsval = GOwindowval;open_product_autoselct("answer1", 4);}
					   else if(GOwindowval.equals("N")){Windowentrydoorsval = GOwindowval;open_product_autoselct("answer1", 5);}
					   else if(GOwindowval.equals("NSUB1")){Windowentrydoorsval = GOwindowval;open_product_autoselct("answer1", 6);}
					   else if(GOwindowval.equals("X")){Windowentrydoorsval = GOwindowval;open_product_autoselct("answer1", 7);}System.out.println("GOwindowval"+GOwindowval);
					   
					   
					   if(GOgaraval.equals("NA")){Garagedoorsval = GOgaraval;open_product_autoselct("answer2", 0);}
					   else if(GOgaraval.equals("A")){Garagedoorsval = GOgaraval;open_product_autoselct("answer2", 1);}
					   else if(GOgaraval.equals("B")){Garagedoorsval = GOgaraval;open_product_autoselct("answer2", 2);}
					   else if(GOgaraval.equals("C")){Garagedoorsval = GOgaraval;open_product_autoselct("answer2", 3);}
					   else if(GOgaraval.equals("D")){Garagedoorsval = GOgaraval;open_product_autoselct("answer2", 4);}
					   else if(GOgaraval.equals("N")){Garagedoorsval = GOgaraval;open_product_autoselct("answer2", 5);}
					   else if(GOgaraval.equals("NSUB2")){Garagedoorsval = GOgaraval;open_product_autoselct("answer2", 6);}
					   else if(GOgaraval.equals("X")){Garagedoorsval = GOgaraval;open_product_autoselct("answer2", 7);}System.out.println("GOgaraval"+GOgaraval);
					   
					   
					   if(GOskylightsval.equals("NA")){Skylightsval = GOskylightsval;open_product_autoselct("answer3", 0);}
					   else if(GOskylightsval.equals("A")){Skylightsval = GOskylightsval;open_product_autoselct("answer3", 1);}
					   else if(GOskylightsval.equals("B")){Skylightsval = GOskylightsval;open_product_autoselct("answer3", 2);}
					   else if(GOskylightsval.equals("C")){Skylightsval = GOskylightsval;open_product_autoselct("answer3", 3);}
					   else if(GOskylightsval.equals("D")){Skylightsval = GOskylightsval;open_product_autoselct("answer3", 4);}
					   else if(GOskylightsval.equals("N")){Skylightsval = GOskylightsval;open_product_autoselct("answer3", 5);}
					   else if(GOskylightsval.equals("NSUB3")){Skylightsval = GOskylightsval;open_product_autoselct("answer3", 6);}
					   else if(GOskylightsval.equals("X")){Skylightsval = GOskylightsval;open_product_autoselct("answer3", 7);}System.out.println("GOskylightsval"+GOskylightsval);
					   
					   if(GOglassblockval.equals("NA")){Glassblocksval = GOglassblockval;open_product_autoselct("answer4", 0);}
					   else if(GOglassblockval.equals("A")){Glassblocksval = GOglassblockval;open_product_autoselct("answer4", 1);}
					   else if(GOglassblockval.equals("B")){Glassblocksval = GOglassblockval;open_product_autoselct("answer4", 2);}
					   else if(GOglassblockval.equals("C")){Glassblocksval = GOglassblockval;open_product_autoselct("answer4", 3);}
					   else if(GOglassblockval.equals("D")){Glassblocksval = GOglassblockval;open_product_autoselct("answer4", 4);}
					   else if(GOglassblockval.equals("N")){Glassblocksval = GOglassblockval;open_product_autoselct("answer4", 5);}
					   else if(GOglassblockval.equals("NSUB4")){Glassblocksval = GOglassblockval;open_product_autoselct("answer4", 6);}
					   else if(GOglassblockval.equals("X")){Glassblocksval = GOglassblockval;open_product_autoselct("answer4", 7);}System.out.println("GOglassblockval"+GOglassblockval);
					   
					   
					   if(NGOentryval.equals("NA")){Entrydoorsval = NGOentryval;open_product_autoselct("answer5", 0);}
					   else if(NGOentryval.equals("A")){Entrydoorsval = NGOentryval;open_product_autoselct("answer5", 1);}
					   else if(NGOentryval.equals("B")){Entrydoorsval = NGOentryval;open_product_autoselct("answer5", 2);}
					   else if(NGOentryval.equals("C")){Entrydoorsval = NGOentryval;open_product_autoselct("answer5", 3);}
					   else if(NGOentryval.equals("D")){Entrydoorsval = NGOentryval;open_product_autoselct("answer5", 4);}
					   else if(NGOentryval.equals("N")){Entrydoorsval = NGOentryval;open_product_autoselct("answer5", 5);}
					   else if(NGOentryval.equals("NSUB5")){Entrydoorsval = NGOentryval;open_product_autoselct("answer5", 6);}
					   else if(NGOentryval.equals("X")){Entrydoorsval = NGOentryval;open_product_autoselct("answer5", 7);}System.out.println("NGOentryval"+NGOentryval);
					   
					   
					   if(NGOgaragedoorval.equals("NA")){NGOGarageDoorsval = NGOgaragedoorval;open_product_autoselct("answer6", 0);}
					   else if(NGOgaragedoorval.equals("A")){NGOGarageDoorsval = NGOgaragedoorval;open_product_autoselct("answer6", 1);}
					   else if(NGOgaragedoorval.equals("B")){NGOGarageDoorsval = NGOgaragedoorval;open_product_autoselct("answer6", 2);}
					   else if(NGOgaragedoorval.equals("C")){NGOGarageDoorsval = NGOgaragedoorval;open_product_autoselct("answer6", 3);}
					   else if(NGOgaragedoorval.equals("D")){NGOGarageDoorsval = NGOgaragedoorval;open_product_autoselct("answer6", 4);}
					   else if(NGOgaragedoorval.equals("N")){NGOGarageDoorsval = NGOgaragedoorval;open_product_autoselct("answer6", 5);}
					   else if(NGOgaragedoorval.equals("NSUB6")){NGOGarageDoorsval = NGOgaragedoorval;open_product_autoselct("answer6", 6);}
					   else if(NGOgaragedoorval.equals("X")){NGOGarageDoorsval = NGOgaragedoorval;open_product_autoselct("answer6", 7);}System.out.println("NGOgaragedoorval"+NGOgaragedoorval);
					   
					   
					   cf.setvalueradio(GOwindowval,GOcol1rdio);
					   cf.setvalueradio(GOgaraval,GOcol2rdio);
					   cf.setvalueradio(GOskylightsval,GOcol3rdio);
					   cf.setvalueradio(GOglassblockval,GOcol4rdio);
					   cf.setvalueradio(NGOentryval,NGOcol1rdio);			   
					   cf.setvalueradio(NGOgaragedoorval,NGOcol2rdio);System.out.println("NGOcol2rdio");
			   }
			   
			   
			  }
		   else
		   {
			   ((TextView)findViewById(R.id.savetxtop)).setVisibility(cf.v1.GONE);
		   }
		   c1.close();
		}
		catch (Exception E)
		{
	    	System.out.println("EE in open protection"+E.getMessage());
			String strerrorlog="Retrieving OpenProtect - WINDMIT";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
		try
		{
			Cursor c2=cf.SelectTablefunction(cf.Wind_QuesCommtbl, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
			if(c2.getCount()>0)
			{
				c2.moveToFirst();
				edopcomments.setText(cf.decode(c2.getString(c2.getColumnIndex("fld_openprotectioncomments"))));
			}
		}
		catch(Exception e)
		{
			System.out.println("exception"+e.getMessage());			
		}
	}
	private void set_visible1(String valueOf) {
		// TODO Auto-generated method stub		
			for (int i = 0; i <= 3; i++) {
				if ((i + 1) != (Integer.parseInt(rdiochk))) {
					rellyout[i].setVisibility(View.GONE);
				} else {
					rellyout[i].setVisibility(View.VISIBLE);
				}
			}
			// function for set cisible once the radio button clicked
		
	}
		// this is initializing the form
		private void open_product_autoselct(String s, int i) {
			// TODO Auto-generated method stub
			String temp2[] = { "false", "false", "false", "false", "false",
					"false", "false", "false" };
			for (int j = 0; j < temp2.length; j++) {
				if (j == i) {
					temp2[j] = "true";
				} else {
					temp2[j] = "false";
				}
			}
			autoselection.put(s, temp2);

		}
	private void Declarations() {
		// TODO Auto-generated method stub
		cf.setupUI((ScrollView) findViewById(R.id.scr));
		if(cf.identityval==31 && cf.onlinspectionid.equals("12") || cf.identityval==32 && cf.onlinspectionid.equals("12"))
		{
			cf.save.setText("Save & Next");
		}
		else
		{
			cf.save.setText("Save");
			
		}
		((TextView)findViewById(R.id.viewtxt1)).setText(Html.fromHtml("<font color='#0000FF'><u>View Text</u></font>"));
		((TextView)findViewById(R.id.viewtxt1)).setOnClickListener((OnClickListener) new clicker1());
		((TextView)findViewById(R.id.viewtxt2)).setText(Html.fromHtml("<font color='#0000FF'><u>View Text</u></font>"));
		((TextView)findViewById(R.id.viewtxt2)).setOnClickListener((OnClickListener) new clicker1());
		
		((TextView)findViewById(R.id.openprot_header)).setText(Html
				.fromHtml("What is the weakest form of wind borne debris protection installed on the structure ? First, use the table to determine the weakest form of protection for each category of opening. Second, (a) check one answer below (A, B, C, N, or X) based upon the lowest protection level for ALL Glazed openings and (b) check the protection level for all Non-Glazed openings (.1, .2, or .3) as applicable."));
		
		String miamelink = "<a href=''>" + "Miame-Dade Product Approval" + "</a>";
		String fbclink = "<a href=''>" + "FBC Product Approval" + "</a>";
		((TextView)findViewById(R.id.miamedadeprodapprlink)).setText(Html.fromHtml(miamelink));
		((TextView)findViewById(R.id.miamedadeprodapprlink)).setOnClickListener((OnClickListener) new clicker1());
		
		((TextView)findViewById(R.id.fbcprodapprlink)).setText(Html.fromHtml(fbclink));
		((TextView)findViewById(R.id.fbcprodapprlink)).setOnClickListener((OnClickListener) new clicker1());
		 
		 for(int i=0;i<rellyout.length;i++)
    	 {
    		rellyout[i] = (RelativeLayout)findViewById(rwoptid[i]);
    	 }
		 for(int i=0;i<rdiobtnoption.length;i++)
    	 {
    		rdiobtnoption[i] = (RadioButton)findViewById(rdiooptionid[i]);
    	 }		 
		 for(int i=0;i<GOcol1rdio.length;i++)
    	 {
    		GOcol1rdio[i] = (RadioButton)findViewById(firstcolid[i]);   			
    	 }
		 for(int i=0;i<GOcol2rdio.length;i++)
    	 {
    		GOcol2rdio[i] = (RadioButton)findViewById(secondcolid[i]);   			
    	 }
		 for(int i=0;i<GOcol3rdio.length;i++)
    	 {
    		GOcol3rdio[i] = (RadioButton)findViewById(thirdcolid[i]);   			
    	 }		 
		 for(int i=0;i<GOcol4rdio.length;i++)
    	 {
    		GOcol4rdio[i] = (RadioButton)findViewById(fourthcolid[i]);
    	 }
		 for(int i=0;i<NGOcol1rdio.length;i++)
    	 {
    		NGOcol1rdio[i] = (RadioButton)findViewById(fifthcolid[i]);   			
    	 }
		 for(int i=0;i<NGOcol2rdio.length;i++)
    	 {
    		NGOcol2rdio[i] = (RadioButton)findViewById(sixthcolid[i]);   			
    	 }
		 
		 optArdgroup=(RadioGroup) findViewById(R.id.rdgroupoptA);
		 optBrdgroup=(RadioGroup) findViewById(R.id.rdgroupoptB);
		 optCrdgroup=(RadioGroup) findViewById(R.id.rdgroupoptC);
		 optNrdgroup=(RadioGroup) findViewById(R.id.rdgroupoptN);
		 edopcomments=(EditText) findViewById(R.id.opcomment);
		 edopcomments.addTextChangedListener(new textwatcher());
		TextView helptxt = (TextView) findViewById(R.id.help);
			helptxt.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					if (((RadioButton)findViewById(R.id.rdioques1ans1)).isChecked()) {
						cf.alertcontent = "The opening protection to this home was verified as meeting the requirements of selection A for glazed openings and A1 for non glazed openings, meeting the large missile impact 9lb rating, for question 7  of the OIR B1 -1802, Opening Protection. Refer to attached photographs and documentation.";
					} else if (((RadioButton)findViewById(R.id.rdioques1ans2)).isChecked()) {
						cf.alertcontent = "The opening protection to this home was verified as meeting the requirements of selection A for glazed openings and A2 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that all glazed openings were verified as impact rated. A non glazed garage door opening was however verified as wind only rated and not impact rated, being identified on the Opening Protection Level Chart as level D. Refer to attached photographs.";
					} else if (((RadioButton)findViewById(R.id.rdioques1ans3)).isChecked()) {
						cf.alertcontent = "The opening protection to this home was verified as meeting the requirements of selection A for glazed openings and A3 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that all glazed openings were verified as impact rated. One or more non glazed openings did not meet level A, large 9 lb missile impact rating as identified on the Opening Protection Level Chart as level D. Refer to attached photographs.";
					} else if (((RadioButton)findViewById(R.id.rdioques2ans1)).isChecked()) {
						cf.alertcontent = "The opening protection to this home was verified as meeting the requirements of selection B for glazed openings and B1 for non glazed openings, meeting the large 4-8lb  missile impact rating, for question 7  of the OIR B1 -1802, Opening Protection. Refer to attached photographs.";
					} else if (((RadioButton)findViewById(R.id.rdioques2ans2)).isChecked()) {
						cf.alertcontent = "The opening protection to this home was verified as meeting the requirements of selection B for glazed openings and B2 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that all glazed openings were verified as 4-8lb missile impact rating. A non glazed garage door opening was however verified as wind only rated and not impact rated, being identified on the Opening Protection Level Chart as level D. Refer to attached photographs.";
					} else if (((RadioButton)findViewById(R.id.rdioques2ans3)).isChecked()) {
						cf.alertcontent = "The opening protection to this home was verified as meeting the requirements of selection  B for glazed openings and B3 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that all glazed openings were verified as 4-8lb missile impact rating. One or more non glazed opening did not meet level B large missile, 4-8lb, impact rating as identified on the Opening Protection Level Chart. Refer to attached photographs.";
					} else if (((RadioButton)findViewById(R.id.rdioques3ans1)).isChecked()) {
						cf.alertcontent = "The opening protection to this home was verified as meeting the requirements of selection C for glazed openings and C1 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection. This means that all openings to this home were verified as meeting the Florida Building Code requirements for wood structural panels. Refer to attached Photographs.";
					} else if (((RadioButton)findViewById(R.id.rdioques3ans2)).isChecked()) {
						cf.alertcontent = "The opening protection to this home was verified as meeting the requirements of selection C for glazed openings and C2 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that all openings to this home were verified as meeting the Florida Building Code requirements for wood structural panels apart from the non glazed garage door. This door opening was verified as wind only rated and not impact rated, and identified on the Opening Protection Level Chart as level D. Refer to attached photographs.";
					} else if (((RadioButton)findViewById(R.id.rdioques3ans3)).isChecked()) {
						cf.alertcontent = "The opening protection to this home was verified as meeting the requirements of selection  C for glazed openings and C3 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that all glazed openings were verified as protected with wood structural panels that are compliant to the Florida Building Code. One or more non glazed opening did not meet level A,B or C impact rating as identified on the Opening Protection Level Chart. Refer to attached photographs.";
					} else if (((RadioButton)findViewById(R.id.rdioques4ans1)).isChecked()) {
						cf.alertcontent = "The opening protection to this home was verified as meeting the requirements of selection N for glazed openings and N1 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection. This means that the opening protection was present to all openings that could not be verified as meeting the impact requirements of selection A or B. Refer to attached Photographs.";
					} else if (((RadioButton)findViewById(R.id.rdioques4ans2)).isChecked()) {
						cf.alertcontent = "The opening protection to this home was verified as meeting the requirements of selection N for glazed openings and N2 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that we were unable to verify the opening protection as meeting large missile impact rating as outline in selection A and B, apart from the non glazed garage door. This door opening was verified as wind only rated and not impact rated, and identified on the Opening Protection Level Chart as level D. Refer to attached photographs.";
					} else if (((RadioButton)findViewById(R.id.rdioques4ans3)).isChecked()) {
						cf.alertcontent = "The opening protection to this home was verified as meeting the requirements of selection  N for glazed openings and N3 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that some openings were not protected. Refer to attached photographs.";
					} else if (((RadioButton)findViewById(R.id.OpenProtoption5)).isChecked()) {
						cf.alertcontent = "The opening protection to this home was verified as meeting the requirements of selection X for glazed, for question 7  of the OIR B1 -1802, Opening Protection. This means that some or all openings did not have verifiable opening protection, meeting the impact requirements of selection A or B. Refer to attached Photographs.";
					} else {
						cf.alertcontent = "To see help comments, please select Opening Protection options.";
					}
					cf.showhelp("HELP",cf.alertcontent);
				}
			});
	}
	
	public void alert() {
		Builder builder = new AlertDialog.Builder(WindMitOpenProt.this);
		builder.setTitle("No Internet Connection");
		builder.setMessage("Please Turn On Wifi");
		builder.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {

			}
		});
		builder.show();
	}

	class textwatcher implements TextWatcher
    {
	         public int type;
	         textwatcher()
	    	{
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {
	    			// TODO Auto-generated method stub
	    		cf.showing_limit(s.toString(),(TextView)findViewById(R.id.op_tv_type1),500); 
	    	}

	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,

	    			int after) {
	    		// TODO Auto-generated method stub
	     	}

	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,

	    			int count) {
	    		// TODO Auto-generated method stub
	    	}
	   }	
	public void set_visible() {
		// function for set cisible once the radio button clicked
		for (int i = 0; i <= 3; i++) {
			if ((i + 1) != (Integer.parseInt(rdiochk))) {
				rellyout[i].setVisibility(View.GONE);
			} else {
				rellyout[i].setVisibility(View.VISIBLE);
			}
		}
		// function for set cisible once the radio button clicked
	}
	
	public void hidemainoption()
	{
		 for(int i=0;i<=4;i++)
    	 {			 
			 rdiobtnoption[i].setChecked(false);		 
    	 }
	}
	private void hidesuboption()
	{
		optA=true;try{if(optA){optArdgroup.clearCheck();}}catch(Exception e){}
		optB=true;try{if(optB){optBrdgroup.clearCheck();}}catch(Exception e){}
		optC=true;try{if(optC){optCrdgroup.clearCheck();}}catch(Exception e){}
		optD=true;try{if(optD){optNrdgroup.clearCheck();}}catch(Exception e){}
	}
	protected void poen_product_autoselct(String s, int i) {
		// TODO Auto-generated method stub
		try {
			String temp2[] = { "false", "false", "false", "false", "false",
					"false", "false", "false" };
			for (int j = 0; j < temp2.length; j++) {
				if (j == i - 1) {
					temp2[j] = "true";
				} else {
					temp2[j] = "false";
				}
			}
			autoselection.put(s, temp2);
			String answer_1[] = autoselection.get("answer1");
			String answer_2[] = autoselection.get("answer2");
			String answer_3[] = autoselection.get("answer3");
			String answer_4[] = autoselection.get("answer4");
			String answer_5[] = autoselection.get("answer5");
			String answer_6[] = autoselection.get("answer6");

			if ((answer_1[0] == "true" || answer_1[1] == "true"
					|| answer_1[2] == "true" || answer_1[3] == "true"
					|| answer_1[4] == "true" || answer_1[5] == "true"
					|| answer_1[6] == "true" || answer_1[7] == "true")
					&& (answer_2[0] == "true" || answer_2[1] == "true"
							|| answer_2[2] == "true" || answer_2[3] == "true"
							|| answer_2[4] == "true" || answer_2[5] == "true"
							|| answer_2[6] == "true" || answer_2[7] == "true")
					&& (answer_3[0] == "true" || answer_3[1] == "true"
							|| answer_3[2] == "true" || answer_3[3] == "true"
							|| answer_3[4] == "true" || answer_3[5] == "true"
							|| answer_3[6] == "true" || answer_3[7] == "true")
					&& (answer_4[0] == "true" || answer_4[1] == "true"
							|| answer_4[2] == "true" || answer_4[3] == "true"
							|| answer_4[4] == "true" || answer_4[5] == "true"
							|| answer_4[6] == "true" || answer_4[7] == "true")) {
				
				
				if(answer_1[7] == "true" || answer_2[7] == "true"
						|| answer_3[7] == "true" || answer_4[7] == "true")
				{
					rdiochk = "5";
					suboption = "0";
					set_visible();
					hidemainoption();
					rdiobtnoption[4].setChecked(true);
					hidesuboption();
					commentsfill = "The opening protection to this home was verified as meeting the requirements of selection X for glazed, for question 7  of the OIR B1 -1802, Opening Protection. This means that some or all openings did not have verifiable opening protection, meeting the impact requirements of selection A or B. Refer to attached Photographs.";
					edopcomments.setText(commentsfill);
					edopcomments.setText("");
				}
				else if ((answer_4[7] == "true")
						&& ((count_value.trim().equals("miami-dade")) || (count_value
								.trim().equals("broward")))) {
					rdiochk = "5";
					suboption = "0";
					set_visible();
					hidemainoption();
					rdiobtnoption[4].setChecked(true);
					hidesuboption();
					commentsfill = "The opening protection to this home was verified as meeting the requirements of selection X for glazed, for question 7  of the OIR B1 -1802, Opening Protection. This means that some or all openings did not have verifiable opening protection, meeting the impact requirements of selection A or B. Refer to attached Photographs.";
					edopcomments.setText(commentsfill);
					edopcomments.setText("");

				} else if ((answer_1[5] == "true" || answer_2[5] == "true"
						|| answer_3[5] == "true" || answer_4[5] == "true"
						|| answer_1[6] == "true" || answer_2[6] == "true"
						|| answer_3[6] == "true" || answer_4[6] == "true")) {
					System.out.println("1one");
					rdiochk = "4";
					
					hidemainoption();
					rdiobtnoption[3].setChecked(true);
					set_visible();
					rellyout[3].setVisibility(cf.v1.VISIBLE);
					if (answer_5[7] == "true" && answer_6[7] == "true") {
						rdiochk = "4";
						suboption = "3";
						set_visible();
						rellyout[3].setVisibility(cf.v1.VISIBLE);
						hidesuboption();
						((RadioButton) optNrdgroup.findViewWithTag("3")).setChecked(true);
						commentsfill = "The opening protection to this home was verified as meeting the requirements of selection  N for glazed openings and N3 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that some openings were not protected. Refer to attached photographs.";
						edopcomments.setText(commentsfill);
						edopcomments.setText("");

					} else if (answer_5[4] == "true" && answer_6[4] == "true") {
						rdiochk = "4";
						suboption = "2";
						set_visible();
						rellyout[3].setVisibility(cf.v1.VISIBLE);
						hidesuboption();
						((RadioButton) optNrdgroup.findViewWithTag("2")).setChecked(true);
						commentsfill = "The opening protection to this home was verified as meeting the requirements of selection N for glazed openings and N2 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that we were unable to verify the opening protection as meeting large missile impact rating as outline in selection A and B, apart from the non glazed garage door. This door opening was verified as wind only rated and not impact rated, and identified on the Opening Protection Level Chart as level D. Refer to attached photographs.";
						edopcomments.setText(commentsfill);
						edopcomments.setText("");
						
					} else if ((answer_5[0] == "true" || answer_5[1] == "true"
							|| answer_5[2] == "true" || answer_5[3] == "true"
							|| answer_5[5] == "true" || answer_5[6] == "true")
							&& (answer_6[0] == "true" || answer_6[1] == "true"
									|| answer_6[2] == "true"
									|| answer_6[3] == "true"
									|| answer_6[5] == "true" || answer_6[6] == "true")) {
						rdiochk = "4";
						suboption = "1";
						set_visible();
						rellyout[3].setVisibility(cf.v1.VISIBLE);
						hidesuboption();
						((RadioButton) optNrdgroup.findViewWithTag("1")).setChecked(true);						
						commentsfill = "The opening protection to this home was verified as meeting the requirements of selection N for glazed openings and N1 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection. This means that the opening protection was present to all openings that could not be verified as meeting the impact requirements of selection A or B. Refer to attached Photographs.";
						edopcomments.setText(commentsfill);
						edopcomments.setText("");						
					} else {
						edopcomments.setText("");						
						suboption = "";
						hidesuboption();
					}
				} else if (answer_1[3] == "true" || answer_2[3] == "true"
						|| answer_3[3] == "true" || answer_4[3] == "true") {
					System.out.println("2two");
					rdiochk = "3";
					hidemainoption();
					rdiobtnoption[2].setChecked(true);
					set_visible();
					rellyout[2].setVisibility(cf.v1.VISIBLE);

					if ((answer_5[6] == "true" || answer_5[7] == "true" || answer_5[5] == "true")
							&& (answer_6[6] == "true" || answer_6[5] == "true" || answer_6[7] == "true")) {
						rdiochk = "3";
						suboption = "3";
						set_visible();
						rellyout[2].setVisibility(cf.v1.VISIBLE);
						hidesuboption();
						((RadioButton) optCrdgroup.findViewWithTag("3")).setChecked(true);
						commentsfill = "The opening protection to this home was verified as meeting the requirements of selection  C for glazed openings and C3 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that all glazed openings were verified as protected with wood structural panels that are compliant to the Florida Building Code. One or more non glazed opening did not meet level A,B or C impact rating as identified on the Opening Protection Level Chart. Refer to attached photographs.";
						edopcomments.setText(commentsfill);
					} else if ((answer_5[4] == "true" && answer_6[4] == "true")) {
						rdiochk = "3";
						suboption = "2";
						set_visible();
						rellyout[2].setVisibility(cf.v1.VISIBLE);
						hidesuboption();
						((RadioButton) optCrdgroup.findViewWithTag("2")).setChecked(true);
						commentsfill = "The opening protection to this home was verified as meeting the requirements of selection C for glazed openings and C2 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that all openings to this home were verified as meeting the Florida Building Code requirements for wood structural panels apart from the non glazed garage door. This door opening was verified as wind only rated and not impact rated, and identified on the Opening Protection Level Chart as level D. Refer to attached photographs.";
						edopcomments.setText(commentsfill);

					} else if ((answer_5[1] == "true" || answer_5[2] == "true"
							|| answer_5[3] == "true" || answer_5[0] == "true")
							&& (answer_6[1] == "true" || answer_6[0] == "true"
									|| answer_6[2] == "true" || answer_6[3] == "true")) {
						rdiochk = "3";
						suboption = "1";
						set_visible();
						rellyout[2].setVisibility(cf.v1.VISIBLE);
						hidesuboption();
						((RadioButton) optCrdgroup.findViewWithTag("1")).setChecked(true);
						commentsfill = "The opening protection to this home was verified as meeting the requirements of selection C for glazed openings and C1 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection. This means that all openings to this home were verified as meeting the Florida Building Code requirements for wood structural panels. Refer to attached Photographs.";
						edopcomments.setText(commentsfill);
					} else {
						edopcomments.setText("");
						suboption = "";
						hidesuboption();
					}
				} else if ((answer_1[2] == "true" || answer_2[2] == "true"
						|| answer_3[2] == "true" || answer_4[2] == "true")) {
					System.out.println("3three");
					rdiochk = "2";
					hidemainoption();
					rdiobtnoption[1].setChecked(true);
					set_visible();
					rellyout[1].setVisibility(cf.v1.VISIBLE);

					if ((answer_5[6] == "true" || answer_5[7] == "true"
							|| answer_5[5] == "true" || answer_5[3] == "true")
							&& (answer_6[3] == "true" || answer_6[6] == "true"
									|| answer_6[5] == "true" || answer_6[7] == "true")) {
						rdiochk = "2";
						suboption = "3";
						set_visible();
						rellyout[1].setVisibility(cf.v1.VISIBLE);
						hidesuboption();
						((RadioButton) optBrdgroup.findViewWithTag("3")).setChecked(true);
						commentsfill = "The opening protection to this home was verified as meeting the requirements of selection  B for glazed openings and B3 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that all glazed openings were verified as 4-8lb missile impact rating. One or more non glazed opening did not meet level B large missile, 4-8lb, impact rating as identified on the Opening Protection Level Chart. Refer to attached photographs.";
						edopcomments.setText(commentsfill);

					} else if ((answer_5[4] == "true" && answer_6[4] == "true")) {
						rdiochk = "2";
						suboption = "2";
						set_visible();
						rellyout[1].setVisibility(cf.v1.VISIBLE);				
						hidemainoption();
						rdiobtnoption[1].setChecked(true);
						hidesuboption();
						((RadioButton) optBrdgroup.findViewWithTag("2")).setChecked(true);
						commentsfill = "The opening protection to this home was verified as meeting the requirements of selection B for glazed openings and B2 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that all glazed openings were verified as 4-8lb missile impact rating. A non glazed garage door opening was however verified as wind only rated and not impact rated, being identified on the Opening Protection Level Chart as level D. Refer to attached photographs.";
						edopcomments.setText(commentsfill);

					} else if ((answer_5[1] == "true" || answer_5[2] == "true" || answer_5[0] == "true")
							&& (answer_6[0] == "true" || answer_6[1] == "true" || answer_6[2] == "true")) {
						rdiochk = "2";
						suboption = "1";
						hidesuboption();
						((RadioButton) optBrdgroup.findViewWithTag("1")).setChecked(true);
						commentsfill = "The opening protection to this home was verified as meeting the requirements of selection B for glazed openings and B1 for non glazed openings, meeting the large 4-8lb  missile impact rating, for question 7  of the OIR B1 -1802, Opening Protection. Refer to attached photographs.";
						edopcomments.setText(commentsfill);
					} else {
						edopcomments.setText("");
						suboption = "";
						set_visible();
						hidesuboption();
					}
				} else if ((answer_1[1] == "true" || answer_2[1] == "true"
						|| answer_3[1] == "true" || answer_4[1] == "true")
						|| (answer_1[0] == "true" || answer_2[0] == "true"
								|| answer_3[0] == "true" || answer_4[0] == "true")) {
					System.out.println("4four");
					hidemainoption();
					rdiobtnoption[0].setChecked(true);
					rdiochk="1";
					set_visible();
					rellyout[0].setVisibility(cf.v1.VISIBLE);

					rdiochk = "1";
					if ((answer_5[6] == "true" || answer_5[2] == "true"
							|| answer_5[7] == "true" || answer_5[5] == "true" || answer_5[3] == "true")
							&& (answer_6[2] == "true" || answer_6[3] == "true"
									|| answer_6[6] == "true"
									|| answer_6[5] == "true" || answer_6[7] == "true")) {
						
						rdiochk = "1";
						suboption = "3";
						set_visible();
						rellyout[0].setVisibility(cf.v1.VISIBLE);
						hidesuboption();
						((RadioButton) optArdgroup.findViewWithTag("3")).setChecked(true);						
						commentsfill = "The opening protection to this home was verified as meeting the requirements of selection A for glazed openings and A3 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that all glazed openings were verified as impact rated. One or more non glazed openings did not meet level A, large 9 lb missile impact rating as identified on the Opening Protection Level Chart as level D. Refer to attached photographs.";
						edopcomments.setText(commentsfill);

					} else if ((answer_5[4] == "true" && answer_6[4] == "true")) {
						
						rdiochk = "1";
						suboption = "2";
						set_visible();
						rellyout[0].setVisibility(cf.v1.VISIBLE);
						hidemainoption();
						rdiobtnoption[0].setChecked(true);
						hidesuboption();
						((RadioButton) optArdgroup.findViewWithTag("2")).setChecked(true);
						
						commentsfill = "The opening protection to this home was verified as meeting the requirements of selection A for glazed openings and A2 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that all glazed openings were verified as impact rated. A non glazed garage door opening was however verified as wind only rated and not impact rated, being identified on the Opening Protection Level Chart as level D. Refer to attached photographs.";
						edopcomments.setText(commentsfill);

					} else if ((answer_5[1] == "true" || answer_5[0] == "true")
							&& (answer_6[1] == "true" || answer_6[0] == "true")) {
						
						rdiochk = "1";
						suboption = "1";						
						set_visible();
						hidemainoption();
						rdiobtnoption[0].setChecked(true);
						hidesuboption();						
						set_visible();
						rellyout[0].setVisibility(cf.v1.VISIBLE);
						hidemainoption();
						rdiobtnoption[0].setChecked(true);
						hidesuboption();
						((RadioButton) optArdgroup.findViewWithTag("1")).setChecked(true);
						commentsfill = "The opening protection to this home was verified as meeting the requirements of selection A for glazed openings and A1 for non glazed openings, meeting the large missile impact 9lb rating, for question 7  of the OIR B1 -1802, Opening Protection. Refer to attached photographs and documentation.";
						edopcomments.setText(commentsfill);
					} 
					else 
					{						
						edopcomments.setText("");
						suboption = "";
						hidesuboption();
					}
				} 
				else 
				{
					System.out.println("5ive");
					
					edopcomments.setText("");
					rdiochk = "";
					suboption = "";
					hidemainoption();
					hidesuboption();
					rdiobtnoption[4].setChecked(true);
				}
			} else 
			{
				System.out.println("elsesel");
				edopcomments.setText("");
				rdiochk = "";
				suboption = "";
				set_visible();
				hidemainoption();
				hidesuboption();
			}

		} catch (Exception e) {
			System.out.println("error in the click even " + e.getMessage());
		}
	}
	private void optionhidefirstcol()
	{
		 for(int i=0;i<GOcol1rdio.length;i++)
    	{
    			GOcol1rdio[i].setChecked(false); 
    	}
	}
	private void optionhidesecondcol()
	{
		 for(int i=0;i<GOcol2rdio.length;i++)
    	{
    			GOcol2rdio[i].setChecked(false); 
    	}
	}
	private void optionhidethirdcol()
	{
		 for(int i=0;i<GOcol3rdio.length;i++)
    	{
    			GOcol3rdio[i].setChecked(false); 
    	}
	}
	private void optionhidefourthcol()
	{
		 for(int i=0;i<GOcol4rdio.length;i++)
    	{
    			GOcol4rdio[i].setChecked(false); 
    	}
	}
	private void optionhidefifthcol()
	{
		 for(int i=0;i<NGOcol1rdio.length;i++)
    	{
    			NGOcol1rdio[i].setChecked(false); 
    	}
	}
	private void optionhidesixthcol()
	{
		 for(int i=0;i<NGOcol2rdio.length;i++)
    	{
			NGOcol2rdio[i].setChecked(false); 
    	}
	}
	public void clicker(View v) {
		switch(v.getId())
		{		
		case R.id.loadcomments:
			cf.findinspectionname(cf.identityval);Open_suboption();
			int len=((EditText)findViewById(R.id.opcomment)).getText().toString().length();			
			if(!tmp.equals(""))
			{
				load_comment=true;
					if(load_comment)
					{
						load_comment=false;
						int loc1[] = new int[2];
						v.getLocationOnScreen(loc1);
						Intent in = cf.loadComments(tmp,loc1);
						in.putExtra("insp_name", cf.inspname);
						in.putExtra("insp_ques", "Opening Protection");
						in.putExtra("length", len);
						in.putExtra("max_length", 500);
						startActivityForResult(in, cf.loadcomment_code);
					}
			}
			else
			{
				cf.ShowToast("Please select the option for Opening Protection.",0);
			}			
			break;
		case R.id.OpenProtoption1:
			rdiochk="1";hidemainoption();hidesuboption();set_visible();
			rdiobtnoption[0].setChecked(true);
			break;
		case R.id.OpenProtoption2:
			rdiochk="2";hidemainoption();hidesuboption();set_visible();
			rdiobtnoption[1].setChecked(true);
			break;
		case R.id.OpenProtoption3:
			rdiochk="3";hidemainoption();hidesuboption();set_visible();
			rdiobtnoption[2].setChecked(true);
			break;
		case R.id.OpenProtoption4:
			rdiochk="4";hidemainoption();hidesuboption();set_visible();
			rdiobtnoption[3].setChecked(true);
			break;
		case R.id.OpenProtoption5:
			commentsfill = "The opening protection to this home was verified as meeting the requirements of selection X for glazed, for question 7  of the OIR B1 -1802, Opening Protection. This means that some or all openings did not have verifiable opening protection, meeting the impact requirements of selection A or B. Refer to attached Photographs.";
			((EditText)findViewById(R.id.opcomment)).setText(commentsfill);
			rdiochk="5";hidemainoption();hidesuboption();set_visible();
			rdiobtnoption[4].setChecked(true);
			break;
		case R.id.helpoptA: 
			cf.alerttitle="A � All Glazed Openings � Large Missile 9 lb.";
			  cf.alertcontent="Exterior openings cyclic pressure and 9 lbs. large missile (4.5 lbs. for skylights only): All glazed openings are protected at a minimum, with impact-resistant coverings or products listed as wind-borne debris protection devices in the product approval system of the State of Florida or Miami-Dade County AND meet the requirements of one of the following for �Cyclic Pressure and Large Missile Impact� (Level A in the table above).";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
		 case R.id.helpoptB: 
			 cf.alerttitle="All Glazed Openings 4 � 8 lb Large Missile";
			 cf.alertcontent="Exterior opening protection � cyclic pressure and 4-8 lbs. large missile (2-4.5 lbs. for skylights only):All glazed openings are protected, at a minimum, with impact-resistant coverings or products listed as wind-borne debris protection devices in the product approval system of the State of Florida or Miami-Dade County AND meet the requirements of one of the following for �Cyclic Pressure and Large Missile Impact� (Level B in the table above)"; cf.showhelp(cf.alerttitle,cf.alertcontent);
			 cf.showhelp(cf.alerttitle,cf.alertcontent);
			 break;
		 case R.id.helpoptC: 
			 cf.alerttitle="C � FBC Plywood";
			 cf.alertcontent="Exterior opening protection � wood structural panels meeting FBC: All glazed openings are covered with plywood/OSB meeting the requirements of Table 1609.1.2 of the FBC 2007 (Level C in the table above).";
			 cf.showhelp(cf.alerttitle,cf.alertcontent);
			 break;
		 case R.id.helpoptD: 
			 cf.alerttitle="N � Opening Protection Not Verified";
			 cf.alertcontent="Exterior opening protection (unverified shutter systems with no documentation):All glazed openings are protected with protective coverings not meeting the requirements of answer A, B or C OR systems that appear to meet answer A or B with no documentation of compliance (Level N in the table above).";
			 cf.showhelp(cf.alerttitle,cf.alertcontent);
			 break;
		 case R.id.helpoptE: 
			cf.alerttitle="X � None / Some Not Protected";
			cf.alertcontent="None or some glazed openings:One or more glazed openings classified as Level X in the table above";
			cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		case R.id.hme:
    		cf.gohome();
    		break;
		
		case R.id.save:			
				comm = edopcomments.getText().toString();
				if (GOcol1rdio[0].isChecked() == true
						|| GOcol1rdio[1].isChecked() == true
						|| GOcol1rdio[2].isChecked() == true
						|| GOcol1rdio[3].isChecked() == true
						|| GOcol1rdio[4].isChecked() == true
						|| GOcol1rdio[5].isChecked() == true
						|| GOcol1rdio[6].isChecked() == true
						|| GOcol1rdio[7].isChecked() == true) {
					if (GOcol2rdio[0].isChecked() == true
							|| GOcol2rdio[1].isChecked() == true
							|| GOcol2rdio[2].isChecked() == true
							|| GOcol2rdio[3].isChecked() == true
							|| GOcol2rdio[4].isChecked() == true
							|| GOcol2rdio[5].isChecked() == true
							|| GOcol2rdio[6].isChecked() == true
							|| GOcol2rdio[7].isChecked() == true) {
						if (GOcol3rdio[0].isChecked() == true
								|| GOcol3rdio[1].isChecked() == true
								|| GOcol3rdio[2].isChecked() == true
								|| GOcol3rdio[3].isChecked() == true
								|| GOcol3rdio[4].isChecked() == true
								|| GOcol3rdio[5].isChecked() == true
								|| GOcol3rdio[6].isChecked() == true
								|| GOcol3rdio[7].isChecked() == true) {
							if (GOcol4rdio[0].isChecked() == true
									|| GOcol4rdio[1].isChecked() == true
									|| GOcol4rdio[2].isChecked() == true
									|| GOcol4rdio[3].isChecked() == true
									|| GOcol4rdio[4].isChecked() == true
									|| GOcol4rdio[5].isChecked() == true
									|| GOcol4rdio[6].isChecked() == true
									|| GOcol4rdio[7].isChecked() == true) {
								if (NGOcol1rdio[0].isChecked() == true
										|| NGOcol1rdio[1].isChecked() == true
										|| NGOcol1rdio[2].isChecked() == true
										|| NGOcol1rdio[3].isChecked() == true
										|| NGOcol1rdio[4].isChecked() == true
										|| NGOcol1rdio[5].isChecked() == true
										|| NGOcol1rdio[6].isChecked() == true
										|| NGOcol1rdio[7].isChecked() == true) {
									if (NGOcol2rdio[0].isChecked() == true
											|| NGOcol2rdio[1].isChecked() == true
											|| NGOcol2rdio[2].isChecked() == true
											|| NGOcol2rdio[3].isChecked() == true
											|| NGOcol2rdio[4].isChecked() == true
											|| NGOcol2rdio[5].isChecked() == true
											|| NGOcol2rdio[6].isChecked() == true
											|| NGOcol2rdio[7].isChecked() == true) {
										if (rdiobtnoption[0].isChecked() == true
												|| rdiobtnoption[1].isChecked() == true
												|| rdiobtnoption[2].isChecked() == true
												|| rdiobtnoption[3].isChecked() == true
												|| rdiobtnoption[4].isChecked() == true) {
											
											if(rdiobtnoption[0].isChecked()){rdiochk="1";}
											else if(rdiobtnoption[1].isChecked()){rdiochk="2";}
											else if(rdiobtnoption[2].isChecked()){rdiochk="3";}
											else if(rdiobtnoption[3].isChecked()){rdiochk="4";}
											else if(rdiobtnoption[4].isChecked()){rdiochk="5";}
											else {rdiochk="";}
											
											if (rdiobtnoption[0].isChecked() == true) {
												optArdgpID=optArdgroup.getCheckedRadioButtonId();
												suboption = (optArdgpID==-1) ? "":((RadioButton) optArdgroup.findViewById(optArdgpID)).getTag().toString();
												
												if (((RadioButton)findViewById(R.id.rdioques1ans1)).isChecked() == false && 
														((RadioButton)findViewById(R.id.rdioques1ans2)).isChecked() == false && 
														((RadioButton)findViewById(R.id.rdioques1ans3)).isChecked() == false) {
													cf.ShowToast("Please select the option for All Glazed Openings - Large Missile 9 lb.",0);
													chkstatus[0] = "false";
												} else {
													chkstatus[0] = "true";
												}
											} else if (rdiobtnoption[1].isChecked()) {
												
												optBrdgpID=optBrdgroup.getCheckedRadioButtonId();
												suboption = (optBrdgpID==-1) ? "":((RadioButton) optBrdgroup.findViewById(optBrdgpID)).getTag().toString();
												if (((RadioButton)findViewById(R.id.rdioques2ans1)).isChecked() == false && 
														((RadioButton)findViewById(R.id.rdioques2ans2)).isChecked() == false && 
														((RadioButton)findViewById(R.id.rdioques2ans3)).isChecked() == false) {
													cf.ShowToast("Please select the option for All Glazed Openings 4 - 8 lb Large Missile.",0);
													chkstatus[1] = "false";
												} else {
													chkstatus[1] = "true";
												}
											} else if (rdiobtnoption[2].isChecked() == true) {
												optCrdgpID=optCrdgroup.getCheckedRadioButtonId();
												suboption = (optCrdgpID==-1) ? "":((RadioButton) optCrdgroup.findViewById(optCrdgpID)).getTag().toString();
												if (((RadioButton)findViewById(R.id.rdioques3ans1)).isChecked() == false && 
														((RadioButton)findViewById(R.id.rdioques3ans2)).isChecked() == false && 
														((RadioButton)findViewById(R.id.rdioques3ans3)).isChecked() == false) {
													cf.ShowToast("Please select the option for FBC Plywood.",0);
													chkstatus[2] = "false";
												} else {
													chkstatus[2] = "true";
												}
											} else if (rdiobtnoption[3].isChecked() == true) {
												optNrdgpID=optNrdgroup.getCheckedRadioButtonId();
												suboption = (optNrdgpID==-1) ? "":((RadioButton) optNrdgroup.findViewById(optNrdgpID)).getTag().toString();
												if (((RadioButton)findViewById(R.id.rdioques4ans1)).isChecked() == false && 
														((RadioButton)findViewById(R.id.rdioques4ans2)).isChecked() == false && 
														((RadioButton)findViewById(R.id.rdioques4ans3)).isChecked() == false) {
													cf.ShowToast("Please select the option for Opening Protection Not Verified.",0);
													chkstatus[3] = "false";
												} else {
													chkstatus[3] = "true";
												}
											}

											
											if (chkstatus[0] == "true"
													&& chkstatus[1] == "true"
													&& chkstatus[2] == "true"
													&& chkstatus[3] == "true") {
												
												if (comm.trim().equals("")) 
												{
													cf.ShowToast("Please enter the comments for Opening Protection.",0);
													cf.setFocus(edopcomments);
												}
												else
												{
													insertdata();
												}
											}
										} else {
											cf.ShowToast("Please select the option for Opening Protection.",0);

										}

									} else {
										cf.ShowToast("Please select Garage Doors for Non-Glazed Openings.",0);

									}
								} else {
									cf.ShowToast("Please select Entry Doors for Non-Glazed Openings.",0);

								}
							} else {
								cf.ShowToast("Please select Glass Block for Non-Glazed Openings",0);

							}
						} else {
							cf.ShowToast("Please select Skylights for Glazed Openings.",0);

						}
					} else {
						cf.ShowToast("Please select Garage Doors for Glazed Openings.",0);

					}
				} else {
					cf.ShowToast("Please select Windows or Entry Doors for Glazed Openings.",0);

				}

			
		
			break;
		case R.id.NAans1:
			Windowentrydoorsval = "NA";
			s = "answer1";
			poen_product_autoselct(s, 1);
			optionhidefirstcol();
		    ((RadioButton)findViewById(R.id.NAans1)).setChecked(true);
			break;

		case R.id.NAans2:
			s = "answer2";
			poen_product_autoselct(s, 1);
			Garagedoorsval = "NA";
			optionhidesecondcol();
		    ((RadioButton)findViewById(R.id.NAans2)).setChecked(true);
			break;

		case R.id.NAans3:
			s = "answer3";
			poen_product_autoselct(s, 1);
			Skylightsval = "NA";
			optionhidethirdcol();
			((RadioButton)findViewById(R.id.NAans3)).setChecked(true);
			break;

		case R.id.NAans4:
			s = "answer4";
			poen_product_autoselct(s, 1);
			Glassblocksval = "NA";
			Skylightsval = "NA";
			optionhidefourthcol();
			((RadioButton)findViewById(R.id.NAans4)).setChecked(true);
			break;

		case R.id.NAans5:
			s = "answer5";
			poen_product_autoselct(s, 1);
			Entrydoorsval = "NA";
			Skylightsval = "NA";
			optionhidefifthcol();
			((RadioButton)findViewById(R.id.NAans5)).setChecked(true);
			break;

		case R.id.NAans6:
			s = "answer6";
			poen_product_autoselct(s, 1);
			NGOGarageDoorsval = "NA";
			Skylightsval = "NA";
			optionhidesixthcol();
			((RadioButton)findViewById(R.id.NAans6)).setChecked(true);
			break;

		case R.id.rdioAans1:
			s = "answer1";
			poen_product_autoselct(s, 2);
			Windowentrydoorsval = "A";
			optionhidefirstcol();
			((RadioButton)findViewById(R.id.rdioAans1)).setChecked(true);
			break;

		case R.id.rdioAans2:
			s = "answer2";
			poen_product_autoselct(s, 2);
			Garagedoorsval = "A";
			optionhidesecondcol();
			((RadioButton)findViewById(R.id.rdioAans2)).setChecked(true);
			break;

		case R.id.rdioAans3:
			s = "answer3";
			poen_product_autoselct(s, 2);
			Skylightsval = "A";
			optionhidethirdcol();
			((RadioButton)findViewById(R.id.rdioAans3)).setChecked(true);
			break;

		case R.id.rdioAans4:
			s = "answer4";
			poen_product_autoselct(s, 2);
			Glassblocksval = "A";
			optionhidefourthcol();
			((RadioButton)findViewById(R.id.rdioAans4)).setChecked(true);
			break;

		case R.id.rdioAans5:
			s = "answer5";
			poen_product_autoselct(s, 2);
			Entrydoorsval = "A";
			optionhidefifthcol();
			((RadioButton)findViewById(R.id.rdioAans5)).setChecked(true);
			break;
		case R.id.rdioAans6:
			s = "answer6";
			poen_product_autoselct(s, 2);
			NGOGarageDoorsval = "A";
			optionhidesixthcol();
			((RadioButton)findViewById(R.id.rdioAans6)).setChecked(true);
			break;
		case R.id.rdioBans1:
			s = "answer1";
			poen_product_autoselct(s, 3);
			Windowentrydoorsval = "B";
			optionhidefirstcol();
			((RadioButton)findViewById(R.id.rdioBans1)).setChecked(true);
			break;
		case R.id.rdioBans2:
			s = "answer2";
			poen_product_autoselct(s, 3);
			Garagedoorsval = "B";
			optionhidesecondcol();
			((RadioButton)findViewById(R.id.rdioBans2)).setChecked(true);
			break;
		case R.id.rdioBans3:
			s = "answer3";
			poen_product_autoselct(s, 3);
			Skylightsval = "B";
			optionhidethirdcol();
			((RadioButton)findViewById(R.id.rdioBans3)).setChecked(true);
			break;

		case R.id.rdioBans4:
			s = "answer4";
			poen_product_autoselct(s, 3);
			Glassblocksval = "B";
			optionhidefourthcol();
			((RadioButton)findViewById(R.id.rdioBans4)).setChecked(true);
			break;

		case R.id.rdioBans5:
			s = "answer5";
			poen_product_autoselct(s, 3);
			Entrydoorsval = "B";
			optionhidefifthcol();
			((RadioButton)findViewById(R.id.rdioBans5)).setChecked(true);
			break;
		case R.id.rdioBans6:
			s = "answer6";
			poen_product_autoselct(s, 3);
			NGOGarageDoorsval = "B";
			optionhidesixthcol();
			((RadioButton)findViewById(R.id.rdioBans6)).setChecked(true);
			break;

		case R.id.rdioCans1:
			s = "answer1";
			poen_product_autoselct(s, 4);
			Windowentrydoorsval = "C";
			optionhidefirstcol();
			((RadioButton)findViewById(R.id.rdioCans1)).setChecked(true);
			break;
		case R.id.rdioCans2:
			s = "answer2";
			poen_product_autoselct(s, 4);
			Garagedoorsval = "C";
			optionhidesecondcol();
			((RadioButton)findViewById(R.id.rdioCans2)).setChecked(true);

			break;
		case R.id.rdioCans3:
			s = "answer3";
			poen_product_autoselct(s, 4);
			Skylightsval = "C";
			optionhidethirdcol();
			((RadioButton)findViewById(R.id.rdioCans3)).setChecked(true);
			break;
		case R.id.rdioCans4:
			s = "answer4";
			poen_product_autoselct(s, 4);
			Glassblocksval = "C";
			optionhidefourthcol();
			((RadioButton)findViewById(R.id.rdioCans4)).setChecked(true);

			break;
		case R.id.rdioCans5:
			s = "answer5";
			poen_product_autoselct(s, 4);
			Entrydoorsval = "C";
			optionhidefifthcol();
			((RadioButton)findViewById(R.id.rdioCans5)).setChecked(true);

			break;
		case R.id.rdioCans6:
			s = "answer6";
			poen_product_autoselct(s, 4);
			NGOGarageDoorsval = "C";
			optionhidesixthcol();
			((RadioButton)findViewById(R.id.rdioCans6)).setChecked(true);

			break;

		
		case R.id.rdioDans5:
			s = "answer5";
			poen_product_autoselct(s, 5);
			Entrydoorsval = "D";
			optionhidefifthcol();
			((RadioButton)findViewById(R.id.rdioDans5)).setChecked(true);

			break;
		case R.id.rdioDans6:
			s = "answer6";
			poen_product_autoselct(s, 5);
			NGOGarageDoorsval = "D";
			optionhidesixthcol();
			((RadioButton)findViewById(R.id.rdioDans6)).setChecked(true);

			break;

		case R.id.rdioN1ans1:
			s = "answer1";
			poen_product_autoselct(s, 6);
			Windowentrydoorsval = "N";
			optionhidefirstcol();
			((RadioButton)findViewById(R.id.rdioN1ans1)).setChecked(true);

			break;
		case R.id.rdioN1ans2:
			s = "answer2";
			poen_product_autoselct(s, 6);
			Garagedoorsval = "N";
			optionhidesecondcol();
			((RadioButton)findViewById(R.id.rdioN1ans2)).setChecked(true);

			break;
		case R.id.rdioN1ans3:
			s = "answer3";
			poen_product_autoselct(s, 6);
			Skylightsval = "N";
			optionhidethirdcol();
			((RadioButton)findViewById(R.id.rdioN1ans3)).setChecked(true);
			break;
		case R.id.rdioN1ans4:
			s = "answer4";
			poen_product_autoselct(s, 6);
			Glassblocksval = "N";
			optionhidefourthcol();
			((RadioButton)findViewById(R.id.rdioN1ans4)).setChecked(true);

			break;
		case R.id.rdioN1ans5:
			s = "answer5";
			poen_product_autoselct(s, 6);
			Entrydoorsval = "N";
			optionhidefifthcol();
			((RadioButton)findViewById(R.id.rdioN1ans5)).setChecked(true);
			break;
		case R.id.rdioN1ans6:
			s = "answer6";
			poen_product_autoselct(s, 6);
			NGOGarageDoorsval = "N";
			optionhidesixthcol();
			((RadioButton)findViewById(R.id.rdioN1ans6)).setChecked(true);
			break;
		case R.id.rdioN2ans1:
			s = "answer1";
			poen_product_autoselct(s, 7);
			Windowentrydoorsval = "NSUB1";
			optionhidefirstcol();
			((RadioButton)findViewById(R.id.rdioN2ans1)).setChecked(true);
			break;
		case R.id.rdioN2ans2:
			s = "answer2";
			poen_product_autoselct(s, 7);
			Garagedoorsval = "NSUB2";
			optionhidesecondcol();
			((RadioButton)findViewById(R.id.rdioN2ans2)).setChecked(true);
			break;
		case R.id.rdioN2ans3:
			s = "answer3";
			poen_product_autoselct(s, 7);
			Skylightsval = "NSUB3";
			optionhidethirdcol();
			((RadioButton)findViewById(R.id.rdioN2ans3)).setChecked(true);
			break;
		case R.id.rdioN2ans4:
			s = "answer4";
			poen_product_autoselct(s, 7);
			Glassblocksval = "NSUB4";
			optionhidefourthcol();
			((RadioButton)findViewById(R.id.rdioN2ans4)).setChecked(true);

			break;
		case R.id.rdioN2ans5:
			s = "answer5";
			poen_product_autoselct(s, 7);
			Entrydoorsval = "NSUB5";
			optionhidefifthcol();
			((RadioButton)findViewById(R.id.rdioN2ans5)).setChecked(true);

			break;
		case R.id.rdioN2ans6:
			s = "answer6";
			poen_product_autoselct(s, 7);
			NGOGarageDoorsval = "NSUB6";
			optionhidesixthcol();
			((RadioButton)findViewById(R.id.rdioN2ans6)).setChecked(true);

			break;

		case R.id.rdioXans1:
			s = "answer1";
			poen_product_autoselct(s, 8);
			Windowentrydoorsval = "X";
			optionhidefirstcol();
			((RadioButton)findViewById(R.id.rdioXans1)).setChecked(true);
			break;
			
		case R.id.rdioXans2:
			s = "answer2";
			poen_product_autoselct(s, 8);
			Garagedoorsval = "X";
			optionhidesecondcol();
			((RadioButton)findViewById(R.id.rdioXans2)).setChecked(true);
			break;
			
		case R.id.rdioXans3:
			s = "answer3";
			poen_product_autoselct(s, 8);
			Skylightsval = "X";
			optionhidethirdcol();
			((RadioButton)findViewById(R.id.rdioXans3)).setChecked(true);
			break;
			
		case R.id.rdioXans4:
			s = "answer4";
			poen_product_autoselct(s, 8);
			Glassblocksval = "X";
			optionhidefourthcol();
			((RadioButton)findViewById(R.id.rdioXans4)).setChecked(true);
			break;
			
		case R.id.rdioXans5:
			s = "answer5";
			poen_product_autoselct(s, 8);
			Entrydoorsval = "X";
			optionhidefifthcol();
			((RadioButton)findViewById(R.id.rdioXans5)).setChecked(true);
			break;
			
		case R.id.rdioXans6:
			s = "answer6";
			poen_product_autoselct(s, 8);
			NGOGarageDoorsval = "X";
			optionhidesixthcol();
			((RadioButton)findViewById(R.id.rdioXans6)).setChecked(true);
			break;
		case R.id.rdioques1ans1:
			commentsfill = "The opening protection to this home was verified as meeting the requirements of selection A for glazed openings and A1 for non glazed openings, meeting the large missile impact 9lb rating, for question 7  of the OIR B1 -1802, Opening Protection. Refer to attached photographs and documentation.";
			((EditText)findViewById(R.id.opcomment)).setText(commentsfill);
			suboption = "1";
			((RadioButton) findViewById(R.id.rdioques1ans1)).setChecked(true);
			break;
		case R.id.rdioques1ans2:
			commentsfill = "The opening protection to this home was verified as meeting the requirements of selection A for glazed openings and A2 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that all glazed openings were verified as impact rated. A non glazed garage door opening was however verified as wind only rated and not impact rated, being identified on the Opening Protection Level Chart as level D. Refer to attached photographs.";
			((EditText)findViewById(R.id.opcomment)).setText(commentsfill);
			suboption = "2";
			((RadioButton) findViewById(R.id.rdioques1ans2)).setChecked(true);
			break;
		case R.id.rdioques1ans3:
			commentsfill = "The opening protection to this home was verified as meeting the requirements of selection A for glazed openings and A3 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that all glazed openings were verified as impact rated. One or more non glazed openings did not meet level A, large 9 lb missile impact rating as identified on the Opening Protection Level Chart as level D. Refer to attached photographs.";
			((EditText)findViewById(R.id.opcomment)).setText(commentsfill);
			suboption = "3";
			((RadioButton) findViewById(R.id.rdioques1ans3)).setChecked(true);
			break;
		case R.id.rdioques2ans1:
			commentsfill = "The opening protection to this home was verified as meeting the requirements of selection B for glazed openings and B1 for non glazed openings, meeting the large 4-8lb  missile impact rating, for question 7  of the OIR B1 -1802, Opening Protection. Refer to attached photographs.";
			((EditText)findViewById(R.id.opcomment)).setText(commentsfill);
			suboption = "1";
			((RadioButton) findViewById(R.id.rdioques2ans1)).setChecked(true);
			break;
		case R.id.rdioques2ans2:
			commentsfill = "The opening protection to this home was verified as meeting the requirements of selection B for glazed openings and B2 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that all glazed openings were verified as 4-8lb missile impact rating. A non glazed garage door opening was however verified as wind only rated and not impact rated, being identified on the Opening Protection Level Chart as level D. Refer to attached photographs.";
			((EditText)findViewById(R.id.opcomment)).setText(commentsfill);
			suboption = "2";
			((RadioButton) findViewById(R.id.rdioques2ans2)).setChecked(true);
			break;
		case R.id.rdioques2ans3:
			commentsfill = "The opening protection to this home was verified as meeting the requirements of selection  B for glazed openings and B3 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that all glazed openings were verified as 4-8lb missile impact rating. One or more non glazed opening did not meet level B large missile, 4-8lb, impact rating as identified on the Opening Protection Level Chart. Refer to attached photographs.";
			((EditText)findViewById(R.id.opcomment)).setText(commentsfill);
			suboption = "3";
			((RadioButton) findViewById(R.id.rdioques2ans3)).setChecked(true);
			break;
		case R.id.rdioques3ans1:
			commentsfill = "The opening protection to this home was verified as meeting the requirements of selection B for glazed openings and B2 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that all glazed openings were verified as 4-8lb missile impact rating. A non glazed garage door opening was however verified as wind only rated and not impact rated, being identified on the Opening Protection Level Chart as level D. Refer to attached photographs.";
			((EditText)findViewById(R.id.opcomment)).setText(commentsfill);
			suboption = "1";
			((RadioButton) findViewById(R.id.rdioques3ans1)).setChecked(true);
			break;
		case R.id.rdioques3ans2:
			commentsfill = "The opening protection to this home was verified as meeting the requirements of selection C for glazed openings and C2 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that all openings to this home were verified as meeting the Florida Building Code requirements for wood structural panels apart from the non glazed garage door. This door opening was verified as wind only rated and not impact rated, and identified on the Opening Protection Level Chart as level D. Refer to attached photographs.";
			((EditText)findViewById(R.id.opcomment)).setText(commentsfill);
			suboption = "2";
			((RadioButton) findViewById(R.id.rdioques3ans2)).setChecked(true);
			break;
		case R.id.rdioques3ans3:
			commentsfill = "The opening protection to this home was verified as meeting the requirements of selection  C for glazed openings and C3 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that all glazed openings were verified as protected with wood structural panels that are compliant to the Florida Building Code. One or more non glazed opening did not meet level A,B or C impact rating as identified on the Opening Protection Level Chart. Refer to attached photographs.";
			((EditText)findViewById(R.id.opcomment)).setText(commentsfill);
			suboption = "3";
			((RadioButton) findViewById(R.id.rdioques3ans3)).setChecked(true);
			break;
		case R.id.rdioques4ans1:
			commentsfill = "The opening protection to this home was verified as meeting the requirements of selection N for glazed openings and N1 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection. This means that the opening protection was present to all openings that could not be verified as meeting the impact requirements of selection A or B. Refer to attached Photographs.";
			((EditText)findViewById(R.id.opcomment)).setText(commentsfill);
			suboption = "1";
			((RadioButton) findViewById(R.id.rdioques4ans1)).setChecked(true);
			break;
		case R.id.rdioques4ans2:
			commentsfill = "The opening protection to this home was verified as meeting the requirements of selection N for glazed openings and N2 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that we were unable to verify the opening protection as meeting large missile impact rating as outline in selection A and B, apart from the non glazed garage door. This door opening was verified as wind only rated and not impact rated, and identified on the Opening Protection Level Chart as level D. Refer to attached photographs.";
			((EditText)findViewById(R.id.opcomment)).setText(commentsfill);
			suboption = "2";
			((RadioButton) findViewById(R.id.rdioques4ans2)).setChecked(true);
			break;
		case R.id.rdioques4ans3:
			commentsfill = "The opening protection to this home was verified as meeting the requirements of selection  N for glazed openings and N3 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that some openings were not protected. Refer to attached photographs.";
			((EditText)findViewById(R.id.opcomment)).setText(commentsfill);
			suboption = "3";
			((RadioButton) findViewById(R.id.rdioques4ans3)).setChecked(true);
			break;
		}
	}
	private void Open_suboption() {
		// TODO Auto-generated method stub
		for(int i=0;i<=4;i++)
   	 	{
			if(rdiobtnoption[i].isChecked())
			{
				tmp=b1_q7[i];	
			}
   		}   	 		
	}
	private void insertdata() {
		// TODO Auto-generated method stub
		Cursor c1=cf.SelectTablefunction(cf.Wind_Questiontbl, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
		try
		{
			System.out.println("Windowentrydoorsval"+Windowentrydoorsval+"/n"+Garagedoorsval+"/n"+Skylightsval+"/n"+Glassblocksval+"/n"+Entrydoorsval+"/n"+NGOGarageDoorsval);
			
		if(c1.getCount()==0)
			 {
			 cf.arr_db.execSQL("INSERT INTO "
						+ cf.Wind_Questiontbl
						+ " (fld_srid,insp_typeid,fld_bcyearbuilt,fld_bcpermitappdate,fld_buildcode,fld_roofdeck,fld_rdothertext," +
						"	fld_roofwall,fld_roofwallsub,fld_roofwallclipsminval,fld_roofwallclipssingminval,fld_roofwallclipsdoubminval,fld_rwothertext," +
						"	fld_roofgeometry ,fld_geomlength ,fld_roofgeomtotalarea ,fld_roofgeomothertext ,fld_swr,fld_optype,fld_openprotect,fld_opsubvalue,fld_oplevelchart," +
						"   fld_opGOwindentdoorval,fld_opGOgardoorval,fld_opGOskylightsval,fld_opGOglassblockval,fld_opNGOentrydoorval,fld_opNGOgaragedoorval, " +
						"	fld_wcvalue,fld_wcreinforcement,fld_quesmodifydate)"
					
					+ "VALUES ('" + cf.selectedhomeid + "','"+cf.identityval+"','','','','" + 0 + "','','" +
					+ 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0	+ "','','"+
					+ 0 + "','" + 0 + "','" + 0 + "','','" + 0 + "','"+ 0 +"','"+ rdiochk + "','" + suboption + "','" + 0 + "','" +
					Windowentrydoorsval + "','" + Garagedoorsval + "','" + Skylightsval + "','" + Glassblocksval + "','" + Entrydoorsval + "','"+ NGOGarageDoorsval +"','" +
					+ 0 + "','','"+ cf.datewithtime + "')");
			 }
			 else
			 {
						 cf.arr_db.execSQL("UPDATE " + cf.Wind_Questiontbl
									+ " SET fld_openprotect ='" + rdiochk
									+ "',fld_opsubvalue='" + suboption
									+ "',fld_opGOwindentdoorval='"+ Windowentrydoorsval+ "',fld_opGOgardoorval='"+ Garagedoorsval+ "',fld_opGOskylightsval='"+ Skylightsval
									+ "',fld_opGOglassblockval='"+ Glassblocksval+ "',fld_opNGOentrydoorval='"+ Entrydoorsval+ "',fld_opNGOgaragedoorval='"+ 
									NGOGarageDoorsval+ "'  WHERE fld_srid ='" + cf.selectedhomeid
									+ "' and insp_typeid='"+cf.identityval+"'");
			 } 
			 Cursor c2 = cf.SelectTablefunction(cf.Wind_QuesCommtbl, "where fld_srid='"+ cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'");
				if (c2.getCount() == 0) 
				{
					cf.arr_db.execSQL("INSERT INTO "
							+ cf.Wind_QuesCommtbl
							+ " (fld_srid,insp_typeid,fld_buildcodecomments,fld_roofcovercomments,fld_roofdeckcomments,fld_roofwallcomments," +
							"fld_roofgeometrycomments,fld_swrcomments,fld_openprotectioncomments,fld_wallconscomments,fld_insoverallcomments )"
							+ "VALUES('"+cf.selectedhomeid+"','"+cf.identityval+"','','','','','','','"+cf.encode(edopcomments.getText().toString())+"','','')");
				}
				else
				{
					cf.arr_db.execSQL("UPDATE " + cf.Wind_QuesCommtbl
							+ " SET fld_openprotectioncomments='"+cf.encode(edopcomments.getText().toString())+ "' WHERE fld_srid ='" + cf.selectedhomeid + "'  and insp_typeid='"+cf.identityval+"'");
				}				
				if(((Button)findViewById(R.id.save)).getText().toString().equals("Save"))
				{
					if(cf.identityval==31)
					{
						cf.ShowToast("Opening Protection details saved successfully.",0);
						cf.gotoclass(cf.identityval, WindMitWallCons.class);
					}
					else
					{
						cf.ShowToast("Opening Protection details saved successfully.",0);
						cf.gotoclass(cf.identityval, WindMitCommareas.class);
					}
				}															
				else
				{
					cf.ShowToast("Opening Protection details saved successfully.",0);
					cf.gotoclass(cf.identityval, WindMitWallCons.class);
				}
		}
		catch(Exception e)
		{
			System.out.println("insetrupdate"+e.getMessage());
		}	
	}
	private void getcountname() {
		/*try {
			Cursor cur = cf.db.rawQuery(
					"select * from Retail_inpgeneralinfo where s_SRID='"
							+ cf.Homeid + "'", null);

			if(cur.getCount()>0){
			cur.moveToFirst();
			if (cur != null) {

				count_value = cur.getString(cur.getColumnIndex("s_County")).toLowerCase();

			}
			}
		} catch (Exception e) {
			//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ OpenProtect.this +" problem in retrieving county name on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

		}*/
	}
	class clicker1 implements OnClickListener {
		public void onClick(View v) {


			if (v == ((TextView)findViewById(R.id.miamedadeprodapprlink)))
			{
					if (cf.isInternetOn() == true) {
						weblink = true;
						setContentView(R.layout.weblink);
						WebView webview1 = (WebView) findViewById(R.id.webview);
						webview1.getSettings().setJavaScriptEnabled(true);
						webview1.loadUrl("http://www.miamidade.gov/building/pc-search_app.asp");
					//cf.goclass(30);
					} else {
					alert();
					}
			}
			else if (v == ((TextView)findViewById(R.id.fbcprodapprlink))) {
					if (cf.isInternetOn() == true) {
						weblink = true;
						setContentView(R.layout.weblink);						
						WebView webview1 = (WebView) findViewById(R.id.webview);
						webview1.getSettings().setJavaScriptEnabled(true);
						webview1.loadUrl("http://www.floridabuilding.org/pr/pr_app_srch.aspx");
					//cf.goclass(30);
					} else {
					alert();
					}

			}
			else if (v == ((TextView)findViewById(R.id.viewtxt1))) {
				cf.alerttitle="View Text";
				cf.alertcontent="1. Miame-Dade County PA 201, 202 and 203.<br/> 2. Florida Building Code Testing Application Standard (TAS) 201, 202 and 203 <br/>3. American Society for Testing and Materials (ASTM) E 1886 ASTM E 1996 <br/>4. Southern Standards Technical Document (SSTD) 12.<br/>5 For Skylights Only: ASTM E 1886 and ASTM E-1996 6.For Garage Doors Only: ANSI/DASMA 115).";
				cf.showhelp(cf.alerttitle,cf.alertcontent);
			}
			else if (v == ((TextView)findViewById(R.id.viewtxt2))) {
				cf.alerttitle="View Text";
				cf.alertcontent="1. ASTM E 1886 and ASTM E 1996 (Large Missile - 4.5 lb.) <br/>2. SSTD 12 (Large Missile - 4 lb. to 8lb.)<br/> 3. For Skylights Only: ASTM E 1886 and ASTM E 1996 (Large Missile - 2 to 4.5 lb.)";
				cf.showhelp(cf.alerttitle,cf.alertcontent);
			}
		}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
 		// replaces the default 'Back' button action
 		if (keyCode == KeyEvent.KEYCODE_BACK) {
 			if(weblink==true)
 			{
 				cf.gotoclass(cf.identityval, WindMitOpenProt.class);
 			}
 			else
 			{
 				cf.gotoclass(cf.identityval, WindMitSkylights.class);	
 			}			
 			return true;
 		}
 		
 		return super.onKeyDown(keyCode, event);
 	}
	/**on activity result for the load comments**/
	 @Override
	 		protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	 		try
	 		{	
	 			if(requestCode==cf.loadcomment_code)
	 			{
	 				load_comment=true;
	 				if(resultCode==RESULT_OK)
		 			{
		 				String openprotcomments = edopcomments.getText().toString();
		 				edopcomments.setText(openprotcomments +" "+data.getExtras().getString("Comments"));		 				
		 			}
	 			}
	 		}
	 		catch (Exception e) {
	 			// TODO: handle exception
	 			System.out.println("the erro was "+e.getMessage());
	 		}
	 	}/**on activity result for the load comments ends**/
}