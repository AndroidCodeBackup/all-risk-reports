package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;

public class BIFoundation extends Activity {
	CommonFunctions cf;
	String foundationchk_value="",founnotappl="",Foundation="",Foundation_NA="";
	public CheckBox foundationchk[] = new CheckBox[10];
	Cursor c1;
	public int[] chkfoundationid = {R.id.foundation_chk1,R.id.foundation_chk2,R.id.foundation_chk3,R.id.foundation_chk4,R.id.foundation_chk5,R.id.foundation_chk6,R.id.foundation_chk7,R.id.foundation_chk8,R.id.foundation_chk9,R.id.foundation_chk10};
	
	public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	       
	         cf = new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);				
		 	}
	        setContentView(R.layout.foundation);
	        cf.getDeviceDimensions();
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
         	hdr_layout.addView(new HdrOnclickListener(this,1,"General","Building Info","Foundation",1,1,cf));
         	LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
         	main_layout.setMinimumWidth(cf.wd);
         	main_layout.addView(new MyOnclickListener(getApplicationContext(), 1, 0,0,cf));
            LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
            submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 16, 1,0,cf));
	        LinearLayout submenu2_layout = (LinearLayout) findViewById(R.id.submenu1); 
	        submenu2_layout.addView(new MyOnclickListener(getApplicationContext(), 454, 1,0,cf));
	       /* TableRow tblrw = (TableRow)findViewById(R.id.row2);
	        tblrw.setMinimumHeight(cf.ht); 		*/
	        Declaration();
	        cf.CreateARRTable(61);
	        setValue();
	    }
	private void Declaration() {
		// TODO Auto-generated method stub
		cf.setupUI((ScrollView) findViewById(R.id.scr));
		 for(int i=0;i<foundationchk.length;i++)
	   	 {
	   		foundationchk[i] = (CheckBox)findViewById(chkfoundationid[i]);
	   	 }		
		 ((EditText)findViewById(R.id.edfoundation_other)).addTextChangedListener(new CustomTextWatcherSpace(((EditText)findViewById(R.id.edfoundation_other))));
		  
	}
	private void setValue() {
		// TODO Auto-generated method stub
		selecttbl();
		if(cf.C_GEN.getCount()==0)
		{
			((CheckBox)findViewById(R.id.founationnotapplicable)).setChecked(true);
			((LinearLayout)findViewById(R.id.foundationlin)).setVisibility(cf.v1.GONE);
		}
		else
		{
			if(Foundation_NA.equals("0") || Foundation_NA.equals(""))
			{
				
				if(!Foundation.equals(""))
				{
					((LinearLayout)findViewById(R.id.foundationlin)).setVisibility(cf.v1.VISIBLE);
					cf.setvaluechk1(Foundation,foundationchk,((EditText)findViewById(R.id.edfoundation_other)));
					((TextView)findViewById(R.id.savetxtfound)).setVisibility(cf.v1.VISIBLE);
				}
				else
				{
					((LinearLayout)findViewById(R.id.foundationlin)).setVisibility(cf.v1.GONE);
					((TextView)findViewById(R.id.savetxtfound)).setVisibility(cf.v1.GONE);
					((CheckBox)findViewById(R.id.founationnotapplicable)).setChecked(true);
				}
			}
			else
			{
				((CheckBox)findViewById(R.id.founationnotapplicable)).setChecked(true);
				((LinearLayout)findViewById(R.id.foundationlin)).setVisibility(cf.v1.GONE);
				((TextView)findViewById(R.id.savetxtfound)).setVisibility(cf.v1.VISIBLE);
			}
		}
	}
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.mouseoverid:
			cf.ShowToast("Use for 4 Point, Commercial Mitigation,Sinkhole, Replacement Cost Inspections.",0);
			break;
		case R.id.foundation_chk10:
			if(((CheckBox)findViewById(R.id.foundation_chk10)).isChecked())
			{
				((EditText)findViewById(R.id.edfoundation_other)).setVisibility(cf.v1.VISIBLE);
				cf.setFocus((EditText)findViewById(R.id.edfoundation_other));
			}
			else
			{
				((EditText)findViewById(R.id.edfoundation_other)).setVisibility(cf.v1.GONE);
				((EditText)findViewById(R.id.edfoundation_other)).setText("");
			}				
			break;
		case R.id.foundationsave:
			selecttbl();
			if(((CheckBox)findViewById(R.id.founationnotapplicable)).isChecked()==false)
			{
				foundationchk_value= cf.getselected_chk(foundationchk);
				 String foundtaionotrval =(foundationchk[foundationchk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.edfoundation_other)).getText().toString():""; 
				 foundationchk_value+=foundtaionotrval;
					if(foundationchk_value.equals(""))
					 {
						 cf.ShowToast("Please select the option for Foundation." , 0);
					 }
					 else
					 {
						 if(foundationchk[foundationchk.length-1].isChecked())
						 {	 
							 if(foundtaionotrval.trim().equals("&#40;"))
							 {
								 cf.ShowToast("Please enter the other text for Foundation.", 0);
								cf.setFocus((EditText)findViewById(R.id.edfoundation_other));
							 }
							 else
							 {
								 InsertFoundation();
							 }							 
						 }
						 else
						 {
							 InsertFoundation();	
						 }
					 }
			}
			else
			{
				 InsertFoundation();	
			}
			break;
		case R.id.founationnotapplicable:
			if(((CheckBox)findViewById(R.id.founationnotapplicable)).isChecked())
			{
				founnotappl="1";
				if(!Foundation.equals(""))
				{
					AlertDialog.Builder builder = new AlertDialog.Builder(BIFoundation.this);
					builder.setTitle("Confirmation");
					builder.setIcon(R.drawable.alertmsg);
					builder.setMessage("Do you want to Clear the Foundation data?").setCancelable(false)
							.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,int id1) 
										{
												foundationclear();
												((CheckBox)findViewById(R.id.founationnotapplicable)).setChecked(true);
												((LinearLayout)findViewById(R.id.foundationlin)).setVisibility(cf.v1.GONE);
												((TextView)findViewById(R.id.savetxtfound)).setVisibility(cf.v1.GONE);
										}
									})
							.setNegativeButton("No",new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,	int id) {											
											((CheckBox)findViewById(R.id.founationnotapplicable)).setChecked(false);
											dialog.cancel();											
									}
							});
					builder.show();
				}
				else
				{
					((TextView)findViewById(R.id.savetxtfound)).setVisibility(cf.v1.GONE);
					((LinearLayout)findViewById(R.id.foundationlin)).setVisibility(cf.v1.GONE);
				}				
			}
			else
			{
				founnotappl="0";foundationclear();
				((LinearLayout)findViewById(R.id.foundationlin)).setVisibility(cf.v1.VISIBLE);
				((TextView)findViewById(R.id.savetxtfound)).setVisibility(cf.v1.GONE);
			}
			break;
		}		
	}
	private void selecttbl() {
		c1 = cf.SelectTablefunction(cf.BI_General, " where fld_srid='"+cf.selectedhomeid+"'");
		if(c1.getCount()>0)
		{
			c1.moveToFirst();
			Foundation_NA = c1.getString(c1.getColumnIndex("BI_FOUN_NA"));
			Foundation = cf.decode(c1.getString(c1.getColumnIndex("BI_FOUN")));
		}
	}
	protected void foundationclear() {
		// TODO Auto-generated method stub
		for(int i=0;i<foundationchk.length;i++)
		{
			foundationchk[i].setChecked(false);
		}
		((EditText)findViewById(R.id.edfoundation_other)).setText("");
		((EditText)findViewById(R.id.edfoundation_other)).setVisibility(cf.v1.GONE);Foundation="";foundationchk_value="";
	}
	private void InsertFoundation() {
		// TODO Auto-generated method stub
		if(((CheckBox)findViewById(R.id.founationnotapplicable)).isChecked()){founnotappl="1";foundationchk_value="";}else{founnotappl="0";}
		updatenotappl();
		cf.ShowToast("Foundation saved successfully",1);
		//((TextView)findViewById(R.id.savetxtfound)).setVisibility(cf.v1.VISIBLE);
		cf.goclass(455);
	}
	private void updatenotappl()
	{
		try
		{			
			selecttbl();
			if(c1.getCount()==0)
			{
				cf.arr_db.execSQL("INSERT INTO " + cf.BI_General + " (fld_srid,BI_FOUN,BI_FOUN_NA)"
						+ "VALUES ('"+cf.selectedhomeid+"','"+cf.encode(foundationchk_value)+"','"+founnotappl+"')");
			}
			else
			{
				cf.arr_db.execSQL("UPDATE "
						+ cf.BI_General
						+ " SET BI_FOUN='"+cf.encode(foundationchk_value)+"',BI_FOUN_NA='"+founnotappl+"'"
						+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
			}
		}
		catch(Exception e)
		{
			System.out.println("ins "+e.getMessage());
		}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			cf.goclass(453);
			return true;
		}		
		return super.onKeyDown(keyCode, event);
	}
}