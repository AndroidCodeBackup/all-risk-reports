package idsoft.inspectiondepot.ARR;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeoutException;

import org.kobjects.base64.Base64;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class HomeScreen extends Activity{
	CommonFunctions cf;
	GetQuestionsintext cd;byte[] raw;
	int maxBarValue = 0,typeid=0; // Maximum value of horizontal progress bar
	 public String HomeId,InspectorId,ScheduledDate,ScheduledCreatedDate,AssignedDate,Schedulecomments,InspectionStartTime,InspectionEndTime,
     PH_Fname,PH_Lname,PH_Mname,PH_Address1,PH_Address2,PH_City,PH_State,PH_Zip,PH_County,PH_Inspectionfees,PH_CONTPERSON,CompID,YearBuilt,
     PH_InsuranceCompany,PH_HPhone,PH_WPhone,PH_CPhone,PH_Email,PH_Policyno,PH_Status,PH_SubStatus,PH_Noofstories,IsInspected,BuildingSize,
     PH_InspectionTypeId,PH_IsInspected="0",PH_IsUploaded="0",PH_EmailChk="0",PH_InspectionName,insp_name="",latitude,longtitude,PH_MainInspectionTypeId,
    		  MailingAddress,MailingAddress2,CommercialFlag,Mailingcity,MailingState,MailingCounty,Mailingzip,BuildingNo;
	MultiSpinner multiSpinner;
	Thread myThread = null;
	
	String sel_inspection,formattedDate2="";
	ProgressDialog progressDialog2,progressDialog,progressDialog1,progDialog;
	int k=0,total,usercheck,vcode,typeBar,mState;
	String newcode="",newversion="";
	public ProgressThread progThread;
	public static int RUNNING = 1;
	public int delay = 40;
	AlertDialog alertDialog;
	ProgressDialog pd;
	private int messsage=0;
	@Override
	   public void onCreate(Bundle savedInstanceState) {

	        super.onCreate(savedInstanceState);
	        cf=new CommonFunctions(this);
	        cd=new GetQuestionsintext(this);
	        setContentView(R.layout.home);
	        progDialog = new ProgressDialog(this);
	        cf.getInspectorId();
	        vcode = getcurrentversioncode();
	        cf.CreateARRTable(0);
	        

	        if(cf.application_sta)
	        {
	        	Bundle extras = getIntent().getExtras();
	    		if (extras != null)
	    		{
	    			try 
	    			{
	    				String value = extras.get("upgrade").toString();
	    				final String newversion1 = extras.get("newversion").toString();
	    				if (value.equals("true")) {
	    					alertDialog = new AlertDialog.Builder(HomeScreen.this)
	    							.create();
	    					alertDialog.setMessage("New Version "+newversion1+" of All Risk Reports inspection is available. The Previous data will not be affected , Would you like to upgrade ?");
	    					alertDialog.setButton("Yes",new DialogInterface.OnClickListener() {
	    								public void onClick(DialogInterface dialog,int which) {
	    									try {
	    										if (alertDialog.isShowing()) {

	    											alertDialog.cancel();

	    											alertDialog.dismiss();
	      
	    										}
	    										String source = "<b><font color=#FFFFFF>" + "Please wait Upgrading All Risk Reports inspection version "
	    												+ "</font></b>";
	    										progressDialog1 = ProgressDialog.show(HomeScreen.this, "",Html.fromHtml(source),true);
	    										new Thread() {
	    											public void run() {
	    												// Looper.prepare();
	    												try {
	    													Update();
	    													
	    												} catch (SocketTimeoutException s) {
	    													k = 5;
	    													handler.sendEmptyMessage(0);
	    												} catch (NetworkErrorException n) {
	    													k = 5;
	    													handler.sendEmptyMessage(0);

	    												} catch (IOException io) {
	    													k = 5;
	    													handler.sendEmptyMessage(0);

	    												} catch (XmlPullParserException x) {
	    													k = 5;
	    													handler.sendEmptyMessage(0);

	    												} catch (Exception e) {
	    													k = 8;
	    													handler.sendEmptyMessage(0);
	    												}
	    											};

	    											private Handler handler = new Handler() {
	    												public void handleMessage(
	    														Message msg) {
	    													progressDialog1.dismiss();

	    													if (k == 2) {
	    														cf.ShowToast("Internet connection is not available.",1);

	    													} else if (k == 8) {
	    														cf.ShowToast("Error in login please try again latter .",1);

	    													} else if (k == 5) {
	    														cf.ShowToast("There is a problem on your Network. Please try again later with better Network..",1);

	    													}

	    												}
	    											};
	    										}.start();

	    									} catch (Exception e) {
	    										System.out
	    												.println("problme while updgrde"
	    														+ e);
	    									}
	    								}
	    							});
	    					
	    					  alertDialog.setButton2("No", new DialogInterface.OnClickListener() {  
	    					      public void onClick(DialogInterface dialog, int which) 
	    					      {  
	    					    	  alertDialog.cancel();
	    					      }
	    					    }); 
	    					
	    					alertDialog.show();
	    				}

	    			} catch (Exception e) {
	    				System.out.println("errorsdsd" + e.getMessage());
	    			}
	    		}
	        }else
		        {
		        	AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(HomeScreen.this);
						alertDialog1
						.setMessage("In order to use the latest version of All Risk Reports Inspection, You must update IDMA. Please click Ok to update IDMA.");
					alertDialog1.setCancelable(false);
		          	alertDialog1.setPositiveButton("Ok",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int which) {
								progressDialog2 = ProgressDialog.show(HomeScreen.this, "",
									"Downloading. Please wait...");
							new Thread() {
								private int usercheck = 0;
								private int myhandler;

								public void run() {
									Looper.prepare();
									SoapObject webresult;
									SoapObject request = new SoapObject(cf.NAMESPACE, "GeAPKFile_IDMS");
									
									SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
											SoapEnvelope.VER11);
									envelope.dotNet = true;
									request.addProperty("InspectorID",cf.Insp_id);
									envelope.setOutputSoapObject(request);
									      
									HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL_IDMA);

									try {
										androidHttpTransport.call(cf.NAMESPACE+"GeAPKFile_IDMS", envelope);
										Object response = envelope.getResponse();
										System.out.println("the responce"+response);
										byte[] b;
										if (cf.checkresponce(response)) // check the response is valid or
																		// not
										{
											b = Base64.decode(response.toString());

											try {
												String PATH = Environment.getExternalStorageDirectory()
														+ "/Download/IDMA";
												File file = new File(PATH);
												file.mkdirs();

												File outputFile = new File(PATH + "/IDMA.apk");
												FileOutputStream fileOuputStream = new FileOutputStream(outputFile);
												fileOuputStream.write(b);
												fileOuputStream.close();

												//cf.fn_logout(cf.Insp_id);  //FOR LOGOUT
												
												myhandler=2;
												
												
											} catch (IOException e) {
												System.out.println("catch");
												myhandler=1;
											}
										}
										else {
											
											myhandler=0;
											}
									} catch (IOException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
										myhandler=0;
									} catch (XmlPullParserException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
										myhandler=0;
									}
									handler2.sendEmptyMessage(0);
								};
								private Handler handler2 = new Handler() {
									

									@Override
									public void handleMessage(Message msg) {
										
										progressDialog2.dismiss();
										if(myhandler==0)
										{
											cf.ShowToast("There is a problem on your Network.\n Please try again later with better Network.",0);
											
										}
										else if(myhandler==1)
										{
											cf.ShowToast("Update error!",0);
											
										}
										else if(myhandler==2)
										{
											Intent intent = new Intent(Intent.ACTION_VIEW);
											intent.setDataAndType(Uri.fromFile(new File(Environment
													.getExternalStorageDirectory()
													+ "/Download/IDMA/"
													+ "IDMA.apk")),
													"application/vnd.android.package-archive");
											startActivity(intent);
										}
									}
								};}.start();
						
		
						}
					});
		       
		          	alertDialog1.show();
		          	}
	        	
	       Runnable runnable = new CountDownRunner();
			myThread = new Thread(runnable);
			myThread.start();
			try 
			{
				String vn = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
				((TextView)findViewById(R.id.versionname)).setText("Version "+vn);
			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			 Calendar c = Calendar.getInstance();
			SimpleDateFormat df2 = new SimpleDateFormat("MM-dd-yyyy");			
			formattedDate2 = df2.format(c.getTime());
			((TextView)findViewById(R.id.welcomename)).setText(Html.fromHtml("<font color=#f7a218>" + "Welcome "+"</font>"+ cf.Insp_firstname.toUpperCase() +" "+cf.Insp_lastname.toUpperCase()));
			((TextView)findViewById(R.id.insptype)).setText(Html.fromHtml("<font color=#f7a218>" + "Inspection Type : "+"</font> All Risk Reports"));
			ImageView im =(ImageView) findViewById(R.id.head_insp_info);
			im.setVisibility(View.VISIBLE);
			im.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					    Intent s2= new Intent(HomeScreen.this,PolicyholdeInfoHead.class);
						Bundle b2 = new Bundle();
						s2.putExtra("homeid", cf.selectedhomeid);
						s2.putExtra("Type", "Inspector");
						s2.putExtra("insp_id", cf.Insp_id);
						((Activity) cf.con).startActivityForResult(s2, cf.info_requestcode);
				}
			});
			Animating_button();    
	}
	
	public void Animating_button() {
			// TODO Auto-generated method stub
			/** Start animating for the  Check for updates**/
			 cf.CreateARRTable(0);
				Cursor c12 = cf.SelectTablefunction(cf.ARRVersion, " ");   
				c12.moveToFirst();
				System.out.println("there is no more issues"+c12.getCount());
				if (c12.getCount() >= 1) {
					System.out.println("there is no more issues cones in the count db er"+c12.getString(1)+" app ver "+vcode);
					
					if (!c12.getString(1).trim().equals(String.valueOf(vcode).trim())) {
						try
						{
							
							int v_c=Integer.parseInt(c12.getString(1).trim());
							System.out.println("v_c "+v_c + " vcode" +vcode);
							if(v_c>vcode)
							{
								System.out.println("inside andima");
								
								//animatebutton(); // use the function for change the image for a
								final ScaleAnimation zoom = new ScaleAnimation((float)1.00,(float)0.90, (float)1.00,(float)0.90,(float) 100,(float) 20); 
								final AnimationSet animSet = new AnimationSet(false);
								zoom.setRepeatMode(Animation.REVERSE);
								zoom.setRepeatCount(Animation.INFINITE);
								animSet.addAnimation(zoom);
								animSet.setDuration(300);
								AlphaAnimation alpha = new AlphaAnimation(1, (float)0.9);
						        alpha.setDuration(300); // Make animation instant
						        alpha.setRepeatMode(Animation.REVERSE);
						        alpha.setRepeatCount(Animation.INFINITE);
						        animSet.addAnimation(alpha);
						        ((ImageView)findViewById(R.id.img_update)).setAnimation(animSet);
								animSet.start();
							}
						}
						catch(Exception e)
						{
							
						}
					
					} 
				}
				/** End animating for the  Check foru pdates**/
				/** Start animating for the  Check for new inspection starts **/
				 cf.CreateARRTable(20);
					Cursor c20 = cf.SelectTablefunction(cf.IDMAVersion, " ");   
					c20.moveToFirst();
					if(c20.getCount()>=1)
					{
					int old_v_c=0,new_v_c=0;
					for(int i=0;i<c20.getCount();i++)
					{/**New for the Version code in the applicaation **/
						 if(c20.getString(c20.getColumnIndex("ID_VersionType")).equals("IDMA_Old")) 
						 {
							 old_v_c=c20.getInt(c20.getColumnIndex("ID_VersionCode")); 
							 
						 } /**Old  for the Version code in the application which in the server to update**/
						 else if(c20.getString(c20.getColumnIndex("ID_VersionType")).equals("IDMA_New"))
						 {
							 new_v_c=c20.getInt(c20.getColumnIndex("ID_VersionCode")); 
						 }
						
						 c20.moveToNext();
				    }
					
						if (old_v_c<new_v_c || old_v_c==0) {
							try
							{
								
								
									//animatebutton(); // use the function for change the image for a
									final ScaleAnimation zoom1 = new ScaleAnimation((float)1.00,(float)0.90, (float)1.00,(float)0.90,(float) 100,(float) 20); 
									final AnimationSet animSet1 = new AnimationSet(false);
									zoom1.setRepeatMode(Animation.REVERSE);
									zoom1.setRepeatCount(Animation.INFINITE);
									animSet1.addAnimation(zoom1);
									animSet1.setDuration(300);
									AlphaAnimation alpha1 = new AlphaAnimation(1, (float)0.9);
							        alpha1.setDuration(300); // Make animation instant
							        alpha1.setRepeatMode(Animation.REVERSE);
							        alpha1.setRepeatCount(Animation.INFINITE);
							        animSet1.addAnimation(alpha1);
							        
									((ImageView) findViewById(R.id.HS_chk_new_insp)).setAnimation(animSet1);
									animSet1.start();
									System.out.println("No more issues ended");
								
							}
							catch(Exception e)
							{
								
							}
						
					 
					}
				}
					
				/** Ends animating for the  Check for new inspection starts **/
				/** Start animating for the  Inspector information  **/
					
					
				
		}
	class CountDownRunner implements Runnable {
		// @Override
		public void run() {
			while (!Thread.currentThread().isInterrupted()) {
				try {
					doWork();
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
				} catch (Exception e) {
				}
			}
		}
	}
	public void doWork() {
		runOnUiThread(new Runnable() {
			public void run() {
				try {
					TextView txtCurrentTime = (TextView) findViewById(R.id.txtdate);
					Date dt = new Date();
					int hours = dt.getHours();
					int minutes = dt.getMinutes();
					int seconds = dt.getSeconds();
					String curTime = hours + ":" + minutes + ":" + seconds;
					
					txtCurrentTime.setText(formattedDate2 + " " + curTime);
				} catch (Exception e) {
				}
			}
		});
	}
	 public void clicker(View v)
	 {Intent in;
	 int[] loc = new int[2];
		 switch(v.getId())
		 {
		 case R.id.show:
			 ((Button)findViewById(R.id.show)).clearAnimation();
			 startActivity(new Intent(HomeScreen.this,ShowInspector.class));			
			 break;
		 case R.id.HS_chk_new_insp:
			 getIDMA_application();
			 break;
		 case R.id.img_startinspection:
			  startActivity(new Intent(getApplicationContext(),Dashboard.class));
			  break;
		 case R.id.img_deleteinspection:
			 cf.CreateARRTable(3);
			 String sql = "select * from " + cf.policyholder + " where ARR_PH_InspectorId = '" + cf.encode(cf.Insp_id) + "' and ARR_PH_Status!='90'";
			 Cursor cur = cf.arr_db.rawQuery(sql, null);
			 if(cur.getCount()==0)
			 {
				 cf.ShowToast("No Records Found.",0);
			 }
			 else
			 {
				 startActivity(new Intent(getApplicationContext(),Deleteinspection.class));
			 }  
			  break;
		 case R.id.img_import:
			if(cf.isInternetOn()==true) 
			 {
				total = 0;
				showDialog(1);
				start_import();					
			}
			else 
			{
				usercheck = 1;
				handler.sendEmptyMessage(0);
			}
			 break;
		 case R.id.img_export:
			 cf.CreateARRTable(3);
			 Cursor c1 = cf.arr_db.rawQuery("select * from " + cf.policyholder + " where ARR_PH_InspectorId='" + cf.encode(cf.Insp_id)+ "' and ARR_PH_IsInspected='1'",null);
			 if(c1.getCount()==0)
			 {
				 cf.ShowToast("No Records Found.", 0);
			 }
			 else
			 {
				 Intent inte = new Intent(getApplicationContext(),Export.class);
				  inte.putExtra("type", "export");
				  startActivity(inte); 
			 } 
			 break;
		 case R.id.img_search:
			 online_search();
			 break;
		 case R.id.img_update:
			 if (cf.isInternetOn()) {	
				    String source = "<b><font color=#FFFFFF>" + "Checking for updates... Please wait."
							+ "</font></b>";
					progressDialog = ProgressDialog.show(HomeScreen.this, "",Html.fromHtml(source),true);
					
					new Thread() {
						private int usercheck = 0;

						public void run() {
							 Looper.prepare();
							try {
								if (getversioncodefromweb_arr()== true) {
									System.out.println("getvertrue");
									if (k != 5 && k != 8) {
										usercheck = 1;
										
									}
									handler.sendEmptyMessage(0);
									progressDialog.dismiss();
								} else {
									
									progressDialog.dismiss();
									Intent imp = new Intent(HomeScreen.this,
											HomeScreen.class);
									imp.putExtra("newversion", newversion);
									imp.putExtra("upgrade", "true");
									imp.putExtra("userpermitted", "false");
									startActivity(imp);
									
								}

							} catch (SocketTimeoutException s) {
								k = 5;
								handler.sendEmptyMessage(0);
							} catch (NetworkErrorException n) {
								k = 5;
								handler.sendEmptyMessage(0);
							} catch (IOException io) {
								k = 5;
								handler.sendEmptyMessage(0);
							} catch (XmlPullParserException x) {
								
								k = 5;
								handler.sendEmptyMessage(0);
							} catch (Exception e) {
								progressDialog.dismiss();
								k = 8;
								handler.sendEmptyMessage(0);
							}
						};
						 public boolean getversioncodefromweb_arr()throws NetworkErrorException,SocketTimeoutException, XmlPullParserException, IOException
						 {
							 SoapObject request = new SoapObject(cf.NAMESPACE, "GETVERSIONINFORMATION");
							 SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
							 envelope.dotNet = true;
							 envelope.setOutputSoapObject(request);
							 HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL_ARR);

							 androidHttpTransport.call(cf.NAMESPACE+"GETVERSIONINFORMATION", envelope);
							 SoapObject result = (SoapObject) envelope.getResponse();
							 SoapObject obj = (SoapObject) result.getProperty(0);
							 newcode = String.valueOf(obj.getProperty("VersionCode"));
							 newversion = String.valueOf(obj.getProperty("VersionName"));
							 System.out.println(" the ersio and version code "+newcode+"dfsg"+newversion);
							 if (!"null".equals(newcode) && null != newcode && !newcode.equals(""))
							 {
								 
								
								 if (vcode>=Integer.parseInt(newcode)) 
								 {
									
									 return true;
								 } 
								 else 
								 {
									 
										 try 
										 {
											 cf.CreateARRTable(20);
											 Cursor c12 = cf.SelectTablefunction(cf.ARRVersion," ");
											 if (c12.getCount() < 1) 
											 {
												 try 
												 {
													 cf.arr_db.execSQL("INSERT INTO "+ cf.ARRVersion + "(arrVId,ARR_VersionCode,ARR_VersionName,ModifiedDate)"
														+ " VALUES ('1','"+newcode+"','" + newversion+ "',date('now'))");
												 } 
												 catch (Exception e) 
												 {
													
												 }
											 } 
											 else 
											 {
												 try 
												 {
													 cf.arr_db.execSQL("UPDATE " + cf.ARRVersion + " set ARR_VersionCode='"+newcode+"',ARR_VersionName='" + newversion + "',ModifiedDate =date('now')");
												 }
												 catch (Exception e) 
												 {
													
												 }
											 }
											 
										 }
										 catch (Exception e) 
										 {
											
										 }
										 return false;
								 }
							 }
							 return true;
						 }
						private Handler handler = new Handler() {
							@Override
							public void handleMessage(Message msg) {
							if (k == 5 || k == 8) {
									
								cf.ShowToast("There is a problem on your Network. Please try again later with better Network.. ",0);
								} else if (usercheck == 1) {
									usercheck = 0;
									cf.ShowToast("You don�t have any updates to upgrade ARR inspection.",0);
								}

							}
						};
					}.start();
					
				
				
			} else {
				cf.ShowToast("Please enable the Internet connection.",0);
			}
			 break;
		 case R.id.HS_toAddComments:
			  startActivity(new Intent(this,CommentsLibraryAdding.class));
			  break;
		 case R.id.HS_backup:
			 if(cf.isInternetOn())
			 {
			 AlertDialog.Builder b =new AlertDialog.Builder(HomeScreen.this);
				b.setTitle("Confirmation");
				b.setMessage("Previous backup will be cleared and new backup will be created. \n" +
						"Are you sure, Do you want to Get backup of your comments?");
				b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						 getbackup();
					}
				});
				b.setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
				dialog.cancel();
					}
				});   
				AlertDialog al=b.create();
				al.setIcon(R.drawable.alertmsg);
				al.setCancelable(false);
				al.show();    
		 }
		 else
		 {
			 cf.ShowToast("Please enable your internet connection", 0);
		 }
		break;
		 case R.id.HS_restore:
			 if(cf.isInternetOn())
			 {
			 AlertDialog.Builder b1 =new AlertDialog.Builder(HomeScreen.this);
				b1.setTitle("Confirmation");
				b1.setMessage("Only the comments which you got backup will be restored other may be lost. \n" +
						"Are you sure, Do you want to Restore your comments?");
				b1.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						
						restoredata();
					}
				});
				b1.setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.cancel();
					}
				});   
				AlertDialog a=b1.create();
				a.setIcon(R.drawable.alertmsg);
				a.setCancelable(false);
				a.show(); 
			 }
			 else
			 {
				 cf.ShowToast("Please enable your internet connection", 0);
			 }
			 
		break;	 
		 case R.id.HS_back_main:
			  if(cf.application_sta)
			  {			 
				  Intent loginpage = new Intent(Intent.ACTION_MAIN);
				  loginpage.setComponent(new ComponentName("idsoft.inspectiondepot.IDMA","idsoft.inspectiondepot.IDMA.ApplicationMenu"));
				  startActivity(loginpage);
				  final Animation animTranslate = AnimationUtils.loadAnimation(this, R.anim.lefttoright);
			  }  
			  else 
			  {
				 cf.ShowToast("Please install the application then press for the main menu",0);
			  }			 
			 break;
		 }
	 }
	 
	 public void Update() throws NetworkErrorException, IOException,
		SocketTimeoutException, XmlPullParserException, Exception
		{
			SoapObject webresult;
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.dotNet = true;
			SoapObject request = new SoapObject(cf.NAMESPACE, "GETCURRENTAPKFILE");		
			request.addProperty("InspectorID", Integer.parseInt(cf.Insp_id));		
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL_ARR);
			androidHttpTransport.call(cf.NAMESPACE+"GETCURRENTAPKFILE", envelope);
			String response = envelope.getResponse().toString();
			System.out.println("reespo "+response);
			
	
			byte[] b;
			if (cf.checkresponce(response)) // check the response is valid or not
			{
				b = Base64.decode(response.toString());
				try {
					String PATH = Environment.getExternalStorageDirectory()
							+ "/Download";
					File file = new File(PATH);
					file.mkdirs();
		
					File outputFile = new File(PATH + "/ARR.apk");
					FileOutputStream fileOuputStream = new FileOutputStream(
							outputFile);
					fileOuputStream.write(b);
					fileOuputStream.close();
						try
						{	cf.arr_db.execSQL("UPDATE " + cf.inspectorlogin
												+ " SET Fld_InspectorFlag=0" + " WHERE Fld_InspectorId ='"
											+ cf.Insp_id + "'");
						}
						catch(Exception e)
						{
							
							System.out.println("dsfshjgjgs"+e.getMessage());
						}
					Cursor c12 = cf.SelectTablefunction(cf.ARRVersion," ");
					if (c12.getCount() < 1) {
						
						try {
							cf.arr_db.execSQL("INSERT INTO "
									+ cf.ARRVersion
									+ "(arrVId,ARR_VersionCode,ARR_VersionName,ModifiedDate)"
									+ " VALUES ('1','"+newcode+"','" + newversion
									+ "',date('now'))");
		
						} catch (Exception e) {
							System.out.println("INSERT "+e.getMessage());
						}
						
					} else {
						try {
							cf.arr_db.execSQL("UPDATE " + cf.ARRVersion
									+ " set ARR_VersionCode='"+newcode+"',ARR_VersionName='"
									+ newversion
									+ "'");
						} catch (Exception e) {
							System.out.println(" UPDATE "+e.getMessage());
						}
					}
		
					
					progressDialog1.dismiss();
					Intent intent = new Intent(Intent.ACTION_VIEW);
					intent.setDataAndType(Uri.fromFile(new File(Environment
							.getExternalStorageDirectory()
							+ "/Download/"
							+ "ARR.apk")),
							"application/vnd.android.package-archive");
					startActivityForResult(intent, 0);
					
				} catch (Exception e) {
					cf.ShowToast("Update error!",1);
				}
		} else {
			progressDialog1.dismiss();
			cf.ShowToast("There is a problem on your Network.\n Please try again later with better Network.",1);
		}
	 }
	 
	 private void restoredata() {

			// TODO Auto-generated method stub
		
				String source = "<font color=#FFFFFF>Retrieving comments. Please wait..."
						+ "</font>";		        
				pd = ProgressDialog.show(this,"", Html.fromHtml(source), true);
				 new Thread() {
						public void run() {

				try {
					SQLiteDatabase Secondry_db = HomeScreen.this.openOrCreateDatabase("Temp", 1, null);
					SoapObject request = new SoapObject(cf.NAMESPACE,"GETANDROIDIMAGECOMMETNS");
					SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
					envelope.dotNet = true;
					request.addProperty("inspectorID",Integer.parseInt(cf.Insp_id));
					envelope.setOutputSoapObject(request);
					HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL_ARR);
					androidHttpTransport.call(cf.NAMESPACE+"GETANDROIDIMAGECOMMETNS",envelope);System.out.println("request"+request);
					String result =  envelope.getResponse().toString();System.out.println("GETANDROIDIMAGECOMMETNS"+result);
					
					if(result.equals("AAAAAAAAAAAAAA=="))
					{
						messsage=8; 
						handler5.sendEmptyMessage(0);
					}
					else if(result.equals("null"))
					{
						messsage=2; 
						handler5.sendEmptyMessage(0);						
					}
					else
					{
						byte[] b = Base64.decode(result.toString());
						String backupDBPath ="/data/idsoft.inspectiondepot.ARR/databases/Temp";
						File data = Environment.getDataDirectory();
						File backupDB = new File(data, backupDBPath);
											
						FileOutputStream out = new FileOutputStream(backupDB);
						out.write(b);
						out.close();
						
						Secondry_db = HomeScreen.this.openOrCreateDatabase("Temp", 1, null);
						Cursor c2=Secondry_db.rawQuery(" SELECT * FROM "+cf.Comments_lib+ " Where CL_inspid='"+cf.Insp_id+"'" , null);
						cf.CreateARRTable(9);
						c2.moveToFirst();
						int max =100,min=0;
						if(c2.getCount()>0)
						{
							cf.arr_db.execSQL("Delete from "+cf.Comments_lib+" Where CL_inspid='"+cf.Insp_id+"'");
							for(int m=0;m<c2.getCount();) // this was used only 500 insert allowed per query so we are divide the whole entry in to no of querys
							{
									String s="";
								for(int i=0;i<c2.getColumnCount();i++)
								{
									if(i+1<c2.getColumnCount())
									 s+="'"+c2.getString(i)+"' as "+c2.getColumnName(i)+" , ";
									else
										s+="'"+c2.getString(i)+"' as "+c2.getColumnName(i)+"";
								}
								c2.moveToNext();
								m++;
								for(int j=min+1;j<c2.getCount() && j<max;j++)
								{
									s+=" UNION SELECT  ";
									s+="'"+c2.getString(0)+"','"+c2.getString(1)+"','"+c2.getString(2)+"' ,'"+c2.getString(3)+"' ,'"+c2.getString(4)+"' ,'"+c2.getString(5)+"' ,'"+c2.getString(6)+"' ,'"+c2.getString(7)+"' ,'"+c2.getString(8)+"' ,'"+c2.getString(9)+"' ";
									/*for(int i=0;i<c2.getColumnCount();i++)
									{
										if(i+1<c2.getColumnCount())
										 s+="'"+c2.getString(i)+"' , ";
										else
											s+="'"+c2.getString(i)+"' ";
									}*/
									m++;
									c2.moveToNext();
								}
								min=min+100;
								max=max+100;
							//	Secondry_db.execSQL("INSERT into "+cf.Comments_lib+" Select "+s);//insert the data into the new table
								cf.arr_db.execSQL("INSERT into "+cf.Comments_lib+" Select "+s);
								messsage=1; 
							}
							
							handler5.sendEmptyMessage(0);
						}
					}

					
				}catch (Exception e) {
					// TODO: handle exception
					
				System.out.println("the error was="+e.getMessage());
				handler5.sendEmptyMessage(0);
				
			}
						}
				 }.start();
				
				  
		}
	final Handler handler5 = new Handler() {

			public void handleMessage(Message msg) {
				if(messsage==1)
					cf.ShowToast("Comments retrieving completed ", 0);
				else if(messsage==2)
					cf.ShowToast("No backup available" , 0);
				else if(messsage==3)
					cf.ShowToast("Sorry you don't have any comments to get backup", 1);
				else if(messsage==4)
					cf.ShowToast("You have successfully backed up your comments", 1);
				else if(messsage==8)
					cf.ShowToast("You dont have comments to restore.", 1);
				else
					cf.ShowToast("Some internet problem occurred please try again later with good internet connection", 0);
				pd.dismiss();
			}
			
	};
	 private void getbackup() {

			String source = "<font color=#FFFFFF>Getting back up your comments. Please wait..."
					+ "</font>";		        
			pd = ProgressDialog.show(this,"", Html.fromHtml(source), true);
			 new Thread() {
					public void run() {
			try
			{
		
				cf.CreateARRTable(9);
	        Cursor c2=cf.SelectTablefunction(cf.Comments_lib, " Where CL_inspid='"+cf.Insp_id+"'");//Get the values from the table 
			SQLiteDatabase Secondry_db = HomeScreen.this.openOrCreateDatabase("Temp", 1, null); // Create new database for sperate the table 
			         
			Secondry_db.execSQL("CREATE TABLE IF NOT EXISTS "+cf.Comments_lib+" (_id INTEGER PRIMARY KEY AUTOINCREMENT,CL_inspid VARCHAR( 5 ) DEFAULT ( '' )," +
			 		"CL_inspectionName VARCHAR( 40 )  DEFAULT ( '' ),CL_type VARCHAR( 20 )  DEFAULT ( '' ),CL_question VARCHAR( 20 )  DEFAULT ( '' )," +
			 		"CL_option  VARCHAR( 30 )  DEFAULT ( '' ),CL_Comments    VARCHAR( 250 )  DEFAULT ( '' ),CL_Active    BOOLEAN  DEFAULT false,CL_date     datetime default current_timestamp," +
			 		"CL_Mdate datetime default current_timestamp);"); // Created the table in the new database
			Secondry_db.execSQL("Delete from "+cf.Comments_lib); //Delete if the database contains data already
		
			c2.moveToFirst();
			int max =100,min=0;
			if(c2.getCount()>0)
			{
		for(int m =0;m<c2.getCount();)
		{
				String s="";
				for(int i=0;i<c2.getColumnCount();i++)
				{
					if(i+1<c2.getColumnCount())
					 s+="'"+c2.getString(i)+"' as "+c2.getColumnName(i)+" , ";
					else
						s+="'"+c2.getString(i)+"' as "+c2.getColumnName(i)+"";
					
				}
				
				
				c2.moveToNext();
				m++;
				int j=1;
				for(j=min+1;j<c2.getCount() && j<max;j++)
				{
					s+=" UNION SELECT  ";
					 s+="'"+c2.getString(0)+"','"+c2.getString(1)+"','"+c2.getString(2)+"' ,'"+c2.getString(3)+"' ,'"+c2.getString(4)+"' ,'"+c2.getString(5)+"' ,'"+c2.getString(6)+"' ,'"+c2.getString(7)+"' ,'"+c2.getString(8)+"' ,'"+c2.getString(9)+"' ";
					c2.moveToNext();
					m++;
				}
				min=min+100;
				max=max+100;
				Secondry_db.execSQL("INSERT into "+cf.Comments_lib+" Select "+s);//insert the data into the new table
			}
			
			File data = Environment.getDataDirectory();
			String backupDBPath ="/data/idsoft.inspectiondepot.ARR/databases/Temp";
			File currentDB = new File(data, backupDBPath);
			if(currentDB.exists())
	        {
	        	 RandomAccessFile f = new RandomAccessFile(currentDB, "r");
	             try {
	                 // Get and check length
	                 long longlength = f.length();
	                 int length = (int) longlength;
	                 if (length != longlength)
	                     throw new IOException("File size >= 2 GB");
	                 // Read file and return data
	                 raw = new byte[length];
	                 f.readFully(raw);
	             }
	             finally {
	                 f.close();
	             }
	                 try
	                 {
	                	 
	                SoapObject request = new SoapObject(cf.NAMESPACE,"ANDROIDIMAGECOMMETNS"); 
	 				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
	 				envelope.dotNet = true; 
	 				MarshalBase64 marshal = new MarshalBase64();
	 				request.addProperty("inspectorID",Integer.parseInt(cf.Insp_id));
	 				request.addProperty("Comments",raw);
	 				request.addProperty("Filenamewithext","ARRtoaddcomments.sql");
	 				marshal.register(envelope);
	 				envelope.setOutputSoapObject(request);System.out.println("ANDROIDIMAGECOMMETNS"+request);
	 				
	 				HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL_ARR);
	 				androidHttpTransport.call(cf.NAMESPACE+"ANDROIDIMAGECOMMETNS ",envelope);
	 				String result =  envelope.getResponse().toString();
	 				System.out.println("Res="+result);
	 				messsage=4;
	 				handler5.sendEmptyMessage(0);
	                 
	             } 
	             catch(Exception e)
	             {
	            	 System.out.println("catc"+e.getMessage());	 
	             }
	           }
			}
			else
			{
				messsage=3;
				handler5.sendEmptyMessage(0);
			}
	        
				}
			        catch (Exception e) {
						// TODO: handle exception
			        	messsage=0;
			        	handler5.sendEmptyMessage(0);
					}

					}
			 }.start();
			
		}
	 private void online_search()
	 {
		 	final EditText tvxtstname,tvlstname,tvploicy;
		 	final Dialog dialog = new Dialog(HomeScreen.this,android.R.style.Theme_Translucent_NoTitleBar);
			dialog.getWindow().setContentView(R.layout.onlinesearch);

			tvxtstname = (EditText) dialog.findViewById(R.id.txtfirstname);
			tvlstname = (EditText) dialog.findViewById(R.id.txtlastname);
			tvploicy = (EditText) dialog.findViewById(R.id.txtpolicy);
			tvxtstname.requestFocusFromTouch();
			Button btnsrch = (Button) dialog.findViewById(R.id.search);
			btnsrch.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					if (tvxtstname.getText().toString().trim().equals("")
							&& tvlstname.getText().toString().trim().equals("")
							&& tvploicy.getText().toString().trim().equals("")) {
						cf.ShowToast("Please enter atleast one field to search.",0);
					} else {
						cf.hidekeyboard(tvxtstname);
						cf.hidekeyboard(tvlstname);
						cf.hidekeyboard(tvploicy);
						dialog.dismiss();
						show();
					}
				}

				private void show() {

					// TODO Auto-generated method stub
					if(cf.isInternetOn())
					{
						Intent srch = new Intent(HomeScreen.this,SearchInspection.class);
						srch.putExtra("first", tvxtstname.getText().toString());
						srch.putExtra("last", tvlstname.getText().toString());
						srch.putExtra("policy", tvploicy.getText().toString());
						startActivity(srch);
					}
					else
					{
						cf.ShowToast("Internet connection not available.",0);
						cf.hidekeyboard();						
					}
				}

			});
			Button btncls = (Button) dialog.findViewById(R.id.cls);
			btncls.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					cf.hidekeyboard(tvxtstname);
					cf.hidekeyboard(tvlstname);
					cf.hidekeyboard(tvploicy);
					dialog.dismiss();
				}

			});
			ImageView btn_helpclose = (ImageView) dialog.findViewById(R.id.helpclose);
			btn_helpclose.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View arg0) {

					// TODO Auto-generated method stub
					dialog.cancel();
				}
				
			});			
			dialog.show();
			 
		
	 }
	 private void start_import() {
			// TODO Auto-generated method stub
			 new Thread() {
				public void run() {

					try {
						if (getversioncodefromweb() == true) 
						{
							 total = 15;
					  		 try
							 {
					  			 
									SoapObject policy_holder = cf.Calling_WS1(cf.Insp_id,"UPDATEMOBILEDB_SCHEASSIGN");
									if (policy_holder.equals("anyType{}")) 
									{
										cf.ShowToast("Server is busy. Please try again later.",0);
										startActivity(new Intent(HomeScreen.this,HomeScreen.class));
									} 
									else 
									{
										GetinsptypeName();
										total = 20;
										GetPolicyholderInfo(policy_holder);
										total=30;
										GetAgentinformation();
										total=40;
										GetRealtorinformation();
										total=50;
										Get1802CommentsInfo();
										total = 65;
										GetOriginalQuesMitigationInfo();
										total = 80;		
										/*GetCloudInfo();
										total = 90;	*/		
										GetInspectionName();
										total = 90;
										GetAllInspectionName();
										total = 95;	
										GetAdditionalService();										
										total =100;
										usercheck = 4;
									}
									
								}
								catch (SocketTimeoutException s) {
									System.out.println("s "+s.getMessage());
									    usercheck = 2;
										total = 100;
										
								 } catch (NetworkErrorException n) {
									 System.out.println("n "+n.getMessage());
									    usercheck = 2;
									    total = 100;
									   
								 } catch (IOException io) {
									 System.out.println("io "+io.getMessage());
									    usercheck = 2;
									    total = 100;
									    
								 } catch (XmlPullParserException x) {
									 System.out.println("x "+x.getMessage());
									 	usercheck = 2;
									 	total = 100;
									    
								 }
			               	     catch (Exception e) {
			               	    	 System.out.println("e "+e.getMessage());
			               	    	    usercheck = 3;
										total = 100;									
								}
							}
						    else 
						    {
						    	total = 100;
						    }
						 }
						 catch (Exception e) {
							 System.out.println("commeonexcep"+e.getMessage());
								cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ HomeScreen.this +" "+" in the stage of(catch) importing  at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
								
						}
					}
			 }
			 .start();
		}
	 
	 public void GetinsptypeName() throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException, XmlPullParserException 
	 {
		// TODO Auto-generated method stub
		 cf.CreateARRTable(13);int Cnt;
		 SoapObject request = new SoapObject(cf.NAMESPACE,"GETINSPECTIONTYPENAME");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL_ARR);
			androidHttpTransport.call(cf.NAMESPACE+"GETINSPECTIONTYPENAME",envelope);
			SoapObject result = (SoapObject) envelope.getResponse();
			System.out.println("reul"+result);
			
			 
			 if(String.valueOf(result).equals("null") || String.valueOf(result).equals(null) || String.valueOf(result).equals("anytype{}"))
			 {
				 total = 20;
			 }
			 else
			 {
				 
				 Cnt = result.getPropertyCount();
				 double Totaltmp = 0.0;
				 int countchk = Cnt;
				 Totaltmp = (5.00 / (double) countchk);
				 double temptot = Totaltmp;
				 int temptotal = total;
				
				for(int i=0;i<Cnt;i++)
				{
					SoapObject temp_result=(SoapObject) result.getProperty(i);
					try
					{
						String InspectionType = String.valueOf(temp_result.getProperty("InspectionTypeID"));
						String InspectionName = String.valueOf(temp_result.getProperty("InspectionName"));
						
						
						Cursor c1=cf.SelectTablefunction(cf.dyninspname," where ARR_PH_InspectorId ='"+cf.Insp_id+"' and ARR_Insp_Type_ID='"+InspectionType+"'");
						if(c1.getCount()==0)
						{
							cf.arr_db.execSQL("Insert into "+cf.dyninspname+" (ARR_PH_InspectorId,ARR_Insp_Type_ID,ARR_Insp_Name) values" +
									"('"+cf.Insp_id+"','"+InspectionType+"','"+cf.encode(InspectionName)+"') ");
						}
						else 
						{
							cf.arr_db.execSQL("UPDATE "+cf.dyninspname+" SET ARR_Insp_Type_ID='"+InspectionType+"',ARR_Insp_Name='"+cf.encode(InspectionName)+"' WHERE ARR_PH_InspectorId='"+cf.Insp_id+"'");
						}
						 if (total <= 21) 
						 {
							 total = temptotal + (int) (Totaltmp);
	 					 } 
						 else 
						 {
							total = 20;
						 }
						Totaltmp += temptot;
					}
					catch(Exception e)
					{
						System.out.println("eeeeeee"+e.getMessage());
						cf.Error_LogFile_Creation("Error  while create or select or delete, or insert the data in the dynamic inspection nmae information in import ");
					}			
				}
			 }
	}
	 public void Get1802CommentsInfo() throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException, XmlPullParserException 
	 {
			// TODO Auto-generated method stub
		 		cf.CreateARRTable(9);int Cnt;
		 		String InspectionID,InspectionType,questId,OptionId, Comments,question,suboption;
			 	SoapObject request = new SoapObject(cf.NAMESPACE,"LOADCOMMANDLIBRARY");
				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				envelope.dotNet = true;
				envelope.setOutputSoapObject(request);
				HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL_ARR);
				androidHttpTransport.call(cf.NAMESPACE+"LOADCOMMANDLIBRARY",envelope);
				SoapObject comments1802 = (SoapObject) envelope.getResponse();
				
				
				if(String.valueOf(comments1802).equals("null") || String.valueOf(comments1802).equals(null) || String.valueOf(comments1802).equals("anytype{}"))
				 {
					 Cnt=0; total = 65;
				 }
				 else
				 {
					  Cnt = comments1802.getPropertyCount();
					 double Totaltmp = 0.0;
					 int countchk = Cnt;
					 Totaltmp = (15.00 / (double) countchk);
					 double temptot = Totaltmp;
					 int temptotal = total;
					
					for(int i=0;i<Cnt;i++)
					{
						SoapObject temp_result=(SoapObject) comments1802.getProperty(i);
						try
						{
							InspectionID = String.valueOf(temp_result.getProperty("InspectionID"));
							InspectionType = String.valueOf(temp_result.getProperty("InspectionType"));
							questId = String.valueOf(temp_result.getProperty("questId"));
							OptionId = String.valueOf(temp_result.getProperty("OptionId"));
							Comments = String.valueOf(temp_result.getProperty("Command"));
							
							if(InspectionID.equals("13"))
								InspectionID="1";
							else if(InspectionID.equals("750"))
								InspectionID="2";
							else if(InspectionID.equals("28"))
								InspectionID="3";
							else if(InspectionID.equals("9"))
								InspectionID="4";
							else if(InspectionID.equals("62"))
								InspectionID="5";
							else if(InspectionID.equals("90"))
								InspectionID="6";
							else if(InspectionID.equals("11"))
								InspectionID="7";
							else if(InspectionID.equals("18"))
								InspectionID="8";
							else if(InspectionID.equals("751"))
								InspectionID="9";
							
							question = cd.getquestiontext(Integer.parseInt(questId));
							suboption = cd.getsuboptiontext(Integer.parseInt(OptionId),Integer.parseInt(questId));
							
							Cursor c2=cf.SelectTablefunction(cf.Comments_lib," where CL_inspid ='"+cf.Insp_id+"' and CL_type='28' and CL_inspectionName='"+InspectionID+"' and CL_question='"+cf.encode(question)+"' and CL_option='"+cf.encode(suboption)+"' and CL_Comments='"+cf.encode(Comments)+"'");
							if(c2.getCount()==0)
							{
								cf.arr_db.execSQL("Insert into "+cf.Comments_lib+" (CL_inspid,CL_inspectionName,CL_type,CL_question,CL_option,CL_Comments,CL_Active) values" +
										"('"+cf.Insp_id+"','"+InspectionID+"','"+InspectionType+"','"+cf.encode(question)+"','"+cf.encode(suboption)+"','"+cf.encode(Comments)+"','true') ");
							}
							else // if(c2.getCount()>=1)
							{
								//cf.arr_db.execSQL("UPDATE "+cf.Comments_lib+" SET CL_Comments='"+cf.encode(Comments)+"' WHERE CL_inspid='"+cf.Insp_id+"' and CL_question='"+cf.encode(question)+"' and CL_option='"+cf.encode(suboption)+"'");
							}
							 if (total <= 66) 
							 {
								 total = temptotal + (int) (Totaltmp);
		 					 } 
							 else 
							 {
								total = 65;
							 }
							Totaltmp += temptot;
						}
						catch(Exception e)
						{
							System.out.println("eeeeeee"+e.getMessage());
							cf.Error_LogFile_Creation("Error  while create or select or delete, or insert the data in the cloud information in import ");
						}			
					}
				 }
		}
	 public void GetAdditionalService() throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException, XmlPullParserException 
	 {
		 int Cnt;
		 	String SRID="",IsRecord="",ContactEmail="",UserTypeName="",PhoneNumber="",MobileNumber="",BestTimetoCallYou="",BestDay="",FirstChoice="",SecondChoice="",ThirdChoice="",FeedbackComments="";
		 	cf.CreateARRTable(6);
		 	SoapObject request = new SoapObject(cf.NAMESPACE,"GETADDITIONALSERVICEINFO");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			request.addProperty("InspectorID",Integer.parseInt(cf.Insp_id));
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL_ARR);
			androidHttpTransport.call(cf.NAMESPACE+"GETADDITIONALSERVICEINFO",envelope);
			SoapObject addiserviceres = (SoapObject) envelope.getResponse();
			
			 if(String.valueOf(addiserviceres).equals("null") || String.valueOf(addiserviceres).equals(null) || String.valueOf(addiserviceres).equals("anytype{}"))
			 {
					Cnt=0;total = 100;
			  }
			 else
			 {
				 Cnt = addiserviceres.getPropertyCount();
				 double Totaltmp = 0.0;
				 int countchk = Cnt;
				 Totaltmp = (5.00 / (double) countchk);
				 double temptot = Totaltmp;
				 int temptotal = total;
				for(int i=0;i<Cnt;i++)
				{					
					SoapObject temp_result=(SoapObject) addiserviceres.getProperty(i);
					SRID = (String.valueOf(temp_result.getProperty("SRID")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("SRID")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("SRID")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("SRID"));
					IsRecord=(String.valueOf(temp_result.getProperty("IsRecord")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("IsRecord")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("IsRecord")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("IsRecord"));
					UserTypeName=(String.valueOf(temp_result.getProperty("UserTypeName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("UserTypeName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("UserTypeName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("UserTypeName"));
					ContactEmail=(String.valueOf(temp_result.getProperty("ContactEmail")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("ContactEmail")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("ContactEmail")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("ContactEmail"));
					PhoneNumber=(String.valueOf(temp_result.getProperty("PhoneNumber")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("PhoneNumber")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("PhoneNumber")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("PhoneNumber"));
					MobileNumber=(String.valueOf(temp_result.getProperty("MobileNumber")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("MobileNumber")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("MobileNumber")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("MobileNumber"));
					BestTimetoCallYou=(String.valueOf(temp_result.getProperty("BestTimetoCallYou")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("BestTimetoCallYou")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("BestTimetoCallYou")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("BestTimetoCallYou"));
					BestDay=(String.valueOf(temp_result.getProperty("BestDay")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("BestDay")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("BestDay")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("BestDay"));
					FirstChoice=(String.valueOf(temp_result.getProperty("FirstChoice")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("FirstChoice")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("FirstChoice")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("FirstChoice"));
					SecondChoice=(String.valueOf(temp_result.getProperty("SecondChoice")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("SecondChoice")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("SecondChoice")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("SecondChoice"));
					ThirdChoice=(String.valueOf(temp_result.getProperty("ThirdChoice")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("ThirdChoice")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("ThirdChoice")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("ThirdChoice"));
					FeedbackComments=(String.valueOf(temp_result.getProperty("FeedbackComments")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("FeedbackComments")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("FeedbackComments")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("FeedbackComments"));
					
					try
					{
					Cursor c1 = cf.SelectTablefunction(cf.Additional_table, " where ARR_AD_INSPID='" + cf.Insp_id + "' and ARR_AD_SRID='"+SRID+"'");
					if(c1.getCount()>0)
					{
						
						cf.arr_db.execSQL("UPDATE "+cf.Additional_table+" set ARR_AD_UserTypeName='"+cf.encode(UserTypeName)+"',ARR_AD_ContactEmail='"+cf.encode(ContactEmail)+"'," +
										"ARR_AD_PhoneNumber='"+cf.encode(PhoneNumber)+"',ARR_AD_MobileNumber='"+cf.encode(MobileNumber)+"',ARR_AD_BestTimetoCallYou='"+cf.encode(BestTimetoCallYou)+"'," +
											"ARR_AD_Bestdaycall='"+cf.encode(BestDay)+"',ARR_AD_FirstChoice='"+cf.encode(FirstChoice)+"',ARR_AD_SecondChoice='"+cf.encode(SecondChoice)+"',ARR_AD_ThirdChoice='"+cf.encode(ThirdChoice)+"'," +
											"SF_AD_FeedbackComments='"+cf.encode(FeedbackComments)+"' WHERE ARR_AD_SRID='"+SRID+"' and " +
											"ARR_AD_INSPID='"+cf.Insp_id+"'");
					}
					else
					{
							cf.arr_db.execSQL("INSERT INTO "
									+ cf.Additional_table
									+ "(ARR_AD_SRID,ARR_AD_INSPID,ARR_AD_IsRecord,ARR_AD_UserTypeName,ARR_AD_ContactEmail,ARR_AD_PhoneNumber,ARR_AD_MobileNumber,ARR_AD_BestTimetoCallYou,ARR_AD_Bestdaycall,ARR_AD_FirstChoice,ARR_AD_SecondChoice,ARR_AD_ThirdChoice,SF_AD_FeedbackComments)"
									+ " VALUES ('"+SRID+"','"+cf.Insp_id+"','','"+cf.encode(IsRecord)+"','"+cf.encode(ContactEmail)+"','"+cf.encode(PhoneNumber)+"','"+cf.encode(MobileNumber)+"','"+cf.encode(BestTimetoCallYou)+"','"+cf.encode(BestDay)+"','"+cf.encode(FirstChoice)+"','"+cf.encode(SecondChoice)+"','"+cf.encode(ThirdChoice)+"','"+cf.encode(FeedbackComments)+"')");
					}
					}
					catch(Exception e)
					{
						System.out.println("eee"+e.getMessage());
					}
					if (total <= 100) {
						
						total = temptotal + (int) (Totaltmp);
					} else {
						total =100;
					}
				}
			}
	 }
	 public void GetCloudInfo() throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException, XmlPullParserException {
			// TODO Auto-generated method stub
			    cf.CreateARRTable(11);int Cnt;
		 		String SRID,  inspectionType,assignmentId, assignmentMasterId, inspectionPolicy_Id, priorAssignmentId, assignmentType, policyId, policyVersion, policyEndorsement, policyEffectiveDate, policyForm, policySystem, lob, structureCount, structureNumber, structureDescription, propertyAddress, propertyAddress2, propertyCity, propertyState, propertyCounty, propertyZip, insuredFirstName, insuredLastName, insuredHomePhone, insuredWorkPhone, insuredAlternatePhone, insuredEmail, insuredMailingAddress, insuredMailingAddress2, insuredMailingCity, insuredMailingState, insuredMailingZip, agencyID, agencyName, agencyPhone, agencyFax, agencyEmail, agencyPrincipalFirstName, agencyPrincipalLastName, agencyPrincipalEmail, agencyFEIN, agencyMailingAddress, agencyMailingAddress2, agencyMailingCity, agencyMailingState, agencyMailingZip, agentID, agentFirstName, agentLastName, agentEmail, previousInspectionDate, previousInspectionCompany, previousInspectorFirstName, previousInspectorLastName, previousInspectorLicense, yearBuilt, squareFootage, numberOfStories, construction, roofCovering, roofDeckAttachment, roofWallAttachment, roofShape, secondaryWaterResistance, openingCoverage, buildingType;
				SoapObject request = new SoapObject(cf.NAMESPACE,"LOADCLOUDINFORMATION");
				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				envelope.dotNet = true;
				request.addProperty("InspectorID",cf.Insp_id);
				envelope.setOutputSoapObject(request);
				HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL_ARR);
				androidHttpTransport.call(cf.NAMESPACE+"LOADCLOUDINFORMATION",envelope);
				SoapObject cloudinfo = (SoapObject) envelope.getResponse();
				
				
				if(String.valueOf(cloudinfo).equals("null") || String.valueOf(cloudinfo).equals(null) || String.valueOf(cloudinfo).equals("anytype{}"))
				{
					Cnt=0;total = 90;
				}
				else
				{
					  Cnt = cloudinfo.getPropertyCount();
					 double Totaltmp = 0.0;
					 int countchk = Cnt;
					 Totaltmp = (10.00 / (double) countchk);
					 double temptot = Totaltmp;
					 int temptotal = total;
					cf.CreateARRTable(5);
					for(int i=0;i<cloudinfo.getPropertyCount();i++)
					{
						
						SoapObject temp_result=(SoapObject) cloudinfo.getProperty(i);
						SRID = (String.valueOf(temp_result.getProperty("SRID")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("SRID")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("SRID")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("SRID"));
						assignmentId=(String.valueOf(temp_result.getProperty("assignmentId")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("assignmentId")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("assignmentId")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("assignmentId"));
						assignmentMasterId=(String.valueOf(temp_result.getProperty("assignmentMasterId")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("assignmentMasterId")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("assignmentMasterId")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("assignmentMasterId"));
						inspectionPolicy_Id=(String.valueOf(temp_result.getProperty("inspectionPolicy_Id")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("inspectionPolicy_Id")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("inspectionPolicy_Id")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("inspectionPolicy_Id"));
						priorAssignmentId=(String.valueOf(temp_result.getProperty("priorAssignmentId")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("priorAssignmentId")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("priorAssignmentId")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("priorAssignmentId"));
						inspectionType=(String.valueOf(temp_result.getProperty("inspectionType")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("inspectionType")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("inspectionType")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("inspectionType"));
						assignmentType=(String.valueOf(temp_result.getProperty("assignmentType")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("assignmentType")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("assignmentType")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("assignmentType"));
						policyId=(String.valueOf(temp_result.getProperty("policyId")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("policyId")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("policyId")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("policyId"));
						policyVersion=(String.valueOf(temp_result.getProperty("policyVersion")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("policyVersion")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("policyVersion")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("policyVersion"));
						policyEndorsement=(String.valueOf(temp_result.getProperty("policyEndorsement")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("policyEndorsement")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("policyEndorsement")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("policyEndorsement"));
						policyEffectiveDate=(String.valueOf(temp_result.getProperty("policyEffectiveDate")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("policyEffectiveDate")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("policyEffectiveDate")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("policyEffectiveDate"));
						policyForm=(String.valueOf(temp_result.getProperty("policyForm")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("policyForm")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("policyForm")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("policyForm"));
						policySystem=(String.valueOf(temp_result.getProperty("policySystem")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("policySystem")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("policySystem")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("policySystem"));					
						lob=(String.valueOf(temp_result.getProperty("lob")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("lob")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("lob")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("lob"));					
						structureCount=(String.valueOf(temp_result.getProperty("structureCount")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("structureCount")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("structureCount")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("structureCount"));					
						structureNumber=(String.valueOf(temp_result.getProperty("structureNumber")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("structureNumber")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("structureNumber")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("structureNumber"));
						structureDescription=(String.valueOf(temp_result.getProperty("structureDescription")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("structureDescription")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("structureDescription")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("structureDescription"));
						propertyAddress=(String.valueOf(temp_result.getProperty("propertyAddress")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("propertyAddress")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("propertyAddress")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("propertyAddress"));
						propertyAddress2=(String.valueOf(temp_result.getProperty("propertyAddress2")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("propertyAddress2")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("propertyAddress2")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("propertyAddress2"));
						propertyCity=(String.valueOf(temp_result.getProperty("propertyCity")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("propertyCity")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("propertyCity")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("propertyCity"));
						propertyState=(String.valueOf(temp_result.getProperty("propertyState")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("propertyState")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("propertyState")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("propertyState"));
						propertyCounty=(String.valueOf(temp_result.getProperty("propertyCounty")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("propertyCounty")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("propertyCounty")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("propertyCounty"));
						propertyZip=(String.valueOf(temp_result.getProperty("propertyZip")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("propertyZip")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("propertyZip")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("propertyZip"));
						insuredFirstName=(String.valueOf(temp_result.getProperty("insuredFirstName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("insuredFirstName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("insuredFirstName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("insuredFirstName"));
						insuredLastName=(String.valueOf(temp_result.getProperty("insuredLastName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("insuredLastName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("insuredLastName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("insuredLastName"));
						insuredHomePhone=(String.valueOf(temp_result.getProperty("insuredHomePhone")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("insuredHomePhone")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("insuredHomePhone")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("insuredHomePhone"));
						insuredWorkPhone=(String.valueOf(temp_result.getProperty("insuredWorkPhone")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("insuredWorkPhone")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("insuredWorkPhone")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("insuredWorkPhone"));
						insuredAlternatePhone=(String.valueOf(temp_result.getProperty("insuredAlternatePhone")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("insuredAlternatePhone")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("insuredAlternatePhone")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("insuredAlternatePhone"));
						insuredEmail=(String.valueOf(temp_result.getProperty("insuredEmail")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("insuredEmail")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("insuredEmail")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("insuredEmail"));
						insuredMailingAddress=(String.valueOf(temp_result.getProperty("insuredMailingAddress")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("insuredMailingAddress")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("insuredMailingAddress")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("insuredMailingAddress"));
						insuredMailingAddress2=(String.valueOf(temp_result.getProperty("insuredMailingAddress2")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("insuredMailingAddress2")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("insuredMailingAddress2")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("insuredMailingAddress2"));
						insuredMailingCity=(String.valueOf(temp_result.getProperty("insuredMailingCity")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("insuredMailingCity")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("insuredMailingCity")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("insuredMailingCity"));
						insuredMailingState=(String.valueOf(temp_result.getProperty("insuredMailingState")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("insuredMailingState")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("insuredMailingState")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("insuredMailingState"));
						insuredMailingZip=(String.valueOf(temp_result.getProperty("insuredMailingZip")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("insuredMailingZip")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("insuredMailingZip")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("insuredMailingZip"));
						agencyID=(String.valueOf(temp_result.getProperty("agencyID")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("agencyID")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("agencyID")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("agencyID"));
						agencyName=(String.valueOf(temp_result.getProperty("agencyName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("agencyName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("agencyName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("agencyName"));
						agencyPhone=(String.valueOf(temp_result.getProperty("agencyPhone")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("agencyPhone")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("agencyPhone")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("agencyPhone"));
						agencyFax=(String.valueOf(temp_result.getProperty("agencyFax")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("agencyFax")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("agencyFax")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("agencyFax"));
						agencyEmail=(String.valueOf(temp_result.getProperty("agencyEmail")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("agencyEmail")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("agencyEmail")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("agencyEmail"));
						agencyPrincipalFirstName=(String.valueOf(temp_result.getProperty("agencyPrincipalFirstName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("agencyPrincipalFirstName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("agencyPrincipalFirstName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("agencyPrincipalFirstName"));
						agencyPrincipalLastName=(String.valueOf(temp_result.getProperty("agencyPrincipalLastName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("agencyPrincipalLastName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("agencyPrincipalLastName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("agencyPrincipalLastName"));
						agencyPrincipalEmail=(String.valueOf(temp_result.getProperty("agencyPrincipalEmail")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("agencyPrincipalEmail")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("agencyPrincipalEmail")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("agencyPrincipalEmail"));
						agencyFEIN=(String.valueOf(temp_result.getProperty("agencyFEIN")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("agencyFEIN")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("agencyFEIN")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("agencyFEIN"));
						agencyMailingAddress=(String.valueOf(temp_result.getProperty("agencyMailingAddress")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("agencyMailingAddress")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("agencyMailingAddress")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("agencyMailingAddress"));
						agencyMailingAddress2=(String.valueOf(temp_result.getProperty("agencyMailingAddress2")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("agencyMailingAddress2")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("agencyMailingAddress2")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("agencyMailingAddress2"));
						agencyMailingCity=(String.valueOf(temp_result.getProperty("agencyMailingCity")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("agencyMailingCity")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("agencyMailingCity")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("agencyMailingCity"));
						agencyMailingState=(String.valueOf(temp_result.getProperty("agencyMailingState")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("agencyMailingState")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("agencyMailingState")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("agencyMailingState"));
						agencyMailingZip=(String.valueOf(temp_result.getProperty("agencyMailingZip")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("agencyMailingZip")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("agencyMailingZip")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("agencyMailingZip"));
						agentID=(String.valueOf(temp_result.getProperty("agentID")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("agentID")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("agentID")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("agentID"));
						agentFirstName=(String.valueOf(temp_result.getProperty("agentFirstName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("agentFirstName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("agentFirstName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("agentFirstName"));
						agentLastName=(String.valueOf(temp_result.getProperty("agentLastName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("agentLastName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("agentLastName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("agentLastName"));
						agentEmail=(String.valueOf(temp_result.getProperty("agentEmail")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("agentEmail")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("agentEmail")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("agentEmail"));
						previousInspectionDate=(String.valueOf(temp_result.getProperty("previousInspectionDate")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("previousInspectionDate")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("previousInspectionDate")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("previousInspectionDate"));
						previousInspectionCompany=(String.valueOf(temp_result.getProperty("previousInspectionCompany")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("previousInspectionCompany")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("previousInspectionCompany")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("previousInspectionCompany"));
						previousInspectorFirstName=(String.valueOf(temp_result.getProperty("previousInspectorFirstName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("previousInspectorFirstName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("previousInspectorFirstName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("previousInspectorFirstName"));
						previousInspectorLastName=(String.valueOf(temp_result.getProperty("previousInspectorLastName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("previousInspectorLastName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("previousInspectorLastName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("previousInspectorLastName"));
						previousInspectorLicense=(String.valueOf(temp_result.getProperty("previousInspectorLicense")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("previousInspectorLicense")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("previousInspectorLicense")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("yearBuilt"));
						yearBuilt=(String.valueOf(temp_result.getProperty("yearBuilt")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("yearBuilt")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("yearBuilt")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("yearBuilt"));
						squareFootage=(String.valueOf(temp_result.getProperty("squareFootage")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("squareFootage")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("squareFootage")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("squareFootage"));
						numberOfStories=(String.valueOf(temp_result.getProperty("numberOfStories")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("numberOfStories")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("numberOfStories")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("numberOfStories"));
						construction=(String.valueOf(temp_result.getProperty("construction")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("construction")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("construction")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("construction"));
						roofCovering=(String.valueOf(temp_result.getProperty("roofCovering")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("roofCovering")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("roofCovering")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("roofCovering"));
						roofDeckAttachment=(String.valueOf(temp_result.getProperty("roofDeckAttachment")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("roofDeckAttachment")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("roofDeckAttachment")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("roofDeckAttachment"));
						roofWallAttachment=(String.valueOf(temp_result.getProperty("roofWallAttachment")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("roofWallAttachment")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("roofWallAttachment")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("roofWallAttachment"));
						roofShape=(String.valueOf(temp_result.getProperty("roofShape")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("roofShape")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("roofShape")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("roofShape"));
						secondaryWaterResistance=(String.valueOf(temp_result.getProperty("secondaryWaterResistance")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("secondaryWaterResistance")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("secondaryWaterResistance")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("secondaryWaterResistance"));
						openingCoverage=(String.valueOf(temp_result.getProperty("openingCoverage")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("openingCoverage")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("openingCoverage")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("openingCoverage"));
						buildingType=(String.valueOf(temp_result.getProperty("buildingType")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("buildingType")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("buildingType")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("buildingType"));
						try
						{
							try {
								cf.arr_db.execSQL("INSERT INTO "
										+ cf.cloud_tbl
										+ " (SRID,assignmentId,assignmentMasterId,inspectionPolicy_Id,priorAssignmentId,inspectionType,assignmentType,policyId,policyVersion,policyEndorsement,policyEffectiveDate,policyForm,policySystem,lob,structureCount,structureNumber,structureDescription,propertyAddress,propertyAddress2,propertyCity,propertyState,propertyCounty,propertyZip,insuredFirstName,insuredLastName,insuredHomePhone,insuredWorkPhone,insuredAlternatePhone,insuredEmail,insuredMailingAddress,insuredMailingAddress2,insuredMailingCity,insuredMailingState,insuredMailingZip,agencyID,agencyName,agencyPhone,agencyFax,agencyEmail,agencyPrincipalFirstName,agencyPrincipalLastName,agencyPrincipalEmail,agencyFEIN,agencyMailingAddress,agencyMailingAddress2,agencyMailingCity,agencyMailingState,agencyMailingZip,agentID,agentFirstName,agentLastName,agentEmail,previousInspectionDate,previousInspectionCompany,previousInspectorFirstName,previousInspectorLastName,previousInspectorLicense,yearBuilt,squareFootage,numberOfStories,construction,roofCovering,roofDeckAttachment,roofWallAttachment,roofShape,secondaryWaterResistance,openingCoverage,buildingType)"
										+ " VALUES ('" + SRID + "','"
										+ assignmentId + "','" + assignmentMasterId
										+ "','" + inspectionPolicy_Id + "','"
										+ priorAssignmentId + "','"
										+ inspectionType + "','" + assignmentType
										+ "','" + policyId + "','" + policyVersion
										+ "','" + policyEndorsement + "','"
										+ policyEffectiveDate + "','" + policyForm
										+ "','" + policySystem + "','" + lob
										+ "','" + structureCount + "','"
										+ structureNumber + "','"
										+ structureDescription + "','"
										+ propertyAddress + "','"
										+ propertyAddress2 + "','" + propertyCity
										+ "','" + propertyState + "','"
										+ propertyCounty + "','" + propertyZip
										+ "','" + insuredFirstName + "','"
										+ insuredLastName + "','"
										+ insuredHomePhone + "','"
										+ insuredWorkPhone + "','"
										+ insuredAlternatePhone + "','"
										+ insuredEmail + "','"
										+ insuredMailingAddress + "','"
										+ insuredMailingAddress2 + "','"
										+ insuredMailingCity + "','"
										+ insuredMailingState + "','"
										+ insuredMailingZip + "','" + agencyID
										+ "','" + agencyName + "','" + agencyPhone
										+ "','" + agencyFax + "','" + agencyEmail
										+ "','" + agencyPrincipalFirstName + "','"
										+ agencyPrincipalLastName + "','"
										+ agencyPrincipalEmail + "','" + agencyFEIN
										+ "','" + agencyMailingAddress + "','"
										+ agencyMailingAddress2 + "','"
										+ agencyMailingCity + "','"
										+ agencyMailingState + "','"
										+ agencyMailingZip + "','" + agentID
										+ "','" + agentFirstName + "','"
										+ agentLastName + "','" + agentEmail
										+ "','" + previousInspectionDate + "','"
										+ previousInspectionCompany + "','"
										+ previousInspectorFirstName + "','"
										+ previousInspectorLastName + "','"
										+ previousInspectorLicense + "','"
										+ yearBuilt + "','" + squareFootage + "','"
										+ numberOfStories + "','" + construction
										+ "','" + roofCovering + "','"
										+ roofDeckAttachment + "','"
										+ roofWallAttachment + "','" + roofShape
										+ "','" + secondaryWaterResistance + "','"
										+ openingCoverage + "','" + buildingType
										+ "')");
	
							} catch (Exception e) {
	
							}
						}
						catch(Exception e)
						{
							cf.Error_LogFile_Creation("Error  while create or select or delete, or insert the data in the cloud information in import ");
						}
						if (total <= 91) {
	
							total = temptotal + (int) (Totaltmp);
						} else {
							total =90;
						}
				}
			}
		}
	 public void GetInspectionName() throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException, XmlPullParserException {
			// TODO Auto-generated method stub
		 		int Cnt;
			    cf.CreateARRTable(14);
		 		String ID,Name,insp_local = "";
				SoapObject request = new SoapObject(cf.NAMESPACE,"LoadARRInspectionType ");
				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				envelope.dotNet = true;
				//request.addProperty("InspectorID",2387);
				envelope.setOutputSoapObject(request);
				HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL_ARR);
				androidHttpTransport.call(cf.NAMESPACE+"LoadARRInspectionType ",envelope);
				SoapObject inspnameinfo = (SoapObject) envelope.getResponse();
				
				
				if(String.valueOf(inspnameinfo).equals("null") || String.valueOf(inspnameinfo).equals(null) || String.valueOf(inspnameinfo).equals("anytype{}"))
				{
					Cnt=0;total = 90;
				}
				else
				{
				
				 Cnt = inspnameinfo.getPropertyCount();
				 double Totaltmp = 0.0;
				 int countchk = Cnt;
				 Totaltmp = (10.00 / (double) countchk);
				 double temptot = Totaltmp;
				 int temptotal = total;

					for(int i=0;i<inspnameinfo.getPropertyCount();i++)
					{
						
						SoapObject temp_result=(SoapObject) inspnameinfo.getProperty(i);
						ID = (String.valueOf(temp_result.getProperty("ID")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("ID")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("ID")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("ID"));
						Name=(String.valueOf(temp_result.getProperty("Name")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("Name")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("Name")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("Name"));
						try
						{
							
							if(ID.equals("13"))		  {insp_local="1";}
							else if(ID.equals("750")) {insp_local="2";}
							else if(ID.equals("28"))  {insp_local="3";}
							else if(ID.equals("9"))	  {insp_local="4";}
							else if(ID.equals("62"))  {insp_local="5";}
							else if(ID.equals("90"))  {insp_local="6";}
							else if(ID.equals("11"))  {insp_local="7";}
							else if(ID.equals("18"))  {insp_local="8";}
							else if(ID.equals("751")) {insp_local="9";}
							else { insp_local = "10"; }
							
									Cursor c1 = cf.SelectTablefunction(cf.inspnamelist, " where ARR_Insp_ID='" + cf.encode(ID) + "'");
									if(c1.getCount()==0)
									{
											cf.arr_db.execSQL("Insert into "+cf.inspnamelist+" (ARR_Insp_ID,ARR_Insp_Name,ARR_Custom_ID) values" +
													"('"+ID+"','"+cf.encode(Name).trim()+"','"+insp_local+"') ");
											System.out.println("Insert into "+cf.inspnamelist+" (ARR_Insp_ID,ARR_Insp_Name,ARR_Custom_ID) values" +
													"('"+ID+"','"+cf.encode(Name).trim()+"','"+insp_local+"') ");
									}
									else 
									{
											cf.arr_db.execSQL("UPDATE "+cf.inspnamelist+" SET ARR_Insp_Name='"+cf.encode(Name).trim()+"',ARR_Custom_ID='"+insp_local+"' WHERE ARR_Insp_ID='"+ID+"'");
									}
						}
						catch(Exception e)
						{
							cf.Error_LogFile_Creation("Error  while create or select or delete, or insert the data in the cloud information in import ");
						}
						if (total <= 91) {
	
							total = temptotal + (int) (Totaltmp);
						} else {
							total =90;
						}
				}
			}
		}
	 public void GetAllInspectionName() throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException, XmlPullParserException {
			// TODO Auto-generated method stub
		 		int Cnt;
			    cf.CreateARRTable(21);
		 		String ID,Name,insp_local = "";
				SoapObject request = new SoapObject(cf.NAMESPACE,"LOADALLINSPECTIONTYPE ");
				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				envelope.dotNet = true;
				//request.addProperty("InspectorID",2387);
				envelope.setOutputSoapObject(request);
				HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL_ARR);
				androidHttpTransport.call(cf.NAMESPACE+"LOADALLINSPECTIONTYPE ",envelope);
				SoapObject inspnameinfo = (SoapObject) envelope.getResponse();
				System.out.println("LOADALLINSPECTIONTYPE"+inspnameinfo);				
				if(String.valueOf(inspnameinfo).equals("null") || String.valueOf(inspnameinfo).equals(null) || String.valueOf(inspnameinfo).equals("anytype{}"))
				{
					Cnt=0;total = 95;
				}
				else
				{
				
				 Cnt = inspnameinfo.getPropertyCount();
				 double Totaltmp = 0.0;
				 int countchk = Cnt;
				 Totaltmp = (5.00 / (double) countchk);
				 double temptot = Totaltmp;
				 int temptotal = total;

					for(int i=0;i<inspnameinfo.getPropertyCount();i++)
					{
						
						SoapObject temp_result=(SoapObject) inspnameinfo.getProperty(i);
						ID = (String.valueOf(temp_result.getProperty("ID")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("ID")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("ID")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("ID"));
						Name=(String.valueOf(temp_result.getProperty("Name")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("Name")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("Name")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("Name"));
						try
						{
							
							if(ID.equals("13"))		  {insp_local="1";}
							else if(ID.equals("750")) {insp_local="2";}
							else if(ID.equals("28"))  {insp_local="3";}
							else if(ID.equals("9"))	  {insp_local="4";}
							else if(ID.equals("62"))  {insp_local="5";}
							else if(ID.equals("90"))  {insp_local="6";}
							else if(ID.equals("11"))  {insp_local="7";}
							else if(ID.equals("18"))  {insp_local="8";}
							else if(ID.equals("751")) {insp_local="9";}
							else { insp_local = "10"; }
							cf.CreateARRTable(21);
									Cursor c1 = cf.SelectTablefunction(cf.allinspnamelist, " where ARR_Insp_ID='" + cf.encode(ID) + "'");
									if(c1.getCount()==0)
									{
										System.out.println("Insert into "+cf.allinspnamelist+" (ARR_Insp_ID,ARR_Insp_Name,ARR_Custom_ID) values" +
												"('"+ID+"','"+cf.encode(Name).trim()+"','"+insp_local+"') ");
											cf.arr_db.execSQL("Insert into "+cf.allinspnamelist+" (ARR_Insp_ID,ARR_Insp_Name,ARR_Custom_ID) values" +
													"('"+ID+"','"+cf.encode(Name).trim()+"','"+insp_local+"') ");
											
									}
									else 
									{
											cf.arr_db.execSQL("UPDATE "+cf.allinspnamelist+" SET ARR_Insp_Name='"+cf.encode(Name).trim()+"',ARR_Custom_ID='"+insp_local+"' WHERE ARR_Insp_ID='"+ID+"'");
									}
						}
						catch(Exception e)
						{
							cf.Error_LogFile_Creation("Error  while create or select or delete, or insert the data in the custom information in import ");
						}
						if (total <= 96) {
	
							total = temptotal + (int) (Totaltmp);
						} else {
							total =95;
						}
				}
			}
		}
	 public void GetOriginalQuesMitigationInfo() throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException, XmlPullParserException {
			// TODO Auto-generated method stub
		 int Cnt;
		 		cf.CreateARRTable(12);
			    String SRID,bc_original, rc_original, rd_original, rw_original, rg_original, swr_original, op_original, wc_original;
			 	SoapObject request = new SoapObject(cf.NAMESPACE,"LOADORIGINALQUSDATATEXT");
				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				envelope.dotNet = true;
				request.addProperty("InspectorID",cf.Insp_id);
				envelope.setOutputSoapObject(request);
				HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL_ARR);
				androidHttpTransport.call(cf.NAMESPACE+"LOADORIGINALQUSDATATEXT",envelope);
				SoapObject originaltext = (SoapObject) envelope.getResponse();
				
				if(String.valueOf(originaltext).equals("null") || String.valueOf(originaltext).equals(null) || String.valueOf(originaltext).equals("anytype{}"))
				{
					Cnt=0;total = 80;
				}
				else
				{
				 Cnt = originaltext.getPropertyCount();
				 double Totaltmp = 0.0;
				 int countchk = Cnt;
				 Totaltmp = (15.00 / (double) countchk);
				 double temptot = Totaltmp;
				 int temptotal = total;
				
				for(int i=0;i<Cnt;i++)
				{
					
					SoapObject temp_result=(SoapObject) originaltext.getProperty(i);
					try
					{
					SRID = (String.valueOf(temp_result.getProperty("SRID")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("SRID")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("SRID")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("SRID"));
					bc_original=(String.valueOf(temp_result.getProperty("BuildCodeOriginal")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("BuildCodeOriginal")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("BuildCodeOriginal")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("BuildCodeOriginal"));
					rc_original=(String.valueOf(temp_result.getProperty("RoofCoverOriginal")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RoofCoverOriginal")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RoofCoverOriginal")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RoofCoverOriginal"));
					rd_original=(String.valueOf(temp_result.getProperty("RoofDeckOriginal")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RoofDeckOriginal")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RoofDeckOriginal")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RoofDeckOriginal"));					
					rw_original=(String.valueOf(temp_result.getProperty("RoofWallOriginalValue")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RoofWallOriginalValue")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RoofWallOriginalValue")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RoofWallOriginalValue"));
					rg_original=(String.valueOf(temp_result.getProperty("RoofGeoOriginalValue")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RoofGeoOriginalValue")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RoofGeoOriginalValue")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RoofGeoOriginalValue"));
					swr_original=(String.valueOf(temp_result.getProperty("SWROriginalValue")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("SWROriginalValue")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("SWROriginalValue")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("SWROriginalValue"));					
					op_original=(String.valueOf(temp_result.getProperty("OpenProtectOriginalValue")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("OpenProtectOriginalValue")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("OpenProtectOriginalValue")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("OpenProtectOriginalValue"));
					wc_original=(String.valueOf(temp_result.getProperty("WallConstrOriginal")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("WallConstrOriginal")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("WallConstrOriginal")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("WallConstrOriginal"));
					
					
						Cursor c2=cf.SelectTablefunction(cf.quesorigmiti_tbl," where SRID ='"+SRID+"'");
						if(c2.getCount()==0)
						{
							cf.arr_db.execSQL("Insert into "+cf.quesorigmiti_tbl+" (SRID,bcoriginal,rcoriginal,rdoriginal,rworiginal,rgoriginal,swroriginal,oporiginal,wcoriginal) values" +
									"('"+SRID+"','"+cf.encode(bc_original)+"','"+cf.encode(rc_original)+"','"+cf.encode(rd_original)+"','"+cf.encode(rw_original)+"','"+cf.encode(rg_original)+"','"+cf.encode(swr_original)+"','"+cf.encode(op_original)+"','"+cf.encode(wc_original)+"') ");
						}
						else if(c2.getCount()>=1)
						{
							cf.arr_db.execSQL("UPDATE "+cf.quesorigmiti_tbl+" SET bcoriginal='"+cf.encode(bc_original)+"',rcoriginal='"+cf.encode(rc_original)+"',rdoriginal='"+cf.encode(rd_original)+"',rworiginal='"+cf.encode(rw_original)+"',rgoriginal='"+cf.encode(rg_original)+"'," +
							"swroriginal='"+cf.encode(swr_original)+"',oporiginal='"+cf.encode(op_original)+"',wcoriginal='"+cf.encode(wc_original)+"' WHERE SRID='"+SRID+"'");
						}
						if (total <= 81) {

							total = temptotal + (int) (Totaltmp);
						} else {
							total = 80;
						}
						Totaltmp += temptot;
					}
					catch(Exception e)
					{
						cf.Error_LogFile_Creation("Error  while create or select or delete, or insert the data in the question original mitigation in import ");
					}
					
					
				}
				}
		}
	 public void GetAgentinformation() throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException, XmlPullParserException {
		// TODO Auto-generated method stub
		 	int Cnt;
			String SRID,AgencyName,AgentName,AgentAddress,AgentAddress2,AgentCity,AgentCounty,AgentRole,AgentState,AgentZip,AgentOffPhone,AgentContactPhone,AgentFax,AgentEmail,AgentWebSite;
		 	SoapObject request = new SoapObject(cf.NAMESPACE,"LOADAGENTINFORMATION");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			request.addProperty("InspectorID",cf.Insp_id);
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL_ARR);
			androidHttpTransport.call(cf.NAMESPACE+"LOADAGENTINFORMATION",envelope);
			SoapObject agentinfo = (SoapObject) envelope.getResponse();
			
			
			if(String.valueOf(agentinfo).equals("null") || String.valueOf(agentinfo).equals(null) || String.valueOf(agentinfo).equals("anytype{}"))
			{
				Cnt=0;total = 40;
			}
			else
			{
			 	 Cnt = agentinfo.getPropertyCount();
				 double Totaltmp = 0.0;
				 int countchk = Cnt;
				 Totaltmp = (10.00 / (double) countchk);
				 double temptot = Totaltmp;
				 int temptotal = total;
				cf.CreateARRTable(5);
				
				for(int i=0;i<agentinfo.getPropertyCount();i++)
				{
					SoapObject temp_result=(SoapObject) agentinfo.getProperty(i);
					SRID = (String.valueOf(temp_result.getProperty("SRID")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("SRID")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("SRID")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("SRID"));
					AgencyName=(String.valueOf(temp_result.getProperty("AgencyName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgencyName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgencyName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgencyName"));
					AgentName=(String.valueOf(temp_result.getProperty("AgentName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentName"));
					AgentAddress=(String.valueOf(temp_result.getProperty("AgentAddress")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentAddress")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentAddress")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentAddress"));
					AgentAddress2=(String.valueOf(temp_result.getProperty("AgentAddress2")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentAddress2")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentAddress2")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentAddress2"));
					AgentCity=(String.valueOf(temp_result.getProperty("AgentCity")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentCity")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentCity")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentCity"));
					AgentCounty=(String.valueOf(temp_result.getProperty("AgentCounty")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentCounty")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentCounty")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentCounty"));
					AgentRole=(String.valueOf(temp_result.getProperty("AgentRole")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentRole")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentRole")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentRole"));
					AgentState=(String.valueOf(temp_result.getProperty("AgentState")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentState")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentState")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentState"));
					AgentZip=(String.valueOf(temp_result.getProperty("AgentZip")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentZip")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentZip")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentZip"));
					AgentOffPhone=(String.valueOf(temp_result.getProperty("AgentOffPhone")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentOffPhone")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentOffPhone")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentOffPhone"));
					AgentContactPhone=(String.valueOf(temp_result.getProperty("AgentContactPhone")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentContactPhone")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentContactPhone")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentContactPhone"));
					AgentFax=(String.valueOf(temp_result.getProperty("AgentFax")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentFax")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentFax")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentFax"));
					AgentEmail=(String.valueOf(temp_result.getProperty("AgentEmail")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentEmail")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentEmail")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentEmail"));
					AgentWebSite=(String.valueOf(temp_result.getProperty("AgentWebSite")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("AgentWebSite")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("AgentWebSite")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("AgentWebSite"));
					
					try
					{
						Cursor c2=cf.SelectTablefunction(cf.Agent_tabble," where ARR_AI_SRID ='"+SRID+"'");
						if(c2.getCount()==0)
						{
							cf.arr_db.execSQL("Insert into "+cf.Agent_tabble+" (ARR_AI_SRID,ARR_AI_AgencyName,ARR_AI_AgentName,ARR_AI_AgentAddress,ARR_AI_AgentAddress2,ARR_AI_AgentCity,ARR_AI_AgentCounty,ARR_AI_AgentRole,ARR_AI_AgentState,ARR_AI_AgentZip,ARR_AI_AgentOffPhone,ARR_AI_AgentContactPhone,ARR_AI_AgentFax,ARR_AI_AgentEmail,ARR_AI_AgentWebSite) values" +
									"('"+SRID+"','"+AgencyName+"','"+AgentName+"','"+AgentAddress+"','"+AgentAddress2+"','"+AgentCity+"','"+AgentCounty+"','"+AgentRole+"','"+AgentState+"','"+AgentZip+"','"+cf.encode(AgentOffPhone)+"','"+cf.encode(AgentContactPhone)+"','"+AgentFax+"','"+AgentEmail+"','"+AgentWebSite+"') ");
						}
						else if(c2.getCount()>=1)
						{
							cf.arr_db.execSQL("UPDATE "+cf.Agent_tabble+" SET ARR_AI_SRID='"+SRID+"',ARR_AI_AgencyName='"+AgencyName+"',ARR_AI_AgentName='"+AgentName+"',ARR_AI_AgentAddress='"+AgentAddress+"',ARR_AI_AgentAddress2='"+AgentAddress2+"'," +
							"ARR_AI_AgentCity='"+AgentCity+"',ARR_AI_AgentCounty='"+AgentCounty+"',ARR_AI_AgentRole='"+AgentRole+"',ARR_AI_AgentState='"+AgentState+"',ARR_AI_AgentZip='"+AgentZip+"',ARR_AI_AgentOffPhone='"+AgentOffPhone+"'," +
							"ARR_AI_AgentContactPhone='"+AgentContactPhone+"',ARR_AI_AgentFax='"+AgentFax+"',ARR_AI_AgentEmail='"+AgentEmail+"',ARR_AI_AgentWebSite='"+AgentWebSite+"' WHERE ARR_AI_SRID='"+SRID+"'");
						}
					}
					catch(Exception e)
					{
						cf.Error_LogFile_Creation("Error  while create or select or delete, or insert the data in the cloud information in import ");
					}
					if (total <= 41) {
	
						total = temptotal + (int) (Totaltmp);
					} else {
						total = 40;
					}
					Totaltmp += temptot;
					
				}
			 }
	}
	 		public void GetRealtorinformation() throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException, XmlPullParserException {
			// TODO Auto-generated method stub
			 	int Cnt;
				SoapObject request = new SoapObject(cf.NAMESPACE,"LOADREALTORINFORMATION");
				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				envelope.dotNet = true;
				request.addProperty("InspectorID",cf.Insp_id);
				envelope.setOutputSoapObject(request);
				HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL_ARR);
				androidHttpTransport.call(cf.NAMESPACE+"LOADREALTORINFORMATION",envelope);
				SoapObject realtorinfo = (SoapObject) envelope.getResponse();
				System.out.println("realtorinfo="+realtorinfo);
				String SRID,RealtorName,CompanyName,RealtorAddress,RealtorAddress2,RealtorCity,RealtorCounty,RolesDesc,RealtorState,RealtorZip,RealtorWorkPhone,Realtor_Fax,RealtorEmail,Realtor_Website;
			 	
				if(String.valueOf(realtorinfo).equals("null") || String.valueOf(realtorinfo).equals(null) || String.valueOf(realtorinfo).equals("anytype{}"))
				{
					Cnt=0;total = 50;
				}
				else
				{
				 	 Cnt = realtorinfo.getPropertyCount();
					 double Totaltmp = 0.0;
					 int countchk = Cnt;
					 Totaltmp = (10.00 / (double) countchk);
					 double temptot = Totaltmp;
					 int temptotal = total;
					 cf.CreateARRTable(15);
					
					for(int i=0;i<realtorinfo.getPropertyCount();i++)
					{
						SoapObject temp_result=(SoapObject) realtorinfo.getProperty(i);
						SRID = (String.valueOf(temp_result.getProperty("s_SRID")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("s_SRID")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("s_SRID")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("s_SRID"));
						RealtorName=(String.valueOf(temp_result.getProperty("RealtorName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorName"));
						CompanyName=(String.valueOf(temp_result.getProperty("CompanyName")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("CompanyName")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("CompanyName")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("CompanyName"));
						RealtorAddress=(String.valueOf(temp_result.getProperty("RealtorAddress")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorAddress")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorAddress")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorAddress"));
						RealtorAddress2=(String.valueOf(temp_result.getProperty("RealtorAddress2")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorAddress2")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorAddress2")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorAddress2"));
						RealtorCity=(String.valueOf(temp_result.getProperty("RealtorCity")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorCity")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorCity")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorCity"));
						RealtorCounty=(String.valueOf(temp_result.getProperty("RealtorCounty")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorCounty")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorCounty")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorCounty"));
						RolesDesc=(String.valueOf(temp_result.getProperty("RolesDesc")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RolesDesc")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RolesDesc")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RolesDesc"));
						RealtorState=(String.valueOf(temp_result.getProperty("Realtor_State")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("Realtor_State")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("Realtor_State")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("Realtor_State"));
						RealtorZip=(String.valueOf(temp_result.getProperty("RealtorZip")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorZip")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorZip")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorZip"));
						RealtorWorkPhone=(String.valueOf(temp_result.getProperty("RealtorWorkPhone")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorWorkPhone")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorWorkPhone")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorWorkPhone"));
						Realtor_Fax=(String.valueOf(temp_result.getProperty("Realtor_Fax")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("Realtor_Fax")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("Realtor_Fax")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("Realtor_Fax"));
						RealtorEmail=(String.valueOf(temp_result.getProperty("RealtorEmail")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("RealtorEmail")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("RealtorEmail")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("RealtorEmail"));
						Realtor_Website=(String.valueOf(temp_result.getProperty("Realtor_Website")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("Realtor_Website")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("Realtor_Website")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("Realtor_Website"));
						
						try
						{
							Cursor c2=cf.SelectTablefunction(cf.Relator_table," where ARR_RL_SRID ='"+SRID+"'");
							if(c2.getCount()==0)
							{
								cf.arr_db.execSQL("Insert into "+cf.Relator_table+" (ARR_RL_SRID,ARR_RL_ComapanyName,ARR_RL_RealtorName,ARR_RL_RealAddress,ARR_RL_RealAddress2,ARR_RL_RealCity,ARR_RL_RealCounty,ARR_RL_RealRole,ARR_RL_RealState,ARR_RL_RealZip,ARR_RL_RealContactPhone,ARR_RL_RealFax,ARR_RL_RealEmail,ARR_RL_RealWebSite) values" +
										"('"+SRID+"','"+CompanyName+"','"+RealtorName+"','"+RealtorAddress+"','"+RealtorAddress2+"','"+RealtorCity+"','"+RealtorCounty+"','"+RolesDesc+"','"+RealtorState+"','"+RealtorZip+"','"+cf.encode(RealtorWorkPhone)+"','"+Realtor_Fax+"','"+RealtorEmail+"','"+Realtor_Website+"') ");
							}
							else if(c2.getCount()>=1)
							{
								cf.arr_db.execSQL("UPDATE "+cf.Relator_table+" SET ARR_RL_SRID='"+SRID+"',ARR_RL_ComapanyName='"+CompanyName+"',ARR_RL_RealtorName='"+RealtorName+"',ARR_RL_RealAddress='"+RealtorAddress+"',ARR_RL_RealAddress2='"+RealtorAddress2+"'," +
								"ARR_RL_RealCity='"+RealtorCity+"',ARR_RL_RealCounty='"+RealtorCounty+"',ARR_RL_RealRole='"+RolesDesc+"',ARR_RL_RealState='"+RealtorState+"',ARR_RL_RealZip='"+RealtorZip+"',ARR_RL_RealContactPhone='"+cf.encode(RealtorWorkPhone)+"'," +
								"ARR_RL_RealContactPhone='"+RealtorWorkPhone+"',ARR_RL_RealFax='"+Realtor_Fax+"',ARR_RL_RealEmail='"+RealtorEmail+"',ARR_RL_RealWebSite='"+Realtor_Website+"' WHERE ARR_RL_SRID='"+SRID+"'");
							}
						}
						catch(Exception e)
						{
							cf.Error_LogFile_Creation("Error  while create or select or delete, or insert the data in the cloud information in import ");
						}
						if (total <= 51) {
		
							total = temptotal + (int) (Totaltmp);
						} else {
							total = 50;
						}
						Totaltmp += temptot;
						
					}
				 }
		}
	 public void GetPolicyholderInfo(SoapObject objInsert) throws NetworkErrorException,IOException, XmlPullParserException, SocketTimeoutException
	 {
		 String Workorderno,ScopeOfInspection,ScopeOfInspectionOthers,InsuredIs,InsuredIsOthers,NeighbourhoodIs,NeighbourhoodIsSub,NeighbourhoodIsSubOthers,
		 OccupancyType,OccupancyTypeOthers,BuildingOccupOccupiedPercent,BuildingOccupVacantPercent,BuildingOccupancy,BuildingOccupancyOthers,PermitConfirmed,
		 PermitConfirmedComments,BalconiesPresent,AdditionalStructures,ElectricityTurnedOn,WaterTurnedOn,AccessConfirmed,PresentlyOccupied,SewerServices,
		 BuildingType,BuildingTypeOthers,BuildingNumberText,WaterService,LightPoles,FencesPrepLine,adrschk,NoOfUnits,Permitconfirmed,PermitConfcomments;
		 int Cnt,isCustomInspection;
		 cf.CreateARRTable(3);
		 
		 System.out.println("objInsert"+objInsert.getPropertyCount()+""+objInsert);
		 
		 if(String.valueOf(objInsert).equals("null") || String.valueOf(objInsert).equals(null) || String.valueOf(objInsert).equals("anytype{}"))
		 {
				Cnt=0;total = 50;
		 }
		 else
		 {
			Cnt = objInsert.getPropertyCount();			
			double Totaltmp = 0.0;
			int countchk = Cnt;
			Totaltmp = (10.00 / (double) countchk);
			double temptot = Totaltmp;
			int temptotal = total;
			for (int i = 0; i < Cnt; i++) 
			{
				SoapObject obj = (SoapObject) objInsert.getProperty(i);
				
				try 
				{
					 HomeId = (String.valueOf(obj.getProperty("SRID")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("SRID")).equals("NA"))?"":(String.valueOf(obj.getProperty("SRID")).equals("N/A"))?"":String.valueOf(obj.getProperty("SRID"));
					
					 PH_Fname = (String.valueOf(obj.getProperty("FirstName")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("FirstName")).equals("NA"))?"":(String.valueOf(obj.getProperty("FirstName")).equals("N/A"))?"":String.valueOf(obj.getProperty("FirstName"));
					 PH_Lname = (String.valueOf(obj.getProperty("LastName")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("LastName")).equals("NA"))?"":(String.valueOf(obj.getProperty("LastName")).equals("N/A"))?"":String.valueOf(obj.getProperty("LastName"));
					 PH_Mname = (String.valueOf(obj.getProperty("MiddleName")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("MiddleName")).equals("NA"))?"":(String.valueOf(obj.getProperty("MiddleName")).equals("N/A"))?"":String.valueOf(obj.getProperty("MiddleName"));
					 PH_Address1 = (String.valueOf(obj.getProperty("Address1")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Address1")).equals("NA"))?"":(String.valueOf(obj.getProperty("Address1")).equals("N/A"))?"":String.valueOf(obj.getProperty("Address1"));
					 PH_Address2 = (String.valueOf(obj.getProperty("Address2")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Address2")).equals("NA"))?"":(String.valueOf(obj.getProperty("Address2")).equals("N/A"))?"":String.valueOf(obj.getProperty("Address2"));
					 PH_City = (String.valueOf(obj.getProperty("City")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("City")).equals("NA"))?"":(String.valueOf(obj.getProperty("City")).equals("N/A"))?"":String.valueOf(obj.getProperty("City"));
					 PH_State = (String.valueOf(obj.getProperty("State")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("State")).equals("NA"))?"":(String.valueOf(obj.getProperty("State")).equals("N/A"))?"":String.valueOf(obj.getProperty("State"));
					 PH_County = (String.valueOf(obj.getProperty("Country")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Country")).equals("NA"))?"":(String.valueOf(obj.getProperty("Country")).equals("N/A"))?"":String.valueOf(obj.getProperty("Country"));
					 PH_Zip = (String.valueOf(obj.getProperty("Zip")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Zip")).equals("NA"))?"":(String.valueOf(obj.getProperty("Zip")).equals("N/A"))?"":String.valueOf(obj.getProperty("Zip"));
					 PH_HPhone = (String.valueOf(obj.getProperty("HomePhone")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("HomePhone")).equals("NA"))?"":(String.valueOf(obj.getProperty("HomePhone")).equals("N/A"))?"":String.valueOf(obj.getProperty("HomePhone"));
					 PH_CPhone = (String.valueOf(obj.getProperty("CellPhone")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("CellPhone")).equals("NA"))?"":(String.valueOf(obj.getProperty("CellPhone")).equals("N/A"))?"":String.valueOf(obj.getProperty("CellPhone"));
					 PH_WPhone = (String.valueOf(obj.getProperty("WorkPhone")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("WorkPhone")).equals("NA"))?"":(String.valueOf(obj.getProperty("WorkPhone")).equals("N/A"))?"":String.valueOf(obj.getProperty("WorkPhone"));
					 PH_Email = (String.valueOf(obj.getProperty("Email")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Email")).equals("NA"))?"":(String.valueOf(obj.getProperty("Email")).equals("N/A"))?"":String.valueOf(obj.getProperty("Email"));
					 PH_CONTPERSON = (String.valueOf(obj.getProperty("ContactPerson")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("ContactPerson")).equals("NA"))?"":(String.valueOf(obj.getProperty("ContactPerson")).equals("N/A"))?"":String.valueOf(obj.getProperty("ContactPerson"));
					 PH_Policyno = (String.valueOf(obj.getProperty("OwnerPolicyNo")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("OwnerPolicyNo")).equals("NA"))?"":(String.valueOf(obj.getProperty("OwnerPolicyNo")).equals("N/A"))?"":String.valueOf(obj.getProperty("OwnerPolicyNo"));
					 PH_Status = (String.valueOf(obj.getProperty("Status")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Status")).equals("NA"))?"":(String.valueOf(obj.getProperty("Status")).equals("N/A"))?"":String.valueOf(obj.getProperty("Status"));
					 PH_SubStatus = (String.valueOf(obj.getProperty("SubStatusID")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("SubStatusID")).equals("NA"))?"":(String.valueOf(obj.getProperty("SubStatusID")).equals("N/A"))?"":String.valueOf(obj.getProperty("SubStatusID"));
					 CompID = (String.valueOf(obj.getProperty("CompanyId")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("CompanyId")).equals("NA"))?"":(String.valueOf(obj.getProperty("CompanyId")).equals("N/A"))?"":String.valueOf(obj.getProperty("CompanyId"));
					 InspectorId = (String.valueOf(obj.getProperty("InspectorId")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectorId")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectorId")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectorId"));
					 ScheduledDate = (String.valueOf(obj.getProperty("ScheduledDate")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("ScheduledDate")).equals("NA"))?"":(String.valueOf(obj.getProperty("ScheduledDate")).equals("N/A"))?"":String.valueOf(obj.getProperty("ScheduledDate"));
					 YearBuilt = (String.valueOf(obj.getProperty("YearBuilt")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("YearBuilt")).equals("NA"))?"":(String.valueOf(obj.getProperty("YearBuilt")).equals("N/A"))?"":String.valueOf(obj.getProperty("YearBuilt"));
					 PH_Noofstories = (String.valueOf(obj.getProperty("Nstories")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Nstories")).equals("NA"))?"":(String.valueOf(obj.getProperty("Nstories")).equals("N/A"))?"":String.valueOf(obj.getProperty("Nstories"));
					 PH_MainInspectionTypeId = (String.valueOf(obj.getProperty("MaininspectiontypeId")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("MaininspectiontypeId")).equals("NA"))?"":(String.valueOf(obj.getProperty("MaininspectiontypeId")).equals("N/A"))?"":String.valueOf(obj.getProperty("MaininspectiontypeId"));
					 PH_InspectionName = (String.valueOf(obj.getProperty("InspectionNames")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectionNames")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectionNames")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectionNames"));
					 ScheduledCreatedDate = (String.valueOf(obj.getProperty("ScheduleCreatedDate")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("ScheduleCreatedDate")).equals("NA"))?"":(String.valueOf(obj.getProperty("ScheduleCreatedDate")).equals("N/A"))?"":String.valueOf(obj.getProperty("ScheduleCreatedDate"));
					 AssignedDate = (String.valueOf(obj.getProperty("AssignedDate")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("AssignedDate")).equals("NA"))?"":(String.valueOf(obj.getProperty("AssignedDate")).equals("N/A"))?"":String.valueOf(obj.getProperty("AssignedDate"));
					 InspectionStartTime = (String.valueOf(obj.getProperty("InspectionStartTime")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectionStartTime")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectionStartTime")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectionStartTime"));
					 InspectionEndTime = (String.valueOf(obj.getProperty("InspectionEndTime")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectionEndTime")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectionEndTime")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectionEndTime"));
					 Schedulecomments = (String.valueOf(obj.getProperty("InspectionComment")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectionComment")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectionComment")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectionComment"));
					 IsInspected= (String.valueOf(obj.getProperty("IsInspected")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("IsInspected")).equals("NA"))?"":(String.valueOf(obj.getProperty("IsInspected")).equals("N/A"))?"":String.valueOf(obj.getProperty("IsInspected"));
					 PH_InsuranceCompany = (String.valueOf(obj.getProperty("InsuranceCompany")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InsuranceCompany")).equals("NA"))?"":(String.valueOf(obj.getProperty("InsuranceCompany")).equals("N/A"))?"":String.valueOf(obj.getProperty("InsuranceCompany"));
					 BuildingSize = (String.valueOf(obj.getProperty("BuildingSize")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("BuildingSize")).equals("NA"))?"":(String.valueOf(obj.getProperty("BuildingSize")).equals("N/A"))?"":String.valueOf(obj.getProperty("BuildingSize"));
					 latitude = (String.valueOf(obj.getProperty("Latitude")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Latitude")).equals("NA"))?"":(String.valueOf(obj.getProperty("Latitude")).equals("N/A"))?"":String.valueOf(obj.getProperty("Latitude"));
					 longtitude = (String.valueOf(obj.getProperty("Longitude")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Longitude")).equals("NA"))?"":(String.valueOf(obj.getProperty("Longitude")).equals("N/A"))?"":String.valueOf(obj.getProperty("Longitude"));				
					 PH_InspectionTypeId = (String.valueOf(obj.getProperty("InspectionIds")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectionIds")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectionIds")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectionIds"));
					 MailingAddress= (String.valueOf(obj.getProperty("MailingAddress")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("MailingAddress")).equals("NA"))?"":(String.valueOf(obj.getProperty("MailingAddress")).equals("N/A"))?"":String.valueOf(obj.getProperty("MailingAddress"));
					 MailingAddress2 = (String.valueOf(obj.getProperty("MailingAddress2")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("MailingAddress2")).equals("NA"))?"":(String.valueOf(obj.getProperty("MailingAddress2")).equals("N/A"))?"":String.valueOf(obj.getProperty("MailingAddress2"));
					 Mailingcity = (String.valueOf(obj.getProperty("Mailingcity")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Mailingcity")).equals("NA"))?"":(String.valueOf(obj.getProperty("Mailingcity")).equals("N/A"))?"":String.valueOf(obj.getProperty("Mailingcity"));
					 MailingState = (String.valueOf(obj.getProperty("MailingState")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("MailingState")).equals("NA"))?"":(String.valueOf(obj.getProperty("MailingState")).equals("N/A"))?"":String.valueOf(obj.getProperty("MailingState"));
					 MailingCounty = (String.valueOf(obj.getProperty("MailingCounty")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("MailingCounty")).equals("NA"))?"":(String.valueOf(obj.getProperty("MailingCounty")).equals("N/A"))?"":String.valueOf(obj.getProperty("MailingCounty"));				
					 Mailingzip = (String.valueOf(obj.getProperty("Mailingzip")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Mailingzip")).equals("NA"))?"":(String.valueOf(obj.getProperty("Mailingzip")).equals("N/A"))?"":String.valueOf(obj.getProperty("Mailingzip"));
					
					 CommercialFlag = (String.valueOf(obj.getProperty("i_COMMflag")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("i_COMMflag")).equals("NA"))?"":(String.valueOf(obj.getProperty("i_COMMflag")).equals("N/A"))?"":String.valueOf(obj.getProperty("i_COMMflag"));
					 Workorderno= (String.valueOf(obj.getProperty("Workorderno")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Workorderno")).equals("NA"))?"":(String.valueOf(obj.getProperty("Workorderno")).equals("N/A"))?"":String.valueOf(obj.getProperty("Workorderno"));
					 
					 
					 ScopeOfInspection= (String.valueOf(obj.getProperty("ScopeOfInspection")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("ScopeOfInspection")).equals("NA"))?"":(String.valueOf(obj.getProperty("ScopeOfInspection")).equals("N/A"))?"":String.valueOf(obj.getProperty("ScopeOfInspection"));
					 ScopeOfInspectionOthers= (String.valueOf(obj.getProperty("ScopeOfInspectionOthers")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("ScopeOfInspectionOthers")).equals("NA"))?"":(String.valueOf(obj.getProperty("ScopeOfInspectionOthers")).equals("N/A"))?"":String.valueOf(obj.getProperty("ScopeOfInspectionOthers"));
					 InsuredIs= (String.valueOf(obj.getProperty("InsuredIs")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InsuredIs")).equals("NA"))?"":(String.valueOf(obj.getProperty("InsuredIs")).equals("N/A"))?"":String.valueOf(obj.getProperty("InsuredIs"));
					 InsuredIsOthers= (String.valueOf(obj.getProperty("InsuredIsOthers")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InsuredIsOthers")).equals("NA"))?"":(String.valueOf(obj.getProperty("InsuredIsOthers")).equals("N/A"))?"":String.valueOf(obj.getProperty("InsuredIsOthers"));
					 NeighbourhoodIs= (String.valueOf(obj.getProperty("NeighbourhoodIs")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("NeighbourhoodIs")).equals("NA"))?"":(String.valueOf(obj.getProperty("NeighbourhoodIs")).equals("N/A"))?"":String.valueOf(obj.getProperty("NeighbourhoodIs"));
					 NeighbourhoodIsSub= (String.valueOf(obj.getProperty("NeighbourhoodIsSub")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("NeighbourhoodIsSub")).equals("NA"))?"":(String.valueOf(obj.getProperty("NeighbourhoodIsSub")).equals("N/A"))?"":String.valueOf(obj.getProperty("NeighbourhoodIsSub"));
					 NeighbourhoodIsSubOthers= (String.valueOf(obj.getProperty("NeighbourhoodIsSubOthers")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("NeighbourhoodIsSubOthers")).equals("NA"))?"":(String.valueOf(obj.getProperty("NeighbourhoodIsSubOthers")).equals("N/A"))?"":String.valueOf(obj.getProperty("NeighbourhoodIsSubOthers"));
					 OccupancyType= (String.valueOf(obj.getProperty("OccupancyType")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("OccupancyType")).equals("NA"))?"":(String.valueOf(obj.getProperty("OccupancyType")).equals("N/A"))?"":String.valueOf(obj.getProperty("OccupancyType"));
					 OccupancyTypeOthers= (String.valueOf(obj.getProperty("OccupancyTypeOthers")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("OccupancyTypeOthers")).equals("NA"))?"":(String.valueOf(obj.getProperty("OccupancyTypeOthers")).equals("N/A"))?"":String.valueOf(obj.getProperty("OccupancyTypeOthers"));
					 NoOfUnits= (String.valueOf(obj.getProperty("NoOfUnits")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("NoOfUnits")).equals("NA"))?"":(String.valueOf(obj.getProperty("NoOfUnits")).equals("N/A"))?"":String.valueOf(obj.getProperty("NoOfUnits"));
					 BuildingOccupOccupiedPercent= (String.valueOf(obj.getProperty("BuildingOccupOccupiedPercent")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("BuildingOccupOccupiedPercent")).equals("NA"))?"":(String.valueOf(obj.getProperty("BuildingOccupOccupiedPercent")).equals("N/A"))?"":String.valueOf(obj.getProperty("BuildingOccupOccupiedPercent"));
					 BuildingOccupVacantPercent= (String.valueOf(obj.getProperty("BuildingOccupVacantPercent")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("BuildingOccupVacantPercent")).equals("NA"))?"":(String.valueOf(obj.getProperty("BuildingOccupVacantPercent")).equals("N/A"))?"":String.valueOf(obj.getProperty("BuildingOccupVacantPercent"));
					 BuildingOccupancy= (String.valueOf(obj.getProperty("BuildingOccupancy")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("BuildingOccupancy")).equals("NA"))?"":(String.valueOf(obj.getProperty("BuildingOccupancy")).equals("N/A"))?"":String.valueOf(obj.getProperty("BuildingOccupancy"));
					 BuildingOccupancyOthers= (String.valueOf(obj.getProperty("BuildingOccupancyOthers")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("BuildingOccupancyOthers")).equals("NA"))?"":(String.valueOf(obj.getProperty("BuildingOccupancyOthers")).equals("N/A"))?"":String.valueOf(obj.getProperty("BuildingOccupancyOthers"));
					 AdditionalStructures= (String.valueOf(obj.getProperty("AdditionalStructures")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("AdditionalStructures")).equals("NA"))?"":(String.valueOf(obj.getProperty("AdditionalStructures")).equals("N/A"))?"":String.valueOf(obj.getProperty("AdditionalStructures"));
					 ElectricityTurnedOn= (String.valueOf(obj.getProperty("ElectricityTurnedOn")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("ElectricityTurnedOn")).equals("NA"))?"":(String.valueOf(obj.getProperty("ElectricityTurnedOn")).equals("N/A"))?"":String.valueOf(obj.getProperty("ElectricityTurnedOn"));
					 WaterTurnedOn= (String.valueOf(obj.getProperty("WaterTurnedOn")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("WaterTurnedOn")).equals("NA"))?"":(String.valueOf(obj.getProperty("WaterTurnedOn")).equals("N/A"))?"":String.valueOf(obj.getProperty("WaterTurnedOn"));
					 AccessConfirmed= (String.valueOf(obj.getProperty("AccessConfirmed")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("AccessConfirmed")).equals("NA"))?"":(String.valueOf(obj.getProperty("AccessConfirmed")).equals("N/A"))?"":String.valueOf(obj.getProperty("AccessConfirmed"));
					 PresentlyOccupied= (String.valueOf(obj.getProperty("PresentlyOccupied")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("PresentlyOccupied")).equals("NA"))?"":(String.valueOf(obj.getProperty("PresentlyOccupied")).equals("N/A"))?"":String.valueOf(obj.getProperty("PresentlyOccupied"));
					 SewerServices= (String.valueOf(obj.getProperty("SewerServices")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("SewerServices")).equals("NA"))?"":(String.valueOf(obj.getProperty("SewerServices")).equals("N/A"))?"":String.valueOf(obj.getProperty("SewerServices"));
					 BuildingType= (String.valueOf(obj.getProperty("BuildingType")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("BuildingType")).equals("NA"))?"":(String.valueOf(obj.getProperty("BuildingType")).equals("N/A"))?"":String.valueOf(obj.getProperty("BuildingType"));
					 BuildingTypeOthers= (String.valueOf(obj.getProperty("BuildingTypeOthers")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("BuildingTypeOthers")).equals("NA"))?"":(String.valueOf(obj.getProperty("BuildingTypeOthers")).equals("N/A"))?"":String.valueOf(obj.getProperty("BuildingTypeOthers"));
					 WaterService= (String.valueOf(obj.getProperty("WaterService")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("WaterService")).equals("NA"))?"":(String.valueOf(obj.getProperty("WaterService")).equals("N/A"))?"":String.valueOf(obj.getProperty("WaterService"));
					 LightPoles= (String.valueOf(obj.getProperty("LightPoles")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("LightPoles")).equals("NA"))?"":(String.valueOf(obj.getProperty("LightPoles")).equals("N/A"))?"":String.valueOf(obj.getProperty("LightPoles"));
					 FencesPrepLine= (String.valueOf(obj.getProperty("FencesPrepLine")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("FencesPrepLine")).equals("NA"))?"":(String.valueOf(obj.getProperty("FencesPrepLine")).equals("N/A"))?"":String.valueOf(obj.getProperty("FencesPrepLine"));
					 Permitconfirmed= (String.valueOf(obj.getProperty("PermitConfirmed")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("PermitConfirmed")).equals("NA"))?"":(String.valueOf(obj.getProperty("PermitConfirmed")).equals("N/A"))?"":String.valueOf(obj.getProperty("PermitConfirmed"));
					 BuildingNo = (String.valueOf(obj.getProperty("Buildingno")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("Buildingno")).equals("NA"))?"":(String.valueOf(obj.getProperty("Buildingno")).equals("N/A"))?"":String.valueOf(obj.getProperty("Buildingno"));
					 BuildingNumberText= (String.valueOf(obj.getProperty("BuildingNumberText")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("BuildingNumberText")).equals("NA"))?"":(String.valueOf(obj.getProperty("BuildingNumberText")).equals("N/A"))?"":String.valueOf(obj.getProperty("BuildingNumberText"));
					System.out.println("BuildingNumberText"+BuildingNumberText+"PH_Fname"+PH_Fname+"PH_Lname"+PH_Lname+"BuildingNo"+BuildingNo+"LAST="+CommercialFlag);
					 cf.CreateARRTable(150);
					 Cursor c3 = cf.SelectTablefunction(cf.BI_GeneralDup," where SRID='"+cf.encode(HomeId)+"'");
					 if(c3.getCount() == 0)
					 {
						 cf.arr_db.execSQL("INSERT INTO "
									+ cf.BI_GeneralDup
									+ " (SRID,ScopeOfInspection,fld_Scopeofinspother,fld_Insuredis,fld_Insuredisother,fld_Neighbourhoodis," +
									"fld_NeighbourhoodisSub,fld_NeighbourhoodisSubOther,fld_OccupancyType,fld_OccupancyTypeother,fld_noofunits," +
									"fld_BuildOccuper,fld_BuildVacantper,fld_BuildOccu,fld_BuildOccuother,fld_Permitconfirmed,fld_addistru,fld_elec_turnon," +
									"fld_water_turnon,fld_acesss_confirmed,fld_pres_occu,fld_sewer_serv,fld_build_type,fld_build_typeother," +
									"fld_waterservice,fld_LightPoles,fld_FencesPrepLine)"
									+ "VALUES ('"+cf.encode(HomeId)+"','"+cf.encode(ScopeOfInspection)+"','"+cf.encode(ScopeOfInspectionOthers)+"','"+cf.encode(InsuredIs)+"'," +
											"'"+cf.encode(InsuredIsOthers)+"','"+cf.encode(NeighbourhoodIs)+"','"+cf.encode(NeighbourhoodIsSub)+"','"+cf.encode(NeighbourhoodIsSubOthers)+"','"+cf.encode(OccupancyType)+"'," +
											"'"+cf.encode(OccupancyTypeOthers)+"','"+NoOfUnits+"','"+BuildingOccupOccupiedPercent+"','"+BuildingOccupVacantPercent+"'," +
											"'"+cf.encode(BuildingOccupancy)+"','"+cf.encode(BuildingOccupancyOthers)+"','"+cf.encode(Permitconfirmed)+"','"+cf.encode(AdditionalStructures)+"','"+cf.encode(ElectricityTurnedOn)+"','"+cf.encode(WaterTurnedOn)+"'," +
											"'"+cf.encode(AccessConfirmed)+"','"+cf.encode(PresentlyOccupied)+"','"+cf.encode(SewerServices)+"','"+cf.encode(BuildingType)+"','"+cf.encode(BuildingTypeOthers)+"'," +
											"'"+cf.encode(WaterService)+"','"+cf.encode(LightPoles)+"','"+cf.encode(FencesPrepLine)+"')");
					 }
					 else
					 {
						 cf.arr_db.execSQL("UPDATE " + cf.BI_GeneralDup
									+ " SET ScopeOfInspection='"+cf.encode(ScopeOfInspection)+"',fld_Scopeofinspother='" + cf.encode(ScopeOfInspectionOthers)
									+ "',fld_Insuredis='"+ cf.encode(InsuredIs)+ "',fld_Insuredisother='"+ cf.encode(InsuredIsOthers)+ "'," +
									"fld_Neighbourhoodis='"+ cf.encode(NeighbourhoodIs) + "',fld_NeighbourhoodisSub='"+ cf.encode(NeighbourhoodIsSub) + "',fld_NeighbourhoodisSubOther='"
									+ cf.encode(NeighbourhoodIsSubOthers) + "',fld_OccupancyType='"+cf.encode(OccupancyType)+"',fld_OccupancyTypeother='"+cf.encode(OccupancyTypeOthers)+"',fld_noofunits='"+NoOfUnits+"'," +
									"fld_BuildOccuper='"+BuildingOccupOccupiedPercent+"',fld_Permitconfirmed='"+cf.encode(Permitconfirmed)+"',fld_BuildVacantper='"+BuildingOccupVacantPercent+"',fld_BuildOccu='"+cf.encode(BuildingOccupancy)+"'," +
									"fld_BuildOccuother='"+cf.encode(BuildingOccupancyOthers)+"',fld_addistru='"+cf.encode(AdditionalStructures)+"',fld_elec_turnon='"+cf.encode(ElectricityTurnedOn)+"'," +
									"fld_water_turnon='"+cf.encode(WaterTurnedOn)+"',fld_acesss_confirmed='"+cf.encode(AccessConfirmed)+"',fld_pres_occu='"+cf.encode(PresentlyOccupied)+"'," +
									"fld_sewer_serv='"+cf.encode(SewerServices)+"',fld_build_type='"+cf.encode(BuildingType)+"',fld_build_typeother='"+cf.encode(BuildingTypeOthers)+"'," +
									"fld_waterservice='"+cf.encode(WaterService)+"',fld_LightPoles='"+cf.encode(LightPoles)+"',fld_FencesPrepLine='"+cf.encode(FencesPrepLine)+"' " +
									"WHERE SRID ='" + cf.encode(HomeId)+ "'");
					 }
					 //System.out.println("HomeId="+HomeId+"PH_HPhone="+PH_HPhone+"PH_WPhone"+PH_WPhone+"PH_CPhone"+PH_CPhone+"MailingState"+MailingState);
					 System.out.println("WWWWW="+PH_InspectionTypeId);
					 String s="";
					 if(PH_InspectionTypeId.contains(","))
					 {
						 String id[] = PH_InspectionTypeId.split(",");
						 for(int k=0;k<id.length;k++)
						 {
							 System.out.println("Klenth="+id.length+" K ="+id[k]);
							 if(id[k].matches("9") || id[k].matches("11") || id[k].matches("13") || id[k].matches("18") || id[k].matches("28")
									 || id[k].matches("750") || id[k].matches("751"))
							 {
									 s += "true"+"~";
							 }
							 else
							 {
									 s += "false"+"~";
							 }
						 }			
					 }
					 else
					 {
						 if(PH_InspectionTypeId.matches("9") || PH_InspectionTypeId.matches("11") || PH_InspectionTypeId.matches("13") || PH_InspectionTypeId.matches("18") || PH_InspectionTypeId.matches("28")
								 || PH_InspectionTypeId.matches("750") || PH_InspectionTypeId.matches("751"))
						 {
							 s = "true";
						 }
						 else
						 {
							 s = "false";
						 }
					 }
					 
					 System.out.println("NEed s="+s);
					 
					 if(s.contains("false"))
					 {
						 isCustomInspection=1;
					 }
					 else
					 {
						 isCustomInspection=0;
					 }
					 System.out.println("BFORE shh");
					 Cursor c1 = cf.SelectTablefunction(cf.policyholder," where ARR_PH_InspectorId='" + cf.encode(cf.Insp_id) + "' and ARR_PH_SRID='"+cf.encode(HomeId)+"'");
					 if(c1.getCount() >= 1)
					 {
						 c1.moveToFirst();
								 if(c1.getString(c1.getColumnIndex("ARR_PH_IsInspected")).equals("0"))
								 {
									 cf.arr_db.execSQL("UPDATE "
												+ cf.policyholder
												+ " SET  ARR_PH_FirstName ='"+ cf.encode(PH_Fname)+"'," +
												       " ARR_PH_LastName  ='"+ cf.encode(PH_Lname)+"'," +
												       " ARR_PH_Address1 ='"+ cf.encode(PH_Address1)+"'," +
												       " ARR_PH_Address2 ='"+ cf.encode(PH_Address2)+"'," +
												       " ARR_PH_City ='"+ cf.encode(PH_City)+"'," +
												       " ARR_PH_Zip ='"+ cf.encode(PH_Zip)+"'," +
												       " ARR_PH_State ='"+ cf.encode(PH_State)+"', "+
												       " ARR_PH_County ='"+ cf.encode(PH_County)+"',"+
												       " ARR_PH_Policyno ='"+ cf.encode(PH_Policyno)+"',"+
												       " ARR_PH_Inspectionfees ='"+ cf.encode(PH_Inspectionfees)+"',"+
												       " ARR_PH_InsuranceCompany ='"+ cf.encode(PH_InsuranceCompany)+"',"+
												       " ARR_PH_HomePhone ='"+ cf.encode(PH_HPhone)+"',"+
												       " ARR_PH_WorkPhone ='"+ cf.encode(PH_WPhone)+"',"+
												       " ARR_PH_CellPhone ='"+ cf.encode(PH_CPhone)+"', "+
												       " ARR_PH_NOOFSTORIES ='"+cf.encode(PH_Noofstories)+"',"+
												       " ARR_PH_WEBSITE  ='',"+
												       " ARR_PH_Email ='"+ cf.encode(PH_Email)+"', "+										       
												       " ARR_PH_EmailChkbx ='"+ cf.encode(PH_EmailChk)+"', "+
												       " ARR_PH_InspectionTypeId ='"+ cf.encode(PH_MainInspectionTypeId)+"', "+
												       " ARR_PH_IsInspected ='"+ cf.encode(PH_IsInspected)+"',"+
												       " ARR_PH_IsUploaded ='"+ cf.encode(PH_IsUploaded)+"',"+
												       " ARR_PH_Status='"+ cf.encode(PH_Status)+"',"+
												       " ARR_PH_SubStatus='"+ cf.encode(PH_SubStatus)+"'," +
												       " ARR_Schedule_ScheduledDate ='"+ cf.encode(ScheduledDate)+"'," +
												       " ARR_Schedule_InspectionStartTime='"+cf.encode(InspectionStartTime)+"'," +
												       " ARR_Schedule_InspectionEndTime ='"+cf.encode(InspectionEndTime)+"'," +
												       " ARR_Schedule_Comments ='"+cf.encode(Schedulecomments)+"',"+
												       " ARR_Schedule_ScheduleCreatedDate ='"+cf.encode(ScheduledCreatedDate)+"',"+
												       " ARR_Schedule_AssignedDate ='"+cf.encode(AssignedDate)+"',"+
												       " ARR_ScheduleFlag =0 ,iscustominspection='"+isCustomInspection+"'," +
												       " ARR_YearBuilt ='"+YearBuilt+"',"+
												       " ARR_ContactPerson ='"+cf.encode(PH_CONTPERSON)+"',fld_noofbuildings='"+BuildingNo+"',BuidingSize='"+cf.decode(BuildingSize)+"',fld_latitude='"+latitude+"',fld_longitude='"+longtitude+"',fld_inspectionids='"+cf.encode(PH_InspectionTypeId)+"',Company_ID='"+CompID+"',Commercial_Flag='"+CommercialFlag+"'"+
												       " where ARR_PH_SRID='" + cf.encode(HomeId)+ "' and ARR_PH_InspectorId='"+ cf.encode(InspectorId)+"'");
									 } }
						 else
						 {
							 /*INSERTING THE TABLE - POLICYHOLDER*/
							
							 cf.arr_db.execSQL("INSERT INTO "
										+ cf.policyholder
										+ " (ARR_PH_InspectorId,ARR_PH_SRID,ARR_PH_FirstName,ARR_PH_LastName,ARR_PH_Address1,ARR_PH_Address2," +
										" ARR_PH_City,ARR_PH_Zip,ARR_PH_State,ARR_PH_County,ARR_PH_Policyno,ARR_PH_Inspectionfees,ARR_PH_InsuranceCompany," +
										" ARR_PH_HomePhone,ARR_PH_WorkPhone,ARR_PH_CellPhone,ARR_PH_Email,ARR_PH_EmailChkbx,ARR_PH_InspectionTypeId," +
										" ARR_PH_IsInspected,ARR_PH_IsUploaded,ARR_PH_Status,ARR_PH_SubStatus,ARR_Schedule_ScheduledDate,ARR_Schedule_InspectionStartTime,"+
									  " ARR_Schedule_InspectionEndTime,ARR_Schedule_Comments,ARR_Schedule_ScheduleCreatedDate,ARR_Schedule_AssignedDate,ARR_ScheduleFlag,ARR_YearBuilt,ARR_ContactPerson,BuidingSize,ARR_PH_NOOFSTORIES,ARR_PH_WEBSITE,fld_latitude,fld_longitude,fld_homeownersign,fld_homewonercaption,fld_paperworksign,fld_paperworkcaption,fld_noofbuildings,fld_inspectionids,Company_ID,Commercial_Flag,iscustominspection)"
										+ " VALUES ('"+ cf.encode(InspectorId)+"','"
										+ cf.encode(HomeId)+"','"
										+ cf.encode(PH_Fname)+"','"
										+ cf.encode(PH_Lname)+"','"
										+ cf.encode(PH_Address1)+"','"
										+ cf.encode(PH_Address2)+"','"
										+ cf.encode(PH_City)+"','"
										+ cf.encode(PH_Zip)+"','"
										+ cf.encode(PH_State)+"','"
										+ cf.encode(PH_County)+"','"
										+ cf.encode(PH_Policyno)+"','"
										+ cf.encode(PH_Inspectionfees)+"','"
										
										+ cf.encode(PH_InsuranceCompany)+"','"
										+ cf.encode(PH_HPhone)+"','"
										+ cf.encode(PH_WPhone)+"','"
										+ cf.encode(PH_CPhone)+"','"
										+ cf.encode(PH_Email)+"','"
										+ cf.encode(PH_EmailChk)+"','"
										+ cf.encode(PH_MainInspectionTypeId)+"','"
										+ cf.encode(PH_IsInspected)+"','"
										+ cf.encode(PH_IsUploaded)+"','"
										+ cf.encode(PH_Status)+"','"
										+ cf.encode(PH_SubStatus)+"','"
										+ cf.encode(ScheduledDate)+"','"+cf.encode(InspectionStartTime)+"','"+cf.encode(InspectionEndTime)+"','"
										+ cf.encode(Schedulecomments)+"','"+cf.encode(ScheduledCreatedDate)+"','"+cf.encode(AssignedDate)+"','0','"+YearBuilt+"','"+cf.encode(PH_CONTPERSON)+"','"+cf.encode(BuildingSize)+"','"+cf.encode(PH_Noofstories)+"','','"+latitude+"','"+longtitude+"','','','','','"+BuildingNo+"','"+cf.encode(PH_InspectionTypeId)+"','"+CompID+"','"+CommercialFlag+"','"+isCustomInspection+"')");
						 }
					 System.out.println("SPHNOOT="+PH_Noofstories+"CommercialFlag"+CommercialFlag);
						 if(PH_Noofstories.equals("1") || PH_Noofstories.equals("2")  || PH_Noofstories.equals("3"))
						 {
							 typeid=32;
						 }
						 else if(PH_Noofstories.equals("4") || PH_Noofstories.equals("5")  || PH_Noofstories.equals("6"))
						 {
							 typeid=33;	
						 }
						 else if(Integer.parseInt(PH_Noofstories)>6)
						 {
							 typeid=34;
						}
					 cf.CreateARRTable(70);
					 try
						{
						 System.out.println("came heeree before");
							Cursor cbui = cf.SelectTablefunction(cf.Comm_Building, " where Commercial_Flag='"+CommercialFlag+"' and fld_srid='"+HomeId+"'");
							System.out.println(" where Commercial_Flag='"+CommercialFlag+"' and fld_srid='"+HomeId+"'"+cbui.getCount());
							if(cbui.getCount()==0)
							{
								
								if(!CommercialFlag.equals("0"))
								{
									System.out.println("INSERT INTO " +cf.Comm_Building + " (fld_srid,insp_typeid,BUILDING_NO,Commercial_Flag)"+ "VALUES ('"+HomeId+"','"+typeid+"','"+BuildingNumberText+"','"+CommercialFlag+"')");
									cf.arr_db.execSQL("INSERT INTO " +cf.Comm_Building + " (fld_srid,insp_typeid,BUILDING_NO,Commercial_Flag)"+ "VALUES ('"+HomeId+"','"+typeid+"','"+BuildingNumberText+"','"+CommercialFlag+"')");
									
								}
							}
							else
							{	
								if(!CommercialFlag.equals("0"))
								{
									System.out.println("UPDATE "+ cf.Comm_Building+ " SET BUILDING_NO='"+BuildingNumberText+"' WHERE fld_srid ='"+ HomeId + "'");
									cf.arr_db.execSQL("UPDATE "+ cf.Comm_Building+ " SET BUILDING_NO='"+BuildingNumberText+"' WHERE fld_srid ='"+ HomeId + "'");
								}
							}
						}
						
						catch(Exception e){System.out.println("dsdsfdsdsddfds="+e.getMessage());}
					 
					 
					cf.CreateARRTable(4);
						
					if(cf.encode(PH_Address1.trim()).equals(cf.encode(MailingAddress.trim())) && cf.encode(PH_Address2.trim()).equals(cf.encode(MailingAddress2.trim())) &&
							cf.encode(PH_City.trim()).equals(cf.encode(Mailingcity.trim())) && cf.encode(PH_State.trim()).equals(cf.encode(MailingState.trim())) &&
							cf.encode(PH_County.trim()).equals(cf.encode(MailingCounty.trim())) && PH_Zip.trim().equals(Mailingzip.trim()))
					{
						adrschk = "1";
					}
					else
					{
						adrschk = "0";
					}
					        Cursor c21 = cf.SelectTablefunction(cf.MailingPolicyHolder," where ARR_ML_PH_InspectorId='" + cf.encode(cf.Insp_id) + "' and ARR_ML_PH_SRID='"+cf.encode(HomeId)+"'");
							int rws = c21.getCount();
							if(rws >= 1)
							{
								cf.arr_db.execSQL("UPDATE " + cf.MailingPolicyHolder
										+ " SET ARR_ML_PH_Address1='" + cf.encode(MailingAddress)
										+ "',ARR_ML_PH_Address2='"
										+ cf.encode(MailingAddress2)
										+ "',ARR_ML='"+ adrschk+"',ARR_ML_PH_City='"
										+ cf.encode(Mailingcity)
										+ "',ARR_ML_PH_Zip='"
										+ cf.encode(Mailingzip) + "',ARR_ML_PH_State='"
										+ cf.encode(MailingState) + "',ARR_ML_PH_County='"
										+ cf.encode(MailingCounty) + "' WHERE ARR_ML_PH_SRID ='" + cf.encode(HomeId)
										+ "'");
							}
							else
							{
								cf.arr_db.execSQL("INSERT INTO "
										+ cf.MailingPolicyHolder
										+ " (ARR_ML_PH_SRID,ARR_ML_PH_InspectorId,ARR_ML,ARR_ML_PH_Address1,ARR_ML_PH_Address2,ARR_ML_PH_City,ARR_ML_PH_Zip,ARR_ML_PH_State,ARR_ML_PH_County)"
										+ "VALUES ('"+cf.encode(HomeId)+"','"+cf.encode(InspectorId)+"','0','"+cf.encode(MailingAddress)+"','"
									    + cf.encode(MailingAddress2)+"','"+cf.encode(Mailingcity)+"','"
									    + cf.encode(Mailingzip)+"','"+cf.encode(MailingState)+"','"
									    + cf.encode(MailingCounty)+"')");
							}
					 cf.CreateARRTable(2);
					 		String insp_id="";
					 		
					 	
						/*	if(PH_InspectionTypeId.matches("13")){
								PH_InspectionTypeId = PH_InspectionTypeId.replace("13","1");}
							if(PH_InspectionTypeId.matches("750")){
								PH_InspectionTypeId = PH_InspectionTypeId.replace("750","2");}
							if(PH_InspectionTypeId.matches("28")){
								PH_InspectionTypeId = PH_InspectionTypeId.replace("28","3");}
							if(PH_InspectionTypeId.matches("9"))
							{
								PH_InspectionTypeId = PH_InspectionTypeId.replace("9","4");	
								if(PH_Noofstories.equals("1") || PH_Noofstories.equals("2")  || PH_Noofstories.equals("3"))
								{
									PH_InspectionTypeId = PH_InspectionTypeId.replace("9","4");	
								}
								else if(PH_Noofstories.equals("4") || PH_Noofstories.equals("5")  || PH_Noofstories.equals("6"))
								{
									PH_InspectionTypeId = PH_InspectionTypeId.replace("9","5");	
								}
								else 
								{
									PH_InspectionTypeId = PH_InspectionTypeId.replace("9","6");	
								}
							}
							if(PH_InspectionTypeId.matches("62")){
								PH_InspectionTypeId = PH_InspectionTypeId.replace("62","5");}
							if(PH_InspectionTypeId.matches("90")){
								PH_InspectionTypeId = PH_InspectionTypeId.replace("90","6");}
							if(PH_InspectionTypeId.matches("11")){
								PH_InspectionTypeId = PH_InspectionTypeId.replace("11","7");}
							if(PH_InspectionTypeId.matches("18")){
								PH_InspectionTypeId = PH_InspectionTypeId.replace("18","8");}
							if(PH_InspectionTypeId.matches("751")){
								PH_InspectionTypeId = PH_InspectionTypeId.replace("751","9");}*/
							
							
						 Cursor c2 = cf.SelectTablefunction(cf.Select_insp, " where SI_InspectorId='" + cf.encode(cf.Insp_id) + "' and SI_srid='"+HomeId+"'");
						 if(c2.getCount()==0)
						 {
								cf.arr_db.execSQL(" INSERT INTO "+cf.Select_insp +" (SI_InspectorId,SI_srid,SI_InspectionNames) VALUES('"+cf.Insp_id+"','"+HomeId+"','"+cf.encode(PH_InspectionTypeId)+"')");
						 }
						 else
						 {	 	
							 cf.arr_db.execSQL(" UPDATE  "+cf.Select_insp+" SET SI_InspectionNames='"+cf.encode(PH_InspectionTypeId)+"' WHERE SI_srid='"+HomeId+"' and SI_InspectorId='" + cf.encode(cf.Insp_id) + "'");
						 } 

					 
					 if (total <= 31) 
					 {
						 total = temptotal + (int) (Totaltmp);
  					 } 
					 else 
					 {
						total = 30;
					 }
					Totaltmp += temptot;
				}
				catch(Exception e)
				{
					System.out.println("Exception"+e.getMessage());
				}
			}
			}
		 /**For pagination **/
			try
			{
				
				 SoapObject onlresult=cf.Calling_WS1(cf.Insp_id, "UPDATEMOBILEDBCOUNT");
				// System.out.println("onlresult"+cf.onlresult);
				 retrieveonlinedata(onlresult);
			}
			catch (Exception e) {
				// TODO: handle exception
			}
			
	 }
	 private void retrieveonlinedata(SoapObject result) {
			
			int Cnt = result.getPropertyCount();
			int onpre=0,onuts=0,oncan=0,onrr=0,ontyppre=0,ontyputs=0,ontypcan=0,ontyprr=0;
			if (Cnt >= 1) {
				SoapObject obj = (SoapObject) result;

				for (int i = 0; i < obj.getPropertyCount(); i++) {
					SoapObject obj1 = (SoapObject) obj.getProperty(i);
					if (!obj1.getProperty("i_maininspectiontype").toString()
							.equals("")) {

					
							ontyppre += Integer.parseInt(obj1.getProperty(
									"Comp_Ins_in_Online").toString());
							ontyputs += Integer.parseInt(obj1.getProperty("UTS")
									.toString());
							ontypcan += Integer.parseInt(obj1.getProperty("Can")
									.toString());
							ontyprr +=  Integer.parseInt(obj1.getProperty("Completed")
															.toString());
							
						
					}
				}
				try
				{
					//insp_id varchar(50),C_28_pre varchar(5) Default('0'),C_28_uts varchar(5) Default('0'),C_28_can varchar(5) Default('0'),C_28_RR varchar(5) Default('0')
					Cursor c=cf.SelectTablefunction(cf.count_tbl, " WHERE insp_id='"+cf.Insp_id+"'");
					if(c.getCount()>0)
					{
						cf.arr_db.execSQL(" UPDATE "+cf.count_tbl+" SET C_28_pre='"+ontyppre+"',C_28_uts='"+ontyputs+"',C_28_can='"+ontypcan+"',C_28_RR='"+ontyprr+"',C_29_pre='"+onpre+"',C_29_uts='"+onuts+"',C_29_can='"+oncan+"',C_29_RR='"+onrr+"' Where  insp_id='"+cf.Insp_id+"'");
					}
					else
					{
						cf.arr_db.execSQL(" INSERT INTO  "+cf.count_tbl+" (insp_id,C_28_pre,C_28_uts,C_28_can,C_28_RR,C_29_pre,C_29_uts,C_29_can,C_29_RR) VALUES " +
								"('"+cf.Insp_id+"','"+ontyppre+"',"+ontyputs+","+ontypcan+","+ontyprr+","+onpre+","+onuts+","+oncan+","+onrr+")");
					}
					c.close();
				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}
	}
	/**For pagination **/
	 private int getcurrentversioncode() {
			// TODO Auto-generated method stub

			try {
				vcode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;

			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return vcode;
		}

	 public boolean getversioncodefromweb() throws NetworkErrorException,SocketTimeoutException, XmlPullParserException, IOException 
		 {
			 SoapObject request = new SoapObject(cf.NAMESPACE, "GETVERSIONINFORMATION");
			 SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			 envelope.dotNet = true;
			 envelope.setOutputSoapObject(request);
			 HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL_ARR);
	
			 androidHttpTransport.call(cf.NAMESPACE+"GETVERSIONINFORMATION", envelope);
			 SoapObject result = (SoapObject) envelope.getResponse();
			 SoapObject obj = (SoapObject) result.getProperty(0);
			 newcode = String.valueOf(obj.getProperty("VersionCode"));
			 newversion = String.valueOf(obj.getProperty("VersionName"));System.out.println("request="+newcode+"new="+newversion);
			 
			 if (!"null".equals(newcode) && null != newcode && !newcode.equals(""))
			 {
				 
				
				 if (Integer.toString(vcode).equals(newcode)) 
				 {
					
					// return true;
				 } 
				 else 
				 {
					 
						 try 
						 {
							 cf.CreateARRTable(20);
							 Cursor c12 = cf.SelectTablefunction(cf.ARRVersion," ");
							 if (c12.getCount() < 1) 
							 {
								 try 
								 {
									 cf.arr_db.execSQL("INSERT INTO "+ cf.ARRVersion + "(arrVId,ARR_VersionCode,ARR_VersionName,ModifiedDate)"
										+ " VALUES ('1','"+newcode+"','" + newversion+ "',date('now'))");
								 } 
								 catch (Exception e) 
								 {
									
								 }
							 } 
							 else 
							 {
								 try 
								 {
									 cf.arr_db.execSQL("UPDATE " + cf.ARRVersion + " set ARR_VersionCode='"+newcode+"',ARR_VersionName='" + newversion + "',ModifiedDate =date('now')");
								 }
								 catch (Exception e) 
								 {
									
								 }
							 }
							 
						 }
						 catch (Exception e) 
						 {
							//return true;
						 }
				 }
			 }
			 	SoapObject request1 = new SoapObject(cf.NAMESPACE, "GetVersionInformation_IDMS");
				SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(
						SoapEnvelope.VER11);
				envelope1.dotNet = true;
				envelope1.setOutputSoapObject(request1);
				HttpTransportSE androidHttpTransport1 = new HttpTransportSE(cf.URL_IDMA);
		    	androidHttpTransport1.call(cf.NAMESPACE+"GetVersionInformation_IDMS", envelope1);
		    	
		    	SoapObject result1 = (SoapObject) envelope1.getResponse();
				
				SoapObject obj1 = (SoapObject) result1.getProperty(0);
				String newcode = String.valueOf(obj1.getProperty("VersionCode"));
				String newversion = String.valueOf(obj1.getProperty("VersionName"));
				
				if (!"null".equals(newcode) && null != newcode && !newcode.equals("")) {
						try {     
							cf.CreateARRTable(20);
							Cursor c12 = cf.SelectTablefunction(cf.IDMAVersion," Where ID_VersionType='IDMA_New' ");

							if (c12.getCount() < 1) {
								
									cf.arr_db.execSQL("INSERT INTO "
											+ cf.IDMAVersion
											+ "(VId,ID_VersionCode,ID_VersionName,ID_VersionType)"
											+ " VALUES ('2','"+newcode+"','" + newversion
											+ "','IDMA_New')");

								

							} else {
									cf.arr_db.execSQL("UPDATE " + cf.IDMAVersion
											+ " set ID_VersionCode='"+newcode+"',ID_VersionName='"
											+ newversion
											+ "' Where  ID_VersionType='IDMA_New'");
								
							}
					
						} catch (Exception e) {
					
							System.out.println("new data  error  "+e.getMessage());
						}

					}  

			 
			 return true;
		 }
	 final Handler handler = new Handler() {

		public void handleMessage(Message msg) {
			 if(usercheck==1)
			 {
				 usercheck = 0;
				 cf.ShowToast("Internet connection not available.",0);
				 startActivity(new Intent(HomeScreen.this, HomeScreen.class));
			 }
			 else if(usercheck==2)
			 {
				 usercheck = 0;
				 cf.ShowToast("There is a problem on your network, so please try again later with better network.",0);
				 startActivity(new Intent(HomeScreen.this, HomeScreen.class));
			 }
			 else if(usercheck==3)
			 {
				 usercheck = 0;
				 cf.ShowToast("There is a problem on your application. Please contact Paperless administrator.",0);
				 startActivity(new Intent(HomeScreen.this, HomeScreen.class));
			 }
			 else if(usercheck==4)
			 {
				 usercheck=0;
				 progDialog.dismiss();
				 cf.alerttitle="Import";
				 cf.alertcontent="Import successfully completed";
				 cf.showalert(cf.alerttitle,cf.alertcontent);
				// startActivity(new Intent(HomeScreen.this, HomeScreen.class));
			 }
			
		}

	};
	 @Override
	 protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		try
		{
			System.out.println("values"+requestCode+" result"+resultCode);
			if(requestCode==cf.loadcomment_code)
			{
				if(resultCode==RESULT_OK)
				{
					cf.ShowToast("You have selected the comments \n "+data.getExtras().getString("Comments"), 0);
					
				}
				else if(resultCode==RESULT_CANCELED)
				{
					//cf.ShowToast("You have canceled  the comments selction ",0);
				}
				
					
			}
		}
		catch (Exception e) {
			// TODO: handle exception
			System.out.println("the erro was "+e.getMessage());
		}
	}
	 
	 public boolean onKeyDown(int keyCode, KeyEvent event) {
			// replaces the default 'Back' button action
		Intent loginpage;
			if (keyCode == KeyEvent.KEYCODE_BACK) {
				if(cf.application_sta)
				{
					 loginpage = new Intent(Intent.ACTION_MAIN);
					  loginpage.setComponent(new ComponentName("idsoft.inspectiondepot.IDMA","idsoft.inspectiondepot.IDMA.ApplicationMenu"));
				}
				else if ((keyCode == KeyEvent.KEYCODE_HOME)) 
				{
					android.os.Process.killProcess(0);
			        finish();                     
			        return true;
			    }
				else
				{
			
				loginpage = new Intent(HomeScreen.this,AllRiskReportsActivity.class);
				loginpage.putExtra("back", "exit");
			
				
				}
				startActivity(loginpage);
				return true;
			}
	
			return super.onKeyDown(keyCode, event);
			
		}
	 public void getIDMA_application() {
			// TODO Auto-generated method stub
		
				if (cf.isInternetOn()) {
					String source = "<b><font color=#FFFFFF>" + "Checking for new inspections... Please wait."
							+ "</font></b>";
					progressDialog = ProgressDialog.show(this, "",Html.fromHtml(source),true);

					
					new Thread() {
						private int usercheck = 0;

						public void run() {
							// Looper.prepare();
						
							try {
								if (getIDMAversioncodefromweb() == true) {
									if (k != 5 && k != 8) {
										usercheck = 1;
									}
									handler.sendEmptyMessage(0);
									progressDialog.dismiss();
								} else {
									
									progressDialog.dismiss();  
									
									k=20;
									handler.sendEmptyMessage(0);

								}

							} catch (SocketTimeoutException s) {
								k = 5;
								handler.sendEmptyMessage(0);
							} catch (NetworkErrorException n) {
								k = 5;
								handler.sendEmptyMessage(0);
							} catch (IOException io) {
								k = 5;
								handler.sendEmptyMessage(0);
							} catch (XmlPullParserException x) {
								k = 5;
								handler.sendEmptyMessage(0);
							} catch (Exception e) {
								progressDialog.dismiss();
								k = 8;
								handler.sendEmptyMessage(0);
							}
							
						};

						private boolean getIDMAversioncodefromweb() throws IOException,NetworkErrorException,SocketTimeoutException, XmlPullParserException, Exception {
							// TODO Auto-generated method stub
							
							 SoapObject request1 = new SoapObject(cf.NAMESPACE, "GetVersionInformation_IDMS");
								SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(
										SoapEnvelope.VER11);
								envelope1.dotNet = true;
								envelope1.setOutputSoapObject(request1);
								HttpTransportSE androidHttpTransport1 = new HttpTransportSE(cf.URL_IDMA);
						    	androidHttpTransport1.call(cf.NAMESPACE+"GetVersionInformation_IDMS", envelope1);
						    	
						    	SoapObject result1 = (SoapObject) envelope1.getResponse();System.out.println("sres"+result1);
								
								SoapObject obj1 = (SoapObject) result1.getProperty(0);
								newcode = String.valueOf(obj1.getProperty("VersionCode"));
								newversion = String.valueOf(obj1.getProperty("VersionName"));
								System.out.println("newcode"+newcode+"newver"+newversion);
								int newvcode=0;
								try
								{
								newvcode=Integer.parseInt(newcode);System.out.println("newco"+newcode);
								}catch (Exception e) {
									// TODO: handle exception
								}
								
								if (!"null".equals(newcode) && null != newcode && !newcode.equals("")) {
									
									Cursor c12 = cf.SelectTablefunction(cf.IDMAVersion," Where ID_VersionType='IDMA_Old' ");
									if(c12.getCount()>=1)
									{
										c12.moveToFirst();
										int V_code=c12.getInt(c12.getColumnIndex("ID_VersionCode"));
										
										if(V_code<newvcode)
										{
											k=20;
											return true;
											
										}
										else
										{
											usercheck=1;
											k=0;
											return true;
										}
										
									}
									else
									{
										k=5;
										return false;
									}
									
								}else
								{
									k=5;
									return false;
									
								}
								
						}

						private Handler handler = new Handler() {
						public int m=0;
						private AlertDialog alertDialog;

							@Override
							public void handleMessage(Message msg) {
								System.out.println("comes in the handler k"+k);
								if (k == 5 || k == 8) {
									cf.ShowToast("There is a problem on your Network. Please try again later with better Network.",0);
									progressDialog.dismiss();
								}else if(k==20)
								{
								
									alertDialog = new AlertDialog.Builder(HomeScreen.this).create();
									
									alertDialog.setMessage("New IDMA Version of "
											+ newversion
											+ " for IDMA is available,The Previous data will not be affected, Would you like to Upgrade.");
							alertDialog.setButton("Yes",
									new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,
												int which) {
											String source = "<b><font color=#FFFFFF>" +" Please wait... Upgrading Version  "
													+ newversion
													+ " will take few minutes ...</font></b>";
											progressDialog1 = ProgressDialog.show(HomeScreen.this, "",Html.fromHtml(source),true);

									/*progressDialog1 = ProgressDialog
											.show(HomeScreen.this,
													"",
													"<font color=#FFFFFF>Please wait... Upgrading Version  "
															+ newversion
															+ " will take few minutes ...</font>");*/
									new Thread() {
										public void run() {
											// Looper.prepare();
											
									try
									{
										
										upgrdingIDMS();
									} catch (SocketTimeoutException s) {
										m=1;
										//cf.ShowToast("You have problem in your server connection please conntact paperless inspection admin.",1);
									
									} catch (NetworkErrorException n) {
										m=2;
										//cf.ShowToast("You have internet problem please try again later.",1);
									
									} catch (IOException io) {
										m=1;
									//	cf.ShowToast("You have problem in your server connection please contact papperless inspection admin.",1);
									
									} catch (XmlPullParserException x) {
										m=1;
										//cf.ShowToast("You have problem in your server connection please conntact paperless inspection admin.",1);
									} catch (Exception e) {
										m=8;
										//cf.ShowToast("You have problem in your upgrading .",1);
									}
										};

										

										private Handler handler = new Handler() {
											public void handleMessage(
													Message msg) {
												progressDialog1.dismiss();

												if (m == 2) {
													cf.ShowToast("Internet connection not available.",0);

												} else if (m == 8) {cf.ShowToast("Error in login. Please try again later.",0);

												} else if (m == 5) {
													cf.ShowToast("There is a problem on your Network. Please try again later with better Network.",0);

												}
												else if(m==1)
												{
													cf.ShowToast("You have problem in your server connection. Please contact paperless administrator.",0);
												}

											}
										};
									}.start();
										}
							});
							alertDialog.show();
									
								}
								else if (usercheck == 1 && k!=20 ) {
								
								
									usercheck = 0;
									cf.ShowToast("You don�t have any New Inspection.",0);
								}
								}

							};
						
					}.start();
				} else {
					cf.ShowToast("Internet connection not available.",0);
				}
				
		}
	 public void upgrdingIDMS() throws IOException,NetworkErrorException,SocketTimeoutException, XmlPullParserException, Exception
			{
				SoapObject webresult;
				SoapObject request = new SoapObject(cf.NAMESPACE, "GeAPKFile_IDMS");
				
				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
						SoapEnvelope.VER11);
				envelope.dotNet = true;
				envelope.setOutputSoapObject(request);

				HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL_IDMA);

				androidHttpTransport.call(cf.NAMESPACE+"GeAPKFile_IDMS", envelope);

				Object response = envelope.getResponse();

				byte[] b;
				if (cf.checkresponce(response)) // check the response is valid or
												// not
				{
					b = Base64.decode(response.toString());

					try {
						String PATH = Environment.getExternalStorageDirectory()
								+ "/Download/IDMS";
						File file = new File(PATH);
						file.mkdirs();

						File outputFile = new File(PATH + "/IDMS.apk");
						FileOutputStream fileOuputStream = new FileOutputStream(
								outputFile);
						fileOuputStream.write(b);
						fileOuputStream.close();

						//cf.fn_logout(cf.InspectorId); /* FOR LOGOUT*/
						cf.CreateARRTable(0);
						Cursor c12 = cf.SelectTablefunction(cf.IDMAVersion," ");

						if (c12.getCount() < 1) {
							try {
								cf.arr_db.execSQL("INSERT INTO "
										+ cf.IDMAVersion
										+ "(VId,ID_VersionCode,ID_VersionName,ID_VersionType)"
										+ " VALUES ('2','"+newcode+"','" + newversion
										+ "','IDMA_New')");

							} catch (Exception e) {
								
							}

						} else {
							try {
								cf.arr_db.execSQL("UPDATE " + cf.IDMAVersion
										+ " set ID_VersionCode='"+newcode+"',ID_VersionName='"
										+ newversion
										+ "' Where  ID_VersionType='IDMA_New'");
							} catch (Exception e) {
							
							}
						}
						progressDialog1.dismiss();
						Intent intent = new Intent(Intent.ACTION_VIEW);
						intent.setDataAndType(Uri.fromFile(new File(Environment
								.getExternalStorageDirectory()
								+ "/Download/IDMS/"
								+ "IDMS.apk")),
								"application/vnd.android.package-archive");
						startActivity(intent);
					} catch (IOException e) {
						cf.ShowToast("Error occured while updating the new version. Please contact paperless administrator!",0);
					}
				} else {
					progressDialog1.dismiss();
					cf.ShowToast("There is a problem on your Network. Please try again later with better Network.",0);
				}	
			}
	 @Override
		protected Dialog onCreateDialog(int id) {
			switch (id) {
			case 1:
				progDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
				progDialog.setMax(maxBarValue);	
				String source = "<font color=#FFFFFF>Importing please wait..."	+ "</font>";
		        progDialog.setMessage(Html.fromHtml(source));	
				progDialog.setCancelable(false);	
				progThread = new ProgressThread(handler1);	
				progThread.start();	
				return progDialog;
			default:
				return null;
			}
		}

	 	final Handler handler1 = new Handler() {
			public void handleMessage(Message msg) {
				// Get the current value of the variable total from the message data
				// and update the progress bar.
				int total = msg.getData().getInt("total");
				progDialog.setProgress(total);
				//progDialog.setProgressStyle(R.style.progtext);
				
				
				if (total == 100) {
					progDialog.setCancelable(true);
					removeDialog(typeBar);
					handler.sendEmptyMessage(0);
					progThread.setState(ProgressThread.DONE);
			 	}

			}
		};
		private class ProgressThread extends Thread {

			// Class constants defining state of the thread
			final static int DONE = 0;

			Handler mHandler;
			ProgressThread(Handler h) {
				mHandler = h;
			}

			@Override
			public void run() {
				mState = RUNNING;
				while (mState == RUNNING) {
					try {
						Thread.sleep(delay);
					} catch (InterruptedException e) {
						Log.e("ERROR", "Thread was Interrupted");
					}

					Message msg = mHandler.obtainMessage();
					Bundle b = new Bundle();
					b.putInt("total", total);
					msg.setData(b);
					mHandler.sendMessage(msg);

				}
			}
			public void setState(int state) {
				mState = state;
			}

		}
public void onBackPressed()
{
	finish();
	}
}