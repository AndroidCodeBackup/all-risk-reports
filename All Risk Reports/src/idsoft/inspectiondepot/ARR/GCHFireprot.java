/*
   ************************************************************
   * Project: ALL RISK REPORT
   * Module : GCHElectric.java
   * Creation History:
      Created By : Gowri on 
   * Modification History:
      Last Modified By : Gowri on 04/04/2012
   ************************************************************  
*/
package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.WindMitCommRestsupp.checklistenetr;
import idsoft.inspectiondepot.ARR.WindMitCommRestsupp.textwatcher;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

public class GCHFireprot extends Activity {
	CommonFunctions cf;
	Cursor c1;
	boolean f[]=new boolean[7];
	protected static final int DATE_DIALOG_ID = 0;
	boolean fire1chk=false,fire2chk=false,fire3chk=false,fire4chk=false,fire5chk=false;
	ArrayAdapter howoftenadap;
	int fireprot_id1,fireprot_id2,fireprot_id3,fireprot_id4,k=0,fireprotectionchkval=0,chk1=0,chk2=0,chk3=0,chk4=0;
	String nameandmodel="",datelastservice="",howoftenserviced="",servicecomp="",fireprotnotappl="0",fireprot_na="",retieve_fire_prot_val="",fireprotval1,fireprotval2,fireprotval3,fireprotval4,automatedtxt="",fireprotval="",fireprottxt1="",fireprottxt2="",fireprottxt3="",fireprottxt4="",filterstxt="",fireprottxt5="";
	RadioGroup fireprotrdgp1,fireprotrdgp2,fireprotrdgp3,fireprotrdgp4,fireprotrdgp5;
	@Override
	   public void onCreate(Bundle savedInstanceState) {

	        super.onCreate(savedInstanceState);
	        cf=new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);				
		 	}
	        setContentView(R.layout.gchfireprot);
	        cf.getDeviceDimensions();
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
			hdr_layout.addView(new HdrOnclickListener(this,1,"General Conditions & Hazards","Fire Protection",4,0,cf));
	        LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
	        mainmenu_layout.setMinimumWidth(cf.wd);
	        mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 4, 0,0,cf));
	        LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 417, 1,0,cf));
			TableRow tblrw = (TableRow)findViewById(R.id.row2);
			tblrw.setMinimumHeight(cf.ht);
			cf.CreateARRTable(417);
			declarations();
			Fireprot_SetValue();
	}
	private void declarations() 
	{
		cf.setupUI((ScrollView) findViewById(R.id.scr));
		   fireprotrdgp1 = (RadioGroup)findViewById(R.id.automated_rdg);fireprotrdgp1.setOnCheckedChangeListener(new checklistenetr(1));
		    fireprotrdgp2 = (RadioGroup)findViewById(R.id.usercont_rdg);fireprotrdgp2.setOnCheckedChangeListener(new checklistenetr(2));
		    fireprotrdgp3 = (RadioGroup)findViewById(R.id.manualshut_rdg);fireprotrdgp3.setOnCheckedChangeListener(new checklistenetr(3));   
		    fireprotrdgp4 = (RadioGroup)findViewById(R.id.portablefree_rdg);fireprotrdgp4.setOnCheckedChangeListener(new checklistenetr(4));
		    fireprotrdgp5 = (RadioGroup)findViewById(R.id.automated2_rdg);fireprotrdgp5.setOnCheckedChangeListener(new checklistenetr(5));
		    
	 ((EditText)findViewById(R.id.edundercontract_othr)).addTextChangedListener(new CustomTextWatcherSpace(((EditText)findViewById(R.id.edundercontract_othr))));
	 ((EditText)findViewById(R.id.edmanualshut_othr)).addTextChangedListener(new CustomTextWatcherSpace(((EditText)findViewById(R.id.edmanualshut_othr))));
	 ((EditText)findViewById(R.id.edtportablefireext_othr)).addTextChangedListener(new CustomTextWatcherSpace(((EditText)findViewById(R.id.edtportablefireext_othr))));
	}	
	
	class checklistenetr implements OnCheckedChangeListener
	{
		int i;
		public checklistenetr(int i) {
			// TODO Auto-generated constructor stub
		   this.i=i;
		}
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			 RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
		        boolean isChecked = checkedRadioButton.isChecked();
		        if (isChecked)
		        {
		          switch (i) 
		          {
		          case 1:
						fireprottxt1= checkedRadioButton.getText().toString().trim();
						//chkvalues();
						if(!retieve_fire_prot_val.equals(""))
						{
							
						}
						else
						{
							if(fireprottxt1.equals("No"))
							{
								 ((EditText)findViewById(R.id.edfirenameandmodel)).setText("Not Applicable");
								 ((EditText)findViewById(R.id.eddatelastserviced)).setText("Not Determined");
								 ((EditText)findViewById(R.id.edhowoftenserviced)).setText("Not Applicable");
								 ((EditText)findViewById(R.id.edserviceompany)).setText("Not Applicable");
								 ((Button)findViewById(R.id.datelastserviced)).setEnabled(false);
								 ((EditText)findViewById(R.id.edfirenameandmodel)).setEnabled(false);
								 ((EditText)findViewById(R.id.eddatelastserviced)).setEnabled(false);
								 ((EditText)findViewById(R.id.edhowoftenserviced)).setEnabled(false);
								 ((EditText)findViewById(R.id.edserviceompany)).setEnabled(false);
								 
								((CheckBox)findViewById(R.id.namenotapppl)).setChecked(true);
								((CheckBox)findViewById(R.id.datelastservrdio)).setChecked(true);
								((CheckBox)findViewById(R.id.howoftennotapppl)).setChecked(true);
								((CheckBox)findViewById(R.id.servcompnotapppl)).setChecked(true);
								((RadioButton) fireprotrdgp2.findViewWithTag("Not Determined")).setChecked(true);
								((RadioButton) fireprotrdgp3.findViewWithTag("Not Applicable")).setChecked(true);
								((RadioButton) fireprotrdgp4.findViewWithTag("Not Applicable")).setChecked(true);
							}
							else
							{
								((EditText)findViewById(R.id.edfirenameandmodel)).setEnabled(true);
								((EditText)findViewById(R.id.eddatelastserviced)).setEnabled(true);
								((EditText)findViewById(R.id.edhowoftenserviced)).setEnabled(true);
								((EditText)findViewById(R.id.edserviceompany)).setEnabled(true);
								((Button)findViewById(R.id.datelastserviced)).setEnabled(true);
								UncheckFireProtection(2);
							}
						}
						break;
					case 2:
						fireprottxt2= checkedRadioButton.getText().toString().trim();			
						if(fireprottxt2.equals("Other"))
						{
							((EditText)findViewById(R.id.edundercontract_othr)).setVisibility(cf.v1.VISIBLE);
							cf.setFocus(((EditText)findViewById(R.id.edundercontract_othr)));
						}
						else
						{((EditText)findViewById(R.id.edundercontract_othr)).setVisibility(cf.v1.GONE);
						((EditText)findViewById(R.id.edundercontract_othr)).setText("");}						
						break;
					case 3:
						fireprottxt3= checkedRadioButton.getText().toString().trim();	
						if(fireprottxt3.equals("Other"))
						{
							((EditText)findViewById(R.id.edmanualshut_othr)).setVisibility(cf.v1.VISIBLE);
							cf.setFocus(((EditText)findViewById(R.id.edmanualshut_othr)));
						}
						else
						{((EditText)findViewById(R.id.edmanualshut_othr)).setVisibility(cf.v1.GONE);
						((EditText)findViewById(R.id.edmanualshut_othr)).setText("");}
						
						break;
					case 4:
						fireprottxt4= checkedRadioButton.getText().toString().trim();	
						if(fireprottxt4.equals("Other"))
						{
							((EditText)findViewById(R.id.edtportablefireext_othr)).setVisibility(cf.v1.VISIBLE);
							cf.setFocus(((EditText)findViewById(R.id.edtportablefireext_othr)));
						}
						else
						{((EditText)findViewById(R.id.edtportablefireext_othr)).setVisibility(cf.v1.GONE);
						((EditText)findViewById(R.id.edtportablefireext_othr)).setText("");}
						
						break;
					case 5:
						fireprottxt5= checkedRadioButton.getText().toString().trim();							
						break;
				
		          }
		        }
		}
		
	}
	private void Fireprot_SetValue()	
	{
		chkvalues();
		if(c1.getCount()==0)
		{
			((CheckBox)findViewById(R.id.fireprot_chk)).setChecked(true);
			((LinearLayout)findViewById(R.id.fireprot_table)).setVisibility(cf.v1.GONE);
		}
		else
		{	
				if(fireprot_na.equals("0") || fireprot_na.equals(""))
				{
					((LinearLayout)findViewById(R.id.fireprot_table)).setVisibility(cf.v1.VISIBLE);
					if(!retieve_fire_prot_val.equals(""))
				    {
						String Fireprot_Split[]= retieve_fire_prot_val.split("&#94;"); 	
						((RadioButton) fireprotrdgp1.findViewWithTag(Fireprot_Split[0])).setChecked(true);	
						//if(Fireprot_Split[0].equals("No"))
						//{
								if(Fireprot_Split[1].equals("Not Applicable"))
								{
									((CheckBox)findViewById(R.id.namenotapppl)).setChecked(true);	
									((EditText)findViewById(R.id.edfirenameandmodel)).setEnabled(false);
									((EditText)findViewById(R.id.edfirenameandmodel)).setText("Not Applicable");
								}
								else
								{
									((EditText)findViewById(R.id.edfirenameandmodel)).setText(Fireprot_Split[1]);	
								}
								if(Fireprot_Split[2].equals("Not Determined"))
								{
									k=1;
									((EditText)findViewById(R.id.eddatelastserviced)).setText("Not Determined");
									((EditText)findViewById(R.id.eddatelastserviced)).setEnabled(false);
									((CheckBox)findViewById(R.id.datelastservrdio)).setChecked(true);
								}
								else
								{
									k=0;
									((EditText)findViewById(R.id.eddatelastserviced)).setText(Fireprot_Split[2]);	
								}
								String FIRE_PROT_OPT1[] = Fireprot_Split[3].split("&#39;");
								
								((RadioButton) fireprotrdgp2.findViewWithTag(FIRE_PROT_OPT1[0])).setChecked(true);
								if(FIRE_PROT_OPT1[0].equals("Other"))
								{
									((EditText)findViewById(R.id.edundercontract_othr)).setVisibility(cf.v1.VISIBLE);
									((EditText)findViewById(R.id.edundercontract_othr)).setText(FIRE_PROT_OPT1[1]);
								}
								else
								{
									((EditText)findViewById(R.id.edundercontract_othr)).setVisibility(cf.v1.GONE);
									((EditText)findViewById(R.id.edundercontract_othr)).setText("");
								}			
								
								if(Fireprot_Split[4].equals("Not Applicable"))
								{
									((EditText)findViewById(R.id.edhowoftenserviced)).setText("Not Applicable");
									((CheckBox)findViewById(R.id.howoftennotapppl)).setChecked(true);
									((EditText)findViewById(R.id.edhowoftenserviced)).setEnabled(false);
								}
								else
								{
									((EditText)findViewById(R.id.edhowoftenserviced)).setText(Fireprot_Split[4]);
								}
								
								if(Fireprot_Split[5].equals("Not Applicable"))
								{
									((EditText)findViewById(R.id.edserviceompany)).setText("Not Applicable");
									((CheckBox)findViewById(R.id.servcompnotapppl)).setChecked(true);
									((EditText)findViewById(R.id.edserviceompany)).setEnabled(false);
								}
								else
								{
									((EditText)findViewById(R.id.edserviceompany)).setText(Fireprot_Split[5]);
								}
								
								
								String FIRE_PROT_OPT2[] = Fireprot_Split[6].split("&#39;");			
								((RadioButton) fireprotrdgp3.findViewWithTag(FIRE_PROT_OPT2[0])).setChecked(true);
								if(FIRE_PROT_OPT2[0].equals("Other"))
								{
									((EditText)findViewById(R.id.edmanualshut_othr)).setVisibility(cf.v1.VISIBLE);
									((EditText)findViewById(R.id.edmanualshut_othr)).setText(FIRE_PROT_OPT2[1]);
								}
								else
								{
									((EditText)findViewById(R.id.edmanualshut_othr)).setVisibility(cf.v1.GONE);
									((EditText)findViewById(R.id.edmanualshut_othr)).setText("");
								}
								
								String FIRE_PROT_OPT3[] = Fireprot_Split[7].split("&#39;");			
								((RadioButton) fireprotrdgp4.findViewWithTag(FIRE_PROT_OPT3[0])).setChecked(true);
								if(FIRE_PROT_OPT3[0].equals("Other"))
								{
									((EditText)findViewById(R.id.edtportablefireext_othr)).setVisibility(cf.v1.VISIBLE);
									((EditText)findViewById(R.id.edtportablefireext_othr)).setText(FIRE_PROT_OPT3[1]);
								}
								else
								{
									((EditText)findViewById(R.id.edtportablefireext_othr)).setVisibility(cf.v1.GONE);
									((EditText)findViewById(R.id.edtportablefireext_othr)).setText("");
								}						
						/*}
						else
						{
						}*/
						((RadioButton) fireprotrdgp5.findViewWithTag(Fireprot_Split[8])).setChecked(true);
						((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
				    }
				    else
				    {
				    	((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.GONE);		    	
				    }
				}
				else
				{
					((CheckBox)findViewById(R.id.fireprot_chk)).setChecked(true);
					((LinearLayout)findViewById(R.id.fireprot_table)).setVisibility(cf.v1.GONE);
					((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);	
				}
		}
	}
	
	
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.datelastservrdio:
			if(((CheckBox)findViewById(R.id.datelastservrdio)).isChecked())
			{	
				k=1;
				((EditText)findViewById(R.id.eddatelastserviced)).setText("Not Determined");
				((EditText)findViewById(R.id.eddatelastserviced)).setEnabled(false);
				((Button)findViewById(R.id.datelastserviced)).setEnabled(false);
			}
			else
			{
				k=0;
				((Button)findViewById(R.id.datelastserviced)).setEnabled(true);	
				((EditText)findViewById(R.id.eddatelastserviced)).setText("");
			}			
			break;
		case R.id.datelastserviced:
			if(k==0)
			{
				((Button)findViewById(R.id.datelastserviced)).setEnabled(true);	
				cf.showDialogDate(((EditText)findViewById(R.id.eddatelastserviced)));
			}
			else
			{
				
			}
			break;
		
		case R.id.namenotapppl:
			if(((CheckBox)findViewById(R.id.namenotapppl)).isChecked())
			{
				((EditText)findViewById(R.id.edfirenameandmodel)).setText("Not Applicable");
				((EditText)findViewById(R.id.edfirenameandmodel)).setEnabled(false);
			}
			else
			{
				((EditText)findViewById(R.id.edfirenameandmodel)).setEnabled(true);
				((EditText)findViewById(R.id.edfirenameandmodel)).setText("");
			}
		break;
		
		case R.id.howoftennotapppl:
			if(((CheckBox)findViewById(R.id.howoftennotapppl)).isChecked())
			{
				((EditText)findViewById(R.id.edhowoftenserviced)).setText("Not Applicable");
				((EditText)findViewById(R.id.edhowoftenserviced)).setEnabled(false);
			}
			else
			{
				((EditText)findViewById(R.id.edhowoftenserviced)).setEnabled(true);
				((EditText)findViewById(R.id.edhowoftenserviced)).setText("");
			}
			break;
		case R.id.servcompnotapppl:		
			if(((CheckBox)findViewById(R.id.servcompnotapppl)).isChecked())
			{
				((EditText)findViewById(R.id.edserviceompany)).setText("Not Applicable");
				((EditText)findViewById(R.id.edserviceompany)).setEnabled(false);
			}
			else
			{
				((EditText)findViewById(R.id.edserviceompany)).setEnabled(true);
				((EditText)findViewById(R.id.edserviceompany)).setText("");
			}
			break;
		case R.id.fireprot_chk:
			if(((CheckBox)findViewById(R.id.fireprot_chk)).isChecked())
			{
				
				 if(!retieve_fire_prot_val.equals(""))
				{
					showunchkalert();
				}
				else
				{
					 UncheckFireProtection(1);
					((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.GONE);
					((LinearLayout)findViewById(R.id.fireprot_table)).setVisibility(cf.v1.GONE);
				}				
			}
			else
			{
				 UncheckFireProtection(1);
				 ((LinearLayout)findViewById(R.id.fireprot_table)).setVisibility(cf.v1.VISIBLE);
				((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.GONE);
			}
			break;
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.save:
			if(((CheckBox)findViewById(R.id.fireprot_chk)).isChecked())
			{
				fireprotnotappl="1";
				insertfireprotectionval();
				cf.ShowToast("Fire Protection saved successfully", 0);
				((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
				cf.goclass(418);
			}
			else
			{
				System.out.println("fireprottxt1"+fireprottxt1);
					if(!fireprottxt1.trim().equals(""))
					{
						if(fireprottxt1.equals("Yes"))
						{
							validation1();
						}
						else
						{
							validation1();
						}
					}
					else
					{
						cf.ShowToast("Please select the option for Automatic Fire Suppression System Provided.", 0);
					}
			}
			break;
		}
	}
	
	
	private void validation1() {
		// TODO Auto-generated method stub
		if(!((EditText)findViewById(R.id.edfirenameandmodel)).getText().toString().trim().equals("") || ((CheckBox)findViewById(R.id.namenotapppl)).isChecked()==true)
		 {
			 if(!((EditText)findViewById(R.id.eddatelastserviced)).getText().toString().trim().equals("") || ((CheckBox)findViewById(R.id.datelastservrdio)).isChecked()==true)
			 {
				 	if(k==0)
				 	{
				 		if(!((EditText)findViewById(R.id.eddatelastserviced)).getText().toString().equals(""))
				 		{
				 			if(cf.checkfortodaysdategch(((EditText)findViewById(R.id.eddatelastserviced)).getText().toString())=="false")
			 		       	{
								 cf.ShowToast("Date Last Serviced should not exceed the current year.",0);		 		       		
			 		       	}
							else
							{
								fireprotremvalidation();
							}	
				 		}						
					}
					else
					{
						fireprotremvalidation();
					}								 
			 }
			 else
			 {
				 cf.ShowToast("Please select the option for Date Last Serviced.",0);
			 }
		 }
		 else
		 {
			 cf.ShowToast("Please enter the Name and Model Number of System.", 0);
			 cf.setFocus(((EditText)findViewById(R.id.edfirenameandmodel)));
		 }
	}
	private void fireprotremvalidation() {
		// TODO Auto-generated method stub
		f[0]=((fireprottxt2.equals("Other")) && ((EditText)findViewById(R.id.edundercontract_othr)).getText().toString().trim().equals("")) ? false:true;
		f[1]=((fireprottxt3.equals("Other")) && ((EditText)findViewById(R.id.edmanualshut_othr)).getText().toString().trim().equals("")) ? false:true;
		f[2]=((fireprottxt4.equals("Other")) && ((EditText)findViewById(R.id.edtportablefireext_othr)).getText().toString().trim().equals("")) ? false:true;

		if(!fireprottxt2.trim().equals("") && f[0]==true)
		 {
			 if(!((EditText)findViewById(R.id.edhowoftenserviced)).getText().toString().trim().equals("") || ((CheckBox)findViewById(R.id.howoftennotapppl)).isChecked()==true)
			 {
				 if(!((EditText)findViewById(R.id.edserviceompany)).getText().toString().trim().equals("") || ((CheckBox)findViewById(R.id.servcompnotapppl)).isChecked()==true)
				 {
					 if(!fireprottxt3.trim().equals("") && f[1]==true)
					 {
						 if(!fireprottxt4.trim().equals("") && f[2]==true)
						 {
							 if(!fireprottxt5.equals(""))
							 {
								 insertfireprotectionval();
								 cf.ShowToast("Fire Protection saved successfully", 0);
									((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
									cf.goclass(418);
							 }
							 else
							 {
								 cf.ShowToast("Please select the option for Automated Fuel Shut off Provided to Cooking Equipment as part of Automatic Fire Extinguishing System.",0);											 
							 }
						 }
						 else
						 {
							 if(fireprottxt4.equals("")){cf.ShowToast("Please select the option for Portable Fire Extinguishers.",0);}
								else if(f[2]==false)
								{
									 cf.ShowToast("Please enter the other text for Portable Fire Extinguishers.",0);
									 cf.setFocus(((EditText)findViewById(R.id.edtportablefireext_othr)));									
								}
						 }						 
					 }
					 else
					 {
						 if(fireprottxt3.equals("")){cf.ShowToast("Please select the option for Manual Shut Off also provided.",0);}
							else if(f[1]==false)
							{
								 cf.ShowToast("Please enter the other text for Manual Shut Off also provided.",0);
								 cf.setFocus(((EditText)findViewById(R.id.edmanualshut_othr)));
							}
					 }
				 }
				 else
				 {
					 cf.ShowToast("Please enter the text for Servicing Company.", 0);
				     cf.setFocus(((EditText)findViewById(R.id.edserviceompany)));
				 }
			 }
			 else
			 {
				 cf.ShowToast("Please enter the text for How often Serviced.", 0);
				 cf.setFocus(((EditText)findViewById(R.id.edhowoftenserviced)));
			 }
		 }
		 else
		 {
			 if(fireprottxt2.equals("")){cf.ShowToast("Please select the option for Under Contract.",0);}
		  	 else if(f[0]==false)
			 {
				 cf.ShowToast("Please enter the other text for Under Contract.",0);
				 cf.setFocus(((EditText)findViewById(R.id.edundercontract_othr)));
			 }
		 }
	}
	protected void UncheckFireProtection(int i) {
		// TODO Auto-generated method stub
		fire1chk=true;fire2chk=true;fire3chk=true;fire4chk=true;fire5chk=true;	
		if(i==1){
			try{if(fire1chk){fireprotrdgp1.clearCheck();}}catch(Exception e){}
			try{if(fire5chk){fireprotrdgp5.clearCheck();}}catch(Exception e){}
			fireprottxt1="";fireprottxt5="";retieve_fire_prot_val="";
		}
		((EditText)findViewById(R.id.edfirenameandmodel)).setText("");
		((EditText)findViewById(R.id.eddatelastserviced)).setText("");
		((EditText)findViewById(R.id.edhowoftenserviced)).setText("");
		((EditText)findViewById(R.id.edserviceompany)).setText("");
		((EditText)findViewById(R.id.edundercontract_othr)).setVisibility(cf.v1.GONE);
		((EditText)findViewById(R.id.edundercontract_othr)).setText("");
		((EditText)findViewById(R.id.edtportablefireext_othr)).setVisibility(cf.v1.GONE);
		((EditText)findViewById(R.id.edtportablefireext_othr)).setText("");
		((EditText)findViewById(R.id.edmanualshut_othr)).setVisibility(cf.v1.GONE);
		((EditText)findViewById(R.id.edmanualshut_othr)).setText("");			
		try{if(fire2chk){fireprotrdgp2.clearCheck();}}catch(Exception e){}	
		try{if(fire3chk){fireprotrdgp3.clearCheck();}}catch(Exception e){}	
		try{if(fire4chk){fireprotrdgp4.clearCheck();}}catch(Exception e){}		
		//((RadioButton)findViewById(R.id.datelastservrdio)).setChecked(false);		
		fireprottxt2="";fireprottxt3="";fireprottxt4="";fireprotectionchkval=0;
		((CheckBox)findViewById(R.id.namenotapppl)).setChecked(false);
		((CheckBox)findViewById(R.id.datelastservrdio)).setChecked(false);
		((CheckBox)findViewById(R.id.howoftennotapppl)).setChecked(false);
		((CheckBox)findViewById(R.id.servcompnotapppl)).setChecked(false);
		//((TableLayout)findViewById(R.id.tblfireprotection)).setVisibility(cf.v1.GONE);
	}
	
	private void chkvalues()
	{
		try
		{
			c1 = cf.arr_db.rawQuery("SELECT * FROM "+ cf.GCH_FireProtection + " WHERE fld_srid='"+ cf.selectedhomeid + "'", null);				
			int rws = c1.getCount();
			if(rws>0)
			{
				c1.moveToFirst();
				fireprot_na = c1.getString(c1.getColumnIndex("fld_fireprot_na"));
				retieve_fire_prot_val = cf.decode(c1.getString(c1.getColumnIndex("fld_fireprotection")));				
		    }c1.close();
		}
		catch(Exception e){
			System.out.println("EEe"+e.getMessage());
			
		}
	}
	public void showunchkalert() {
		// TODO Auto-generated method stub
		AlertDialog.Builder bl = new Builder(GCHFireprot.this);
		bl.setTitle("Confirmation");
		bl.setMessage(Html.fromHtml("Do you want to clear the Fire Protection saved data?"));
		bl.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog,
											int id) {
										
												UncheckFireProtection(1);//insertfireprotectionval();
												//((CheckBox)findViewById(R.id.fireprot_chk)).setChecked(true);
												((LinearLayout)findViewById(R.id.fireprot_table)).setVisibility(cf.v1.GONE);
												((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.GONE);
							    		 	 
										 
									}
		});
		bl.setNegativeButton("No",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog,
							int id) {
						
							 ((CheckBox)findViewById(R.id.fireprot_chk)).setChecked(false);
					
						 }
        });
		AlertDialog al=bl.create();
		al.setIcon(R.drawable.alertmsg);
		al.setCancelable(false);
		al.show();
	}
	protected void updatenotapplfireprot() {
		// TODO Auto-generated method stub
		
		try
		{
		chkvalues();
			if(c1.getCount()==0)
			{
				cf.arr_db.execSQL("INSERT INTO " + cf.GCH_FireProtection + " (fld_srid,fld_fireprot_na,fld_fireprotection)"
						+ "VALUES ('"+cf.selectedhomeid+"','"+fireprotnotappl+"','"+fireprotval+"')");		
			}
			else
			{
				cf.arr_db.execSQL("UPDATE "	+ cf.GCH_FireProtection	+ " SET fld_fireprot_na='"+fireprotnotappl+"',fld_fireprotection='"+fireprotval+"' WHERE fld_srid ='"+ cf.selectedhomeid + "'");
			}	
			
		}
		catch(Exception e)
		{
			System.out.println("sdfdfds"+e.getMessage());
		}
	}
	private void insertfireprotectionval() {
		// TODO Auto-generated method stub		
		if(!fireprottxt1.equals("") &&  !fireprottxt5.equals(""))
		{
			if(((EditText)findViewById(R.id.edfirenameandmodel)).getText().toString().equals(""))
			{
				nameandmodel = ((CheckBox)findViewById(R.id.namenotapppl)).getText().toString();
			}
			else
			{
				nameandmodel = ((EditText)findViewById(R.id.edfirenameandmodel)).getText().toString();
			}
			
			if(((EditText)findViewById(R.id.eddatelastserviced)).getText().toString().equals(""))
			{
				datelastservice = ((CheckBox)findViewById(R.id.datelastservrdio)).getText().toString();
			}
			else
			{
				datelastservice = ((EditText)findViewById(R.id.eddatelastserviced)).getText().toString();
			}
			if(((EditText)findViewById(R.id.edhowoftenserviced)).getText().toString().equals(""))
			{
				howoftenserviced= ((CheckBox)findViewById(R.id.howoftennotapppl)).getText().toString();
			}
			else
			{
				howoftenserviced =((EditText)findViewById(R.id.edhowoftenserviced)).getText().toString();
			}
			if(((EditText)findViewById(R.id.edhowoftenserviced)).getText().toString().equals(""))
			{
				servicecomp= ((CheckBox)findViewById(R.id.servcompnotapppl)).getText().toString();
			}
			else
			{
				servicecomp =((EditText)findViewById(R.id.edserviceompany)).getText().toString();
			}
			
			fireprotval = fireprottxt1+"&#94;"+cf.encode(nameandmodel)+"&#94;"+cf.encode(datelastservice)+"&#94;"+fireprottxt2+"&#39;"+cf.encode(((EditText)findViewById(R.id.edundercontract_othr)).getText().toString())+"&#94;"+
					cf.encode(howoftenserviced)+"&#94;"+cf.encode(servicecomp)+"&#94;"+
					fireprottxt3+"&#39;"+cf.encode(((EditText)findViewById(R.id.edmanualshut_othr)).getText().toString())+"&#94;"+
					fireprottxt4+"&#39;"+cf.encode(((EditText)findViewById(R.id.edtportablefireext_othr)).getText().toString())+"&#94;"+fireprottxt5;
		}	
		else
		{
			fireprotval="";
		}
		updatenotapplfireprot();
		
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
 		// replaces the default 'Back' button action
 		if (keyCode == KeyEvent.KEYCODE_BACK) {
 			cf.goclass(48);
 			return true;
 		}
 		
 		return super.onKeyDown(keyCode, event);
 	}
}