package idsoft.inspectiondepot.ARR;
import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.maps.GeoPoint;
public class Maps extends FragmentActivity implements Runnable {

	static GeoPoint point;
	final static int MAX_RESULT = 10;
	private static final String TAG = null;
	Drawable drawable;
    GoogleMap googleMap;
    CommonFunctions cf;
    static String addr, addr1;
	static String straddr;
	static String strcty;
	int ichk, k;
	String strstatnam, strcntry;
	static String newstraddr;
	ProgressDialog pd;
	static String newstrcty;
	String newstrstatnam, newstrcntry, InspectionType, status;
	TextView policyholderinfo;
	private String PH_address;
	private String PH_city;
	private String PH_state;
	private String PH_county;
	private String PH_zip;
	static Double lon = new Double(0);
	static Double lat = new Double(0);
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map1);
        cf=new CommonFunctions(this);
        Bundle bunQ1extras1 = getIntent().getExtras();
		cf.getDeviceDimensions();
		if (bunQ1extras1 != null) {
			cf.getExtras(bunQ1extras1);				
	 	}
		
		LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
		hdr_layout.addView(new HdrOnclickListener(this,1,"Map","",1,2,cf));
		
		cf.mainmenu_layout = (LinearLayout) findViewById(R.id.header);
        cf.mainmenu_layout.setMinimumWidth(cf.wd);
        cf.mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 9, 0,0, cf));
        
        TableRow tblrw = (TableRow)findViewById(R.id.row2);
		tblrw.setMinimumHeight(cf.ht);
		int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());

        // Showing status
        
        if(status!=ConnectionResult.SUCCESS){ // Google Play Services are not available

            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();

        }else { // Google Play Services are available

            // Getting reference to the SupportMapFragment of activity_main.xml
            SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapview);

            // Getting GoogleMap object from the fragment
            googleMap = fm.getMap();

            // Enabling MyLocation Layer of Google Map
            googleMap.setMyLocationEnabled(true);
            googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
            gepMapinfo1();
        }   
	}	
	private void gepMapinfo1() {
		// TODO Auto-generated method stub
		
	try{	Cursor c=cf.arr_db.rawQuery("Select * from "+cf.policyholder+" where ARR_PH_InspectorId='"+cf.Insp_id +"' and ARR_PH_SRID='"+cf.selectedhomeid+"'",null);
	if(c.getCount()>0)
	{
			c.moveToFirst();
			
	    	PH_address = cf.decode(c.getString(c.getColumnIndex("ARR_PH_Address1")));
	    	PH_city = cf.decode(c.getString(c.getColumnIndex("ARR_PH_City")));
	    	PH_state = cf.decode(c.getString(c.getColumnIndex("ARR_PH_State")));
	    	PH_county = cf.decode(c.getString(c.getColumnIndex("ARR_PH_County")));
	    	PH_zip = cf.decode(c.getString(c.getColumnIndex("ARR_PH_Zip")));
	    	if (PH_address.contains("")) {PH_address = PH_address.replace(" ", "+");}
		     if (PH_city.contains("")) {PH_city = PH_city.replace(" ", "+");}
		     if (PH_state.contains("")) {PH_state = PH_state.replace(" ", "+");}
		     if (PH_county.contains("")) {PH_county = PH_county.replace(" ", "+");}
		     addr = PH_address + "," + PH_city + "," + PH_state + "," + PH_county;			
		 }
	else
	{
		addr ="florida";
	}
	     
		    System.out.println("the address "+addr);
	}catch(Exception e){  System.out.println("the address erro"+e.getMessage());}
	  
		    String source = "<b><font color=#00FF33>Loading map. Please wait..."
					+ "</font></b>";
			pd = ProgressDialog.show(Maps.this, "", Html.fromHtml(source), true);
			/*ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo info = conMgr.getActiveNetworkInfo();
			if (info != null && info.isConnected()) {
				ichk = 0;
			} else {
				ichk = 1;
			}*/
			Thread thread = new Thread(Maps.this);
			thread.start();
	}
	public void clicker(View v)
	{
		switch(v.getId())
		{
		case R.id.hme:
			cf.gohome();
			break;
		}
	}
	public void run() {

	// TODO Auto-generated method stub
	if (cf.isInternetOn()) {
		getLocationInfo(addr);
		k = 1;
	} else {
		k = 2;
	}     
	handler.sendEmptyMessage(0);
}

private Handler handler = new Handler() {
	public void handleMessage(Message msg) {
		pd.dismiss();
		if (k == 1) {
			System.out.println("no more ieuse "+googleMap);
			if(googleMap!=null)
			{   
				System.out.println("comes correc2"+addr);
				googleMap.addMarker(new MarkerOptions().position(new LatLng(lat,lon)).title("Home Owner").icon(BitmapDescriptorFactory.fromResource(R.drawable.blueicon)).snippet(addr.replace("+", " ")));
				googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(lat,lon)));
			}

		} else if (k == 2) {
			cf.ShowToast("Sorry, There is no Network availability.",1);

		}

	}
};
public static JSONObject getLocationInfo(String address) {
	HttpGet httpGet = new HttpGet(
			"http://maps.google.com/maps/api/geocode/json?address="
					+ address + "&sensor=false");
System.out.println("url"+"http://maps.google.com/maps/api/geocode/json?address="
					+ address + "&sensor=false");
	HttpClient client = new DefaultHttpClient();
	HttpResponse response;
	StringBuilder stringBuilder = new StringBuilder();

	try {
		response = client.execute(httpGet);
		HttpEntity entity = response.getEntity();
		InputStream stream = entity.getContent();
		int b;
		while ((b = stream.read()) != -1) {
			stringBuilder.append((char) b);
		}
	} catch (ClientProtocolException e) {
		Log.i(TAG, "ClientProtocolException" + e.getMessage());
	} catch (IOException e) {
		Log.i(TAG, "IOException" + e.getMessage());
	}

	JSONObject jsonObject = new JSONObject();
	try {
		Log.i(TAG, "fdfgd");
		jsonObject = new JSONObject(stringBuilder.toString());
		Log.i(TAG, "fds" + jsonObject);
		getGeoPoint(jsonObject);
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	return jsonObject;
}
public static GeoPoint getGeoPoint(JSONObject jsonObject) {

	 
	Log.i(TAG, "latsdgfsdf");
	try {

		lon = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
				.getJSONObject("geometry").getJSONObject("location")
				.getDouble("lng");

		lat = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
				.getJSONObject("geometry").getJSONObject("location")
				.getDouble("lat");
		Log.i(TAG, "lat=" + lat + lon);
		//point = new GeoPoint((int) (lat * 1E6), (int) (lon * 1E6));
		//showmap(lat, lon);

	} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return new GeoPoint((int) (lat * 1E6), (int) (lon * 1E6));

}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			cf.goback(8);
			return true;
		}
		return true;
	}
	
}