package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class DrywallSummary extends Activity{
	CommonFunctions cf;
	boolean load_comment=true;
	RadioGroup dry1rdgval,dry2rdgval,dry3rdgval;
	String dry1rdgtxt="None at inspection",dry2rdgtxt="None at inspection",dry3rdgtxt="None at inspection";
	Cursor Drysum_save;
	RadioGroup home_ow[]=new RadioGroup[9];
	EditText home_ed;
	
	
	@Override
	   public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        cf=new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);
				
		 	}
	        setContentView(R.layout.drywallsummary);
	        cf.getDeviceDimensions();
	        
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
			hdr_layout.addView(new HdrOnclickListener(this,1,"Chinese Drywall","Summary of Conditions/Inspectors Findings ",6,0,cf));
	        LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
	        mainmenu_layout.setMinimumWidth(cf.wd);
	        mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 6, 0,0,cf));
	        LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 61, 1,0,cf));
	        TableRow tblrw = (TableRow)findViewById(R.id.row2);
			tblrw.setMinimumHeight(cf.ht);
			cf.CreateARRTable(7);
			declarations();
			summary_setvalues();
	}
	private void summary_setvalues() {
		// TODO Auto-generated method stub
		try
		{
			Cursor dry_home=cf.SelectTablefunction(cf.DRY_home, " WHERE fld_srid='"+cf.selectedhomeid+"'");
			if(dry_home.getCount()>0)
			{
				dry_home.moveToFirst();
				String home_pre=cf.decode(dry_home.getString(dry_home.getColumnIndex("home_pre"))),	home_assessment=cf.decode(dry_home.getString(dry_home.getColumnIndex("home_assessment"))),	home_priorof_pre=cf.decode(dry_home.getString(dry_home.getColumnIndex("home_priorof_pre"))),	
				home_attorny=cf.decode(dry_home.getString(dry_home.getColumnIndex("home_attorny"))),	home_odor=cf.decode(dry_home.getString(dry_home.getColumnIndex("home_odor"))),	home_furnishing=cf.decode(dry_home.getString(dry_home.getColumnIndex("home_furnishing"))),	home_breakdown=cf.decode(dry_home.getString(dry_home.getColumnIndex("home_breakdown"))),	
				home_cleanevidence=cf.decode(dry_home.getString(dry_home.getColumnIndex("home_cleanevidence"))),	home_test_requ=cf.decode(dry_home.getString(dry_home.getColumnIndex("home_test_requ"))),	home_testing_iden=cf.decode(dry_home.getString(dry_home.getColumnIndex("home_testing_iden"))),	
				home_signed=cf.decode(dry_home.getString(dry_home.getColumnIndex("home_signed"))),	home_comments=cf.decode(dry_home.getString(dry_home.getColumnIndex("home_comments")));
				setvalue_home(home_ow[0],home_pre);
				setvalue_home(home_ow[1],home_assessment);
				/*setvalue_home(home_ow[2],home_priorof_pre);
				setvalue_home(home_ow[3],home_attorny);*/
				setvalue_home(home_ow[2],home_odor);
				setvalue_home(home_ow[3],home_furnishing);
				setvalue_home(home_ow[4],home_breakdown);
				setvalue_home(home_ow[5],home_cleanevidence);
				setvalue_home(home_ow[6],home_test_requ);
				setvalue_home(home_ow[7],home_testing_iden);
				setvalue_home(home_ow[8],home_signed);
				home_ed.setText(home_comments);
				
				
			}
			
		   Cursor SCf_retrive=cf.SelectTablefunction(cf.DRY_sumcond, " where fld_srid='"+cf.selectedhomeid+"'");
		   if(SCf_retrive.getCount()>0)
		   {  
			   SCf_retrive.moveToFirst();
			   ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
			   dry1rdgtxt=SCf_retrive.getString(SCf_retrive.getColumnIndex("presofsulfur"));
			   ((RadioButton) dry1rdgval.findViewWithTag(dry1rdgtxt)).setChecked(true);
			   dry2rdgtxt=SCf_retrive.getString(SCf_retrive.getColumnIndex("obscopper"));
			   ((RadioButton) dry2rdgval.findViewWithTag(dry2rdgtxt)).setChecked(true);
			   dry3rdgtxt=SCf_retrive.getString(SCf_retrive.getColumnIndex("presofdry"));
			   ((RadioButton) dry3rdgval.findViewWithTag(dry3rdgtxt)).setChecked(true);
			   ((EditText)findViewById(R.id.inspcomment)).setText(cf.decode(SCf_retrive.getString(SCf_retrive.getColumnIndex("addcomments"))));
		   }
		   else
		   {
			   ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.INVISIBLE);
			   
		   }
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}
	private void setvalue_home(RadioGroup radioGroup, String home_pre) {
		// TODO Auto-generated method stub
		if(radioGroup.findViewWithTag(home_pre)!=null)
		{
			((RadioButton)radioGroup.findViewWithTag(home_pre)).setChecked(true);
		}
		
	}
	private void declarations() {
		// TODO Auto-generated method stub
		cf.setupUI((ScrollView) findViewById(R.id.scr));
		
		dry1rdgval = (RadioGroup)findViewById(R.id.dry1_rdg);
		dry1rdgval.setOnCheckedChangeListener(new checklistenetr(1));
		dry2rdgval = (RadioGroup)findViewById(R.id.dry2_rdg);
		dry2rdgval.setOnCheckedChangeListener(new checklistenetr(2));
		dry3rdgval = (RadioGroup)findViewById(R.id.dry3_rdg);
		dry3rdgval.setOnCheckedChangeListener(new checklistenetr(3));
		((RadioButton) dry1rdgval.findViewWithTag(dry1rdgtxt)).setChecked(true);
	    ((RadioButton) dry2rdgval.findViewWithTag(dry2rdgtxt)).setChecked(true);
	    ((RadioButton) dry3rdgval.findViewWithTag(dry3rdgtxt)).setChecked(true);
		   
		((EditText)findViewById(R.id.inspcomment)).addTextChangedListener(new textwatcher(1));
		home_ow[0]=(RadioGroup) findViewById(R.id.dry3_hm_RG1);
		home_ow[1]=(RadioGroup) findViewById(R.id.dry3_hm_RG2);
		//home_ow[2]=(RadioGroup) findViewById(R.id.dry3_hm_RG3);
		//home_ow[3]=(RadioGroup) findViewById(R.id.dry3_hm_RG4);
		home_ow[2]=(RadioGroup) findViewById(R.id.dry3_hm_RG5);
		home_ow[3]=(RadioGroup) findViewById(R.id.dry3_hm_RG6);
		home_ow[4]=(RadioGroup) findViewById(R.id.dry3_hm_RG7);
		home_ow[5]=(RadioGroup) findViewById(R.id.dry3_hm_RG8);
		home_ow[6]=(RadioGroup) findViewById(R.id.dry3_hm_RG9);
		home_ow[7]=(RadioGroup) findViewById(R.id.dry3_hm_RG10);
		home_ow[8]=(RadioGroup) findViewById(R.id.dry3_hm_RG11);
		home_ed=(EditText) findViewById(R.id.dry3_hm_EDcomment);
		home_ed.addTextChangedListener(new textwatcher(2));
		((ImageView) findViewById(R.id.drywall_sum_hide)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String tag=v.getTag().toString();
				if(tag.equals("plus"))
				{
					findViewById(R.id.dry1lin).setVisibility(View.VISIBLE);
					v.setTag("minus");
					((ImageView)v).setImageDrawable(getResources().getDrawable(R.drawable.minus));
				}
				else
				{
					findViewById(R.id.dry1lin).setVisibility(View.GONE);
					v.setTag("plus");
					((ImageView)v).setImageDrawable(getResources().getDrawable(R.drawable.plus));
				}
				
			}
		});
		((ImageView) findViewById(R.id.drywall_home_hide)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String tag=v.getTag().toString();
				if(tag.equals("plus"))
				{
					findViewById(R.id.drywall_home_content).setVisibility(View.VISIBLE);
					v.setTag("minus");
					((ImageView)v).setImageDrawable(getResources().getDrawable(R.drawable.minus));
				}
				else
				{
					findViewById(R.id.drywall_home_content).setVisibility(View.GONE);
					v.setTag("plus");
					((ImageView)v).setImageDrawable(getResources().getDrawable(R.drawable.plus));
				}
				
			}
		});
	}
	class checklistenetr implements OnCheckedChangeListener
	{
    	int i;
		public checklistenetr(int i) {
			// TODO Auto-generated constructor stub
		   this.i=i;
		}

		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			   RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
		       boolean isChecked = checkedRadioButton.isChecked();
		       if (isChecked)
		        {
		          switch (i) {
					case 1:
						dry1rdgtxt= checkedRadioButton.getText().toString().trim();
			         break;
					case 2:
						dry2rdgtxt= checkedRadioButton.getText().toString().trim();
			         break;
					case 3:
						dry3rdgtxt= checkedRadioButton.getText().toString().trim();
			         break;
					default:
						break;
					}
		        }
		     }
    }	
	class textwatcher implements TextWatcher
    {
	         public int type;
	         textwatcher(int type)
	    	{
	    		this.type=type;
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
	    		if(this.type==1)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.insp_tv_type1),1500); 
	    			if(s.toString().startsWith(" "))
	    			{
	    				((EditText)findViewById(R.id.inspcomment)).setText("");
	    			}
	    		}
	    		if(this.type==2)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.insp_tv_type2),1500); 
	    			if(s.toString().startsWith(" "))
	    			{
	    				((EditText)findViewById(R.id.inspcomment)).setText("");
	    			}
	    		}
	    	
	         }

	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,int after) {
	    		// TODO Auto-generated method stub
	     	}

	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,int count) {
	    		// TODO Auto-generated method stub
	    	}
	}	
	public void clicker(View v)
	{
		switch (v.getId()) {
			case R.id.hme:
				cf.gohome();
				break;
			case R.id.loadcomments:/***Call for the comments***/
				int len=((EditText)findViewById(R.id.inspcomment)).getText().toString().length();
				if(load_comment )
				{
						load_comment=false;
						int loc[] = new int[2];
						v.getLocationOnScreen(loc);
						Intent in1 = cf.loadComments("Summary of Conditions/Inspectors Findings Comments",loc);
						in1.putExtra("length", len);
						in1.putExtra("max_length", 500);
						startActivityForResult(in1, cf.loadcomment_code);
				}
				break;
			case R.id.save:
				/*if(((EditText)findViewById(R.id.inspcomment)).getText().toString().trim().equals(""))
				{
					cf.ShowToast("Please enter the Summary of Conditions/Inspectors Findings Comments.", 0);
					cf.setFocus(((EditText)findViewById(R.id.inspcomment)));
					((EditText)findViewById(R.id.inspcomment)).setText("");
				}
				else
				{*/
					if(check_home())
					{
					chk_values();
					try
					{
					Cursor chk=cf.SelectTablefunction(cf.DRY_home, " WHERE fld_srid='"+cf.selectedhomeid+"'");
					String home_pre=cf.encode(get_rg_value(home_ow[0])),home_assessment=cf.encode(get_rg_value(home_ow[1])),home_odor=cf.encode(get_rg_value(home_ow[2])),home_furnishing=cf.encode(get_rg_value(home_ow[3])),home_breakdown=cf.encode(get_rg_value(home_ow[4])),home_cleanevidence=cf.encode(get_rg_value(home_ow[5])),home_test_requ=cf.encode(get_rg_value(home_ow[6])),home_testing_iden=cf.encode(get_rg_value(home_ow[7])),home_signed=cf.encode(get_rg_value(home_ow[8])),home_comments=cf.encode(home_ed.getText().toString().trim());
					if(chk.getCount()>0)
					{
						
						cf.arr_db.execSQL(" UPDATE "+cf.DRY_home+" SET home_pre='"+home_pre+"',home_assessment='"+home_assessment+"',home_priorof_pre='',home_attorny='',home_odor='"+home_odor+"',home_furnishing='"+home_furnishing+"',home_breakdown='"+home_breakdown+"',home_cleanevidence='"+home_cleanevidence+"',home_test_requ='"+home_test_requ+"',home_testing_iden='"+home_testing_iden+"',home_signed='"+home_signed+"',home_comments='"+home_comments+"' WHERE  fld_srid='"+cf.selectedhomeid+"'");
					}
					else
					{
						cf.arr_db.execSQL("INSERT INTO "+cf.DRY_home+" (fld_srid,home_pre,home_assessment,home_priorof_pre,home_attorny,home_odor,home_furnishing,home_breakdown,home_cleanevidence,home_test_requ,home_testing_iden,home_signed,home_comments) " +
								"VALUES ('"+cf.selectedhomeid+"','"+home_pre+"','"+home_assessment+"','','','"+home_odor+"','"+home_furnishing+"','"+home_breakdown+"','"+home_cleanevidence+"','"+home_test_requ+"','"+home_testing_iden+"','"+home_signed+"','"+home_comments+"')");
					}
					
					}
					catch (Exception e) {
						// TODO: handle exception
						System.out.println("th update isseus="+e.getMessage()); 
					}
					if(Drysum_save.getCount()>0)
					{
						try
						{
							cf.arr_db.execSQL("UPDATE "+cf.DRY_sumcond+ " set presofsulfur='"+dry1rdgtxt+"',obscopper='"+dry2rdgtxt+"',"+
						                       "presofdry='"+dry3rdgtxt+"',"+
									           "addcomments='"+cf.encode(((EditText)findViewById(R.id.inspcomment)).getText().toString())+"' where fld_srid='"+cf.selectedhomeid+"'");
							cf.ShowToast("Summary of Conditions/Inspectors Findings saved successfully.", 1);
						}
						catch (Exception E)
						{
							String strerrorlog="Updating the Summary Conditions - Drywall";
							cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
						}
					}
					else
					{
						try
						{
							cf.arr_db.execSQL("INSERT INTO "
									+ cf.DRY_sumcond
									+ " (fld_srid,presofsulfur,obscopper,presofdry,addcomments,drywallcomments,atticcomments,"+
									   "applianccomments,hvaccomments,assesmentcomments)"
									+ "VALUES('"+cf.selectedhomeid+"','"+dry1rdgtxt+"','"+dry2rdgtxt+"',"+
									"'"+dry3rdgtxt+"','"+cf.encode(((EditText)findViewById(R.id.inspcomment)).getText().toString())+"','',"+
									"'','','','')");
							 cf.ShowToast("Summary of Conditions/Inspectors Findings saved successfully.", 1);
						}
						catch (Exception E)
						{
							String strerrorlog="Inserting the Summary Conditions - Drywall";
							cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
						}
					}
					((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
				    cf.goclass(62);
					}
				//}
				break;
			default:
				break;
		}
	}
	private String get_rg_value(RadioGroup radioGroup) {
		// TODO Auto-generated method stub
		return (radioGroup.getCheckedRadioButtonId()!=-1)? radioGroup.findViewById(radioGroup.getCheckedRadioButtonId()).getTag().toString():"";
	//	return null;
	}
	private boolean check_home() {
		// TODO Auto-generated method stub
		String titaray[]={"Homeowner present at the time of inspection","Reason for Chinese Drywall Assessment","Homeowner experiencing sulfur-like odor or other unusual odors",
				"Homeowner experiencing black coating on furnishings and fittings","Homeowner experiencing mechanical equipment breakdowns","Homeowner knowingly or unknowingly cleaned surface of any evidence","Material testing requested","Signed copy of the Pre-Inspection Agreement received","Homeowner agreed to material testing"};

		for(int i=0;i<home_ow.length;i++)
		{
			if(home_ow[i].getCheckedRadioButtonId()==-1)
			{
				cf.ShowToast("Please select option for "+titaray[i], 1);
				return false;
			}
		}
		if(home_ed.getText().toString().trim().equals(""))
		{
			cf.ShowToast("Please enter value for HomeOwner confirmed following repairs/ comments", 1);
			return false;
		}
		return true;
	}
	private void chk_values() {
		// TODO Auto-generated method stub
		try
		{
			 Drysum_save = cf.SelectTablefunction(cf.DRY_sumcond, " where fld_srid='"+cf.selectedhomeid+"'");
			 
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
 		// replaces the default 'Back' button action
 		if (keyCode == KeyEvent.KEYCODE_BACK) {
 			cf.isaccess= cf.chk_InspTypeQuery(cf.selectedhomeid,"Sinkhole Inspection");
			if(cf.isaccess==true)
 			cf.goback(5);
			else
				cf.goback(0);
 			return true;
 		}
 		
 		return super.onKeyDown(keyCode, event);
 	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		  if(requestCode==cf.loadcomment_code)
		  {
			load_comment=true;
			if(resultCode==RESULT_OK)
			{
				 cf.ShowToast("Comments added successfully.", 0);
				 String atccomts = ((EditText)findViewById(R.id.inspcomment)).getText().toString();
				 ((EditText)findViewById(R.id.inspcomment)).setText((atccomts+" "+data.getExtras().getString("Comments")).trim());
				
			}
			
		}
    }
}
