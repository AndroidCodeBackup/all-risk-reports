package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;


import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.R.color;
import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView; 
import android.widget.TextView;
import android.widget.Toast;

public class InspectionListing_paging extends Activity
{
	int rws;
	ScrollView sv;String status,insptype,substatus;
	String strtit,res = "",inspdata,anytype = "anyType{}",sql,statusname;
	EditText search_text;
	LinearLayout onlinspectionlist;
	TextView tvstatus[];
	Button deletebtn[];
	public String[] data,countarr;
	CommonFunctions cf;
	CommonDeleteInsp del;
	ProgressDialog pd;
	String headernote="";
	int  total=0,show_handler=0;
    boolean online_rec=false;
    SoapObject result = null;
    private int rec_per_page;
	private int numbeofpage;
	private int Status=0;
	private int Substatus=0;
	int current=0;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			status = extras.getString("Status");
			total = extras.getInt("Total");
			online_rec = extras.getBoolean("Online");
			//statusname = extras.getString("statusname");
		}
		setContentView(R.layout.inspectionlisting_paging);
		if(status.equals("CIO"))
		{
			Status=40;
			Substatus=41;
			((TextView) findViewById(R.id.status)).setText(" Completed in Online");
		}
		else if(status.equals("UTS"))
		{
			Status=110;
			Substatus=111;
			((TextView) findViewById(R.id.status)).setText(" Unable to Schedule");
		}
		else if(status.equals("CAN"))
		{
			Status=90;
			Substatus=0;
			((TextView) findViewById(R.id.status)).setText(" Canceled");
		}
		else if(status.equals("RR"))
		{
			Status=777;
			Substatus=0;
			((TextView) findViewById(R.id.status)).setText(" Reports Ready");
		}
		
		current=1;
		rec_per_page=50;
		if(total>rec_per_page)
		{
			numbeofpage=(int) (total/rec_per_page);
			findViewById(R.id.page_prev).setVisibility(View.GONE);
			
			if(total%rec_per_page!=0)
			{
				numbeofpage++;
			}
		}
		else
		{
			numbeofpage=1;
			findViewById(R.id.pre_next).setVisibility(View.GONE);
			
		}
		
		
		cf = new CommonFunctions(this);
		del = new CommonDeleteInsp(this); 
		cf.getInspectorId();
		cf.CreateARRTable(3);
		onlinspectionlist = (LinearLayout) findViewById(R.id.linlayoutdyn);
		search_text = (EditText) findViewById(R.id.search_text);
		search_text.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				search_text.setFocusable(true);
				search_text.setFocusableInTouchMode(true);	
				return false;
			}
		});
		((EditText) findViewById(R.id.page_ed_go_to)).setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				((EditText) findViewById(R.id.page_ed_go_to)).setFocusable(true);
				((EditText) findViewById(R.id.page_ed_go_to)).setFocusableInTouchMode(true);	
				return false;
			}
		});
		
		cf.CreateARRTable(66);
		TextView note  = (TextView) findViewById(R.id.note);
		
		if("Assign".equals(statusname))
		{
			headernote="POLICY HOLDER NAME | POLICY NUMBER | ADDRESS | CITY | STATE | COUNTY | ZIP | INSPECTION NAME";
		}
		else
		{
			
			headernote="POLICY HOLDER NAME | POLICY NUMBER | ADDRESS | CITY | STATE | COUNTY | ZIP | INSPECTION DATE | INSPECTION START TIME  - END TIME | INSPECTION NAME";
		}		
		System.out.println("header note"+headernote);
		((TextView) findViewById(R.id.inspectiontype)).setText(cf.strret);
		((TextView) findViewById(R.id.note)).setText(headernote);
		/*String source = "<font color=#FFFFFF>Loading data. Please wait..."	+ "</font>";
		pd = ProgressDialog.show(InspectionListing_paging.this,"", Html.fromHtml(source), true);
		 new Thread(new Runnable() {
                public void run() {
                	gethomeownerlist();
                	finishedHandler.sendEmptyMessage(0);
                }
            }).start();	
		 */
		get_fromweb(1,rec_per_page);
		 ImageView im =(ImageView) findViewById(R.id.head_insp_info);
			im.setVisibility(View.VISIBLE);
			im.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					    Intent s2= new Intent(InspectionListing_paging.this,PolicyholdeInfoHead.class);
						Bundle b2 = new Bundle();
						s2.putExtra("homeid", cf.selectedhomeid);
						s2.putExtra("Type", "Inspector");
						s2.putExtra("insp_id", cf.Insp_id);
						((Activity) cf.con).startActivityForResult(s2, cf.info_requestcode);
				}
			});
	}
	private void get_fromweb(final int start,final int end) {
		// TODO Auto-generated method stub
		 String source = "<font color=#FFFFFF>Loading data. Please wait..."	+ "</font>";
		 pd = ProgressDialog.show(InspectionListing_paging.this,"", Html.fromHtml(source), true);
		 new Thread(new Runnable() {
             public void run() {
            	 gethomeownerdetails(start,end);
             	
             }
         }).start();
		//dbquery();
	}
	private void gethomeownerdetails(int start,int end)
	{
		if(end>total)
		{
			end=total;
		}
		
		
		SoapObject request = new SoapObject(cf.NAMESPACE, "UPDATEMOBILEDB_PAGING");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("Statusid",Status);
		request.addProperty("Substatusid",Substatus);
		request.addProperty("Inspectiontypeid",28);
		request.addProperty("Startid",start);
		request.addProperty("Endid",end);
		request.addProperty("Searchtext",res);
		request.addProperty("Inspectorid",cf.Insp_id.toString());
		System.out.println(" the property="+request);
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL_ARR);
		result = null;
		try
		{
			androidHttpTransport.call("http://tempuri.org/UPDATEMOBILEDB_PAGING", envelope);
			result = (SoapObject) envelope.getResponse();
			//System.out.println(" the result "+result);
			if (result.equals("anyType{}")) {
				cf.ShowToast("Server is busy. Please try again later",1);
				cf.gohome();
				show_handler=0;
			} else {
				show_handler=1;
				finishedHandler.sendEmptyMessage(0);
					
					
			}
		} catch (Exception e) {
			try {
				throw e;
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}
	}
	private void InsertDate(SoapObject objInsert)  
	{
		 cf.dropTable(66);	
		 cf.CreateARRTable(66);
	     int Cnt = objInsert.getPropertyCount();
	     if(current*rec_per_page>=total)
	     {
	     	findViewById(R.id.page_next).setVisibility(View.GONE);
	     }
	     else
	     {
	     	findViewById(R.id.page_next).setVisibility(View.VISIBLE);
	     }
	     for (int i = 0; i < Cnt; i++) 
	     {
	    	 
			SoapObject obj = (SoapObject) objInsert.getProperty(i);
			try {
					String  email, cperson,inspectionName, IsInspected = "0", insurancecompanyname, IsUploaded = "0", homeid, firstname, lastname, middlename, addr1, addr2, city, state, country, zip, homephone, cellphone, workphone, ownerpolicyno, companyid, inspectorid, wId, cId, inspectorfirstname, inspectorlastname, scheduleddate, yearbuilt, nstories, inspectionstarttime, inspectionendtime, inspectioncomment, inspectiontypeid;
					homeid = String.valueOf(obj.getProperty("SRID"));
					firstname = String.valueOf(obj.getProperty("FirstName"));
					lastname = String.valueOf(obj.getProperty("LastName"));
					addr1 = String.valueOf(obj.getProperty("Address1"));
					city = String.valueOf(obj.getProperty("City"));
					state = String.valueOf(obj.getProperty("State"));
					country = String.valueOf(obj.getProperty("Country"));
					zip = String.valueOf(obj.getProperty("Zip"));
					ownerpolicyno = String
							.valueOf(obj.getProperty("OwnerPolicyNo"));
					scheduleddate = String
							.valueOf(obj.getProperty("ScheduledDate"));
			    	inspectionstarttime = String.valueOf(obj
							.getProperty("InspectionStartTime"));
					inspectionendtime = String.valueOf(obj
							.getProperty("InspectionEndTime"));
				    inspectionName = String.valueOf(obj.getProperty("InspectionNames")).trim();
				   cf.arr_db.execSQL("INSERT INTO "
							+ cf.OnlineTable
							+ " (s_SRID,InspectorId,Status,SubStatus,InspectionTypeId,s_OwnersNameFirst,s_OwnersNameLast,s_propertyAddress,s_City,s_State,s_County,s_ZipCode,s_OwnerPolicyNumber,ScheduleDate,InspectionStartTime,InspectionEndTime,InspectionName)"
							+ " VALUES ('" + homeid + "','"+ cf.Insp_id+"','"
							+ status+"','"
							+ substatus+"','"+insptype+"','"
							+ cf.encode(firstname) + "','"
							+ cf.encode(lastname) + "','"
							+ cf.encode(addr1) + "','"
							+ cf.encode(city) + "','"
							+ cf.encode(state) + "','"
							+ cf.encode(country) + "','" + zip
							+ "','" + cf.encode(ownerpolicyno) + "','"
							+ scheduleddate + "','" + inspectionstarttime + "','"
							+ inspectionendtime+"','"+inspectionName+"')");
					
				} 
			    catch (Exception e) 
			    {
					System.out.println("e=" + e.getMessage());
			    }
	     }
	  }
	private Handler finishedHandler = new Handler() {
	    @Override 
	    public void handleMessage(Message msg) {
	        pd.dismiss();
	        InsertDate(result);
	         dbquery();
	    }
	};
	private void dbquery() {
		data = null;		
		countarr = null;
		rws = 0;
		sql = "select * from " + cf.OnlineTable;
		/*if (!res.equals("")) {

			sql += " where (s_OwnersNameFirst like '%" + res
					+ "%' or s_OwnersNameLast like '%" + res
					+ "%' or s_OwnerPolicyNumber like '%" + res
					+ "%') and InspectorId='" + cf.Insp_id + "'";
			

		} else {
			
		}*/
		//System.out.println("sql "+sql);
		Cursor cur = cf.arr_db.rawQuery(sql, null);
		rws = cur.getCount();
		((TextView)findViewById(R.id.page_txt_no_fo_page)).setText("Current Page/Total Pages: "+current+"/"+numbeofpage);
		if(current>1)
		{
			findViewById(R.id.page_prev).setVisibility(View.VISIBLE);
		}
		((TextView)findViewById(R.id.totla_no)).setText("Showing Records/Total Records : " + rws+"/"+total);
		countarr= new String[rws];
		data = new String[rws];
		int j = 0;
		cur.moveToFirst();
		if (cur.getCount() >0) {
			((TextView) findViewById(R.id.noofrecords)).setText("No of Records : "+rws);
			((LinearLayout) findViewById(R.id.dynamiclayout)).setVisibility(cf.v1.VISIBLE);
			((TextView) findViewById(R.id.norecordstxt)).setVisibility(cf.v1.GONE);
			do {
				
				String s =(cf.decode(cur.getString(cur.getColumnIndex("s_OwnersNameFirst"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("s_OwnersNameFirst")));
				data[j] =  s+ " ";
				s=(cf.decode(cur.getString(cur.getColumnIndex("s_OwnersNameLast"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("s_OwnersNameLast")));
				this.data[j] += s + " | ";
				s=(cf.decode(cur.getString(cur.getColumnIndex("s_OwnerPolicyNumber"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("s_OwnerPolicyNumber")));
				this.data[j] += s+ " | ";
				s=(cf.decode(cur.getString(cur.getColumnIndex("s_propertyAddress"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("s_propertyAddress")));
				this.data[j]+= s + " | ";
				s=(cf.decode(cur.getString(cur.getColumnIndex("s_City"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("s_City")));
				this.data[j] += s + " | ";
				s=(cf.decode(cur.getString(cur.getColumnIndex("s_State"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("s_State")));
				this.data[j] += s + " | ";
				s=(cf.decode(cur.getString(cur.getColumnIndex("s_County"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("s_County")));
				this.data[j] += s + " | ";
				s=(cf.decode(cur.getString(cur.getColumnIndex("s_ZipCode"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("s_ZipCode")));
				this.data[j] += s+ " | ";
				
					s=(cf.decode(cur.getString(cur.getColumnIndex("ScheduleDate"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("ScheduleDate")));
					this.data[j] += s+ " | ";
					s=(cf.decode(cur.getString(cur.getColumnIndex("InspectionStartTime"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("InspectionStartTime")));
					this.data[j] += s+ " - ";
					s=(cf.decode(cur.getString(cur.getColumnIndex("InspectionEndTime"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("InspectionEndTime")));
					this.data[j] += s+ " | ";
				
				s=(cf.decode(cur.getString(cur.getColumnIndex("InspectionName"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("InspectionName"))).trim();
				this.data[j] += s + " | ";
				

				countarr[j]=cur.getString(cur.getColumnIndex("s_SRID"));
				j++;
				
			} while (cur.moveToNext());
			search_text.setText("");
			display();
			cf.hidekeyboard(search_text);
		} else {
			cf.ShowToast("Sorry, No results found for your search criteria.",1);
			onlinspectionlist.removeAllViews();
			((LinearLayout) findViewById(R.id.dynamiclayout)).setVisibility(cf.v1.GONE);
			((TextView) findViewById(R.id.norecordstxt)).setVisibility(cf.v1.VISIBLE);
			cf.hidekeyboard(search_text);
		}

	}
	
	private void display() {
		
		onlinspectionlist.removeAllViews();
		sv = new ScrollView(this);
		onlinspectionlist.addView(sv);

		if(total>rec_per_page)
		{
			LinearLayout.LayoutParams li =(LinearLayout.LayoutParams) sv.getLayoutParams();
			li.setMargins(0, 0, 0, 50);
		}
		final LinearLayout l1 = new LinearLayout(this);
		l1.setOrientation(LinearLayout.VERTICAL);
		sv.addView(l1);
		
		View v = new View(this);
		v.setBackgroundResource(R.color.black);
		l1.addView(v,LayoutParams.FILL_PARENT,1);
		if (data.length>=1)
		{				
			for (int i = 0; i < data.length; i++) 
			{		
				tvstatus = new TextView[rws];
				deletebtn = new Button[rws];
				LinearLayout l2 = new LinearLayout(this);
				l2.setOrientation(LinearLayout.HORIZONTAL);
				l1.addView(l2);
	
				LinearLayout lchkbox = new LinearLayout(this);
				LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(900, ViewGroup.LayoutParams.WRAP_CONTENT);
				paramschk.topMargin = 8;
				paramschk.leftMargin = 20;
				paramschk.bottomMargin = 10;
				l2.addView(lchkbox);
	
				tvstatus[i] = new TextView(this);
				tvstatus[i].setTag("textbtn" + i);
				tvstatus[i].setText(data[i]);
				tvstatus[i].setTextColor(Color.WHITE);
				tvstatus[i].setTextSize(TypedValue.COMPLEX_UNIT_PX,14);
				lchkbox.addView(tvstatus[i], paramschk);
				
				
				 if (i % 2 == 0) {
					 l2.setBackgroundColor(Color.parseColor("#13456d"));
				    	} else {
				    	l2.setBackgroundColor(Color.parseColor("#386588"));
				    	}
				 if(data.length!=(i+1))
					{
						 View v1 = new View(this);
						 v1.setBackgroundResource(R.color.white);
						 l1.addView(v1,LayoutParams.FILL_PARENT,1);	 
					}
			}
			View v2 = new View(this);
			v2.setBackgroundResource(R.color.black);
			onlinspectionlist.addView(v2,LayoutParams.FILL_PARENT,1);
		}
		else
		{
		
		}
	}
	public void clicker(View v) {
		switch (v.getId()) {
	
		case R.id.home:
			cf.gohome();
			break;
		case R.id.search:
			
			String temp = cf.encode(search_text.getText().toString().trim());
			if (temp.equals("")) {
				cf.ShowToast("Please enter the Name or Policy Number to search.", 1);
				search_text.requestFocus();
			} 
			else
			{
				res = temp;
				//String Search=search_text.getText().toString().trim();
				search_text.setText("");
				current=1;
				get_fromweb(1,rec_per_page);
				//dbquery();
			}
			break;
		case R.id.clear:
			search_text.setText("");
			res = "";
			get_fromweb(1,rec_per_page);
			//dbquery();
			cf.hidekeyboard();
			break;
		case R.id.page_next:
			search_text.setText("");
			res = "";
			current++;
			get_fromweb(((rec_per_page*(current-1))+1),(rec_per_page*current));
		break;
		case R.id.page_prev:
			search_text.setText("");
			res = "";
			current--;
			get_fromweb(((rec_per_page*(current-1))+1),(rec_per_page*current));
		break;
		case R.id.page_goto:
			search_text.setText("");
			String go=((EditText) findViewById(R.id.page_ed_go_to)).getText().toString();
			if(!go.equals(""))
			{
				if(Integer.parseInt(go)<=numbeofpage)
				{
					if(Integer.parseInt(go)>0)
					{

					if(Integer.parseInt(go)!=current)
					{
						current=Integer.parseInt(go);
						((EditText) findViewById(R.id.page_ed_go_to)).setText("");
						get_fromweb(((rec_per_page*(current-1))+1),(rec_per_page*current));
					}else
					{
						cf.ShowToast("Request page and current page are same ", 0);
					}
					}else
					{
						cf.ShowToast("Enter valid page no ", 0);
					}
				}
				else
				{
					cf.ShowToast("Invalid page no", 0);
				}
			}else
			{
				cf.ShowToast("Invalid page no", 0);
			}
					
		break;	
		}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			startActivity(new Intent(InspectionListing_paging.this,Dashboard.class));
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}