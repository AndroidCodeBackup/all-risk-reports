/*
   ************************************************************
   * Project: ALL RISK REPORT
   * Module : GCHElectric.java
   * Creation History:
      Created By : Gowri on 
   * Modification History:
      Last Modified By : Gowri on 04/04/2012
   ************************************************************  
*/
package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.WindMitCommRestsupp.checklistenetr;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

public class GCHMisc extends Activity {
	CommonFunctions cf;
	Cursor c1;
	int miscrdgp_id1,miscrdgp_id3,miscrdgp_id2,miscchkval=0;
	String misnotappl="0",miscell_na="",misc3text="",miscellaneousval="",miscellaneousval1="",miscellaneousval2="",soiled_val="",desccomments_val="",descoption_val="",trash_val="",miscellanval="";
	RadioGroup miscrdgp1,miscrdgp2,miscrdgp3;
	boolean mischk1=false,mischk2=false,mischk3=false;
	@Override
	   public void onCreate(Bundle savedInstanceState) {

	        super.onCreate(savedInstanceState);
	        cf=new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);				
		 	}System.out.println("inside gch misc");
	        setContentView(R.layout.gchmisc);
	        cf.getDeviceDimensions();
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
			hdr_layout.addView(new HdrOnclickListener(this,1,"General Conditions & Hazards","Miscellaneous",4,0,cf));
	        LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
	        mainmenu_layout.setMinimumWidth(cf.wd);
	        mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 4, 0,0,cf));
	        LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 418, 1,0,cf));
			/*TableRow tblrw = (TableRow)findViewById(R.id.row2);
			tblrw.setMinimumHeight(cf.ht);*/
			cf.CreateARRTable(418);
			declarations();
			Misc_SetValue();
	}
	private void declarations() 
	{
		cf.setupUI((ScrollView) findViewById(R.id.scr));
		miscrdgp1 = (RadioGroup)findViewById(R.id.misc1_rdg);
	    miscrdgp2 = (RadioGroup)findViewById(R.id.misc2_rdg);
	    miscrdgp3 = (RadioGroup)findViewById(R.id.misc3_rdg);miscrdgp3.setOnCheckedChangeListener(new checklistenetr(1));
	    ((EditText)findViewById(R.id.edtmisc)).addTextChangedListener(new textwatcher(1));
	}	
	class textwatcher implements TextWatcher
	{
	       public int type;
	        textwatcher(int type)
	    	{
	    		this.type=type;
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {
	    		// TODO Auto-generated method stub	    	
	    		if(this.type==1)
	    		{
		    		if(!s.toString().equals(""))
		    		{
		    			mischk3=true;	
		    			try{if(mischk3){miscrdgp3.clearCheck();}}catch(Exception e){System.out.println("CC"+e.getMessage());}
		    		}
	    		}
	    	}
	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,

	    			int after) {
	    		// TODO Auto-generated method stub
	    	}
	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,int count) {
	    		// TODO Auto-generated method stub
	    	}	    	
	    }
	class checklistenetr implements OnCheckedChangeListener
	{
		int i;
		public checklistenetr(int i) {
			// TODO Auto-generated constructor stub
		   this.i=i;
		}
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			 RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
		        boolean isChecked = checkedRadioButton.isChecked();
		        if (isChecked)
		        {
		          switch (i) 
		          {
		          case 1:
		        	  ((EditText)findViewById(R.id.edtmisc)).setText("");
						misc3text= checkedRadioButton.getText().toString().trim();
						System.out.println("misc3text"+misc3text);
						((RadioButton) miscrdgp3.findViewWithTag(misc3text)).setChecked(true);
						
					break;
		          }
		        }
		}
		
	}
	private void Misc_SetValue()	
	{
		chkvalues();System.out.println("came after chkvalue"+miscell_na);
		if(c1.getCount()==0)
		{
			((CheckBox)findViewById(R.id.misc_chk)).setChecked(true);
			((LinearLayout)findViewById(R.id.miscellin)).setVisibility(cf.v1.GONE);
		}
		else
		{
		if(miscell_na.equals("0") || miscell_na.equals(""))
		{
			((CheckBox)findViewById(R.id.misc_chk)).setChecked(false);
			((LinearLayout)findViewById(R.id.miscellin)).setVisibility(cf.v1.VISIBLE);
			
			if(!trash_val.equals("") || !soiled_val.equals("") || !descoption_val.equals(""))
			{
				if(!trash_val.equals("")){
				((RadioButton) miscrdgp1.findViewWithTag(trash_val)).setChecked(true);
				}
				
				if(!soiled_val.equals("")){
				((RadioButton) miscrdgp2.findViewWithTag(soiled_val)).setChecked(true);
				}
				if(desccomments_val.equals(""))
				{
					if(!descoption_val.equals(""))
					{
						((RadioButton) miscrdgp3.findViewWithTag(descoption_val)).setChecked(true);
					}
				}
				if(!desccomments_val.equals("")){
				((EditText)findViewById(R.id.edtmisc)).setText(desccomments_val);
				}
				((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);			
			}
			else{
				((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.GONE);				
			}			
		}
		else
		{
			((CheckBox)findViewById(R.id.misc_chk)).setChecked(true);
			((LinearLayout)findViewById(R.id.miscellin)).setVisibility(cf.v1.GONE);
			((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
		}System.out.println("completed misc setvalue");
		}
	}
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.misc_chk:
			if(((CheckBox)findViewById(R.id.misc_chk)).isChecked())
			{
				
				if(!trash_val.equals(""))
				{
					showunchkalert();
				}
				else
				{
					UncheckMiscellaneous();
					((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.GONE);
					((LinearLayout)findViewById(R.id.miscellin)).setVisibility(cf.v1.GONE);
				}				
			}
			else
			{
				UncheckMiscellaneous();
				 ((LinearLayout)findViewById(R.id.miscellin)).setVisibility(cf.v1.VISIBLE);
				((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.GONE);
			}
			break;
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.save:
			if(((CheckBox)findViewById(R.id.misc_chk)).isChecked())
			{
				misnotappl="1";
				insertmiscvalues();
				cf.ShowToast("Miscellaneous saved successfully", 0);
				((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
				cf.goclass(49);
				// cf.gotoclass(35, WindMitCommRestsupp.class);		
			}
			else
			{
				misnotappl="0";
				miscrdgp_id1= miscrdgp1.getCheckedRadioButtonId();	
				miscellaneousval1 = (miscrdgp_id1==-1)? "":((RadioButton) findViewById(miscrdgp_id1)).getText().toString();
				miscrdgp_id2= miscrdgp2.getCheckedRadioButtonId();	
				miscellaneousval2 = (miscrdgp_id2==-1)? "":((RadioButton) findViewById(miscrdgp_id2)).getText().toString();
				
				if(!miscellaneousval1.trim().equals(""))
				{
					 if(!((EditText)findViewById(R.id.edtmisc)).getText().toString().trim().equals("") || !misc3text.equals(""))
					 {
						 if(!miscellaneousval2.trim().equals(""))
						 {
							 insertmiscvalues();
							 cf.ShowToast("Miscellaneous saved successfully", 0);
								((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
								cf.goclass(49);
								 //cf.gotoclass(35, WindMitCommRestsupp.class);		
						 }
						 else
						 {
								cf.ShowToast("Please select the option for Soiled Linens stored in closed metal containers.", 0);
						 }
					}
					else
					{
						 cf.ShowToast("Please enter the Describe Disposal of Ash Tray Contents throughout Business Day and at Closing.", 0);
						 cf.setFocus(((EditText)findViewById(R.id.edtmisc)));
					}
				}
				else
				{
					cf.ShowToast("Please select the options for Trash Removed from Premises at close of each day.", 0);
				}
			}
			break;
		}
	}
	protected void UncheckMiscellaneous() {
		// TODO Auto-generated method stub
		mischk1=true;mischk2=true;mischk3=true;
		 try{if(mischk1){miscrdgp1.clearCheck();}}catch(Exception e){}
		 try{if(mischk2){miscrdgp2.clearCheck();}}catch(Exception e){}
		 try{if(mischk3){miscrdgp3.clearCheck();}}catch(Exception e){}
		 ((EditText)findViewById(R.id.edtmisc)).setText("");
		 miscellaneousval1="";miscellaneousval2="";miscchkval=0;trash_val="";
	}
	
	private void chkvalues()
	{
		try
		{
			c1 = cf.arr_db.rawQuery("SELECT * FROM "+ cf.GCH_Misc + " WHERE fld_srid='"+ cf.selectedhomeid + "'", null);				
			int rws = c1.getCount();
			if(rws>0)
			{
				c1.moveToFirst();
				miscell_na = c1.getString(c1.getColumnIndex("fld_misc_na"));	
				trash_val = c1.getString(c1.getColumnIndex("fld_trash"));
				descoption_val = c1.getString(c1.getColumnIndex("fld_descroption"));
				desccomments_val = cf.decode(c1.getString(c1.getColumnIndex("fld_desccomments")));
				soiled_val = c1.getString(c1.getColumnIndex("fld_soiled"));
				
		    }c1.close();
		}
		catch(Exception e){
			System.out.println("EEe"+e.getMessage());
			
		}
	}
	public void showunchkalert() {
		// TODO Auto-generated method stub
		AlertDialog.Builder bl = new Builder(GCHMisc.this);
		bl.setTitle("Confirmation");
		bl.setMessage(Html.fromHtml("Do you want to clear the Miscellaneous saved data?"));
		bl.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog,
											int id) {
										
												UncheckMiscellaneous();//insertmiscvalues();
												//((CheckBox)findViewById(R.id.misc_chk)).setChecked(true);
												((LinearLayout)findViewById(R.id.miscellin)).setVisibility(cf.v1.GONE);
												((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.GONE);
							    		 	 
										 
									}
		});
		bl.setNegativeButton("No",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog,
							int id) {
						
							 ((CheckBox)findViewById(R.id.misc_chk)).setChecked(false);
					
						 }
        });
		AlertDialog al=bl.create();
		al.setIcon(R.drawable.alertmsg);
		al.setCancelable(false);
		al.show();
	}
	
	private void insertmiscvalues() {
		// TODO Auto-generated method stub	
		
		try
		{
			chkvalues();
			
			if(c1.getCount()==0)
			{
				cf.arr_db.execSQL("INSERT INTO " + cf.GCH_Misc + " (fld_srid,fld_misc_na,fld_trash,fld_descroption,fld_desccomments,fld_soiled)"
						+ "VALUES ('"+cf.selectedhomeid+"','"+misnotappl+"','"+miscellaneousval1+"','"+misc3text+"','"+cf.encode(((EditText)findViewById(R.id.edtmisc)).getText().toString().trim())+"','"+miscellaneousval2+"')");
			}
			else
			{
				cf.arr_db.execSQL("UPDATE "	+ cf.GCH_Misc	+ " SET fld_misc_na='"+misnotappl+"',fld_trash='"+miscellaneousval1+"'," +
						"fld_descroption='"+misc3text+"',fld_desccomments='"+cf.encode(((EditText)findViewById(R.id.edtmisc)).getText().toString().trim())+"',fld_soiled='"+miscellaneousval2+"' WHERE fld_srid ='"+ cf.selectedhomeid + "'");	
			}	
			
		}
		catch(Exception e)
		{
			System.out.println("sdfdfds"+e.getMessage());
		}
		
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
 		// replaces the default 'Back' button action
 		if (keyCode == KeyEvent.KEYCODE_BACK) {
 			cf.goclass(417);
 			return true;
 		}
 		
 		return super.onKeyDown(keyCode, event);
 	}
}