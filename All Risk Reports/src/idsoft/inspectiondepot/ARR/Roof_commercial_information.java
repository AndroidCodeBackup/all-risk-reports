package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.BIGeneralData.Spin_Selectedlistener;
import idsoft.inspectiondepot.ARR.RoofSection.RCN_clicker;
import idsoft.inspectiondepot.ARR.RoofSection.RCT_EditClick;
import idsoft.inspectiondepot.ARR.RoofSection.onclick_viewimage;

import java.io.IOException;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeoutException;

import org.xmlpull.v1.XmlPullParserException;


import android.R.color;
import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.RelativeLayout.LayoutParams;

public class Roof_commercial_information extends Activity implements Runnable {
CommonFunctions cf;
private String submenu,Roof_page_value[];
private int mYear, mMonth,mDay;

Spinner Sp_RCT[]=new Spinner[8];
String arr_RCT[]={"-Select-","Asphalt/Fiberglass Shingle","Concrete/Clay Tile","Metal","Built Up Tar and Gravel","Modified Bitumen","Wood Shake/Shingle","Membrane","Other"},
arr_YOI[]=null,arr_RS[]={"-Select-","<1/12","1/12","2/12","3/12","4/12","5/12","6/12","7/12","8/12","9/12","10/12","11/12","12/12","13/12","14/12","Flat","Unknown"},
arr_RSH[]={"-Select-","Hip Roof","Gable Roof","Mansard Roof","Flat Roof","Shed Roof","Butterfly Roof","Other"},
arr_RST[]={"-Select-","Conventional Frame","Wood Truss","Steel Truss","Concrete Frame","Structural","Steel Frame","Other"},
arr_BC[]={"-Select-",">2001 FBC","1994 SFBC","Pre 2001 FBC","Pre 1994 SFBC","Not Applicable","Beyond Scope of Inspection","Not Determined"},
arr_RC[]={"-Select-","Simple Roof Shape","Moderate Roof Shape","Complex Roof Shape"},
arr_ARL[]={"-Select-","Not Determined","Roof Aged","1+ Year","2+ Years","3+ Years","4+ Years","5+ Years","6+ Years","7+ Years","8+ Years","9+ Years",">10 Years"};
EditText RCT_other[]=new EditText[5];
EditText RCT_ed[]=new EditText[2];
CheckBox RCT_chk[]=new CheckBox[2];
Button RCT_bt[]=new Button[2];
TextView tv;
int pick_img_code=24,editid=0,mDayofWeek,selmDayofWeek,hours,minutes,seconds,amorpm;
RadioGroup RCN_RG[]=new RadioGroup[14];
EditText RCN_other[]=new EditText[14];
TextView RCN_tv_limit[]=new TextView[14];
EditText RC_cmt;
ImageView ADD_expend,RCN_expend;
CheckBox ADD_chk_top,RCN_chk_top,RCT_chk_top;
RadioGroup R_ADD_WUWC,R_ADD_PDRUC,R_ADD_RME,R_ADD_RMEL,R_ADD_DRMS;
EditText R_ADD_PN,R_ADD_PD,R_ADD_other_I,R_ADD_HVAC_per,R_ADD_VEN_per,R_ADD_STOR_per,R_ADD_HVAC_loc,R_ADD_VEN_loc,R_ADD_STOR_loc,R_ADD_Other_E;
CheckBox R_ADD_HVAC_E ,R_ADD_VEN_E ,R_ADD_STOR_E ,R_ADD_PIP ,R_ADD_clean_E,R_ADD_ANT_P ,R_ADD_LSE ,R_ADD_O_E;
int RCT_total_per=100;
TableLayout RC_Additional,RCN_Condition_N_tbl;
String R_ADD_V_WUWC="",R_ADD_V_PDRUC="" ,R_ADD_V_PN ="",R_ADD_V_PD ="",R_ADD_V_RME="" ,R_ADD_V_HVAC_E="false" ,R_ADD_V_VEN_E="false" ,R_ADD_V_STOR_E="false" ,
R_ADD_V_PIP="" ,R_ADD_V_clean_E="",R_ADD_V_ANT_P ="",R_ADD_V_LSE="" ,R_ADD_V_O_E="" ,R_ADD_V_RMEL="",
R_ADD_V_DRMS ="",R_ADD_V_other_I="";
private boolean load_comment=true;
private boolean RCT_pre_m=false;
private LinearLayout ADD_layout_head;
private LinearLayout RCN_layout_head;
private ProgressDialog pd;
private int show_handler;
CheckBox RCN_chk_FC[]=new CheckBox[4];
EditText Amount;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		cf=new CommonFunctions(this);
		cf.CreateARRTable(431);
		Bundle extras = getIntent().getExtras();
		 if (extras != null) {
		
				cf.selectedhomeid = extras.getString("homeid");
				cf.onlinspectionid = extras.getString("InspectionType");
				
				   
			    cf.onlstatus = extras.getString("status");
			    cf.Roof=extras.getString("Roof");
			    
			    if(cf.Roof.equals(cf.getResourcesvalue(R.string.insp_4)) || cf.Roof.equals(cf.getResourcesvalue(R.string.insp_5)) || cf.Roof.equals(cf.getResourcesvalue(R.string.insp_6)))
			    {
			    	cf.identityval = extras.getInt("identity");
			    }
			    submenu=extras.getString("for_loading");
			    if(submenu==null)
				{	
					submenu="";
				}
			    cf.getPolicyholderInformation(cf.selectedhomeid);
			    setContentView(R.layout.roof_commercial_information);
				cf.getInspectorId();
				cf.getDeviceDimensions();
				/* cf.CreateARRTable(10);
				 cf.CreateARRTable(99);
				 cf.CreateARRTable(431);
				 cf.CreateARRTable(43);
				 cf.CreateARRTable(432);
				 cf.CreateARRTable(433);
				 cf.CreateARRTable(434);*/
				System.out.println("testsssssssssssssssss");
				 custom_creat();
		 }
	}
	public void custom_creat()
			{
		System.out.println("custom_creat");
				 cf.loadinsp_n=cf.Roof;
				 Roof_page_value=getRoofValueForVariables(cf.Roof);
				 if(cf.Roof.equals(cf.Roof_insp[0]))
				 {
					System.out.println("its 11");
						    LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
							hdr_layout.addView(new HdrOnclickListener(this,1,cf.All_insp_list[1],"Roof System",2,0,cf));
				         	LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
				         	main_layout.setMinimumWidth(cf.wd-20);
				         	main_layout.addView(new MyOnclickListener(getApplicationContext(), 2, 0,0,cf));
						    LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
						    submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 21, 1,0,cf));
						    cf.innersubmenu_layout = (LinearLayout) findViewById(R.id.innersubmenu);//** WINDMIT TYPE SUBMENU **//
					          cf.innersubmenu_layout.addView(new MyOnclickListener(this,cf.identityval,1,144, cf));
						    
						    
						    findViewById(R.id.sampleCitizenCertificate).setVisibility(View.VISIBLE);
						    findViewById(R.id.includeCitizenForm).setVisibility(View.VISIBLE);
						    
				}
				 else if(cf.Roof.equals(cf.Roof_insp[1]))
				 {
					 System.out.println("its 22");
					
						    LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
							hdr_layout.addView(new HdrOnclickListener(this,1,cf.All_insp_list[2],"",29,2,cf));
				         	LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
				         	main_layout.setMinimumWidth(cf.wd);
				         	main_layout.addView(new MyOnclickListener(getApplicationContext(), 29, 0,0,cf));
						
				       ((Button) findViewById(R.id.GCH_RC_savebtn)).setText("Save");
				       ((LinearLayout) findViewById(R.id.contentrel)).setBackgroundColor(0);
				       LinearLayout.LayoutParams lp =new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
				       lp.setMargins(0, 0, 0, 0);
				       ((LinearLayout) findViewById(R.id.insidecontentrel)).setLayoutParams(lp);
				       ((LinearLayout) findViewById(R.id.Submenu_layout)).setLayoutParams(lp);
				       ((LinearLayout) findViewById(R.id.Submenu_layout)).setPadding(0, 0, 0,0);
				       ((LinearLayout) findViewById(R.id.typesubmenu)).setPadding(0, 0, 0,0);
					 
				 }
				 else  if(cf.Roof.equals(cf.Roof_insp[4]))
				 {System.out.println("its 33");
						    LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
							hdr_layout.addView(new HdrOnclickListener(this,1,cf.All_insp_list[7],"Roof System",4,0,cf));
				         	LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
				         	main_layout.setMinimumWidth(cf.wd-10);
				         	main_layout.addView(new MyOnclickListener(getApplicationContext(), 4, 0,0,cf));
				         	
					 	    LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
						    submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 43, 1,0,cf));
						    
						    cf.innersubmenu_layout = (LinearLayout) findViewById(R.id.innersubmenu);//** WINDMIT TYPE SUBMENU **//
					          cf.innersubmenu_layout.addView(new MyOnclickListener(this,cf.identityval,1,144, cf));
						    findViewById(R.id.sampleCitizenCertificate).setVisibility(View.VISIBLE);
						    findViewById(R.id.includeCitizenForm).setVisibility(View.VISIBLE);
				/***	 For GCH Ends***/
				 }
				 else if(cf.Roof.equals(cf.getResourcesvalue(R.string.insp_5)))
				 {
					//cf.getPolicyholderInformation(cf.selectedhomeid);
					 System.out.println("its 44");
					if(cf.identityval==33 || cf.identityval==34){ cf.typeidentity=313;}
				          
						    LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
						   hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","Commercial Type II","Roof System",3,1,cf));
					       
					       
				         	LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
				         	main_layout.setMinimumWidth(cf.wd-20);
				         	main_layout.addView(new MyOnclickListener(getApplicationContext(), 3, 0,0,cf));
				         	
				           cf.mainlayout= (LinearLayout) findViewById(R.id.insidecontentrel);
		      	        
					        
							
				          cf.type_submenulayout =(LinearLayout) findViewById(R.id.typesubmenu); //** WINDMIT TYPE SUBMENU **//
				          cf.type_submenulayout.addView(new MyOnclickListener(this, 300, 1,0, cf));
				          cf.windmittypechk(cf.identityval,((RadioButton)findViewById(R.id.b1802_chk)),((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((TextView)findViewById(R.id.commtxt)),((ImageView)findViewById(R.id.editbno))); // To check the checkbox of choosen inspection type
				          cf.uncheckcommercial(((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((RadioButton)findViewById(R.id.b1802_chk))); 
				      	   
				          cf.submenu_layout = (LinearLayout) findViewById(R.id.submenu);//** WINDMIT TYPE SUBMENU **//
				          cf.submenu_layout .addView(new MyOnclickListener(this,cf.identityval,1,cf.typeidentity, cf));
				          
				          
				          cf.innersubmenu_layout = (LinearLayout) findViewById(R.id.innersubmenu);//** WINDMIT TYPE SUBMENU **//
				          cf.innersubmenu_layout.addView(new MyOnclickListener(this,cf.identityval,1,144, cf));
				          
				          LinearLayout.LayoutParams li=(LinearLayout.LayoutParams) findViewById(R.id.GCH_RC_ED_Li_Comments).getLayoutParams();
					       li.width=cf.wd-90;
					       findViewById(R.id.GCH_RC_ED_Li_Comments).setLayoutParams(li);
				        /*  cf.tblrw = (TableRow)findViewById(R.id.row2);
				          cf.tblrw.setMinimumHeight(cf.ht);	*/
				          cf.save = (Button)findViewById(R.id.GCH_RC_savebtn);
					
					 findViewById(R.id.Add_help).setVisibility(View.VISIBLE);
					 findViewById(R.id.RCN_help).setVisibility(View.VISIBLE);
					 /****commercial type 2 ends**/
				 } 
				 else if(cf.Roof.equals(cf.getResourcesvalue(R.string.insp_6)))
				 {
					 /****commercial type 3 satrts**/
					 System.out.println("its 55");
						 if(cf.identityval==33 || cf.identityval==34){ cf.typeidentity=313;}
			          
						    LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
						     hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","Commercial Type III","Roof System",3,1,cf));
					        
							LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
				         	main_layout.setMinimumWidth(cf.wd-20);
				         	main_layout.addView(new MyOnclickListener(getApplicationContext(), 3, 0,0,cf));
				         
				          cf.type_submenulayout =(LinearLayout) findViewById(R.id.typesubmenu); //** WINDMIT TYPE SUBMENU **//
				          cf.type_submenulayout.addView(new MyOnclickListener(this, 300, 1,0, cf));
				          cf.windmittypechk(cf.identityval,((RadioButton)findViewById(R.id.b1802_chk)),((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((TextView)findViewById(R.id.commtxt)),((ImageView)findViewById(R.id.editbno))); // To check the checkbox of choosen inspection type
				          cf.uncheckcommercial(((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((RadioButton)findViewById(R.id.b1802_chk))); 
				      	   
				  		 cf.mainlayout= (LinearLayout) findViewById(R.id.insidecontentrel);
				          System.out.println("testtet");
				          cf.submenu_layout = (LinearLayout) findViewById(R.id.submenu);//** WINDMIT TYPE SUBMENU **//
				          System.out.println("cf.typeidentity"+cf.typeidentity);
				          cf.submenu_layout .addView(new MyOnclickListener(this,cf.identityval,1,cf.typeidentity, cf));
				          System.out.println("submeny"+cf.identityval);
				          cf.innersubmenu_layout = (LinearLayout) findViewById(R.id.innersubmenu);//** WINDMIT TYPE SUBMENU **//
				          cf.innersubmenu_layout.addView(new MyOnclickListener(this,cf.identityval,1,144, cf));
				          
				          LinearLayout.LayoutParams li=(LinearLayout.LayoutParams) findViewById(R.id.GCH_RC_ED_Li_Comments).getLayoutParams();
					       li.width=cf.wd-90;
					       findViewById(R.id.GCH_RC_ED_Li_Comments).setLayoutParams(li);
				         /* cf.tblrw = (TableRow)findViewById(R.id.row2);
				          cf.tblrw.setMinimumHeight(cf.ht);*/
				          cf.save = (Button)findViewById(R.id.GCH_RC_savebtn);
					
					 /****Roof Survay ends**/
					 findViewById(R.id.Add_help).setVisibility(View.VISIBLE);
					 findViewById(R.id.RCN_help).setVisibility(View.VISIBLE);
				 } 
				 else if(cf.Roof.equals(cf.getResourcesvalue(R.string.insp_4)))
				 {System.out.println("its 66");
					
						 System.out.println("cf.identityval"+cf.identityval);
						 if(cf.identityval==32 || cf.identityval==33 || cf.identityval==34){ cf.typeidentity=313;}
			          
						    LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
						    hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","Commercial Type I","Roof System",3,1,cf));
				
							LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
				         	main_layout.setMinimumWidth(cf.wd-20);
				         	main_layout.addView(new MyOnclickListener(getApplicationContext(), 3, 0,0,cf));
				          cf.mainlayout= (LinearLayout) findViewById(R.id.insidecontentrel);
				          
				          cf.type_submenulayout =(LinearLayout) findViewById(R.id.typesubmenu); //** WINDMIT TYPE SUBMENU **//
				          cf.type_submenulayout.addView(new MyOnclickListener(this, 300, 1,0, cf));
				          cf.windmittypechk(cf.identityval,((RadioButton)findViewById(R.id.b1802_chk)),((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((TextView)findViewById(R.id.commtxt)),((ImageView)findViewById(R.id.editbno))); // To check the checkbox of choosen inspection type
				          cf.uncheckcommercial(((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((RadioButton)findViewById(R.id.b1802_chk))); 
				      	  
				          cf.submenu_layout = (LinearLayout) findViewById(R.id.submenu);//** WINDMIT TYPE SUBMENU **//
				          cf.submenu_layout .addView(new MyOnclickListener(this,cf.identityval,1,cf.typeidentity, cf));
				          
				          cf.innersubmenu_layout = (LinearLayout) findViewById(R.id.innersubmenu);//** WINDMIT TYPE SUBMENU **//
				          cf.innersubmenu_layout.addView(new MyOnclickListener(this,cf.identityval,1,144, cf));
				          
				          LinearLayout.LayoutParams li=(LinearLayout.LayoutParams) findViewById(R.id.GCH_RC_ED_Li_Comments).getLayoutParams();
				       li.width=cf.wd-90;
				       findViewById(R.id.GCH_RC_ED_Li_Comments).setLayoutParams(li);
				        /*  cf.tblrw = (TableRow)findViewById(R.id.row2);
				          cf.tblrw.setMinimumHeight(cf.ht);*/
				          cf.save = (Button)findViewById(R.id.GCH_RC_savebtn);
					 
					 /****commercial type 1 ends**/
				 } 	       
					
					LinearLayout Li = (LinearLayout)findViewById(R.id.contentrel);
					Li.setMinimumHeight(cf.ht);
					final Calendar cal = Calendar.getInstance();
					mYear = cal.get(Calendar.YEAR);
					mMonth = cal.get(Calendar.MONTH);
					mDay = cal.get(Calendar.DAY_OF_MONTH);
					this.setTheme(R.style.AppTheme);
					cf.setupUI(((View)findViewById(R.id.contentrel)));
					try
					 {
						cf.CreateARRTable(431);
						 check_for_dublication();
					 }
					 catch (Exception e) {
						// TODO: handle exception
						 System.out.println("error "+e.getMessage());
						 Declaration();
					}
					System.out.println("comes ocrectly1");
			}
	private String[] getRoofValueForVariables(String Roof) {
				// TODO Auto-generated method stub
				String Roof_page_value[]=new String[7];
				cf.loadinsp_o="Overall Roof Commnets";
				Roof_page_value[0]="2";
				 Roof_page_value[1]="1";
				 Roof_page_value[2]="3";
				 
				if(Roof.equals(cf.Roof_insp[0]))
				 {
					/*** For 4poin***/
					 
					 Roof_page_value[3]=cf.RCT_tit;
					 Roof_page_value[4]="Other Damage Noted";
					 Roof_page_value[5]="1";
					 cf.loadinsp_q=cf.getResourcesvalue(R.string.four_1);
					 cf.loadinsp_n=cf.All_insp_list[1];
					 
					 /****For 4poin ends**/
			      }
				 else if(Roof.equals(cf.Roof_insp[1]))
				 {
					/*** Roof Survey***/
					
					 Roof_page_value[3]=cf.RCT_tit;
					 Roof_page_value[4]="Other Damage Noted";
					 Roof_page_value[5]="2";
					 cf.loadinsp_n=cf.All_insp_list[2];
					 cf.loadinsp_q=cf.getResourcesvalue(R.string.roof_1);
					
					 /****Roof Survay ends**/
					 
				 }
				 else  if(Roof.equals(cf.Roof_insp[4]))
				 {
						   
					 /*** For GCH***/
					 Roof_page_value[3]=cf.RCT_tit;
					 Roof_page_value[4]="Other Damage Noted";
					 cf.loadinsp_q=cf.getResourcesvalue(R.string.gch_3);
					 Roof_page_value[5]="7";
					
					// colomprerow=5;
					 cf.loadinsp_n=cf.All_insp_list[7];
					 /***	 For GCH Ends***/
				 }
				 else if(Roof.equals(cf.getResourcesvalue(R.string.insp_5)))
				 {
					
					/*** Roof Survey***/
					 Roof_page_value[3]=cf.RCT_tit;
					 Roof_page_value[4]="Is there any damage to the roof structure (Cuts,Holes,etc)?";
					 cf.loadinsp_q=cf.getResourcesvalue(R.string.com2_2);
					 Roof_page_value[5]="5";
					 
					 cf.loadinsp_n=cf.All_insp_list[5];
					 		 /****Roof Survay ends**/
				 } 
				 else if(Roof.equals(cf.getResourcesvalue(R.string.insp_6)))
				 {
					
					/*** Roof Survey***/
					 Roof_page_value[3]=cf.RCT_tit;
					 Roof_page_value[4]="Is there any damage to the roof structure (Cuts,Holes,etc)?";
					 cf.loadinsp_q=cf.getResourcesvalue(R.string.com2_2);
					 Roof_page_value[5]="6";
					
					 cf.loadinsp_n=cf.All_insp_list[6];
					  /****Roof Survay ends**/
				 }
				 else if (Roof.equals(cf.getResourcesvalue(R.string.insp_4)))
				 {
					
					/*** Roof Survey***/
					 Roof_page_value[3]=cf.RCT_tit;
					 Roof_page_value[4]="Other Damage Noted";//"Other Condition Present";
					 cf.loadinsp_q=cf.getResourcesvalue(R.string.com2_2);
					 Roof_page_value[5]="4";
					 cf.loadinsp_n=cf.All_insp_list[4];
				
					  /****Roof Survay ends**/
				 }
		        else 
				 {
					 /*** For GCH***/
					
					 Roof_page_value[3]=cf.RCT_tit;
					 Roof_page_value[4]="Other Condition Present";
					 cf.loadinsp_q=cf.getResourcesvalue(R.string.gch_3);
					 Roof_page_value[5]="7";
					 
				 }
				return Roof_page_value;
			}	
	private void check_for_dublication() {
		// TODO Auto-generated method stub
		Cursor c =cf.SelectTablefunction(cf.Roof_additional, " WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+Roof_page_value[5]+"'");
		Cursor c1 =cf.SelectTablefunction(cf.Rct_table, " WHERE RCT_Ssrid='"+cf.selectedhomeid+"' and RCT_insp_id='"+Roof_page_value[5]+"'");
		Cursor c2 =cf.SelectTablefunction(cf.RCN_table, " WHERE R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id='"+Roof_page_value[5]+"'");
		if(c.getCount()<=0 && c1.getCount()<=0 && c2.getCount()<=0)
		{
			
			 c =cf.arr_db.rawQuery(" SELECT DISTINCT R_ADD_insp_id FROM "+cf.Roof_additional+" WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id<>'"+Roof_page_value[5]+"'",null);
			 c1 =cf.arr_db.rawQuery(" SELECT DISTINCT RCT_insp_id FROM "+cf.Rct_table+" WHERE RCT_Ssrid='"+cf.selectedhomeid+"' and RCT_insp_id<>'"+Roof_page_value[5]+"'",null);
			 c2 =cf.arr_db.rawQuery(" SELECT DISTINCT R_RCN_insp_id FROM "+cf.RCN_table+" WHERE R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id<>'"+Roof_page_value[5]+"'",null);
		String insp="";
		c.moveToFirst();c1.moveToFirst();c2.moveToFirst();
		for(int i=0;i<c.getCount()||i<c1.getCount()||i<c2.getCount();i++)
		{
			if(i<c.getCount())
			{
				if(!insp.contains(c.getString(c.getColumnIndex("R_ADD_insp_id"))))
					insp+=c.getString(c.getColumnIndex("R_ADD_insp_id"))+",";
				c.moveToNext();
			}
			if(i<c1.getCount())
			{
				if(!insp.contains(c1.getString(c1.getColumnIndex("RCT_insp_id"))))
					insp+=c1.getString(c1.getColumnIndex("RCT_insp_id"))+",";
				c1.moveToNext();
			}
			if(i<c2.getCount())
			{
				if(!insp.contains(c2.getString(c2.getColumnIndex("R_RCN_insp_id"))))
					insp+=c2.getString(c2.getColumnIndex("R_RCN_insp_id"))+",";
				c2.moveToNext();
			}
			
		}
		if(!insp.equals(""))
		{
			if(insp.endsWith(","))
			{
				insp=insp.substring(0,insp.length()-1);
						
			}
			
			Cursor val= cf.SelectTablefunction(cf.inspnamelist, " WHERE ARR_Custom_ID in ("+insp+") order by ARR_Custom_ID");
			val.moveToFirst();
			if(val.getCount()>0)
			{
				String inspection[]=new String[val.getCount()+1];
				inspection[0]="--Select--";
				for(int i=1;i<=val.getCount();i++)
				{
					inspection[i]=cf.decode(val.getString(val.getColumnIndex("ARR_Insp_Name")));
					val.moveToNext();
				}
				final Dialog dialog = new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar);
				dialog.getWindow().setContentView(R.layout.alert);
				ImageView btn_helpclose = (ImageView) dialog.findViewById(R.id.choose_close);
				LinearLayout hide=(LinearLayout) dialog.findViewById(R.id.maintable);
				final LinearLayout show=(LinearLayout) dialog.findViewById(R.id.choose_insp);
				hide.setVisibility(View.GONE);
				show.setVisibility(View.VISIBLE);
				Button btn_save = (Button) dialog.findViewById(R.id.choose_save);
				Button btn_cancel = (Button) dialog.findViewById(R.id.choose_cancel);
				final Spinner sp=(Spinner) dialog.findViewById(R.id.choose_sp);
				ArrayAdapter ad =new ArrayAdapter(this,android.R.layout.simple_spinner_item, inspection);
				ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				sp.setAdapter(ad);
				btn_helpclose.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						Declaration();
							dialog.cancel();
					}
					
				});
				btn_cancel.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						Declaration();
						dialog.cancel();
					}
					
				});
				btn_save.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						if(sp.getSelectedItem().toString().equals("-Select-"))
						{
							cf.ShowToast("Please select the Inspection Type", 0);
						}
						else
						{
							/**Start duplicating ****/
							Cursor val= cf.SelectTablefunction(cf.inspnamelist, " WHERE ARR_Insp_Name ='"+cf.encode(sp.getSelectedItem().toString())+"'");
							String insp_id="";
							if(val.getCount()>0)
							{
								val.moveToFirst();
								insp_id=val.getString(val.getColumnIndex("ARR_Custom_ID"));
							}
							if(val!=null)
							{
								val.close();
							}
							
							val= cf.SelectTablefunction(cf.Roof_additional, " WHERE  R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+insp_id+"' ");
							if(val.getCount()>0)
							{
								cf.arr_db.execSQL(" INSERT INTO "+cf.Roof_additional+" (R_ADD_Ssrid,R_ADD_insp_id,R_ADD_full,R_ADD_scope,R_ADD_na,R_ADD_comment,R_ADD_citizen,R_ADD_WUWC,R_ADD_PDRUC,R_ADD_PN,R_ADD_PD,R_ADD_RME,R_ADD_HVAC_E,R_ADD_VEN_E,R_ADD_STOR_E,R_ADD_PIP,R_ADD_clean_E,R_ADD_ANT_P,R_ADD_LSE,R_ADD_O_E,R_ADD_RMEL,R_ADD_DRMS,R_ADD_other_I,R_RCT_na,R_ADD_saved) " +
										" Select R_ADD_Ssrid,'"+Roof_page_value[5]+"',R_ADD_full,R_ADD_scope,R_ADD_na,R_ADD_comment,R_ADD_citizen,R_ADD_WUWC,R_ADD_PDRUC,R_ADD_PN,R_ADD_PD,R_ADD_RME,R_ADD_HVAC_E,R_ADD_VEN_E,R_ADD_STOR_E,R_ADD_PIP,R_ADD_clean_E,R_ADD_ANT_P,R_ADD_LSE,R_ADD_O_E,R_ADD_RMEL,R_ADD_DRMS,R_ADD_other_I,R_RCT_na,'0' FROM "+cf.Roof_additional+"  WHERE  R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+insp_id+"'" );
								
							}
							
							val= cf.SelectTablefunction(cf.Rct_table, " WHERE  RCT_Ssrid='"+cf.selectedhomeid+"' and RCT_insp_id='"+insp_id+"' ");
							if(val.getCount()>0)
							{
								/*cf.arr_db.execSQL(" INSERT INTO "+cf.Rct_table+" (RCT_Ssrid,RCT_insp_id,RCT_rct,RCT_PAD,RCT_yoi,RCT_npv,RCT_rsl,RCT_rsh,RCT_rst,RCT_bc,RCT_rc,RCT_arfl,RCT_pre,RCT_Rsh_per,RCT_other_rct,RCT_other_yoi,RCT_other_rsh,RCT_other_rst,RCT_other_aRFL,RCT_saved) " +
										" Select RCT_Ssrid,'"+Roof_page_value[5]+"',RCT_rct,RCT_PAD,RCT_yoi,RCT_npv,RCT_rsl,RCT_rsh,RCT_rst,RCT_bc,RCT_rc,RCT_arfl,RCT_pre,RCT_Rsh_per,RCT_other_rct,RCT_other_yoi,RCT_other_rsh,RCT_other_rst,RCT_other_aRFL,'0' FROM "+cf.Rct_table+"  WHERE  RCT_Ssrid='"+cf.selectedhomeid+"' and RCT_insp_id='"+insp_id+"'" );
								val.moveToFirst();*/
								cf.arr_db.execSQL(" INSERT INTO "+cf.Rct_table+" (RCT_Ssrid,RCT_insp_id,RCT_rct,RCT_PAD,RCT_yoi,RCT_npv,RCT_rsl,RCT_rsh,RCT_rst,RCT_bc,RCT_rc,RCT_arfl,RCT_pre,RCT_Rsh_per,RCT_other_rct,RCT_other_yoi,RCT_other_rsh,RCT_other_rst,RCT_other_aRFL,RCT_saved) " +
										" Select RCT_Ssrid,'"+Roof_page_value[5]+"',RCT_rct,RCT_PAD,RCT_yoi,RCT_npv,RCT_rsl,RCT_rsh,RCT_rst,RCT_bc,RCT_rc,RCT_arfl,RCT_pre,RCT_Rsh_per,RCT_other_rct,RCT_other_yoi,RCT_other_rsh,RCT_other_rst,RCT_other_aRFL,'0' FROM "+cf.Rct_table+"  WHERE  RCT_Ssrid='"+cf.selectedhomeid+"' and RCT_insp_id='"+insp_id+"'" );
								val.moveToFirst();
								for(int i=0;i<val.getCount();i++,val.moveToNext())
								{
									Cursor master=cf.SelectTablefunction(cf.Roof_master, " Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+Roof_page_value[5]+"' and RM_Covering='"+val.getString(val.getColumnIndex("RCT_rct"))+"' and RM_module_id='0'");
									if(master.getCount()<=0)
									{
										
										cf.arr_db.execSQL("INSERT INTO "+cf.Roof_master+" (RM_inspectorid,RM_srid,RM_insp_id,RM_module_id,RM_Covering) Values ('"+cf.Insp_id+"','"+cf.selectedhomeid+"','"+Roof_page_value[5]+"','0','"+val.getString(val.getColumnIndex("RCT_rct"))+"')");
										//System.out.println(" master sql=INSERT INTO "+cf.Roof_master+" (RM_inspectorid,RM_srid,RM_insp_id,RM_module_id,RM_Covering) Values ('"+cf.Insp_id+"','"+cf.selectedhomeid+"','"+cf.encode(insp)+"','0','"+cf.encode(cover)+"')");
									}
									cf.arr_db.execSQL("INSERT INTO "+cf.Roofcoverimages+" (RIM_masterid,RIM_module_id,RIM_Path,RIM_Order,RIM_Caption) SELECT (Select RM_masterid  as RIM_masterid from "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(Roof_page_value[5])+"' and RM_Enable='1' and RM_Covering='"+val.getString(val.getColumnIndex("RCT_rct"))+"' and RM_module_id='0'),RIM_module_id,RIM_Path,RIM_Order,RIM_Caption  From "+cf.Roofcoverimages+" " +
										"WHERE (RIM_masterid=(Select RM_masterid from "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(insp_id)+"' and RM_Enable='1' and RM_Covering='"+val.getString(val.getColumnIndex("RCT_rct"))+"' and RM_module_id='0' )) ");
								}
							}
							val= cf.SelectTablefunction(cf.RCN_table, " WHERE  R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id='"+insp_id+"' ");
							if(val.getCount()>0)
							{
								cf.arr_db.execSQL(" INSERT INTO "+cf.RCN_table+" (R_RCN_Ssrid,R_RCN_insp_id,R_RCN_OCR,R_RCN_RRN,R_RCN_VSL,R_RCN_VSTDS,R_RCN_VSCBS,R_RCN_VSDTS,R_RCN_VDRS,R_RCN_ODN,R_RCN_HRMS,R_RCN_FC,R_RCN_GRAC,R_RCN_RLN,R_RCN_RPN,R_RCN_HREBR,R_RCN_WORR,R_RCN_RMRA,R_RCN_SFPE,R_RCN_Amount,R_RCN_EVPWD,R_RCN_RME,R_RCN_BFP,R_RCN_ESM,R_RCN_SOSAS,R_RCN_DRS,R_RCN_na,R_RCN_saved) " +
										" Select R_RCN_Ssrid,'"+Roof_page_value[5]+"',R_RCN_OCR,R_RCN_RRN,R_RCN_VSL,R_RCN_VSTDS,R_RCN_VSCBS,R_RCN_VSDTS,R_RCN_VDRS,R_RCN_ODN,R_RCN_HRMS,R_RCN_FC,R_RCN_GRAC,R_RCN_RLN,R_RCN_RPN,R_RCN_HREBR,R_RCN_WORR,R_RCN_RMRA,R_RCN_SFPE,R_RCN_Amount,R_RCN_EVPWD,R_RCN_RME,R_RCN_BFP,R_RCN_ESM,R_RCN_SOSAS,R_RCN_DRS,R_RCN_na,'0' FROM "+cf.RCN_table+"  WHERE  R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id='"+insp_id+"'   " );
							}
							
							/*val= cf.SelectTablefunction(cf.RCN_table_dy, " WHERE  R_RCN_D_Ssrid='"+cf.selectedhomeid+"' and R_RCN_D_insp_id in ("+insp_id+") ");
							if(val.getCount()>0)
							{
								val.moveToFirst();
								for(int i=0;i<val.getCount();i++,val.moveToNext())
								{
									cf.arr_db.execSQL(" INSERT INTO "+cf.RCN_table_dy+" (R_RCN_D_Ssrid,R_RCN_D_insp_id,R_RCN_D_title,R_RCN_D_option,R_RCN_D_Other,R_RCN_D_saved) " +
										" Select R_RCN_D_Ssrid,'"+Roof_page_value[5]+"',R_RCN_D_title,R_RCN_D_option,R_RCN_D_Other,R_RCN_D_saved FROM "+cf.RCN_table_dy+"  WHERE  id='"+val.getString(val.getColumnIndex("id"))+"' " );
								}
							}*/
							
							if(val!=null)
							{
								val.close();
							}
							Declaration();
							/**End duplicating ****/
							dialog.dismiss();
						}
					}
					
				});
				
				dialog.setCancelable(false);
				dialog.show();
			}
			else
			{
				Declaration();
			}
			if(val!=null)
			{
				val.close();
			}
			
		}
		else
		{
			Declaration();
		}
			//inspection
			
			
			 
		}
		else
		{
			Declaration();
		}
		if(c!=null)
		{
			c.close();
		}
		if(c1!=null)
		{
			c1.close();
		}
		if(c2!=null)
		{
			c2.close();
		}
	}
	private void Declaration()
	{
		arr_YOI=cf.yearbuilt;
		
		
		/**RCT Declaration**/
		Sp_RCT[0]=(Spinner) findViewById(R.id.Roof_RCT_sp_RCT);
		Sp_RCT[1]=(Spinner) findViewById(R.id.Roof_RCT_sp_YOI);
		Sp_RCT[2]=(Spinner) findViewById(R.id.Roof_RCT_sp_RS);
		Sp_RCT[3]=(Spinner) findViewById(R.id.Roof_RCT_sp_RSH);
		Sp_RCT[4]=(Spinner) findViewById(R.id.Roof_RCT_sp_RST);
		Sp_RCT[5]=(Spinner) findViewById(R.id.Roof_RCT_sp_BC);
		Sp_RCT[6]=(Spinner) findViewById(R.id.Roof_RCT_sp_RC);
		Sp_RCT[7]=(Spinner) findViewById(R.id.Roof_RCT_sp_ARL);
		set_value_spinner(Sp_RCT[0],arr_RCT);
		set_value_spinner(Sp_RCT[1],arr_YOI);
		set_value_spinner(Sp_RCT[2],arr_RS);
		set_value_spinner(Sp_RCT[3],arr_RSH);
		set_value_spinner(Sp_RCT[4],arr_RST);
		set_value_spinner(Sp_RCT[5],arr_BC);
		set_value_spinner(Sp_RCT[6],arr_RC);
		set_value_spinner(Sp_RCT[7],arr_ARL);
		RCT_other[0]=(EditText) findViewById(R.id.Roof_RCT_other_RCT);
		RCT_other[1]=(EditText) findViewById(R.id.Roof_RCT_other_YOI);
		RCT_other[2]=(EditText) findViewById(R.id.Roof_RCT_other_RSH);
		RCT_other[3]=(EditText) findViewById(R.id.Roof_RCT_other_RST);
		RCT_other[4]=(EditText) findViewById(R.id.Roof_RCT_other_ARL);
		RCT_ed[0]=(EditText) findViewById(R.id.Roof_RCT_ED_PAD);
		RCT_ed[1]=(EditText) findViewById(R.id.Roof_RCT_ED_RSH);
		RCT_chk[0]=(CheckBox) findViewById(R.id.Roof_RCT_CHK_PVC);
		RCT_chk[1]=(CheckBox) findViewById(R.id.Roof_RCT_CHK_PRE);
		RCT_bt[0]=(Button) findViewById(R.id.Roof_RCT_BT_PAD);
		RCT_bt[1]=(Button) findViewById(R.id.Roof_RCT_BT_upload);
		tv=(TextView) findViewById(R.id.Roof_RCT_TV_upload);
		
		Sp_RCT[0].setOnItemSelectedListener(new spiner_lis(Sp_RCT[0],RCT_other[0]));
		Sp_RCT[1].setOnItemSelectedListener(new spiner_lis(Sp_RCT[1],RCT_other[1]));
		
	
		
		Sp_RCT[2].setOnItemSelectedListener(new Spin_Selectedlistener());
		
		Sp_RCT[3].setOnItemSelectedListener(new spiner_lis(Sp_RCT[3],RCT_other[2],RCT_ed[1]));
		Sp_RCT[4].setOnItemSelectedListener(new spiner_lis(Sp_RCT[4],RCT_other[3]));
		Sp_RCT[7].setOnItemSelectedListener(new spiner_lis(Sp_RCT[7],RCT_other[4]));
		tv.setText(Html.fromHtml("<u>View/Edit Photos</u>"));
		
	/*	RCT_ed[0].addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(!s.toString().equals("") && !s.toString().equals("N/A"))
				{
					String[] val=s.toString().split("/");
					if(val[2]!=null)
					{
						sp_setvalues(Sp_RCT[1],val[2],RCT_other[1]);
						//Sp_RCT[1].setEnabled(false);
						RCT_other[1].setEnabled(false);
						
					}
				}
				else
				{
					Sp_RCT[1].setEnabled(true);
					RCT_other[1].setEnabled(true);
				}
			}
		});*/
		RCT_ed[0].addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(!s.toString().equals("") && !s.toString().equals("N/A"))
				{
					if(compare_schedule(s.toString()))
					{
					String[] val=s.toString().split("/");
					if(val[2]!=null)
					{
						sp_setvalues(Sp_RCT[1],val[2],RCT_other[1]);
						//Sp_RCT[1].setEnabled(false);
						RCT_other[1].setEnabled(false);
					}
					}
					else
					{
						RCT_ed[0].setText("");
						cf.ShowToast("Permit application date should not be greater than Inspection date", 1);
					}
				}
				else
				{
					Sp_RCT[1].setEnabled(true);
					RCT_other[1].setEnabled(true);
				}
			}

			private boolean compare_schedule(String s) {
				// TODO Auto-generated method stub
				try
				{
				cf.CreateARRTable(3);
				Cursor c =cf.SelectTablefunction(cf.policyholder, " WHERE ARR_PH_SRID='"+cf.selectedhomeid+"'");
				
				if(c.getCount()>0)
				{
				c.moveToFirst();
					String sch=cf.decode(c.getString(c.getColumnIndex("ARR_Schedule_ScheduledDate")));
					if(!sch.equals(""))
					{
						String formatString = "MM/dd/yyyy";
						SimpleDateFormat df = new SimpleDateFormat(formatString);
						//df.parse(sch);//
						Date d=new Date(sch);
						Date sel=new Date(s);
						System.out.println("the scheduyle="+sel);
						System.out.println(d.compareTo(sel));
						if(d.compareTo(sel)>=0)
						{
							return true;
						}
						else
							return false;
					}else
						return true;
					
				}
				else
					return true;
				//return false;
				}catch (Exception e) {
					// TODO: handle exception
					System.out.println("theissue"+e.getMessage());
					return false;
							
				}
			}
		});
		RCT_chk[0].setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(RCT_chk[0].isChecked())
				{
					RCT_ed[0].setText("N/A");
					//RCT_bt[0].setVisibility(View.INVISIBLE);
					//sp_setvalues(Sp_RCT[1],"Unknown",RCT_other[1]);
				}
				else
				{
					RCT_ed[0].setText("");
					//RCT_bt[0].setVisibility(View.VISIBLE);
					//Sp_RCT[1].setSelection(0);
				}
			}
		});
		RCT_chk[1].setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(RCT_chk[1].isChecked())
				{
					if(RCT_pre_m)
					{
						final AlertDialog.Builder ab =new AlertDialog.Builder(Roof_commercial_information.this);
						ab.setTitle("Add Predominant");
						ab.setMessage("Already selected in a Roof Covering Type. Do you want to change this?");
						ab.setIcon(R.drawable.alertmsg);
						ab.setCancelable(false);
						ab.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								ab.setCancelable(true);
								dialog.dismiss();
							}
						})
						.setNegativeButton("No", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								RCT_chk[1].setChecked(false);
								ab.setCancelable(true);
								dialog.dismiss();
								
							}
						});
						AlertDialog al=ab.create();
						al.show();
					}
				}
				
			}
		});
		RCT_bt[0].setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				RCT_ed[0].setText("");
				showDialogDate(RCT_ed[0]);
			}
		});
		RCT_bt[1].setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent in=new Intent(Roof_commercial_information.this,RoofImagePicking.class);
			    in.putExtra("roof_cover_img", true);
			    String cv="";
			    try
			    {
			    	cv=Sp_RCT[0].getSelectedItem().toString().trim();
			    
			   
			    if(!cv.equals("-Select-"))
			    {
				    in.putExtra("Cover", cv);
				    in.putExtra("Insp_id",Roof_page_value[5]);
				    in.putExtra("SRID",cf.selectedhomeid);
				    startActivityForResult(in, pick_img_code);
			    }
			    else
			    {
			    	cf.ShowToast("Please select Roof Covering Type ", 0);
			    }
			    }
			    catch (Exception e) {
					// TODO: handle exception
				}
			}
			
		});
		tv.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent in=new Intent(Roof_commercial_information.this,RoofImagePicking.class);
			    in.putExtra("roof_cover_img", true);
			    String cv="";
			    try
			    {
			    	cv=Sp_RCT[0].getSelectedItem().toString().trim();
			    
			   
			    if(!cv.equals("-Select-"))
			    {
				    in.putExtra("Cover", cv);
				    in.putExtra("Insp_id",Roof_page_value[5]);
				    in.putExtra("SRID",cf.selectedhomeid);
				    startActivityForResult(in, pick_img_code);
			    }
			    else
			    {
			    	cf.ShowToast("Please select Roof Covering Type ", 0);
			    }
			    }
			    catch (Exception e) {
					// TODO: handle exception
				}
			}
		});
		/**RCT Declaration ends**/
		
		/****Additional roof section***/
		
		R_ADD_WUWC=(RadioGroup) findViewById(R.id.ADD_RG_WUWC);
		R_ADD_PDRUC=(RadioGroup) findViewById(R.id.ADD_RG_PDRUC);
		R_ADD_RME=(RadioGroup) findViewById(R.id.ADD_RG_RME);
		R_ADD_RMEL=(RadioGroup) findViewById(R.id.ADD_RG_RMEL);
		R_ADD_DRMS=(RadioGroup) findViewById(R.id.ADD_RG_DRMS);
		R_ADD_PN=(EditText) findViewById(R.id.ADD_ED_PN);
		R_ADD_PD=(EditText) findViewById(R.id.ADD_ED_PD);
		R_ADD_other_I=(EditText) findViewById(R.id.ADD_ED_Other);
		R_ADD_HVAC_per=(EditText) findViewById(R.id.ADD_ED_HVAC_E_per);
		R_ADD_VEN_per=(EditText) findViewById(R.id.ADD_ED_VEN_E_per);
		R_ADD_STOR_per=(EditText) findViewById(R.id.ADD_ED_STOR_F_per);
		R_ADD_HVAC_loc=(EditText) findViewById(R.id.ADD_ED_HVAC_E_Loc);
		R_ADD_VEN_loc=(EditText) findViewById(R.id.ADD_ED_VEN_E_Loc);
		R_ADD_STOR_loc=(EditText) findViewById(R.id.ADD_ED_STOR_F_Loc);
		R_ADD_Other_E=(EditText) findViewById(R.id.ADD_ED_O_E_other);
		R_ADD_HVAC_E=(CheckBox) findViewById(R.id.ADD_CHK_HVAC_E);
		R_ADD_VEN_E =(CheckBox) findViewById(R.id.ADD_CHK_VEN_E);
		R_ADD_STOR_E =(CheckBox) findViewById(R.id.ADD_CHK_STOR_F);
		R_ADD_PIP =(CheckBox) findViewById(R.id.ADD_CHK_PIP);
		R_ADD_clean_E=(CheckBox) findViewById(R.id.ADD_CHK_clean_E);
		R_ADD_ANT_P =(CheckBox) findViewById(R.id.ADD_CHK_ANT_P);
		R_ADD_LSE =(CheckBox) findViewById(R.id.ADD_CHK_LSE);
		R_ADD_O_E=(CheckBox) findViewById(R.id.ADD_CHK_OE);
		R_ADD_other_I.addTextChangedListener(new TextWatcher() {

			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				cf.showing_limit(s.toString(), ((TextView)findViewById(R.id.ADD_other_limit)), 250);
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		((Button) findViewById(R.id.ADD_BT_PD)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				R_ADD_PD.setText("");
				cf.showDialogDate(R_ADD_PD);
			}
		});
		RC_Additional=(TableLayout) findViewById(R.id.RC_Additional);
		R_ADD_PDRUC.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				try{
				if(group.findViewById(checkedId).getTag().equals("Yes"))
				{
					RC_Additional.findViewWithTag("pd_no").setVisibility(View.VISIBLE);
					RC_Additional.findViewWithTag("pd_date").setVisibility(View.VISIBLE);
					try
					{
						Cursor c =cf.SelectTablefunction(cf.Rct_table, " WHERE RCT_Ssrid='"+cf.selectedhomeid+"' and RCT_insp_id='"+Roof_page_value[5]+"' and RCT_pre='true'");
						System.out.println( " WHERE RCT_Ssrid='"+cf.selectedhomeid+"' and RCT_insp_id='"+Roof_page_value[5]+"' and RCT_pre='true'"+"COut="+c.getCount());
						if(c.getCount()>0)
						{
							c.moveToFirst();
							String permitdate = cf.decode(c.getString(c.getColumnIndex("RCT_PAD")));
							System.out.println("premidtae="+permitdate);
							if(!permitdate.equals(""))
							{
								R_ADD_PD.setText(permitdate);
							}
						}
					}
					catch (Exception e) {
						// TODO: handle exception
						System.out.println("EEE="+e.getMessage());
					}
				}
				else
				{
					RC_Additional.findViewWithTag("pd_no").setVisibility(View.GONE);
					RC_Additional.findViewWithTag("pd_date").setVisibility(View.GONE);
					R_ADD_PN.setText("");
					R_ADD_PD.setText("");
				}
				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}
		});
		R_ADD_RME.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				try{
				if(group.findViewById(checkedId).getTag().equals("Yes"))
				{
					RC_Additional.findViewWithTag("RMS").setVisibility(View.VISIBLE);
					RC_Additional.findViewWithTag("RMS1").setVisibility(View.VISIBLE);
					RC_Additional.findViewWithTag("RMS2").setVisibility(View.VISIBLE);
					
					
					((RadioButton) R_ADD_RMEL.findViewWithTag("Yes")).setChecked(true);
					((RadioButton) R_ADD_DRMS.findViewWithTag("Yes")).setChecked(true);
					
				}
				else
				{
					RC_Additional.findViewWithTag("RMS").setVisibility(View.GONE);
					R_ADD_HVAC_E.setChecked(false);R_ADD_VEN_E.setChecked(false);R_ADD_STOR_E.setChecked(false);R_ADD_PIP.setChecked(false);
					R_ADD_clean_E.setChecked(false);R_ADD_ANT_P.setChecked(false);R_ADD_LSE.setChecked(false);R_ADD_O_E.setChecked(false);
					R_ADD_HVAC_per.setText("");R_ADD_VEN_per.setText("");R_ADD_STOR_per.setText("");
					R_ADD_HVAC_loc.setText("");R_ADD_VEN_loc.setText("");R_ADD_STOR_loc.setText("");
					R_ADD_HVAC_per.setEnabled(false);R_ADD_VEN_per.setEnabled(false);R_ADD_STOR_per.setEnabled(false);
					R_ADD_HVAC_loc.setEnabled(false);R_ADD_VEN_loc.setEnabled(false);R_ADD_STOR_loc.setEnabled(false);
					R_ADD_Other_E.setText("");
					R_ADD_Other_E.setVisibility(View.GONE);
					
					RC_Additional.findViewWithTag("RMS1").setVisibility(View.GONE);
					R_ADD_RMEL.clearCheck();
					
					RC_Additional.findViewWithTag("RMS2").setVisibility(View.GONE);
					R_ADD_DRMS.clearCheck();
				}
				}catch (Exception e) {
					// TODO: handle exception
				}
				
			}
		});
		R_ADD_HVAC_E.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(((CheckBox) v).isChecked())
				{
					R_ADD_HVAC_per.setEnabled(true);
					R_ADD_HVAC_loc.setEnabled(true);
					R_ADD_HVAC_loc.setText("Main Roof");
					
				}
				else
				{
					R_ADD_HVAC_per.setText("");
					R_ADD_HVAC_loc.setText("");
					R_ADD_HVAC_per.setEnabled(false);
					R_ADD_HVAC_loc.setEnabled(false);
				}
			}
		});
		R_ADD_VEN_E.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(((CheckBox) v).isChecked())
				{
					R_ADD_VEN_per.setEnabled(true);
					R_ADD_VEN_loc.setEnabled(true);
					R_ADD_VEN_loc.setText("Main Roof");
					
				}
				else
				{
					R_ADD_VEN_per.setText("");
					R_ADD_VEN_loc.setText("");
					R_ADD_VEN_per.setEnabled(false);
					R_ADD_VEN_loc.setEnabled(false);
				}
			}
		});
		R_ADD_STOR_E.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(((CheckBox) v).isChecked())
				{
					R_ADD_STOR_per.setEnabled(true);
					R_ADD_STOR_loc.setEnabled(true);
					R_ADD_STOR_loc.setText("Main Roof");

				}
				else
				{
					R_ADD_STOR_per.setText("");
					R_ADD_STOR_loc.setText("");
					R_ADD_STOR_per.setEnabled(false);
					R_ADD_STOR_loc.setEnabled(false);
				}
			}
		});
		R_ADD_O_E.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(((CheckBox) v).isChecked())
				{
					R_ADD_Other_E.setVisibility(View.VISIBLE);
				}
				else
				{
					R_ADD_Other_E.setText("");
					R_ADD_Other_E.setVisibility(View.GONE);
				}
			}
		});
		/***Additional roof section ends***/
		
		/***Roof condition noted**/
		
		
		RCN_RG[0]=(RadioGroup) findViewById(R.id.Roof_RCN_RG_GRAC);
		RCN_RG[1]=(RadioGroup) findViewById(R.id.Roof_RCN_RG_RLN);
		RCN_RG[2]=(RadioGroup) findViewById(R.id.Roof_RCN_RG_RPN);
		RCN_RG[3]=(RadioGroup) findViewById(R.id.Roof_RCN_RG_HREBR);
		RCN_RG[4]=(RadioGroup) findViewById(R.id.Roof_RCN_RG_WORR);
		RCN_RG[5]=(RadioGroup) findViewById(R.id.Roof_RCN_RG_RMRA);
		RCN_RG[6]=(RadioGroup) findViewById(R.id.Roof_RCN_RG_SFPE);
		RCN_RG[7]=(RadioGroup) findViewById(R.id.Roof_RCN_RG_EVPWD);
		RCN_RG[8]=(RadioGroup) findViewById(R.id.Roof_RCN_RG_RME);
		RCN_RG[9]=(RadioGroup) findViewById(R.id.Roof_RCN_RG_BFP);
		RCN_RG[10]=(RadioGroup) findViewById(R.id.Roof_RCN_RG_ESM);
		RCN_RG[11]=(RadioGroup) findViewById(R.id.Roof_RCN_RG_SOSAS);
		RCN_RG[12]=(RadioGroup) findViewById(R.id.Roof_RCN_RG_DRS);
		RCN_RG[13]=(RadioGroup) findViewById(R.id.Roof_RCN_RG_HRMS);
		
		
		RCN_chk_FC[0]=(CheckBox) findViewById(R.id.Roof_RCN_CHK_FC1);
		RCN_chk_FC[1]=(CheckBox) findViewById(R.id.Roof_RCN_CHK_FC2);
		RCN_chk_FC[2]=(CheckBox) findViewById(R.id.Roof_RCN_CHK_FC3);
		RCN_chk_FC[3]=(CheckBox) findViewById(R.id.Roof_RCN_CHK_FC4);
		Amount =(EditText) findViewById(R.id.Roof_RCN_ED_Amount);
		
		
		
		RCN_other[13]=(EditText) findViewById(R.id.Roof_RCN_ED_HRMS);
		RCN_other[0]=(EditText) findViewById(R.id.Roof_RCN_ED_GRAC);
		RCN_other[1]=(EditText) findViewById(R.id.Roof_RCN_ED_RLN);
		RCN_other[2]=(EditText) findViewById(R.id.Roof_RCN_ED_RPN);
		RCN_other[3]=(EditText) findViewById(R.id.Roof_RCN_ED_HRER);
		RCN_other[4]=(EditText) findViewById(R.id.Roof_RCN_ED_WORR);
		RCN_other[5]=(EditText) findViewById(R.id.Roof_RCN_ED_RMRA);
		RCN_other[6]=(EditText) findViewById(R.id.Roof_RCN_ED_SFPE);
		RCN_other[7]=(EditText) findViewById(R.id.Roof_RCN_ED_EVPWD);
		//RCN_other[8]=(EditText) findViewById(R.id.Roof_RCN_ED_RME);
		RCN_other[9]=(EditText) findViewById(R.id.Roof_RCN_ED_BFP);
		RCN_other[10]=(EditText) findViewById(R.id.Roof_RCN_ED_ESM);
		RCN_other[11]=(EditText) findViewById(R.id.Roof_RCN_ED_SOSAS);
		RCN_other[12]=(EditText) findViewById(R.id.Roof_RCN_ED_DRS);
		
		RCN_tv_limit[13]=(TextView) findViewById(R.id.Roof_RCN_TVL_HRMS);
		RCN_tv_limit[0]=(TextView) findViewById(R.id.Roof_RCN_TVL_GRAC);
		RCN_tv_limit[1]=(TextView) findViewById(R.id.Roof_RCN_TVL_RLN);
		RCN_tv_limit[2]=(TextView) findViewById(R.id.Roof_RCN_TVL_RPN);
		RCN_tv_limit[3]=(TextView) findViewById(R.id.Roof_RCN_TVL_HRER);
		RCN_tv_limit[4]=(TextView) findViewById(R.id.Roof_RCN_TVL_WORR);
		RCN_tv_limit[5]=(TextView) findViewById(R.id.Roof_RCN_TVL_RMRA);
		RCN_tv_limit[6]=(TextView) findViewById(R.id.Roof_RCN_TVL_SFPE);
		RCN_tv_limit[7]=(TextView) findViewById(R.id.Roof_RCN_TVL_EVPWD);
		//RCN_tv_limit[8]=(TextView) findViewById(R.id.Roof_RCN_TVL_RME);
		RCN_tv_limit[9]=(TextView) findViewById(R.id.Roof_RCN_TVL_BFP);
		RCN_tv_limit[10]=(TextView) findViewById(R.id.Roof_RCN_TVL_ESM);
		RCN_tv_limit[11]=(TextView) findViewById(R.id.Roof_RCN_TVL_SOSAS);
		RCN_tv_limit[12]=(TextView) findViewById(R.id.Roof_RCN_TVL_DRS);
		RCN_Condition_N_tbl=(TableLayout)findViewById(R.id.RCN_Condition_N_tbl);
		for (int i=0;i<RCN_RG.length;i++){
			if(i==13)
			{
				RCN_RG[i].setOnCheckedChangeListener(new RCN_click(RCN_other[i],RCN_tv_limit[i],"No",RCN_Condition_N_tbl.findViewWithTag("HRMS")));	
			}
			else if(i==2)
			{
				
				RCN_RG[i].setOnCheckedChangeListener(new RCN_click(RCN_other[i],RCN_tv_limit[i],"Yes-BVCN",RCN_Condition_N_tbl.findViewWithTag("RPN")));
			}
			else if(i==4)
			{
				RCN_RG[i].setOnCheckedChangeListener(new RCN_click(RCN_other[i],RCN_tv_limit[i],"Yes-BVCN"));
			}
			else if(i==6)
			{
				RCN_RG[i].setOnCheckedChangeListener(new RCN_click(RCN_other[i],RCN_tv_limit[i],"Yes",RCN_Condition_N_tbl.findViewWithTag("SFPE")));
			}
			else if(i==0)
			{
				RCN_RG[i].setOnCheckedChangeListener(new RCN_click(RCN_other[i],RCN_tv_limit[i],"No"));
			}
			else
			{
				System.out.println("what is i="+i);
				RCN_RG[i].setOnCheckedChangeListener(new RCN_click(RCN_other[i],RCN_tv_limit[i],"Yes"));
			}
		}
		ADD_chk_top =(CheckBox) findViewById(R.id.Add_chk_top);
		 ADD_layout_head =(LinearLayout) findViewById(R.id.ADD_layou_head);
		ADD_expend=(ImageView) findViewById(R.id.ADD_expend);
		RCN_chk_top =(CheckBox) findViewById(R.id.RCN_chk_top);
		RCN_layout_head =(LinearLayout) findViewById(R.id.RCN_layou_head);
	 	ADD_expend.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String tag=v.getTag().toString();
				if(tag.equals("plus"))
				{
					if(!ADD_chk_top.isChecked())
					{
						ADD_layout_head.setVisibility(View.VISIBLE);
						ADD_expend.setTag("minus");
						ADD_expend.setImageDrawable(getResources().getDrawable(R.drawable.minus));
						if(findViewById(R.id.RCN_comp).getVisibility()!=View.VISIBLE)
						{
							RCN_expend.setTag("plus");
							RCN_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
							RCN_layout_head.setVisibility(View.GONE);	
						}
						
					}
				}
				else
				{
					ADD_layout_head.setVisibility(View.GONE);
					ADD_expend.setTag("plus");
					ADD_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
				}
			}
		});
	 	RCN_expend=(ImageView) findViewById(R.id.RCN_expend);
	 	RCN_expend.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String tag=v.getTag().toString();
				if(tag.equals("plus"))
				{
					if(!RCN_chk_top.isChecked())
					{
						RCN_layout_head.setVisibility(View.VISIBLE);
						RCN_expend.setTag("minus");
						RCN_expend.setImageDrawable(getResources().getDrawable(R.drawable.minus));
						if(findViewById(R.id.ADD_comp).getVisibility()!=View.VISIBLE)
						{
							ADD_expend.setTag("plus");
							ADD_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
							ADD_layout_head.setVisibility(View.GONE);	
						}
					}
				}
				else
				{
					RCN_layout_head.setVisibility(View.GONE);
					RCN_expend.setTag("plus");
					RCN_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
				}
			}
		});
	 	
	 	
	 	ADD_chk_top.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				if(ADD_chk_top.isChecked())
				{
				
					Cursor c =cf.SelectTablefunction(cf.Roof_additional, " WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+Roof_page_value[5]+"'");
					c.moveToFirst();
					if(c.getCount()>0)
					{
						if(!c.getString(c.getColumnIndex("R_ADD_WUWC")).equals(""))
						{
						
						final AlertDialog.Builder ab =new AlertDialog.Builder(Roof_commercial_information.this);
						ab.setTitle("Confirmation");
						ab.setMessage(Html.fromHtml("Do you want to clear the Additional Roof Details saved data?"));
						ab.setIcon(R.drawable.alertmsg);
						ab.setCancelable(false);
						ab.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								cf.arr_db.execSQL(" UPDATE "+cf.Roof_additional+" SET " +
										"R_ADD_WUWC ='',R_ADD_PDRUC ='',R_ADD_PN ='',R_ADD_PD ='', R_ADD_RME ='',R_ADD_HVAC_E ='',R_ADD_VEN_E ='',R_ADD_STOR_E ='',R_ADD_PIP ='',R_ADD_clean_E='',R_ADD_ANT_P ='',R_ADD_LSE ='',R_ADD_O_E='',R_ADD_RMEL ='',R_ADD_DRMS ='',R_ADD_other_I=''" +
										" WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+Roof_page_value[5]+"'");
								
								/*cf.ShowToast("Roof Condition Noted question deleted successfully ", 0);
								TableLayout tbl_dy=((TableLayout)findViewById(R.id.RCN_Condition_N_DY_tbl));
								tbl_dy.removeView(tbl_dy.findViewById(id));*/
								clear_ADD();
								ADD_expend.setTag("plus");
		         				ADD_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
								ADD_layout_head.setVisibility(View.GONE);
								
								//Show_dynamic_RCN();
							}
							
						})
						.setNegativeButton("No", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								ADD_chk_top.setChecked(false);
							}
						});
						AlertDialog al=ab.create();
						al.show();
						}
						else
						{	

							clear_ADD();
							ADD_expend.setTag("plus");
	         				ADD_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
							ADD_layout_head.setVisibility(View.GONE);
						}
					}
					else
					{	

						clear_ADD();
						ADD_expend.setTag("plus");
         				ADD_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
						ADD_layout_head.setVisibility(View.GONE);
					}
					if(c!=null)
					{
						c.close();
					}
					
					if(findViewById(R.id.RCN_comp).getVisibility()!=View.VISIBLE)
					{
						clear_ADD();
						RCN_layout_head.setVisibility(View.GONE);
						RCN_expend.setTag("plus");
         				RCN_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
					}
					
				}
				else
				{
					
					ADD_expend.setTag("minus");
     				ADD_expend.setImageDrawable(getResources().getDrawable(R.drawable.minus));
					ADD_layout_head.setVisibility(View.VISIBLE);
					findViewById(R.id.ADD_comp).setVisibility(View.INVISIBLE);
					if(findViewById(R.id.RCN_comp).getVisibility()!=View.VISIBLE)
					{
						RCN_layout_head.setVisibility(View.GONE);
						RCN_expend.setTag("plus");
         				RCN_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
					}
					
				}
				ADD_chk_top.setFocusableInTouchMode(true);
				ADD_chk_top.requestFocus();
				ADD_chk_top.setFocusableInTouchMode(false);
				ADD_chk_top.setFocusable(false);
				ADD_chk_top.setFocusable(true);
				
			}
		});
	 	
	 	RCN_chk_top.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(RCN_chk_top.isChecked())
				{
					Cursor c =cf.SelectTablefunction(cf.RCN_table, " WHERE R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id='"+Roof_page_value[5]+"'");
					if(c.getCount()>0)
					{
						c.moveToFirst();
						if(!cf.decode(c.getString(c.getColumnIndex("R_RCN_HRMS"))).equals(""))
						{
							final AlertDialog.Builder ab =new AlertDialog.Builder(Roof_commercial_information.this);
							ab.setTitle("Confirmation");
							ab.setMessage(Html.fromHtml("Do you want to clear the Roof Conditions Noted saved data?"));
							ab.setIcon(R.drawable.alertmsg);
							ab.setCancelable(false);
							ab.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
								
								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									cf.arr_db.execSQL(" DELETE FROM "+cf.RCN_table_dy+" WHERE R_RCN_D_Ssrid='"+cf.selectedhomeid+"' and R_RCN_D_insp_id='"+Roof_page_value[5]+"'");
									cf.arr_db.execSQL(" DELETE FROM "+cf.RCN_table+" WHERE R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id='"+Roof_page_value[5]+"'");
									/*cf.ShowToast("Roof Condition Noted question deleted successfully ", 0);
									TableLayout tbl_dy=((TableLayout)findViewById(R.id.RCN_Condition_N_DY_tbl));
									tbl_dy.removeView(tbl_dy.findViewById(id));*/
									clear_RCN();
									RCN_expend.setTag("plus");
			         				RCN_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
									RCN_layout_head.setVisibility(View.GONE);
									
									//Show_dynamic_RCN();
								}
								
							})
							.setNegativeButton("No", new DialogInterface.OnClickListener() {
								
								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									RCN_chk_top.setChecked(false);
								}
							});
							AlertDialog al=ab.create();
							al.show();
					}
					else
					{
						clear_RCN();
						RCN_expend.setTag("plus");
         				RCN_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
						RCN_layout_head.setVisibility(View.GONE);
					}
					}
					else
					{
						clear_RCN();
						RCN_expend.setTag("plus");
         				RCN_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
						RCN_layout_head.setVisibility(View.GONE);
					}
					/*if(findViewById(R.id.ADD_comp).getVisibility()!=View.VISIBLE)
					{
						ADD_expend.setTag("plus");
         				ADD_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
						ADD_layout_head.setVisibility(View.GONE);	
					}*/
					
				}
				else
				{
					
					RCN_expend.setTag("minus");
     				RCN_expend.setImageDrawable(getResources().getDrawable(R.drawable.minus));
					RCN_layout_head.setVisibility(View.VISIBLE);
					if(findViewById(R.id.ADD_comp).getVisibility()!=View.VISIBLE)
					{
						ADD_expend.setTag("plus");
         				ADD_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
						ADD_layout_head.setVisibility(View.GONE);	
					}
					findViewById(R.id.RCN_comp).setVisibility(View.INVISIBLE);
					
				}
				
				RCN_chk_top.setFocusableInTouchMode(true);
				RCN_chk_top.requestFocus();
			    RCN_chk_top.setFocusableInTouchMode(false);
			    RCN_chk_top.setFocusable(false);
				RCN_chk_top.setFocusable(true);
			}

			
		});
	 	RCT_chk_top=(CheckBox)findViewById(R.id.RCT_chk_top);
	 	RCT_chk_top.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(RCT_chk_top.isChecked())
				{
					Cursor c =cf.SelectTablefunction(cf.Rct_table, " WHERE RCT_Ssrid='"+cf.selectedhomeid+"' and RCT_insp_id='"+Roof_page_value[5]+"'");
					if(c.getCount()>0)
					{
						final AlertDialog.Builder ab =new AlertDialog.Builder(Roof_commercial_information.this);
						ab.setTitle("Confirmation");
						ab.setMessage(Html.fromHtml("Do you want to clear the Roof Covering Type saved data?"));
						ab.setIcon(R.drawable.alertmsg);
						ab.setCancelable(false);
						ab.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								cf.arr_db.execSQL(" DELETE FROM "+cf.Rct_table+" WHERE RCT_Ssrid='"+cf.selectedhomeid+"' and RCT_insp_id='"+Roof_page_value[5]+"'");
								cf.arr_db.execSQL(" DELETE FROM "+cf.Roofcoverimages+" Where RIM_masterid=(Select RM_masterid from "+cf.Roof_master+
										" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+Roof_page_value[5]+"') ");
								
								/*cf.ShowToast("Roof Condition Noted question deleted successfully ", 0);
								TableLayout tbl_dy=((TableLayout)findViewById(R.id.RCN_Condition_N_DY_tbl));
								tbl_dy.removeView(tbl_dy.findViewById(id));*/
								clear_RCT();
								findViewById(R.id.RCT_content).setVisibility(View.GONE);
								show_RCT_values();
								
								//Show_dynamic_RCN();
							}
							
						})
						.setNegativeButton("No", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								RCT_chk_top.setChecked(false);
							}
						});
						AlertDialog al=ab.create();
						al.show();
					}
					else
					{
						clear_RCT();
						findViewById(R.id.RCT_content).setVisibility(View.GONE);
					}
					
				}
				else
				{
					
					findViewById(R.id.RCT_content).setVisibility(View.VISIBLE);
					findViewById(R.id.RCT_comp).setVisibility(View.INVISIBLE);
					
				}
				
			
			}
		});
		/***Roof condition noted ends**/
		RC_cmt=(EditText) findViewById(R.id.GCH_RC_ED_Li_Comments);
		RC_cmt.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				cf.showing_limit(s.toString(), ((TextView) findViewById(R.id.GCH_RC_ED_txt_Comments)), 500);
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		show_RCT_values();
		Additional_show_value();
		Show_RCN_values();
		Show_generalvalue();
		
	}
	class Spin_Selectedlistener implements OnItemSelectedListener
	{
		public Spin_Selectedlistener() {
			// TODO Auto-generated constructor stub
			
		}
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
			// TODO Auto-generated method stub
			String roofslopeval = Sp_RCT[2].getSelectedItem().toString();
			if(roofslopeval.equals("Flat"))
			{
				Sp_RCT[3].setSelection(4);
			}
			else
			{
				if(Sp_RCT[3].getSelectedItem().toString().equals("Flat Roof") && editid==0)
				//if(Sp_RCT[3].getSelectedItem().toString().equals("Flat Roof"))
				{
					Sp_RCT[3].setSelection(0);
				}
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
	}
	private void Show_generalvalue() {
		// TODO Auto-generated method stub
		Cursor c = cf.SelectTablefunction(cf.Roof_additional, " WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+Roof_page_value[5]+"' ");
		if(c.getCount()>0)
		{
			c.moveToFirst();
			
			String  comment=cf.decode(c.getString(c.getColumnIndex("R_ADD_comment"))),
				  Na=cf.decode(c.getString(c.getColumnIndex("R_ADD_na")));
		
			if(comment.equals(""))
			{
				RC_cmt.setText("No comments.");
			}
			else
			{
				RC_cmt.setText(comment);	
			}
			if(Na.equals("true"))
			{
				ADD_chk_top.setChecked(true);
				
				ADD_expend.setTag("plus");
 				ADD_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
				ADD_layout_head.setVisibility(View.GONE);
			}
			if(c.getString(c.getColumnIndex("R_RCT_na")).equals("true"))
			{
				RCT_chk_top.setChecked(true);
				findViewById(R.id.RCT_content).setVisibility(View.GONE);
			
			}
			
			if((c.getString(c.getColumnIndex("R_ADD_saved")).equals("1") || c.getString(c.getColumnIndex("R_ADD_saved")).equals("2")) && (!c.getString(c.getColumnIndex("R_ADD_WUWC")).equals("") || c.getString(c.getColumnIndex("R_ADD_na")).equals("true")))
			{
				findViewById(R.id.ADD_comp).setVisibility(View.VISIBLE);
			}
			else
			{
				findViewById(R.id.ADD_comp).setVisibility(View.INVISIBLE);
			}
			if(c.getString(c.getColumnIndex("R_ADD_citizen")).equals("true"))
			{
				((CheckBox)findViewById(R.id.includeCitizenForm)).setChecked(true);
			}
			if(c.getString(c.getColumnIndex("R_RCT_na")).equals("true") && c.getString(c.getColumnIndex("R_ADD_saved")).equals("2"))
			{
				findViewById(R.id.RCT_comp).setVisibility(View.VISIBLE);
			}
			
		}
		c=cf.SelectTablefunction(cf.RCN_table, " WHERE R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id='"+Roof_page_value[5]+"' ");
		if(c.getCount()>0)
		{
			c.moveToFirst();
			if(c.getString(c.getColumnIndex("R_RCN_saved")).equals("1"))
			{
				findViewById(R.id.RCN_comp).setVisibility(View.VISIBLE);
			}
			if(c.getString(c.getColumnIndex("R_RCN_na")).equals("true"))
			{
				RCN_chk_top.setChecked(true);
				clear_RCN();
				RCN_expend.setTag("plus");
 				RCN_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
				RCN_layout_head.setVisibility(View.GONE);
			}
			
			
			
		}
		if(c!=null)
			c.close();
	}
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.sampleCitizenCertificate:
			Intent in=new Intent(this,CitizenCertification.class);
			cf.putExtraIntent(in);
			in.putExtra("insp_id", Roof_page_value[5]);
			startActivityForResult(in, 10);
			break;
		case R.id.loadcomments:
			/***Call for the comments***/
			getRoofValueForVariables(cf.Roof);
			int len=RC_cmt.getText().toString().length();
			
				if(load_comment )
				{
					load_comment=false;
					int loc[] = new int[2];
					v.getLocationOnScreen(loc);
					Intent in1 = cf.loadComments("Overall Roof Comments",loc);
					in1.putExtra("length", len);
					in1.putExtra("max_length", 500);
					startActivityForResult(in1, cf.loadcomment_code);
				}
		break;
		case R.id.Add_help:
			cf.ShowToast("Complete for all Wind Mitigation Inspection. ", 0);
		break;
		case R.id.RCN_help:
			cf.ShowToast("Complete for all Wind Mitigation Inspection. ", 0);
		break;
		case R.id.abbrev:
			Intent s= new Intent(this,PolicyholdeInfoHead.class);
			s.putExtra("homeid", cf.selectedhomeid);
			s.putExtra("Type", "Abbreviation");
			s.putExtra("insp_id", cf.Insp_id);
			startActivityForResult(s, cf.info_requestcode);
		break;
		case R.id.Roof_RCT_SA:
			RCT_validation();
		break;
		case R.id.Roof_RCT_clear:
			clear_RCT();
		break;
		case R.id.ADD_save:
			Additional_validation();
			break;
		case R.id.RC_AMC:
			Add_more_condition();
		break;
		/*case R.id.RCN_save:
			RCN_validation();
		break;*/
		case R.id.GCH_RC_savebtn:
			roofvaldiation();
			//over_all_validation();
		break;
		case R.id.GCH_RC_homebtn:
			cf.gohome();
		break;
		default:
			break;
		}
	}
	private void roofvaldiation() 
	{
		String scope="";	
		
		Cursor c =cf.SelectTablefunction(cf.Rct_table, " WHERE RCT_Ssrid='"+cf.selectedhomeid+"' and RCT_insp_id='"+Roof_page_value[5]+"' ");
		System.out.println("roofvla="+c.getCount());
		if(c.getCount()>0 || RCT_chk_top.isChecked()) 
		{
			System.out.println("percec");
		if(RCT_total_per==0 || RCT_chk_top.isChecked())
		{
			if(RCT_pre_m || RCT_chk_top.isChecked())
			{
				c =cf.SelectTablefunction(cf.Roof_additional, " WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+Roof_page_value[5]+"'  and (R_ADD_saved='1' or R_ADD_saved='2')");
				c.moveToFirst();
				System.out.println("croof="+c.getCount());
				
				if(ADD_chk_top.isChecked()|| Roof_page_value[5].equals("7"))
				{
					/*System.out.println("croof="+c.getColumnIndex("R_ADD_WUWC"));
					String s = (ADD_chk_top.isChecked())?"":c.getString(c.getColumnIndex("R_ADD_WUWC"));
					System.out.println("sss"+s+"checke"+ADD_chk_top.isChecked());
					if(!s.equals("") || ADD_chk_top.isChecked())
					{
						*/
						RCN_validation();
						
				}
				else
				{
						System.out.println("sdsdsd");
						if(R_ADD_WUWC.getCheckedRadioButtonId()!=-1)
						{
							if(R_ADD_PDRUC.getCheckedRadioButtonId()!=-1)
							{
								if(!R_ADD_PN.getText().toString().trim().equals("") || RC_Additional.findViewWithTag("pd_no").getVisibility()==View.GONE)
								{
									if(!R_ADD_PD.getText().toString().trim().equals("") || RC_Additional.findViewWithTag("pd_date").getVisibility()==View.GONE)
									{
										if(R_ADD_RME.getCheckedRadioButtonId()!=-1)
										{
											if(validate_RME() ||  RC_Additional.findViewWithTag("RMS").getVisibility()==View.GONE )
											{
												if(R_ADD_RMEL.getCheckedRadioButtonId()!=-1 ||  RC_Additional.findViewWithTag("RMS1").getVisibility()==View.GONE )
												{
													if(R_ADD_DRMS.getCheckedRadioButtonId()!=-1 ||  RC_Additional.findViewWithTag("RMS2").getVisibility()==View.GONE )
													{
														RCN_validation();
													}
													else
													{
														cf.ShowToast("Please select value for "+getResources().getString(R.string.DamageR), 0);
													}
												}
												else
												{
													cf.ShowToast("Please select value for "+getResources().getString(R.string.EquipmentL), 0);
												}
											}
										}
										else
										{
											cf.ShowToast("Please select value for "+getResources().getString(R.string.COMER4), 0);
										}
										
									}else
									{
										cf.ShowToast("Please enter value for Date", 0);
									}	
								}else
								{
									cf.ShowToast("Please enter value for Permit No", 0);
								}
								
							}else
							{
								cf.ShowToast("Please select value for "+getResources().getString(R.string.COMER2), 0);
							}					
						}
						else
	
						{
							cf.ShowToast("Please select value for "+getResources().getString(R.string.ROOF_Survey5), 0);
						}
				}					
			
					
			
						
			}else
			{
				cf.ShowToast("Please select predominant for any one Roof Covering Types", 0);
			}
		}else
		{
			cf.ShowToast("Sum of Roof Shape (%) percentage should be 100. in Roof Covering Types", 0);
		}	
		}else
		{
			cf.ShowToast("Please save atleast one Roof Covering Types", 0);
		}			
	}
	
	public void showDialogDate(EditText edt) {
		// TODO Auto-generated method stub
	    getCalender();
		Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialog= new DatePickerDialog(Roof_commercial_information.this, new mDateSetListener(edt),mYear, mMonth, mDay);
        dialog.show();
	}
	public void getCalender() {
		// TODO Auto-generated method stub
		final Calendar c = Calendar.getInstance();
			mYear = c.get(Calendar.YEAR);
			mMonth = c.get(Calendar.MONTH);
			mDay = c.get(Calendar.DAY_OF_MONTH);
			
			mDayofWeek = c.get(Calendar.DAY_OF_WEEK);
			hours = c.get(Calendar.HOUR);
			minutes = c.get(Calendar.MINUTE);
			seconds = c.get(Calendar.SECOND);
			amorpm = c.get(Calendar.AM_PM);	
	}
	class mDateSetListener implements DatePickerDialog.OnDateSetListener
	{
		EditText v;
		mDateSetListener(EditText v)
		{
			this.v=v;
		}
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			// TODO Auto-generated method stub
			getCalender();
			mYear = year;
			mMonth = monthOfYear+1;
			mDay = dayOfMonth;
			selmDayofWeek = mDayofWeek ;
			
			v.setText(new StringBuilder()
					// Month is 0 based so add 1
					.append(cf.pad(mMonth)).append("/").append(cf.pad(mDay)).append("/")
					.append(mYear).append(" "));
			if(!RCT_ed[0].getText().toString().equals("N/A") && !RCT_ed[0].getText().toString().equals(""))
			{
				RCT_chk[0].setChecked(false);
			}
			
		}
		
	}
	private void over_all_validation() {
		// TODO Auto-generated method stub
		String scope="";
			Cursor c =cf.SelectTablefunction(cf.Rct_table, " WHERE RCT_Ssrid='"+cf.selectedhomeid+"' and RCT_insp_id='"+Roof_page_value[5]+"' ");
			if(c.getCount()>0 || RCT_chk_top.isChecked())
			{
			if(RCT_total_per==0 || RCT_chk_top.isChecked())
			{
				if(RCT_pre_m || RCT_chk_top.isChecked())
				{
					c =cf.SelectTablefunction(cf.Roof_additional, " WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+Roof_page_value[5]+"'  and (R_ADD_saved='1' or R_ADD_saved='2')");
					c.moveToFirst();
					System.out.println("croof="+c.getCount());
				if(c.getCount()>0 || ADD_chk_top.isChecked()|| Roof_page_value[5].equals("7"))
				{
					//System.out.println("croof="+c.getColumnIndex("R_ADD_WUWC"));
					String s = (ADD_chk_top.isChecked())?"":c.getString(c.getColumnIndex("R_ADD_WUWC"));
					if(!s.equals("") || ADD_chk_top.isChecked())
					{
					c =cf.SelectTablefunction(cf.RCN_table, " WHERE R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id='"+Roof_page_value[5]+"' and R_RCN_saved='1'");
					if(c.getCount()>0  || RCN_chk_top.isChecked())
					{
						/*if(!RC_cmt.getText().toString().trim().equals(""))
						{*/
							//cf.arr_db.execSQL(" UPDATE "+cf.Roof_additional+" SET R_ADD_scope='"+cf.encode(scope)+"',R_ADD_na='"+ADD_chk_top.isChecked()+"',R_ADD_comment='"+cf.encode(RC_cmt.getText().toString().trim())+"',R_ADD_citizen='"+((CheckBox)findViewById(R.id.includeCitizenForm)).isChecked()+"'  WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+Roof_page_value[5]+"'");
							 c =cf.SelectTablefunction(cf.RCN_table, " WHERE R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id='"+Roof_page_value[5]+"' ");
							if(c.getCount()>0)
							{
								cf.arr_db.execSQL(" UPDATE "+cf.RCN_table+" SET R_RCN_saved='1',R_RCN_na='"+RCN_chk_top.isChecked()+"'  WHERE R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id='"+Roof_page_value[5]+"'");
							}
							else
							{
								cf.arr_db.execSQL(" INSERT INTO "+cf.RCN_table+" (R_RCN_na,R_RCN_Ssrid,R_RCN_insp_id,R_RCN_saved) VALUES ('"+RCN_chk_top.isChecked()+"','"+cf.selectedhomeid+"','"+Roof_page_value[5]+"','1')");
							}
							c=cf.SelectTablefunction(cf.Roof_additional, " WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+Roof_page_value[5]+"'");
							if(c.getCount()>0)
							{
								cf.arr_db.execSQL(" UPDATE "+cf.Roof_additional+" SET R_ADD_saved='2',R_RCT_na='"+RCT_chk_top.isChecked()+"', R_ADD_scope='"+cf.encode(scope)+"',R_ADD_na='"+ADD_chk_top.isChecked()+"',R_ADD_comment='"+cf.encode(RC_cmt.getText().toString().trim())+"',R_ADD_citizen='"+((CheckBox)findViewById(R.id.includeCitizenForm)).isChecked()+"'  WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+Roof_page_value[5]+"'");
							}
							else
							{
									cf.arr_db.execSQL(" INSERT INTO "+cf.Roof_additional+" (R_RCT_na,R_ADD_scope,R_ADD_na,R_ADD_comment,R_ADD_citizen,R_ADD_Ssrid,R_ADD_insp_id,R_ADD_saved) Values ('"+RCT_chk_top.isChecked()+"','"+cf.encode(scope)+"','"+ADD_chk_top.isChecked()+"','"+cf.encode(RC_cmt.getText().toString().trim())+"','"+((CheckBox)findViewById(R.id.includeCitizenForm)).isChecked()+"','"+cf.selectedhomeid+"','"+Roof_page_value[5]+"','2')");	
							}
							cf.ShowToast("Roof information saved successfully", 1);
							checkforsurveyaleret();
						/*}
						else
						{
							cf.ShowToast("Please enter Roof Comments", 0);
						}*/
					}
					else
					{
						cf.ShowToast("Please save  Roof Condition Noted", 0);
					}
					}
					else
					{
						cf.ShowToast("Please save  Roof Additional Information", 0);
					}
				}
				else
				{
					cf.ShowToast("Please save  Roof Additional Information", 0);
				}
				}else
				{
					cf.ShowToast("Please select predominant for any one Roof Covering Types", 0);
				}
			}else
			{
				cf.ShowToast("Sum of Roof Shape (%) percentage should be 100. in Roof Covering Types", 0);
			}	
			}else
			{
				cf.ShowToast("Please save atleast one Roof Covering Types", 0);
			}
		
	}
	/***RCT table function****/
	protected void sp_setvalues(Spinner spinner, String string,EditText ed) {
		// TODO Auto-generated method stub
		ArrayAdapter  ad =(ArrayAdapter) spinner.getAdapter();
		System.out.println(" the value year"+string);
		if(!string.equals("") && !string.equals("N/A"))
		{
		int pos=ad.getPosition(string.trim());
		if(pos==-1)
		{
			int other_pos=ad.getPosition("Other");
			if(other_pos!=-1)
			{
				spinner.setSelection(other_pos);
				ed.setText(string);
			}
		}
		else
		{
			spinner.setSelection(pos);
		}
		}
		
	}
	class spiner_lis implements OnItemSelectedListener
	{
		EditText other;
		EditText per=null;;
		Spinner sp;
		public spiner_lis(Spinner sp,EditText editText) {
			// TODO Auto-generated constructor stub
			other=editText;
			this.sp=sp;
		}

		public spiner_lis(Spinner sp,EditText editText, EditText per) {
			// TODO Auto-generated constructor stub
			other=editText;
			this.per=per;
			this.sp=sp;
		}

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			System.out.println("Sp_RCT[1]"+Sp_RCT[1].getSelectedItem().toString());
			if(!Sp_RCT[1].getSelectedItem().toString().equals("Unknown"))
			{
				RCT_ed[0].setText("");
				RCT_bt[0].setVisibility(View.VISIBLE);
				RCT_chk[0].setChecked(false);
			}
			
			if(sp.getSelectedItem().toString().trim().equals("Other"))
			{
				other.setVisibility(View.VISIBLE);
			}
			else
			{
				other.setText("");
				other.setVisibility(View.GONE);
			}
			
			if(per!=null)
			{
				if(!sp.getSelectedItem().toString().trim().equals("-Select-"))
				{
					/*
					if(!((Button) findViewById(R.id.Roof_RCT_SA)).getText().equals("Update"))
					{
						per.setText(RCT_total_per+"");
					}*/
					if(per.getText().toString().equals(""))
					{
						per.setText(RCT_total_per+"");
					}
					per.setEnabled(true);
				}
				else
				{
					per.setEnabled(false);
					per.setText("");
				}
			}
			
			if(sp.getId()==R.id.Roof_RCT_sp_RCT)
			{
				if(sp.getSelectedItemPosition()!=0)
				{
					Cursor c=cf.arr_db.rawQuery("Select * from "+cf.Roofcoverimages+" Where RIM_masterid=(Select RM_masterid from "+cf.Roof_master+
		    				" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+Roof_page_value[5]+"' and RM_Covering='"+cf.encode(sp.getSelectedItem().toString())+"') order by RIM_Order", null);
		    		
		    		if(c.getCount()>0)
		    		{
		    			tv.setVisibility(View.VISIBLE);
		    			tv.setText(Html.fromHtml("<u>View/Edit Photos ("+c.getCount()+")</u>"));
		    		
				    	
		    		}
		    		else
		    			{
		    				tv.setVisibility(View.GONE);
		    			}
		    			
				}
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	private void set_value_spinner(Spinner spinner, String[] arr_RCT2) {
		// TODO Auto-generated method stub
		ArrayAdapter adapter;
		adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, arr_RCT2);
	 	adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	 	spinner.setAdapter(adapter);
	}
	private void clear_RCT() {
		// TODO Auto-generated method stub
		for(int i=0;i<Sp_RCT.length;i++)
		{
			Sp_RCT[i].setSelection(0);
		}
		RCT_ed[0].setText("");
		RCT_ed[1].setText("");
		RCT_chk[0].setChecked(false);
		RCT_chk[1].setChecked(false);
		tv.setVisibility(View.GONE);
		((Button) findViewById(R.id.Roof_RCT_SA)).setText(getResources().getString(R.string.S_Addmore));
		Sp_RCT[0].setEnabled(true);
		RCT_bt[0].setVisibility(View.VISIBLE);
	}
	private boolean RCT_validation() {
		// TODO Auto-generated method stub
		if(spinner_validation(Sp_RCT[0],RCT_other[0],"Roof Covering Type",false))
		{
			if(!RCT_ed[0].getText().toString().trim().equals(""))
			{
				boolean no_per=RCT_chk[0].isChecked();
				if(spinner_validation(Sp_RCT[1],RCT_other[1],"Year of Installation",no_per) )
				{
					if(year_validation(RCT_other[1],"Year of Installation"))
					{
						if(spinner_validation(Sp_RCT[2],null,"Roof Slope",no_per) )
						{
							if(spinner_validation(Sp_RCT[3],RCT_other[2],"Roof Shape (%)",no_per) )
							{
								if(percent_validation(RCT_ed[1],Sp_RCT[3],"Roof Shape (%)",no_per))
								{
									if(spinner_validation(Sp_RCT[4],RCT_other[3],"Roof Structure",no_per) )
									{
										if(spinner_validation(Sp_RCT[5],null,"Building Code",no_per) )
										{
											if(spinner_validation(Sp_RCT[6],null,"Roof Complexity",no_per) )
											{
												if(spinner_validation(Sp_RCT[7],RCT_other[4],"Approximate Remaining Functional Life",no_per) )
												{
													if(duplicate_RCT(Sp_RCT[0]))
													{
														save_rct_values();
													}
													
												}
												
											}
										}
										
									}
									
								}
							}
						}
					}
				}
			}
			else
			{
				cf.ShowToast("Please select date for Permit Application Date", 0);
			}
		}
		return false;
	}
	private void save_rct_values() {
		// TODO Auto-generated method stub

		 String RCT_rct , RCT_PAD, RCT_yoi,  RCT_rsl, RCT_rsh , RCT_rst , RCT_bc , RCT_rc , RCT_arfl ,  RCT_Rsh_per
		, RCT_other_rct , RCT_other_yoi, RCT_other_rsh , RCT_other_rst , RCT_other_aRFL,RCT_pre,RCT_npv;
		 RCT_rct=remove_empty(Sp_RCT[0].getSelectedItem().toString().trim());
		 RCT_yoi=remove_empty(Sp_RCT[1].getSelectedItem().toString().trim());
		 RCT_rsl=remove_empty(Sp_RCT[2].getSelectedItem().toString().trim());
		 RCT_rsh=remove_empty(Sp_RCT[3].getSelectedItem().toString().trim());
		 RCT_rst=remove_empty(Sp_RCT[4].getSelectedItem().toString().trim());
		 RCT_bc=remove_empty(Sp_RCT[5].getSelectedItem().toString().trim());
		 RCT_rc=remove_empty(Sp_RCT[6].getSelectedItem().toString().trim());
		 RCT_arfl=remove_empty(Sp_RCT[7].getSelectedItem().toString().trim());
		 RCT_other_rct=remove_empty(RCT_other[0].getText().toString().trim());
		 RCT_other_yoi=remove_empty(RCT_other[1].getText().toString().trim());
		 RCT_other_rsh=remove_empty(RCT_other[2].getText().toString().trim()); 
		 RCT_other_rst=remove_empty(RCT_other[3].getText().toString().trim()); 
		 RCT_other_aRFL=remove_empty(RCT_other[4].getText().toString().trim());
		 RCT_PAD=RCT_ed[0].getText().toString().trim();
		 RCT_PAD=(RCT_PAD.equals("N/A"))?"":remove_empty(RCT_PAD);
		 RCT_Rsh_per=remove_empty(RCT_ed[1].getText().toString().trim());
		 if(RCT_chk[0].isChecked())
		 {
			 RCT_npv="true"; 
		 }
		 else
		 {
			 RCT_npv="false";
		 }
		 if(RCT_chk[1].isChecked())
		 {
			 RCT_pre="true"; 
		 }
		 else
		 {
			 RCT_pre="false";
		 }
		 //RCT_pre=RCT_chk[1].isChecked();
		 System.out.println("comes correctly2");
		 try
		 {
		 if(RCT_pre.equals("true"))
		 {
			 cf.arr_db.execSQL(" UPDATE "+cf.Rct_table+" SET RCT_pre='false' WHERE RCT_Ssrid='"+cf.selectedhomeid+"' and RCT_insp_id='"+Roof_page_value[5]+"'");
		 }
		 Cursor c =cf.SelectTablefunction(cf.Rct_table, " WHERE RCT_Ssrid='"+cf.selectedhomeid+"' and RCT_rct='"+cf.encode(Sp_RCT[0].getSelectedItem().toString().trim())+"' and RCT_insp_id='"+Roof_page_value[5]+"'");
			if(c.getCount()>0)
			{
				cf.arr_db.execSQL(" UPDATE  "+cf.Rct_table+" SET RCT_rct='"+RCT_rct+"',RCT_PAD='"+RCT_PAD+"',RCT_yoi='"+RCT_yoi+"',RCT_rsl='"+RCT_rsl+"',RCT_rsh='"+RCT_rsh+"',RCT_rst='"+RCT_rst+"',RCT_bc='"+RCT_bc+"',RCT_rc='"+RCT_rc+"',RCT_arfl='"+RCT_arfl+"'," +
						"RCT_Rsh_per='"+RCT_Rsh_per+"',RCT_other_rct='"+RCT_other_rct+"',RCT_other_yoi='"+RCT_other_yoi+"',RCT_other_rsh='"+RCT_other_rsh+"',RCT_other_rst='"+RCT_other_rst+"',RCT_other_aRFL='"+RCT_other_aRFL+"',RCT_npv='"+RCT_npv+"',RCT_pre='"+RCT_pre+"' WHERE RCT_Ssrid='"+cf.selectedhomeid+"' and RCT_rct='"+cf.encode(Sp_RCT[0].getSelectedItem().toString().trim())+"' and RCT_insp_id='"+Roof_page_value[5]+"'");
			}
			else
			{
				cf.arr_db.execSQL(" INSERT INTO "+cf.Rct_table+" (RCT_Ssrid,RCT_rct,RCT_PAD,RCT_yoi,RCT_rsl,RCT_rsh,RCT_rst,RCT_bc,RCT_rc,RCT_arfl,RCT_Rsh_per,RCT_other_rct,RCT_other_yoi,RCT_other_rsh,RCT_other_rst,RCT_other_aRFL,RCT_insp_id,RCT_npv,RCT_pre)" +
				" VALUES ('"+cf.selectedhomeid+"','"+RCT_rct+"','"+RCT_PAD+"','"+RCT_yoi+"','"+RCT_rsl+"','"+RCT_rsh+"','"+RCT_rst+"','"+RCT_bc+"','"+RCT_rc+"','"+RCT_arfl+"','"+RCT_Rsh_per+"','"+RCT_other_rct+"','"+RCT_other_yoi+"','"+RCT_other_rsh+"','"+RCT_other_rst+"','"+RCT_other_aRFL+"','"+Roof_page_value[5]+"','"+RCT_npv+"','"+RCT_pre+"')");
			}
			c=cf.SelectTablefunction(cf.Roof_additional, " WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+Roof_page_value[5]+"'");
			if(c.getCount()>0)
			{
				cf.arr_db.execSQL(" UPDATE "+cf.Roof_additional+" SET R_RCT_na='"+RCT_chk_top.isChecked()+"'  WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+Roof_page_value[5]+"'");
			}
			if(c!=null)
			{
				c.close();
			}
		 	}
			catch (Exception e) {
				// TODO: handle exception
				System.out.println(" the error was "+e.getMessage());
			}
		 cf.ShowToast("Roof Covering Type saved successfully", 0);
		 clear_RCT();
			show_RCT_values();
	}
	private String remove_empty(String trim) {
		// TODO Auto-generated method stub
		if(trim!=null)
		{
			if(!trim.equals("-Select-") && !trim.equals("--Select--"))
			{
				if(!trim.equals("N/A"))
				{
					return cf.encode(trim);
				}
			}
		}
		
		return "";
	}
	private void show_RCT_values() {
		// TODO Auto-generated method stub
		RCT_total_per=100;
		 Cursor c =cf.SelectTablefunction(cf.Rct_table, " WHERE RCT_Ssrid='"+cf.selectedhomeid+"'  and RCT_insp_id='"+Roof_page_value[5]+"'");
		 TableLayout	RCT_ShowValue= (TableLayout) findViewById(R.id.RC_RCT_ShowValue);
		 RCT_ShowValue.removeAllViews();
		int  colomprerow=6;
			if(c.getCount()>0)
			{
				String arr_title[]={"No","Roof Covering Type","Permit Application Date","Year of Insallation","No Permit/Verification Documents","Roof Slope","Roof Shape (%)","Roof Structure","Building Code","Roof Complexity","Approximate Remaining Functional Life","Predominant","Upload Photo","Edit/Delete"};
				int width[]={30,120,140,125,160,100,120,130,110,125,110,100,120,110};
				 
				 
					c.moveToFirst();
					
					RCT_ShowValue.setVisibility(View.VISIBLE);
					TableRow tbl =new TableRow(this);
					TableRow.LayoutParams lin_params = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
					tbl.setBackgroundResource(R.drawable.roofhead); 
					tbl.setLayoutParams(lin_params);
					tbl.setGravity(Gravity.CENTER_VERTICAL);
					
					TableLayout li_opt=new TableLayout(this);
					li_opt.setOrientation(LinearLayout.VERTICAL);
					/**Sub row linear **/
					TableRow li_spu =new TableRow(this);
					li_opt.addView(li_spu,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
					for(int i=0;i<arr_title.length;i++)
					{
						if(i<2)
						{
							TextView Ed=new TextView(this,null,R.attr.text_view);
							Ed.setVisibility(View.VISIBLE);
							Ed.setTextColor(Color.WHITE);
							tbl.addView(Ed,width[i],LayoutParams.WRAP_CONTENT);
							Ed.setText(arr_title[i]);
							View v=new View(this);
							v.setBackgroundResource(R.color.black);
							tbl.addView(v,1,LayoutParams.FILL_PARENT);
						}
						else
						{
							if((i-2)%colomprerow==0 && (i-2)!=0)
							{
								View v=new View(this);
								v.setBackgroundResource(R.color.row_head_line);
								li_opt.addView(v,LayoutParams.FILL_PARENT,1);
								
								li_spu=new TableRow(this);
								li_opt.addView(li_spu,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
							}
							else if((i-2)!=0)
							{
								View v=new View(this);
								v.setBackgroundResource(R.color.black);
								li_spu.addView(v,1,LayoutParams.FILL_PARENT);
							}
								
							TextView Ed=new TextView(this,null,R.attr.text_view);
							Ed.setVisibility(View.VISIBLE);
							Ed.setTextColor(Color.WHITE);
							
							li_spu.addView(Ed,width[i],LayoutParams.WRAP_CONTENT);
							Ed.setText(arr_title[i]);
						}
					}
					tbl.addView(li_opt,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
					RCT_ShowValue.addView(tbl);
					c.moveToFirst();
					TextView Ed=null;
					for(int i=0;i<c.getCount();i++,c.moveToNext())
					{
						 String RCT_rct , RCT_PAD, RCT_yoi,  RCT_rsl, RCT_rsh , RCT_rst , RCT_bc , RCT_rc , RCT_arfl ,  RCT_Rsh_per
							, RCT_other_rct , RCT_other_yoi, RCT_other_rsh , RCT_other_rst , RCT_other_aRFL,RCT_pre,RCT_npv;
							 RCT_rct=cf.decode(c.getString(c.getColumnIndex("RCT_rct")));
							 RCT_PAD =cf.decode(c.getString(c.getColumnIndex("RCT_PAD")));
							 RCT_yoi=cf.decode(c.getString(c.getColumnIndex("RCT_yoi")));
							 RCT_rsl =cf.decode(c.getString(c.getColumnIndex("RCT_rsl")));
							 RCT_rsh=cf.decode(c.getString(c.getColumnIndex("RCT_rsh")));
							 RCT_rst =cf.decode(c.getString(c.getColumnIndex("RCT_rst")));
							 RCT_bc =cf.decode(c.getString(c.getColumnIndex("RCT_bc")));
							 RCT_rc =cf.decode(c.getString(c.getColumnIndex("RCT_rc")));
							 RCT_arfl =cf.decode(c.getString(c.getColumnIndex("RCT_arfl")));
							 RCT_Rsh_per=cf.decode(c.getString(c.getColumnIndex("RCT_Rsh_per")));
							 if(!RCT_Rsh_per.equals(""))
							 {
								 try
								 {
									 RCT_total_per -=Integer.parseInt(RCT_Rsh_per);
								 }
								 catch (Exception e) {
									// TODO: handle exception
								}
							 }
							 RCT_other_rct =cf.decode(c.getString(c.getColumnIndex("RCT_other_rct")));
							 RCT_other_yoi =cf.decode(c.getString(c.getColumnIndex("RCT_other_yoi")));
							 RCT_other_rsh=cf.decode(c.getString(c.getColumnIndex("RCT_other_rsh")));
							 RCT_other_rst=cf.decode(c.getString(c.getColumnIndex("RCT_other_rst")));
							 RCT_other_aRFL=cf.decode(c.getString(c.getColumnIndex("RCT_other_aRFL")));
							 RCT_pre=cf.decode(c.getString(c.getColumnIndex("RCT_pre")));
							 if(RCT_pre.equals("true"))
							 {
								 RCT_pre_m=true;
							 }
							 RCT_npv=cf.decode(c.getString(c.getColumnIndex("RCT_npv")));
						
						String value[]=new String[arr_title.length];
						value[0]=(i+1)+"";
						if(RCT_rct.equals("Other"))
							value[1]=RCT_other_rct;
						else
							value[1]=RCT_rct;
						
						value[2]=RCT_PAD;
						
						if(RCT_yoi.equals("Other"))
							value[3]=RCT_other_yoi;
						else
							value[3]=RCT_yoi;
						value[4]=RCT_npv;
						value[5]=RCT_rsl;
						
						if(RCT_rsh.equals("Other"))
							value[6]=RCT_other_rsh;
						else
							value[6]=RCT_rsh;
						if(!RCT_Rsh_per.equals(""))
						{
							value[6]+="("+RCT_Rsh_per+"%)";
						}
						if(RCT_rst.equals("Other"))
							value[7]=RCT_other_rst;
						else
							value[7]=RCT_rst;
						value[8]=RCT_bc;
						value[9]=RCT_rc;
						
						if(RCT_arfl.equals("Other"))
							value[10]=RCT_other_aRFL;
						else
							value[10]=RCT_arfl;
						value[11]=RCT_pre;
						value[12]=0+"";
						value[13]="";
						tbl=new TableRow(this);
						//tbl.setBackgroundResource(R.drawable.roofhead); 
						tbl.setLayoutParams(lin_params);
						tbl.setGravity(Gravity.CENTER_VERTICAL);
						
						
						if(i%2==0)
						{
							
							tbl.setBackgroundResource(R.drawable.rooflist);
						}
						else
						{
							tbl.setBackgroundResource(R.drawable.rooflistw);
							
						}
						
						
						li_opt=new TableLayout(this);
						li_opt.setOrientation(LinearLayout.VERTICAL);
						li_spu=new TableRow(this);
						li_opt.addView(li_spu,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
						
						for(int j=0;j<arr_title.length;j++)
						{
							try
							{
								Ed=new TextView(this,null,R.attr.text_view);
							if(j<2)
							{
								Ed.setVisibility(View.VISIBLE);
								Ed.setTextColor(Color.BLACK);
								tbl.addView(Ed,width[j],LayoutParams.WRAP_CONTENT);
								Ed.setText(value[j]);
								View v=new View(this);
								v.setBackgroundResource(R.color.black);
								tbl.addView(v,1,LayoutParams.FILL_PARENT);
							}
							else
							{
								if((j-2)%colomprerow==0 && (j-2)!=0)
								{
									View v=new View(this);
									v.setBackgroundResource(R.color.row_head_line);
									li_opt.addView(v,LayoutParams.FILL_PARENT,1);
									
									li_spu=new TableRow(this);
									li_opt.addView(li_spu,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
								}
								else if((j-2)!=0)
								{
									View v=new View(this);
									v.setBackgroundResource(R.color.black);
									li_spu.addView(v,1,LayoutParams.FILL_PARENT);
								}
								
								if(j==4 || j==11 )
								{
									if(value[j].equals("true"))
									{
										ImageView  iv=new ImageView(this);
										iv.setBackgroundResource(R.drawable.tick_icon);
										iv.setVisibility(View.VISIBLE);
										RelativeLayout rl=new RelativeLayout(this);
										rl.setGravity(Gravity.CENTER);
										rl.addView(iv);
										li_spu.addView(rl,width[j],LayoutParams.WRAP_CONTENT);
									}
									else
									{
										
										Ed.setVisibility(View.VISIBLE);
										Ed.setTextColor(Color.BLACK);
									
										li_spu.addView(Ed,width[j],LayoutParams.WRAP_CONTENT);
										Ed.setText("N/A");
										
									}
									
								}
								else if(j==13)
								{
									LinearLayout li=new LinearLayout(this);
									ImageView im_up =new ImageView(this,null,R.attr.Roof_RCN_Tit_img);
																
									im_up.setTag(c.getString(c.getColumnIndex("id")));
									li.addView(im_up);
									im_up.setVisibility(View.VISIBLE);
									im_up.setPadding(5, 5, 0, 5);
									im_up.setOnClickListener(new RCT_edit_delete(1));
									ImageView im_dl =new ImageView(this,null,R.attr.Roof_RCN_opt_img);
									im_dl.setTag(cf.decode(c.getString(c.getColumnIndex("id"))));
									im_dl.setPadding(5, 5, 0, 5);
									im_dl.setVisibility(View.VISIBLE);
									im_dl.setOnClickListener(new RCT_edit_delete(2));
									li.addView(im_dl);
									li.setGravity(Gravity.CENTER);
									li_spu.addView(li);
								}
								else if(j==12)
					    		{
									Cursor c1=cf.arr_db.rawQuery("Select * from "+cf.Roofcoverimages+" Where RIM_masterid=(Select RM_masterid from "+cf.Roof_master+
						    				" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+Roof_page_value[5]+"' and RM_Covering='"+cf.encode(RCT_rct)+"') order by RIM_Order", null);
						    		
						    		if(c1.getCount()>0)
						    		{
					    			Ed.setText(Html.fromHtml("<u>View Photos ("+c1.getCount()+")</u>"));
					    			Ed.setOnClickListener(new RCT_edit_delete(3));
					    			Ed.setTag(cf.decode(c.getString(c.getColumnIndex("id"))));
					    			Ed.setTextColor(Color.BLUE);
					    			li_spu.addView(Ed);
						    		}
						    		else
						    		{
						    			Ed.setText("N/A");
						    			//Ed.setOnClickListener(new RCT_edit_delete(3));
						    			Ed.setTag(cf.decode(c.getString(c.getColumnIndex("id"))));
						    			li_spu.addView(Ed);
						    		}
									
					    		}
								else
								{
									
									Ed.setVisibility(View.VISIBLE);
									Ed.setTextColor(Color.BLACK);
									li_spu.addView(Ed,width[j],LayoutParams.WRAP_CONTENT);
									if(!value[j].equals(""))
										Ed.setText(value[j]);
									else
										Ed.setText("N/A");
								}
								
								
							}
						
						}catch (Exception e) {
							// TODO: handle exception
							System.out.println(" 	 "+e.getMessage()+" ="+j);
						}
						}
						tbl.addView(li_opt,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
						RCT_ShowValue.addView(tbl);
				//	c.moveToNext();
					
					}
					if(RCT_total_per==0 && RCT_pre_m)
					{
						findViewById(R.id.RCT_comp).setVisibility(View.VISIBLE);
					}
					else
					{
						findViewById(R.id.RCT_comp).setVisibility(View.INVISIBLE);
					}
			}
			else
			{
				findViewById(R.id.RCT_comp).setVisibility(View.INVISIBLE);	
			}
	}
	class RCT_edit_delete implements OnClickListener
	{
int type;
		public RCT_edit_delete(int i) {
			// TODO Auto-generated constructor stub
			type=i;
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			final String id=v.getTag().toString();
			if(type==1)
			{
				Edit_RCT(id);
			}
			else if(type==2)
			{
				final AlertDialog.Builder ab =new AlertDialog.Builder(Roof_commercial_information.this);
				ab.setTitle("Confirmation");
				ab.setMessage(Html.fromHtml("Do you want to delete the selected Roof Covering Type?"));
				ab.setIcon(R.drawable.alertmsg);
				ab.setCancelable(false);
				ab.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						cf.arr_db.execSQL(" DELETE FROM "+cf.Rct_table+" WHERE id='"+id+"'");
						cf.ShowToast("Roof Covering Type deleted successfully ", 0);
						show_RCT_values();
					}
					
				})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						
					}
				});
				AlertDialog al=ab.create();
				al.show();
			}
			else if(type==3)
			{
				Intent in=new Intent(Roof_commercial_information.this,RoofImagePicking.class);
			    in.putExtra("roof_cover_img", true);
				Cursor c =cf.SelectTablefunction(cf.Rct_table, " WHERE id='"+id+"'");
				c.moveToFirst();
				String cv="";
				if(c.getCount()>0)
				{
					cv= cf.decode(c.getString(c.getColumnIndex("RCT_rct")));	
				}
			    
			    try
			    {
			    	
			        in.putExtra("Cover", cv);
				    in.putExtra("Insp_id",Roof_page_value[5]);
				    in.putExtra("SRID",cf.selectedhomeid);
				    in.putExtra("readonly",true);
				    startActivityForResult(in, pick_img_code);
			    
			    }
			    catch (Exception e) {
					// TODO: handle exception
				}
			}
			
		}

		
		
	}
	private void Edit_RCT(String id) {
		// TODO Auto-generated method stub
		editid=1;
		Cursor c =cf.SelectTablefunction(cf.Rct_table, " WHERE id='"+id+"'");
		c.moveToFirst();
		if(c.getCount()>0)
		{
			((Button) findViewById(R.id.Roof_RCT_SA)).setText("Update");
			 String RCT_rct , RCT_PAD, RCT_yoi,  RCT_rsl, RCT_rsh , RCT_rst , RCT_bc , RCT_rc , RCT_arfl ,  RCT_Rsh_per
				, RCT_other_rct , RCT_other_yoi, RCT_other_rsh , RCT_other_rst , RCT_other_aRFL,RCT_pre,RCT_npv;
				 RCT_rct=cf.decode(c.getString(c.getColumnIndex("RCT_rct")));
				 RCT_PAD =cf.decode(c.getString(c.getColumnIndex("RCT_PAD")));
				 RCT_yoi=cf.decode(c.getString(c.getColumnIndex("RCT_yoi")));
				 RCT_rsl =cf.decode(c.getString(c.getColumnIndex("RCT_rsl")));
				 RCT_rsh=cf.decode(c.getString(c.getColumnIndex("RCT_rsh")));
				 RCT_rst =cf.decode(c.getString(c.getColumnIndex("RCT_rst")));
				 RCT_bc =cf.decode(c.getString(c.getColumnIndex("RCT_bc")));
				 RCT_rc =cf.decode(c.getString(c.getColumnIndex("RCT_rc")));
				 RCT_arfl =cf.decode(c.getString(c.getColumnIndex("RCT_arfl")));
				 RCT_Rsh_per=cf.decode(c.getString(c.getColumnIndex("RCT_Rsh_per")));
				 RCT_other_rct =cf.decode(c.getString(c.getColumnIndex("RCT_other_rct")));
				 RCT_other_yoi =cf.decode(c.getString(c.getColumnIndex("RCT_other_yoi")));
				 RCT_other_rsh=cf.decode(c.getString(c.getColumnIndex("RCT_other_rsh")));
				 RCT_other_rst=cf.decode(c.getString(c.getColumnIndex("RCT_other_rst")));
				 RCT_other_aRFL=cf.decode(c.getString(c.getColumnIndex("RCT_other_aRFL")));
				 RCT_pre=cf.decode(c.getString(c.getColumnIndex("RCT_pre")));
				 RCT_npv=cf.decode(c.getString(c.getColumnIndex("RCT_npv")));
				 sp_setvalues(Sp_RCT[0], RCT_rct,null);
				 sp_setvalues(Sp_RCT[1], RCT_yoi,null);
				 sp_setvalues(Sp_RCT[2], RCT_rsl,null);
				 sp_setvalues(Sp_RCT[3], RCT_rsh,null);
				 sp_setvalues(Sp_RCT[4], RCT_rst,null);
				 sp_setvalues(Sp_RCT[5], RCT_bc,null);
				 sp_setvalues(Sp_RCT[6], RCT_rc,null);
				 sp_setvalues(Sp_RCT[7], RCT_arfl,null);
				 RCT_other[0].setText(RCT_other_rct);
				 RCT_other[1].setText(RCT_other_yoi);
				 RCT_other[2].setText(RCT_other_rsh);
				 RCT_other[3].setText(RCT_other_rst);
				 RCT_other[4].setText(RCT_other_aRFL);
				 RCT_ed[0].setText(RCT_PAD);
				 RCT_ed[1].setText(RCT_Rsh_per);
				 if(RCT_npv.equals("true"))
				 {
					 	RCT_chk[0].setChecked(true);
					 	RCT_ed[0].setText("N/A");
						//RCT_bt[0].setVisibility(View.INVISIBLE);
				 }
				 if(RCT_pre.equals("true"))
					 	RCT_chk[1].setChecked(true);
				 
				 Sp_RCT[0].setEnabled(false);
		}
		if(c!=null)
			c.close();
	}
	private boolean duplicate_RCT(Spinner spinner) {
		// TODO Auto-generated method stub
		
		Cursor c =cf.SelectTablefunction(cf.Rct_table, " WHERE RCT_Ssrid='"+cf.selectedhomeid+"' and RCT_rct='"+cf.encode(Sp_RCT[0].getSelectedItem().toString().trim())+"' and  RCT_insp_id='"+Roof_page_value[5]+"'");
		if(!((Button) findViewById(R.id.Roof_RCT_SA)).getText().equals("Update"))
		{
		if(c.getCount()>0)
		{
			c.close();
			cf.ShowToast("Sorry selecteed Roof Covering Type already saved", 0);
			return false;
		}
		if(c!=null)
			c.close();
		}
		
		return true;
	}
	private boolean percent_validation(EditText editText, Spinner spinner,String string, boolean no_per) {
		// TODO Auto-generated method stub
		if(spinner.getSelectedItemPosition()!=0)
		{
			if(!editText.getText().toString().trim().equals(""))
			{
				int per =Integer.parseInt(editText.getText().toString());
				if(per>0 && per <=100){
					Cursor c =cf.SelectTablefunction(cf.Rct_table, " WHERE RCT_Ssrid='"+cf.selectedhomeid+"' and RCT_rct<>'"+cf.encode(Sp_RCT[0].getSelectedItem().toString().trim())+"' and  RCT_insp_id='"+Roof_page_value[5]+"'");
					if(c.getCount()>0)
					{ int total_per=0;
						c.moveToFirst();
						for(int i =0;i<c.getCount();i++,c.moveToNext())
						{
							 String RCT_Rsh_per=cf.decode(c.getString(c.getColumnIndex("RCT_Rsh_per")));
							 if(!RCT_Rsh_per.equals(""))
							 {
								 try
								 {
									total_per +=Integer.parseInt(RCT_Rsh_per);
								 }
								 catch (Exception e) {
									// TODO: handle exception
								}
							 }
						}
						if((total_per+per)>100)
						{
							cf.ShowToast("Percentage exceeds! Sum of Roof Shape (%) percentage should be 100.", 0);
							return false;
						}
						else
						{
							return true;
							
						}
					}
					else
					{
						return true;
					}
					
				}
				else
				{
					cf.ShowToast("Please enter valid percentage for "+string, 0);
				}
			}
			else
			{
				cf.ShowToast("Please enter percentage for "+string, 0);
			}
		}
		else
		{
			return true;
		}
		return false;
	}
	private boolean year_validation(EditText editText,String s) {
		// TODO Auto-generated method stub
		if(!editText.getText().toString().trim().equals(""))
		{
			int year=Integer.parseInt(editText.getText().toString());
			if(year>1000)
			{
				if(year<mYear)
				{
					return true;
				}
				else
				{
					cf.ShowToast("The entered year should not be greater than the current year in "+s, 0);
					return false;
				}
			}
			else
			{
				cf.ShowToast("Please enter the valid year in "+s, 0);
				return false;
			}
		}
		else{
		return true;
		}
	}
	private boolean spinner_validation(Spinner spinner, EditText editText,String string,boolean b) {
		// TODO Auto-generated method stub
		if(spinner.getSelectedItemPosition()!=0)
		{
			if(spinner.getSelectedItem().toString().trim().equals("Other") && editText!=null)
			{
				if(!editText.getText().toString().trim().equals(""))
				{
					return true;
				}
				else
				{
					cf.ShowToast("Please enter the other value for "+string, 0);
					return false;
				}
			}
			else 
			{
				
				return true;
			}
		}
		else if(b)
		{
			return true;
		}
		else
		{
			cf.ShowToast("Please select value for "+string, 0);
			return false;
		}
		
	}
	/***RCT table function ends****/
	/***Additional table function**/
	private void Additional_validation() {
		// TODO Auto-generated method stub
		
		if(R_ADD_WUWC.getCheckedRadioButtonId()!=-1)
		{
			if(R_ADD_PDRUC.getCheckedRadioButtonId()!=-1)
			{
				if(!R_ADD_PN.getText().toString().trim().equals("") || RC_Additional.findViewWithTag("pd_no").getVisibility()==View.GONE)
				{
					if(!R_ADD_PD.getText().toString().trim().equals("") || RC_Additional.findViewWithTag("pd_date").getVisibility()==View.GONE)
					{
						if(R_ADD_RME.getCheckedRadioButtonId()!=-1)
						{
							if(validate_RME() ||  RC_Additional.findViewWithTag("RMS").getVisibility()==View.GONE )
							{
								if(R_ADD_RMEL.getCheckedRadioButtonId()!=-1 ||  RC_Additional.findViewWithTag("RMS1").getVisibility()==View.GONE )
								{
									if(R_ADD_DRMS.getCheckedRadioButtonId()!=-1 ||  RC_Additional.findViewWithTag("RMS2").getVisibility()==View.GONE )
									{
										save_addeditonal();
									}
									else
									{
										cf.ShowToast("Please select value for "+getResources().getString(R.string.DamageR), 0);
									}
								}
								else
								{
									cf.ShowToast("Please select value for "+getResources().getString(R.string.EquipmentL), 0);
								}
							}
						}
						else
						{
							cf.ShowToast("Please select value for "+getResources().getString(R.string.COMER4), 0);
						}
						
					}else
					{
						cf.ShowToast("Please enter value for Date", 0);
					}	
				}else
				{
					cf.ShowToast("Please enter value for Permit No", 0);
				}
				
			}else
			{
				cf.ShowToast("Please select value for "+getResources().getString(R.string.COMER2), 0);
			}
			
		}else
		{
			cf.ShowToast("Please select value for "+getResources().getString(R.string.ROOF_Survey5), 0);
		}
		
		
		
	}
	private void save_addeditonal() {
		// TODO Auto-generated method stub
		String s="";
		String R_ADD_V_WUWC=remove_empty((R_ADD_WUWC.getCheckedRadioButtonId()!=-1)?R_ADD_WUWC.findViewById(R_ADD_WUWC.getCheckedRadioButtonId()).getTag().toString():""),
			   R_ADD_V_PDRUC=remove_empty((R_ADD_PDRUC.getCheckedRadioButtonId()!=-1)?R_ADD_PDRUC.findViewById(R_ADD_PDRUC.getCheckedRadioButtonId()).getTag().toString():"") ,
			   R_ADD_V_PN =remove_empty(R_ADD_PN.getText().toString()),
			   R_ADD_V_PD =remove_empty(R_ADD_PD.getText().toString()), 
			   R_ADD_V_RME=remove_empty(R_ADD_RME.findViewById(R_ADD_RME.getCheckedRadioButtonId()).getTag().toString()) ,
			   R_ADD_V_HVAC_E="false" ,R_ADD_V_VEN_E="false" ,R_ADD_V_STOR_E="false" ,
			   R_ADD_V_PIP=R_ADD_PIP.isChecked()+"" ,
			   R_ADD_V_clean_E=R_ADD_clean_E.isChecked()+"",
			   R_ADD_V_ANT_P =R_ADD_ANT_P.isChecked()+"",
			   R_ADD_V_LSE=R_ADD_LSE.isChecked()+"" ,
			   R_ADD_V_O_E=R_ADD_O_E.isChecked()+"" ,
			   R_ADD_V_RMEL=remove_empty((R_ADD_RMEL.getCheckedRadioButtonId()!=-1)?R_ADD_RMEL.findViewById(R_ADD_RMEL.getCheckedRadioButtonId()).getTag().toString():"") ,
			   R_ADD_V_DRMS =remove_empty((R_ADD_DRMS.getCheckedRadioButtonId()!=-1)?R_ADD_DRMS.findViewById(R_ADD_DRMS.getCheckedRadioButtonId()).getTag().toString():"") ,
			   R_ADD_V_other_I=remove_empty(R_ADD_other_I.getText().toString().trim());
			   if(R_ADD_HVAC_E.isChecked())
			   {
				   R_ADD_V_HVAC_E=remove_empty("true#&45"+R_ADD_HVAC_per.getText().toString().trim()+"#&45"+R_ADD_HVAC_loc.getText().toString().trim());
			   }
			   if(R_ADD_VEN_E.isChecked())
			   {
				   R_ADD_V_VEN_E=remove_empty("true#&45"+R_ADD_VEN_per.getText().toString().trim()+"#&45"+R_ADD_VEN_loc.getText().toString().trim());
			   }
			   if(R_ADD_STOR_E.isChecked())
			   {
				   R_ADD_V_STOR_E=remove_empty("true#&45"+R_ADD_STOR_per.getText().toString().trim()+"#&45"+R_ADD_STOR_loc.getText().toString().trim());
			   }
			   if(R_ADD_V_O_E.equals("true"))
			   {
				   R_ADD_V_O_E=remove_empty("true#&45"+R_ADD_Other_E.getText().toString().trim());
			   }
		
		Cursor c =cf.SelectTablefunction(cf.Roof_additional, " WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+Roof_page_value[5]+"'");
		if(c.getCount()>0)
		{
		//	R_ADD_Ssrid VARCHAR( 5 ) DEFAULT ( '' ), R_ADD_insp_id VARCHAR( 5 ) DEFAULT ( '' ), R_ADD_full VARCHAR( 5 ) DEFAULT ( '' ), R_ADD_scope VARCHAR( 150 ) DEFAULT ( '' ), R_ADD_na VARCHAR( 5 ) DEFAULT ( 'false' ), R_ADD_comment VARCHAR( 500 ) DEFAULT ( '' ), R_ADD_saved VARCHAR( 1 ) DEFAULT ( '1' )
			
			cf.arr_db.execSQL(" UPDATE "+cf.Roof_additional+" SET R_ADD_na='"+ADD_chk_top.isChecked()+"',R_ADD_WUWC ='"+R_ADD_V_WUWC+"',R_ADD_PDRUC ='"+R_ADD_V_PDRUC+"',R_ADD_PN ='"+R_ADD_V_PN+"',R_ADD_PD ='"+R_ADD_V_PD+"', R_ADD_RME ='"+R_ADD_V_RME+"',R_ADD_HVAC_E ='"+R_ADD_V_HVAC_E+"'," +
			"R_ADD_VEN_E ='"+R_ADD_V_VEN_E+"',R_ADD_STOR_E ='"+R_ADD_V_STOR_E+"',R_ADD_PIP ='"+R_ADD_V_PIP+"',R_ADD_clean_E='"+R_ADD_V_clean_E+"',R_ADD_ANT_P ='"+R_ADD_V_ANT_P+"',R_ADD_LSE ='"+R_ADD_V_LSE+"',R_ADD_O_E ='"+R_ADD_V_O_E+"',R_ADD_RMEL ='"+R_ADD_V_RMEL+"',R_ADD_DRMS ='"+R_ADD_V_DRMS+"',R_ADD_other_I='"+R_ADD_V_other_I+"',R_ADD_saved='1'" +
					" WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+Roof_page_value[5]+"'");
		}
		else
		{
			cf.arr_db.execSQL(" INSERT INTO "+cf.Roof_additional+" (R_ADD_Ssrid,R_ADD_insp_id,R_ADD_WUWC ,R_ADD_PDRUC ,R_ADD_PN ,R_ADD_PD , R_ADD_RME ,R_ADD_HVAC_E ,R_ADD_VEN_E ,R_ADD_STOR_E ,R_ADD_PIP ,R_ADD_clean_E,R_ADD_ANT_P ,R_ADD_LSE ,R_ADD_O_E,R_ADD_RMEL ,R_ADD_DRMS ,R_ADD_other_I) VALUES " +
					"('"+cf.selectedhomeid+"','"+Roof_page_value[5]+"','"+R_ADD_V_WUWC +"','"+R_ADD_V_PDRUC +"','"+R_ADD_V_PN +"','"+R_ADD_V_PD +"','"+ R_ADD_V_RME +"','"+R_ADD_V_HVAC_E +"','"+R_ADD_V_VEN_E +"','"+R_ADD_V_STOR_E +"','"+R_ADD_V_PIP +"','"+R_ADD_V_clean_E+"','"+R_ADD_V_ANT_P +"','"+R_ADD_V_LSE +"','"+R_ADD_V_O_E+"','"+R_ADD_V_RMEL +"','"+R_ADD_V_DRMS +"','"+R_ADD_V_other_I+"')");
		}
		findViewById(R.id.ADD_comp).setVisibility(View.VISIBLE);
		cf.ShowToast("Roof Additional information saved successfully", 0);
		if(c!=null)
			c.close();
	}
	private boolean validate_RME() {
		// TODO Auto-generated method stub
		if(R_ADD_HVAC_E .isChecked()||R_ADD_VEN_E .isChecked()||R_ADD_STOR_E .isChecked()||R_ADD_PIP .isChecked()||R_ADD_clean_E.isChecked()||R_ADD_ANT_P .isChecked()||R_ADD_LSE .isChecked()||R_ADD_O_E.isChecked())
		{
			if(R_ADD_HVAC_E .isChecked())
			{
				if(!R_ADD_HVAC_per.getText().toString().trim().equals(""))
				{
					  if(!Character.toString(R_ADD_HVAC_per.getText().toString().trim().charAt(0)).equals("0"))
		    		  {
	    			    	 
		    		  }
					  else
					  {
						  cf.ShowToast("Invalid Entry! Please enter the valid # of Units for HVAC Equipment.", 0);
						  R_ADD_HVAC_loc.setText("");
						  return false;
					  }					
				}
				else
				{
					cf.ShowToast("Please enter # of Units for HVAC Equipment", 0);
					return false;
				}
				if(R_ADD_HVAC_loc.getText().toString().trim().equals(""))
				{
					cf.ShowToast("Please enter location for HVAC Equipment", 0);
					return false;
				}
			}
			if(R_ADD_VEN_E.isChecked())
			{
				if(!R_ADD_VEN_per.getText().toString().trim().equals(""))
				{
					 if(!Character.toString(R_ADD_VEN_per.getText().toString().trim().charAt(0)).equals("0"))
		    		  {
	    			    	 
		    		  }
					  else
					  {
						  cf.ShowToast("Invalid Entry! Please enter the valid # of Units for Ventilation Equipment.", 0);
						  R_ADD_VEN_per.setText("");
						  return false;
					  }	
				}
				else
				{
					cf.ShowToast("Please enter # of Units for Ventilation Equipment", 0);
					return false;
				}
				if(R_ADD_VEN_loc.getText().toString().trim().equals(""))
				{
					cf.ShowToast("Please enter location for Ventilation Equipment", 0);
					return false;
				}
			}
			if(R_ADD_STOR_E.isChecked())
			{
				if(!R_ADD_STOR_per.getText().toString().trim().equals(""))
				{
					 if(!Character.toString(R_ADD_STOR_per.getText().toString().trim().charAt(0)).equals("0"))
		    		  {
	    			    	 
		    		  }
					  else
					  {
						  cf.ShowToast("Invalid Entry! Please enter the valid # of Units for Storage Facility.", 0);
						  R_ADD_STOR_per.setText("");
						  return false;
					  }
				}
				else
				{
					cf.ShowToast("Please enter # of Units for Storage Facility", 0);
					return false;
				}
				if(R_ADD_STOR_loc.getText().toString().trim().equals(""))
				{
					cf.ShowToast("Please enter location for Storage Facility", 0);
					return false;
				}
			}
			if(R_ADD_O_E.isChecked() && R_ADD_Other_E.getText().toString().trim().equals(""))
			{
				cf.ShowToast("Please enter value for Other Equipment", 0);
				return false;
			}
		}
		else
		{
			if(RC_Additional.findViewWithTag("RMS").getVisibility()==View.VISIBLE )
			{
				cf.ShowToast("Please select value for "+getResources().getString(R.string.COMER3), 0);
				return false;
			}
		}
		return true;
	}
	private void Additional_show_value()
	{
		Cursor c =cf.SelectTablefunction(cf.Roof_additional, " WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+Roof_page_value[5]+"'");
		if(c.getCount()>0)
		{
			c.moveToFirst();
			String R_ADD_V_WUWC=cf.decode(c.getString(c.getColumnIndex("R_ADD_WUWC"))), 
				R_ADD_V_PDRUC=cf.decode(c.getString(c.getColumnIndex("R_ADD_PDRUC"))) ,
				R_ADD_V_PN =cf.decode(c.getString(c.getColumnIndex("R_ADD_PN"))),
				R_ADD_V_PD =cf.decode(c.getString(c.getColumnIndex("R_ADD_PD"))), 
				R_ADD_V_RME =cf.decode(c.getString(c.getColumnIndex("R_ADD_RME"))),
				R_ADD_V_HVAC_E=cf.decode(c.getString(c.getColumnIndex("R_ADD_HVAC_E"))) ,
				R_ADD_V_VEN_E=cf.decode(c.getString(c.getColumnIndex("R_ADD_VEN_E"))) ,
				R_ADD_V_STOR_E=cf.decode(c.getString(c.getColumnIndex("R_ADD_STOR_E"))) ,
				R_ADD_V_PIP=cf.decode(c.getString(c.getColumnIndex("R_ADD_PIP"))) ,
				R_ADD_V_clean_E=cf.decode(c.getString(c.getColumnIndex("R_ADD_clean_E"))),
				R_ADD_V_ANT_P=cf.decode(c.getString(c.getColumnIndex("R_ADD_ANT_P"))) ,
				R_ADD_V_LSE=cf.decode(c.getString(c.getColumnIndex("R_ADD_LSE"))) ,
				R_ADD_V_O_E=cf.decode(c.getString(c.getColumnIndex("R_ADD_O_E"))),
				R_ADD_V_RMEL =cf.decode(c.getString(c.getColumnIndex("R_ADD_RMEL"))),
				R_ADD_V_DRMS=cf.decode(c.getString(c.getColumnIndex("R_ADD_DRMS"))) ,
				R_ADD_V_other_I=cf.decode(c.getString(c.getColumnIndex("R_ADD_other_I")));
			
			if(R_ADD_WUWC.findViewWithTag(R_ADD_V_WUWC)!=null)
			{
				((RadioButton) R_ADD_WUWC.findViewWithTag(R_ADD_V_WUWC)).setChecked(true);
			}
			if(R_ADD_PDRUC.findViewWithTag(R_ADD_V_PDRUC)!=null)
			{
				((RadioButton) R_ADD_PDRUC.findViewWithTag(R_ADD_V_PDRUC)).setChecked(true);
			}
			if(R_ADD_RME.findViewWithTag(R_ADD_V_RME)!=null)
			{
				((RadioButton) R_ADD_RME.findViewWithTag(R_ADD_V_RME)).setChecked(true);
			}
			R_ADD_PN.setText(R_ADD_V_PN);
			R_ADD_PD.setText(R_ADD_V_PD);
			if(R_ADD_V_HVAC_E.contains("#&45"))
			{
				R_ADD_HVAC_E.setChecked(true);
				R_ADD_HVAC_per.setEnabled(true);
				R_ADD_HVAC_loc.setEnabled(true);
				String[] temp=R_ADD_V_HVAC_E.split("#&45");
				R_ADD_HVAC_per.setText(temp[1]);
				R_ADD_HVAC_loc.setText(temp[2]);
			}
			else
			{
				R_ADD_HVAC_E.setChecked(false);
			}
			if(R_ADD_V_VEN_E.contains("#&45"))
			{
				R_ADD_VEN_E.setChecked(true);
				R_ADD_VEN_per.setEnabled(true);
				R_ADD_VEN_loc.setEnabled(true);
				String[] temp=R_ADD_V_VEN_E.split("#&45");
				R_ADD_VEN_per.setText(temp[1]);
				R_ADD_VEN_loc.setText(temp[2]);
			}
			else
			{
				R_ADD_VEN_E.setChecked(false);
			}
			if(R_ADD_V_STOR_E.contains("#&45"))
			{
				R_ADD_STOR_E.setChecked(true);
				R_ADD_STOR_per.setEnabled(true);
				R_ADD_STOR_loc.setEnabled(true);
				String[] temp=R_ADD_V_STOR_E.split("#&45");
				R_ADD_STOR_per.setText(temp[1]);
				R_ADD_STOR_loc.setText(temp[2]);
			}
			else
			{
				R_ADD_STOR_E.setChecked(false);
			}
			if(R_ADD_V_PIP.equals("true"))
			{
				R_ADD_PIP.setChecked(true);
			}
			if(R_ADD_V_clean_E.equals("true"))
			{
				R_ADD_clean_E.setChecked(true);
			}
			if(R_ADD_V_ANT_P.equals("true"))
			{
				R_ADD_ANT_P.setChecked(true);
			}
			if(R_ADD_V_LSE.equals("true"))
			{
				R_ADD_LSE.setChecked(true);
			}
			System.out.println("R_ADD_V_O_E="+R_ADD_V_O_E);
			if(R_ADD_V_O_E.contains("#&45"))
			{
				R_ADD_O_E.setChecked(true);
				R_ADD_Other_E.setVisibility(View.VISIBLE);
				String[] temp=R_ADD_V_O_E.split("#&45");
				System.out.println("R_ADD_V_O_E 1="+temp[1]);
				R_ADD_Other_E.setText(temp[1]);
			}
			if(R_ADD_RMEL.findViewWithTag(R_ADD_V_RMEL)!=null)
			{
				((RadioButton) R_ADD_RMEL.findViewWithTag(R_ADD_V_RMEL)).setChecked(true);
			}
			if(R_ADD_DRMS.findViewWithTag(R_ADD_V_DRMS)!=null)
			{
				((RadioButton) R_ADD_DRMS.findViewWithTag(R_ADD_V_DRMS)).setChecked(true);
			}
			R_ADD_other_I.setText(R_ADD_V_other_I);
			
			//(add_rg.getCheckedRadioButtonId()!=-1)? add_rg.findViewWithTag(add_rg.getCheckedRadioButtonId()).getTag().toString():"";
			
			
		}
		if(c!=null)
			c.close();
	}
	/***Additional table function**/
	private void clear_ADD()
	{
		R_ADD_WUWC.clearCheck(); 
		R_ADD_PDRUC.clearCheck();
		R_ADD_RME.clearCheck();
		R_ADD_other_I.setText("");
		
		R_ADD_RMEL.clearCheck();
		R_ADD_DRMS.clearCheck();
		
		R_ADD_HVAC_E.setChecked(false);
		R_ADD_VEN_E.setChecked(false);
		R_ADD_STOR_E.setChecked(false);
		R_ADD_PIP.setChecked(false);
		R_ADD_clean_E.setChecked(false);
		R_ADD_ANT_P.setChecked(false);
		R_ADD_LSE.setChecked(false);
		R_ADD_O_E.setChecked(false);
		R_ADD_other_I.setText("");
		
		R_ADD_PN.setText("");
		R_ADD_PD.setText("");
		R_ADD_HVAC_loc.setText("");
		R_ADD_VEN_loc.setText("");
		R_ADD_STOR_loc.setText("");
		R_ADD_HVAC_per.setText("");
		R_ADD_VEN_per.setText("");
		R_ADD_STOR_per.setText("");
		R_ADD_Other_E.setText("");
		
		
	}
	/**RCN table function***/
	private void Show_RCN_values() {
		// TODO Auto-generated method stub
		Cursor c =cf.SelectTablefunction(cf.RCN_table, " WHERE R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id='"+Roof_page_value[5]+"' ");
		if(c.getCount()>0)
		{
			
			c.moveToFirst();
			String R_RCN_HRMS=cf.decode(c.getString(c.getColumnIndex("R_RCN_HRMS"))),
				  R_RCN_FC=cf.decode(c.getString(c.getColumnIndex("R_RCN_FC"))),
				  R_RCN_GRAC=cf.decode(c.getString(c.getColumnIndex("R_RCN_GRAC"))), 
				  R_RCN_RLN=cf.decode(c.getString(c.getColumnIndex("R_RCN_RLN"))), 
				  R_RCN_RPN=cf.decode(c.getString(c.getColumnIndex("R_RCN_RPN"))),
				  R_RCN_HREBR=cf.decode(c.getString(c.getColumnIndex("R_RCN_HREBR"))),
				  R_RCN_WORR=cf.decode(c.getString(c.getColumnIndex("R_RCN_WORR"))),
				  R_RCN_RMRA=cf.decode(c.getString(c.getColumnIndex("R_RCN_RMRA"))),
				  R_RCN_SFPE=cf.decode(c.getString(c.getColumnIndex("R_RCN_SFPE"))),
				  R_RCN_Amount=cf.decode(c.getString(c.getColumnIndex("R_RCN_Amount"))),
				  R_RCN_EVPWD=cf.decode(c.getString(c.getColumnIndex("R_RCN_EVPWD"))),
				  R_RCN_RME=cf.decode(c.getString(c.getColumnIndex("R_RCN_RME"))),
				  R_RCN_BFP=cf.decode(c.getString(c.getColumnIndex("R_RCN_BFP"))),
				  R_RCN_ESM=cf.decode(c.getString(c.getColumnIndex("R_RCN_ESM"))),
				  R_RCN_SOSAS=cf.decode(c.getString(c.getColumnIndex("R_RCN_SOSAS"))),
				  R_RCN_DRS=cf.decode(c.getString(c.getColumnIndex("R_RCN_DRS")));
			
		if(!R_RCN_HRMS.equals(""))
		{
			
			set_value_radio(RCN_RG[0],R_RCN_GRAC,RCN_other[0]);
			set_value_radio(RCN_RG[1],R_RCN_RLN,RCN_other[1]);
			set_value_radio(RCN_RG[2],R_RCN_RPN,RCN_other[2]);
			set_value_radio(RCN_RG[3],R_RCN_HREBR,RCN_other[3]);
			set_value_radio(RCN_RG[4],R_RCN_WORR,RCN_other[4]);
			set_value_radio(RCN_RG[5],R_RCN_RMRA,RCN_other[5]);
			set_value_radio(RCN_RG[6],R_RCN_SFPE,RCN_other[6]);
			set_value_radio(RCN_RG[7],R_RCN_EVPWD,RCN_other[7]);
			set_value_radio(RCN_RG[8],R_RCN_RME,RCN_other[8]);
			set_value_radio(RCN_RG[9],R_RCN_BFP,RCN_other[9]);
			set_value_radio(RCN_RG[10],R_RCN_ESM,RCN_other[10]);
			set_value_radio(RCN_RG[11],R_RCN_SOSAS,RCN_other[11]);
			set_value_radio(RCN_RG[12],R_RCN_DRS,RCN_other[12]);
			set_value_radio(RCN_RG[13],R_RCN_HRMS,RCN_other[13]);
			if(!R_RCN_FC.equals(""))
			{
				for(int j=0;j<RCN_chk_FC.length;j++)
						{
							if(R_RCN_FC.contains(RCN_chk_FC[j].getText().toString().trim()))
							{
								
								RCN_chk_FC[j].setChecked(true);
								
							}
						}
			}
			if(!Amount.equals(""))
			{
				Amount.setText(R_RCN_Amount);
			}
			
		}
		else
		{
			for(int i=0;i<RCN_RG.length;i++)
			{
				if(RCN_RG[i].findViewWithTag("BSI")!=null)
				{
					System.out.println(" the value "+i);
					((RadioButton) RCN_RG[i].findViewWithTag("BSI")).setChecked(true);
				}
			}
		}
		}
		else
		{
			for(int i=0;i<RCN_RG.length;i++)
			{
				if(RCN_RG[i].findViewWithTag("BSI")!=null)
				{
					System.out.println(" the value "+i);
					((RadioButton) RCN_RG[i].findViewWithTag("BSI")).setChecked(true);
				}
			}
		}
		if(c!=null)
			c.close();
		Show_dynamic_RCN();
	}
	private void set_value_radio(RadioGroup radioGroup, String RG_val,
			EditText editText) {
		// TODO Auto-generated method stub
		String other_val=null;
		if(!RG_val.equals(""))
		{
			if(RG_val.contains("#&45"))
			{
				String[]tem=RG_val.split("#&45");
				RG_val=tem[0];
				other_val=tem[1];
			}
			if(radioGroup.findViewWithTag(RG_val.trim())!=null)
			{
				((RadioButton) radioGroup.findViewWithTag(RG_val.trim())).setChecked(true);
			}
		}
		if(other_val!=null)
		{
			editText.setText(other_val);
		}
		
	}
	public void clear_RCN() {
		// TODO Auto-generated method stub
		for(int i =0;i<RCN_RG.length;i++)
		{
			RCN_RG[i].clearCheck();
			if(RCN_RG[i].findViewWithTag("BSI")!=null)
			{
				((RadioButton)RCN_RG[i].findViewWithTag("BSI")).setChecked(true);
			}
			else
			{
				RCN_RG[i].clearCheck();
			}
			
				
		}
		
		cf.arr_db.execSQL("DELETE FROM "+cf.RCN_table_dy+" WHERE R_RCN_D_Ssrid='"+cf.selectedhomeid+"' and R_RCN_D_insp_id='"+Roof_page_value[5]+"' and R_RCN_D_saved='0'");
		Show_RCN_values();
	}
	class RCN_click implements OnCheckedChangeListener
	{
		EditText comments;
		TextView tv;
		String default_v;
		View hide=null;
		public RCN_click(EditText editText, TextView textView,String s) {
			// TODO Auto-generated constructor stub
			comments=editText;
			tv=textView;
			default_v=s;
			
			if(comments!=null)
			{
				comments.addTextChangedListener(new TextWatcher() {

				
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					// TODO Auto-generated method stub
					cf.showing_limit(s.toString(), tv, 250);
				}
				
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub
					
				}
			});
			}
			}
		public RCN_click(EditText editText, TextView textView,String s,View v) {
			// TODO Auto-generated constructor stub
			comments=editText;
			tv=textView;
			default_v=s;
			hide=v;
			if(comments!=null)
			{
				comments.addTextChangedListener(new TextWatcher() {

				
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					// TODO Auto-generated method stub
					cf.showing_limit(s.toString(), tv, 250);
				}
				
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub
					
				}
			});
			}
			}

		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
		try
		{
			if(((RadioButton)group.findViewWithTag(default_v)).isChecked())
			{
				if(comments!=null)
				{
					comments.setVisibility(View.VISIBLE);
					tv.setVisibility(View.VISIBLE);
				}
				if(hide!=null)
				{
					hide.setVisibility(View.VISIBLE);
				}
			}
			else
			{
				if(comments!=null)
				{
					comments.setText("");
					comments.setVisibility(View.GONE);
					tv.setVisibility(View.GONE);
				}
				if(hide!=null)
				{
					if(hide.getTag().equals("HRMS"))
					{
						for(int i =0;i<RCN_chk_FC.length;i++)
						{
							RCN_chk_FC[i].setChecked(false);
						}
					}
					if(hide.getTag().equals("RPN"))
					{
						RCN_RG[5].clearCheck();
					}
					if(hide.getTag().equals("SFPE"))
					{
						Amount.setText("");
					}
					
					hide.setVisibility(View.GONE);
				}
			
		}
		}
		catch (Exception e) {
			// TODO: handle exception
			System.out.println(" the eror"+e.getMessage()+default_v);
			
		}
		
	}
}
	private void RCN_validation() {
		// TODO Auto-generated method stub
		String[] validation_txt={"Roof Appears in Satisfactory Condition","Roof Leakage Noted","Roof Ponding Noted","Has Roof ever been Replaced","Was Old Roof Removed","Roof Maintenance Records Available","Is There A Specific Fund In Place That Is Earmarked For Roof Replacement",
				"Is There Any Evidence of Previous Water Damage","Is There Roofing Meterial Extended Up the parapet","Is There Base Flashing at Parapet","Is There Any Evidence of Stretching of the Membrane","Are all Seams Overlapped Sufficiently to Avoid Seepage","Is There Any Damage to the Roof Structure (Cuts, Holes, ect)","Has The Roof Mounted Structure Been AdeQuately Installed To Pervent Water Seepage Into The Structure"}; 
		for(int i=0;i< RCN_RG.length;i++)
		{
			if(RCN_RG[i].getCheckedRadioButtonId()!=-1 ||(findViewById(R.id.Roof_RCN_TR5).getVisibility()==View.GONE && i==3))
			{
				if(RCN_other[i]!=null )
				{
					if(RCN_other[i].getVisibility()==View.VISIBLE && RCN_other[i].getText().toString().trim().equals(""))
					{
						cf.ShowToast("Please enter comments for "+validation_txt[i], 0);
						return;
					}
				}				
			}
			else
			{
				cf.ShowToast("Please select value for "+validation_txt[i], 0);
				return;
			}
			if(i==13)
			{
				if(!RCN_chk_FC[0].isChecked() && !RCN_chk_FC[1].isChecked() && !RCN_chk_FC[2].isChecked()&& !RCN_chk_FC[3].isChecked() && RCN_Condition_N_tbl.findViewWithTag("HRMS").getVisibility()==View.VISIBLE)
				{
					cf.ShowToast("Please select value  for Flashing/Coping", 0);
					return;
				}
			}
			if(i==7)
			{
				if(Amount.getText().toString().trim().equals("") && RCN_Condition_N_tbl.findViewWithTag("SFPE").getVisibility()==View.VISIBLE)
				{
					cf.ShowToast("Please select value  for Amount under "+validation_txt[i], 0);
					return;
				}
			}
		}
		Cursor c = cf.SelectTablefunction(cf.RCN_table_dy, " WHERE R_RCN_D_Ssrid='"+cf.selectedhomeid+"' and R_RCN_D_insp_id='"+Roof_page_value[5]+"' ");
		if(c.getCount()>0)
		{
			TableLayout tbl_dy=((TableLayout)findViewById(R.id.RCN_Condition_N_DY_tbl));
			c.moveToFirst();
			for(int i=0;i<c.getCount();i++,c.moveToNext())
			{
				int id=c.getInt(c.getColumnIndex("id"));
				if(tbl_dy.findViewById(id+1000)!=null)
				{
					RadioGroup rl =(RadioGroup) tbl_dy.findViewById(id+1000);
					if(rl.getCheckedRadioButtonId()!=-1)
					{
						if(tbl_dy.findViewById(id+2000)!=null)
						{
							EditText ed =(EditText) tbl_dy.findViewById(id+2000);
							if(ed.getVisibility()==View.VISIBLE && ed.getText().toString().trim().equals(""))
							{
								cf.ShowToast("Please enter comments for "+cf.decode(c.getString(c.getColumnIndex("R_RCN_D_title"))), 0);
								return;
							}
						}
						
					}
					else
					{
						cf.ShowToast("Please select value for "+cf.decode(c.getString(c.getColumnIndex("R_RCN_D_title"))), 0);
						return;
					}
				}
			}
		}
		if(c!=null)
			c.close();System.out.println("came last");
		save_RCN1();
	}
	private void save_RCN1() {
		// TODO Auto-generated method stub
		String scope="";
		String HRMS=get_rg_value(RCN_RG[13],RCN_other[13]),
		FC="",
		GRAC=get_rg_value(RCN_RG[0],RCN_other[0]),
		RLN=get_rg_value(RCN_RG[1],RCN_other[1]),
		RPN=get_rg_value(RCN_RG[2],RCN_other[2]),
		HREBR=get_rg_value(RCN_RG[3],RCN_other[3]),
		WORR=get_rg_value(RCN_RG[4],RCN_other[4]),
		RMRA=get_rg_value(RCN_RG[5],RCN_other[5]),
		SFPE=get_rg_value(RCN_RG[6],RCN_other[6]),
		Amount_v=cf.encode(Amount.getText().toString().trim()),
		EVPWD=get_rg_value(RCN_RG[7],RCN_other[7]),
		RME=get_rg_value(RCN_RG[8],RCN_other[8]),
		BFP=get_rg_value(RCN_RG[9],RCN_other[9]),
		ESM=get_rg_value(RCN_RG[10],RCN_other[10]),
		SOSAS=get_rg_value(RCN_RG[11],RCN_other[11]),
		DRS=get_rg_value(RCN_RG[12],RCN_other[12]);
		
		
		System.out.println("came save_RCN1");
		Cursor c =cf.SelectTablefunction(cf.RCN_table, " WHERE R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id='"+Roof_page_value[5]+"'");
		System.out.println("cccc="+c.getCount());
		if(c.getCount()>0  || RCN_chk_top.isChecked())
		{
			/*if(!RC_cmt.getText().toString().trim().equals(""))
			{*/
				//cf.arr_db.execSQL(" UPDATE "+cf.Roof_additional+" SET R_ADD_scope='"+cf.encode(scope)+"',R_ADD_na='"+ADD_chk_top.isChecked()+"',R_ADD_comment='"+cf.encode(RC_cmt.getText().toString().trim())+"',R_ADD_citizen='"+((CheckBox)findViewById(R.id.includeCitizenForm)).isChecked()+"'  WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+Roof_page_value[5]+"'");
				 c =cf.SelectTablefunction(cf.RCN_table, " WHERE R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id='"+Roof_page_value[5]+"' ");
				 System.out.println("FFF"+c.getCount());
				if(c.getCount()>0)
				{
					cf.arr_db.execSQL(" UPDATE "+cf.RCN_table+" SET R_RCN_saved='1',R_RCN_na='"+RCN_chk_top.isChecked()+"'  WHERE R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id='"+Roof_page_value[5]+"'");
				}
				else
				{
					cf.arr_db.execSQL(" INSERT INTO "+cf.RCN_table+" (R_RCN_na,R_RCN_Ssrid,R_RCN_insp_id,R_RCN_saved) VALUES ('"+RCN_chk_top.isChecked()+"','"+cf.selectedhomeid+"','"+Roof_page_value[5]+"','1')");
				}
				
			/*}
			else
			{
				cf.ShowToast("Please enter Roof Comments", 0);
			}*/
		}System.out.println("xomeprlerp");
		
		
		for(int i=0;i<RCN_chk_FC.length;i++)
		{
			if(RCN_chk_FC[i].isChecked())
			{
				FC+=RCN_chk_FC[i].getText().toString().trim()+",";
			}
			
		}
		if(FC.endsWith(","))
		{
			FC=FC.substring(0,FC.length()-1);
		}
		FC=cf.encode(FC);System.out.println("FC="+FC);
		Cursor c1 =cf.SelectTablefunction(cf.RCN_table, " WHERE R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id='"+Roof_page_value[5]+"' ");
		System.out.println("FCcccc="+c1.getCount());
		if(c1.getCount()<=0)
		{
			cf.arr_db.execSQL(" INSERT INTO "+cf.RCN_table+" ( R_RCN_Ssrid,R_RCN_insp_id,R_RCN_HRMS,R_RCN_FC,R_RCN_GRAC,R_RCN_RLN,R_RCN_RPN,R_RCN_HREBR,R_RCN_WORR,R_RCN_RMRA,R_RCN_SFPE,R_RCN_Amount,R_RCN_EVPWD,R_RCN_RME,R_RCN_BFP,R_RCN_ESM,R_RCN_SOSAS,R_RCN_DRS) values" +
					"('"+cf.selectedhomeid+"','"+Roof_page_value[5]+"','"+HRMS+"','"+FC+"','"+GRAC+"','"+RLN+"','"+RPN+"','"+HREBR+"','"+WORR+"','"+RMRA+"','"+SFPE+"','"+Amount_v+"','"+EVPWD+"','"+RME+"','"+BFP+"','"+ESM+"','"+SOSAS+"','"+DRS+"')");
		}
		else
		{
			
			cf.arr_db.execSQL(" UPDATE "+cf.RCN_table+" SET R_RCN_na='"+RCN_chk_top.isChecked()+"',R_RCN_HRMS='"+HRMS+"',R_RCN_FC='"+FC+"',R_RCN_GRAC='"+GRAC+"',R_RCN_RLN='"+RLN+"',R_RCN_RPN='"+RPN+"',R_RCN_HREBR='"+HREBR+"',R_RCN_WORR='"+WORR+"',R_RCN_RMRA='"+RMRA+"',R_RCN_SFPE='"+SFPE+"',R_RCN_Amount='"+Amount_v+"',R_RCN_EVPWD='"+EVPWD+"',R_RCN_RME='"+RME+"',R_RCN_BFP='"+BFP+"',R_RCN_ESM='"+ESM+"',R_RCN_SOSAS='"+SOSAS+"',R_RCN_DRS='"+DRS+"',R_RCN_saved='1' WHERE  R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id='"+Roof_page_value[5]+"' ");
		}
		System.out.println("FCccompel");
		/***Dynamic table enty***/
		c=cf.SelectTablefunction(cf.RCN_table_dy, " WHERE R_RCN_D_Ssrid='"+cf.selectedhomeid+"' and R_RCN_D_insp_id='"+Roof_page_value[5]+"' ");
		if(c.getCount()>0)
		{
			c.moveToFirst();
			TableLayout tbl_dy=((TableLayout)findViewById(R.id.RCN_Condition_N_DY_tbl));
			for(int i=0;i<c.getCount();i++,c.moveToNext())
			{
				String option="",other="";
				int id=c.getInt(c.getColumnIndex("id"));
				if(tbl_dy.findViewById(id+1000)!=null)
				{
					RadioGroup rl =(RadioGroup) tbl_dy.findViewById(id+1000);
					if(rl.getCheckedRadioButtonId()!=-1)
					{
						option= findViewById(rl.getCheckedRadioButtonId()).getTag().toString();
						if(tbl_dy.findViewById(id+2000)!=null)
						{
							other =((EditText) tbl_dy.findViewById(id+2000)).getText().toString().trim();
							
						}
						
					}
					
				}
				cf.arr_db.execSQL(" UPDATE "+cf.RCN_table_dy+" SET  R_RCN_D_saved='1',R_RCN_D_option='"+cf.encode(option)+"',R_RCN_D_Other='"+cf.encode(other)+"' WHERE id='"+id+"'");
			}
		}
		
		
		if(ADD_chk_top.isChecked())
		{
			System.out.println("ARR_TOP");
			 R_ADD_V_WUWC="";R_ADD_V_PDRUC="" ;R_ADD_V_PN ="";R_ADD_V_PD ="";R_ADD_V_RME="" ;R_ADD_V_HVAC_E="false" ;R_ADD_V_VEN_E="false" ;R_ADD_V_STOR_E="false" ;
					R_ADD_V_PIP="" ;R_ADD_V_clean_E="";R_ADD_V_ANT_P ="";R_ADD_V_LSE="" ;R_ADD_V_O_E="" ;R_ADD_V_RMEL="";
					R_ADD_V_DRMS ="";R_ADD_V_other_I="";
		}
		else
		{
			System.out.println("ARR_TOP ELSE");
			 R_ADD_V_WUWC=remove_empty((R_ADD_WUWC.getCheckedRadioButtonId()!=-1)?R_ADD_WUWC.findViewById(R_ADD_WUWC.getCheckedRadioButtonId()).getTag().toString():"");
					   R_ADD_V_PDRUC=remove_empty((R_ADD_PDRUC.getCheckedRadioButtonId()!=-1)?R_ADD_PDRUC.findViewById(R_ADD_PDRUC.getCheckedRadioButtonId()).getTag().toString():"") ;
					   R_ADD_V_PN =remove_empty(R_ADD_PN.getText().toString());
					   R_ADD_V_PD =remove_empty(R_ADD_PD.getText().toString()); 
					   R_ADD_V_RME=remove_empty(R_ADD_RME.findViewById(R_ADD_RME.getCheckedRadioButtonId()).getTag().toString());
					   R_ADD_V_HVAC_E="false" ;R_ADD_V_VEN_E="false" ;R_ADD_V_STOR_E="false";
					   R_ADD_V_PIP=R_ADD_PIP.isChecked()+"";
					   R_ADD_V_clean_E=R_ADD_clean_E.isChecked()+"";
					   R_ADD_V_ANT_P =R_ADD_ANT_P.isChecked()+"";
					   R_ADD_V_LSE=R_ADD_LSE.isChecked()+"" ;
					   R_ADD_V_O_E=R_ADD_O_E.isChecked()+"" ;
					   R_ADD_V_RMEL=remove_empty((R_ADD_RMEL.getCheckedRadioButtonId()!=-1)?R_ADD_RMEL.findViewById(R_ADD_RMEL.getCheckedRadioButtonId()).getTag().toString():"") ;
					   R_ADD_V_DRMS =remove_empty((R_ADD_DRMS.getCheckedRadioButtonId()!=-1)?R_ADD_DRMS.findViewById(R_ADD_DRMS.getCheckedRadioButtonId()).getTag().toString():"") ;
					   R_ADD_V_other_I=remove_empty(R_ADD_other_I.getText().toString().trim());
			  if(R_ADD_HVAC_E.isChecked())
			   {
				   R_ADD_V_HVAC_E=remove_empty("true#&45"+R_ADD_HVAC_per.getText().toString().trim()+"#&45"+R_ADD_HVAC_loc.getText().toString().trim());
			   }
			   if(R_ADD_VEN_E.isChecked())
			   {
				   R_ADD_V_VEN_E=remove_empty("true#&45"+R_ADD_VEN_per.getText().toString().trim()+"#&45"+R_ADD_VEN_loc.getText().toString().trim());
			   }
			   if(R_ADD_STOR_E.isChecked())
			   {
				   R_ADD_V_STOR_E=remove_empty("true#&45"+R_ADD_STOR_per.getText().toString().trim()+"#&45"+R_ADD_STOR_loc.getText().toString().trim());
			   }
			   if(R_ADD_V_O_E.equals("true"))
			   {
				   R_ADD_V_O_E=remove_empty("true#&45"+R_ADD_Other_E.getText().toString().trim());
			   }
			   System.out.println("ARR_TOP Roof_additional"+R_ADD_V_WUWC+"test"+R_ADD_V_PDRUC);
		}

			String s="";
			
			System.out.println("R_ADD_V_other_I");
						 
				   
			Cursor c5 =cf.SelectTablefunction(cf.Roof_additional, " WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+Roof_page_value[5]+"'");
			System.out.println("c5sd="+c5.getCount());
			if(c5.getCount()>0)
			{
			//	R_ADD_Ssrid VARCHAR( 5 ) DEFAULT ( '' ), R_ADD_insp_id VARCHAR( 5 ) DEFAULT ( '' ), R_ADD_full VARCHAR( 5 ) DEFAULT ( '' ), R_ADD_scope VARCHAR( 150 ) DEFAULT ( '' ), R_ADD_na VARCHAR( 5 ) DEFAULT ( 'false' ), R_ADD_comment VARCHAR( 500 ) DEFAULT ( '' ), R_ADD_saved VARCHAR( 1 ) DEFAULT ( '1' )
				System.out.println(" UPDATE "+cf.Roof_additional+" SET R_ADD_na='"+ADD_chk_top.isChecked()+"',R_ADD_WUWC ='"+R_ADD_V_WUWC+"',R_ADD_PDRUC ='"+R_ADD_V_PDRUC+"',R_ADD_PN ='"+R_ADD_V_PN+"',R_ADD_PD ='"+R_ADD_V_PD+"', R_ADD_RME ='"+R_ADD_V_RME+"',R_ADD_HVAC_E ='"+R_ADD_V_HVAC_E+"'," +
				"R_ADD_VEN_E ='"+R_ADD_V_VEN_E+"',R_ADD_STOR_E ='"+R_ADD_V_STOR_E+"',R_ADD_PIP ='"+R_ADD_V_PIP+"',R_ADD_clean_E='"+R_ADD_V_clean_E+"',R_ADD_ANT_P ='"+R_ADD_V_ANT_P+"',R_ADD_LSE ='"+R_ADD_V_LSE+"',R_ADD_O_E ='"+R_ADD_V_O_E+"',R_ADD_RMEL ='"+R_ADD_V_RMEL+"',R_ADD_DRMS ='"+R_ADD_V_DRMS+"',R_ADD_other_I='"+R_ADD_V_other_I+"',R_ADD_saved='2',R_RCT_na='"+RCT_chk_top.isChecked()+"',R_ADD_scope='"+cf.encode(scope)+"',R_ADD_comment='"+cf.encode(RC_cmt.getText().toString().trim())+"',R_ADD_citizen='"+((CheckBox)findViewById(R.id.includeCitizenForm)).isChecked()+"' " +
						" WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+Roof_page_value[5]+"'");
				cf.arr_db.execSQL(" UPDATE "+cf.Roof_additional+" SET R_ADD_na='"+ADD_chk_top.isChecked()+"',R_ADD_WUWC ='"+R_ADD_V_WUWC+"',R_ADD_PDRUC ='"+R_ADD_V_PDRUC+"',R_ADD_PN ='"+R_ADD_V_PN+"',R_ADD_PD ='"+R_ADD_V_PD+"', R_ADD_RME ='"+R_ADD_V_RME+"',R_ADD_HVAC_E ='"+R_ADD_V_HVAC_E+"'," +
				"R_ADD_VEN_E ='"+R_ADD_V_VEN_E+"',R_ADD_STOR_E ='"+R_ADD_V_STOR_E+"',R_ADD_PIP ='"+R_ADD_V_PIP+"',R_ADD_clean_E='"+R_ADD_V_clean_E+"',R_ADD_ANT_P ='"+R_ADD_V_ANT_P+"',R_ADD_LSE ='"+R_ADD_V_LSE+"',R_ADD_O_E ='"+R_ADD_V_O_E+"',R_ADD_RMEL ='"+R_ADD_V_RMEL+"',R_ADD_DRMS ='"+R_ADD_V_DRMS+"',R_ADD_other_I='"+R_ADD_V_other_I+"',R_ADD_saved='2',R_RCT_na='"+RCT_chk_top.isChecked()+"',R_ADD_scope='"+cf.encode(scope)+"',R_ADD_comment='"+cf.encode(RC_cmt.getText().toString().trim())+"',R_ADD_citizen='"+((CheckBox)findViewById(R.id.includeCitizenForm)).isChecked()+"' " +
						" WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+Roof_page_value[5]+"'");
			}
			else
			{
				cf.arr_db.execSQL(" INSERT INTO "+cf.Roof_additional+" (R_ADD_Ssrid,R_ADD_insp_id,R_ADD_WUWC ,R_ADD_PDRUC ,R_ADD_PN ,R_ADD_PD , R_ADD_RME ,R_ADD_HVAC_E ,R_ADD_VEN_E ,R_ADD_STOR_E ,R_ADD_PIP ,R_ADD_clean_E,R_ADD_ANT_P ,R_ADD_LSE ,R_ADD_O_E,R_ADD_RMEL ,R_ADD_DRMS ,R_ADD_other_I,R_RCT_na,R_ADD_scope,R_ADD_na,R_ADD_comment,R_ADD_citizen,R_ADD_saved) VALUES " +
						"('"+cf.selectedhomeid+"','"+Roof_page_value[5]+"','"+R_ADD_V_WUWC +"','"+R_ADD_V_PDRUC +"','"+R_ADD_V_PN +"','"+R_ADD_V_PD +"','"+ R_ADD_V_RME +"','"+R_ADD_V_HVAC_E +"','"+R_ADD_V_VEN_E +"','"+R_ADD_V_STOR_E +"','"+R_ADD_V_PIP +"','"+R_ADD_V_clean_E+"','"+R_ADD_V_ANT_P +"','"+R_ADD_V_LSE +"','"+R_ADD_V_O_E+"','"+R_ADD_V_RMEL +"','"+R_ADD_V_DRMS +"','"+R_ADD_V_other_I+"','"+RCT_chk_top.isChecked()+"','"+cf.encode(scope)+"','"+ADD_chk_top.isChecked()+"','"+cf.encode(RC_cmt.getText().toString().trim())+"','"+((CheckBox)findViewById(R.id.includeCitizenForm)).isChecked()+"','2')");
			}System.out.println("Sdsd");
			findViewById(R.id.ADD_comp).setVisibility(View.VISIBLE);
		System.out.println("fdsfdsfsdfdsf");
			
			findViewById(R.id.RCN_comp).setVisibility(View.VISIBLE);
		cf.ShowToast("Roof Information saved successfully", 0);
		movetonext();
	}
	private void save_RCN() {
		// TODO Auto-generated method stub
		
		String HRMS=get_rg_value(RCN_RG[13],RCN_other[13]),
		FC="",
		GRAC=get_rg_value(RCN_RG[0],RCN_other[0]),
		RLN=get_rg_value(RCN_RG[1],RCN_other[1]),
		RPN=get_rg_value(RCN_RG[2],RCN_other[2]),
		HREBR=get_rg_value(RCN_RG[3],RCN_other[3]),
		WORR=get_rg_value(RCN_RG[4],RCN_other[4]),
		RMRA=get_rg_value(RCN_RG[5],RCN_other[5]),
		SFPE=get_rg_value(RCN_RG[6],RCN_other[6]),
		Amount_v=cf.encode(Amount.getText().toString().trim()),
		EVPWD=get_rg_value(RCN_RG[7],RCN_other[7]),
		RME=get_rg_value(RCN_RG[8],RCN_other[8]),
		BFP=get_rg_value(RCN_RG[9],RCN_other[9]),
		ESM=get_rg_value(RCN_RG[10],RCN_other[10]),
		SOSAS=get_rg_value(RCN_RG[11],RCN_other[11]),
		DRS=get_rg_value(RCN_RG[12],RCN_other[12]);
		for(int i=0;i<RCN_chk_FC.length;i++)
		{
			if(RCN_chk_FC[i].isChecked())
			{
				FC+=RCN_chk_FC[i].getText().toString().trim()+",";
			}
			
		}
		if(FC.endsWith(","))
		{
			FC=FC.substring(0,FC.length()-1);
		}
		FC=cf.encode(FC);
		Cursor c =cf.SelectTablefunction(cf.RCN_table, " WHERE R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id='"+Roof_page_value[5]+"' ");
		if(c.getCount()<=0)
		{
			cf.arr_db.execSQL(" INSERT INTO "+cf.RCN_table+" ( R_RCN_Ssrid,R_RCN_insp_id,R_RCN_HRMS,R_RCN_FC,R_RCN_GRAC,R_RCN_RLN,R_RCN_RPN,R_RCN_HREBR,R_RCN_WORR,R_RCN_RMRA,R_RCN_SFPE,R_RCN_Amount,R_RCN_EVPWD,R_RCN_RME,R_RCN_BFP,R_RCN_ESM,R_RCN_SOSAS,R_RCN_DRS) values" +
					"('"+cf.selectedhomeid+"','"+Roof_page_value[5]+"','"+HRMS+"','"+FC+"','"+GRAC+"','"+RLN+"','"+RPN+"','"+HREBR+"','"+WORR+"','"+RMRA+"','"+SFPE+"','"+Amount_v+"','"+EVPWD+"','"+RME+"','"+BFP+"','"+ESM+"','"+SOSAS+"','"+DRS+"')");
		}
		else
		{
			
			cf.arr_db.execSQL(" UPDATE "+cf.RCN_table+" SET R_RCN_na='"+RCN_chk_top.isChecked()+"',R_RCN_HRMS='"+HRMS+"',R_RCN_FC='"+FC+"',R_RCN_GRAC='"+GRAC+"',R_RCN_RLN='"+RLN+"',R_RCN_RPN='"+RPN+"',R_RCN_HREBR='"+HREBR+"',R_RCN_WORR='"+WORR+"',R_RCN_RMRA='"+RMRA+"',R_RCN_SFPE='"+SFPE+"',R_RCN_Amount='"+Amount_v+"',R_RCN_EVPWD='"+EVPWD+"',R_RCN_RME='"+RME+"',R_RCN_BFP='"+BFP+"',R_RCN_ESM='"+ESM+"',R_RCN_SOSAS='"+SOSAS+"',R_RCN_DRS='"+DRS+"',R_RCN_saved='1' WHERE  R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id='"+Roof_page_value[5]+"' ");
		}
		/***Dynamic table enty***/
		c=cf.SelectTablefunction(cf.RCN_table_dy, " WHERE R_RCN_D_Ssrid='"+cf.selectedhomeid+"' and R_RCN_D_insp_id='"+Roof_page_value[5]+"' ");
		if(c.getCount()>0)
		{
			c.moveToFirst();
			TableLayout tbl_dy=((TableLayout)findViewById(R.id.RCN_Condition_N_DY_tbl));
			for(int i=0;i<c.getCount();i++,c.moveToNext())
			{
				String option="",other="";
				int id=c.getInt(c.getColumnIndex("id"));
				if(tbl_dy.findViewById(id+1000)!=null)
				{
					RadioGroup rl =(RadioGroup) tbl_dy.findViewById(id+1000);
					if(rl.getCheckedRadioButtonId()!=-1)
					{
						option= findViewById(rl.getCheckedRadioButtonId()).getTag().toString();
						if(tbl_dy.findViewById(id+2000)!=null)
						{
							other =((EditText) tbl_dy.findViewById(id+2000)).getText().toString().trim();
							
						}
						
					}
					
				}
				cf.arr_db.execSQL(" UPDATE "+cf.RCN_table_dy+" SET  R_RCN_D_saved='1',R_RCN_D_option='"+cf.encode(option)+"',R_RCN_D_Other='"+cf.encode(other)+"' WHERE id='"+id+"'");
			}
		}
		findViewById(R.id.RCN_comp).setVisibility(View.VISIBLE);
		cf.ShowToast("Roof Condition Noted saved successfully", 0);
	}
	private String get_rg_value(RadioGroup radioGroup, EditText editText) {
		// TODO Auto-generated method stub
		if(radioGroup.getCheckedRadioButtonId()!=-1)
		{
			String val=radioGroup.findViewById(radioGroup.getCheckedRadioButtonId()).getTag().toString();
			
			if(editText!=null)
			{
				if(editText.getVisibility()==View.VISIBLE)
				{
				
					if(!editText.getText().toString().trim().equals(""))
					{
						val+="#&45"+editText.getText();
					}
				}
			}
			return cf.encode(val);
		}
		else
		{
		 return "";	
		}
	}
	private void Add_more_condition() {
		// TODO Auto-generated method stub
		final Dialog dialog = new Dialog(Roof_commercial_information.this,android.R.style.Theme_Translucent_NoTitleBar);
		dialog.getWindow().setContentView(R.layout.alert);
		dialog.findViewById(R.id.Add_caption).setVisibility(View.VISIBLE);
	//	dialog.findViewById(R.id.main_head).setVisibility(View.GONE);
		((TextView) dialog.findViewById(R.id.add_caption_tit)).setText("Enter title");//Roof Condition Noted question
		final EditText input =(EditText)dialog.findViewById(R.id.caption_text);
		((TextView) dialog.findViewById(R.id.EN_txtid1)).setText("Enter Roof Condition Noted question title");//
		
		InputFilter[] FilterArray;
		FilterArray = new InputFilter[1];
		FilterArray[0] = new InputFilter.LengthFilter(50);
		input.setFilters(FilterArray);
		Button save=(Button)dialog.findViewById(R.id.caption_save);
		Button can=(Button)dialog.findViewById(R.id.caption_cancel);
		ImageView cls=(ImageView)dialog.findViewById(R.id.caption_close);
		cls.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.setCancelable(true);
				dialog.cancel();

			}
		});
		save.setOnClickListener(new OnClickListener() {							
			@Override
			public void onClick(View v) {
				
				String title=input.getText().toString().trim();
				if(!title.equals(""))
				{
					
				Cursor c=	cf.SelectTablefunction(cf.RCN_table_dy, " WHERE R_RCN_D_Ssrid='"+cf.selectedhomeid+"' and R_RCN_D_insp_id='"+Roof_page_value[5]+"' and R_RCN_D_title='"+cf.encode(title)+"'");
				if(c.getCount()<=0)
				{
					cf.arr_db.execSQL(" INSERT INTO "+cf.RCN_table_dy+" (R_RCN_D_Ssrid,R_RCN_D_insp_id,R_RCN_D_title,R_RCN_D_option,R_RCN_D_Other) VALUES" +
							" ('"+cf.selectedhomeid+"','"+Roof_page_value[5]+"','"+cf.encode(title)+"','BSI','' )");
				 c =cf.SelectTablefunction(cf.RCN_table_dy, " WHERE R_RCN_D_Ssrid='"+cf.selectedhomeid+"' and R_RCN_D_insp_id='"+Roof_page_value[5]+"' and R_RCN_D_title='"+cf.encode(title)+"'");
				 c.moveToFirst();
					Show_dynamic_RCN(c);
					dialog.setCancelable(true);
					dialog.cancel();
				}
				else
				{
					cf.ShowToast("Already Exists! Please enter a different title", 0);
				}
				if(c!=null)
					c.close();
				}
				else
				{
					cf.ShowToast("Please enter Roof Condition Noted question Title", 0);
					
				}
				
					
				/*id INTEGER PRIMARY KEY AUTOINCREMENT, R_RCN_D_Ssrid VARCHAR( 5 ) DEFAULT ( '' ), R_RCN_D_insp_id VARCHAR( 5 ) DEFAULT ( '' ), R_RCN_D_title VARCHAR( 50 ) DEFAULT ( '' ), R_RCN_D_option VARCHAR( 15 ) DEFAULT ( '' ), R_RCN_D_Other VARCHAR( 250 ) DEFAULT ( '' ), R_RCN_D_saved VARCHAR( 1 ) DEFAULT ( '0' ))");*/
			}
		});
		
		can.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
						dialog.setCancelable(true);
						dialog.cancel();
						
					}
				});
		dialog.setCancelable(false);
		dialog.show();
	}
	protected void Show_dynamic_RCN() {
		// TODO Auto-generated method stub
		String option_arr[]={"Yes","No","N/D","BSI","N/A"};
		Cursor c=cf.SelectTablefunction(cf.RCN_table_dy, " WHERE R_RCN_D_Ssrid='"+cf.selectedhomeid+"' and R_RCN_D_insp_id='"+Roof_page_value[5]+"' ");
		TableLayout tbl_dy=((TableLayout)findViewById(R.id.RCN_Condition_N_DY_tbl));
		tbl_dy.removeAllViews();
		if(c.getCount()>0)
		{
			c.moveToFirst();
			for(int i=0;i<c.getCount();i++,c.moveToNext())
			{
				Show_dynamic_RCN(c);
			}
		}
	}
	protected void Show_dynamic_RCN(Cursor c) {
		// TODO Auto-generated method stub
		String option_arr[]={"Yes","No","N/D","BSI","N/A"};
		TableLayout tbl_dy=((TableLayout)findViewById(R.id.RCN_Condition_N_DY_tbl));
		
		if(c.getCount()>0)
		{
		
				int id=c.getInt(c.getColumnIndex("id"));
				String title=cf.decode(c.getString(c.getColumnIndex("R_RCN_D_title"))),
				option=cf.decode(c.getString(c.getColumnIndex("R_RCN_D_option"))),
				other=cf.decode(c.getString(c.getColumnIndex("R_RCN_D_Other")));
				TableRow tbl=new TableRow(this);
	    		tbl.setId(id);/***Set the dynamic id for the Each Row for retrive the value ***/
				TextView tit_tv=new TextView(this,null,R.attr.Roof_RCN_Tit_txt);
	    		tit_tv.setWidth(0);
	    		TextView tit_star=new TextView(this,null,R.attr.star);
				tit_star.setTag("Star");
				tit_star.setPadding(5, 0, 5, 0);
				tbl.addView(tit_star);
	    		tit_star.setVisibility(View.VISIBLE);
	    		tit_tv.setText(title);
				tit_tv.setTag("Title_txt");
	    		tbl.addView(tit_tv,250,LayoutParams.WRAP_CONTENT);
	    		TextView colon_tv=new TextView(this,null,R.attr.colon_withoutmargin);
	    		colon_tv.setPadding(0, 0, 0, 0);
	    		tbl.addView(colon_tv);
	    		TableRow.LayoutParams clp=(android.widget.TableRow.LayoutParams) colon_tv.getLayoutParams();
	    		clp.setMargins(0, 0, 0, 0);
	    		LinearLayout li= new LinearLayout(this);
	    		li.setOrientation(LinearLayout.VERTICAL);
	    		RadioGroup rg= new RadioGroup(this);
	    		rg.setId(id+1000);
	    		li.addView(rg);
	    		rg.setOrientation(RadioGroup.HORIZONTAL);
	    		tbl.addView(li);
	    		
	    		EditText ed =new EditText(this,null,R.attr.Roof_RCN_yes_ed);
	    		ed.setTag("Comments");
	    		ed.setId(id+2000);
	    		InputFilter[] FilterArray;
	    		FilterArray = new InputFilter[1];
	    		FilterArray[0] = new InputFilter.LengthFilter(250);
	    		ed.setFilters(FilterArray);
	    		li.addView(ed, 500, LayoutParams.WRAP_CONTENT);
	    		TextView yes_tv=new TextView(this,null,R.attr.Roof_RCN_yes_txt);
	    		yes_tv.setTag("Comments_txt");
	    		li.addView(yes_tv);
	    		rg.setOnCheckedChangeListener(new RCN_click(ed, yes_tv, "Yes"));
	    		ImageView opt_img=new ImageView(this,null,R.attr.Roof_RCN_opt_img);
	    		tbl.addView(opt_img);
	    		opt_img.setVisibility(View.VISIBLE);
	    		opt_img.setOnClickListener(new RCN_D_delete(id));
	    		ed.setText(other);
	    		tbl_dy.addView(tbl);
	    		for(int m =0;m<option_arr.length;m++)
	    		{
	    			
	    			RadioButton rb= new RadioButton(this,null,R.attr.Roof_RCN_opt_rb);
	    			rb.setText(option_arr[m]);
	    			rb.setTag(option_arr[m]);
	    			rg.addView(rb,100,LayoutParams.WRAP_CONTENT);
	    			if(option_arr[m].equals(option))
	    			{
	    				rb.setChecked(true);	
	    			}
	    		}
	    		//ed.setVisibility(View.GONE);
	    		ed.setOnTouchListener(new OnTouchListener() {
					
					@Override
					public boolean onTouch(View v, MotionEvent event) {
						// TODO Auto-generated method stub
						((EditText) v).setFocusable(true);
				    	((EditText) v).setFocusableInTouchMode(true);
				    	((EditText) v).requestFocus();
				    	((EditText) v).setSelection(((EditText) v).length());   
						return false;
					}
				});
			
		}
	}
	class RCN_D_delete implements OnClickListener
	{
		int id;

		public RCN_D_delete(int id) {
			// TODO Auto-generated constructor stub
			this.id=id;
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			final AlertDialog.Builder ab =new AlertDialog.Builder(Roof_commercial_information.this);
			ab.setTitle("Confirmation");
			ab.setMessage(Html.fromHtml("Do you want to delete the selected Roof Condition Noted question?"));
			ab.setIcon(R.drawable.alertmsg);
			ab.setCancelable(false);
			ab.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					cf.arr_db.execSQL(" DELETE FROM "+cf.RCN_table_dy+" WHERE id='"+id+"'");
					cf.ShowToast("Roof Condition Noted question deleted successfully ", 0);
					TableLayout tbl_dy=((TableLayout)findViewById(R.id.RCN_Condition_N_DY_tbl));
					tbl_dy.removeView(tbl_dy.findViewById(id));
					
					
					//Show_dynamic_RCN();
				}
				
			})
			.setNegativeButton("No", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					
				}
			});
			AlertDialog al=ab.create();
			al.show();
		}
		
	}
	/**RCN table function end***/
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if(requestCode==14)
		{
		switch (resultCode) {
				case 10:
					cf.ShowToast("You have cancelled the generate Citizen Certificate ", 0);
					movetonext();
				break;
				case RESULT_OK:
				/**Before that need to generate the form throw the web service**/
					System.out.println("RESULT_OK 10");
				cf.ShowToast("You have generated Citizen Certificate", 0);
			
				movetonext();
				break;
				
		}
		}
		else if(requestCode==pick_img_code)
		{
		String cv="";
			switch (resultCode) {
				case RESULT_CANCELED:
				//	cf.ShowToast("You have canceld the Roof cover image selection ", 0);
					cv=data.getExtras().getString("Cover");
					//((EditText)findViewById(id1).findViewWithTag("Path")).setText("");
				break;
				case RESULT_OK:
					/*cf.ShowToast("The image uploaded successfully.",1);*/
				
					
					cv=data.getExtras().getString("Cover");
					//((EditText)findViewById(id).findViewWithTag("Path")).setText(data.getExtras().getString("path").trim());
				break;
				
			}
			try
			{
				System.out.println("ccccv="+cv);
		    		Cursor c=cf.arr_db.rawQuery("Select * from "+cf.Roofcoverimages+" Where RIM_masterid=(Select RM_masterid from "+cf.Roof_master+
		    				" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+Roof_page_value[5]+"' and RM_Covering='"+cf.encode(cv)+"') order by RIM_Order", null);
		    		
		    		if(c.getCount()>0)
		    		{
		    			if(cf.encode(cv).equals(Sp_RCT[0].getSelectedItem().toString()))
		    			{
		    				tv.setVisibility(View.VISIBLE);
			    			tv.setText(Html.fromHtml("<u>View/Edit Photos ("+c.getCount()+")</u>"));
			    		}
		    		}
		    		else
		    			{
		    				tv.setVisibility(View.INVISIBLE);
		    				System.out.println("null  only cames");
		    			}
		    			
		    		
		    	
		    }
		    catch (Exception e) {
				// TODO: handle exception
			}
		}
		else if(requestCode==cf.loadcomment_code)
		{
			load_comment=true;
			if(resultCode==RESULT_OK)
			{
				RC_cmt.setText((RC_cmt.getText().toString()+" "+data.getExtras().getString("Comments")).trim());
				
			}
			
			
				
		}

	}
	private void movetonext() {
		// TODO Auto-generated method stub
		 if(cf.Roof.equals(cf.Roof_insp[0]))
		 {
			boolean  s=false;
		 /*** For 4poin***/
			
			 /***For hide the attic if the External only selected menas ends**/
		
				cf.goclass(22);
		
		 }
		 else if(cf.Roof.equals(cf.Roof_insp[1]))
		 {
			
				cf.goclass(29);
			
		 }
		 else  if(cf.Roof.equals(cf.Roof_insp[4]))
		 {
				
		 /*** For GCH***/
			
			 cf.goclass(47);
		/***	 For GCH Ends***/
		 }
		 else if(cf.Roof.equals(cf.getResourcesvalue(R.string.insp_5)))
		 {
			 
			/*** Roof Survey***/
				/*if(cf.identityval==33 || cf.identityval==34)
				{*/
					cf.gotoclass(33, WindMitCommAuxbuildings.class);
				//}
				
			 /****Roof Survay ends**/
		 } 
		 else if (cf.Roof.equals(cf.getResourcesvalue(R.string.insp_6)))
		 {
			 cf.gotoclass(34, WindMitCommAuxbuildings.class);
		 }
		 else if (cf.Roof.equals(cf.getResourcesvalue(R.string.insp_4)))
		 {
			 cf.gotoclass(32, RoofSection.class);
		 }
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			
			Intent  myintent=null;
			if(cf.Roof.equals(cf.Roof_insp[0]))
			 {
			    /*** For 4poin***/
				 cf.goback(0);	 
			 }
			 else if(cf.Roof.equals(cf.Roof_insp[1]))
			 {
				/*** Roof Survey***/
				 cf.chk_InspTypeQuery(cf.selectedhomeid,cf.Roof_insp[0]);
			
				 if(cf.isaccess)
				 {
					 cf.Roof=cf.Roof_insp[0];
					 cf.goback(1);
				 
				 }
				 else
				 {
					 cf.goback(0);
				 }
			        
			 }
			 else  if(cf.Roof.equals(cf.Roof_insp[4]))
			 {
					
			 /*** For GCH***/
				
				 cf.goclass(416);
			/***	 For GCH Ends***/
			 }
			 else if(cf.Roof.equals(cf.getResourcesvalue(R.string.insp_5)))
			 {
				
				/*** Roof Survey***/
				/*if(cf.identityval==33 || cf.identityval==34)
				{*/
			 
				 cf.identityval=33; 
			 
					cf.gotoclass(cf.identityval, WindMitCommareas.class);
				//}
				
				 /****Roof Survay ends**/
			 }
			 else if(cf.Roof.equals(cf.getResourcesvalue(R.string.insp_6)))
			 {
				
				/*** Roof Survey***/
				/*if(cf.identityval==33 || cf.identityval==34)
				{*/
			 
				 cf.identityval=34; 
			 
					cf.gotoclass(cf.identityval, WindMitCommareas.class);
				//}
				
				 /****Roof Survay ends**/
			 }
			 else if(cf.Roof.equals(cf.getResourcesvalue(R.string.insp_4)))
			 {
				
				/*** Roof Survey***/
				/*if(cf.identityval==33 || cf.identityval==34)
				{*/
			 
				 cf.identityval=32; 
			 
					cf.gotoclass(cf.identityval, WindMitCommareas.class);
				//}
				
				 /****Roof Survay ends**/
			 }
			 else 
			 {
				 /*** For GCH***/
				 cf.goback(0);
				 
				 
			/***	 For GCH Ends***/ 
			 }
			
		return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}	
	/**For citizen genration form***/
	protected boolean generate_form()throws SocketException, NetworkErrorException, TimeoutException, IOException, XmlPullParserException {
		// TODO Auto-generated method stub
		
		cf.request=cf.SoapRequest("GENERATECITIZENFORMPDF");
		cf.request.addProperty("SRID", cf.selectedhomeid);
		cf.request.addProperty("Inspectionid", Roof_page_value[5]);
	
		
		cf.request.addProperty("InspectorID", cf.Insp_id);
		String sql=" Select ph.ARR_PH_FirstName||' '||ph.ARR_PH_LastName as ph_Name, ph.ARR_PH_Address1||' '||ph.ARR_PH_Address2 as ph_Address,ph.ARR_PH_Policyno,ph.ARR_Schedule_ScheduledDate From  "+cf.policyholder+" as ph WHERE ARR_PH_SRID='"+cf.selectedhomeid+"'";
		Cursor cs=cf.arr_db.rawQuery(sql, null);
		System.out.println(sql);
		cs.moveToFirst();
		if(cs.getCount()>0)
		{
			cf.request.addProperty("Insuredname", cf.decode(cs.getString(cs.getColumnIndex("ph_Name"))));
			cf.request.addProperty("Policyno", cf.decode(cs.getString(cs.getColumnIndex("ARR_PH_Policyno"))));
			cf.request.addProperty("Inspectedaddress", cf.decode(cs.getString(cs.getColumnIndex("ph_Address"))));
			cf.request.addProperty("Dateofinspection", cf.decode(cs.getString(cs.getColumnIndex("ARR_Schedule_ScheduledDate"))));
			
		
		}
		/***Roof covering type value **/
		sql=" SELECT * FROM "+cf.Rct_table+" WHERE RCT_Ssrid='"+cf.selectedhomeid+"' and RCT_insp_id='"+Roof_page_value[5]+"' and RCT_pre='true'";
		System.out.println("the sql1"+sql);
		cs=cf.arr_db.rawQuery(sql, null);
		System.out.println(sql);
		cs.moveToFirst();
		String roof_cover="",appr="",roof_age="",date="";
		if(cs.getCount()>0)
		{
			
			roof_cover=cf.decode(cs.getString(cs.getColumnIndex("RCT_rct")));
			date=cf.decode(cs.getString(cs.getColumnIndex("RCT_PAD")));
			roof_age=cf.decode(cs.getString(cs.getColumnIndex("RCT_yoi")));
			appr=cf.decode(cs.getString(cs.getColumnIndex("RCT_arfl")));
			if(roof_cover.equals("Other"))
			{
				roof_cover=cf.decode(cs.getString(cs.getColumnIndex("RCT_other_rct")));
			}
			if(roof_age.equals("Other"))
			{
				roof_age=cf.decode(cs.getString(cs.getColumnIndex("RCT_other_yoi")));
			}
			if(appr.equals("Other"))
			{
				appr=cf.decode(cs.getString(cs.getColumnIndex("RCT_other_aRFL")));
			}
		}
		cf.request.addProperty("Roofcovering", roof_cover);
		cf.request.addProperty("datelastupdate", date);
		cf.request.addProperty("Ageofroof", roof_age);
		cf.request.addProperty("remainingroof", appr);
		sql=" SELECT R_RCN_VSL,R_RCN_VSCBS FROM "+cf.RCN_table+" WHERE R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id='"+Roof_page_value[5]+"'";
		System.out.println("the sql1"+sql);
		cs=cf.arr_db.rawQuery(sql, null);
		cs.moveToFirst();
		if(cs.getCount()>0)
		{
			String VSL=cs.getString(cs.getColumnIndex("R_RCN_VSL")),VSL_o="",
			VSCBS=cs.getString(cs.getColumnIndex("R_RCN_VSCBS")),VSCBS_o="";
			if(!VSL.equals(""))
			{
				if(VSL.contains("#&45"))
				{
					String[]tem=VSL.split("#&45");
					VSL=tem[0];
					VSL_o=tem[1];
					cf.request.addProperty("Singofleaks", VSL);
					cf.request.addProperty("Singofleaksvalue", VSL_o);
					cf.request.addProperty("Singofleaksvalue", "");
				}
				else
				{
					cf.request.addProperty("Singofleaks", VSL);
					
				}
			}
			if(!VSCBS.equals(""))
			{
				if(VSCBS.contains("#&45"))
				{
					String[]tem=VSCBS.split("#&45");
					VSCBS=tem[0];
					VSCBS_o=tem[1];
					cf.request.addProperty("Singofdamaged", VSCBS);
					cf.request.addProperty("Singdamagedvalue", VSCBS_o);
				}else
				{
					cf.request.addProperty("Singofdamaged", VSCBS);
					cf.request.addProperty("Singdamagedvalue", "");
				}
			}
		}
		/*sql=" Select GRI.*,M.mst_text from "+cf.Roof_cover_type+" GRI LEFT JOIN "+cf.master_tbl+" M on GRI.RCT_TypeValue=m.mst_custom_id and M.mst_module_id='2' WHERE RCT_masterid=(SELECT RCT_masterid from "+cf.Roof_cover_type+" Where RCT_TypeValue='11' and RCT_masterid in " +
				"(SELECT RM_masterid from "+cf.Roof_master+" where RM_insp_id='2' and RM_srid='"+cf.selectedhomeid+"' and RM_module_id='0') and RCT_FieldVal='true' ) "; 
		System.out.println("the sql1"+sql);
		cs=cf.arr_db.rawQuery(sql, null);
		System.out.println(sql);
		cs.moveToFirst();
		String roof_cover="",appr="",roof_age="",date="";
		if(cs.getCount()>0)
		{
			do
			{
				
				
				if(cs.getString(cs.getColumnIndex("RCT_TypeValue")).equals("1"))
				{
					roof_cover=cf.decode(cs.getString(cs.getColumnIndex("RCT_FieldVal")));
					
				}
				if(cs.getString(cs.getColumnIndex("RCT_TypeValue")).equals("2"))
				{
					date=cf.decode(cs.getString(cs.getColumnIndex("RCT_FieldVal")));
					
					
				}
				if(cs.getString(cs.getColumnIndex("RCT_TypeValue")).equals("3"))
				{
					roof_age=cf.decode(cs.getString(cs.getColumnIndex("RCT_FieldVal")));
					
				}
				if(cs.getString(cs.getColumnIndex("RCT_TypeValue")).equals("10"))
				{
					appr=cf.decode(cs.getString(cs.getColumnIndex("RCT_FieldVal")));
					
				}
			}
			while(cs.moveToNext());
			cf.request.addProperty("Roofcovering", roof_cover);
			cf.request.addProperty("datelastupdate", date);
			cf.request.addProperty("Ageofroof", roof_age);
			cf.request.addProperty("remainingroof", appr);
		}
		
		*//***Roof covering type value  **//*
		*//**Roof condition noted value **//*
		sql=" Select GRI.*,M.mst_text from "+cf.General_Roof_information+" GRI LEFT JOIN "+cf.master_tbl+" M on GRI.RC_Condition_type=m.mst_custom_id and M.mst_module_id='2' WHERE RC_masterid=" +
				"(SELECT RM_masterid from "+cf.Roof_master+" where RM_insp_id='2' and RM_srid='"+cf.selectedhomeid+"' and RM_module_id='1') and (RC_Condition_type='16' or RC_Condition_type='18')  "; 
		System.out.println("the sql1"+sql);
		cs=cf.arr_db.rawQuery(sql, null);
		cs.moveToFirst();
		if(cs.getCount()>0)
		{
			
			do
			{
				
				String val=cf.decode(cs.getString(cs.getColumnIndex("RC_Condition_type_val"))),
						desc=cf.decode(cs.getString(cs.getColumnIndex("RC_Condition_type_DESC"))),
						tit=cf.decode(cs.getString(cs.getColumnIndex("RC_Condition_type")));
				if(tit.trim().equals("18"))
				{
					
					
					cf.request.addProperty("Singofdamaged", val);
					cf.request.addProperty("Singdamagedvalue", desc);
					
					
					
					
				}
				if(tit.trim().equals("16"))
				{
					cf.request.addProperty("Singofleaks", val);
					cf.request.addProperty("Singofleaksvalue", desc);
					
				}
			}while(cs.moveToNext());
		}*/
		String timeStamp = new SimpleDateFormat("MM/dd/yyyy").format(Calendar.getInstance().getTime());
		cf.request.addProperty("Date", timeStamp);
		System.out.println("the request "+cf.request);
		boolean result=cf.SoapResponse("GENERATECITIZENFORMPDF", cf.request);
		
		/**Roof condition noted value ends **/
		return result;
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		boolean res=false;
		try
		{
					res=generate_form();
				
		}
		catch (SocketException e) {
			// TODO: handle exception
			
		}
		catch (NetworkErrorException e) {
			// TODO: handle exception
			
		}
		catch (TimeoutException e) {
			// TODO: handle exception
			
		}
		catch (IOException e) {
			// TODO: handle exception
			
		}
		catch (XmlPullParserException e) {
			// TODO: handle exception
			
		}
		catch (Exception e) {
			// TODO: handle exception
			
		}
		if(res)
		{
			show_handler=1;
		}
		else
		{
			show_handler=2;
			//movetonext();
		}
		handler.sendEmptyMessage(0);
	}
	private Handler handler = new Handler() {
		

		public void handleMessage(Message msg) {
			pd.dismiss();
			if(show_handler==1)
			{

				
					cf.ShowToast("Citizen Certification Form generated successfully. ", 0);
					movetonext();
			}
			else if(show_handler==2)
			{
				cf.ShowToast(" Network unavailable! Unable to generate Citizen Certification Form. Please try again later", 0);
			}
				
		}
	};
	private void checkforsurveyaleret() {
		// TODO Auto-generated method stub
		if(((CheckBox)findViewById(R.id.includeCitizenForm)).isChecked() && (Roof_page_value[5].equals("1") || Roof_page_value[5].equals("7"))  )
		//if(Roof.equals(cf.Roof_insp[1]))
		{
			/****Alert to get the confirmation from the user for generate the citizen form**/
			AlertDialog.Builder alt_bld = new AlertDialog.Builder(this);
			AlertDialog alert = null;
			alt_bld.setMessage("Roof Information saved successfully .\n  To create a Citizens Roof Certification Form, an internet connection is required. This can be done later online before submittal or when you have an internet connection.")
			.setCancelable(false)
			.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			

			

			public void onClick(DialogInterface dialog, int id) {
			// Action for 'Yes' Button
				try
				{
				
					ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
					NetworkInfo info = conMgr.getActiveNetworkInfo();
					if(cf.isInternetOn() && info!=null)
					{
						
							dialog.cancel();
							String source = "<b><font color=#00FF33>Generating Citizen Certification Form. Please wait..."
									+ "</font></b>";
								pd = ProgressDialog.show(Roof_commercial_information.this, "", Html.fromHtml(source), true);
								Thread td =new Thread(Roof_commercial_information.this);
								td.start();
									//res=generate_form();
								
					
					}
					else
					{
						cf.ShowToast("There is a problem on your network. Please try again!", 0);
					}
				//	movetonext();
					//dialog.cancel();
				}catch (Exception e) {
					// TODO: handle exception
					System.out.println("The problem in the inserting the selected inspection"+e.getMessage());
				}
			}
			})
			.setNegativeButton("No", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
			//  Action for 'NO' Button
				//((Dialog) dialog).setCancelable(true);
				movetonext();
			dialog.cancel();
			}
			});
		
			// Title for AlertDialog
			alert = alt_bld.create();
			alert.setTitle("Citizen Certificate");
			// Icon for AlertDialog
			alert.setIcon(R.drawable.alertmsg);
			alert.show();
			alert.setCancelable(false);
			/****Alert to get the confirmation from the user for generate the citizen form Ends**/
		}
		else
		{
			cf.ShowToast("Roof Information saved successfully ", 1);
			movetonext();
		}	
	}
	/**For citizen genration form ends***/
}
