package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class newcallatempt extends Activity {
	String no_txt[]=new String[7];
	CommonFunctions cf;
	String assdate="";
	
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cf = new CommonFunctions(this);
        Bundle extras = getIntent().getExtras();
		if (extras != null) {
			cf.selectedhomeid = extras.getString("homeid");
			cf.onlinspectionid = extras.getString("InspectionType");
		    cf.onlstatus = extras.getString("status");
   	     }
   	   setContentView(R.layout.newcallattempt);
       cf.getDeviceDimensions();
	   LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
	   hdr_layout.setMinimumWidth(cf.wd);
	   hdr_layout.addView(new HdrOnclickListener(this,1,"General","Call Attempt",1,0,cf));
	   LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
       main_layout.setMinimumWidth(cf.wd);
       main_layout.addView(new MyOnclickListener(getApplicationContext(), 1, 0,0,cf));
	   LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
	   submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 13, 1,0,cf));
	   TableRow tbr=(TableRow)findViewById(R.id.row2);
	   tbr.setMinimumHeight(cf.ht);
	   
	   set_defaultPHphonenos();
	   set_defaultAgentphonenos();
	   set_defaultAdditionalphonenos();
	}
	private void set_defaultAdditionalphonenos() {
		// TODO Auto-generated method stub
		try
		{
			cf.CreateARRTable(6);
		    Cursor cur = cf.SelectTablefunction(cf.Additional_table, " where ARR_AD_SRID='"+ cf.selectedhomeid + "'");
			int  rws = cur.getCount();
		    cur.moveToFirst();
		    if (cur != null && rws>0) {
			 
			 do{
				 ((TextView)findViewById(R.id.ACI_BDC)).setText((cur.getString(cur.getColumnIndex("ARR_AD_Bestdaycall")).trim().equals(""))?"N/A":cur.getString(cur.getColumnIndex("ARR_AD_Bestdaycall")).trim());
				 ((TextView)findViewById(R.id.ACI_BTC)).setText((cur.getString(cur.getColumnIndex("ARR_AD_BestTimetoCallYou")).trim().equals(""))?"N/A":cur.getString(cur.getColumnIndex("ARR_AD_BestTimetoCallYou")).trim());
				 ((TextView)findViewById(R.id.ACI_first)).setText((cur.getString(cur.getColumnIndex("ARR_AD_FirstChoice")).trim().equals(""))?"N/A":cur.getString(cur.getColumnIndex("ARR_AD_FirstChoice")).trim());
				 ((TextView)findViewById(R.id.ACI_second)).setText((cur.getString(cur.getColumnIndex("ARR_AD_SecondChoice")).trim().equals(""))?"N/A":cur.getString(cur.getColumnIndex("ARR_AD_SecondChoice")).trim());
				 ((TextView)findViewById(R.id.ACI_third)).setText((cur.getString(cur.getColumnIndex("ARR_AD_ThirdChoice")).trim().equals(""))?"N/A":cur.getString(cur.getColumnIndex("ARR_AD_ThirdChoice")).trim());
				 no_txt[5]=(cur.getString(cur.getColumnIndex("ARR_AD_MobileNumber")).trim().equals(""))?"N/A":cur.getString(cur.getColumnIndex("ARR_AD_MobileNumber")).trim();
				 no_txt[6]=(cur.getString(cur.getColumnIndex("ARR_AD_PhoneNumber")).trim().equals(""))?"N/A":cur.getString(cur.getColumnIndex("ARR_AD_PhoneNumber")).trim();
			 } while(cur.moveToNext());
			 }
			 if(no_txt[5].equals(""))
	         {
				 ((TextView)findViewById(R.id.ACI_mbl_no)).setText("Not Available");
	       	 }
	         else
	         {
	        	 ((TextView)findViewById(R.id.ACI_mbl_no)).setText(no_txt[5]);
	         }
			 if(no_txt[6].equals(""))
	         {
				 ((TextView)findViewById(R.id.ACI_ofc_no)).setText("Not Available");
	       	  
	         }
	         else
	         {
	        	 ((TextView)findViewById(R.id.ACI_ofc_no)).setText(no_txt[6]);
	         }
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		
	
	}
	private void set_defaultAgentphonenos() {
		// TODO Auto-generated method stub
		try
		{
			cf.CreateARRTable(5);
			Cursor c2=cf.SelectTablefunction(cf.Agent_tabble, " where ARR_AI_SRID='" + cf.selectedhomeid + "'");
			if(c2.getCount()>0)
			{
				c2.moveToFirst();
				no_txt[3]=c2.getString(c2.getColumnIndex("ARR_AI_AgentOffPhone")).trim();
				no_txt[4]=c2.getString(c2.getColumnIndex("ARR_AI_AgentContactPhone")).trim();
				if(no_txt[3].equals(""))
                {
				((TextView)findViewById(R.id.Call_A_ph)).setText("Not Available");
              	  
                }
                else
                {
                	((TextView)findViewById(R.id.Call_A_ph)).setText(no_txt[3]);
                }
				if(no_txt[4].equals(""))
                {
					((TextView)findViewById(R.id.Call_A_C_ph)).setText("Not Available");
              	               	
                }
                else
                {
                	((TextView)findViewById(R.id.Call_A_C_ph)).setText(no_txt[4]);
                }
			}
			else
			{
				((TextView)findViewById(R.id.Call_A_ph)).setText("Not Available");
				((TextView)findViewById(R.id.Call_A_C_ph)).setText("Not Available");
			}
			
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}
	private void set_defaultPHphonenos() {
		// TODO Auto-generated method stub
		try
		{
			Cursor cur1=cf.SelectTablefunction(cf.policyholder, " where ARR_PH_SRID='" + cf.selectedhomeid + "' and ARR_PH_InspectorId='"	+ cf.Insp_id + "'");
			
			if(cur1.getCount()>0)
			{
				  cur1.moveToFirst();
                  assdate=cf.decode(cur1.getString(cur1.getColumnIndex("ARR_Schedule_AssignedDate")));
                  no_txt[0]=cf.decode(cur1.getString(cur1.getColumnIndex("ARR_PH_HomePhone"))).trim();
                  no_txt[1]=cf.decode(cur1.getString(cur1.getColumnIndex("ARR_PH_WorkPhone"))).trim();
                  no_txt[2]=cf.decode(cur1.getString(cur1.getColumnIndex("ARR_PH_CellPhone"))).trim();
                  
                  if(no_txt[0].equals(""))
                  {
                	  ((TextView)findViewById(R.id.Call_H_ph)).setText("Not Available");
                  }
                  else
                  {
                	  ((TextView)findViewById(R.id.Call_H_ph)).setText(no_txt[0]);
                  }
                  if(no_txt[1].equals(""))
                  {
                	  ((TextView)findViewById(R.id.Call_W_ph)).setText("Not Available");
                  }
                  else
                  {
                	  ((TextView)findViewById(R.id.Call_W_ph)).setText(no_txt[1]);
                  }
                  if(no_txt[2].equals(""))
                  {
                	  ((TextView)findViewById(R.id.Call_C_ph)).setText("Not Available");
                  }
                  else
                  {
                	  ((TextView)findViewById(R.id.Call_C_ph)).setText(no_txt[2]);
                  }
			}
		}catch (Exception e) {
			// TODO: handle exception
		}
               
	}
	public void clicker(View v)
	{
		 switch (v.getId()) {
			 case R.id.hme:
				 cf.gohome();
				 break;
			 case R.id.c_edit_H_ph:
				 break;
			 case R.id.c_edit_W_ph:
				 break;
			 case R.id.c_edit_C_ph:
				 break;
			 
			default:
				break;
		}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intimg = new Intent(getApplicationContext(), Agent.class);
			intimg.putExtra("homeid", cf.selectedhomeid);
			intimg.putExtra("InspectionType", cf.onlinspectionid);
			intimg.putExtra("status", cf.onlstatus);
			
			startActivity(intimg);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

}
