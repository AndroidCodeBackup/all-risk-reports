package idsoft.inspectiondepot.ARR;

import java.util.ArrayList;
import java.util.List;

import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.BIWallstructure.multi;
import idsoft.inspectiondepot.ARR.MultiSpinner.MultiSpinnerListener;
import idsoft.inspectiondepot.ARR.RoofSection.selectchange;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class BIWallcladding extends Activity {
	CommonFunctions cf;
	TextView tv[] = new TextView[101];
	private List<String> items= new ArrayList<String>();
	Cursor c1;
	String floorchoosen="",wallclad="";
	String spnwallclad[] = new String[15];
	public MultiSpinner spnwallcladnumbers[] = new MultiSpinner[14];	
	public int[] spnwcnoid = {R.id.spnwallcladno1,R.id.spnwallcladno2,R.id.spnwallcladno3,R.id.spnwallcladno4,R.id.spnwallcladno5,R.id.spnwallcladno6,R.id.spnwallcladno7,R.id.spnwallcladno8,R.id.spnwallcladno9,R.id.spnwallcladno10,R.id.spnwallcladno11,R.id.spnwallcladno12,R.id.spnwallcladno13,R.id.spnwallcladno14};
	
	public TextView tvwcothertxt[] = new TextView[14];	
	public int[] tvwcothertxtid = {R.id.tvwcothertxt1,R.id.tvwcothertxt2,R.id.tvwcothertxt3,R.id.tvwcothertxt4,R.id.tvwcothertxt5,R.id.tvwcothertxt6,R.id.tvwcothertxt7,R.id.tvwcothertxt8,R.id.tvwcothertxt9,R.id.tvwcothertxt10,R.id.tvwcothertxt11,R.id.tvwcothertxt12,R.id.tvwcothertxt13,R.id.tvwcothertxt14};
	
	public TextView tvwallcladding[] = new TextView[14];
	public int[] wallcladdingid = {R.id.tvwcdetails1,R.id.tvwcdetails2,R.id.tvwcdetails3,R.id.tvwcdetails4,R.id.tvwcdetails5,R.id.tvwcdetails6,R.id.tvwcdetails7,R.id.tvwcdetails8,R.id.tvwcdetails9,R.id.tvwcdetails10,R.id.tvwcdetails11,R.id.tvwcdetails12,R.id.tvwcdetails13,R.id.tvwcdetails14};
	
	public TextView tvwctitle[] = new TextView[14];
	public int[] wallcladdingtitleid = {R.id.wctitle1,R.id.wctitle2,R.id.wctitle3,R.id.wctitle4,R.id.wctitle5,R.id.wctitle6,R.id.wctitle7,R.id.wctitle8,R.id.wctitle9,R.id.wctitle10,R.id.wctitle11,R.id.wctitle12,R.id.wctitle13,R.id.wctitle14};
	
	public TextView dummytext1[] = new TextView[14];
	public int[] dummytvid = {R.id.tvwc1,R.id.tvwc2,R.id.tvwc3,R.id.tvwc4,R.id.tvwc5,R.id.tvwc6,R.id.tvwc7,R.id.tvwc8,R.id.tvwc9,R.id.tvwc10,R.id.tvwc11,R.id.tvwc12,R.id.tvwc13,R.id.tvwc14};
	
	public TextView tvstoriesdisp1[] = new TextView[14];
	public int[] tvstoriesdispid = {R.id.tvwcstor1,R.id.tvwcstor2,R.id.tvwcstor3,R.id.tvwcstor4,R.id.tvwcstor5,R.id.tvwcstor6,R.id.tvwcstor7,R.id.tvwcstor8,R.id.tvwcstor9,R.id.tvwcstor10,R.id.tvwcstor11,R.id.tvwcstor12,R.id.tvwcstor13,R.id.tvwcstor14};
	
	Dialog dialog1;TableLayout myTable;
	String Wall_Split[]=new String[14];String WC_Split[]=new String[14];
	String s1="",wcnotappl="0",wcvalue="",Wallcladding="",WC_NA="",wcothervalue="",totwc="",spnwc1="",spnwc2="",spnwc3="",spnwc4="",spnwc5="",spnwc6="",spnwc7="",spnwc8="",spnwc9="",spnwc10="",spnwc11="",spnwc12="",spnwc13="",spnwc14="";
	TableRow tr;int l=0;
	
	String wallcladvalue[] = new String[15];
	public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	       
	        cf = new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);				
		 	}
	        setContentView(R.layout.wallcladding);
	        cf.getDeviceDimensions();
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
         	hdr_layout.addView(new HdrOnclickListener(this,1,"General","Building Info","Wall Cladding",1,1,cf));
         	LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
         	main_layout.setMinimumWidth(cf.wd);
         	main_layout.addView(new MyOnclickListener(getApplicationContext(), 1, 0,0,cf));
            LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
           submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 16, 1,0,cf));
	        LinearLayout submenu2_layout = (LinearLayout) findViewById(R.id.submenu1); 
	        submenu2_layout.addView(new MyOnclickListener(getApplicationContext(), 456, 1,0,cf));
	        TableRow tblrw = (TableRow)findViewById(R.id.row2);
	        tblrw.setMinimumHeight(cf.ht);
	        cf.CreateARRTable(3);
	        cf.CreateARRTable(61);
	        Declaration();
	        Wallcladdingstories();
	        setValue();
	    }
	private void Declaration() {
		// TODO Auto-generated method stub
		cf.setupUI((ScrollView) findViewById(R.id.scr));
		
		 for(int i=0;i<tvwctitle.length;i++)
	   	 {
		   tvwctitle[i] = (TextView)findViewById(wallcladdingtitleid[i]);
		 }		 
		try
		{
			for(int k=0;k<tvwallcladding.length;k++)
	   	 	{
				tvwallcladding[k] = (TextView)findViewById(wallcladdingid[k]);
				tvwallcladding[k].setOnClickListener(new OnClickListener1(k));		
	   	 	}
		}catch(Exception e){}
		
		 for(int i=0;i<tvwcothertxt.length;i++)
		 {
			tvwcothertxt[i] = (TextView)findViewById(tvwcothertxtid[i]);		   		
	   	 }
		 for(int i=0;i<dummytext1.length;i++)
   	     {
	    	dummytext1[i] = (TextView)findViewById(dummytvid[i]);	   		
   	     }
	     for(int i=0;i<tvstoriesdisp1.length;i++)
   	     {
	   		tvstoriesdisp1[i] = (TextView)findViewById(tvstoriesdispid[i]);	   	
   	     }

		items.clear();
		try
		{
			cf.getPolicyholderInformation(cf.selectedhomeid);
			for(int j=0;j<Integer.parseInt(cf.Stories);j++)
			{
				items.add(cf.spnstories[j]);
			}  
		}
		catch (Exception e) {
			// TODO: handle exception
		}
			try
			{
			for(int i=0;i<spnwallcladnumbers.length;i++)
		   	{
			   	spnwallcladnumbers[i] = (MultiSpinner)findViewById(spnwcnoid[i]);
			   	spnwallcladnumbers[i].setItems(items,"--Select--",new multi1());	   			
			   	spnwallcladnumbers[i].setSelectedtext(dummytext1[i]);			   
		   	 } 
			}catch(Exception e){}
		
		((EditText)findViewById(R.id.edwallcladdingothr)).addTextChangedListener(new CustomTextWatcherSpace(((EditText)findViewById(R.id.edwallcladdingothr))));
	}
	private void Wallcladdingstories() {
		// TODO Auto-generated method stub
		try{
			selecttbl();
			if(!Wallcladding.equals(""))
			{
				wcvalue="";
				String Wall_Cladding_Split[] = Wallcladding.split("\\^",-1);
				for(int k=0;k<Wall_Cladding_Split.length;k++)
				{
					if(!Wall_Cladding_Split[k].trim().equals(""))
		    		{
		    			String WC_split[] = Wall_Cladding_Split[k].split(",");
		    			for(int j=0;j<WC_split.length;j++)
		    			{
		    					if(!items.contains(WC_split[j].trim()))
			    				{
			    					if(l==0)
				    				{
				    					wallcladvalue[k] = Wall_Cladding_Split[k].replace(WC_split[j],"");								
										l++;
				    				}
				    				else
				    				{
				    					wallcladvalue[k] = wallcladvalue[k].replace(WC_split[j],"");			    						
				    				}
			    				}
			    				else
			    				{
			    					if(l==0){
										if(wallcladvalue[k]==null) {wallcladvalue[k]="";wallcladvalue[k] =Wall_Cladding_Split[k];}
										else{wallcladvalue[k] =Wall_Cladding_Split[k];}
									}
			    				}	
			    				
		    			}l=0;	
		    		}		
		    		else
		    		{
		    				wallcladvalue[k]="";
		    		}
		    		
		    		if(!wallcladvalue[k].equals(""))
		    		{
		    			if(wallcladvalue[k].contains(",,"))
			    		{
			    			wallcladvalue[k]=wallcladvalue[k].replace(",,", "");
			    		}
		    			if(!wallcladvalue[k].equals(""))
		    			{
		    				String lastChar = wallcladvalue[k].substring(wallcladvalue[k].length()-1);
		    				if (lastChar.contains(",")) {
		    					wallcladvalue[k] = wallcladvalue[k].substring(0,wallcladvalue[k].length()-1);
		    				}
		    			}
		    		}	    
		    		
		    		
		    		spnwallcladnumbers[k].setItems1(items,"--Select--",new multi1(),wallcladvalue[k]);	    	
		    		wcvalue +=wallcladvalue[k]+"^";
		    		
				}				
				if(wcvalue.substring(wcvalue.lastIndexOf("^")).equals("^"))
				{
					wcvalue= wcvalue.substring(0,wcvalue.lastIndexOf("^"));
				}
				
			}
			if(c1.getCount()!=0)
			{
				cf.arr_db.execSQL("UPDATE "	+ cf.BI_General	+ " SET BI_WALLCLAD='" + wcvalue+ "' WHERE fld_srid ='"+ cf.selectedhomeid + "'");
			}		
		}
	    catch(Exception e)
	    {
	    	System.out.println("wallcladding updating"+e.getMessage());
	    }
	}
	class multi1 implements MultiSpinnerListener{
			public void onItemsSelected(boolean[] selected) {
				// TODO Auto-generated method stub
				try
				{
					for(int i=0;i<spnwallcladnumbers.length;i++)
					{
						if(!"You have selected --Select--".equals(spnwallcladnumbers[i].getSelectedtext().toString()))
						{
							if(spnwallcladnumbers[i].getSelectedtext().toString().contains(","))
							{
								floorchoosen = " to view the selected Floors";
							}
							else
							{
								floorchoosen = " to view the selected Floor";
							}
							
							System.out.println("floorchoosen"+spnwallcladnumbers[i].getSelectedtext().toString());
							if(!spnwallcladnumbers[i].getSelectedtext().toString().equals(""))
							{								
								tvstoriesdisp1[i].setText("You have selected " +spnwallcladnumbers[i].getSelectedtext().toString());
								tvwallcladding[i].setVisibility(cf.v1.VISIBLE);
								tvwcothertxt[i].setVisibility(cf.v1.VISIBLE);
								tvwallcladding[i].setText(Html.fromHtml("<font color='#0000FF'><u>Click here</u></font>"));
								tvwcothertxt[i].setText(Html.fromHtml("<font color='#D11C1C'>"+floorchoosen+"</font>"));
								
							}else
							{
								tvstoriesdisp1[i].setVisibility(cf.v1.GONE);
								tvwallcladding[i].setVisibility(cf.v1.INVISIBLE);
								tvwcothertxt[i].setVisibility(cf.v1.INVISIBLE);
							}
						}
					}
				}
				catch(Exception e)
				{
					System.out.println("E "+e.getMessage());
				}
			}
	 }	
	class OnClickListener1 implements OnClickListener {
		   int id;
		   OnClickListener1(int id1)
			{
				this.id=id1;
			}
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog1 = new Dialog(BIWallcladding.this,android.R.style.Theme_Translucent_NoTitleBar);
				dialog1.getWindow().setContentView(R.layout.alertwallstories);
				((TextView)dialog1.findViewById(R.id.txtheading)).setText(tvwctitle[id].getText().toString());
				if(tvwctitle[id].getText().toString().equals("Other"))
				{
					((TextView)dialog1.findViewById(R.id.txtheading)).setText(tvwctitle[id].getText().toString()+" ("+((EditText)findViewById(R.id.edwallcladdingothr)).getText().toString()+")");
				}
				else
				{
					((TextView)dialog1.findViewById(R.id.txtheading)).setText(tvwctitle[id].getText().toString());
				}						
				myTable = (TableLayout)dialog1.findViewById(R.id.mainLayout);
				alertwallstories(id);
				
				((ImageView)dialog1.findViewById(R.id.imagehelpclose)).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog1.setCancelable(true);
						dialog1.dismiss();
					}
				});		
				
				((Button)dialog1.findViewById(R.id.okbtn)).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog1.setCancelable(true);
						dialog1.dismiss();
					}
				});
				dialog1.show();
			}
	    }
	protected void alertwallstories(int val) {
		// TODO Auto-generated method stub
		cf.getPolicyholderInformation(cf.selectedhomeid);
		
		System.out.println("SSSS"+spnwallcladnumbers[val].getSelectedtext().toString());
		
		if(!spnwallcladnumbers[val].getSelectedtext().toString().equals(""))
		{
			wallclad= spnwallcladnumbers[val].getSelectedtext().toString().replace("You have selected ","");			
			Wall_Split = wallclad.trim().split(",");
		}
		/*if(WC_Split[val]==null)
		{
			wallclad= spnwallcladnumbers[val].getSelectedtext().toString().replace("You have selected ","");			
			Wall_Split = wallclad.trim().split(",");
		}
		else
		{
			System.out.println("ELEE");
			Wall_Split = WC_Split[val].split(",");
		}*/
		
		int k=0;
			for(int i=0;i<10;i++)
	        {
				tr =new TableRow(this);
		        TableRow.LayoutParams params = new TableRow.LayoutParams(55,30);
		        params.setMargins(1, 1, 0, 1);
		        tr.setOrientation(tr.HORIZONTAL);
		        for (int j=1;j<=10;j++)
		        {
		        	 tv[j] = new TextView(this);
		        	 tv[j].setTypeface(null, Typeface.BOLD);
		        	 tv[j].setGravity(Gravity.CENTER);
		        	 tv[j].setLayoutParams(params);
		        	 tv[j].setTextColor(Color.BLACK);
		        	 tv[j].setBackgroundResource(R.color.Wall_Light);
		        	 String l = ((i*10)+j)+"";	 
		 	         if(Integer.parseInt(l)<=Integer.parseInt(cf.Stories))
		        	 {
		        		 tv[j].setText(l);
		        		 tv[j].setBackgroundResource(R.color.Wall_selected);		 
		        	 }
		        	 else
		        	 {
		        		 tv[j].setText(l); 
		        		 tv[j].setTextColor(Color.parseColor("#bbd3e8"));
		        	 }
		 	         if(k<Wall_Split.length){
		 	        	if(Wall_Split[k].trim().equals(l))
		 	        	 {
		 	        		 tv[j].setBackgroundResource(R.color.Wall_dark);	
		 	        		 tv[j].setTextColor(Color.WHITE);k++;
		 	        	 }
		 	         }
		 	         	
		 	        tr.addView(tv[j]);
		        }    
		        myTable.addView(tr); 
	        } 
	}
	private void setValue() {
		// TODO Auto-generated method stub
		try
		{
			selecttbl();
			if(c1.getCount()==0)
			{
				((CheckBox)findViewById(R.id.wcnotappl)).setChecked(true);
				((LinearLayout)findViewById(R.id.wallcladdinglin)).setVisibility(cf.v1.GONE);
			}
			else
			{
				if(WC_NA.equals("0") || WC_NA.equals(""))
				{
					((LinearLayout)findViewById(R.id.wallcladdinglin)).setVisibility(cf.v1.VISIBLE);
						if(!Wallcladding.equals(""))
						{
							WC_Split = Wallcladding.split("\\^",-1);			
								for(int k=0;k<WC_Split.length;k++)
								{
										dummytext1[k].setText("");
										spnwallcladnumbers[k].setItems1(items,"--Select--",new multi1(),WC_Split[k]);
										spnwallcladnumbers[k].setSelectedtext(dummytext1[k]);
										if(spnwallcladnumbers[k].getSelectedtext().toString().equals("You have selected "))
										{				
											spnwallclad[k] =  spnwallcladnumbers[k].getSelectedtext().toString().replace("You have selected ","");
										}
										else
										{
											spnwallclad[k] =  spnwallcladnumbers[k].getSelectedtext().toString();
										}
										if(!spnwallclad[k].equals(""))
										{
											
											if(spnwallclad[k].contains(","))
											{
												floorchoosen = " to view the selected Floors";
											}
											else
											{
												floorchoosen = " to view the selected Floor";
											}
											
											tvwallcladding[k].setVisibility(cf.v1.VISIBLE);
											tvwcothertxt[k].setVisibility(cf.v1.VISIBLE);
											tvwallcladding[k].setText(Html.fromHtml("<font color='#0000FF'><u>Click here</u></font>"));
											tvwcothertxt[k].setText(Html.fromHtml("<font color='#D11C1C'>"+floorchoosen+"</font>"));
										}
										else
										{
											tvwallcladding[k].setVisibility(cf.v1.INVISIBLE);
											tvwcothertxt[k].setVisibility(cf.v1.INVISIBLE);
										}				
								 }
								 if(!wcothervalue.equals(""))
								 {
									((EditText)findViewById(R.id.edwallcladdingothr)).setText(wcothervalue);
								 }
								 else
								 {
									 ((EditText)findViewById(R.id.edwallcladdingothr)).setText("");
								 }	
								 ((TextView)findViewById(R.id.savetxtwc)).setVisibility(cf.v1.VISIBLE);
							}
						else
						{
							((LinearLayout)findViewById(R.id.wallcladdinglin)).setVisibility(cf.v1.GONE);
							((TextView)findViewById(R.id.savetxtwc)).setVisibility(cf.v1.GONE);
							((CheckBox)findViewById(R.id.wcnotappl)).setChecked(true);
						}
				}
				else
				{
					((CheckBox)findViewById(R.id.wcnotappl)).setChecked(true);
					((LinearLayout)findViewById(R.id.wallcladdinglin)).setVisibility(cf.v1.GONE);
					((TextView)findViewById(R.id.savetxtwc)).setVisibility(cf.v1.VISIBLE);
				}
			}
		}
		catch(Exception e)
		{
			
		}
	}
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.mouseoverid:
			cf.ShowToast("Use for 4 Point, Commercial Mitigation,Sinkhole, Replacement Cost Inspections.",0);
			break;
		case R.id.wcsave:
			selecttbl();
			if(((CheckBox)findViewById(R.id.wcnotappl)).isChecked()==false)
			{	
				if(!"".equals(spnwallcladnumbers[0].getSelectedtext().toString()) || !"".equals(spnwallcladnumbers[1].getSelectedtext().toString())
						||	!"".equals(spnwallcladnumbers[2].getSelectedtext().toString()) || !"".equals(spnwallcladnumbers[3].getSelectedtext().toString())
						||	!"".equals(spnwallcladnumbers[4].getSelectedtext().toString()) || !"".equals(spnwallcladnumbers[5].getSelectedtext().toString())
						||	!"".equals(spnwallcladnumbers[6].getSelectedtext().toString()) || !"".equals(spnwallcladnumbers[7].getSelectedtext().toString())
						||	!"".equals(spnwallcladnumbers[8].getSelectedtext().toString()) || !"".equals(spnwallcladnumbers[9].getSelectedtext().toString())
						||	!"".equals(spnwallcladnumbers[10].getSelectedtext().toString()) || !"".equals(spnwallcladnumbers[11].getSelectedtext().toString())
						||	!"".equals(spnwallcladnumbers[12].getSelectedtext().toString()) || !"".equals(spnwallcladnumbers[13].getSelectedtext().toString()))
				{
						if(spnwallcladnumbers[13].getSelectedtext().toString().equals(""))
						{
							InsertWallCladding();
						}
						else
						{
							if(((EditText)findViewById(R.id.edwallcladdingothr)).getText().toString().trim().equals(""))
							{
								cf.ShowToast("Please enter the other text for Wall Cladding",1);
								cf.setFocus((EditText)findViewById(R.id.edwallcladdingothr));
							}
							else
							{
								InsertWallCladding();
							}
						}
				}
				else
				{
					((EditText)findViewById(R.id.edwallcladdingothr)).setText("");
					for(int i=0;i<tvwallcladding.length;i++)
					{
						tvwallcladding[i].setVisibility(cf.v1.INVISIBLE);
						tvwcothertxt[i].setVisibility(cf.v1.INVISIBLE);	
					}
					cf.ShowToast("Please select atleast one Wall Cladding.",1);			
				}
			}
			else
			{
				InsertWallCladding();
			}
			break;
		case R.id.wcnotappl:			
			if(((CheckBox)findViewById(R.id.wcnotappl)).isChecked())
			{
				wcnotappl="1";totwc="";
				if(!Wallcladding.equals(""))
				{
					AlertDialog.Builder builder = new AlertDialog.Builder(BIWallcladding.this);
					builder.setTitle("Confirmation");
					builder.setIcon(R.drawable.alertmsg);
					builder.setMessage("Do you want to Clear the Wall Cladding Information?").setCancelable(false)
							.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,int id1) 
										{
												wallcladdingclear();
												((CheckBox)findViewById(R.id.wcnotappl)).setChecked(true);
												((LinearLayout)findViewById(R.id.wallcladdinglin)).setVisibility(cf.v1.GONE);
												((TextView)findViewById(R.id.savetxtwc)).setVisibility(cf.v1.GONE);
										}
									})
							.setNegativeButton("No",new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,	int id) {
											
											((CheckBox)findViewById(R.id.wcnotappl)).setChecked(false);
											dialog.cancel();											
									}
							});
					builder.show();
				}
				else
				{
					((TextView)findViewById(R.id.savetxtwc)).setVisibility(cf.v1.GONE);
					((LinearLayout)findViewById(R.id.wallcladdinglin)).setVisibility(cf.v1.GONE);
				}
				
			}
			else
			{
				wcnotappl="0";totwc="";wallcladdingclear();
				((LinearLayout)findViewById(R.id.wallcladdinglin)).setVisibility(cf.v1.VISIBLE);
				((TextView)findViewById(R.id.savetxtwc)).setVisibility(cf.v1.GONE);
			}
			break;
		}		
	}
	protected void wallcladdingclear() {
		// TODO Auto-generated method stub
		
		for(int i=0;i<tvwallcladding.length;i++)
		{
			tvwallcladding[i].setVisibility(cf.v1.INVISIBLE);
			tvwcothertxt[i].setVisibility(cf.v1.INVISIBLE);	
			dummytext1[i].setText("");
			spnwallcladnumbers[i].setItems1(items,"--Select--",new multi1(),dummytext1[i].getText().toString());
			spnwallcladnumbers[i].setSelectedtext(dummytext1[i]);	
		}
		((EditText)findViewById(R.id.edwallcladdingothr)).setText("");Wallcladding="";
	}
	private void InsertWallCladding() {
		// TODO Auto-generated method stub
		spnwc1=spnwallcladnumbers[0].getSelectedtext().toString();
		spnwc2=spnwallcladnumbers[1].getSelectedtext().toString();
		spnwc3=spnwallcladnumbers[2].getSelectedtext().toString();
		spnwc4=spnwallcladnumbers[3].getSelectedtext().toString();
		spnwc5=spnwallcladnumbers[4].getSelectedtext().toString();
		spnwc6=spnwallcladnumbers[5].getSelectedtext().toString();
		spnwc7=spnwallcladnumbers[6].getSelectedtext().toString();
		spnwc8=spnwallcladnumbers[7].getSelectedtext().toString();
		spnwc9=spnwallcladnumbers[8].getSelectedtext().toString();	
		spnwc10=spnwallcladnumbers[9].getSelectedtext().toString();
		spnwc11=spnwallcladnumbers[10].getSelectedtext().toString();
		spnwc12=spnwallcladnumbers[11].getSelectedtext().toString();
		spnwc13=spnwallcladnumbers[12].getSelectedtext().toString();
		spnwc14=spnwallcladnumbers[13].getSelectedtext().toString();
		
		//System.out.println("value="+spnwc1+"/n"+spnwc2+"/n"+spnwc3+"/n"+spnwc4+"/n"+spnwc5+"/n"+spnwc6+"/n"+spnwc7+"/n"+spnwc8+"/n"+spnwc9+"/n"+spnwc10+"/n"+spnwc11+"/n"+spnwc12+"/n"+spnwc13+"/n"+spnwc14+"/n");
		
		if(!spnwc1.equals("")){spnwc1=spnwc1.replace("You have selected ", "");spnwc1=spnwc1.replace(" ", "");}else{spnwc1=" ";}
		if(!spnwc2.equals("")){spnwc2=spnwc2.replace("You have selected ", "");spnwc2=spnwc2.replace(" ", "");}else{spnwc2=" ";}
		if(!spnwc3.equals("")){spnwc3=spnwc3.replace("You have selected ", "");spnwc3=spnwc3.replace(" ", "");}else{spnwc3=" ";}
		if(!spnwc4.equals("")){spnwc4=spnwc4.replace("You have selected ", "");spnwc4=spnwc4.replace(" ", "");}else{spnwc4=" ";}
		if(!spnwc5.equals("")){spnwc5=spnwc5.replace("You have selected ", "");spnwc5=spnwc5.replace(" ", "");}else{spnwc5=" ";}
		if(!spnwc6.equals("")){spnwc6=spnwc6.replace("You have selected ", "");spnwc6=spnwc6.replace(" ", "");}else{spnwc6=" ";}
		if(!spnwc7.equals("")){spnwc7=spnwc7.replace("You have selected ", "");spnwc7=spnwc7.replace(" ", "");}else{spnwc7=" ";}
		if(!spnwc8.equals("")){spnwc8=spnwc8.replace("You have selected ", "");spnwc8=spnwc8.replace(" ", "");}else{spnwc8=" ";}
		if(!spnwc9.equals("")){spnwc9=spnwc9.replace("You have selected ", "");spnwc9=spnwc9.replace(" ", "");}else{spnwc9=" ";}
		if(!spnwc10.equals("")){spnwc10=spnwc10.replace("You have selected ", "");spnwc10=spnwc10.replace(" ", "");}else{spnwc10=" ";}
		if(!spnwc11.equals("")){spnwc11=spnwc11.replace("You have selected ", "");spnwc11=spnwc11.replace(" ", "");}else{spnwc11=" ";}
		if(!spnwc12.equals("")){spnwc12=spnwc12.replace("You have selected ", "");spnwc12=spnwc12.replace(" ", "");}else{spnwc12=" ";}
		if(!spnwc13.equals("")){spnwc13=spnwc13.replace("You have selected ", "");spnwc13=spnwc13.replace(" ", "");}else{spnwc13=" ";}
		if(!spnwc14.equals("")){spnwc14=spnwc14.replace("You have selected ", "");spnwc14=spnwc14.replace(" ", "");}else{spnwc14=" ";}
		
		if(!"".equals(spnwc1) || !"".equals(spnwc2) || !"".equals(spnwc3) || !"".equals(spnwc4) || !"".equals(spnwc5) ||  !"".equals(spnwc6) || !"".equals(spnwc7) || !"".equals(spnwc8) 
		|| !"".equals(spnwc9) || !"".equals(spnwc10) || !"".equals(spnwc11) || !"".equals(spnwc12) || !"".equals(spnwc13) || !"".equals(spnwc14))
		{
			totwc=spnwc1+"^"+spnwc2+"^"+spnwc3+"^"+spnwc4+"^"+spnwc5+"^"+spnwc6+"^"+spnwc7+"^"+spnwc8+"^"+spnwc9+"^"+spnwc10+"^"+spnwc11+"^"+spnwc12+"^"+spnwc13+"^"+spnwc14;
		}
		
		
		if(((CheckBox)findViewById(R.id.wcnotappl)).isChecked()){wcnotappl="1";totwc="";s1="";}else{wcnotappl="0";}
		updatenotappl();
		cf.ShowToast("Wall Cladding saved successfully",1);
		cf.goclass(457);
		
	}
	private void updatenotappl() {
		// TODO Auto-generated method stub
		if(spnwc14.trim().equals(""))
		{
			s1 = ""; 
		}
		else
		{
			s1=cf.encode(((EditText)findViewById(R.id.edwallcladdingothr)).getText().toString().trim());
		}	
		try
		{
			selecttbl();
			if(c1.getCount()==0)
			{
				cf.arr_db.execSQL("INSERT INTO " + cf.BI_General + " (fld_srid,BI_WALLCLAD,BI_WALLCLAD_OTHER,BI_WC_NA)"
						+ "VALUES ('"+cf.selectedhomeid+"','"+totwc+"','"+s1+"','"+wcnotappl+"')");
			}
			else
			{
				cf.arr_db.execSQL("UPDATE "
						+ cf.BI_General
						+ " SET BI_WC_NA='"+wcnotappl+"', BI_WALLCLAD='"+totwc+"',BI_WALLCLAD_OTHER='"+s1+"'"
						+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
			}			
		}
		catch(Exception e)
		{
			System.out.println("wsnotappl"+e.getMessage());
		}
	}
		private void selecttbl() {
			c1= cf.SelectTablefunction(cf.BI_General, " where fld_srid='"+cf.selectedhomeid+"'");
			if(c1.getCount()>0)
			{
				c1.moveToFirst();
				 wcothervalue = cf.decode(c1.getString(c1.getColumnIndex("BI_WALLCLAD_OTHER")));
				 Wallcladding= cf.decode(c1.getString(c1.getColumnIndex("BI_WALLCLAD")));
				 WC_NA= c1.getString(c1.getColumnIndex("BI_WC_NA"));
			}
		}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			cf.goclass(455);
			return true;
		}		
		return super.onKeyDown(keyCode, event);
	}
}