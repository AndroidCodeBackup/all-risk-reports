/*

* Project: ALL RISK REPORT
* Module : Attic.java
* Creation History:
Created By : Gowri on 
* Modification History:
Last Modified By : Gowri on 3/6/2013
************************************************************ 
*/
package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.Electrical.checklistenetr;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.AvoidXfermode;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;

import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.*;

import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TableRow;
import android.widget.TextView;

public class Attic extends Activity{
	public int[] atticval = {R.id.atcchk1,R.id.atcchk2,R.id.atcchk3,R.id.atcchk4};
	CheckBox[] atcchkopt = new CheckBox[4];
	public int[] gws = {R.id.gws_chk1,R.id.gws_chk2,R.id.gws_chk3,R.id.gws_chk4,R.id.gws_chk5,R.id.gws_chk6,R.id.gws_chk7};
	CheckBox[] gwschk = new CheckBox[7];
	public int[] rwc = {R.id.rwc_chk1,R.id.rwc_chk2,R.id.rwc_chk3,R.id.rwc_chk4,R.id.rwc_chk5,R.id.rwc_chk6,R.id.rwc_chk7,
			            R.id.rwc_chk8,R.id.rwc_chk9,R.id.rwc_chk10};
	CheckBox[] rwcchk = new CheckBox[10];
	public int[] rdm = {R.id.rdm_chk1,R.id.rdm_chk2,R.id.rdm_chk3,R.id.rdm_chk4,R.id.rdm_chk5,R.id.rdm_chk6,R.id.rdm_chk7,
            R.id.rdm_chk8,R.id.rdm_chk9,R.id.rdm_chk10,R.id.rdm_chk11};
    CheckBox[] rdmchk = new CheckBox[11];
    public int[] rsm = {R.id.rsm_chk1,R.id.rsm_chk2,R.id.rsm_chk3,R.id.rsm_chk4,R.id.rsm_chk5,R.id.rsm_chk7,R.id.rsm_chk8,R.id.rsm_chk9,R.id.rsm_chk10,R.id.rsm_chk11,R.id.rsm_chk6};
    CheckBox[] rsmchk = new CheckBox[11];
    public int[] rdt = {R.id.rdt_chk1,R.id.rdt_chk2,R.id.rdt_chk3,R.id.rdt_chk4,R.id.rdt_chk5,R.id.rdt_chk6,R.id.rdt_chk7,
    		R.id.rdt_chk8,R.id.rdt_chk9,R.id.rdt_chk10,R.id.rdt_chk11};
    CheckBox[] rdtchk = new CheckBox[11];
    public int[] rda = {R.id.rda_chk1,R.id.rda_chk2,R.id.rda_chk3,R.id.rda_chk4,R.id.rda_chk5,R.id.rda_chk6,R.id.rda_chk7,
    		R.id.rda_chk8,R.id.rda_chk9,R.id.rda_chk10,R.id.rda_chk11};
    CheckBox[] rdachk = new CheckBox[11];
    public int[] rts = {R.id.rts_chk1,R.id.rts_chk2,R.id.rts_chk3,R.id.rts_chk4,R.id.rts_chk5,R.id.rts_chk6,R.id.rts_chk7};
    CheckBox[] rtschk = new CheckBox[7];
    public int[] ns = {R.id.ns_chk1,R.id.ns_chk2,R.id.ns_chk3,R.id.ns_chk4,R.id.ns_chk5,R.id.ns_chk6};
    CheckBox[] nschk = new CheckBox[6];
    public int[] ans = {R.id.ans_chk1,R.id.ans_chk2,R.id.ans_chk3,R.id.ans_chk4,R.id.ans_chk5,R.id.ans_chk6,R.id.ans_chk7,R.id.ans_chk8};
    CheckBox[] anschk = new CheckBox[8];
	CommonFunctions cf;
	String selatcopt="",acmsrdtxt="Beyond Scope of Inspection",acdsrdtxt="Beyond Scope of Inspection",acdrmrdtxt="Beyond Scope of Inspection",
			acnsardtxt="Beyond Scope of Inspection",acotrrdtxt="Beyond Scope of Inspection",
			rwc3rdgtxt="",rwc4rdgtxt="",rwc5rdgtxt="",rwctrdgtxt="",rwc1trdgtxt="",rdmchk_vlaue="",showstr="",
			rsmchk_vlaue="",rdtchk_vlaue="",rdachk_vlaue="",rtschk_vlaue="",nschk_vlaue="",retrdmtxt="",
		    anschk_vlaue="",rwcchk_vlaue="",gwschk_vlaue="",atticcomt="",retgwschk_vlaue="",atcchk_vlaue="",retacmsrdtxt="";
	RadioGroup acmsrdgval,acdsrdgval,acdrmrdgval,acnsardgval,acotrrdgval,
	           rwc3rdgval,rwc4rdgval,rwc5rdgval,rwctrdgval,rwc1trdgval;
	int j1;
	String acval="Overall Condition of Roof";
	String atc="",rdchkval,retgwst,retrtow,rwcchkval,gwschkval,acchkval,retrd,retrwc,retgws,retacn;
	Cursor chkattic_save,chkatc_save;
	boolean r=false,s=false,a=false,b=false,c=false,d=false,e=false,f=false,g=false,h=false,i=false,j=false,k=false,
			nl3ck=false,nl4ck=false,nl5ck=false,tsck=false,ts15ck=false,msck=false,dsck=false,drmck=false,nsack=false,otrck=false,
			load_comment=true,gws_save=false,ac_save=false,rd_save=false,rwc_save=false;
	 boolean atcchk=false;

	
	@Override
	   public void onCreate(Bundle savedInstanceState) {

	        super.onCreate(savedInstanceState);
	        cf=new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);
				
		 	}
	        setContentView(R.layout.attic);
	        cf.getDeviceDimensions();
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
			hdr_layout.addView(new HdrOnclickListener(this,1,"Four Point Inspection","Attic System",2,0,cf));
         	LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
         	main_layout.setMinimumWidth(cf.wd);
         	main_layout.addView(new MyOnclickListener(getApplicationContext(), 2, 0,0,cf));
	        LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 22, 1,0,cf));
			/*TableRow tblrw = (TableRow)findViewById(R.id.row2);
			tblrw.setMinimumHeight(cf.ht-100);*/
			findViewById(R.id.insidecontentrel).setMinimumHeight(cf.ht-250);
			cf.CreateARRTable(22);
			try
			{
				chkatc_save=cf.SelectTablefunction(cf.Four_Electrical_Attic, " where fld_srid='"+cf.selectedhomeid+"'");
				if(chkatc_save.getCount()==0){
					cf.arr_db.execSQL("INSERT INTO "
							+ cf.Four_Electrical_Attic
							+ " (fld_srid,atcinc,atcoptions,atccomments,rfdcinc,rfdcmat,rfstmat,rfdcthi,rfdcatt,rftruss,"+
							"nailspac,missednails,rfwallinc,rtowopt,nail3,nail4,nail5,truss,truss1,gablewallinc,gablewallopt,"+
							"atccondinc,moisture,dsheath,droof,nsalt,othertxt,other)"
							+ "VALUES('"+cf.selectedhomeid+"','','','','','','','','','','','',"+
							  "'','','','','','','','','','','','','','','','')");

				}
				else
				{
					
				}
			}
			catch (Exception e) {
				// TODO: handle exception
			}
			chk_values();
			declarations();
			Attic_SetValue();
	}
	private void Attic_SetValue() {
		// TODO Auto-generated method stub
		try{
			  chk_values();
			   Cursor attic_retrive=cf.SelectTablefunction(cf.Four_Electrical_Attic, " where fld_srid='"+cf.selectedhomeid+"'");
			   System.out.println("attic_retrive.getCount()="+attic_retrive.getCount());
			   if(attic_retrive.getCount()>0)
			   {  
				   attic_retrive.moveToFirst();
				   if(cf.decode(attic_retrive.getString(attic_retrive.getColumnIndex("Attic_Na"))).equals("false"))
					{
						findViewById(R.id.attic_na_content).setVisibility(View.VISIBLE);
					
				  
				   if(!cf.decode(attic_retrive.getString(attic_retrive.getColumnIndex("gablewallinc"))).equals("") || !cf.decode(attic_retrive.getString(attic_retrive.getColumnIndex("atccondinc"))).equals("")|| !cf.decode(attic_retrive.getString(attic_retrive.getColumnIndex("rfwallinc"))).equals("")|| !cf.decode(attic_retrive.getString(attic_retrive.getColumnIndex("rfdcinc"))).equals(""))
				   {
				  /* if(attic_retrive.getString(attic_retrive.getColumnIndex("atcinc")).equals("Yes"))
				   {*/
					   System.out.println("inside");
					   selatcopt=cf.decode(attic_retrive.getString(attic_retrive.getColumnIndex("atcoptions")));
					   if(!selatcopt.equals(""))
					   {
						   cf.setvaluechk1(selatcopt,atcchkopt,((EditText)findViewById(R.id.rdmotr)));
					   }
					   else
					   {
						   atcchkopt[0].setChecked(true);
						   atcchkopt[3].setChecked(true);
						   
					   }
					   System.out.println("roofdeck");
					   if(attic_retrive.getString(attic_retrive.getColumnIndex("rfdcinc")).equals("0"))
					   {
					    	    ((LinearLayout)findViewById(R.id.rdlin)).setVisibility(cf.v1.VISIBLE);
								((TextView)findViewById(R.id.savetxtrd)).setVisibility(cf.v1.VISIBLE);
								((ImageView)findViewById(R.id.shwrfdeck)).setEnabled(true);
								((ImageView)findViewById(R.id.shwrfdeck)).setVisibility(cf.v1.GONE);
								((ImageView)findViewById(R.id.hdwrfdeck)).setVisibility(cf.v1.VISIBLE);
							//	((RelativeLayout)findViewById(R.id.atticrelrdchk)).setEnabled(true);
							    rdmchk_vlaue=cf.decode(attic_retrive.getString(attic_retrive.getColumnIndex("rfdcmat")));
							    System.out.println("rdmchk_vlaue"+rdmchk_vlaue);							    
							    cf.setvaluechk1(rdmchk_vlaue,rdmchk,((EditText)findViewById(R.id.rdmotr)));
							    
							    rsmchk_vlaue=cf.decode(attic_retrive.getString(attic_retrive.getColumnIndex("rfstmat")));
							    System.out.println("rsmchk_vlaue"+rsmchk_vlaue);
							    cf.setvaluechk1(rsmchk_vlaue,rsmchk,((EditText)findViewById(R.id.rsmotr)));
							    
							    rdtchk_vlaue=cf.decode(attic_retrive.getString(attic_retrive.getColumnIndex("rfdcthi")));
							    cf.setvaluechk1(rdtchk_vlaue,rdtchk,((EditText)findViewById(R.id.rdtotr)));
							    rdachk_vlaue=cf.decode(attic_retrive.getString(attic_retrive.getColumnIndex("rfdcatt")));
							    cf.setvaluechk1(rdachk_vlaue,rdachk,((EditText)findViewById(R.id.rdaotr)));
							    rtschk_vlaue=cf.decode(attic_retrive.getString(attic_retrive.getColumnIndex("rftruss")));
							    cf.setvaluechk1(rtschk_vlaue,rtschk,((EditText)findViewById(R.id.rdaotr)));
							    nschk_vlaue=cf.decode(attic_retrive.getString(attic_retrive.getColumnIndex("nailspac")));
							    cf.setvaluechk1(nschk_vlaue,nschk,((EditText)findViewById(R.id.rdmotr)));
							    anschk_vlaue=cf.decode(attic_retrive.getString(attic_retrive.getColumnIndex("missednails")));
							    cf.setvaluechk1(anschk_vlaue,anschk,((EditText)findViewById(R.id.rdmotr)));
					   }
					   else if(attic_retrive.getString(attic_retrive.getColumnIndex("rfdcinc")).equals("1"))
					   {
							   ((LinearLayout)findViewById(R.id.rdlin)).setVisibility(cf.v1.GONE);
							   ((TextView)findViewById(R.id.savetxtrd)).setVisibility(cf.v1.VISIBLE);
							   ((CheckBox)findViewById(R.id.rd_na)).setChecked(true);
							   ((ImageView)findViewById(R.id.shwrfdeck)).setEnabled(false);
							   ((ImageView)findViewById(R.id.shwrfdeck)).setVisibility(cf.v1.VISIBLE);
							   ((ImageView)findViewById(R.id.hdwrfdeck)).setVisibility(cf.v1.GONE);
							
					   }
					   else
					   {
						   Cursor c =cf.SelectTablefunction(cf.Rct_table, " WHERE RCT_Ssrid='"+cf.selectedhomeid+"' and RCT_insp_id='1'");
						   if(c.getCount()>0){
							   c.moveToFirst();
							  
							   for(int m=0;m<rsmchk.length;m++)
							   {
								   c.moveToFirst();
							   for(int i=0;i<c.getCount();i++,c.moveToNext())
							   {
								  // System.out.println(" top the values rsmchk_vlaue="+rsmchk_vlaue+" RCT="+c.getString(c.getColumnIndex("RCT_rst"))+" rsmchk[m]="+rsmchk[m].getText().toString().trim());
								   if(!rsmchk_vlaue.contains(cf.decode(c.getString(c.getColumnIndex("RCT_rst")))))
								   {
									  
									   if(cf.decode(c.getString(c.getColumnIndex("RCT_rst"))).equals(rsmchk[m].getText().toString().trim()))
									  {
									   if(!cf.decode(c.getString(c.getColumnIndex("RCT_rst"))).equals("Other"))
									   {
										   if(!rsmchk_vlaue.equals(""))
										   {
										   rsmchk_vlaue+="&#94;"+cf.decode(c.getString(c.getColumnIndex("RCT_rst")));
										   }
										   else
										   {
											   rsmchk_vlaue=cf.decode(c.getString(c.getColumnIndex("RCT_rst")));
										   }
									   }
									   else
									   {
										   rsmchk_vlaue+="&#40;"+cf.decode(c.getString(c.getColumnIndex("RCT_other_rst")));
									   }
								      }
									   }
									  
								   }
								  
							   }
							 //  System.out.println("rsmchk_vlaue"+rsmchk_vlaue);
							    cf.setvaluechk1(rsmchk_vlaue,rsmchk,((EditText)findViewById(R.id.rsmotr)));
						   }
						  // if()
						 //  rsmchk_vlaue=cf.decode(attic_retrive.getString(attic_retrive.getColumnIndex("rfstmat")));
						    
					   }
					  // System.out.println("val="+attic_retrive.getString(attic_retrive.getColumnIndex("rfwallinc")));
					   if(attic_retrive.getString(attic_retrive.getColumnIndex("rfwallinc")).equals("0"))
					   {
						   ((LinearLayout)findViewById(R.id.rwclin)).setVisibility(cf.v1.VISIBLE);
						   ((TextView)findViewById(R.id.savetxtrwc)).setVisibility(cf.v1.VISIBLE);
						   ((ImageView)findViewById(R.id.shwrfwall)).setEnabled(true);
						   ((ImageView)findViewById(R.id.shwrfwall)).setVisibility(cf.v1.GONE);
						   ((ImageView)findViewById(R.id.hdrfwall)).setVisibility(cf.v1.VISIBLE);
					        rwcchk_vlaue=cf.decode(attic_retrive.getString(attic_retrive.getColumnIndex("rtowopt")));
						    cf.setvaluechk1(rwcchk_vlaue,rwcchk,((EditText)findViewById(R.id.rwcotr)));
						    rwc3rdgtxt=cf.decode(attic_retrive.getString(attic_retrive.getColumnIndex("nail3")));
							((RadioButton) rwc3rdgval.findViewWithTag(rwc3rdgtxt)).setChecked(true);
							rwc4rdgtxt=cf.decode(attic_retrive.getString(attic_retrive.getColumnIndex("nail4")));
							((RadioButton) rwc4rdgval.findViewWithTag(rwc4rdgtxt)).setChecked(true);
							rwc5rdgtxt=cf.decode(attic_retrive.getString(attic_retrive.getColumnIndex("nail5")));
							((RadioButton) rwc5rdgval.findViewWithTag(rwc5rdgtxt)).setChecked(true);
							rwctrdgtxt=cf.decode(attic_retrive.getString(attic_retrive.getColumnIndex("truss")));
							((RadioButton) rwctrdgval.findViewWithTag(rwctrdgtxt)).setChecked(true);
							rwc1trdgtxt=cf.decode(attic_retrive.getString(attic_retrive.getColumnIndex("truss1")));
							((RadioButton) rwc1trdgval.findViewWithTag(rwc1trdgtxt)).setChecked(true);
					   }
					   else if(attic_retrive.getString(attic_retrive.getColumnIndex("rfwallinc")).equals("1")){
						   ((LinearLayout)findViewById(R.id.rwclin)).setVisibility(cf.v1.GONE);
						   ((TextView)findViewById(R.id.savetxtrwc)).setVisibility(cf.v1.VISIBLE);
						   ((CheckBox)findViewById(R.id.rwc_na)).setChecked(true);
						   ((ImageView)findViewById(R.id.shwrfwall)).setEnabled(false);
						   ((ImageView)findViewById(R.id.shwrfwall)).setVisibility(cf.v1.VISIBLE);
						   ((ImageView)findViewById(R.id.hdrfwall)).setVisibility(cf.v1.GONE);
					   }
			     
					   
					   if(attic_retrive.getString(attic_retrive.getColumnIndex("gablewallinc")).equals("0"))
					   {
						   ((LinearLayout)findViewById(R.id.gwslin)).setVisibility(cf.v1.VISIBLE);
						   ((TextView)findViewById(R.id.savetxtgws)).setVisibility(cf.v1.VISIBLE);
						   ((ImageView)findViewById(R.id.shwgwall)).setEnabled(true);
						   ((ImageView)findViewById(R.id.shwgwall)).setVisibility(cf.v1.GONE);
						   ((ImageView)findViewById(R.id.hdgwall)).setVisibility(cf.v1.VISIBLE);
						    gwschk_vlaue=cf.decode(attic_retrive.getString(attic_retrive.getColumnIndex("gablewallopt")));
						    cf.setvaluechk1(gwschk_vlaue,gwschk,((EditText)findViewById(R.id.gwsotr)));
						   
					   }else if(attic_retrive.getString(attic_retrive.getColumnIndex("gablewallinc")).equals("1")){
						   ((LinearLayout)findViewById(R.id.gwslin)).setVisibility(cf.v1.GONE);
						   ((TextView)findViewById(R.id.savetxtgws)).setVisibility(cf.v1.VISIBLE);
						   ((CheckBox)findViewById(R.id.gws_na)).setChecked(true);
						   ((ImageView)findViewById(R.id.shwrfwall)).setEnabled(false);
						   ((ImageView)findViewById(R.id.shwgwall)).setVisibility(cf.v1.VISIBLE);
						   ((ImageView)findViewById(R.id.hdgwall)).setVisibility(cf.v1.GONE);
					  }
					  
					   if(attic_retrive.getString(attic_retrive.getColumnIndex("atccondinc")).equals("0"))
					   {
						   ((LinearLayout)findViewById(R.id.aclin)).setVisibility(cf.v1.VISIBLE);
						   ((TextView)findViewById(R.id.savetxtac)).setVisibility(cf.v1.VISIBLE);
						   ((ImageView)findViewById(R.id.shwatc)).setEnabled(true);
						   ((ImageView)findViewById(R.id.shwatc)).setVisibility(cf.v1.GONE);
						   ((ImageView)findViewById(R.id.hdatc)).setVisibility(cf.v1.VISIBLE);
						   String str1 =cf.decode(attic_retrive.getString(attic_retrive.getColumnIndex("moisture")));
						    if(str1.contains("Yes")){ 
							    String[] temp = str1.split("&#126;");
							    acmsrdtxt = temp[0];
							    ((RadioButton) acmsrdgval.findViewWithTag(acmsrdtxt)).setChecked(true);
							    ((LinearLayout)findViewById(R.id.msyslin)).setVisibility(cf.v1.VISIBLE);
							    ((EditText)findViewById(R.id.msyscomment)).setText(temp[1]);
						    }else{
						    	acmsrdtxt = str1;
						    	((RadioButton) acmsrdgval.findViewWithTag(acmsrdtxt)).setChecked(true);
							    ((LinearLayout)findViewById(R.id.msyslin)).setVisibility(cf.v1.GONE);
						    }
						    String str2 =cf.decode(attic_retrive.getString(attic_retrive.getColumnIndex("dsheath")));
						    if(str2.contains("Yes")){ 
							    String[] temp = str2.split("&#126;");
							    acdsrdtxt = temp[0];
							    ((RadioButton) acdsrdgval.findViewWithTag(acdsrdtxt)).setChecked(true);
							    ((LinearLayout)findViewById(R.id.dsyslin)).setVisibility(cf.v1.VISIBLE);
							    ((EditText)findViewById(R.id.dsyscomment)).setText(temp[1]);
						    }else{
						    	acdsrdtxt = str2;
						    	((RadioButton) acdsrdgval.findViewWithTag(acdsrdtxt)).setChecked(true);
							    ((LinearLayout)findViewById(R.id.dsyslin)).setVisibility(cf.v1.GONE);
						    }
						    String str3 =cf.decode(attic_retrive.getString(attic_retrive.getColumnIndex("droof")));
						    if(str3.contains("Yes")){ 
							    String[] temp = str3.split("&#126;");
							    acdrmrdtxt = temp[0];
							    ((RadioButton) acdrmrdgval.findViewWithTag(acdrmrdtxt)).setChecked(true);
							    ((LinearLayout)findViewById(R.id.drmyslin)).setVisibility(cf.v1.VISIBLE);
							    ((EditText)findViewById(R.id.drmyscomment)).setText(temp[1]);
						    }else{
						    	acdrmrdtxt = str3;
						    	((RadioButton) acdrmrdgval.findViewWithTag(acdrmrdtxt)).setChecked(true);
							    ((LinearLayout)findViewById(R.id.drmyslin)).setVisibility(cf.v1.GONE);
						    }
						    String str4 =cf.decode(attic_retrive.getString(attic_retrive.getColumnIndex("nsalt")));
						    if(str4.contains("Yes")){ 
							    String[] temp = str4.split("&#126;");
							    acnsardtxt = temp[0];
							    ((RadioButton) acnsardgval.findViewWithTag(acnsardtxt)).setChecked(true);
							    ((LinearLayout)findViewById(R.id.nsayslin)).setVisibility(cf.v1.VISIBLE);
							    ((EditText)findViewById(R.id.nsayscomment)).setText(temp[1]);
						    }else{
						    	acnsardtxt = str4;
						    	((RadioButton) acnsardgval.findViewWithTag(acnsardtxt)).setChecked(true);
							    ((LinearLayout)findViewById(R.id.nsayslin)).setVisibility(cf.v1.GONE);
						    }
						    ((EditText)findViewById(R.id.otrcond)).setText(cf.decode(attic_retrive.getString(attic_retrive.getColumnIndex("othertxt"))));
						    String str5 =cf.decode(attic_retrive.getString(attic_retrive.getColumnIndex("other")));
						    if(str5.contains("Yes")){ 
							    String[] temp = str5.split("&#126;");
							    acotrrdtxt = temp[0];
							    ((RadioButton) acotrrdgval.findViewWithTag(acotrrdtxt)).setChecked(true);
							    ((LinearLayout)findViewById(R.id.otryslin)).setVisibility(cf.v1.VISIBLE);
							    ((EditText)findViewById(R.id.otryscomment)).setText(temp[1]);
						    }else{
						    	acotrrdtxt = str5;
						    	((RadioButton) acotrrdgval.findViewWithTag(acotrrdtxt)).setChecked(true);
							    ((LinearLayout)findViewById(R.id.otryslin)).setVisibility(cf.v1.GONE);
						    }
						   
					  }else if(attic_retrive.getString(attic_retrive.getColumnIndex("atccondinc")).equals("1")){
						   ((LinearLayout)findViewById(R.id.aclin)).setVisibility(cf.v1.GONE);
						   ((TextView)findViewById(R.id.savetxtac)).setVisibility(cf.v1.VISIBLE);
						   ((CheckBox)findViewById(R.id.ac_na)).setChecked(true);
						   ((ImageView)findViewById(R.id.shwatc)).setEnabled(false);
						   ((ImageView)findViewById(R.id.shwatc)).setVisibility(cf.v1.VISIBLE);
						   ((ImageView)findViewById(R.id.hdatc)).setVisibility(cf.v1.GONE);
						 }
					   
				  // } 
				   atticcomt = cf.decode(attic_retrive.getString(attic_retrive.getColumnIndex("atccomments")));
				   if(!atticcomt.equals("")){
				      ((EditText)findViewById(R.id.atticcomment)).setText(atticcomt);
				     }
				   else
				   {
					   ((EditText)findViewById(R.id.atticcomment)).setText("No comments.");
				   }
			   }
			   else
			   {
				   if(chkattic())
				   {
					   ((EditText)findViewById(R.id.atticcomment)).setText("Exterior Inspection Only");
					   //((EditText)findViewById(R.id.atticcomment)).setEnabled(true);
					   ((CheckBox)findViewById(R.id.rd_na)).setChecked(true);
					   /*((CheckBox)findViewById(R.id.rd_na)).setEnabled(false);*/
					    ((CheckBox)findViewById(R.id.rwc_na)).setChecked(true);
					   //((CheckBox)findViewById(R.id.rwc_na)).setEnabled(false);
					   ((CheckBox)findViewById(R.id.gws_na)).setChecked(true);
					   //((CheckBox)findViewById(R.id.gws_na)).setEnabled(false);
					   ((CheckBox)findViewById(R.id.ac_na)).setChecked(true);
					   //((CheckBox)findViewById(R.id.ac_na)).setEnabled(false);
					   ((CheckBox)findViewById(R.id.atcchk3)).setChecked(true);
					   
				   }
				   else
				   {
					   atcchkopt[0].setChecked(true);
					   atcchkopt[3].setChecked(true); 
				   }
				   Cursor c =cf.SelectTablefunction(cf.Rct_table, " WHERE RCT_Ssrid='"+cf.selectedhomeid+"' and RCT_insp_id='1'");
				   if(c.getCount()>0){
					  
					   String s[]=new String[c.getCount()];
					   String s_other[]=new String[c.getCount()];
					   for(int m=0;m<rsmchk.length;m++)
					   {  c.moveToFirst();
					   for(int i=0;i<c.getCount();i++,c.moveToNext())
					   {
						   //System.out.println(" the values rsmchk_vlaue="+rsmchk_vlaue+" RCT="+c.getString(c.getColumnIndex("RCT_rst"))+" rsmchk[m]="+rsmchk[m].getText().toString().trim()+" m="+m);
						   if(!rsmchk_vlaue.contains(cf.decode(c.getString(c.getColumnIndex("RCT_rst")))))
						   {
							   
							   if(cf.decode(c.getString(c.getColumnIndex("RCT_rst"))).equals(rsmchk[m].getText().toString().trim()))
									   {
							   if(!cf.decode(c.getString(c.getColumnIndex("RCT_rst"))).equals("Other"))
							   {
								   if(!rsmchk_vlaue.equals(""))
								   {
								   rsmchk_vlaue+="&#94;"+cf.decode(c.getString(c.getColumnIndex("RCT_rst")));
								   }
								   else
								   {
									   rsmchk_vlaue=cf.decode(c.getString(c.getColumnIndex("RCT_rst")));
								   }
							   }
							   else
							   {
								   rsmchk_vlaue+="&#40;"+cf.decode(c.getString(c.getColumnIndex("RCT_other_rst")));
							   }
									   }
							   }
							  
						   }
						  
					   }
					   //System.out.println("rsmchk_vlaue"+rsmchk_vlaue);
					    cf.setvaluechk1(rsmchk_vlaue,rsmchk,((EditText)findViewById(R.id.rsmotr)));
				   }
					    
						   
					   
				   
			   }
			}
			else
			{
				findViewById(R.id.attic_na_content).setVisibility(View.GONE);
				((CheckBox)findViewById(R.id.attic_na)).setChecked(true);
				
					   if(chkattic())
					   {
						   ((EditText)findViewById(R.id.atticcomment)).setText("Exterior Inspection Only");
						   //((EditText)findViewById(R.id.atticcomment)).setEnabled(true);
						   ((CheckBox)findViewById(R.id.rd_na)).setChecked(true);
						   /*((CheckBox)findViewById(R.id.rd_na)).setEnabled(false);*/
						    ((CheckBox)findViewById(R.id.rwc_na)).setChecked(true);
						   //((CheckBox)findViewById(R.id.rwc_na)).setEnabled(false);
						   ((CheckBox)findViewById(R.id.gws_na)).setChecked(true);
						   //((CheckBox)findViewById(R.id.gws_na)).setEnabled(false);
						   ((CheckBox)findViewById(R.id.ac_na)).setChecked(true);
						   //((CheckBox)findViewById(R.id.ac_na)).setEnabled(false);
						   ((CheckBox)findViewById(R.id.atcchk3)).setChecked(true);
						   
					   }
					   else
					   {
						   atcchkopt[0].setChecked(true);
						   atcchkopt[3].setChecked(true); 
					   }
					   Cursor c =cf.SelectTablefunction(cf.Rct_table, " WHERE RCT_Ssrid='"+cf.selectedhomeid+"' and RCT_insp_id='1'");
					   if(c.getCount()>0){
						  
						   String s[]=new String[c.getCount()];
						   String s_other[]=new String[c.getCount()];
						   for(int m=0;m<rsmchk.length;m++)
						   {  c.moveToFirst();
						   for(int i=0;i<c.getCount();i++,c.moveToNext())
						   {
							   //System.out.println(" the values rsmchk_vlaue="+rsmchk_vlaue+" RCT="+c.getString(c.getColumnIndex("RCT_rst"))+" rsmchk[m]="+rsmchk[m].getText().toString().trim()+" m="+m);
							   if(!rsmchk_vlaue.contains(cf.decode(c.getString(c.getColumnIndex("RCT_rst")))))
							   {
								   
								   if(cf.decode(c.getString(c.getColumnIndex("RCT_rst"))).equals(rsmchk[m].getText().toString().trim()))
										   {
								   if(!cf.decode(c.getString(c.getColumnIndex("RCT_rst"))).equals("Other"))
								   {
									   if(!rsmchk_vlaue.equals(""))
									   {
									   rsmchk_vlaue+="&#94;"+cf.decode(c.getString(c.getColumnIndex("RCT_rst")));
									   }
									   else
									   {
										   rsmchk_vlaue=cf.decode(c.getString(c.getColumnIndex("RCT_rst")));
									   }
								   }
								   else
								   {
									   rsmchk_vlaue+="&#40;"+cf.decode(c.getString(c.getColumnIndex("RCT_other_rst")));
								   }
										   }
								   }
								  
							   }
							  
						   }
						   //System.out.println("rsmchk_vlaue"+rsmchk_vlaue);
						    cf.setvaluechk1(rsmchk_vlaue,rsmchk,((EditText)findViewById(R.id.rsmotr)));
					   }
			}
			   }
			   else
			   {
				   if(chkattic())
				   {
					   ((EditText)findViewById(R.id.atticcomment)).setText("Exterior Inspection Only");
					   //((EditText)findViewById(R.id.atticcomment)).setEnabled(true);
					   ((CheckBox)findViewById(R.id.rd_na)).setChecked(true);
					   /*((CheckBox)findViewById(R.id.rd_na)).setEnabled(false);*/
					    ((CheckBox)findViewById(R.id.rwc_na)).setChecked(true);
					   //((CheckBox)findViewById(R.id.rwc_na)).setEnabled(false);
					   ((CheckBox)findViewById(R.id.gws_na)).setChecked(true);
					   //((CheckBox)findViewById(R.id.gws_na)).setEnabled(false);
					   ((CheckBox)findViewById(R.id.ac_na)).setChecked(true);
					   //((CheckBox)findViewById(R.id.ac_na)).setEnabled(false);
					   ((CheckBox)findViewById(R.id.atcchk3)).setChecked(true);
					   
				   }
				   else
				   {
					   atcchkopt[0].setChecked(true);
					   atcchkopt[3].setChecked(true); 
				   }
				   Cursor c =cf.SelectTablefunction(cf.Rct_table, " WHERE RCT_Ssrid='"+cf.selectedhomeid+"' and RCT_insp_id='1'");
				   if(c.getCount()>0){
					  
					   String s[]=new String[c.getCount()];
					   String s_other[]=new String[c.getCount()];
					   for(int m=0;m<rsmchk.length;m++)
					   {  c.moveToFirst();
					   for(int i=0;i<c.getCount();i++,c.moveToNext())
					   {
						   //System.out.println(" the values rsmchk_vlaue="+rsmchk_vlaue+" RCT="+c.getString(c.getColumnIndex("RCT_rst"))+" rsmchk[m]="+rsmchk[m].getText().toString().trim()+" m="+m);
						   if(!rsmchk_vlaue.contains(cf.decode(c.getString(c.getColumnIndex("RCT_rst")))))
						   {
							   
							   if(cf.decode(c.getString(c.getColumnIndex("RCT_rst"))).equals(rsmchk[m].getText().toString().trim()))
									   {
							   if(!cf.decode(c.getString(c.getColumnIndex("RCT_rst"))).equals("Other"))
							   {
								   if(!rsmchk_vlaue.equals(""))
								   {
								   rsmchk_vlaue+="&#94;"+cf.decode(c.getString(c.getColumnIndex("RCT_rst")));
								   }
								   else
								   {
									   rsmchk_vlaue=cf.decode(c.getString(c.getColumnIndex("RCT_rst")));
								   }
							   }
							   else
							   {
								   rsmchk_vlaue+="&#40;"+cf.decode(c.getString(c.getColumnIndex("RCT_other_rst")));
							   }
									   }
							   }
							  
						   }
						  
					   }
					   //System.out.println("rsmchk_vlaue"+rsmchk_vlaue);
					    cf.setvaluechk1(rsmchk_vlaue,rsmchk,((EditText)findViewById(R.id.rsmotr)));
				   }
					    
						   
					   
				   
			   }
			   
		}
	    catch (Exception E)
		{
			String strerrorlog="Retrieving Attic - FourPoint";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}	
		
	}
	private boolean chkattic()
	{
		cf.CreateARRTable(431);
		Cursor c2=cf.arr_db.rawQuery(" SELECT R_ADD_scope  from "+cf.Roof_additional+" Where R_ADD_Ssrid='"+cf.selectedhomeid+"' and  R_ADD_insp_id='1' ",null);
		
		 if(c2.getCount()>0)
		 {
			 c2.moveToFirst();
			 String rccondtype=cf.decode(c2.getString(c2.getColumnIndex("R_ADD_scope")));
			 System.out.println("the exterior value="+rccondtype);
			 if(rccondtype.trim().equals("Exterior only") || rccondtype.trim().equals("Exterior only,"))
			 {
					 atcchk=true;
			 }
			 else
			 {
				 atcchk=false;
				
			 }
		}
		else
		{
			 atcchk=false;
		}
		 return atcchk;
	}
	private void declarations() {
		// TODO Auto-generated method stub
		cf.setupUI((ScrollView) findViewById(R.id.scr));
		
		//if(atcchk_vlaue.equals("Yes")){
			((LinearLayout)findViewById(R.id.atticlinchk)).setVisibility(cf.v1.VISIBLE);
			((LinearLayout)findViewById(R.id.atticlin2chk)).setVisibility(cf.v1.VISIBLE);
			//((ImageView)findViewById(R.id.shwrfdeck)).setEnabled(false);
			//((RelativeLayout)findViewById(R.id.atticrelrdchk)).setVisibility(cf.v1.VISIBLE);
			/*((RelativeLayout)findViewById(R.id.atticrelrwcchk)).setVisibility(cf.v1.VISIBLE);
			((RelativeLayout)findViewById(R.id.atticrelgwschk)).setVisibility(cf.v1.VISIBLE);
			((RelativeLayout)findViewById(R.id.atticrelacchk)).setVisibility(cf.v1.VISIBLE);*/
			
		  /** ROOF DECK STARTS **/
					((CheckBox)findViewById(R.id.attic_na)).setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							if(((CheckBox) v).isChecked())
							{
								findViewById(R.id.attic_na_content).setVisibility(View.GONE);
							}
							else
							{
								findViewById(R.id.attic_na_content).setVisibility(View.VISIBLE);	
							}
						}
					});	
			for(int i=0;i<rdmchk.length;i++){
				rdmchk[i] = (CheckBox)findViewById(rdm[i]);
			}
			for(int i=0;i<rsmchk.length;i++){
				rsmchk[i] = (CheckBox)findViewById(rsm[i]);
			}
			for(int i=0;i<rdtchk.length;i++){
				rdtchk[i] = (CheckBox)findViewById(rdt[i]);
			}
			for(int i=0;i<rdachk.length;i++){
				rdachk[i] = (CheckBox)findViewById(rda[i]);
			}
			
			for(int i=0;i<rtschk.length;i++){
				rtschk[i] = (CheckBox)findViewById(rts[i]);
			}
			
			for(int i=0;i<nschk.length;i++){
				nschk[i] = (CheckBox)findViewById(ns[i]);
			}
			
			for(int i=0;i<anschk.length;i++){
				anschk[i] = (CheckBox)findViewById(ans[i]);
			}
			
			if(rdmchk_vlaue.equals("")){
			anschk[6].setChecked(true);
			rdachk[9].setChecked(true);
			rtschk[5].setChecked(true);
			nschk[5].setChecked(true);
			rdtchk[9].setChecked(true);
			}
		 /** ROOF DECK ENDS **/
			
		 /** ROOF WALL CONNECTIONS STARTS **/
			for(int i=0;i<rwcchk.length;i++){
				rwcchk[i] = (CheckBox)findViewById(rwc[i]);
			}
			rwc3rdgval = (RadioGroup)findViewById(R.id.nails3_rdg);
			rwc3rdgval.setOnCheckedChangeListener(new checklistenetr(6));
			rwc4rdgval = (RadioGroup)findViewById(R.id.nails4_rdg);
			rwc4rdgval.setOnCheckedChangeListener(new checklistenetr(7));
			rwc5rdgval = (RadioGroup)findViewById(R.id.nails5_rdg);
			rwc5rdgval.setOnCheckedChangeListener(new checklistenetr(10));
			rwctrdgval = (RadioGroup)findViewById(R.id.truss_rdg);
			rwctrdgval.setOnCheckedChangeListener(new checklistenetr(8));
			rwc1trdgval = (RadioGroup)findViewById(R.id.truss15_rdg);
			rwc1trdgval.setOnCheckedChangeListener(new checklistenetr(9));
			
			if(retrwc.equals(""))
			{
				((CheckBox)findViewById(R.id.rwc_na)).setChecked(true);
			}
	     /** ROOF WALL CONNECTIONS ENDS **/
			
		 /** GABLE WALL SHEATHING STARTS **/
			for(int i=0;i<gwschk.length;i++){
				gwschk[i] = (CheckBox)findViewById(gws[i]);
			}
			
			if(retgws.equals(""))
			{
				((CheckBox)findViewById(R.id.gws_na)).setChecked(true);
			}
		 /** GABLE WALL SHEATHING ENDS **/
			
		 /** ATTIC CONDITIONS DECLARATIONS STARTS **/
				for(int i=0;i<atcchkopt.length;i++){
					atcchkopt[i] = (CheckBox)findViewById(atticval[i]);
				}
				
			 acmsrdgval = (RadioGroup)findViewById(R.id.ms_rdg);
			 acmsrdgval.setOnCheckedChangeListener(new checklistenetr(1));
			 acdsrdgval = (RadioGroup)findViewById(R.id.ds_rdg);
			 acdsrdgval.setOnCheckedChangeListener(new checklistenetr(2));
			 acdrmrdgval = (RadioGroup)findViewById(R.id.drm_rdg);
			 acdrmrdgval.setOnCheckedChangeListener(new checklistenetr(3));
			 acnsardgval = (RadioGroup)findViewById(R.id.nsa_rdg);
			 acnsardgval.setOnCheckedChangeListener(new checklistenetr(4));
			 acotrrdgval = (RadioGroup)findViewById(R.id.otr_rdg);
			 acotrrdgval.setOnCheckedChangeListener(new checklistenetr(5));
			 ((EditText)findViewById(R.id.msyscomment)).addTextChangedListener(new textwatcher(1));
			 ((EditText)findViewById(R.id.dsyscomment)).addTextChangedListener(new textwatcher(2));
			 ((EditText)findViewById(R.id.drmyscomment)).addTextChangedListener(new textwatcher(3));
			 ((EditText)findViewById(R.id.nsayscomment)).addTextChangedListener(new textwatcher(4));
			 ((EditText)findViewById(R.id.otryscomment)).addTextChangedListener(new textwatcher(5));
			atc = checkattic();
			
			
		 /** ATTIC CONDITIONS DECLARATIONS ENDS **/
			/*}
		else{
			((LinearLayout)findViewById(R.id.atticlinchk)).setVisibility(cf.v1.GONE);
			((LinearLayout)findViewById(R.id.atticlin2chk)).setVisibility(cf.v1.GONE);
			((RelativeLayout)findViewById(R.id.atticrelrdchk)).setVisibility(cf.v1.GONE);
			((RelativeLayout)findViewById(R.id.atticrelrwcchk)).setVisibility(cf.v1.GONE);
			((RelativeLayout)findViewById(R.id.atticrelgwschk)).setVisibility(cf.v1.GONE);
			((RelativeLayout)findViewById(R.id.atticrelacchk)).setVisibility(cf.v1.GONE);
		}*/
		 /** ATTIC COMMENTS STARTS **/
		 ((EditText)findViewById(R.id.atticcomment)).addTextChangedListener(new textwatcher(6));
		 /** ATTIC COMMENTS ENDS **/
		 ((EditText)findViewById(R.id.rdmotr)).addTextChangedListener(new CustomTextWatcherSpace(((EditText)findViewById(R.id.rdmotr))));
		 ((EditText)findViewById(R.id.rsmotr)).addTextChangedListener(new CustomTextWatcherSpace(((EditText)findViewById(R.id.rsmotr))));
		 ((EditText)findViewById(R.id.rdtotr)).addTextChangedListener(new CustomTextWatcherSpace(((EditText)findViewById(R.id.rdtotr))));		 
		 ((EditText)findViewById(R.id.rwcotr)).addTextChangedListener(new CustomTextWatcherSpace(((EditText)findViewById(R.id.rwcotr))));
		 ((EditText)findViewById(R.id.gwsotr)).addTextChangedListener(new CustomTextWatcherSpace(((EditText)findViewById(R.id.gwsotr))));
		 ((EditText)findViewById(R.id.otrcond)).addTextChangedListener(new CustomTextWatcherSpace(((EditText)findViewById(R.id.otrcond))));
		 
		
	}
	class textwatcher implements TextWatcher
    {
	         public int type;
	         textwatcher(int type)
	    	{
	    		this.type=type;
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {


	    		// TODO Auto-generated method stub
	    		if(this.type==1)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.msyscom_tv_type1),250); 
	    		}
	    		else if(this.type==2)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.dsyscom_tv_type1),250); 
	    		}
	    		else if(this.type==3)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.drmyscom_tv_type1),250);
	    		}
	    		else if(this.type==4)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.nsayscom_tv_type1),250);
	    		}
	    		else if(this.type==5)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.otryscom_tv_type1),250);
	    		}
	    		else if(this.type==6)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.atticcom_tv_type1),500);
	    			
	    		}
	    		
	     }

	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,

	    			int after) {
	    		// TODO Auto-generated method stub
	     	}

	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,

	    			int count) {
	    		// TODO Auto-generated method stub
	    	}
	   }	
    class checklistenetr implements OnCheckedChangeListener, android.widget.RadioGroup.OnCheckedChangeListener
	{
    	int i;
		public checklistenetr(int i) {
			// TODO Auto-generated constructor stub
		   this.i=i;
		}

		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			   RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
		        boolean isChecked = checkedRadioButton.isChecked();
		        if (isChecked)
		        {
		          switch (i) {
					case 1:
						acmsrdtxt= checkedRadioButton.getText().toString().trim();msck=false;
						if(acmsrdtxt.equals("Yes"))
						{
							((LinearLayout)findViewById(R.id.msyslin)).setVisibility(cf.v1.VISIBLE);
						}
						else
						{
							((EditText)findViewById(R.id.msyscomment)).setText("");
							((LinearLayout)findViewById(R.id.msyslin)).setVisibility(cf.v1.GONE);
						}
					  break;
					case 2:
						acdsrdtxt= checkedRadioButton.getText().toString().trim();dsck=false;
						if(acdsrdtxt.equals("Yes"))
						{
							((LinearLayout)findViewById(R.id.dsyslin)).setVisibility(cf.v1.VISIBLE);
						}
						else
						{
							((EditText)findViewById(R.id.dsyscomment)).setText("");
							((LinearLayout)findViewById(R.id.dsyslin)).setVisibility(cf.v1.GONE);
						}
					  break;
					case 3:
						acdrmrdtxt= checkedRadioButton.getText().toString().trim();drmck=false;
						if(acdrmrdtxt.equals("Yes"))
						{
							((LinearLayout)findViewById(R.id.drmyslin)).setVisibility(cf.v1.VISIBLE);
						}
						else
						{
							((EditText)findViewById(R.id.drmyscomment)).setText("");
							((LinearLayout)findViewById(R.id.drmyslin)).setVisibility(cf.v1.GONE);
						}
					  break;
					case 4:
						acnsardtxt= checkedRadioButton.getText().toString().trim();nsack=false;
						if(acnsardtxt.equals("Yes"))
						{
							((LinearLayout)findViewById(R.id.nsayslin)).setVisibility(cf.v1.VISIBLE);
						}
						else
						{
							((EditText)findViewById(R.id.nsayscomment)).setText("");
							((LinearLayout)findViewById(R.id.nsayslin)).setVisibility(cf.v1.GONE);
						}
					  break;
					case 5:
						acotrrdtxt= checkedRadioButton.getText().toString().trim();otrck=false;
						if(acotrrdtxt.equals("Yes"))
						{
							((LinearLayout)findViewById(R.id.otryslin)).setVisibility(cf.v1.VISIBLE);
						}
						else
						{
							((EditText)findViewById(R.id.otryscomment)).setText("");
							((LinearLayout)findViewById(R.id.otryslin)).setVisibility(cf.v1.GONE);
						}
					  break;
					case 6:
						rwc3rdgtxt= checkedRadioButton.getText().toString().trim();nl3ck=false;
						break;
					case 7:
						rwc4rdgtxt= checkedRadioButton.getText().toString().trim();nl4ck=false;
						break;
					case 8:
						rwctrdgtxt= checkedRadioButton.getText().toString().trim();tsck=false;
						
						break;
					case 9:
						rwc1trdgtxt= checkedRadioButton.getText().toString().trim();ts15ck=false;
						break;
					case 10:
						rwc5rdgtxt= checkedRadioButton.getText().toString().trim();nl5ck=false;
						break;
		          }
		        }
		}

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			// TODO Auto-generated method stub
			
		}

		
		
	}
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.rshelpid:
			cf.ShowToast("Complete for Four Point / Wind Mitigation reports.",0);
			break;
		case R.id.rwchelpid:
			cf.ShowToast("Complete for Type I Commercial and Residential Wind Mitigation reports.",0);
			break;
		case R.id.gwshelpid:
			cf.ShowToast("Complete for 1802 inspections and Commercial Type I Wind Mitigation inspections only.",0);
			break;
		case R.id.com:
			/***Call for the comments***/
			int len=((EditText)findViewById(R.id.atticcomment)).getText().toString().length();
			if(load_comment )
				{
					load_comment=false;
					int loc[] = new int[2];
					v.getLocationOnScreen(loc);
					Intent in1 = cf.loadComments("Overall Attic Comments",loc);
					in1.putExtra("length", len);
					in1.putExtra("max_length", 500);
					startActivityForResult(in1, cf.loadcomment_code);
				}
			
			 break;
	
		case R.id.gws_na:/** GABLE WALL SHEATHING NOT APPLICABLE CHECKBOX **/
			 if(((CheckBox)findViewById(R.id.gws_na)).isChecked())
			 {
				 gwschkval="1";//chk_values();
				 if(gws_save){chk_values();}
				 System.out.println("retgwst="+retgwst+gws_save);
	 			  if(!retgwst.equals("")){
	 				    showunchkalert(3);
	 			  }else{
	 				    gwschkval="1";gwschk_vlaue="";retgwst="";
	    			    cf.Set_UncheckBox(gwschk, ((EditText)findViewById(R.id.gwsotr)));
	 			    //	cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Attic+ " set gablewallinc='"+1+"' where fld_srid='"+cf.selectedhomeid+"'");
	 			    	((ImageView)findViewById(R.id.shwgwall)).setVisibility(cf.v1.VISIBLE);
	 			    	((ImageView)findViewById(R.id.hdgwall)).setVisibility(cf.v1.GONE);
	 			    	((ImageView)findViewById(R.id.shwgwall)).setEnabled(false);
	 			    	((LinearLayout)findViewById(R.id.gwslin)).setVisibility(cf.v1.GONE);
	 			    	//((TextView)findViewById(R.id.savetxtgws)).setVisibility(cf.v1.VISIBLE);
			        }
			 }
			 else
			 {
				 gwschkval="0";gwschk_vlaue="";retgwst="";
				 /*try
					{
					    cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Attic+ " set gablewallinc='"+2+"' where fld_srid='"+cf.selectedhomeid+"'");
						 
					}
					catch (Exception E)
					{
						String strerrorlog="Updating the GABLE WALL SHEATHING - FourPoint";
						cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
					}*/
				 ((ImageView)findViewById(R.id.shwgwall)).setVisibility(cf.v1.GONE);
			     ((ImageView)findViewById(R.id.hdgwall)).setVisibility(cf.v1.VISIBLE);
			     ((ImageView)findViewById(R.id.shwgwall)).setEnabled(true);
				 ((CheckBox)findViewById(R.id.gws_na)).setChecked(false);
				 ((LinearLayout)findViewById(R.id.gwslin)).setVisibility(cf.v1.VISIBLE);
				 ((TextView)findViewById(R.id.savetxtgws)).setVisibility(cf.v1.INVISIBLE);
			 }
			break;
		
		case R.id.ac_na:/** ATTIC CONDITIONS NOTED NOT APPLICABLE CHECKBOX **/
			
			 if(((CheckBox)findViewById(R.id.ac_na)).isChecked())
			 {
				 acchkval="1";//chk_values();
				 if(ac_save){chk_values();}
				 System.out.println("retacmsrdtxt="+retacmsrdtxt);
	 			  if(!retacmsrdtxt.equals("")){
	 				    showunchkalert(4);
	 			  }else{
	 			    	clearac();defaultac();acchkval="1";
	 			    	//cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Attic+ " set atccondinc='"+1+"' where fld_srid='"+cf.selectedhomeid+"'");
			        	((LinearLayout)findViewById(R.id.aclin)).setVisibility(cf.v1.GONE);
	 			    	((ImageView)findViewById(R.id.shwatc)).setVisibility(cf.v1.VISIBLE);
	 			    	((ImageView)findViewById(R.id.hdatc)).setVisibility(cf.v1.GONE);
	 			    	((ImageView)findViewById(R.id.shwatc)).setEnabled(false);
			        	//((TextView)findViewById(R.id.savetxtac)).setVisibility(cf.v1.VISIBLE);
			        }
			 }
			 else
			 {
				 acchkval="0";clearac();defaultac();
				 /*try
					{
					    cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Attic+ " set atccondinc='"+2+"' where fld_srid='"+cf.selectedhomeid+"'");
						 
					}
					catch (Exception E)
					{
						String strerrorlog="Updating the ATTIC CONDITIONS - FourPoint";
						cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
					}*/
				 ((CheckBox)findViewById(R.id.ac_na)).setChecked(false);
				 ((LinearLayout)findViewById(R.id.aclin)).setVisibility(cf.v1.VISIBLE);
				 ((ImageView)findViewById(R.id.shwatc)).setVisibility(cf.v1.GONE);
			     ((ImageView)findViewById(R.id.hdatc)).setVisibility(cf.v1.VISIBLE);
			     ((ImageView)findViewById(R.id.shwatc)).setEnabled(true);
				 atc = checkattic();
				 ((TextView)findViewById(R.id.savetxtac)).setVisibility(cf.v1.INVISIBLE);
			 }
			break;
		
		
		case R.id.save:
			
				selatcopt= cf.getselected_chk(atcchkopt);
				if(!((CheckBox)findViewById(R.id.attic_na)).isChecked())
				{
					   if(selatcopt.equals(""))
					   {
						   cf.ShowToast("Please select the option for Scope of Attic Inspection.",0);
					   }
					   else
					   {
							 if(((CheckBox)findViewById(R.id.rd_na)).isChecked())
							 {
								 
								 RTWConnectionvalidation();
							 }
							 else
							 {
								 RoodDeck_Checkbox();  
							 }
					   }				 
				}
				else
				{
					comments();
				}			
			break;
		case R.id.rd_na: /** ROOF DECK NOT APPLICABLE **/
			 if(((CheckBox)findViewById(R.id.rd_na)).isChecked())
			 {
				 rdchkval="1";
				 if(rd_save){chk_values();}
				// chk_values();
	 			  if(!retrdmtxt.equals("")){
	 				    showunchkalert(1);
	 			    }else{
	 			    	clearrd();rdchkval="1";
	 			    	//cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Attic+ " set rfdcinc='"+1+"' where fld_srid='"+cf.selectedhomeid+"'");
	 			    	((ImageView)findViewById(R.id.shwrfdeck)).setVisibility(cf.v1.VISIBLE);
	 			    	((ImageView)findViewById(R.id.hdwrfdeck)).setVisibility(cf.v1.GONE);
	 			    	((ImageView)findViewById(R.id.shwrfdeck)).setEnabled(false);
	 			    	((LinearLayout)findViewById(R.id.rdlin)).setVisibility(cf.v1.GONE);
			        	//((TextView)findViewById(R.id.savetxtrd)).setVisibility(cf.v1.VISIBLE);
			        }
			 }
			 else
			 {
				 rdchkval="0";
				/* try
				 {
					 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Attic+ " set rfdcinc='"+2+"' where fld_srid='"+cf.selectedhomeid+"'");
				 }
				 catch (Exception E)
				 {
					 String strerrorlog="Updating the ROOF STRUCTURE - FourPoint";
					 cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}*/
				 ((ImageView)findViewById(R.id.shwrfdeck)).setVisibility(cf.v1.GONE);
			     ((ImageView)findViewById(R.id.hdwrfdeck)).setVisibility(cf.v1.VISIBLE);
			     ((ImageView)findViewById(R.id.shwrfdeck)).setEnabled(true);
				 ((CheckBox)findViewById(R.id.rd_na)).setChecked(false);
				 ((LinearLayout)findViewById(R.id.rdlin)).setVisibility(cf.v1.VISIBLE);
				 ((TextView)findViewById(R.id.savetxtrd)).setVisibility(cf.v1.INVISIBLE);
			 }
			break;
		
		case R.id.rwc_na: /** ROOF WALL CONNECTION NOT APPLICABLE **/
			 if(((CheckBox)findViewById(R.id.rwc_na)).isChecked())
			 {
				 rwcchkval="1";//chk_values();
				 if(rwc_save){chk_values();}
	 			  if(!retrtow.equals("")){
	 				    showunchkalert(2);
	 			    }else{
	 			    	
	 			    	clearrwc();rwcchkval="1";
	 			    //	cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Attic+ " set rfwallinc='"+1+"' where fld_srid='"+cf.selectedhomeid+"'");
	 			    	((ImageView)findViewById(R.id.shwrfwall)).setVisibility(cf.v1.VISIBLE);
	 			    	((ImageView)findViewById(R.id.hdrfwall)).setVisibility(cf.v1.GONE);
	 			    	((ImageView)findViewById(R.id.shwrfwall)).setEnabled(false);
	 			    	((LinearLayout)findViewById(R.id.rwclin)).setVisibility(cf.v1.GONE);
			        	//((TextView)findViewById(R.id.savetxtrwc)).setVisibility(cf.v1.VISIBLE);
			        	
			       }
			 }
			 else
			 {
				 rwcchkval="0";
				/* try
				 {
					    cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Attic+ " set rfwallinc='"+2+"' where fld_srid='"+cf.selectedhomeid+"'");
						 
				 }
				 catch (Exception E)
				 {
						String strerrorlog="Updating the ROOF WALL CONNECTIONS - FourPoint";
						cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				 }*/
				 ((ImageView)findViewById(R.id.shwrfwall)).setVisibility(cf.v1.GONE);
			     ((ImageView)findViewById(R.id.hdrfwall)).setVisibility(cf.v1.VISIBLE);
			     ((ImageView)findViewById(R.id.shwrfwall)).setEnabled(true);
				 ((CheckBox)findViewById(R.id.rwc_na)).setChecked(false);
				 ((LinearLayout)findViewById(R.id.rwclin)).setVisibility(cf.v1.VISIBLE);
				 ((TextView)findViewById(R.id.savetxtrwc)).setVisibility(cf.v1.INVISIBLE);
			 }
			break;
		case R.id.shwrfdeck:
			if(!((CheckBox)findViewById(R.id.rd_na)).isChecked()){
				hideotherlayouts();
				((CheckBox)findViewById(R.id.rd_na)).setChecked(false);
				((LinearLayout)findViewById(R.id.rdlin)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdwrfdeck)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.shwrfdeck)).setVisibility(cf.v1.GONE);
			}
			break;
		case R.id.hdwrfdeck:
			((ImageView)findViewById(R.id.shwrfdeck)).setVisibility(cf.v1.VISIBLE);
			((ImageView)findViewById(R.id.hdwrfdeck)).setVisibility(cf.v1.GONE);
			((LinearLayout)findViewById(R.id.rdlin)).setVisibility(cf.v1.GONE);
			break;
		case R.id.shwrfwall:
			if(!((CheckBox)findViewById(R.id.rwc_na)).isChecked()){
				hideotherlayouts();
				((CheckBox)findViewById(R.id.rwc_na)).setChecked(false);
				((LinearLayout)findViewById(R.id.rwclin)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdrfwall)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.shwrfwall)).setVisibility(cf.v1.GONE);
				
			}
			break;
		case R.id.hdrfwall:
			((ImageView)findViewById(R.id.shwrfwall)).setVisibility(cf.v1.VISIBLE);
			((ImageView)findViewById(R.id.hdrfwall)).setVisibility(cf.v1.GONE);
			((LinearLayout)findViewById(R.id.rwclin)).setVisibility(cf.v1.GONE);
			break;
		case R.id.shwgwall:
			if(!((CheckBox)findViewById(R.id.gws_na)).isChecked()){
				hideotherlayouts();
				((CheckBox)findViewById(R.id.gws_na)).setChecked(false);
				((LinearLayout)findViewById(R.id.gwslin)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdgwall)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.shwgwall)).setVisibility(cf.v1.GONE);
			}
			break;
		case R.id.hdgwall:
			((ImageView)findViewById(R.id.shwgwall)).setVisibility(cf.v1.VISIBLE);
			((ImageView)findViewById(R.id.hdgwall)).setVisibility(cf.v1.GONE);
			((LinearLayout)findViewById(R.id.gwslin)).setVisibility(cf.v1.GONE);
			break;
		case R.id.shwatc:
			if(!((CheckBox)findViewById(R.id.ac_na)).isChecked()){
				hideotherlayouts();
				((CheckBox)findViewById(R.id.ac_na)).setChecked(false);
				((LinearLayout)findViewById(R.id.aclin)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdatc)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.shwatc)).setVisibility(cf.v1.GONE);
			}
			break;
		case R.id.hdatc:
			((ImageView)findViewById(R.id.shwatc)).setVisibility(cf.v1.VISIBLE);
			((ImageView)findViewById(R.id.hdatc)).setVisibility(cf.v1.GONE);
			((LinearLayout)findViewById(R.id.aclin)).setVisibility(cf.v1.GONE);
			break;
	
		case R.id.gws_chk7:/** GABLE WALL SHEATHING - OTHER **/
			  if(gwschk[gwschk.length-1].isChecked()){
			 	  ((EditText)findViewById(R.id.gwsotr)).setVisibility(v.VISIBLE);
			      cf.setFocus(((EditText)findViewById(R.id.gwsotr)));
			  }else{
			  	  ((EditText)findViewById(R.id.gwsotr)).setVisibility(v.GONE);
			      ((EditText)findViewById(R.id.gwsotr)).setText("");
			  }
			  break;
		case R.id.rwc_chk10:/** ROOF WALL CONNECTIONS - OTHER **/
			  if(rwcchk[rwcchk.length-1].isChecked()){
			 	  ((EditText)findViewById(R.id.rwcotr)).setVisibility(v.VISIBLE);
			 	  cf.setFocus(((EditText)findViewById(R.id.rwcotr)));
			  }
			  else{
			 	  ((EditText)findViewById(R.id.rwcotr)).setVisibility(v.GONE);
			      ((EditText)findViewById(R.id.rwcotr)).setText("");
			  }
			  break;
		case R.id.rdm_chk11:/** ROOF DECK MATERIALS - OTHER **/
			  if(rdmchk[rdmchk.length-1].isChecked()){
			 	  ((EditText)findViewById(R.id.rdmotr)).setVisibility(v.VISIBLE);
			 	  cf.setFocus(((EditText)findViewById(R.id.rdmotr)));
			  }else{
			 	  ((EditText)findViewById(R.id.rdmotr)).setVisibility(v.GONE);
			      ((EditText)findViewById(R.id.rdmotr)).setText("");
			  }
			 break;
		case R.id.rsm_chk6:/** ROOF STRUCTURE MATERIALS - OTHER **/
			  if(rsmchk[rsmchk.length-1].isChecked()){
			    ((EditText)findViewById(R.id.rsmotr)).setVisibility(v.VISIBLE);
			    cf.setFocus(((EditText)findViewById(R.id.rsmotr)));
			  }else{ ((EditText)findViewById(R.id.rsmotr)).setVisibility(v.GONE);
			       ((EditText)findViewById(R.id.rsmotr)).setText("");
			  }
			 break;
		case R.id.rdt_chk11:/** ROOF DECK THICKNESS - OTHER **/
			  if(rdtchk[rdtchk.length-1].isChecked()){
				  ((EditText)findViewById(R.id.rdtotr)).setVisibility(v.VISIBLE);
				  cf.setFocus(((EditText)findViewById(R.id.rdtotr)));
			  }else{
				  ((EditText)findViewById(R.id.rdtotr)).setVisibility(v.GONE);
			      ((EditText)findViewById(R.id.rdtotr)).setText("");
			  }
			  break;
		case R.id.rda_chk11:/** ROOF DECK ATTACHMENT - OTHER **/
			  if(rdachk[rdachk.length-1].isChecked()){
				  ((EditText)findViewById(R.id.rdaotr)).setVisibility(v.VISIBLE);
				  cf.setFocus(((EditText)findViewById(R.id.rdaotr)));
			  }else{
				  ((EditText)findViewById(R.id.rdaotr)).setVisibility(v.GONE);
			      ((EditText)findViewById(R.id.rdaotr)).setText("");
			  }
			  break;
		default:
			break;
		}
	}
	private void roofstrucvalidation() {
		// TODO Auto-generated method stub
		if(!((CheckBox)findViewById(R.id.rd_na)).isChecked())
		{
				 rdchkval="0";
				 RoodDeck_Checkbox();
		}
		else
		{
			rdchkval="1";RTWConnectionvalidation();
		}
	}
	private String checkattic() {
		// TODO Auto-generated method stub
		atc="";
		/*cf.CreateARRTable(10);
		cf.CreateARRTable(99);
		cf.CreateARRTable(43);*/
		cf.CreateARRTable(431);
		System.out.println("ceel"+cf.selectedhomeid);
		Cursor ctc = cf.arr_db.rawQuery("Select R_RCN_OCR from "+cf.RCN_table+" where  R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id='1' ",null);
		
		if(ctc.getCount()>0)
		{
			ctc.moveToFirst();
			atc = cf.decode(ctc.getString(ctc.getColumnIndex("R_RCN_OCR")));System.out.println("atc"+atc);
			if(atc.equals("Good")||atc.equals("Excellent"))
			 {
				 ((RadioButton)acmsrdgval.findViewWithTag("No")).setChecked(true);
				 ((RadioButton)acdsrdgval.findViewWithTag("No")).setChecked(true);
				 ((RadioButton)acdrmrdgval.findViewWithTag("No")).setChecked(true);
				 ((RadioButton)acnsardgval.findViewWithTag("No")).setChecked(true);
				 ((RadioButton)acotrrdgval.findViewWithTag("No")).setChecked(true);
			 }
			 else
			 {
				 ((RadioButton)acmsrdgval.findViewWithTag("BSI")).setChecked(true);
				 ((RadioButton)acdsrdgval.findViewWithTag("BSI")).setChecked(true);
				 ((RadioButton)acdrmrdgval.findViewWithTag("BSI")).setChecked(true);
				 ((RadioButton)acnsardgval.findViewWithTag("BSI")).setChecked(true);
				 ((RadioButton)acotrrdgval.findViewWithTag("BSI")).setChecked(true);
			 }
		}
		else
		{
			 ((RadioButton)acmsrdgval.findViewWithTag("N/A")).setChecked(true);
			 ((RadioButton)acdsrdgval.findViewWithTag("N/A")).setChecked(true);
			 ((RadioButton)acdrmrdgval.findViewWithTag("N/A")).setChecked(true);
			 ((RadioButton)acnsardgval.findViewWithTag("N/A")).setChecked(true);
			 ((RadioButton)acotrrdgval.findViewWithTag("N/A")).setChecked(true);
	
		}
		return atc;
	}
	private void damagedrotted() {
		// TODO Auto-generated method stub
		if(acdsrdtxt.equals("Yes"))
		{
			 if(((EditText)findViewById(R.id.dsyscomment)).getText().toString().trim().equals(""))
			 {
				 cf.ShowToast("Please enter the Comments for Damaged/Rotted Sheathing under Attic Conditions Noted.",0);
				 cf.setFocus(((EditText)findViewById(R.id.dsyscomment)));
				// cf.ShowError(((EditText)findViewById(R.id.dsyscomment)),"Please enter the Comments under Damaged/Rotted Sheathing.");
			 }
			 else
			 {
			    acdsrdtxt += "&#126;"+((EditText)findViewById(R.id.dsyscomment)).getText().toString().trim();
			    damagedroof();
			 }
		 }
		 else
		 {
			 acdsrdtxt=acdsrdtxt;
			 damagedroof();
		 }
	}
	private void damagedroof() {
		// TODO Auto-generated method stub
		if(acdrmrdtxt.equals("Yes"))
		{
			 if(((EditText)findViewById(R.id.drmyscomment)).getText().toString().trim().equals(""))
			 {
				 cf.ShowToast("Please enter the Comments for Damaged/Broken Roof Members Noted under Attic Conditions Noted.",0);
				 cf.setFocus(((EditText)findViewById(R.id.drmyscomment)));
				 //cf.ShowError(((EditText)findViewById(R.id.drmyscomment)),"Please enter the Comments under Damaged/Broken Roof Members.");
			 }
			 else
			 {
			     acdrmrdtxt += "&#126;"+((EditText)findViewById(R.id.drmyscomment)).getText().toString().trim();
			     nsa();
			 }
		 }
		 else
		 {
			 acdrmrdtxt=acdrmrdtxt;
			 nsa();
		 }
	}
	private void nsa() {
		// TODO Auto-generated method stub
		 if(acnsardtxt.equals("Yes"))
		 {
			 if(((EditText)findViewById(R.id.nsayscomment)).getText().toString().trim().equals(""))
			 {
				 cf.ShowToast("Please enter the Comments for Non-Standard Alterations Noted under Attic Conditions Noted.",0);
				 cf.setFocus(((EditText)findViewById(R.id.nsayscomment)));
				// cf.ShowError(((EditText)findViewById(R.id.nsayscomment)),"Please enter the Comments under Non Standard Alterations.");
			 }
			 else
			 {
			    acnsardtxt += "&#126;"+((EditText)findViewById(R.id.nsayscomment)).getText().toString().trim();
			    other();
			 }
		 }
		 else
		 {
			 acnsardtxt=acnsardtxt;
			 other();
		 }
	}
	private void other() {
		// TODO Auto-generated method stub
		
		if(acotrrdtxt.equals("Yes") || acotrrdtxt.equals("BSI"))
		{
/*			 if(((EditText)findViewById(R.id.otrcond)).getText().toString().trim().equals("")){
				 cf.ShowToast("Please enter the Other text for Attic Conditions Noted.",0);
				 ((EditText)findViewById(R.id.otrcond)).setText("");
				 cf.setFocus(((EditText)findViewById(R.id.otrcond)));
			 }
			 else
			 {*/	
				 if(acotrrdtxt.equals("Yes"))
				 {
					 if(((EditText)findViewById(R.id.otryscomment)).getText().toString().trim().equals(""))
					 {
						 cf.ShowToast("Please enter the other Comments under Attic Conditions Noted - Other.",0);
						 cf.setFocus(((EditText)findViewById(R.id.otryscomment)));
					 }
					 else
					 {
					    acotrrdtxt += "&#126;"+((EditText)findViewById(R.id.otryscomment)).getText().toString().trim();
					   comments();
					 }
				 }
				 else
				 {
					 acotrrdtxt=acotrrdtxt;
					 comments();
					 //AtticCond_Insert();
					// AtticCond_Insert();
				 }
			// }
		}
		else
		{
			 acotrrdtxt=acotrrdtxt;
			 comments();
			 //AtticCond_Insert();			
		}
		 
	}
	private void nails3() {
		// TODO Auto-generated method stub
		if(rwc3rdgtxt.equals(""))
		{
   		  cf.ShowToast("Please select the option for Minimum 2 Nails on Front Face under Roof to Wall Connections.",0);
   	    }
		else
		{
			 if(rwc4rdgtxt.equals(""))
			 {
				 cf.ShowToast("Please select the option for Minimum 3 Nails - Both Sides under Roof to Wall Connections.",0);
	    	 }
			 else
			 {
				 if(rwc5rdgtxt.equals(""))
				 {
					 cf.ShowToast("Please select the option for Minimum 4 Nails - Both Sides under Roof to Wall Connections.",0);
		    	 }
				 else
				 {
					 if(rwctrdgtxt.equals(""))
					 {
						 cf.ShowToast("Please select the option for Every Truss/Rafter Nails under Roof to Wall Connections.",0);
			    	 }
					 else
					 {
						 if(rwc1trdgtxt.equals(""))
						 {
							String str = "Please select the option for Within 1.5&#34; of Trusses under Roof to Wall Connections.";
							cf.ShowToast1(Html.fromHtml(str), 0);
				    	 }
						 else
						 {
							 GSValidation();
						 }
				     }
				 }
		     }
		}
	}
	
	private void GSValidation() {
		// TODO Auto-generated method stub
		if(!((CheckBox)findViewById(R.id.gws_na)).isChecked())
		{
				 gwschkval="0";
				 GWS_checkbox();
		}
		else
		{
			gwschkval="1";
			Atticcondnotedvalidation();
			//GWS_Insert();
		}

		
	}
	private void rwcchk() {
		// TODO Auto-generated method stub
		//if(((LinearLayout)findViewById(R.id.rwclin)).isShown())
		if(((CheckBox)findViewById(R.id.rwc_na)).isChecked())
		{
			chk_values();
			if(retrwc.equals(""))
			{
				cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Attic+ " set Attic_Na='"+((CheckBox)findViewById(R.id.attic_na)).isChecked()+"',rfwallinc='"+1+"' where fld_srid='"+cf.selectedhomeid+"'");
			    	
			}
			else
			{
				
			}
			gwcchk();
		}
		else
		{  
			 chk_values();
			 if(rwcchk_vlaue.equals(""))
			 {
					cf.ShowToast("Please save the options under Roof Wall Connections.",0);
			 }
			 else
			 {
			    gwcchk();
			 }
		}
	
	}
	private void gwcchk() {
		// TODO Auto-generated method stub
		//if(((LinearLayout)findViewById(R.id.gwslin)).isShown())
		if(((CheckBox)findViewById(R.id.gws_na)).isChecked())
		{
			chk_values();
			if(retgws.equals(""))
			{
				cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Attic+ " set Attic_Na='"+((CheckBox)findViewById(R.id.attic_na)).isChecked()+"',gablewallinc='"+1+"' where fld_srid='"+cf.selectedhomeid+"'");
			}
			else
			{
				
			}
			acnchk();
		}
		else
		{
			 chk_values();
			 if(gwschk_vlaue.equals(""))
			 {
				cf.ShowToast("Please save the options under Gable Wall Sheathing.",0);
			 }
			 else
			 {
			   acnchk();
			 }
	   }
	
	}
	private void acnchk() {
		// TODO Auto-generated method stub
		if(((CheckBox)findViewById(R.id.ac_na)).isChecked())
		{
			chk_values();
			
			comments();
		}
		else
		{
			chk_values();
			if(retacmsrdtxt.equals(""))
			{
				cf.ShowToast("Please save the options under Attic Conditions Noted.",0);
			}
			else
			{
				
			    comments();
			}
	   }
	  
	}
	private void comments() {
		// TODO Auto-generated method stub
		CheckBox na= (CheckBox)findViewById(R.id.attic_na);
		/*if(((EditText)findViewById(R.id.atticcomment)).getText().toString().trim().equals("") && !na.isChecked() ){
			cf.ShowToast("Please enter the Overall Attic Comments.",0);
			cf.setFocus(((EditText)findViewById(R.id.atticcomment)));
			//cf.ShowError(((EditText)findViewById(R.id.atticcomment)),"Please enter the Overall Attic Comments.");
		}
		else
		{*/
			 if(((CheckBox)findViewById(R.id.gws_na)).isChecked() || na.isChecked())
			 {
				 gwschkval="1";gwschk_vlaue="";
			 }
			 else
			 {
				 gwschkval="0";
			 }

             if(((CheckBox)findViewById(R.id.ac_na)).isChecked() || na.isChecked())
			 {
				 acchkval="1";acmsrdtxt="";
			 }
             else
             {
            	 acchkval="0";
             }

             if(((CheckBox)findViewById(R.id.rd_na)).isChecked() || na.isChecked())
			 {
				 rdchkval="1";rdmchk_vlaue="";
			 }
             else
             {
            	 rdchkval="0";
             }

             if(((CheckBox)findViewById(R.id.rwc_na)).isChecked() || na.isChecked())
			 {
				 rwcchkval="1";rwcchk_vlaue="";
			 }
             else
             {
            	 rwcchkval="0";
             }
			try
			{
				System.out.println("UPDATE "+cf.Four_Electrical_Attic+ " set atcoptions='"+cf.encode(selatcopt)+"',"+
			     "rfdcinc='"+rdchkval+"',rfdcmat='"+cf.encode(rdmchk_vlaue)+"',"+
		         "rfstmat='"+cf.encode(rsmchk_vlaue)+"',rfdcthi='"+cf.encode(rdtchk_vlaue)+"',rfdcatt='"+cf.encode(rdachk_vlaue)+"',"+
			     "rftruss='"+cf.encode(rtschk_vlaue)+"',nailspac='"+cf.encode(nschk_vlaue)+"',missednails='"+cf.encode(anschk_vlaue)+"',"+
		         "rfwallinc='"+rwcchkval+"',rtowopt='"+cf.encode(rwcchk_vlaue)+"',"+
		         "nail3='"+cf.encode(rwc3rdgtxt)+"',nail4='"+cf.encode(rwc4rdgtxt)+"',nail5='"+cf.encode(rwc5rdgtxt)+"',truss='"+cf.encode(rwctrdgtxt)+"',"+
			     "truss1='"+cf.encode(rwc1trdgtxt)+"',"+
		         "gablewallinc='"+gwschkval+"',gablewallopt='"+cf.encode(gwschk_vlaue)+"',"+
			     "atccomments='"+cf.encode(((EditText)findViewById(R.id.atticcomment)).getText().toString())+"',"+
				 "atccondinc='"+acchkval+"',moisture='"+cf.encode(acmsrdtxt)+"',"+
		         "dsheath='"+cf.encode(acdsrdtxt)+"',droof='"+cf.encode(acdrmrdtxt)+"',nsalt='"+cf.encode(acnsardtxt)+"',"+
			     "othertxt='"+cf.encode(((EditText)findViewById(R.id.otrcond)).getText().toString().trim())+"',"+
		         "other='"+cf.encode(acotrrdtxt)+"',Attic_Na='"+na.isChecked()+"' where fld_srid='"+cf.selectedhomeid+"'");
				 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Attic+ " set atcoptions='"+cf.encode(selatcopt)+"',"+
			     "rfdcinc='"+rdchkval+"',rfdcmat='"+cf.encode(rdmchk_vlaue)+"',"+
		         "rfstmat='"+cf.encode(rsmchk_vlaue)+"',rfdcthi='"+cf.encode(rdtchk_vlaue)+"',rfdcatt='"+cf.encode(rdachk_vlaue)+"',"+
			     "rftruss='"+cf.encode(rtschk_vlaue)+"',nailspac='"+cf.encode(nschk_vlaue)+"',missednails='"+cf.encode(anschk_vlaue)+"',"+
		         "rfwallinc='"+rwcchkval+"',rtowopt='"+cf.encode(rwcchk_vlaue)+"',"+
		         "nail3='"+cf.encode(rwc3rdgtxt)+"',nail4='"+cf.encode(rwc4rdgtxt)+"',nail5='"+cf.encode(rwc5rdgtxt)+"',truss='"+cf.encode(rwctrdgtxt)+"',"+
			     "truss1='"+cf.encode(rwc1trdgtxt)+"',"+
		         "gablewallinc='"+gwschkval+"',gablewallopt='"+cf.encode(gwschk_vlaue)+"',"+
			     "atccomments='"+cf.encode(((EditText)findViewById(R.id.atticcomment)).getText().toString())+"',"+
				 "atccondinc='"+acchkval+"',moisture='"+cf.encode(acmsrdtxt)+"',"+
		         "dsheath='"+cf.encode(acdsrdtxt)+"',droof='"+cf.encode(acdrmrdtxt)+"',nsalt='"+cf.encode(acnsardtxt)+"',"+
			     "othertxt='"+cf.encode(((EditText)findViewById(R.id.otrcond)).getText().toString().trim())+"',"+
		         "other='"+cf.encode(acotrrdtxt)+"',Attic_Na='"+na.isChecked()+"' where fld_srid='"+cf.selectedhomeid+"'");
				cf.ShowToast("Attic System saved successfully.", 0);
				  cf.goclass(23);
			} 
			catch (Exception E)
			{
				String strerrorlog="Updating the ATTIC COMMENTS - FourPoint";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			}
		//}
	}
	private void RoodDeck_Checkbox() {
		// TODO Auto-generated method stub
	 rdmchk_vlaue= cf.getselected_chk(rdmchk);
   	 String veptypchk_valueotr=(rdmchk[rdmchk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.rdmotr)).getText().toString():""; // append the other text value in to the selected option
   	 rdmchk_vlaue+=veptypchk_valueotr;
   	 if(rdmchk_vlaue.equals(""))
	 {
   		cf.ShowToast("Please select the option for Roof Deck Materials under Roof Structure.",0);
	 }
   	 else
   	 {
   		 if(rdmchk[rdmchk.length-1].isChecked())
		 {
			 if(veptypchk_valueotr.trim().equals("&#40;"))
			 {
				 cf.ShowToast("Please enter the Other text for Roof Deck Materials under Roof Structure.",0);
				 cf.setFocus(((EditText)findViewById(R.id.rdmotr)));
				// cf.ShowError(((EditText)findViewById(R.id.rdmotr)),"Please enter the Other text for Roof Deck Materials.");
			 }
			 else
			 {
				roofstructure();
			 }
		 }
   		 else
   		 {
   			roofstructure();
   		 }
	  }
  	
	}
	private void roofstructure() {
		// TODO Auto-generated method stub
		 System.out.println("insiderdmchk_vlaue= "+rdmchk_vlaue);
		 rsmchk_vlaue= cf.getselected_chk(rsmchk);
		 String rsmchk_valueotr=(rsmchk[rsmchk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.rsmotr)).getText().toString():""; // append the other text value in to the selected option
		 rsmchk_vlaue+=rsmchk_valueotr;
		 if(rsmchk_vlaue.equals(""))
		 {
			cf.ShowToast("Please select the option for Roof Structural Materials under Roof Structure.",0);
		 }
		 else
		 {
			 if(rsmchk[rsmchk.length-1].isChecked())
			 {
				 if(rsmchk_valueotr.trim().equals("&#40;"))
				 {
					 cf.ShowToast("Please enter the Other text for Roof Structural Materials under Roof Structure.",0);
					 cf.setFocus(((EditText)findViewById(R.id.rsmotr)));
					 //cf.ShowError(((EditText)findViewById(R.id.rsmotr)),"Please enter the Other text for Roof Structural Materials.");
				 }
				 else
				 {
					roofthick();
				 }
			 }
			 else
			 {
				 roofthick();
			 }
	     }
	   	 
	}
	private void roofthick() {
		// TODO Auto-generated method stub
		 rdtchk_vlaue= cf.getselected_chk(rdtchk);
		 String rdtchk_valueotr=(rdtchk[rdtchk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.rdtotr)).getText().toString():""; // append the other text value in to the selected option
		 rdtchk_vlaue+=rdtchk_valueotr;
		 if(rdtchk_vlaue.equals(""))
		 {
			 cf.ShowToast("Please select the option for Roof Deck Thickness under Roof Structure.",0);
		 }
		 else
		 {
			 if(rdtchk[rdtchk.length-1].isChecked())
			 {
				 if(rdtchk_valueotr.trim().equals("&#40;"))
				 {
					 cf.ShowToast("Please enter the Other text for Roof Deck Thickness under Roof Structure.",0);
					 cf.setFocus(((EditText)findViewById(R.id.rdtotr)));
					// cf.ShowError(((EditText)findViewById(R.id.rdtotr)),"Please enter the Other text for Roof Deck Thickness.");
				 }
				 else
				 {
					 roofattach();
				 }
			 }
			 else
			 {
				 roofattach();
			 }
	    }
	}
	private void roofattach() {
		// TODO Auto-generated method stub
		 rdachk_vlaue= cf.getselected_chk(rdachk);
		 String rdachk_valueotr=(rdachk[rdachk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.rdaotr)).getText().toString():""; // append the other text value in to the selected option
		 rdachk_vlaue+=rdachk_valueotr;
		 if(rdachk_vlaue.equals(""))
		 {
			 cf.ShowToast("Please select the option for Roof Deck Attachment under Roof Structure.",0);
		 }
		 else
		 {
			 if(rdachk[rdachk.length-1].isChecked())
			 {
				 if(rdachk_valueotr.trim().equals("&#40;"))
				 {
					 cf.ShowToast("Please enter the Other text for Roof Deck Attachment under Roof Structure.",0);
					 cf.setFocus(((EditText)findViewById(R.id.rdaotr)));
					// cf.ShowError(((EditText)findViewById(R.id.rdaotr)),"Please enter the Other text for Roof Deck Attachment.");
				 }
				 else
				 {
					 rooftruss();
				 }
			 }
			 else
			 {
				 rooftruss();
			 }
	    }
	}
	private void rooftruss() {
		// TODO Auto-generated method stub
	 	 rtschk_vlaue= cf.getselected_chk(rtschk);
		 if(rtschk_vlaue.equals(""))
		 {
			 cf.ShowToast("Please select the option for Roof Truss/Rafter Spacing under Roof Structure.",0);
		 }
		 else
		 {
			 nschk_vlaue= cf.getselected_chk(nschk);
			 if(nschk_vlaue.equals(""))
			 {
				 cf.ShowToast("Please select the option for Nail Spacing under Roof Structure.",0);
			 }
			 else
			 { 
				 anschk_vlaue= cf.getselected_chk(anschk);
				 if(anschk_vlaue.equals(""))
				 {
					 cf.ShowToast("Please select the option for Average # of missed nails in 4&#34; of structure under Roof Structure.",0);
				 }
				 else
				 {
					 RTWConnectionvalidation();
					 //RoofDeck_Insert();
				 }
		     }
	     }
	}
	private void RTWConnectionvalidation() {
		// TODO Auto-generated method stub
		if(!((CheckBox)findViewById(R.id.rwc_na)).isChecked())
		{
				 showstr="";rwcchkval="0";
				 rwcchk_vlaue= cf.getselected_chk(rwcchk);
		    	 String rwcchk_valueotr=(rwcchk[rwcchk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.rwcotr)).getText().toString():""; // append the other text value in to the selected option
		    	 rwcchk_vlaue+=rwcchk_valueotr;
		    	 if(rwcchk_vlaue.equals(""))
				 {
		    		cf.ShowToast("Please select the option for Roof to Wall Connection under Roof to Wall Connections.",0);
		    		
				 }
		    	 else
		    	 {
		    		 if(rwcchk[rwcchk.length-1].isChecked())
					 {
						 if(rwcchk_valueotr.trim().equals("&#40;"))
						 {
							 cf.ShowToast("Please enter the Other text for Roof to Wall Connection under Roof to Wall Connections.",0);
							 cf.setFocus(((EditText)findViewById(R.id.rwcotr)));
						 }
						 else
						 {
							nails3();
						 }
					 }
		    		 else
		    		 {
		    			 nails3();
		    		 }
	    	    }
		}
	    else
	    {
	    	rwcchkval="1";rwcchk_vlaue="";rwc3rdgtxt="";rwc4rdgtxt="";rwc5rdgtxt="";rwctrdgtxt="";rwc1trdgtxt="";
	    	GSValidation();
	    	//RoofWall_Insert();
	     }
	}
	private void GWS_checkbox() {
		// TODO Auto-generated method stub
	 gwschk_vlaue= cf.getselected_chk(gwschk);
   	 String gwschk_valueotr=(gwschk[gwschk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.gwsotr)).getText().toString():""; // append the other text value in to the selected option
   	 gwschk_vlaue+=gwschk_valueotr;
   	 if(gwschk_vlaue.equals(""))
	 {
   		 cf.ShowToast("Please select the option for Gable Wall Sheathing(Check Type) under Gable Wall Sheathing.",0);
	 }
   	 else
   	 {
   		  if(gwschk[gwschk.length-1].isChecked())
		  {
			 if(gwschk_valueotr.trim().equals("&#40;"))
			 {
				 cf.ShowToast("Please enter the Other text for Gable Wall Sheathing(Check Type) under Gable Wall Sheathing.",0);
				 cf.setFocus(((EditText)findViewById(R.id.gwsotr)));
			 }
			 else
			 {
				 Atticcondnotedvalidation() ;
			 }
		  }
   		  else
   		  {
   			Atticcondnotedvalidation() ;
   		  }
	 }
	}
	private void Atticcondnotedvalidation() {
		// TODO Auto-generated method stub
		if(!((CheckBox)findViewById(R.id.ac_na)).isChecked())
		{
			acchkval="0";
			if(acmsrdtxt.equals("Yes"))
			{
				 if(((EditText)findViewById(R.id.msyscomment)).getText().toString().trim().equals(""))
				 {
					 cf.ShowToast("Please enter the Comments for Moisture Stains/Leakage Noted under Attic Conditions Noted.",0);
					 cf.setFocus(((EditText)findViewById(R.id.msyscomment)));
				 }
				 else
				 {
					 acmsrdtxt += "&#126;"+((EditText)findViewById(R.id.msyscomment)).getText().toString().trim();
					 damagedrotted();
			     }
			 }
			 else
			 {
				 acmsrdtxt = acmsrdtxt;
				 damagedrotted();
			 }
		}
		else
		{
			acchkval="1";
			comments();
		}
	}
	private void GWS_Insert() {
		// TODO Auto-generated method stub
		    try
			{
				 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Attic+ " set Attic_Na='"+((CheckBox)findViewById(R.id.attic_na)).isChecked()+"',gablewallinc='"+gwschkval+"',gablewallopt='"+cf.encode(gwschk_vlaue)+"' where fld_srid='"+cf.selectedhomeid+"'");
				 ((TextView)findViewById(R.id.savetxtgws)).setVisibility(cf.v1.VISIBLE);
				 cf.ShowToast("Gable Wall Sheathing saved successfully.", 0); 
				 gws_save=true;
			}
			catch (Exception E)
			{
				String strerrorlog="Updating the GABLE WALL SHEATHING - FourPoint";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			}
	}
	private void chk_values() {
		// TODO Auto-generated method stub
		try
		{
			chkattic_save=cf.SelectTablefunction(cf.Four_Electrical_Attic, " where fld_srid='"+cf.selectedhomeid+"'");
			if(chkattic_save.getCount()>0){
				chkattic_save.moveToFirst();
				atcchk_vlaue = chkattic_save.getString(chkattic_save.getColumnIndex("atcinc"));
				retgwst = cf.decode(chkattic_save.getString(chkattic_save.getColumnIndex("gablewallopt")));
				rdmchk_vlaue = cf.decode(chkattic_save.getString(chkattic_save.getColumnIndex("rfdcmat")));
				retrtow = chkattic_save.getString(chkattic_save.getColumnIndex("rtowopt"));
				retacmsrdtxt = cf.decode(chkattic_save.getString(chkattic_save.getColumnIndex("moisture")));
				retrdmtxt = cf.decode(chkattic_save.getString(chkattic_save.getColumnIndex("rfdcmat")));
				retrd = chkattic_save.getString(chkattic_save.getColumnIndex("rfdcinc"));
				retrwc = chkattic_save.getString(chkattic_save.getColumnIndex("rfwallinc"));
				retgws = chkattic_save.getString(chkattic_save.getColumnIndex("gablewallinc"));
				retacn = chkattic_save.getString(chkattic_save.getColumnIndex("atccondinc"));
			}
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}
	private void AtticCond_Insert() {
		// TODO Auto-generated method stub
		try
		{
			System.out.println("acmsrdtxt"+acmsrdtxt);
			 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Attic+ " set Attic_Na='"+((CheckBox)findViewById(R.id.attic_na)).isChecked()+"', atccondinc='"+acchkval+"',moisture='"+cf.encode(acmsrdtxt)+"',"+
		      "dsheath='"+cf.encode(acdsrdtxt)+"',droof='"+cf.encode(acdrmrdtxt)+"',nsalt='"+cf.encode(acnsardtxt)+"',"+
			  "othertxt='"+cf.encode(((EditText)findViewById(R.id.otrcond)).getText().toString().trim())+"',"+
		      "other='"+cf.encode(acotrrdtxt)+"' where fld_srid='"+cf.selectedhomeid+"'"); 
			  ((TextView)findViewById(R.id.savetxtac)).setVisibility(cf.v1.VISIBLE);
			   cf.ShowToast("Attic Conditions Noted saved successfully.", 0);ac_save=true;
			   
		}
		catch (Exception E)
		{
			String strerrorlog="Updating the ATTIC CONDITIONS FourPoint";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
	private void RoofWall_Insert() {
		// TODO Auto-generated method stub
		try
		{
			System.out.println("tryinsert");
			 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Attic+ " set Attic_Na='"+((CheckBox)findViewById(R.id.attic_na)).isChecked()+"',rfwallinc='"+rwcchkval+"',rtowopt='"+cf.encode(rwcchk_vlaue)+"',"+
		      "nail3='"+cf.encode(rwc3rdgtxt)+"',nail4='"+cf.encode(rwc4rdgtxt)+"',nail5='"+cf.encode(rwc5rdgtxt)+"',truss='"+cf.encode(rwctrdgtxt)+"',"+
			  "truss1='"+cf.encode(rwc1trdgtxt)+"' where fld_srid='"+cf.selectedhomeid+"'");
			 String[] rwc = rwcchkval.split("~");
			 ((TextView)findViewById(R.id.savetxtrwc)).setVisibility(cf.v1.VISIBLE);
			 cf.ShowToast("Roof Wall Connections saved successfully.", 0); 
			 rwc_save=true;
		}
		catch (Exception E)
		{
			System.out.println("catchinside"+E.getMessage());
			String strerrorlog="Updating the ROOF WALL CONNECTIONS - FourPoint";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
	private void RoofDeck_Insert() {
		// TODO Auto-generated method stub
		try
		{
			cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Attic+ " set Attic_Na='"+((CheckBox)findViewById(R.id.attic_na)).isChecked()+"',rfdcinc='"+rdchkval+"',rfdcmat='"+cf.encode(rdmchk_vlaue)+"',"+
		      "rfstmat='"+cf.encode(rsmchk_vlaue)+"',rfdcthi='"+cf.encode(rdtchk_vlaue)+"',rfdcatt='"+cf.encode(rdachk_vlaue)+"',"+
			  "rftruss='"+cf.encode(rtschk_vlaue)+"',nailspac='"+cf.encode(nschk_vlaue)+"',missednails='"+cf.encode(anschk_vlaue)+"' where fld_srid='"+cf.selectedhomeid+"'");
			 ((TextView)findViewById(R.id.savetxtrd)).setVisibility(cf.v1.VISIBLE);
			 cf.ShowToast("Roof Structure saved successfully.", 0); 
			 rd_save=true;
		}
		catch (Exception E)
		{
			String strerrorlog="Updating the ROOF DECK - FourPoint";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
	private void uncheckattic(CheckBox[] atcchkopt2) {
		// TODO Auto-generated method stub
		for(int i=0;i<atcchkopt2.length;i++){
			atcchkopt2[i].setChecked(false);
		}
	}

	private void Uncheckhdrs(CheckBox[] atcchk2) {
		// TODO Auto-generated method stub
		for(int i=0;i<atcchk2.length;i++)
		{
			atcchk2[i].setChecked(false);
			
		}
		chk_values();
		if(!rdmchk_vlaue.equals("")){
			//atcchk[0].setChecked(true);
			((LinearLayout)findViewById(R.id.rdlin)).setVisibility(cf.v1.VISIBLE);
		}
		else
		{
			clearrd();
		}
		if(!rwcchk_vlaue.equals("")){
			//atcchk[1].setChecked(true);
			((LinearLayout)findViewById(R.id.rwclin)).setVisibility(cf.v1.VISIBLE);
		}
		else
		{
			clearrwc();
		}
		if(!gwschk_vlaue.equals("")){
			//atcchk[2].setChecked(true);
			((LinearLayout)findViewById(R.id.gwslin)).setVisibility(cf.v1.VISIBLE);
		}
		else
		{
			gwschkval="0";gwschk_vlaue="";retgwst="";
			 cf.Set_UncheckBox(gwschk, ((EditText)findViewById(R.id.gwsotr)));
		}
		if(!retacmsrdtxt.equals("")){
			//atcchk[3].setChecked(true);
			((LinearLayout)findViewById(R.id.aclin)).setVisibility(cf.v1.VISIBLE);
		}
		else
		{
			clearac();
		}
	}
	private void hideotherlayouts() {
		// TODO Auto-generated method stub
		((LinearLayout)findViewById(R.id.rdlin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.rwclin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.gwslin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.aclin)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.shwrfdeck)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwrfwall)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwgwall)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwatc)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.hdwrfdeck)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdrfwall)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdgwall)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdatc)).setVisibility(cf.v1.GONE);
		
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
 		// replaces the default 'Back' button action
 		if (keyCode == KeyEvent.KEYCODE_BACK) {
 			cf.Roof=cf.All_insp_list[1];
 			cf.goback(1);
 			return true;
 		}
 		
 		return super.onKeyDown(keyCode, event);
 	}
	public void showunchkalert(final int i) {
		// TODO Auto-generated method stub
		String titname="";
		switch (i) {
		case 1:
			titname="Roof Structure";
			break;
		case 2:
			titname="Roof Wall Connections";
			break;
		case 3:
			titname="Gable Wall Sheathing";
			break;
		case 4:
			titname="Attic Conditions Noted";
			break;
		default:
			break;
		}
		AlertDialog.Builder bl = new Builder(Attic.this);
		bl.setTitle("Confirmation");
		bl.setMessage(Html.fromHtml("Do you want to clear the "+titname+" data?"));
		bl.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog,
											int id) {
										 switch(i){
										 case 1:
											 ((TextView)findViewById(R.id.savetxtrd)).setVisibility(cf.v1.INVISIBLE);
							    			 clearrd();rdchkval="1";rd_save=false;
							    			// RoofDeck_Insert();
							    			 ((LinearLayout)findViewById(R.id.rdlin)).setVisibility(cf.v1.GONE);
							    			 ((ImageView)findViewById(R.id.shwrfdeck)).setVisibility(cf.v1.VISIBLE);
							    			 ((ImageView)findViewById(R.id.hdwrfdeck)).setVisibility(cf.v1.GONE);
							    			 ((ImageView)findViewById(R.id.shwrfdeck)).setEnabled(false);
							    			// ((RelativeLayout)findViewById(R.id.atticrelrdchk)).setEnabled(false);
							    			 break;
										 case 2:
											 ((TextView)findViewById(R.id.savetxtrwc)).setVisibility(cf.v1.INVISIBLE);
							    			 clearrwc();rwcchkval="1";rwc_save=false;
							    			// RoofWall_Insert();
							    			 ((LinearLayout)findViewById(R.id.rwclin)).setVisibility(cf.v1.GONE);
							    			 ((ImageView)findViewById(R.id.shwrfwall)).setVisibility(cf.v1.VISIBLE);
							    			 ((ImageView)findViewById(R.id.hdrfwall)).setVisibility(cf.v1.GONE);
							    			 ((ImageView)findViewById(R.id.shwrfwall)).setEnabled(false);
							    			// ((RelativeLayout)findViewById(R.id.atticrelrwcchk)).setEnabled(false);
											 break;
										 case 3:
											 ((TextView)findViewById(R.id.savetxtgws)).setVisibility(cf.v1.INVISIBLE);
							    			 gwschkval="1";gwschk_vlaue="";retgwst="";gws_save=false;
							    			 cf.Set_UncheckBox(gwschk, ((EditText)findViewById(R.id.gwsotr)));
							    			// GWS_Insert();
							    			 ((LinearLayout)findViewById(R.id.gwslin)).setVisibility(cf.v1.GONE);
							    			 ((ImageView)findViewById(R.id.shwgwall)).setVisibility(cf.v1.VISIBLE);
							    			 ((ImageView)findViewById(R.id.hdgwall)).setVisibility(cf.v1.GONE);
							    			 ((ImageView)findViewById(R.id.shwgwall)).setEnabled(false);
							    			// ((RelativeLayout)findViewById(R.id.atticrelgwschk)).setEnabled(false);
											 break;
										 case 4:
											 ((TextView)findViewById(R.id.savetxtac)).setVisibility(cf.v1.INVISIBLE);
							    			 clearac();defaultac();acmsrdtxt="";acchkval="1";ac_save=false;
							    			// AtticCond_Insert();
							    			 //defaultac();
							    			 ((ImageView)findViewById(R.id.shwatc)).setVisibility(cf.v1.VISIBLE);
							    			 ((ImageView)findViewById(R.id.hdatc)).setVisibility(cf.v1.GONE);
							    			 ((ImageView)findViewById(R.id.shwatc)).setEnabled(false);
							    			// ((RelativeLayout)findViewById(R.id.atticrelacchk)).setEnabled(false);
											 break;
										
										 }
										 	 
										 
									}
		});
		bl.setNegativeButton("No",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog,
							int id) {
						switch(i){
						 case 1:
							 ((CheckBox)findViewById(R.id.rd_na)).setChecked(false);
							 ((ImageView)findViewById(R.id.shwrfdeck)).setVisibility(cf.v1.GONE);
				    		 ((ImageView)findViewById(R.id.hdwrfdeck)).setVisibility(cf.v1.VISIBLE);
				    		 ((LinearLayout)findViewById(R.id.rdlin)).setVisibility(cf.v1.VISIBLE);
							break;
						 case 2:
							 ((CheckBox)findViewById(R.id.rwc_na)).setChecked(false);
							 ((ImageView)findViewById(R.id.shwrfwall)).setVisibility(cf.v1.GONE);
				    		 ((ImageView)findViewById(R.id.hdrfwall)).setVisibility(cf.v1.VISIBLE);
				    		 ((LinearLayout)findViewById(R.id.rwclin)).setVisibility(cf.v1.VISIBLE);
							break;
						 case 3:
							 ((CheckBox)findViewById(R.id.gws_na)).setChecked(false);
							 ((ImageView)findViewById(R.id.shwgwall)).setVisibility(cf.v1.GONE);
				    		 ((ImageView)findViewById(R.id.hdgwall)).setVisibility(cf.v1.VISIBLE);
				    		 ((LinearLayout)findViewById(R.id.gwslin)).setVisibility(cf.v1.VISIBLE);
							 break;
						 case 4:
							 ((CheckBox)findViewById(R.id.ac_na)).setChecked(false);
							 ((ImageView)findViewById(R.id.shwatc)).setVisibility(cf.v1.GONE);
				    		 ((ImageView)findViewById(R.id.hdatc)).setVisibility(cf.v1.VISIBLE);
				    		 ((LinearLayout)findViewById(R.id.aclin)).setVisibility(cf.v1.VISIBLE);
							 break;
						 }
					}
        });
		AlertDialog al=bl.create();
		al.setIcon(R.drawable.alertmsg);
		al.setCancelable(false);
		al.show();
		
	}
	protected void clearrd() {
		// TODO Auto-generated method stub
		 rdchkval="0";rdmchk_vlaue="";rsmchk_vlaue="";rdtchk_vlaue="";
		 rdachk_vlaue="";rtschk_vlaue="";nschk_vlaue="";anschk_vlaue="";
		 cf.Set_UncheckBox(rdmchk, ((EditText)findViewById(R.id.rdmotr)));
		 cf.Set_UncheckBox(rsmchk, ((EditText)findViewById(R.id.rsmotr)));
		 cf.Set_UncheckBox(rdtchk, ((EditText)findViewById(R.id.rdtotr)));
		 cf.Set_UncheckBox(rdachk, ((EditText)findViewById(R.id.rdaotr)));
		 cf.Set_UncheckBox(rtschk, ((EditText)findViewById(R.id.rdmotr)));
		 cf.Set_UncheckBox(nschk, ((EditText)findViewById(R.id.rdmotr)));
		 cf.Set_UncheckBox(anschk, ((EditText)findViewById(R.id.rdmotr)));
		 anschk[6].setChecked(true);
		 rdachk[9].setChecked(true);
		 rtschk[5].setChecked(true);
		 nschk[5].setChecked(true);
		 rdtchk[9].setChecked(true);
		 retrdmtxt="";
	}
	protected void clearrwc() {
		// TODO Auto-generated method stub
		 rwcchkval="0";rwcchk_vlaue="";nl3ck=true;rwc3rdgtxt="";
		 nl4ck=true;nl5ck=true;rwc4rdgtxt="";rwc5rdgtxt="";tsck=true;rwctrdgtxt="";
		 ts15ck=true;rwc1trdgtxt="";retrtow="";
		 try{if(nl3ck){rwc3rdgval.clearCheck();}}catch(Exception e){}
		 try{if(nl4ck){rwc4rdgval.clearCheck();}}catch(Exception e){}
		 try{if(nl5ck){rwc5rdgval.clearCheck();}}catch(Exception e){}
		 try{if(tsck){rwctrdgval.clearCheck();}}catch(Exception e){}
		 try{if(ts15ck){rwc1trdgval.clearCheck();}}catch(Exception e){}
		 cf.Set_UncheckBox(rwcchk, ((EditText)findViewById(R.id.rwcotr)));
	}
	protected void defaultac() {
		// TODO Auto-generated method stub
		 ((RadioButton)acmsrdgval.findViewWithTag("N/A")).setChecked(true);
		 ((RadioButton)acdsrdgval.findViewWithTag("N/A")).setChecked(true);
		 ((RadioButton)acdrmrdgval.findViewWithTag("N/A")).setChecked(true);
		 ((RadioButton)acnsardgval.findViewWithTag("N/A")).setChecked(true);
		 ((RadioButton)acotrrdgval.findViewWithTag("N/A")).setChecked(true);
	     ((LinearLayout)findViewById(R.id.aclin)).setVisibility(cf.v1.GONE);
	}
	protected void clearac() {
		// TODO Auto-generated method stub
		acchkval="0";msck=true;dsck=true;drmck=true;nsack=true;otrck=true;
		 try{if(msck){acmsrdgval.clearCheck();}}catch(Exception e){}
		 try{if(dsck){acdsrdgval.clearCheck();}}catch(Exception e){}
		 try{if(drmck){acdrmrdgval.clearCheck();}}catch (Exception e) {}
		 try{if(nsack){acnsardgval.clearCheck();}}catch(Exception e){}
		 try{if(otrck){acotrrdgval.clearCheck();}}catch(Exception e){}
		 acmsrdtxt="";((EditText)findViewById(R.id.msyscomment)).setText("");
		 ((LinearLayout)findViewById(R.id.msyslin)).setVisibility(cf.v1.GONE);
		 acdsrdtxt="";((EditText)findViewById(R.id.dsyscomment)).setText("");
		 ((LinearLayout)findViewById(R.id.dsyslin)).setVisibility(cf.v1.GONE);
		 acdrmrdtxt="";((EditText)findViewById(R.id.drmyscomment)).setText("");
		 ((LinearLayout)findViewById(R.id.drmyslin)).setVisibility(cf.v1.GONE);
		 acnsardtxt="";((EditText)findViewById(R.id.nsayscomment)).setText("");
		 ((LinearLayout)findViewById(R.id.nsayslin)).setVisibility(cf.v1.GONE);
		 acotrrdtxt="";((EditText)findViewById(R.id.otryscomment)).setText("");
		 ((EditText)findViewById(R.id.otrcond)).setText("");retacmsrdtxt="";
		 ((LinearLayout)findViewById(R.id.otryslin)).setVisibility(cf.v1.GONE);
		 atc = checkattic();
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	
		  if(requestCode==cf.loadcomment_code)
		  {
			load_comment=true;
			if(resultCode==RESULT_OK)
			{
				 cf.ShowToast("Comments added successfully.", 0);
				 String atccomts = ((EditText)findViewById(R.id.atticcomment)).getText().toString();
				 ((EditText)findViewById(R.id.atticcomment)).setText((atccomts+" "+data.getExtras().getString("Comments")).trim());
				
			}
			
		}

	}
}