package idsoft.inspectiondepot.ARR;

import java.io.File;

import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


public class Dashboard extends Activity implements Runnable{
	CommonFunctions cf;
	LinearLayout mainlinear;
	int v;
	TextView txtexplain,txtcit,txtcio,txtuts,txtcan,txtrr,onltxtcio,onltxtuts,onltxtcan,onltxtrr;
	 public TextView releasecode,welcome,txtversion;
	 public ImageView img_InspectorPhoto;
	 public String versionname,newcode,newversion;
	 public int retschedule=0,retassign=0,retcio=0,retuts=0,retcan=0,retrr=0,retcit=0,carrtotal=0,rettotal=0,
			 onlassign=0,onlsch=0,onlcio=0,onluts=0,onlcan=0,onlrr=0,onltotal=0;
	 public Button btn_retassign,btn_retschedule,btn_retcit,btn_retcio,btn_retuts,btn_retcan,btn_rettotal,btn_retrr,	            
	               btn_onlretassign,btn_onlretschedule,btn_onlretcio,btn_onlretuts,btn_onlretcan,btn_onlretrr,btn_onlrettotal;
	 public int onlretassign,onlretschedule,onlretcio,onlretuts,onlretcan,onlrettotal,onlretrr;
	 ProgressDialog pd;
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        cf = new CommonFunctions(this);
	        setContentView(R.layout.dash);
	        cf.getInspectorId();
	        welcome = (TextView) this.findViewById(R.id.welcomename);
	        welcome.setText(cf.Insp_firstname.toUpperCase()+" "+cf.Insp_lastname.toUpperCase());
	        txtcit = (TextView)findViewById(R.id.txtcit);
	        txtcio = (TextView)findViewById(R.id.txtcio);
	        txtuts = (TextView)findViewById(R.id.txtuts);
	        txtcan = (TextView)findViewById(R.id.txtcan);
	        txtrr = (TextView)findViewById(R.id.txtrr);
	        onltxtcio = (TextView)findViewById(R.id.onltxtcio);
	        onltxtuts = (TextView)findViewById(R.id.onltxtuts);
	        onltxtrr = (TextView)findViewById(R.id.onltxtrr);
	        onltxtcan = (TextView)findViewById(R.id.onltxtcan);
	        txtcit.setText(Html.fromHtml("CIT "+cf.redcolor));
	        txtcio.setText(Html.fromHtml("CIO "+cf.redcolor));
	        txtuts.setText(Html.fromHtml("UTS "+cf.redcolor));
	        txtcan.setText(Html.fromHtml("CAN "+cf.redcolor));
	        txtrr.setText(Html.fromHtml("RR "+cf.redcolor));
	        onltxtcio.setText(Html.fromHtml("CIO "+cf.redcolor));
	        onltxtuts.setText(Html.fromHtml("UTS "+cf.redcolor));
	        onltxtcan.setText(Html.fromHtml("CAN "+cf.redcolor));
	        onltxtrr.setText(Html.fromHtml("RR "+cf.redcolor));
	        txtexplain = (TextView)findViewById(R.id.txtbriefexplanation);
            txtexplain.setText(Html.fromHtml("CIT "+cf.redcolor+"  - Completed Inspections in Tablet " + ";"+" "
							+ "CIO "+cf.redcolor+"  - Completed Inspections in Online" + "<br/>"
							+ "UTS "+cf.redcolor+"  - Unable to Schedule " + ";"+" "
							+ "CAN "+cf.redcolor+"  - Unable to Schedule " + ";"+" "
							+ "RR "+cf.redcolor+" - Reports Ready" + "<br/>"));
            
            btn_retassign = (Button)findViewById(R.id.retassign);
            btn_retschedule= (Button)findViewById(R.id.retschedule);
            btn_retcit = (Button)findViewById(R.id.retcit);
            btn_retcio = (Button)findViewById(R.id.retcio);
            btn_retuts = (Button)findViewById(R.id.retuts);
            btn_retcan = (Button)findViewById(R.id.retcan);
            btn_retrr = (Button)findViewById(R.id.retrr);
            btn_rettotal = (Button)findViewById(R.id.rettotal);
            
            
            btn_onlretassign = (Button)findViewById(R.id.onlretassign);
            btn_onlretschedule= (Button)findViewById(R.id.onlretschedule);
            btn_onlretcio = (Button)findViewById(R.id.onlretcio);
            btn_onlretuts = (Button)findViewById(R.id.onlretuts);
            btn_onlretcan = (Button)findViewById(R.id.onlretcan);
            btn_onlretrr = (Button)findViewById(R.id.onlretrr);
            btn_onlrettotal = (Button)findViewById(R.id.onlrettotal);
            db_sttaus();
            dynamic_inspname();
            
            ImageView im =(ImageView) findViewById(R.id.head_insp_info);
    		im.setVisibility(View.VISIBLE);
    		im.setOnClickListener(new OnClickListener() {
    			
    			@Override
    			public void onClick(View v) {
    				// TODO Auto-generated method stub
    				    Intent s2= new Intent(Dashboard.this,PolicyholdeInfoHead.class);
    					Bundle b2 = new Bundle();
    					s2.putExtra("homeid", cf.selectedhomeid);
    					s2.putExtra("Type", "Inspector");
    					s2.putExtra("insp_id", cf.Insp_id);
    					((Activity) cf.con).startActivityForResult(s2, cf.info_requestcode);
    			}
    		});
           
	 }
	 private void dynamic_inspname() {
		// TODO Auto-generated method stub
			/*try 
			{
				cf.CreateARRTable(13);
				Cursor c1 =cf.arr_db.rawQuery("select ARR_Insp_Name as a from  "+cf.dyninspname+" where ARR_Insp_Type_ID='36' and ARR_PH_InspectorId='"+cf.Insp_id+"'", null);	
				Cursor c2 =cf.arr_db.rawQuery("select ARR_Insp_Name as a from  "+cf.dyninspname+" where ARR_Insp_Type_ID='37' and ARR_PH_InspectorId='"+cf.Insp_id+"'", null);
				c1.moveToFirst();
				c2.moveToFirst();
				String arrcartit = cf.decode(c1.getString(c1.getColumnIndex("a")));
				String arrrettit = cf.decode(c2.getString(c2.getColumnIndex("a")));
				
				((TextView)findViewById(R.id.arrcarid)).setText(arrcartit);
				((TextView)findViewById(R.id.arrretid)).setText(arrrettit);
				((TextView)findViewById(R.id.arronlcarid)).setText(arrcartit);
				((TextView)findViewById(R.id.arronlretid)).setText(arrrettit);
			}
			catch(Exception e)
			{
				System.out.println("error in dynamic ="+e.getMessage());
			}*/
	}
	
	 public static boolean deleteDir(File dir) {
		    if (dir != null && dir.isDirectory()) {
		        String[] children = dir.list();
		        for (int i = 0; i < children.length; i++) {
		            boolean success = deleteDir(new File(dir, children[i]));
		            System.out.println("success"+success);
		            if (!success) {
		                return false;
		            }
		        }
		    }

		    return dir.delete();
		}
	private void db_sttaus() {
		// TODO Auto-generated method stub
		cf.CreateARRTable(3);
				try {
					Cursor cur = cf.SelectTablefunction(cf.policyholder," where ARR_PH_InspectorId='" + cf.encode(cf.Insp_id) + "'");
					System.out.println(" where ARR_PH_InspectorId='" + cf.encode(cf.Insp_id) + "'"+cur.getCount());
					cur.moveToFirst();
					if (cur != null) {
						do{
							
							/*CHECKING ARR */
							if (cf.encode(cf.Insp_id).equals(cf.encode(cur.getString(cur.getColumnIndex("ARR_PH_InspectorId")))))
							{
								if(cf.encode(cur.getString(cur.getColumnIndex("ARR_PH_Status"))).equals("40")
										&& !cf.encode(cur.getString(cur.getColumnIndex("ARR_PH_SubStatus"))).equals("41")) {
									retschedule++;
								}
								if (cf.encode(cur.getString(cur.getColumnIndex("ARR_PH_Status"))).equals("30")) {
									retassign++;
								}
								if (cf.encode(cur.getString(cur.getColumnIndex("ARR_PH_SubStatus"))).equals("41")) {
									retcio++;
								}
								if (cf.encode(cur.getString(cur.getColumnIndex("ARR_PH_Status"))).equals("110")) {
									retuts++;
								}
								if (cf.encode(cur.getString(cur.getColumnIndex("ARR_PH_Status"))).equals("111")) {
									retcan++;
								}
								if (cf.encode(cur.getString(cur.getColumnIndex("ARR_PH_IsInspected"))).equals("1")) {
									retcit++;
								}
							
								if((cf.encode(cur.getString(cur.getColumnIndex("ARR_PH_Status"))).equals("2") && cf.encode(cur.getString(cur.getColumnIndex("ARR_PH_SubStatus"))).equals("0"))
									|| (cf.encode(cur.getString(cur.getColumnIndex("ARR_PH_Status"))).equals("5") && cf.encode(cur.getString(cur.getColumnIndex("ARR_PH_SubStatus"))).equals("0"))
									|| (cf.encode(cur.getString(cur.getColumnIndex("ARR_PH_Status"))).equals("2") && cf.encode(cur.getString(cur.getColumnIndex("ARR_PH_SubStatus"))).equals("121"))
									|| (cf.encode(cur.getString(cur.getColumnIndex("ARR_PH_Status"))).equals("2") && cf.encode(cur.getString(cur.getColumnIndex("ARR_PH_SubStatus"))).equals("122")) 
									|| cf.encode(cur.getString(cur.getColumnIndex("ARR_PH_Status"))).equals("140")) 
								{
									retrr++;
								}
								
								
							}
							
						} while (cur.moveToNext());
					}
					cur.close();
				} catch (Exception e) {
					cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ Dashboard.this +" "+" in the processing stage of separating carrier and retail data from PH table  at "+" "+ cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
					
				}
				try
				{
					Cursor c=cf.SelectTablefunction(cf.count_tbl, " WHERE insp_id='"+cf.Insp_id+"'");
					c.moveToFirst();
					if(c.getCount()>0)
					{
						retcio=c.getInt(c.getColumnIndex("C_28_pre"));
						retuts=c.getInt(c.getColumnIndex("C_28_uts"));
						retcan=c.getInt(c.getColumnIndex("C_28_can"));
						retrr=c.getInt(c.getColumnIndex("C_28_RR"));
						/*typ16pre=c.getInt(c.getColumnIndex("C_28_pre"));
						typ16uts=c.getInt(c.getColumnIndex("C_28_uts"));
						typ16can=c.getInt(c.getColumnIndex("C_28_can"));
						reportsready16=c.getInt(c.getColumnIndex("C_28_RR"));
*/						
						//cf.db.execSQL(" UPDATE "+cf.count_tbl+" SET (C_28_pre='"+ontyppre+"',C_28_uts='"+ontyputs+"',C_28_can='"+ontypcan+"',C_28_RR='"+ontyprr+"',C_29_pre='"+onpre+"',C_29_uts='"+onuts+"',C_29_can='"+oncan+"',C_29_RR='"+onrr+"')");
					}
				}catch (Exception e) {
					// TODO: handle exception
				}
				rettotal = retschedule + retassign + retcit	+ retcio + retuts + retrr + retcan;
               
                btn_retassign.setText(String.valueOf(retassign));
                btn_retschedule.setText(String.valueOf(retschedule));
                btn_retcit.setText(String.valueOf(retcit));
                btn_retcio.setText(String.valueOf(retcio));
                btn_retuts.setText(String.valueOf(retuts));
                btn_retcan.setText(String.valueOf(retcan));
                btn_retrr.setText(String.valueOf(retrr));
                btn_rettotal.setText(String.valueOf(rettotal));
               
	}
	public void clicker(View v)
	 {
		Intent in ;
		  switch(v.getId())
		  {
		  
		  case R.id.home:
			  cf.gohome();
			  break;
		  case R.id.retassign: /*CLICK EVENT FOR RETAIL ASSIGN*/
			  cf.MoveTo_HomeOwner("28",retassign,"Assign",1);
			  break;
		  case R.id.retschedule: /*CLICK EVENT FOR RETAIL SCHEDULE*/
			  cf.MoveTo_HomeOwner("28",retschedule,"Schedule",1);
			  break;
		  case R.id.retcit: /*CLICK EVENT FOR RETAIL CIT*/
			  cf.MoveTo_HomeOwner("28",retcit,"CIT",1);
			  break;
		  case R.id.retcio: /*CLICK EVENT FOR RETAIL CIO*/
			  //cf.MoveTo_HomeOwner("28",retcio,"CIO",2);
			  if(retcio>0)
			  {
			  in=new Intent(Dashboard.this,InspectionListing_paging.class);
			  in.putExtra("Status", "CIO");
			  in.putExtra("Total", retcio);
			  in.putExtra("Online", false);
			  startActivity(in);
			  }
			  else
			  {
				  cf.ShowToast("No records found for the Completed in Online  status", 0);
			  }
			  break;
		  case R.id.retuts: /*CLICK EVENT FOR RETAIL UTS*/
			 // cf.MoveTo_HomeOwner("28",retuts,"UTS",2);
			  if(retuts>0)
			  {
			  in=new Intent(Dashboard.this,InspectionListing_paging.class);
			  in.putExtra("Status", "UTS");
			  in.putExtra("Total", retuts);
			  in.putExtra("Online", false);
			  startActivity(in);
			  }
			  else
			  {
				  cf.ShowToast("No records found for the Unable to Schedule  status", 0);
			  }
			  break;
		  case R.id.retcan: /*CLICK EVENT FOR RETAIL CAN*/
			  //cf.MoveTo_HomeOwner("28",retcan,"CAN",2);
			  if(retcan>0)
			  {
			  in=new Intent(Dashboard.this,InspectionListing_paging.class);
			  in.putExtra("Status", "CAN");
			  in.putExtra("Total", retcan);
			  in.putExtra("Online", false);
			  startActivity(in);
			  }
			  else
			  {
				  cf.ShowToast("No records found for the Cancel status", 0);
			  }    
			  break;
		  case R.id.retrr: /*CLICK EVENT FOR RETAIL RR*/
			 // cf.MoveTo_HomeOwner("28",retrr,"RR",2);
			  if(retrr>0)
			  {
			  in=new Intent(Dashboard.this,InspectionListing_paging.class);
			  in.putExtra("Status", "RR");
			  in.putExtra("Total", retrr);
			  in.putExtra("Online", false);
			  startActivity(in);
			  }
			  else
			  {
				  cf.ShowToast("No records found for the Reports Ready status", 0);
			  }
			 break;
		 
		 
		  case R.id.online:
			//  mainlinear.setActivated(false);
			  cf.alerttitle="Synchronization";
			  cf.alertcontent="Do you want to Synchronize with Online?";
			    final Dialog dialog1 = new Dialog(Dashboard.this,android.R.style.Theme_Translucent_NoTitleBar);
				dialog1.getWindow().setContentView(R.layout.alertsync);
				TextView txttitle = (TextView) dialog1.findViewById(R.id.txthelp);
				txttitle.setText( cf.alerttitle);
				TextView txt = (TextView) dialog1.findViewById(R.id.txtid);
				txt.setText(Html.fromHtml( cf.alertcontent));
				Button btn_yes = (Button) dialog1.findViewById(R.id.yes);
				Button btn_cancel = (Button) dialog1.findViewById(R.id.cancel);
				btn_yes.setOnClickListener(new OnClickListener()
				{
                	@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						dialog1.dismiss();
						if(cf.isInternetOn()==true)
						{
                           String source = "<font color=#FFFFFF>Loading data. Please wait..."
									+ "</font>";
							pd = ProgressDialog.show(Dashboard.this,"", Html.fromHtml(source), true);
							Thread thread = new Thread(Dashboard.this);
							thread.start();
						}
						else
						{
							cf.ShowToast("Internet connection not available.",0);
						}
						
					}
					
				});
				btn_cancel.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						dialog1.dismiss();
						
					}
					
				});
				dialog1.setCancelable(false);
				dialog1.show();
			    
			  break;
		  case R.id.onlretassign: /*CLICK EVENT FOR ONLINE RETAIL -ASSIGN*/
			  cf.MoveTo_OnlineList("0",onlassign,"30","0","Assign");
			  break;
		  case R.id.onlretschedule: /*CLICK EVENT FOR ONLINE RETAIL -SCHEDULE*/
			  cf.MoveTo_OnlineList("0",onlsch,"40","0","Schedule");
			  break;
		  case R.id.onlretcio: /*CLICK EVENT FOR ONLINE RETAIL -CIO*/
			  //cf.MoveTo_OnlineList("0",onlcio,"40","41","CIO");
			  if(onlcio>0)
			  {
			  in=new Intent(Dashboard.this,InspectionListing_paging.class);
			  in.putExtra("Status", "CIO");
			  in.putExtra("Total", onlcio);
			  in.putExtra("Online", false);
			  startActivity(in);
			  }
			  else
			  {
				  cf.ShowToast("No records found for the Completed in Online  status", 0);
			  }
			  break;
		  case R.id.onlretuts: /*CLICK EVENT FOR ONLINE RETAIL -UTS*/
			 // cf.MoveTo_OnlineList("0",onluts,"110","111","UTS");
			  if(onluts>0)
			  {
			  in=new Intent(Dashboard.this,InspectionListing_paging.class);
			  in.putExtra("Status", "UTS");
			  in.putExtra("Total", onluts);
			  in.putExtra("Online", false);
			  startActivity(in);
			  }
			  else
			  {
				  cf.ShowToast("No records found for the Unable to Schedule  status", 0);
			  }
			  break;
		  case R.id.onlretcan: /*CLICK EVENT FOR ONLINE RETAIL -CAN*/
			//  cf.MoveTo_OnlineList("0",onlcan,"111","0","CAN");
			  if(onlcan>0)
			  {
			  in=new Intent(Dashboard.this,InspectionListing_paging.class);
			  in.putExtra("Status", "CAN");
			  in.putExtra("Total", onlcan);
			  in.putExtra("Online", false);
			  startActivity(in);
			  }
			  else
			  {
				  cf.ShowToast("No records found for the Cancel status", 0);
			  }    
			  break;	
		  case R.id.onlretrr: /*CLICK EVENT FOR ONLINE RETAIL -RR*/
		//	  cf.MoveTo_OnlineList("1",onlrr,"2","0","RR");
			  if(onlrr>0)
			  {
			  in=new Intent(Dashboard.this,InspectionListing_paging.class);
			  in.putExtra("Status", "RR");
			  in.putExtra("Total", onlrr);
			  in.putExtra("Online", false);
			  startActivity(in);
			  }
			  else
			  {
				  cf.ShowToast("No records found for the Reports Ready status", 0);
			  }
			  break;	
		  }
	 }
	public void run() {
		// TODO Auto-generated method stub
			try {
				SoapObject onlresult=cf.Calling_WS1(cf.Insp_id, "UPDATEMOBILEDBCOUNT");
				 retrieveonlinedata(onlresult);
                 v=0;
                
			} catch (Exception e) {
				System.out.println("e="+e.getMessage());
               v=1;
			}
			 handler.sendEmptyMessage(0);
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			pd.dismiss();
		if(v==0){
			if (onlassign != 0) {
				btn_onlretassign.setText(String.valueOf(onlassign));

			} else {
				btn_onlretassign.setText("0");
			}
			if (onlsch != 0) {
				btn_onlretschedule.setText(String.valueOf(onlsch));

			} else {
				btn_onlretschedule.setText("0");
			}
			if (onlcio != 0) {
				btn_onlretcio.setText(String.valueOf(onlcio));

			} else {
				btn_onlretcio.setText("0");
			}
			if (onluts != 0) {
				btn_onlretuts.setText(String.valueOf(onluts));

			} else {
				btn_onlretuts.setText("0");
			}
			if (onlcan != 0) {
				btn_onlretcan.setText(String.valueOf(onlcan));

			} else {
				btn_onlretcan.setText("0");
			}
			if (onlrr != 0) {
				btn_onlretrr.setText(String.valueOf(onlrr));

			} else {
				btn_onlretrr.setText("0");
			}
			
			if (onltotal != 0) {
				btn_onlrettotal.setText(String.valueOf(onltotal));

			} else {
				btn_onlrettotal.setText("0");
			}
		 }
		 else if(v==1)
		 {
			 cf.ShowToast("There is a problem in the server. Please contact paperless admin.", 0);
		 }
		}
	};
	public void retrieveonlinedata(SoapObject onlresult)
	{
		onlretassign=0;onlretschedule=0;onlretcio=0;onlretuts=0;onlretcan=0;onlretrr=0;onlrettotal=0;
		onlassign=0;onlsch=0;onlcio=0;onluts=0;onlcan=0;onlrr=0;onltotal=0;
		int Cnt = onlresult.getPropertyCount();
		if (Cnt >= 1) {
			System.out.println("onlresult"+onlresult);
			for (int i = 0; i < Cnt; i++) 
			{
				SoapObject obj1 = (SoapObject) onlresult.getProperty(i);
				int Cnt1 = obj1.getPropertyCount();
				for (int j = 0; j < Cnt1; j++) 
				{
					PropertyInfo pi = new PropertyInfo();
					obj1.getPropertyInfo(j, pi);
					if (!obj1.getProperty("i_maininspectiontype").toString().equals("")) 
					{
						if(pi.name.equals("Assign"))
						{
							onlretassign = Integer.parseInt(obj1.getProperty(j).toString());
							onlassign = onlassign+onlretassign;							
						}
						
						if(pi.name.equals("Sch"))
						{
							onlretschedule = Integer.parseInt(obj1.getProperty(j).toString());
							onlsch = onlsch+onlretschedule;							
						}
						
						if(pi.name.equals("Comp_Ins_in_Online"))
						{
							onlretcio = Integer.parseInt(obj1.getProperty(j).toString());
							onlcio = onlcio+onlretcio;							
						}
						
						if(pi.name.equals("UTS"))
						{
							onlretuts = Integer.parseInt(obj1.getProperty(j).toString());
							onluts = onluts+onlretuts;							
						}
						
						if(pi.name.equals("Can"))
						{
							onlretcan = Integer.parseInt(obj1.getProperty(j).toString());
							onlcan = onlcan+onlretcan;							
						}
						
						if(pi.name.equals("Completed"))
						{
							onlretrr = Integer.parseInt(obj1.getProperty(j).toString());
							onlrr = onlrr+onlretrr;							
						}
						
						if(pi.name.equals("Total"))
						{
							onlrettotal = Integer.parseInt(obj1.getProperty(j).toString());
							onltotal = onltotal+onlrettotal;							
						}
						//System.out.println("pi.fnal"+onlassign+" "+onlsch+" "+onlcio+" "+onluts+" "+onlcan+" "+onlrr+" "+onltotal);
					}
				}
			}
		}
	}
	 public boolean onKeyDown(int keyCode, KeyEvent event) {
			// replaces the default 'Back' button action
			if (keyCode == KeyEvent.KEYCODE_BACK) {
				Intent loginpage = new Intent(Dashboard.this,HomeScreen.class);
				loginpage.putExtra("back", "exit");
				startActivity(loginpage);
				return true;
			}
			if (keyCode == KeyEvent.KEYCODE_MENU) {

			}
			return super.onKeyDown(keyCode, event);
		}

}