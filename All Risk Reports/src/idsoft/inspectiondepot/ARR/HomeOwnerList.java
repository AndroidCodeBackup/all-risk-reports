package idsoft.inspectiondepot.ARR;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;

import idsoft.inspectiondepot.ARR.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

public class HomeOwnerList extends Activity{
	CommonFunctions cf;
	CommonDeleteInsp del;
	String colomnname="ARR_PH_Status",inspectioname="",headernote="",statusofdata="40",res="";			
	int[] colors = new int[] { 0x30FF0000, 000000 };	
	LinearLayout onlinspectionlist;
	EditText search_text;
	String[] data,countarr;
	int rws;
	 
	 @Override
     public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        
	        cf = new CommonFunctions(this);
	        del = new CommonDeleteInsp(this); 
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.onlinspectionid = extras.getString("InspectionType");
				cf.onlstatus = extras.getString("status");
		 	}
			
			if ("Assign".equals(cf.onlstatus)) {
				colomnname="ARR_PH_Status";
				statusofdata = "30";
			}
			if ("Schedule".equals(cf.onlstatus)) {
				colomnname="ARR_PH_Status";
				statusofdata = "40";
		
			}
			if ("CIT".equals(cf.onlstatus)) {
				cf.onlstatus="Completed Inspection in Tablet";
				colomnname="ARR_PH_IsInspected";
				statusofdata = "1";
		
			}
	        setContentView(R.layout.inspectionlist);
	        cf.getInspectorId();
	        cf.CreateARRTable(3);
	        cf.CreateARRTable(2);
	        cf.CreateARRTable(21);
	
			TextView note  = (TextView) findViewById(R.id.note);
			
			if("Assign".equals(cf.onlstatus))
			{
				headernote="POLICY HOLDER NAME | POLICY NUMBER | ADDRESS | CITY | STATE | COUNTY | ZIP | INSPECTION NAME";
			}
			else
			{
				headernote="POLICY HOLDER NAME | POLICY NUMBER | ADDRESS | CITY | STATE | COUNTY | ZIP | INSPECTION DATE | INSPECTION START TIME  - END TIME | INSPECTION NAME | STORIES | BUILDING NUMBER";
			}
			
			
			((TextView) findViewById(R.id.status)).setText(cf.onlstatus);			
			((TextView) findViewById(R.id.inspectiontype)).setText(cf.strret);
			((TextView) findViewById(R.id.note)).setText(headernote);
			
			onlinspectionlist = (LinearLayout) findViewById(R.id.linlayoutdyn);
			search_text = (EditText)findViewById(R.id.search_text);
			dbquery();
			
			ImageView im =(ImageView) findViewById(R.id.head_insp_info);
			im.setVisibility(View.VISIBLE);
			im.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					    Intent s2= new Intent(HomeOwnerList.this,PolicyholdeInfoHead.class);
						Bundle b2 = new Bundle();
						s2.putExtra("homeid", cf.selectedhomeid);
						s2.putExtra("Type", "Inspector");
						s2.putExtra("insp_id", cf.Insp_id);
						((Activity) cf.con).startActivityForResult(s2, cf.info_requestcode);
				}
			});
	 }
	
	 public void clicker(View v)
	 {
		  switch(v.getId())
		  {
		  case R.id.search:
			  String temp = cf.encode(search_text.getText().toString());
				if (temp.equals("")) 
				{
					cf.ShowToast("Please enter the Name or Policy Number to search.", 0);
					search_text.requestFocus();
				} 
				else 
				{
					res = temp;
					dbquery();
					
				}			  
			  break;
		  case R.id.clear:
			  	search_text.setText("");
				res = "";
				dbquery(); 
			  break;
			  
		  case R.id.home:
			  cf.gohome();
			  break;
		  case R.id.deleteall:
			  String temp1="(";
			  Cursor c=cf.arr_db.rawQuery(" Select * from "+cf.policyholder+" where "+colomnname+" ="+statusofdata+" and ARR_PH_InspectorId='"+cf.Insp_id+"' and ARR_PH_SubStatus<>41",null);
			  if( c.getCount()>=1)
			 {
				 c.moveToFirst();
				 for(int i=0;i<c.getCount();i++)
				 {
					 temp1+="'"+c.getString(c.getColumnIndex("ARR_PH_SRID"))+"'";
					 if((i+1)==(c.getCount()))
					 {
						 temp1+=")";
					 }
					 else
					 {
						 temp1+=",";
						 c.moveToNext();
					 }
				 }
				
				 del.delinsp_bystatus(temp1);
			 }else
			 {
				 cf.ShowToast("Record unavailable", 0);
			 }
			  
			  break;
		  }
	 }
	 private void dbquery() {
		
		 String sql="";
			data = null;
			countarr = null;
			rws = 0;
			try		
			{
				
				sql = "select * from " + cf.policyholder
						+ " where ARR_PH_Status='" + cf.encode(statusofdata) + "' and ARR_PH_InspectorId = '"
						+ cf.encode(cf.Insp_id) + "' and ARR_PH_SubStatus!=41";
				/**Quesr y for excute the search text **/
				if (!res.trim().equals("")) {
					sql += " and (ARR_PH_FirstName like '%" + cf.encode(res)
							+ "%' or ARR_PH_LastName like '%" + cf.encode(res)
							+ "%' or ARR_PH_Policyno like '%" + cf.encode(res) + "%' ) ";
					if (cf.onlstatus.equals("CIT")) 
					{
						sql += " and ARR_PH_IsInspected=1 ";
					}
				} /**Quesr y for excute the search text ends **/
				else if (cf.onlstatus.equals("CIT")) 
				{
					sql += " and  ARR_PH_IsInspected=1 ";
				}
				sql += " order by ARR_Schedule_ScheduledDate DESC, ARR_Schedule_InspectionStartTime DESC";
				
				Cursor cur = cf.arr_db.rawQuery(sql, null);
				
			
				System.out.println("sql="+sql+"Co"+cur.getCount());
				if (cur.getCount() >= 1) {
					rws = cur.getCount();
					((TextView) findViewById(R.id.noofrecords)).setText("No of Records : "+rws);
					((LinearLayout) findViewById(R.id.dynamiclayout)).setVisibility(cf.v1.VISIBLE);
					((TextView) findViewById(R.id.norecordstxt)).setVisibility(cf.v1.GONE);
					
					
					
					data = new String[rws];
					countarr = new String[rws];
					int j = 0;
					cur.moveToFirst();
					
					do {
						String dbinspid = cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_InspectorId")));
						String dbinsptypeid = cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_InspectionTypeId")));
						String scheduleddate = cf.decode(cur.getString(cur.getColumnIndex("ARR_Schedule_ScheduledDate")));
					
						if (cf.Insp_id.equals(dbinspid)) 
						//if (cf.Insp_id.equals(dbinspid)	&& cf.onlinspectionid.equals(dbinsptypeid)) 
						{
							
							if (cur.getString(cur.getColumnIndex(this.colomnname)).equals(statusofdata))
							{
								cf.selinspname="";
								String s =(cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_FirstName"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_FirstName")));
								data[j] =  s+ " ";
								s=(cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_LastName"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_LastName")));
								this.data[j] += s + " | ";
								s=(cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_Policyno"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_Policyno")));
								this.data[j] += s+ " | ";
								s=(cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_Address1"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_Address1")));
								this.data[j]+= s + " | ";
								s=(cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_City"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_City")));
								this.data[j] += s + " | ";
								s=(cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_State"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_State")));
								this.data[j] += s + " | ";
								s=(cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_County"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_County")));
								this.data[j] += s + " | ";
								s=(cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_Zip"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_Zip")));
								this.data[j] += s+ " | ";
								
								if(!cf.onlstatus.equals("Assign")) 
								{
									s=(cur.getString(cur.getColumnIndex("ARR_Schedule_ScheduledDate"))).trim().equals("") ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("ARR_Schedule_ScheduledDate")));
									this.data[j] += s + " | ";
									s=(cur.getString(cur.getColumnIndex("ARR_Schedule_InspectionStartTime"))).trim().equals("") ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("ARR_Schedule_InspectionStartTime")));
									this.data[j] += s + " - ";
									s=(cur.getString(cur.getColumnIndex("ARR_Schedule_InspectionEndTime"))).trim().equals("") ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("ARR_Schedule_InspectionEndTime")));
									this.data[j] += s +" | ";
								}
								
								

								
								cf.getinspectioname(cur.getString(cur.getColumnIndex("ARR_PH_SRID")));System.out.println("cf.selinspname"+cf.selinspname);
								String insptext ="";
								if(!cf.selinspname.equals(""))
								{
									String inspid[]= cf.selinspname.split(",");
									for(int i=0;i<inspid.length;i++)
							    	{
										Cursor c =cf.SelectTablefunction(cf.allinspnamelist, " Where ARR_Insp_ID='"+inspid[i]+"'");
							    		if(c.getCount()>0)
							    	    {
							    	   		 c.moveToFirst();
							    	   		insptext += cf.decode(c.getString(c.getColumnIndex("ARR_Insp_Name"))).trim()+","; 
							    	    }
							    	 }	
										if(insptext.trim().length()>1)
										{
											insptext = insptext.substring(0, insptext.length() - 1);	
										}								
									this.data[j] += insptext+" | " ;
								}
								
								
								s=(cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_NOOFSTORIES"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_NOOFSTORIES")));
								this.data[j] += s+ " | ";
								
								if(cf.decode(cur.getString(cur.getColumnIndex("Commercial_Flag"))).equals("0"))
								{
									s=(cf.decode(cur.getString(cur.getColumnIndex("fld_noofbuildings"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("fld_noofbuildings")));
									this.data[j] += s;
								}
								else
								{
									cf.CreateARRTable(70);
									Cursor curbuilding= cf.SelectTablefunction(cf.Comm_Building, " where fld_srid='"+cur.getString(cur.getColumnIndex("ARR_PH_SRID"))+"'");
									if(curbuilding.getCount()==0)
									{
										s=(cf.decode(cur.getString(cur.getColumnIndex("fld_noofbuildings"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("fld_noofbuildings")));
										this.data[j] += "1 of "+ s;
									}
									else
									{
										curbuilding.moveToFirst();
										
										
										s=(cf.decode(cur.getString(cur.getColumnIndex("fld_noofbuildings"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("fld_noofbuildings")));
										this.data[j] += curbuilding.getString(curbuilding.getColumnIndex("BUILDING_NO"))+" of "+ s;
									}
								}
								
								
								
								countarr[j] = cur.getString(cur.getColumnIndex("ARR_PH_SRID"));
								
								
								if (data[j].contains("null")) {
									data[j] = data[j].replace("null", "");
								}
								
								j++;
							}
						}
					} while (cur.moveToNext());
					search_text.setText("");
					display();
					cf.hidekeyboard();
				} else {
					onlinspectionlist.removeAllViews();
					
					((LinearLayout) findViewById(R.id.dynamiclayout)).setVisibility(cf.v1.GONE);
					((TextView) findViewById(R.id.norecordstxt)).setVisibility(cf.v1.VISIBLE);
					if(res.equals(""))
					{
						cf.gohome();
					}
					else
					{
						cf.ShowToast("Sorry! No results found.", 0);
						cf.hidekeyboard();
					}	
				}
				
			}catch(Exception e)
			
			{
				System.out.println("EE "+e.getMessage());	
			}
			
		}
     

	private void display() {
	
			onlinspectionlist.removeAllViews();
			ScrollView sv = new ScrollView(this);
			TextView[] tvstatus;
			Button [] deletebtn;
			onlinspectionlist.addView(sv);
			
			final LinearLayout l1 = new LinearLayout(this);
			l1.setOrientation(LinearLayout.VERTICAL);
			sv.addView(l1);
			
			View v = new View(this);
			v.setBackgroundResource(R.color.black);
			l1.addView(v,LayoutParams.FILL_PARENT,1);
				 
			if (data.length>=1) {				
				for (int i = 0; i < data.length; i++) {					
					tvstatus = new TextView[rws];
					deletebtn = new Button[rws];
					LinearLayout l2 = new LinearLayout(this);
					LinearLayout.LayoutParams mainparamschk = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
					
					l2.setLayoutParams(mainparamschk);
					l2.setOrientation(LinearLayout.HORIZONTAL);
					l1.addView(l2);
					
					LinearLayout lchkbox = new LinearLayout(this);
					LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
					paramschk.topMargin = 8;
					paramschk.leftMargin = 20;
					paramschk.bottomMargin = 10;
					l2.addView(lchkbox);
					
					tvstatus[i] = new TextView(this);
					tvstatus[i].setTag("textbtn" + i);

					tvstatus[i].setWidth(700);
					tvstatus[i].setText(data[i]);
					
					tvstatus[i].setTextColor(Color.WHITE);
					tvstatus[i].setTextSize(TypedValue.COMPLEX_UNIT_SP,14);
					
				    lchkbox.addView(tvstatus[i], paramschk);
				    LinearLayout ldelbtn = new LinearLayout(this);
					LinearLayout.LayoutParams paramsdelbtn = new LinearLayout.LayoutParams(
					ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
			        paramsdelbtn.setMargins(0, 10, 10, 0); //left, top, right, bottom
					ldelbtn.setLayoutParams(mainparamschk);
					ldelbtn.setGravity(Gravity.RIGHT);
				    l2.addView(ldelbtn);
				    deletebtn[i] = new Button(this);
					deletebtn[i].setBackgroundResource(R.drawable.deletebtn1);
					deletebtn[i].setTag("deletebtn" + i);
					deletebtn[i].setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
					deletebtn[i].setPadding(30, 0, 0, 0);
					ldelbtn.addView(deletebtn[i], paramsdelbtn);
					
					tvstatus[i].setOnClickListener(new View.OnClickListener() {

						public void onClick(final View v) {
							String getidofselbtn = v.getTag().toString();
							final String repidofselbtn = getidofselbtn.replace("textbtn", "");
							final int s = Integer.parseInt(repidofselbtn);

							String select = countarr[s];
							cf.selectedhomeid = select.toString();
							String ScheduleDate;
							if (cf.onlstatus.equals("Assign")) {
								try {
									Cursor cur = cf.arr_db.rawQuery("select * from "+ cf.policyholder + " where ARR_PH_SRID='" + cf.encode(cf.selectedhomeid) + "'",
											null);
									cur.moveToFirst();
									if (cur != null) {
										do {
											ScheduleDate = cf.decode(cur.getString(cur.getColumnIndex("ARR_Schedule_ScheduledDate")));
										if (ScheduleDate.equals("Null")
													|| ScheduleDate
															.equals("Not Available")
													|| ScheduleDate.equals("")
													|| ScheduleDate.equals("N/A")) 
											{
												cf.nextscreen();
											}
											else 
											{
												if(cf.onlstatus.equals("Completed Inspection in Tablet"))
												{
													cf.onlstatus="CIT";
												}
												cf.nextscreen();
											}
										} while (cur.moveToNext());
									}
								} catch (Exception e) {
								}
							} else {
								
								if(cf.onlstatus.equals("Completed Inspection in Tablet"))
								{
									cf.onlstatus="CIT";
								}
								cf.nextscreen();
								
							}
								
						}
					});

					deletebtn[i].setOnClickListener(new View.OnClickListener() {
						public void onClick(final View v) {
							String getidofselbtn = v.getTag().toString();
							final String repidofselbtn = getidofselbtn.replace("deletebtn", "");
							final int cvrtstr = Integer.parseInt(repidofselbtn);
							final String dt = countarr[cvrtstr];
							 final Dialog dialog1 = new Dialog(HomeOwnerList.this,android.R.style.Theme_Translucent_NoTitleBar);
								dialog1.getWindow().setContentView(R.layout.alertsync);
								TextView txttitle = (TextView) dialog1.findViewById(R.id.txthelp);
								txttitle.setText("Confirmation");
								TextView txt = (TextView) dialog1.findViewById(R.id.txtid);
								txt.setText(Html.fromHtml("Do you want to delete this inspection?"));
								Button btn_yes = (Button) dialog1.findViewById(R.id.yes);
								Button btn_cancel = (Button) dialog1.findViewById(R.id.cancel);
								btn_yes.setOnClickListener(new OnClickListener()
								{
									@Override
									public void onClick(View v) {
										// TODO Auto-generated method stub
										dialog1.dismiss();
										del.delinsp_byrecord(dt);
										dbquery();				
									}
								});
								btn_cancel.setOnClickListener(new OnClickListener()
								{
									public void onClick(View arg0) {
										// TODO Auto-generated method stub
										dialog1.setCancelable(true);
										dialog1.dismiss();
									}			
								});
								dialog1.setCancelable(false);		
								dialog1.show();
						}
					});
					
					 if (i % 2 == 0) {
						 l2.setBackgroundColor(Color.parseColor("#13456d"));
					    	} else {
					    	l2.setBackgroundColor(Color.parseColor("#386588"));
					    	}
					if(data.length!=(i+1))
					{
						 View v1 = new View(this);
						 v1.setBackgroundResource(R.color.white);
						 l1.addView(v1,LayoutParams.FILL_PARENT,1);	 
					}
				}
				View v2 = new View(this);
				 v2.setBackgroundResource(R.color.black);
				 onlinspectionlist.addView(v2,LayoutParams.FILL_PARENT,1);
			}

		}

     /***need to add gowri*/
	 protected void gotoselctinsp() {
			// TODO Auto-generated method stub
			 cf.CreateARRTable(2);
			 try
			 {
			    Intent intimg = new Intent(HomeOwnerList.this,SelectInspections.class);
				cf.putExtraIntent(intimg);
				startActivity(intimg);
			}
		    catch (Exception E)
			{
				String strerrorlog="Retrieving Inspectiontype data - Select Inspection table";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			}
			    
		}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
			if (keyCode == KeyEvent.KEYCODE_BACK) {
				startActivity(new Intent(HomeOwnerList.this,Dashboard.class));
				return true;
			}
			return super.onKeyDown(keyCode, event);
		}
}