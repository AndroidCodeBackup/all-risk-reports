/*
************************************************************
* Project: ALL RISK REPORT
* Module : WindMitCommareas.java
* Creation History:
Created By : Nalini on 10/24/2012
* Modification History:
Last Modified By : Gowri on 04/04/2013
************************************************************ 
*/

package idsoft.inspectiondepot.ARR;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.CommercialRD.RDrdioclicker;
import idsoft.inspectiondepot.ARR.CommercialSWR.SWRrdioclicker;
import idsoft.inspectiondepot.ARR.WindMitCommAuxbuildings.Touch_Listener;
import idsoft.inspectiondepot.ARR.WindMitCommAuxbuildings.textwatcher;
import idsoft.inspectiondepot.ARR.WindMitCommRestsupp.RestSupp_EditClick;
import idsoft.inspectiondepot.ARR.WindMitCommRestsupp.Spin_Selectedlistener;
import idsoft.inspectiondepot.ARR.WindMitCommRestsupp.checklistenetr;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class CommercialOP extends Activity{
	CommonFunctions cf;
	String c1_q4[] = {"Class A (Hurricane Impact)","Class B (Basic Impact)","Not Applicable","Not Inspected","Unknown"};
	String opvalue="",opAvalue="",openprotvalue="",subvalue="",tmp="";
	RadioButton oprdio[] = new RadioButton[5];
	int[] oprdioid = {R.id.openprotrdio1,R.id.openprotrdio2,R.id.openprotrdio3,R.id.openprotrdio4,R.id.openprotrdio5};
	
	CheckBox opsubchk[] = new CheckBox[4];
	int[] opchkid = {R.id.openA1,R.id.openA2,R.id.openA3,R.id.openA4};
	
	boolean load_comment=true;
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cf=new CommonFunctions(this);	 
        Bundle extras = getIntent().getExtras();
  		if (extras != null) {
  			cf.getExtras(extras);				
  	 	}
  		  setContentView(R.layout.commwindopeningprot);
          cf.getDeviceDimensions();
          
          if(cf.identityval==32){ cf.typeidentity=309;}
          else if(cf.identityval==33){cf.typeidentity=310;}
          else if(cf.identityval==34){cf.typeidentity=310;}
         
          
          LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
          if(cf.identityval==33)
          {
            hdr_layout.addView(new HdrOnclickListener(this,1,"Commercial Type II","Commercial Wind","Opening Protection",3,1,cf));
          }
          else if(cf.identityval==34)
          {
        	  hdr_layout.addView(new HdrOnclickListener(this,1,"Commercial Type III","Commercial Wind","Opening Protection",3,1,cf));
          }
          
          cf.mainmenu_layout = (LinearLayout) findViewById(R.id.header); //** HEADER MENU **//
          cf.mainmenu_layout.setMinimumWidth(cf.wd);
          cf.mainmenu_layout.addView(new MyOnclickListener(this, 3, 0,0, cf));
         
          cf.type_submenulayout =(LinearLayout) findViewById(R.id.typesubmenu); //** WINDMIT TYPE SUBMENU **//
          cf.type_submenulayout.addView(new MyOnclickListener(this, 300, 1,0, cf));
          cf.windmittypechk(cf.identityval,((RadioButton)findViewById(R.id.b1802_chk)),((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((TextView)findViewById(R.id.commtxt)),((ImageView)findViewById(R.id.editbno))); // To check the checkbox of choosen inspection type
          cf.uncheckcommercial(((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((RadioButton)findViewById(R.id.b1802_chk))); 
      	     cf.submenu_layout = (LinearLayout) findViewById(R.id.submenu);//** WINDMIT TYPE SUBMENU **//
          cf.submenu_layout.addView(new MyOnclickListener(this,cf.identityval,1,314, cf));
          
          cf.innersubmenu_layout = (LinearLayout) findViewById(R.id.innersubmenu);//** WINDMIT TYPE SUBMENU **//
          cf.innersubmenu_layout.addView(new MyOnclickListener(this,cf.identityval,1,127, cf));
          
          cf.mainlayout = (LinearLayout)findViewById(R.id.bc_table);
          cf.save = (Button)findViewById(R.id.save);
          cf.tblrw = (TableRow)findViewById(R.id.row2);
          cf.tblrw.setMinimumHeight(cf.ht);			
          Declaration(); cf.CreateARRTable(40);setValue();
    }
	private void setValue() {
		// TODO Auto-generated method stub		
		try
		{
			Cursor c1 = cf.arr_db.rawQuery("SELECT * FROM " + cf.Commercial_Wind + " WHERE fld_srid='"+ cf.selectedhomeid + "'  and insp_typeid='"+cf.identityval+"'", null);				
			if(c1.getCount()>0)
			{
				c1.moveToFirst();
				String openprotvalue = c1.getString(c1.getColumnIndex("fld_openingprotection"));
				if(!openprotvalue.equals(""))
				{
					String opspilt[] = openprotvalue.split("~");
					
					 if(opspilt[0].equals("A"))
					 {
						((LinearLayout)findViewById(R.id.openprotAlin)).setVisibility(cf.v1.VISIBLE);
						((TextView)findViewById(R.id.txt1)).setVisibility(cf.v1.VISIBLE);
						cf.setvaluechk1(opspilt[1], opsubchk,((EditText)findViewById(R.id.dummyid)));
					 }
					 else if(opspilt[0].equals("B"))
					 {
						 ((LinearLayout)findViewById(R.id.openprotBlin)).setVisibility(cf.v1.VISIBLE);
						 ((TextView)findViewById(R.id.txt2)).setVisibility(cf.v1.VISIBLE);
						 ((CheckBox)findViewById(R.id.openB1)).setChecked(true);
					 }
					   if(!String.valueOf(opspilt[0]).equals(""))
						{
							for(int i=0;i<oprdio.length;i++)
							{
								if(String.valueOf(opspilt[0]).equals(oprdio[i].getTag().toString()))
								{
									oprdio[i].setChecked(true);
								
								}
							}
						}
					   ((TextView)findViewById(R.id.savetxtop)).setVisibility(cf.v1.VISIBLE);
				}
				else
				{
					((TextView)findViewById(R.id.savetxtop)).setVisibility(cf.v1.GONE);
				}
				   String openprotcomments = cf.decode(c1.getString(c1.getColumnIndex("fld_opcomments")));
				   if(!openprotcomments.equals(""))
				   {
					   ((EditText)findViewById(R.id.opcomment)).setText(openprotcomments);
					   //((TextView)findViewById(R.id.savetxtcomm)).setVisibility(cf.v1.VISIBLE);
				   }
				   else
				   {
					  // ((TextView)findViewById(R.id.savetxtcomm)).setVisibility(cf.v1.GONE);					   
				   }
				   
			}
			else
			{
				((TextView)findViewById(R.id.savetxtop)).setVisibility(cf.v1.GONE);
			}
			
		}catch(Exception e)
		{
			System.out.println("catch 1"+e.getMessage());
		}
	}
	private void Declaration() {
		// TODO Auto-generated method stub
		cf.setupUI((ScrollView) findViewById(R.id.scr));
		 for(int i=0;i<oprdio.length;i++)
	   	 {
			 oprdio[i] = (RadioButton)findViewById(oprdioid[i]); 	
			 oprdio[i].setOnClickListener(new OPrdioclicker());	   
	   	 }	
		 for(int i=0;i<opsubchk.length;i++)
	   	 {
			 opsubchk[i] = (CheckBox)findViewById(opchkid[i]); 
	   	 }	
		 ((EditText)findViewById(R.id.opcomment)).addTextChangedListener(new textwatcher());
		 
		 
		 ((RadioButton)findViewById(R.id.openprotrdio1)).setText(Html.fromHtml("<b>Class A (Hurricane Impact)</b>" + " -- All glazed openings (windows, skylights, sliding glass doors, doors with windows, etc.) less than 30 feet above grade must be protected with impact resistant coverings (e.g. shutters), impact resistant doors, and/or impact resistant glazing that meet the Small Missile ( 9 lb.) impact requirements of: "));
		 ((RadioButton)findViewById(R.id.openprotrdio2)).setText(Html.fromHtml("<b>Class B (Basic Impact)</b>"+" -- All glazed openings (windows, skylights, sliding glass doors, doors with windows, etc.) less than 30 feet above grade must be protected with impact resistant coverings (e.g. shutters), impact resistant doors, and/or impact resistant glazing that meet the Large Missile ( 4.5 lb.) impact requirements of "));
	}
	class textwatcher implements TextWatcher
    {
	         public int type;
	         textwatcher()
	    	{
	        	
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {


	    		// TODO Auto-generated method stub
	    		cf.showing_limit(s.toString(),(TextView)findViewById(R.id.op_tv_type1),500); 
	    		
		    		if(s.toString().startsWith(" "))
					{
		    			((EditText)findViewById(R.id.opcomment)).setText("");
					}
		
	    		}

	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,

	    			int after) { 
	    		// TODO Auto-generated method stub
	     	}

	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,

	    			int count) {
	    		// TODO Auto-generated method stub
	    	}
	   }	
	class  OPrdioclicker implements OnClickListener
	{
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			for(int i=0;i<oprdio.length;i++)
			{
				if(v.getId()==oprdio[i].getId())
				{
					oprdio[i].setChecked(true);
					if(v.getId()==oprdio[0].getId())
					{
						((LinearLayout)findViewById(R.id.openprotAlin)).setVisibility(cf.v1.VISIBLE);
						((LinearLayout)findViewById(R.id.openprotBlin)).setVisibility(cf.v1.GONE);
						((TextView)findViewById(R.id.txt1)).setVisibility(cf.v1.VISIBLE);
						((CheckBox)findViewById(R.id.openB1)).setChecked(false);
					}
					else if(v.getId()==oprdio[1].getId())
					{
						((LinearLayout)findViewById(R.id.openprotAlin)).setVisibility(cf.v1.GONE);
						((LinearLayout)findViewById(R.id.openprotBlin)).setVisibility(cf.v1.VISIBLE);
						((TextView)findViewById(R.id.txt2)).setVisibility(cf.v1.VISIBLE);
						for(int k=0;k<opchkid.length;k++)
						{
							opsubchk[k].setChecked(false);
						}
					}
					else
					{
						((LinearLayout)findViewById(R.id.openprotAlin)).setVisibility(cf.v1.GONE);
						((LinearLayout)findViewById(R.id.openprotBlin)).setVisibility(cf.v1.GONE);
						((TextView)findViewById(R.id.txt1)).setVisibility(cf.v1.GONE);
						((TextView)findViewById(R.id.txt2)).setVisibility(cf.v1.GONE);
					}
				}
				else
				{
					oprdio[i].setChecked(false);
				}
			}
		}
	}
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.loadcomments:
			/***Call for the comments***/
			cf.findinspectionname(cf.identityval);OP_suboption();
			int len=((EditText)findViewById(R.id.opcomment)).getText().toString().length();			
			if(!tmp.equals(""))
			{
					if(load_comment)
					{
						load_comment=false;
						int loc1[] = new int[2];
						v.getLocationOnScreen(loc1);
						Intent in = cf.loadComments(tmp,loc1);
						in.putExtra("insp_name", cf.inspname);
						in.putExtra("insp_ques", "Opening Protection");
						in.putExtra("length", len);
						in.putExtra("max_length", 500);
						startActivityForResult(in, cf.loadcomment_code);
					}
			}
			else
			{
				cf.ShowToast("Please select the option for Opening Protection." , 0);
			}	
			break;
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.save:
		
				opvalue=cf.getselecctedtag_radio(oprdio);	
				if(opvalue.equals(""))
				{
					cf.ShowToast("Please select the option for Opening Protection." , 0);
				}
				else
				{
					if(oprdio[0].isChecked())
					{
						opAvalue=cf.getselected_chk(opsubchk);
						if(opAvalue.equals(""))
						{
							cf.ShowToast("Please check atleast one option under Class A (Hurricane Impact).", 1);
						}
						else
						{
							InsertOpeningProt();
						}
					}
					else if(oprdio[1].isChecked())
					{
						if(((CheckBox)findViewById(R.id.openB1)).isChecked())
						{
							InsertOpeningProt();
						}
						else
						{
							cf.ShowToast("Please check atleast one option under Class B (Basic Impact).", 1);
						}
					}
					else
					{
						InsertOpeningProt();
					}
				}
			
			break;
		}
	}
	private void OP_suboption() {
		// TODO Auto-generated method stub
		 for(int i=0;i<oprdio.length;i++)
    	 {
			 if(oprdio[i].isChecked())
			 {
				tmp = c1_q4[i];
			 }
    	 }
		
	}
	private void InsertOpeningProt() {
		// TODO Auto-generated method stub
		if(((EditText)findViewById(R.id.opcomment)).getText().toString().trim().equals(""))
		{
			cf.ShowToast("Please enter the Opening Protection Comments.",1);
			cf.setFocus(((EditText)findViewById(R.id.opcomment)));
		}
		else
		{
			if(oprdio[0].isChecked()){
				subvalue=opAvalue;
			}
			else
			{
				subvalue=((CheckBox)findViewById(R.id.openB1)).getText().toString();
			}
			
			openprotvalue = opvalue+"~"+subvalue;
			try
			{
				Cursor c1 =cf.SelectTablefunction(cf.Commercial_Wind, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
				if(c1.getCount()==0)
				{
					cf.arr_db.execSQL("INSERT INTO "
							+ cf.Commercial_Wind
							+ " (fld_srid,insp_typeid,fld_openingprotection,fld_opcomments)"
							+ "VALUES ('"+cf.selectedhomeid+"','"+cf.identityval+"','"+openprotvalue+"','"+cf.encode(((EditText)findViewById(R.id.opcomment)).getText().toString().trim())+"')");
				}
				else
				{
					cf.arr_db.execSQL("UPDATE "
						+ cf.Commercial_Wind
						+ " SET fld_openingprotection='" + openprotvalue + "', fld_opcomments='"+cf.encode(((EditText)findViewById(R.id.opcomment)).getText().toString().trim())+"'"
						+ " WHERE fld_srid ='"+ cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'");			
				
				}
				cf.ShowToast("Opening Protection saved successfully",1);
				cf.gotoclass(cf.identityval, Certification.class);
			}
			catch(Exception e)
			{
				System.out.println("catch inside "+e.getMessage());
			}
		}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
 		// replaces the default 'Back' button action
 		if (keyCode == KeyEvent.KEYCODE_BACK) {
 			cf.gotoclass(cf.identityval, CommercialSWR.class);
 			return true;
 		}
 		
 		return super.onKeyDown(keyCode, event);
 	}
	 /**on activity result for the load comments**/
	 @Override
	 		protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	 		try
	 		{
	 			if(requestCode==cf.loadcomment_code)
	 			{
	 				load_comment=true;
	 				if(resultCode==RESULT_OK)
	 				{
	 					String opcomm = ((EditText)findViewById(R.id.opcomment)).getText().toString();	 
	 					((EditText)findViewById(R.id.opcomment)).setText(opcomm +" "+data.getExtras().getString("Comments"));
		 			}
	 			}
	 		}
	 		catch (Exception e) {
	 			// TODO: handle exception
	 			System.out.println("the erro was "+e.getMessage());
	 		}
	 	}/**on activity result for the load comments ends**/
}