/*
   ************************************************************
   * Project: ALL RISK REPORT
   * Module : GCHComments.java
   * Creation History:
      Created By : Gowri on 
   * Modification History:
      Last Modified By : Gowri on 04/04/2013
   ************************************************************  
*/
package idsoft.inspectiondepot.ARR;
import idsoft.inspectiondepot.ARR.Observation4.textwatcher;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Addendum extends Activity{
	CommonFunctions cf;
	boolean load_comment=true;
	@Override
	   public void onCreate(Bundle savedInstanceState) {

	        super.onCreate(savedInstanceState);
	        cf=new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);
		 	}
	        setContentView(R.layout.fourcomments);
	        cf.getDeviceDimensions();
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
			hdr_layout.addView(new HdrOnclickListener(this,1,"Four Point Inspection","Addendum",4,0,cf));
	        LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
	        mainmenu_layout.setMinimumWidth(cf.wd);
	        mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 2, 0,0,cf));
	        LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 26, 1,0,cf));
			TableRow tblrw = (TableRow)findViewById(R.id.row2);
			tblrw.setMinimumHeight(cf.ht);
			((EditText)findViewById(R.id.fourcomment)).addTextChangedListener(new textwatcher(1));
			cf.CreateARRTable(50);
			try
			{
			   Cursor c1=cf.SelectTablefunction(cf.Addendum, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='1'");
			   if(c1.getCount()>0)
			   {  
				   c1.moveToFirst();
				  // ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
				   ((EditText)findViewById(R.id.fourcomment)).setText(cf.decode(c1.getString(c1.getColumnIndex("addendumcomment")))); 
			   }
			   else
			   {
				   //((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.GONE);
				   ((EditText)findViewById(R.id.fourcomment)).setText("No Comments.");
			   }
			}
		    catch (Exception E)
			{
				String strerrorlog="Addendum COMMETS - Fourpoint";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			}
	}
	 class textwatcher implements TextWatcher


	    {
	         public int type;
	         textwatcher(int type)
	    	{
	    		this.type=type;
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {


	    		// TODO Auto-generated method stub
	    		if(this.type==1)
	    		{
	    			
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.addencom_tv_type1),500);
	    			if(s.toString().startsWith(" "))
	    			{
	    				((EditText)findViewById(R.id.fourcomment)).setText("");
	    			}
	    		}
	    	
	    		
	    	}

	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,

	    			int after) {
	    		// TODO Auto-generated method stub
	     	}

	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,

	    			int count) {
	    		// TODO Auto-generated method stub
	    	}
	   }	
	public void clicker(View v)
	{
		switch(v.getId())
		{
		case R.id.loadcomments:
			/***Call for the comments***/
			 int len=((EditText)findViewById(R.id.fourcomment)).getText().toString().length();
			 System.out.println("load_comment"+load_comment+"length="+len);
			 
			 
			 if(load_comment )
				{
					load_comment=false;
					int loc[] = new int[2];
					v.getLocationOnScreen(loc);
					
					Intent in1 = cf.loadComments("Addendum Comments",loc);
					in1.putExtra("insp_ques","Addendum");
					in1.putExtra("length", len);
					in1.putExtra("max_length", 500);
					startActivityForResult(in1, cf.loadcomment_code);
				}
			
			 break;
		case R.id.hme:
			cf.gohome();
				break;
		
		case R.id.save:
			if(((EditText)findViewById(R.id.fourcomment)).getText().toString().trim().equals(""))
			{
				cf.ShowToast("Please enter the Addendum Comments.", 0);
			}
			else
			{
				Cursor c1=null;
				try
				{
					c1=cf.SelectTablefunction(cf.Addendum, " where fld_srid='"+cf.selectedhomeid+"' and  insp_typeid='1'");
					if(c1.getCount()>0)
					{
						try
						{
							cf.arr_db.execSQL("UPDATE "+cf.Addendum+ " set addendumcomment ='"+cf.encode(((EditText)findViewById(R.id.fourcomment)).getText().toString())+"' where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='1'");
							cf.ShowToast("Addendum Comments updated successfully.", 1);
						    //((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
						   
						}
						catch (Exception E)
						{
							String strerrorlog="Updating the COMMENTS - GCH";
							cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
						}
					}
					else
					{
						try
						{
							cf.arr_db.execSQL("INSERT INTO "
								+ cf.Addendum
								+ " (fld_srid,insp_typeid,addendumcomment)"
								+ "VALUES ('"+cf.selectedhomeid+"','1','"+cf.encode(((EditText)findViewById(R.id.fourcomment)).getText().toString())+"')");

						 cf.ShowToast("Addendum Comments saved successfully.", 1);
						// ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE); 
						
						}
						catch (Exception E)
						{
							String strerrorlog="Inserting the COMMENTS- GCH";
							cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
						}
					}
				}
				catch (Exception E)
				{
						String strerrorlog="Checking the rows inserted in the Addendum COMMENTS table.";
						cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
			}
			break;
		}
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	
		  if(requestCode==cf.loadcomment_code)
		  {
			  load_comment=true;
			if(resultCode==RESULT_OK)
			{
				cf.ShowToast("Comments added successfully.", 0);
				 String gchcomts = ((EditText)findViewById(R.id.fourcomment)).getText().toString();
				 ((EditText)findViewById(R.id.fourcomment)).setText((gchcomts+" "+data.getExtras().getString("Comments")).trim());
				
			}
			
		}

	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
 		// replaces the default 'Back' button action
 		if (keyCode == KeyEvent.KEYCODE_BACK) {
 			cf.goclass(25);
 			return true;
 		}
 		
 		return super.onKeyDown(keyCode, event);
 	}
}
