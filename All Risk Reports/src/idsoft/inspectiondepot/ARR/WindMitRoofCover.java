/*
************************************************************
* Project: ALL RISK REPORT
* Module : WindMitRoofCover.java
* Creation History:
Created By : Nalini on 10/24/2012
* Modification History:
Last Modified By : Gowri on 11/28/2012
************************************************************ 
*/
package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.WindMitBuildCode.textwatcher;
import idsoft.inspectiondepot.ARR.WindMitCommAuxbuildings.Spin_Selectedlistener;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

public class WindMitRoofCover extends Activity{
	CommonFunctions cf;
	boolean b1=false,b2=false,b3=false,b4=false,b5=false,b6=false;
	TextView miameweblinkopen, fbcweblinkopen;
	public int RoofCoverValue[] = { 0, 0, 0, 0, 0, 0 };
	protected static final int DATE_DIALOG_ID = 0;
	boolean staus_autoselect[] = { false, false, false };
	final boolean[] b = { false, false, false, false };
	int predominant=0,Current_year;
	int yearinfo1 = 0, yearinfo2 = 0, yearinfo3 = 0, yearinfo4 = 0,yearinfo5 = 0, yearinfo6 = 0;
	RadioButton radio_most[] = new RadioButton[6];
	int[] rcoptionid = {R.id.Rcrdioopt1,R.id.Rcrdioopt2,R.id.Rcrdioopt3,R.id.Rcrdioopt4};
	RadioButton roofcoveroption[] = new RadioButton[4];
	boolean weblink=false;
	
	 String yearbuilt[] = { "--Select--","Unknown","2014","2013","2012","2011","2010","2009","2008","2007","2006","2005","2004","2003","2002","2001","2000","1999","1998","1997","1996",
				"1995","1994","1993","1992","1991","1990","1989","1988","1987","1986","1985","1984","1983","1982","1981","1980","1979","1978","1977","1976",
				"1975","1974","1973","1972","1971","1970","1969","1968","1967","1966","1965","1964","1963","1962","1961","1960","1959","1958","1957","1956",
				"1955","1954","1953","1952","1951","1950","1949","1948","1947","1946","1945","1944","1943","1942","1941","1940","1939","1938","1937","1936",
				"1935","1934","1933","1932","1931","1930","1929","1928","1927","1926","1925","1924","1923","1922","1921","1920","1919","1918","1917","1916",
				"1915","1914","1913","1912","1911","1910","1909","1908","1907","1906","1905","1904","1903","1902","1901","1900"};
	
	String permitvalue[]=new String[6],prodapprval[]=new String[6],yearvalue[]=new String[6],noinfovalue[]=new String[6];	
	int[] rcpermitid = {R.id.edpermitappdate1,R.id.edpermitappdate2,R.id.edpermitappdate3,R.id.edpermitappdate4,R.id.edpermitappdate5,R.id.edpermitappdate6};
	int[] rcprodid = {R.id.edproduct1,R.id.edproduct2,R.id.edproduct3,R.id.edproduct4,R.id.edproduct5,R.id.edproduct6};
	int[] rcyearid = {R.id.spnryr1,R.id.spnryr2,R.id.spnryr3,R.id.spnryr4,R.id.spnryr5,R.id.spnryr6};	
	int[] rcdateiconid = {R.id.dateicon1,R.id.dateicon2,R.id.dateicon3,R.id.dateicon4,R.id.dateicon5,R.id.dateicon6};
	Button btndateroof[] = new Button[6];	
	int[] rcnoinfoid = {R.id.chknoinfo1,R.id.chknoinfo2,R.id.chknoinfo3,R.id.chknoinfo4,R.id.chknoinfo5,R.id.chknoinfo6};
	CheckBox chknoinfoprovide[] = new CheckBox[6];
	boolean load_comment=true;
	EditText edtroofpermit[] = new EditText[6];
	EditText edtroofproduct[] = new EditText[6];
	Spinner spnyear[] = new Spinner[6];
	/*String yearb[] = { "--Select--", "1990", "1991", "1992", "1993", "1994",
			"1995", "1996", "1997", "1998", "1999", "2000", "2001", "2002",
			"2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010",
			"2011", "2012","2013" };*/
	public String chkboxvalidation[] = { "", "", "", "", "", "" };
	Map<String, boolean[]> set_auto_select = new HashMap<String, boolean[]>();
	int[] chkroofcovertypeid = {R.id.chkroofcovertype1,R.id.chkroofcovertype2,R.id.chkroofcovertype3,R.id.chkroofcovertype4,R.id.chkroofcovertype5,R.id.chkroofcovertype6};
	CheckBox chk_rctype[] = new CheckBox[6];TableRow tblrw;
	String tmp="",selected_check_list,rcvalue="",count_value,
			permitvalue1="",permitvalue2="",permitvalue3="",permitvalue4="",permitvalue5="",permitvalue6="",
			prodapprvalue1="",prodapprvalue2="",prodapprvalue3="",prodapprvalue4="",prodapprvalue5="",prodapprvalue6="",
			yearvalue1="",yearvalue2="",yearvalue3="",yearvalue4="",yearvalue5="",yearvalue6="",
			roofconcrete="", roofasphalt="", roofmetal="", roofbuiltup="", roofother="",roofmembrane="",
					yearasphalt="",yearconcrete;
	Spinner spnasphalt,spnmetal,spnconcrete,spnmembrane,spnother,spnbuildup;
	public String chkstatus[] = { "true", "true", "true", "true", "true",
			"true", "true" }, chkstatus1[] = { "true", "true", "true", "true",
			"true", "true", "true" }, chkdatestatus[] = { "true", "true",
			"true", "true", "true", "true", "true" };
	String  edpermitdateval1 = "",edpermitdateval2 = "", edpermitdateval3 = "",	edpermitdateval4 = "", edpermitdateval5 = "",edpermitdateval6="",
			edprodappval1="",edprodappval2="",edprodappval3="",edprodappval4="",edprodappval5="",edprodappval6="",
			spnryearval1="",spnryearval2="",spnryearval3="",spnryearval4="",spnryearval5="",spnryearval6="",
			yearvalasp = "", yearvalconc = "", yearvalmetal = "",rccommentsval="",
			yearvalbuiltup = "", yearvalmemb = "", yearvalother = "",updatecnt="";
	String permit1="",product1="",year1="",permit2="",product2="",year2="",permit3="",product3="",year3="",permit4="",product4="",year4="",
		   permit5="",product5="",year5="",permit6="",product6="",year6="",othercovertext="";
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cf=new CommonFunctions(this);	
        Bundle extras = getIntent().getExtras();
		if (extras != null) {
			cf.getExtras(extras);				
	 	}	
		setContentView(R.layout.windmitroofcover);
        cf.getDeviceDimensions();
        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
        if(cf.identityval==31)
        {
         hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","B1 1802(Rev.01/12)","Roof Cover",3,1,cf));
        }
        else if(cf.identityval==32)
        { 
        	hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","Commercial Type I","Roof Cover",3,1,cf));
        }
		LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
     	main_layout.setMinimumWidth(cf.wd);
     	main_layout.addView(new MyOnclickListener(getApplicationContext(), 3, 0,0,cf));
       
        cf.type_submenulayout =(LinearLayout) findViewById(R.id.typesubmenu); //** WINDMIT TYPE SUBMENU **//
        cf.type_submenulayout.addView(new MyOnclickListener(this, 300, 1,0, cf));
       
        cf.submenu_layout = (LinearLayout) findViewById(R.id.submenu);//** WINDMIT TYPE SUBMENU **//	        
        cf.submenu_layout .addView(new MyOnclickListener(this,cf.identityval,1,302, cf));
        cf.windmittypechk(cf.identityval,((RadioButton)findViewById(R.id.b1802_chk)),((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((TextView)findViewById(R.id.commtxt)),((ImageView)findViewById(R.id.editbno))); // To check the checkbox of choosen inspection type
        cf.uncheckcommercial(((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((RadioButton)findViewById(R.id.b1802_chk))); 
    	                
        cf.mainlayout = (LinearLayout)findViewById(R.id.bc_table);
        cf.save = (Button)findViewById(R.id.save);
        cf.tblrw = (TableRow)findViewById(R.id.row2);
        cf.tblrw.setMinimumHeight(cf.ht);			
		cf.CreateARRTable(31);
		cf.CreateARRTable(32);
		cf.CreateARRTable(33);
		cf.getCalender();
		final Calendar c = Calendar.getInstance();
		Current_year = c.get(Calendar.YEAR);
		Declarations();		
		RoofCover_setValue();
    }
	private void RoofCover_setValue() {
		// TODO Auto-generated method stub
		try
		{
			Cursor c1 = cf.arr_db.rawQuery("SELECT * FROM "	+ cf.quesroofcover + " WHERE SRID='"
					+ cf.selectedhomeid + "' and INSP_TYPEID='"+cf.identityval+"'", null);	
		    if(c1.getCount()>0)
		    {  
			   c1.moveToFirst();
			   predominant = c1.getInt(c1.getColumnIndex("RoofPreDominant"));
			   int roofcovervalue = c1.getInt(c1.getColumnIndex("RoofCoverValue"));
			   
			   if (predominant != 0) {
					for (int i = 0; i <= 5; i++) {
						radio_most[i].setEnabled(false);
						if (predominant - 1 == i) {
							radio_most[i].setChecked(true);
						} else {
							radio_most[i].setChecked(false);
						}

					}
					((TextView)findViewById(R.id.savetxtrc)).setVisibility(cf.v1.VISIBLE);
				}
			   else
			   {
				   ((TextView)findViewById(R.id.savetxtrc)).setVisibility(cf.v1.GONE);
			   }
				
			    String permitdate1 = cf.decode(c1.getString(c1.getColumnIndex("PermitApplnDate1")));
			    String permitdate2 = cf.decode(c1.getString(c1.getColumnIndex("PermitApplnDate2")));
			    String permitdate3 = cf.decode(c1.getString(c1.getColumnIndex("PermitApplnDate3")));
			    String permitdate4 = cf.decode(c1.getString(c1.getColumnIndex("PermitApplnDate4")));
			    String permitdate5 = cf.decode(c1.getString(c1.getColumnIndex("PermitApplnDate5")));
			    String permitdate6 = cf.decode(c1.getString(c1.getColumnIndex("PermitApplnDate6")));
			    
			    String prodappr1 = cf.decode(c1.getString(c1.getColumnIndex("ProdApproval1")));
			    String prodappr2 = cf.decode(c1.getString(c1.getColumnIndex("ProdApproval2")));
			    String prodappr3 = cf.decode(c1.getString(c1.getColumnIndex("ProdApproval3")));
			    String prodappr4 = cf.decode(c1.getString(c1.getColumnIndex("ProdApproval4")));
			    String prodappr5 = cf.decode(c1.getString(c1.getColumnIndex("ProdApproval5")));
			    String prodappr6 = cf.decode(c1.getString(c1.getColumnIndex("ProdApproval6")));
			    
			    String installyr1 = c1.getString(c1.getColumnIndex("InstallYear1"));
			    String installyr2 = c1.getString(c1.getColumnIndex("InstallYear2"));
			    String installyr3 = c1.getString(c1.getColumnIndex("InstallYear3"));
			    String installyr4 = c1.getString(c1.getColumnIndex("InstallYear4"));
			    String installyr5 = c1.getString(c1.getColumnIndex("InstallYear5"));
			    String installyr6 = c1.getString(c1.getColumnIndex("InstallYear6"));
			    
			    String noinfo1 = c1.getString(c1.getColumnIndex("NoInfo1"));
			    String noinfo2 = c1.getString(c1.getColumnIndex("NoInfo2"));
			    String noinfo3 = c1.getString(c1.getColumnIndex("NoInfo3"));
			    String noinfo4 = c1.getString(c1.getColumnIndex("NoInfo4"));
			    String noinfo5 = c1.getString(c1.getColumnIndex("NoInfo5"));
			    String noinfo6 = c1.getString(c1.getColumnIndex("NoInfo6"));
			   
			    if ("1".equals(String.valueOf(roofcovervalue))) 
				{
					  rcvalue = "1";
					 ((RadioButton)findViewById(R.id.Rcrdioopt1)).setChecked(true);
				}  
			    else if ("2".equals(String.valueOf(roofcovervalue))) 
				{
					  rcvalue = "2";
					  ((RadioButton)findViewById(R.id.Rcrdioopt2)).setChecked(true); System.out.println("aasas");
				}  else if ("3".equals(String.valueOf(roofcovervalue))) 
				{
					  rcvalue = "3";
					  ((RadioButton)findViewById(R.id.Rcrdioopt3)).setChecked(true);
				} else if ("4".equals(String.valueOf(roofcovervalue))) {
					  rcvalue = "4";
					  ((RadioButton)findViewById(R.id.Rcrdioopt4)).setChecked(true);
				} else { rcvalue = "0";}	  
			    System.out.println("noinfo1="+noinfo1+"permitdate1"+permitdate1+"prodappr1"+prodappr1+"installyr1"+installyr1+"test");
			    
			    if(!"".equals(permitdate1) || !"".equals(prodappr1)  || (!"--Select--".equals(installyr1) && !"".equals(installyr1)) || !"0".equals(noinfo1))
			    {
			    	System.out.println("noiinside 1");
					 chk_rctype[0].setChecked(true);
					 Setchk_value(0); 
					 if(!"".equals(permitdate1)){  edtroofpermit[0].setText(permitdate1);} 
					 if(!"".equals(prodappr1)) 	{  edtroofproduct[0].setText(prodappr1);}			 
					 if(!"".equals(installyr1)) { b1=true; spnyear[0].setSelection(getkeyarray(yearbuilt,cf.decode(installyr1)));}		
					 if("1".equals(noinfo1))	{  chknoinfoprovide[0].setChecked(true); }  else{ chknoinfoprovide[0].setChecked(false); }
					 
					 radio_most[0].setEnabled(true); 
					 RoofCoverValue[0] = 1;
					 set_auto_select.put("1", this.b); 
					 //auto_select(1, 0);				  
			    }   
			   if(!"".equals(permitdate2)  || !"".equals(prodappr2) ||  (!"--Select--".equals(installyr2) && !"".equals(installyr2)) || !"0".equals(noinfo2))
			  {  
				 chk_rctype[1].setChecked(true);
				 Setchk_value(1);
				 if(!"".equals(permitdate2)){  edtroofpermit[1].setText(permitdate2);}
				 if(!"".equals(prodappr2)) 	{  edtroofproduct[1].setText(prodappr2);}				 
				 if(!"".equals(installyr2)) {  b2=true; spnyear[1].setSelection(getkeyarray(yearbuilt,cf.decode(installyr2)));}				
				 if("1".equals(noinfo2))	{  chknoinfoprovide[1].setChecked(true); }  else{ chknoinfoprovide[1].setChecked(false); }
				 
				 radio_most[1].setEnabled(true);
				 RoofCoverValue[1] = 1;
				 set_auto_select.put("2", this.b);
				 //auto_select(2, 0);				 
			  }  
			  System.out.println("noinfo3="+noinfo3+"permitdate3"+permitdate3+"prodappr3"+prodappr3+"installyr3"+installyr3+"test");
			 if(!"".equals(permitdate3) || !"".equals(prodappr3)  || (!"--Select--".equals(installyr3) && !"".equals(installyr3)) || !"0".equals(noinfo3))
			 {  
				 chk_rctype[2].setChecked(true);
				 Setchk_value(2);
				 if(!"".equals(permitdate3)){  edtroofpermit[2].setText(permitdate3);}
				 if(!"".equals(prodappr3)) 	{  edtroofproduct[2].setText(prodappr3);}				 
				 if(!"".equals(installyr3)) { b3=true; spnyear[2].setSelection(getkeyarray(yearbuilt,cf.decode(installyr3)));}				
				 if("1".equals(noinfo3))	{  chknoinfoprovide[2].setChecked(true); }  else{ chknoinfoprovide[2].setChecked(false); }
				 
				 radio_most[2].setEnabled(true);
				 RoofCoverValue[2] = 1;
				 set_auto_select.put("3", this.b);
				// auto_select(3, 0);				 
			  }   
			  if(!"".equals(permitdate4) || !"".equals(prodappr4)  || (!"--Select--".equals(installyr4) && !"".equals(installyr4)) || !"0".equals(noinfo4))
			  {  
				 chk_rctype[3].setChecked(true);
				 Setchk_value(3);
				 if(!"".equals(permitdate4)){  edtroofpermit[3].setText(permitdate4);}
				 if(!"".equals(prodappr4)) 	{  edtroofproduct[3].setText(prodappr4);}				 
				 if(!"".equals(installyr4)) { b4=true; spnyear[3].setSelection(getkeyarray(yearbuilt,cf.decode(installyr4)));}				
				 if("1".equals(noinfo4))	{  chknoinfoprovide[3].setChecked(true); }  else{ chknoinfoprovide[3].setChecked(false); }
				 
				 radio_most[3].setEnabled(true);
				 RoofCoverValue[3] = 1;
				 set_auto_select.put("4", this.b);
				 //auto_select(4, 0);
			  }   
			  if(!"".equals(permitdate5) || !"".equals(prodappr5)  || (!"--Select--".equals(installyr5) && !"".equals(installyr5)) || !"0".equals(noinfo5))
			  { 
				  chk_rctype[4].setChecked(true);
				  Setchk_value(4);
				  if(!"".equals(permitdate5)){  edtroofpermit[4].setText(permitdate5);}
				  if(!"".equals(prodappr5))  {  edtroofproduct[4].setText(prodappr5);}				 
				  if(!"".equals(installyr5)) {  b5=true; spnyear[4].setSelection(getkeyarray(yearbuilt,cf.decode(installyr5)));}				
				  if("1".equals(noinfo5))	 {  chknoinfoprovide[4].setChecked(true); }  else{ chknoinfoprovide[4].setChecked(false); }

				 radio_most[4].setEnabled(true);
				 RoofCoverValue[4] = 1;
				 set_auto_select.put("5", this.b);
				 //auto_select(5, 0);
			  } 
			  if(!"".equals(permitdate6)  || !"".equals(prodappr6)  || (!"--Select--".equals(installyr6) && !"".equals(installyr6)) || !"0".equals(noinfo6))
			  {  
				  chk_rctype[5].setChecked(true);
				  ((EditText)findViewById(R.id.edother)).setEnabled(true);
				  Setchk_value(5);
				  if(!"".equals(permitdate6)){  edtroofpermit[5].setText(permitdate6);}
				  if(!"".equals(prodappr6))  {  edtroofproduct[5].setText(prodappr6);}				 
				  if(!"".equals(installyr6)) { b6=true; spnyear[5].setSelection(getkeyarray(yearbuilt,cf.decode(installyr6)));}				
				  if("1".equals(noinfo6))	 {  chknoinfoprovide[5].setChecked(true); }  else{ chknoinfoprovide[5].setChecked(false); }
				  if (!"".equals(cf.decode(c1.getString(c1.getColumnIndex("RoofCoverTypeOther"))))) 
				  {
					  ((EditText)findViewById(R.id.edother)).setText(cf.decode(c1.getString(c1.getColumnIndex("RoofCoverTypeOther"))));
				  }
				  radio_most[5].setEnabled(true);
				  RoofCoverValue[5] = 1;
				  set_auto_select.put("6", this.b);
				 // auto_select(6, 0);
			  }
			  
		   }
		   else
		   {
			   ((TextView)findViewById(R.id.savetxtrc)).setVisibility(cf.v1.GONE);
		   }
		    
		    Cursor c12=cf.SelectTablefunction(cf.Wind_QuesCommtbl, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
			 if(c12.getCount()>0)
			 {	
				  c12.moveToFirst();
				  ((EditText)findViewById(R.id.rccomment)).setText(cf.decode(c12.getString(c12.getColumnIndex("fld_roofcovercomments"))));
			}
		 
		}
	    catch (Exception E)
		{
	    	System.out.println("EE dsfdf"+E.getMessage());
			String strerrorlog="Retrieving RoofCover - WINDMIT";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
	private void Declarations() {
		cf.setupUI((ScrollView) findViewById(R.id.scr));
		((TextView)findViewById(R.id.windrc_header)).setText(Html
				.fromHtml("Select all roof covering types in use. Provide the permit application date OR FBC/MDC Product Approval number OR Year of Original Installation/Replacement OR indicate that no information was available to verify compliance for each roof covering identified."));
		
		String miamelink = "<a href=''>" + "Miame-Dade Product Approval" + "</a>";
		String fbclink = "<a href=''>" + "FBC Product Approval" + "</a>";
		((TextView)findViewById(R.id.miamedadeprodapprlink)).setText(Html.fromHtml(miamelink));
		((TextView)findViewById(R.id.miamedadeprodapprlink)).setOnClickListener((OnClickListener) new clicker1());
		
		((TextView)findViewById(R.id.fbcprodapprlink)).setText(Html.fromHtml(fbclink));
		((TextView)findViewById(R.id.fbcprodapprlink)).setOnClickListener((OnClickListener) new clicker1());
		
		for(int i=0;i<chk_rctype.length;i++)
    	 {
    		try
    		{
    			chk_rctype[i] = (CheckBox)findViewById(chkroofcovertypeid[i]); 
    			
    		}
			catch(Exception e)
			{
				System.out.println("error= "+e.getMessage());
			}
    	 }
		 for(int i=0;i<roofcoveroption.length;i++)
    	 {
			 try
	    	 {
				 roofcoveroption[i] = (RadioButton)findViewById(rcoptionid[i]);
	    	}
			 catch(Exception e)
			{
					System.out.println("error= "+e.getMessage());
			}
    	 }
		 
		//for the permit date auto selection starts here
			TextWatcher watcher = new TextWatcher() {

				public void onTextChanged(CharSequence s, int start, int before,
						int count) {
					// TODO Auto-generated method stub

				}

				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
					// TODO Auto-generated method stub

				}

				public void afterTextChanged(Editable e) {
					// TODO Auto-generated method stub
					int id = 0;
					try {

						View v = getCurrentFocus();
						id = v.getId();
						int select = 0;
						switch (id) {
						case R.id.edpermitappdate1:
							select = 1;
							break;
						case R.id.edpermitappdate2:
							select = 2;
							break;
						case R.id.edpermitappdate3:
							select = 3;
							break;
						case R.id.edpermitappdate4:
							select = 4;
							break;
						case R.id.edpermitappdate5:
							select = 5;
							break;
						case R.id.edpermitappdate6:
							select = 6;
							break;
						case R.id.edproduct1:
							select = 1;
							break;
						case R.id.edproduct2:
							select = 2;
							break;
						case R.id.edproduct3:
							select = 3;
							break;
						case R.id.edproduct4:
							select = 4;
							break;
						case R.id.edproduct5:
							select = 5;
							break;
						case R.id.edproduct6:
							select = 6;
							break;

						}
						auto_select(select, 1);
					} catch (Exception m) {
						System.out.println("m "+m.getMessage());
					}
				}

			};
		 
		 
		 for(int i=0;i<edtroofpermit.length;i++)
    	 {
			 try
	    	 {
				 edtroofpermit[i] =(EditText)findViewById(rcpermitid[i]);
				 edtroofpermit[i].setEnabled(false);
			 }
			 catch(Exception e)
			{
					System.out.println("error= "+e.getMessage());
			}
    	 }
		 for(int i=0;i<edtroofpermit.length;i++)
    	 {
			 try
	    	 {
				 edtroofproduct[i] =(EditText)findViewById(rcprodid[i]);
				 edtroofproduct[i].setEnabled(false);
			 }
			 catch(Exception e)
			 {
				 System.out.println("error= "+e.getMessage());
			 }
    	 }
		 for(int i=0;i<chknoinfoprovide.length;i++)
    	 {
			 try
	    	 {
				 chknoinfoprovide[i] =(CheckBox)findViewById(rcnoinfoid[i]);
				 chknoinfoprovide[i].setEnabled(false);
	    	}
			 catch(Exception e)
			{
					System.out.println("error= "+e.getMessage());
			}
    	 }
		 chknoinfoprovide[0].setOnClickListener(myOnClickListener);
		 chknoinfoprovide[1].setOnClickListener(myOnClickListener);
		 chknoinfoprovide[2].setOnClickListener(myOnClickListener);
		 chknoinfoprovide[3].setOnClickListener(myOnClickListener);
		 chknoinfoprovide[4].setOnClickListener(myOnClickListener);
		 chknoinfoprovide[5].setOnClickListener(myOnClickListener);
		 
		 
		 ((EditText)findViewById(R.id.edother)).setEnabled(false);
		 ArrayAdapter adapter2 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, yearbuilt);
		 adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 for(int i=0;i<spnyear.length;i++)
    	 {
			 try
	    	 {
				 spnyear[i] =(Spinner)findViewById(rcyearid[i]);spnyear[i].setEnabled(false);
				 spnyear[i].setAdapter(adapter2);
	    	}
			 catch(Exception e)
			{
					System.out.println("error= "+e.getMessage());
			}
    	 }
		 spnyear[0].setOnItemSelectedListener(new  Spin_Selectedlistener(1)); 
		 spnyear[1].setOnItemSelectedListener(new  Spin_Selectedlistener(2)); 
		 spnyear[2].setOnItemSelectedListener(new  Spin_Selectedlistener(3)); 
		 spnyear[3].setOnItemSelectedListener(new  Spin_Selectedlistener(4)); 
		 spnyear[4].setOnItemSelectedListener(new  Spin_Selectedlistener(5)); 
		 spnyear[5].setOnItemSelectedListener(new  Spin_Selectedlistener(6)); 
		 
		 
		 spnyear[0].setOnTouchListener(new OnTouchListener() {				
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					b1=false;
					return false;
				}
			});
		 spnyear[1].setOnTouchListener(new OnTouchListener() {		
		
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					b2=false;
					return false;
				}
			});
		 spnyear[2].setOnTouchListener(new OnTouchListener() {				
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					b3=false;
					return false;
				}
			});
		 spnyear[3].setOnTouchListener(new OnTouchListener() {				
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					b4=false;
					return false;
				}
			});
		 spnyear[4].setOnTouchListener(new OnTouchListener() {				
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					b5=false;
					return false;
				}
			});
		 spnyear[5].setOnTouchListener(new OnTouchListener() {				
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					b6=false;
					return false;
				}
			});
		 
    	 for(int i=0;i<btndateroof.length;i++)
    	 {
			 try
	    	 {
				 btndateroof[i] =(Button)findViewById(rcdateiconid[i]);
				 btndateroof[i].setEnabled(false);
				 
	    	}
			 catch(Exception e)
			{
					System.out.println("error= "+e.getMessage());
			}
    	 }
	
	// getcountname();
    	 
    		edtroofpermit[0].addTextChangedListener(watcher); 
        	edtroofpermit[1].addTextChangedListener(watcher); 
        	edtroofpermit[2].addTextChangedListener(watcher); 
        	edtroofpermit[3].addTextChangedListener(watcher); 
        	edtroofpermit[4].addTextChangedListener(watcher); 
        	edtroofpermit[5].addTextChangedListener(watcher); 
        	 
        	edtroofproduct[0].addTextChangedListener(watcher);
        	edtroofproduct[1].addTextChangedListener(watcher);
        	edtroofproduct[2].addTextChangedListener(watcher);
        	edtroofproduct[3].addTextChangedListener(watcher);
        	edtroofproduct[4].addTextChangedListener(watcher);
        	edtroofproduct[5].addTextChangedListener(watcher);
    	 
        	((EditText)findViewById(R.id.rccomment)).addTextChangedListener(new textwatcher1());
    	
	    	radio_most[0] = (RadioButton) findViewById(R.id.most_rdio1);
	 		radio_most[1] = (RadioButton) findViewById(R.id.most_rdio2);
	 		radio_most[2] = (RadioButton) findViewById(R.id.most_rdio3);
	 		radio_most[3] = (RadioButton) findViewById(R.id.most_rdio4);
	 		radio_most[4] = (RadioButton) findViewById(R.id.most_rdio5);
	 		radio_most[5] = (RadioButton) findViewById(R.id.most_rdio6);
			
 		for (int i = 0; i <= 5; i++) {
			radio_most[i].setEnabled(false);
			radio_most[i].setOnClickListener(new OnClickListener() {

				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					int check = 7;
					switch (arg0.getId()) {
					case R.id.most_rdio1:
						check = 0;
						predominant = 1;
						break;
					case R.id.most_rdio2:
						check = 1;
						predominant = 2;
						break;
					case R.id.most_rdio3:
						check = 2;
						predominant = 3;
						break;
					case R.id.most_rdio4:
						check = 3;
						predominant = 4;
						break;
					case R.id.most_rdio5:
						check = 4;
						predominant = 5;
						break;
					case R.id.most_rdio6:
						check = 5;
						predominant = 6;
						break;

					}
					for (int i = 0; i <= 5; i++) {
						if (check == i) {
							radio_most[i].setChecked(true);
						} else {
							radio_most[i].setChecked(false);
						}

					}

				}

			});
		}
		
 		TextView helptxt = (TextView) findViewById(R.id.help);
		helptxt.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				 if(!roofcoveroption[0].isChecked() && !roofcoveroption[1].isChecked() && !roofcoveroption[2].isChecked() && !roofcoveroption[3].isChecked()) {
					 cf.alertcontent = "To see help comments, please select Roof Covering options.";
					}
					else if (roofcoveroption[0].isChecked()) {
						cf.alertcontent = "This home was verified as meeting the requirements of selection A of the OIR B1 -1802 Question 2, Roof Covering.";
					} else if (roofcoveroption[1].isChecked()) {
						cf.alertcontent = "This home was verified as meeting the requirements of selection B of the OIR B1 -1802 Question 2 Roof Covering.";
					} else if (roofcoveroption[2].isChecked()) {
						cf.alertcontent = "This home was verified as NOT meeting the requirements of the OIR B1-1802, Question 1, selection A or B.";
					} else if (roofcoveroption[3].isChecked()) {
						cf.alertcontent = "This home was verified as NOT meeting the requirements of the OIR B1-1802, Question 1, selection A or B.";
					} else {
						cf.alertcontent = "To see help comments, please select Roof Covering options.";
					}

				 	cf.showhelp("HELP",cf.alertcontent);
			}
		});
			
	}
	
	class Spin_Selectedlistener implements OnItemSelectedListener
	{
		int i;
		public Spin_Selectedlistener(int i) {
			// TODO Auto-generated constructor stub
			this.i=i;
		}

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			if(i==1)
			{
				System.out.println("i = "+i+ " posit "+arg2);
				if (arg2 != 0) 
				{
					if (!b1) {
					auto_select(1, 1);
					}
				}				
			}
			else if(i==2)
			{
				System.out.println("i = "+i+ " posit "+arg2);
				if (arg2 != 0) 
				{
					if (!b2) {
						auto_select(2, 1);
					}
				}
			}
			else if(i==3)
			{
				System.out.println("i = "+i+ " posit "+arg2);
				if (arg2 != 0) 
				{
					if (!b3) {
					auto_select(3, 1);
					}
				}
			}
			else if(i==4)
			{
				System.out.println("i = "+i+ " posit "+arg2);
				if (arg2 != 0) 
				{
					if (!b4) {
					auto_select(4, 1);
					}
				}
			}
			else if(i==5)
			{
				System.out.println("i = "+i+ " posit "+arg2);
				if (arg2 != 0) 
				{
					if (!b5) {
					auto_select(5, 1);
					}
				}
			}
			else if(i==6)
			{
				System.out.println("i = "+i+ " posit "+arg2);
				if (arg2 != 0) 
				{
					if (!b6) {
					auto_select(6, 1);
					}
				}
			}
		}
		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub			
		}		
	}
	
	class textwatcher1 implements TextWatcher
    {
	         public int type;
	         textwatcher1()
	    	{
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {
	    		// TODO Auto-generated method stub
	    		cf.showing_limit(s.toString(),(TextView)findViewById(R.id.rc_tv_type1),500); 
	    		
	}

	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,

	    			int after) {
	    		// TODO Auto-generated method stub
	     	}

	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,

	    			int count) {
	    		// TODO Auto-generated method stub
	    	}
	   }	
	
	OnClickListener myOnClickListener = new OnClickListener() {

		public void onClick(View v) {
			// TODO Auto-generated method stub
			int select;
			switch (v.getId()) {
			case R.id.chknoinfo1:
				if (chknoinfoprovide[0].isChecked()) {
					if (chk_rctype[0].isChecked()) {
						boolean[] bl = set_auto_select.get("1").clone();
						set_auto_select.remove("1");
						bl[3] = true;
						set_auto_select.put("1", bl);
						select_radio();
						edtroofpermit[0].setText("");
						edtroofproduct[0].setText("");
						spnyear[0].setSelection(0);
					}
				} else {
					try{
					boolean[] bl = set_auto_select.get("1").clone();
					set_auto_select.remove("1");
					bl[3] = false;
					set_auto_select.put("1", bl);
					select_radio();}
					catch(Exception e){}
				}
				break;
			case R.id.chknoinfo2:
				if (chknoinfoprovide[1].isChecked() && (chk_rctype[1].isChecked()))
				{
					boolean[] bl = set_auto_select.get("2").clone();
					set_auto_select.remove("2");
					bl[3] = true;
					set_auto_select.put("2", bl);
					select_radio();
					edtroofpermit[1].setText("");
					edtroofproduct[1].setText("");
					spnyear[1].setSelection(0);
				} else 
				{
					try{
					boolean[] bl = set_auto_select.get("2").clone();
					set_auto_select.remove("2");
					bl[3] = false;
					set_auto_select.put("2", bl);
					select_radio();
					}catch(Exception e){}
				}
				break;
			case R.id.chknoinfo3:
				if (chknoinfoprovide[2].isChecked() && (chk_rctype[2].isChecked()))
				{					
					boolean[] bl = set_auto_select.get("3").clone();
					set_auto_select.remove("3");
					bl[3] = true;
					set_auto_select.put("3", bl);
					select_radio();
					edtroofpermit[2].setText("");
					edtroofproduct[2].setText("");
					spnyear[2].setSelection(0);
				} else {
					try
					{
					boolean[] bl = set_auto_select.get("3").clone();
					set_auto_select.remove("3");
					bl[3] = false;
					set_auto_select.put("3", bl);
					select_radio();
					}catch(Exception e){}
				}
				break;
			case R.id.chknoinfo4:
				if (chknoinfoprovide[3].isChecked() && (chk_rctype[3].isChecked()))
				{
					boolean[] bl = set_auto_select.get("4").clone();
					set_auto_select.remove("4");
					bl[3] = true;
					set_auto_select.put("4", bl);
					select_radio();
					edtroofpermit[3].setText("");
					edtroofproduct[3].setText("");
					spnyear[3].setSelection(0);
				} else {
					try{
					boolean[] bl = set_auto_select.get("4").clone();
					set_auto_select.remove("4");
					bl[3] = false;
					set_auto_select.put("4", bl);
					select_radio();
					}catch(Exception e){}
				}
				break;
			case R.id.chknoinfo5:
				if (chknoinfoprovide[4].isChecked() && (chk_rctype[4].isChecked()))
				{
					boolean[] bl = set_auto_select.get("5").clone();
					set_auto_select.remove("5");
					bl[3] = true;
					set_auto_select.put("5", bl);
					select_radio();
					edtroofpermit[4].setText("");
					edtroofproduct[4].setText("");
					spnyear[4].setSelection(0);
				} else {
					try{
					boolean[] bl = set_auto_select.get("5").clone();
					set_auto_select.remove("5");
					bl[3] = false;
					set_auto_select.put("5", bl);
					select_radio();
					}catch(Exception e){}
				}
				break;
			case R.id.chknoinfo6:
				if (chknoinfoprovide[5].isChecked() && (chk_rctype[5].isChecked()))
				{
					boolean[] bl = set_auto_select.get("6").clone();
					set_auto_select.remove("6");
					bl[3] = true;
					set_auto_select.put("6", bl);
					select_radio();
					edtroofpermit[5].setText("");
					edtroofproduct[5].setText("");
					spnyear[5].setSelection(0);
				} else {
					try{
					boolean[] bl = set_auto_select.get("6").clone();
					set_auto_select.remove("6");
					bl[3] = false;
					set_auto_select.put("6", bl);
					((EditText) findViewById(R.id.edother)).setText("");
					select_radio();
					}catch(Exception e){}
				}
				break;

			}
		}

	};
	public void Setchk_value(int i) {
		// TODO Auto-generated method stub
		staus_autoselect[0] = false;
		staus_autoselect[1] = false;
		staus_autoselect[2] = false;
		edtroofpermit[i].setEnabled(true);edtroofpermit[i].setCursorVisible(false);
		edtroofproduct[i].setEnabled(true);//edtroofproduct[i].setCursorVisible(false);
		spnyear[i].setEnabled(true);
		chknoinfoprovide[i].setEnabled(true);
		btndateroof[i].setEnabled(true);
	}
	public void Setunchk_value(int i) {
		// TODO Auto-generated method stub
		edtroofpermit[i].setEnabled(false);	 	edtroofpermit[i].setText("");
		edtroofproduct[i].setEnabled(false);    edtroofproduct[i].setText("");
		spnyear[i].setEnabled(false); 			spnyear[i].setSelection(0);
		chknoinfoprovide[i].setEnabled(false);  chknoinfoprovide[i].setChecked(false);
		btndateroof[i].setEnabled(false);		
	}
	public void clicker(View v) {
		switch(v.getId())
		{
		case R.id.loadcomments:
			/***Call for the comments***/
			cf.findinspectionname(cf.identityval);RC_suboption();
			int len=((EditText)findViewById(R.id.rccomment)).getText().toString().length();			
			if(!tmp.equals(""))
			{
				/*if(len<500)
				{*/
					if(load_comment)
					{
						load_comment=false;
						int loc1[] = new int[2];
						v.getLocationOnScreen(loc1);
						Intent in = cf.loadComments(tmp,loc1);
						in.putExtra("insp_name", cf.inspname);
						in.putExtra("insp_ques", "Roof Cover");
						in.putExtra("length", len);
						in.putExtra("max_length", 500);
						startActivityForResult(in, cf.loadcomment_code);
					}
				/*}
				else
				{
					cf.ShowToast("You have exceed the maximum length.", 0);
				}*/
			}
			else
			{
				cf.ShowToast("Please select the option for Roof Cover.",0);
			}	
			break;
		case R.id.chkroofcovertype1: /** FIRST ROOF COVERING TYPE CHECK BOX TO BE SELECTED **/
			if (((CheckBox) v).isChecked()) {
				RoofCoverValue[0] = 1;
				set_auto_select.put("1", b);
				selected_check_list += "chk0~";
				roofasphalt = "1";
				Setchk_value(0);
				setPreDominant(0, true);

			} else {

				RoofCoverValue[0] = 0;
				set_auto_select.remove("1");
				roofasphalt = "0";
				chkstatus[0] = "true";
				chkstatus1[0] = "false";
				Setunchk_value(0);
				permitvalue1="";prodapprvalue1="";yearvalue1="";
				select_radio();
				setPreDominant(0, false);
			}
			break;
			
		case R.id.chkroofcovertype2: /** SECOND ROOF COVERING TYPE CHECK BOX TO BE SELECTED **/
			if (((CheckBox) v).isChecked()) {
				RoofCoverValue[1] = 1;
				set_auto_select.put("2", b);
				selected_check_list += "chk1~";
				roofconcrete = "1";
				Setchk_value(1);
				radio_most[1].setEnabled(true);
				setPreDominant(1, true);

			} else {

				RoofCoverValue[1] = 0;
				set_auto_select.remove("2");
				roofconcrete = "0";
				chkstatus[1] = "true";
				chkstatus1[1] = "false";
				Setunchk_value(1);
				permitvalue2="";prodapprvalue2="";yearvalue2="";
				select_radio();
				setPreDominant(1, false);
			}
			break;
		case R.id.chkroofcovertype3: /** THIRD ROOF COVERING TYPE CHECK BOX TO BE SELECTED **/
			if (((CheckBox) v).isChecked()) {
				RoofCoverValue[2] = 1;
				set_auto_select.put("3", b);
				selected_check_list += "chk2~";
				roofmetal = "1";
				Setchk_value(2);
				setPreDominant(2, true);

			} else {

				RoofCoverValue[2] = 0;
				set_auto_select.remove("3");
				roofmetal = "0";
				chkstatus[2] = "true";
				chkstatus1[2] = "false";
				Setunchk_value(2);				
				permitvalue3="";prodapprvalue3="";yearvalue3="";
				select_radio();
				setPreDominant(2, false);
			}
			break;
		case R.id.chkroofcovertype4: /** FOURTH ROOF COVERING TYPE CHECK BOX TO BE SELECTED **/
			if (((CheckBox) v).isChecked()) {
				RoofCoverValue[3] = 1;
				set_auto_select.put("4", b);
				selected_check_list += "chk3~";
				roofbuiltup = "1";
				Setchk_value(3);
				setPreDominant(3, true);

			} else {

				RoofCoverValue[3] = 0;
				set_auto_select.remove("4");
				roofbuiltup = "0";
				chkstatus[3] = "true";
				chkstatus1[3] = "false";
				Setunchk_value(3);
				permitvalue4="";prodapprvalue4="";yearvalue4="";
				select_radio();
				setPreDominant(3, false);
			}
			break;
		case R.id.chkroofcovertype5: /** FIFTH ROOF COVERING TYPE CHECK BOX TO BE SELECTED **/
			if (((CheckBox) v).isChecked()) {
				RoofCoverValue[4] = 1;
				set_auto_select.put("5", b);
				selected_check_list += "chk4~";
				roofmembrane = "1";
				Setchk_value(4);
				setPreDominant(4, true);

			} else {

				RoofCoverValue[4] = 0;
				set_auto_select.remove("5");
				roofmembrane = "0";
				chkstatus[4] = "true";
				chkstatus1[4] = "false";
				Setunchk_value(4);
				permitvalue5="";prodapprvalue5="";yearvalue5="";
				select_radio();
				setPreDominant(4, false);
			}
			break;
		case R.id.chkroofcovertype6: /** SIXTH ROOF COVERING TYPE CHECK BOX TO BE SELECTED **/
			if (((CheckBox) v).isChecked()) {
				RoofCoverValue[5] = 1;
				set_auto_select.put("6", b);
				selected_check_list += "chk5~";
				roofother = "1";
				((EditText)findViewById(R.id.edother)).setEnabled(true);
				Setchk_value(5);
				setPreDominant(5, true);
			} else {
				RoofCoverValue[5] = 0;
				set_auto_select.remove("6");
				roofother = "0";
				chkstatus[5] = "true";
				chkstatus1[5] = "false";
				((EditText)findViewById(R.id.edother)).setEnabled(false);
				((EditText)findViewById(R.id.edother)).setText("");
				Setunchk_value(5);
				permitvalue6="";prodapprvalue6="";yearvalue6="";
				select_radio();
				setPreDominant(5, false);
			}
			break;
		case R.id.Rcrdioopt1:
			((EditText)findViewById(R.id.rccomment)).setText("This home was verified as meeting the requirements of selection A of the OIR B1 -1802 Question 2, Roof Covering.");
			rcvalue = "1";
			hidecontrols();
			roofcoveroption[0].setChecked(true);			
			break;
		case R.id.Rcrdioopt2:
			((EditText)findViewById(R.id.rccomment)).setText("This home was verified as meeting the requirements of selection B of the OIR B1 -1802 Question 2 Roof Covering.");
			rcvalue = "2";
			hidecontrols();
			roofcoveroption[1].setChecked(true);			
			break;		
		case R.id.Rcrdioopt3:
			((EditText)findViewById(R.id.rccomment)).setText("This home was verified as meeting the requirements of selection C of the OIR B1 -1802 Question 2 Roof Covering.");
			rcvalue = "3";
			hidecontrols();
			roofcoveroption[2].setChecked(true);			
			break;		
		case R.id.Rcrdioopt4:
			((EditText)findViewById(R.id.rccomment)).setText("This home was verified as meeting the requirements of selection D of the OIR B1 -1802 Question 2, Roof Covering.");
			rcvalue = "4";
			hidecontrols();
			roofcoveroption[3].setChecked(true);			
			break;
		case R.id.helpoptA:
			cf.alerttitle="A - Meets FBC(PD -03/2/02) or Original (after 2004)";
			cf.alertcontent="All roof coverings listed above meet the FBC with an FBC or Miami-Dade product approval listing current at time of installation OR have a roofing permit application dated on or after 3/1/02 Or the roof is original and built in 2004 or later.";
			cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		case R.id.helpoptB:
			cf.alerttitle="B - Meets SFBC (PD 09/01/1994) or Post 1997";
			cf.alertcontent="All roof coverings have a Miami-Dade product approval listing current at time of installation OR (for the HVHZ only) a roofing permit application dated after 9/1/1994 and before 3/1/2002 OR the roof is original and built in 1997 or later.";
			cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		case R.id.helpoptC:
			cf.alerttitle="C - One or more does not meet A or B";
			cf.alertcontent="One or more roof coverings do not meet the requirements of answer A or B.";
			cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		case R.id.helpoptD:
			cf.alerttitle="D - None Meet A or B";				
			cf.alertcontent="No roof coverings meet the requirements of answer A or B."; 
			cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		case R.id.hme:
    		cf.gohome();
    		break;
		case R.id.dateicon1:/** DATE ICON 1 TO BE CLICKED **/
			System.out.println("dateicon1");
			dateiconclck();System.out.println("dateiconclck");chkboxvalidation[0] = "1";System.out.println("chkboxvalidation");	
			break;
		case R.id.dateicon2:/** DATE ICON 2 TO BE CLICKED **/
			dateiconclck();chkboxvalidation[1] = "1";	
			break;
		case R.id.dateicon3:/** DATE ICON 3 TO BE CLICKED **/
			dateiconclck();chkboxvalidation[2] = "1";
			break;
		case R.id.dateicon4:/** DATE ICON 4 TO BE CLICKED **/
			dateiconclck();chkboxvalidation[3] = "1";	
			break;
		case R.id.dateicon5:/** DATE ICON 5 TO BE CLICKED **/
			dateiconclck();chkboxvalidation[4] = "1";	
			break;
		case R.id.dateicon6:/** DATE ICON 6 TO BE CLICKED **/
			dateiconclck();chkboxvalidation[5] = "1";	
			break;
		case R.id.save:
			
			boolean chkglobal = false;
			rccommentsval = ((EditText)findViewById(R.id.rccomment)).getText().toString();
			
			if ((roofasphalt == "1" || chk_rctype[0].isChecked()) && (chkglobal == false)) {
				permit1 = edtroofpermit[0].getText().toString();
				product1 = edtroofproduct[0].getText().toString();
				year1 = spnyear[0].getSelectedItem().toString();
System.out.println("year111"+year1);
				
				if (("".equals(permit1)) && ("".equals(product1)) && ("--Select--".equals(year1)) && (chknoinfoprovide[0].isChecked() == false)) {
					cf.ShowToast("Please select any one of the Required value for Asphalt/Fiberglass Shingle.",0);
					chkglobal = true;
					chkstatus[0] = "false";
				} else {
					chkstatus[0] = "true";

					if (!"".equals(year1) && !"--Select--".equals(year1) && !"".equals(permit1)) {
						try{
						int year = Integer.parseInt(year1);
						if (checkpermitdategraterthanbuild(year1, permit1) == true) {
							chkdatestatus[0] = "true";
						} else {
							chkglobal = true;
							chkdatestatus[0] = "false";
							edtroofpermit[0].setEnabled(true);
							edtroofpermit[0].setText("");
							//edtroofpermit[0].requestFocus();
						}
						}catch(Exception e){}

					} else {
						chkdatestatus[0] = "true";
					}
					System.out.println("year1"+year1);
					if (!"".equals(year1)
							&& !"--Select--".equals(year1)) {
						try{
						int year = Integer.parseInt(year1);
						System.out.println("year "+year + " "+Current_year);
						
						if (year >= 1990 && year <= Current_year) {
							System.out.println("leess  "+year);
							yearvalasp = year1;
							chkstatus[0] = "true";
						} else {
							System.out.println("ygreater11 "+year);
							cf.ShowToast("Please enter the Date in valid format.",0);
							chkglobal = true;
							spnyear[0].requestFocus();
							chkglobal = true;
							chkstatus[0] = "false";
						}
						}catch(Exception e){}
					} else {
						chkstatus[0] = "true";
					}

				}
			}
			if ((roofconcrete == "1" || chk_rctype[1].isChecked() == true)	&& (chkglobal == false)) {
				permit2 = edtroofpermit[1].getText().toString();
				product2 = edtroofproduct[1].getText().toString();
				year2 = spnyear[1].getSelectedItem().toString();

				
				if (("".equals(permit2)) && ("".equals(product2)) && ("--Select--".equals(year2)) && (chknoinfoprovide[1].isChecked() == false)) 
				{
					cf.ShowToast("Please select any one of the Required value for Concrete/Clay Tile.",0);				
					chkglobal = true;
					chkstatus[1] = "false";
				} else {

					if (!"".equals(year2) && !"--Select--".equals(year2) && !"".equals(permit2)) {
						try{
						int year = Integer.parseInt(year2);
						if (checkpermitdategraterthanbuild(year2,permit2) == true) {
							chkdatestatus[1] = "true";
						} else {
							chkglobal = true;
							chkdatestatus[1] = "false";
							edtroofpermit[1].setEnabled(true);
							edtroofpermit[1].setText("");
							//edtroofpermit[1].requestFocus();
						}
						}catch(Exception e){}
					} else {
						chkdatestatus[1] = "true";
					}

					//chkstatus[1] = "true";
					System.out.println("year2"+year2);
					if (!"".equals(year2)
							&& !"--Select--".equals(year2)) {
						try{
						int year = Integer.parseInt(year2);
						
						System.out.println("yearns "+year+"current "+Current_year);
						
						if (year >= 1990 && year <= Current_year) {
							System.out.println("year2 ins "+year);
							
							yearvalconc = year2;
							chkstatus[1] = "true";
						} else {
							System.out.println("ygreater22 "+year);
							cf.ShowToast("Please enter the Date in valid format.",0);
							spnyear[1].requestFocus();
							chkglobal = true;
							chkstatus[1] = "false";
						}
						}catch(Exception e){}
					} else {
						chkstatus[1] = "true";
					}

				}
			}
			
			if ((roofmetal == "1" || chk_rctype[2].isChecked() == true)	&& (chkglobal == false)) {
				permit3 = edtroofpermit[2].getText().toString();
				product3 = edtroofproduct[2].getText().toString();
				year3 = spnyear[2].getSelectedItem().toString();

				if (("".equals(permit3)) && ("".equals(product3)) && ("--Select--".equals(year3)) && (chknoinfoprovide[2].isChecked() == false)) {
					cf.ShowToast("Please select any one of the Required value for Metal.",0);
					chkglobal = true;
					chkstatus[2] = "false";
				} else {

					if (!"".equals(year3) && !"--Select--".equals(year3) && !"".equals(permit3)) {
						try{
						int year = Integer.parseInt(year3);
						if (checkpermitdategraterthanbuild(year3,permit3) == true) {
							chkdatestatus[2] = "true";
						} else {
							chkglobal = true;
							chkdatestatus[2] = "false";
							edtroofpermit[2].setEnabled(true);
							edtroofpermit[2].setText("");
							//edtroofpermit[2].requestFocus();
						}
						}catch(Exception e){}
					} else {
						chkdatestatus[2] = "true";
					}
System.out.println("year3"+year3+" "+Current_year);
					if (!"".equals(year3) && !"--Select--".equals(year3)) {
						try{
						int year = Integer.parseInt(year3);
						if (year >= 1990 && year <= Current_year) {
							yearvalmetal = year3;
							chkstatus[2] = "true";
						} else {
							System.out.println("ygreater333 "+year);
							cf.ShowToast("Please enter the Date in valid format.",0);
							spnyear[2].requestFocus();
							chkglobal = true;
							chkstatus[2] = "false";
						}
						}catch(Exception e){}
					} else {
						chkstatus[2] = "true";
					}
				}
			}
			if ((roofbuiltup == "1" || chk_rctype[3].isChecked() == true) && (chkglobal == false)) {
				permit4 = edtroofpermit[3].getText().toString();
				product4 = edtroofproduct[3].getText().toString();
				year4 = spnyear[3].getSelectedItem().toString();

				if (("".equals(permit4)) && ("".equals(product4)) && ("--Select--".equals(year4)) && (chknoinfoprovide[3].isChecked() == false)) {
					cf.ShowToast("Please select any one of the Required value for Built Up.",0);
					chkglobal = true;
					chkstatus[3] = "false";
				} else {

					if (!"".equals(year4) && !"--Select--".equals(year4) && !"".equals(permit4)) {
						try{
						int year = Integer.parseInt(year4);
						if (checkpermitdategraterthanbuild(year4,permit4) == true) {
							chkdatestatus[3] = "true";
						} else {
							chkglobal = true;
							chkdatestatus[3] = "false";
							edtroofpermit[3].setEnabled(true);
							edtroofpermit[3].setText("");
							//edtroofpermit[3].requestFocus();
						}
						}catch(Exception e){}
					} else {
						chkdatestatus[3] = "true";
					}
					System.out.println("year4"+year4+" "+Current_year);
					if (!"".equals(year4)
							&& !"--Select--".equals(year4)) {
						try{
						int year = Integer.parseInt(year4);
						if (year >= 1990 && year <= Current_year) {
							yearvalbuiltup = year4;
							chkstatus[3] = "true";
						} else {
							System.out.println("ygreater444 "+year);
							cf.ShowToast("Please enter the Date in valid format.",0);
							spnyear[3].requestFocus();
							chkglobal = true;
							chkstatus[3] = "false";
						}
						}catch(Exception e){}
					} else {
						chkstatus[3] = "true";
					}
				}
			}
			
			if ((roofmembrane == "1" || chk_rctype[4].isChecked() == true)	&& (chkglobal == false)) {
				permit5 = edtroofpermit[4].getText().toString();
				product5 = edtroofproduct[4].getText().toString();
				year5 = spnyear[4].getSelectedItem().toString();

				if (("".equals(permit5)) && ("".equals(product5)) && ("--Select--".equals(year5)) && (chknoinfoprovide[4].isChecked() == false)) {
					cf.ShowToast("Please select any one of the Required value for Membrane.",0);
					chkglobal = true;
					chkstatus[4] = "false";
				} else {

					if (!"".equals(year5)	&& !"--Select--".equals(year5)	&& !"".equals(permit5)) {
						try{
						int year = Integer.parseInt(year5);
						if (checkpermitdategraterthanbuild(year5,permit5) == true) {
							chkdatestatus[4] = "true";
						} else {
							chkglobal = true;
							chkdatestatus[4] = "false";
							edtroofpermit[4].setEnabled(true);
							edtroofpermit[4].setText("");
							//edtroofpermit[4].requestFocus();
						}
						}catch(Exception e){}
					} else {
						chkdatestatus[4] = "true";
					}
					System.out.println("year5"+year5);
					if (!"".equals(year5)
							&& !"--Select--".equals(year5)) {
						try{
						int year = Integer.parseInt(year5);
						if (year >= 1990 && year <= Current_year) {
							yearvalmemb = year5;
							chkstatus[4] = "true";
						} else {
							System.out.println("ygreater 555 "+year);
							cf.ShowToast("Please enter the Date in valid format.",0);
							spnyear[4].requestFocus();
							chkglobal = true;
							chkstatus[4] = "false";
						}
						}catch(Exception e){}
					} else {
						chkstatus[4] = "true";
					}
				}
			}
			if ((roofother == "1" || chk_rctype[5].isChecked() == true)	&& (chkglobal == false)) {
				permit6 = edtroofpermit[5].getText().toString();
				product6 = edtroofproduct[5].getText().toString();
				year6 = spnyear[5].getSelectedItem().toString();
				othercovertext = ((EditText)findViewById(R.id.edother)).getText().toString();

				if (("".equals(permit6))
						&& ("".equals(product6))
						&& ("--Select--".equals(year6))
						&& (chknoinfoprovide[5].isChecked() == false)) {
					cf.ShowToast("Please select any one of the Required value for Other.",0);
					//cf.setFocus(((EditText)findViewById(R.id.edother)));
					chkglobal = true;
					chkstatus[5] = "false";
				} else {
					System.out.println("year6"+year6);
					if (!"".equals(year6) && !"--Select--".equals(year6)
							&& !"".equals(permit6)) {
						try{
						int year = Integer.parseInt(year6);
						if (checkpermitdategraterthanbuild(year6,
								permit6) == true) {
							chkdatestatus[5] = "true";
						} else {
							chkglobal = true;
							chkdatestatus[5] = "false";
							edtroofpermit[5].setEnabled(true);
							edtroofpermit[5].setText("");
							//edtroofpermit[5].requestFocus();
						}
						}catch(Exception e){}
					} else {
						chkdatestatus[5] = "true";
					}

					if (("".equals(othercovertext.trim()) && (chkglobal == false))) {						
						cf.ShowToast("Please enter the other text under Roof Covering Type.",0);
						cf.setFocus(((EditText)findViewById(R.id.edother)));
						chkstatus[6] = "false";
						chkglobal = true;
					} else {
						chkstatus[6] = "true";
					}

					if (!"".equals(year6) && !"--Select--".equals(year6)) {
						try{
						int year = Integer.parseInt(year6);
						if (year >= 1990 && year <= Current_year) {
							yearvalother = year6;
							chkstatus[5] = "true";
						} else {
							System.out.println("ygreater 6666"+year);
							cf.ShowToast("Please enter the Date in valid format.",0);
							spnyear[5].requestFocus();
							chkglobal = true;
							chkstatus[5] = "false";
						}
						}catch(Exception e){}
					} else {
						chkstatus[5] = "true";
					}
				}
			} else {
				chkstatus[5] = "true";
				chkstatus[6] = "true";
			}
			if (chk_rctype[0].isChecked() || chk_rctype[1].isChecked() || (chk_rctype[2].isChecked() || chk_rctype[3].isChecked() == true) 
					|| chk_rctype[4].isChecked() || chk_rctype[5].isChecked()) {

				if ((!roofcoveroption[0].isChecked()) && (!roofcoveroption[1].isChecked()) 	&& (!roofcoveroption[2].isChecked())	&& (!roofcoveroption[3].isChecked() && (chkglobal == false))) {
					cf.ShowToast("Please select the option for Roof Cover.",0);
					chkglobal = true;

				} else if (rccommentsval.trim().equals("") && (chkglobal == false)) {
					cf.ShowToast("Please enter the Roof Cover Comments.",0);
					cf.setFocus(((EditText)findViewById(R.id.rccomment)));
					chkglobal = true;

				} else if ((!radio_most[0].isChecked())
						&& (!radio_most[1].isChecked())
						&& (!radio_most[2].isChecked())
						&& (!radio_most[3].isChecked())
						&& (!radio_most[4].isChecked())
						&& (!radio_most[5].isChecked() && (chkglobal == false))) {
					cf.ShowToast("Please select Predominant for Roof Covering Type.",0);
					chkglobal = true;

				} else {
					if ((chkstatus[0] == "true") && (chkstatus[1] == "true")
							&& (chkstatus[2] == "true")
							&& (chkstatus[3] == "true")
							&& (chkstatus[4] == "true")
							&& (chkstatus[5] == "true")
							&& (chkstatus[6] == "true")
							&& (chkdatestatus[0] == "true")
							&& (chkdatestatus[1] == "true")
							&& (chkdatestatus[2] == "true")
							&& (chkdatestatus[3] == "true")
							&& (chkdatestatus[4] == "true")
							&& (chkdatestatus[5] == "true")
							&& (chkglobal = true)) {
						insertdata();
					}
				}
			}

			else {
				cf.ShowToast("Please select Roof Covering Type.",0);
				chkglobal = true;
			}		
			break;
		}
	}
	
	private void RC_suboption() {
		// TODO Auto-generated method stub
		if(roofcoveroption[0].isChecked()){tmp ="Meets FBC(PD -03/2/02) or Original (after 2004)";}
		else if(roofcoveroption[1].isChecked()){tmp ="Meets SFBC (PD 09/01/1994) or Post 1997";}
		else if(roofcoveroption[2].isChecked()){tmp ="One or more does not meet A or B";}
		else if(roofcoveroption[3].isChecked()){tmp ="Meets FBC(PD -03/2/02) or Original (after 2004)";}   	
		else {tmp="";}
	}
	private void insertdata() {
		// TODO Auto-generated method stub
		try
		{
		if ((chkstatus[0] == "true") && (chkstatus[1] == "true")
				&& (chkstatus[2] == "true") && (chkstatus[3] == "true")
				&& (chkstatus[4] == "true") && (chkstatus[5] == "true")
				&& (chkstatus[6] == "true")) {
			
			if (chknoinfoprovide[0].isChecked() == true) {	yearinfo1 = 1;} else { yearinfo1 = 0; }
			if (chknoinfoprovide[1].isChecked() == true) {	yearinfo2 = 1;} else { yearinfo2 = 0; }
			if (chknoinfoprovide[2].isChecked() == true) {	yearinfo3 = 1;} else { yearinfo3 = 0; }
			if (chknoinfoprovide[3].isChecked() == true) {	yearinfo4 = 1;} else { yearinfo4 = 0; }
			if (chknoinfoprovide[4].isChecked() == true) {	yearinfo5 = 1;} else { yearinfo5 = 0; }
			if (chknoinfoprovide[5].isChecked() == true) {	yearinfo6 = 1;} else { yearinfo6 = 0; }
			updatecnt = "1";
			
					String RoofCoverValue_f = "";
					System.out.println("RoofCoverValue.length "+RoofCoverValue.length);
					for (int i = 0; i < RoofCoverValue.length; i++) {
						if (RoofCoverValue[i] != 0) {
							RoofCoverValue_f += String.valueOf(i + 1) + "^";
							System.out.println("RoofCoverValue_f "+RoofCoverValue_f);
						}
					}
					RoofCoverValue_f = RoofCoverValue_f.substring(0,RoofCoverValue_f.length() - 1);

					Cursor c2 = cf.arr_db.rawQuery("SELECT * FROM " + cf.quesroofcover
							+ " WHERE SRID='" + cf.selectedhomeid + "' and INSP_TYPEID='"+cf.identityval+"'", null);
					System.out.println("count "+c2.getCount());
					if(c2.getCount()==0)
					{
						System.out.println("INSERT INTO "
								+ cf.quesroofcover
								+ " (SRID,INSP_TYPEID,RoofCoverType,RoofCoverValue,RoofPreDominant,PermitApplnDate1,PermitApplnDate2,PermitApplnDate3,PermitApplnDate4,PermitApplnDate5,PermitApplnDate6,RoofCoverTypeOther,ProdApproval1,ProdApproval2,ProdApproval3,ProdApproval4,ProdApproval5,ProdApproval6,InstallYear1,InstallYear2,InstallYear3,InstallYear4,InstallYear5,InstallYear6,NoInfo1,NoInfo2,NoInfo3,NoInfo4,NoInfo5,NoInfo6)"
								+ "VALUES ('" + cf.selectedhomeid  + "','"+cf.identityval+"','"
								+ cf.encode(RoofCoverValue_f) + "','"
								+ cf.encode(rcvalue) + "','"
								+ predominant + "','"
								+ cf.encode(permit1) + "','"
								+ cf.encode(permit2)
								+ "','" + cf.encode(permit3)
								+ "','" + cf.encode(permit4)
								+ "','"
								+ cf.encode(permit5)
								+ "','" + cf.encode(permit6)
								+ "','" + cf.encode(othercovertext)
								+ "','"
								+ cf.encode(product1)
								+ "','"
								+ cf.encode(product2)
								+ "','" + cf.encode(product3)
								+ "','"
								+ cf.encode(product4)
								+ "','"
								+ cf.encode(product5)
								+ "','" + cf.encode(product6)
								+ "','" + cf.encode(spnyear[0].getSelectedItem().toString())
								+ "','" + cf.encode(spnyear[1].getSelectedItem().toString())
								+ "','" + cf.encode(spnyear[2].getSelectedItem().toString())
								+ "','" + cf.encode(spnyear[3].getSelectedItem().toString())
								+ "','" + cf.encode(spnyear[4].getSelectedItem().toString())
								+ "','" + cf.encode(spnyear[5].getSelectedItem().toString())
								+ "','" + yearinfo1 + "','" + yearinfo2 + "','"
								+ yearinfo3 + "','" + yearinfo4 + "','" + yearinfo5
								+ "','" + yearinfo6 + "')");
						
						cf.arr_db.execSQL("INSERT INTO "
								+ cf.quesroofcover
								+ " (SRID,INSP_TYPEID,RoofCoverType,RoofCoverValue,RoofPreDominant,PermitApplnDate1,PermitApplnDate2,PermitApplnDate3,PermitApplnDate4,PermitApplnDate5,PermitApplnDate6,RoofCoverTypeOther,ProdApproval1,ProdApproval2,ProdApproval3,ProdApproval4,ProdApproval5,ProdApproval6,InstallYear1,InstallYear2,InstallYear3,InstallYear4,InstallYear5,InstallYear6,NoInfo1,NoInfo2,NoInfo3,NoInfo4,NoInfo5,NoInfo6)"
								+ "VALUES ('" + cf.selectedhomeid  + "','"+cf.identityval+"','"
								+ cf.encode(RoofCoverValue_f) + "','"
								+ cf.encode(rcvalue) + "','"
								+ predominant + "','"
								+ cf.encode(permit1) + "','"
								+ cf.encode(permit2)
								+ "','" + cf.encode(permit3)
								+ "','" + cf.encode(permit4)
								+ "','"
								+ cf.encode(permit5)
								+ "','" + cf.encode(permit6)
								+ "','" + cf.encode(othercovertext)
								+ "','"
								+ cf.encode(product1)
								+ "','"
								+ cf.encode(product2)
								+ "','" + cf.encode(product3)
								+ "','"
								+ cf.encode(product4)
								+ "','"
								+ cf.encode(product5)
								+ "','" + cf.encode(product6)
								+ "','" + cf.encode(yearvalasp)
								+ "','" + cf.encode(yearvalconc)
								+ "','" + cf.encode(yearvalmetal)
								+ "','" + cf.encode(yearvalbuiltup)
								+ "','" + cf.encode(yearvalmemb)
								+ "','" + cf.encode(yearvalother)
								+ "','" + yearinfo1 + "','" + yearinfo2 + "','"
								+ yearinfo3 + "','" + yearinfo4 + "','" + yearinfo5
								+ "','" + yearinfo6 + "')");
					}
					else
					{
						if (roofasphalt == "1" 	|| chk_rctype[0].isChecked() || chkstatus1[0] == "false") {
							System.out.println("UPDATE " + cf.quesroofcover
									+ " SET RoofCoverType='" + cf.encode(RoofCoverValue_f)
									+ "',RoofCoverValue='" + rcvalue
									+ "',RoofPreDominant='" + predominant
									+ "',PermitApplnDate1='" + cf.encode(permit1)
									+ "',ProdApproval1 ='"
									+ cf.encode(product1)
									+ "',InstallYear1='" + yearvalasp
									+ "',NoInfo1='" + yearinfo1 + "'"
									+ " WHERE SRID ='" + cf.selectedhomeid+ "' and INSP_TYPEID='"+cf.identityval+"'");
							cf.arr_db.execSQL("UPDATE " + cf.quesroofcover
									+ " SET RoofCoverType='" + cf.encode(RoofCoverValue_f)
									+ "',RoofCoverValue='" + rcvalue
									+ "',RoofPreDominant='" + predominant
									+ "',PermitApplnDate1='" + cf.encode(permit1)
									+ "',ProdApproval1 ='"
									+ cf.encode(product1)
									+ "',InstallYear1='" + spnyear[0].getSelectedItem().toString() 
									+ "',NoInfo1='" + yearinfo1 + "'"
									+ " WHERE SRID ='" + cf.selectedhomeid+ "' and INSP_TYPEID='"+cf.identityval+"'");

						}
						if (roofconcrete == "1" || chk_rctype[1].isChecked() || chkstatus1[1] == "false") {
							System.out.println("update "+"UPDATE " + cf.quesroofcover
									+ " SET RoofCoverType='" + cf.encode(RoofCoverValue_f)
									+ "',RoofCoverValue='" + rcvalue
									+ "',RoofPreDominant='" + predominant
									+ "',PermitApplnDate2='" + cf.encode(permit2)
									+ "',ProdApproval2 ='"
									+ cf.encode(product2)
									+ "',InstallYear2='" + yearvalconc
									+ "',NoInfo2='" + yearinfo2 + "'"
									+ " WHERE SRID ='" + cf.selectedhomeid + "' and INSP_TYPEID='"+cf.identityval+"'");
							cf.arr_db.execSQL("UPDATE " + cf.quesroofcover
									+ " SET RoofCoverType='" + cf.encode(RoofCoverValue_f)
									+ "',RoofCoverValue='" + rcvalue
									+ "',RoofPreDominant='" + predominant
									+ "',PermitApplnDate2='" + cf.encode(permit2)
									+ "',ProdApproval2 ='"
									+ cf.encode(product2)
									+ "',InstallYear2='" + spnyear[1].getSelectedItem().toString()
									+ "',NoInfo2='" + yearinfo2 + "'"
									+ " WHERE SRID ='" + cf.selectedhomeid + "' and INSP_TYPEID='"+cf.identityval+"'");

						}
						if (roofmetal == "1" || chk_rctype[2].isChecked() || chkstatus1[2] == "false") 
						{
							cf.arr_db.execSQL("UPDATE " + cf.quesroofcover
									+ " SET RoofCoverType='" + cf.encode(RoofCoverValue_f)
									+ "',RoofCoverValue='" + rcvalue
									+ "',RoofPreDominant='" + predominant
									+ "',PermitApplnDate3='" + cf.encode(permit3)
									+ "',ProdApproval3 ='"
									+ cf.encode(product3)
									+ "',InstallYear3='" + spnyear[2].getSelectedItem().toString()
									+ "',NoInfo3='" + yearinfo3 + "'"
									+ " WHERE SRID ='" + cf.selectedhomeid + "' and INSP_TYPEID='"+cf.identityval+"'");

						}
						if (roofbuiltup == "1"	|| chk_rctype[3].isChecked() || chkstatus1[3] == "false") {
							cf.arr_db.execSQL("UPDATE " + cf.quesroofcover
									+ " SET RoofCoverType='" + cf.encode(RoofCoverValue_f)
									+ "',RoofCoverValue='" + rcvalue
									+ "',RoofPreDominant='" + predominant
									+ "',PermitApplnDate4='" + cf.encode(permit4)
									+ "',ProdApproval4 ='"
									+ cf.encode(product4)
									+ "',InstallYear4='" + spnyear[3].getSelectedItem().toString()
									+ "',NoInfo4='" + yearinfo4 + "'"
									+ " WHERE SRID ='" + cf.selectedhomeid + "' and INSP_TYPEID='"+cf.identityval+"'");

						}
						if (roofmembrane == "1"  || chk_rctype[4].isChecked() || chkstatus1[4] == "false") {
							cf.arr_db.execSQL("UPDATE " + cf.quesroofcover
									+ " SET RoofCoverType='" + cf.encode(RoofCoverValue_f)
									+ "',RoofCoverValue='" + rcvalue
									+ "',RoofPreDominant='" + predominant
									+ "',PermitApplnDate5='" + cf.encode(permit5)
									+ "',ProdApproval5='"
									+ cf.encode(product5)
									+ "',InstallYear5='" + spnyear[4].getSelectedItem().toString()
									+ "',NoInfo5='" + yearinfo5 + "'"
									+ " WHERE SRID ='" + cf.selectedhomeid + "' and INSP_TYPEID='"+cf.identityval+"'");
						}
						if (roofother == "1" || chk_rctype[5].isChecked()	|| chkstatus1[5] == "false") {
							cf.arr_db.execSQL("UPDATE " + cf.quesroofcover
									+ " SET RoofCoverType='" + cf.encode(RoofCoverValue_f)
									+ "',RoofCoverValue='" + rcvalue
									+ "',RoofPreDominant='" + predominant
									+ "',PermitApplnDate6='" + cf.encode(permit6)
									+ "',ProdApproval6='"
									+ cf.encode(product6)
									+ "',InstallYear6='" + spnyear[5].getSelectedItem().toString()
									+ "',NoInfo6='" + yearinfo6
									+ "',RoofCoverTypeOther='"
									+ cf.encode(othercovertext) + "'"
									+ " WHERE SRID ='" + cf.selectedhomeid + "' and INSP_TYPEID='"+cf.identityval+"'");
						}
					}
					Cursor c3 = cf.SelectTablefunction(cf.Wind_QuesCommtbl, "where fld_srid='"+ cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'");
					if (c3.getCount() == 0) 
					{
						cf.arr_db.execSQL("INSERT INTO "
								+ cf.Wind_QuesCommtbl
								+ " (fld_srid,insp_typeid,fld_buildcodecomments,fld_roofcovercomments,fld_roofdeckcomments,fld_roofwallcomments," +
								"fld_roofgeometrycomments,fld_swrcomments,fld_openprotectioncomments,fld_wallconscomments,fld_insoverallcomments )"
								+ "VALUES('"+cf.selectedhomeid+"','"+cf.identityval+"','','"+ cf.encode(((EditText)findViewById(R.id.rccomment)).getText().toString())	+ "','','','','','','','')");
					
					System.out.println("INSERT INTO "
								+ cf.Wind_QuesCommtbl
								+ " (fld_srid,insp_typeid,fld_buildcodecomments,fld_roofcovercomments,fld_roofdeckcomments,fld_roofwallcomments," +
								"fld_roofgeometrycomments,fld_swrcomments,fld_openprotectioncomments,fld_wallconscomments,fld_insoverallcomments )"
								+ "VALUES('"+cf.selectedhomeid+"','"+cf.identityval+"','','"+ cf.encode(((EditText)findViewById(R.id.rccomment)).getText().toString())	+ "','','','','','','','')");
					
					}
					else
					{
						cf.arr_db.execSQL("UPDATE " + cf.Wind_QuesCommtbl
								+ " SET fld_roofcovercomments='"+ cf.encode(((EditText)findViewById(R.id.rccomment)).getText().toString())	+ "' WHERE fld_srid ='" + cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'");
					}
					cf.ShowToast("Roof Cover Details has been saved successfully.", 0);
					cf.gotoclass(cf.identityval, WindMitRoofDeck.class);
		     }
		     else
		     {
			
		      }
		}catch(Exception e)
		{
			System.out.println("Ecxcc "+e.getMessage());
		}
	}
	private void dateiconclck()
	{
		chkboxvalidation[0] = "";
		chkboxvalidation[1] = "";
		chkboxvalidation[2] = "";
		chkboxvalidation[3] = "";
		chkboxvalidation[4] = "";
		chkboxvalidation[5] = "";
		showDialog(DATE_DIALOG_ID);
	}
	protected void auto_select(int select, int radioslect) {
		// TODO Auto-generated method stub
		try {
			String year = "", product = "", permit = "", stat;
			CheckBox ch = null;

			switch (select) {
			case 1:
				year = ((Spinner)findViewById(R.id.spnryr1)).getSelectedItem().toString();
				ch = ((CheckBox)findViewById(R.id.chknoinfo1));
				product = ((EditText)findViewById(R.id.edproduct1)).getText().toString().trim();
				permit =  ((EditText)findViewById(R.id.edpermitappdate1)).getText().toString();
				stat = "true";
				break;
			case 2:
				year = ((Spinner)findViewById(R.id.spnryr2)).getSelectedItem().toString();
				ch = ((CheckBox)findViewById(R.id.chknoinfo2));
				product = ((EditText)findViewById(R.id.edproduct2)).getText().toString().trim();
				permit =  ((EditText)findViewById(R.id.edpermitappdate2)).getText().toString();
				stat = "true";
				break;
			case 3:
				year = ((Spinner)findViewById(R.id.spnryr3)).getSelectedItem().toString();
				ch = ((CheckBox)findViewById(R.id.chknoinfo3));
				product = ((EditText)findViewById(R.id.edproduct3)).getText().toString().trim();
				permit =  ((EditText)findViewById(R.id.edpermitappdate3)).getText().toString();
				stat = "true";
				break;
			case 4:
				year = ((Spinner)findViewById(R.id.spnryr4)).getSelectedItem().toString();
				ch = ((CheckBox)findViewById(R.id.chknoinfo4));
				product = ((EditText)findViewById(R.id.edproduct4)).getText().toString().trim();
				permit =  ((EditText)findViewById(R.id.edpermitappdate4)).getText().toString();
				stat = "true";
				break;
			case 5:
				year = ((Spinner)findViewById(R.id.spnryr5)).getSelectedItem().toString();
				ch = ((CheckBox)findViewById(R.id.chknoinfo5));
				product = ((EditText)findViewById(R.id.edproduct5)).getText().toString().trim();
				permit =  ((EditText)findViewById(R.id.edpermitappdate5)).getText().toString();
				stat = "true";
				break;
			case 6:
				year = ((Spinner)findViewById(R.id.spnryr6)).getSelectedItem().toString();
				ch = ((CheckBox)findViewById(R.id.chknoinfo6));
				product = ((EditText)findViewById(R.id.edproduct6)).getText().toString().trim();
				permit =  ((EditText)findViewById(R.id.edpermitappdate6)).getText().toString();
				stat = "true";
				break;
			}

			int i1 = 0, i2 = 0, j = 0, j1 = 0, j2 = 0, install = 0;
			boolean[] z = { false, false, false, false };
			String re;
			if (!permit.equals("")) {
				i1 = permit.indexOf("/");
				String result = permit.substring(0, i1);
				i2 = permit.lastIndexOf("/");
				String result1 = permit.substring(i1 + 1, i2);
				String result2 = permit.substring(i2 + 1);
				result2 = result2.trim();
				try{
				j = Integer.parseInt(result2);
				j1 = Integer.parseInt(result1);
				j2 = Integer.parseInt(result);
				}catch(Exception e){}
			} else {
				j = 0;
				j1 = 0;
				j2 = 0;
			}
			if (!year.equals("") && !year.equals("--Select--")) {
				try{
				install = Integer.parseInt(year);
				}catch(Exception e){}
			} else {
				install = 0;
			}
			if (((j >= 2002) && (j2 >= 3 || j > 2002) && (j1 > 1 || j > 2002 || j2 > 3))
					|| (install >= 2004)) {

				boolean tmp[];

				z = set_auto_select.get(String.valueOf(select)).clone();
				set_auto_select.remove(String.valueOf(select));

				z[0] = true;
				z[1] = false;
				z[2] = false;

				set_auto_select.put(String.valueOf(select), z);

			} else if ((j <= 2002 && j >= 1994)
					&& ((j2 <= 3 || j < 2002) && (j2 >= 9 || j > 1994))
					&& ((j1 < 2 || j < 2002 || j2 < 3) && (j1 > 1 || j > 1994 || j2 > 9))
					&& ((count_value.toLowerCase().trim().equals("miami-dade")) || (count_value
							.toLowerCase().trim().equals("broward")))
					&& !(j == 0 && j1 == 0 && j2 == 0)) {
				z = set_auto_select.get(String.valueOf(select)).clone();
				z[1] = true;
				z[2] = false;
				z[0] = false;
				set_auto_select.put(String.valueOf(select), z);
			} else if (product.equals("")
					&& ((j <= 1994) && (j2 <= 9 || j < 1994) && (j1 <= 1
							|| j < 1994 || j2 < 9))) {
				z = set_auto_select.get(String.valueOf(select)).clone();
				z[2] = true;
				z[1] = false;
				z[0] = false;
				set_auto_select.put(String.valueOf(select), z);

			} else {
				z = set_auto_select.get(String.valueOf(select)).clone();
				z[2] = false;
				z[1] = false;
				z[0] = false;
				set_auto_select.put(String.valueOf(select), z);
			}

			if (radioslect == 1) {
				select_radio();
			} else {
				if (ch.isChecked()) {
					z = set_auto_select.get(String.valueOf(select)).clone();
					z[3] = true;
					set_auto_select.put(String.valueOf(select), z);
				} else {
					z = set_auto_select.get(String.valueOf(select)).clone();
					z[3] = false;
					set_auto_select.put(String.valueOf(select), z);

				}

			}

		} catch (Exception e) {
			//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofCover.this +" problem in autoslecting roof cover option on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

		}
	}
	private void select_radio() {
		// TODO Auto-generated method stub
		// if()

		boolean tmp[], sta = true, stb = true, stc = true, std = true;
		for (Iterator it = set_auto_select.keySet().iterator(); it.hasNext();) {
			String key = it.next().toString();
			tmp = set_auto_select.get(key).clone();
			if ((tmp[3] == false)) {
				std = false;

			} else {
				tmp[0] = false;
				tmp[1] = false;
				tmp[2] = false;
			}
			if ((tmp[0] == false)) {
				sta = false;
			}
			if ((tmp[1] == false)) {
				stb = false;
			}
			if ((tmp[2] == false) || tmp[1] == true || tmp[0] == true
					|| tmp[3] == true) {
				stc = false;
			}

		}
		if (sta && !stb && !std && !stc) 
		{
			rcvalue = "1";
			hidecontrols();
			roofcoveroption[0].setChecked(true);
			((EditText)findViewById(R.id.rccomment)).setText("This home was verified as meeting the requirements of selection A of the OIR B1 -1802 Question 2, Roof Covering.");
		} else if (stb && !sta && !std && !stc) 
		{
			rcvalue = "2";
			hidecontrols();
			roofcoveroption[1].setChecked(true);
			((EditText)findViewById(R.id.rccomment)).setText("This home was verified as meeting the requirements of selection B of the OIR B1 -1802 Question 2 Roof Covering.");
			
		} else if (stc && !stb && !sta && !std) 
		{
			rcvalue = "3";
			hidecontrols();
			roofcoveroption[2].setChecked(true);
			((EditText)findViewById(R.id.rccomment)).setText("This home was verified as meeting the requirements of selection C of the OIR B1 -1802 Question 2 Roof Covering.");
			
		} else if (std && !stb && !sta && !stc) {
			rcvalue = "4";
			hidecontrols();
			roofcoveroption[3].setChecked(true);
			((EditText)findViewById(R.id.rccomment)).setText("This home was verified as meeting the requirements of selection D of the OIR B1 -1802 Question 2, Roof Covering.");
			
		} else {
			hidecontrols();
			((EditText)findViewById(R.id.rccomment)).setText("");

		}
	}
	private void hidecontrols()
	{
		 for(int i=0;i<4;i++)
    	 {
    		roofcoveroption[i].setChecked(false);
    	 }
	}
	
	private void setPreDominant(int i, boolean b1) {
		if (b1) {
			for (int k = 0; k <= i; k++) {
				if (i == k) {
					radio_most[i].setEnabled(true);

				}
			}
		} else {
			for (int k = 0; k <= i; k++) {
				if (i == k) {
					radio_most[i].setEnabled(false);
					radio_most[i].setChecked(false);
					if (predominant == i + 1) {
						predominant = 0;
					}
				}
			}
		}
	}

	private boolean checkpermitdategraterthanbuild(String yearbuilt3,
			String permitDate3) {
		// TODO Auto-generated method stub
		int i1 = permitDate3.indexOf("/");
		String result = permitDate3.substring(0, i1);
		int i2 = permitDate3.lastIndexOf("/");
		String result1 = permitDate3.substring(i1 + 1, i2);
		String result2 = permitDate3.substring(i2 + 1);
		result2 = result2.trim();
		
		int j = Integer.parseInt(result2);
		final int j1 = Integer.parseInt(result1);
		final int j2 = Integer.parseInt(result);
		yearbuilt3 = yearbuilt3.trim();
		int yr = Integer.parseInt(yearbuilt3);
		
		if (yr < j) {
			cf.ShowToast("Year of Installation should be greater than the Permit Application Year.",0);
			return false;
		} else {
			return true;
		}
	}

	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			System.out.println("Current_year "+cf.mYear+ " "+cf.mMonth+ " "+cf.mDay);
			cf.mYear = year;
			cf.mMonth = monthOfYear;
			cf.mDay = dayOfMonth;
			
			
			if (chkboxvalidation[0].equals("1")) {
				edtroofpermit[0].setText(new StringBuilder()
						// Month is 0 based so add 1
						.append(cf.mMonth + 1).append("/").append(cf.mDay)
						.append("/").append(cf.mYear).append(" "));
			} else if (chkboxvalidation[1].equals("1")) {
				edtroofpermit[1].setText(new StringBuilder()
						// Month is 0 based so add 1
						.append(cf.mMonth + 1).append("/").append(cf.mDay)
						.append("/").append(cf.mYear).append(" "));
			} else if (chkboxvalidation[2].equals("1")) {
				edtroofpermit[2].setText(new StringBuilder()
						// Month is 0 based so add 1
						.append(cf.mMonth + 1).append("/").append(cf.mDay)
						.append("/").append(cf.mYear).append(" "));
			} else if (chkboxvalidation[3].equals("1")) {
				edtroofpermit[3].setText(new StringBuilder()
						// Month is 0 based so add 1
						.append(cf.mMonth + 1).append("/").append(cf.mDay)
						.append("/").append(cf.mYear).append(" "));
			} else if (chkboxvalidation[4].equals("1")) {
				edtroofpermit[4].setText(new StringBuilder()
						// Month is 0 based so add 1
						.append(cf.mMonth + 1).append("/").append(cf.mDay)
						.append("/").append(cf.mYear).append(" "));
			} else if (chkboxvalidation[5].equals("1")) {
				edtroofpermit[5].setText(new StringBuilder()
						// Month is 0 based so add 1
						.append(cf.mMonth + 1).append("/").append(cf.mDay)
						.append("/").append(cf.mYear).append(" "));
			} else {
			}

		}
	};
	public int getkeyarray(String[] arr, String value) {
		if (value.trim().equals("")) {
			return 0;
		}
		for (int i = 1; i < arr.length; i++) {
			if (arr[i].equals(value)) {
				return i;
			}

		}
		return 0;
	}
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			System.out.println("DATE_DIALOG_ID ");
			return new DatePickerDialog(this, mDateSetListener, cf.mYear, cf.mMonth,
					cf.mDay);

		}
		return null;
	}
	
	class clicker1 implements OnClickListener {
		public void onClick(View v) {

			if (v == ((TextView)findViewById(R.id.miamedadeprodapprlink)))
			{
					if (cf.isInternetOn() == true) {
						weblink = true;
						setContentView(R.layout.weblink);
						WebView webview1 = (WebView) findViewById(R.id.webview);
						webview1.getSettings().setJavaScriptEnabled(true);
						webview1.loadUrl("http://www.miamidade.gov/building/pc-search_app.asp");
					//cf.goclass(30);
					} else {
					alert();
					}
			}
			else if (v == ((TextView)findViewById(R.id.fbcprodapprlink))) {
					if (cf.isInternetOn() == true) {
						weblink = true;
						setContentView(R.layout.weblink);						
						WebView webview1 = (WebView) findViewById(R.id.webview);
						webview1.getSettings().setJavaScriptEnabled(true);
						webview1.loadUrl("http://www.floridabuilding.org/pr/pr_app_srch.aspx");
					//cf.goclass(30);
					} else {
					alert();
					}

			}
		}
	}
	public void alert() {
		Builder builder = new AlertDialog.Builder(WindMitRoofCover.this);
		builder.setTitle("No Internet Connection");
		builder.setMessage("Please Turn On Wifi");
		builder.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {

			}
		});
		builder.show();
	}

	 public boolean onKeyDown(int keyCode, KeyEvent event) {
	 		// replaces the default 'Back' button action
	 		if (keyCode == KeyEvent.KEYCODE_BACK) {
	 			if(weblink==true)
	 			{
	 				cf.gotoclass(cf.identityval, WindMitRoofCover.class);	
	 			}
	 			else
	 			{
	 				cf.gotoclass(cf.identityval, WindMitBuildCode.class);
	 			}
	 			return true;
	 		}
	 		
	 		return super.onKeyDown(keyCode, event);
	 	}
	 /**on activity result for the load comments**/
	 @Override
	 		protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	 		try
	 		{
	 			if(requestCode==cf.loadcomment_code)
	 			{
	 				load_comment=true;
	 				if(resultCode==RESULT_OK)
	 				{
	 					String rccomm = ((EditText)findViewById(R.id.rccomment)).getText().toString();	 
	 					((EditText)findViewById(R.id.rccomment)).setText(rccomm +" "+data.getExtras().getString("Comments"));
		 			}
	 			}
	 		}
	 		catch (Exception e) {
	 			// TODO: handle exception
	 			System.out.println("the erro was "+e.getMessage());
	 		}
	 	}/**on activity result for the load comments ends**/
}
