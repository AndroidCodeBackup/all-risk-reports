package idsoft.inspectiondepot.ARR;

import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;


public class SummaryFunctions {
	TextView FE,RE,LE,BE,AE,AP,OP,RP,EP,IP,GI,RAP,CW,CWO,HVAV,ELEC,PLUM,MF,CB,FB,Ext,FP,DP;
	TableLayout summaryTable;
	String separated[],All_insp_list[];
	Context con;
	Cursor c1;
	CommonFunctions cf;
	public String inspections="0",fdcomments="",insptypeid,sql,selinspname="",tmp="",singleinspname="";
	View v1;
	public SummaryFunctions(Context con) 
	{
		this.con =con;
		cf=new CommonFunctions(this.con);
	All_insp_list=cf.All_insp_list;
	}
	public void displayphotosummary(String selectedhomeid)
	{
		cf.CreateARRTable(83);
		cf.CreateARRTable(84);
		final Dialog dialog1 = new Dialog(this.con,android.R.style.Theme_Translucent_NoTitleBar);
		dialog1.getWindow().setContentView(R.layout.alertsummary);		
		summaryTable = (TableLayout)dialog1.findViewById(R.id.tblsummary);
		((TextView)dialog1.
				findViewById(R.id.txtbriefexplanation)).setText(Html.fromHtml("<font color=red><b>Note :</b> </font>"+"<b>FE</b> - Front Elevation, " 
	 			+ "<b>RE</b> - Right Elevation, " 
	 			+ "<b>BE</b> - Back Elevation, " 
				+ "<b>LE</b> - Left Elevation," + "<br/>"
				+ "<b>At.P</b> - Attic Photographs, " 
				+ "<b>Ad.P</b> - Additional Photographs, "
				+ "<b>OP</b> - Other Photographs,"+ "<br/>"
	 			+ "<b>RP</b> - Roof Photographs, "
				+ "<b>EP</b> - Exterior Photographs, "
				+ "<b>IP</b> - Internal Photographs,"+ "<br/>"
				+ "<b>GI</b> - Ground Images, " 
				+ "<b>RAP</b> - Roof and Attic Photographs, " 
	 			+ "<b>HV</b> - HVAC Photographs, " 
				+ "<b>Ele</b> - Electrical Photographs," + "<br/>"
				+ "<b>PL</b> - Plumbing Photographs, "
				+ "<b>DP</b> - Drywall Photographs, "
				+ "<b>IFP</b> - Interior Fixture Photographs," + "<br/>" 
	 			+ "<b>Et.P</b> - External Photographs, "
	 			+ "<b>FP</b> - Front Photo" + "<br/>"));
		
		 LayoutInflater inflater = (LayoutInflater)this.con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);		 
		 View h1 = (ViewGroup)inflater.inflate(R.layout.hvac_listview_header, null);
		 LinearLayout th= (LinearLayout)h1.findViewById(R.id.summary_hdr);th.setVisibility(cf.v1.VISIBLE);
		 TableLayout.LayoutParams lp = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
	 	 lp.setMargins(2, 0, 2, 2);
	 	
	 	summaryTable.removeAllViews();
	 	((ViewGroup) h1).removeAllViews();	 	
	 	summaryTable.addView(th,lp);
	 	summaryTable.setVisibility(View.VISIBLE);


		// TODO Auto-generated method stub
	   	 Cursor c =cf.SelectTablefunction(cf.Select_insp, " Where SI_srid='"+selectedhomeid+"' and SI_InspectionNames!=''");
	   	 if(c.getCount()>0)
	   	 {
	   		 c.moveToFirst();
	   		 selinspname = cf.decode(c.getString(c.getColumnIndex("SI_InspectionNames")));
	   		 System.out.println("selidfgdfgdfgnspname"+selinspname);
	   		 tmp =selinspname;
	   		if(tmp.contains(","))
			{
	   			tmp = tmp.replace(",","~");
	   			String arr[] = tmp.split("~");
				for(int i=0;i<arr.length;i++)
				{
					
					if(arr[i].contentEquals("9") || arr[i].contentEquals("11") || arr[i].contentEquals("13") || arr[i].contentEquals("18")
							|| arr[i].contentEquals("28") || arr[i].contentEquals("750") || arr[i].contentEquals("751"))
					{
						
					}
					else
					{
						tmp = tmp.replaceAll("\\b"+arr[i]+"\\b", "");
						 if(tmp.contains("~~"))
						 {
							 tmp = tmp.replaceAll("~~","~");	
						 }
					}
				}
				if(tmp.startsWith("~"))
				{
					tmp = tmp.substring(1, tmp.length());
				}
				if(tmp.endsWith("~"))
				{
					tmp = tmp.substring(0, tmp.length() - 1);
				}
				tmp = tmp.replace("~",",");
				
			}	
	   		System.out.println("tmpcustom="+tmp);
	   	 }
	  
	 	System.out.println("selinspname"+tmp);
	 	separated = tmp.split(",");
	 	System.out.println("length="+separated.length);
	 	
	 	
	 	 
	 	
		for (int i = 0; i < separated.length; i++) 
		{	
			View t1 =(View) inflater.inflate(R.layout.hvac_listview, null);
			LinearLayout t = (LinearLayout)t1.findViewById(R.id.summary_val);
			t.setVisibility(v1.VISIBLE);
			t.setId(44444+i);/// Set some id for further use
			
			
			
			FE= (TextView) t.findViewWithTag("txtFE");
			RE= (TextView) t.findViewWithTag("txtRE");
			LE= (TextView) t.findViewWithTag("txtLE");
			BE= (TextView) t.findViewWithTag("txtBE");
			AE= (TextView) t.findViewWithTag("txtAttiPhotos");
			AP= (TextView) t.findViewWithTag("txtAddiPhotos");
			OP= (TextView) t.findViewWithTag("txtOtherPhotos");
			RP= (TextView) t.findViewWithTag("txtRoofPhotos");
			EP= (TextView) t.findViewWithTag("txtExtPhotos");
			IP= (TextView) t.findViewWithTag("txtIntPhotos");
			GI= (TextView) t.findViewWithTag("txtGroImages");
			RAP= (TextView) t.findViewWithTag("txtRoofatticPhotos");
			DP= (TextView) t.findViewWithTag("txtDrywall");			
			HVAV= (TextView) t.findViewWithTag("txtHVAC");				
			ELEC= (TextView) t.findViewWithTag("txtElec");
			PLUM= (TextView) t.findViewWithTag("txtPlumbing");
			MF= (TextView) t.findViewWithTag("txtIntFix");
			FP= (TextView) t.findViewWithTag("txtFront");
			FB= (TextView) t.findViewWithTag("txtFeedback");
			Ext= (TextView) t.findViewWithTag("txtExterior");

			FE.setBackgroundResource(R.drawable.imgempty);
         	RE.setBackgroundResource(R.drawable.imgempty);
         	LE.setBackgroundResource(R.drawable.imgempty);
         	BE.setBackgroundResource(R.drawable.imgempty);
         	AE.setBackgroundResource(R.drawable.imgempty);
         	AP.setBackgroundResource(R.drawable.imgempty);
         	OP.setBackgroundResource(R.drawable.imgempty);
         	RP.setBackgroundResource(R.drawable.imgempty);
         	PLUM.setBackgroundResource(R.drawable.imgempty);
         	ELEC.setBackgroundResource(R.drawable.imgempty);
         	EP.setBackgroundResource(R.drawable.imgempty);
         	HVAV.setBackgroundResource(R.drawable.imgempty);
         	RAP.setBackgroundResource(R.drawable.imgempty);
         	GI.setBackgroundResource(R.drawable.imgempty);
         	DP.setBackgroundResource(R.drawable.imgempty);
         	IP.setBackgroundResource(R.drawable.imgempty);
         	MF.setBackgroundResource(R.drawable.imgempty);
         	FP.setBackgroundResource(R.drawable.imgempty);
         	FB.setBackgroundResource(R.drawable.imgunsaved);
         	Ext.setBackgroundResource(R.drawable.imgempty);
         	System.out.println("separated"+separated[i]);
         	getsingleinspectiotextname(separated[i]);    	System.out.println("singleinspname"+singleinspname);
         	String getelevtxtarr[] = getelevation(singleinspname); 		 	 
         	chkelevpresfornsp(getelevtxtarr);
         	
         	TextView txtinspname= (TextView) t.findViewWithTag("str1");
			txtinspname.setText(singleinspname);	
         	
			 try
         	 {/*
				 if(separated[i].equals("4"))
				 {
					 sql="Select distinct ARR_IM_Elevation from "+cf.ImageTable+" Where ARR_IM_SRID='"+selectedhomeid+"' and ARR_IM_Insepctiontype='"+separated[i]+"'";
				 }
				 else
				 {
					 sql="Select distinct ARR_IM_Elevation from "+cf.ImageTable+" Where ARR_IM_SRID='"+selectedhomeid+"'";
				 }*/
				 if(separated[i].equals("9"))
				 {
					 cf.getPolicyholderInformation(selectedhomeid);
					 	if(cf.Stories.equals("1") || cf.Stories.equals("2")  || cf.Stories.equals("3"))
						{
					 		separated[i]	="4";
						}
						else if(cf.Stories.equals("4") || cf.Stories.equals("5")  || cf.Stories.equals("6"))
						{
							separated[i]	="5";
						}
						else 
						{	
							separated[i]	="6";
						}	
				 }
				 else if(separated[i].equals("11"))
				 {
					 separated[i]	="7";
				 }
				 else if(separated[i].equals("13"))
				 {
					 separated[i]	="1";
				 }
				 else if(separated[i].equals("18"))
				 {
					 separated[i]	="8";
				 }
				 else if(separated[i].equals("28"))
				 {
					 separated[i]	="3";
				 }
				 else if(separated[i].equals("750"))
				 {
					 separated[i]	="2";
				 }
				 else if(separated[i].equals("751"))
				 {
					 separated[i]	="9";
				 }
				 
				 
				 
				 System.out.println("separated"+separated[i]);
				 sql="Select distinct ARR_IM_Elevation from "+cf.ImageTable+" Where ARR_IM_SRID='"+selectedhomeid+"' and ARR_IM_Insepctiontype='"+separated[i]+"'";
				 System.out.println("Select distinct ARR_IM_Elevation from "+cf.ImageTable+" Where ARR_IM_SRID='"+selectedhomeid+"' and ARR_IM_Insepctiontype='"+separated[i]+"'");
				 
				 c1= cf.arr_db.rawQuery(sql, null);
					System.out.println("c1=="+c1.getCount());
					if(c1.getCount()>0)
	         	    {
	         		  c1.moveToFirst();
	         		  do
	         		  {
		        		 insptypeid = c1.getString(c1.getColumnIndex("ARR_IM_Elevation"));System.out.println("insptypeid=="+insptypeid+"separated[i]="+separated[i]);
			         	 String getelevtxt = getelvationtext(Integer.parseInt(insptypeid),separated[i]);		         	
			         	System.out.println("getelevtxt=="+getelevtxt);
			         	if(getelevtxt.equals("Front Elevation"))
			         	{
			         		FE.setBackgroundResource(R.drawable.imgsaved);
			         	}
			         	if(getelevtxt.equals("Right Elevation"))
			         	{
			         		RE.setBackgroundResource(R.drawable.imgsaved);
			         	}
			         	if(getelevtxt.equals("Left Elevation"))
			         	{
			         		LE.setBackgroundResource(R.drawable.imgsaved);
			         	}
			         	if(getelevtxt.equals("Back Elevation"))
			         	{
			         		BE.setBackgroundResource(R.drawable.imgsaved);
			         	}
			         	if(getelevtxt.equals("Attic Photographs"))
			         	    {
			         		AE.setBackgroundResource(R.drawable.imgsaved);
			         	}
			         	if(getelevtxt.equals("Additional Photographs"))
			         	{
			         		AP.setBackgroundResource(R.drawable.imgsaved);
			         	}
			         	if(getelevtxt.equals("Other Photographs"))
			         	{
			         		OP.setBackgroundResource(R.drawable.imgsaved);
			         	}
			         	if(getelevtxt.equals("Roof Photographs"))
			         	{
			         		RP.setBackgroundResource(R.drawable.imgsaved);
			         	}
			         	if(getelevtxt.equals("Plumbing Photographs"))
			         	{
			         		PLUM.setBackgroundResource(R.drawable.imgsaved);
			         	}
			         	if(getelevtxt.equals("Electrical Photographs"))
			         	{
			         		ELEC.setBackgroundResource(R.drawable.imgsaved);
			         	}
			         	if(getelevtxt.equals("HVAC Photographs"))
			         	{
			         		HVAV.setBackgroundResource(R.drawable.imgsaved);
			         	}
			         	if(getelevtxt.equals("External Photographs"))
			         	{
			         		Ext.setBackgroundResource(R.drawable.imgsaved);
			         	}
			        	if(getelevtxt.equals("Copper wiring"))
			         	{
			         		CW.setBackgroundResource(R.drawable.imgsaved);
			         	}
			        	if(getelevtxt.equals("Copper wiring at outlets and switches"))
			         	{
			         		CWO.setBackgroundResource(R.drawable.imgsaved);
			         	}
			        	if(getelevtxt.equals("Roof and attic Photographs"))
			         	{
			        		RAP.setBackgroundResource(R.drawable.imgsaved);
			         	}
			        	if(getelevtxt.equals("Drywall Photographs"))
			         	{
			        		DP.setBackgroundResource(R.drawable.imgsaved);
			         	}
			        	if(getelevtxt.equals("Internal Photographs"))
			         	{
			         		IP.setBackgroundResource(R.drawable.imgsaved);
			         	}
			        	if(getelevtxt.equals("Interior Fixture Photographs"))
			         	{
			         		MF.setBackgroundResource(R.drawable.imgsaved);
			         	}			        	
			        	if(getelevtxt.equals("Exterior Photographs"))
			         	{
			         		EP.setBackgroundResource(R.drawable.imgsaved);
			         	}
			        	if(getelevtxt.equals("Front Photo"))
			         	{
			        		FP.setBackgroundResource(R.drawable.imgsaved);
			         	}
			        	
	         		 }while(c1.moveToNext());
	         	 }
         	 }
         	catch(Exception e)
         	{
         		System.out.println("sdfdf"+e.getMessage());
         	}
			    String sql1="Select * from "+cf.FeedBackInfoTable+" Where ARR_FI_Srid='"+selectedhomeid+"' and ARR_FI_Inspection='"+separated[i]+"'";
			    Cursor c2= cf.arr_db.rawQuery(sql1, null);
				if(c2.getCount()>0)
         	    {
         		  /*c2.moveToFirst();
         		 fdcomments = c2.getString(c2.getColumnIndex("ARR_FI_FeedbackComments"));
         		 if(!fdcomments.equals(""))
        		 {*/
        			FB.setBackgroundResource(R.drawable.imgsaved);
        		 //}
        		 
         	 }
				
			((ViewGroup) t1).removeAllViews();
			summaryTable.addView(t,lp);
		}
		ImageView close = (ImageView) dialog1.findViewById(R.id.helpclose);
		close.setOnClickListener(new OnClickListener()
		{
		public void onClick(View arg0) {
				// TODO Auto-generated method stub
			dialog1.dismiss();
			}
			
		});
		dialog1.show();
	}
	
	private void getsingleinspectiotextname(String id) {
		// TODO Auto-generated method stub
	    System.out.println("signli="+id);	
	    		Cursor c =cf.SelectTablefunction(cf.inspnamelist, " Where ARR_Insp_ID='"+id+"'");
	    		if(c.getCount()>0)
	    	   	{
	    	   		 c.moveToFirst();
	    	   		 singleinspname = cf.decode(c.getString(c.getColumnIndex("ARR_Insp_Name")));
	    	   		 System.out.println("signle="+singleinspname);
	    	   		
	    	   	}	
	    
	}

	public String getinspectionid(String string) {
		// TODO Auto-generated method stub
		for(int k=0;k<All_insp_list.length;k++)
		{
			if(All_insp_list[k].equals(string))
			{
				inspections=k+"";
			}
		}
		return inspections;
	}
	public String[] getelevation(String selected)
	{
		System.out.println("selected"+selected);
		String[] arr=null;
		if(selected.trim().equals(All_insp_list[3].trim()))
		{
			arr=this.con.getResources().getStringArray(R.array.B1_1802_Inspection);					
		}
		else if(selected.trim().equals(All_insp_list[1].trim()))
		{
			arr=this.con.getResources().getStringArray(R.array.Four_Point_Inspection);					
		}
		else if(selected.trim().equals(All_insp_list[4].trim()))
		{
			System.out.println("selectedinside 11111");
			arr=this.con.getResources().getStringArray(R.array.Commercial_Type_I);					
		}
		else if(selected.trim().equals(All_insp_list[5].trim()))
		{
			System.out.println("selectedinside 222222");
			arr=this.con.getResources().getStringArray(R.array.Commercial_Type_II);					
		}
		else if(selected.trim().equals(All_insp_list[6].trim()))
		{
			System.out.println("selectedinside 33333333");
			arr=this.con.getResources().getStringArray(R.array.Commercial_Type_III);					
		}
		else if(selected.trim().equals(All_insp_list[7].trim()))
		{
			arr=this.con.getResources().getStringArray(R.array.General_Conditions_Hazards);					
		}
		else if(selected.trim().equals(All_insp_list[8].trim()))
		{
			arr=this.con.getResources().getStringArray(R.array.Sinkhole_Inspection);					
		}
		else if(selected.trim().equals(All_insp_list[9].trim()))
		{
			arr=this.con.getResources().getStringArray(R.array.Chinese_Drywall);					
		}
		else if(selected.trim().equals(All_insp_list[2].trim()))
		{
			arr=this.con.getResources().getStringArray(R.array.Roof_Survey);		
			//max_allowed=2;
		}
		System.out.println("arr="+arr);
		return arr;
	}
	public String getelvationtext(int elev,String insp)
	{
		String selected="";
		
		
		if(insp.equals("1"))
		{
			 if(elev==1) selected="Exterior Photographs";
			 if(elev==2) selected="Roof Photographs";
			 if(elev==3) selected="Plumbing Photographs";
			 if(elev==4) selected="Electrical Photographs";
			 if(elev==5) selected="HVAC Photographs";
			 if(elev==7) selected="Other Photographs";
			 if(elev==23) selected="Front Photo";
		}
		else if(insp.equals("2"))
		{
			
			if(elev==2)selected="Roof Photographs";
			if(elev==7)selected="Other Photographs";
			/**need to change ***/
			if(elev==1)selected="Exterior Photographs";
			if(elev==6)selected="Attic Photographs";	
			if(elev==23) selected="Front Photo";
		}
		else if(insp.equals("3"))
		{
			
			if(elev==17)selected="Front Elevation";
			if(elev==18)selected="Right Elevation";
			if(elev==19)selected="Left Elevation";
			if(elev==20)selected="Back Elevation";
			if(elev==6)selected="Attic Photographs";
			if(elev==12)selected="Additional Photographs";
			if(elev==7)selected="Other Photographs";
			if(elev==23) selected="Front Photo";
		}
		else if(insp.equals("4"))
		{
			if(elev==17)selected="Front Elevation";
			if(elev==18)selected="Right Elevation";
			if(elev==19)selected="Left Elevation";
			if(elev==20)selected="Back Elevation";
			if(elev==6)selected="Attic Photographs";
			if(elev==12)selected="Additional Photographs";
			if(elev==7)selected="Other Photographs";
			if(elev==23) selected="Front Photo";
		}
		else if(insp.equals("5"))
		{
			if(elev==17)selected="Front Elevation";
			if(elev==18)selected="Right Elevation";
			if(elev==19)selected="Left Elevation";
			if(elev==20)selected="Back Elevation";
			if(elev==6)selected="Attic Photographs";
			if(elev==12)selected="Additional Photographs";
			if(elev==7)selected="Other Photographs";
			if(elev==23) selected="Front Photo";
		}
		else if(insp.equals("6"))
		{
			 if(elev==17) selected="Front Elevation";
			 if(elev==18) selected="Right Elevation";
			 if(elev==19) selected="Left Elevation";
			 if(elev==20) selected="Back Elevation";
			 if(elev==6) selected="Attic Photographs";
			 if(elev==12) selected="Additional Photographs";
			 if(elev==7) selected="Other Photographs";
			 if(elev==23) selected="Front Photo";
		}
		else if(insp.equals("7"))
		{
			 if(elev==17) selected="Front Elevation";
			 if(elev==18) selected="Right Elevation";
			 if(elev==19) selected="Left Elevation";
			 if(elev==20) selected="Back Elevation";
			 if(elev==6) selected="Attic Photographs";
			 if(elev==12) selected="Additional Photographs";
			 if(elev==2) selected="Roof Photographs";
			 if(elev==7) selected="Other Photographs";		
			 if(elev==23) selected="Front Photo";
			
		}
		else if(insp.equals("8"))
		{
			 if(elev==8) selected="External Photographs";
			 if(elev==9) selected="Roof and attic Photographs";
			 if(elev==10) selected="Ground and adjoining images";
			 if(elev==11) selected="Internal Photographs";
			 if(elev==12) selected="Additional Photographs";
			 if(elev==7) selected="Other Photographs";
			 if(elev==23) selected="Front Photo";
		}
		else if(insp.equals("9"))
		{
			if(elev==25) selected="Internal Photographs";
			 if(elev==5) selected="HVAC Photographs";
			 if(elev==4) selected="Electrical Photographs";
			 if(elev==3) selected="Plumbing Photographs";
			 if(elev==7) selected="Other Photographs";
			 if(elev==1) selected="Exterior Photographs";
			 if(elev==26) selected="Interior Fixture Photographs";
			 if(elev==27) selected="Drywall Photographs";
			 if(elev==23) selected="Front Photo";
		
		}
		
		return selected;
	}
	public void chkelevpresfornsp(String getelevtxtarr[]) {
		// TODO Auto-generated method stub
		for(int i=0;i<getelevtxtarr.length;i++)
		{
			System.out.println("ELEEL="+getelevtxtarr[i]);
			if(getelevtxtarr[i].equals("Front Elevation"))
			{
				FE.setBackgroundResource(R.drawable.imgunsaved);
			}
			if(getelevtxtarr[i].equals("Right Elevation"))
	    	{
	    		RE.setBackgroundResource(R.drawable.imgunsaved);
	    	}
	    	if(getelevtxtarr[i].equals("Left Elevation"))
	    	{
	    		LE.setBackgroundResource(R.drawable.imgunsaved);
	    	}
	    	if(getelevtxtarr[i].equals("Back Elevation"))
	    	{
	    		BE.setBackgroundResource(R.drawable.imgunsaved);
	    	}
	    	if(getelevtxtarr[i].equals("Attic Photographs"))
	    	{
	    		AE.setBackgroundResource(R.drawable.imgunsaved);
	    	}
	    	if(getelevtxtarr[i].equals("Additional Photographs"))
	    	{
	    		AP.setBackgroundResource(R.drawable.imgunsaved);
	    	}
	    	if(getelevtxtarr[i].equals("Other Photographs"))
	    	 {
	    		OP.setBackgroundResource(R.drawable.imgunsaved);
	    	 }
	    	if(getelevtxtarr[i].equals("Roof Photographs"))
	    	 {
	    		RP.setBackgroundResource(R.drawable.imgunsaved);
	    	 }
	    	if(getelevtxtarr[i].equals("Plumbing Photographs"))
	    	 {
	    		PLUM.setBackgroundResource(R.drawable.imgunsaved);
	    	 }
	    	if(getelevtxtarr[i].equals("Electrical Photographs"))
	    	 {
	    		ELEC.setBackgroundResource(R.drawable.imgunsaved);
	    	 }
	    	if(getelevtxtarr[i].equals("HVAC Photographs"))
	    	 {
	    		HVAV.setBackgroundResource(R.drawable.imgunsaved);
	    	 }	    	
	    	if(getelevtxtarr[i].equals("Exterior Photographs"))
	    	 {
	    		EP.setBackgroundResource(R.drawable.imgunsaved);
	    	 }	    	
		   	if(getelevtxtarr[i].equals("Roof and attic Photographs"))
		    {
		    	RAP.setBackgroundResource(R.drawable.imgunsaved);
		   	}
		   	if(getelevtxtarr[i].equals("Ground and adjoining images"))
		    {
		    	GI.setBackgroundResource(R.drawable.imgunsaved);
		    }
		   	if(getelevtxtarr[i].equals("Internal Photographs"))
		    {
		    	IP.setBackgroundResource(R.drawable.imgunsaved);
		    }
		   	if(getelevtxtarr[i].equals("External Photographs"))
         	{
         		Ext.setBackgroundResource(R.drawable.imgunsaved);
         	}
		}
	}
}