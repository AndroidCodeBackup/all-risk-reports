package idsoft.inspectiondepot.ARR;

import java.util.regex.Pattern;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.EditText;

public class CustomTextWatcher implements TextWatcher 
{
	EditText etphone;
	int keyDel,id_len,before,start;
	CustomTextWatcher(EditText edtid)
 	{
		etphone = edtid;
 	}
	@Override
	public void afterTextChanged(Editable s) {
		
		    if(keyDel==0)
			    {
			    	String ed_val=etphone.getText().toString().trim();
			    	ed_val=ed_val.replace("(", "");
			    	ed_val=ed_val.replace(")", "");
			    	ed_val=ed_val.replace("-", "");
			    	ed_val=ed_val.trim();
			    	System.out.println(ed_val);
			    	
			    			if(ed_val.length()>6)
			    			{
			    				keyDel=1;
			    				etphone.setText("("+ed_val.substring(0,3)+")"+ed_val.substring(3,6)+"-"+ed_val.substring(6,ed_val.length()));
			    					
			    			}
			    			else if(ed_val.length()==6)
			    			{
			    				keyDel=1;
			    				etphone.setText("("+ed_val.substring(0,3)+")"+ed_val.substring(3,ed_val.length()));
		    					
			    			}
			    			else if(ed_val.length()>3)
			    			{
			    				keyDel=1;
			    				etphone.setText("("+ed_val.substring(0,3)+")"+ed_val.substring(3,ed_val.length()));
			    					
			    			}
			    			else if(ed_val.length()==3)
			    			{
			    				keyDel=1;
			    				etphone.setText("("+ed_val.substring(0,ed_val.length()));
			    				
			    			}
			    			else if(ed_val.length()==1) 
			    			{
			    				keyDel=1;
			    				etphone.setText("("+ed_val);
			    				
			    			}
			    			else if(ed_val.equals(""))
			    			{
			    				keyDel=1;
			    				etphone.setText("");
			    				
			    			}
			    				
			    }
			    else
			    {
			    	keyDel=0;
			    }
			    	
		    etphone.setSelection(etphone.getText().toString().length());
	
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub
		if (etphone.getText().toString().startsWith(" "))
        {
			etphone.setText("");
        }
  
		if (etphone.getText().toString().trim().matches("^0") )
        {
			etphone.setText("");
        }

	}

}
