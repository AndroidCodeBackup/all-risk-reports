/*
************************************************************
* Project: ALL RISK REPORTgolin
* 
* Module : Electrical.java
* Creation History:
Created By : Gowri on 
* Modification History:
Last Modified By : Gowri on 04/08/2013
************************************************************ 
*/
package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.BIGeneralData.RadioArrayclickerInsured;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Electrical extends Activity{
	private static final int DATE_DIALOG_ID = 0;
	CommonFunctions cf;
	public int[] electhdrs = {R.id.ged_chk,R.id.epe_chk,R.id.vep_chk,R.id.wir_chk,
			R.id.lfr_chk,R.id.go_chk};
	public CheckBox hdrschk[] = new CheckBox[6];
	public int[] veptyp = {R.id.typ_chk1,R.id.typ_chk2,R.id.typ_chk3,R.id.typ_chk4,R.id.typ_chk5,
			R.id.typ_chk6,R.id.typ_chk7,R.id.typ_chk8,R.id.typ_chk9};
	public CheckBox veptypchk[] = new CheckBox[9];
/*	public int[] recphdrs = {R.id.rec_chk1,R.id.rec_chk2,R.id.rec_chk3,R.id.rec_chk4,R.id.rec_chk5,
			R.id.rec_chk6,R.id.rec_chk7};
	public CheckBox recpchk[] = new CheckBox[7];*/
	/*public int[] gfcrecphdrs = {R.id.gfc_chk1,R.id.gfc_chk2,R.id.gfc_chk3,R.id.gfc_chk4,R.id.gfc_chk5,
			R.id.gfc_chk6};
	public CheckBox gfcrecpchk[] = new CheckBox[6];*/
	public int[] vbchdrs = {R.id.vbc_chk1,R.id.vbc_chk2,R.id.vbc_chk3,R.id.vbc_chk4};
	public CheckBox vbcchk[] = new CheckBox[4];
	public int[] lfrhdrs = {R.id.lfr_chk1,R.id.lfr_chk2,R.id.lfr_chk3,R.id.lfr_chk4,R.id.lfr_chk5};
	public CheckBox lfrchk[] = new CheckBox[5];
	public RadioButton typerd[] = new RadioButton[9];
	public int[] obs = {R.id.hz_chk1,R.id.hz_chk2,R.id.hz_chk3,R.id.hz_chk4,R.id.hz_chk5,
			R.id.hz_chk6,R.id.hz_chk7,R.id.hz_chk8,R.id.hz_chk9,R.id.hz_chk10};
	public CheckBox hzchk[] = new CheckBox[10];
	public int[] wirhdrs = {R.id.wir_chk1,R.id.wir_chk2,R.id.wir_chk3,R.id.wir_chk4,R.id.wir_chk5};
	public CheckBox wirchk[] = new CheckBox[5];
	public int[] alumhdrs = {R.id.alu_chk1,R.id.alu_chk2,R.id.alu_chk3,R.id.alu_chk4,R.id.alu_chk5};
	public CheckBox alumchk[] = new CheckBox[5];
	public int[] alumfsthdrs = {R.id.fst_chk1,R.id.fst_chk2,R.id.fst_chk3,R.id.fst_chk4};
	public CheckBox alumfstchk[] = new CheckBox[4];
	public int[] alumsechdrs = {R.id.sec_chk1,R.id.sec_chk2,R.id.sec_chk3,R.id.sec_chk4};
	public CheckBox alumsecchk[] = new CheckBox[4];
	public int[] alumthdhdrs = {R.id.thd_chk1,R.id.thd_chk2,R.id.thd_chk3,R.id.thd_chk4};
	public CheckBox alumthdchk[] = new CheckBox[4];
	public int[] mptahdrs = {R.id.mpt_chk1,R.id.mpt_chk2,R.id.mpt_chk3,R.id.mpt_chk4,R.id.mpt_chk5,R.id.mpt_chk6};
	public RadioButton mptachk[] = new RadioButton[6];
	
	public int[] mptypehdrs = {R.id.mptype_chk1,R.id.mptype_chk2,R.id.mptype_chk3,R.id.mptype_chk4,R.id.mptype_chk5,R.id.mptype_chk6,R.id.mptype_chk7};
	public RadioButton mptypechk[] = new RadioButton[7];
	
	public int[] servmainhdrs = {R.id.eservm1,R.id.eservm2,R.id.eservm3,R.id.eservm4,R.id.eservm5};
	public RadioButton servmainchk[] = new RadioButton[5];
	
	//public int[] etypehdrs = {R.id.etyp_chk1,R.id.etyp_chk2,R.id.etyp_chk3,R.id.etyp_chk4,R.id.etyp_chk5,R.id.etyp_chk6};
	//public CheckBox etypechk[] = new CheckBox[6];
	public CheckBox sptchk[] = new CheckBox[4];
	public int[] sphdrs = {R.id.sptyp_chk1,R.id.sptyp_chk2,R.id.sptyp_chk3,R.id.sptyp_chk4};
	public CheckBox spchk[] = new CheckBox[4];
	String[] arrtype=null;
	String b[] = {"true","true","true"};
	String[] temp,spp_in_DB;
	RadioGroup esrdgval,strdgval,emrdgval,smrdgval,veprdgval,escrdgval,hzprdgval,
	mmpcrdgval,mptrdgval,dsclrdgval,updrdgval,sprdgval,yicrdgval,epcrdgval;
	String esrdgtxt="",strdgtxt="",emrdgtxt="",smrdgtxt="",veprdgtxt="",veptypchk_vlaue="",sptchk_vlaue="",
		   rectypchk_value="",gfcrectypchk_value="",vbcchk_value="",lfrchk_vlaue="",escrdgtxt="",sastr="--Select--",
		   detretincvbc="",detretvbc="",detretinclfr="0",detretlfr="",detretlfrotr="",detretincoes="0",
		   detretoes="",detretoesnocomt="",detretaddcomt="",strvbclfrotr="",strnocomments="",strnosubpanel="--Select--",
		   straddcomments="",hazrdgtxt="",lsrdgtxt="",updrdgtxt="",upgdyesstr="",sprdgtxt="",
		   hzchk_vlaue="",lcrdgtxt="",wirchk_vlaue="",alumchk_vlaue="",alumfstchk_vlaue="",agestr="",dsclrdgtxt="",
		   alumsecchk_vlaue="",alumthdchk_vlaue="",showstr="",mmpcrdgtxt="",mptrdgtxt="",mptachk_vlaue="",mptypechk_vlaue="",eservmainchk_value="",
		   spnotr="", strtypchk="",yicrdgtxt="",etypchk_vlaue="",locstr="--Select--",epcrdgtxt="",retesrdtxt="",
		   rethaztxt="",retwirtxt="",retvprdtxt="",spchk_vlaue="",mainlocstr="",spampstr="--Select--",retrecptxt="",
		   retmmpcondtxt="",retspptxt="",edlutxt="";
	boolean epchk = false; 
	//RadioGroup lsrdgval,lcrdgval;
	int k,gedchkval=0,epval=0,mpo=0,nc=0,vepchkval=0,recchkval=0,vbcchkval=2,lfrchkval=2,oeschkval=2,gochkval=0,wirchkval=0,elechkval=0,Current_select_id,addchkval=2;
	Cursor chkged_save,chkvep_save,chkrec_save,chkdet_save,chkgo_save,chkwir_save,chkmpl_save,chkspl_save,chkspp_save,
	chkveb_save,chklfr_save,chkoes_save;
	EditText etlfr,etnocomments,etaddcomments,splocet,etaluminnocomments,ethaznocomments;
	public String[] alumarr=null,subtypechkval=null;
	TextView etnosubpanels;
	Spinner agespin,locspn,mainlocspn,spampsspin;
	ArrayAdapter ageadap,saadap,locadap,mainlocadap,spampsadap;
	ImageView[] edtimg;
	TableLayout spptbl;
	String subamps[] = { "--Select--","40 AMPS","50 AMPS","60 AMPS","100 AMPS","150 AMPS","200 AMPS","Other"},subarr[];
	String loctn[] = {"--Select--","Garage","Right Side","Left Side","Other"};
	String mploctn[] = {"--Select--","Garage","Laundry","Hallway","Kitchen","Other"};
	String num[] = { "--Select--","1","2","3","4","5","6","7","8","9"};
	boolean esck=false,stck=false,emck=false,smck=false,yicck=false,vepck=false,
			hzpck=false,lsck=false,lcck=false,escck=false,mmpck=true,mptack=false,
			yicfck=false,dlck=false,ruck=false,pcck=false,spck=true,load_comment=true,
			ged_sve=false,chk1=true,chk2=true,vep_sve=false,wir_sve=false,recp_sve=false,
			vbc_sve=false,lfr_sve=false,go_sve=false,oes_sve=false;

	Cursor sp_retrive,mp_retrive,chkcom_save;
	@Override
	   public void onCreate(Bundle savedInstanceState) {

	        super.onCreate(savedInstanceState);
	        cf=new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);
				
		 	}
	        setContentView(R.layout.electrical);
	        cf.getDeviceDimensions();
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
			hdr_layout.addView(new HdrOnclickListener(this,1,"Four Point Inspection","Electrical System",2,0,cf));
         	LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
         	main_layout.setMinimumWidth(cf.wd);
         	main_layout.addView(new MyOnclickListener(getApplicationContext(), 2, 0,0,cf));
	        LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 24, 1,0,cf));
			TableRow tblrw = (TableRow)findViewById(R.id.row2);
			tblrw.setMinimumHeight(cf.ht);
			declarations();
			cf.CreateARRTable(24);
			cf.CreateARRTable(242);
			cf.CreateARRTable(243);
			cf.CreateARRTable(244);
			cf.CreateARRTable(245);
			cf.CreateARRTable(246);
			cf.CreateARRTable(247);
			cf.CreateARRTable(248);
			cf.CreateARRTable(249);
			GED_setvalue();MP_setvlaue();SP_setvalue();
			VEP_setvalue();WIR_setvalue();//RECP_setvalue();
			Det_setvalue();OBS_setvalue();
			
			chk_values();
			if(sp_retrive.getCount()>0 && mp_retrive.getCount()>0)
			{
				((TextView)findViewById(R.id.savetxt2)).setVisibility(cf.v1.VISIBLE);
			}
			else
			{
				((TextView)findViewById(R.id.savetxt2)).setVisibility(cf.v1.INVISIBLE);
			}
			/*if(!retmmpcondtxt.equals("") && !retspptxt.equals("")){
				 ((TextView)findViewById(R.id.savetxt2)).setVisibility(cf.v1.VISIBLE);
			}else{
				 ((TextView)findViewById(R.id.savetxt2)).setVisibility(cf.v1.INVISIBLE);
			}*/
	}
	private void SP_setvalue() {
		// TODO Auto-generated method stub
		
		try{
			   sp_retrive=cf.SelectTablefunction(cf.Four_Electrical_SubPanel, " where fld_srid='"+cf.selectedhomeid+"'");
			   if(sp_retrive.getCount()>0)
			   {  
				   sp_retrive.moveToFirst();
				   if(sp_retrive.getInt(sp_retrive.getColumnIndex("fld_electinc"))==0)
				   {
					   ((CheckBox)findViewById(R.id.epe_chk)).setChecked(false);
					   ((LinearLayout)findViewById(R.id.epelin)).setVisibility(cf.v1.VISIBLE);
					   ((ImageView)findViewById(R.id.shwep)).setVisibility(cf.v1.GONE);
					   ((ImageView)findViewById(R.id.hdep)).setVisibility(cf.v1.VISIBLE);
					    sprdgtxt=cf.decode(sp_retrive.getString(sp_retrive.getColumnIndex("spp")));
					   ((RadioButton) sprdgval.findViewWithTag(sprdgtxt)).setChecked(true);
					    if(sprdgtxt.equals("Yes")){
					    	((LinearLayout)findViewById(R.id.spyes)).setVisibility(cf.v1.VISIBLE);
					    	 strnosubpanel = cf.decode(sp_retrive.getString(sp_retrive.getColumnIndex("nosub")));
							 etnosubpanels.setText(strnosubpanel);
							 try
							  {
								  cf.arr_db.execSQL("UPDATE  "+cf.Four_Electrical_SubPanelPresent+" SET fld_flag='0' Where fld_srid='"+cf.selectedhomeid+"' ");

							  }catch (Exception e) {
								// TODO: handle exception
							}
							 ShowSubPanelValue();
							   
					    }else{
					    	((LinearLayout)findViewById(R.id.spyes)).setVisibility(cf.v1.GONE);
					    }
				   }
			   }
			   
			 
		}
	    catch (Exception E)
		{
			String strerrorlog="Retrieving Sub Panel - FourPoint";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}	
		
	}
	
	private void MP_setvlaue() {
		// TODO Auto-generated method stub
		try{
			   mp_retrive=cf.SelectTablefunction(cf.Four_Electrical_MainPanel, " where fld_srid='"+cf.selectedhomeid+"'");
			   if(mp_retrive.getCount()>0)
			   {  
				   mp_retrive.moveToFirst();
				   if(mp_retrive.getInt(mp_retrive.getColumnIndex("fld_electinc"))==0)
				   {
					   ((CheckBox)findViewById(R.id.epe_chk)).setChecked(false);
					   ((LinearLayout)findViewById(R.id.epelin)).setVisibility(cf.v1.VISIBLE);
					   ((ImageView)findViewById(R.id.shwep)).setVisibility(cf.v1.GONE);
					   ((ImageView)findViewById(R.id.hdep)).setVisibility(cf.v1.VISIBLE);
					   
					   mptypechk_vlaue=cf.decode(mp_retrive.getString(mp_retrive.getColumnIndex("mptype")));

					   if(!mptypechk_vlaue.equals("")){
						   cf.setRadioBtnValue(mptypechk_vlaue,mptypechk);
						   if(mptypechk_vlaue.contains("Other"))
						   {
							   String mptypevalarr[] = mptypechk_vlaue.split("&#40;");
							   ((EditText)findViewById(R.id.mptype_otr)).setVisibility(cf.v1.VISIBLE);
							   ((EditText)findViewById(R.id.mptype_otr)).setText(mptypevalarr[1]);
						   }
					   }
						   
						   mptachk_vlaue=cf.decode(mp_retrive.getString(mp_retrive.getColumnIndex("mptypopt")));
						   if(!mptachk_vlaue.equals(""))
						   {
								   cf.setRadioBtnValue(mptachk_vlaue,mptachk);
								   if(mptachk_vlaue.contains("Other"))
								   {
									   String mpvalarr[] = mptachk_vlaue.split("&#40;");
									   System.out.println("mptachk_vlauearr="+mpvalarr[0]);
									   ((EditText)findViewById(R.id.mpt_otr)).setVisibility(cf.v1.VISIBLE);
									   System.out.println("mptachk_vlauearr111="+mpvalarr[1]);
									   ((EditText)findViewById(R.id.mpt_otr)).setText(mpvalarr[1]);
								   }
								   						   
								   if(mptachk[5].isChecked())
								   {
									   ((TextView)findViewById(R.id.mpttxt)).setVisibility(cf.v1.VISIBLE);
								   }else
								   {
									   ((TextView)findViewById(R.id.mpttxt)).setVisibility(cf.v1.INVISIBLE);
								   }
						   }
					   agestr=cf.decode(mp_retrive.getString(mp_retrive.getColumnIndex("agempl")));
					   
					   agespin.setSelection(ageadap.getPosition(agestr)); 
					   if(agestr.equals("Other")){mpo=1;
						   ((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.VISIBLE);
						   ((EditText)findViewById(R.id.age_otr)).setText(cf.decode(mp_retrive.getString(mp_retrive.getColumnIndex("agemplotr"))));
					   }else{
						   ((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.GONE);
					   }
					   
					   yicrdgtxt=cf.decode(mp_retrive.getString(mp_retrive.getColumnIndex("yiconf")));
					   ((RadioButton) yicrdgval.findViewWithTag(yicrdgtxt)).setChecked(true);
					   mainlocstr = cf.decode(mp_retrive.getString(mp_retrive.getColumnIndex("mplocation")));
					   mainlocspn.setSelection(mainlocadap.getPosition(mainlocstr));
					   if(mainlocstr.equals("Other")){
						   ((EditText)findViewById(R.id.mploc_otr)).setVisibility(cf.v1.VISIBLE);
						   ((EditText)findViewById(R.id.mploc_otr)).setText(cf.decode(mp_retrive.getString(mp_retrive.getColumnIndex("mplocationotr"))));
					   }else{
						   ((EditText)findViewById(R.id.mploc_otr)).setVisibility(cf.v1.INVISIBLE);
					   }
					   dsclrdgtxt=cf.decode(mp_retrive.getString(mp_retrive.getColumnIndex("dslocation")));
					   ((RadioButton) dsclrdgval.findViewWithTag(dsclrdgtxt)).setChecked(true);
					   if(dsclrdgtxt.equals("Other")){
						   ((EditText)findViewById(R.id.dsc_location)).setVisibility(cf.v1.VISIBLE);
						   ((EditText)findViewById(R.id.dsc_location)).setText(cf.decode(mp_retrive.getString(mp_retrive.getColumnIndex("dslocationotr"))));
					   }else{
						   ((EditText)findViewById(R.id.dsc_location)).setVisibility(cf.v1.GONE);
					   }
					   
					   updrdgtxt=cf.decode(mp_retrive.getString(mp_retrive.getColumnIndex("recupd")));
					   ((RadioButton) updrdgval.findViewWithTag(updrdgtxt)).setChecked(true);
					   if(updrdgtxt.equals("Yes")){
						   ((LinearLayout)findViewById(R.id.perlin)).setVisibility(cf.v1.VISIBLE);
						   ((EditText)findViewById(R.id.upd_yes)).setText(cf.decode(mp_retrive.getString(mp_retrive.getColumnIndex("recupdyes"))));
						   ((LinearLayout)findViewById(R.id.rushow)).setVisibility(cf.v1.VISIBLE);
						    etypchk_vlaue=cf.decode(mp_retrive.getString(mp_retrive.getColumnIndex("rectyp")));
						    //cf.setvaluechk1(etypchk_vlaue,etypechk,((EditText)findViewById(R.id.mpt_otr)));
						    String retdl = cf.decode(mp_retrive.getString(mp_retrive.getColumnIndex("recdate")));
						    if(retdl.equals("Not Determined")){
								  ((CheckBox)findViewById(R.id.ndchk)).setChecked(true); nc=1;
							}
						    else
						    {
								 ((CheckBox)findViewById(R.id.ndchk)).setChecked(false);
								  nc=0; 
								
							 }
						    ((EditText)findViewById(R.id.edlutxt)).setText(retdl);
						    epcrdgtxt=cf.decode(mp_retrive.getString(mp_retrive.getColumnIndex("recpc")));
							((RadioButton) epcrdgval.findViewWithTag(epcrdgtxt)).setChecked(true);
					   }else{
						   ((LinearLayout)findViewById(R.id.perlin)).setVisibility(cf.v1.GONE);
						   ((LinearLayout)findViewById(R.id.rushow)).setVisibility(cf.v1.GONE);
					   }
				   }
				   else
				   {
					   ((CheckBox)findViewById(R.id.epe_chk)).setChecked(true);
					   ((LinearLayout)findViewById(R.id.epelin)).setVisibility(cf.v1.GONE);
					   ((ImageView)findViewById(R.id.shwep)).setVisibility(cf.v1.VISIBLE);
					   ((ImageView)findViewById(R.id.hdep)).setVisibility(cf.v1.GONE);
				   }
			   }
			   else
			   {
				   defaultage();
				   defaultdlup();
				   
				   
			   }
		}
	    catch (Exception E)
		{
			String strerrorlog="Retrieving Main Panel - FourPoint";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}		   
			
	}
	private void defaultage()
	{
		cf.getPolicyholderInformation(cf.selectedhomeid);
		if(cf.YearPH.contains("Other"))
		{
			  String otheryear[] = cf.YearPH.split("&#126;");
			  int spinnerPosition = ageadap.getPosition(otheryear[0]);
			   agespin.setSelection(spinnerPosition);
			  
			  ((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.VISIBLE);
			  ((EditText)findViewById(R.id.age_otr)).setText(otheryear[1]);			   
		}
		else
		{
			int spinnerPosition = ageadap.getPosition(cf.YearPH);
			   agespin.setSelection(spinnerPosition);		  
			 ((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.GONE);
		}		
		
		
		((RadioButton) dsclrdgval.findViewWithTag("Main Panel")).setChecked(true);
		((RadioButton) updrdgval.findViewWithTag("No")).setChecked(true);
	}
	private void WIR_setvalue() {
		// TODO Auto-generated method stub
		try{
			   Cursor wir_retrive=cf.SelectTablefunction(cf.Four_Electrical_Wiring, " where fld_srid='"+cf.selectedhomeid+"'");
			   System.out.println("test"+wir_retrive.getCount());
			   if(wir_retrive.getCount()>0)
			   {  
				   wir_retrive.moveToFirst();
				   System.out.println("wircount="+wir_retrive.getInt(wir_retrive.getColumnIndex("fld_wirinc")));
				   if(wir_retrive.getInt(wir_retrive.getColumnIndex("fld_wirinc"))==0)
				   {
					   ((TextView)findViewById(R.id.savetxtwir)).setVisibility(cf.v1.VISIBLE);
					   ((CheckBox)findViewById(R.id.wir_chk)).setChecked(false);
					   ((ImageView)findViewById(R.id.shwwir)).setVisibility(cf.v1.GONE);
					   ((ImageView)findViewById(R.id.hdwir)).setVisibility(cf.v1.VISIBLE);
					   
					   ((LinearLayout)findViewById(R.id.wirlin)).setVisibility(cf.v1.VISIBLE);
					   
					   wirchk_vlaue=cf.decode(wir_retrive.getString(wir_retrive.getColumnIndex("wirtyp")));
					   cf.setvaluechk1(wirchk_vlaue,wirchk,((EditText)findViewById(R.id.wir_otr)));
					   System.out.println("wirchk_vlaue"+wirchk_vlaue);
					   
					   if(wirchk_vlaue.contains("Aluminum")){
						   ((LinearLayout)findViewById(R.id.aluminshow)).setVisibility(cf.v1.VISIBLE);
						   alumchk_vlaue=cf.decode(wir_retrive.getString(wir_retrive.getColumnIndex("aluminopt")));
						   alumchk_vlaue=alumchk_vlaue.replace(",","&#94;");
						   cf.setvaluechk1(alumchk_vlaue,alumchk,((EditText)findViewById(R.id.typ_otr)));
						   if(alumchk_vlaue.contains("Beyond Scope of Inspection")){
							   ((LinearLayout)findViewById(R.id.firstchklin)).setVisibility(cf.v1.GONE);
							   ((LinearLayout)findViewById(R.id.secondchklin)).setVisibility(cf.v1.GONE);
							   ((LinearLayout)findViewById(R.id.thirdchklin)).setVisibility(cf.v1.GONE);
						   }else{
							   alumfstchk_vlaue=cf.decode(wir_retrive.getString(wir_retrive.getColumnIndex("aluminsubopt1")));
							   System.out.println("alumfstchk_vlaue="+alumfstchk_vlaue);
							   if(!alumfstchk_vlaue.equals("")){
								   ((LinearLayout)findViewById(R.id.firstchklin)).setVisibility(cf.v1.VISIBLE);
								   cf.setvaluechk1(alumfstchk_vlaue,alumfstchk,((EditText)findViewById(R.id.typ_otr)));
							   }else{
								   ((LinearLayout)findViewById(R.id.firstchklin)).setVisibility(cf.v1.GONE);
							   }
							   alumsecchk_vlaue=cf.decode(wir_retrive.getString(wir_retrive.getColumnIndex("aluminsubopt2")));
							   System.out.println("alumsecchk_vlaue="+alumsecchk_vlaue);
							   if(!alumsecchk_vlaue.equals("")){
								   ((LinearLayout)findViewById(R.id.secondchklin)).setVisibility(cf.v1.VISIBLE);
								   cf.setvaluechk1(alumsecchk_vlaue,alumsecchk,((EditText)findViewById(R.id.typ_otr)));
							   }else{
								   ((LinearLayout)findViewById(R.id.secondchklin)).setVisibility(cf.v1.GONE);
							   }
							   alumthdchk_vlaue=cf.decode(wir_retrive.getString(wir_retrive.getColumnIndex("aluminsubopt3")));
							   if(!alumthdchk_vlaue.equals("")){
								   ((LinearLayout)findViewById(R.id.thirdchklin)).setVisibility(cf.v1.VISIBLE);
								   cf.setvaluechk1(alumthdchk_vlaue,alumthdchk,((EditText)findViewById(R.id.wir_otr)));
							   }else{
								   ((LinearLayout)findViewById(R.id.thirdchklin)).setVisibility(cf.v1.GONE);
							   }
						   }
					   }else{
						   ((LinearLayout)findViewById(R.id.aluminshow)).setVisibility(cf.v1.GONE);
					   }
					   
					   Cursor obs_retrive=cf.SelectTablefunction(cf.Four_Electrical_GO, " where fld_srid='"+cf.selectedhomeid+"'");
					   System.out.println("obs_retrive="+obs_retrive.getCount()); 
					   if(obs_retrive.getCount()>0)
				       {  
						   obs_retrive.moveToFirst();
						   hazrdgtxt=cf.decode(obs_retrive.getString(obs_retrive.getColumnIndex("haz")));System.out.println("hazrdgtxt"+hazrdgtxt);
						   ((RadioButton) hzprdgval.findViewWithTag(hazrdgtxt)).setChecked(true);
						   if(hazrdgtxt.equals("Yes"))
						   {
						    	((LinearLayout)findViewById(R.id.hazyesshow)).setVisibility(cf.v1.VISIBLE);
						     	hzchk_vlaue=cf.decode(obs_retrive.getString(obs_retrive.getColumnIndex("hazyes")));
							    cf.setvaluechk1(hzchk_vlaue,hzchk,((EditText)findViewById(R.id.hz_otr)));
							    ethaznocomments.setText(cf.decode(obs_retrive.getString(obs_retrive.getColumnIndex("haznocomt"))));
						    }
						    else
						    {
						    	((LinearLayout)findViewById(R.id.hazyesshow)).setVisibility(cf.v1.GONE);
						    	
						    }
				       }			   
				   }
				   else
				   {
					   ((TextView)findViewById(R.id.savetxtwir)).setVisibility(cf.v1.VISIBLE);
					   ((CheckBox)findViewById(R.id.wir_chk)).setChecked(true);
					   ((LinearLayout)findViewById(R.id.wirlin)).setVisibility(cf.v1.GONE);
					   ((ImageView)findViewById(R.id.shwwir)).setVisibility(cf.v1.VISIBLE);
					   ((ImageView)findViewById(R.id.hdwir)).setVisibility(cf.v1.GONE);
				   }
			   }
		}
	    catch (Exception E)
		{
			String strerrorlog="Retrieving Wiring Type - FourPoint";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
	private void OBS_setvalue() {
		// TODO Auto-generated method stub
		try{
			   Cursor obs_retrive=cf.SelectTablefunction(cf.Four_Electrical_GO, " where fld_srid='"+cf.selectedhomeid+"'");
			  System.out.println("obs_retrive="+obs_retrive.getCount()); if(obs_retrive.getCount()>0)
			   {  
				   obs_retrive.moveToFirst();
				   if(obs_retrive.getInt(obs_retrive.getColumnIndex("fld_goinc"))==0)
				   {
					   ((TextView)findViewById(R.id.savetxt7)).setVisibility(cf.v1.VISIBLE);
					   ((CheckBox)findViewById(R.id.go_chk)).setChecked(false);
					   ((LinearLayout)findViewById(R.id.golin)).setVisibility(cf.v1.VISIBLE);
					   ((ImageView)findViewById(R.id.shwgo)).setVisibility(cf.v1.GONE);
					   ((ImageView)findViewById(R.id.hdgo)).setVisibility(cf.v1.VISIBLE);
					   				   
					   /*hazrdgtxt=cf.decode(obs_retrive.getString(obs_retrive.getColumnIndex("haz")));System.out.println("hazrdgtxt"+hazrdgtxt);
					   ((RadioButton) hzprdgval.findViewWithTag(hazrdgtxt)).setChecked(true);
					   if(hazrdgtxt.equals("Yes")){
					    	((LinearLayout)findViewById(R.id.hazyesshow)).setVisibility(cf.v1.VISIBLE);
					     	hzchk_vlaue=cf.decode(obs_retrive.getString(obs_retrive.getColumnIndex("hazyes")));
						    cf.setvaluechk1(hzchk_vlaue,hzchk,((EditText)findViewById(R.id.hz_otr)));
						    ethaznocomments.setText(cf.decode(obs_retrive.getString(obs_retrive.getColumnIndex("haznocomt"))));
					    }else{
					    	((LinearLayout)findViewById(R.id.hazyesshow)).setVisibility(cf.v1.GONE);
					    	
					    }*/
					   
					   //lsrdgtxt=cf.decode(obs_retrive.getString(obs_retrive.getColumnIndex("recp")));
					   //((RadioButton) lsrdgval.findViewWithTag(lsrdgtxt)).setChecked(true);
					/*   if(lsrdgtxt.equals("Yes")){
					    	((LinearLayout)findViewById(R.id.lsyescommentsshow)).setVisibility(cf.v1.VISIBLE);
					    	 etlsyescomments.setText(cf.decode(obs_retrive.getString(obs_retrive.getColumnIndex("recpyescomt"))));
					    }else{
					    	((LinearLayout)findViewById(R.id.lsyescommentsshow)).setVisibility(cf.v1.GONE);
					     }*/
					   
					  /* lcrdgtxt=cf.decode(obs_retrive.getString(obs_retrive.getColumnIndex("lconn")));
					   ((RadioButton) lcrdgval.findViewWithTag(lcrdgtxt)).setChecked(true);
					   if(lcrdgtxt.equals("Yes")){
					    	((LinearLayout)findViewById(R.id.lcyescommentsshow)).setVisibility(cf.v1.VISIBLE);
					    	 etlcyescomments.setText(cf.decode(obs_retrive.getString(obs_retrive.getColumnIndex("lconnyescomt"))));
					    }else{
					    	((LinearLayout)findViewById(R.id.lcyescommentsshow)).setVisibility(cf.v1.GONE);
					     }*/
				   }
				   else  if(obs_retrive.getInt(obs_retrive.getColumnIndex("fld_goinc"))==1)
				   {
					   ((TextView)findViewById(R.id.savetxt7)).setVisibility(cf.v1.VISIBLE);
					   ((CheckBox)findViewById(R.id.go_chk)).setChecked(true);
					   ((LinearLayout)findViewById(R.id.golin)).setVisibility(cf.v1.GONE);
					   ((ImageView)findViewById(R.id.shwgo)).setVisibility(cf.v1.VISIBLE);
					   ((ImageView)findViewById(R.id.hdgo)).setVisibility(cf.v1.GONE);
				   }
			   }
			   else
			   {
				   defaultgenobs();
			   }
		}
	    catch (Exception E)
		{
			String strerrorlog="Retrieving Observation - FourPoint";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
	private void Det_setvalue() {
		// TODO Auto-generated method stub
		try{
			   Cursor det_retrive=cf.SelectTablefunction(cf.Four_Electrical_Details, " where fld_srid='"+cf.selectedhomeid+"'");
			   if(det_retrive.getCount()>0)
			   {  
				   System.out.println("detre=");
				   det_retrive.moveToFirst();
				   vbcchk_value=cf.decode(det_retrive.getString(det_retrive.getColumnIndex("vbc")));System.out.println("vbcchk_value"+vbcchk_value);
				   if(!vbcchk_value.equals(""))
				   {
					   cf.setvaluechk1(vbcchk_value,vbcchk,((EditText)findViewById(R.id.typ_otr)));
				   }
				   System.out.println("thahhh");
				   
				   
				   System.out.println("THinf="+det_retrive.getInt(det_retrive.getColumnIndex("fld_lfrin")));
				   if(det_retrive.getInt(det_retrive.getColumnIndex("fld_lfrin"))==0)
				   {
					   ((TextView)findViewById(R.id.savetxt6)).setVisibility(cf.v1.VISIBLE);
					   ((CheckBox)findViewById(R.id.lfr_chk)).setChecked(false);
					   ((ImageView)findViewById(R.id.shwlf)).setVisibility(cf.v1.GONE);
					   ((ImageView)findViewById(R.id.hdlf)).setVisibility(cf.v1.VISIBLE);
					   ((LinearLayout)findViewById(R.id.lfrlin)).setVisibility(cf.v1.VISIBLE);
					   lfrchk_vlaue=cf.decode(det_retrive.getString(det_retrive.getColumnIndex("lfr")));
					   cf.setvaluechk1(lfrchk_vlaue,lfrchk,((EditText)findViewById(R.id.lfr_otr)));
					   
				   }
				   else if(det_retrive.getInt(det_retrive.getColumnIndex("fld_lfrin"))==1)
				   {
					   ((TextView)findViewById(R.id.savetxt6)).setVisibility(cf.v1.VISIBLE);
					   ((CheckBox)findViewById(R.id.lfr_chk)).setChecked(true);
					   ((LinearLayout)findViewById(R.id.lfrlin)).setVisibility(cf.v1.GONE);
					   ((ImageView)findViewById(R.id.shwlf)).setVisibility(cf.v1.VISIBLE);
					   ((ImageView)findViewById(R.id.hdlf)).setVisibility(cf.v1.GONE);
				   }

				    escrdgtxt=cf.decode(det_retrive.getString(det_retrive.getColumnIndex("oes")));System.out.println("escrdgtxt="+escrdgtxt);
				   ((RadioButton) escrdgval.findViewWithTag(escrdgtxt)).setChecked(true);  
				   if(escrdgtxt.equals("Yes, based on visual observations at the time of inspection"))
				    {
				    	((LinearLayout)findViewById(R.id.yescondshow)).setVisibility(cf.v1.VISIBLE);
				    	((LinearLayout)findViewById(R.id.nocommentsshow)).setVisibility(cf.v1.GONE);
				    }
				   else
				    {
				    	((LinearLayout)findViewById(R.id.yescondshow)).setVisibility(cf.v1.VISIBLE);
				    	((LinearLayout)findViewById(R.id.nocommentsshow)).setVisibility(cf.v1.VISIBLE);
				    	 etnocomments.setText(cf.decode(det_retrive.getString(det_retrive.getColumnIndex("oescomment"))));
					}
				   System.out.println("dsfdsdfdf");
				 /*  if(det_retrive.getInt(det_retrive.getColumnIndex("fld_vbcinc"))==0)
				   {
					   ((TextView)findViewById(R.id.savetxt5)).setVisibility(cf.v1.VISIBLE);
					   ((CheckBox)findViewById(R.id.vbc_chk)).setChecked(false);
					   ((ImageView)findViewById(R.id.shwvbc)).setVisibility(cf.v1.GONE);
					   ((ImageView)findViewById(R.id.hdvbc)).setVisibility(cf.v1.VISIBLE);
					   ((LinearLayout)findViewById(R.id.vbclin)).setVisibility(cf.v1.VISIBLE);
					   vbcchk_value=cf.decode(det_retrive.getString(det_retrive.getColumnIndex("vbc")));
					   cf.setvaluechk1(vbcchk_value,vbcchk,((EditText)findViewById(R.id.typ_otr)));
					   
				   }
				   else if(det_retrive.getInt(det_retrive.getColumnIndex("fld_vbcinc"))==1)
				   {
					   ((TextView)findViewById(R.id.savetxt5)).setVisibility(cf.v1.VISIBLE);
					   ((CheckBox)findViewById(R.id.vbc_chk)).setChecked(true);
					   ((ImageView)findViewById(R.id.shwvbc)).setVisibility(cf.v1.VISIBLE);
					   ((ImageView)findViewById(R.id.hdvbc)).setVisibility(cf.v1.GONE);
					   ((LinearLayout)findViewById(R.id.vbclin)).setVisibility(cf.v1.GONE);
				   }*/
				
				/*   if(det_retrive.getInt(det_retrive.getColumnIndex("fld_oesinc"))==0)
				   {
					   ((TextView)findViewById(R.id.savetxt8)).setVisibility(cf.v1.VISIBLE);
					   ((CheckBox)findViewById(R.id.oes_chk)).setChecked(false);
					   ((ImageView)findViewById(R.id.shwcon)).setVisibility(cf.v1.GONE);
					   ((ImageView)findViewById(R.id.hdcon)).setVisibility(cf.v1.VISIBLE);
					   ((LinearLayout)findViewById(R.id.oeslin)).setVisibility(cf.v1.VISIBLE);
					    escrdgtxt=cf.decode(det_retrive.getString(det_retrive.getColumnIndex("oes")));
					   ((RadioButton) escrdgval.findViewWithTag(escrdgtxt)).setChecked(true);
					    if(escrdgtxt.equals("Yes, based on visual observations at the time of inspection")){
					    	((LinearLayout)findViewById(R.id.yescondshow)).setVisibility(cf.v1.VISIBLE);
					    	((LinearLayout)findViewById(R.id.nocommentsshow)).setVisibility(cf.v1.GONE);
					    }else{
					    	((LinearLayout)findViewById(R.id.yescondshow)).setVisibility(cf.v1.VISIBLE);
					    	((LinearLayout)findViewById(R.id.nocommentsshow)).setVisibility(cf.v1.VISIBLE);
					    	 etnocomments.setText(cf.decode(det_retrive.getString(det_retrive.getColumnIndex("oescomment"))));
						}else{
					    	((LinearLayout)findViewById(R.id.nocommentsshow)).setVisibility(cf.v1.VISIBLE);
					    	((LinearLayout)findViewById(R.id.yescondshow)).setVisibility(cf.v1.GONE);
					    	 etnocomments.setText(cf.decode(det_retrive.getString(det_retrive.getColumnIndex("oescomment"))));
					    }
					   
				   }
				   else if(det_retrive.getInt(det_retrive.getColumnIndex("fld_oesinc"))==1)
				   {
					   ((TextView)findViewById(R.id.savetxt8)).setVisibility(cf.v1.VISIBLE);
					   ((CheckBox)findViewById(R.id.oes_chk)).setChecked(true);
					   ((LinearLayout)findViewById(R.id.oeslin)).setVisibility(cf.v1.GONE);
					   ((ImageView)findViewById(R.id.shwcon)).setVisibility(cf.v1.VISIBLE);
					   ((ImageView)findViewById(R.id.hdcon)).setVisibility(cf.v1.GONE);
				   }*/
				   
				   
				   System.out.println("commen"+det_retrive.getString(det_retrive.getColumnIndex("addcomment")));
				   if(!cf.decode(det_retrive.getString(det_retrive.getColumnIndex("addcomment"))).equals("")){
					   etaddcomments.setText(cf.decode(det_retrive.getString(det_retrive.getColumnIndex("addcomment"))));
				   }
				   else
				   {
					   etaddcomments.setText("No comments.");
				   }
			   }
			   else
			   {
				   etaddcomments.setText("No comments.");
			   }
			  
		}
	    catch (Exception E)
		{
			String strerrorlog="Retrieving Receptacles/Outlets - FourPoint";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
	/*private void RECP_setvalue() {
		// TODO Auto-generated method stub
		try
		{
		   Cursor recp_retrive=cf.SelectTablefunction(cf.Four_Electrical_Receptacles, " where fld_srid='"+cf.selectedhomeid+"'");
		   if(recp_retrive.getCount()>0)
		   {  
			   recp_retrive.moveToFirst();
			   if(recp_retrive.getInt(recp_retrive.getColumnIndex("fld_inc"))==0)
			   {
				   ((CheckBox)findViewById(R.id.recp_chk)).setChecked(false);
				   ((LinearLayout)findViewById(R.id.recplin)).setVisibility(cf.v1.VISIBLE);
				   ((ImageView)findViewById(R.id.shwrec)).setVisibility(cf.v1.GONE);
				   ((ImageView)findViewById(R.id.hdrec)).setVisibility(cf.v1.VISIBLE);
				   ((TextView)findViewById(R.id.savetxt4)).setVisibility(cf.v1.VISIBLE);
				    rectypchk_value=cf.decode(recp_retrive.getString(recp_retrive.getColumnIndex("receptacle")));
				    cf.setvaluechk1(rectypchk_value,recpchk,((EditText)findViewById(R.id.typ_otr)));
				    gfcrectypchk_value=cf.decode(recp_retrive.getString(recp_retrive.getColumnIndex("gfcireceptacle")));
				    cf.setvaluechk1(gfcrectypchk_value,gfcrecpchk,((EditText)findViewById(R.id.typ_otr)));
				  
			   }
			   else
			   {
				   ((CheckBox)findViewById(R.id.recp_chk)).setChecked(true);
				   ((LinearLayout)findViewById(R.id.recplin)).setVisibility(cf.v1.GONE);
				   ((TextView)findViewById(R.id.savetxt4)).setVisibility(cf.v1.VISIBLE);
				   ((ImageView)findViewById(R.id.shwrec)).setVisibility(cf.v1.VISIBLE);
				   ((ImageView)findViewById(R.id.hdrec)).setVisibility(cf.v1.GONE);
			   }
			   }
		   else
		   {
			   ((TextView)findViewById(R.id.savetxt4)).setVisibility(cf.v1.INVISIBLE);
		   }
		}
	    catch (Exception E)
		{
			String strerrorlog="Retrieving Receptacles/Outlets - FourPoint";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}*/
	private void VEP_setvalue() {
		// TODO Auto-generated method stub
		try
		{
		   Cursor vep_retrive=cf.SelectTablefunction(cf.Four_Electrical_Panel, " where fld_srid='"+cf.selectedhomeid+"'");
		   if(vep_retrive.getCount()>0)
		   {  
			   vep_retrive.moveToFirst();
			   if(vep_retrive.getInt(vep_retrive.getColumnIndex("fld_inc"))==0)
			   {
				   ((TextView)findViewById(R.id.savetxt3)).setVisibility(cf.v1.VISIBLE);
				   ((CheckBox)findViewById(R.id.vep_chk)).setChecked(false);
				   ((ImageView)findViewById(R.id.shwvep)).setVisibility(cf.v1.GONE);
				   ((ImageView)findViewById(R.id.hdvep)).setVisibility(cf.v1.VISIBLE);
				   ((LinearLayout)findViewById(R.id.veplin)).setVisibility(cf.v1.VISIBLE);
				   veprdgtxt=cf.decode(vep_retrive.getString(vep_retrive.getColumnIndex("visiblepanel")));
				   ((RadioButton) veprdgval.findViewWithTag(veprdgtxt)).setChecked(true);
				   if(veprdgtxt.equals("Yes")){
					   ((LinearLayout)findViewById(R.id.panelshow)).setVisibility(cf.v1.VISIBLE);
					   veptypchk_vlaue=cf.decode(vep_retrive.getString(vep_retrive.getColumnIndex("type")));
					   cf.setvaluechk1(veptypchk_vlaue,veptypchk,((EditText)findViewById(R.id.typ_otr)));
					  
				   }else{
					   ((LinearLayout)findViewById(R.id.panelshow)).setVisibility(cf.v1.GONE);  
				   }
				  
			   }
			   else
			   {
				   ((CheckBox)findViewById(R.id.vep_chk)).setChecked(true);
				   ((LinearLayout)findViewById(R.id.veplin)).setVisibility(cf.v1.GONE);
				   ((TextView)findViewById(R.id.savetxt3)).setVisibility(cf.v1.VISIBLE);
				   ((ImageView)findViewById(R.id.shwvep)).setVisibility(cf.v1.VISIBLE);
				   ((ImageView)findViewById(R.id.hdvep)).setVisibility(cf.v1.GONE);
			   }
				
			   
		   }
		   else
		   {
			   ((TextView)findViewById(R.id.savetxt3)).setVisibility(cf.v1.INVISIBLE);
		   }
		}
	    catch (Exception E)
		{
			String strerrorlog="Retrieving Visible Electrical Panel - FourPoint";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
	private void GED_setvalue() {
		// TODO Auto-generated method stub
		try
		{
		   Cursor ged_retrive=cf.SelectTablefunction(cf.Four_Electrical_General, " where fld_srid='"+cf.selectedhomeid+"'");
		   System.out.println("ged_retrive"+ged_retrive);
		   if(ged_retrive.getCount()>0)
		   {  
			    ged_retrive.moveToFirst();
			   if(ged_retrive.getInt(ged_retrive.getColumnIndex("fld_inc"))==0)
			   {
				   ((CheckBox)findViewById(R.id.ged_chk)).setChecked(false);
				   ((ImageView)findViewById(R.id.shwged)).setVisibility(cf.v1.GONE);
				   ((ImageView)findViewById(R.id.hdged)).setVisibility(cf.v1.VISIBLE);
				   ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
				   ((LinearLayout)findViewById(R.id.gedlin)).setVisibility(cf.v1.VISIBLE);
				   esrdgtxt=cf.decode(ged_retrive.getString(ged_retrive.getColumnIndex("electricalservice")));
				   ((RadioButton) esrdgval.findViewWithTag(esrdgtxt)).setChecked(true);
				   strdgtxt=cf.decode(ged_retrive.getString(ged_retrive.getColumnIndex("servicetype")));
				   ((RadioButton) strdgval.findViewWithTag(strdgtxt)).setChecked(true);
				   emrdgtxt=cf.decode(ged_retrive.getString(ged_retrive.getColumnIndex("electricalmeter")));
				   ((RadioButton) emrdgval.findViewWithTag(emrdgtxt)).setChecked(true);
				   locstr = cf.decode(ged_retrive.getString(ged_retrive.getColumnIndex("location")));
				   locspn.setSelection(locadap.getPosition(locstr));
				   if(locstr.equals("Other")){
					   ((EditText)findViewById(R.id.loc_otr)).setVisibility(cf.v1.VISIBLE);
					   ((EditText)findViewById(R.id.loc_otr)).setText(cf.decode(ged_retrive.getString(ged_retrive.getColumnIndex("locationotr"))));
				   }else{
					   ((EditText)findViewById(R.id.loc_otr)).setVisibility(cf.v1.INVISIBLE);
				   }
				    eservmainchk_value=cf.decode(ged_retrive.getString(ged_retrive.getColumnIndex("servicemain")));System.out.println("DDD="+eservmainchk_value);
				    cf.setRadioBtnValue(eservmainchk_value,servmainchk);
				    
				    String eservmainchk_valueother = cf.decode(ged_retrive.getString(ged_retrive.getColumnIndex("servicemainotr")));					   
				   if(eservmainchk_value.contains("Other")){
					   ((EditText)findViewById(R.id.sm_otr)).setVisibility(cf.v1.VISIBLE);
					   ((EditText)findViewById(R.id.sm_otr)).setText(eservmainchk_valueother);
				   }else{
					   ((EditText)findViewById(R.id.sm_otr)).setVisibility(cf.v1.GONE);
				   }
			   }
			   else if(ged_retrive.getInt(ged_retrive.getColumnIndex("fld_inc"))==1)
			   {
				   ((CheckBox)findViewById(R.id.ged_chk)).setChecked(true);
				   ((LinearLayout)findViewById(R.id.gedlin)).setVisibility(cf.v1.GONE);
				   ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
				   ((ImageView)findViewById(R.id.shwged)).setVisibility(cf.v1.VISIBLE);
				   ((ImageView)findViewById(R.id.hdged)).setVisibility(cf.v1.GONE);
			   }
		   }
		   else
		   {
			   ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.INVISIBLE);System.out.println("text cmae");
			    defaultgenelecdet();
		   }
		}
	    catch (Exception E)
		{
			String strerrorlog="Retrieving General Electrical Details - FourPoint";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
	private void declarations() {
		// TODO Auto-generated method stub
		cf.setupUI((ScrollView) findViewById(R.id.scr));
		 for(int i=0;i<9;i++)
    	 {
    		try{
    			hdrschk[i] = (CheckBox)findViewById(electhdrs[i]);
    		}
			catch(Exception e)
			{
				System.out.println("error= "+e.getMessage());
			}
    	 }
		 for(int i=0;i<9;i++)
    	 {
    		try{
    			veptypchk[i] = (CheckBox)findViewById(veptyp[i]);
    		}
			catch(Exception e)
			{
				System.out.println("error= "+e.getMessage());
			}
        }
	/*	 for(int i=0;i<7;i++)
		 {
			try{
				recpchk[i] = (CheckBox)findViewById(recphdrs[i]);
			}
			catch(Exception e)
			{
				System.out.println("error= "+e.getMessage());
			}
		 }

		for(int i=0;i<6;i++)
		{
			try{
				gfcrecpchk[i] = (CheckBox)findViewById(gfcrecphdrs[i]);
			}
			catch(Exception e)
			{
				System.out.println("error= "+e.getMessage());
			}
		}*/
		for(int i=0;i<4;i++)
		{
			try{
				vbcchk[i] = (CheckBox)findViewById(vbchdrs[i]);
			}
			catch(Exception e)
			{
				System.out.println("error= "+e.getMessage());
			}
		}
		for(int i=0;i<5;i++)
		{
			try{
				lfrchk[i] = (CheckBox)findViewById(lfrhdrs[i]);
			}
			catch(Exception e)
			{
				System.out.println("error= "+e.getMessage());
			}
		}
		for(int i=0;i<10;i++)
	   	 {
	   		try{
	   			hzchk[i] = (CheckBox)findViewById(obs[i]);
	   		}
				catch(Exception e)
				{
					System.out.println("error= "+e.getMessage());
				}
	       }
		for(int i=0;i<5;i++)
		{
			try{
				wirchk[i] = (CheckBox)findViewById(wirhdrs[i]);
			}
			catch(Exception e)
			{
				System.out.println("error= "+e.getMessage());
			}
		}
		for(int i=0;i<alumchk.length;i++)
		{
			try{
				alumchk[i] = (CheckBox)findViewById(alumhdrs[i]);
			}
			catch(Exception e)
			{
				System.out.println("error= "+e.getMessage());
			}
		}
		for(int i=0;i<4;i++)
		{
			try{
				alumfstchk[i] = (CheckBox)findViewById(alumfsthdrs[i]);
			}
			catch(Exception e)
			{
				System.out.println("error= "+e.getMessage());
			}
		}
		for(int i=0;i<4;i++)
		{
			try{
				alumsecchk[i] = (CheckBox)findViewById(alumsechdrs[i]);
			}
			catch(Exception e)
			{
				System.out.println("error= "+e.getMessage());
			}
		}
		for(int i=0;i<4;i++)
		{
			try{
				alumthdchk[i] = (CheckBox)findViewById(alumthdhdrs[i]);
			}
			catch(Exception e)
			{
				System.out.println("error= "+e.getMessage());
			}
		}
		for(int i=0;i<mptachk.length;i++)
	   	 {
	   		try{
	   			mptachk[i] = (RadioButton)findViewById(mptahdrs[i]);
	   			mptachk[i].setOnClickListener(new MainPanelampclicker());
	   		}
				catch(Exception e)
				{
					System.out.println("error= "+e.getMessage());
				}
	   	 }
		
		for(int i=0;i<mptypechk.length;i++)
	   	 {
	   		try{
	   			mptypechk[i] = (RadioButton)findViewById(mptypehdrs[i]);
	   			mptypechk[i].setOnClickListener(new MainPanelTypeclicker());
	   		}
				catch(Exception e)
				{
					System.out.println("error= "+e.getMessage());
				}
	   	 }
		
		for(int i=0;i<servmainchk.length;i++)
	   	 {
	   		try{
	   			servmainchk[i] = (RadioButton)findViewById(servmainhdrs[i]);
	   			servmainchk[i].setOnClickListener(new ServiceMainclicker());
	   		}
				catch(Exception e)
				{
					System.out.println("error= "+e.getMessage());
				}
	   	 }
		
		
		
		/*for(int i=0;i<6;i++)
		{
			try{
				etypechk[i] = (CheckBox)findViewById(etypehdrs[i]);
			}
			catch(Exception e)
			{
				System.out.println("error= "+e.getMessage());
			}
		}*/	
		 locspn=(Spinner) findViewById(R.id.et_location);
		 locadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, loctn);
		 locadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 locspn.setAdapter(locadap);
		 locspn.setOnItemSelectedListener(new  Spin_Selectedlistener(2));
		 esrdgval = (RadioGroup)findViewById(R.id.es_rdg);
		 esrdgval.setOnCheckedChangeListener(new checklistenetr(1));
		 strdgval = (RadioGroup)findViewById(R.id.st_rdg);
		 strdgval.setOnCheckedChangeListener(new checklistenetr(2));
		 emrdgval = (RadioGroup)findViewById(R.id.em_rdg);
		 emrdgval.setOnCheckedChangeListener(new checklistenetr(3));
		 /*smrdgval = (RadioGroup)findViewById(R.id.sm_rdg);
		 smrdgval.setOnCheckedChangeListener(new checklistenetr(4));*/
		 veprdgval = (RadioGroup)findViewById(R.id.vep_rdg);
		 veprdgval.setOnCheckedChangeListener(new checklistenetr(5));
		 escrdgval = (RadioGroup)findViewById(R.id.esc_rdg);
		 escrdgval.setOnCheckedChangeListener(new checklistenetr(6));
		 etlfr = (EditText)findViewById(R.id.lfr_otr);
		 mainlocspn=(Spinner) findViewById(R.id.mp_location);
		 mainlocadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, mploctn);
		 mainlocadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 mainlocspn.setAdapter(mainlocadap);
		 mainlocspn.setOnItemSelectedListener(new  Spin_Selectedlistener(3));
		 etnocomments = (EditText)findViewById(R.id.nocomment);
		 etaddcomments = (EditText)findViewById(R.id.eleccomment);
		 etnocomments.addTextChangedListener(new textwatcher(1));
		 etaddcomments.addTextChangedListener(new textwatcher(2));
		 hzprdgval = (RadioGroup)findViewById(R.id.hzp_rdg);
		 hzprdgval.setOnCheckedChangeListener(new checklistenetr(8));
		 ethaznocomments = (EditText)findViewById(R.id.haznocomment);
		 ethaznocomments.addTextChangedListener(new textwatcher(4));
		 /*lsrdgval = (RadioGroup)findViewById(R.id.ls_rdg);
		 lsrdgval.setOnCheckedChangeListener(new checklistenetr(9));
		 etlsyescomments = (EditText)findViewById(R.id.lsyescomment);
		 etlsyescomments.addTextChangedListener(new textwatcher(5));
		 lcrdgval = (RadioGroup)findViewById(R.id.lc_rdg);
		 lcrdgval.setOnCheckedChangeListener(new checklistenetr(10));
		 etlcyescomments = (EditText)findViewById(R.id.lcyescomment);
		 etlcyescomments.addTextChangedListener(new textwatcher(6));*/
		 
		 //mmpcrdgval = (RadioGroup)findViewById(R.id.mmpc_rdg);
		 //mmpcrdgval.setOnCheckedChangeListener(new checklistenetr(11));
		 //mptrdgval= (RadioGroup)findViewById(R.id.mpt_rdg);
		 //mptrdgval.setOnCheckedChangeListener(new checklistenetr(12));
		 ((EditText)findViewById(R.id.mpt_otr)).addTextChangedListener(new textwatcher(7));
		 agespin=(Spinner) findViewById(R.id.age_spin);
		 ageadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, cf.yearbuilt);
		 ageadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 agespin.setAdapter(ageadap);//agespin.setSelection(ageadap.getPosition("2012"));
		 agespin.setOnItemSelectedListener(new  Spin_Selectedlistener(1));
		 ((EditText)findViewById(R.id.age_otr)).addTextChangedListener(new textwatcher(8));
		 dsclrdgval= (RadioGroup)findViewById(R.id.dcl_rdg);
		 dsclrdgval.setOnCheckedChangeListener(new checklistenetr(13));
		 updrdgval= (RadioGroup)findViewById(R.id.upd_rdg);
		 updrdgval.setOnCheckedChangeListener(new checklistenetr(14));
		 ((EditText)findViewById(R.id.upd_yes)).addTextChangedListener(new textwatcher(9));
		 sprdgval= (RadioGroup)findViewById(R.id.sp_rdg);
		 sprdgval.setOnCheckedChangeListener(new checklistenetr(15));
		 
		 /** DECLARING SUB PANELS STARTS **/
		 etnosubpanels = (TextView)findViewById(R.id.sp_no);etnosubpanels.setText("0");
		 splocet = (EditText)findViewById(R.id.et_splocation);
		 spampsspin=(Spinner) findViewById(R.id.spamp_spin);
		 spampsadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, subamps);
		 spampsadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 spampsspin.setAdapter(spampsadap);
		 spampsspin.setOnItemSelectedListener(new  Spin_Selectedlistener(4));
		 ((EditText)findViewById(R.id.etotr_spamp)).addTextChangedListener(new textwatcher(10));
		 for(int i=0;i<4;i++)
			{
				try{
					spchk[i] = (CheckBox)findViewById(sphdrs[i]);
				}
				catch(Exception e)
				{
					System.out.println("error= "+e.getMessage());
				}
			}	
		 spptbl = (TableLayout)findViewById(R.id.sppvalue);
		 /** DECLARING SUB PANELS ENDS **/
		
		 yicrdgval = (RadioGroup)findViewById(R.id.yic_rdg);
		 yicrdgval.setOnCheckedChangeListener(new checklistenetr(16));
		 ((RadioButton)yicrdgval.findViewWithTag("Original")).setChecked(true);
		 epcrdgval = (RadioGroup)findViewById(R.id.epc_rdg);
		 epcrdgval.setOnCheckedChangeListener(new checklistenetr(17));
		 ((CheckBox)findViewById(R.id.vbc_chk3)).setChecked(true);
		 
		 
		 
		 
		// recpchk[0].setChecked(true);
	}
	class  ServiceMainclicker implements OnClickListener
	{
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			for(int i=0;i<servmainchk.length;i++)
			{
				if(v.getId()==servmainchk[i].getId())
				{	
					 if(servmainchk[servmainchk.length-1].isChecked())
					 {
						 ((EditText)findViewById(R.id.sm_otr)).setVisibility(cf.v1.VISIBLE);
						 cf.setFocus(((EditText)findViewById(R.id.sm_otr)));
						 
					 }
					 else
					 {
						 ((EditText)findViewById(R.id.sm_otr)).setVisibility(cf.v1.GONE);
						 ((EditText)findViewById(R.id.sm_otr)).setText("");						
					 }
					 servmainchk[i].setChecked(true);
				}
				else
				{
					servmainchk[i].setChecked(false);
					 ((EditText)findViewById(R.id.sm_otr)).setVisibility(cf.v1.GONE);
					 ((EditText)findViewById(R.id.sm_otr)).setText("");	
				}
			}
		}
	}
	
	class  MainPanelTypeclicker implements OnClickListener
	{
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			for(int i=0;i<mptypechk.length;i++)
			{
				if(v.getId()==mptypechk[i].getId())
				{	
					 if(mptypechk[mptypechk.length-1].isChecked())
					 {
						 ((EditText)findViewById(R.id.mptype_otr)).setVisibility(cf.v1.VISIBLE);
						 cf.setFocus(((EditText)findViewById(R.id.mptype_otr)));
						 
					 }
					 else
					 {
						 ((EditText)findViewById(R.id.mptype_otr)).setVisibility(cf.v1.GONE);
						 ((EditText)findViewById(R.id.mptype_otr)).setText("");						
					 }
					 mptypechk[i].setChecked(true);
				}
				else
				{
					mptypechk[i].setChecked(false);
					 ((EditText)findViewById(R.id.mptype_otr)).setVisibility(cf.v1.GONE);
					 ((EditText)findViewById(R.id.mptype_otr)).setText("");	
				}
			}
		}
	}
	
	class  MainPanelampclicker implements OnClickListener
	{
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			for(int i=0;i<mptachk.length;i++)
			{
				if(v.getId()==mptachk[i].getId())
				{	
					 if(mptachk[mptachk.length-1].isChecked())
					 {
						 ((EditText)findViewById(R.id.mpt_otr)).setVisibility(cf.v1.VISIBLE);
						 cf.setFocus(((EditText)findViewById(R.id.mpt_otr)));
						 ((TextView)findViewById(R.id.mpttxt)).setVisibility(cf.v1.VISIBLE);
					 }
					 else
					 {
						 ((EditText)findViewById(R.id.mpt_otr)).setVisibility(cf.v1.GONE);
						 ((TextView)findViewById(R.id.mpttxt)).setVisibility(cf.v1.GONE);
						 ((EditText)findViewById(R.id.mpt_otr)).setText("");						
					 }
					 mptachk[i].setChecked(true);
				}
				else
				{
					mptachk[i].setChecked(false);
					 ((EditText)findViewById(R.id.mpt_otr)).setVisibility(cf.v1.GONE);
					 ((TextView)findViewById(R.id.mpttxt)).setVisibility(cf.v1.GONE);
					 ((EditText)findViewById(R.id.mpt_otr)).setText("");	
				}
			}
		}
	}
	class Spin_Selectedlistener implements OnItemSelectedListener
	{
		int i;
		public Spin_Selectedlistener(int i) {
			// TODO Auto-generated constructor stub
			this.i=i;
		}

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			if(i==1){
			agestr = agespin.getSelectedItem().toString();System.out.println("agestr="+agestr+mpo);
				if(agestr.equals("Other")){
					((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.VISIBLE);
                     if(mpo==0){cf.setFocus(((EditText)findViewById(R.id.age_otr)));}
				}else if(agestr.equals("Unknown")){
					((EditText)findViewById(R.id.age_otr)).setText("");
					((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.GONE);
					((RadioButton)yicrdgval.findViewWithTag("Unknown")).setChecked(true);
					
				}
				else{System.out.println("spinelse");
				
				if(epval==0)
				{
						((RadioButton)yicrdgval.findViewWithTag("Original")).setChecked(true);
					
					epval=1;	
				}
				else if(epval==1)
				{	
					System.out.println("inside ssval");
					epchk=true;
					try{if(epchk){yicrdgval.clearCheck();}}catch(Exception e){}
				}
				
				
					((EditText)findViewById(R.id.age_otr)).setText("");
					((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.GONE);
				   }
			}
			else if(i==2){
				locstr = locspn.getSelectedItem().toString();
				if(locstr.equals("Other"))
				{
					((EditText)findViewById(R.id.loc_otr)).setVisibility(cf.v1.VISIBLE);
				}
				else
				{
					((EditText)findViewById(R.id.loc_otr)).setText("");
					((EditText)findViewById(R.id.loc_otr)).setVisibility(cf.v1.INVISIBLE);
				}
			}
			else if(i==3){
				mainlocstr = mainlocspn.getSelectedItem().toString();
				if(mainlocstr.equals("Other"))
				{
					((EditText)findViewById(R.id.mploc_otr)).setVisibility(cf.v1.VISIBLE);
				}
				else
				{
					((EditText)findViewById(R.id.mploc_otr)).setText("");
					((EditText)findViewById(R.id.mploc_otr)).setVisibility(cf.v1.INVISIBLE);
				}
			}
			else if(i==4){
				spampstr  = spampsspin.getSelectedItem().toString();
				if(spampstr.equals("Other")){
					((EditText)findViewById(R.id.etotr_spamp)).setVisibility(cf.v1.VISIBLE);
					((TextView)findViewById(R.id.spttxt)).setVisibility(cf.v1.VISIBLE);
					cf.setFocus(((EditText)findViewById(R.id.etotr_spamp)));
				}else{
					((EditText)findViewById(R.id.etotr_spamp)).setText("");
					((EditText)findViewById(R.id.etotr_spamp)).setVisibility(cf.v1.INVISIBLE);
					((TextView)findViewById(R.id.spttxt)).setVisibility(cf.v1.INVISIBLE);
				}
			}
			
		
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	class textwatcher implements TextWatcher
    {
	         public int type;
	         textwatcher(int type)
	    	{
	    		this.type=type;
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {


	    		// TODO Auto-generated method stub
	    		if(this.type==1)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.no_tv_type1),250); 
	    		}
	    		else if(this.type==2)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.eleccom_tv_type1),500); 
	    		}
	    		else if(this.type==4)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.hazno_tv_type1),250);
	    		}
	    		/*else if(this.type==5)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.lsyes_tv_type1),250);
	    		}
	    		else if(this.type==6)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.lcyes_tv_type1),250);
	    		}*/
	    		else if(this.type==7)
	    		{
	    			 if(!s.toString().equals("")){
	    			     if(Character.toString(s.toString().charAt(0)).equals("0"))
		    			 {
		    				 cf.ShowToast("Main Panel Amperage should be Greater than 0.", 0);
		    				 ((EditText)findViewById(R.id.mpt_otr)).setText("");
		    				 cf.hidekeyboard();
		    			 }
	    			 }
	    		}
	    		else if(this.type==8)
	    		{
	    			 if(!s.toString().equals("")){
	    			     if(Character.toString(s.toString().charAt(0)).equals("0"))
		    			 {
		    				 cf.ShowToast("Invalid Entry! Please enter valid year.", 0);
		    				 ((EditText)findViewById(R.id.age_otr)).setText("");
		    				  cf.hidekeyboard();
		    			 }
	    			 }
	    		}
	    		else if(this.type==9)
	    		{
	    			 if(!s.toString().equals("")){
	    			     if(Character.toString(s.toString().charAt(0)).equals("0"))
		    			 {
		    				 cf.ShowToast("Approximate Percentage Update should be Greater than 0 and Less than or Equal to 100.", 0);
		    				 ((EditText)findViewById(R.id.upd_yes)).setText("");
		    				 cf.hidekeyboard();
		    			 }
	    			 }
	    		}
	    		else if(this.type==10)
	    		{
	    			 if(!s.toString().equals("")){
	    			     if(Character.toString(s.toString().charAt(0)).equals("0"))
		    			 {
		    				 cf.ShowToast("Sub Panel Amperage should be Greater than 0.", 0);
		    				 ((EditText)findViewById(R.id.etotr_spamp)).setText("");
		    				 cf.hidekeyboard();
		    			 }
	    			 }
	    		}
	     }

	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,

	    			int after) {
	    		// TODO Auto-generated method stub
	     	}

	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,

	    			int count) {
	    		// TODO Auto-generated method stub
	    	}
	   }	
	class checklistenetr implements OnCheckedChangeListener
	{
    	int i;
		public checklistenetr(int i) {
			// TODO Auto-generated constructor stub
		   this.i=i;
		}

		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			   RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
		        boolean isChecked = checkedRadioButton.isChecked();
		        if (isChecked)
		        {
		          switch (i) {
					case 1:
						esrdgtxt= checkedRadioButton.getText().toString().trim();esck=false;
					  break;
					case 2:
						strdgtxt= checkedRadioButton.getText().toString().trim();stck=false;
					  break;
					case 3:
						emrdgtxt= checkedRadioButton.getText().toString().trim();emck=false;
					  break;
					case 4:
						smrdgtxt= checkedRadioButton.getText().toString().trim();smck=false;
						if(smrdgtxt.equals("Other")){
							((EditText)findViewById(R.id.sm_otr)).setVisibility(cf.v1.VISIBLE);
							 cf.setFocus(((EditText)findViewById(R.id.sm_otr)));
						}else{((EditText)findViewById(R.id.sm_otr)).setVisibility(cf.v1.GONE);
						((EditText)findViewById(R.id.sm_otr)).setText("");}
						((EditText)findViewById(R.id.mpt_otr)).clearFocus();
					  break;
					case 5:
						veprdgtxt= checkedRadioButton.getText().toString().trim();vepck=false;
						if(veprdgtxt.equals("Yes"))
						{
							((LinearLayout)findViewById(R.id.panelshow)).setVisibility(cf.v1.VISIBLE);
						}
						else
						{
							cf.Set_UncheckBox(veptypchk, ((EditText)findViewById(R.id.typ_otr)));
							((EditText)findViewById(R.id.typ_otr)).setText("");
							((EditText)findViewById(R.id.typ_otr)).setVisibility(cf.v1.GONE);
							((LinearLayout)findViewById(R.id.panelshow)).setVisibility(cf.v1.GONE);
						}
					  break;
					case 6:
						escrdgtxt= checkedRadioButton.getText().toString().trim();escck=false;
						if(escrdgtxt.equals("Yes, based on visual observations at the time of inspection")){
							((LinearLayout)findViewById(R.id.nocommentsshow)).setVisibility(cf.v1.GONE);
						    ((LinearLayout)findViewById(R.id.yescondshow)).setVisibility(cf.v1.VISIBLE);}
						else if(escrdgtxt.equals("No-Some deficiencies observed")){
							
							((LinearLayout)findViewById(R.id.nocommentsshow)).setVisibility(cf.v1.VISIBLE);
						    ((LinearLayout)findViewById(R.id.yescondshow)).setVisibility(cf.v1.VISIBLE);
						    ((EditText)findViewById(R.id.nocomment)).setText("No-Some deficiencies observed");
						    }
							else if(escrdgtxt.equals("Repair Needed")){
							
							((LinearLayout)findViewById(R.id.nocommentsshow)).setVisibility(cf.v1.VISIBLE);
						    ((LinearLayout)findViewById(R.id.yescondshow)).setVisibility(cf.v1.VISIBLE);
						    ((EditText)findViewById(R.id.nocomment)).setText("Repair Needed");
						    }
						/*else{
							((LinearLayout)findViewById(R.id.nocommentsshow)).setVisibility(cf.v1.VISIBLE);
							((LinearLayout)findViewById(R.id.yescondshow)).setVisibility(cf.v1.GONE);
							((EditText)findViewById(R.id.nocomment)).setText("");
						}*/
						break;
					case 8:
						hazrdgtxt= checkedRadioButton.getText().toString().trim();System.out.println("hazrdgtxtchekc="+hazrdgtxt);
						if(hazrdgtxt.equals("Yes")){
							((LinearLayout)findViewById(R.id.hazyesshow)).setVisibility(cf.v1.VISIBLE);hzpck=false;
						}else{
							 cf.Set_UncheckBox(hzchk, ((EditText)findViewById(R.id.hz_otr)));
							 ((EditText)findViewById(R.id.hz_otr)).setText("");
							 ((EditText)findViewById(R.id.hz_otr)).setVisibility(cf.v1.GONE);
							 ((EditText)findViewById(R.id.haznocomment)).setText("");
							 ((LinearLayout)findViewById(R.id.hazyesshow)).setVisibility(cf.v1.GONE);
						}
						break;
					/*case 9:
						lsrdgtxt= checkedRadioButton.getText().toString().trim();lsck=false;
						if(lsrdgtxt.equals("Yes")){
							((LinearLayout)findViewById(R.id.lsyescommentsshow)).setVisibility(cf.v1.VISIBLE);
						}else
						{
							((EditText)findViewById(R.id.lsyescomment)).setText("");
							((LinearLayout)findViewById(R.id.lsyescommentsshow)).setVisibility(cf.v1.GONE);
						}
						break;
					case 10:
						lcrdgtxt= checkedRadioButton.getText().toString().trim();lcck=false;
						if(lcrdgtxt.equals("Yes")){
							((LinearLayout)findViewById(R.id.lcyescommentsshow)).setVisibility(cf.v1.VISIBLE);
						}
						else
						{
							((EditText)findViewById(R.id.lcyescomment)).setText("");
							((LinearLayout)findViewById(R.id.lcyescommentsshow)).setVisibility(cf.v1.GONE);
						}
						break;*/
					case 11:
						mmpcrdgtxt= checkedRadioButton.getText().toString().trim();
					  break;
					case 12:
						mptrdgtxt= checkedRadioButton.getText().toString().trim();
					  break;
					case 13:
						dsclrdgtxt= checkedRadioButton.getText().toString().trim();
						if(dsclrdgtxt.equals(""))
							cf.ShowToast("Please select the option for Disconnect Location.", 0);
						else
							if(dsclrdgtxt.equals("Other")){
								((EditText)findViewById(R.id.dsc_location)).setVisibility(cf.v1.VISIBLE);
								//cf.setFocus(((EditText)findViewById(R.id.dsc_location)));
							}else{
								((EditText)findViewById(R.id.dsc_location)).setVisibility(cf.v1.GONE);
								((EditText)findViewById(R.id.dsc_location)).setText("");
							}
								
						break;
					case 14:
						updrdgtxt= checkedRadioButton.getText().toString().trim();
						if(updrdgtxt.equals("Yes")){
							((LinearLayout)findViewById(R.id.perlin)).setVisibility(cf.v1.VISIBLE);
							((EditText)findViewById(R.id.upd_yes)).setText("");
							((TextView)findViewById(R.id.edatid)).setVisibility(cf.v1.VISIBLE);
							//cf.setFocus(((EditText)findViewById(R.id.upd_yes)));
							((LinearLayout)findViewById(R.id.rushow)).setVisibility(cf.v1.VISIBLE);
						}else{
							//cf.Set_UncheckBox(etypechk, ((EditText)findViewById(R.id.upd_yes)));
							((EditText)findViewById(R.id.edlutxt)).setText("");nc=0;
							((CheckBox)findViewById(R.id.ndchk)).setChecked(false);
							((Button)findViewById(R.id.egetdlup)).setEnabled(true);
							((TextView)findViewById(R.id.edatid)).setVisibility(cf.v1.INVISIBLE);
							 try{if(pcck){epcrdgval.clearCheck();}}catch(Exception e){}
							((LinearLayout)findViewById(R.id.perlin)).setVisibility(cf.v1.GONE);
							((LinearLayout)findViewById(R.id.rushow)).setVisibility(cf.v1.GONE);
						} 
						break;
					case 15:
						sprdgtxt= checkedRadioButton.getText().toString().trim();
						if(sprdgtxt.equals("Yes")){
							((LinearLayout)findViewById(R.id.spyes)).setVisibility(cf.v1.VISIBLE);
							
							
						}else{
							((LinearLayout)findViewById(R.id.spyes)).setVisibility(cf.v1.GONE);
						}
						break;
					case 16:
						yicrdgtxt= checkedRadioButton.getText().toString().trim();yicck=false;
						if(yicrdgtxt.equals("Unknown")){epval=1;
							((RadioButton)yicrdgval.findViewWithTag("Unknown")).setChecked(true);
							agespin.setSelection(ageadap.getPosition("Unknown"));
						}
						else
						{
							
								if(yicrdgtxt.equals("Estimate"))
								{
									agespin.setSelection(0);
								}
								epval=2;
						}
						
						/*else if(yicrdgtxt.equals("Known")|| yicrdgtxt.equals("Estimate")){
							agespin.setSelection(0);
						}*/
					  break;
					case 17:
						epcrdgtxt= checkedRadioButton.getText().toString().trim();
						if(epcrdgtxt.equals("Yes"))
						{
							((TextView)findViewById(R.id.edatid)).setVisibility(cf.v1.VISIBLE);
						}
						else
						{
							((TextView)findViewById(R.id.edatid)).setVisibility(cf.v1.INVISIBLE);
						}
					  break;
		          }
		        }
		}

		
	}
	public void clicker(View v){
		switch (v.getId()) {
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.ndchk:
			if(((CheckBox)findViewById(R.id.ndchk)).isChecked())
			{
				nc=1;
				((Button)findViewById(R.id.egetdlup)).setEnabled(false);
				((EditText)findViewById(R.id.edlutxt)).setText("Not Determined");
				((EditText)findViewById(R.id.edlutxt)).setFocusable(false);
				((EditText)findViewById(R.id.upd_yes)).setFocusable(false);
				
			}else{
				nc=0;
				((Button)findViewById(R.id.egetdlup)).setEnabled(true);
				((EditText)findViewById(R.id.edlutxt)).setFocusable(true);
				((EditText)findViewById(R.id.edlutxt)).setText("");
			}
			break;
		case R.id.elecloadcomments:
			/***Call for the electrical issues/hazards present comments***/
			k=2;
			 int elen=((EditText)findViewById(R.id.haznocomment)).getText().toString().length();
			 if(load_comment )
			 {
					load_comment=false;
					int loc[] = new int[2];
					v.getLocationOnScreen(loc);
					Intent in1 = cf.loadComments("Electrical Issues/Hazards Present Comments",loc);
					in1.putExtra("length", elen);
					in1.putExtra("max_length", 250);
					startActivityForResult(in1, cf.loadcomment_code);
			}
			break;
		case R.id.loadcomments:
			/***Call for the comments***/
			k=1;
			 int len=((EditText)findViewById(R.id.eleccomment)).getText().toString().length();
			if(load_comment )
				{
					load_comment=false;
					int loc[] = new int[2];
					v.getLocationOnScreen(loc);
					Intent in1 = cf.loadComments("Overall Electrical Comments",loc);
					in1.putExtra("length", len);
					in1.putExtra("max_length", 500);
					startActivityForResult(in1, cf.loadcomment_code);
				}
			
			 break;
		case R.id.ged_chk:
			 if(((CheckBox)findViewById(R.id.ged_chk)).isChecked()){
				 gedchkval=1;
				 if(ged_sve){chk_values();}
				 System.out.println("retesrdtxt="+ged_sve+retesrdtxt);
				 if(!retesrdtxt.equals("")){
				     showunchkalert(1); 
				 }else{
					 clearged();gedchkval=1;//ged_Insert();
					 ((LinearLayout)findViewById(R.id.gedlin)).setVisibility(cf.v1.GONE);
					// ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
					 ((ImageView)findViewById(R.id.shwged)).setVisibility(cf.v1.VISIBLE);
	 			     ((ImageView)findViewById(R.id.hdged)).setVisibility(cf.v1.GONE);
	 			     ((ImageView)findViewById(R.id.shwged)).setEnabled(false);
	     }
			}else{
				 gedchkval=0;hideotherlayouts();clearged();
				/* try
				 {
					 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_General+ " set fld_inc='"+2+"' where fld_srid='"+cf.selectedhomeid+"'");
				 }
				 catch (Exception E)
				 {
					 String strerrorlog="Updating the WATER SUPPLY SERVICE PLUMBING - FourPoint";
					 cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				 }*/
				 ((ImageView)findViewById(R.id.shwged)).setVisibility(cf.v1.GONE);
			     ((ImageView)findViewById(R.id.hdged)).setVisibility(cf.v1.VISIBLE);
			     ((ImageView)findViewById(R.id.shwged)).setEnabled(true);
				((CheckBox)findViewById(R.id.ged_chk)).setChecked(false);
				 ((LinearLayout)findViewById(R.id.gedlin)).setVisibility(cf.v1.VISIBLE);
				 ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.INVISIBLE);
			}
		
			break;
		case R.id.shwged:
			if(!((CheckBox)findViewById(R.id.ged_chk)).isChecked()){
				hideotherlayouts();
				((CheckBox)findViewById(R.id.ged_chk)).setChecked(false);
				((LinearLayout)findViewById(R.id.gedlin)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdged)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.shwged)).setVisibility(cf.v1.GONE);
			}
			break;
		case R.id.hdged:
			((ImageView)findViewById(R.id.shwged)).setVisibility(cf.v1.VISIBLE);
			((ImageView)findViewById(R.id.hdged)).setVisibility(cf.v1.GONE);
			((LinearLayout)findViewById(R.id.gedlin)).setVisibility(cf.v1.GONE);
			break;
		case R.id.shwep:
			epval=0;
			if(!((CheckBox)findViewById(R.id.epe_chk)).isChecked()){
				hideotherlayouts();
				((CheckBox)findViewById(R.id.epe_chk)).setChecked(false);
				((LinearLayout)findViewById(R.id.epelin)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdep)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.shwep)).setVisibility(cf.v1.GONE);
			}
			break;
		case R.id.hdep:
			((ImageView)findViewById(R.id.shwep)).setVisibility(cf.v1.VISIBLE);
			((ImageView)findViewById(R.id.hdep)).setVisibility(cf.v1.GONE);
			((LinearLayout)findViewById(R.id.epelin)).setVisibility(cf.v1.GONE);
			break;
		case R.id.epe_chk:
			if(((CheckBox)findViewById(R.id.epe_chk)).isChecked()){
				 elechkval=1;System.out.println("chk="+chk1+"chk2="+chk2);
				 if(chk1 && chk2){chk_values();}
				 System.out.println("retmmpcondtxt"+retmmpcondtxt+"retspptxt"+retspptxt);
				 if(!retmmpcondtxt.equals("")|| !retspptxt.equals("")){
					 System.out.println("tess");
				     showunchkalert(2); 
				 }else{
					 System.out.println("elese");
					 clearep();elechkval=1;//mainpanel_Insert();sp_Insert();
					 ((LinearLayout)findViewById(R.id.epelin)).setVisibility(cf.v1.GONE);
					// ((TextView)findViewById(R.id.savetxt2)).setVisibility(cf.v1.VISIBLE);
					 ((ImageView)findViewById(R.id.shwep)).setVisibility(cf.v1.VISIBLE);
	 			     ((ImageView)findViewById(R.id.hdep)).setVisibility(cf.v1.GONE);
	 			     ((ImageView)findViewById(R.id.shwep)).setEnabled(false);
			   }
			}else{
				 elechkval=0;hideotherlayouts();clearep();
				 ((LinearLayout)findViewById(R.id.spyes)).setVisibility(cf.v1.VISIBLE);
				 /*try
				 {
					 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_MainPanel+ " set fld_electinc='"+2+"' where fld_srid='"+cf.selectedhomeid+"'");
				     cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_SubPanel+ " set fld_electinc='"+2+"' where fld_srid='"+cf.selectedhomeid+"'");
				 }
				 catch (Exception E)
				 {
					 String strerrorlog="Updating the WATER SUPPLY SERVICE PLUMBING - FourPoint";
					 cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				 }*/
				 ((ImageView)findViewById(R.id.shwep)).setVisibility(cf.v1.GONE);
			     ((ImageView)findViewById(R.id.hdep)).setVisibility(cf.v1.VISIBLE);
			     ((ImageView)findViewById(R.id.shwep)).setEnabled(true);
				((CheckBox)findViewById(R.id.epe_chk)).setChecked(false);
				 ((LinearLayout)findViewById(R.id.epelin)).setVisibility(cf.v1.VISIBLE);
				((TextView)findViewById(R.id.savetxt2)).setVisibility(cf.v1.INVISIBLE);
			}
		
			break;
		case R.id.shwvep:
			if(!((CheckBox)findViewById(R.id.vep_chk)).isChecked()){
				hideotherlayouts();
				((CheckBox)findViewById(R.id.vep_chk)).setChecked(false);
				((LinearLayout)findViewById(R.id.veplin)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdvep)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.shwvep)).setVisibility(cf.v1.GONE);
			}
			break;
		case R.id.hdvep:
			((ImageView)findViewById(R.id.shwvep)).setVisibility(cf.v1.VISIBLE);
			((ImageView)findViewById(R.id.hdvep)).setVisibility(cf.v1.GONE);
			((LinearLayout)findViewById(R.id.veplin)).setVisibility(cf.v1.GONE);
			break;
		case R.id.vep_chk:
			if(((CheckBox)findViewById(R.id.vep_chk)).isChecked()){
				vepchkval=1;
				if(vep_sve){chk_values();}
				 if(!retvprdtxt.equals("")){
				     showunchkalert(3); 
				 }else{
					 clearvep();vepchkval=1;//VEP_Insert();
					 ((LinearLayout)findViewById(R.id.veplin)).setVisibility(cf.v1.GONE);
					 //((TextView)findViewById(R.id.savetxt3)).setVisibility(cf.v1.VISIBLE);
					 ((ImageView)findViewById(R.id.shwvep)).setVisibility(cf.v1.VISIBLE);
	 			     ((ImageView)findViewById(R.id.hdvep)).setVisibility(cf.v1.GONE);
	 			     ((ImageView)findViewById(R.id.shwvep)).setEnabled(false);
			    }
			}else{
				vepchkval=0;hideotherlayouts();clearvep();
				/* try
				 {
					 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Panel+ " set fld_inc='"+2+"' where fld_srid='"+cf.selectedhomeid+"'");
				 }
				 catch (Exception E)
				 {
					 String strerrorlog="Updating the WATER SUPPLY SERVICE PLUMBING - FourPoint";
					 cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				 }*/
				 ((ImageView)findViewById(R.id.shwvep)).setVisibility(cf.v1.GONE);
			     ((ImageView)findViewById(R.id.hdvep)).setVisibility(cf.v1.VISIBLE);
			     ((ImageView)findViewById(R.id.shwvep)).setEnabled(true);
				((CheckBox)findViewById(R.id.vep_chk)).setChecked(false);
				 ((LinearLayout)findViewById(R.id.veplin)).setVisibility(cf.v1.VISIBLE);
				 ((TextView)findViewById(R.id.savetxt3)).setVisibility(cf.v1.INVISIBLE);
			}
	
    		break;
		case R.id.shwwir:
			if(!((CheckBox)findViewById(R.id.wir_chk)).isChecked()){
				hideotherlayouts();
				((CheckBox)findViewById(R.id.wir_chk)).setChecked(false);
				((LinearLayout)findViewById(R.id.wirlin)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdwir)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.shwwir)).setVisibility(cf.v1.GONE);
			}
			break;
		case R.id.hdwir:
			((ImageView)findViewById(R.id.shwwir)).setVisibility(cf.v1.VISIBLE);
			((ImageView)findViewById(R.id.hdwir)).setVisibility(cf.v1.GONE);
			((LinearLayout)findViewById(R.id.wirlin)).setVisibility(cf.v1.GONE);
			break;
		case R.id.wir_chk:
			if(((CheckBox)findViewById(R.id.wir_chk)).isChecked()){
				wirchkval=1;
				if(wir_sve){chk_values();}
				 if(!retwirtxt.equals("")){
				     showunchkalert(4); 
				 }else{
					 clearwir();wirchkval=1;//WIR_Insert();
					 ((LinearLayout)findViewById(R.id.wirlin)).setVisibility(cf.v1.GONE);
					 //((TextView)findViewById(R.id.savetxtwir)).setVisibility(cf.v1.VISIBLE);
					 ((ImageView)findViewById(R.id.shwwir)).setVisibility(cf.v1.VISIBLE);
	 			     ((ImageView)findViewById(R.id.hdwir)).setVisibility(cf.v1.GONE);
	 			     ((ImageView)findViewById(R.id.shwwir)).setEnabled(false);
			   }
			}else{
				wirchkval=0;hideotherlayouts();clearwir();
				 /*try
				 {
					 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Wiring+ " set fld_wirinc='"+2+"' where fld_srid='"+cf.selectedhomeid+"'");
				 }
				 catch (Exception E)
				 {
					 String strerrorlog="Updating the WATER SUPPLY SERVICE PLUMBING - FourPoint";
					 cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				 }*/
				 ((ImageView)findViewById(R.id.shwwir)).setVisibility(cf.v1.GONE);
			     ((ImageView)findViewById(R.id.hdwir)).setVisibility(cf.v1.VISIBLE);
			     ((ImageView)findViewById(R.id.shwwir)).setEnabled(true);
				((CheckBox)findViewById(R.id.wir_chk)).setChecked(false);
				 ((LinearLayout)findViewById(R.id.wirlin)).setVisibility(cf.v1.VISIBLE);
				 ((TextView)findViewById(R.id.savetxtwir)).setVisibility(cf.v1.INVISIBLE);
			}
	
			break;
		/*case R.id.shwrec:
			if(!((CheckBox)findViewById(R.id.recp_chk)).isChecked()){
				hideotherlayouts();
				((CheckBox)findViewById(R.id.recp_chk)).setChecked(false);
				((LinearLayout)findViewById(R.id.recplin)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdrec)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.shwrec)).setVisibility(cf.v1.GONE);
			}
			break;
		case R.id.hdrec:
			((ImageView)findViewById(R.id.shwrec)).setVisibility(cf.v1.VISIBLE);
			((ImageView)findViewById(R.id.hdrec)).setVisibility(cf.v1.GONE);
			((LinearLayout)findViewById(R.id.recplin)).setVisibility(cf.v1.GONE);
			break;
		case R.id.recp_chk:
			if(((CheckBox)findViewById(R.id.recp_chk)).isChecked()){
				 recchkval=1;
				 if(recp_sve){chk_values();}
				 if(!retrecptxt.equals("")){
				     showunchkalert(5); 
				 }else{
					 clearrec();recchkval=1;//RECEP_Insert();
					 ((LinearLayout)findViewById(R.id.recplin)).setVisibility(cf.v1.GONE);
					// ((TextView)findViewById(R.id.savetxt4)).setVisibility(cf.v1.VISIBLE);
					 ((ImageView)findViewById(R.id.shwrec)).setVisibility(cf.v1.VISIBLE);
	 			     ((ImageView)findViewById(R.id.hdrec)).setVisibility(cf.v1.GONE);
	 			     ((ImageView)findViewById(R.id.shwrec)).setEnabled(false);
				  }
			}else{
				recchkval=0;hideotherlayouts();clearrec();
				 try
				 {
					 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Receptacles+ " set fld_inc='"+2+"' where fld_srid='"+cf.selectedhomeid+"'");
				 }
				 catch (Exception E)
				 {
					 String strerrorlog="Updating the WATER SUPPLY SERVICE PLUMBING - FourPoint";
					 cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				 }
				 ((ImageView)findViewById(R.id.shwrec)).setVisibility(cf.v1.GONE);
			     ((ImageView)findViewById(R.id.hdrec)).setVisibility(cf.v1.VISIBLE);
			     ((ImageView)findViewById(R.id.shwrec)).setEnabled(true);
				((CheckBox)findViewById(R.id.recp_chk)).setChecked(false);
				 ((LinearLayout)findViewById(R.id.recplin)).setVisibility(cf.v1.VISIBLE);
				 ((TextView)findViewById(R.id.savetxt4)).setVisibility(cf.v1.INVISIBLE);
			}
	
			break;*/
		/*case R.id.shwvbc:
			if(!((CheckBox)findViewById(R.id.vbc_chk)).isChecked()){
				hideotherlayouts();
				((CheckBox)findViewById(R.id.vbc_chk)).setChecked(false);
				((LinearLayout)findViewById(R.id.vbclin)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdvbc)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.shwvbc)).setVisibility(cf.v1.GONE);
			}
			break;
		case R.id.hdvbc:
			((ImageView)findViewById(R.id.shwvbc)).setVisibility(cf.v1.VISIBLE);
			((ImageView)findViewById(R.id.hdvbc)).setVisibility(cf.v1.GONE);
			((LinearLayout)findViewById(R.id.vbclin)).setVisibility(cf.v1.GONE);
			break;
		case R.id.vbc_chk:
			if(((CheckBox)findViewById(R.id.vbc_chk)).isChecked()){
				vbcchkval=1;
				if(vbc_sve){chk_values();}
				 if(!detretvbc.equals("")){
				     showunchkalert(6); 
				 }else{
					 clearvbc();vbcchkval=1;//VBC_Insert();
					 ((LinearLayout)findViewById(R.id.vbclin)).setVisibility(cf.v1.GONE);
					 //((TextView)findViewById(R.id.savetxt5)).setVisibility(cf.v1.VISIBLE);
					 ((ImageView)findViewById(R.id.shwvbc)).setVisibility(cf.v1.VISIBLE);
	 			     ((ImageView)findViewById(R.id.hdvbc)).setVisibility(cf.v1.GONE);
	 			     ((ImageView)findViewById(R.id.shwvbc)).setEnabled(false);
				  }
			}else{
				vbcchkval=0;hideotherlayouts();clearvbc();
				 try
				 {
					 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Details+ " set fld_vbcinc='"+2+"' where fld_srid='"+cf.selectedhomeid+"'");
				 }
				 catch (Exception E)
				 {
					 String strerrorlog="Updating the WATER SUPPLY SERVICE PLUMBING - FourPoint";
					 cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				 }
				 ((ImageView)findViewById(R.id.shwvbc)).setVisibility(cf.v1.GONE);
			     ((ImageView)findViewById(R.id.hdvbc)).setVisibility(cf.v1.VISIBLE);
			     ((ImageView)findViewById(R.id.shwvbc)).setEnabled(true);
				((CheckBox)findViewById(R.id.vbc_chk)).setChecked(false);
				 ((LinearLayout)findViewById(R.id.vbclin)).setVisibility(cf.v1.VISIBLE);
				 ((TextView)findViewById(R.id.savetxt5)).setVisibility(cf.v1.INVISIBLE);
			}
		
			break;*/
		case R.id.shwlf:
			if(!((CheckBox)findViewById(R.id.lfr_chk)).isChecked()){
				hideotherlayouts();
				((CheckBox)findViewById(R.id.lfr_chk)).setChecked(false);
				((LinearLayout)findViewById(R.id.lfrlin)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdlf)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.shwlf)).setVisibility(cf.v1.GONE);
			}
			break;
		case R.id.hdlf:
			((ImageView)findViewById(R.id.shwlf)).setVisibility(cf.v1.VISIBLE);
			((ImageView)findViewById(R.id.hdlf)).setVisibility(cf.v1.GONE);
			((LinearLayout)findViewById(R.id.lfrlin)).setVisibility(cf.v1.GONE);
			break;
		case R.id.lfr_chk:
			if(((CheckBox)findViewById(R.id.lfr_chk)).isChecked()){
				lfrchkval=1;
				if(lfr_sve){chk_values();}
				 if(!detretlfr.equals("")){
				     showunchkalert(7); 
				 }else{
					 clearlf();lfrchkval=1;//LFR_Insert();
					 ((LinearLayout)findViewById(R.id.lfrlin)).setVisibility(cf.v1.GONE);
					// ((TextView)findViewById(R.id.savetxt6)).setVisibility(cf.v1.VISIBLE);
					 ((ImageView)findViewById(R.id.shwlf)).setVisibility(cf.v1.VISIBLE);
	 			     ((ImageView)findViewById(R.id.hdlf)).setVisibility(cf.v1.GONE);
	 			     ((ImageView)findViewById(R.id.shwlf)).setEnabled(false);
				 }
			}else{
				lfrchkval=0;hideotherlayouts();clearlf();
				/* try
				 {
					 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Details+ " set fld_lfrin='"+2+"' where fld_srid='"+cf.selectedhomeid+"'");
				 }
				 catch (Exception E)
				 {
					 String strerrorlog="Updating the WATER SUPPLY SERVICE PLUMBING - FourPoint";
					 cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				 }*/
				 ((ImageView)findViewById(R.id.shwlf)).setVisibility(cf.v1.GONE);
			     ((ImageView)findViewById(R.id.hdlf)).setVisibility(cf.v1.VISIBLE);
			     ((ImageView)findViewById(R.id.shwlf)).setEnabled(true);
				((CheckBox)findViewById(R.id.lfr_chk)).setChecked(false);
				 ((LinearLayout)findViewById(R.id.lfrlin)).setVisibility(cf.v1.VISIBLE);
				((TextView)findViewById(R.id.savetxt6)).setVisibility(cf.v1.INVISIBLE);
			}
		
			break;
		case R.id.shwgo:
			if(!((CheckBox)findViewById(R.id.go_chk)).isChecked()){
				hideotherlayouts();
				((CheckBox)findViewById(R.id.go_chk)).setChecked(false);
				((LinearLayout)findViewById(R.id.golin)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdgo)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.shwgo)).setVisibility(cf.v1.GONE);
			}
			break;
		case R.id.hdgo:
			((ImageView)findViewById(R.id.shwgo)).setVisibility(cf.v1.VISIBLE);
			((ImageView)findViewById(R.id.hdgo)).setVisibility(cf.v1.GONE);
			((LinearLayout)findViewById(R.id.golin)).setVisibility(cf.v1.GONE);
			break;
		case R.id.go_chk:
			if(((CheckBox)findViewById(R.id.go_chk)).isChecked()){
				gochkval=1;
				if(go_sve){chk_values();}
				 if(!rethaztxt.equals("")){
				     showunchkalert(8); 
				 }else{
					 cleargo();gochkval=1;//GO_Insert();
					 ((LinearLayout)findViewById(R.id.golin)).setVisibility(cf.v1.GONE);
					 //((TextView)findViewById(R.id.savetxt7)).setVisibility(cf.v1.VISIBLE);
					 ((ImageView)findViewById(R.id.shwgo)).setVisibility(cf.v1.VISIBLE);
	 			     ((ImageView)findViewById(R.id.hdgo)).setVisibility(cf.v1.GONE);
	 			     ((ImageView)findViewById(R.id.shwgo)).setEnabled(false);
			   }
			}else{
				gochkval=0;hideotherlayouts();cleargo();
				/* try
				 {
					 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_GO+ " set fld_goinc='"+2+"' where fld_srid='"+cf.selectedhomeid+"'");
				 }
				 catch (Exception E)
				 {
					 String strerrorlog="Updating the WATER SUPPLY SERVICE PLUMBING - FourPoint";
					 cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				 }*/
				 ((ImageView)findViewById(R.id.shwgo)).setVisibility(cf.v1.GONE);
			     ((ImageView)findViewById(R.id.hdgo)).setVisibility(cf.v1.VISIBLE);
			     ((ImageView)findViewById(R.id.shwgo)).setEnabled(true);
				((CheckBox)findViewById(R.id.go_chk)).setChecked(false);
				 ((LinearLayout)findViewById(R.id.golin)).setVisibility(cf.v1.VISIBLE);
				 ((TextView)findViewById(R.id.savetxt7)).setVisibility(cf.v1.INVISIBLE);
			}
		
			break;
		/*case R.id.shwcon:
			if(!((CheckBox)findViewById(R.id.oes_chk)).isChecked()){
				hideotherlayouts();
				((CheckBox)findViewById(R.id.oes_chk)).setChecked(false);
				((LinearLayout)findViewById(R.id.oeslin)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdcon)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.shwcon)).setVisibility(cf.v1.GONE);
			}
			break;
		case R.id.hdcon:
			((ImageView)findViewById(R.id.shwcon)).setVisibility(cf.v1.VISIBLE);
			((ImageView)findViewById(R.id.hdcon)).setVisibility(cf.v1.GONE);
			((LinearLayout)findViewById(R.id.oeslin)).setVisibility(cf.v1.GONE);
			break;
		case R.id.oes_chk:
			if(((CheckBox)findViewById(R.id.oes_chk)).isChecked()){
				oeschkval=1;
				if(oes_sve){chk_values();}
				 if(!detretoes.equals("")){
				     showunchkalert(9); 
				 }else{
					 clearcon();oeschkval=1;//OES_Insert();
					 ((LinearLayout)findViewById(R.id.oeslin)).setVisibility(cf.v1.GONE);
					 //((TextView)findViewById(R.id.savetxt8)).setVisibility(cf.v1.VISIBLE);
					 ((ImageView)findViewById(R.id.shwcon)).setVisibility(cf.v1.VISIBLE);
	 			     ((ImageView)findViewById(R.id.hdcon)).setVisibility(cf.v1.GONE);
	 			     ((ImageView)findViewById(R.id.shwcon)).setEnabled(false);
				  }
			}else{
				oeschkval=0;hideotherlayouts();clearcon();
				 try
				 {
					 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Details+ " set fld_oesinc='"+2+"' where fld_srid='"+cf.selectedhomeid+"'");
				 }
				 catch (Exception E)
				 {
					 String strerrorlog="Updating the WATER SUPPLY SERVICE PLUMBING - FourPoint";
					 cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				 }
				 ((ImageView)findViewById(R.id.shwcon)).setVisibility(cf.v1.GONE);
			     ((ImageView)findViewById(R.id.hdcon)).setVisibility(cf.v1.VISIBLE);
			     ((ImageView)findViewById(R.id.shwcon)).setEnabled(true);
				((CheckBox)findViewById(R.id.oes_chk)).setChecked(false);
				((LinearLayout)findViewById(R.id.oeslin)).setVisibility(cf.v1.VISIBLE);
				((TextView)findViewById(R.id.savetxt8)).setVisibility(cf.v1.INVISIBLE);
			}
	
			break;*/
		/*case R.id.gedsave:
			if(((CheckBox)findViewById(R.id.ged_chk)).isChecked())
			{
				gedchkval=1;
			}
			else{
			    gedchkval=0;
				if(esrdgtxt.equals(""))
				{
					cf.ShowToast("Please select the option for Electrical Service.", 0);
					
				}
				else
				{	if(strdgtxt.equals(""))
						cf.ShowToast("Please select the option for Service Type.", 0);
					else
						if(emrdgtxt.equals(""))
							cf.ShowToast("Please select the option for Electric Meter.", 0);
						else
							if(locstr.equals("--Select--"))
							{
								cf.ShowToast("Please select Location.", 0);
							}
							else
							{
								if(locstr.equals("Other"))
								{
									if(((EditText)findViewById(R.id.loc_otr)).getText().toString().trim().equals("")){
										   cf.ShowToast("Please enter the Other text under Location.", 0);
										   cf.setFocus(((EditText)findViewById(R.id.loc_otr)));
									}
									else
									{
										nextged();
									}
								}
								else
								{
									nextged();
								}
								
							}
				}
			}
			break;*/
		 case R.id.typ_chk9:/** TYPE CHECKBOX OTHER **/
			  if(veptypchk[veptypchk.length-1].isChecked())
			  { 
				  ((EditText)findViewById(R.id.typ_otr)).setVisibility(v.VISIBLE);
				  cf.setFocus(((EditText)findViewById(R.id.typ_otr)));
			  }
			  else
			  {
				 cf.clearother( ((EditText)findViewById(R.id.typ_otr)));
			  }
			  break;
		 /*case R.id.vepsave:
			 if(((CheckBox)findViewById(R.id.vep_chk)).isChecked())
			 {
					 vepchkval=1;
			 }
			 else
			 {
				     vepchkval=0;
					 if(veprdgtxt.equals(""))
						 cf.ShowToast("Please select the option for Visible Electrical Panel/Disconnect Issues Noted.", 0);
					 else
						 if(veprdgtxt.equals("Yes")){
							 veptypchk_vlaue= cf.getselected_chk(veptypchk);
					    	 String veptypchk_valueotr=(veptypchk[veptypchk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.typ_otr)).getText().toString():""; // append the other text value in to the selected option
					    	 veptypchk_vlaue+=veptypchk_valueotr;
					    	 if(veptypchk_vlaue.equals(""))
							 {
								 cf.ShowToast("Please select the option for Type." , 0);
							 }
					    	 else
					    	 {
					    		 if(veptypchk[veptypchk.length-1].isChecked())
								 {
									 if(veptypchk_valueotr.trim().equals("&#40;"))
									 {
										 cf.ShowToast("Please enter the Other text for Type.", 0);
										 cf.setFocus(((EditText)findViewById(R.id.typ_otr)));
									 }
									 else
									 {
										VEP_Insert();	
									 }
								 }
								 else
								 {
									 VEP_Insert();
								 }
				    	    }
						 }else{
							 VEP_Insert();
						 }
			 }	
			 break;
		 case R.id.recpsave:
			 if(((CheckBox)findViewById(R.id.recp_chk)).isChecked())
			 {
					 recchkval=1;
			 }
			 else
			 {       recchkval=0;
					 rectypchk_value= cf.getselected_chk(recpchk);
			    	 if(rectypchk_value.equals(""))
					 {
						 cf.ShowToast("Please select the option for Receptacles/Outlets." , 0);
					 }
			    	 else
			    	 {
			    		 gfcrectypchk_value= cf.getselected_chk(gfcrecpchk);
				    	 if(gfcrectypchk_value.equals(""))
						 {
							 cf.ShowToast("Please select the option for GFCI Receptacles Location." , 0);
						 }
				    	 else
				    	 {
				    		RECEP_Insert();
			    	     }
		    	     }
			 }
			 break;
		 case R.id.vbcsave:
			 if(((CheckBox)findViewById(R.id.vbc_chk)).isChecked())
			 {
					 vbcchkval=1;
			 }
			 else
			 {
				     vbcchkval=0;
					 vbcchk_value= cf.getselected_chk(vbcchk);
			    	 if(vbcchk_value.equals(""))
					 {
						 cf.ShowToast("Please select the option for Visible Branch Circuity." , 0);
					 }
			    	 else
			    	 {
			    		VBC_Insert();
		    	     }
			 }
			 break;
		case R.id.lfrsave:
			 if(((CheckBox)findViewById(R.id.lfr_chk)).isChecked())
			 {
					 lfrchkval=1;
			 }
			 else
			 {
				     lfrchkval=0;
					 lfrchk_vlaue= cf.getselected_chk(lfrchk);
			    	 String lfrchk_valueotr=(lfrchk[lfrchk.length-1].isChecked())? "&#94;"+((EditText)findViewById(R.id.lfr_otr)).getText().toString():""; // append the other text value in to the selected option
			    	 lfrchk_vlaue+=lfrchk_valueotr;
			    	 if(lfrchk_vlaue.equals(""))
					 {
						 cf.ShowToast("Please select the option for Light Fixtures/Receptacles." , 0);
					 }
			    	 else
			    	 {
			    		 LFR_Insert();
			    
		    	 }
			 }
			 break;
		 case R.id.oessave:
			 if(((CheckBox)findViewById(R.id.oes_chk)).isChecked())
			 {
				oeschkval=1;
			 }
			 else
			 {
				 oeschkval=0;
				if(escrdgtxt.equals("")){
					cf.ShowToast("Please select the option for Is the electrical system generally in good working order.", 0);
				}else{
					if(!escrdgtxt.equals("Yes, based on visual observations at the time of inspection")){
						if(((EditText)findViewById(R.id.nocomment)).getText().toString().trim().equals("")){
							cf.ShowToast("Please enter the comments.", 0);
							 cf.setFocus(((EditText)findViewById(R.id.nocomment)));
						}else{
							OES_Insert();
						}
					}else{
						OES_Insert();
					}
				}
			 }
			 break;
		 case R.id.gosave:
			 if(((CheckBox)findViewById(R.id.go_chk)).isChecked())
			 {
				gochkval=1;
				
			}else{
				 gochkval=0;GO_Hazards();
			 }
			 break;*/
		 case R.id.hz_chk10:/** OBSERVATION CHECKBOX OTHER **/
			  if(hzchk[hzchk.length-1].isChecked())
			  { 
				  ((EditText)findViewById(R.id.hz_otr)).setVisibility(v.VISIBLE);
				  cf.setFocus(((EditText)findViewById(R.id.hz_otr)));
			  }
			  else
			  {
				  cf.clearother(((EditText)findViewById(R.id.hz_otr)));
			  }
			  break;
		 case R.id.wir_chk5:/** TYPE CHECKBOX OTHER **/
			  if(wirchk[wirchk.length-1].isChecked())
			  { 
				  ((EditText)findViewById(R.id.wir_otr)).setVisibility(v.VISIBLE);
				  cf.setFocus(((EditText)findViewById(R.id.wir_otr)));
			  }
			  else
			  {
				  cf.clearother(((EditText)findViewById(R.id.wir_otr)));
			  }
			  break;
		 /*case R.id.wirsave:
			 if(((CheckBox)findViewById(R.id.wir_chk)).isChecked())
			 {
				 wirchkval=1;
			 }
			 else
			 {
				 wirchkval=0;
				 wirchk_vlaue= cf.getselected_chk(wirchk);
		    	 String wirchk_valueotr=(wirchk[wirchk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.wir_otr)).getText().toString():""; // append the other text value in to the selected option
		    	 wirchk_vlaue+=wirchk_valueotr;
		    	 if(wirchk_vlaue.equals(""))
				 {
					 cf.ShowToast("Please select the option for Wiring Type." , 0);
				 }
		    	 else
		    	 {
		    		 if(wirchk[wirchk.length-1].isChecked())
					 {
						 if(wirchk_valueotr.trim().equals("&#40;"))
						 {
							 cf.ShowToast("Please enter the Other text for Wiring Type.", 0);
							 cf.setFocus(((EditText)findViewById(R.id.wir_otr)));
						 }
						 else
						 {
							// wirchk_vlaue +=(wirchk[wirchk.length-1].isChecked())? "^"+((EditText)findViewById(R.id.wir_otr)).getText().toString():""; // append the other text value in to the selected option
							 chk_aluminium();
						 }
					 }
					 else
					 {
						 chk_aluminium();
					 }
	    	    }
			 }
			 break;*/
		 case R.id.wir_chk3:
			 if(((CheckBox)findViewById(R.id.wir_chk3)).isChecked())
			 {
				 ((LinearLayout)findViewById(R.id.aluminshow)).setVisibility(cf.v1.VISIBLE);
			 }else{
				 ((LinearLayout)findViewById(R.id.aluminshow)).setVisibility(cf.v1.GONE);
				 cf.Set_UncheckBox(alumchk, ((EditText)findViewById(R.id.wir_otr)));
				 cf.Set_UncheckBox(alumfstchk, ((EditText)findViewById(R.id.wir_otr)));
				 cf.Set_UncheckBox(alumsecchk, ((EditText)findViewById(R.id.wir_otr)));
				 cf.Set_UncheckBox(alumthdchk, ((EditText)findViewById(R.id.wir_otr)));
				 ((CheckBox)findViewById(R.id.alu_chk1)).setEnabled(true);
				 ((CheckBox)findViewById(R.id.alu_chk2)).setEnabled(true);
				 ((CheckBox)findViewById(R.id.alu_chk3)).setEnabled(true);
				 ((LinearLayout)findViewById(R.id.firstchklin)).setVisibility(cf.v1.GONE);
				 ((LinearLayout)findViewById(R.id.secondchklin)).setVisibility(cf.v1.GONE);
				 ((LinearLayout)findViewById(R.id.thirdchklin)).setVisibility(cf.v1.GONE);
			 }
			 break;
		 case R.id.alu_chk1:
			 if(((CheckBox)findViewById(R.id.alu_chk1)).isChecked()&& !((CheckBox)findViewById(R.id.alu_chk4)).isChecked())
			 {
				 ((LinearLayout)findViewById(R.id.firstchklin)).setVisibility(cf.v1.VISIBLE);
				 
			 }else{
				 ((LinearLayout)findViewById(R.id.firstchklin)).setVisibility(cf.v1.GONE);
				 cf.Set_UncheckBox(alumfstchk, ((EditText)findViewById(R.id.wir_otr)));alumfstchk_vlaue="";
			 }
			 break;
		 case R.id.alu_chk2:
			 if(((CheckBox)findViewById(R.id.alu_chk2)).isChecked()&& !((CheckBox)findViewById(R.id.alu_chk4)).isChecked())
			 {
				 ((LinearLayout)findViewById(R.id.secondchklin)).setVisibility(cf.v1.VISIBLE);
			 }else{
				 ((LinearLayout)findViewById(R.id.secondchklin)).setVisibility(cf.v1.GONE);
				 cf.Set_UncheckBox(alumsecchk, ((EditText)findViewById(R.id.wir_otr)));alumsecchk_vlaue="";
			 }
			 break;
		 case R.id.alu_chk3:
			 if(((CheckBox)findViewById(R.id.alu_chk3)).isChecked()&& !((CheckBox)findViewById(R.id.alu_chk4)).isChecked())
			 {
				 ((LinearLayout)findViewById(R.id.thirdchklin)).setVisibility(cf.v1.VISIBLE);
			 }else{
				 ((LinearLayout)findViewById(R.id.thirdchklin)).setVisibility(cf.v1.GONE);
				 cf.Set_UncheckBox(alumthdchk, ((EditText)findViewById(R.id.wir_otr)));alumthdchk_vlaue="";
			 }
			 break;
		 case R.id.alu_chk4:
			 if(((CheckBox)findViewById(R.id.alu_chk4)).isChecked())
			 {
				 ((LinearLayout)findViewById(R.id.thirdchklin)).setVisibility(cf.v1.GONE);
				 ((LinearLayout)findViewById(R.id.secondchklin)).setVisibility(cf.v1.GONE);
				 ((LinearLayout)findViewById(R.id.firstchklin)).setVisibility(cf.v1.GONE);
				 ((CheckBox)findViewById(R.id.alu_chk1)).setChecked(false);((CheckBox)findViewById(R.id.alu_chk1)).setEnabled(false);
				 ((CheckBox)findViewById(R.id.alu_chk2)).setChecked(false);((CheckBox)findViewById(R.id.alu_chk2)).setEnabled(false);
				 ((CheckBox)findViewById(R.id.alu_chk3)).setChecked(false);((CheckBox)findViewById(R.id.alu_chk3)).setEnabled(false);
			 }else{
				 ((CheckBox)findViewById(R.id.alu_chk1)).setEnabled(true);
				 ((CheckBox)findViewById(R.id.alu_chk2)).setEnabled(true);
				 ((CheckBox)findViewById(R.id.alu_chk3)).setEnabled(true);
				 if(((CheckBox)findViewById(R.id.alu_chk3)).isChecked())
				 {((LinearLayout)findViewById(R.id.thirdchklin)).setVisibility(cf.v1.VISIBLE);}
				 if(((CheckBox)findViewById(R.id.alu_chk2)).isChecked())
				 {((LinearLayout)findViewById(R.id.secondchklin)).setVisibility(cf.v1.VISIBLE);}
				 if(((CheckBox)findViewById(R.id.alu_chk1)).isChecked())
				 {((LinearLayout)findViewById(R.id.firstchklin)).setVisibility(cf.v1.VISIBLE);}
				 cf.Set_UncheckBox(alumfstchk, ((EditText)findViewById(R.id.wir_otr)));
				 cf.Set_UncheckBox(alumsecchk, ((EditText)findViewById(R.id.wir_otr)));
				 cf.Set_UncheckBox(alumthdchk, ((EditText)findViewById(R.id.wir_otr)));
			 }
			 break;
		/* case R.id.mpsave:
			 if(((CheckBox)findViewById(R.id.epe_chk)).isChecked())
			 {
				 elechkval=1;
			 }
			 else
			 {
				 elechkval=0;
				 if(mmpcrdgtxt.equals(""))
					 cf.ShowToast("Please select the option for Meter to Main Panel Conductor.", 0);
				 else{
					 if(mptrdgtxt.equals("")){
						 cf.ShowToast("Please select the option for Main Panel Type.", 0);
					 }else{
						 mptachk_vlaue= cf.getselected_chk(mptachk);
				    	 String mptachk_valueotr=(mptachk[mptachk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.mpt_otr)).getText().toString():""; // append the other text value in to the selected option
				    	 mptachk_vlaue+=mptachk_valueotr;
				    	 if(mptachk_vlaue.equals(""))
						 {
							 cf.ShowToast("Please select the option for Main Panel Amperage." , 0);
						 }
				    	 else
				    	 {
				    		 if(mptachk[mptachk.length-1].isChecked())
							 {
								 if(mptachk_valueotr.trim().equals("&#40;"))
								 {
									 cf.ShowToast("Please enter the Other text for  Main Panel Amperage.", 0);
									 cf.setFocus(((EditText)findViewById(R.id.mpt_otr)));
								 }
								 else
								 {
									// mptachk_vlaue +=(mptachk[mptachk.length-1].isChecked())? "^"+((EditText)findViewById(R.id.mpt_otr)).getText().toString():""; // append the other text value in to the selected option
									    String mpttext=((EditText)findViewById(R.id.mpt_otr)).getText().toString();
										int fv = Character.getNumericValue(mpttext.charAt(0));
										if(fv==0)
										{
											cf.ShowToast("Please enter the valid Other Main Panel Amperage.", 0);
											cf.setFocus(((EditText)findViewById(R.id.mpt_otr)));
										}
										else
										{
									         mainpanel_Age();
										}
								 }
							 }
							 else
							 {
								 mainpanel_Age();
							 }
				        }
		    	     }
				 }
			 }
			 break;*/
		 case R.id.mpt_chk6:/** TYPE CHECKBOX OTHER **/
			  if(mptachk[mptachk.length-1].isChecked())
			  { 
				  ((EditText)findViewById(R.id.mpt_otr)).setVisibility(v.VISIBLE);
				  ((TextView)findViewById(R.id.mpttxt)).setVisibility(v.VISIBLE);
				  cf.setFocus(((EditText)findViewById(R.id.mpt_otr)));
			  }
			  else
			  {
				  cf.clearother(((EditText)findViewById(R.id.mpt_otr)));
				  ((TextView)findViewById(R.id.mpttxt)).setVisibility(v.INVISIBLE);
			  }
			  break;
		/* case R.id.spsave:
			 if(((CheckBox)findViewById(R.id.epe_chk)).isChecked())
			 {
				 elechkval=1;
			 }
			 else
			 {
				 elechkval=0;
				 if(sprdgtxt.equals("")){
					 cf.ShowToast("Please select the option for Sub Panels Present.", 0);
				 }else{
					 if(sprdgtxt.equals("Yes")){
						 chk_values();
						 if(chkspp_save.getCount()==0){
							 cf.ShowToast("Please save atleast one Sub Panel Information.", 0);
						 }else{
							 sp_Insert();
						 }
						
					 }else{
						 sp_Insert();
					 }
				 }
				 
			 }
			 break;*/
		 case R.id.addsp:
			chk_values();
			if(((Button)findViewById(R.id.addsp)).getText().toString().equals("Update")){
				next();
			}
			else
			{
				if(chkspp_save.getCount()==9){
					cf.ShowToast("Limit Exceeds! You have already added 9 Sub Panels.",0);
					splocet.setText("");spampsspin.setSelection(0);((EditText)findViewById(R.id.etotr_spamp)).setText("");
					 for(int i=0;i<spchk.length;i++)
						{
						 spchk[i].setChecked(false);
							
						}
		    	}
				else
				{
					next();
					 
				}
			}
			
			
			 break;
		 case R.id.clearsp:
			 splocet.setText("");spampsspin.setSelection(0);((EditText)findViewById(R.id.etotr_spamp)).setText("");
			 for(int i=0;i<spchk.length;i++)
				{
				 spchk[i].setChecked(false);
					
				}
			 ((Button)findViewById(R.id.addsp)).setText("Save/Add More Sub Panels");
			 break;
		 case R.id.egetdlup:
				if(nc==0)
				{
					cf.showDialogDate(((EditText)findViewById(R.id.edlutxt)));
				}
				else
				{
					
				}
				break;
		 case R.id.save:
			 if(((CheckBox)findViewById(R.id.ged_chk)).isChecked())
				{
					gedchkval=1;electricalpanelenclosure();
				}
				else{
				    gedchkval=0;
					if(esrdgtxt.equals(""))
					{
						cf.ShowToast("Please select the option for Electrical Service under General Electrical Details.", 0);
						
					}
					else
					{	if(strdgtxt.equals(""))
							cf.ShowToast("Please select the option for Service Type under General Electrical Details.", 0);
						else
							if(emrdgtxt.equals(""))
								cf.ShowToast("Please select the option for Electric Meter under General Electrical Details.", 0);
							else
								if(locstr.equals("--Select--"))
								{
									cf.ShowToast("Please select Location under General Electrical Details.", 0);
								}
								else
								{
									if(locstr.equals("Other"))
									{
										if(((EditText)findViewById(R.id.loc_otr)).getText().toString().trim().equals("")){
											   cf.ShowToast("Please enter the Other text for Location under General Electrical Details.", 0);
											   cf.setFocus(((EditText)findViewById(R.id.loc_otr)));
										}
										else
										{
											nextged();
										}
									}
									else
									{
										nextged();
									}
									
								}
					}
				}
			 break;
		/*case R.id.save:
			
			chk_values();
			System.out.println("retesrdtxt="+retesrdtxt);	
			  if(!((CheckBox)findViewById(R.id.ged_chk)).isChecked()&& retesrdtxt.equals(""))
			  {
				  cf.ShowToast("Please save the sections under General Electrical Details.", 0);
			  }else{
				  if(!((CheckBox)findViewById(R.id.epe_chk)).isChecked() && retmmpcondtxt.equals("")){
					  cf.ShowToast("Please save the sections under Electrical Panel/Enclosures - Main Panel.", 0);
				  }else{
					  if(!((CheckBox)findViewById(R.id.epe_chk)).isChecked() && retspptxt.equals("")){
						  cf.ShowToast("Please save the sections under Electrical Panel/Enclosures - Sub Panel.", 0);
					  }else{
						  
						  if(!((CheckBox)findViewById(R.id.epe_chk)).isChecked() && sprdgtxt.equals("Yes") && chkspp_save.getCount()==0){
							  cf.ShowToast("Please save atleast one Sub Panel Type.", 0);
						  }else{
							  if(!((CheckBox)findViewById(R.id.vep_chk)).isChecked()&& retvprdtxt.equals(""))
							  {
								  cf.ShowToast("Please save the sections under Visible Electrical Panel/Disconnect Issues Noted.", 0);
							  }else{
								  if(!((CheckBox)findViewById(R.id.wir_chk)).isChecked() && retwirtxt.equals("")){
									 cf.ShowToast("Please save the sections under Wiring Type.", 0); 
								  }else{

									  if(!((CheckBox)findViewById(R.id.recp_chk)).isChecked()&& retrecptxt.equals(""))
									  {
										  cf.ShowToast("Please save the sections under Receptacles/Outlets.", 0);
									  }else{
										  System.out.println("detretvbc"+detretvbc);
										  if(!((CheckBox)findViewById(R.id.vbc_chk)).isChecked()&& detretvbc.equals(""))
										  {
											  cf.ShowToast("Please save the sections under Visible Branch Circuity.", 0);
										  }else{
											  if(!((CheckBox)findViewById(R.id.lfr_chk)).isChecked() && detretlfr.equals(""))
											  {
												  cf.ShowToast("Please save the sections under Light Fixtures/Receptacles.", 0);
											  }else{
												  if(!((CheckBox)findViewById(R.id.go_chk)).isChecked() && rethaztxt.equals("")){
													  cf.ShowToast("Please save the sections under General Observations.", 0);
												  }else{
													  if(!((CheckBox)findViewById(R.id.oes_chk)).isChecked()&& detretoes.equals(""))
													  {
														  cf.ShowToast("Please save the sections under Conditional of Overall Electrical Services.", 0);
													  }else{
														  if(etaddcomments.getText().toString().trim().equals("")){
															  cf.ShowToast("Please enter the Overall Electrical Comments.", 0);
															  cf.setFocus(etaddcomments);
														  }else{
															  chk_values();
															  if(((CheckBox)findViewById(R.id.ged_chk)).isChecked())
															  {
																 gedchkval=1;esrdgtxt="";
															  }
															  else
															  {
																  gedchkval=0;
															  }
															  if(((CheckBox)findViewById(R.id.epe_chk)).isChecked())
															  {
																  elechkval=1;retmmpcondtxt="";retspptxt="";
															  }
															  else
															  {
																  elechkval=0;
															  }
															  if(((CheckBox)findViewById(R.id.vep_chk)).isChecked())
															  {
																 vepchkval=1;veprdgtxt="";
															  }
															  else
															  {
																  vepchkval=0;
															  }
															  if(((CheckBox)findViewById(R.id.wir_chk)).isChecked())
															  {
																 wirchkval=1;wirchk_vlaue="";
															  }
															  else
															  {
																  wirchkval=0;
															  }
															  if(((CheckBox)findViewById(R.id.recp_chk)).isChecked())
															  {
																 recchkval=1;rectypchk_value="";
															  }
															  else
															  {
																  recchkval=0;
															  }
															  if(((CheckBox)findViewById(R.id.vbc_chk)).isChecked())
															  {
																 vbcchkval=1;vbcchk_value="";
															  }
															  else
															  {
																  vbcchkval=0;
															  }
															  if(((CheckBox)findViewById(R.id.lfr_chk)).isChecked())
															  {
																 lfrchkval=1;lfrchk_vlaue="";
															  }
															  else
															  {
																  lfrchkval=0;
															  }
															  if(((CheckBox)findViewById(R.id.go_chk)).isChecked())
															  {
																 gochkval=1;hazrdgtxt="";
															  }
															  else
															  {
																  gochkval=0;
															  }
															  if(((CheckBox)findViewById(R.id.oes_chk)).isChecked())
															  {
																 oeschkval=1;escrdgtxt="";
															  }
															  else
															  {
																  oeschkval=0;
															  }
															  if(((EditText)findViewById(R.id.eleccomment)).getText().toString().trim().equals(""))
															  {
																  addchkval=1;
															  }
															  else
															  {
																  addchkval=0;
															  }
															  
															  if(chkged_save.getCount()>0)
															  {
																    try
																	{
																    	if(gedchkval==1)
																    	{
																    		cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_General+ " set fld_inc='"+gedchkval+"',electricalservice='',servicetype='',electricalmeter='',location='',locationotr='',servicemain='',servicemainotr='' where fld_srid='"+cf.selectedhomeid+"'");
																    	}
																    	else
																    	{
																    		cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_General+ " set fld_inc='"+gedchkval+"' where fld_srid='"+cf.selectedhomeid+"'");
																    	}
																	}
																	catch (Exception E)
																	{
																		String strerrorlog="Updating the general electrical details - FourPoint";
																		cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
																	}	
															  }
															  else
															  {
																  try
																	{
																		cf.arr_db.execSQL("INSERT INTO "
																				+ cf.Four_Electrical_General
																				+ " (fld_srid,fld_inc)"
																				+ "VALUES('"+cf.selectedhomeid+"','"+gedchkval+"')");
																		
																 	}
																	catch (Exception E)
																	{
																		String strerrorlog="Inserting the general electrical details - FourPoint";
																		cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
																	}
															  }
															  if(chkmpl_save.getCount()>0)
															  {
																    try
																    {
																    	if(elechkval==1)
																    	{
																    		cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_MainPanel+ " set fld_electinc='"+elechkval+"',mmpcond='',mptype='',mptypopt='',agempl='',agemplotr='',yiconf='',mplocation='',mplocationotr='',dslocation='',dslocationotr='',recupd='',recupdyes='',rectyp='',recdate='',recpc='' where fld_srid='"+cf.selectedhomeid+"'");
																    	}
																    	else
																    	{
																    		cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_MainPanel+ " set fld_electinc='"+elechkval+"' where fld_srid='"+cf.selectedhomeid+"'");
																    	}
																	
																    }
																    catch (Exception E)
																	{
																		String strerrorlog="Updating the mainpanel - FourPoint";
																		cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
																	}
																    chk2=true;
															  }
															  else
															  {
																  try
																	{
																		cf.arr_db.execSQL("INSERT INTO "
																				+ cf.Four_Electrical_MainPanel
																				+ " (fld_srid,fld_electinc)"
																				+ "VALUES('"+cf.selectedhomeid+"','"+elechkval+"')");
																		
																 	}
																	catch (Exception E)
																	{
																		String strerrorlog="Inserting the mainpanel - FourPoint";
																		cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
																	}
																  chk2=true;
															  }
															  if(chkspl_save.getCount()>0)
															  {
																    try
																    {
																    	if(elechkval==1)
																    	{
																    		cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_SubPanel+ " set fld_electinc='"+elechkval+"',spp='',nosub='' where fld_srid='"+cf.selectedhomeid+"'");
																    	}
																    	else
																    	{
																    		cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_SubPanel+ " set fld_electinc='"+elechkval+"' where fld_srid='"+cf.selectedhomeid+"'");
																    	}
																		
																    }
																    catch (Exception E)
																	{
																		String strerrorlog="Updating the supppanel - FourPoint";
																		cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
																	}
																    chk1=true;
															  }
															  else
															  {
																  try
																	{
																		cf.arr_db.execSQL("INSERT INTO "
																				+ cf.Four_Electrical_SubPanel
																				+ " (fld_srid,fld_electinc)"
																				+ "VALUES('"+cf.selectedhomeid+"','"+elechkval+"')");
																		
																 	}
																	catch (Exception E)
																	{
																		String strerrorlog="Inserting the mainpanel - FourPoint";
																		cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
																	}
																  chk1=true;
															  }
															  if(((CheckBox)findViewById(R.id.epe_chk)).isChecked())
															 {
																  try
																  {
																	  cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_SubPanelPresent+" Where fld_srid='"+cf.selectedhomeid+"'");
																  }
																  catch (Exception e) {
																		// TODO: handle exception
																	}
																}
																else
																{
																	try
																	  {
																		
																		  cf.arr_db.execSQL("UPDATE  "+cf.Four_Electrical_SubPanelPresent+" SET fld_flag='0' Where fld_srid='"+cf.selectedhomeid+"' ");

																	  }catch (Exception e) {
																		// TODO: handle exception
																	}
																}
															  if(chkvep_save.getCount()>0)
															  {
																    try
																	{
																    	if(vepchkval==1)
																    	{
																    		cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Panel+ " set fld_inc='"+vepchkval+"',visiblepanel='',type='' where fld_srid='"+cf.selectedhomeid+"'");
																    	}
																    	else
																    	{
																    		cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Panel+ " set fld_inc='"+vepchkval+"' where fld_srid='"+cf.selectedhomeid+"'");
																    	}
																		
																	}
																	catch (Exception E)
																	{
																		String strerrorlog="Updating the visible electricla panel - FourPoint";
																		cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
																	}	
															  }
															  else
															  {
																  try
																	{
																		cf.arr_db.execSQL("INSERT INTO "
																				+ cf.Four_Electrical_Panel
																				+ " (fld_srid,fld_inc)"
																				+ "VALUES('"+cf.selectedhomeid+"','"+vepchkval+"')");
																		
																 	}
																	catch (Exception E)
																	{
																		String strerrorlog="Inserting the visible electricla panel - FourPoint";
																		cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
																	}
															  }
														    if(chkwir_save.getCount()>0)
														    {
															    try
																{
															    	if(wirchkval==1)
															    	{
															    		cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Wiring+ " set fld_wirinc='"+wirchkval+"',wirtyp='',aluminopt='',aluminsubopt1='',aluminsubopt2='',aluminsubopt3='' where fld_srid='"+cf.selectedhomeid+"'");
															    	}
															    	else
															    	{
															    		cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Wiring+ " set fld_wirinc='"+wirchkval+"' where fld_srid='"+cf.selectedhomeid+"'");
															    	}
																	
																}
																catch (Exception E)
																{
																	String strerrorlog="Updating the Comments - FourPoint";
																	cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
																}
														    }
														    else
														    {
														    	try
																{
																	cf.arr_db.execSQL("INSERT INTO "
																			+ cf.Four_Electrical_Wiring
																			+ " (fld_srid,fld_wirinc)"
																			+ "VALUES('"+cf.selectedhomeid+"','"+wirchkval+"')");
																	
															 	}
																catch (Exception E)
																{
																	String strerrorlog="Inserting the Visble Branch Circuity - FourPoint";
																	cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
																}
														    }
														    if(chkrec_save.getCount()>0)
														    {
															    try
																{
															    	if(recchkval==1)
															    	{
															    		cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Receptacles+ " set fld_inc='"+recchkval+"',receptacle='',gfcireceptacle='' where fld_srid='"+cf.selectedhomeid+"'");
															    	}
															    	else
															    	{
															    		cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Receptacles+ " set fld_inc='"+recchkval+"' where fld_srid='"+cf.selectedhomeid+"'");
															    	}
																	
																}
																catch (Exception E)
																{
																	String strerrorlog="Updating the receptacles - FourPoint";
																	cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
																}
														    }
														    else
														    {
														    	try
																{
																	cf.arr_db.execSQL("INSERT INTO "
																			+ cf.Four_Electrical_Receptacles
																			+ " (fld_srid,fld_inc)"
																			+ "VALUES('"+cf.selectedhomeid+"','"+recchkval+"')");
																	
															 	}
																catch (Exception E)
																{
																	String strerrorlog="Inserting the receptacles - FourPoint";
																	cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
																}
														    }
														     
														    if(chkgo_save.getCount()>0 )
														    {
															    try
																{
															    	if(gochkval==1)
															    	{
															    		cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_GO+ " set fld_goinc='"+gochkval+"',haz='',hazyes='',haznocomt='',recp='',recpyescomt='',lconn='',lconnyescomt='' where fld_srid='"+cf.selectedhomeid+"'");
															    	}
															    	else
															    	{
															    		cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_GO+ " set fld_goinc='"+gochkval+"' where fld_srid='"+cf.selectedhomeid+"'");
															    	}
																	
																}
																catch (Exception E)
																{
																	String strerrorlog="Updating the general observations - FourPoint";
																	cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
																}	
														    }
														    else
														    {
														    	try
																{
																	cf.arr_db.execSQL("INSERT INTO "
																			+ cf.Four_Electrical_GO
																			+ " (fld_srid,fld_goinc)"
																			+ "VALUES('"+cf.selectedhomeid+"','"+gochkval+"')");
																	
															 	}
																catch (Exception E)
																{
																	String strerrorlog="Inserting the general observations - FourPoint";
																	cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
																}
														    }
														    if(chkveb_save.getCount()>0)
														    {
															    try
																{
																	cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Details+ " set fld_vbcinc='"+vbcchkval+"',vbc='"+cf.encode(vbcchk_value)+"',fld_lfrin='"+lfrchkval+"',lfr='"+cf.encode(lfrchk_vlaue)+"',lfrotr='"+cf.encode(strvbclfrotr)+"',fld_oesinc='"+oeschkval+"',oes='"+cf.encode(escrdgtxt)+"',oescomment='"+cf.encode(strnocomments)+"',addchk='"+addchkval+"',addcomment='"+cf.encode(etaddcomments.getText().toString())+"' where fld_srid='"+cf.selectedhomeid+"'");
																	cf.ShowToast("Electrical System has been saved successfully.", 1);
																	cf.goclass(25);
																}
																catch (Exception E)
																{
																	String strerrorlog="Updating the conditional - FourPoint";
																	cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
																}	
														    }
														    else
														    {
														    	try
																{
														    		cf.arr_db.execSQL("INSERT INTO "
																			+ cf.Four_Electrical_Details
																			+ " (fld_srid,fld_vbcinc,vbc,fld_lfrin,lfr,lfrotr,fld_oesinc,oes,oescomment,addchk,addcomment)"
																			+ "VALUES('"+cf.selectedhomeid+"','"+vbcchkval+"','"+cf.encode(vbcchk_value)+"','"+lfrchkval+"',"+
																			"'"+cf.encode(lfrchk_vlaue)+"','"+cf.encode(strvbclfrotr)+"',"+
																			"'"+oeschkval+"','"+cf.encode(escrdgtxt)+"','"+cf.encode(strnocomments)+"','"+addchkval+"',"+
																			"'"+cf.encode(etaddcomments.getText().toString())+"')");
														    		cf.ShowToast("Electrical System has been saved successfully.", 1);
																	  cf.goclass(25);
															 	}
																catch (Exception E)
																{
																	String strerrorlog="Inserting the conditional - FourPoint";
																	cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
																}
														    }
														 // }
													  }
												  }
											  }
										  }

									  }
							      }
						  }
						  }		  
					  }
				  }
			
			  }
			break;*/
		default:
			break;
		}
	}
	
	
	private void nextged() 
	{
		// TODO Auto-generated method stub
		eservmainchk_value= cf.getslected_radio(servmainchk);
    	// String eservmainchk_valueotr=(servmainchk[servmainchk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.sm_otr)).getText().toString():""; // append the other text value in to the selected option
    	 //eservmainchk_value+=eservmainchk_valueotr;

		
		
		if(eservmainchk_value.equals(""))
		{
			cf.ShowToast("Please select the option for Service Main under General Electrical Details.", 0);
		}
		else
		{
			 if(servmainchk[servmainchk.length-1].isChecked())
			 {
				 if(((EditText)findViewById(R.id.sm_otr)).getText().toString().trim().equals(""))
				 {
					 cf.ShowToast("Please enter the Other text for Service Main under General Electrical Details.", 0);
					 cf.setFocus(((EditText)findViewById(R.id.sm_otr)));
				 }
				 else
				 {
					 electricalpanelenclosure();
				 }
			 }
			 else
			 {
				 electricalpanelenclosure();
			 }
			
		}
	}
	private void electricalpanelenclosure()
	{
		 if(((CheckBox)findViewById(R.id.epe_chk)).isChecked())
		 {
			 elechkval=1;VEPvalidation();
		 }
		 else
		 {
			 elechkval=0;
			 /*if(mmpcrdgtxt.equals(""))
				 cf.ShowToast("Please select the option for Meter to Main Panel Conductor under Electrical Panel/Enclosures.", 0);
			 else{*/
			 mptypechk_vlaue= cf.getslected_radio(mptypechk);
	    	 String mptypechk_valueotr=(mptypechk[mptypechk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.mptype_otr)).getText().toString():""; // append the other text value in to the selected option
	    	 mptypechk_vlaue+=mptypechk_valueotr;
	    	 
			 	 if(mptypechk_vlaue.equals(""))
				 {
					 cf.ShowToast("Please select the option for Main Panel Type under Electrical Panel/Enclosures.", 0);
				 }
				 else
				 {
					 if(mptypechk[mptypechk.length-1].isChecked())
					 {
						 if(mptypechk_valueotr.trim().equals("&#40;"))
						 {
							 cf.ShowToast("Please enter the Other text for Main Panel Type under Electrical Panel/Enclosures.", 0);
							 cf.setFocus(((EditText)findViewById(R.id.mptype_otr)));
						 }
						 else
						 {
							 mainpanelamperage();
						 }
					 }
					 else
					 {
						 mainpanelamperage();
					 }
	    	     }
		 	 //}
		 }
 	  }
	
	private void mainpanelamperage()
	{
		 mptachk_vlaue= cf.getslected_radio(mptachk);
    	 String mptachk_valueotr=(mptachk[mptachk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.mpt_otr)).getText().toString():""; // append the other text value in to the selected option
    	 mptachk_vlaue+=mptachk_valueotr;
    	 if(mptachk_vlaue.equals(""))
		 {
			 cf.ShowToast("Please select the option for Main Panel Amperage under Electrical Panel/Enclosures." , 0);
		 }
    	 else
    	 {
    		 if(mptachk[mptachk.length-1].isChecked())
			 {
				 if(mptachk_valueotr.trim().equals("&#40;"))
				 {
					 cf.ShowToast("Please enter the Other text for Main Panel Amperage under Electrical Panel/Enclosures.", 0);
					 cf.setFocus(((EditText)findViewById(R.id.mpt_otr)));
				 }
				 else
				 {
					// mptachk_vlaue +=(mptachk[mptachk.length-1].isChecked())? "^"+((EditText)findViewById(R.id.mpt_otr)).getText().toString():""; // append the other text value in to the selected option
					    String mpttext=((EditText)findViewById(R.id.mpt_otr)).getText().toString();
						int fv = Character.getNumericValue(mpttext.charAt(0));
						if(fv==0)
						{
							cf.ShowToast("Please enter the valid Other Main Panel Amperage under Electrical Panel/Enclosures.", 0);
							cf.setFocus(((EditText)findViewById(R.id.mpt_otr)));
						}
						else
						{
					         mainpanel_Age();
						}
				 }
			 }
			 else
			 {
				 mainpanel_Age();
			 }
        }
	}
	private void next() {
		// TODO Auto-generated method stub
		if(splocet.getText().toString().trim().equals("")){
			 cf.ShowToast("Please enter the Sub Panel Location.",0);
			 cf.setFocus(splocet);
			 }
			 else
			 {
				 if(spampstr.equals("--Select--")){
					 cf.ShowToast("Please select the Sub Panel(s) Amperage.",0);
				 }
				 else
				 {
					 if(spampstr.trim().equals("Other")){
						  if(((EditText)findViewById(R.id.etotr_spamp)).getText().toString().trim().equals("")){
							 cf.ShowToast("Please enter the Sub Panel(s) Amperage - Other",0);
							 cf.setFocus(((EditText)findViewById(R.id.etotr_spamp)));
						  }
						  else
						  {
							 String spttext=((EditText)findViewById(R.id.etotr_spamp)).getText().toString();
							 int fv = Character.getNumericValue(spttext.charAt(0));
							 if(fv==0)
							 {
								cf.ShowToast("Please enter the valid Other Sub Panel(s) Amperage.", 0);
								cf.setFocus(((EditText)findViewById(R.id.etotr_spamp)));
							 }
							 else
							 {
							    spampstr += "~"+((EditText)findViewById(R.id.etotr_spamp)).getText().toString();
							    subpaneltype();
							 }
						 }
					 }
					 else
					 {
						 subpaneltype();
					 }
				 }
			 }
	}
	private void subpaneltype() {
		// TODO Auto-generated method stub
		 if(((CheckBox)findViewById(R.id.epe_chk)).isChecked())
		 {
			 elechkval=1;
		 }
		else
		{
			 elechkval=0;
		}
		 spchk_vlaue= cf.getselected_chk(spchk);
		 
		 if(spchk_vlaue.equals(""))
		 {
			cf.ShowToast("Please select the option for Sub Panel Type.",0);
		 }else
		 {
				try{
					
					if(((Button)findViewById(R.id.addsp)).getText().toString().equals("Update")){
						cf.arr_db.execSQL("UPDATE  "+cf.Four_Electrical_SubPanelPresent+" SET splocation='"+cf.encode(splocet.getText().toString())+"',"+
						           "spamps='"+cf.encode(spampstr)+"',sptype='"+cf.encode(spchk_vlaue)+"' where sppId='"+Current_select_id+"' ");
						cf.ShowToast("Sub Panels saved successfully.",0);
					((Button)findViewById(R.id.addsp)).setText("Save/Add More Sub Panels");
					
					}else{
						chk_values();
						if(chkspp_save.getCount()==9){
							showstr = "Limit Exceeds! You have already added 9 Sub Panels.";
						}else{
							cf.arr_db.execSQL("INSERT INTO "
									+ cf.Four_Electrical_SubPanelPresent
									+ " (fld_srid,fld_flag,splocation,spamps,sptype)"
									+ "VALUES('"+cf.selectedhomeid+"','0','"+cf.encode(splocet.getText().toString())+"','"+cf.encode(spampstr)+"',"+
									"'"+cf.encode(spchk_vlaue)+"')");
							cf.ShowToast("Sub Panels saved successfully.",0);;
					 }
					
					} sp_Insert();clearsp();ShowSubPanelValue();
					
				
				}catch(Exception e1){
					System.out.println("subpanel="+e1.getMessage());
				}
		 }
	}
	private void ShowSubPanelValue() {
		// TODO Auto-generated method stub
		((LinearLayout)findViewById(R.id.spyes)).setVisibility(cf.v1.VISIBLE);
		((Button)findViewById(R.id.addsp)).setText("Save/Add More Sub Panels");
		   Cursor spp_retrieve= cf.SelectTablefunction(cf.Four_Electrical_SubPanelPresent, " Where fld_srid='"+cf.selectedhomeid+"' and fld_flag='0'");
	       etnosubpanels.setText(Integer.toString(spp_retrieve.getCount()));
		   if(spp_retrieve.getCount()>0)
			{
			   spp_retrieve.moveToFirst();
			   
	    	   try
				{
	    		   spptbl.removeAllViews();
				}
				catch (Exception e) {
					// TODO: handle exception
				}
				LinearLayout h1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvac_listview_header, null); 
				TableRow th = (TableRow)h1.findViewById(R.id.spp_hdr);th.setVisibility(cf.v1.VISIBLE);
				TableLayout.LayoutParams lp = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
			 	lp.setMargins(2, 0, 2, 2); h1.removeAllViews(); 
			 	th.setPadding(10, 0, 0, 0);
			 	
			 	spptbl.addView(th,lp);
			 	spp_in_DB=new String[spp_retrieve.getCount()];
			 	spptbl.setVisibility(View.VISIBLE); 
				
				for(int i=0;i<spp_retrieve.getCount();i++)
				{
					TextView sploctxt,spampstxt,spptyptxt;
					ImageView edit,delete,htrimgtxt;
					String spno="",sploc="",spamp="",sptyp="";
					spno=spp_retrieve.getString(0);
					sploc=cf.decode(spp_retrieve.getString(3));
					spamp=cf.decode(spp_retrieve.getString(4));
					sptyp=cf.decode(spp_retrieve.getString(5));
					
					LinearLayout t1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvac_listview, null);
					TableRow t = (TableRow)t1.findViewById(R.id.spp);t.setVisibility(cf.v1.VISIBLE);
					t.setId(44444+i);/// Set some id for further use
					
					
					sploctxt= (TextView) t.findViewWithTag("loc");
					sploctxt.setText(sploc);
				 	spampstxt= (TextView) t.findViewWithTag("amp");
				 	if(spamp.contains("Other~")){spamp=spamp.replace("Other~", "");
				 	spampstxt.setText(Html.fromHtml(spamp)+" AMPS");}
				 	else{spampstxt.setText(spamp);}
				 	
				 	spptyptxt= (TextView) t.findViewWithTag("typ");
				 	if(sptyp.contains("&#94;")){sptyp=sptyp.replace("&#94;", ",");}
				 	else{sptyp=sptyp;}
				 	spptyptxt.setText(sptyp);
				 
				 	edit= (ImageView) t.findViewWithTag("spp_edit");
				 	edit.setId(789456+i);
				 	edit.setTag(spp_retrieve.getString(0));
	                edit.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
						try
						{
						int i=Integer.parseInt(v.getTag().toString());
						  updatespp(i);
						}
						catch (Exception e) {
							// TODO: handle exception
						}
						}
	                });
						
				 	delete=(ImageView) t.findViewWithTag("spp_delete");
				 	delete.setTag(spp_retrieve.getString(0));
	                delete.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(final View v) {
							// TODO Auto-generated method stub
							showstr="";
							AlertDialog.Builder builder = new AlertDialog.Builder(Electrical.this);
							builder.setMessage("Do you want to delete the selected Sub Panel Type?")
							        .setCancelable(false)
									.setPositiveButton("Yes",
											new DialogInterface.OnClickListener() {

												public void onClick(DialogInterface dialog,
														int id) {
													
													try
													{
													int i=Integer.parseInt(v.getTag().toString());
													if(i==Current_select_id)
													{
														Current_select_id=0;
													}
													cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_SubPanelPresent+" Where sppId='"+i+"'");
													showstr = "Sub panel type deleted successfully.";
													clearsp();ShowSubPanelValue();
													}
													catch (Exception e) {
														// TODO: handle exception
													}cf.ShowToast(showstr, 0);
												}
											})
									.setNegativeButton("No",
											new DialogInterface.OnClickListener() {
												public void onClick(DialogInterface dialog,
														int id) {

													dialog.cancel();
												}
											});
							
							AlertDialog alert = builder.create();
							alert.setTitle("Confirmation");
							alert.setIcon(R.drawable.alertmsg);
							alert.show();
						
						}
					});
				 	t1.removeAllViews();
					t.setPadding(10, 0, 0, 0);
					spptbl.addView(t,lp);
					spp_retrieve.moveToNext();
				}
			}else{spptbl.setVisibility(View.GONE); }
	}
	protected void updatespp(int i) {
		// TODO Auto-generated method stub
		clearsp();((LinearLayout)findViewById(R.id.spyes)).setVisibility(cf.v1.VISIBLE);showstr="";
		Current_select_id=i;
		try{Cursor sppret=cf.SelectTablefunction(cf.Four_Electrical_SubPanelPresent, " Where sppId='"+i+"'");
		if(sppret.getCount()>0)
		{
			sppret.moveToFirst();
			TextView sploctxt,spampstxt,spptyptxt;
		    String spno="",sploc="",spamp="",sptyp="";
		    spno=sppret.getString(0);
			sploc=cf.decode(sppret.getString(3));
			spamp=cf.decode(sppret.getString(4));
			sptyp=cf.decode(sppret.getString(5));
			
			splocet.setText(sploc);
			
			if(spamp.contains("Other")){ 
			    String[] temp = spamp.split("~");
			    spamp = temp[0];
			    
			    ((EditText)findViewById(R.id.etotr_spamp)).setText(temp[1]);
			    ((EditText)findViewById(R.id.etotr_spamp)).setVisibility(cf.v1.VISIBLE);
			    ((TextView)findViewById(R.id.spttxt)).setVisibility(cf.v1.VISIBLE);
		    }else{
		    	spamp = spamp;((EditText)findViewById(R.id.etotr_spamp)).setVisibility(cf.v1.GONE);
		    	((TextView)findViewById(R.id.spttxt)).setVisibility(cf.v1.INVISIBLE);
			 }		
			int spamppos =spampsadap.getPosition(spamp);
			spampsspin.setSelection(spamppos);
			 cf.setvaluechk1(sptyp,spchk,((EditText)findViewById(R.id.et_splocation)));
			
			((Button)findViewById(R.id.addsp)).setText("Update");
		}
		}catch(Exception e){}
	}
	private void clearsp() {
		// TODO Auto-generated method stub
		 splocet.setText("");spampsspin.setSelection(0);((EditText)findViewById(R.id.etotr_spamp)).setText("");
		 for(int i=0;i<spchk.length;i++)
			{
			 spchk[i].setChecked(false);
				
			}
		 
		 ((LinearLayout)findViewById(R.id.spyes)).setVisibility(cf.v1.GONE);
	}
	
	private void sp_Insert() {
		// TODO Auto-generated method stub
	         chk_values();
	         if(chkspl_save.getCount()==0){
				  try
					{   
					    cf.arr_db.execSQL("INSERT INTO "
								+ cf.Four_Electrical_SubPanel
								+ " (fld_srid,fld_electinc,spp,nosub)"
								+ "VALUES('"+cf.selectedhomeid+"','"+elechkval+"','"+cf.encode(sprdgtxt)+"','"+cf.encode(Integer.toString(chkspp_save.getCount()))+"')");
					   if(elechkval==1){ 
					    	((TextView)findViewById(R.id.savetxt2)).setVisibility(cf.v1.INVISIBLE);
					    }
						else{
							if(chk2){chk_values();}
							else
							{
								retmmpcondtxt="";
							}
							System.out.println("retmmpcondtxt"+retmmpcondtxt);
							if(!retmmpcondtxt.equals("")){
						    	((TextView)findViewById(R.id.savetxt2)).setVisibility(cf.v1.VISIBLE);
						    }cf.ShowToast("Sub Panels saved successfully.", 0);
						}
					   chk1=true;
					   
				 	}
					catch (Exception E)
					{
						String strerrorlog="Inserting the SUBPANEL TYPE - FourPoint";
						cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
					}
	          }
	          else{
	        	  try{System.out.println("counupd");
	        		  cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_SubPanel+ " set fld_electinc='"+elechkval+"',"+
	        	          "spp='"+cf.encode(sprdgtxt)+"',nosub='"+cf.encode(Integer.toString(chkspp_save.getCount()))+"' where fld_srid='"+cf.selectedhomeid+"'");
	        		  if(sprdgtxt.equals("No")|| sprdgtxt.equals("Unknown"))
	        		  {
	        			  cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_SubPanelPresent+" Where fld_srid='"+cf.selectedhomeid+"'");
							
	        		  }System.out.println("counupdatedt");
	        		  chk_values();
	        		  if(elechkval==1){ 
					    	((TextView)findViewById(R.id.savetxt2)).setVisibility(cf.v1.INVISIBLE);
					    }
						else{
							if(chk2){chk_values();}
							else
							{
								retmmpcondtxt="";
							}
							System.out.println("retmmpcondtxt"+retmmpcondtxt);
							if(!retmmpcondtxt.equals("")){
						    	((TextView)findViewById(R.id.savetxt2)).setVisibility(cf.v1.VISIBLE);
						    }cf.ShowToast("Sub Panels saved successfully.", 0);
						}
	        		   chk1=true;
	        	  }catch (Exception E)
					{
						String strerrorlog="UPDATING the SUBPANEL TYPE - FourPoint";
						cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
					}
	          }

	}
	private void mainpanel_Age() {

		// TODO Auto-generated method stub
		if(agestr.equals("--Select--")){
			cf.ShowToast("Please select Age of Main Panel under Electrical Panel/Enclosures.", 0);
		}else{
			if(agestr.equals("Other")){
				if(((EditText)findViewById(R.id.age_otr)).getText().toString().trim().equals("")){
					cf.ShowToast("Please enter the Year under Age of Main Panel under Electrical Panel/Enclosures.", 0);
					 cf.setFocus(((EditText)findViewById(R.id.age_otr)));
				}else{
					if(((EditText)findViewById(R.id.age_otr)).getText().toString().length()<4){
						cf.ShowToast("Please enter the valid year under Electrical Panel/Enclosures.", 0);
						 cf.setFocus(((EditText)findViewById(R.id.age_otr)));
					}else{
						if(cf.yearValidation(((EditText)findViewById(R.id.age_otr)).getText().toString()).equals("true"))
						{
							cf.ShowToast("Year should not exceed the current year under Electrical Panel/Enclosures.", 0);
							cf.setFocus(((EditText)findViewById(R.id.age_otr)));
						}
						else if(cf.yearValidation(((EditText)findViewById(R.id.age_otr)).getText().toString()).equals("zero"))
						{
							cf.ShowToast("Please enter the valid year under Electrical Panel/Enclosures.", 0);
							cf.setFocus(((EditText)findViewById(R.id.age_otr)));
						}
						else
						{
								YIC();
						}
						
					}
				}
			}else{
				YIC();
		  }
		}
	}
	private void YIC() {
		// TODO Auto-generated method stub
		 if(yicrdgtxt.equals("")){
			 cf.ShowToast("Please select the option for Year of Installed Confidence under Electrical Panel/Enclosures." , 0);
		 }
		 else{
			 if(mainlocstr.trim().equals("--Select--"))
			 {
				 cf.ShowToast("Please select the Main Panel Location under Electrical Panel/Enclosures.", 0);
			 }
			 else
			 {
				 if(mainlocstr.equals("Other"))
				 {
					 if(((EditText)findViewById(R.id.mploc_otr)).getText().toString().trim().equals("")){
						 cf.ShowToast("Please enter the Other text for Main Panel Location under Electrical Panel/Enclosures.", 0);
						 cf.setFocus(((EditText)findViewById(R.id.mploc_otr)));
					 } 
					 else
					 {
						 disconect();
					 }
				 }
				 else
				 {
					 disconect();
				 }
				 
			 }
		 }
	}
	private void disconect() {
		// TODO Auto-generated method stub
		if(dsclrdgtxt.equals(""))
		{
		 	 cf.ShowToast("Please select the option for Disconnect Location under Electrical Panel/Enclosures.", 0);
		}
		else
		{		
			if(dsclrdgtxt.equals("Other")){
					 if(((EditText)findViewById(R.id.dsc_location)).getText().toString().trim().equals("")){
						 cf.ShowToast("Please enter the Other text for Disconnect Location under Electrical Panel/Enclosures.", 0);
						 cf.setFocus(((EditText)findViewById(R.id.dsc_location)));
					 }else{
						 recentlyupdated();}
				 }else{
					 recentlyupdated();
				 }
		}
	}
	private void recentlyupdated() {
		// TODO Auto-generated method stub
		if(updrdgtxt.equals("")){
			cf.ShowToast("Please select the option for System Recently Updated under Electrical Panel/Enclosures.", 0);
		}else{
			if(updrdgtxt.equals("Yes")){
				if(((EditText)findViewById(R.id.upd_yes)).getText().toString().trim().equals("")){
					cf.ShowToast("Please enter the Approximate Percentage Update under Electrical Panel/Enclosures.", 0);
					 cf.setFocus(((EditText)findViewById(R.id.upd_yes)));
				}else{
					upgdyesstr = ((EditText)findViewById(R.id.upd_yes)).getText().toString();
					
					if(Integer.parseInt(upgdyesstr)<=0 || Integer.parseInt(upgdyesstr)>100){
						 cf.ShowToast("Approximate Percentage Update should be Greater than 0 and Less than or Equal to 100 under Electrical Panel/Enclosures.", 0);
						 cf.setFocus(((EditText)findViewById(R.id.upd_yes)));
					}else{
				         /*etypchk_vlaue= cf.getselected_chk(etypechk);
						 if(etypchk_vlaue.equals("")){
							 cf.ShowToast("Please select the option for Type under Electrical Panel/Enclosures." , 0);
						 }
						 else{*/
							 
						datelastupdated();
							
						 //}
					}
				}
			}else{
				subpanelvalidation();
			}
		}
	}
	private void datelastupdated() {
		// TODO Auto-generated method stub
		/*if(epcrdgtxt.equals(""))
		{
     		cf.ShowToast("Please select the option for Permit Confirmed under Electrical Panel/Enclosures.",0);
	    }
		else
		{*/
		/*	if(epcrdgtxt.equals("Yes"))
			{*/
				if(((CheckBox)findViewById(R.id.ndchk)).isChecked())
    			{
					edlutxt= ((EditText)findViewById(R.id.edlutxt)).getText().toString();
					permitconfimed();
    			}
				else
				{
					if(((EditText)findViewById(R.id.edlutxt)).getText().toString().trim().equals("")){
	 		        	cf.ShowToast("Please select Date Last Updated under Electrical Panel/Enclosures.",0);
	 		        	cf.setFocus(((EditText)findViewById(R.id.edlutxt)));
	 		        }else{
	 		        	 if(cf.checkfortodaysdate(((EditText)findViewById(R.id.edlutxt)).getText().toString())=="false")
	 		        	 {
	 		        		 cf.ShowToast("Date Last Updated should not exceed current Year/Month/Date under Electrical Panel/Enclosures.",0);
	 		        		 cf.setFocus(((EditText)findViewById(R.id.edlutxt)));
	 		        	 }
	 		        	 else
	 		        	 {
	 		        		edlutxt= ((EditText)findViewById(R.id.edlutxt)).getText().toString();
	 		        		Preeinsert();
	 		        	 }
	 		        	 
	 		        }
				}
		/*	}
			else
			{
				Preeinsert();
			}*/
		//}
	}
	private void Preeinsert() {
		// TODO Auto-generated method stub
		if(!((EditText)findViewById(R.id.edlutxt)).getText().toString().trim().equals("")&&!((EditText)findViewById(R.id.edlutxt)).getText().toString().equals("Not Determined"))
		{
			 if(cf.checkfortodaysdate(((EditText)findViewById(R.id.edlutxt)).getText().toString())=="false")
	         {
	        	 cf.ShowToast("Date Last Updated should not exceed current Year/Month/Date under Electrical Panel/Enclosures.",0);
	        	 cf.setFocus(((EditText)findViewById(R.id.edlutxt)));
	       	 }
	       	 else
	       	 {
	       		edlutxt= ((EditText)findViewById(R.id.edlutxt)).getText().toString();
	       		permitconfimed();
	       	 }
		}
		else
		{
			edlutxt= ((EditText)findViewById(R.id.edlutxt)).getText().toString();
			permitconfimed();
		}
	}
	private void permitconfimed()
	{
		if(epcrdgtxt.equals(""))
		{
	 		cf.ShowToast("Please select the option for Permit Confirmed under Electrical Panel/Enclosures.",0);
	    }
		else
		{
			/*if(epcrdgtxt.equals("Yes"))
			{*/
				subpanelvalidation();
			/*}
			else
			{
				Preeinsert();
			}*/
		}
	}
	
	private void subpanelvalidation()
	{
		 if(((CheckBox)findViewById(R.id.epe_chk)).isChecked())
		 {
			 elechkval=1;VEPvalidation();
		 }
		 else
		 {
			 elechkval=0;
			 if(sprdgtxt.equals("")){
				 cf.ShowToast("Please select the option for Sub Panels Present.", 0);
			 }else{
				 if(sprdgtxt.equals("Yes")){
					 chk_values();
					 if(chkspp_save.getCount()==0){
						 cf.ShowToast("Please save atleast one Sub Panel Information.", 0);
					 }else{
						 VEPvalidation();
					 }
					
				 }else{
					 VEPvalidation();
				 }
			 }
			 
		 }
	}
	
	private void VEPvalidation() {
		// TODO Auto-generated method stub
		 if(((CheckBox)findViewById(R.id.vep_chk)).isChecked())
		 {
				 vepchkval=1;WiringTypeValidation();
		 }
		 else
		 {
			     vepchkval=0;
				 if(veprdgtxt.equals(""))
					 cf.ShowToast("Please select the option for Visible Electrical Panel/Disconnect Issues Noted.", 0);
				 else
					 if(veprdgtxt.equals("Yes")){
						 veptypchk_vlaue= cf.getselected_chk(veptypchk);
				    	 String veptypchk_valueotr=(veptypchk[veptypchk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.typ_otr)).getText().toString():""; // append the other text value in to the selected option
				    	 veptypchk_vlaue+=veptypchk_valueotr;
				    	 if(veptypchk_vlaue.equals(""))
						 {
							 cf.ShowToast("Please select the option for Type under Visible Electrical Panel/Disconnect Issues Noted." , 0);
						 }
				    	 else
				    	 {
				    		 if(veptypchk[veptypchk.length-1].isChecked())
							 {
								 if(veptypchk_valueotr.trim().equals("&#40;"))
								 {
									 cf.ShowToast("Please enter the Other text for Type under Visible Electrical Panel/Disconnect Issues Noted.", 0);
									 cf.setFocus(((EditText)findViewById(R.id.typ_otr)));
								 }
								 else
								 {
									WiringTypeValidation();	
								 }
							 }
							 else
							 {
								 WiringTypeValidation();
							 }
			    	    }
					 }else{
						 WiringTypeValidation();
					 }
		 }	
		
		
	}
	private void WiringTypeValidation() {
		// TODO Auto-generated method stub
		if(((CheckBox)findViewById(R.id.wir_chk)).isChecked())
		 {
			 wirchkval=1;LightFixvalidation();
		 }
		 else
		 {
			 wirchkval=0;
			 wirchk_vlaue= cf.getselected_chk(wirchk);
	    	 String wirchk_valueotr=(wirchk[wirchk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.wir_otr)).getText().toString():""; // append the other text value in to the selected option
	    	 wirchk_vlaue+=wirchk_valueotr;
	    	 if(wirchk_vlaue.equals(""))
			 {
				 cf.ShowToast("Please select the option for Wiring Type under Branch/Circuitry." , 0);
			 }
	    	 else
	    	 {
	    		 if(wirchk[wirchk.length-1].isChecked())
				 {
					 if(wirchk_valueotr.trim().equals("&#40;"))
					 {
						 cf.ShowToast("Please enter the Other text for Wiring Type under Branch/Circuitry.", 0);
						 cf.setFocus(((EditText)findViewById(R.id.wir_otr)));
					 }
					 else
					 {
						// wirchk_vlaue +=(wirchk[wirchk.length-1].isChecked())? "^"+((EditText)findViewById(R.id.wir_otr)).getText().toString():""; // append the other text value in to the selected option
						 chk_aluminium();
					 }
				 }
				 else
				 {
					 chk_aluminium();
				 }
   	    }
		 }
	}
	private void mainpanel_Insert() {
		// TODO Auto-generated method stub
		chk_values();
		  
		  if(chkmpl_save.getCount()==0)
		  {
			  try
				{
				    cf.arr_db.execSQL("INSERT INTO "
							+ cf.Four_Electrical_MainPanel
							+ " (fld_srid,fld_electinc,mmpcond,mptype,mptypopt,agempl,agemplotr,yiconf,mplocation,mplocationotr,dslocation,"+
						      "dslocationotr,recupd,recupdyes,rectyp,recdate,recpc)"
							+ "VALUES('"+cf.selectedhomeid+"','"+elechkval+"','"+cf.encode(mmpcrdgtxt)+"','"+cf.encode(mptypechk_vlaue)+"',"+
							"'"+cf.encode(mptachk_vlaue)+"','"+cf.encode(agestr)+"','"+cf.encode(((EditText)findViewById(R.id.age_otr)).getText().toString())+"',"+
							"'"+cf.encode(yicrdgtxt)+"','"+cf.encode(mainlocstr)+"','"+cf.encode(((EditText)findViewById(R.id.mploc_otr)).getText().toString())+"',"+
							"'"+cf.encode(dsclrdgtxt)+"','"+cf.encode(((EditText)findViewById(R.id.dsc_location)).getText().toString())+"',"+
							"'"+cf.encode(updrdgtxt)+"','"+cf.encode(upgdyesstr)+"','"+cf.encode(etypchk_vlaue)+"',"+
							"'"+cf.encode(edlutxt)+"','"+cf.encode(epcrdgtxt)+"')");
				    if(elechkval==1){ 
				    	((TextView)findViewById(R.id.savetxt2)).setVisibility(cf.v1.INVISIBLE);
				    }
					else{
						System.out.println("chk1"+chk1);
						if(chk1){chk_values();}
						else
						{
							retspptxt="";
						}
						System.out.println("retspptxt"+retspptxt);
						if(!retspptxt.equals("")){
					    	((TextView)findViewById(R.id.savetxt2)).setVisibility(cf.v1.VISIBLE);
					   } cf.ShowToast("Main Panel saved successfully.", 0);
					}
				    chk2=true;
			 	}
				catch (Exception E)
				{
					String strerrorlog="Inserting the MAINPANEL TYPE - FourPoint";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
	    }
		else
		{
			try
			{
				cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_MainPanel+ " set fld_electinc='"+elechkval+"',mmpcond='"+cf.encode(mmpcrdgtxt)+"',"+
			                  "mptype='"+cf.encode(mptypechk_vlaue)+"',mptypopt='"+cf.encode(mptachk_vlaue)+"',agempl='"+cf.encode(agestr)+"',"+
						      "agemplotr='"+cf.encode(((EditText)findViewById(R.id.age_otr)).getText().toString())+"',yiconf='"+cf.encode(yicrdgtxt)+"',"+
			                  "mplocation='"+cf.encode(mainlocstr)+"',mplocationotr='"+cf.encode(((EditText)findViewById(R.id.mploc_otr)).getText().toString())+"',"+
						      "dslocation='"+cf.encode(dsclrdgtxt)+"',"+
						      "dslocationotr='"+cf.encode(((EditText)findViewById(R.id.dsc_location)).getText().toString())+"',recupd='"+cf.encode(updrdgtxt)+"',"+
			                  "recupdyes='"+cf.encode(upgdyesstr)+"',rectyp='"+cf.encode(etypchk_vlaue)+"',"+
						      "recdate='"+cf.encode(edlutxt)+"',"+
			                  "recpc='"+cf.encode(epcrdgtxt)+"' where fld_srid='"+cf.selectedhomeid+"'");
				if(elechkval==1){ 
			    	((TextView)findViewById(R.id.savetxt2)).setVisibility(cf.v1.INVISIBLE);
			    }
				else{
					System.out.println("chk1"+chk1);
					if(chk1){chk_values();}
					else
					{
						retspptxt="";
					}
					System.out.println("retspptxt"+retspptxt);
					if(!retspptxt.equals("")){
				    	((TextView)findViewById(R.id.savetxt2)).setVisibility(cf.v1.VISIBLE);
				    }cf.ShowToast("Main Panel saved successfully.", 0);
				}
				chk2=true;
			   
			}
			catch (Exception E)
			{
				String strerrorlog="Updating the MAINPANEL TYPE - FourPoint";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			}
		}
		
	}
	private void chk_aluminium() {
		// TODO Auto-generated method stub
		if(wirchk_vlaue.contains("Aluminum")){
			 alumchk_vlaue= cf.getselected_chk(alumchk);
			 if(alumchk_vlaue.equals("")){
				 cf.ShowToast("Please provide details of all remediation under Wiring Type - Aluminum." , 0);
			 }
			 else{
				 alumchk_vlaue=alumchk_vlaue.replace("&#94;",",");
			     alumarr = alumchk_vlaue.split(",");
			     showstr="";
			     if(!alumchk_vlaue.contains("Beyond Scope of Inspection")){
				 if(chkarray("Entire home rewired with copper")){
					 alumfstchk_vlaue= cf.getselected_chk(alumfstchk);
					 if(alumfstchk_vlaue.equals("")){
						 showstr+="Please select the options under Entire home rewired with copper.\n";
						  b[0]="false";
					 }
					 else{ b[0]="true";
						
					 }
				 }else{b[0]="true";}
				  if(chkarray("Connections repaired via COPALUM crimp")){
					 alumsecchk_vlaue= cf.getselected_chk(alumsecchk);
					 if(alumsecchk_vlaue.equals("")){
						 showstr+="Please select the options under Connections repaired via COPALUM crimp.\n";
						 b[1]="false";
					 }
					 else{ b[1]="true";
					 }
				 }else{b[1]="true";}
				  if(chkarray("Connections repaired via AlumiConn")){
					 alumthdchk_vlaue= cf.getselected_chk(alumthdchk);
					 if(alumthdchk_vlaue.equals("")){
						 showstr+="Please select the options under Connections repaired via AlumiConn." ;
						 b[2]="false";
					 }else{
						 b[2]="true";
			     	 }
				 }
				  if(!showstr.trim().equals("")){
				  cf.ShowToast(showstr, 0);}
				  if(b[0] == "true" && b[1] == "true" && b[2] == "true"){
					  BranchCircuity();
				     
				  
				  }
			     }else{
			    	 BranchCircuity();
			     }
			 }
		}else{
			BranchCircuity();
		}
							 
		
	}
	private void RecepOutletsValidation()
	{
		 /*if(((CheckBox)findViewById(R.id.recp_chk)).isChecked())
		 {
				 recchkval=1;VisibleBranchValidation();
		 }
		 else
		 {       recchkval=0;
				 rectypchk_value= cf.getselected_chk(recpchk);
		    	 if(rectypchk_value.equals(""))
				 {
					 cf.ShowToast("Please select the option for Receptacles/Outlets." , 0);
				 }
		    	 else
		    	 {
		    		 gfcrectypchk_value= cf.getselected_chk(gfcrecpchk);
			    	 if(gfcrectypchk_value.equals(""))
					 {
						 cf.ShowToast("Please select the option for GFCI Receptacles Location under Receptacles/Outlets." , 0);
					 }
			    	 else
			    	 {*/
			    		//VisibleBranchValidation();   //need to test
		    	     /*}
	    	     }
		 }*/
	}
/*	private void VisibleBranchValidation() {
		// TODO Auto-generated method stub
		if(((CheckBox)findViewById(R.id.vbc_chk)).isChecked())
		 {
				 vbcchkval=1;LightFixvalidation();
		 }
		 else
		 {
			     vbcchkval=0;
				 vbcchk_value= cf.getselected_chk(vbcchk);
		    	 if(vbcchk_value.equals(""))
				 {
					 cf.ShowToast("Please select the option for Visible Branch Circuity." , 0);
				 }
		    	 else
		    	 {
		    		LightFixvalidation();
	    	     }
		 }
	}*/
	private void BranchCircuity()
	{
		 vbcchk_value= cf.getselected_chk(vbcchk);
    	 if(vbcchk_value.equals(""))
		 {
			 cf.ShowToast("Please select the option for Visible Branch Circuity Observation under Branch/Circuitry." , 0);
		 }
    	 else
    	 {
    		 GO_Hazards();
	     }
	}
	
	private void LightFixvalidation() {
		// TODO Auto-generated method stub
		if(((CheckBox)findViewById(R.id.lfr_chk)).isChecked())
		 {
				 lfrchkval=1;GO_Switches();
		 }
		 else
		 {
			     lfrchkval=0;
				 lfrchk_vlaue= cf.getselected_chk(lfrchk);
		    	 String lfrchk_valueotr=(lfrchk[lfrchk.length-1].isChecked())? "&#94;"+((EditText)findViewById(R.id.lfr_otr)).getText().toString():""; // append the other text value in to the selected option
		    	 lfrchk_vlaue+=lfrchk_valueotr;
		    	 if(lfrchk_vlaue.equals(""))
				 {
					 cf.ShowToast("Please select the option for Light Fixtures/Receptacles." , 0);
				 }
		    	 else
		    	 {
		    		 GO_Switches();
		    	 }
		 }
	}
	private boolean chkarray(String string) {
		// TODO Auto-generated method stub
		boolean chkb=false;
		if(alumarr!=null){
			for(int i=0;i<alumarr.length;i++){
				if(string.trim().equals(alumarr[i])){
					chkb=true;
					return true;
				}
			}
		}
		
		 return chkb;
		
	}
	private void WIR_Insert() {
		// TODO Auto-generated method stub
		chk_values();
		  
		  if(chkwir_save.getCount()==0)
		  {
			  try
				{
				    cf.arr_db.execSQL("INSERT INTO "
							+ cf.Four_Electrical_Wiring
							+ " (fld_srid,fld_wirinc,wirtyp,aluminopt,aluminsubopt1,aluminsubopt2,aluminsubopt3)"
							+ "VALUES('"+cf.selectedhomeid+"','"+wirchkval+"','"+cf.encode(wirchk_vlaue)+"','"+cf.encode(alumchk_vlaue)+"',"+
							"'"+cf.encode(alumfstchk_vlaue)+"','"+cf.encode(alumsecchk_vlaue)+"','"+cf.encode(alumthdchk_vlaue)+"')");
				    if(wirchkval==1){ ((TextView)findViewById(R.id.savetxtwir)).setVisibility(cf.v1.INVISIBLE);}
					else{ ((TextView)findViewById(R.id.savetxtwir)).setVisibility(cf.v1.VISIBLE);
					cf.ShowToast("Wiring Type saved successfully.", 0);} 
				    wir_sve=true;
			 	}
				catch (Exception E)
				{
					String strerrorlog="Inserting the WIRING TYPE - FourPoint";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
				
		}
		else
		{
			try
			{
				cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Wiring+ " set fld_wirinc='"+wirchkval+"',wirtyp='"+cf.encode(wirchk_vlaue)+"',"+
			         "aluminopt='"+cf.encode(alumchk_vlaue)+"',aluminsubopt1='"+cf.encode(alumfstchk_vlaue)+"',"+
					 "aluminsubopt2='"+cf.encode(alumsecchk_vlaue)+"',aluminsubopt3='"+cf.encode(alumthdchk_vlaue)+"' where fld_srid='"+cf.selectedhomeid+"'");
				if(wirchkval==1){ ((TextView)findViewById(R.id.savetxtwir)).setVisibility(cf.v1.INVISIBLE);}
				else{ ((TextView)findViewById(R.id.savetxtwir)).setVisibility(cf.v1.VISIBLE);
				cf.ShowToast("Wiring Type saved successfully.", 0);} 
				 wir_sve=true;
			}
			catch (Exception E)
			{
				String strerrorlog="Updating the WIRING TYPE - FourPoint";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			}
		}
		
		
	}
	private void GO_Hazards() {
		// TODO Auto-generated method stub
		/*if(((CheckBox)findViewById(R.id.go_chk)).isChecked())
		 {
			gochkval=1;COEValidation();
			
		}else{
			 gochkval=0;*/
			 if(hazrdgtxt.equals("")){
					cf.ShowToast("Please select the option for Electrical Issues/Hazards Present under Branch/Circuitry.", 0);
				}else{
					
					 if(hazrdgtxt.equals("Yes")){
					   	hzchk_vlaue= cf.getselected_chk(hzchk);
				    	 String hzchk_valueotr=(hzchk[hzchk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.hz_otr)).getText().toString():""; // append the other text value in to the selected option
				    	 hzchk_vlaue+=hzchk_valueotr;
				    	 if(hzchk_vlaue.equals(""))
						 {
							 cf.ShowToast("Please select the option under Electrical Issues/Hazards Present under Branch/Circuitry." , 0);
						 }
				    	 else
				    	 {
				    		 if(hzchk[hzchk.length-1].isChecked())
							 {
								 if(hzchk_valueotr.trim().equals("&#40;"))
								 {
									 cf.ShowToast("Please enter the Other text for Electrical Issues/Hazards Present - Yes.", 0);
									 cf.setFocus(((EditText)findViewById(R.id.hz_otr)));
								 }
								 else
								 {
									 hzchk_vlaue +=(hzchk[hzchk.length-1].isChecked())? "^"+((EditText)findViewById(R.id.hz_otr)).getText().toString():""; // append the other text value in to the selected option
									 /*if(ethaznocomments.getText().toString().trim().equals("")){
											cf.ShowToast("Please enter the Comments under Electrical Issues/Hazards Present.", 0);
											 cf.setFocus(ethaznocomments);
										}else{*/
											LightFixvalidation();
										//}
								 }
							 }
							 else
							 {
								 /*if(ethaznocomments.getText().toString().trim().equals("")){
										cf.ShowToast("Please enter the Comments under Electrical Issues/Hazards Present.", 0);
										 cf.setFocus(ethaznocomments);
									}else{*/
										LightFixvalidation();
									//}
							 }
				    	 }
				    	
					}else{
						LightFixvalidation();
					}
				}
		 //}
		
	}
	private void GO_Switches() {
		// TODO Auto-generated method stub
		if(((CheckBox)findViewById(R.id.go_chk)).isChecked())
		 {
				Ele_Insert();
		 }
		 else
		 {
			 COEValidation();
				/* if(lsrdgtxt.equals("")){
					 cf.ShowToast("Please select the option for any accessible receptacles/light switches found with elevated temperatures.", 0);
				 }else{
					 if(lsrdgtxt.equals("Yes")){
						 if(etlsyescomments.getText().toString().trim().equals("")){
							 cf.ShowToast("Please enter the Comments under any accessible receptacles/light switches found with elevated temperatures.", 0);
							 cf.setFocus(etlsyescomments);
						 }else{
							 GO_lconnections(); 
						 }
					 }else{
						 GO_lconnections(); 
					 }
				 }*/
		 }
	}
	private void GO_lconnections() {
		// TODO Auto-generated method stub
		/*if(lcrdgtxt.equals("")){
			 cf.ShowToast("Please select the option for Were there any loose connections or overheating/arcing evident.", 0);
		 }else{
			 if(lcrdgtxt.equals("Yes")){
				 if(etlcyescomments.getText().toString().trim().equals("")){
					 cf.ShowToast("Please enter the Comments under Were there any loose connections or overheating/arcing evident.", 0);
					 cf.setFocus(etlcyescomments);
				 }else{
					COEValidation();
				 }
			 }else{
				 COEValidation(); 
			 }
		 }*/
	}
	private void COEValidation()
	{
		/*if(((CheckBox)findViewById(R.id.oes_chk)).isChecked())
		 {
			oeschkval=1;Ele_Insert();
		 }
		 else
		 {
			 oeschkval=0;*/
			if(escrdgtxt.equals("")){
				cf.ShowToast("Please select the option for Is the electrical system generally in good working order.", 0);
			}else{
				if(!escrdgtxt.equals("Yes, based on visual observations at the time of inspection")){
					if(((EditText)findViewById(R.id.nocomment)).getText().toString().trim().equals("")){
						cf.ShowToast("Please enter the comments.", 0);
						 cf.setFocus(((EditText)findViewById(R.id.nocomment)));
					}else{
						Ele_Insert();
					}
				}else{
					Ele_Insert();
				}
			}
		// }
		
	}
	private void Ele_Insert()
	{
		System.out.println("Elecinsert values");
		  chk_values();
		  strnocomments = etnocomments.getText().toString();
		  if(((CheckBox)findViewById(R.id.ged_chk)).isChecked())
		  {
			 gedchkval=1;esrdgtxt="";
		  }
		  else
		  {
			  gedchkval=0;
		  }
		  if(((CheckBox)findViewById(R.id.epe_chk)).isChecked())
		  {
			  elechkval=1;retmmpcondtxt="";retspptxt="";
		  }
		  else
		  {
			  elechkval=0;
		  }
		  if(((CheckBox)findViewById(R.id.vep_chk)).isChecked())
		  {
			 vepchkval=1;veprdgtxt="";
		  }
		  else
		  {
			  vepchkval=0;
		  }
		  if(((CheckBox)findViewById(R.id.wir_chk)).isChecked())
		  {
			 wirchkval=1;wirchk_vlaue="";
		  }
		  else
		  {
			  wirchkval=0;
		  }
		  
		/*  if(((CheckBox)findViewById(R.id.recp_chk)).isChecked())
		  {
			 recchkval=1;rectypchk_value="";
		  }
		  else
		  {
			  recchkval=0;
		  }*/
		/*  if(((CheckBox)findViewById(R.id.vbc_chk)).isChecked())
		  {
			 vbcchkval=1;vbcchk_value="";
		  }
		  else
		  {
			  vbcchkval=0;
		  }*/
		  if(((CheckBox)findViewById(R.id.lfr_chk)).isChecked())
		  {
			 lfrchkval=1;lfrchk_vlaue="";
		  }
		  else
		  {
			  lfrchkval=0;
		  }
	/*	  if(((CheckBox)findViewById(R.id.oes_chk)).isChecked())
		  {
			 oeschkval=1;escrdgtxt="";
		  }
		  else
		  {
			  oeschkval=0;
		  }*/
		  if(((EditText)findViewById(R.id.eleccomment)).getText().toString().trim().equals(""))
		  {
			  addchkval=1;
		  }
		  else
		  {
			  addchkval=0;
		  }
		  System.out.println("WWWW="+chkged_save.getCount());
		  
		  if(chkged_save.getCount()>0)
		  {
			    try
				{
			    	System.out.println("gedchkval"+gedchkval);
			    	if(gedchkval==1)
			    	{
			    		cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_General+ " set fld_inc='"+gedchkval+"',electricalservice='',servicetype='',electricalmeter='',location='',locationotr='',servicemain='',servicemainotr='' where fld_srid='"+cf.selectedhomeid+"'");
			    	}
			    	else
			    	{
			    		System.out.println("UPDATE "+cf.Four_Electrical_General+ " set fld_inc='"+gedchkval+"',electricalservice='"+cf.encode(esrdgtxt)+"',"+
			   			     "servicetype='"+cf.encode(strdgtxt)+"',electricalmeter='"+cf.encode(emrdgtxt)+"',"+
			   				 "location='"+cf.encode(locstr)+"',locationotr='"+cf.encode(((EditText)findViewById(R.id.loc_otr)).getText().toString())+"',"+
			   			     "servicemain='"+cf.encode(eservmainchk_value)+"',"+
			   				 "servicemainotr='"+cf.encode(((EditText)findViewById(R.id.sm_otr)).getText().toString())+"'  where fld_srid='"+cf.selectedhomeid+"'");
			    		cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_General+ " set fld_inc='"+gedchkval+"',electricalservice='"+cf.encode(esrdgtxt)+"',"+
			   			     "servicetype='"+cf.encode(strdgtxt)+"',electricalmeter='"+cf.encode(emrdgtxt)+"',"+
			   				 "location='"+cf.encode(locstr)+"',locationotr='"+cf.encode(((EditText)findViewById(R.id.loc_otr)).getText().toString())+"',"+
			   			     "servicemain='"+cf.encode(eservmainchk_value)+"',"+
			   				 "servicemainotr='"+cf.encode(((EditText)findViewById(R.id.sm_otr)).getText().toString())+"'  where fld_srid='"+cf.selectedhomeid+"'");
			    	}
				}
				catch (Exception E)
				{
					System.out.println("E11="+E.getMessage());
					String strerrorlog="Updating the general electrical details - FourPoint";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}	
		  }
		  else
		  {
			  try
				{
				  
				  cf.arr_db.execSQL("INSERT INTO "
							+ cf.Four_Electrical_General
							+ " (fld_srid,fld_inc,electricalservice,servicetype,electricalmeter,location,locationotr,servicemain,servicemainotr)"
							+ "VALUES('"+cf.selectedhomeid+"','"+gedchkval+"','"+cf.encode(esrdgtxt)+"','"+cf.encode(strdgtxt)+"','"+cf.encode(emrdgtxt)+"',"+
							"'"+cf.encode(locstr)+"','"+cf.encode(((EditText)findViewById(R.id.loc_otr)).getText().toString())+"','"+cf.encode(eservmainchk_value)+"',"+
							"'"+cf.encode(((EditText)findViewById(R.id.sm_otr)).getText().toString())+"')");
			 	}
				catch (Exception E)
				{
					System.out.println("E22="+E.getMessage());
					String strerrorlog="Inserting the general electrical details - FourPoint";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
		  }
		  if(chkmpl_save.getCount()>0)
		  {
			    try
			    {
			    	if(elechkval==1)
			    	{
			    		cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_MainPanel+ " set fld_electinc='"+elechkval+"',mmpcond='',mptype='',mptypopt='',agempl='',agemplotr='',yiconf='',mplocation='',mplocationotr='',dslocation='',dslocationotr='',recupd='',recupdyes='',rectyp='',recdate='',recpc='' where fld_srid='"+cf.selectedhomeid+"'");
			    	}
			    	else
			    	{
			    		//cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_MainPanel+ " set fld_electinc='"+elechkval+"' where fld_srid='"+cf.selectedhomeid+"'");
			    		cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_MainPanel+ " set fld_electinc='"+elechkval+"',mmpcond='"+cf.encode(mmpcrdgtxt)+"',"+
				                  "mptype='"+cf.encode(mptypechk_vlaue)+"',mptypopt='"+cf.encode(mptachk_vlaue)+"',agempl='"+cf.encode(agestr)+"',"+
							      "agemplotr='"+cf.encode(((EditText)findViewById(R.id.age_otr)).getText().toString())+"',yiconf='"+cf.encode(yicrdgtxt)+"',"+
				                  "mplocation='"+cf.encode(mainlocstr)+"',mplocationotr='"+cf.encode(((EditText)findViewById(R.id.mploc_otr)).getText().toString())+"',"+
							      "dslocation='"+cf.encode(dsclrdgtxt)+"',"+
							      "dslocationotr='"+cf.encode(((EditText)findViewById(R.id.dsc_location)).getText().toString())+"',recupd='"+cf.encode(updrdgtxt)+"',"+
				                  "recupdyes='"+cf.encode(upgdyesstr)+"',rectyp='"+cf.encode(etypchk_vlaue)+"',"+
							      "recdate='"+cf.encode(edlutxt)+"',"+
				                  "recpc='"+cf.encode(epcrdgtxt)+"' where fld_srid='"+cf.selectedhomeid+"'");
			    		
			    	}
				
			    }
			    catch (Exception E)
				{
					System.out.println("E33="+E.getMessage());
					String strerrorlog="Updating the mainpanel - FourPoint";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
			    chk2=true;
		  }
		  else
		  {
			  try
				{
				  
				  cf.arr_db.execSQL("INSERT INTO "
							+ cf.Four_Electrical_MainPanel
							+ " (fld_srid,fld_electinc,mmpcond,mptype,mptypopt,agempl,agemplotr,yiconf,mplocation,mplocationotr,dslocation,"+
						      "dslocationotr,recupd,recupdyes,rectyp,recdate,recpc)"
							+ "VALUES('"+cf.selectedhomeid+"','"+elechkval+"','"+cf.encode(mmpcrdgtxt)+"','"+cf.encode(mptypechk_vlaue)+"',"+
							"'"+cf.encode(mptachk_vlaue)+"','"+cf.encode(agestr)+"','"+cf.encode(((EditText)findViewById(R.id.age_otr)).getText().toString())+"',"+
							"'"+cf.encode(yicrdgtxt)+"','"+cf.encode(mainlocstr)+"','"+cf.encode(((EditText)findViewById(R.id.mploc_otr)).getText().toString())+"',"+
							"'"+cf.encode(dsclrdgtxt)+"','"+cf.encode(((EditText)findViewById(R.id.dsc_location)).getText().toString())+"',"+
							"'"+cf.encode(updrdgtxt)+"','"+cf.encode(upgdyesstr)+"','"+cf.encode(etypchk_vlaue)+"',"+
							"'"+cf.encode(edlutxt)+"','"+cf.encode(epcrdgtxt)+"')");
				/*  
					cf.arr_db.execSQL("INSERT INTO "
							+ cf.Four_Electrical_MainPanel
							+ " (fld_srid,fld_electinc)"
							+ "VALUES('"+cf.selectedhomeid+"','"+elechkval+"')");*/
					
			 	}
				catch (Exception E)
				{
					System.out.println("E44"+E.getMessage());
					String strerrorlog="Inserting the mainpanel - FourPoint";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
			  chk2=true;
		  }
		  if(chkspl_save.getCount()>0)
		  {
			    try
			    {
			    	if(elechkval==1)
			    	{
			    		cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_SubPanel+ " set fld_electinc='"+elechkval+"',spp='',nosub='' where fld_srid='"+cf.selectedhomeid+"'");
			    	}
			    	else
			    	{
			    		//cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_SubPanel+ " set fld_electinc='"+elechkval+"' where fld_srid='"+cf.selectedhomeid+"'");
			    		 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_SubPanel+ " set fld_electinc='"+elechkval+"',"+
			        	          "spp='"+cf.encode(sprdgtxt)+"',nosub='"+cf.encode(Integer.toString(chkspp_save.getCount()))+"' where fld_srid='"+cf.selectedhomeid+"'");

			    	}
					
			    }
			    catch (Exception E)
				{
					System.out.println("E55="+E.getMessage());
					String strerrorlog="Updating the supppanel - FourPoint";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
			    chk1=true;
		  }
		  else
		  {
			  try
				{
				  cf.arr_db.execSQL("INSERT INTO "
							+ cf.Four_Electrical_SubPanel
							+ " (fld_srid,fld_electinc,spp,nosub)"
							+ "VALUES('"+cf.selectedhomeid+"','"+elechkval+"','"+cf.encode(sprdgtxt)+"','"+cf.encode(Integer.toString(chkspp_save.getCount()))+"')");

					
			 	}
				catch (Exception E)
				{
					System.out.println("E66="+E.getMessage());
					String strerrorlog="Inserting the mainpanel - FourPoint";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
			  chk1=true;
		  }
		  
		  
		  
		  if(chkspp_save.getCount()>0)
		  {
			    try
			    {
			    	if(((CheckBox)findViewById(R.id.epe_chk)).isChecked())
					 {
			    		 cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_SubPanelPresent+" Where fld_srid='"+cf.selectedhomeid+"'");
			    	}
			    	else
			    	{
			    		//cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_SubPanel+ " set fld_electinc='"+elechkval+"' where fld_srid='"+cf.selectedhomeid+"'");
			    		cf.arr_db.execSQL("UPDATE  "+cf.Four_Electrical_SubPanelPresent+" SET fld_flag='0' Where fld_srid='"+cf.selectedhomeid+"' ");

			    	}
					
			    }
			    catch (Exception E)
				{
					System.out.println("E77="+E.getMessage());
					String strerrorlog="Updating the supppanel - FourPoint";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
			    chk1=true;
		  }
		  else
		  {
			  try
				{
				  cf.arr_db.execSQL("INSERT INTO "
							+ cf.Four_Electrical_SubPanelPresent
							+ " (fld_srid,fld_flag,splocation,spamps,sptype)"
							+ "VALUES('"+cf.selectedhomeid+"','0','"+cf.encode(splocet.getText().toString())+"','"+cf.encode(spampstr)+"',"+
							"'"+cf.encode(spchk_vlaue)+"')");
					
			 	}
				catch (Exception E)
				{
					System.out.println("E88="+E.getMessage());
					String strerrorlog="Inserting the Electrical subpanel - FourPoint";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
			  
		  }
		  
		  /*if(((CheckBox)findViewById(R.id.epe_chk)).isChecked())
		 {
			  try
			  {
				  cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_SubPanelPresent+" Where fld_srid='"+cf.selectedhomeid+"'");
			  }
			  catch (Exception e) {
					// TODO: handle exception
				}
			}
			else
			{
				try
				  {
					
					  cf.arr_db.execSQL("UPDATE  "+cf.Four_Electrical_SubPanelPresent+" SET fld_flag='0' Where fld_srid='"+cf.selectedhomeid+"' ");

				  }catch (Exception e) {
					// TODO: handle exception
				}
			}*/
		  if(chkvep_save.getCount()>0)
		  {
			    try
				{
			    	if(vepchkval==1)
			    	{
			    		cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Panel+ " set fld_inc='"+vepchkval+"',visiblepanel='',type='' where fld_srid='"+cf.selectedhomeid+"'");
			    	}
			    	else
			    	{
			    		cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Panel+ " set fld_inc='"+vepchkval+"',visiblepanel='"+cf.encode(veprdgtxt)+"',"+
			   			     "type='"+cf.encode(veptypchk_vlaue)+"' where fld_srid='"+cf.selectedhomeid+"'");

			    	}
					
				}
				catch (Exception E)
				{
					System.out.println("E99="+E.getMessage());
					String strerrorlog="Updating the visible electricla panel - FourPoint";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}	
		  }
		  else
		  {
			  try
				{
				  
				  cf.arr_db.execSQL("INSERT INTO "
							+ cf.Four_Electrical_Panel
							+ " (fld_srid,fld_inc,visiblepanel,type)"
							+ "VALUES('"+cf.selectedhomeid+"','"+vepchkval+"','"+cf.encode(veprdgtxt)+"','"+cf.encode(veptypchk_vlaue)+"')");
					
			 	}
				catch (Exception E)
				{
					System.out.println("E10="+E.getMessage());
					String strerrorlog="Inserting the visible electricla panel - FourPoint";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
		  }
	    if(chkwir_save.getCount()>0)
	    {
		    try
			{
		    	if(wirchkval==1)
		    	{
		    		cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Wiring+ " set fld_wirinc='"+wirchkval+"',wirtyp='',aluminopt='',aluminsubopt1='',aluminsubopt2='',aluminsubopt3='' where fld_srid='"+cf.selectedhomeid+"'");
		    	}
		    	else
		    	{
		    		//cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Wiring+ " set fld_wirinc='"+wirchkval+"' where fld_srid='"+cf.selectedhomeid+"'");
		    		cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Wiring+ " set fld_wirinc='"+wirchkval+"',wirtyp='"+cf.encode(wirchk_vlaue)+"',"+
					         "aluminopt='"+cf.encode(alumchk_vlaue)+"',aluminsubopt1='"+cf.encode(alumfstchk_vlaue)+"',"+
							 "aluminsubopt2='"+cf.encode(alumsecchk_vlaue)+"',aluminsubopt3='"+cf.encode(alumthdchk_vlaue)+"' where fld_srid='"+cf.selectedhomeid+"'");
		    	}
				
			}
			catch (Exception E)
			{
				System.out.println("E21="+E.getMessage());
				String strerrorlog="Updating the Comments - FourPoint";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			}
	    }
	    else
	    {
	    	try
			{
	    		 cf.arr_db.execSQL("INSERT INTO "
							+ cf.Four_Electrical_Wiring
							+ " (fld_srid,fld_wirinc,wirtyp,aluminopt,aluminsubopt1,aluminsubopt2,aluminsubopt3)"
							+ "VALUES('"+cf.selectedhomeid+"','"+wirchkval+"','"+cf.encode(wirchk_vlaue)+"','"+cf.encode(alumchk_vlaue)+"',"+
							"'"+cf.encode(alumfstchk_vlaue)+"','"+cf.encode(alumsecchk_vlaue)+"','"+cf.encode(alumthdchk_vlaue)+"')");
			
				
		 	}
			catch (Exception E)
			{System.out.println("E22="+E.getMessage());
				String strerrorlog="Inserting the Visble Branch Circuity - FourPoint";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			}
	    }
	    if(chkrec_save.getCount()>0)
	    {
		    try
			{
		    	if(recchkval==1)
		    	{
		    		cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Receptacles+ " set fld_inc='"+recchkval+"',receptacle='',gfcireceptacle='' where fld_srid='"+cf.selectedhomeid+"'");
		    	}
		    	else
		    	{
		    		//cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Receptacles+ " set fld_inc='"+recchkval+"' where fld_srid='"+cf.selectedhomeid+"'");
		    		cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Receptacles+ " set fld_inc='"+recchkval+"',receptacle='"+cf.encode(rectypchk_value)+"',"+
		   			     "gfcireceptacle='"+cf.encode(gfcrectypchk_value)+"' where fld_srid='"+cf.selectedhomeid+"'");
		    	}
				
			}
			catch (Exception E)
			{System.out.println("E23="+E.getMessage());
				String strerrorlog="Updating the receptacles - FourPoint";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			}
	    }
	    else
	    {
	    	try
			{
	    		cf.arr_db.execSQL("INSERT INTO "
						+ cf.Four_Electrical_Receptacles
						+ " (fld_srid,fld_inc,receptacle,gfcireceptacle)"
						+ "VALUES('"+cf.selectedhomeid+"','"+recchkval+"','"+cf.encode(rectypchk_value)+"','"+cf.encode(gfcrectypchk_value)+"')");
				
		 	}
			catch (Exception E)
			{System.out.println("E24="+E.getMessage());
				String strerrorlog="Inserting the receptacles - FourPoint";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			}
	    }
	     System.out.println("chkgo_save"+chkgo_save.getCount());
	    if(chkgo_save.getCount()>0 )
	    {
		    try
			{
		    	if(gochkval==1)
		    	{
		    		System.out.println("UPDATE "+cf.Four_Electrical_GO+ " set fld_goinc='"+gochkval+"',haz='"+cf.encode(hazrdgtxt)+"',hazyes='"+cf.encode(hzchk_vlaue)+"',haznocomt='"+cf.encode(ethaznocomments.getText().toString())+"',recp='',recpyescomt='',lconn='',lconnyescomt='' where fld_srid='"+cf.selectedhomeid+"'");
		    		cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_GO+ " set fld_goinc='"+gochkval+"',haz='"+cf.encode(hazrdgtxt)+"',hazyes='"+cf.encode(hzchk_vlaue)+"',haznocomt='"+cf.encode(ethaznocomments.getText().toString())+"',recp='',recpyescomt='',lconn='',lconnyescomt='' where fld_srid='"+cf.selectedhomeid+"'");
		    	}
		    	else
		    	{
		    		//cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_GO+ " set fld_goinc='"+gochkval+"' where fld_srid='"+cf.selectedhomeid+"'");
		    		cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_GO+ " set fld_goinc='"+gochkval+"',"+
					         "haz='"+cf.encode(hazrdgtxt)+"',"+
							 "hazyes='"+cf.encode(hzchk_vlaue)+"',haznocomt='"+cf.encode(ethaznocomments.getText().toString())+"',"+
					         "recp='"+cf.encode(lsrdgtxt)+"',recpyescomt='',"+
							 "lconn='"+cf.encode(lcrdgtxt)+"',lconnyescomt='' where fld_srid='"+cf.selectedhomeid+"'");
		    		System.out.println("UPDATE "+cf.Four_Electrical_GO+ " set fld_goinc='"+gochkval+"',"+
					         "haz='"+cf.encode(hazrdgtxt)+"',"+
							 "hazyes='"+cf.encode(hzchk_vlaue)+"',haznocomt='"+cf.encode(ethaznocomments.getText().toString())+"',"+
					         "recp='"+cf.encode(lsrdgtxt)+"',recpyescomt='',"+
							 "lconn='"+cf.encode(lcrdgtxt)+"',lconnyescomt='' where fld_srid='"+cf.selectedhomeid+"'");
		    	}
				
			}
			catch (Exception E)
			{System.out.println("E25="+E.getMessage());
				String strerrorlog="Updating the general observations - FourPoint";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			}	
	    }
	    else
	    {
	    	System.out.println("inside elso gochk");
	    	try
			{
System.out.println("INSERT INTO "
		+ cf.Four_Electrical_GO
		+ " (fld_srid,fld_goinc,haz,hazyes,haznocomt,recp,recpyescomt,lconn,lconnyescomt)"
		+ "VALUES('"+cf.selectedhomeid+"','"+gochkval+"',"+
		"'"+cf.encode(hazrdgtxt)+"','"+cf.encode(hzchk_vlaue)+"','"+cf.encode(ethaznocomments.getText().toString())+"',"+
		"'"+cf.encode(lsrdgtxt)+"','',"+
		"'"+cf.encode(lcrdgtxt)+"',"+
		"'')");
	    	


cf.arr_db.execSQL("INSERT INTO "
							+ cf.Four_Electrical_GO
							+ " (fld_srid,fld_goinc,haz,hazyes,haznocomt,recp,recpyescomt,lconn,lconnyescomt)"
							+ "VALUES('"+cf.selectedhomeid+"','"+gochkval+"',"+
							"'"+cf.encode(hazrdgtxt)+"','"+cf.encode(hzchk_vlaue)+"','"+cf.encode(ethaznocomments.getText().toString())+"',"+
							"'"+cf.encode(lsrdgtxt)+"','',"+
							"'"+cf.encode(lcrdgtxt)+"',"+
							"'')");
				
		 	}
			catch (Exception E)
			{System.out.println("E26="+E.getMessage());
				String strerrorlog="Inserting the general observations - FourPoint";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			}
	    }
	    if(chkveb_save.getCount()>0)
	    {
		    try
			{
				cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Details+ " set fld_vbcinc='"+vbcchkval+"',vbc='"+cf.encode(vbcchk_value)+"',fld_lfrin='"+lfrchkval+"',lfr='"+cf.encode(lfrchk_vlaue)+"',lfrotr='"+cf.encode(strvbclfrotr)+"',fld_oesinc='"+oeschkval+"',oes='"+cf.encode(escrdgtxt)+"',oescomment='"+cf.encode(strnocomments)+"',addchk='"+addchkval+"',addcomment='"+cf.encode(etaddcomments.getText().toString())+"' where fld_srid='"+cf.selectedhomeid+"'");
				cf.ShowToast("Electrical System has been saved successfully.", 1);
				cf.goclass(25);
			}
			catch (Exception E)
			{System.out.println("E27="+E.getMessage());
				String strerrorlog="Updating the conditional - FourPoint";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			}	
	    }
	    else
	    {
	    	try
			{
	    		cf.arr_db.execSQL("INSERT INTO "
						+ cf.Four_Electrical_Details
						+ " (fld_srid,fld_vbcinc,vbc,fld_lfrin,lfr,lfrotr,fld_oesinc,oes,oescomment,addchk,addcomment)"
						+ "VALUES('"+cf.selectedhomeid+"','"+vbcchkval+"','"+cf.encode(vbcchk_value)+"','"+lfrchkval+"',"+
						"'"+cf.encode(lfrchk_vlaue)+"','"+cf.encode(strvbclfrotr)+"',"+
						"'"+oeschkval+"','"+cf.encode(escrdgtxt)+"','"+cf.encode(strnocomments)+"','"+addchkval+"',"+
						"'"+cf.encode(etaddcomments.getText().toString())+"')");
	    	//	cf.ShowToast("Electrical System has been saved successfully.", 1);
				  cf.goclass(25);
		 	}
			catch (Exception E)
			{System.out.println("E28="+E.getMessage());
				String strerrorlog="Inserting the conditional - FourPoint";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			}
	    }
	}
	
	/*private void COESValidation()
	{
		if(((CheckBox)findViewById(R.id.oes_chk)).isChecked())
		 {
			oeschkval=1;
		 }
		 else
		 {
			 oeschkval=0;
			if(escrdgtxt.equals("")){
				cf.ShowToast("Please select the option for Is the electrical system generally in good working order.", 0);
			}else{
				if(!escrdgtxt.equals("Yes, based on visual observations at the time of inspection")){
					if(((EditText)findViewById(R.id.nocomment)).getText().toString().trim().equals("")){
						cf.ShowToast("Please enter the comments.", 0);
						 cf.setFocus(((EditText)findViewById(R.id.nocomment)));
					}else{
						OES_Insert();
					}
				}else{
					OES_Insert();
				}
			}
		 }
	}*/
	private void GO_Insert() {
		// TODO Auto-generated method stub
		chk_values();
			  
		  if(chkgo_save.getCount()==0)
		  {
			  try
				{
				    cf.arr_db.execSQL("INSERT INTO "
							+ cf.Four_Electrical_GO
							+ " (fld_srid,fld_goinc,haz,hazyes,haznocomt,recp,recpyescomt,lconn,lconnyescomt)"
							+ "VALUES('"+cf.selectedhomeid+"','"+gochkval+"',"+
							"'"+cf.encode(hazrdgtxt)+"','"+cf.encode(hzchk_vlaue)+"','"+cf.encode(ethaznocomments.getText().toString())+"',"+
							"'"+cf.encode(lsrdgtxt)+"','',"+
							"'"+cf.encode(lcrdgtxt)+"',"+
							"'')");
				    if(gochkval==1){ ((TextView)findViewById(R.id.savetxt7)).setVisibility(cf.v1.INVISIBLE);}
					else{ ((TextView)findViewById(R.id.savetxt7)).setVisibility(cf.v1.VISIBLE);
					cf.ShowToast("General Observations saved successfully.", 0);} 
				    go_sve=true;
			 	}
				catch (Exception E)
				{
					String strerrorlog="Inserting the GENERAL OBSERVATION - FourPoint";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
				
		}
		else
		{
			try
			{
				cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_GO+ " set fld_goinc='"+gochkval+"',"+
			         "haz='"+cf.encode(hazrdgtxt)+"',"+
					 "hazyes='"+cf.encode(hzchk_vlaue)+"',haznocomt='"+cf.encode(ethaznocomments.getText().toString())+"',"+
			         "recp='"+cf.encode(lsrdgtxt)+"',recpyescomt='',"+
					 "lconn='"+cf.encode(lcrdgtxt)+"',lconnyescomt='' where fld_srid='"+cf.selectedhomeid+"'");
				 if(gochkval==1){ ((TextView)findViewById(R.id.savetxt7)).setVisibility(cf.v1.INVISIBLE);}
					else{ ((TextView)findViewById(R.id.savetxt7)).setVisibility(cf.v1.VISIBLE);
					cf.ShowToast("General Observations saved successfully.", 0);} 
				 go_sve=true;
			}
			catch (Exception E)
			{
				String strerrorlog="Updating the GENERAL OBSERVATION - FourPoint";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			}
		}
	}
	private void VBC_Insert() {/*
		// TODO Auto-generated method stub
		  chk_values();
		  strvbclfrotr = etlfr.getText().toString();
		  strnocomments = etnocomments.getText().toString();
		  straddcomments = etaddcomments.getText().toString();
		  
		  if(chkdet_save.getCount()==0)
		  {
			  try
				{
					cf.arr_db.execSQL("INSERT INTO "
							+ cf.Four_Electrical_Details
							+ " (fld_srid,fld_vbcinc,vbc,fld_lfrin,lfr,lfrotr,fld_oesinc,oes,oescomment,addchk,addcomment)"
							+ "VALUES('"+cf.selectedhomeid+"','"+vbcchkval+"','"+cf.encode(vbcchk_value)+"','"+lfrchkval+"',"+
							"'"+cf.encode(lfrchk_vlaue)+"','"+cf.encode(strvbclfrotr)+"',"+
							"'"+oeschkval+"','"+cf.encode(escrdgtxt)+"','"+cf.encode(etnocomments.getText().toString())+"','"+addchkval+"',"+
							"'"+cf.encode(straddcomments)+"')");
					if(vbcchkval==1){ ((TextView)findViewById(R.id.savetxt5)).setVisibility(cf.v1.INVISIBLE);}
					else{ ((TextView)findViewById(R.id.savetxt5)).setVisibility(cf.v1.VISIBLE);
					cf.ShowToast("Visible Branch Circuity saved successfully.", 0);} 
					vbc_sve=true;
			 	}
				catch (Exception E)
				{
					String strerrorlog="Inserting the Visble Branch Circuity - FourPoint";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
				
		}
		else
		{
			try
			{
				 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Details+ " set fld_vbcinc='"+vbcchkval+"',vbc='"+cf.encode(vbcchk_value)+"' where fld_srid='"+cf.selectedhomeid+"'");
				 if(vbcchkval==1){ ((TextView)findViewById(R.id.savetxt5)).setVisibility(cf.v1.INVISIBLE);}
					else{ ((TextView)findViewById(R.id.savetxt5)).setVisibility(cf.v1.VISIBLE);
					cf.ShowToast("Visible Branch Circuity saved successfully.", 0);} 
				 vbc_sve=true;
			}
			catch (Exception E)
			{
				String strerrorlog="Updating the Visble Branch Circuity - FourPoint";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			}
		}
	*/}
	private void OES_Insert() {/*
		// TODO Auto-generated method stub
		  chk_values();
		  strvbclfrotr = etlfr.getText().toString();
		  strnocomments = etnocomments.getText().toString();
		  straddcomments = etaddcomments.getText().toString();
		  
		  if(chkdet_save.getCount()==0)
		  {
			  try
				{
					cf.arr_db.execSQL("INSERT INTO "
							+ cf.Four_Electrical_Details
							+ " (fld_srid,fld_vbcinc,vbc,fld_lfrin,lfr,lfrotr,fld_oesinc,oes,oescomment,addchk,addcomment)"
							+ "VALUES('"+cf.selectedhomeid+"','"+vbcchkval+"','"+cf.encode(vbcchk_value)+"','"+lfrchkval+"',"+
							"'"+cf.encode(lfrchk_vlaue)+"','"+cf.encode(strvbclfrotr)+"',"+
							"'"+oeschkval+"','"+cf.encode(escrdgtxt)+"','"+cf.encode(etnocomments.getText().toString())+"','"+addchkval+"',"+
							"'"+cf.encode(straddcomments)+"')");
					if(oeschkval==1){ ((TextView)findViewById(R.id.savetxt8)).setVisibility(cf.v1.INVISIBLE);}
					else{ ((TextView)findViewById(R.id.savetxt8)).setVisibility(cf.v1.VISIBLE);
					cf.ShowToast("Conditional of Overall Electrical Services saved successfully.", 0);} 
					oes_sve=true;
			 	}
				catch (Exception E)
				{
					String strerrorlog="Inserting the Condiiton of Electrical Services - FourPoint";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
				
		}
		else
		{
			try
			{
				 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Details+ " set fld_oesinc='"+oeschkval+"',oes='"+cf.encode(escrdgtxt)+"',"+
						 "oescomment='"+cf.encode(etnocomments.getText().toString())+"' where fld_srid='"+cf.selectedhomeid+"'");
				 if(oeschkval==1){ ((TextView)findViewById(R.id.savetxt8)).setVisibility(cf.v1.INVISIBLE);}
					else{ ((TextView)findViewById(R.id.savetxt8)).setVisibility(cf.v1.VISIBLE);
					cf.ShowToast("Conditional of Overall Electrical Services saved successfully.", 0);} 
				 oes_sve=true;
			}
			catch (Exception E)
			{
				String strerrorlog="Updating the Condiiton of Electrical Services - FourPoint";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			}
		}
	*/}
	private void LFR_Insert() {
		// TODO Auto-generated method stub
		  chk_values();
		  strvbclfrotr = etlfr.getText().toString();
		  strnocomments = etnocomments.getText().toString();
		  straddcomments = etaddcomments.getText().toString();
		  
		  if(chkdet_save.getCount()==0)
		  {
			  try
				{
					cf.arr_db.execSQL("INSERT INTO "
							+ cf.Four_Electrical_Details
							+ " (fld_srid,fld_vbcinc,vbc,fld_lfrin,lfr,lfrotr,fld_oesinc,oes,oescomment,addchk,addcomment)"
							+ "VALUES('"+cf.selectedhomeid+"','"+vbcchkval+"','"+cf.encode(vbcchk_value)+"','"+lfrchkval+"',"+
							"'"+cf.encode(lfrchk_vlaue)+"','"+cf.encode(strvbclfrotr)+"',"+
							"'"+oeschkval+"','"+cf.encode(escrdgtxt)+"','"+cf.encode(etnocomments.getText().toString())+"','"+addchkval+"',"+
							"'"+cf.encode(straddcomments)+"')");
					if(lfrchkval==1){ ((TextView)findViewById(R.id.savetxt6)).setVisibility(cf.v1.INVISIBLE);}
					else{ ((TextView)findViewById(R.id.savetxt6)).setVisibility(cf.v1.VISIBLE);
					cf.ShowToast("Light Fixtures/Receptacles saved successfully.", 0);} 
					lfr_sve=true;
			 	}
				catch (Exception E)
				{
					String strerrorlog="Inserting the Light Fixtures/Receptacles - FourPoint";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
				
		}
		else
		{
			try
			{
				 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Details+ " set fld_lfrin='"+lfrchkval+"',lfr='"+cf.encode(lfrchk_vlaue)+"',"+
						 "lfrotr='"+cf.encode(strvbclfrotr)+"' where fld_srid='"+cf.selectedhomeid+"'");
				 if(lfrchkval==1){ ((TextView)findViewById(R.id.savetxt6)).setVisibility(cf.v1.INVISIBLE);}
					else{ ((TextView)findViewById(R.id.savetxt6)).setVisibility(cf.v1.VISIBLE);
					cf.ShowToast("Light Fixtures/Receptacles saved successfully.", 0);} 
				 lfr_sve=true;
			}
			catch (Exception E)
			{
				String strerrorlog="Updating the Light Fixtures/Receptacles - FourPoint";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			}
		}
	}
	/*private void RECEP_Insert() {
		// TODO Auto-generated method stub
		 chk_values();
		  if(chkrec_save.getCount()==0)
		  {
			  try
				{
					cf.arr_db.execSQL("INSERT INTO "
							+ cf.Four_Electrical_Receptacles
							+ " (fld_srid,fld_inc,receptacle,gfcireceptacle)"
							+ "VALUES('"+cf.selectedhomeid+"','"+recchkval+"','"+cf.encode(rectypchk_value)+"','"+cf.encode(gfcrectypchk_value)+"')");
					 if(recchkval==1){ ((TextView)findViewById(R.id.savetxt4)).setVisibility(cf.v1.INVISIBLE);}
					 else{ ((TextView)findViewById(R.id.savetxt4)).setVisibility(cf.v1.VISIBLE);
						cf.ShowToast("Receptacles/Outlets saved successfully.", 0);} 
					recp_sve=true;
			 	}
				catch (Exception E)
				{
					String strerrorlog="Inserting the Receptacles/Outlets - FourPoint";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
				
		}
		else
		{
			try
			{
				cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Receptacles+ " set fld_inc='"+recchkval+"',receptacle='"+cf.encode(rectypchk_value)+"',"+
			     "gfcireceptacle='"+cf.encode(gfcrectypchk_value)+"' where fld_srid='"+cf.selectedhomeid+"'");
				 if(recchkval==1){ ((TextView)findViewById(R.id.savetxt4)).setVisibility(cf.v1.INVISIBLE);}
					else{ ((TextView)findViewById(R.id.savetxt4)).setVisibility(cf.v1.VISIBLE);
					cf.ShowToast("Receptacles/Outlets saved successfully.", 0);} 
				 recp_sve=true;
			}
			catch (Exception E)
			{
				String strerrorlog="Updating the Receptacles/Outlets - FourPoint";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			}
		}
	}*/
	private void VEP_Insert() {
		// TODO Auto-generated method stub
		  chk_values();
		  if(chkvep_save.getCount()==0)
		  {
			  try
				{
					cf.arr_db.execSQL("INSERT INTO "
							+ cf.Four_Electrical_Panel
							+ " (fld_srid,fld_inc,visiblepanel,type)"
							+ "VALUES('"+cf.selectedhomeid+"','"+vepchkval+"','"+cf.encode(veprdgtxt)+"','"+cf.encode(veptypchk_vlaue)+"')");
					if(vepchkval==1){ ((TextView)findViewById(R.id.savetxt3)).setVisibility(cf.v1.INVISIBLE);}
					else{ ((TextView)findViewById(R.id.savetxt3)).setVisibility(cf.v1.VISIBLE);
					cf.ShowToast("Visible Electrical Panel/Disconnect Issues Noted saved successfully.", 0);}
					vep_sve=true;
			 	}
				catch (Exception E)
				{
					String strerrorlog="Inserting the Visible Electrical Panel  - FourPoint";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
				
		}
		else
		{
			try
			{
				cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Panel+ " set fld_inc='"+vepchkval+"',visiblepanel='"+cf.encode(veprdgtxt)+"',"+
			     "type='"+cf.encode(veptypchk_vlaue)+"' where fld_srid='"+cf.selectedhomeid+"'");
				if(vepchkval==1){ ((TextView)findViewById(R.id.savetxt3)).setVisibility(cf.v1.INVISIBLE);}
				else{ ((TextView)findViewById(R.id.savetxt3)).setVisibility(cf.v1.VISIBLE);
				cf.ShowToast("Visible Electrical Panel/Disconnect Issues Noted saved successfully.", 0);}
				vep_sve=true;
			}
			catch (Exception E)
			{
				String strerrorlog="Updating the Visible Electrical Panel - FourPoint";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			}
		}
	}
	private void ged_Insert() {
		// TODO Auto-generated method stub
		 chk_values();
		  if(chkged_save.getCount()==0)
		  {
			  try
				{
					cf.arr_db.execSQL("INSERT INTO "
							+ cf.Four_Electrical_General
							+ " (fld_srid,fld_inc,electricalservice,servicetype,electricalmeter,location,locationotr,servicemain,servicemainotr)"
							+ "VALUES('"+cf.selectedhomeid+"','"+gedchkval+"','"+cf.encode(esrdgtxt)+"','"+cf.encode(strdgtxt)+"','"+cf.encode(emrdgtxt)+"',"+
							"'"+cf.encode(locstr)+"','"+cf.encode(((EditText)findViewById(R.id.loc_otr)).getText().toString())+"','"+cf.encode(eservmainchk_value)+"',"+
							"'"+cf.encode(((EditText)findViewById(R.id.sm_otr)).getText().toString())+"')");
					if(gedchkval==1){ ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.INVISIBLE);}
					else{ ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
					cf.ShowToast("General Electrical Details saved successfully.", 0);}
					ged_sve=true;
					
			 	}
				catch (Exception E)
				{
					String strerrorlog="Inserting the General Electrical Details  - FourPoint";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
				
		}
		else
		{
			try
			{
				cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_General+ " set fld_inc='"+gedchkval+"',electricalservice='"+cf.encode(esrdgtxt)+"',"+
			     "servicetype='"+cf.encode(strdgtxt)+"',electricalmeter='"+cf.encode(emrdgtxt)+"',"+
				 "location='"+cf.encode(locstr)+"',locationotr='"+cf.encode(((EditText)findViewById(R.id.loc_otr)).getText().toString())+"',"+
			     "servicemain='"+cf.encode(eservmainchk_value)+"',"+
				 "servicemainotr='"+cf.encode(((EditText)findViewById(R.id.sm_otr)).getText().toString())+"'  where fld_srid='"+cf.selectedhomeid+"'");
				if(gedchkval==1){ ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.INVISIBLE);}
				else{ ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
				cf.ShowToast("General Electrical Details saved successfully.", 0);}
				ged_sve=true;
			}
			catch (Exception E)
			{
				String strerrorlog="Updating the General Electrical Details - FourPoint";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			}
		}
	}
	private void chk_values() {
		// TODO Auto-generated method stub
		try
		{
			chkged_save=cf.SelectTablefunction(cf.Four_Electrical_General, " where fld_srid='"+cf.selectedhomeid+"' AND fld_inc<>''");
			if(chkged_save.getCount()>0){
				chkged_save.moveToFirst();
				retesrdtxt = chkged_save.getString(chkged_save.getColumnIndex("electricalservice"));
			}
			
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		try
		{
			chkvep_save=cf.SelectTablefunction(cf.Four_Electrical_Panel, " where fld_srid='"+cf.selectedhomeid+"' AND fld_inc<>''");
			System.out.println("chkvep_save"+chkvep_save.getCount());
			if(chkvep_save.getCount()>0){
				chkvep_save.moveToFirst();
				retvprdtxt = chkvep_save.getString(chkvep_save.getColumnIndex("visiblepanel"));
			}
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		try
		{
			chkrec_save=cf.SelectTablefunction(cf.Four_Electrical_Receptacles, " where fld_srid='"+cf.selectedhomeid+"' AND fld_inc<>''");
			if(chkrec_save.getCount()>0){
				chkrec_save.moveToFirst();
				retrecptxt = chkrec_save.getString(chkrec_save.getColumnIndex("receptacle"));
			}
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		try
		{
			chkveb_save=cf.SelectTablefunction(cf.Four_Electrical_Details, " where fld_srid='"+cf.selectedhomeid+"' AND fld_vbcinc<>''");
			System.out.println("chkveb_save"+chkveb_save.getCount());
			if(chkveb_save.getCount()>0){
				chkveb_save.moveToFirst();
				detretincvbc = chkveb_save.getString(chkveb_save.getColumnIndex("fld_vbcinc"));
				detretvbc = cf.decode(chkveb_save.getString(chkveb_save.getColumnIndex("vbc")));
				
			}
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		try
		{
			chkcom_save=cf.SelectTablefunction(cf.Four_Electrical_Details, " where fld_srid='"+cf.selectedhomeid+"' AND addchk<>''");
			System.out.println("chk="+chkcom_save.getCount());
			if(chkcom_save.getCount()>0){
				chkcom_save.moveToFirst();
				detretaddcomt = cf.decode(chkcom_save.getString(chkcom_save.getColumnIndex("addcomment")));
		}
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		try
		{
			chkdet_save=cf.SelectTablefunction(cf.Four_Electrical_Details, " where fld_srid='"+cf.selectedhomeid+"'");
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		try
		{
			chklfr_save=cf.SelectTablefunction(cf.Four_Electrical_Details, " where fld_srid='"+cf.selectedhomeid+"' AND fld_lfrin<>''");
			System.out.println("chklfr_save"+chklfr_save.getCount());
			if(chklfr_save.getCount()>0){
				chklfr_save.moveToFirst();
				detretinclfr = chklfr_save.getString(chklfr_save.getColumnIndex("fld_lfrin"));
				detretlfr = cf.decode(chklfr_save.getString(chklfr_save.getColumnIndex("lfr")));
				detretlfrotr = cf.decode(chklfr_save.getString(chklfr_save.getColumnIndex("lfrotr")));
			}
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		try
		{
			chkoes_save=cf.SelectTablefunction(cf.Four_Electrical_Details, " where fld_srid='"+cf.selectedhomeid+"' AND fld_oesinc<>''");
			if(chkoes_save.getCount()>0){
				chkoes_save.moveToFirst();
				detretincoes = chkoes_save.getString(chkoes_save.getColumnIndex("fld_oesinc"));
				detretoes = cf.decode(chkoes_save.getString(chkoes_save.getColumnIndex("oes")));
				detretoesnocomt = cf.decode(chkoes_save.getString(chkoes_save.getColumnIndex("oescomment")));
			}
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		try
		{
			chkgo_save=cf.SelectTablefunction(cf.Four_Electrical_GO, " where fld_srid='"+cf.selectedhomeid+"' AND fld_goinc<>''");
			if(chkgo_save.getCount()>0){
				chkgo_save.moveToFirst();
				rethaztxt = chkgo_save.getString(chkgo_save.getColumnIndex("haz"));
			}
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		try
		{
			chkwir_save=cf.SelectTablefunction(cf.Four_Electrical_Wiring, " where fld_srid='"+cf.selectedhomeid+"' AND fld_wirinc<>''");
			if(chkwir_save.getCount()>0){
				chkwir_save.moveToFirst();
				retwirtxt = chkwir_save.getString(chkwir_save.getColumnIndex("wirtyp"));
			}
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		try
		{
			chkmpl_save=cf.SelectTablefunction(cf.Four_Electrical_MainPanel, " where fld_srid='"+cf.selectedhomeid+"' AND fld_electinc<>''");
			if(chkmpl_save.getCount()>0){
				chkmpl_save.moveToFirst();
				retmmpcondtxt = chkmpl_save.getString(chkmpl_save.getColumnIndex("mmpcond"));
			}
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		try
		{
			chkspl_save=cf.SelectTablefunction(cf.Four_Electrical_SubPanel, " where fld_srid='"+cf.selectedhomeid+"' AND fld_electinc<>''");
			if(chkspl_save.getCount()>0){
				chkspl_save.moveToFirst();
				retspptxt = chkspl_save.getString(chkspl_save.getColumnIndex("spp"));
			}
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		try
		{
			chkspp_save=cf.SelectTablefunction(cf.Four_Electrical_SubPanelPresent, " where fld_srid='"+cf.selectedhomeid+"'");
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}
	private void hideotherlayouts() {
		// TODO Auto-generated method stub
		((LinearLayout)findViewById(R.id.gedlin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.epelin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.veplin)).setVisibility(cf.v1.GONE);
		//((LinearLayout)findViewById(R.id.recplin)).setVisibility(cf.v1.GONE);
		//((LinearLayout)findViewById(R.id.vbclin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.lfrlin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.golin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.wirlin)).setVisibility(cf.v1.GONE);
		//((LinearLayout)findViewById(R.id.oeslin)).setVisibility(cf.v1.GONE);
		
		((ImageView)findViewById(R.id.shwged)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwep)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwvep)).setVisibility(cf.v1.VISIBLE);
		//((ImageView)findViewById(R.id.shwrec)).setVisibility(cf.v1.VISIBLE);
		//((ImageView)findViewById(R.id.shwvbc)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwlf)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwgo)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwwir)).setVisibility(cf.v1.VISIBLE);
		//((ImageView)findViewById(R.id.shwcon)).setVisibility(cf.v1.VISIBLE);
		
		((ImageView)findViewById(R.id.hdged)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdep)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdvep)).setVisibility(cf.v1.GONE);
	//	((ImageView)findViewById(R.id.hdrec)).setVisibility(cf.v1.GONE);
		//((ImageView)findViewById(R.id.hdvbc)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdlf)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdgo)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdwir)).setVisibility(cf.v1.GONE);
		//((ImageView)findViewById(R.id.hdcon)).setVisibility(cf.v1.GONE);
		
		/*chk_values();
		if(retesrdtxt.equals("")){
			((LinearLayout)findViewById(R.id.gedlin)).setVisibility(cf.v1.GONE);
		}
		if(retmmpcondtxt.equals("")|| retspptxt.equals("")){
		     ((LinearLayout)findViewById(R.id.epelin)).setVisibility(cf.v1.GONE);
		}
		if(retvprdtxt.equals("")){
		 ((LinearLayout)findViewById(R.id.veplin)).setVisibility(cf.v1.GONE);
		}
		if(retrecptxt.equals("")){
		  ((LinearLayout)findViewById(R.id.recplin)).setVisibility(cf.v1.GONE);
		}
		if(detretvbc.equals("")){
			((LinearLayout)findViewById(R.id.vbclin)).setVisibility(cf.v1.GONE);
		}
		if(detretlfr.equals("")){
		 ((LinearLayout)findViewById(R.id.lfrlin)).setVisibility(cf.v1.GONE);
		}
		if(rethaztxt.equals("")){
			((LinearLayout)findViewById(R.id.golin)).setVisibility(cf.v1.GONE);
		}
		if(retwirtxt.equals("")){
		((LinearLayout)findViewById(R.id.wirlin)).setVisibility(cf.v1.GONE);
		}
		if(detretoes.equals("")){
			((LinearLayout)findViewById(R.id.oeslin)).setVisibility(cf.v1.GONE);
		}
	*/
	}
	public void unCheck(CheckBox[] hdrschk) {
		// TODO Auto-generated method stub
		for(int i=0;i<hdrschk.length;i++)
		{
			hdrschk[i].setChecked(false);
			
		}
		if(!retesrdtxt.equals("")){
			hdrschk[0].setChecked(true);((LinearLayout)findViewById(R.id.gedlin)).setVisibility(cf.v1.VISIBLE);
		}
		else
		{
			clearged();
		}
		if(!retmmpcondtxt.equals("")&&!retspptxt.equals("")){
			hdrschk[1].setChecked(true);((LinearLayout)findViewById(R.id.epelin)).setVisibility(cf.v1.VISIBLE);
		}
		else
		{
			clearep();
		}
		if(!retvprdtxt.equals("")){
			hdrschk[2].setChecked(true);((LinearLayout)findViewById(R.id.veplin)).setVisibility(cf.v1.VISIBLE);
		}
		else
		{
			clearvep();
		}
		if(!retwirtxt.equals("")){
			hdrschk[3].setChecked(true);((LinearLayout)findViewById(R.id.wirlin)).setVisibility(cf.v1.VISIBLE);
		}
		else
		{
			clearwir();
		}
		/*if(!retrecptxt.equals("")){
			hdrschk[4].setChecked(true);((LinearLayout)findViewById(R.id.recplin)).setVisibility(cf.v1.VISIBLE);
		}
		else
		{
			clearrec();
		}*/
		/*if(chkdet_save.getCount()>0 && !detretvbc.equals("")){
			hdrschk[5].setChecked(true);((LinearLayout)findViewById(R.id.vbclin)).setVisibility(cf.v1.VISIBLE);
		}
		else
		{
			clearvbc();
		}*/
		if(chkdet_save.getCount()>0 && !detretlfr.equals("")){
			hdrschk[6].setChecked(true);((LinearLayout)findViewById(R.id.lfrlin)).setVisibility(cf.v1.VISIBLE);
		}
		else
		{
			clearlf();
		}
		/*if(!detretoes.equals("")){
			hdrschk[8].setChecked(true);((LinearLayout)findViewById(R.id.oeslin)).setVisibility(cf.v1.VISIBLE);
		}
		else
		{
			clearcon();
		}*/
		if(!rethaztxt.equals("")){
			hdrschk[7].setChecked(true);((LinearLayout)findViewById(R.id.golin)).setVisibility(cf.v1.VISIBLE);
		}
		else
		{
			cleargo();
		}
		
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
 		// replaces the default 'Back' button action
 		if (keyCode == KeyEvent.KEYCODE_BACK) {
 			cf.goclass(23);
 			return true;
 		}
 		
 		return super.onKeyDown(keyCode, event);
 	}
	
   	public void showunchkalert(final int i) {
		// TODO Auto-generated method stub
		String titname="";
		switch (i) {
		case 1:
			titname=((TextView)findViewById(R.id.tit1)).getText().toString();
			break;
		case 2:
			titname=((TextView)findViewById(R.id.tit2)).getText().toString();
			break;
		case 3:
			titname=((TextView)findViewById(R.id.tit3)).getText().toString();
			break;
		case 4:
			titname=((TextView)findViewById(R.id.tit4)).getText().toString();
			break;
		case 5:
			titname=((TextView)findViewById(R.id.tit5)).getText().toString();
			break;
	/*	case 6:
			titname=((TextView)findViewById(R.id.tit6)).getText().toString();
			break;*/
		case 7:
			titname=((TextView)findViewById(R.id.tit7)).getText().toString();
			break;
		case 8:
			titname=((TextView)findViewById(R.id.tit8)).getText().toString();
			break;
		/*case 9:
			titname=((TextView)findViewById(R.id.tit9)).getText().toString();
			break;*/
		default:
			break;
		}
		AlertDialog.Builder bl = new Builder(Electrical.this);
		bl.setTitle("Confirmation");
		bl.setIcon(R.drawable.alertmsg);
		bl.setMessage(Html.fromHtml("Do you want to clear the "+titname+" data?"));
		bl.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog,
											int id) {
										 switch(i){
										 case 1:
											 ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.INVISIBLE);
											 clearged();gedchkval=1;ged_sve=false;//ged_Insert();
							    			 ((LinearLayout)findViewById(R.id.gedlin)).setVisibility(cf.v1.GONE);
							    			// ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
							    			 ((ImageView)findViewById(R.id.shwged)).setVisibility(cf.v1.VISIBLE);
							    			 ((ImageView)findViewById(R.id.hdged)).setVisibility(cf.v1.GONE);
							    			 ((ImageView)findViewById(R.id.shwged)).setEnabled(false);
							    			 break;
										 case 2:
											 ((TextView)findViewById(R.id.savetxt2)).setVisibility(cf.v1.INVISIBLE);
											 clearep();elechkval=1;chk1=false;chk2=false;//mainpanel_Insert();sp_Insert();
											// cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_SubPanelPresent+" Where fld_srid='"+cf.selectedhomeid+"'");
											
											 ((LinearLayout)findViewById(R.id.epelin)).setVisibility(cf.v1.GONE);
											 ((LinearLayout)findViewById(R.id.spyes)).setVisibility(cf.v1.GONE);
											// ((TextView)findViewById(R.id.savetxt2)).setVisibility(cf.v1.VISIBLE);
											 ((ImageView)findViewById(R.id.shwep)).setVisibility(cf.v1.VISIBLE);
							    			 ((ImageView)findViewById(R.id.hdep)).setVisibility(cf.v1.GONE);
							    			 ((ImageView)findViewById(R.id.shwep)).setEnabled(false);
							    			break;
										 case 3:
											 ((TextView)findViewById(R.id.savetxt3)).setVisibility(cf.v1.INVISIBLE);
											 clearvep();vepchkval=1;vep_sve=false;//VEP_Insert();
							    			 ((LinearLayout)findViewById(R.id.veplin)).setVisibility(cf.v1.GONE);
							    			 //((TextView)findViewById(R.id.savetxt3)).setVisibility(cf.v1.VISIBLE);
							    			 ((ImageView)findViewById(R.id.shwvep)).setVisibility(cf.v1.VISIBLE);
							    			 ((ImageView)findViewById(R.id.hdvep)).setVisibility(cf.v1.GONE);
							    			 ((ImageView)findViewById(R.id.shwvep)).setEnabled(false);
											 break;
									/*	 case 4:
											 ((TextView)findViewById(R.id.savetxtwir)).setVisibility(cf.v1.INVISIBLE);
											 clearwir();wirchkval=1;wir_sve=false;//WIR_Insert();
							    			 ((LinearLayout)findViewById(R.id.wirlin)).setVisibility(cf.v1.GONE);
							    			 //((TextView)findViewById(R.id.savetxtwir)).setVisibility(cf.v1.VISIBLE);
							    			 ((ImageView)findViewById(R.id.shwwir)).setVisibility(cf.v1.VISIBLE);
							    			 ((ImageView)findViewById(R.id.hdwir)).setVisibility(cf.v1.GONE);
							    			 ((ImageView)findViewById(R.id.shwwir)).setEnabled(false);
											 break;
										 case 5:
											 ((TextView)findViewById(R.id.savetxt4)).setVisibility(cf.v1.INVISIBLE);
											 clearrec();recchkval=1;recp_sve=false;//RECEP_Insert();
							    			 ((LinearLayout)findViewById(R.id.recplin)).setVisibility(cf.v1.GONE);
							    			 //((TextView)findViewById(R.id.savetxt4)).setVisibility(cf.v1.VISIBLE);
							    			 ((ImageView)findViewById(R.id.shwrec)).setVisibility(cf.v1.VISIBLE);
							    			 ((ImageView)findViewById(R.id.hdrec)).setVisibility(cf.v1.GONE);
							    			 ((ImageView)findViewById(R.id.shwrec)).setEnabled(false);
											 break;*/
									/*	 case 6:
											 ((TextView)findViewById(R.id.savetxt5)).setVisibility(cf.v1.INVISIBLE);
											 clearvbc();vbcchkval=1;vbc_sve=false;//VBC_Insert();
							    			 ((LinearLayout)findViewById(R.id.vbclin)).setVisibility(cf.v1.GONE);
							    			 //((TextView)findViewById(R.id.savetxt5)).setVisibility(cf.v1.VISIBLE);
							    			 ((ImageView)findViewById(R.id.shwvbc)).setVisibility(cf.v1.VISIBLE);
							    			 ((ImageView)findViewById(R.id.hdvbc)).setVisibility(cf.v1.GONE);
							    			 ((ImageView)findViewById(R.id.shwvbc)).setEnabled(false);
											 break;*/
										 case 7:
											 ((TextView)findViewById(R.id.savetxt6)).setVisibility(cf.v1.INVISIBLE);
											 clearlf();lfrchkval=1;lfr_sve=false;//LFR_Insert();
							    			 ((LinearLayout)findViewById(R.id.lfrlin)).setVisibility(cf.v1.GONE);
							    			// ((TextView)findViewById(R.id.savetxt6)).setVisibility(cf.v1.VISIBLE);
							    			 ((ImageView)findViewById(R.id.shwlf)).setVisibility(cf.v1.VISIBLE);
							    			 ((ImageView)findViewById(R.id.hdlf)).setVisibility(cf.v1.GONE);
							    			 ((ImageView)findViewById(R.id.shwlf)).setEnabled(false);
											 break;
										 case 8:
											 ((TextView)findViewById(R.id.savetxt7)).setVisibility(cf.v1.INVISIBLE);
											 cleargo();gochkval=1;go_sve=false;//GO_Insert();
							    			 ((LinearLayout)findViewById(R.id.golin)).setVisibility(cf.v1.GONE);
							    			// ((TextView)findViewById(R.id.savetxt7)).setVisibility(cf.v1.VISIBLE);
							    			 ((ImageView)findViewById(R.id.shwgo)).setVisibility(cf.v1.VISIBLE);
							    			 ((ImageView)findViewById(R.id.hdgo)).setVisibility(cf.v1.GONE);
							    			 ((ImageView)findViewById(R.id.shwgo)).setEnabled(false);
											 break;
										/* case 9:
											 ((TextView)findViewById(R.id.savetxt8)).setVisibility(cf.v1.INVISIBLE);
											 clearcon();oeschkval=1;oes_sve=false;//OES_Insert();
							    			 ((LinearLayout)findViewById(R.id.oeslin)).setVisibility(cf.v1.GONE);
							    			// ((TextView)findViewById(R.id.savetxt8)).setVisibility(cf.v1.VISIBLE);
							    			 ((ImageView)findViewById(R.id.shwcon)).setVisibility(cf.v1.VISIBLE);
							    			 ((ImageView)findViewById(R.id.hdcon)).setVisibility(cf.v1.GONE);
							    			 ((ImageView)findViewById(R.id.shwcon)).setEnabled(false);
											 break;*/
										 }
										 	 
										 
									}
		});
		bl.setNegativeButton("No",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog,
							int id) {
						switch(i){
						 case 1:
							 ((CheckBox)findViewById(R.id.ged_chk)).setChecked(false);
							 ((ImageView)findViewById(R.id.shwged)).setVisibility(cf.v1.GONE);
				    		 ((ImageView)findViewById(R.id.hdged)).setVisibility(cf.v1.VISIBLE);
				    		 ((LinearLayout)findViewById(R.id.gedlin)).setVisibility(cf.v1.VISIBLE);
							 break;
						 case 2:
							 ((CheckBox)findViewById(R.id.epe_chk)).setChecked(false);
							 ((ImageView)findViewById(R.id.shwep)).setVisibility(cf.v1.GONE);
				    		 ((ImageView)findViewById(R.id.hdep)).setVisibility(cf.v1.VISIBLE);
				    		 ((LinearLayout)findViewById(R.id.epelin)).setVisibility(cf.v1.VISIBLE);
							 break;
						 case 3:
							 ((CheckBox)findViewById(R.id.vep_chk)).setChecked(false);
							 ((ImageView)findViewById(R.id.shwvep)).setVisibility(cf.v1.GONE);
				    		 ((ImageView)findViewById(R.id.hdvep)).setVisibility(cf.v1.VISIBLE);
				    		 ((LinearLayout)findViewById(R.id.veplin)).setVisibility(cf.v1.VISIBLE);
						 break;
						 case 4:
							 ((CheckBox)findViewById(R.id.wir_chk)).setChecked(false);
							 ((ImageView)findViewById(R.id.shwwir)).setVisibility(cf.v1.GONE);
				    		 ((ImageView)findViewById(R.id.hdwir)).setVisibility(cf.v1.VISIBLE);
				    		 ((LinearLayout)findViewById(R.id.wirlin)).setVisibility(cf.v1.VISIBLE);
							 break;
					/*	 case 5:
							 ((CheckBox)findViewById(R.id.recp_chk)).setChecked(false);
							 ((ImageView)findViewById(R.id.shwrec)).setVisibility(cf.v1.GONE);
				    		 ((ImageView)findViewById(R.id.hdrec)).setVisibility(cf.v1.VISIBLE);
				    		 ((LinearLayout)findViewById(R.id.recplin)).setVisibility(cf.v1.VISIBLE);
							 break;*/
					/*	 case 6:
							 ((CheckBox)findViewById(R.id.vbc_chk)).setChecked(false);
							 ((ImageView)findViewById(R.id.shwvbc)).setVisibility(cf.v1.GONE);
				    		 ((ImageView)findViewById(R.id.hdvbc)).setVisibility(cf.v1.VISIBLE);
				    		 ((LinearLayout)findViewById(R.id.vbclin)).setVisibility(cf.v1.VISIBLE);
							 break;*/
						 case 7:
							 ((CheckBox)findViewById(R.id.lfr_chk)).setChecked(false);
							 ((ImageView)findViewById(R.id.shwlf)).setVisibility(cf.v1.GONE);
				    		 ((ImageView)findViewById(R.id.hdlf)).setVisibility(cf.v1.VISIBLE);
				    		 ((LinearLayout)findViewById(R.id.lfrlin)).setVisibility(cf.v1.VISIBLE);
							break;
						 case 8:
							 ((CheckBox)findViewById(R.id.go_chk)).setChecked(false);
							 ((ImageView)findViewById(R.id.shwgo)).setVisibility(cf.v1.GONE);
				    		 ((ImageView)findViewById(R.id.hdgo)).setVisibility(cf.v1.VISIBLE);
				    		 ((LinearLayout)findViewById(R.id.golin)).setVisibility(cf.v1.VISIBLE);
							 break;
						/* case 9:
							 ((CheckBox)findViewById(R.id.oes_chk)).setChecked(false);
							 ((ImageView)findViewById(R.id.shwcon)).setVisibility(cf.v1.GONE);
				    		 ((ImageView)findViewById(R.id.hdcon)).setVisibility(cf.v1.VISIBLE);
				    		 ((LinearLayout)findViewById(R.id.oeslin)).setVisibility(cf.v1.VISIBLE);
						 break;*/
						 }
					}
        });
		AlertDialog al=bl.create();
		al.setIcon(R.drawable.alertmsg);
		al.setCancelable(false);
		al.show();		
	}
	protected void clearep() {
		// TODO Auto-generated method stub
		elechkval=0;mmpck=true;mptack=true;yicfck=true;dlck=true;ruck=true;
		 pcck=true;spck=true;
		 try{if(mmpck)mmpcrdgval.clearCheck();}catch(Exception e){}
		 try{if(mptack)mptrdgval.clearCheck();}catch(Exception e){}
		 try{if(yicfck)yicrdgval.clearCheck();}catch(Exception e){}
		 try{if(dlck)dsclrdgval.clearCheck();}catch(Exception e){}
		 try{if(ruck)updrdgval.clearCheck();}catch(Exception e){}
		 try{if(pcck)epcrdgval.clearCheck();}catch(Exception e){}
		 try{if(spck)sprdgval.clearCheck();}catch(Exception e){}
		 ((RadioButton)yicrdgval.findViewWithTag("Original")).setChecked(true);
		 mmpcrdgtxt="";mptrdgtxt="";mptachk_vlaue="";mptypechk_vlaue="";yicrdgtxt="Original";
		 agestr="";mainlocstr="--Select--";dsclrdgtxt="";updrdgtxt="";etypchk_vlaue="";
		 epcrdgtxt="";sprdgtxt="";spampstr="";spchk_vlaue="";
		// cf.Set_UncheckBox(mptachk, ((EditText)findViewById(R.id.mpt_otr)));
		 
		 for(int i=0;i<mptachk.length;i++)
		 {
			 mptachk[i].setChecked(false);
		 }
		 
		 
		 ((TextView)findViewById(R.id.mpttxt)).setVisibility(cf.v1.INVISIBLE);
		 defaultage();defaultdlup();
		// agespin.setSelection(ageadap.getPosition("2012"));
		 ((EditText)findViewById(R.id.age_otr)).setText("");
		 ((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.GONE);
		 ((EditText)findViewById(R.id.mploc_otr)).setText("");
		 ((EditText)findViewById(R.id.mploc_otr)).setVisibility(cf.v1.INVISIBLE);
		 ((EditText)findViewById(R.id.dsc_location)).setText("");
		 ((EditText)findViewById(R.id.dsc_location)).setVisibility(cf.v1.GONE);
		 ((EditText)findViewById(R.id.upd_yes)).setText("");
		 ((LinearLayout)findViewById(R.id.perlin)).setVisibility(cf.v1.GONE);
		//  cf.Set_UncheckBox(etypechk, ((EditText)findViewById(R.id.upd_yes)));
		  /*((EditText)findViewById(R.id.edlutxt)).setText("");
		  ((CheckBox)findViewById(R.id.ndchk)).setChecked(false);
		  ((Button)findViewById(R.id.egetdlup)).setEnabled(true);nc=0;*/
		  mainlocspn.setSelection(0);
		  ((LinearLayout)findViewById(R.id.rushow)).setVisibility(cf.v1.GONE);
		  ((TextView)findViewById(R.id.sp_no)).setText("0");
		  ((EditText)findViewById(R.id.et_splocation)).setText("");
		  ((Spinner)findViewById(R.id.spamp_spin)).setSelection(0);
		  ((TextView)findViewById(R.id.spttxt)).setVisibility(cf.v1.INVISIBLE);
		  cf.Set_UncheckBox(spchk, ((EditText)findViewById(R.id.upd_yes)));
		  ((LinearLayout)findViewById(R.id.spyes)).setVisibility(cf.v1.GONE);
		  cf.arr_db.execSQL("UPDATE  "+cf.Four_Electrical_SubPanelPresent+" SET fld_flag='1' Where fld_srid='"+cf.selectedhomeid+"' ");
		  retmmpcondtxt="";retspptxt="";
		  ShowSubPanelValue();
	}
	private void defaultdlup() {
		// TODO Auto-generated method stub
		((CheckBox)findViewById(R.id.ndchk)).setChecked(true);
		 ((Button)findViewById(R.id.egetdlup)).setEnabled(false);
		 ((EditText)findViewById(R.id.edlutxt)).setText("Not Determined");
	}
	protected void clearcon() {
		// TODO Auto-generated method stub
		 oeschkval=0;escck=true;
		 try{if(escck){escrdgval.clearCheck();}}catch(Exception e){}
		 escrdgtxt="";
		 ((LinearLayout)findViewById(R.id.yescondshow)).setVisibility(cf.v1.GONE);
		 ((EditText)findViewById(R.id.nocomment)).setText("");
		 ((LinearLayout)findViewById(R.id.nocommentsshow)).setVisibility(cf.v1.GONE);detretoes="";
	}
	protected void cleargo() {
		// TODO Auto-generated method stub
		 gochkval=0;//hzpck=true;
		 lsck=true;lcck=true;
		 //try{if(hzpck){hzprdgval.clearCheck();}}catch(Exception e){}
		 //try{if(lsck){lsrdgval.clearCheck();}}catch(Exception e){}
		 //try{if(lcck){lcrdgval.clearCheck();}}catch (Exception e) {}
		 //lsrdgtxt="";lcrdgtxt="";
		 //cf.Set_UncheckBox(hzchk, ((EditText)findViewById(R.id.hz_otr)));
		/* ((EditText)findViewById(R.id.lsyescomment)).setText("");
		 ((LinearLayout)findViewById(R.id.lsyescommentsshow)).setVisibility(cf.v1.GONE);
		 ((EditText)findViewById(R.id.lcyescomment)).setText("");
		 ((LinearLayout)findViewById(R.id.lcyescommentsshow)).setVisibility(cf.v1.GONE);rethaztxt="";*/
		 
		 defaultgenobs();
	}
	protected void clearlf() {
		// TODO Auto-generated method stub
		 lfrchkval=0;lfrchk_vlaue="";
		 cf.Set_UncheckBox(lfrchk, ((EditText)findViewById(R.id.lfr_otr)));
		 ((EditText)findViewById(R.id.lfr_otr)).setText("");
		 ((EditText)findViewById(R.id.lfr_otr)).setVisibility(cf.v1.GONE);detretlfr="";
	}
	protected void clearvbc() {
		// TODO Auto-generated method stub
		 vbcchkval=0;vbcchk_value="";
		 cf.Set_UncheckBox(vbcchk, ((EditText)findViewById(R.id.wir_otr)));detretvbc="";
	}
	/*protected void clearrec() {
		// TODO Auto-generated method stub
		 recchkval=0;rectypchk_value="";gfcrectypchk_value="";
		 cf.Set_UncheckBox(recpchk, ((EditText)findViewById(R.id.wir_otr)));
		 cf.Set_UncheckBox(gfcrecpchk, ((EditText)findViewById(R.id.wir_otr)));retrecptxt="";
		 recpchk[0].setChecked(true);
	}*/
	protected void clearwir() {
		// TODO Auto-generated method stub
		 wirchkval=0;wirchk_vlaue="";alumchk_vlaue="";alumfstchk_vlaue="";
		 alumsecchk_vlaue="";alumthdchk_vlaue="";
		 cf.Set_UncheckBox(wirchk, ((EditText)findViewById(R.id.wir_otr)));
		 cf.Set_UncheckBox(alumchk, ((EditText)findViewById(R.id.wir_otr)));
		 ((LinearLayout)findViewById(R.id.aluminshow)).setVisibility(cf.v1.GONE);
		 cf.Set_UncheckBox(alumfstchk, ((EditText)findViewById(R.id.wir_otr)));
		 ((LinearLayout)findViewById(R.id.firstchklin)).setVisibility(cf.v1.GONE);
		 cf.Set_UncheckBox(alumsecchk, ((EditText)findViewById(R.id.wir_otr)));
		 ((LinearLayout)findViewById(R.id.secondchklin)).setVisibility(cf.v1.GONE);
		 cf.Set_UncheckBox(alumthdchk, ((EditText)findViewById(R.id.wir_otr)));
		 ((LinearLayout)findViewById(R.id.thirdchklin)).setVisibility(cf.v1.GONE);
		 ((CheckBox)findViewById(R.id.alu_chk1)).setEnabled(true);
		 ((CheckBox)findViewById(R.id.alu_chk2)).setEnabled(true);
		 ((CheckBox)findViewById(R.id.alu_chk3)).setEnabled(true);
		 retwirtxt="";
	}
	protected void clearvep() {
		// TODO Auto-generated method stub
		 vepchkval=0;vepck=true;
		 try{if(vepck){veprdgval.clearCheck();}}catch (Exception e) {}
		 veprdgtxt="";veptypchk_vlaue="";
		 cf.Set_UncheckBox(veptypchk, ((EditText)findViewById(R.id.typ_otr)));
		 ((EditText)findViewById(R.id.typ_otr)).setText("");
		 ((EditText)findViewById(R.id.typ_otr)).setVisibility(cf.v1.GONE);
		 ((LinearLayout)findViewById(R.id.panelshow)).setVisibility(cf.v1.GONE);retvprdtxt="";
	}
	protected void clearged() {
		// TODO Auto-generated method stub
		 gedchkval=0;esck=true;stck=true;emck=true;smck=true;
		 try{if(esck){esrdgval.clearCheck();}}catch(Exception e){}
		 try{if(stck){strdgval.clearCheck();}}catch(Exception e){}
		 try{if(emck){emrdgval.clearCheck();}}catch(Exception e){}
		 //try{if(smck){smrdgval.clearCheck();}}catch (Exception e) {}
		 esrdgtxt="";strdgtxt="";emrdgtxt="";locstr="--Select--";smrdgtxt="";
		 locspn.setSelection(0);retesrdtxt="";
		 ((EditText)findViewById(R.id.sm_otr)).setText("");
		 ((EditText)findViewById(R.id.sm_otr)).setVisibility(cf.v1.GONE);
		 ((EditText)findViewById(R.id.loc_otr)).setText("");
		 ((EditText)findViewById(R.id.loc_otr)).setVisibility(cf.v1.INVISIBLE);defaultgenelecdet();
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	
		  if(requestCode==cf.loadcomment_code)
		  {
			 load_comment=true;
			if(resultCode==RESULT_OK)
			{
				
				 if(k==1)
				 {
					  String eleccomts = ((EditText)findViewById(R.id.eleccomment)).getText().toString();
				     ((EditText)findViewById(R.id.eleccomment)).setText((eleccomts +" "+data.getExtras().getString("Comments")).trim());
				 }
				 else if(k==2)
				 {
					 String hazcomts = ((EditText)findViewById(R.id.haznocomment)).getText().toString();
				     ((EditText)findViewById(R.id.haznocomment)).setText((hazcomts +" "+data.getExtras().getString("Comments")).trim());
				 }
				 cf.ShowToast("Comments added successfully.", 0);
			}
			
		}

	}
	private void defaultgenobs()
	{
		//((RadioButton) lsrdgval.findViewWithTag("Beyond Scope of Inspection")).setChecked(true);
		
	}
	private void defaultgenelecdet()
	{
		System.out.println("ttt");
		cf.getPolicyholderInformation(cf.selectedhomeid);
		try
		{
			System.out.println("tttryrr");
			
		if(Integer.parseInt(cf.YearPH)>=1900)
		{
			((RadioButton) emrdgval.findViewWithTag("Outside")).setChecked(true);
			((RadioButton) esrdgval.findViewWithTag("Underground")).setChecked(true);	
			((RadioButton) strdgval.findViewWithTag("Single Phase")).setChecked(true);
			((RadioButton) findViewById(R.id.eservm4)).setChecked(true); 
					//((RadioButton) smrdgval.findViewWithTag("Aluminum")).setChecked(true);
		}
		}catch(Exception e)
		{
			
		}
	}
}