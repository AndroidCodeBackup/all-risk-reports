package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.CommonFunctions.mDateSetListener;
import idsoft.inspectiondepot.ARR.RoofSection.RCN_clicker;
import idsoft.inspectiondepot.ARR.RoofSection.RCT_EditClick;
import idsoft.inspectiondepot.ARR.RoofSection.onclick_viewimage;
import idsoft.inspectiondepot.ARR.Roof_commercial_information.Spin_Selectedlistener;

import java.io.IOException;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeoutException;

import org.xmlpull.v1.XmlPullParserException;


import android.R.color;
import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.RelativeLayout.LayoutParams;

public class Roof_section_common extends Activity implements Runnable {
CommonFunctions cf;
private String submenu,Roof_page_value[];
private int mYear, mMonth,mDay;
CheckBox Chk_Scope[] = new CheckBox[6];
Spinner Sp_RCT[]=new Spinner[8];
String arr_RCT[]={"-Select-","Asphalt/Fiberglass Shingle","Concrete/Clay Tile","Metal","Built Up Tar and Gravel","Modified Bitumen","Wood Shake/Shingle","Membrane","Other"},
arr_YOI[]=null,arr_RS[]={"-Select-","<1/12","1/12","2/12","3/12","4/12","5/12","6/12","7/12","8/12","9/12","10/12","11/12","12/12","13/12","14/12","Flat","Unknown"},
arr_RSH[]={"-Select-","Hip Roof","Gable Roof","Mansard Roof","Flat Roof","Shed Roof","Butterfly Roof","Other"},
arr_RST[]={"-Select-","Conventional Frame","Wood Truss","Steel Truss","Concrete Frame","Structural","Steel Frame","Other"},
arr_BC[]={"-Select-",">2001 FBC","1994 SFBC","Pre 2001 FBC","Pre 1994 SFBC","Not Applicable","Beyond Scope of Inspection","Not Determined"},
arr_RC[]={"-Select-","Simple Roof Shape","Moderate Roof Shape","Complex Roof Shape"},
arr_ARL[]={"-Select-","Not Determined","Roof Aged","1+ Year","2+ Years","3+ Years","4+ Years","5+ Years","6+ Years","7+ Years","8+ Years","9+ Years",">10 Years"};
EditText RCT_other[]=new EditText[5];
EditText RCT_ed[]=new EditText[2];
CheckBox RCT_chk[]=new CheckBox[2];
Button RCT_bt[]=new Button[2];
TextView tv;
int pick_img_code=24,editid=0,mDayofWeek,selmDayofWeek,hours,minutes,seconds,amorpm;
RadioGroup add_rg,RCN_RG[]=new RadioGroup[8];
EditText RCN_other[]=new EditText[8];
TextView RCN_tv_limit[]=new TextView[8];
EditText RC_cmt;
ImageView ADD_expend,RCN_expend;
CheckBox ADD_chk_top,RCN_chk_top,RCT_chk_top;
int RCT_total_per=100;
private boolean load_comment=true;
private boolean RCT_pre_m=false;
private LinearLayout ADD_layout_head;
private LinearLayout RCN_layout_head;
private ProgressDialog pd;
private int show_handler;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		cf=new CommonFunctions(this);
		cf.CreateARRTable(431);
		Bundle extras = getIntent().getExtras();
		 if (extras != null) {
		
				cf.selectedhomeid = extras.getString("homeid");
				cf.onlinspectionid = extras.getString("InspectionType");
				
				   
			    cf.onlstatus = extras.getString("status");
			    cf.Roof=extras.getString("Roof");
			    System.out.println("Roof="+cf.Roof);
			    if(cf.Roof.equals(cf.getResourcesvalue(R.string.insp_4)) || cf.Roof.equals(cf.getResourcesvalue(R.string.insp_5)) || cf.Roof.equals(cf.getResourcesvalue(R.string.insp_6)))
			    {
			    	cf.identityval = extras.getInt("identity");
			    }
			    submenu=extras.getString("for_loading");
			    if(submenu==null)
				{	
					submenu="";
				}
			    cf.getPolicyholderInformation(cf.selectedhomeid);
			    setContentView(R.layout.roof_section_common);
				cf.getInspectorId();
				cf.getDeviceDimensions();
				/* cf.CreateARRTable(10);
				 cf.CreateARRTable(99);
				 cf.CreateARRTable(431);
				 cf.CreateARRTable(43);
				 cf.CreateARRTable(432);
				 cf.CreateARRTable(433);
				 cf.CreateARRTable(434);*/
				 custom_creat();
				 
		 }
		 
		 System.out.println("inside foooo");
	}
	private void check_for_dublication() {
		// TODO Auto-generated method stub
		Cursor c =cf.SelectTablefunction(cf.Roof_additional, " WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+Roof_page_value[5]+"'");
		Cursor c1 =cf.SelectTablefunction(cf.Rct_table, " WHERE RCT_Ssrid='"+cf.selectedhomeid+"' and RCT_insp_id='"+Roof_page_value[5]+"'");
		Cursor c2 =cf.SelectTablefunction(cf.RCN_table, " WHERE R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id='"+Roof_page_value[5]+"'");
		if(c.getCount()<=0 && c1.getCount()<=0 && c2.getCount()<=0)
		{
			
			 c =cf.arr_db.rawQuery(" SELECT DISTINCT R_ADD_insp_id FROM "+cf.Roof_additional+" WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id<>'"+Roof_page_value[5]+"'",null);
			 c1 =cf.arr_db.rawQuery(" SELECT DISTINCT RCT_insp_id FROM "+cf.Rct_table+" WHERE RCT_Ssrid='"+cf.selectedhomeid+"' and RCT_insp_id<>'"+Roof_page_value[5]+"'",null);
			 c2 =cf.arr_db.rawQuery(" SELECT DISTINCT R_RCN_insp_id FROM "+cf.RCN_table+" WHERE R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id<>'"+Roof_page_value[5]+"'",null);
		String insp="";
		c.moveToFirst();c1.moveToFirst();c2.moveToFirst();
		System.out.println(" the count "+c.getCount()+" "+c1.getCount()+" "+c2.getCount());
		for(int i=0;i<c.getCount()||i<c1.getCount()||i<c2.getCount();i++)
		{
			if(i<c.getCount())
			{
				if(!insp.contains(c.getString(c.getColumnIndex("R_ADD_insp_id"))))
					insp+=c.getString(c.getColumnIndex("R_ADD_insp_id"))+",";
				c.moveToNext();
			}
			if(i<c1.getCount())
			{
				if(!insp.contains(c1.getString(c1.getColumnIndex("RCT_insp_id"))))
					insp+=c1.getString(c1.getColumnIndex("RCT_insp_id"))+",";
				c1.moveToNext();
			}
			if(i<c2.getCount())
			{
				if(!insp.contains(c2.getString(c2.getColumnIndex("R_RCN_insp_id"))))
					insp+=c2.getString(c2.getColumnIndex("R_RCN_insp_id"))+",";
				c2.moveToNext();
			}
			
		}
		System.out.println(" the insp "+insp);
		if(!insp.equals(""))
		{
			if(insp.endsWith(","))
			{
				insp=insp.substring(0,insp.length()-1);
						
			}
			if(insp.contains("5") || insp.contains("6"))
			{
				insp=insp.replace("5", "4");
				insp=insp.replace("6", "4");
						
			}
			Cursor val= cf.SelectTablefunction(cf.inspnamelist, " WHERE ARR_Custom_ID in ("+insp+") order by ARR_Custom_ID ");
			System.out.println(" the val "+val.getCount());
			val.moveToFirst();
			if(val.getCount()>0)
			{
				String inspection[]=new String[val.getCount()+1];
				inspection[0]="--Select--";
				for(int i=1;i<=val.getCount();i++)
				{
					inspection[i]=cf.decode(val.getString(val.getColumnIndex("ARR_Insp_Name")));
					val.moveToNext();
				}
				final Dialog dialog = new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar);
				dialog.getWindow().setContentView(R.layout.alert);
				ImageView btn_helpclose = (ImageView) dialog.findViewById(R.id.choose_close);
				LinearLayout hide=(LinearLayout) dialog.findViewById(R.id.maintable);
				final LinearLayout show=(LinearLayout) dialog.findViewById(R.id.choose_insp);
				hide.setVisibility(View.GONE);
				show.setVisibility(View.VISIBLE);
				Button btn_save = (Button) dialog.findViewById(R.id.choose_save);
				Button btn_cancel = (Button) dialog.findViewById(R.id.choose_cancel);
				final Spinner sp=(Spinner) dialog.findViewById(R.id.choose_sp);
				ArrayAdapter ad =new ArrayAdapter(this,android.R.layout.simple_spinner_item, inspection);
				ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				sp.setAdapter(ad);
				btn_helpclose.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						Declaration();
							dialog.cancel();
					}
					
				});
				btn_cancel.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						Declaration();
						dialog.cancel();
					}
					
				});
				btn_save.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						if(sp.getSelectedItem().toString().equals("-Select-"))
						{
							cf.ShowToast("Please select the Inspection Type", 0);
						}
						else
						{
							/**Start duplicating ****/
							Cursor val= cf.SelectTablefunction(cf.inspnamelist, " WHERE ARR_Insp_Name ='"+cf.encode(sp.getSelectedItem().toString())+"'");
							String insp_id="";
							if(val.getCount()>0)
							{
								val.moveToFirst();
								insp_id=val.getString(val.getColumnIndex("ARR_Custom_ID"));
								if(insp_id.equals("4"))
								{
									insp_id="4,5,6";
								}
							}
							if(val!=null)
							{
								val.close();
							}
							
							val= cf.SelectTablefunction(cf.Roof_additional, " WHERE  R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id in ("+insp_id+") ");
							if(val.getCount()>0)
							{
								cf.arr_db.execSQL(" INSERT INTO "+cf.Roof_additional+" (R_ADD_Ssrid,R_ADD_insp_id,R_ADD_full,R_ADD_scope,R_ADD_na,R_ADD_comment,R_ADD_citizen,R_ADD_WUWC,R_ADD_PDRUC,R_ADD_PN,R_ADD_PD,R_ADD_RME,R_ADD_HVAC_E,R_ADD_VEN_E,R_ADD_STOR_E,R_ADD_PIP,R_ADD_clean_E,R_ADD_ANT_P,R_ADD_LSE,R_ADD_O_E,R_ADD_RMEL,R_ADD_DRMS,R_ADD_other_I,R_RCT_na,R_ADD_saved) " +
										" Select R_ADD_Ssrid,'"+Roof_page_value[5]+"',R_ADD_full,R_ADD_scope,R_ADD_na,R_ADD_comment,R_ADD_citizen,R_ADD_WUWC,R_ADD_PDRUC,R_ADD_PN,R_ADD_PD,R_ADD_RME,R_ADD_HVAC_E,R_ADD_VEN_E,R_ADD_STOR_E,R_ADD_PIP,R_ADD_clean_E,R_ADD_ANT_P,R_ADD_LSE,R_ADD_O_E,R_ADD_RMEL,R_ADD_DRMS,R_ADD_other_I,R_RCT_na,'0' FROM "+cf.Roof_additional+"  WHERE  R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id in ("+insp_id+")" );
								
							}
							
							val= cf.SelectTablefunction(cf.Rct_table, " WHERE  RCT_Ssrid='"+cf.selectedhomeid+"' and RCT_insp_id in ("+insp_id+") ");
							if(val.getCount()>0)
							{
								cf.arr_db.execSQL(" INSERT INTO "+cf.Rct_table+" (RCT_Ssrid,RCT_insp_id,RCT_rct,RCT_PAD,RCT_yoi,RCT_npv,RCT_rsl,RCT_rsh,RCT_rst,RCT_bc,RCT_rc,RCT_arfl,RCT_pre,RCT_Rsh_per,RCT_other_rct,RCT_other_yoi,RCT_other_rsh,RCT_other_rst,RCT_other_aRFL,RCT_saved) " +
										" Select RCT_Ssrid,'"+Roof_page_value[5]+"',RCT_rct,RCT_PAD,RCT_yoi,RCT_npv,RCT_rsl,RCT_rsh,RCT_rst,RCT_bc,RCT_rc,RCT_arfl,RCT_pre,RCT_Rsh_per,RCT_other_rct,RCT_other_yoi,RCT_other_rsh,RCT_other_rst,RCT_other_aRFL,'0' FROM "+cf.Rct_table+"  WHERE  RCT_Ssrid='"+cf.selectedhomeid+"' and RCT_insp_id in ("+insp_id+")" );
								val.moveToFirst();
								for(int i=0;i<val.getCount();i++,val.moveToNext())
								{
									Cursor master=cf.SelectTablefunction(cf.Roof_master, " Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+Roof_page_value[5]+"' and RM_Covering='"+val.getString(val.getColumnIndex("RCT_rct"))+"' and RM_module_id='0'");
									if(master.getCount()<=0)
									{
										
										cf.arr_db.execSQL("INSERT INTO "+cf.Roof_master+" (RM_inspectorid,RM_srid,RM_insp_id,RM_module_id,RM_Covering) Values ('"+cf.Insp_id+"','"+cf.selectedhomeid+"','"+Roof_page_value[5]+"','0','"+val.getString(val.getColumnIndex("RCT_rct"))+"')");
										//System.out.println(" master sql=INSERT INTO "+cf.Roof_master+" (RM_inspectorid,RM_srid,RM_insp_id,RM_module_id,RM_Covering) Values ('"+cf.Insp_id+"','"+cf.selectedhomeid+"','"+cf.encode(insp)+"','0','"+cf.encode(cover)+"')");
									}
									cf.arr_db.execSQL("INSERT INTO "+cf.Roofcoverimages+" (RIM_masterid,RIM_module_id,RIM_Path,RIM_Order,RIM_Caption) SELECT (Select RM_masterid  as RIM_masterid from "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(Roof_page_value[5])+"' and RM_Enable='1' and RM_Covering='"+val.getString(val.getColumnIndex("RCT_rct"))+"' and RM_module_id='0'),RIM_module_id,RIM_Path,RIM_Order,RIM_Caption  From "+cf.Roofcoverimages+" " +
										"WHERE (RIM_masterid=(Select RM_masterid from "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id in ("+insp_id+") and RM_Enable='1' and RM_Covering='"+val.getString(val.getColumnIndex("RCT_rct"))+"' and RM_module_id='0' )) ");
									/*System.out.println(" the quesry"+"INSERT INTO "+cf.Roofcoverimages+" (RIM_masterid,RIM_module_id,RIM_Path,RIM_Order,RIM_Caption) SELECT (Select RM_masterid  as RIM_masterid from "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(Roof_page_value[5])+"' and RM_Enable='1' and RM_Covering='"+val.getString(val.getColumnIndex("RCT_rct"))+"' and RM_module_id='0'),RIM_module_id,RIM_Path,RIM_Order,RIM_Caption  From "+cf.Roofcoverimages+" " +
										"WHERE (RIM_masterid=(Select RM_masterid from "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id in ("+insp_id+") and RM_Enable='1' and RM_Covering='"+val.getString(val.getColumnIndex("RCT_rct"))+"' and RM_module_id='0' )) ");*/
								}
							}
							val= cf.SelectTablefunction(cf.RCN_table, " WHERE  R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id in ("+insp_id+") ");
							if(val.getCount()>0)
							{
								cf.arr_db.execSQL(" INSERT INTO "+cf.RCN_table+" (R_RCN_Ssrid,R_RCN_insp_id,R_RCN_OCR,R_RCN_RRN,R_RCN_VSL,R_RCN_VSTDS,R_RCN_VSCBS,R_RCN_VSDTS,R_RCN_VDRS,R_RCN_ODN,R_RCN_HRMS,R_RCN_FC,R_RCN_GRAC,R_RCN_RLN,R_RCN_RPN,R_RCN_HREBR,R_RCN_WORR,R_RCN_RMRA,R_RCN_SFPE,R_RCN_Amount,R_RCN_EVPWD,R_RCN_RME,R_RCN_BFP,R_RCN_ESM,R_RCN_SOSAS,R_RCN_DRS,R_RCN_na,R_RCN_saved) " +
										" Select R_RCN_Ssrid,'"+Roof_page_value[5]+"',R_RCN_OCR,R_RCN_RRN,R_RCN_VSL,R_RCN_VSTDS,R_RCN_VSCBS,R_RCN_VSDTS,R_RCN_VDRS,R_RCN_ODN,R_RCN_HRMS,R_RCN_FC,R_RCN_GRAC,R_RCN_RLN,R_RCN_RPN,R_RCN_HREBR,R_RCN_WORR,R_RCN_RMRA,R_RCN_SFPE,R_RCN_Amount,R_RCN_EVPWD,R_RCN_RME,R_RCN_BFP,R_RCN_ESM,R_RCN_SOSAS,R_RCN_DRS,R_RCN_na,'0' FROM "+cf.RCN_table+"  WHERE  R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id in ("+insp_id+")   " );
							}
							
							
							/*val= cf.SelectTablefunction(cf.RCN_table_dy, " WHERE  R_RCN_D_Ssrid='"+cf.selectedhomeid+"' and R_RCN_D_insp_id in ("+insp_id+") ");
							if(val.getCount()>0)
							{
								val.moveToFirst();
								for(int i=0;i<val.getCount();i++,val.moveToNext())
								{
									cf.arr_db.execSQL(" INSERT INTO "+cf.RCN_table_dy+" (R_RCN_D_Ssrid,R_RCN_D_insp_id,R_RCN_D_title,R_RCN_D_option,R_RCN_D_Other,R_RCN_D_saved) " +
										" Select R_RCN_D_Ssrid,'"+Roof_page_value[5]+"',R_RCN_D_title,R_RCN_D_option,R_RCN_D_Other,R_RCN_D_saved FROM "+cf.RCN_table_dy+"  WHERE  id='"+val.getString(val.getColumnIndex("id"))+"' " );
								}
							}
							*/
							if(val!=null)
							{
								val.close();
							}
							Declaration();
							/**End duplicating ****/
							dialog.dismiss();
						}
					}
					
				});
				
				dialog.setCancelable(false);
				dialog.show();
			}
			else
			{
				Declaration();
			}
			if(val!=null)
			{
				val.close();
			}
			
		}
		else
		{
			Declaration();
		}
			//inspection
			
			
			 
		}
		else
		{
			Declaration();
		}
		if(c!=null)
		{
			c.close();
		}
		if(c1!=null)
		{
			c1.close();
		}
		if(c2!=null)
		{
			c2.close();
		}
	}
	public void custom_creat()
			{
		
				 cf.loadinsp_n=cf.Roof;
				 Roof_page_value=getRoofValueForVariables(cf.Roof);
				 if(cf.Roof.equals(cf.Roof_insp[0]))
				 {
					
						    LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
							hdr_layout.addView(new HdrOnclickListener(this,1,cf.All_insp_list[1],"Roof System",2,0,cf));
				         	LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
				         	main_layout.setMinimumWidth(cf.wd-20);
				         	main_layout.addView(new MyOnclickListener(getApplicationContext(), 2, 0,0,cf));
						    LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
						    submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 21, 1,0,cf));
						    findViewById(R.id.sampleCitizenCertificate).setVisibility(View.VISIBLE);
						    findViewById(R.id.includeCitizenForm).setVisibility(View.VISIBLE);
						    findViewById(R.id.RC_additional_main).setVisibility(View.GONE);
						    ((TableRow)findViewById(R.id.Roof_RCN_TR8)).setVisibility(View.GONE);						    
						    
				}
				 else if(cf.Roof.equals(cf.Roof_insp[1]))
				 {
					
					
						    LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
							hdr_layout.addView(new HdrOnclickListener(this,1,cf.All_insp_list[2],"",29,2,cf));
				         	LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
				         	main_layout.setMinimumWidth(cf.wd);
				         	main_layout.addView(new MyOnclickListener(getApplicationContext(), 29, 0,0,cf));
						
				       ((Button) findViewById(R.id.GCH_RC_savebtn)).setText("Save");
				       ((LinearLayout) findViewById(R.id.contentrel)).setBackgroundColor(0);
				       LinearLayout.LayoutParams lp =new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
				       lp.setMargins(0, 0, 0, 0);
				       ((LinearLayout) findViewById(R.id.insidecontentrel)).setLayoutParams(lp);
				       ((LinearLayout) findViewById(R.id.Submenu_layout)).setLayoutParams(lp);
				       ((LinearLayout) findViewById(R.id.Submenu_layout)).setPadding(0, 0, 0,0);
				       ((LinearLayout) findViewById(R.id.typesubmenu)).setPadding(0, 0, 0,0);
				       findViewById(R.id.sampleCitizenCertificate).setVisibility(View.VISIBLE);
					    findViewById(R.id.includeCitizenForm).setVisibility(View.VISIBLE);
					    findViewById(R.id.submenu).setPadding(0, 0, 0, 0);
					    ((TextView)findViewById(R.id.roof_comments)).setText("Overall Roof Comments (Addendum)");
				 }
				 else  if(cf.Roof.equals(cf.Roof_insp[4]))
				 {
						    LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
							hdr_layout.addView(new HdrOnclickListener(this,1,cf.All_insp_list[7],"Roof System",4,0,cf));
				         	LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
				         	main_layout.setMinimumWidth(cf.wd-10);
				         	main_layout.addView(new MyOnclickListener(getApplicationContext(), 4, 0,0,cf));
				         	
					 	    LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
						    submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 43, 1,0,cf));
						    findViewById(R.id.RC_survay_head).setVisibility(View.GONE);
						    findViewById(R.id.RC_additional_main).setVisibility(View.GONE);
				/***	 For GCH Ends***/
				 }
				 else if(cf.Roof.equals(cf.getResourcesvalue(R.string.insp_5)))
				 {
					
					if(cf.identityval==33 || cf.identityval==34){ cf.typeidentity=313;}
				          
						    LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
						   hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","Commercial Type II","Roof System",3,1,cf));
					       
					       
				         	LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
				         	main_layout.setMinimumWidth(cf.wd-10);
				         	main_layout.addView(new MyOnclickListener(getApplicationContext(), 3, 0,0,cf));
				         	
				           cf.mainlayout= (LinearLayout) findViewById(R.id.insidecontentrel);
		      	        
					        
							
				          cf.type_submenulayout =(LinearLayout) findViewById(R.id.typesubmenu); //** WINDMIT TYPE SUBMENU **//
				          cf.type_submenulayout.addView(new MyOnclickListener(this, 300, 1,0, cf));
				          cf.windmittypechk(cf.identityval,((RadioButton)findViewById(R.id.b1802_chk)),((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((TextView)findViewById(R.id.commtxt)),((ImageView)findViewById(R.id.editbno))); // To check the checkbox of choosen inspection type
				          cf.uncheckcommercial(((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((RadioButton)findViewById(R.id.b1802_chk))); 
				      	   
				          cf.submenu_layout = (LinearLayout) findViewById(R.id.submenu);//** WINDMIT TYPE SUBMENU **//
				          cf.submenu_layout .addView(new MyOnclickListener(this,cf.identityval,1,cf.typeidentity, cf));
				          
				         
				         // cf.tblrw = (TableRow)findViewById(R.id.row2);
				        //  cf.tblrw.setMinimumHeight(cf.ht);	
				          cf.save = (Button)findViewById(R.id.GCH_RC_savebtn);
					
					 findViewById(R.id.Add_help).setVisibility(View.VISIBLE);
					 findViewById(R.id.RCN_help).setVisibility(View.VISIBLE);
					 /****commercial type 2 ends**/
				 } 
				 else if(cf.Roof.equals(cf.getResourcesvalue(R.string.insp_6)))
				 {
					 /****commercial type 3 satrts**/
					
						 if(cf.identityval==33 || cf.identityval==34){ cf.typeidentity=313;}
			          
						    LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
						     hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","Commercial Type III","Roof System",3,1,cf));
					        
							LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
				         	main_layout.setMinimumWidth(cf.wd-10);
				         	main_layout.addView(new MyOnclickListener(getApplicationContext(), 3, 0,0,cf));
				         
				          cf.type_submenulayout =(LinearLayout) findViewById(R.id.typesubmenu); //** WINDMIT TYPE SUBMENU **//
				          cf.type_submenulayout.addView(new MyOnclickListener(this, 300, 1,0, cf));
				          cf.windmittypechk(cf.identityval,((RadioButton)findViewById(R.id.b1802_chk)),((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((TextView)findViewById(R.id.commtxt)),((ImageView)findViewById(R.id.editbno))); // To check the checkbox of choosen inspection type
				          cf.uncheckcommercial(((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((RadioButton)findViewById(R.id.b1802_chk))); 
				      	   
				  		 cf.mainlayout= (LinearLayout) findViewById(R.id.insidecontentrel);
				          
				          cf.submenu_layout = (LinearLayout) findViewById(R.id.submenu);//** WINDMIT TYPE SUBMENU **//
				          cf.submenu_layout .addView(new MyOnclickListener(this,cf.identityval,1,cf.typeidentity, cf));
				          
				          
				         // cf.tblrw = (TableRow)findViewById(R.id.row2);
				         // cf.tblrw.setMinimumHeight(cf.ht);
				          cf.save = (Button)findViewById(R.id.GCH_RC_savebtn);
					
					 /****Roof Survay ends**/
					 findViewById(R.id.Add_help).setVisibility(View.VISIBLE);
					 findViewById(R.id.RCN_help).setVisibility(View.VISIBLE);
				 } 
				 else if(cf.Roof.equals(cf.getResourcesvalue(R.string.insp_4)))
				 {
					System.out.println("inside eeeeee");
						 System.out.println("cf.identityval"+cf.identityval);
						 if(cf.identityval==32 || cf.identityval==33 || cf.identityval==34){ cf.typeidentity=313;}
			          
						    LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
						    hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","Commercial Type I","Roof System",3,1,cf));
				
							LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
				         	main_layout.setMinimumWidth(cf.wd-10);
				         	main_layout.addView(new MyOnclickListener(getApplicationContext(), 3, 0,0,cf));
				          cf.mainlayout= (LinearLayout) findViewById(R.id.insidecontentrel);
				          
				          cf.type_submenulayout =(LinearLayout) findViewById(R.id.typesubmenu); //** WINDMIT TYPE SUBMENU **//
				          cf.type_submenulayout.addView(new MyOnclickListener(this, 300, 1,0, cf));
				          cf.windmittypechk(cf.identityval,((RadioButton)findViewById(R.id.b1802_chk)),((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((TextView)findViewById(R.id.commtxt)),((ImageView)findViewById(R.id.editbno))); // To check the checkbox of choosen inspection type
				          cf.uncheckcommercial(((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((RadioButton)findViewById(R.id.b1802_chk))); 
				      	  System.out.println("inside com11111");
				          cf.submenu_layout = (LinearLayout) findViewById(R.id.submenu);//** WINDMIT TYPE SUBMENU **//
				          cf.submenu_layout .addView(new MyOnclickListener(this,cf.identityval,1,cf.typeidentity, cf));
				          System.out.println("inside com1typeiden="+cf.typeidentity);
				          
				          cf.innersubmenu_layout = (LinearLayout) findViewById(R.id.innersubmenu);//** WINDMIT TYPE SUBMENU **//
				          System.out.println("innermae");
				          cf.innersubmenu_layout.addView(new MyOnclickListener(this,cf.identityval,1,144, cf));
					    
				          /*cf.tblrw = (TableRow)findViewById(R.id.row2);
				          cf.tblrw.setMinimumHeight(cf.ht);
				          cf.save = (Button)findViewById(R.id.GCH_RC_savebtn);*/
					 
					 /****commercial type 1 ends**/
				 } 
		       
					
					LinearLayout Li = (LinearLayout)findViewById(R.id.contentrel);
					Li.setMinimumHeight(cf.ht);
					final Calendar cal = Calendar.getInstance();
					mYear = cal.get(Calendar.YEAR);
					mMonth = cal.get(Calendar.MONTH);
					mDay = cal.get(Calendar.DAY_OF_MONTH);
					this.setTheme(R.style.AppTheme);
					cf.setupUI(((View)findViewById(R.id.contentrel)));
					try
					 {
						cf.CreateARRTable(431);
						 check_for_dublication();
					 }
					 catch (Exception e) {
						// TODO: handle exception
						 System.out.println("error "+e.getMessage());
						 Declaration();
					}
					
					System.out.println("comes ocrectly1");
			}
	private String[] getRoofValueForVariables(String Roof) {
				// TODO Auto-generated method stub
				String Roof_page_value[]=new String[7];
				cf.loadinsp_o="Overall Roof Commnets";
				Roof_page_value[0]="2";
				 Roof_page_value[1]="1";
				 Roof_page_value[2]="3";
				 
				if(Roof.equals(cf.Roof_insp[0]))
				 {
					/*** For 4poin***/
					 
					 Roof_page_value[3]=cf.RCT_tit;
					 Roof_page_value[4]="Other Damage Noted";
					 Roof_page_value[5]="1";
					 cf.loadinsp_q=cf.getResourcesvalue(R.string.four_1);
					 cf.loadinsp_n=cf.All_insp_list[1];
					 
					 /****For 4poin ends**/
			      }
				 else if(Roof.equals(cf.Roof_insp[1]))
				 {
					/*** Roof Survey***/
					
					 Roof_page_value[3]=cf.RCT_tit;
					 Roof_page_value[4]="Other Damage Noted";
					 Roof_page_value[5]="2";
					 cf.loadinsp_n=cf.All_insp_list[2];
					 cf.loadinsp_q=cf.getResourcesvalue(R.string.roof_1);
					
					 /****Roof Survay ends**/
					 
				 }
				 else  if(Roof.equals(cf.Roof_insp[4]))
				 {
						   
					 /*** For GCH***/
					 Roof_page_value[3]=cf.RCT_tit;
					 Roof_page_value[4]="Other Damage Noted";
					 cf.loadinsp_q=cf.getResourcesvalue(R.string.gch_3);
					 Roof_page_value[5]="7";
					
					// colomprerow=5;
					 cf.loadinsp_n=cf.All_insp_list[7];
					 /***	 For GCH Ends***/
				 }
				 else if(Roof.equals(cf.getResourcesvalue(R.string.insp_5)))
				 {
					
					/*** Roof Survey***/
					 Roof_page_value[3]=cf.RCT_tit;
					 Roof_page_value[4]="Is there any damage to the roof structure (Cuts,Holes,etc)?";
					 cf.loadinsp_q=cf.getResourcesvalue(R.string.com2_2);
					 Roof_page_value[5]="5";
					 
					 cf.loadinsp_n=cf.All_insp_list[5];
					 		 /****Roof Survay ends**/
				 } 
				 else if(Roof.equals(cf.getResourcesvalue(R.string.insp_6)))
				 {
					
					/*** Roof Survey***/
					 Roof_page_value[3]=cf.RCT_tit;
					 Roof_page_value[4]="Is there any damage to the roof structure (Cuts,Holes,etc)?";
					 cf.loadinsp_q=cf.getResourcesvalue(R.string.com2_2);
					 Roof_page_value[5]="6";
					
					 cf.loadinsp_n=cf.All_insp_list[6];
					  /****Roof Survay ends**/
				 }
				 else if (Roof.equals(cf.getResourcesvalue(R.string.insp_4)))
				 {
					
					/*** Roof Survey***/
					 Roof_page_value[3]=cf.RCT_tit;
					 Roof_page_value[4]="Other Damage Noted";//"Other Condition Present";
					 cf.loadinsp_q=cf.getResourcesvalue(R.string.com2_2);
					 Roof_page_value[5]="4";
					 cf.loadinsp_n=cf.All_insp_list[4];
				
					  /****Roof Survay ends**/
				 }
		        else 
				 {
					 /*** For GCH***/
					
					 Roof_page_value[3]=cf.RCT_tit;
					 Roof_page_value[4]="Other Condition Present";
					 cf.loadinsp_q=cf.getResourcesvalue(R.string.gch_3);
					 Roof_page_value[5]="7";
					 
				 }
				return Roof_page_value;
			}	
	private void Declaration()
	{
		arr_YOI=cf.yearbuilt;
		Chk_Scope[0]=(CheckBox) findViewById(R.id.RC_RSurvay_chk1);
		Chk_Scope[1]=(CheckBox) findViewById(R.id.RC_RSurvay_chk2);
		Chk_Scope[2]=(CheckBox) findViewById(R.id.RC_RSurvay_chk3);
		Chk_Scope[3]=(CheckBox) findViewById(R.id.RC_RSurvay_chk4);
		Chk_Scope[4]=(CheckBox) findViewById(R.id.RC_RSurvay_chk5);
		Chk_Scope[5]=(CheckBox) findViewById(R.id.RC_RSurvay_chk6);
		
		/**RCT Declaration**/
		Sp_RCT[0]=(Spinner) findViewById(R.id.Roof_RCT_sp_RCT);
		Sp_RCT[1]=(Spinner) findViewById(R.id.Roof_RCT_sp_YOI);
		Sp_RCT[2]=(Spinner) findViewById(R.id.Roof_RCT_sp_RS);
		Sp_RCT[3]=(Spinner) findViewById(R.id.Roof_RCT_sp_RSH);
		Sp_RCT[4]=(Spinner) findViewById(R.id.Roof_RCT_sp_RST);
		Sp_RCT[5]=(Spinner) findViewById(R.id.Roof_RCT_sp_BC);
		Sp_RCT[6]=(Spinner) findViewById(R.id.Roof_RCT_sp_RC);
		Sp_RCT[7]=(Spinner) findViewById(R.id.Roof_RCT_sp_ARL);
		set_value_spinner(Sp_RCT[0],arr_RCT);
		set_value_spinner(Sp_RCT[1],arr_YOI);
		set_value_spinner(Sp_RCT[2],arr_RS);
		set_value_spinner(Sp_RCT[3],arr_RSH);
		set_value_spinner(Sp_RCT[4],arr_RST);
		set_value_spinner(Sp_RCT[5],arr_BC);
		set_value_spinner(Sp_RCT[6],arr_RC);
		set_value_spinner(Sp_RCT[7],arr_ARL);
		RCT_other[0]=(EditText) findViewById(R.id.Roof_RCT_other_RCT);
		RCT_other[1]=(EditText) findViewById(R.id.Roof_RCT_other_YOI);
		RCT_other[2]=(EditText) findViewById(R.id.Roof_RCT_other_RSH);
		RCT_other[3]=(EditText) findViewById(R.id.Roof_RCT_other_RST);
		RCT_other[4]=(EditText) findViewById(R.id.Roof_RCT_other_ARL);
		RCT_ed[0]=(EditText) findViewById(R.id.Roof_RCT_ED_PAD);
		RCT_ed[1]=(EditText) findViewById(R.id.Roof_RCT_ED_RSH);
		RCT_chk[0]=(CheckBox) findViewById(R.id.Roof_RCT_CHK_PVC);
		RCT_chk[1]=(CheckBox) findViewById(R.id.Roof_RCT_CHK_PRE);
		RCT_bt[0]=(Button) findViewById(R.id.Roof_RCT_BT_PAD);
		RCT_bt[1]=(Button) findViewById(R.id.Roof_RCT_BT_upload);
		tv=(TextView) findViewById(R.id.Roof_RCT_TV_upload);
		
		Sp_RCT[0].setOnItemSelectedListener(new spiner_lis(Sp_RCT[0],RCT_other[0]));
		Sp_RCT[1].setOnItemSelectedListener(new spiner_lis(Sp_RCT[1],RCT_other[1]));
		Sp_RCT[2].setOnItemSelectedListener(new Spin_Selectedlistener());
		Sp_RCT[3].setOnItemSelectedListener(new spiner_lis(Sp_RCT[3],RCT_other[2],RCT_ed[1]));
		Sp_RCT[4].setOnItemSelectedListener(new spiner_lis(Sp_RCT[4],RCT_other[3]));
		Sp_RCT[7].setOnItemSelectedListener(new spiner_lis(Sp_RCT[7],RCT_other[4]));
		tv.setText(Html.fromHtml("<u>View/Edit Photos</u>"));
		
		RCT_ed[0].addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(!s.toString().equals("") && !s.toString().equals("N/A"))
				{
					if(compare_schedule(s.toString()))
					{
					String[] val=s.toString().split("/");
					if(val[2]!=null)
					{
						sp_setvalues(Sp_RCT[1],val[2],RCT_other[1]);
						//Sp_RCT[1].setEnabled(false);
						//RCT_other[1].setEnabled(false);
					}
					}
					else
					{
						RCT_ed[0].setText("");
						cf.ShowToast("Permit application date should not be greater than Inspection date", 1);
					}
				}
				else
				{
					Sp_RCT[1].setEnabled(true);
					RCT_other[1].setEnabled(true);
				}
			}

			private boolean compare_schedule(String s) {
				// TODO Auto-generated method stub
				try
				{
				cf.CreateARRTable(3);
				Cursor c =cf.SelectTablefunction(cf.policyholder, " WHERE ARR_PH_SRID='"+cf.selectedhomeid+"'");
				
				if(c.getCount()>0)
				{
				c.moveToFirst();
					String sch=cf.decode(c.getString(c.getColumnIndex("ARR_Schedule_ScheduledDate")));
					if(!sch.equals(""))
					{
						String formatString = "MM/dd/yyyy";
						SimpleDateFormat df = new SimpleDateFormat(formatString);
						//df.parse(sch);//
						Date d=new Date(sch);
						Date sel=new Date(s);
						System.out.println("the scheduyle="+sel);
						System.out.println(d.compareTo(sel));
						if(d.compareTo(sel)>=0)
						{
							return true;
						}
						else
							return false;
					}else
						return true;
					
				}
				else
					return true;
				//return false;
				}catch (Exception e) {
					// TODO: handle exception
					System.out.println("theissue"+e.getMessage());
					return false;
							
				}
			}
		});
		RCT_chk[0].setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(RCT_chk[0].isChecked())
				{
					RCT_ed[0].setText("N/A");
					//RCT_bt[0].setVisibility(View.INVISIBLE);
					//sp_setvalues(Sp_RCT[1],"Unknown",RCT_other[1]);
					//Sp_RCT[1].setEnabled(false);
					//RCT_other[1].setEnabled(false);
					//RCT_other[1].setSelection()
				}
				else
				{
					RCT_ed[0].setText("");
					//RCT_bt[0].setVisibility(View.VISIBLE);
					//Sp_RCT[1].setSelection(0);
				}
			}
		});
		RCT_chk[1].setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(RCT_chk[1].isChecked())
				{
					if(RCT_pre_m)
					{
						final AlertDialog.Builder ab =new AlertDialog.Builder(Roof_section_common.this);
						ab.setTitle("Add Predominant");
						ab.setMessage("Already selected in a Roof Covering Type. Do you want to change this?");
						ab.setIcon(R.drawable.alertmsg);
						ab.setCancelable(false);
						ab.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								ab.setCancelable(true);
								dialog.dismiss();
							}
						})
						.setNegativeButton("No", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								RCT_chk[1].setChecked(false);
								ab.setCancelable(true);
								dialog.dismiss();
								
							}
						});
						AlertDialog al=ab.create();
						al.show();
					}
				}
				
			}
		});
		RCT_bt[0].setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				RCT_ed[0].setText("");
				showDialogDate(RCT_ed[0]);
			
			}
		});
		RCT_bt[1].setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent in=new Intent(Roof_section_common.this,RoofImagePicking.class);
			    in.putExtra("roof_cover_img", true);
			    String cv="";
			    try
			    {
			    	cv=Sp_RCT[0].getSelectedItem().toString().trim();
			    
			   
			    if(!cv.equals("-Select-"))
			    {
				    in.putExtra("Cover", cv);
				    in.putExtra("Insp_id",Roof_page_value[5]);
				    in.putExtra("SRID",cf.selectedhomeid);
				    startActivityForResult(in, pick_img_code);
			    }
			    else
			    {
			    	cf.ShowToast("Please select Roof Covering Type ", 0);
			    }
			    }
			    catch (Exception e) {
					// TODO: handle exception
				}
			}
			
		});
		tv.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent in=new Intent(Roof_section_common.this,RoofImagePicking.class);
			    in.putExtra("roof_cover_img", true);
			    String cv="";
			    try
			    {
			    	cv=Sp_RCT[0].getSelectedItem().toString().trim();
			    
			   
			    if(!cv.equals("-Select-"))
			    {
				    in.putExtra("Cover", cv);
				    in.putExtra("Insp_id",Roof_page_value[5]);
				    in.putExtra("SRID",cf.selectedhomeid);
				    startActivityForResult(in, pick_img_code);
			    }
			    else
			    {
			    	cf.ShowToast("Please select Roof Covering Type ", 0);
			    }
			    }
			    catch (Exception e) {
					// TODO: handle exception
				}
			}
		});
		/**RCT Declaration ends**/
		
		/****Additional roof section***/
		add_rg=(RadioGroup) findViewById(R.id.ADD_RG);
		/***Additional roof section ends***/
		
		/***Roof condition noted**/
		RCN_RG[0]=(RadioGroup) findViewById(R.id.Roof_RCN_RG_OCR);
		RCN_RG[1]=(RadioGroup) findViewById(R.id.Roof_RCN_RG_RRN);
		RCN_RG[2]=(RadioGroup) findViewById(R.id.Roof_RCN_RG_VSL);
		RCN_RG[3]=(RadioGroup) findViewById(R.id.Roof_RCN_RG_VSTDS);
		RCN_RG[4]=(RadioGroup) findViewById(R.id.Roof_RCN_RG_VSCBS);
		RCN_RG[5]=(RadioGroup) findViewById(R.id.Roof_RCN_RG_VSDTS);
		RCN_RG[6]=(RadioGroup) findViewById(R.id.Roof_RCN_RG_VDRS);
		RCN_RG[7]=(RadioGroup) findViewById(R.id.Roof_RCN_RG_ODN);
		
		RCN_other[0]=(EditText) findViewById(R.id.Roof_RCN_ED_OCR);
		RCN_other[1]=(EditText) findViewById(R.id.Roof_RCN_ED_RRN);
		RCN_other[2]=(EditText) findViewById(R.id.Roof_RCN_ED_VSL);
		RCN_other[3]=(EditText) findViewById(R.id.Roof_RCN_ED_VSTDS);
		RCN_other[4]=(EditText) findViewById(R.id.Roof_RCN_ED_VSCBS);
		RCN_other[5]=(EditText) findViewById(R.id.Roof_RCN_ED_VSDTS);
		RCN_other[6]=(EditText) findViewById(R.id.Roof_RCN_ED_VDRS);
		RCN_other[7]=(EditText) findViewById(R.id.Roof_RCN_ED_ODN);
		
		RCN_tv_limit[0]=(TextView)findViewById(R.id.Roof_RCN_TVL_OCR);
		RCN_tv_limit[1]=(TextView) findViewById(R.id.Roof_RCN_TVL_RRN);
		RCN_tv_limit[2]=(TextView) findViewById(R.id.Roof_RCN_TVL_VSL);
		RCN_tv_limit[3]=(TextView) findViewById(R.id.Roof_RCN_TVL_VSTDS);
		RCN_tv_limit[4]=(TextView) findViewById(R.id.Roof_RCN_TVL_VSCBS);
		RCN_tv_limit[5]=(TextView) findViewById(R.id.Roof_RCN_TVL_VSDTS);
		RCN_tv_limit[6]=(TextView) findViewById(R.id.Roof_RCN_TVL_VDRS);
		RCN_tv_limit[7]=(TextView) findViewById(R.id.Roof_RCN_TVL_ODN);
		
		for (int i=0;i<RCN_RG.length;i++){
			if(i==0)
			{
				RCN_RG[i].setOnCheckedChangeListener(new RCN_click(RCN_other[i],RCN_tv_limit[i],"Poor"));	
			}
			else{
				RCN_RG[i].setOnCheckedChangeListener(new RCN_click(RCN_other[i],RCN_tv_limit[i],"Yes"));
			}
		}
		RCN_other[0].addTextChangedListener(new TextWatcher() {

			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				cf.showing_limit(s.toString(), RCN_tv_limit[0], 250);
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		RCN_RG[0].setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			
			

				@Override
				public void onCheckedChanged(RadioGroup group, int checkedId) {
					// TODO Auto-generated method stub
					if(((RadioButton)group.findViewWithTag("Poor")).isChecked())
					{
						
						RCN_other[0].setVisibility(View.VISIBLE);
						RCN_tv_limit[0].setVisibility(View.VISIBLE);
						
						
					}
					else
					{
						RCN_other[0].setText("");
						RCN_other[0].setVisibility(View.GONE);
						RCN_tv_limit[0].setVisibility(View.GONE);
					}
					try
					{
					if(((RadioButton)group.findViewWithTag("Poor")).isChecked()||((RadioButton)group.findViewWithTag("Fair")).isChecked())
					{
						for (int i=1;i<RCN_RG.length;i++){
							((RadioButton)RCN_RG[i].findViewWithTag("BSI")).setChecked(true);						
						}
					}
					if(((RadioButton)group.findViewWithTag("Excellent")).isChecked()||((RadioButton)group.findViewWithTag("Good")).isChecked())
					{
						for (int i=1;i<RCN_RG.length;i++){
							((RadioButton)RCN_RG[i].findViewWithTag("No")).setChecked(true);						
						}
					}
					}catch (Exception e) {
						// TODO: handle exception
					}
				}
			});
		

		
		ADD_chk_top =(CheckBox) findViewById(R.id.Add_chk_top);
		 ADD_layout_head =(LinearLayout) findViewById(R.id.ADD_layou_head);
		ADD_expend=(ImageView) findViewById(R.id.ADD_expend);
		RCN_chk_top =(CheckBox) findViewById(R.id.RCN_chk_top);
		RCN_layout_head =(LinearLayout) findViewById(R.id.RCN_layou_head);
	 	ADD_expend.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String tag=v.getTag().toString();
				if(tag.equals("plus"))
				{
					if(!ADD_chk_top.isChecked())
					{
						ADD_layout_head.setVisibility(View.VISIBLE);
						ADD_expend.setTag("minus");
						ADD_expend.setImageDrawable(getResources().getDrawable(R.drawable.minus));
						if(findViewById(R.id.RCN_comp).getVisibility()!=View.VISIBLE)
						{
							RCN_expend.setTag("plus");
							RCN_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
							RCN_layout_head.setVisibility(View.GONE);	
						}
						
					}
				}
				else
				{
					ADD_layout_head.setVisibility(View.GONE);
					ADD_expend.setTag("plus");
					ADD_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
				}
			}
		});
	 	RCN_expend=(ImageView) findViewById(R.id.RCN_expend);
	 	RCN_expend.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String tag=v.getTag().toString();
				if(tag.equals("plus"))
				{
					if(!RCN_chk_top.isChecked())
					{
						RCN_layout_head.setVisibility(View.VISIBLE);
						RCN_expend.setTag("minus");
						RCN_expend.setImageDrawable(getResources().getDrawable(R.drawable.minus));
						if(findViewById(R.id.ADD_comp).getVisibility()!=View.VISIBLE)
						{
							ADD_expend.setTag("plus");
							ADD_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
							ADD_layout_head.setVisibility(View.GONE);	
						}
					}
				}
				else
				{
					RCN_layout_head.setVisibility(View.GONE);
					RCN_expend.setTag("plus");
					RCN_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
				}
			}
		});
	 	
	 	
	 	ADD_chk_top.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				if(ADD_chk_top.isChecked())
				{
				
					Cursor c =cf.SelectTablefunction(cf.Roof_additional, " WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+Roof_page_value[5]+"'");
					c.moveToFirst();
					if(c.getCount()>0)
					{
						if(!c.getString(c.getColumnIndex("R_ADD_full")).equals(""))
						{
						
						final AlertDialog.Builder ab =new AlertDialog.Builder(Roof_section_common.this);
						ab.setTitle("Confirmation");
						ab.setMessage(Html.fromHtml("Do you want to clear the Additional Roof Details saved data?"));
						ab.setIcon(R.drawable.alertmsg);
						ab.setCancelable(false);
						ab.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								cf.arr_db.execSQL(" UPDATE "+cf.Roof_additional+" SET R_ADD_full='' WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+Roof_page_value[5]+"'");
								
								/*cf.ShowToast("Roof Condition Noted question deleted successfully ", 0);
								TableLayout tbl_dy=((TableLayout)findViewById(R.id.RCN_Condition_N_DY_tbl));
								tbl_dy.removeView(tbl_dy.findViewById(id));*/
								add_rg.clearCheck();
								ADD_expend.setTag("plus");
		         				ADD_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
								ADD_layout_head.setVisibility(View.GONE);
								
								//Show_dynamic_RCN();
							}
							
						})
						.setNegativeButton("No", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								ADD_chk_top.setChecked(false);
							}
						});
						AlertDialog al=ab.create();
						al.show();
						}
						else
						{	

							add_rg.clearCheck();
							ADD_expend.setTag("plus");
	         				ADD_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
							ADD_layout_head.setVisibility(View.GONE);
						}
					}
					else
					{	

						add_rg.clearCheck();
						ADD_expend.setTag("plus");
         				ADD_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
						ADD_layout_head.setVisibility(View.GONE);
					}
					if(findViewById(R.id.RCN_comp).getVisibility()!=View.VISIBLE)
					{
						RCN_layout_head.setVisibility(View.GONE);
						RCN_expend.setTag("plus");
         				RCN_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
					}
					
				}
				else
				{
					
					ADD_expend.setTag("minus");
     				ADD_expend.setImageDrawable(getResources().getDrawable(R.drawable.minus));
					ADD_layout_head.setVisibility(View.VISIBLE);
					findViewById(R.id.ADD_comp).setVisibility(View.INVISIBLE);
					if(findViewById(R.id.RCN_comp).getVisibility()!=View.VISIBLE)
					{
						RCN_layout_head.setVisibility(View.GONE);
						RCN_expend.setTag("plus");
         				RCN_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
					}
					
				}
				ADD_chk_top.setFocusableInTouchMode(true);
				ADD_chk_top.requestFocus();
				ADD_chk_top.setFocusableInTouchMode(false);
				ADD_chk_top.setFocusable(false);
				ADD_chk_top.setFocusable(true);
				
			}
		});
	 	
	 	RCN_chk_top.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(RCN_chk_top.isChecked())
				{
					Cursor c =cf.SelectTablefunction(cf.RCN_table, " WHERE R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id='"+Roof_page_value[5]+"'");
					if(c.getCount()>0)
					{
						c.moveToFirst();
						if(!cf.decode(c.getString(c.getColumnIndex("R_RCN_OCR"))).equals(""))
						{
							final AlertDialog.Builder ab =new AlertDialog.Builder(Roof_section_common.this);
							ab.setTitle("Confirmation");
							ab.setMessage(Html.fromHtml("Do you want to clear the Roof Conditions Noted saved data?"));
							ab.setIcon(R.drawable.alertmsg);
							ab.setCancelable(false);
							ab.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
								
								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									cf.arr_db.execSQL(" DELETE FROM "+cf.RCN_table_dy+" WHERE R_RCN_D_Ssrid='"+cf.selectedhomeid+"' and R_RCN_D_insp_id='"+Roof_page_value[5]+"'");
									cf.arr_db.execSQL(" DELETE FROM "+cf.RCN_table+" WHERE R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id='"+Roof_page_value[5]+"'");
									/*cf.ShowToast("Roof Condition Noted question deleted successfully ", 0);
									TableLayout tbl_dy=((TableLayout)findViewById(R.id.RCN_Condition_N_DY_tbl));
									tbl_dy.removeView(tbl_dy.findViewById(id));*/
									clear_RCN();
									RCN_expend.setTag("plus");
			         				RCN_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
									RCN_layout_head.setVisibility(View.GONE);
									
									//Show_dynamic_RCN();
								}
								
							})
							.setNegativeButton("No", new DialogInterface.OnClickListener() {
								
								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									RCN_chk_top.setChecked(false);
								}
							});
							AlertDialog al=ab.create();
							al.show();
						}
						else
						{
							clear_RCN();
							RCN_expend.setTag("plus");
	         				RCN_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
							RCN_layout_head.setVisibility(View.GONE);
						}
						/*if(findViewById(R.id.ADD_comp).getVisibility()!=View.VISIBLE)
						{
							ADD_expend.setTag("plus");
	         				ADD_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
							ADD_layout_head.setVisibility(View.GONE);	
						}*/
					}
					else
					{
						clear_RCN();
						RCN_expend.setTag("plus");
         				RCN_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
						RCN_layout_head.setVisibility(View.GONE);
					}
					
				}
				else
				{
					
					RCN_expend.setTag("minus");
     				RCN_expend.setImageDrawable(getResources().getDrawable(R.drawable.minus));
					RCN_layout_head.setVisibility(View.VISIBLE);
					if(findViewById(R.id.ADD_comp).getVisibility()!=View.VISIBLE)
					{
						ADD_expend.setTag("plus");
         				ADD_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
						ADD_layout_head.setVisibility(View.GONE);	
					}
					findViewById(R.id.RCN_comp).setVisibility(View.INVISIBLE);
					
				}
				
				RCN_chk_top.setFocusableInTouchMode(true);
				RCN_chk_top.requestFocus();
			    RCN_chk_top.setFocusableInTouchMode(false);
			    RCN_chk_top.setFocusable(false);
				RCN_chk_top.setFocusable(true);
			}

			
		});
		RCT_chk_top=(CheckBox)findViewById(R.id.RCT_chk_top);
	 	RCT_chk_top.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(RCT_chk_top.isChecked())
				{
					Cursor c =cf.SelectTablefunction(cf.Rct_table, " WHERE RCT_Ssrid='"+cf.selectedhomeid+"' and RCT_insp_id='"+Roof_page_value[5]+"'");
					if(c.getCount()>0)
					{
						final AlertDialog.Builder ab =new AlertDialog.Builder(Roof_section_common.this);
						ab.setTitle("Confirmation");
						ab.setMessage(Html.fromHtml("Do you want to clear the Roof Covering Type saved data?"));
						ab.setIcon(R.drawable.alertmsg);
						ab.setCancelable(false);
						ab.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								cf.arr_db.execSQL(" DELETE FROM "+cf.Rct_table+" WHERE RCT_Ssrid='"+cf.selectedhomeid+"' and RCT_insp_id='"+Roof_page_value[5]+"'");
								cf.arr_db.execSQL(" DELETE FROM "+cf.Roofcoverimages+" Where RIM_masterid=(Select RM_masterid from "+cf.Roof_master+
										" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+Roof_page_value[5]+"')");
								/*cf.ShowToast("Roof Condition Noted question deleted successfully ", 0);
								TableLayout tbl_dy=((TableLayout)findViewById(R.id.RCN_Condition_N_DY_tbl));
								tbl_dy.removeView(tbl_dy.findViewById(id));*/
								clear_RCT();
								findViewById(R.id.RCT_content).setVisibility(View.GONE);
								show_RCT_values();
								
								//Show_dynamic_RCN();
							}
							
						})
						.setNegativeButton("No", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								RCT_chk_top.setChecked(false);
							}
						});
						AlertDialog al=ab.create();
						al.show();
					}
					else
					{
						clear_RCT();
						findViewById(R.id.RCT_content).setVisibility(View.GONE);
					}
					/*if(findViewById(R.id.ADD_comp).getVisibility()!=View.VISIBLE)
					{
						clear_RCT();
						findViewById(R.id.RCT_content).setVisibility(View.GONE);	
					}*/
					
				}
				else
				{
					
					findViewById(R.id.RCT_content).setVisibility(View.VISIBLE);
					findViewById(R.id.RCT_comp).setVisibility(View.INVISIBLE);
					
				}
				
			
			}
		});
		/***Roof condition noted ends**/
		RC_cmt=(EditText) findViewById(R.id.GCH_RC_ED_Li_Comments);
		 LinearLayout.LayoutParams li=(LinearLayout.LayoutParams) RC_cmt.getLayoutParams();
	       li.width=cf.wd-90;
	       RC_cmt.setLayoutParams(li);
		RC_cmt.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				cf.showing_limit(s.toString(), ((TextView) findViewById(R.id.GCH_RC_ED_txt_Comments)), 500);
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		show_RCT_values();
		Additional_show_value();
		Show_RCN_values();
		Show_generalvalue();
		
	}
	public void showDialogDate(EditText edt) {
		// TODO Auto-generated method stub
	    getCalender();
		Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialog= new DatePickerDialog(Roof_section_common.this, new mDateSetListener(edt),mYear, mMonth, mDay);
        dialog.show();
	}
	public void getCalender() {
		// TODO Auto-generated method stub
		final Calendar c = Calendar.getInstance();
			mYear = c.get(Calendar.YEAR);
			mMonth = c.get(Calendar.MONTH);
			mDay = c.get(Calendar.DAY_OF_MONTH);
			
			mDayofWeek = c.get(Calendar.DAY_OF_WEEK);
			hours = c.get(Calendar.HOUR);
			minutes = c.get(Calendar.MINUTE);
			seconds = c.get(Calendar.SECOND);
			amorpm = c.get(Calendar.AM_PM);	
	}
	class mDateSetListener implements DatePickerDialog.OnDateSetListener
	{
		EditText v;
		mDateSetListener(EditText v)
		{
			this.v=v;
		}
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			// TODO Auto-generated method stub
			getCalender();
			mYear = year;
			mMonth = monthOfYear+1;
			mDay = dayOfMonth;
			selmDayofWeek = mDayofWeek ;
			
			v.setText(new StringBuilder()
					// Month is 0 based so add 1
					.append(cf.pad(mMonth)).append("/").append(cf.pad(mDay)).append("/")
					.append(mYear).append(" "));
			if(!RCT_ed[0].getText().toString().equals("N/A") && !RCT_ed[0].getText().toString().equals(""))
			{
				RCT_chk[0].setChecked(false);
			}
			
		}
		
	}
	private void Show_generalvalue() {
		// TODO Auto-generated method stub
		Cursor c = cf.SelectTablefunction(cf.Roof_additional, " WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+Roof_page_value[5]+"' ");
		if(c.getCount()>0)
		{
			c.moveToFirst();
			String Scope=cf.decode(c.getString(c.getColumnIndex("R_ADD_scope"))),
				  comment=cf.decode(c.getString(c.getColumnIndex("R_ADD_comment"))),
				  Na=cf.decode(c.getString(c.getColumnIndex("R_ADD_na")));
			if(!Scope.equals(""))
			{
				for(int i =0;i<Chk_Scope.length;i++)
				{
					if(Scope.contains(Chk_Scope[i].getText().toString().trim()))
					{
						Chk_Scope[i].setChecked(true);
					}
				}
			}
			if(comment.equals(""))
			{
				RC_cmt.setText("No comments.");
			}
			else
			{
				RC_cmt.setText(comment);	
			}
			
			if(Na.equals("true"))
			{
				ADD_chk_top.setChecked(true);
				add_rg.clearCheck();
				ADD_expend.setTag("plus");
 				ADD_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
				ADD_layout_head.setVisibility(View.GONE);
			}
			if(c.getString(c.getColumnIndex("R_RCT_na")).equals("true"))
			{
				RCT_chk_top.setChecked(true);
				findViewById(R.id.RCT_content).setVisibility(View.GONE);
				findViewById(R.id.RCT_comp).setVisibility(View.VISIBLE);
			
			}
			if((c.getString(c.getColumnIndex("R_ADD_saved")).equals("1") || c.getString(c.getColumnIndex("R_ADD_saved")).equals("2")) && (!c.getString(c.getColumnIndex("R_ADD_full")).equals("") || c.getString(c.getColumnIndex("R_ADD_na")).equals("true")))
			{
				findViewById(R.id.ADD_comp).setVisibility(View.VISIBLE);
			}
			else
			{
				findViewById(R.id.ADD_comp).setVisibility(View.INVISIBLE);
			}
			if(c.getString(c.getColumnIndex("R_ADD_citizen")).equals("true"))
			{
				((CheckBox)findViewById(R.id.includeCitizenForm)).setChecked(true);
			}
			if(c.getString(c.getColumnIndex("R_RCT_na")).equals("true") && c.getString(c.getColumnIndex("R_ADD_saved")).equals("1"))
			{
				findViewById(R.id.RCT_comp).setVisibility(View.VISIBLE);
			}
			
			
		}
		else
		{
			/***Start populate the scope of inspection from the order inspection**/
			cf.getBIGeneraldata(cf.selectedhomeid);
			Log.d("DEFAULT", cf.ScopeofInspection);
			if(!cf.ScopeofInspection.equals(""))
			{
				String arr[]=cf.ScopeofInspection.split(",");
				Log.d("DEFAULT", "length="+arr.length);
				for(int i=0;i<arr.length;i++)
				{
					for(int j=0;j<Chk_Scope.length;j++)
					{
						if(arr[i].trim().toLowerCase().equals(Chk_Scope[j].getText().toString().trim().toLowerCase()))
						{
							Chk_Scope[j].setChecked(true);
						}
						/*if(arr[i].trim().contains("Other("))
						{
							
						}*/
					}
					
				}
			}
			else
			{
				if(cf.Roof.equals(cf.Roof_insp[0]))
				{
					Chk_Scope[2].setChecked(true);
					Chk_Scope[5].setChecked(true);	
				}
			}			
		}
		c=cf.SelectTablefunction(cf.RCN_table, " WHERE R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id='"+Roof_page_value[5]+"' ");
		if(c.getCount()>0)
		{
			c.moveToFirst();
			if(c.getString(c.getColumnIndex("R_RCN_saved")).equals("1"))
			{
				findViewById(R.id.RCN_comp).setVisibility(View.VISIBLE);
			}
			if(c.getString(c.getColumnIndex("R_RCN_na")).equals("true"))
			{
				RCN_chk_top.setChecked(true);
				clear_RCN();
				RCN_expend.setTag("plus");
 				RCN_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
				RCN_layout_head.setVisibility(View.GONE);
			}
			
		}
	}
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.sampleCitizenCertificate:
			Intent in=new Intent(this,CitizenCertification.class);
			cf.putExtraIntent(in);
			in.putExtra("insp_id", Roof_page_value[5]);
			startActivityForResult(in, 10);
			break;
		case R.id.loadcomments:
			/***Call for the comments***/
			getRoofValueForVariables(cf.Roof);
			int len=RC_cmt.getText().toString().length();
			
				if(load_comment )
				{
					load_comment=false;
					int loc[] = new int[2];
					v.getLocationOnScreen(loc);
					Intent in1 = cf.loadComments("Overall Roof Comments",loc);
					in1.putExtra("length", len);
					in1.putExtra("max_length", 500);
					startActivityForResult(in1, cf.loadcomment_code);
				}
		break;
		case R.id.Add_help:
			cf.ShowToast("Complete for all Wind Mitigation Inspection. ", 0);
		break;
		case R.id.RCN_help:
			cf.ShowToast("Complete for all Wind Mitigation Inspection. ", 0);
		break;
		case R.id.abbrev:
			Intent s= new Intent(this,PolicyholdeInfoHead.class);
			s.putExtra("homeid", cf.selectedhomeid);
			s.putExtra("Type", "Abbreviation");
			s.putExtra("insp_id", cf.Insp_id);
			startActivityForResult(s, cf.info_requestcode);
		break;
		case R.id.Roof_RCT_SA:
			RCT_validation();
		break;
		case R.id.Roof_RCT_clear:
			clear_RCT();
		break;
		/*case R.id.ADD_save:
			Additional_validation();
			break;*/
		case R.id.RC_AMC:
			Add_more_condition();
		break;
		/*case R.id.RCN_save:
			RCN_validation();
		break;*/
		case R.id.GCH_RC_savebtn:
			roofvaldiation();
			//over_all_validation();
		break;
		case R.id.GCH_RC_homebtn:
			cf.gohome();
		break;
		
		default:
			break;
		}
	}
	private void roofvaldiation() {
		// TODO Auto-generated method stub
		String scope="";
		String comments="";
		for(int i=0;i<Chk_Scope.length;i++ )
		{
			if(Chk_Scope[i].isChecked())
			{
				scope+=Chk_Scope[i].getText().toString().trim()+",";
			}
		}
		if((!scope.equals("") || Roof_page_value[5].equals("7"))  )
		{
			Cursor c =cf.SelectTablefunction(cf.Rct_table, " WHERE RCT_Ssrid='"+cf.selectedhomeid+"' and RCT_insp_id='"+Roof_page_value[5]+"' ");
			if(c.getCount()>0 || RCT_chk_top.isChecked())
			{
			if(RCT_total_per==0 || RCT_chk_top.isChecked())
			{
				if(RCT_pre_m || RCT_chk_top.isChecked())
				{
					//if(ADD_chk_top.isChecked() || Roof_page_value[5].equals("7"))
					if(ADD_chk_top.isChecked())
					{
						RCNValdiation();
					}
					else
					{
						String s=(add_rg.getCheckedRadioButtonId()!=-1)? add_rg.findViewById(add_rg.getCheckedRadioButtonId()).getTag().toString():"";
						if(s.equals(""))
						{
							cf.ShowToast("Please select value for Full leakage review part of inspection under Additional Roof Details.", 0);
						}
						else
						{
							RCNValdiation();
						}
					}
				
				}else
				{
					cf.ShowToast("Please select predominant for any one Roof Covering Types", 0);
				}
			}else
			{
				cf.ShowToast("Sum of Roof Shape (%) percentage should be 100. in Roof Covering Types", 0);
			}	
			}else
			{
				cf.ShowToast("Please save atleast one Roof Covering Types", 0);
			}
		}
		else
		{
			cf.ShowToast("Please select value for "+getResources().getString(R.string.roof_add_4poin_txt2), 0);
		}
	}
	private void RCNValdiation() {
		// TODO Auto-generated method stub
		if(RCN_chk_top.isChecked())
		{
			save_RCN();
		}
		else
		{
			String[] validation_txt={"Overall Condition of Roof","Recent Repairs Noted","Visible Signs of Leakage","Visible Signs of Torn/Damaged Shingles/Roof Membrane","Visible Signs of Curling/Brittle Shingles/Roof Membrane","Visible Signs of Damaged Tiling/Slates/Metal Roofing","Visible Damage to Roof Structure/Sheathing","Other Damage Noted"}; 
			for(int i=0;i< RCN_RG.length;i++)
			{
				if(RCN_RG[i].getCheckedRadioButtonId()!=-1)
				{
					if(RCN_other[i].getVisibility()==View.VISIBLE && RCN_other[i].getText().toString().trim().equals(""))
					{
						cf.ShowToast("Please enter comments for "+validation_txt[i], 0);
						return;
					}
					
				}
				else
				{
					cf.ShowToast("Please select value for "+validation_txt[i], 0);
					return;
				}
			}
			Cursor c1 = cf.SelectTablefunction(cf.RCN_table_dy, " WHERE R_RCN_D_Ssrid='"+cf.selectedhomeid+"' and R_RCN_D_insp_id='"+Roof_page_value[5]+"' ");
			if(c1.getCount()>0)
			{
				TableLayout tbl_dy=((TableLayout)findViewById(R.id.RCN_Condition_N_DY_tbl));
				c1.moveToFirst();
				for(int i=0;i<c1.getCount();i++,c1.moveToNext())
				{
					int id=c1.getInt(c1.getColumnIndex("id"));
					if(tbl_dy.findViewById(id+1000)!=null)
					{
						RadioGroup rl =(RadioGroup) tbl_dy.findViewById(id+1000);
						if(rl.getCheckedRadioButtonId()!=-1)
						{
							if(tbl_dy.findViewById(id+2000)!=null)
							{
								EditText ed =(EditText) tbl_dy.findViewById(id+2000);
								if(ed.getVisibility()==View.VISIBLE && ed.getText().toString().trim().equals(""))
								{
									cf.ShowToast("Please enter comments for "+cf.decode(c1.getString(c1.getColumnIndex("R_RCN_D_title"))), 0);
									return;
								}
							}						
						}
						else
						{
							cf.ShowToast("Please select value for "+cf.decode(c1.getString(c1.getColumnIndex("R_RCN_D_title"))), 0);
							return;
						}
					}
				}
			}
			save_RCN();
		}
		
	}
	private void over_all_validation() {
		// TODO Auto-generated method stub
		String scope="";
		String comments="";
		for(int i=0;i<Chk_Scope.length;i++ )
		{
			if(Chk_Scope[i].isChecked())
			{
				scope+=Chk_Scope[i].getText().toString().trim()+",";
			}
		}
		if((!scope.equals("") || Roof_page_value[5].equals("7"))  )
		{
			Cursor c =cf.SelectTablefunction(cf.Rct_table, " WHERE RCT_Ssrid='"+cf.selectedhomeid+"' and RCT_insp_id='"+Roof_page_value[5]+"' ");
			if(c.getCount()>0 || RCT_chk_top.isChecked())
			{
			if(RCT_total_per==0 || RCT_chk_top.isChecked())
			{
				if(RCT_pre_m || RCT_chk_top.isChecked())
				{
					c =cf.SelectTablefunction(cf.Roof_additional, " WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+Roof_page_value[5]+"' and (R_ADD_saved='1' or R_ADD_saved='2') ");
				if(c.getCount()>0 || ADD_chk_top.isChecked()|| Roof_page_value[5].equals("7"))
				{
					 c =cf.SelectTablefunction(cf.RCN_table, " WHERE R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id='"+Roof_page_value[5]+"'  and R_RCN_saved='1'");
					if(c.getCount()>0 || RCN_chk_top.isChecked())
					{
					/*	if(!RC_cmt.getText().toString().trim().equals(""))
						{ */
							c =cf.SelectTablefunction(cf.RCN_table, " WHERE R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id='"+Roof_page_value[5]+"' ");
							
							if(c.getCount()>0)
							{
								cf.arr_db.execSQL(" UPDATE "+cf.RCN_table+" SET R_RCN_saved='1' ,R_RCN_na='"+RCN_chk_top.isChecked()+"'  WHERE R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id='"+Roof_page_value[5]+"'");
							}
							else
							{
								cf.arr_db.execSQL(" INSERT INTO "+cf.RCN_table+" (R_RCN_na,R_RCN_Ssrid,R_RCN_insp_id,R_RCN_saved) VALUES ('"+RCN_chk_top.isChecked()+"','"+cf.selectedhomeid+"','"+Roof_page_value[5]+"','1')");
							}
							c=cf.SelectTablefunction(cf.Roof_additional, " WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+Roof_page_value[5]+"'");
							if(c.getCount()>0)
							{
								cf.arr_db.execSQL(" UPDATE "+cf.Roof_additional+" SET R_ADD_saved='2',R_RCT_na='"+RCT_chk_top.isChecked()+"', R_ADD_scope='"+cf.encode(scope)+"',R_ADD_na='"+ADD_chk_top.isChecked()+"',R_ADD_comment='"+cf.encode(RC_cmt.getText().toString().trim())+"',R_ADD_citizen='"+((CheckBox)findViewById(R.id.includeCitizenForm)).isChecked()+"'  WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+Roof_page_value[5]+"'");
							}
							else
							{
									cf.arr_db.execSQL(" INSERT INTO "+cf.Roof_additional+" (R_RCT_na,R_ADD_scope,R_ADD_na,R_ADD_comment,R_ADD_citizen,R_ADD_Ssrid,R_ADD_insp_id,R_ADD_saved) Values ('"+RCT_chk_top.isChecked()+"','"+cf.encode(scope)+"','"+ADD_chk_top.isChecked()+"','"+cf.encode(RC_cmt.getText().toString().trim())+"','"+((CheckBox)findViewById(R.id.includeCitizenForm)).isChecked()+"','"+cf.selectedhomeid+"','"+Roof_page_value[5]+"','2')");	
							}
							cf.ShowToast("Roof information saved successfully", 1);
							checkforsurveyaleret();
						/*}
						else
						{
							cf.ShowToast("Please enter Roof Comments", 0);
						}*/
					}
					else
					{
						cf.ShowToast("Please save  Roof Condition Noted", 0);
					}
				}
				else
				{
					cf.ShowToast("Please save  Roof Additional Information", 0);
				}
				}else
				{
					cf.ShowToast("Please select predominant for any one Roof Covering Types", 0);
				}
			}else
			{
				cf.ShowToast("Sum of Roof Shape (%) percentage should be 100. in Roof Covering Types", 0);
			}	
			}else
			{
				cf.ShowToast("Please save atleast one Roof Covering Types", 0);
			}
		}
		else
		{
			cf.ShowToast("Please select value for "+getResources().getString(R.string.roof_add_4poin_txt2), 0);
		}
	}
	/***RCT table function****/
	protected void sp_setvalues(Spinner spinner, String string,EditText ed) {
		// TODO Auto-generated method stub
		ArrayAdapter  ad =(ArrayAdapter) spinner.getAdapter();
		System.out.println(" the value year"+string);
		if(!string.equals("") && !string.equals("N/A"))
		{
		int pos=ad.getPosition(string.trim());
		if(pos==-1)
		{
			int other_pos=ad.getPosition("Other");
			if(other_pos!=-1)
			{
				spinner.setSelection(other_pos);
				ed.setText(string);
			}
		}
		else
		{
			spinner.setSelection(pos);
		}
		}
		
	}
	class spiner_lis implements OnItemSelectedListener
	{
		EditText other;
		EditText per=null;;
		Spinner sp;
		public spiner_lis(Spinner sp,EditText editText) {
			// TODO Auto-generated constructor stub
			other=editText;
			this.sp=sp;
		}

		public spiner_lis(Spinner sp,EditText editText, EditText per) {
			// TODO Auto-generated constructor stub
			other=editText;
			this.per=per;
			this.sp=sp;
		}

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			/*System.out.println("Sp_RCT[1]"+Sp_RCT[1].getSelectedItem().toString());
			if(!Sp_RCT[1].getSelectedItem().toString().equals("Unknown"))
			{
				RCT_ed[0].setText("");
				RCT_bt[0].setVisibility(View.VISIBLE);
				RCT_chk[0].setChecked(false);
			}*/
			if(sp.getSelectedItem().toString().trim().equals("Other"))
			{
				other.setVisibility(View.VISIBLE);
			}
			else
			{
				other.setText("");
				other.setVisibility(View.GONE);
			}
			
			if(per!=null)
			{
				if(!sp.getSelectedItem().toString().trim().equals("-Select-"))
				{
					System.out.println("RCT_total_per"+RCT_total_per);
					/*if(!((Button) findViewById(R.id.Roof_RCT_SA)).getText().equals("Update"))
					{*/
					if(per.getText().toString().equals(""))
					{
						per.setText(RCT_total_per+"");
					}
					per.setEnabled(true);
				}
				else
				{
					per.setEnabled(false);
					per.setText("");
				}
			}
			
			if(sp.getId()==R.id.Roof_RCT_sp_RCT)
			{
				if(sp.getSelectedItemPosition()!=0)
				{
					cf.CreateARRTable(432);
					cf.CreateARRTable(435);
					
					Cursor c=cf.arr_db.rawQuery("Select * from "+cf.Roofcoverimages+" Where RIM_masterid=(Select RM_masterid from "+cf.Roof_master+
		    				" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+Roof_page_value[5]+"' and RM_Covering='"+cf.encode(sp.getSelectedItem().toString())+"') order by RIM_Order", null);
		    		
		    		if(c.getCount()>0)
		    		{
		    			tv.setVisibility(View.VISIBLE);
		    			tv.setText(Html.fromHtml("<u>View/Edit Photos ("+c.getCount()+")</u>"));
		    		
				    	
		    		}
		    		else
		    			{
		    				tv.setVisibility(View.GONE);
		    			}
		    			
				}
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	class Spin_Selectedlistener implements OnItemSelectedListener
	{
		public Spin_Selectedlistener() {
			// TODO Auto-generated constructor stub
			
		}
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
			// TODO Auto-generated method stub
			String roofslopeval = Sp_RCT[2].getSelectedItem().toString();
			if(roofslopeval.equals("Flat"))
			{
				Sp_RCT[3].setSelection(4);
			}
			else
			{
				
				if(Sp_RCT[3].getSelectedItem().toString().equals("Flat Roof") && editid==0)
				{
					Sp_RCT[3].setSelection(0);
				}
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
	}
	
	private void set_value_spinner(Spinner spinner, String[] arr_RCT2) {
		// TODO Auto-generated method stub
		ArrayAdapter adapter;
		adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, arr_RCT2);
	 	adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	 	spinner.setAdapter(adapter);
	}
	private void clear_RCT() {
		// TODO Auto-generated method stub
		for(int i=0;i<Sp_RCT.length;i++)
		{
			Sp_RCT[i].setSelection(0);
		}
		RCT_ed[0].setText("");
		RCT_ed[1].setText("");
		RCT_chk[0].setChecked(false);
		RCT_chk[1].setChecked(false);
		tv.setVisibility(View.GONE);
		((Button) findViewById(R.id.Roof_RCT_SA)).setText(getResources().getString(R.string.S_Addmore));
		Sp_RCT[0].setEnabled(true);
		RCT_bt[0].setVisibility(View.VISIBLE);
	}
	private boolean RCT_validation() {
		// TODO Auto-generated method stub
		if(spinner_validation(Sp_RCT[0],RCT_other[0],"Roof Covering Type",false))
		{
			if(!RCT_ed[0].getText().toString().trim().equals(""))
			{
				boolean no_per=RCT_chk[0].isChecked();
				if(spinner_validation(Sp_RCT[1],RCT_other[1],"Year of Installation",no_per) )
				{
					if(year_validation(RCT_other[1],"Year of Installation"))
					{
						if(spinner_validation(Sp_RCT[2],null,"Roof Slope",no_per) )
						{
							if(spinner_validation(Sp_RCT[3],RCT_other[2],"Roof Shape (%)",no_per) )
							{
								if(percent_validation(RCT_ed[1],Sp_RCT[3],"Roof Shape (%)",no_per))
								{
									if(spinner_validation(Sp_RCT[4],RCT_other[3],"Roof Structure",no_per) )
									{
										if(spinner_validation(Sp_RCT[5],null,"Building Code",no_per) )
										{
											if(spinner_validation(Sp_RCT[6],null,"Roof Complexity",no_per) )
											{
												if(spinner_validation(Sp_RCT[7],RCT_other[4],"Approximate Remaining Functional Life",no_per) )
												{
													if(duplicate_RCT(Sp_RCT[0]))
													{
														save_rct_values();
													}
													
												}
												
											}
										}
										
									}
									
								}
							}
						}
					}
				}
			}
			else
			{
				cf.ShowToast("Please select date for Permit Application Date", 0);
			}
		}
		return false;
	}
	private void save_rct_values() {
		// TODO Auto-generated method stub

		 String RCT_rct , RCT_PAD, RCT_yoi,  RCT_rsl, RCT_rsh , RCT_rst , RCT_bc , RCT_rc , RCT_arfl ,  RCT_Rsh_per
		, RCT_other_rct , RCT_other_yoi, RCT_other_rsh , RCT_other_rst , RCT_other_aRFL,RCT_pre,RCT_npv;
		 RCT_rct=remove_empty(Sp_RCT[0].getSelectedItem().toString().trim());
		 RCT_yoi=remove_empty(Sp_RCT[1].getSelectedItem().toString().trim());
		 RCT_rsl=remove_empty(Sp_RCT[2].getSelectedItem().toString().trim());
		 RCT_rsh=remove_empty(Sp_RCT[3].getSelectedItem().toString().trim());
		 RCT_rst=remove_empty(Sp_RCT[4].getSelectedItem().toString().trim());
		 RCT_bc=remove_empty(Sp_RCT[5].getSelectedItem().toString().trim());
		 RCT_rc=remove_empty(Sp_RCT[6].getSelectedItem().toString().trim());
		 RCT_arfl=remove_empty(Sp_RCT[7].getSelectedItem().toString().trim());
		 RCT_other_rct=remove_empty(RCT_other[0].getText().toString().trim());
		 RCT_other_yoi=remove_empty(RCT_other[1].getText().toString().trim());
		 RCT_other_rsh=remove_empty(RCT_other[2].getText().toString().trim()); 
		 RCT_other_rst=remove_empty(RCT_other[3].getText().toString().trim()); 
		 RCT_other_aRFL=remove_empty(RCT_other[4].getText().toString().trim());
		 RCT_PAD=RCT_ed[0].getText().toString().trim();
		 RCT_PAD=(RCT_PAD.equals("N/A"))?"":remove_empty(RCT_PAD);
		 RCT_Rsh_per=remove_empty(RCT_ed[1].getText().toString().trim());
		 if(RCT_chk[0].isChecked())
		 {
			 RCT_npv="true"; 
		 }
		 else
		 {
			 RCT_npv="false";
		 }
		 if(RCT_chk[1].isChecked())
		 {
			 RCT_pre="true"; 
		 }
		 else
		 {
			 RCT_pre="false";
		 }
		 //RCT_pre=RCT_chk[1].isChecked();
		 System.out.println("comes correctly2");
		 try
		 {
		 if(RCT_pre.equals("true"))
		 {
			 cf.arr_db.execSQL(" UPDATE "+cf.Rct_table+" SET RCT_pre='false' WHERE RCT_Ssrid='"+cf.selectedhomeid+"' and RCT_insp_id='"+Roof_page_value[5]+"'");
		 }
		 Cursor c =cf.SelectTablefunction(cf.Rct_table, " WHERE RCT_Ssrid='"+cf.selectedhomeid+"' and RCT_rct='"+cf.encode(Sp_RCT[0].getSelectedItem().toString().trim())+"' and RCT_insp_id='"+Roof_page_value[5]+"'");
			if(c.getCount()>0)
			{
				cf.arr_db.execSQL(" UPDATE  "+cf.Rct_table+" SET RCT_rct='"+RCT_rct+"',RCT_PAD='"+RCT_PAD+"',RCT_yoi='"+RCT_yoi+"',RCT_rsl='"+RCT_rsl+"',RCT_rsh='"+RCT_rsh+"',RCT_rst='"+RCT_rst+"',RCT_bc='"+RCT_bc+"',RCT_rc='"+RCT_rc+"',RCT_arfl='"+RCT_arfl+"'," +
						"RCT_Rsh_per='"+RCT_Rsh_per+"',RCT_other_rct='"+RCT_other_rct+"',RCT_other_yoi='"+RCT_other_yoi+"',RCT_other_rsh='"+RCT_other_rsh+"',RCT_other_rst='"+RCT_other_rst+"',RCT_other_aRFL='"+RCT_other_aRFL+"',RCT_npv='"+RCT_npv+"',RCT_pre='"+RCT_pre+"' WHERE RCT_Ssrid='"+cf.selectedhomeid+"' and RCT_rct='"+cf.encode(Sp_RCT[0].getSelectedItem().toString().trim())+"' and RCT_insp_id='"+Roof_page_value[5]+"'");
			}
			else
			{
				cf.arr_db.execSQL(" INSERT INTO "+cf.Rct_table+" (RCT_Ssrid,RCT_rct,RCT_PAD,RCT_yoi,RCT_rsl,RCT_rsh,RCT_rst,RCT_bc,RCT_rc,RCT_arfl,RCT_Rsh_per,RCT_other_rct,RCT_other_yoi,RCT_other_rsh,RCT_other_rst,RCT_other_aRFL,RCT_insp_id,RCT_npv,RCT_pre)" +
				" VALUES ('"+cf.selectedhomeid+"','"+RCT_rct+"','"+RCT_PAD+"','"+RCT_yoi+"','"+RCT_rsl+"','"+RCT_rsh+"','"+RCT_rst+"','"+RCT_bc+"','"+RCT_rc+"','"+RCT_arfl+"','"+RCT_Rsh_per+"','"+RCT_other_rct+"','"+RCT_other_yoi+"','"+RCT_other_rsh+"','"+RCT_other_rst+"','"+RCT_other_aRFL+"','"+Roof_page_value[5]+"','"+RCT_npv+"','"+RCT_pre+"')");
			}
			c=cf.SelectTablefunction(cf.Roof_additional, " WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+Roof_page_value[5]+"'");
			if(c.getCount()>0)
			{
				cf.arr_db.execSQL(" UPDATE "+cf.Roof_additional+" SET R_RCT_na='"+RCT_chk_top.isChecked()+"'  WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+Roof_page_value[5]+"'");
			}
			if(c!=null)
			{
				c.close();
			}
		 	}
			catch (Exception e) {
				// TODO: handle exception
				System.out.println(" the error was "+e.getMessage());
			}
		 cf.ShowToast("Roof Covering Type saved successfully", 0);
		 clear_RCT();
			show_RCT_values();
	}
	private String remove_empty(String trim) {
		// TODO Auto-generated method stub
		if(trim!=null)
		{
			if(!trim.equals("-Select-") && !trim.equals("--Select--"))
			{
				if(!trim.equals("N/A"))
				{
					return cf.encode(trim);
				}
			}
		}
		
		return "";
	}
	private void show_RCT_values() {
		// TODO Auto-generated method stub
		RCT_total_per=100;
		 Cursor c =cf.SelectTablefunction(cf.Rct_table, " WHERE RCT_Ssrid='"+cf.selectedhomeid+"'  and RCT_insp_id='"+Roof_page_value[5]+"'");
		 
		 TableLayout	RCT_ShowValue= (TableLayout) findViewById(R.id.RC_RCT_ShowValue);
		 RCT_ShowValue.removeAllViews();
		int  colomprerow=6;
			if(c.getCount()>0)
			{
				String arr_title[]={"No","Roof Covering Type","Permit Application Date","Year of Insallation","No Permit/Verification Documents","Roof Slope","Roof Shape (%)","Roof Structure","Building Code","Roof Complexity","Approximate Remaining Functional Life","Predominant","Upload Photo","Edit/Delete"};
				int width[]={30,120,140,125,160,100,120,130,110,125,110,100,120,110};
				// findViewById(R.id.RCT_comp).setVisibility(View.VISIBLE);
				 
					c.moveToFirst();
					
					RCT_ShowValue.setVisibility(View.VISIBLE);
					TableRow tbl =new TableRow(this);
					TableRow.LayoutParams lin_params = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
					tbl.setBackgroundResource(R.drawable.roofhead); 
					tbl.setLayoutParams(lin_params);
					tbl.setGravity(Gravity.CENTER_VERTICAL);
					
					TableLayout li_opt=new TableLayout(this);
					li_opt.setOrientation(LinearLayout.VERTICAL);
					/**Sub row linear **/
					TableRow li_spu =new TableRow(this);
					li_opt.addView(li_spu,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
					for(int i=0;i<arr_title.length;i++)
					{
						if(i<2)
						{
							TextView Ed=new TextView(this,null,R.attr.text_view);
							Ed.setVisibility(View.VISIBLE);
							Ed.setTextColor(Color.WHITE);
							tbl.addView(Ed,width[i],LayoutParams.WRAP_CONTENT);
							Ed.setText(arr_title[i]);
							View v=new View(this);
							v.setBackgroundResource(R.color.black);
							tbl.addView(v,1,LayoutParams.FILL_PARENT);
						}
						else
						{
							if((i-2)%colomprerow==0 && (i-2)!=0)
							{
								View v=new View(this);
								v.setBackgroundResource(R.color.row_head_line);
								li_opt.addView(v,LayoutParams.FILL_PARENT,1);
								
								li_spu=new TableRow(this);
								li_opt.addView(li_spu,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
							}
							else if((i-2)!=0)
							{
								View v=new View(this);
								v.setBackgroundResource(R.color.black);
								li_spu.addView(v,1,LayoutParams.FILL_PARENT);
							}
								
							TextView Ed=new TextView(this,null,R.attr.text_view);
							Ed.setVisibility(View.VISIBLE);
							Ed.setTextColor(Color.WHITE);
							
							li_spu.addView(Ed,width[i],LayoutParams.WRAP_CONTENT);
							Ed.setText(arr_title[i]);
						}
					}
					tbl.addView(li_opt,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
					RCT_ShowValue.addView(tbl);
					c.moveToFirst();
					TextView Ed=null;
					for(int i=0;i<c.getCount();i++,c.moveToNext())
					{
						 String RCT_rct , RCT_PAD, RCT_yoi,  RCT_rsl, RCT_rsh , RCT_rst , RCT_bc , RCT_rc , RCT_arfl ,  RCT_Rsh_per
							, RCT_other_rct , RCT_other_yoi, RCT_other_rsh , RCT_other_rst , RCT_other_aRFL,RCT_pre,RCT_npv;
							 RCT_rct=cf.decode(c.getString(c.getColumnIndex("RCT_rct")));
							 RCT_PAD =cf.decode(c.getString(c.getColumnIndex("RCT_PAD")));
							 RCT_yoi=cf.decode(c.getString(c.getColumnIndex("RCT_yoi")));
							 RCT_rsl =cf.decode(c.getString(c.getColumnIndex("RCT_rsl")));
							 RCT_rsh=cf.decode(c.getString(c.getColumnIndex("RCT_rsh")));
							 RCT_rst =cf.decode(c.getString(c.getColumnIndex("RCT_rst")));
							 RCT_bc =cf.decode(c.getString(c.getColumnIndex("RCT_bc")));
							 RCT_rc =cf.decode(c.getString(c.getColumnIndex("RCT_rc")));
							 RCT_arfl =cf.decode(c.getString(c.getColumnIndex("RCT_arfl")));
							 RCT_Rsh_per=cf.decode(c.getString(c.getColumnIndex("RCT_Rsh_per")));
							 int pe=0;
							 if(!RCT_Rsh_per.equals(""))
							 {
								 try
								 {
									 pe=Integer.parseInt(RCT_Rsh_per);
								 }
								 catch (Exception e) {
									// TODO: handle exception
									 pe=0;
								}
								 RCT_total_per -=pe;
							 }
							 RCT_other_rct =cf.decode(c.getString(c.getColumnIndex("RCT_other_rct")));
							 RCT_other_yoi =cf.decode(c.getString(c.getColumnIndex("RCT_other_yoi")));
							 RCT_other_rsh=cf.decode(c.getString(c.getColumnIndex("RCT_other_rsh")));
							 RCT_other_rst=cf.decode(c.getString(c.getColumnIndex("RCT_other_rst")));
							 RCT_other_aRFL=cf.decode(c.getString(c.getColumnIndex("RCT_other_aRFL")));
							 RCT_pre=cf.decode(c.getString(c.getColumnIndex("RCT_pre")));
							 if(RCT_pre.equals("true"))
							 {
								 RCT_pre_m=true;
							 }
							 RCT_npv=cf.decode(c.getString(c.getColumnIndex("RCT_npv")));
						
						String value[]=new String[arr_title.length];
						value[0]=(i+1)+"";
						if(RCT_rct.equals("Other"))
							value[1]=RCT_other_rct;
						else
							value[1]=RCT_rct;
						
						value[2]=RCT_PAD;
						
						if(RCT_yoi.equals("Other"))
							value[3]=RCT_other_yoi;
						else
							value[3]=RCT_yoi;
						value[4]=RCT_npv;
						value[5]=RCT_rsl;
						
						if(RCT_rsh.equals("Other"))
							value[6]=RCT_other_rsh;
						else
							value[6]=RCT_rsh;
						if(!RCT_Rsh_per.equals(""))
						{
							value[6]+="("+RCT_Rsh_per+"%)";
						}
						if(RCT_rst.equals("Other"))
							value[7]=RCT_other_rst;
						else
							value[7]=RCT_rst;
						value[8]=RCT_bc;
						value[9]=RCT_rc;
						
						if(RCT_arfl.equals("Other"))
							value[10]=RCT_other_aRFL;
						else
							value[10]=RCT_arfl;
						value[11]=RCT_pre;
						value[12]=0+"";
						value[13]="";
						tbl=new TableRow(this);
						//tbl.setBackgroundResource(R.drawable.roofhead); 
						tbl.setLayoutParams(lin_params);
						tbl.setGravity(Gravity.CENTER_VERTICAL);
						
						
						if(i%2==0)
						{
							
							tbl.setBackgroundResource(R.drawable.rooflist);
						}
						else
						{
							tbl.setBackgroundResource(R.drawable.rooflistw);
							
						}
						
						
						li_opt=new TableLayout(this);
						li_opt.setOrientation(LinearLayout.VERTICAL);
						li_spu=new TableRow(this);
						li_opt.addView(li_spu,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
						
						for(int j=0;j<arr_title.length;j++)
						{
							try
							{
								Ed=new TextView(this,null,R.attr.text_view);
							if(j<2)
							{
								Ed.setVisibility(View.VISIBLE);
								Ed.setTextColor(Color.BLACK);
								tbl.addView(Ed,width[j],LayoutParams.WRAP_CONTENT);
								Ed.setText(value[j]);
								View v=new View(this);
								v.setBackgroundResource(R.color.black);
								tbl.addView(v,1,LayoutParams.FILL_PARENT);
							}
							else
							{
								if((j-2)%colomprerow==0 && (j-2)!=0)
								{
									View v=new View(this);
									v.setBackgroundResource(R.color.row_head_line);
									li_opt.addView(v,LayoutParams.FILL_PARENT,1);
									
									li_spu=new TableRow(this);
									li_opt.addView(li_spu,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
								}
								else if((j-2)!=0)
								{
									View v=new View(this);
									v.setBackgroundResource(R.color.black);
									li_spu.addView(v,1,LayoutParams.FILL_PARENT);
								}
								
								if(j==4 || j==11 )
								{
									if(value[j].equals("true"))
									{
										ImageView  iv=new ImageView(this);
										iv.setBackgroundResource(R.drawable.tick_icon);
										iv.setVisibility(View.VISIBLE);
										RelativeLayout rl=new RelativeLayout(this);
										rl.setGravity(Gravity.CENTER);
										rl.addView(iv);
										li_spu.addView(rl,width[j],LayoutParams.WRAP_CONTENT);
									}
									else
									{
										
										Ed.setVisibility(View.VISIBLE);
										Ed.setTextColor(Color.BLACK);
									
										li_spu.addView(Ed,width[j],LayoutParams.WRAP_CONTENT);
										Ed.setText("N/A");
										
									}
									
								}
								else if(j==13)
								{
									LinearLayout li=new LinearLayout(this);
									ImageView im_up =new ImageView(this,null,R.attr.Roof_RCN_Tit_img);
																
									im_up.setTag(c.getString(c.getColumnIndex("id")));
									li.addView(im_up);
									im_up.setVisibility(View.VISIBLE);
									im_up.setPadding(5, 5, 0, 5);
									im_up.setOnClickListener(new RCT_edit_delete(1));
									ImageView im_dl =new ImageView(this,null,R.attr.Roof_RCN_opt_img);
									im_dl.setTag(cf.decode(c.getString(c.getColumnIndex("id"))));
									im_dl.setPadding(5, 5, 0, 5);
									im_dl.setVisibility(View.VISIBLE);
									im_dl.setOnClickListener(new RCT_edit_delete(2));
									li.addView(im_dl);
									li.setGravity(Gravity.CENTER);
									li_spu.addView(li);
								}
								else if(j==12)
					    		{
									Cursor c1=cf.arr_db.rawQuery("Select * from "+cf.Roofcoverimages+" Where RIM_masterid=(Select RM_masterid from "+cf.Roof_master+
						    				" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+Roof_page_value[5]+"' and RM_Covering='"+cf.encode(RCT_rct)+"') order by RIM_Order", null);
						    		
						    		if(c1.getCount()>0)
						    		{
					    			Ed.setText(Html.fromHtml("<u>View Photos ("+c1.getCount()+")</u>"));
					    			Ed.setOnClickListener(new RCT_edit_delete(3));
					    			Ed.setTag(cf.decode(c.getString(c.getColumnIndex("id"))));
					    			Ed.setTextColor(Color.BLUE);
					    			li_spu.addView(Ed);
						    		}
						    		else
						    		{
						    			Ed.setText("N/A");
						    			//Ed.setOnClickListener(new RCT_edit_delete(3));
						    			Ed.setTag(cf.decode(c.getString(c.getColumnIndex("id"))));
						    			li_spu.addView(Ed);
						    		}
									
					    		}
								else
								{
									
									Ed.setVisibility(View.VISIBLE);
									Ed.setTextColor(Color.BLACK);
									li_spu.addView(Ed,width[j],LayoutParams.WRAP_CONTENT);
									if(!value[j].equals(""))
										Ed.setText(value[j]);
									else
										Ed.setText("N/A");
								}
								
								
							}
						
						}catch (Exception e) {
							// TODO: handle exception
							System.out.println(" 	 "+e.getMessage()+" ="+j);
						}
						}
						tbl.addView(li_opt,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
						RCT_ShowValue.addView(tbl);
				//	c.moveToNext();
					
					}
					if(RCT_total_per==0 && RCT_pre_m)
					{
						findViewById(R.id.RCT_comp).setVisibility(View.VISIBLE);
					}
					else
					{
						findViewById(R.id.RCT_comp).setVisibility(View.INVISIBLE);
					}
			}
			else
			{
				findViewById(R.id.RCT_comp).setVisibility(View.INVISIBLE);	
			}
	}
	class RCT_edit_delete implements OnClickListener
	{
int type;
		public RCT_edit_delete(int i) {
			// TODO Auto-generated constructor stub
			type=i;
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			final String id=v.getTag().toString();
			if(type==1)
			{
				Edit_RCT(id);
			}
			else if(type==2)
			{
				final AlertDialog.Builder ab =new AlertDialog.Builder(Roof_section_common.this);
				ab.setTitle("Confirmation");
				ab.setMessage(Html.fromHtml("Do you want to delete the selected Roof Covering Type?"));
				ab.setIcon(R.drawable.alertmsg);
				ab.setCancelable(false);
				ab.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						cf.arr_db.execSQL(" DELETE FROM "+cf.Rct_table+" WHERE id='"+id+"'");
						cf.ShowToast("Roof Covering Type deleted successfully ", 0);
						
						show_RCT_values();
					}
					
				})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						
					}
				});
				AlertDialog al=ab.create();
				al.show();
			}
			else if(type==3)
			{
				Intent in=new Intent(Roof_section_common.this,RoofImagePicking.class);
			    in.putExtra("roof_cover_img", true);
				Cursor c =cf.SelectTablefunction(cf.Rct_table, " WHERE id='"+id+"'");
				c.moveToFirst();
				String cv="";
				if(c.getCount()>0)
				{
					cv= cf.decode(c.getString(c.getColumnIndex("RCT_rct")));	
				}
			    
			    try
			    {
			    	
			        in.putExtra("Cover", cv);
				    in.putExtra("Insp_id",Roof_page_value[5]);
				    in.putExtra("SRID",cf.selectedhomeid);
				    in.putExtra("readonly",true);
				    startActivityForResult(in, pick_img_code);
			    
			    }
			    catch (Exception e) {
					// TODO: handle exception
				}
			}
			
		}

		
		
	}
	private void Edit_RCT(String id) {
		// TODO Auto-generated method stub
		editid=1;
		Cursor c =cf.SelectTablefunction(cf.Rct_table, " WHERE id='"+id+"'");
		c.moveToFirst();
		if(c.getCount()>0)
		{
			((Button) findViewById(R.id.Roof_RCT_SA)).setText("Update");
			 String RCT_rct , RCT_PAD, RCT_yoi,  RCT_rsl, RCT_rsh , RCT_rst , RCT_bc , RCT_rc , RCT_arfl ,  RCT_Rsh_per
				, RCT_other_rct , RCT_other_yoi, RCT_other_rsh , RCT_other_rst , RCT_other_aRFL,RCT_pre,RCT_npv;
				 RCT_rct=cf.decode(c.getString(c.getColumnIndex("RCT_rct")));
				 RCT_PAD =cf.decode(c.getString(c.getColumnIndex("RCT_PAD")));
				 RCT_yoi=cf.decode(c.getString(c.getColumnIndex("RCT_yoi")));
				 RCT_rsl =cf.decode(c.getString(c.getColumnIndex("RCT_rsl")));
				 RCT_rsh=cf.decode(c.getString(c.getColumnIndex("RCT_rsh")));
				 RCT_rst =cf.decode(c.getString(c.getColumnIndex("RCT_rst")));
				 RCT_bc =cf.decode(c.getString(c.getColumnIndex("RCT_bc")));
				 RCT_rc =cf.decode(c.getString(c.getColumnIndex("RCT_rc")));
				 RCT_arfl =cf.decode(c.getString(c.getColumnIndex("RCT_arfl")));
				 RCT_Rsh_per=cf.decode(c.getString(c.getColumnIndex("RCT_Rsh_per")));
				 RCT_other_rct =cf.decode(c.getString(c.getColumnIndex("RCT_other_rct")));
				 RCT_other_yoi =cf.decode(c.getString(c.getColumnIndex("RCT_other_yoi")));
				 RCT_other_rsh=cf.decode(c.getString(c.getColumnIndex("RCT_other_rsh")));
				 RCT_other_rst=cf.decode(c.getString(c.getColumnIndex("RCT_other_rst")));
				 RCT_other_aRFL=cf.decode(c.getString(c.getColumnIndex("RCT_other_aRFL")));
				 RCT_pre=cf.decode(c.getString(c.getColumnIndex("RCT_pre")));
				 RCT_npv=cf.decode(c.getString(c.getColumnIndex("RCT_npv")));
				 sp_setvalues(Sp_RCT[0], RCT_rct,null);
				 sp_setvalues(Sp_RCT[1], RCT_yoi,null);
				 sp_setvalues(Sp_RCT[2], RCT_rsl,null);
				 sp_setvalues(Sp_RCT[3], RCT_rsh,null);
				 sp_setvalues(Sp_RCT[4], RCT_rst,null);
				 sp_setvalues(Sp_RCT[5], RCT_bc,null);
				 sp_setvalues(Sp_RCT[6], RCT_rc,null);
				 sp_setvalues(Sp_RCT[7], RCT_arfl,null);
				 RCT_other[0].setText(RCT_other_rct);
				 RCT_other[1].setText(RCT_other_yoi);
				 RCT_other[2].setText(RCT_other_rsh);
				 RCT_other[3].setText(RCT_other_rst);
				 RCT_other[4].setText(RCT_other_aRFL);
				 RCT_ed[0].setText(RCT_PAD);
				 RCT_ed[1].setText(RCT_Rsh_per);
				 if(RCT_npv.equals("true"))
				 {
					 	RCT_chk[0].setChecked(true);
					 	RCT_ed[0].setText("N/A");
						//RCT_bt[0].setVisibility(View.INVISIBLE);
				 }
				 if(RCT_pre.equals("true"))
					 	RCT_chk[1].setChecked(true);
				 
				 Sp_RCT[0].setEnabled(false);
		}
		if(c!=null)
			c.close();
	}
	private boolean duplicate_RCT(Spinner spinner) {
		// TODO Auto-generated method stub
		
		Cursor c =cf.SelectTablefunction(cf.Rct_table, " WHERE RCT_Ssrid='"+cf.selectedhomeid+"' and RCT_rct='"+cf.encode(Sp_RCT[0].getSelectedItem().toString().trim())+"' and  RCT_insp_id='"+Roof_page_value[5]+"'");
		if(!((Button) findViewById(R.id.Roof_RCT_SA)).getText().equals("Update"))
		{
		if(c.getCount()>0)
		{
			c.close();
			cf.ShowToast("Sorry selecteed Roof Covering Type already saved", 0);
			return false;
		}
		if(c!=null)
			c.close();
		}
		
		return true;
	}
	private boolean percent_validation(EditText editText, Spinner spinner,String string, boolean no_per) {
		// TODO Auto-generated method stub
		if(spinner.getSelectedItemPosition()!=0)
		{
			if(!editText.getText().toString().trim().equals(""))
			{
				int per =Integer.parseInt(editText.getText().toString());
				if(per>0 && per <=100){
					Cursor c =cf.SelectTablefunction(cf.Rct_table, " WHERE RCT_Ssrid='"+cf.selectedhomeid+"' and RCT_rct<>'"+cf.encode(Sp_RCT[0].getSelectedItem().toString().trim())+"' and  RCT_insp_id='"+Roof_page_value[5]+"'");
					if(c.getCount()>0)
					{ int total_per=0;
						c.moveToFirst();
						for(int i =0;i<c.getCount();i++,c.moveToNext())
						{
							 String RCT_Rsh_per=cf.decode(c.getString(c.getColumnIndex("RCT_Rsh_per")));
							 if(!RCT_Rsh_per.equals(""))
							 {
								 try
								 {
									total_per +=Integer.parseInt(RCT_Rsh_per);
								 }
								 catch (Exception e) {
									// TODO: handle exception
								}
							 }
						}
						if((total_per+per)>100)
						{
							cf.ShowToast("Percentage exceeds! Sum of Roof Shape (%) percentage should be 100.", 0);
							return false;
						}
						else
						{
							return true;
							
						}
					}
					else
					{
						return true;
					}
					
				}
				else
				{
					cf.ShowToast("Please enter valid percentage for "+string, 0);
				}
			}
			else
			{
				cf.ShowToast("Please enter percentage for "+string, 0);
			}
		}
		else
		{
			return true;
		}
		return false;
	}
	private boolean year_validation(EditText editText,String s) {
		// TODO Auto-generated method stub
		if(!editText.getText().toString().trim().equals(""))
		{
			int year=Integer.parseInt(editText.getText().toString());
			if(year>1000)
			{
				if(year<mYear)
				{
					return true;
				}
				else
				{
					cf.ShowToast("The entered year should not be greater than the current year in "+s, 0);
					return false;
				}
			}
			else
			{
				cf.ShowToast("Please enter the valid year in "+s, 0);
				return false;
			}
		}
		else{
		return true;
		}
	}
	private boolean spinner_validation(Spinner spinner, EditText editText,String string,boolean b) {
		// TODO Auto-generated method stub
		if(spinner.getSelectedItemPosition()!=0)
		{
			if(spinner.getSelectedItem().toString().trim().equals("Other") && editText!=null)
			{
				if(!editText.getText().toString().trim().equals(""))
				{
					return true;
				}
				else
				{
					cf.ShowToast("Please enter the other value for "+string, 0);
					return false;
				}
			}
			else 
			{
				
				return true;
			}
		}
		else if(b)
		{
			return true;
		}
		else
		{
			cf.ShowToast("Please select value for "+string, 0);
			return false;
		}
		
	}
	/***RCT table function ends****/
	/***Additional table function**/
	private void Additional_validation() {
		// TODO Auto-generated method stub
		String s=(add_rg.getCheckedRadioButtonId()!=-1)? add_rg.findViewById(add_rg.getCheckedRadioButtonId()).getTag().toString():"";
		if(s.equals(""))
		{
			cf.ShowToast("Please select value for Full leakage review part of inspection", 0);
		}
		else
		{
			Cursor c =cf.SelectTablefunction(cf.Roof_additional, " WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+Roof_page_value[5]+"'");
			if(c.getCount()>0)
			{
			//	R_ADD_Ssrid VARCHAR( 5 ) DEFAULT ( '' ), R_ADD_insp_id VARCHAR( 5 ) DEFAULT ( '' ), R_ADD_full VARCHAR( 5 ) DEFAULT ( '' ), R_ADD_scope VARCHAR( 150 ) DEFAULT ( '' ), R_ADD_na VARCHAR( 5 ) DEFAULT ( 'false' ), R_ADD_comment VARCHAR( 500 ) DEFAULT ( '' ), R_ADD_saved VARCHAR( 1 ) DEFAULT ( '1' )
				cf.arr_db.execSQL(" UPDATE "+cf.Roof_additional+" SET R_ADD_na='"+ADD_chk_top.isChecked()+"',R_ADD_full='"+cf.encode(s)+"',R_ADD_saved='1' WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+Roof_page_value[5]+"'");
			}
			else
			{
				cf.arr_db.execSQL(" INSERT INTO "+cf.Roof_additional+" (R_ADD_Ssrid,R_ADD_insp_id,R_ADD_full,R_ADD_saved) VALUES ('"+cf.selectedhomeid+"','"+Roof_page_value[5]+"','"+cf.encode(s)+"','1')");
			}
			findViewById(R.id.ADD_comp).setVisibility(View.VISIBLE);
			cf.ShowToast("Roof Additional information saved successfully", 0);
			if(c!=null)
				c.close();
		}
	}
	private void Additional_show_value()
	{
		Cursor c =cf.SelectTablefunction(cf.Roof_additional, " WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+Roof_page_value[5]+"'");
		if(c.getCount()>0)
		{
			c.moveToFirst();
			String s=cf.decode(c.getString(c.getColumnIndex("R_ADD_full")));
			if(!s.equals(""))
			{
				if(add_rg.findViewWithTag(s)!=null)
					{
					((RadioButton)add_rg.findViewWithTag(s)).setChecked(true);
					}
				
			}
			//(add_rg.getCheckedRadioButtonId()!=-1)? add_rg.findViewWithTag(add_rg.getCheckedRadioButtonId()).getTag().toString():"";
			
			
		}
		if(c!=null)
			c.close();
	}
	/***Additional table function**/
	/**RCN table function***/
	private void Show_RCN_values() {
		// TODO Auto-generated method stub
		Cursor c =cf.SelectTablefunction(cf.RCN_table, " WHERE R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id='"+Roof_page_value[5]+"' ");
		if(c.getCount()>0)
		{
			c.moveToFirst();
			String OCR=cf.decode(c.getString(c.getColumnIndex("R_RCN_OCR"))),OCR_o="",
			RRN=cf.decode(c.getString(c.getColumnIndex("R_RCN_RRN"))),RRN_o="",
			VSL=cf.decode(c.getString(c.getColumnIndex("R_RCN_VSL"))),VSL_o="", 
			VSTDS=cf.decode(c.getString(c.getColumnIndex("R_RCN_VSTDS"))),VSTDS_o="",
			VSCBS=cf.decode(c.getString(c.getColumnIndex("R_RCN_VSCBS"))),VSCBS_o="",
			VSDTS=cf.decode(c.getString(c.getColumnIndex("R_RCN_VSDTS"))),VSDTS_o="",
			VDRS=cf.decode(c.getString(c.getColumnIndex("R_RCN_VDRS"))),VDRS_o="",
			ODN=cf.decode(c.getString(c.getColumnIndex("R_RCN_ODN"))),ODN_o="";
			
			
			if(!OCR.equals(""))
			{
				if(OCR.contains("#&45"))
				{
					String[]tem=OCR.split("#&45");
					OCR=tem[0];
					OCR_o=tem[1];
				}
				set_value_radio(RCN_RG[0],OCR,RCN_other[0],OCR_o);
				if(RRN.contains("#&45"))
				{
					String[]tem=RRN.split("#&45");
					RRN=tem[0];
					RRN_o=tem[1];
				}
				set_value_radio(RCN_RG[1],RRN,RCN_other[1],RRN_o);
				if(VSL.contains("#&45"))
				{
					String[]tem=VSL.split("#&45");
					VSL=tem[0];
					VSL_o=tem[1];
				}
				set_value_radio(RCN_RG[2],VSL,RCN_other[2],VSL_o);
				if(VSTDS.contains("#&45"))
				{
					String[]tem=VSTDS.split("#&45");
					VSTDS=tem[0];
					VSTDS_o=tem[1];
				}
				set_value_radio(RCN_RG[3],VSTDS,RCN_other[3],VSTDS_o);
				if(VSCBS.contains("#&45"))
				{
					String[]tem=VSCBS.split("#&45");
					VSCBS=tem[0];
					VSCBS_o=tem[1];
				}
				set_value_radio(RCN_RG[4],VSCBS,RCN_other[4],VSCBS_o);
				if(VSDTS.contains("#&45"))
				{
					String[]tem=VSDTS.split("#&45");
					VSDTS=tem[0];
					VSDTS_o=tem[1];
				}
				set_value_radio(RCN_RG[5],VSDTS,RCN_other[5],VSDTS_o);
				if(VDRS.contains("#&45"))
				{
					String[]tem=VDRS.split("#&45");
					VDRS=tem[0];
					VDRS_o=tem[1];
				}
				set_value_radio(RCN_RG[6],VDRS,RCN_other[6],VDRS_o);
				if(ODN.contains("#&45"))
				{
					String[]tem=ODN.split("#&45");
					ODN=tem[0];
					ODN_o=tem[1];
				}
				set_value_radio(RCN_RG[7],ODN,RCN_other[7],ODN_o);
			}
			else
			{
				for(int i=0;i<RCN_RG.length;i++)
				{
					if(RCN_RG[i].findViewWithTag("BSI")!=null)
					{
						((RadioButton) RCN_RG[i].findViewWithTag("BSI")).setChecked(true);
					}
				}
			}
		}
		else
		{
			for(int i=0;i<RCN_RG.length;i++)
			{
				if(RCN_RG[i].findViewWithTag("BSI")!=null)
				{
					((RadioButton) RCN_RG[i].findViewWithTag("BSI")).setChecked(true);
				}
			}
		}
		if(c!=null)
			c.close();
		Show_dynamic_RCN();
	}
	private void set_value_radio(RadioGroup radioGroup, String RG_val,
			EditText editText, String other_val) {
		// TODO Auto-generated method stub
		if(!RG_val.equals(""))
		{
			if(radioGroup.findViewWithTag(RG_val.trim())!=null)
			{
				((RadioButton) radioGroup.findViewWithTag(RG_val.trim())).setChecked(true);
			}
		}
		if(other_val!=null)
		{
			editText.setText(other_val);
		}
		
	}
	public void clear_RCN() {
		// TODO Auto-generated method stub
		for(int i =0;i<RCN_RG.length;i++)
		{
			RCN_RG[i].clearCheck();
			if(RCN_RG[i].findViewWithTag("BSI")!=null)
			{
				((RadioButton)RCN_RG[i].findViewWithTag("BSI")).setChecked(true);
			}
			else
			{
				RCN_RG[i].clearCheck();
			}
			
				
		}
		
		cf.arr_db.execSQL("DELETE FROM "+cf.RCN_table_dy+" WHERE R_RCN_D_Ssrid='"+cf.selectedhomeid+"' and R_RCN_D_insp_id='"+Roof_page_value[5]+"' and R_RCN_D_saved='0'");
		Show_RCN_values();
	}
	class RCN_click implements OnCheckedChangeListener
	{
		EditText comments;
		TextView tv;
		String default_v;
		public RCN_click(EditText editText, TextView textView,String s) {
			// TODO Auto-generated constructor stub
			comments=editText;
			tv=textView;
			default_v=s;
			comments.addTextChangedListener(new TextWatcher() {

				
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					// TODO Auto-generated method stub
					cf.showing_limit(s.toString(), tv, 250);
				}
				
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub
					
				}
			});
		}

		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			if(((RadioButton)group.findViewWithTag(default_v)).isChecked())
			{
				
				comments.setVisibility(View.VISIBLE);
				tv.setVisibility(View.VISIBLE);
			}
			else
			{
				comments.setText("");
				comments.setVisibility(View.GONE);
				tv.setVisibility(View.GONE);
			}
		}
		
	}
	private void RCN_validation() {
		// TODO Auto-generated method stub
		String[] validation_txt={"Overall Condition of Roof","Recent Repairs Noted","Visible Signs of Leakage","Visible Signs of Torn/Damaged Shingles/Roof Membrane","Visible Signs of Curling/Brittle Shingles/Roof Membrane","Visible Signs of Damaged Tiling/Slates/Metal Roofing","Visible Damage to Roof Structure/Sheathing","Other Damage Noted"}; 
		for(int i=0;i< RCN_RG.length;i++)
		{
			if(RCN_RG[i].getCheckedRadioButtonId()!=-1)
			{
				if(RCN_other[i].getVisibility()==View.VISIBLE && RCN_other[i].getText().toString().trim().equals(""))
				{
					cf.ShowToast("Please enter comments for "+validation_txt[i], 0);
					return;
				}
				
			}
			else
			{
				cf.ShowToast("Please select value for "+validation_txt[i], 0);
				return;
			}
		}
		Cursor c = cf.SelectTablefunction(cf.RCN_table_dy, " WHERE R_RCN_D_Ssrid='"+cf.selectedhomeid+"' and R_RCN_D_insp_id='"+Roof_page_value[5]+"' ");
		if(c.getCount()>0)
		{
			TableLayout tbl_dy=((TableLayout)findViewById(R.id.RCN_Condition_N_DY_tbl));
			c.moveToFirst();
			for(int i=0;i<c.getCount();i++,c.moveToNext())
			{
				int id=c.getInt(c.getColumnIndex("id"));
				if(tbl_dy.findViewById(id+1000)!=null)
				{
					RadioGroup rl =(RadioGroup) tbl_dy.findViewById(id+1000);
					if(rl.getCheckedRadioButtonId()!=-1)
					{
						if(tbl_dy.findViewById(id+2000)!=null)
						{
							EditText ed =(EditText) tbl_dy.findViewById(id+2000);
							if(ed.getVisibility()==View.VISIBLE && ed.getText().toString().trim().equals(""))
							{
								cf.ShowToast("Please enter comments for "+cf.decode(c.getString(c.getColumnIndex("R_RCN_D_title"))), 0);
								return;
							}
						}						
					}
					else
					{
						cf.ShowToast("Please select value for "+cf.decode(c.getString(c.getColumnIndex("R_RCN_D_title"))), 0);
						return;
					}
				}
			}
		}
		if(c!=null)
			c.close();
		save_RCN();
	}
	private void save_RCN() {
		// TODO Auto-generated method stub
		String scope="";
		for(int i=0;i<Chk_Scope.length;i++ )
		{
			if(Chk_Scope[i].isChecked())
			{
				scope+=Chk_Scope[i].getText().toString().trim()+",";
			}
		}
		System.out.println("scope="+scope);
		Cursor c1 =cf.SelectTablefunction(cf.Roof_additional, " WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+Roof_page_value[5]+"' and (R_ADD_saved='1' or R_ADD_saved='2') ");
		System.out.println("c1="+c1.getCount());
	/*	if(ADD_chk_top.isChecked()|| Roof_page_value[5].equals("7"))
		{*/
			 c1 =cf.SelectTablefunction(cf.RCN_table, " WHERE R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id='"+Roof_page_value[5]+"'");
			 System.out.println("c1RCN="+c1.getCount());
			if(c1.getCount()>0 || RCN_chk_top.isChecked())
			{
				/*if(!RC_cmt.getText().toString().trim().equals(""))
				{*/ 
					Cursor c3 =cf.SelectTablefunction(cf.RCN_table, " WHERE R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id='"+Roof_page_value[5]+"' ");
					
					if(c3.getCount()>0)
					{
						cf.arr_db.execSQL(" UPDATE "+cf.RCN_table+" SET R_RCN_saved='1' ,R_RCN_na='"+RCN_chk_top.isChecked()+"'  WHERE R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id='"+Roof_page_value[5]+"'");
					}
					else
					{
						cf.arr_db.execSQL(" INSERT INTO "+cf.RCN_table+" (R_RCN_na,R_RCN_Ssrid,R_RCN_insp_id,R_RCN_saved) VALUES ('"+RCN_chk_top.isChecked()+"','"+cf.selectedhomeid+"','"+Roof_page_value[5]+"','1')");
					}
					
					Cursor c2=cf.SelectTablefunction(cf.Roof_additional, " WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+Roof_page_value[5]+"'");
					System.out.println("c1RADD="+c2.getCount());
					if(c2.getCount()>0)
					{
						System.out.println(" UPDATE "+cf.Roof_additional+" SET R_ADD_saved='2',R_RCT_na='"+RCT_chk_top.isChecked()+"', R_ADD_scope='"+cf.encode(scope)+"',R_ADD_na='"+ADD_chk_top.isChecked()+"',R_ADD_full='',R_ADD_comment='"+cf.encode(RC_cmt.getText().toString().trim())+"',R_ADD_citizen='"+((CheckBox)findViewById(R.id.includeCitizenForm)).isChecked()+"'  WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+Roof_page_value[5]+"'");
						cf.arr_db.execSQL(" UPDATE "+cf.Roof_additional+" SET R_ADD_saved='2',R_RCT_na='"+RCT_chk_top.isChecked()+"', R_ADD_scope='"+cf.encode(scope)+"',R_ADD_na='"+ADD_chk_top.isChecked()+"',R_ADD_comment='"+cf.encode(RC_cmt.getText().toString().trim())+"',R_ADD_citizen='"+((CheckBox)findViewById(R.id.includeCitizenForm)).isChecked()+"'  WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+Roof_page_value[5]+"'");
					}
					else
					{
						System.out.println(" INSERT INTO "+cf.Roof_additional+" (R_RCT_na,R_ADD_scope,R_ADD_na,R_ADD_comment,R_ADD_citizen,R_ADD_Ssrid,R_ADD_insp_id,R_ADD_saved) Values ('"+RCT_chk_top.isChecked()+"','"+cf.encode(scope)+"','"+ADD_chk_top.isChecked()+"','"+cf.encode(RC_cmt.getText().toString().trim())+"','"+((CheckBox)findViewById(R.id.includeCitizenForm)).isChecked()+"','"+cf.selectedhomeid+"','"+Roof_page_value[5]+"','2')");
							cf.arr_db.execSQL(" INSERT INTO "+cf.Roof_additional+" (R_RCT_na,R_ADD_scope,R_ADD_na,R_ADD_comment,R_ADD_citizen,R_ADD_Ssrid,R_ADD_insp_id,R_ADD_saved) Values ('"+RCT_chk_top.isChecked()+"','"+cf.encode(scope)+"','"+ADD_chk_top.isChecked()+"','"+cf.encode(RC_cmt.getText().toString().trim())+"','"+((CheckBox)findViewById(R.id.includeCitizenForm)).isChecked()+"','"+cf.selectedhomeid+"','"+Roof_page_value[5]+"','2')");	
					}
					
					
				//}
			}
		//}
					String s=(add_rg.getCheckedRadioButtonId()!=-1)? add_rg.findViewById(add_rg.getCheckedRadioButtonId()).getTag().toString():"";		
					Cursor c5 =cf.SelectTablefunction(cf.Roof_additional, " WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+Roof_page_value[5]+"'");
					System.out.println("c5="+c5.getCount());
					if(c5.getCount()>0)
					{
						System.out.println("c5="+c5.getCount());
						cf.arr_db.execSQL(" UPDATE "+cf.Roof_additional+" SET R_ADD_na='"+ADD_chk_top.isChecked()+"',R_ADD_full='"+cf.encode(s)+"',R_ADD_saved='2',R_ADD_SCOPE='"+cf.encode(scope)+"' WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+Roof_page_value[5]+"'");
					}
					else
					{
						cf.arr_db.execSQL(" INSERT INTO "+cf.Roof_additional+" (R_ADD_Ssrid,R_ADD_insp_id,R_ADD_full,R_ADD_saved,R_ADD_scope) VALUES ('"+cf.selectedhomeid+"','"+Roof_page_value[5]+"','"+cf.encode(s)+"','2','"+cf.encode(scope)+"')");
					}
					System.out.println("all complet");
		Cursor c =cf.SelectTablefunction(cf.RCN_table, " WHERE R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id='"+Roof_page_value[5]+"' ");
		String OCR=get_rg_value(RCN_RG[0],RCN_other[0]),
			   RRN=get_rg_value(RCN_RG[1],RCN_other[1]),
			   VSL=get_rg_value(RCN_RG[2],RCN_other[2]),
			   VSTDS=get_rg_value(RCN_RG[3],RCN_other[3]),
			   VSCBS=get_rg_value(RCN_RG[4],RCN_other[4]),
			   VSDTS=get_rg_value(RCN_RG[5],RCN_other[5]),
			   VDRS=get_rg_value(RCN_RG[6],RCN_other[6]),
			   ODN=get_rg_value(RCN_RG[7],RCN_other[7]);
		System.out.println("all t"+c.getCount());
		if(c.getCount()<=0)
		{
			System.out.println(" INSERT INTO "+cf.RCN_table+" (R_RCN_Ssrid,R_RCN_insp_id,R_RCN_OCR,R_RCN_RRN,R_RCN_VSL,R_RCN_VSTDS,R_RCN_VSCBS,R_RCN_VSDTS,R_RCN_VDRS,R_RCN_ODN) values" +
					"('"+cf.selectedhomeid+"','"+Roof_page_value[5]+"','"+OCR+"','"+RRN+"','"+VSL+"','"+VSTDS+"','"+VSCBS+"','"+VSDTS+"','"+VDRS+"','"+ODN+"') ");
			cf.arr_db.execSQL(" INSERT INTO "+cf.RCN_table+" (R_RCN_Ssrid,R_RCN_insp_id,R_RCN_OCR,R_RCN_RRN,R_RCN_VSL,R_RCN_VSTDS,R_RCN_VSCBS,R_RCN_VSDTS,R_RCN_VDRS,R_RCN_ODN) values" +
					"('"+cf.selectedhomeid+"','"+Roof_page_value[5]+"','"+OCR+"','"+RRN+"','"+VSL+"','"+VSTDS+"','"+VSCBS+"','"+VSDTS+"','"+VDRS+"','"+ODN+"') ");
		}
		else
		{
			cf.arr_db.execSQL(" UPDATE "+cf.RCN_table+" SET R_RCN_na='"+RCN_chk_top.isChecked()+"',R_RCN_OCR='"+OCR+"',R_RCN_RRN='"+RRN+"',R_RCN_VSL='"+VSL+"',R_RCN_VSTDS='"+VSTDS+"',R_RCN_VSCBS='"+VSCBS+"',R_RCN_VSDTS='"+VSDTS+"',R_RCN_VDRS='"+VDRS+"',R_RCN_ODN='"+ODN+"',R_RCN_saved='1' WHERE  R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id='"+Roof_page_value[5]+"' ");
		}
		/***Dynamic table enty***/
		c=cf.SelectTablefunction(cf.RCN_table_dy, " WHERE R_RCN_D_Ssrid='"+cf.selectedhomeid+"' and R_RCN_D_insp_id='"+Roof_page_value[5]+"' ");
		if(c.getCount()>0)
		{
			c.moveToFirst();
			TableLayout tbl_dy=((TableLayout)findViewById(R.id.RCN_Condition_N_DY_tbl));
			for(int i=0;i<c.getCount();i++,c.moveToNext())
			{
				String option="",other="";
				int id=c.getInt(c.getColumnIndex("id"));
				if(tbl_dy.findViewById(id+1000)!=null)
				{
					RadioGroup rl =(RadioGroup) tbl_dy.findViewById(id+1000);
					if(rl.getCheckedRadioButtonId()!=-1)
					{
						option= findViewById(rl.getCheckedRadioButtonId()).getTag().toString();
						if(tbl_dy.findViewById(id+2000)!=null)
						{
							other =((EditText) tbl_dy.findViewById(id+2000)).getText().toString().trim();
							
						}
						
					}
					
				}
				cf.arr_db.execSQL(" UPDATE "+cf.RCN_table_dy+" SET  R_RCN_D_saved='1',R_RCN_D_option='"+cf.encode(option)+"',R_RCN_D_Other='"+cf.encode(other)+"' WHERE id='"+id+"'");
			}
		}
		findViewById(R.id.RCN_comp).setVisibility(View.VISIBLE);
	//	cf.ShowToast("Roof information saved successfully", 1);
		checkforsurveyaleret();
		//movetonext();
	}
	private String get_rg_value(RadioGroup radioGroup, EditText editText) {
		// TODO Auto-generated method stub
		if(radioGroup.getCheckedRadioButtonId()!=-1)
		{
			String val=radioGroup.findViewById(radioGroup.getCheckedRadioButtonId()).getTag().toString();
			if(editText.getVisibility()==View.VISIBLE)
			{
				if(!editText.getText().toString().trim().equals(""))
				{
					val+="#&45"+editText.getText();
				}
			}
			return cf.encode(val);
		}
		else
		{
		 return "";	
		}
	}
	private void Add_more_condition() {
		// TODO Auto-generated method stub
		final Dialog dialog = new Dialog(Roof_section_common.this,android.R.style.Theme_Translucent_NoTitleBar);
		dialog.getWindow().setContentView(R.layout.alert);
		dialog.findViewById(R.id.Add_caption).setVisibility(View.VISIBLE);
	//	dialog.findViewById(R.id.main_head).setVisibility(View.GONE);
		((TextView) dialog.findViewById(R.id.add_caption_tit)).setText("Enter title");//Roof Condition Noted question
		final EditText input =(EditText)dialog.findViewById(R.id.caption_text);
		((TextView) dialog.findViewById(R.id.EN_txtid1)).setText("Enter Roof Condition Noted question title");//
		
		InputFilter[] FilterArray;
		FilterArray = new InputFilter[1];
		FilterArray[0] = new InputFilter.LengthFilter(50);
		input.setFilters(FilterArray);
		Button save=(Button)dialog.findViewById(R.id.caption_save);
		Button can=(Button)dialog.findViewById(R.id.caption_cancel);
		ImageView cls=(ImageView)dialog.findViewById(R.id.caption_close);
		cls.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.setCancelable(true);
				dialog.cancel();

			}
		});
		save.setOnClickListener(new OnClickListener() {							
			@Override
			public void onClick(View v) {
				
				String title=input.getText().toString().trim();
				if(!title.equals(""))
				{
					
				Cursor c=	cf.SelectTablefunction(cf.RCN_table_dy, " WHERE R_RCN_D_Ssrid='"+cf.selectedhomeid+"' and R_RCN_D_insp_id='"+Roof_page_value[5]+"' and R_RCN_D_title='"+cf.encode(title)+"'");
				if(c.getCount()<=0)
				{
					cf.arr_db.execSQL(" INSERT INTO "+cf.RCN_table_dy+" (R_RCN_D_Ssrid,R_RCN_D_insp_id,R_RCN_D_title,R_RCN_D_option,R_RCN_D_Other) VALUES" +
							" ('"+cf.selectedhomeid+"','"+Roof_page_value[5]+"','"+cf.encode(title)+"','BSI','' )");
				 c =cf.SelectTablefunction(cf.RCN_table_dy, " WHERE R_RCN_D_Ssrid='"+cf.selectedhomeid+"' and R_RCN_D_insp_id='"+Roof_page_value[5]+"' and R_RCN_D_title='"+cf.encode(title)+"'");
				 c.moveToFirst();
					Show_dynamic_RCN(c);
					dialog.setCancelable(true);
					dialog.cancel();
				}
				else
				{
					cf.ShowToast("Already Exists! Please enter a different title", 0);
				}
				if(c!=null)
					c.close();
				}
				else
				{
					cf.ShowToast("Please enter Roof Condition Noted question Title", 0);
					
				}
				
					
				/*id INTEGER PRIMARY KEY AUTOINCREMENT, R_RCN_D_Ssrid VARCHAR( 5 ) DEFAULT ( '' ), R_RCN_D_insp_id VARCHAR( 5 ) DEFAULT ( '' ), R_RCN_D_title VARCHAR( 50 ) DEFAULT ( '' ), R_RCN_D_option VARCHAR( 15 ) DEFAULT ( '' ), R_RCN_D_Other VARCHAR( 250 ) DEFAULT ( '' ), R_RCN_D_saved VARCHAR( 1 ) DEFAULT ( '0' ))");*/
			}
		});
		
		can.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
						dialog.setCancelable(true);
						dialog.cancel();
						
					}
				});
		dialog.setCancelable(false);
		dialog.show();
	}
	protected void Show_dynamic_RCN() {
		// TODO Auto-generated method stub
		String option_arr[]={"Yes","No","N/D","BSI","N/A"};
		Cursor c=cf.SelectTablefunction(cf.RCN_table_dy, " WHERE R_RCN_D_Ssrid='"+cf.selectedhomeid+"' and R_RCN_D_insp_id='"+Roof_page_value[5]+"' ");
		TableLayout tbl_dy=((TableLayout)findViewById(R.id.RCN_Condition_N_DY_tbl));
		tbl_dy.removeAllViews();
		if(c.getCount()>0)
		{
			c.moveToFirst();
			for(int i=0;i<c.getCount();i++,c.moveToNext())
			{
				Show_dynamic_RCN(c);
			}
		}
	}
	protected void Show_dynamic_RCN(Cursor c) {
		// TODO Auto-generated method stub
		String option_arr[]={"Yes","No","N/D","BSI","N/A"};
		TableLayout tbl_dy=((TableLayout)findViewById(R.id.RCN_Condition_N_DY_tbl));
		
		if(c.getCount()>0)
		{
		
				int id=c.getInt(c.getColumnIndex("id"));
				String title=cf.decode(c.getString(c.getColumnIndex("R_RCN_D_title"))),
				option=cf.decode(c.getString(c.getColumnIndex("R_RCN_D_option"))),
				other=cf.decode(c.getString(c.getColumnIndex("R_RCN_D_Other")));
				TableRow tbl=new TableRow(this);
	    		tbl.setId(id);/***Set the dynamic id for the Each Row for retrive the value ***/
				TextView tit_tv=new TextView(this,null,R.attr.Roof_RCN_Tit_txt);
	    		tit_tv.setWidth(0);
	    		TextView tit_star=new TextView(this,null,R.attr.star);
				tit_star.setTag("Star");
				tit_star.setPadding(5, 0, 5, 0);
				tbl.addView(tit_star);
	    		tit_star.setVisibility(View.VISIBLE);
	    		tit_tv.setText(title);
				tit_tv.setTag("Title_txt");
	    		tbl.addView(tit_tv,250,LayoutParams.WRAP_CONTENT);
	    		TextView colon_tv=new TextView(this,null,R.attr.colon_withoutmargin);
	    		colon_tv.setPadding(0, 0, 0, 0);
	    		tbl.addView(colon_tv);
	    		TableRow.LayoutParams clp=(android.widget.TableRow.LayoutParams) colon_tv.getLayoutParams();
	    		clp.setMargins(0, 0, 0, 0);
	    		LinearLayout li= new LinearLayout(this);
	    		li.setOrientation(LinearLayout.VERTICAL);
	    		RadioGroup rg= new RadioGroup(this);
	    		rg.setId(id+1000);
	    		li.addView(rg);
	    		rg.setOrientation(RadioGroup.HORIZONTAL);
	    		tbl.addView(li);
	    		
	    		EditText ed =new EditText(this,null,R.attr.Roof_RCN_yes_ed);
	    		ed.setTag("Comments");
	    		ed.setId(id+2000);
	    		InputFilter[] FilterArray;
	    		FilterArray = new InputFilter[1];
	    		FilterArray[0] = new InputFilter.LengthFilter(250);
	    		ed.setFilters(FilterArray);
	    		li.addView(ed, 500, LayoutParams.WRAP_CONTENT);
	    		TextView yes_tv=new TextView(this,null,R.attr.Roof_RCN_yes_txt);
	    		yes_tv.setTag("Comments_txt");
	    		li.addView(yes_tv);
	    		rg.setOnCheckedChangeListener(new RCN_click(ed, yes_tv, "Yes"));
	    		ImageView opt_img=new ImageView(this,null,R.attr.Roof_RCN_opt_img);
	    		tbl.addView(opt_img);
	    		opt_img.setVisibility(View.VISIBLE);
	    		opt_img.setOnClickListener(new RCN_D_delete(id));
	    		ed.setText(other);
	    		tbl_dy.addView(tbl);
	    		for(int m =0;m<option_arr.length;m++)
	    		{
	    			
	    			RadioButton rb= new RadioButton(this,null,R.attr.Roof_RCN_opt_rb);
	    			rb.setText(option_arr[m]);
	    			rb.setTag(option_arr[m]);
	    			rg.addView(rb,100,LayoutParams.WRAP_CONTENT);
	    			if(option_arr[m].equals(option))
	    			{
	    				rb.setChecked(true);	
	    			}
	    		}
	    		//ed.setVisibility(View.GONE);
	    		ed.setOnTouchListener(new OnTouchListener() {
					
					@Override
					public boolean onTouch(View v, MotionEvent event) {
						// TODO Auto-generated method stub
						((EditText) v).setFocusable(true);
				    	((EditText) v).setFocusableInTouchMode(true);
				    	((EditText) v).requestFocus();
				    	((EditText) v).setSelection(((EditText) v).length());   
						return false;
					}
				});
			
		}
	}
	class RCN_D_delete implements OnClickListener
	{
		int id;

		public RCN_D_delete(int id) {
			// TODO Auto-generated constructor stub
			this.id=id;
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			final AlertDialog.Builder ab =new AlertDialog.Builder(Roof_section_common.this);
			ab.setTitle("Confirmation");
			ab.setMessage(Html.fromHtml("Do you want to delete the selected Roof Condition Noted question?"));
			ab.setIcon(R.drawable.alertmsg);
			ab.setCancelable(false);
			ab.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					cf.arr_db.execSQL(" DELETE FROM "+cf.RCN_table_dy+" WHERE id='"+id+"'");
					cf.ShowToast("Roof Condition Noted question deleted successfully ", 0);
					TableLayout tbl_dy=((TableLayout)findViewById(R.id.RCN_Condition_N_DY_tbl));
					tbl_dy.removeView(tbl_dy.findViewById(id));
					
					
					//Show_dynamic_RCN();
				}
				
			})
			.setNegativeButton("No", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					
				}
			});
			AlertDialog al=ab.create();
			al.show();
		}
		
	}
	/**RCN table function end***/
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if(requestCode==14)
		{
		switch (resultCode) {
				case 10:
					cf.ShowToast("You have cancelled the generate Citizen Certificate ", 0);
					movetonext();
				break;
				case RESULT_OK:
				/**Before that need to generate the form throw the web service**/
					System.out.println("RESULT_OK 10");
				cf.ShowToast("You have generated Citizen Certificate", 0);
			
				movetonext();
				break;
				
		}
		}
		else if(requestCode==pick_img_code)
		{
		String cv="";
			switch (resultCode) {
				case RESULT_CANCELED:
				//	cf.ShowToast("You have canceld the Roof cover image selection ", 0);
					cv=data.getExtras().getString("Cover");
					//((EditText)findViewById(id1).findViewWithTag("Path")).setText("");
				break;
				case RESULT_OK:
					/*cf.ShowToast("The image uploaded successfully.",1);*/
				
					
					cv=data.getExtras().getString("Cover");
					//((EditText)findViewById(id).findViewWithTag("Path")).setText(data.getExtras().getString("path").trim());
				break;
				
			}
			try
			{
				
		    		Cursor c=cf.arr_db.rawQuery("Select * from "+cf.Roofcoverimages+" Where RIM_masterid=(Select RM_masterid from "+cf.Roof_master+
		    				" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+Roof_page_value[5]+"' and RM_Covering='"+cf.encode(cv)+"') order by RIM_Order", null);
		    		
		    		if(c.getCount()>0)
		    		{
		    			tv.setVisibility(View.VISIBLE);
		    			tv.setText(Html.fromHtml("<u>View/Edit Photos ("+c.getCount()+")</u>"));
		    		
				    	
		    		}
		    		else
		    			{
		    				System.out.println("null  only cames");
		    				tv.setVisibility(View.INVISIBLE);
		    			}
		    			
		    		
		    	
		    }
		    catch (Exception e) {
				// TODO: handle exception
			}
		}
		else if(requestCode==cf.loadcomment_code)
		{
			load_comment=true;
			if(resultCode==RESULT_OK)
			{
				RC_cmt.setText((RC_cmt.getText().toString()+" "+data.getExtras().getString("Comments")).trim());
				
			}
			
			
				
		}

	}
	private void movetonext() {
		// TODO Auto-generated method stub
		System.out.println("cf.Roof="+cf.Roof);
		 if(cf.Roof.equals(cf.Roof_insp[0]) )
		 {
			boolean  s=false;
		 /*** For 4poin***/
			
			 /***For hide the attic if the External only selected menas ends**/
		
				cf.goclass(23);
		
		 }
		 else if(cf.Roof.equals(cf.Roof_insp[1]))
		 {
			
				cf.goclass(29);
			
		 }
		 else  if(cf.Roof.equals(cf.Roof_insp[4]))
		 {
				
		 /*** For GCH***/
			
			 cf.goclass(47);
		/***	 For GCH Ends***/
		 }
		 else if(cf.Roof.equals(cf.getResourcesvalue(R.string.insp_5)))
		 {
			 
			/*** Roof Survey***/
				/*if(cf.identityval==33 || cf.identityval==34)
				{*/
					cf.gotoclass(33, WindMitCommAuxbuildings.class);
				//}
				
			 /****Roof Survay ends**/
		 } 
		 else if (cf.Roof.equals(cf.getResourcesvalue(R.string.insp_6)))
		 {
			 cf.gotoclass(34, WindMitCommAuxbuildings.class);
		 }
		 else if (cf.Roof.equals(cf.getResourcesvalue(R.string.insp_4)))
		 {
			 //cf.gotoclass(32, RoofSection.class);
			 cf.gotoclass(32, WindAddendumcomm.class);
			 
		 }
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			
			Intent  myintent=null;
			if(cf.Roof.equals(cf.Roof_insp[0]))
			 {
			    /*** For 4poin***/
				 cf.goback(0);	 
			 }
			 else if(cf.Roof.equals(cf.Roof_insp[1]))
			 {
				/*** Roof Survey***/
				 cf.chk_InspTypeQuery(cf.selectedhomeid,cf.Roof_insp[0]);
			
				 if(cf.isaccess)
				 {
					 cf.Roof=cf.Roof_insp[0];
					 cf.goback(1);
				 
				 }
				 else
				 {
					 cf.goback(0);
				 }
			        
			 }
			 else  if(cf.Roof.equals(cf.Roof_insp[4]))
			 {
					
			 /*** For GCH***/
				
				 cf.goclass(416);
			/***	 For GCH Ends***/
			 }
			 else if(cf.Roof.equals(cf.getResourcesvalue(R.string.insp_5)))
			 {
				
				/*** Roof Survey***/
				/*if(cf.identityval==33 || cf.identityval==34)
				{*/
			 
				 cf.identityval=33; 
			 
					cf.gotoclass(cf.identityval, WindMitCommareas.class);
				//}
				
				 /****Roof Survay ends**/
			 }
			 else if(cf.Roof.equals(cf.getResourcesvalue(R.string.insp_6)))
			 {
				
				/*** Roof Survey***/
				/*if(cf.identityval==33 || cf.identityval==34)
				{*/
			 
				 cf.identityval=34; 
			 
					cf.gotoclass(cf.identityval, WindMitCommareas.class);
				//}
				
				 /****Roof Survay ends**/
			 }
			 else if(cf.Roof.equals(cf.getResourcesvalue(R.string.insp_4)))
			 {
				
				/*** Roof Survey***/
				/*if(cf.identityval==33 || cf.identityval==34)
				{*/
			 
				 cf.identityval=32; 
			 
					cf.gotoclass(cf.identityval, WindMitCommareas.class);
				//}
				
				 /****Roof Survay ends**/
			 }
			 else 
			 {
				 /*** For GCH***/
				 cf.goback(0);
				 
				 
			/***	 For GCH Ends***/ 
			 }
			
		return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}	
	/**For citizen genration form***/
	protected boolean generate_form()throws SocketException, NetworkErrorException, TimeoutException, IOException, XmlPullParserException {
		// TODO Auto-generated method stub
		
		cf.request=cf.SoapRequest("GENERATECITIZENFORMPDF");
		cf.request.addProperty("SRID", cf.selectedhomeid);
		cf.request.addProperty("Inspectionid", Roof_page_value[5]);
		cf.request.addProperty("InspectorID", cf.Insp_id);
		String sql=" Select ph.ARR_PH_FirstName||' '||ph.ARR_PH_LastName as ph_Name, ph.ARR_PH_Address1||' '||ph.ARR_PH_Address2 as ph_Address,ph.ARR_PH_Policyno,ph.ARR_Schedule_ScheduledDate From  "+cf.policyholder+" as ph WHERE ARR_PH_SRID='"+cf.selectedhomeid+"'";
		Cursor cs=cf.arr_db.rawQuery(sql, null);
		System.out.println("sql"+cs.getCount()+"sql="+sql);
		cs.moveToFirst();
		if(cs.getCount()>0)
		{
			cf.request.addProperty("Insuredname", cf.decode(cs.getString(cs.getColumnIndex("ph_Name"))));
			cf.request.addProperty("Policyno", cf.decode(cs.getString(cs.getColumnIndex("ARR_PH_Policyno"))));
			cf.request.addProperty("Inspectedaddress", cf.decode(cs.getString(cs.getColumnIndex("ph_Address"))));
			cf.request.addProperty("Dateofinspection", cf.decode(cs.getString(cs.getColumnIndex("ARR_Schedule_ScheduledDate"))));
			
		System.out.println("here="+cf.request);
		}
		
		/***Roof covering type value **/
		sql=" SELECT * FROM "+cf.Rct_table+" WHERE RCT_Ssrid='"+cf.selectedhomeid+"' and RCT_insp_id='"+Roof_page_value[5]+"' and RCT_pre='true'";
		System.out.println("the sql1"+sql);
		cs=cf.arr_db.rawQuery(sql, null);
		System.out.println(sql);
		cs.moveToFirst();System.out.println("vvv="+cs.getCount());
		String roof_cover="",appr="",roof_age="",date="";
		if(cs.getCount()>0)
		{
			System.out.println("inside");
			roof_cover=cf.decode(cs.getString(cs.getColumnIndex("RCT_rct")));
			date=cf.decode(cs.getString(cs.getColumnIndex("RCT_PAD")));
			roof_age=cf.decode(cs.getString(cs.getColumnIndex("RCT_yoi")));
			appr=cf.decode(cs.getString(cs.getColumnIndex("RCT_arfl")));
			if(roof_cover.equals("Other"))
			{
				roof_cover=cf.decode(cs.getString(cs.getColumnIndex("RCT_other_rct")));
			}
			if(roof_age.equals("Other"))
			{
				roof_age=cf.decode(cs.getString(cs.getColumnIndex("RCT_other_yoi")));
			}
			if(appr.equals("Other"))
			{
				appr=cf.decode(cs.getString(cs.getColumnIndex("RCT_other_aRFL")));
			}
		}
		System.out.println("insidecc="+cf.request);
		
		cf.request.addProperty("Roofcovering", roof_cover);
		cf.request.addProperty("datelastupdate", date);
		cf.request.addProperty("Ageofroof", roof_age);
		cf.request.addProperty("remainingroof", appr);System.out.println("after="+cf.request);
		sql=" SELECT R_RCN_VSL,R_RCN_VSCBS FROM "+cf.RCN_table+" WHERE R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id='"+Roof_page_value[5]+"'";
		System.out.println("the sql1"+sql);
		cs=cf.arr_db.rawQuery(sql, null);
		cs.moveToFirst();
		if(cs.getCount()>0)
		{
			String VSL=cs.getString(cs.getColumnIndex("R_RCN_VSL")),VSL_o="",
			VSCBS=cs.getString(cs.getColumnIndex("R_RCN_VSCBS")),VSCBS_o="";
			if(!VSL.equals(""))
			{
				if(VSL.contains("#&45"))
				{
					String[]tem=VSL.split("#&45");
					VSL=tem[0];
					VSL_o=tem[1];
					cf.request.addProperty("Singofleaks", VSL);
					cf.request.addProperty("Singofleaksvalue", VSL_o);
					cf.request.addProperty("Singofleaksvalue", "");
				}
				else
				{
					cf.request.addProperty("Singofleaks", VSL);
					
				}
			}
			if(!VSCBS.equals(""))
			{
				if(VSCBS.contains("#&45"))
				{
					String[]tem=VSCBS.split("#&45");
					VSCBS=tem[0];
					VSCBS_o=tem[1];
					cf.request.addProperty("Singofdamaged", VSCBS);
					cf.request.addProperty("Singdamagedvalue", VSCBS_o);
				}else
				{
					cf.request.addProperty("Singofdamaged", VSCBS);
					cf.request.addProperty("Singdamagedvalue", "");
				}
			}
		}
		/*sql=" Select GRI.*,M.mst_text from "+cf.Roof_cover_type+" GRI LEFT JOIN "+cf.master_tbl+" M on GRI.RCT_TypeValue=m.mst_custom_id and M.mst_module_id='2' WHERE RCT_masterid=(SELECT RCT_masterid from "+cf.Roof_cover_type+" Where RCT_TypeValue='11' and RCT_masterid in " +
				"(SELECT RM_masterid from "+cf.Roof_master+" where RM_insp_id='2' and RM_srid='"+cf.selectedhomeid+"' and RM_module_id='0') and RCT_FieldVal='true' ) "; 
		System.out.println("the sql1"+sql);
		cs=cf.arr_db.rawQuery(sql, null);
		System.out.println(sql);
		cs.moveToFirst();
		String roof_cover="",appr="",roof_age="",date="";
		if(cs.getCount()>0)
		{
			do
			{
				
				
				if(cs.getString(cs.getColumnIndex("RCT_TypeValue")).equals("1"))
				{
					roof_cover=cf.decode(cs.getString(cs.getColumnIndex("RCT_FieldVal")));
					
				}
				if(cs.getString(cs.getColumnIndex("RCT_TypeValue")).equals("2"))
				{
					date=cf.decode(cs.getString(cs.getColumnIndex("RCT_FieldVal")));
					
					
				}
				if(cs.getString(cs.getColumnIndex("RCT_TypeValue")).equals("3"))
				{
					roof_age=cf.decode(cs.getString(cs.getColumnIndex("RCT_FieldVal")));
					
				}
				if(cs.getString(cs.getColumnIndex("RCT_TypeValue")).equals("10"))
				{
					appr=cf.decode(cs.getString(cs.getColumnIndex("RCT_FieldVal")));
					
				}
			}
			while(cs.moveToNext());
			cf.request.addProperty("Roofcovering", roof_cover);
			cf.request.addProperty("datelastupdate", date);
			cf.request.addProperty("Ageofroof", roof_age);
			cf.request.addProperty("remainingroof", appr);
		}
		
		*//***Roof covering type value  **//*
		*//**Roof condition noted value **//*
		sql=" Select GRI.*,M.mst_text from "+cf.General_Roof_information+" GRI LEFT JOIN "+cf.master_tbl+" M on GRI.RC_Condition_type=m.mst_custom_id and M.mst_module_id='2' WHERE RC_masterid=" +
				"(SELECT RM_masterid from "+cf.Roof_master+" where RM_insp_id='2' and RM_srid='"+cf.selectedhomeid+"' and RM_module_id='1') and (RC_Condition_type='16' or RC_Condition_type='18')  "; 
		System.out.println("the sql1"+sql);
		cs=cf.arr_db.rawQuery(sql, null);
		cs.moveToFirst();
		if(cs.getCount()>0)
		{
			
			do
			{
				
				String val=cf.decode(cs.getString(cs.getColumnIndex("RC_Condition_type_val"))),
						desc=cf.decode(cs.getString(cs.getColumnIndex("RC_Condition_type_DESC"))),
						tit=cf.decode(cs.getString(cs.getColumnIndex("RC_Condition_type")));
				if(tit.trim().equals("18"))
				{
					
					
					cf.request.addProperty("Singofdamaged", val);
					cf.request.addProperty("Singdamagedvalue", desc);
					
					
					
					
				}
				if(tit.trim().equals("16"))
				{
					cf.request.addProperty("Singofleaks", val);
					cf.request.addProperty("Singofleaksvalue", desc);
					
				}
			}while(cs.moveToNext());
		}*/
		String timeStamp = new SimpleDateFormat("MM/dd/yyyy").format(Calendar.getInstance().getTime());
		cf.request.addProperty("Date", timeStamp);
		System.out.println("the request "+cf.request);
		boolean result=cf.SoapResponse("GENERATECITIZENFORMPDF", cf.request);
		System.out.println(" the response"+result);
		/**Roof condition noted value ends **/
		return result;
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		boolean res=false;
		try
		{
					res=generate_form();
				
		}
		catch (SocketException e) {
			// TODO: handle exception
			
		}
		catch (NetworkErrorException e) {
			// TODO: handle exception
			
		}
		catch (TimeoutException e) {
			// TODO: handle exception
			
		}
		catch (IOException e) {
			// TODO: handle exception
			
		}
		catch (XmlPullParserException e) {
			// TODO: handle exception
			
		}
		catch (Exception e) {
			// TODO: handle exception
			
		}
		if(res)
		{
			show_handler=1;
		}
		else
		{
			show_handler=2;
			//movetonext();
		}
		handler.sendEmptyMessage(0);
	}
	private Handler handler = new Handler() {
		

		public void handleMessage(Message msg) {
			pd.dismiss();
			if(show_handler==1)
			{

				
					cf.ShowToast("Citizen Certification Form generated successfully. ", 0);
					movetonext();
				
			}
			else if(show_handler==2)
			{
				cf.ShowToast(" Network unavailable! Unable to generate Citizen Certification Form. Please try again later", 0);
			}
				
		}
	};
	private void checkforsurveyaleret() {
		// TODO Auto-generated method stub
		System.out.println("Roof_page_value[5]"+Roof_page_value[5]);
		if(((CheckBox)findViewById(R.id.includeCitizenForm)).isChecked() && (Roof_page_value[5].equals("1") || Roof_page_value[5].equals("2")))
		//if(Roof.equals(cf.Roof_insp[1]))
		{
			/****Alert to get the confirmation from the user for generate the citizen form**/
			AlertDialog.Builder alt_bld = new AlertDialog.Builder(this);
			AlertDialog alert = null;
			alt_bld.setMessage("Roof Information saved successfully .\nTo create a Citizens Roof Certification Form, an internet connection is required. This can be done later online before submitting or when you have an internet connection.")
			.setCancelable(false)
			.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			

			

			public void onClick(DialogInterface dialog, int id) {
			// Action for 'Yes' Button
				try
				{
				
					ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
					NetworkInfo info = conMgr.getActiveNetworkInfo();
					if(cf.isInternetOn() && info!=null)
					{
						
							dialog.cancel();
							String source = "<b><font color=#00FF33>Generating Citizen Certification Form. Please wait..."
									+ "</font></b>";
								pd = ProgressDialog.show(Roof_section_common.this, "", Html.fromHtml(source), true);
								Thread td =new Thread(Roof_section_common.this);
								td.start();
									//res=generate_form();
								
					
					}
					else
					{
						cf.ShowToast("There is a problem on your network. Please try again!", 0);
					}
				//	movetonext();
					//dialog.cancel();
				}catch (Exception e) {
					// TODO: handle exception
					System.out.println("The problem in the inserting the selected inspection"+e.getMessage());
				}
			}
			})
			.setNegativeButton("No", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
			//  Action for 'NO' Button
				//((Dialog) dialog).setCancelable(true);
				movetonext();
			dialog.cancel();
			}
			});
		
			// Title for AlertDialog
			alert = alt_bld.create();
			alert.setTitle("Citizen Certificate");
			// Icon for AlertDialog
			alert.setIcon(R.drawable.alertmsg);
			alert.show();
			alert.setCancelable(false);
			/****Alert to get the confirmation from the user for generate the citizen form Ends**/
		}
		else
		{
			cf.ShowToast("Roof Information saved successfully ", 1);
			movetonext();
		}	
	}
	/**For citizen genration form ends***/
}
