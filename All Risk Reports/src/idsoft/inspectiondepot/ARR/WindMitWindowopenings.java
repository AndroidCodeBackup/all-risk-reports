package idsoft.inspectiondepot.ARR;

import java.util.ArrayList;
import java.util.List;

import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.WindMitDooropenings.Spin_Selectedlistener;
import idsoft.inspectiondepot.ARR.WindMitSkylights.WindOpen_EditClick;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

public class WindMitWindowopenings extends Activity {
	CommonFunctions cf;
	int windopeningid,rwswindopen;
	Spinner spnelevwindopen,spnwindquantity,spnwindow,spnwindfloor,spnwindprot;
	String BI_WindowOpenings="",WindOpen_NA="",windopennotappl="0",elevspinnertxt="",windothertxt="";
	Cursor c1,c2;
	public String spnofqunatity[] = { "--Select--", "1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30"};
	String[] arrwindowopenings,arrwindelev,arrwindid,arrelevothertxt,arrwinothertxt;
	LinearLayout subpnlshwwindopen;TableLayout dynviewwinopen;
	ArrayAdapter elevswindadap,spnwindquantityadap,spnwindowadap,spnwindprotadap,spnwindflooradap;
	public String spnwindtype[]={"--Select--","Single Hung","Double Hung","Horizontal Hung","Awning/Jalousie","Casement","Fixed Glass","Other"};
	private List<String> items1= new ArrayList<String>();
	
	public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	       
	        cf = new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);				
		 	}
	        setContentView(R.layout.windowopenings);
	        cf.getDeviceDimensions();
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
	        if(cf.identityval==31)
	        {
	          hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","B1 1802(Rev.01/12)","Window Openings",3,1,cf));
	        }
	        else if(cf.identityval==32)
	        { 
	        	hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","Commercial Type I","Window Openings",3,1,cf));
	        }
			LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
	     	main_layout.setMinimumWidth(cf.wd);
	     	main_layout.addView(new MyOnclickListener(getApplicationContext(), 3, 0,0,cf));
	       
	        cf.type_submenulayout =(LinearLayout) findViewById(R.id.typesubmenu); //** WINDMIT TYPE SUBMENU **//
	        cf.type_submenulayout.addView(new MyOnclickListener(this, 300, 1,0, cf));
	        
	        cf.windmittypechk(cf.identityval,((RadioButton)findViewById(R.id.b1802_chk)),((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((TextView)findViewById(R.id.commtxt)),((ImageView)findViewById(R.id.editbno))); // To check the checkbox of choosen inspection type
	        cf.uncheckcommercial(((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((RadioButton)findViewById(R.id.b1802_chk))); 
	    	      
	        cf.submenu_layout = (LinearLayout) findViewById(R.id.submenu);
	        cf.submenu_layout.addView(new MyOnclickListener(this, cf.identityval, 1,315,cf));
	        
	        cf.mainlayout = (LinearLayout)findViewById(R.id.bc_table);
	      // LinearLayout submenu2_layout = (LinearLayout) findViewById(R.id.submenu1); 
	      //submenu2_layout.addView(new MyOnclickListener(getApplicationContext(), 458, 1,0,cf));
	      
	        cf.tblrw = (TableRow)findViewById(R.id.row2);
	        cf.tblrw.setMinimumHeight(cf.ht); 	
	        
	        cf.CreateARRTable(3);
	        cf.CreateARRTable(62);
	        cf.CreateARRTable(312);
	       
	        Declaration();   
	        Show_WindowOpenings_value();
	        setvalue();
	    }
	private void setvalue() {
		// TODO Auto-generated method stub		
		selecttbl();
		System.out.println("WIND="+WindOpen_NA);
		if(WindOpen_NA.equals("") || WindOpen_NA.equals("0"))
		{
			((LinearLayout)findViewById(R.id.windopeningslin)).setVisibility(cf.v1.VISIBLE);
			if(c2.getCount()>0)
			{
				((TextView)findViewById(R.id.savetxtwinopen)).setVisibility(cf.v1.VISIBLE);				
			}
			else
			{
				((TextView)findViewById(R.id.savetxtwinopen)).setVisibility(cf.v1.GONE);
				//((LinearLayout)findViewById(R.id.windopeningslin)).setVisibility(cf.v1.GONE);
			}
		}
		else
		{
			if(c2.getCount()>0)
			{
				((CheckBox)findViewById(R.id.windopennotappl)).setChecked(false);				
				((LinearLayout)findViewById(R.id.windopeningslin)).setVisibility(cf.v1.VISIBLE);
			}
			else
			{
				((CheckBox)findViewById(R.id.windopennotappl)).setChecked(true);
				((LinearLayout)findViewById(R.id.windopeningslin)).setVisibility(cf.v1.GONE);
			}			
			((TextView)findViewById(R.id.savetxtwinopen)).setVisibility(cf.v1.VISIBLE);			
		}
	}
	private void Declaration() {		
		// TODO Auto-generated method stub
		cf.setupUI((ScrollView) findViewById(R.id.scr));
		 dynviewwinopen = (TableLayout)findViewById(R.id.dynamicwindopen);
		 subpnlshwwindopen = (LinearLayout)findViewById(R.id.subpnlshowwindopen);
		
		 spnelevwindopen=(Spinner) findViewById(R.id.elevwindopen);
		 elevswindadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item,cf.spndoorelevation);
		 elevswindadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 spnelevwindopen.setAdapter(elevswindadap);
		 spnelevwindopen.setOnItemSelectedListener(new  Spin_Selectedlistener(1));
		 
		 spnwindquantity=(Spinner) findViewById(R.id.spnquantityywind);
		 spnwindquantityadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item,spnofqunatity);
		 spnwindquantityadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 spnwindquantity.setAdapter(spnwindquantityadap);
		 
		 spnwindow=(Spinner) findViewById(R.id.spnrwindow);
		 spnwindowadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item,spnwindtype);
		 spnwindowadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 spnwindow.setAdapter(spnwindowadap);
		 spnwindow.setOnItemSelectedListener(new  Spin_Selectedlistener(2));
		 
		 spnwindprot=(Spinner) findViewById(R.id.spnprotectwind);
		 spnwindprotadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item,cf.spnwindprottype);
		 spnwindprotadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 spnwindprot.setAdapter(spnwindprotadap);		 
		 try
		 {		
			 items1.clear();
		  	 cf.getPolicyholderInformation(cf.selectedhomeid);
				items1.add("--Select--");
				 for(int k=0;k<Integer.parseInt(cf.Stories);k++)
				 {
					 items1.add(cf.floorarr[k]+" Floor");
			     } 
			}
			catch(Exception e){}
		
		 spnwindfloor=(Spinner) findViewById(R.id.spnpfloorwind);
		 spnwindflooradap = new ArrayAdapter(this,android.R.layout.simple_spinner_item,items1);
		 spnwindflooradap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 spnwindfloor.setAdapter(spnwindflooradap);
		 
		 spnwindfloor.setSelection(1);
		 
	}
	class Spin_Selectedlistener implements OnItemSelectedListener
	{
		int i;
		public Spin_Selectedlistener(int i) {
			// TODO Auto-generated constructor stub
			this.i=i;
		}

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
			// TODO Auto-generated method stub
			if(i==1)
			{
				elevspinnertxt = spnelevwindopen.getSelectedItem().toString();
				if(elevspinnertxt.equals("Other Elevation"))
				{
					((EditText)findViewById(R.id.edtelevother)).setVisibility(cf.v1.VISIBLE);
				}
				else
				{
					((EditText)findViewById(R.id.edtelevother)).setText("");
					((EditText)findViewById(R.id.edtelevother)).setVisibility(cf.v1.GONE);
				}
			}
			else if(i==2)
			{
				windothertxt = spnwindow.getSelectedItem().toString();
				if(windothertxt.equals("Other"))
				{
					((EditText)findViewById(R.id.edtwindother)).setVisibility(cf.v1.VISIBLE);
				}
				else
				{
					((EditText)findViewById(R.id.edtwindother)).setText("");
					((EditText)findViewById(R.id.edtwindother)).setVisibility(cf.v1.GONE);
				}
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
	}
	private void selecttbl() {
		try
		{
			c1= cf.SelectTablefunction(cf.WindDoorSky_NA, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
			if(c1.getCount()>0)
			{
				c1.moveToFirst();
				WindOpen_NA = c1.getString(c1.getColumnIndex("fld_windowopenNA"));
			}
		}
		catch(Exception e)
		{
			
		}
		try
		{
			
			c2 =  cf.SelectTablefunction(cf.BI_Windopenings, " where fld_srid='"+cf.selectedhomeid+"' and insptypeid='"+cf.identityval+"'");
			
		}
		catch(Exception e)
		{
			System.out.println("dsgdfg"+e.getMessage());
		}
	}
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.mouseoverid:
			cf.ShowToast("Use only for Commercial Type I and Residential Wind Mitigation Inspections 1802 only.",0);
			break;
		case R.id.windowsave:
			selecttbl();
			if(((CheckBox)findViewById(R.id.windopennotappl)).isChecked()==false)
			{
				if(c2.getCount()==0)
				{
					cf.ShowToast("Please save Window Openings.", 0);	
				}
				else
				{
					cf.ShowToast("Window Openings saved successfully",0);
					((TextView)findViewById(R.id.savetxtwinopen)).setVisibility(cf.v1.VISIBLE);
					updatenotappl();
					cf.goclass(459);
				}
			}
			else
			{
				windopennotappl  ="1";
				try
				{
					cf.arr_db.execSQL("Delete from "+cf.BI_Windopenings+" Where fld_srid='"+cf.selectedhomeid+"' and insptypeid='"+cf.identityval+"'");					
				}
				catch(Exception e)
				{
					System.out.println("deleting error="+e.getMessage());
				}
				updatenotappl();
				cf.goclass(459);
			}
			break;
		case R.id.windowopencancel:
			clearwindowopenings();						
			break;
		case R.id.windowopensave:
			selecttbl();
			boolean b=((spnelevwindopen.getSelectedItem().toString().equals("Other Elevation")) && ((EditText)findViewById(R.id.edtelevother)).getText().toString().trim().equals("")) ? false:true;
			boolean c=((spnwindow.getSelectedItem().toString().equals("Other")) && ((EditText)findViewById(R.id.edtwindother)).getText().toString().trim().equals("")) ? false:true;
			if(((CheckBox)findViewById(R.id.windopennotappl)).isChecked()==false)
			{
				if(!spnelevwindopen.getSelectedItem().toString().equals("--Select--") && b==true)
				{
					if(!spnwindquantity.getSelectedItem().toString().equals("--Select--"))
					{	
						if(!spnwindow.getSelectedItem().toString().equals("--Select--") && c==true)
						{
							if(!spnwindfloor.getSelectedItem().toString().equals("--Select--"))
							{
								if(!spnwindprot.getSelectedItem().toString().equals("--Select--"))
								{
									InsertWindowOpenings();
								}
								else
								{
									cf.ShowToast("Please select the Protection Codes.", 0);	
								}
							}
							else
							{
								cf.ShowToast("Please select the Floor.", 0);	
							}
						}
						else
						{
							if(spnwindow.getSelectedItem().toString().equals("--Select--"))
							{
								cf.ShowToast("Please select the Window.", 0);	
							}
							else if(c==false)
							{
								cf.ShowToast("Please select the other text for Window.", 0);	
								cf.setFocus(((EditText)findViewById(R.id.edtwindother)));
							}	
						}
					}
					else
					{
						cf.ShowToast("Please select the No. of Quantity.", 0);	
					}
				}
				else
				{
					if(spnelevwindopen.getSelectedItem().toString().equals("--Select--"))
					{
						cf.ShowToast("Please select the Elevation.", 0);
					}
					else if(b==false)
					{
						cf.ShowToast("Please select the other text for Elevation.", 0);	
						cf.setFocus(((EditText)findViewById(R.id.edtelevother)));
					}	
				}
			}
			else
			{
				InsertWindowOpenings();
			}
			break;
		case R.id.windopennotappl:			
			System.out.println("sadsfdsf");
			if(((CheckBox)findViewById(R.id.windopennotappl)).isChecked())
			{
				windopennotappl="1";
				if(rwswindopen>0)
				{
					AlertDialog.Builder builder = new AlertDialog.Builder(WindMitWindowopenings.this);
					builder.setTitle("Confirmation");
					builder.setIcon(R.drawable.alertmsg);
					builder.setMessage("Do you want to Clear the Window Opening Information?").setCancelable(false)
							.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,int id1) 
										{
												clearwindowopenings();
												subpnlshwwindopen.setVisibility(cf.v1.GONE);
												((CheckBox)findViewById(R.id.windopennotappl)).setChecked(true);
												((LinearLayout)findViewById(R.id.windopeningslin)).setVisibility(cf.v1.GONE);
												((TextView)findViewById(R.id.savetxtwinopen)).setVisibility(cf.v1.GONE);
										}
									})
							.setNegativeButton("No",new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,	int id) {
											
											((CheckBox)findViewById(R.id.windopennotappl)).setChecked(false);
											dialog.cancel();											
									}
							});
					builder.show();
				}
				else
				{
					((TextView)findViewById(R.id.savetxtwinopen)).setVisibility(cf.v1.GONE);
					((LinearLayout)findViewById(R.id.windopeningslin)).setVisibility(cf.v1.GONE);
				}
				
			}
			else
			{
				System.out.println("sadsfdsfelseseseese");
				windopennotappl="0";clearwindowopenings();
				((LinearLayout)findViewById(R.id.windopeningslin)).setVisibility(cf.v1.VISIBLE);
				((TextView)findViewById(R.id.savetxtwinopen)).setVisibility(cf.v1.GONE);
			}
			break;
		}		
	}
	private void updatenotappl() {
		// TODO Auto-generated method stub		
		try
		{
			selecttbl();
			if(c1.getCount()==0)
			{
				cf.arr_db.execSQL("INSERT INTO " + cf.WindDoorSky_NA + " (fld_srid,insp_typeid,fld_windowopenNA)"
						+ "VALUES ('"+cf.selectedhomeid+"','"+cf.identityval+"','"+windopennotappl+"')");
			}
			else
			{
				cf.arr_db.execSQL("UPDATE "	+ cf.WindDoorSky_NA	+ " SET fld_windowopenNA='"+windopennotappl+"' WHERE fld_srid ='"+ cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'");
			}			
			
		}
		catch(Exception e)
		{
			System.out.println("sdfdfds"+e.getMessage());
		}
	}
	private void InsertWindowOpenings() {
		// TODO Auto-generated method stub		
	String elevation =spnelevwindopen.getSelectedItem().toString();
	String quantity =spnwindquantity.getSelectedItem().toString();
	String window =spnwindow.getSelectedItem().toString();
	String floor =spnwindfloor.getSelectedItem().toString();
	String protection =spnwindprot.getSelectedItem().toString();	
	if(!quantity.equals("--Select--") && !window.equals("--Select--") && !floor.equals("--Select--") && !protection.equals("--Select--"))
	{
		BI_WindowOpenings=quantity+"&#94;"+window+"&#94;"+floor+"&#94;"+protection;
	}	
	try
	{		
		if(((Button)findViewById(R.id.windowopensave)).getText().toString().equals("Update")){
			cf.arr_db.execSQL("UPDATE "
					+ cf.BI_Windopenings
					+ " SET BI_WindowOpenings='"+cf.encode(BI_WindowOpenings)+"',fld_elevation_othertxt='"+cf.encode(((EditText)findViewById(R.id.edtelevother)).getText().toString())+"',fld_wind_othrtxt='"+cf.encode(((EditText)findViewById(R.id.edtwindother)).getText().toString())+"'"
					+ " WHERE fld_srid ='"+ cf.selectedhomeid + "' and fld_elevation='"+elevation+"' and BI_WindId='"+windopeningid+"' and insptypeid='"+cf.identityval+"'");
			((Button)findViewById(R.id.windowopensave)).setText("Save/Add More Window Openings");
		}
		else
		{
			
			Cursor BI_WindOpen=cf.SelectTablefunction(cf.BI_Windopenings, " where fld_srid='"+cf.selectedhomeid+"' and insptypeid='"+cf.identityval+"'");
			cf.arr_db.execSQL("INSERT INTO "
							+ cf.BI_Windopenings
							+ " (fld_srid,insptypeid,fld_elevation,BI_WindowOpenings,fld_elevation_othertxt,fld_wind_othrtxt)"
							+ "VALUES ('"+cf.selectedhomeid+"','"+cf.identityval+"','"+elevation+"','"+cf.encode(BI_WindowOpenings)+"','"+cf.encode(((EditText)findViewById(R.id.edtelevother)).getText().toString())+"','"+cf.encode(((EditText)findViewById(R.id.edtwindother)).getText().toString())+"')");
		}
		
		cf.ShowToast("Window Openings saved successfully",0);
		((TextView)findViewById(R.id.savetxtwinopen)).setVisibility(cf.v1.VISIBLE);
		clearwindowopenings();spnelevwindopen.setEnabled(true);((Button)findViewById(R.id.windowopensave)).setText("Save/Add More Window Openings");
		Show_WindowOpenings_value();
	}
	catch(Exception e)
	{
		System.out.println("insert"+e.getMessage());
	}
	}
	private void Show_WindowOpenings_value() {
		// TODO Auto-generated method stub
		try
		{
			
	        
			Cursor c1=cf.SelectTablefunction(cf.BI_Windopenings, " where fld_srid='"+cf.selectedhomeid+"' and insptypeid='"+cf.identityval+"'");
			System.out.println(" where fld_srid='"+cf.selectedhomeid+"' and insptypeid='"+cf.identityval+"'");
			
			if(c1.getCount()!=0)
			{				
				((TextView)findViewById(R.id.savetxtwinopen)).setVisibility(cf.v1.VISIBLE);
				rwswindopen = c1.getCount();
			
				arrwindowopenings=new String[rwswindopen];
				arrwindelev = new String[rwswindopen];
				arrwindid= new String[rwswindopen];
				arrelevothertxt=new String[rwswindopen];
				arrwinothertxt=new String[rwswindopen];
					System.out.println("b4mfirst");
				c1.moveToFirst();
				 int i=0;
				 do
				 {
					 System.out.println("b4mdo"+i);
						 arrwindid[i] = c1.getString(c1.getColumnIndex("BI_WindId")); System.out.println("arrwindid"+arrwindid[i]);
						 arrwindowopenings[i] = cf.decode(c1.getString(c1.getColumnIndex("BI_WindowOpenings"))); System.out.println(" arrwindowopenings[i]"+ arrwindowopenings[i]);
						 arrwindelev[i] = c1.getString(c1.getColumnIndex("fld_elevation"));	 System.out.println("arrwindelev[i]"+arrwindelev[i]);
						 arrelevothertxt[i] = cf.decode(c1.getString(c1.getColumnIndex("fld_elevation_othertxt"))); System.out.println("arrelevothertxt[i]"+arrelevothertxt[i]);
						 arrwinothertxt[i] = cf.decode(c1.getString(c1.getColumnIndex("fld_wind_othrtxt")));System.out.println("arrwinothertxt[i]"+arrwinothertxt[i]);
						i++;
 				  } while (c1.moveToNext());	
					
				 subpnlshwwindopen.setVisibility(cf.v1.VISIBLE);
				 Disp_windopenings_value();
						 
				 System.out.println("C1mdisplay");
					
			}
			else
			{
				((TextView)findViewById(R.id.savetxtwinopen)).setVisibility(cf.v1.GONE);dynviewwinopen.setVisibility(View.GONE);
			}
		}
		catch(Exception e){
			
		}
	}
	private void Disp_windopenings_value() {
		// TODO Auto-generated method stub
		dynviewwinopen.removeAllViews();	System.out.println("hvac_listview_header");
		LinearLayout h1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvac_listview_header, null);
		LinearLayout th= (LinearLayout)h1.findViewById(R.id.windowopenings_hdr);th.setVisibility(cf.v1.VISIBLE);
		TableLayout.LayoutParams lp = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
	 	lp.setMargins(2, 0, 2, 2); 
	 	h1.removeAllViews(); 	 	
	 	dynviewwinopen.addView(th,lp);	
	 	dynviewwinopen.setVisibility(View.VISIBLE);	System.out.println("rwswindopen"+rwswindopen);
	 	for (int i = 0; i < rwswindopen; i++) 
		{	
	 		System.out.println("rwswindopeninsde");
	 		
			TextView No,txtelevation,txtnoofquantity,txtwindow,txtfloor,txtprotection;
			ImageView edit,delete;
			
			LinearLayout t1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvac_listview, null);
			LinearLayout t = (LinearLayout)t1.findViewById(R.id.windowopenings);t.setVisibility(cf.v1.VISIBLE);
			t.setId(44444+i);/// Set some id for further use
			
		 	No= (TextView) t.findViewWithTag("No");			
		 	txtelevation= (TextView) t.findViewWithTag("BI_ElevWind"); 	
		 	txtnoofquantity= (TextView) t.findViewWithTag("BI_WindQuantity");
		 	txtwindow= (TextView) t.findViewWithTag("BI_Window");	
		 	txtfloor= (TextView) t.findViewWithTag("BI_WindFloor");	
		 	txtprotection= (TextView) t.findViewWithTag("BI_WindProt");	
		 	
			No.setText(String.valueOf(i+1));	
			if(arrelevothertxt[i].equals(""))
			{
				txtelevation.setText(arrwindelev[i]);
			}
			else
			{
				
				txtelevation.setText("Other Elevation("+arrelevothertxt[i]+")");				
			}
			
			String Wind_open_split[] = arrwindowopenings[i].split("&#94;"); 	
			txtnoofquantity.setText(Wind_open_split[0]);
			
			if(Wind_open_split[1].equals("Other"))
			{
				txtwindow.setText("Other ("+arrwinothertxt[i]+")");
			}
			else
			{
				txtwindow.setText(Wind_open_split[1]);
			}
			
			
			txtfloor.setText(Wind_open_split[2]);
			txtprotection.setText(Wind_open_split[3]);		 	
			
		 	edit= (ImageView) t.findViewWithTag("Windopen_edit");	 
		 	edit.setTag(arrwindid[i]);
		 	edit.setOnClickListener(new WindOpen_EditClick(1,arrwindid[i]));
		 	delete= (ImageView) t.findViewWithTag("Windopen_delete");
		 	delete.setTag(arrwindid[i]);
		 	delete.setOnClickListener(new WindOpen_EditClick(2,arrwindid[i]));		 	
		 	t1.removeAllViews();
		 	dynviewwinopen.addView(t,lp);
		}
	}
	class WindOpen_EditClick implements OnClickListener
	{
		int type;
		int edit; 
		WindOpen_EditClick(int edit,String id1)
		{
			this.edit=edit;
			this.type=Integer.parseInt(id1);
		}
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			//type=Integer.parseInt(v.getTag().toString());
			if(edit==1)
			{		
				((Button)findViewById(R.id.windowopensave)).setText("Update");
				updateWindowOpenings(type);
			}
			else if(edit==2)
			{
				AlertDialog.Builder builder = new AlertDialog.Builder(WindMitWindowopenings.this);
						builder.setMessage("Are you sure, Do you want to delete the selected Window Opening?").setCancelable(false)
								.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog,int id) 
											{												
													cf.arr_db.execSQL("Delete from "+cf.BI_Windopenings+" Where BI_WindId='"+type+"'");
													cf.ShowToast("Deleted successfully.", 0);														
													clearwindowopenings();
													Show_WindowOpenings_value();
											}
										})
								.setNegativeButton("No",new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog, int id) {
												dialog.cancel();
											}
										});
						builder.setTitle("Confirmation");
						builder.setIcon(R.drawable.alertmsg);
						builder.show();		
			}
		}
	}
	public void updateWindowOpenings(int i) {
		// TODO Auto-generated method stub
		windopeningid=i;
		Cursor c1=cf.SelectTablefunction(cf.BI_Windopenings, " where BI_WindId='"+i+"'");	
		if(c1.getCount()>0)
		{
			c1.moveToFirst();
			String WindowOpening_value = cf.decode(c1.getString(c1.getColumnIndex("BI_WindowOpenings")));
			String Wind_open_split[] = WindowOpening_value.split("&#94;"); 	
			
			int spinnerPosition1 = elevswindadap.getPosition(c1.getString(c1.getColumnIndex("fld_elevation")));
			spnelevwindopen.setSelection(spinnerPosition1);
			
			if(c1.getString(c1.getColumnIndex("fld_elevation")).equals("Other Elevation"))
			{
				((EditText)findViewById(R.id.edtelevother)).setVisibility(cf.v1.VISIBLE);
				((EditText)findViewById(R.id.edtelevother)).setText(cf.decode(c1.getString(c1.getColumnIndex("fld_elevation_othertxt"))));
			}
			else
			{
				((EditText)findViewById(R.id.edtelevother)).setVisibility(cf.v1.GONE);
				((EditText)findViewById(R.id.edtelevother)).setText("");
			}
			
			int spinnerPosition2 = spnwindquantityadap.getPosition(Wind_open_split[0]);
			spnwindquantity.setSelection(spinnerPosition2);
			
			
			if(Wind_open_split[1].equals("Other"))
			{
				((EditText)findViewById(R.id.edtwindother)).setVisibility(cf.v1.VISIBLE);
				((EditText)findViewById(R.id.edtwindother)).setText(cf.decode(c1.getString(c1.getColumnIndex("fld_wind_othrtxt"))));
			}
			else
			{
				((EditText)findViewById(R.id.edtwindother)).setVisibility(cf.v1.GONE);
				((EditText)findViewById(R.id.edtwindother)).setText("");
			}
			
			int spinnerPosition3 = spnwindowadap.getPosition(Wind_open_split[1]);
			spnwindow.setSelection(spinnerPosition3);
			int spinnerPosition4 = spnwindflooradap.getPosition(Wind_open_split[2]);
			spnwindfloor.setSelection(spinnerPosition4);
			int spinnerPosition5 = spnwindprotadap.getPosition(Wind_open_split[3]);
			spnwindprot.setSelection(spinnerPosition5);	
			spnelevwindopen.setEnabled(false);
			
			((Button)findViewById(R.id.windowopensave)).setText("Update");
		}
	}
	private void clearwindowopenings() {
		// TODO Auto-generated method stub
		spnelevwindopen.setSelection(0);
		spnwindquantity.setSelection(0);
		spnwindow.setSelection(0);
		spnwindfloor.setSelection(0);
		spnwindprot.setSelection(0);
		((Button)findViewById(R.id.windowopensave)).setText("Save/Add More Window Openings");
		spnelevwindopen.setEnabled(true);
		spnwindfloor.setSelection(1);
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			cf.gotoclass(cf.identityval, WindMitSWR.class);
			return true;
		}		
		return super.onKeyDown(keyCode, event);
	}
}