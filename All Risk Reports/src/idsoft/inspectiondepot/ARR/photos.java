package idsoft.inspectiondepot.ARR;
/*
 ************************************************************
 * Project: ALL RISK REPORT
 * Module : Photos.java
 * Creation History:
    Created By : Rakki s on 10/18/2012
 * Modification History:
    Last Modified By : Gowri on 04/04/2013
 ************************************************************  
*/
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Html;
import android.text.InputFilter;
import android.text.InputType;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
public class photos extends Activity  {

	private static final String TAG = null;
	CommonFunctions cf;
	
	SummaryFunctions sf;
	RadioGroup rg1;private  final int CAPTURE_IMAGE=1;
	RadioButton[] rb;
	public int elev = 0,maxlevel,t = 0,etchangeordermaxLength = 2,chk = 0,globalvar=0,selectedcount,maximumindb,elevdb=0,backclick_FI;
	public Uri CapturedImageURI;
	public String name = "Other",pathforzoom="",imagorder, updstr,finaltext="",path="",updated_path="",customid="",dupflag="0",filePath="";	
	public TextView txthdr;
	ArrayAdapter adapter2;
	Map<String, String[]> elevationcaption_map = new LinkedHashMap<String, String[]>();
	String[] arrpath, arrpathdesc, arrimgord,separated,selectedtestcaption;
	public LinearLayout lnrlayout;	
	Spinner[] spnelevation;
	TextView[] tvstatus;
	boolean isexists=true,isgreater=true,isempty=true;
	/**Rotating the image variable declaration **/	 Bitmap rotated_b;
	 int  currnet_rotated=0;
	 /**Rotating the image variable declaration ends **/
	/** Variable declaration for the multi image selection **/
	String[] pieces1,pieces3;
	protected Button selectImgBtn,cancelBtn,backbtn;
	protected TextView chaild_titleheade,titlehead;
	protected LinearLayout lnrlayout2_FI,lnrlayout1_FI;
	protected Dialog pd;
	public int max_allowed=8;/**Allowed maxmum number of images **/
	Map<String, TextView> map1 = new LinkedHashMap<String, TextView>();
	
	/**Newly added for limiting option **/
	public int limit_start=0; 
	RelativeLayout Lin_Pre_nex=null;
	ImageView prev=null,next=null,upd_img=null;
	File[] Currentfile_list=null;
	private File images=null;
	public  ScrollView scr2=null;
	protected RelativeLayout Rec_count_lin=null;
	protected TextView Total_Rec_cnt=null,showing_rec_cnt;
	public String inspections="0";
	/** Variable declaration for the multi image selection ends **/
	Spinner  insp_sp;
	String roof_survey="false",insptypeid="",fbinsptypeid="";
	TableLayout summaryTable;
	String selected_insp[];
	public int rws =0;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		cf = new CommonFunctions(this);
		sf = new SummaryFunctions(this);
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			try {
				
				cf.selectedhomeid = extras.getString("homeid");
				cf.onlinspectionid = extras.getString("InspectionType");
				cf.onlstatus = extras.getString("status");
				roof_survey=extras.getString("Roof_survey");
				
				if(roof_survey==null)
				{
					roof_survey="false";
				}
				else if(roof_survey.equals(""))
				{
					roof_survey="false";
				}
				
			} catch (Exception e) {
				// TODO: handle exception

			}
		}
		elev=1;
		setContentView(R.layout.photos);
		cf.getDeviceDimensions();
		if(!roof_survey.equals("true"))
		{
			LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
			hdr_layout.addView(new HdrOnclickListener(this,1,"Photos","Elevations",1,0,cf));
			
			LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
			mainmenu_layout.setMinimumWidth(cf.wd);
			mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(),7, 0,0, cf));
			
			LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 68, 1,0,cf));
		}
		else
		{
			 
			((LinearLayout) findViewById(R.id.content_top)).setMinimumWidth(cf.wd-30);
			//((LinearLayout) findViewById(R.id.hme_sav_change)).setVisibility(View.GONE);
			//((RelativeLayout) findViewById(R.id.hme_save)).setVisibility(View.GONE);
			((LinearLayout) findViewById(R.id.can_frm)).setVisibility(View.VISIBLE);
			findViewById(R.id.Heading_li).setVisibility(View.VISIBLE);
			findViewById(R.id.PH_insp_lin).setVisibility(View.GONE);
			findViewById(R.id.PH_elev_lin).setVisibility(View.GONE);
			findViewById(R.id.header).setVisibility(View.GONE);
			inspections="2";
			elev=0;
			name="Roof survey image";
			 max_allowed=2;
			
		}
		System.out.println("comes correct 2");
		String arr[]=null;
		 try
		    {
			/* if(roof_survey.equals("true"))
			 {
				 arr=new String[2];
				 arr[0]=cf.All_insp_list[0];
				 arr[1]=cf.All_insp_list[2];
			 }
			 else
			 {*/
			 	Cursor s=cf.SelectTablefunction(cf.Select_insp, " WHERE SI_srid='"+cf.selectedhomeid+"'");
		    	s.moveToFirst();
		    	if(s.getCount()>0)
		    	{
		    		String tmp=cf.decode(s.getString(s.getColumnIndex("SI_InspectionNames"))).trim();
		    		System.out.println("tmp="+tmp);
		    		if(tmp.length()>0)
		    		{
		    			try
		    			{
			    			Cursor c =cf.SelectTablefunction(cf.inspnamelist, " Where  ARR_Insp_ID in ("+tmp+")  order by ARR_Custom_ID");
			    			System.out.println("c=="+c.getCount());
			    			if(c.getCount()>0)
			    			{
			    				c.moveToFirst();
			    				arr=new String[c.getCount()+1];
			    				selected_insp=new String[c.getCount()];
			    				arr[0]="-Select-";
			    				for(int i=1;i<c.getCount()+1;i++,c.moveToNext())
			    				{System.out.println("arr=="+arr[i]);
			    					arr[i]=cf.decode(c.getString(c.getColumnIndex("ARR_Insp_Name"))).trim();
			    					System.out.println("arr=="+arr[i]);
			    					selected_insp[i-1]=cf.decode(c.getString(c.getColumnIndex("ARR_Insp_Name"))).trim();;
			    				}
			    			}
		    			}
		    			catch (Exception e) {
							// TODO: handle exception
		    				System.out.println("second issues="+e.getMessage());
						}
		    			/*tmp="-Select-^"+tmp;
		    			tmp=tmp.replace("^","~");
		    		
		    			arr=tmp.split("~");*/
		    		}
		    		else
		    		{
		    			arr=cf.All_insp_list;//getResources().getStringArray(R.array.Inspection_types);
		    		}
		    		
		    		
		    	}
		    	else
		    	{
		    		arr=getResources().getStringArray(R.array.Inspection_types);
		    	}
		    	 adapter2 = new ArrayAdapter(photos.this,android.R.layout.simple_spinner_item, arr);
		 		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 		
			 /**hide the take camera icon **/
				((ImageView) findViewById(R.id.head_take_image)).setVisibility(View.GONE);
				/**hide the take camera icon ends **/
		    }
		    catch (Exception e) {
				// TODO: handle exception
		    	System.out.println("the error was"+e.getMessage());
			}
		// System.out.println("correct"+arr[0]);

		
		 
		 rg1 = ((RadioGroup) findViewById(R.id.PH_insp_sp1));
		 
		insp_sp=(Spinner) findViewById(R.id.PH_insp_sp);
		insp_sp.setAdapter(adapter2);
		
		insp_sp.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				inspections="0";
				txthdr.setVisibility(cf.v1.GONE);
				String selected =insp_sp.getSelectedItem().toString();
				String[] arr=null;
				System.out.println("the selected "+selected+" cf.All_insp_list[2]"+cf.All_insp_list[2]);
				if(!selected.equals("-Select-"))
				{					
					max_allowed=8;
					if(selected.trim().equals(cf.All_insp_list[3]))
					{
						arr=getResources().getStringArray(R.array.B1_1802_Inspection);					
					}
					else if(selected.trim().equals(cf.All_insp_list[1]))
					{
						arr=getResources().getStringArray(R.array.Four_Point_Inspection);					
					}
					else if(selected.trim().equals(cf.All_insp_list[4]))
					{
						arr=getResources().getStringArray(R.array.Commercial_Type_I);					
					}
					else if(selected.trim().equals(cf.All_insp_list[5]))
					{
						arr=getResources().getStringArray(R.array.Commercial_Type_II);					
					}
					else if(selected.trim().equals(cf.All_insp_list[6]))
					{
						arr=getResources().getStringArray(R.array.Commercial_Type_III);					
					}
					else if(selected.trim().equals(cf.All_insp_list[7]))
					{
						arr=getResources().getStringArray(R.array.General_Conditions_Hazards);					
					}
					else if(selected.trim().equals(cf.All_insp_list[8]))
					{
						arr=getResources().getStringArray(R.array.Sinkhole_Inspection);					
					}
					else if(selected.trim().equals(cf.All_insp_list[9]))
					{
						arr=getResources().getStringArray(R.array.Chinese_Drywall);					
					}
					else if(selected.trim().equals(cf.All_insp_list[2]))
					{
						arr=getResources().getStringArray(R.array.Roof_Survey);		
						//max_allowed=2;
					}
					else
					{
						((View) findViewById(R.id.PH_elev_lin)).setVisibility(View.GONE);
					}
					System.out.println("comes correctly");
					
					if(arr!=null)
					{
						for(int i=0;i<cf.All_insp_list.length;i++)
						{
							if(cf.All_insp_list[i].equals(selected.trim()))
							{
								inspections=i+"";
							}
						}
						((LinearLayout) findViewById(R.id.PH_insp_lin1)).setVisibility(View.VISIBLE);
						
						
						
						rg1.removeAllViews();
						int k=0;
						 int leng = arr.length/2; 
						TableRow tr[] = new TableRow[leng];;
						rb = new RadioButton[arr.length];
					    rg1.setOrientation(RadioGroup.VERTICAL);//or RadioGroup.VERTICAL
					    
					   	for(int i=1; i<arr.length; i++){
					    	
					        rb[i]  = new RadioButton(photos.this);
					        if (i % 2 != 0) 
						  	{					        	
					        	  k++;
						  		  tr[k-1] = new TableRow(photos.this);
						  		  rg1.addView(tr[k-1]);
						    }
						  	rb[i].setText(arr[i]);
					        tr[k-1].addView(rb[i]);
					  	    //the RadioButtons are added to the radioGroup instead of the layout					        
					        TableRow.LayoutParams trparams = new TableRow.LayoutParams(210, ViewGroup.LayoutParams.WRAP_CONTENT);
							trparams.setMargins(1, 1, 1, 1);
							rb[i].setLayoutParams(trparams);
					        rb[i].setTextColor(Color.BLACK);
					        rb[i].setOnClickListener(new Elev_click(rb[i].getText().toString(),i,arr.length));
					    }
					
					   
						
						
					}
					else
					{
						//((View) findViewById(R.id.PH_elev_lin)).setVisibility(View.GONE);
						//((View) findViewById(R.id.ph_lin2)).setVisibility(View.GONE);
					}
					elev=-1;
					//show_savedvalue();
					lnrlayout.removeAllViews();
					//txthdr.setVisibility(cf.v1.VISIBLE);
				}
				else
				{
					lnrlayout.removeAllViews();
					((LinearLayout) findViewById(R.id.PH_insp_lin1)).setVisibility(View.GONE);
					//txthdr.setVisibility(cf.v1.GONE);
					//elev_sp.setSelection(0);
				}
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
		ScrollView scr = (ScrollView) findViewById(R.id.scr);
		scr.setMinimumHeight(cf.ht);
		cf.getInspectorId();
		//cf.getinspectiondate();
		cf.CreateARRTable(82);
		cf.CreateARRTable(81);
		/** Declaring the name of elevation **/
		
		/** Declaring the name of elevation **/
		/** Declaration objects starts **/
		txthdr = (TextView) findViewById(R.id.titlehdr);
		lnrlayout = (LinearLayout) this.findViewById(R.id.linscrollview);
		
		/** Declaration objects ends **/

		/** Show the saved values **/
		show_savedvalue();
		/** Show the saved values ends **/
	}
	class Elev_click implements OnClickListener
	{
		String elevation;int l,cnt;
			public Elev_click(String s,int k,int count) {
			// TODO Auto-generated constructor stub
				elevation=s;
				l=k;
				cnt=count;
			}

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				System.out.println("cnt"+cnt);
			  	for(int i=1; i<cnt; i++)
		  		{
			  		rb[i].setChecked(false);	System.out.println("cnti"+i);
			  		if(i==l)
			  		{
			  			System.out.println("inside l="+l);
			  			rb[i].setChecked(true);			  			
			  		}
		  		}
				elev=-1;
				System.out.println("what s elevation="+elevation+"inspeci="+inspections);
				elev=getelvationnumber(elevation, inspections);
				name=elevation;
				((View) findViewById(R.id.ph_lin2)).setVisibility(View.VISIBLE);
				txthdr.setVisibility(cf.v1.VISIBLE);
				show_savedvalue();
			}
		}
	public void show_savedvalue() {
		// TODO Auto-generated method stub
	cf.getPolicyholderInformation(cf.selectedhomeid);
		try {
			int rws = 0;
			if(inspections.equals("4"))
				{
					if(cf.Stories.equals("1") || cf.Stories.equals("2")  || cf.Stories.equals("3"))
					{
						inspections="4";		
					}
					else if(cf.Stories.equals("4") || cf.Stories.equals("5")  || cf.Stories.equals("6"))
					{
						inspections="5";			
					}
					else if(Integer.parseInt(cf.Stories)>=7)
					{	
						inspections="6";		
					}
				}
			System.out.println("asfdsfds"+elev);
			Cursor c11 = cf.arr_db.rawQuery("SELECT * FROM " + cf.ImageTable
					+ " WHERE ARR_IM_SRID='" + cf.selectedhomeid
					+ "' and ARR_IM_Elevation='" + elev
					+ "' and ARR_IM_Insepctiontype='"+inspections+"' order by ARR_IM_ImageOrder", null);
			
			System.out.println("SELECT * FROM " + cf.ImageTable
					+ " WHERE ARR_IM_SRID='" + cf.selectedhomeid
					+ "' and ARR_IM_Elevation='" + elev
					+ "' and ARR_IM_Insepctiontype='"+inspections+"' order by ARR_IM_ImageOrder");
			
			System.out.println("c11="+c11.getCount());
		
			maxlevel = rws = c11.getCount();
			if(name.equals("Front Photo"))
			{
				max_allowed=1;
			}
			else
			{
				max_allowed=8;
			}
			int rem = max_allowed - rws;
			arrpath = new String[rws];
			arrpathdesc = new String[rws];
			arrimgord = new String[rws];
			/*String source = "<b><font color=#000000> Number of uploaded images : "
					+ "</font><font color=#DAA520>"
					+ rws
					+ "</font><br/><font color=#000000> Remaining&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
					"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: " 
					+ "</font><font color=#DAA520>" + rem + "</font>";*/
			String source = "<b><font color=#000000> Number of uploaded images/Total : "
					+ "</font><font color=#DAA520>"
					+ rws+"/"+(rws+rem)
					+ "</font>";
			txthdr.setText(Html.fromHtml(source));
			if (rws == 0) {

				lnrlayout.removeAllViews();
				/*
				 * firsttxt.setVisibility(v1.GONE);
				 * fstimg.setVisibility(v1.GONE); updat.setVisibility(v1.GONE);
				 * del.setVisibility(v1.GONE);
				 */
			} else {
				int Column1 = c11.getColumnIndex("ARR_IM_ImageName");
				int Column2 = c11.getColumnIndex("ARR_IM_Description");
				int Column3 = c11.getColumnIndex("ARR_IM_ImageOrder");
				c11.moveToFirst();
				if (c11 != null) {
					int i = 0;
					do {
						commonelevation(cf.decode(c11.getString(Column3)), i); // set
																				// the
																				// caption
																				// form
																				// the
																				// database
						arrpath[i] = cf.decode(c11.getString(Column1));
						arrpathdesc[i] = cf.decode(c11.getString(Column2));
						arrimgord[i] = cf.decode(c11.getString(Column3));
						ColorDrawable sage = new ColorDrawable(this
								.getResources().getColor(R.color.sage));
						i++;
					} while (c11.moveToNext());
					
					dynamicview();
					//cf.hidekeyboard();
					
				}
			}
			
		} catch (Exception e) {
			// Log.i(TAG, "error correct= " + e.getMessage());
		}
	}

	
	private void commonelevation(String imgor, int spin) {
		// TODO Auto-generated method stub
		int imgorder = 0;
		try {
			imgorder = Integer.parseInt(imgor);
		} catch (Exception e) {

		}
		Cursor c1;

		c1 = cf.SelectTablefunction(cf.Photo_caption,
				" WHERE ARR_IMP_Elevation='" + elev
						+ "' and ARR_IMP_Insepctiontype='"+inspections+"' and ARR_IMP_ImageOrder='" + imgor + "'");
		String[] temp;
		if (c1.getCount() > 0) {
			temp = new String[c1.getCount() + 2];
			temp[0] = "Select";
			temp[1] = "ADD PHOTO CAPTION";
			int i = 2;
			c1.moveToFirst();
			do {
				temp[i] = cf.decode(c1.getString(c1
						.getColumnIndex("ARR_IMP_Caption")));
				i++;
			} while (c1.moveToNext());
		} else {
			temp = new String[2];
			temp[0] = "Select";
			temp[1] = "ADD PHOTO CAPTION";
		}
		elevationcaption_map.put("spiner" + spin, temp);
	}

	private void dynamicview() throws FileNotFoundException {
		// TODO Auto-generated method stub

		
		ImageView[] elevationimage;

		lnrlayout.removeAllViews();
		ScrollView sv = new ScrollView(this);
		lnrlayout.addView(sv);

		LinearLayout l1 = new LinearLayout(this);
		l1.setOrientation(LinearLayout.VERTICAL);
		sv.addView(l1);
		// l1.setMinimumWidth(925);

		tvstatus = new TextView[arrpath.length];
		elevationimage = new ImageView[arrpath.length];
		spnelevation = new Spinner[arrpath.length];

		for (int i = 0; i < arrpath.length; i++) {

			LinearLayout l2 = new LinearLayout(this);
			l2.setOrientation(LinearLayout.HORIZONTAL);
			l1.addView(l2);

			LinearLayout lchkbox = new LinearLayout(this);
			LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(
					175, ViewGroup.LayoutParams.WRAP_CONTENT);
			paramschk.topMargin = 20;
			paramschk.leftMargin = 10;
			l2.addView(lchkbox);

			tvstatus[i] = new TextView(this);
			tvstatus[i].setMinimumWidth(175);
			tvstatus[i].setMaxWidth(175);
			tvstatus[i].setText(arrpathdesc[i]);
			tvstatus[i].setTextColor(Color.BLACK);
			tvstatus[i].setTag("imagechange" + i);
			tvstatus[i].setTextSize(TypedValue.COMPLEX_UNIT_PX, 14);
			lchkbox.addView(tvstatus[i], paramschk);

			String j = "0";
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			try {
				BitmapFactory.decodeStream(new FileInputStream(arrpath[i]),
						null, o);
				j = "1";
			} catch (FileNotFoundException e) {
				
				j = "2";
				
			}
			if (j.equals("1")) {
				final int REQUIRED_SIZE = 100;
				int width_tmp = o.outWidth, height_tmp = o.outHeight;
				int scale = 1;
				while (true) {
					if (width_tmp / 2 < REQUIRED_SIZE
							|| height_tmp / 2 < REQUIRED_SIZE)
						break;
					width_tmp /= 2;
					height_tmp /= 2;
					scale *= 2;
				}
				
				
				// Decode with inSampleSize
				BitmapFactory.Options o2 = new BitmapFactory.Options();
				o2.inSampleSize = scale;
				Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(
						arrpath[i]), null, o2);
				BitmapDrawable bmd = new BitmapDrawable(bitmap);

				LinearLayout lelevimage = new LinearLayout(this);
				LinearLayout.LayoutParams paramselevimg = new LinearLayout.LayoutParams(
						100, 50);
				paramselevimg.topMargin = 10;
				paramselevimg.leftMargin = 20;
				l2.addView(lelevimage);

				elevationimage[i] = new ImageView(this);
				elevationimage[i].setMinimumWidth(75);
				elevationimage[i].setMaxWidth(75);
				elevationimage[i].setMinimumHeight(75);
				elevationimage[i].setMaxHeight(75);
				elevationimage[i].setImageDrawable(bmd);
				elevationimage[i].setTag("imagechange" + i);
				lelevimage.addView(elevationimage[i], paramselevimg);
			}
			else
			{
				Bitmap bmp= BitmapFactory.decodeResource(getResources(),R.drawable.photonotavail);
				LinearLayout lelevimage = new LinearLayout(this);
				LinearLayout.LayoutParams paramselevimg = new LinearLayout.LayoutParams(100, 50);
				paramselevimg.topMargin = 10;
				paramselevimg.leftMargin = 20;
				l2.addView(lelevimage);

				elevationimage[i] = new ImageView(this);
				elevationimage[i].setMinimumWidth(75);
				elevationimage[i].setMaxWidth(75);
				elevationimage[i].setMinimumHeight(75);
				elevationimage[i].setMaxHeight(75);
				elevationimage[i].setImageBitmap(bmp);
				elevationimage[i].setTag("imagechange" + i);
				lelevimage.addView(elevationimage[i], paramselevimg);
				
			}
			try
			{
			LinearLayout lspinner = new LinearLayout(this);
			LinearLayout.LayoutParams paramsspinn = new LinearLayout.LayoutParams(
					200, LayoutParams.WRAP_CONTENT);
			paramsspinn.topMargin = 5;
			paramsspinn.leftMargin = 20;
			l2.addView(lspinner);
			tvstatus[i].setOnClickListener(new View.OnClickListener() {

				public void onClick(final View v) {

					String getidofselbtn = v.getTag().toString();
					final String repidofselbtn = getidofselbtn.replace(
							"imagechange", "");
					final int cvrtstr = Integer.parseInt(repidofselbtn);

					String selpath = arrpath[cvrtstr];
					String seltxt = arrpathdesc[cvrtstr];

					Cursor c11 = cf.arr_db.rawQuery(
							"SELECT ARR_IM_ImageOrder FROM " + cf.ImageTable
									+ " WHERE ARR_IM_SRID='"
									+ cf.selectedhomeid
									+ "' and ARR_IM_Elevation='" + elev
									+ "' and  ARR_IM_Insepctiontype='"+inspections+"' and ARR_IM_ImageName='"
									+ cf.encode(arrpath[cvrtstr]) + "'", null);
					c11.moveToFirst();
				   imagorder= c11.getString(c11.getColumnIndex("ARR_IM_ImageOrder"));
					String a;
					chk = 1;
					// dispfirstimg(cvrtstr);
					 dispfirstimg(cvrtstr,seltxt);

				}
			});
			elevationimage[i].setOnClickListener(new View.OnClickListener() {
				public void onClick(final View v) {

					String getidofselbtn = v.getTag().toString();
					final String repidofselbtn = getidofselbtn.replace(
							"imagechange", "");
					final int cvrtstr = Integer.parseInt(repidofselbtn);

					String selpath = arrpath[cvrtstr];
					String seltxt = arrpathdesc[cvrtstr];

					Cursor c11 = cf.arr_db.rawQuery(
							"SELECT ARR_IM_ImageOrder FROM " + cf.ImageTable
									+ " WHERE ARR_IM_SRID='"
									+ cf.selectedhomeid
									+ "' and ARR_IM_Elevation='" + elev
									+ "' and  ARR_IM_Insepctiontype='"+inspections+"' and ARR_IM_ImageName='"
									+ cf.encode(arrpath[cvrtstr]) + "'", null);
					c11.moveToFirst();
					imagorder = c11.getString(c11
							.getColumnIndex("ARR_IM_ImageOrder"));
					String a;
					chk = 1;
					// dispfirstimg(cvrtstr);
					dispfirstimg(cvrtstr, seltxt);

				}
			});
			int n = i + 1;

			String[] SH_IM_Elevation = elevationcaption_map.get("spiner" + i);
			spnelevation[i] = new Spinner(this);
			spnelevation[i].setId(i);
			ArrayAdapter adapter2 = new ArrayAdapter(photos.this,android.R.layout.simple_spinner_item, SH_IM_Elevation);
			adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spnelevation[i].setAdapter(adapter2);
			lspinner.addView(spnelevation[i], paramsspinn);
			spnelevation[i].setOnItemSelectedListener(new MyOnItemSelectedListener1(i));
			
			}
			catch (Exception e) {
				// TODO: handle exception
				System.out.println("ececption"+e.getMessage());
			}
		}
		
	}
	
/** Showing the image for the Update starts **/
	public void dispfirstimg(final int selid, String photocaptions) {
		 System.gc();
		currnet_rotated=0;
		
		final int delimagepos = selid;
		String phtodesc = photocaptions;

		String k;
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;

		final Dialog dialog1 = new Dialog(photos.this,
				android.R.style.Theme_Translucent_NoTitleBar);
		dialog1.getWindow().setContentView(R.layout.alert);

		/**
		 * get the help and update relative layou and set the visbilitys for the
		 * respective relative layout
		 */
		LinearLayout Re = (LinearLayout) dialog1.findViewById(R.id.maintable);
		Re.setVisibility(View.GONE);
		LinearLayout Reup = (LinearLayout) dialog1
				.findViewById(R.id.updateimage);
		Reup.setVisibility(View.VISIBLE);
		/**
		 * get the help and update relative layou and set the visbilitys for the
		 * respective relative layout
		 */
		

		 upd_img = (ImageView) dialog1.findViewById(R.id.firstimg);
		final EditText upd_Ed = (EditText) dialog1.findViewById(R.id.firsttxt);
		upd_Ed.setFocusable(false);
		//upd_Ed.setFocusableInTouchMode(true);
		ImageView btn_helpclose = (ImageView) dialog1.findViewById(R.id.imagehelpclose);
		Button btn_up = (Button) dialog1.findViewById(R.id.update);
		Button btn_del = (Button) dialog1.findViewById(R.id.delete);
		Button rotateleft = (Button) dialog1.findViewById(R.id.rotateleft);
		Button rotateright = (Button) dialog1.findViewById(R.id.rotateright);
        Button zoom = (Button) dialog1.findViewById(R.id.zoom);
		
		zoom.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				if(updated_path.equals(""))
				{
					pathforzoom = arrpath[selid];
				}
				else
				{
					pathforzoom = updated_path;
				}
				Intent reptoit1 = new Intent(photos.this,ImageZoom.class);
				Bundle b=new Bundle();
				reptoit1.putExtra("Path", pathforzoom);
				startActivity(reptoit1);
			}
		});
		upd_img.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Cursor c11 = cf.arr_db.rawQuery("SELECT * FROM " + cf.ImageTable
	 					+ " WHERE ARR_IM_SRID='" + cf.selectedhomeid + "' and ARR_IM_Elevation='" + elev
	 					+ "'  and  ARR_IM_Insepctiontype='"+inspections+"' order by ARR_IM_ImageOrder", null);
	 			t=0;
	 			/***We call the centralized image selection part **/
				String[] selected_paht=null;
				if(c11.getCount()>0)
				{
	 				c11.moveToFirst();
	 				selected_paht = new String[c11.getCount()];
	 				
	 				for(int i=0;i<c11.getCount();i++)
	 				{
	 					selected_paht[i]=cf.decode(c11.getString(c11.getColumnIndex("ARR_IM_ImageName"))); 
	 					
	 					c11.moveToNext();
	 				}
				
				}
				
				//cf.select;
				try
				{
				Intent reptoit1 = new Intent(photos.this,Select_phots.class);
				Bundle b=new Bundle();
				reptoit1.putExtra("Selectedvalue", selected_paht); /**Send the already selected image **/
				reptoit1.putExtra("Maximumcount", 0);/**Total count of image in the database **/
				reptoit1.putExtra("Total_Maximumcount", 1); /***Total count of image we need to accept**/
				reptoit1.putExtra("ok", "true"); /***Total count of image we need to accept**/
				//reptoit1.setClassName("com.idinspection","com.idinspection.Select_phots");
				startActivityForResult(reptoit1,125); /** Call the Select image page in the idma application  image ***/
				}		
				catch ( Exception e) {
					// TODO: handle exception
				}
			}
		});
		 upd_Ed.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				 
				cf.setFocus(upd_Ed);
				return false;
			}
		});
		// update and delete button function

	
		btn_up.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				updstr = upd_Ed.getText().toString();

				try {
					if (!updstr.trim().equals("")) {
						dialog1.setCancelable(true);
						dialog1.dismiss();
						if(updated_path.equals(""))
						{
						cf.arr_db.execSQL("UPDATE " + cf.ImageTable
								+ " SET ARR_IM_Description='"
								+ cf.encode(updstr)
								+ "',ARR_IM_ModifiedOn='' WHERE ARR_IM_SRID ='"
								+ cf.selectedhomeid
								+ "' and ARR_IM_Elevation='" + elev
								+ "'  and  ARR_IM_Insepctiontype='"+inspections+"' and ARR_IM_ImageOrder='" + imagorder + "'");
						}
						else
						{
							cf.arr_db.execSQL("UPDATE " + cf.ImageTable
									+ " SET ARR_IM_Description='"
									+ cf.encode(updstr)
									+ "',ARR_IM_ModifiedOn='',ARR_IM_ImageName='"+cf.encode(updated_path)+"' WHERE ARR_IM_SRID ='"
									+ cf.selectedhomeid
									+ "' and ARR_IM_Elevation='" + elev
									+ "'  and  ARR_IM_Insepctiontype='"+inspections+"' and ARR_IM_ImageOrder='" + imagorder + "'");
							
						}
						cf.ShowToast("Saved successfully.", 0);

					} else {
						cf.ShowToast("Please enter the caption.", 0);cf.hidekeyboard(upd_Ed);
					}
					/**Save the rotated value in to the external stroage place **/
					if(currnet_rotated>0)
					{ 

						try
						{
							/**Create the new image with the rotation **/
							String current=MediaStore.Images.Media.insertImage(getContentResolver(), rotated_b, "My bitmap", "My rotated bitmap");
							  ContentValues values = new ContentValues();
							  values.put(MediaStore.Images.Media.ORIENTATION, 0);
							  photos.this.getContentResolver().update(Uri.parse(current), values, MediaStore.Images.Media.DATA+ "=?", new String[] { current } );
							
							
							if(current!=null)
							{
							String path=getPath(Uri.parse(current));
							if(updated_path.equals(""))
							{
							File fout = new File(arrpath[selid]);
							fout.delete();
							/** delete the selected image **/
							File fin = new File(path);
							/** move the newly created image in the slected image pathe ***/
							fin.renameTo(new File(arrpath[selid]));
							}
							else
							{
								
								File fout = new File(updated_path);
								fout.delete();
								/** delete the selected image **/
								
								File fin = new File(path);
								/** move the newly created image in the slected image pathe ***/
								fin.renameTo(new File(updated_path));
							}
							
							
							
							
						}
						} catch(Exception e)
						{
							System.out.println("Error occure while rotate the image "+e.getMessage());
						}
						
					}
					
				} catch (Exception e) {
					System.out.println("erre " + e.getMessage());
					
				}
				/**Save the rotated value in to the external stroage place ends **/
				updated_path="";
				show_savedvalue();
				
			}

		});
		
		btn_del.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dialog1.setCancelable(true);
				dialog1.dismiss();
				AlertDialog.Builder builder = new AlertDialog.Builder(
						photos.this);
				builder.setMessage(
						"Are you sure, Do you want to delete the image?")
						.setTitle("Confirmation")
						.setIcon(R.drawable.alertmsg)
						.setCancelable(false)
						.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog,
											int id) {
										try {
											Cursor or = cf.arr_db
													.rawQuery(
															"select ARR_IM_ImageOrder from "
																	+ cf.ImageTable
																	+ " WHERE  ARR_IM_SRID ='"
																	+ cf.selectedhomeid
																	+ "' and ARR_IM_Elevation='"
																	+ elev
																	+ "'  and  ARR_IM_Insepctiontype='"+inspections+"' and ARR_IM_ImageName='"
																	+ cf.encode(arrpath[delimagepos])
																	+ "'", null);
											or.moveToFirst();
											int image_order = or.getInt(or
													.getColumnIndex("ARR_IM_ImageOrder"));

											cf.arr_db.execSQL("DELETE FROM "
													+ cf.ImageTable
													+ " WHERE ARR_IM_SRID ='"
													+ cf.selectedhomeid
													+ "' and ARR_IM_Elevation='"
													+ elev
													+ "'  and  ARR_IM_Insepctiontype='"+inspections+"' and ARR_IM_ImageName='"
													+ cf.encode(arrpath[delimagepos])
													+ "'");

											// Get the total count of images in
											// the table
											Cursor c3 = cf
													.SelectTablefunction(
															cf.ImageTable,
															" WHERE ARR_IM_SRID ='"
																	+ cf.selectedhomeid
																	+ "' and ARR_IM_Elevation='"
																	+ elev
																	+ "'  and  ARR_IM_Insepctiontype='"+inspections+"'");
											for (int m = image_order; m <= c3
													.getCount(); m++) {
												int k = m + 1;
												cf.arr_db.execSQL("UPDATE "
														+ cf.ImageTable
														+ " SET ARR_IM_ImageOrder='"
														+ m
														+ "' WHERE ARR_IM_SRID ='"
														+ cf.selectedhomeid
														+ "' and ARR_IM_Elevation='"
														+ elev
														+ "' and  ARR_IM_Insepctiontype='"+inspections+"' and ARR_IM_ImageOrder='"
														+ k + "'");

											}

										} catch (Exception e) {
											System.out.println("exception e  "
													+ e);
										}
										cf.ShowToast(
												"Image has been deleted successfully.",
												1);

										chk = 0;
										try {
											Cursor c11 = cf.arr_db
													.rawQuery(
															"SELECT * FROM "
																	+ cf.ImageTable
																	+ " WHERE ARR_IM_SRID='"
																	+ cf.selectedhomeid
																	+ "' and ARR_IM_Elevation='"
																	+ elev
																	+ "'  and  ARR_IM_Insepctiontype='"+inspections+"'  ",
															null);
											int delchkrws = c11.getCount();

										} catch (Exception e) {

										}
										show_savedvalue();
										// showimages();
									}
								})
						.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {

										dialog.cancel();
									}
								});
				builder.show();
			}
		});
		btn_helpclose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog1.setCancelable(true);
				dialog1.dismiss();
			}
		});
		
		rotateright.setOnClickListener(new OnClickListener() {  
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				System.gc();
				currnet_rotated+=90;
				if(currnet_rotated>=360)
				{
					currnet_rotated=0;
				}
				
				Bitmap myImg;
				try {
					if(updated_path.equals(""))
					{
						myImg = BitmapFactory.decodeStream(new FileInputStream(arrpath[selid]));
					}
					else
					{
						myImg = BitmapFactory.decodeStream(new FileInputStream(updated_path));
					}
					
					Matrix matrix =new Matrix();
					matrix.reset();
					//matrix.setRotate(currnet_rotated);
					matrix.postRotate(currnet_rotated);
					
					 rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
					        matrix, true);
					 System.gc();
					 upd_img.setImageBitmap(rotated_b);

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (Exception e) {
					
				}
				catch (OutOfMemoryError e) {
					System.out.println("comes in to out ot mem exception");
					System.gc();
					try {
						myImg=null;
						System.gc();
						Matrix matrix =new Matrix();
						matrix.reset();
						//matrix.setRotate(currnet_rotated);
						matrix.postRotate(currnet_rotated);
						myImg= cf.ShrinkBitmap(arrpath[selid], 800, 800);
						rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
						
						System.gc();
						upd_img.setImageBitmap(rotated_b); 

					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					 catch (OutOfMemoryError e1) {
							// TODO Auto-generated catch block
						 cf.ShowToast("You cannot rotate this image. Image size exceeds 2MB.",0);
					}
				}

			}
		});
		rotateleft.setOnClickListener(new OnClickListener() {  			
			public void onClick(View v) {

				// TODO Auto-generated method stub
			
				System.gc();
				currnet_rotated-=90;
				if(currnet_rotated<0)
				{
					currnet_rotated=270;
				}

				
				Bitmap myImg;
				try {
					if(updated_path.equals(""))
					{
						myImg = BitmapFactory.decodeStream(new FileInputStream(arrpath[selid]));
					}
					else
					{
						myImg = BitmapFactory.decodeStream(new FileInputStream(updated_path));
					}
					//myImg = BitmapFactory.decodeStream(new FileInputStream(arrpath[selid]));
					Matrix matrix =new Matrix();
					matrix.reset();
					//matrix.setRotate(currnet_rotated);
					System.out.println("Ther is no more issues top ");
					matrix.postRotate(currnet_rotated);
					
					 rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
					        matrix, true);
					 
					 upd_img.setImageBitmap(rotated_b);

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (Exception e) {
					
				}
				catch (OutOfMemoryError e) {
					System.out.println("comes in to out ot mem exception");
					System.gc();
					try {
						myImg=null;
						System.gc();
						Matrix matrix =new Matrix();
						matrix.reset();
						//matrix.setRotate(currnet_rotated);
						matrix.postRotate(currnet_rotated);
						myImg= cf.ShrinkBitmap(arrpath[selid], 800, 800);
						rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
						System.gc();
						upd_img.setImageBitmap(rotated_b); 

					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					 catch (OutOfMemoryError e1) {
							// TODO Auto-generated catch block
						 cf.ShowToast("You can not rotate this image. Image size exceeds 2MB.",0);
					}
				}

			
			}
		});
		
		// update and delete button function ends
		try {
			if (chk == 1) {
				BitmapFactory.decodeStream(new FileInputStream(arrpath[selid]),
						null, o);
			} else {
				BitmapFactory.decodeStream(new FileInputStream(arrpath[0]),
						null, o);
			}
			k = "1";

		} catch (FileNotFoundException e) {
			k = "2";

		}
		if (k.equals("1")) {
			final int REQUIRED_SIZE = 400;
			int width_tmp = o.outWidth, height_tmp = o.outHeight;
			int scale = 1;
			while (true) {
				if (width_tmp / 2 < REQUIRED_SIZE
						|| height_tmp / 2 < REQUIRED_SIZE)
					break;
				width_tmp /= 2;
				height_tmp /= 2;
				scale *= 2;
			}

			// Decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			Bitmap bitmap = null;
			try {
				Cursor c11 = cf.arr_db.rawQuery("SELECT * FROM " + cf.ImageTable
						+ " WHERE ARR_IM_SRID='" + cf.selectedhomeid
						+ "' and ARR_IM_Elevation='" + elev
						+ "'  and  ARR_IM_Insepctiontype='"+inspections+"' and ARR_IM_ImageName='" + cf.encode(arrpath[selid])
						+ "' ", null);
				c11.moveToFirst();
				String SH_IM_Description = c11.getString(c11
						.getColumnIndex("ARR_IM_Description"));

				if (chk == 1) {
					bitmap = BitmapFactory.decodeStream(new FileInputStream(
							arrpath[selid]), null, o2);
					upd_Ed.setText(cf.decode(SH_IM_Description));

				} else {
					bitmap = BitmapFactory.decodeStream(new FileInputStream(
							arrpath[0]), null, o2);
					upd_Ed.setText(cf.decode(arrpathdesc[0]));
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				
			}
			rotated_b=bitmap;
			BitmapDrawable bmd = new BitmapDrawable(bitmap);
			
			upd_img.setImageDrawable(bmd);

			dialog1.setCancelable(false);
			dialog1.show();
		} else {
			upd_Ed.setText(photocaptions);
			rotateright.setVisibility(View.GONE);
			rotateleft.setVisibility(View.GONE);
			dialog1.setCancelable(false);
			dialog1.show();
		}
		upd_Ed.clearFocus();
      //cf.hidekeyboard();
	}
/** Showing the image for the Update starts **/
	private class MyOnItemSelectedListener1 implements OnItemSelectedListener {

		public final int spinnerid;

		MyOnItemSelectedListener1(int id) {
			this.spinnerid = id;
		}

		public void onItemSelected(AdapterView<?> parent, View view,
				final int pos,

				long id) {
			if (pos == 0) {
			} else {

				int maxLength = 99;
				try {

					final int j = spinnerid + 1;
					final String[] photocaption = elevationcaption_map
							.get("spiner" + spinnerid);

					String[] bits = arrpath[spinnerid].split("/");
					final String picname = bits[bits.length - 1];

					if (photocaption[pos].equals("ADD PHOTO CAPTION")) {
						final Dialog dialog = new Dialog(photos.this,android.R.style.Theme_Translucent_NoTitleBar);
						dialog.getWindow().setContentView(R.layout.alert);
						
						LinearLayout Re=(LinearLayout) dialog.findViewById(R.id.maintable);
						Re.setVisibility(View.GONE);
						LinearLayout Reup=(LinearLayout) dialog.findViewById(R.id.updateimage);    
						Reup.setVisibility(View.GONE);
						
						dialog.findViewById(R.id.Add_caption).setVisibility(View.VISIBLE);
					//	dialog.findViewById(R.id.main_head).setVisibility(View.GONE);
						
						final EditText input =(EditText)dialog.findViewById(R.id.caption_text);
						Button save=(Button)dialog.findViewById(R.id.caption_save);
						Button can=(Button)dialog.findViewById(R.id.caption_cancel);
						ImageView cls=(ImageView)dialog.findViewById(R.id.caption_close);
						cls.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								spnelevation[spinnerid].setSelection(0);
								cf.hidekeyboard(input);
								dialog.setCancelable(true);
								dialog.cancel();
							}
						});
						save.setOnClickListener(new OnClickListener() {							
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub

								String commentsadd = input.getText()
										.toString().trim();
								if (!commentsadd.trim().equals("")) {
									Cursor c= cf.SelectTablefunction(cf.Photo_caption, " Where ARR_IMP_INSP_ID='"+cf.Insp_id+"' And ARR_IMP_Caption='"+cf.encode(commentsadd)+"' and " +
											"ARR_IMP_Elevation='" + elev + "' and ARR_IMP_ImageOrder='" + j	+ "' and ARR_IMP_Insepctiontype='"+inspections+"'");
									if(c.getCount()>0)
									{
										cf.ShowToast("Caption already in use. Please try a different caption.",0);
										
									}
									else
									{
										cf.arr_db.execSQL("INSERT INTO "
												+ cf.Photo_caption
												+ " (ARR_IMP_INSP_ID,ARR_IMP_Caption,ARR_IMP_Elevation,ARR_IMP_ImageOrder,ARR_IMP_Insepctiontype)"
												+ " VALUES ('" + cf.Insp_id
												+ "','"
												+ cf.encode(commentsadd)
												+ "','" + elev + "','" + j
												+ "','"+inspections+"')");
									
										cf.ShowToast(
												"Photo Caption added successfully.",
												1);
										dialog.setCancelable(true);
										dialog.cancel();
										cf.hidekeyboard(input);
										show_savedvalue();
										
										
									}
									
									
								} else {
									cf.ShowToast("Please enter the caption.",0);
									
									cf.hidekeyboard(input);
									spnelevation[spinnerid]
											.setSelection(0);  
								}
							
							}
						});
						
						can.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
										spnelevation[spinnerid].setSelection(0);
										cf.hidekeyboard(input);
										dialog.setCancelable(true);
										dialog.cancel();
										
									}
								});
						dialog.setCancelable(false);
						dialog.show();

					} else {
						final Dialog dialog = new Dialog(photos.this);
						dialog.setContentView(R.layout.alertfront);
						
						dialog.setTitle("Choose Option");
						//dialog.setIcon(R.drawable.alertmsg);
						dialog.setCancelable(true);
						final EditText edit_desc = (EditText) dialog
								.findViewById(R.id.edittxtdesc);
						((TextView)dialog.findViewById(R.id.caption_txt)).setText("Caption : "+photocaption[pos]);
						((TextView)dialog.findViewById(R.id.caption_txt)).setVisibility(View.VISIBLE);
						Button button_close = (Button) dialog
								.findViewById(R.id.Button01);
						final Button button_sel = (Button) dialog
								.findViewById(R.id.Button02);
						Button button_edit = (Button) dialog
								.findViewById(R.id.Button03);
						Button button_del = (Button) dialog
								.findViewById(R.id.Button04);
						final Button button_upd = (Button) dialog
								.findViewById(R.id.Button05);

						button_close.setOnClickListener(new OnClickListener() {
							public void onClick(View v) {
								spnelevation[spinnerid].setSelection(0);
								cf.hidekeyboard(edit_desc);
								dialog.setCancelable(true);
								dialog.cancel();
							}

							public void onNothingSelected(AdapterView<?> arg0) {
								// TODO Auto-generated method stub

							}
						});
						button_sel.setOnClickListener(new OnClickListener() {
							public void onClick(View v) {
								try {

									cf.arr_db.execSQL("UPDATE "
											+ cf.ImageTable
											+ " SET ARR_IM_Description='"
											+ cf.encode(photocaption[pos])
											+ "',ARR_IM_Nameext='"
											+ cf.encode(picname)
											+ "',ARR_IM_ModifiedOn='"
											+ cf.encode(cf.datewithtime
													.toString())
											+ "' WHERE ARR_IM_SRID ='"
											+ cf.selectedhomeid
											+ "' and ARR_IM_Elevation='" + elev
											+ "'  and  ARR_IM_Insepctiontype='"+inspections+"' and ARR_IM_ImageOrder='"
											+ arrimgord[spinnerid] + "'");
									spnelevation[spinnerid].setSelection(0);
								} catch (Exception e) {
									cf.Error_LogFile_Creation("Problem in select the caption in the photos at the elevation "
											+ elev
											+ " error  ="
											+ e.getMessage());
								}

								tvstatus[spinnerid].setText(photocaption[pos]);
								dialog.setCancelable(true);
								dialog.cancel();
							}
						});

						button_edit.setOnClickListener(new OnClickListener() {

							public void onClick(View v) {

								button_sel.setVisibility(v.GONE);

								edit_desc.setVisibility(v.VISIBLE);
								edit_desc.setText(photocaption[pos]);
								((TextView)dialog.findViewById(R.id.caption_txt)).setVisibility(View.GONE);
								button_upd.setVisibility(v.VISIBLE);
								System.out.println("edit_desc = ");
								button_upd
										.setOnClickListener(new OnClickListener() {

											//
											public void onClick(View v) {
												if (!edit_desc.getText()
														.toString().trim()
														.equals("")) {
													cf.arr_db.execSQL("UPDATE "
															+ cf.Photo_caption
															+ " set ARR_IMP_Caption = '"
															+ cf.encode(edit_desc
																	.getText()
																	.toString())
															+ "' WHERE ARR_IMP_Elevation ='"
															+ elev
															+ "' and ARR_IMP_ImageOrder='"
															+ arrimgord[j - 1]
															+ "' and ARR_IMP_Insepctiontype='"+inspections+"' and ARR_IMP_Caption='"
															+ cf.encode(photocaption[pos])
															+ "'");
													cf.hidekeyboard(edit_desc);
													show_savedvalue();
													cf.ShowToast("Photo Caption has been saved successfully.",0);
													dialog.setCancelable(true);
													dialog.cancel();
												} else {
													cf.ShowToast("Please enter the caption.",0);cf.hidekeyboard(edit_desc);
												}
											}
										});
							}
						});

						button_del.setOnClickListener(new OnClickListener() {
							public void onClick(View v) {

								AlertDialog.Builder builder = new AlertDialog.Builder(
										photos.this);
								builder.setMessage(
										"Are you sure, Do you want to delete?")
										.setCancelable(false)
										.setTitle("Confirmation")
										.setIcon(R.drawable.alertmsg)
										.setPositiveButton(
												"Yes",
												new DialogInterface.OnClickListener() {
													public void onClick(
															DialogInterface dialog,
															int id) {
														// SH_IMP_INSP_ID,SH_IMP_Caption,SH_IMP_Elevation,SH_IMP_ImageOrder
														cf.arr_db.execSQL("Delete From "
																+ cf.Photo_caption
																+ "  WHERE  ARR_IMP_Elevation ='"
																+ elev
																+ "' and ARR_IMP_ImageOrder='"
																+ arrimgord[j - 1]
																+ "' and ARR_IMP_Insepctiontype='"+inspections+"' and ARR_IMP_Caption='"
																+ cf.encode(photocaption[pos])
																+ "'");
														show_savedvalue();
														cf.ShowToast(
																"Photo Caption has been deleted successfully.",
																1);

													}
												})
										.setNegativeButton(
												"No",
												new DialogInterface.OnClickListener() {
													public void onClick(
															DialogInterface dialog,
															int id) {
														spnelevation[spinnerid]
																.setSelection(0);
														dialog.cancel();
													}
												});
								dialog.setCancelable(true);
								builder.show();
								dialog.cancel();
							}
						});
						dialog.setCancelable(false);
						dialog.show();
					}

				} catch (Exception e) {
					System.out.println("e + " + e.getMessage());
				}

			}
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.

		}

	}
	
	public void clicker(View v) {
		
	
		
		if(v.getId()==R.id.home)
		{
				
			cf.gohome();
		}
		else if(v.getId()== R.id.cancel)
		{
			Intent n1= new Intent();
			setResult(RESULT_OK,n1);
			finish(); 
		}
		else if(v.getId()== R.id.viewsummary)
		{
			sf.displayphotosummary(cf.selectedhomeid);
		}
		else if(!inspections.equals("0"))
		{
			//String elev1=elev_sp.getSelectedItem().toString();
			
			if(elev!=-1)
			{
				switch (v.getId()) {
				/**Fro roof survey**/
				/*case R.id.Generate_frm:
					Cursor c15 = cf.arr_db.rawQuery("SELECT * FROM " + cf.ImageTable
							+ " WHERE ARR_IM_SRID='" + cf.selectedhomeid
							+ "' and ARR_IM_Elevation='" + elev
							+ "' and  ARR_IM_Insepctiontype='"+inspections+"' order by ARR_IM_ImageOrder", null);
					int chkrws15 = c15.getCount();
					if (chkrws15 < max_allowed) {
						cf.ShowToast("You must  upload "+max_allowed+" photos for this section", 1);
					} else {
					Intent n= new Intent();
					setResult(RESULT_OK,n);
					finish();
					}
				break;
				*/case R.id.browsecamera:
					String shwtoast = "",msg="";
					Cursor c12 = cf.arr_db.rawQuery("SELECT * FROM " + cf.ImageTable
							+ " WHERE ARR_IM_SRID='" + cf.selectedhomeid
							+ "' and ARR_IM_Elevation='" + elev
							+ "' and  ARR_IM_Insepctiontype='"+inspections+"' order by ARR_IM_ImageOrder", null);
					int chkrws1 = c12.getCount();
					
					if(name.equals("Front Photo"))
					{
						max_allowed=1;	shwtoast="You cannot add more than "+ max_allowed+" image.";
					}
					else
					{
						max_allowed=8;	shwtoast="You cannot add more than "+ max_allowed+" images.";
					}
					if (chkrws1 < max_allowed) {						
						t = 1;
						System.out.println("e;le="+elev);
						dupflag="0";
						/*if(elev==7)
						{
						*/	
							if(selected_insp.length>1)
	 						{
								AlertDialog.Builder builder = new AlertDialog.Builder(photos.this);
								if(name.equals("Front Elevation") || name.equals("Right Elevation") || name.equals("Back Elevation") || name.equals("Left Elevation"))
								{
									msg=name+" Photographs";									
								}
								else
								{
									msg=name;
								}
								
								
								builder.setMessage("Do you want to Duplicate the " +msg +" to all the Inspections?")
				   			       .setCancelable(false)
				   			       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				   			           public void onClick(DialogInterface dialog, int id) {
				   			        	dupflag="1";
				   			        	startCameraActivity();
				   			        	  
				   			           }
				   			       })
				   			       .setNegativeButton("No", new DialogInterface.OnClickListener() {
				   			           public void onClick(DialogInterface dialog, int id) {
				   			        	dupflag="0";
				   			                dialog.cancel();
				   			             startCameraActivity();
				   			           }
				   			       });
					   			builder.show();
	 						}
							else
							{
								 startCameraActivity();
							}
						/*}
						else
						{
							 startCameraActivity();
						}*/
					} 
					else 
					{
						cf.ShowToast(shwtoast, 0);
					}

					break;
				case R.id.browsetxt:
					String shwtoast1 = "",msg1="";
					Cursor c11 = cf.arr_db.rawQuery("SELECT * FROM " + cf.ImageTable
		 					+ " WHERE ARR_IM_SRID='" + cf.selectedhomeid + "' and ARR_IM_Elevation='" + elev
		 					+ "'  and  ARR_IM_Insepctiontype='"+inspections+"' order by ARR_IM_ImageOrder", null);
		 			int chkrws = c11.getCount();
		 			/***We call the centralized image selection part **/
		 			maximumindb=chkrws;
		 			System.out.println("name="+name);
		 			if(name.equals("Front Photo"))
					{
						max_allowed=1;	shwtoast1="You cannot add more than "+ max_allowed+" image.";
					}
					else
					{
						max_allowed=8;	shwtoast1="You cannot add more than "+ max_allowed+" images.";
					}
		 			if(maximumindb>=max_allowed)
		 			{
		 				cf.ShowToast(shwtoast1, 0);
		 			}else
		 			{
		 				c11.moveToFirst();
		 				final String[] selected_paht = new String[c11.getCount()];
		 				
		 				if(name.equals("Front Elevation") || name.equals("Right Elevation") || name.equals("Back Elevation") || name.equals("Left Elevation"))
						{
		 					msg1=name+" Photographs";
						}
						else
						{
							msg1=name;
						}
						System.out.println("msg="+msg1);
		 				
		 				for(int i=0;i<c11.getCount();i++)
		 				{
		 					selected_paht[i]=cf.decode(c11.getString(c11.getColumnIndex("ARR_IM_ImageName"))); 		 					
		 					c11.moveToNext();
		 				}
		 				t=0;
			 				try
			 				{
			 					dupflag="0";
			 					/*if(elev==7)
								{*/
			 					System.out.println("selected_insp.length"+selected_insp.length);
			 						if(selected_insp.length>1)
			 						{
			 							
										AlertDialog.Builder builder = new AlertDialog.Builder(photos.this);
							   			builder.setMessage("Do you want to Duplicate the " +msg1 +" to all the Inspections?")
						   			       .setCancelable(false)
						   			       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
						   			           public void onClick(DialogInterface dialog, int id) {
						   			        	dupflag="1";
						   			        	selectphotos(selected_paht);
						   			        	  
						   			           }
						   			       })
						   			       .setNegativeButton("No", new DialogInterface.OnClickListener() {
						   			           public void onClick(DialogInterface dialog, int id) {
						   			        	dupflag="0";
						   			                dialog.cancel();
						   			             selectphotos(selected_paht);
						   			           }
						   			       });
							   			builder.show();
			 						}
			 						else
				 					{
				 						selectphotos(selected_paht);
				 					}
					            
								/*}
			 					else
			 					{
			 						selectphotos(selected_paht);
			 					}*/
			 				}  
			 				catch (ActivityNotFoundException e) {
			 					// TODO: handle exception
			 					cf.ShowToast("Your current Inspection Depot Mobile Application does not support this feature", 0);
							}
		 				catch (Exception e) {
							// TODO: handle exception
						}
		 			}
		 	   break;
				
				case R.id.save:
					
					String source = "<b><font color=#000000> Number of uploaded images : "
							+ "</font><font color=#DAA520>"
							+ 0
							+ "</font><br /><font color=#000000>Remaining&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
							"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: "
							+ "</font><font color=#DAA520>" + 8 + "</font>";

					txthdr.setText(Html.fromHtml(source));
					/**Clear all selection ***/
					 c11 = cf.arr_db.rawQuery("SELECT * FROM " + cf.ImageTable
							+ " WHERE ARR_IM_SRID='" + cf.selectedhomeid
							+ "' and ARR_IM_Elevation='" + elev
							+ "' and ARR_IM_Insepctiontype='"+inspections+"' order by ARR_IM_ImageOrder", null);
					 
					 if(c11.getCount()>0)
					 {
						cf.ShowToast("Images saved successfully", 0);
						elev=0;
						inspections="-Select-";
						//elev_sp.setSelection(0);
						insp_sp.setSelection(0);
						lnrlayout.removeAllViews();
						((View) findViewById(R.id.PH_elev_lin)).setVisibility(View.GONE);
						name="";
						/**Clear all selection ***/
						nxtintent();
					 }
					 else
					 {
						 cf.ShowToast("Please upload atleast one image", 0); 
					 }
					break;
				case R.id.changeimageorder:
					change_image_order();
					break;
				
				}
			}
			else
			{
				cf.ShowToast("Please select Elevation Type", 0);
			}
		}
		else
		{
			cf.ShowToast("Please select Inspection Type", 0);
		}
		
		
	}

	private void selectphotos(String[] selected_paht)
	{
			Intent reptoit1 = new Intent(this,Select_phots.class);
			Bundle b=new Bundle();
			reptoit1.putExtra("Selectedvalue", selected_paht); /**Send the already selected image **/
			reptoit1.putExtra("Maximumcount", maximumindb);/**Total count of image in the database **/
			reptoit1.putExtra("Total_Maximumcount", max_allowed); /***Total count of image we need to accept**/
			//reptoit1.setClassName("com.idinspection","com.idinspection.Select_phots");
			startActivityForResult(reptoit1,121); /** Call the Select image page in the idma application  image ***/
	}

	protected void startCameraActivity() {
		/*String fileName = "temp.jpg";
		ContentValues values = new ContentValues();
		values.put(MediaStore.Images.Media.TITLE, fileName);
		CapturedImageURI = getContentResolver().insert(
				MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, CapturedImageURI);
		startActivityForResult(intent, 0);*/
		 Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
    	 intent.putExtra(MediaStore.EXTRA_OUTPUT, setImageUri());
        
		startActivityForResult(intent, 0);
	}
	protected Uri  setImageUri() {
		// TODO Auto-generated method stub
		  File file = new File(Environment.getExternalStorageDirectory() + "/DCIM/", "image" + new Date().getTime() + ".png");
          Uri imgUri = Uri.fromFile(file);
          this.filePath = file.getAbsolutePath();
          System.out.println("file="+file.getAbsolutePath());
		  System.out.println("imgPath="+this.filePath);
		return imgUri;
	}
	public String getImagePath() {
        return filePath;
    }

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		String selectedImagePath = "", picname = "";
		try
		{
		if (t == 0) {

			if (resultCode == RESULT_OK) {
				if (requestCode == 0) {
					System.out.println("testinside cmer");
					Uri selectedImageUri = data.getData();
					selectedImagePath = getPath(selectedImageUri);

					String[] bits = selectedImagePath.split("/");
					picname = bits[bits.length - 1];

				}
				else if(requestCode == 121)
				{
					/** we get the result from the Idma page for this elevation **/
 					try
 					{
					String[] selected_paht;
					separated=selected_insp;
					elevdb=0;
					cf.getPolicyholderInformation(cf.selectedhomeid);
				
					if(elev!=7)
					{
				
						if(dupflag.equals("1"))
						{
				
								for(int i=0;i<separated.length;i++)
							    {
									
									for(int j=0;j<cf.All_insp_list.length;j++)
									{
										if(cf.All_insp_list[j].trim().equals(separated[i].trim()))
										{
											String arr[] = sf.getelevation(separated[i].trim());
											for(int k=0;k<arr.length;k++)
								 			{
								 				if(name.trim().equals(arr[k].trim()))
								 				{
								 					if(j==4)
								 					{
								 						if(cf.Stories.equals("1") || cf.Stories.equals("2")  || cf.Stories.equals("3"))
								 						{
								 							j=4;		
								 						}
								 						else if(cf.Stories.equals("4") || cf.Stories.equals("5")  || cf.Stories.equals("6"))
								 						{
								 							j=5;		
								 						}
								 						else if(Integer.parseInt(cf.Stories)>=7)
								 						{	
								 							j=6;
								 						}
								 					}
								 					
								 					int elev= getelvationnumber(name, String.valueOf(j));
								 					
								 					Cursor c15 = cf.arr_db.rawQuery("SELECT * FROM "
								 	        				+ cf.ImageTable + " WHERE ARR_IM_SRID='"
								 	        				+ cf.selectedhomeid + "' and ARR_IM_Elevation='"
								 	        				+ elev + "'  and  ARR_IM_Insepctiontype='"+j+"' ORDER BY ARR_IM_ImageOrder DESC", null);
								 					
								 					
								 					System.out
															.println("TESTSELECT * FROM "
								 	        				+ cf.ImageTable + " WHERE ARR_IM_SRID='"
								 	        				+ cf.selectedhomeid + "' and ARR_IM_Elevation='"
								 	        				+ elev + "'  and  ARR_IM_Insepctiontype='"+j+"' ORDER BY ARR_IM_ImageOrder DESC");
								 					
								 					
								 					String[] value=	data.getExtras().getStringArray("Selected_array"); 
								 					
								 					if(c15.getCount()>0)
									 				{
									 					c15.moveToFirst();
									 					elevdb = c15.getInt(c15.getColumnIndex("ARR_IM_ImageOrder"));
										 					if(c15.getCount()<max_allowed)
											 				{
										 						System.out
																		.println("what is dupflag="+dupflag);
										 						
												 					for(int m=0;m<value.length;m++)
																	{	
												 						if((elevdb+m+1)<=max_allowed)
											 							{
												 							Cursor c1 = cf.arr_db.rawQuery("SELECT * FROM "
														 	        				+ cf.ImageTable + " WHERE ARR_IM_SRID='"
														 	        				+ cf.selectedhomeid + "' and ARR_IM_Elevation='"
														 	        				+ elev + "'  and  ARR_IM_Insepctiontype='"+j+"' and ARR_IM_ImageName='"+cf.encode(value[m])+"'", null);
												 							
												 							if(c1.getCount()==0)
												 							{
												 								cf.arr_db.execSQL("INSERT INTO "
															 							+ cf.ImageTable
															 							+ " (ARR_IM_SRID,ARR_IM_Insepctiontype,ARR_IM_Elevation,ARR_IM_ImageName,ARR_IM_Description,ARR_IM_Nameext,ARR_IM_CreatedOn,ARR_IM_ModifiedOn,ARR_IM_ImageOrder)"
															 							+ " VALUES ('" + cf.selectedhomeid + "','"+j+"','"
															 							+ elev + "','"
															 							+ cf.encode(value[m]) + "','"
															 							+ cf.encode(name + " "+(elevdb+m+1))
															 							+ "','','"
															 							+ cf.datewithtime + "','" +cf.datewithtime + "','" + (elevdb+1+m) + "')");
												 							}
																		}
																	}
										 						
											 				}
									 				}
									 				else
									 				{
									 					
									 						if(c15.getCount()<max_allowed)
								 							{
									 								for(int m=0;m<value.length;m++ )
																	{	
											 							Cursor c2 = cf.arr_db.rawQuery("SELECT * FROM "
													 	        				+ cf.ImageTable + " WHERE ARR_IM_SRID='"
													 	        				+ cf.selectedhomeid + "' and ARR_IM_Elevation='"
													 	        				+ elev + "'  and  ARR_IM_Insepctiontype='"+j+"' and ARR_IM_ImageName='"+cf.encode(value[m])+"'", null);
											 							if(c2.getCount()==0)
											 							{
											 								cf.arr_db.execSQL("INSERT INTO "
														 							+ cf.ImageTable
														 							+ " (ARR_IM_SRID,ARR_IM_Insepctiontype,ARR_IM_Elevation,ARR_IM_ImageName,ARR_IM_Description,ARR_IM_Nameext,ARR_IM_CreatedOn,ARR_IM_ModifiedOn,ARR_IM_ImageOrder)"
														 							+ " VALUES ('" + cf.selectedhomeid + "','"+j+"','"
														 							+ elev + "','"
														 							+ cf.encode(value[m]) + "','"
														 							+ cf.encode(name + " "+(maximumindb+m+1))
														 							+ "','','"
														 							+ cf.datewithtime + "','" +cf.datewithtime + "','" + (maximumindb+1+m) + "')");	
											 							}
											 						}
										 					}
									 					}
								 					}
								 			}
								 		}
									}
							    }
								
								
						}
						else						 
						{
							
							if(dupflag.equals("0"))
							{
							Cursor c15 = cf.arr_db.rawQuery("SELECT * FROM "
		 	        				+ cf.ImageTable + " WHERE ARR_IM_SRID='"
		 	        				+ cf.selectedhomeid + "' and ARR_IM_Elevation='"
		 	        				+ elev + "'  and  ARR_IM_Insepctiontype='"+inspections+"' ORDER BY ARR_IM_ImageOrder DESC", null);
		 					System.out.println("TTTT="+"SELECT * FROM "
		 	        				+ cf.ImageTable + " WHERE ARR_IM_SRID='"
		 	        				+ cf.selectedhomeid + "' and ARR_IM_Elevation='"
		 	        				+ elev + "'  and  ARR_IM_Insepctiontype='"+inspections+"' ORDER BY ARR_IM_ImageOrder DESC"+c15.getCount());
		 					String[] value=	data.getExtras().getStringArray("Selected_array"); 
		 					
		 					if(c15.getCount()>0)
			 				{
			 					c15.moveToFirst();
			 					elevdb = c15.getInt(c15.getColumnIndex("ARR_IM_ImageOrder"));
			 				}
		 					System.out.println("Sdsd"+elevdb+"mazx="+max_allowed);
		 					
		 					if((elevdb)<=max_allowed)
 							{
		 						System.out.println("greater than");
			 					for(int m=0;m<value.length;m++ )
								{
			 						System.out.println("INSERT INTO "
				 							+ cf.ImageTable
				 							+ " (ARR_IM_SRID,ARR_IM_Insepctiontype,ARR_IM_Elevation,ARR_IM_ImageName,ARR_IM_Description,ARR_IM_Nameext,ARR_IM_CreatedOn,ARR_IM_ModifiedOn,ARR_IM_ImageOrder)"
				 							+ " VALUES ('" + cf.selectedhomeid + "','"+inspections+"','"
				 							+ elev + "','"
				 							+ cf.encode(value[m]) + "','"
				 							+ cf.encode(name + " "+(elevdb+m+1))
				 							+ "','','"
				 							+ cf.datewithtime + "','" +cf.datewithtime + "','" + (elevdb+1+m) + "')");
			 						cf.arr_db.execSQL("INSERT INTO "
				 							+ cf.ImageTable
				 							+ " (ARR_IM_SRID,ARR_IM_Insepctiontype,ARR_IM_Elevation,ARR_IM_ImageName,ARR_IM_Description,ARR_IM_Nameext,ARR_IM_CreatedOn,ARR_IM_ModifiedOn,ARR_IM_ImageOrder)"
				 							+ " VALUES ('" + cf.selectedhomeid + "','"+inspections+"','"
				 							+ elev + "','"
				 							+ cf.encode(value[m]) + "','"
				 							+ cf.encode(name + " "+(elevdb+m+1))
				 							+ "','','"
				 							+ cf.datewithtime + "','" +cf.datewithtime + "','" + (elevdb+1+m) + "')");
								}
 							}
						}
						}
						String elevname = getelvationtext(elev);System.out.println("elevname="+elevname);
						cf.ShowToast(elevname + " Image added successfully", 0);
						show_savedvalue();

					}
					else
					{
						if(dupflag.equals("1"))
						{
						for(int i=0;i<separated.length;i++)
					    {							
							for(int j=0;j<cf.All_insp_list.length;j++)
							{
								if(cf.All_insp_list[j].trim().equals(separated[i].trim()))
								{
									
									if(j==4)
				 					{
				 						if(cf.Stories.equals("1") || cf.Stories.equals("2")  || cf.Stories.equals("3"))
				 						{
				 							j=4;		
				 						}
				 						else if(cf.Stories.equals("4") || cf.Stories.equals("5")  || cf.Stories.equals("6"))
				 						{
				 							j=5;		
				 						}
				 						else if(Integer.parseInt(cf.Stories)>=7)
				 						{	
				 							j=6;
				 						}
				 					}
									
									Cursor c15 = cf.arr_db.rawQuery("SELECT * FROM "
				 	        				+ cf.ImageTable + " WHERE ARR_IM_SRID='"
				 	        				+ cf.selectedhomeid + "' and ARR_IM_Elevation='"
				 	        				+ elev + "'  and  ARR_IM_Insepctiontype='"+j+"' ORDER BY ARR_IM_ImageOrder DESC", null);
				 					System.out.println("dffdsff"+c15.getCount());
				 					String[] value=	data.getExtras().getStringArray("Selected_array"); 
				 					System.out.println("value"+value.length);
				 					if(c15.getCount()>0)
					 				{
					 					c15.moveToFirst();
					 					elevdb = c15.getInt(c15.getColumnIndex("ARR_IM_ImageOrder"));System.out.println("waht is le="+elevdb);
						 					if(c15.getCount()<max_allowed)
							 				{
						 						for(int m=0;m<value.length;m++)
												{	
							 						if((elevdb+m+1)<=max_allowed)
						 							{
							 							Cursor c1 = cf.arr_db.rawQuery("SELECT * FROM "
									 	        				+ cf.ImageTable + " WHERE ARR_IM_SRID='"
									 	        				+ cf.selectedhomeid + "' and ARR_IM_Elevation='"
									 	        				+ elev + "'  and  ARR_IM_Insepctiontype='"+j+"' and ARR_IM_ImageName='"+cf.encode(value[m])+"'", null);
							 							
							 							if(c1.getCount()==0)
							 							{
							 								cf.arr_db.execSQL("INSERT INTO "
										 							+ cf.ImageTable
										 							+ " (ARR_IM_SRID,ARR_IM_Insepctiontype,ARR_IM_Elevation,ARR_IM_ImageName,ARR_IM_Description,ARR_IM_Nameext,ARR_IM_CreatedOn,ARR_IM_ModifiedOn,ARR_IM_ImageOrder)"
										 							+ " VALUES ('" + cf.selectedhomeid + "','"+j+"','"
										 							+ elev + "','"
										 							+ cf.encode(value[m]) + "','"
										 							+ cf.encode(name + " "+(elevdb+m+1))
										 							+ "','','"
										 							+ cf.datewithtime + "','" +cf.datewithtime + "','" + (elevdb+1+m) + "')");
							 							}
													}
												}
							 				}
					 				}
				 					else
				 					{
				 						for(int m=0;m<value.length;m++)
										{	
				 							Cursor c2 = cf.arr_db.rawQuery("SELECT * FROM "
						 	        				+ cf.ImageTable + " WHERE ARR_IM_SRID='"
						 	        				+ cf.selectedhomeid + "' and ARR_IM_Elevation='"
						 	        				+ elev + "'  and  ARR_IM_Insepctiontype='"+j+"' and ARR_IM_ImageName='"+cf.encode(value[m])+"'", null);
				 							if(c2.getCount()==0)
				 							{
				 								cf.arr_db.execSQL("INSERT INTO "
							 							+ cf.ImageTable
							 							+ " (ARR_IM_SRID,ARR_IM_Insepctiontype,ARR_IM_Elevation,ARR_IM_ImageName,ARR_IM_Description,ARR_IM_Nameext,ARR_IM_CreatedOn,ARR_IM_ModifiedOn,ARR_IM_ImageOrder)"
							 							+ " VALUES ('" + cf.selectedhomeid + "','"+j+"','"
							 							+ elev + "','"
							 							+ cf.encode(value[m]) + "','"
							 							+ cf.encode(name + " "+(maximumindb+m+1))
							 							+ "','','"
							 							+ cf.datewithtime + "','" +cf.datewithtime + "','" + (maximumindb+1+m) + "')");	
				 							}
				 						}
		 							
				 					}
								}
							}
					    }
						
						}
						else if(dupflag.equals("0"))
						{
							System.out.println("FFFFFFF");
							Cursor c15 = cf.arr_db.rawQuery("SELECT * FROM "
		 	        				+ cf.ImageTable + " WHERE ARR_IM_SRID='"
		 	        				+ cf.selectedhomeid + "' and ARR_IM_Elevation='"
		 	        				+ elev + "'  and  ARR_IM_Insepctiontype='"+inspections+"' ORDER BY ARR_IM_ImageOrder DESC", null);
		 					
		 					String[] value=	data.getExtras().getStringArray("Selected_array"); 
		 					
		 					if(c15.getCount()>0)
			 				{
			 					c15.moveToFirst();
			 					elevdb = c15.getInt(c15.getColumnIndex("ARR_IM_ImageOrder"));
			 				}
		 					if((elevdb+1)<max_allowed)
 							{
		 						
			 					for(int m=0;m<value.length;m++ )
								{
			 						cf.arr_db.execSQL("INSERT INTO "
				 							+ cf.ImageTable
				 							+ " (ARR_IM_SRID,ARR_IM_Insepctiontype,ARR_IM_Elevation,ARR_IM_ImageName,ARR_IM_Description,ARR_IM_Nameext,ARR_IM_CreatedOn,ARR_IM_ModifiedOn,ARR_IM_ImageOrder)"
				 							+ " VALUES ('" + cf.selectedhomeid + "','"+inspections+"','"
				 							+ elev + "','"
				 							+ cf.encode(value[m]) + "','"
				 							+ cf.encode(name + " "+(elevdb+m+1))
				 							+ "','','"
				 							+ cf.datewithtime + "','" +cf.datewithtime + "','" + (elevdb+1+m) + "')");
								}
 							}
							
						}
					}
					System.out.println("eleeel"+elev);
								String elevname = getelvationtext(elev);System.out.println("elevname="+elevname);
								cf.ShowToast(elevname + " Image added successfully", 0);
								show_savedvalue();
 					}
 					catch(Exception e)
 					{
 						System.out.println("eee="+e.getMessage());
 					}
					
					/** we get the result from the Idma page for this elevation Ends **/	
				}
				else if(requestCode == 125)
				{
					if(upd_img!=null)
					{
						try
						{
						String[] value=	data.getExtras().getStringArray("Selected_array"); /**We pass the array of tje value from the IDAM Select page **/
						if(value.length>0)
						{
							if(!value[0].equals(""))
							{
								updated_path=value[0];
								 currnet_rotated=0;
								 Bitmap b1=cf.ShrinkBitmap(updated_path, 400, 400);
									if(b1!=null)
									{
										upd_img.setImageBitmap(b1);
									}
							}
						}
						
						}
						catch (Exception e) {
							// TODO: handle exception
						}
					}
					
				}
			} else {
				selectedImagePath = "";
				// edbrowse.setText("");
			}
		} else if (t == 1) {
			switch (resultCode) {
			case 0:
				selectedImagePath = "";

				break;
			case -1:
				try {
					/*	String[] projection = { MediaStore.Images.Media.DATA };
					Cursor cursor = managedQuery(CapturedImageURI, projection,
							null, null, null);
					int column_index_data = cursor
							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					cursor.moveToFirst();
					String capturedImageFilePath = cursor
							.getString(column_index_data);*/
					
					System.out.println("filepath");
					
					selectedImagePath = filePath;
					System.out.println("selectedImagePath"+selectedImagePath);
					display_taken_image(selectedImagePath);
							show_savedvalue();
				} catch (Exception e) {
					System.out.println("my exception " + e);
				}

				break;

			}

		}
		}
		catch(Exception e)
		{
			System.out.println("no more issues "+e.getMessage());
		}
		
	}
	 private String getelvationtext(int elev) {
		// TODO Auto-generated method stub
		 String selected="";
		 if(elev==1)	{selected="Exterior Photographs";}
		 else if(elev==2){ selected="Roof Photographs";}
		 else if(elev==3){ selected="Plumbing Photographs";}
		 else if(elev==4){ selected="Electrical Photographs";}
		 else if(elev==5){ selected="HVAC Photographs";}
		 else if(elev==7){ selected="Other Photographs";}
		 else if(elev==23){ selected="Front Photo";}
		 else if(elev==6){selected="Attic Photographs";}
		 else if(elev==17){selected="Front Elevation";}
		 else if(elev==18){selected="Right Elevation";}
		 else if(elev==19){selected="Left Elevation";}
		 else if(elev==20){selected="Back Elevation";}
		 else if(elev==12){selected="Additional Photographs";}	
		 else if(elev==8){ selected="External Photographs";}
		 else if(elev==9){ selected="Roof and attic Photographs";}
		 else if(elev==10){ selected="Ground and adjoining images";}
		 else if(elev==11){ selected="Internal Photographs";}
		 else if(elev==26){ selected="Interior Fixture Photographs";}
		 else if(elev==27){ selected="Drywall Photographs";}
	
		return selected;
	}

	public void getinspectionid(String inspname) 
	 {
			// TODO Auto-generated method stub
		 System.out.println("inspnam"+inspname+"cf.deco"+cf.encode(inspname));
	   	 Cursor c =cf.SelectTablefunction(cf.inspnamelist, " Where ARR_Insp_Name='"+cf.encode(inspname)+"'");System.out.println("sdf"+c.getCount());
	   	 if(c.getCount()>0)
	   	 {
	   		 c.moveToFirst();
	   		 customid = cf.decode(c.getString(c.getColumnIndex("ARR_Custom_ID"))); System.out.println("customid"+customid);
	   	 }
	 }
	public String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}

	public int getImageOrder(int Elevationtype, String ARR_IM_SRID,String inspections) {
		int ImgOrder = 0;
		System.out.println("inspections inside="+inspections);
		Cursor c11 = cf.arr_db.rawQuery("SELECT * FROM " + cf.ImageTable
				+ " WHERE ARR_IM_SRID='" + ARR_IM_SRID
				+ "'  and  ARR_IM_Insepctiontype='"+inspections+"' and ARR_IM_Elevation='" + Elevationtype
				+ "' order by ARR_IM_CreatedOn DESC", null);
		int imgrws = c11.getCount();
		if (imgrws == 0) {
			ImgOrder = imgrws + 1;
		} else {
			ImgOrder = imgrws + 1;

		}
		return ImgOrder;
	}
/** change the image order starts**/
	protected void change_image_order() {
 		// TODO Auto-generated method stub
		 final Dialog dialog;
		 Bitmap[] thumbnails1;
		 String[] arrPath1,str;
		 final int[] imageid;
		int[] SH_IM_ImageOrder;
		 ImageView[] chag_img;
		 final EditText[]  chan_ed;
 		Cursor d11 = cf.arr_db.rawQuery("SELECT * FROM " + cf.ImageTable
 				+ " WHERE ARR_IM_SRID='" + cf.selectedhomeid + "'  and  ARR_IM_Insepctiontype='"+inspections+"' and ARR_IM_Elevation='" + elev
 				+ "' order by ARR_IM_ImageOrder", null);
 		 rws = d11.getCount();
 		if (rws == 0) {
 			cf.ShowToast(" Images not found! Please upload images and try again.",0);
 		} else {
 			String Imagepath[] = new String[rws];

 		  EditText[] et = new EditText[rws];

 		    dialog = new Dialog(photos.this,android.R.style.Theme_Translucent_NoTitleBar);
 			dialog.setContentView(R.layout.changeimageorder);
 			dialog.setTitle("Change Image Order");
 			dialog.setCancelable(false);
 			Button save=(Button) dialog.findViewById(R.id.Save);
 			Button close=(Button) dialog.findViewById(R.id.close);
 			ImageView helpclose=(ImageView) dialog.findViewById(R.id.helpclose);
 			
            TableLayout ln_main=(TableLayout) dialog.findViewById(R.id.changeorder);
 			try {

 				//this.count1 = d11.getCount();
 				thumbnails1 = new Bitmap[rws];
 				arrPath1 = new String[rws];
 				imageid = new int[rws];
 				SH_IM_ImageOrder = new int[rws];
 				str = new String[rws];
 				chag_img = new ImageView[rws];
 				chan_ed=new EditText[rws]; 
 				TableRow ln_sub=null;
 				d11.moveToFirst();

 				for (int i = 0; i < rws; i++) {
 					/**We create the new check box and edit box for the change image order **/
 					chag_img[i]=new ImageView(photos.this); 
 					TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
 					TableRow.LayoutParams lp2 = new TableRow.LayoutParams(100, 100);
 					lp.setMargins(5, 10, 5, 10);
 					chag_img[i].setLayoutParams(lp2);
 					chan_ed[i]=new EditText(photos.this);
 					//chan_ed[i].setMa))(2);
 					chan_ed[i].setInputType(InputType.TYPE_CLASS_NUMBER);
 					chan_ed[i].setLayoutParams(lp);
 					
 					InputFilter[] fArray = new InputFilter[1];
 					fArray[0] = new InputFilter.LengthFilter(etchangeordermaxLength);
 					chan_ed[i].setFilters(fArray);
 					LinearLayout li_tmp=new LinearLayout(photos.this);
 					li_tmp.setLayoutParams(lp);
 					
 					/**We create the new check box and edit box for the change image order ends  **/
 					imageid[i] = Integer.parseInt(d11.getString(0)
 							.toString());
 					SH_IM_ImageOrder[i] = d11.getInt(d11
 							.getColumnIndex("ARR_IM_ImageOrder"));
 					arrPath1[i] = cf.decode(d11.getString(d11
 							.getColumnIndex("ARR_IM_ImageName")));
 					Bitmap b = cf.ShrinkBitmap(arrPath1[i], 130, 130);
 					thumbnails1[i] = b;
 					if(i==0)
 					{
 						
 						LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
							ln_sub = new TableRow(photos.this);
							ln_sub.setLayoutParams(lp1);  
 					}
 					li_tmp.addView(chag_img[i]);
 					li_tmp.addView(chan_ed[i]);
 					ln_sub.addView(li_tmp);
 					chag_img[i].setImageBitmap(b);
 					chan_ed[i].setText(String.valueOf(SH_IM_ImageOrder[i]));
 					d11.moveToNext();
 					if((((i+1)%3)==0 || (i+1)==rws)&& ln_sub!=null )
						{
							ln_main.addView(ln_sub);
							LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
							ln_sub = new TableRow(photos.this);
							ln_sub.setLayoutParams(lp1);  
						}
 					
 				}
 				helpclose.setOnClickListener(new OnClickListener() {

 					public void onClick(View v) {
 						// TODO Auto-generated method stub
 						dialog.setCancelable(true);
 						dialog.cancel();
 					}
 				});
 				close.setOnClickListener(new OnClickListener() {

 					public void onClick(View v) {
 						// TODO Auto-generated method stub
 						dialog.setCancelable(true);
 						dialog.cancel();
 					}
 				});

 				save.setOnClickListener(new OnClickListener() {

 					public void onClick(View v) {
 						// TODO Auto-generated method stub
 						// dialog.cancel();
 						
 						try {
 							Boolean boo = true;
 	 						String temp[] = new String[rws];
 							for (int i = 0; i < imageid.length; i++) {
 								temp[i] = chan_ed[i].getText().toString();
 								int myorder = 0;
 								if (temp[i] != null && !temp[i].equals("")) {
 									myorder = Integer.parseInt(temp[i]);
 									if (myorder <= imageid.length
 											&& myorder != 0) {

 									} else {
 										boo = false;isgreater=false;isexists=true;isempty=true;
 									}
 								} else {
 									boo = false;isempty=false;isgreater=true;isexists=true;
 								}
 							} 							
 							int length = temp.length;
 							for (int i = 0; i < length; i++) {
 								for (int j = 0; j < length; j++) {
 									if (temp[i].equals(temp[j]) && i != j) {
 										boo = false;isexists=false;isgreater=true;isempty=true;
 									}
 								}
 							}							
 							System.out.println("isgreater="+isgreater+"isexists="+isexists+"boo="+boo);
 							if(boo==false && isgreater==false)
 							{
 								//cf.ShowToast("Image Order should match with the total number of Images uploaded.",0);
 								cf.ShowToast("Please enter the image order between 1 -"+rws, 0);
 							}
 							else if(boo==false && isexists==false)
 							{
 								cf.ShowToast("Image Order already exists.",0);
 							}
 							else if(boo==false && isempty==false)
 							{
 								cf.ShowToast("Please enter the image order.",0);
 							} 
 							else 
 							{
 								for (int i = 0; i < imageid.length; i++) {
 									cf.arr_db.execSQL("UPDATE " + cf.ImageTable
 											+ " SET ARR_IM_ImageOrder='" + temp[i]
 											+ "' WHERE ARR_IM_ID='" + imageid[i]
 											+ "' ");
 								}
 								dialog.setCancelable(true);
 								dialog.cancel();
 								cf.ShowToast("Change Image Order saved successfully.",0);
 								show_savedvalue();
 							}
 						}

 						catch (Exception e) {

 						}
 					}
 				});
                
 				dialog.show();

 				
 			} catch (Exception m) {
 				
 			}
 		}

 	}
/** change the image order Ends**/	
	public 	Bitmap ShrinkBitmap(String file, int width, int height) { try {
		BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
		bmpFactoryOptions.inJustDecodeBounds = true;
		Bitmap bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
	
		int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight
				/ (float) height);
		int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth
				/ (float) width);
	
		if (heightRatio > 1 || widthRatio > 1) {
			if (heightRatio > widthRatio) {
				bmpFactoryOptions.inSampleSize = heightRatio;
			} else {
				bmpFactoryOptions.inSampleSize = widthRatio;
			}
		}
	
		bmpFactoryOptions.inJustDecodeBounds = false;
		bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
		return bitmap;
	} catch (Exception e) {
		return null;
	}
	
	}

	private void nxtintent() {
 		// TODO Auto-generated method stub
		
 	/*
 		if (elev == 1) {
 		  Intent myintent =new Intent(photos.this,photos.class);
 		  cf.putExtras(myintent);
 		  myintent.putExtra("type", 52);
 		 startActivity(myintent);
 		} else if (elev == 2) {
 			Intent myintent =new Intent(photos.this,photos.class);
	 		  cf.putExtras(myintent);
	 		 myintent.putExtra("type", 53);
	 		 startActivity(myintent);
 		} else if (elev == 3) {
 			Intent myintent =new Intent(photos.this,photos.class);
	 		  cf.putExtras(myintent);
	 		 myintent.putExtra("type", 54);
	 		startActivity(myintent);
 		} else if (elev == 4) {
 			Intent myintent =new Intent(photos.this,photos.class);
	 		  cf.putExtras(myintent);
	 		 myintent.putExtra("type", 56);
	 		startActivity(myintent);
 		} else if (elev == 5) {

 			Intent myintent =new Intent(photos.this,Feedback.class);
	 		  cf.putExtras(myintent);
	 		 startActivity(myintent);
	 		 
 		} else if (elev == 6) {
 			Intent myintent =new Intent(photos.this,photos.class);
	 		  cf.putExtras(myintent);
	 		 myintent.putExtra("type", 55);
	 		startActivity(myintent);
	 		 
		}*/
 	}
	
 		
 	
	public void display_taken_image(final String slectimage)
	{
		 System.gc();
		 globalvar=0;
		System.out.println("no more issue");
		BitmapFactory.Options o = new BitmapFactory.Options();
 		o.inJustDecodeBounds = true;
	
 		final Dialog dialog1 = new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar);
		dialog1.getWindow().setContentView(R.layout.alert);
		
		/** get the help and update relative layou and set the visbilitys for the respective relative layout */
		LinearLayout Re=(LinearLayout) dialog1.findViewById(R.id.maintable);
		Re.setVisibility(View.GONE);
		LinearLayout Reup=(LinearLayout) dialog1.findViewById(R.id.updateimage);    
		Reup.setVisibility(View.GONE);
		LinearLayout camerapic=(LinearLayout) dialog1.findViewById(R.id.cameraimage);
		camerapic.setVisibility(View.VISIBLE);
		TextView tvcamhelp = (TextView)dialog1.findViewById(R.id.camtxthelp);
		tvcamhelp.setText("Save Picture");
		System.out.println("no more issue 2");
		tvcamhelp.setTextSize(TypedValue.COMPLEX_UNIT_PX,14);
		 ((RelativeLayout)dialog1.findViewById(R.id.cam_photo_update)).setVisibility(View.VISIBLE);
		((RelativeLayout)dialog1.findViewById(R.id.camaddcaption)).setVisibility(View.GONE);
		((TextView) dialog1.findViewById(R.id.elevtxt)).setVisibility(View.GONE);
		((Spinner) dialog1.findViewById(R.id.cameraelev)).setVisibility(View.GONE);
		/** get the help and update relative layou and set the visbilitys for the respective relative layout */
		
		final ImageView upd_img = (ImageView) dialog1.findViewById(R.id.cameraimg);
		ImageView btn_helpclose = (ImageView) dialog1.findViewById(R.id.camhelpclose);
		//((Button) dialog1.findViewById(R.id.camsave)).setVisibility(View.GONE);
		
		final Button btn_save = (Button) dialog1.findViewById(R.id.camsave);
		
		Button rotatecamleft = (Button) dialog1.findViewById(R.id.rotatecamimgleft);
		Button rotatecamright = (Button) dialog1.findViewById(R.id.rotatecamright);
		
		try {
			
			Bitmap myImg = BitmapFactory.decodeStream(new FileInputStream(slectimage));
			Bitmap bitmap = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),null, true);
			 BitmapDrawable bmd = new BitmapDrawable(bitmap);
			  upd_img.setImageDrawable(bmd);
			
				/*BitmapFactory.decodeStream(new FileInputStream(slectimage),
						null, o);
				final int REQUIRED_SIZE = 400;
 			int width_tmp = o.outWidth, height_tmp = o.outHeight;
 			int scale = 1;
 			while (true) {
 				if (width_tmp / 2 < REQUIRED_SIZE
 						|| height_tmp / 2 < REQUIRED_SIZE)
 					break;
 				width_tmp /= 2;
 				height_tmp /= 2;
 				scale *= 2;
 				BitmapFactory.Options o2 = new BitmapFactory.Options();
	 			o2.inSampleSize = scale;
	 			Bitmap bitmap = null;
	 	    	bitmap = BitmapFactory.decodeStream(new FileInputStream(
	 	    			slectimage), null, o2);
	 	       BitmapDrawable bmd = new BitmapDrawable(bitmap);
	 	      System.out.println("no more issue 5");
 			   upd_img.setImageDrawable(bmd);
 			  System.out.println("no more issue 6");
 			}*/
		

	}
		catch(Exception e)
		{
			System.out.println("there is error in the shoe selected img"+e.getMessage());
		}
		btn_save.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {

            	
            	if(globalvar==0)
        		{

            	globalvar=1;
            	btn_save.setVisibility(v.GONE);
            	String[] bits = slectimage.split("/");
        		String picname = bits[bits.length - 1];
        		Cursor c15 = cf.arr_db.rawQuery("SELECT * FROM "
        				+ cf.ImageTable + " WHERE ARR_IM_SRID='"
        				+ cf.selectedhomeid + "' and ARR_IM_Elevation='"
        				+ elev + "'  and  ARR_IM_Insepctiontype='"+inspections+"'", null);
        		int count_tot = c15.getCount();
        		int ImgOrder = getImageOrder(elev, cf.selectedhomeid,inspections);
        		try {
        			
//        			if(name.trim().equals("Front Photo"))
//					{
//						getinspectionid(insp_sp.getSelectedItem().toString());
//						cf.arr_db.execSQL("INSERT INTO "
//	        					+ cf.ImageTable
//	        					+ " (ARR_IM_SRID,ARR_IM_Insepctiontype,ARR_IM_Elevation,ARR_IM_ImageName,ARR_IM_Description,ARR_IM_Nameext,ARR_IM_CreatedOn,ARR_IM_ModifiedOn,ARR_IM_ImageOrder)"
//	        					+ " VALUES ('" + cf.selectedhomeid + "','"+customid+"','"
//	        					+ elev + "','" + cf.encode(slectimage)
//	        					+ "','" + cf.encode(name + (count_tot + 1))
//	        					+ "','" + cf.encode(picname) + "','"
//	        					+ cf.datewithtime + "','" + cf.datewithtime
//	        					+ "','" + ImgOrder + "')");
//					}
//        			else
//        			{
        			if(dupflag.equals("1"))
					{

        					for(int i=0;i<selected_insp.length;i++)
						    {
								for(int j=0;j<cf.All_insp_list.length;j++)
								{
									if(cf.All_insp_list[j].equals(selected_insp[i]))
									{
										
							 			String arr[] = sf.getelevation(selected_insp[i]);
							 			for(int k=0;k<arr.length;k++)
							 			{
							 				if(name.equals(arr[k]))
							 				{
							 					if(j==4)
							 					{
							 						if(cf.Stories.equals("1") || cf.Stories.equals("2")  || cf.Stories.equals("3"))
							 						{
							 							j=4;		
							 						}
							 						else if(cf.Stories.equals("4") || cf.Stories.equals("5")  || cf.Stories.equals("6"))
							 						{
							 							j=5;		
							 						}
							 						else if(Integer.parseInt(cf.Stories)>=7)
							 						{	
							 							j=6;
							 						}
							 					}
							 					System.out
														.println("whats is j="+j);
							 					int elev= getelvationnumber(name, String.valueOf(j));
							 					System.out
														.println("elev inside="+elev);
							 						Cursor c1 = cf.arr_db.rawQuery("SELECT * FROM "
							 	        				+ cf.ImageTable + " WHERE ARR_IM_SRID='"
							 	        				+ cf.selectedhomeid + "' and ARR_IM_Elevation='"
							 	        				+ elev + "'  and  ARR_IM_Insepctiontype='"+j+"'", null);
							 						ImgOrder = getImageOrder(elev, cf.selectedhomeid,String.valueOf(j));System.out.println("SELECT * FROM "
								 	        				+ cf.ImageTable + " WHERE ARR_IM_SRID='"
								 	        				+ cf.selectedhomeid + "' and ARR_IM_Elevation='"
								 	        				+ elev + "'  and  ARR_IM_Insepctiontype='"+j+"'"+"count="+c1.getCount());
							 						System.out.println("ImgOrder="+ImgOrder);
							 					if(c1.getCount()<max_allowed)
							 					{
							 						System.out
															.println("INSERT INTO "
							        					+ cf.ImageTable
							        					+ " (ARR_IM_SRID,ARR_IM_Insepctiontype,ARR_IM_Elevation,ARR_IM_ImageName,ARR_IM_Description,ARR_IM_Nameext,ARR_IM_CreatedOn,ARR_IM_ModifiedOn,ARR_IM_ImageOrder)"
							        					+ " VALUES ('" + cf.selectedhomeid + "','"+j+"','"
							        					+ elev + "','" + cf.encode(slectimage)
							        					+ "','" + cf.encode(name + " "+(ImgOrder))
							        					+ "','" + cf.encode(picname) + "','"
							        					+ cf.datewithtime + "','" + cf.datewithtime
							        					+ "','" + ImgOrder + "')");
							 						cf.arr_db.execSQL("INSERT INTO "
							        					+ cf.ImageTable
							        					+ " (ARR_IM_SRID,ARR_IM_Insepctiontype,ARR_IM_Elevation,ARR_IM_ImageName,ARR_IM_Description,ARR_IM_Nameext,ARR_IM_CreatedOn,ARR_IM_ModifiedOn,ARR_IM_ImageOrder)"
							        					+ " VALUES ('" + cf.selectedhomeid + "','"+j+"','"
							        					+ elev + "','" + cf.encode(slectimage)
							        					+ "','" + cf.encode(name + " "+(ImgOrder))
							        					+ "','" + cf.encode(picname) + "','"
							        					+ cf.datewithtime + "','" + cf.datewithtime
							        					+ "','" + ImgOrder + "')");
							 					}
							 				}
							 			}
									}
								}
					}
					}
        			else if(dupflag.equals("0"))
        			{
        				System.out.println("FFFFFFF");
						Cursor c1 = cf.arr_db.rawQuery("SELECT * FROM "
	 	        				+ cf.ImageTable + " WHERE ARR_IM_SRID='"
	 	        				+ cf.selectedhomeid + "' and ARR_IM_Elevation='"
	 	        				+ elev + "'  and  ARR_IM_Insepctiontype='"+inspections+"' ORDER BY ARR_IM_ImageOrder DESC", null);
	 					
	 					
	 					if(c1.getCount()>0)
		 				{
		 					c1.moveToFirst();
		 					elevdb = c1.getInt(c1.getColumnIndex("ARR_IM_ImageOrder"));
		 				}
	 					System.out.println("elevdb"+elevdb);
	 					if(c1.getCount()<max_allowed)
							{
	 						
		 						cf.arr_db.execSQL("INSERT INTO "
			 							+ cf.ImageTable
			 							+ " (ARR_IM_SRID,ARR_IM_Insepctiontype,ARR_IM_Elevation,ARR_IM_ImageName,ARR_IM_Description,ARR_IM_Nameext,ARR_IM_CreatedOn,ARR_IM_ModifiedOn,ARR_IM_ImageOrder)"
			 							+ " VALUES ('" + cf.selectedhomeid + "','"+inspections+"','"
			 							+ elev + "','"
			 							+ cf.encode(slectimage) + "','"
			 							+ cf.encode(name + " "+(ImgOrder))
			 							+ "','','"
			 							+ cf.datewithtime + "','" +cf.datewithtime + "','" + ImgOrder+ "')");
							}

        			}
        		//}
        			//dialog1.dismiss();
        			
        			/**Save the rotated value in to the external stroage place **/
					if(currnet_rotated>0)
					{ 

						try
						{
							/**Create the new image with the rotation **/
							String current=MediaStore.Images.Media.insertImage(getContentResolver(), rotated_b, "My bitmap", "My rotated bitmap");
							 ContentValues values = new ContentValues();
							  values.put(MediaStore.Images.Media.ORIENTATION, 0);
							  photos.this.getContentResolver().update(Uri.parse(current), values, MediaStore.Images.Media.DATA+ "=?", new String[] { current } );
							
							
							if(current!=null)
							{
							String path=getPath(Uri.parse(current));
							File fout = new File(slectimage);
							fout.delete();
							/** delete the selected image **/
							File fin = new File(path);
							/** move the newly created image in the slected image pathe ***/
							fin.renameTo(new File(slectimage));
							
							
						}
						} catch(Exception e)
						{
							System.out.println("Error occure while rotate the image "+e.getMessage());
						}
						
					}
				
				/**Save the rotated value in to the external stroage place ends **/
					show_savedvalue();
					cf.ShowToast("Selected image added successfully", 0);
					dialog1.setCancelable(true);
	            	dialog1.dismiss();
        		} catch (Exception e) {
        			System.out.println("e= " + e.getMessage());
        		}
        		}
            }
        });
		btn_helpclose.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
            	dialog1.setCancelable(true);
            	dialog1.dismiss();
            }
		});
		rotatecamleft.setOnClickListener(new OnClickListener() {  
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 System.gc();
				 currnet_rotated-=90;
				 System.out.println("Rotating the image right  bfr"+currnet_rotated);
					if(currnet_rotated<0)
					{
						currnet_rotated=270;
					}	
					System.out.println("Rotating the image right  afr"+currnet_rotated);
				Bitmap myImg;
				try {
					myImg = BitmapFactory.decodeStream(new FileInputStream(slectimage));
					Matrix matrix =new Matrix();
					matrix.reset();
					matrix.setRotate(currnet_rotated);
					rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),matrix, true);
					System.gc();
					upd_img.setImageBitmap(rotated_b);

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch(Exception e)
				{
					System.out.println("rota eeee "+e.getMessage());
				}

				catch (OutOfMemoryError e) {
					System.out.println("comes in to out ot mem exception");
					System.gc();
					try {
						myImg=null;
						System.gc();
						Matrix matrix =new Matrix();
						matrix.reset();
						matrix.postRotate(currnet_rotated);
						myImg= cf.ShrinkBitmap(slectimage, 800, 800);
						rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
						System.gc();
						upd_img.setImageBitmap(rotated_b); 

					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					 catch (OutOfMemoryError e1) {
							// TODO Auto-generated catch block
						 cf.ShowToast("You cannot rotate this image. Image size exceeds 2MB.",0);
					}
				}
			}
		});
		rotatecamright.setOnClickListener(new OnClickListener() {  
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 System.gc();
				currnet_rotated+=90;
				System.out.println("Rotating the image right  bfr"+currnet_rotated);
				if(currnet_rotated>=360)
				{
					currnet_rotated=0;
				}
				System.out.println("Rotating the image right  afr"+currnet_rotated);
				Bitmap myImg;
				try {
					myImg = BitmapFactory.decodeStream(new FileInputStream(slectimage));
					Matrix matrix =new Matrix();
					matrix.reset();
					matrix.setRotate(currnet_rotated);
					rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),matrix, true);
					System.gc();
					upd_img.setImageBitmap(rotated_b);

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch(Exception e)
				{
					System.out.println("rota eeee "+e.getMessage());
				}

				catch (OutOfMemoryError e) {
					System.out.println("comes in to out ot mem exception");
					System.gc();
					try {
						myImg=null;
						System.gc();
						Matrix matrix =new Matrix();
						matrix.reset();
						matrix.postRotate(currnet_rotated);
						myImg= cf.ShrinkBitmap(slectimage, 800, 800);
						rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
						System.gc();
						upd_img.setImageBitmap(rotated_b); 

					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					 catch (OutOfMemoryError e1) {
							// TODO Auto-generated catch block
						 cf.ShowToast("You cannot rotate this image. Image size exceeds 2MB.",0);
					}
				}
			}
		});
		dialog1.setCancelable(false);
		dialog1.show();
	}
	public int getelvationnumber(String selected,String insp)
	{
		System.out.println("correct"+selected+" insp"+insp);
		int elev=0;
		if(insp.equals("1"))
		{
			if(selected.equals("Exterior Photographs"))elev=1;
			if(selected.equals("Roof Photographs"))elev=2;
			if(selected.equals("Plumbing Photographs"))elev=3;
			if(selected.equals("Electrical Photographs"))elev=4;
			if(selected.equals("HVAC Photographs"))elev=5;
			if(selected.equals("Other Photographs"))elev=7;
			if(selected.equals("Front Photo"))elev=23;
		}
		else if(insp.equals("2"))
		{
			if(selected.equals("Roof Photographs"))elev=2;
			if(selected.equals("Other Photographs"))elev=7;
			/**need to change ***/
			if(selected.equals("Exterior Photographs"))elev=1;
			if(selected.equals("Attic Photographs"))elev=6;
			if(selected.equals("Front Photo"))elev=23;
			if(selected.equals("Roof Cover Type Photos"))elev=24;
			
		}
		else if(insp.equals("3"))
		{
			//if(selected.equals("Owner signature"))elev=17;
			if(selected.equals("Front Elevation"))elev=17;
			if(selected.equals("Right Elevation"))elev=18;
			if(selected.equals("Left Elevation"))elev=19;
			if(selected.equals("Back Elevation"))elev=20;
			if(selected.equals("Attic Photographs"))elev=6;
			if(selected.equals("Additional Photographs"))elev=12;
			if(selected.equals("Other Photographs"))elev=7;
			if(selected.equals("Front Photo"))elev=23;
		}
		else if(insp.equals("4"))
		{
			//if(selected.equals("Owner signature"))elev=25;
			if(selected.equals("Front Elevation"))elev=17;
			if(selected.equals("Right Elevation"))elev=18;
			if(selected.equals("Left Elevation"))elev=19;
			if(selected.equals("Back Elevation"))elev=20;
			if(selected.equals("Attic Photographs"))elev=6;
			if(selected.equals("Additional Photographs"))elev=12;
			if(selected.equals("Other Photographs"))elev=7;
			if(selected.equals("Front Photo"))elev=23;
		}
		else if(insp.equals("5"))
		{
			//if(selected.equals("Owner signature"))elev=34;
			if(selected.equals("Front Elevation"))elev=17;
			if(selected.equals("Right Elevation"))elev=18;
			if(selected.equals("Left Elevation"))elev=19;
			if(selected.equals("Back Elevation"))elev=20;
			if(selected.equals("Attic Photographs"))elev=6;
			if(selected.equals("Additional Photographs"))elev=12;
			if(selected.equals("Other Photographs"))elev=7;
			if(selected.equals("Front Photo"))elev=23;
		}
		else if(insp.equals("6"))
		{
			//if(selected.equals("Owner signature"))elev=42;
			if(selected.equals("Front Elevation"))elev=17;
			if(selected.equals("Right Elevation"))elev=18;
			if(selected.equals("Left Elevation"))elev=19;
			if(selected.equals("Back Elevation"))elev=20;
			if(selected.equals("Attic Photographs"))elev=6;
			if(selected.equals("Additional Photographs"))elev=12;
			if(selected.equals("Other Photographs"))elev=7;
			if(selected.equals("Front Photo"))elev=23;
		}
		else if(insp.equals("7"))
		{
			if(selected.equals("Front Elevation"))elev=17;
			if(selected.equals("Right Elevation"))elev=18;
			if(selected.equals("Left Elevation"))elev=19;
			if(selected.equals("Back Elevation"))elev=20;
			if(selected.equals("Attic Photographs"))elev=6;
			if(selected.equals("Additional Photographs"))elev=12;
			if(selected.equals("Roof Photographs"))elev=2;
			if(selected.equals("Other Photographs"))elev=7;
			if(selected.equals("Front Photo"))elev=23;
		}
		else if(insp.equals("8"))
		{
			//if(selected.equals("Owner signature"))elev=50;
			if(selected.equals("External Photographs"))elev=8;
			if(selected.equals("Roof and attic Photographs"))elev=9;
			if(selected.equals("Ground and adjoining images"))elev=10;
			if(selected.equals("Internal Photographs"))elev=11;
			if(selected.equals("Additional Photographs"))elev=12;
			if(selected.equals("Other Photographs"))elev=7;
			if(selected.equals("Front Photo"))elev=23;
		}
		else if(insp.equals("9"))
		{
			System.out.println("sdd"+insp+"s;e"+selected);
			/*if(selected.equals("Copper wiring"))elev=57;
			if(selected.equals("Copper wiring at outlets and switches"))elev=58;
			if(selected.equals("Metal Fixtures"))elev=59;
			if(selected.equals("Ceiling board stamps"))elev=60;*/
			if(selected.equals("Other Photographs"))elev=7;
			if(selected.equals("Front Photo"))elev=23;
			if(selected.equals("Interior Photographs"))elev=25;
			if(selected.equals("HVAC Photographs"))elev=5;
			if(selected.equals("Electrical Photographs"))elev=4;
			if(selected.equals("Plumbing Photographs"))elev=3;
			if(selected.equals("Exterior Photographs"))elev=1;
			if(selected.equals("Interior Fixture Photographs"))elev=26;
			if(selected.equals("Drywall Photographs"))elev=27;
			
		System.out.println("whats is ="+elev);
		}
		System.out.println("correct"+selected+" insp"+insp);
		return elev;
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
 		// replaces the default 'Back' button action
 		if (keyCode == KeyEvent.KEYCODE_BACK) {
 			cf.goclass(6);
 			return true;
 		}
 		
 		return super.onKeyDown(keyCode, event);
 	}
	
}