/*
   ************************************************************
   * Project: ALL RISK REPORT
   * Module : GCHHVAC.java
   * Creation History:
      Created By : Gowri on 
   * Modification History:
      Last Modified By : Gowri on 04/04/2013
   ************************************************************  
*/
package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.FireProtection.checklistenetr;
import idsoft.inspectiondepot.ARR.HVAC.textwatcher;
import idsoft.inspectiondepot.ARR.Plumbing.Spin_Selectedlistener;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class GCHHVAC extends Activity{
	CommonFunctions cf;
	int hvacchkval=0;
	public CheckBox[] typechk = new CheckBox[7];
	public CheckBox[] fuelchk = new CheckBox[5];
	public CheckBox[] airchk = new CheckBox[3];
	String typechk_val="",sdlupdated,fuelchk_val="",agestr="",spcrdtxt="",hvacstr="--Select--",airchk_val="",csrd_val="No",updrd_val="",aord_val="",rethvcrdtxt="";
	RadioGroup csrdgval,updrdgval,aordgval,spcrdgval;
	Spinner agespin,hvacspin;
	ArrayAdapter ageadap,hvacadapter;
	String hvacsys[] = {"--Select--","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20"};
	public boolean load_comment=true,spcck=false,suchk=false,saochk=false;
	int sc=0;
	private static final int DATE_DIALOG_ID = 0;
	
	@Override
	   public void onCreate(Bundle savedInstanceState) {

	        super.onCreate(savedInstanceState);
	        cf=new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);
				
		 	}
	        setContentView(R.layout.gchhvac);
	        cf.getDeviceDimensions();
	        
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
			hdr_layout.addView(new HdrOnclickListener(this,1,"General Conditions & Hazards","HVAC System",4,0,cf));
	        LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
	        mainmenu_layout.setMinimumWidth(cf.wd);
	        mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 4, 0,0,cf));
	        LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 47, 1,0,cf));
			TableRow tblrw = (TableRow)findViewById(R.id.row2);
			tblrw.setMinimumHeight(cf.ht);
			cf.CreateARRTable(47);
			cf.CreateARRTable(3);
			cf.getPolicyholderInformation(cf.selectedhomeid);
			
			declaration();
			HVAC_SetValue();
	}
	private void HVAC_SetValue() {
		// TODO Auto-generated method stub
		try
		{
			chk_values();
		   Cursor HVAC_retrive=cf.SelectTablefunction(cf.GCH_Hvactbl, " where fld_srid='"+cf.selectedhomeid+"' AND (fld_hvchk='1' OR (fld_hvchk='0' AND fld_type<>''))");
		   if(HVAC_retrive.getCount()>0)
		   {  
			   HVAC_retrive.moveToFirst();
			   ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
			   if(HVAC_retrive.getInt(HVAC_retrive.getColumnIndex("fld_hvchk"))==1)
			   {
				   ((CheckBox)findViewById(R.id.hc_na)).setChecked(true);
				   ((LinearLayout)findViewById(R.id.hvac_table)).setVisibility(cf.v1.GONE);
			   }
			   else
			   {
				   ((CheckBox)findViewById(R.id.hc_na)).setChecked(false);
				   ((LinearLayout)findViewById(R.id.hvac_table)).setVisibility(cf.v1.VISIBLE);
				   				   
				   String hvstr = HVAC_retrive.getString(HVAC_retrive.getColumnIndex("fld_hvacsystem"));
				   if(hvstr.equals("Not Determined"))
				   {
					  ((CheckBox)findViewById(R.id.nd_chk)).setChecked(true);
					   hvacspin.setEnabled(false);
				   }
				   else
				   {
					   hvacspin.setSelection(hvacadapter.getPosition(hvstr));
				   }
				   
				   typechk_val=cf.decode(HVAC_retrive.getString(HVAC_retrive.getColumnIndex("fld_type")));
				   cf.setvaluechk1(typechk_val,typechk,((EditText)findViewById(R.id.typ_otr)));
				   
				   fuelchk_val=cf.decode(HVAC_retrive.getString(HVAC_retrive.getColumnIndex("fld_fuel")));
				   cf.setvaluechk1(fuelchk_val,fuelchk,((EditText)findViewById(R.id.fuel_otr)));
				   
				   airchk_val=cf.decode(HVAC_retrive.getString(HVAC_retrive.getColumnIndex("fld_air")));
				   cf.setvaluechk1(airchk_val,airchk,((EditText)findViewById(R.id.air_otr)));
				   
				   String cond =cf.decode(HVAC_retrive.getString(HVAC_retrive.getColumnIndex("fld_condsatis")));
				   System.out.println("cond="+cond);
				   String[] ctemp = null;
				   ctemp = cond.split("&#94;");
				   if(cond.contains("No")){ 
					    ((LinearLayout)findViewById(R.id.slin)).setVisibility(cf.v1.VISIBLE);
					    ((EditText)findViewById(R.id.scomment)).setText(ctemp[1]);
				    }else{
				    	((LinearLayout)findViewById(R.id.slin)).setVisibility(cf.v1.GONE);
				    }
				   ((RadioButton) csrdgval.findViewWithTag(ctemp[0])).setChecked(true);
				   
				   String srupd=HVAC_retrive.getString(HVAC_retrive.getColumnIndex("fld_updated"));
				   if(srupd.contains("Yes") || srupd.contains("Yes~")){
					    String[] temp1  = srupd.split("~");
						 updrd_val=temp1[0];
						((EditText)findViewById(R.id.sruotr)).setText(temp1[1]);
						((LinearLayout)findViewById(R.id.srushow)).setVisibility(cf.v1.VISIBLE);
						 String wretdl = HVAC_retrive.getString(HVAC_retrive.getColumnIndex("fld_sdlupd"));
						 if(wretdl.equals("Not Determined")){
							  ((CheckBox)findViewById(R.id.sndchk)).setChecked(true); sc=1;
						 }else{
							 ((CheckBox)findViewById(R.id.sndchk)).setChecked(false);sc=0; 
							 ((EditText)findViewById(R.id.snd_otr)).setVisibility(cf.v1.GONE);
							
						 }
						 ((EditText)findViewById(R.id.sdlutxt)).setText(wretdl);
						 spcrdtxt=HVAC_retrive.getString(HVAC_retrive.getColumnIndex("fld_sconf"));
						((RadioButton) spcrdgval.findViewWithTag(spcrdtxt)).setChecked(true);
					}else{
						updrd_val=srupd;
						((LinearLayout)findViewById(R.id.srushow)).setVisibility(cf.v1.GONE);
					}
					((RadioButton) updrdgval.findViewWithTag(updrd_val)).setChecked(true);
				    
				   String age = HVAC_retrive.getString(HVAC_retrive.getColumnIndex("fld_age"));
				   if(age.contains("Other")|| age.contains("Other&#94;"))
				   { 
				      String[] temp = age.split("&#94;");
				      agestr = temp[0];
				      ((EditText)findViewById(R.id.age_otr)).setText(temp[1]);
				      ((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.VISIBLE);
			       }
				   else
				   {
					  agestr = age;((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.GONE);
				   }
			       agespin.setSelection(ageadap.getPosition(agestr)); 
			       
				   aord_val=HVAC_retrive.getString(HVAC_retrive.getColumnIndex("fld_appearsop"));
				   ((RadioButton) aordgval.findViewWithTag(aord_val)).setChecked(true);
				   
				   ((EditText)findViewById(R.id.otr)).setText(cf.decode(HVAC_retrive.getString(HVAC_retrive.getColumnIndex("fld_other"))));
				   ((EditText)findViewById(R.id.hvaccomment)).setText(cf.decode(HVAC_retrive.getString(HVAC_retrive.getColumnIndex("fld_comments"))));
			  
		     }
		   }
		   else
		   {
			    cf.getinspectioname(cf.selectedhomeid);
			    if(cf.selinspname.equals("4"))
				{
					((CheckBox)findViewById(R.id.hc_na)).setChecked(false);
					((LinearLayout)findViewById(R.id.hvac_table)).setVisibility(cf.v1.VISIBLE);
				}
				else
				{
					((CheckBox)findViewById(R.id.hc_na)).setChecked(true);
					((LinearLayout)findViewById(R.id.hvac_table)).setVisibility(cf.v1.GONE);
				}
			   
		   }
		  
		}
	    catch (Exception E)
		{
			String strerrorlog="Retrieving HVAC - GCH";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
	private void declaration() {

		// TODO Auto-generated method stub
		cf.setupUI((ScrollView)findViewById(R.id.scr));
		((TextView)findViewById(R.id.rowtxt1)).setText(Html.fromHtml(cf.redcolor+" Heating Type"));
		((TextView)findViewById(R.id.rowtxt2)).setText(Html.fromHtml(cf.redcolor+" Fuel"));
		//((TextView)findViewById(R.id.rowtxt3)).setText(Html.fromHtml(cf.redcolor+" Air Conditioning Systems"));
		//((TextView)findViewById(R.id.rowtxt4)).setText(Html.fromHtml(cf.redcolor+" System appears to be in Satisfactory Condition"));
		
		typechk[0] = ((CheckBox)findViewById(R.id.typ_chk1));
		typechk[1] = ((CheckBox)findViewById(R.id.typ_chk2));
		typechk[2] = ((CheckBox)findViewById(R.id.typ_chk3));
		typechk[3] = ((CheckBox)findViewById(R.id.typ_chk4));
		typechk[4] = ((CheckBox)findViewById(R.id.typ_chk5));
		typechk[5] = ((CheckBox)findViewById(R.id.typ_chk6));
		typechk[6] = ((CheckBox)findViewById(R.id.typ_chk7));
		
		fuelchk[0] = ((CheckBox)findViewById(R.id.fuel_chk1));
		fuelchk[1] = ((CheckBox)findViewById(R.id.fuel_chk2));
		fuelchk[2] = ((CheckBox)findViewById(R.id.fuel_chk3));
		fuelchk[3] = ((CheckBox)findViewById(R.id.fuel_chk4));
		fuelchk[4] = ((CheckBox)findViewById(R.id.fuel_chk5));
		
		airchk[0] = ((CheckBox)findViewById(R.id.air_chk1));
		airchk[1] = ((CheckBox)findViewById(R.id.air_chk2));
		airchk[2] = ((CheckBox)findViewById(R.id.air_chk3));
		
		csrdgval = (RadioGroup)findViewById(R.id.cs_rdg);
		csrdgval.setOnCheckedChangeListener(new checklistenetr(1));				
		updrdgval = (RadioGroup)findViewById(R.id.upd_rdg);
		updrdgval.setOnCheckedChangeListener(new checklistenetr(2));	
		aordgval = (RadioGroup)findViewById(R.id.ao_rdg);
		aordgval.setOnCheckedChangeListener(new checklistenetr(3));	
		spcrdgval = (RadioGroup)findViewById(R.id.spc_rdg);
		spcrdgval.setOnCheckedChangeListener(new checklistenetr(4));	
		agespin=(Spinner) findViewById(R.id.age_spin);
		ageadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, cf.yearbuilt);
		ageadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		agespin.setAdapter(ageadap);
		 cf.getPolicyholderInformation(cf.selectedhomeid);
		   int spinnerPosition = ageadap.getPosition(cf.YearPH);
		   agespin.setSelection(spinnerPosition);
		agespin.setOnItemSelectedListener(new  Spin_Selectedlistener(1));
		
		 hvacspin=(Spinner) findViewById(R.id.noofhvac_spin);
		 hvacadapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, hvacsys);
		 hvacadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 hvacspin.setAdapter(hvacadapter);
		 hvacspin.setOnItemSelectedListener(new  Spin_Selectedlistener(2));
		 
		((EditText)findViewById(R.id.age_otr)).addTextChangedListener(new textwatcher(1));
		((EditText)findViewById(R.id.scomment)).addTextChangedListener(new textwatcher(2));
		((EditText)findViewById(R.id.otr)).addTextChangedListener(new textwatcher(3));
		((EditText)findViewById(R.id.hvaccomment)).addTextChangedListener(new textwatcher(4));
		((EditText)findViewById(R.id.sruotr)).addTextChangedListener(new textwatcher(5));
		set_defaultchk();
	}
	class Spin_Selectedlistener implements OnItemSelectedListener
	{
		int i;
		public Spin_Selectedlistener(int i) {
			// TODO Auto-generated constructor stub
			this.i=i;
		}

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			if(i==1)
			{
			    agestr = agespin.getSelectedItem().toString();
				if(agestr.equals("Other")){
					((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.VISIBLE);
					
                }
				else 
				{
					((EditText)findViewById(R.id.age_otr)).setText("");
					((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.GONE);
				}
				
			}
			else if(i==2){
				hvacstr = hvacspin.getSelectedItem().toString();
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	class textwatcher implements TextWatcher
    {
	         public int type;
	         textwatcher(int type)
	    	{
	    		this.type=type;
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {


	    		// TODO Auto-generated method stub
	    		if(this.type==1)
	    		{
	    			 if(!s.toString().equals("")){
	    			     if(Character.toString(s.toString().charAt(0)).equals("0"))
		    			 {
		    				 cf.ShowToast("Invalid Entry!! Please enter valid Year of Age.", 0);
		    				 ((EditText)findViewById(R.id.age_otr)).setText("");
		    				 cf.hidekeyboard();
		    			 }
	    			 }
	    		}
	    		else if(this.type==2)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.s_tv_type1),250); 
	    		}
	    		else if(this.type==3)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.o_tv_type1),250); 
	    		}
	    		else if(this.type==4)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.hvac_tv_type1),500); 
	    		}
	    		else if(this.type==5)
	    		{
	    			 if(!s.toString().equals("")){
	    			     if(Character.toString(s.toString().charAt(0)).equals("0"))
		    			 {
		    				 cf.ShowToast("Approximate Percentage Update should be Greater than 0 and Less than or Equal to 100.", 0);
		    				 ((EditText)findViewById(R.id.sruotr)).setText("");cf.hidekeyboard();
		    			 }
	    			 }
	    		}
	}

	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,

	    			int after) {
	    		// TODO Auto-generated method stub
	     	}

	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,

	    			int count) {
	    		// TODO Auto-generated method stub
	    	}
	   }	
	private void set_defaultchk() {
		// TODO Auto-generated method stub
		((RadioButton) csrdgval.findViewWithTag("No")).setChecked(true);
		((EditText)findViewById(R.id.scomment)).setVisibility(cf.v1.VISIBLE);
		typechk_val="";fuelchk_val="";spcrdtxt="";hvacstr="--Select--";airchk_val="";csrd_val="No";updrd_val="";aord_val="";
		cf.getPolicyholderInformation(cf.selectedhomeid);
		  agestr=cf.YearPH;
		
	}
	class checklistenetr implements OnCheckedChangeListener
	{
    	int i;
		public checklistenetr(int i) {
			// TODO Auto-generated constructor stub
		   this.i=i;
		}

		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			   RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
		        boolean isChecked = checkedRadioButton.isChecked();
		        if (isChecked)
		        {
		          switch (i) {
					case 1:
						csrd_val= checkedRadioButton.getText().toString().trim();
						if(csrd_val.equals("No"))
						{
							((LinearLayout)findViewById(R.id.slin)).setVisibility(cf.v1.VISIBLE);
						}
						else
						{
							((EditText)findViewById(R.id.scomment)).setText("");
							((LinearLayout)findViewById(R.id.slin)).setVisibility(cf.v1.GONE);
						}
						break;
					case 2:
						updrd_val= checkedRadioButton.getText().toString().trim();suchk=false;
						if(updrd_val.equals("Yes"))
						{
							((LinearLayout)findViewById(R.id.srushow)).setVisibility(cf.v1.VISIBLE);
						}
						else
						{
							spcck=true;
							((LinearLayout)findViewById(R.id.srushow)).setVisibility(cf.v1.GONE);
							((EditText)findViewById(R.id.sdlutxt)).setText("");sc=0;
							((EditText)findViewById(R.id.sruotr)).setText("");
							((CheckBox)findViewById(R.id.sndchk)).setChecked(false);
							((Button)findViewById(R.id.sgetdlup)).setEnabled(true);
							((TextView)findViewById(R.id.datid)).setVisibility(cf.v1.INVISIBLE);
							 try{if(spcck){spcrdgval.clearCheck();}}catch(Exception e){}
						}
						break;
					case 3:
						aord_val= checkedRadioButton.getText().toString().trim();saochk=false;
						break;
					case 4:
						spcrdtxt = checkedRadioButton.getText().toString().trim();spcck=false;
						if(spcrdtxt.equals("Yes"))
						{
							((TextView)findViewById(R.id.datid)).setVisibility(cf.v1.VISIBLE);
						}
						else
						{
							((TextView)findViewById(R.id.datid)).setVisibility(cf.v1.INVISIBLE);
						}
						break;
		          }
		       }
		}
	}
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.loadcomments:
			/***Call for the comments***/
			 int len=((EditText)findViewById(R.id.hvaccomment)).getText().toString().length();
			 if(load_comment )
			 {
					load_comment=false;
					int loc[] = new int[2];
					v.getLocationOnScreen(loc);
					Intent in1 = cf.loadComments("HVAC Comments",loc);
					in1.putExtra("length", len);
					in1.putExtra("max_length", 500);
					startActivityForResult(in1, cf.loadcomment_code);
			}
		   break;
		case R.id.helpid:
			cf.ShowToast("Use for Commercial GCH report only.",0);
			break;
		case R.id.hc_na:/** HVAC NOT APPLICABLE CHECKBOX **/
			if(((CheckBox)findViewById(R.id.hc_na)).isChecked())
			{
				
    			if(!rethvcrdtxt.equals("")){
 				    showunchkalert(1);
 			    }
    			else
    			{
    				clearhvac();
    				set_defaultchk();
    				((LinearLayout)findViewById(R.id.hvac_table)).setVisibility(v.GONE);
 			    	((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.INVISIBLE);
 			    }
				
			}
			else
			{
				((LinearLayout)findViewById(R.id.hvac_table)).setVisibility(v.VISIBLE);
				((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.INVISIBLE);
				((CheckBox)findViewById(R.id.hc_na)).setChecked(false);
			}
			
			break;
		case R.id.sndchk:
			if(((CheckBox)findViewById(R.id.sndchk)).isChecked())
			{
				sc=1;
				//((Button)findViewById(R.id.sgetdlup)).setEnabled(false);
				((EditText)findViewById(R.id.snd_otr)).setVisibility(v.INVISIBLE);
				((EditText)findViewById(R.id.sdlutxt)).setText("Not Determined");
				((EditText)findViewById(R.id.sdlutxt)).setFocusable(false);
				((EditText)findViewById(R.id.sruotr)).setFocusable(false);
			}else{
				sc=0;
				((Button)findViewById(R.id.sgetdlup)).setEnabled(true);
				((EditText)findViewById(R.id.snd_otr)).setVisibility(v.GONE);
				((EditText)findViewById(R.id.sdlutxt)).setFocusable(true);
				((EditText)findViewById(R.id.sdlutxt)).setText("");
			}
			break;
		case R.id.sgetdlup:
			//if(sc==0)
			//{  
			((CheckBox)findViewById(R.id.sndchk)).setChecked(false);
				cf.showDialogDate(((EditText)findViewById(R.id.sdlutxt)));
			//}
			//else
			//{
				
			//}
			
			break;
	
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.nd_chk: /** NOT BETERMINED**/
			if(((CheckBox)findViewById(R.id.nd_chk)).isChecked())
			{
				hvacspin.setEnabled(false);
				hvacspin.setSelection(0);
				hvacstr="Not Determined";
			}
			else
			{
				hvacstr="--Select--";
				hvacspin.setEnabled(true);
			}
			break;
		case R.id.typ_chk7:/** TYPE CHECKBOX OTHER **/
			  if(typechk[typechk.length-1].isChecked())
			  { 
				  ((EditText)findViewById(R.id.typ_otr)).setVisibility(v.VISIBLE);
			  }
			  else
			  {
				  ((EditText)findViewById(R.id.typ_otr)).setText("");
				  ((EditText)findViewById(R.id.typ_otr)).setVisibility(v.GONE);
			  }
			  break;
		 case R.id.fuel_chk5:/** FUEL CHECKBOX OTHER **/
			  if(fuelchk[fuelchk.length-1].isChecked())
			  { 
				  ((EditText)findViewById(R.id.fuel_otr)).setVisibility(v.VISIBLE);
			  }
			  else
			  {
				  ((EditText)findViewById(R.id.fuel_otr)).setVisibility(v.GONE);
				  ((EditText)findViewById(R.id.fuel_otr)).setText("");
			  }
			  break;
		 case R.id.air_chk3:/** AIR CHECKBOX OTHER **/
			  if(airchk[airchk.length-1].isChecked())
			  { 
				  ((EditText)findViewById(R.id.air_otr)).setVisibility(v.VISIBLE);
			  }
			  else
			  {
				  ((EditText)findViewById(R.id.air_otr)).setVisibility(v.GONE);
				  ((EditText)findViewById(R.id.air_otr)).setText("");
			  }
			  break;
		
		 case R.id.save:
				if(((CheckBox)findViewById(R.id.hc_na)).isChecked())
				{
					hvacchkval=1;HVACInsert();
					((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
					cf.ShowToast("HVAC System updated successfully.", 0);
					cf.goclass(48);
				}
				else
				{
					hvacchkval=0;
					if(hvacstr.equals("Not Determined"))
					{
						type();
					}
					else
					{
						if(hvacstr.equals("--Select--") && !((CheckBox)findViewById(R.id.nd_chk)).isChecked())
						{
							cf.ShowToast("Please select No. of HVAC Systems.", 0);
						}
						else
						{
							type();
						}
					}
				}
				
				
				break;
		}
	}
	

	private void type() {
		// TODO Auto-generated method stub
		typechk_val= cf.getselected_chk(typechk);
		String typotrval =(typechk[typechk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.typ_otr)).getText().toString():""; 
		typechk_val+=typotrval;
		 if(typechk_val.equals(""))
		 {
			 cf.ShowToast("Please select the option for Heating Type." , 0);
		 }
		 else
		 {
			 if(typechk[typechk.length-1].isChecked())
			 {	 
				 if(typotrval.trim().equals("&#40;"))
				 {
					 cf.ShowToast("Please enter the other text for Heating Type.", 0);
					 cf.setFocus(((EditText)findViewById(R.id.typ_otr)));
				 }
				 else
				 {
					fuelvalidation();	
				 }
			 
			 }
			 else
			 {
				 fuelvalidation();
			 }
		 }
	}
	private void chk_values() {
		// TODO Auto-generated method stub
		try
		{
		   Cursor hvc_retrive=cf.SelectTablefunction(cf.GCH_Hvactbl, " where fld_srid='"+cf.selectedhomeid+"'");
		   int rws = hvc_retrive.getCount();
			if(rws>0){
				hvc_retrive.moveToFirst();
				rethvcrdtxt = hvc_retrive.getString(hvc_retrive.getColumnIndex("fld_type"));
			}
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}
	public void showunchkalert(final int i) {
		// TODO Auto-generated method stub
		AlertDialog.Builder bl = new Builder(GCHHVAC.this);
		bl.setTitle("Confirmation");
		bl.setMessage(Html.fromHtml("Do you want to clear the HVAC System data?"));
		bl.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog,
											int id) {
										 switch(i){
										 case 1:
											 ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.GONE);
							    			  clearhvac();//HVACInsert();
							    			 // cf.arr_db.execSQL("Delete from "+cf.GCH_Hvactbl+" Where fld_srid='"+cf.selectedhomeid+"'");
												 
							    			  set_defaultchk();
							    			 ((LinearLayout)findViewById(R.id.hvac_table)).setVisibility(cf.v1.GONE);
							    			 break;
		                                 
										 }
										 	 
										 
									}
		});
		bl.setNegativeButton("No",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog,
							int id) {
						switch(i){
						 case 1:
							 ((CheckBox)findViewById(R.id.hc_na)).setChecked(false);
							 break;
					
						 }
					}
        });
		AlertDialog al=bl.create();
		al.setIcon(R.drawable.alertmsg);
		al.setCancelable(false);
		al.show();
		
		
	}
	protected void clearhvac() {
		// TODO Auto-generated method stub
		  hvacchkval=0;typechk_val="";fuelchk_val="";airchk_val="";csrd_val="";
		  updrd_val="";aord_val="";rethvcrdtxt="";
		  ((CheckBox)findViewById(R.id.nd_chk)).setChecked(false);
		  hvacspin.setSelection(0);
		   hvacspin.setEnabled(true);suchk=true;spcck=true;saochk=true;
		   try{if(suchk){updrdgval.clearCheck();}}catch(Exception e){}
		  cf.Set_UncheckBox(typechk, ((EditText)findViewById(R.id.typ_otr)));
		  cf.Set_UncheckBox(fuelchk, ((EditText)findViewById(R.id.fuel_otr)));
		  cf.Set_UncheckBox(airchk, ((EditText)findViewById(R.id.air_otr)));
		  ((EditText)findViewById(R.id.age_otr)).setText("");
		  ((EditText)findViewById(R.id.otr)).setText("");
		  ((EditText)findViewById(R.id.sruotr)).setText("");
		  ((CheckBox)findViewById(R.id.sndchk)).setChecked(false);
		  ((Button)findViewById(R.id.sgetdlup)).setEnabled(true);
		  ((EditText)findViewById(R.id.sdlutxt)).setText("");
		  ((EditText)findViewById(R.id.scomment)).setText("");
		  cf.getPolicyholderInformation(cf.selectedhomeid);
		   int spinnerPosition = ageadap.getPosition(cf.YearPH);
		   agespin.setSelection(spinnerPosition);
		  try{if(spcck){spcrdgval.clearCheck();}}catch(Exception e){}
		  try{if(saochk){aordgval.clearCheck();}}catch(Exception e){}
		  ((LinearLayout)findViewById(R.id.srushow)).setVisibility(cf.v1.GONE);
		  ((EditText)findViewById(R.id.hvaccomment)).setText("");
	}
	private void fuelvalidation() {
		// TODO Auto-generated method stub
		fuelchk_val= cf.getselected_chk(fuelchk);
		String fuelotrval =(fuelchk[fuelchk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.fuel_otr)).getText().toString():""; 
		fuelchk_val+=fuelotrval;
		 if(fuelchk_val.equals(""))
		 {
			 cf.ShowToast("Please select the option for Fuel." , 0);
		 }
		 else
		 {
			 if(fuelchk[fuelchk.length-1].isChecked())
			 {	 
				 if(fuelotrval.trim().equals("&#40;"))
				 {
					 cf.ShowToast("Please enter the Other text for Fuel.", 0);
					 cf.setFocus(((EditText)findViewById(R.id.fuel_otr)));
				 }
				 else
				 {
					 airvalidation();	
				 }
			 
			 }
			 else
			 {
				 airvalidation();
			 }
		 }
	}
	private void airvalidation() {
		// TODO Auto-generated method stub
		airchk_val= cf.getselected_chk(airchk);
		String airotrval =(airchk[airchk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.air_otr)).getText().toString():""; 
		airchk_val+=airotrval;
		 if(airchk_val.equals(""))
		 {
			 cf.ShowToast("Please select the option for Air Conditioning Systems." , 0);
		 }
		 else
		 {
			 if(airchk[airchk.length-1].isChecked())
			 {	 
				 if(airotrval.trim().equals("&#40;"))
				 {
					 cf.ShowToast("Please enter the Other text for Air Conditioning Systems.", 0);
					 cf.setFocus(((EditText)findViewById(R.id.air_otr)));
				 }
				 else
				 {
					conditionvalidation();	
				 }
			 
			 }
			 else
			 {
				 conditionvalidation();
			 }
		 }
	}
	private void conditionvalidation() {

		// TODO Auto-generated method stub
		if(csrd_val.equals("No"))
		{
			if(((EditText)findViewById(R.id.scomment)).getText().toString().trim().equals(""))
			{
				cf.ShowToast("Please enter the comments.", 0);
				cf.setFocus(((EditText)findViewById(R.id.scomment)));
			}
			else
			{
				csrd_val += "&#94;"+((EditText)findViewById(R.id.scomment)).getText().toString();
				updated();
			}
		}
		else
		{
			csrd_val=csrd_val;
			updated();
		}
		
	}
	private void updated() {
		// TODO Auto-generated method stub
		if(updrd_val.equals(""))
		{
			cf.ShowToast("Please select the option for System Recently Updated.", 0);
		}
		else
		{
			  if(updrd_val.contains("Yes")|| updrd_val.contains("Yes~"))
			  {
				  String setrup = ((EditText)findViewById(R.id.sruotr)).getText().toString().trim();
				  if(setrup.equals(""))
				  {
						cf.ShowToast("Please enter the Approximate Percentage Update.",0);
						cf.setFocus(((EditText)findViewById(R.id.sruotr)));
				  }
				  else
				  {
					  if(Integer.parseInt(setrup)<=0 || Integer.parseInt(setrup)>100)
					  {
							cf.ShowToast("Approximate Percentage Update should be Greater than 0 and Less than or Equal to 100 .",0);
							cf.setFocus(((EditText)findViewById(R.id.sruotr)));
							((EditText)findViewById(R.id.sruotr)).setText("");
					  }
					  else
					  {
						  updrd_val += "~"+setrup;
						  sconfirmed();
					  }
				  }
			  }
			  else
			  {
				  updrd_val=updrd_val;
				 age();
			  }
			
	  }
		
	}
	private void sconfirmed() {
		// TODO Auto-generated method stub
		 if(spcrdtxt.equals(""))
		 {
			 cf.ShowToast("Please select the option for Permit Confirmed.",0);
	     }
		 else
		 {
			 if(spcrdtxt.equals("Yes"))
			 {
				 if(((CheckBox)findViewById(R.id.sndchk)).isChecked())
     			 {
     				sdlupdated=((EditText)findViewById(R.id.sdlutxt)).getText().toString();
     				age();
     			 }
				 else
				 {
					 if(((EditText)findViewById(R.id.sdlutxt)).getText().toString().trim().equals(""))
    				 {
    					 cf.ShowToast("Please select Date Last Updated.",0);
		 		         cf.setFocus(((EditText)findViewById(R.id.sdlutxt)));
		 		     }
    				 else
    				 {
    					 /*if(cf.checkfortodaysdate(((EditText)findViewById(R.id.sdlutxt)).getText().toString())=="false")
	 		        	 {
	 		        		 cf.ShowToast("Date Last Updated should not exceed Current Year/Month/Date.",0);
	 		        		 cf.setFocus(((EditText)findViewById(R.id.sdlutxt)));
	 		        	 }
	 		        	 else
	 		        	 {*/
    					 System.out.println("seel"+cf.selectedhomeid);
    					 cf.getPolicyholderInformation(cf.selectedhomeid);
    					 System.out.println("cfff="+cf.Insp_date);
	 		        		boolean chkdate = checkforinspectiondate(cf.Insp_date,((EditText)findViewById(R.id.sdlutxt)).getText().toString());
	 		        		System.out.println("chkdat="+chkdate);
	 		        		if(chkdate)
	 		        		{
	 		        			sdlupdated = ((EditText)findViewById(R.id.sdlutxt)).getText().toString();
		 		        		preage();
	 		        		}
	 		        		else
	 		        		{
	 		        			cf.ShowToast("Date Last Updated should not exceed Inspection Date.",0);
		 		        		cf.setFocus(((EditText)findViewById(R.id.sdlutxt)));
	 		        		}
	 		        		
	 		        	 //}
    				 }
				 }
			 }
			 else
			 {
				 preage();
			 }
		 }
	}
	private boolean checkforinspectiondate(String insp_date, String string) {
		// TODO Auto-generated method stub
		System.out.println("insp_date="+insp_date+" string"+string);
		int i1 = string.indexOf("/");
		String result = string.substring(0, i1);
		int i2 = string.lastIndexOf("/");
		String result1 = string.substring(i1 + 1, i2);
		String result2 = string.substring(i2 + 1);
		result2 = result2.trim();
		int smonth= Integer.parseInt(result);
		int sdate = Integer.parseInt(result1);
		int syear = Integer.parseInt(result2);
		
		int ins1 = insp_date.indexOf("/");
		String inspres = insp_date.substring(0, ins1);
		int ins2 = insp_date.lastIndexOf("/");
		String inspres1 = insp_date.substring(ins1 + 1, ins2);
		String inspres2 = insp_date.substring(ins2 + 1);
		inspres2 = inspres2.trim();
		int imonth= Integer.parseInt(inspres);
		int idate = Integer.parseInt(inspres1);
		int iyear = Integer.parseInt(inspres2);
		//mMonth = mMonth + 1;
		
		System.out.println("syear "+syear +"" +iyear);
		System.out.println("smonth "+smonth +"" +imonth);
		System.out.println("sdate "+sdate +"" +idate);
	    if ( syear  < iyear || (smonth < imonth && syear  <= iyear) || (smonth <= imonth && syear <=iyear && sdate <= idate )) {
	    	return true;
		} else {
			return false;
		}
	    
		
	}
	private void preage() {
		// TODO Auto-generated method stub
		if(!((EditText)findViewById(R.id.sdlutxt)).getText().toString().trim().equals("")&&!((EditText)findViewById(R.id.sdlutxt)).getText().toString().equals("Not Determined"))
		{
			/* if(cf.checkfortodaysdate(((EditText)findViewById(R.id.sdlutxt)).getText().toString())=="false")
	         {
	        	 cf.ShowToast("Date Last Updated should not exceed Current Year/Month/Date.",0);
	        	 cf.setFocus(((EditText)findViewById(R.id.sdlutxt)));
	       	 }
	       	 else
	       	 {*/
			
			System.out.println("ssd"+cf.selectedhomeid);
			cf.getPolicyholderInformation(cf.selectedhomeid);
			System.out.println("INSPEC"+cf.Insp_date);
	       		boolean chkdate = checkforinspectiondate(cf.Insp_date,((EditText)findViewById(R.id.sdlutxt)).getText().toString());
	        	if(chkdate)
	        	{
	              sdlupdated = ((EditText)findViewById(R.id.sdlutxt)).getText().toString();
	              age();
	        	}
	        	else
	        	{
	        		cf.ShowToast("Date Last Updated should not exceed Inspection Date.",0);
		        	cf.setFocus(((EditText)findViewById(R.id.sdlutxt)));
	        	}
	       	// }
		}
		else
		{
			sdlupdated = ((EditText)findViewById(R.id.sdlutxt)).getText().toString();
			age();
		}
	}
	private void age() {
		// TODO Auto-generated method stub
		if(agestr.equals("--Select--"))
		{
			cf.ShowToast("Please select Year of Age.", 0);
		}
		else
		{
			if(agestr.contains("Other") || agestr.contains("Other&#94;"))
			{
				if(((EditText)findViewById(R.id.age_otr)).getText().toString().trim().equals(""))
				{
					cf.ShowToast("Please enter the Other text for Year of Age.",0);
					cf.setFocus(((EditText)findViewById(R.id.age_otr)));
				}
				else
				{
					if(((EditText)findViewById(R.id.age_otr)).getText().toString().length()<4){
						cf.ShowToast("Invalid Entry!! Please enter valid Year of Age.", 0);
						cf.setFocus(((EditText)findViewById(R.id.age_otr))); 
					}
					else
					{
						if(cf.yearValidation(((EditText)findViewById(R.id.age_otr)).getText().toString()).equals("true"))
						{
							cf.ShowToast("Year should not exceed the current year.", 0);
							cf.setFocus(((EditText)findViewById(R.id.age_otr)));
						}
						else
						{
							agestr += "&#94;"+((EditText)findViewById(R.id.age_otr)).getText().toString();
							appearscond();
						}
					}
				}
			}
			else
			{
				agestr=agestr;
				appearscond();
			}
		} 
	}
	private void appearscond() {
		// TODO Auto-generated method stub
		if(aord_val.equals(""))
		{
			cf.ShowToast("Please select the option for Systems Appear Operational.", 0);
		}
		else
		{
			 /* if(((EditText)findViewById(R.id.otr)).getText().toString().trim().equals(""))
			  {
				  cf.setFocus(((EditText)findViewById(R.id.otr)));
				  cf.ShowToast("Please enter the Other Information.", 0);
			  }
			  else
			  {*/
				 /* if(((EditText)findViewById(R.id.hvaccomment)).getText().toString().trim().equals(""))
				  {
					  cf.setFocus(((EditText)findViewById(R.id.hvaccomment)));
					  cf.ShowToast("Please enter the HVAC Comments.", 0);
				  }
				  else
				  {*/
					  HVACInsert();
					  ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
					  cf.ShowToast("HVAC System updated successfully.", 0);
					  cf.goclass(48);
				  //}
			  //}
		}
	}
	private void HVACInsert() {
		// TODO Auto-generated method stub
		Cursor HV_save=null;
		if(((CheckBox)findViewById(R.id.nd_chk)).isChecked())
		{
			hvacstr="Not Determined";
		}
		System.out.println("updrd_val="+updrd_val);
		
		try
		{
			HV_save=cf.SelectTablefunction(cf.GCH_Hvactbl, " where fld_srid='"+cf.selectedhomeid+"'");
			if(HV_save.getCount()>0)
			{
				try
				{
					cf.arr_db.execSQL("UPDATE "+cf.GCH_Hvactbl+ " set fld_hvchk='"+hvacchkval+"',fld_hvacsystem='"+hvacstr+"',fld_type='"+cf.encode(typechk_val)+"',fld_fuel='"+cf.encode(fuelchk_val)+"',"+
				                               "fld_air='"+cf.encode(airchk_val)+"',fld_condsatis='"+cf.encode(csrd_val)+"',fld_updated='"+updrd_val+"',"+
							                   "fld_sdlupd='"+sdlupdated+"',fld_sconf='"+spcrdtxt+"',"+
							                   "fld_age='"+agestr+"',fld_appearsop='"+aord_val+"',"+
						                       "fld_other='"+cf.encode(((EditText)findViewById(R.id.otr)).getText().toString())+"',"+
							                   "fld_comments='"+cf.encode(((EditText)findViewById(R.id.hvaccomment)).getText().toString())+"' where fld_srid='"+cf.selectedhomeid+"'");
					/*if(hvacchkval==0){ ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.GONE);}
					else{ ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
					cf.ShowToast("HVAC System updated successfully.", 0);}
					cf.goclass(48);*/
				}
				catch (Exception E)
				{
					String strerrorlog="Updating the HVAC - GCH";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
			}
			else
			{
				try
				{
					cf.arr_db.execSQL("INSERT INTO "
						+ cf.GCH_Hvactbl
						+ " (fld_srid,fld_hvchk,fld_hvacsystem,fld_type,fld_fuel,fld_air,fld_condsatis,fld_updated,fld_sdlupd,fld_sconf,fld_age,fld_appearsop,fld_other,fld_comments)"
						+ "VALUES ('"+cf.selectedhomeid+"','"+hvacchkval+"','"+hvacstr+"','"+cf.encode(typechk_val)+"','"+cf.encode(fuelchk_val)+"',"+
				                               "'"+cf.encode(airchk_val)+"','"+cf.encode(csrd_val)+"','"+updrd_val+"','"+sdlupdated+"',"+
							                   "'"+spcrdtxt+"','"+agestr+"','"+aord_val+"',"+
						                       "'"+cf.encode(((EditText)findViewById(R.id.otr)).getText().toString())+"',"+
						                       "'"+cf.encode(((EditText)findViewById(R.id.hvaccomment)).getText().toString())+"')");
					/*if(hvacchkval==0){ ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.GONE);}
					else{ ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
					cf.ShowToast("HVAC System saved successfully.", 0);} 
				 cf.goclass(48);*/
				}
				catch (Exception E)
				{System.out.println("E"+E.getLocalizedMessage());
					String strerrorlog="Inserting the HVAC- GCH";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
			}
		}
		catch (Exception E)
		{
				String strerrorlog="Checking the rows inserted in the GCH HVAC table.";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		} 
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			cf.goclass(43);
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	
		  if(requestCode==cf.loadcomment_code)
		  {
			  load_comment=true;
			if(resultCode==RESULT_OK)
			{
				cf.ShowToast("Comments added successfully.", 0);
				 String hvaccomts = ((EditText)findViewById(R.id.hvaccomment)).getText().toString();
				 ((EditText)findViewById(R.id.hvaccomment)).setText((hvaccomts+" "+data.getExtras().getString("Comments")).trim());
				
			}
			
		}

	}

}
