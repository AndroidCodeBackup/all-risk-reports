package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;

public class DrywallComments extends Activity{
	CommonFunctions cf;
	Cursor Drysum_save;
	boolean load_comment=true;
	
	@Override
	   public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        cf=new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);
				
		 	}
	        setContentView(R.layout.drywalcommentsl);
	        cf.getDeviceDimensions();
	        
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
			hdr_layout.addView(new HdrOnclickListener(this,1,"Chinese Drywall","Overall Chinese Drywall Comments",6,0,cf));
	        
	        LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
	        mainmenu_layout.setMinimumWidth(cf.wd);
	        mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 6, 0,0,cf));
	        LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 66, 1,0,cf));
			TableRow tblrw = (TableRow)findViewById(R.id.row2);
			tblrw.setMinimumHeight(cf.ht);
	        cf.CreateARRTable(7);
			declarations();
			comments_setvalue();
	}
	private void comments_setvalue() {
		// TODO Auto-generated method stub
		try
		{
		   Cursor SCf_retrive=cf.SelectTablefunction(cf.DRY_sumcond, " where fld_srid='"+cf.selectedhomeid+"'");
		   if(SCf_retrive.getCount()>0)
		   {  
			   SCf_retrive.moveToFirst();
			   
			   if(cf.decode(SCf_retrive.getString(SCf_retrive.getColumnIndex("drywallcomments"))).equals(""))
			   {
				   ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.INVISIBLE);
				   ((EditText)findViewById(R.id.drycomment)).setText(cf.decode(SCf_retrive.getString(SCf_retrive.getColumnIndex("drywallcomments"))));
			   }
			   else
			   {
				   ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
				   ((EditText)findViewById(R.id.drycomment)).setText(cf.decode(SCf_retrive.getString(SCf_retrive.getColumnIndex("drywallcomments"))));
			   }
			   
		   }
		   else
		   {
			   ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.INVISIBLE);
			   ((EditText)findViewById(R.id.drycomment)).setText("No additional Drywall Comments.");
			   
		   }
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}
	private void declarations() {
		// TODO Auto-generated method stub
		cf.setupUI(((EditText)findViewById(R.id.drycomment)));
		((EditText)findViewById(R.id.drycomment)).addTextChangedListener(new textwatcher(1));
	}
	class textwatcher implements TextWatcher
    {
	         public int type;
	         textwatcher(int type)
	    	{
	    		this.type=type;
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
	    		if(this.type==1)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.dry_tv_type1),500); 
	    			if(s.toString().startsWith(" "))
	    			{
	    				((EditText)findViewById(R.id.drycomment)).setText("");
	    			}
	    		}
	    	
	         }

	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,int after) {
	    		// TODO Auto-generated method stub
	     	}

	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,int count) {
	    		// TODO Auto-generated method stub
	    	}
	}	
	public void clicker(View v)
	{
		switch (v.getId()) {
			case R.id.hme:
				cf.gohome();
				break;
			case R.id.loadcomments:/***Call for the comments***/
				int len=((EditText)findViewById(R.id.drycomment)).getText().toString().length();
				if(load_comment )
				{
						load_comment=false;
						int loc[] = new int[2];
						v.getLocationOnScreen(loc);
						Intent in1 = cf.loadComments("Overall Chinese Drywall Comments",loc);
						in1.putExtra("length", len);
						in1.putExtra("max_length", 500);
						startActivityForResult(in1, cf.loadcomment_code);
				}
				break;
			case R.id.save:
				if(((EditText)findViewById(R.id.drycomment)).getText().toString().trim().equals(""))
				{
					cf.ShowToast("Please enter the Overall Chinese Drywall Comments.", 0);
					((EditText)findViewById(R.id.drycomment)).setText("");
					cf.setFocus(((EditText)findViewById(R.id.drycomment)));
				}
				else
				{
					chk_values();
					if(Drysum_save.getCount()>0)
					{
						try
						{
							cf.arr_db.execSQL("UPDATE "+cf.DRY_sumcond+ " set drywallcomments='"+cf.encode(((EditText)findViewById(R.id.drycomment)).getText().toString())+"'where fld_srid='"+cf.selectedhomeid+"'");
							cf.ShowToast("Overall Chinese Drywall Comments saved successfully.", 1);
						}
						catch (Exception E)
						{
							String strerrorlog="Updating the Comments - Drywall";
							cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
						}
					}
					else
					{
						try
						{
							cf.arr_db.execSQL("INSERT INTO "
									+ cf.DRY_sumcond
									+ " (fld_srid,presofsulfur,obscopper,presofdry,addcomments,drywallcomments,atticcomments,"+
									   "applianccomments,hvaccomments,assesmentcomments)"
									+ "VALUES('"+cf.selectedhomeid+"','','',"+
									"'','','"+cf.encode(((EditText)findViewById(R.id.drycomment)).getText().toString())+"',"+
									"'','','','')");
							 cf.ShowToast("Overall Chinese Drywall Comments saved successfully.", 1);
						}
						catch (Exception E)
						{
							String strerrorlog="Inserting the Summary Conditions - Drywall";
							cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
						}
					}
					((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
					cf.goclass(7);
				}
				break;
				
	   }
	}
	private void chk_values() {
		// TODO Auto-generated method stub
		try
		{
			 Drysum_save = cf.SelectTablefunction(cf.DRY_sumcond, " where fld_srid='"+cf.selectedhomeid+"'");
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			cf.goclass(65);
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		  if(requestCode==cf.loadcomment_code)
		  {
			load_comment=true;
			if(resultCode==RESULT_OK)
			{
				 cf.ShowToast("Comments added successfully.", 0);
				 String atccomts = ((EditText)findViewById(R.id.drycomment)).getText().toString();
				 ((EditText)findViewById(R.id.drycomment)).setText((atccomts+" "+data.getExtras().getString("Comments")).trim());
				
			}
			
		}
    }
}
