/*
   ************************************************************
   * Project: ALL RISK REPORT
   * Module : GCHComments.java
   * Creation History:
      Created By : Gowri on 
   * Modification History:
      Last Modified By : Gowri on 04/04/2013
   ************************************************************  
*/
package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.Observation4.textwatcher;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class GCHComments extends Activity{
	CommonFunctions cf;
	boolean load_comment=true;
	@Override
	   public void onCreate(Bundle savedInstanceState) {

	        super.onCreate(savedInstanceState);
	        cf=new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);
		 	}
	        setContentView(R.layout.gchcomments);
	        cf.getDeviceDimensions();
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
			hdr_layout.addView(new HdrOnclickListener(this,1,"General Conditions & Hazards","Overall GCH Comments",4,0,cf));
	        LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
	        mainmenu_layout.setMinimumWidth(cf.wd);
	        mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 4, 0,0,cf));
	        LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 49, 1,0,cf));
			TableRow tblrw = (TableRow)findViewById(R.id.row2);
			tblrw.setMinimumHeight(cf.ht);
			((EditText)findViewById(R.id.gchcomment)).addTextChangedListener(new textwatcher(1));
			cf.CreateARRTable(49);
			try
			{
			   Cursor COMM_retrive=cf.SelectTablefunction(cf.GCH_commenttbl, " where fld_srid='"+cf.selectedhomeid+"'");
			   if(COMM_retrive.getCount()>0)
			   {  
				   COMM_retrive.moveToFirst();
				   ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
				   ((EditText)findViewById(R.id.gchcomment)).setText(cf.decode(COMM_retrive.getString(COMM_retrive.getColumnIndex("gchcomment")))); 
			   }
			   else
			   {
				   ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.GONE);
				   ((EditText)findViewById(R.id.gchcomment)).setText("No additional GCH comments.");
				   
			   }
			}
		    catch (Exception E)
			{
				String strerrorlog="Retrieving COMMETS - GCH";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			}
	}
	 class textwatcher implements TextWatcher


	    {
	         public int type;
	         textwatcher(int type)
	    	{
	    		this.type=type;
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {


	    		// TODO Auto-generated method stub
	    		if(this.type==1)
	    		{
	    			
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.gchcom_tv_type1),500);
	    			if(s.toString().startsWith(" "))
	    			{
	    				((EditText)findViewById(R.id.gchcomment)).setText("");
	    			}
	    		}
	    	
	    		
	    	}

	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,

	    			int after) {
	    		// TODO Auto-generated method stub
	     	}

	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,

	    			int count) {
	    		// TODO Auto-generated method stub
	    	}
	   }	
	public void clicker(View v)
	{
		switch(v.getId())
		{
		case R.id.hme:
			cf.gohome();
				break;
		case R.id.helpid:
			cf.ShowToast("Use for both Commercial and Residential GCH reports.",0);
			break;
		case R.id.loadcomments:
			/***Call for the comments***/
			 
			 int len=((EditText)findViewById(R.id.gchcomment)).getText().toString().length();
			 /*if(len<500)
			 {*/
				if(load_comment )
				{
					load_comment=false;
					int loc[] = new int[2];
					v.getLocationOnScreen(loc);
					Intent in1 = cf.loadComments("Overall GCH Comments",loc);
					in1.putExtra("length", len);
					in1.putExtra("max_length", 500);
					startActivityForResult(in1, cf.loadcomment_code);
				}
			/* }
			 else
			 {
				cf.ShowToast("You have exceeded the maximum length.", 0);
			 }*/
			 break;
		case R.id.save:
			if(((EditText)findViewById(R.id.gchcomment)).getText().toString().trim().equals(""))
			{
				cf.ShowToast("Please enter the Overall GCH Comments.", 0);
			}
			else
			{
				Cursor GCHComm_save=null;
				try
				{
					GCHComm_save=cf.SelectTablefunction(cf.GCH_commenttbl, " where fld_srid='"+cf.selectedhomeid+"'");
					if(GCHComm_save.getCount()>0)
					{
						try
						{
							cf.arr_db.execSQL("UPDATE "+cf.GCH_commenttbl+ " set gchcomment='"+cf.encode(((EditText)findViewById(R.id.gchcomment)).getText().toString())+"' where fld_srid='"+cf.selectedhomeid+"'");
							cf.ShowToast("Overall GCH Comments updated successfully.", 1);
						    ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
						   
						}
						catch (Exception E)
						{
							String strerrorlog="Updating the COMMENTS - GCH";
							cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
						}
					}
					else
					{
						try
						{
							cf.arr_db.execSQL("INSERT INTO "
								+ cf.GCH_commenttbl
								+ " (fld_srid,gchcomment)"
								+ "VALUES ('"+cf.selectedhomeid+"','"+cf.encode(((EditText)findViewById(R.id.gchcomment)).getText().toString())+"')");

						 cf.ShowToast("Overall GCH Comments saved successfully.", 1);
						 ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE); 
						
						}
						catch (Exception E)
						{
							String strerrorlog="Inserting the COMMENTS- GCH";
							cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
						}
					}
				}
				catch (Exception E)
				{
						String strerrorlog="Checking the rows inserted in the GCH COMMENTS table.";
						cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
			}
			break;
		}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			cf.goclass(418);
			//cf.gotoclass(35,WindMitCommRestsupp.class);
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	
		  if(requestCode==cf.loadcomment_code)
		  {
			  load_comment=true;
			if(resultCode==RESULT_OK)
			{
				cf.ShowToast("The Comments added successfully.", 0);
				 String gchcomts = ((EditText)findViewById(R.id.gchcomment)).getText().toString();
				 ((EditText)findViewById(R.id.gchcomment)).setText((gchcomts+" "+data.getExtras().getString("Comments")).trim());
				
			}
			
		}

	}
}
