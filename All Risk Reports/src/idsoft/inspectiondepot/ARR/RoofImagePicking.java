package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import android.R.layout;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class RoofImagePicking extends Activity {
String path="";
CommonFunctions cf;
boolean clear=false;
int id=0;
Cursor c =null;
LinearLayout Dy_li;
String cover="",selected_path[];
String  updated_path="";
//Bitmap b1=null;
//int currnet_rotated=0;
//Button rotateleft,rotateright;
private Uri CapturedImageURI;
private String insp;
private int currnet_rotated;
private ImageView upd_img;
Bitmap rotated_b=null;
Bundle b;
Boolean read_only=false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.roof_pick_image);
		cf=new CommonFunctions(this);
		b= getIntent().getExtras();
		if(b!=null)
		{
			Boolean roof_cover_img=b.getBoolean("roof_cover_img");
			if(roof_cover_img)
			{
				cover =b.getString("Cover");
				insp =b.getString("Insp_id");
				cf.selectedhomeid=b.getString("SRID");
				read_only=b.getBoolean("readonly");
				if(read_only)
				{
					((TextView)findViewById(R.id.takeimg)).setText("View Photos");
					findViewById(R.id.image_rel).setVisibility(View.GONE);
					((TextView)findViewById(R.id.txt_v)).setVisibility(cf.v1.VISIBLE);
					((TextView)findViewById(R.id.txt_v)).setText("Note:Touch/Click the photos to Zoom.");
					findViewById(R.id.clear).setVisibility(View.GONE);
					//findViewById(R.id.save).setVisibility(View.GONE);
				}
			}
			
			
		}
		System.out.println("the cover and insp="+cover+"/"+insp);
		Dy_li=(LinearLayout) findViewById(R.id.dynamic_v);
		show_savedValue();
	}
		
	
	public void clicker(View v)
	{
		Intent n1;
	switch(v.getId())
	 {
	case R.id.save:
		n1= new Intent();
		n1.putExtras(b);
		//n1.putExtra("id", id);
		setResult(RESULT_OK,n1);
		finish();
	
	break;
	case R.id.clear:
		AlertDialog.Builder builder = new AlertDialog.Builder(
				RoofImagePicking.this);
		builder.setMessage(
				"Do you want to delete all the images?")
				.setTitle("Confirmation")
				.setIcon(R.drawable.alertmsg)
				.setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog,
									int id) {
								try {
									
									cf.arr_db.execSQL("Delete  from "+cf.Roofcoverimages+" Where RIM_masterid=(Select RM_masterid from "+cf.Roof_master+
											" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+insp+"' and RM_Covering='"+cf.encode(cover)+"')" );
									cf.ShowToast("Image has been deleted successfully.",0);	
								} catch (Exception e) {
									System.out.println("exception e  "
											+ e);
								}
								

								
								Intent n1= new Intent();
								n1.putExtras(b);
								//n1.putExtra("id", id);
								setResult(RESULT_CANCELED,n1);
								finish();
								dialog.cancel();
								// showimages();
							}
						})
				.setNegativeButton("No",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int id) {

								dialog.cancel();
							}
						});
		builder.show();
	
	
		
		
			
	break;
	case R.id.cancel:
		
		/*if((!path.trim().equals("") || clear))
		{
			Intent n= new Intent();
			n.putExtra("path", path);
			n.putExtra("id", id);
			setResult(RESULT_OK,n);
			finish();
		}
		else
		{*/
		n1= new Intent();
		n1.putExtras(b);
		//n1.putExtra("id", id);
		setResult(RESULT_CANCELED,n1);
	finish();
			
		//}
	break;
	case R.id.browsecamera:
		if(selected_path.length<8)
		{
			String fileName = "temp.jpg";
			ContentValues values = new ContentValues();
			values.put(MediaStore.Images.Media.TITLE, fileName);
			CapturedImageURI = getContentResolver().insert(
					MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			intent.putExtra(MediaStore.EXTRA_OUTPUT, CapturedImageURI);
			startActivityForResult(intent, 0);
		}
		else
		{
			cf.ShowToast("You cannot add more than "+8+" images", 1);
		}	

	break;
	case R.id.browsetxt:
		
		System.out.println("selected_path"+selected_path);
		if(selected_path.length<8)
		{
				Intent reptoit1 = new Intent(this,Select_phots.class);
				Bundle b=new Bundle();
				reptoit1.putExtra("Selectedvalue", selected_path); /**Send the already selected image **/
				reptoit1.putExtra("Maximumcount", selected_path.length);/**Total count of image in the database **/
				reptoit1.putExtra("Total_Maximumcount", 8); /***Total count of image we need to accept**/
				//reptoit1.putExtra("ok", "true"); /***Total count of image we need to accept**/
				//reptoit1.setClassName("com.idinspection","com.idinspection.Select_phots");
				startActivityForResult(reptoit1,121); /** Call the Select image page in the idma application  image ***/
		}
		else
		{
			cf.ShowToast("You cannot add more than "+8+" images", 1);
		}
		/*}
		else
		{
			cf.ShowToast("Please install IDMA new version", 0);
		}*/
				/***We call the centralized image selection part ends **/
	break;
	
	default:
		break;
	}

	
	
	}
	public String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}
	public void show_savedValue()
	{
		c=cf.arr_db.rawQuery("Select * from "+cf.Roofcoverimages+" Where RIM_masterid=(Select RM_masterid from "+cf.Roof_master+
				" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+insp+"' and RM_Covering='"+cf.encode(cover)+"') order by RIM_Order", null);
		Dy_li.removeAllViews();
		
		if(c.getCount()>0)
		{
			
			
		
			if(!read_only)
			{
				findViewById(R.id.clear).setVisibility(View.VISIBLE);
				((TextView)findViewById(R.id.txt_v)).setVisibility(cf.v1.VISIBLE);
				((TextView)findViewById(R.id.txt_v)).setText("Note:Touch/Click the photos to Edit.");
			}
			else
			{
				((TextView)findViewById(R.id.txt_v)).setVisibility(cf.v1.VISIBLE);
				((TextView)findViewById(R.id.txt_v)).setText("Note:Touch/Click the photos to Zoom.");
				findViewById(R.id.clear).setVisibility(View.GONE);
			}
			LinearLayout.LayoutParams lp= new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
			LinearLayout.LayoutParams paramselevimg = new LinearLayout.LayoutParams(100, 100);
			LinearLayout.LayoutParams Ed_pa = new LinearLayout.LayoutParams(200, LayoutParams.WRAP_CONTENT);
			paramselevimg.topMargin = 10;
			paramselevimg.leftMargin = 20;
			ImageView im;
			TextView ed;
			
			BitmapDrawable bmd=null;
			String path,caption;
			c.moveToFirst();
			selected_path=new String[c.getCount()];
			System.out.println("c.getco="+c.getCount());
			for(int i=0;i<c.getCount();i++,c.moveToNext())
			{
				selected_path[i]=path=cf.decode(c.getString(c.getColumnIndex("RIM_Path")));System.out.println("ppp="+path);
				caption=cf.decode(c.getString(c.getColumnIndex("RIM_Caption")));
				LinearLayout li = new LinearLayout(this);
				li.setGravity(Gravity.CENTER_VERTICAL);
				li.setTag(c.getString(c.getColumnIndex("id")));
				li.setOrientation(LinearLayout.HORIZONTAL);
				im=new ImageView(this);
				ed=new TextView(this);
				ed.setWidth(0);
				ed.setPadding(10, 0, 0, 0);
				ed.setTextColor(getResources().getColor(R.color.black));
				System.out.println("path11="+path);
				if(!path.trim().equals(""))
				{
					File f =new File(path);
					if(f.exists())
					{
						Bitmap bitmap=cf.ShrinkBitmap(path, 100, 100);
						bmd = new BitmapDrawable(bitmap);
						im.setBackgroundDrawable(bmd);
					}
					else
					{
						
						im.setBackgroundDrawable(getResources().getDrawable(R.drawable.photonotavail));
					}
				}
				else 
				{
					im.setBackgroundDrawable(getResources().getDrawable(R.drawable.photonotavail));
				}
				
				im = new ImageView(this);
				im.setImageDrawable(bmd);
				im.setTag("ImageView");
				if(!read_only)
				{
					im.setOnClickListener(new image_edit(c.getInt(c.getColumnIndex("id")),caption,path));
				}
				else
				{
					im.setOnClickListener(new image_zoom(path));
				}
				ed.setText(caption);
				li.addView(im, paramselevimg);
				li.addView(ed, Ed_pa);
				Dy_li.addView(li);
			}
		
		}
		else 
		{
			
			selected_path=new String[0];
			findViewById(R.id.clear).setVisibility(View.GONE);
			//((TextView)findViewById(R.id.txt_v)).setVisibility(cf.v1.VISIBLE);
			//((TextView)findViewById(R.id.txt_v)).setText("Note:Touch/Click the photos to Zoom.");
		}

	}
	class image_zoom implements OnClickListener
	{
		String path;
		image_zoom(String path)
		{
			this.path=path;			
		}
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			System.out.println("clcik"+path);
			Intent reptoit1 = new Intent(RoofImagePicking.this,ImageZoom.class);
			Bundle b=new Bundle();
			reptoit1.putExtra("Path", path);System.out.println("clcikrepo");
			startActivity(reptoit1);
		}
		
	}
	class image_edit implements OnClickListener
	{
		int id;
		String path,caption;
		
		image_edit(int id,String caption,String path)
		{
			this.id=id;
			this.path=path;
			this.caption=caption;
		}
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			System.out.println("its comes here ");
			dispfirstimg(id,caption, path);
		}
		
	}
	public void dispfirstimg(final int selid, String photocaptions,final String path) {
		currnet_rotated=0;
		updated_path="";
		final Dialog dialog1 = new Dialog(RoofImagePicking.this,
				android.R.style.Theme_Translucent_NoTitleBar);
		dialog1.getWindow().setContentView(R.layout.alert);

		/**
		 * get the help and update relative layou and set the visbilitys for the
		 * respective relative layout
		 */
		LinearLayout Re = (LinearLayout) dialog1.findViewById(R.id.maintable);
		Re.setVisibility(View.GONE);
		LinearLayout Reup = (LinearLayout) dialog1
				.findViewById(R.id.updateimage);
		Reup.setVisibility(View.VISIBLE);
		/**
		 * get the help and update relative layou and set the visbilitys for the
		 * respective relative layout
		 */
		

		 upd_img = (ImageView) dialog1.findViewById(R.id.firstimg);
		 Bitmap bitmap=cf.ShrinkBitmap(path, 200, 200);
		 if(bitmap!=null)
		 {
			 upd_img.setImageBitmap(bitmap);
		 }
		final EditText upd_Ed = (EditText) dialog1.findViewById(R.id.firsttxt);
		upd_Ed.setFocusable(false);
		upd_Ed.setText(photocaptions);
		//upd_Ed.setFocusableInTouchMode(true);
		ImageView btn_helpclose = (ImageView) dialog1
				.findViewById(R.id.imagehelpclose);
		Button btn_up = (Button) dialog1.findViewById(R.id.update);
		Button btn_del = (Button) dialog1.findViewById(R.id.delete);
		
		Button rotateleft = (Button) dialog1.findViewById(R.id.rotateleft);
		Button rotateright = (Button) dialog1.findViewById(R.id.rotateright);
        Button zoom = (Button) dialog1.findViewById(R.id.zoom);
        btn_del.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				AlertDialog.Builder builder = new AlertDialog.Builder(
						RoofImagePicking.this);
				builder.setMessage(
						"Are you sure, Do you want to delete the image?")
						.setTitle("Confirmation")
						.setIcon(R.drawable.alertmsg)
						.setCancelable(false)
						.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog,
											int id) {
										try {
											Cursor cur =cf.SelectTablefunction(cf.Roofcoverimages, " Where id='"+selid+"'");
											int image_order=0;
											if(cur.getCount()==1)
											{
												cur.moveToFirst();
												image_order=cur.getInt(cur.getColumnIndex("RIM_Order"));
												
											}
											cf.arr_db.execSQL("DELETE FROM "+cf.Roofcoverimages+" Where id='"+selid+"'");
											String  in_val="";
											for(int i=image_order+1;i<=8;i++)
											{
												in_val+="'"+i+"',";
											}
											if(!in_val.equals(""))
											{
												in_val=in_val.substring(0,in_val.length()-1);
												cf.arr_db.execSQL("Update "+cf.Roofcoverimages+" SET RIM_Order=RIM_Order-1 Where  RIM_masterid=(Select RM_masterid from "+cf.Roof_master+
														" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+insp+"' and RM_Covering='"+cf.encode(cover)+"') and  RIM_Order in("+in_val+")");
											}
											
											
											
											
											
										} catch (Exception e) {
											System.out.println("exception e  "
													+ e);
										}
										cf.ShowToast("Image has been deleted successfully.",0);

										dialog1.setCancelable(true);
										dialog1.dismiss();
										show_savedValue();
										// showimages();
									}
								})
						.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {

										dialog.cancel();
									}
								});
				builder.show();
			}
			
		});
        upd_Ed.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				upd_Ed.setFocusable(true);
				upd_Ed.setFocusableInTouchMode(true);
				return false;
			}
		});
        zoom.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				String pathforzoom="";
				if(updated_path.equals(""))
				{
					pathforzoom = path;
				}
				else
				{
					pathforzoom = updated_path;
				}
				Intent reptoit1 = new Intent(RoofImagePicking.this,ImageZoom.class);
				Bundle b=new Bundle();
				reptoit1.putExtra("Path", pathforzoom);
				startActivity(reptoit1);
			}
		});
        upd_img.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try
				{
					
				Intent reptoit1 = new Intent(RoofImagePicking.this,Select_phots.class);
				Bundle b=new Bundle();
				reptoit1.putExtra("Selectedvalue", selected_path); /**Send the already selected image **/
				reptoit1.putExtra("Maximumcount", 0);/**Total count of image in the database **/
				reptoit1.putExtra("Total_Maximumcount", 1); /***Total count of image we need to accept**/
				reptoit1.putExtra("ok", "true"); /***Total count of image we need to accept**/
				//reptoit1.setClassName("com.idinspection","com.idinspection.Select_phots");
				startActivityForResult(reptoit1,125); /** Call the Select image page in the idma application  image ***/
				}		
				catch ( Exception e) {
					// TODO: handle exception
				}
			}
		});
		btn_helpclose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog1.setCancelable(true);
				dialog1.dismiss();
			}
		});
		
		rotateright.setOnClickListener(new OnClickListener() {  
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				System.gc();
				currnet_rotated+=90;
				if(currnet_rotated>=360)
				{
					currnet_rotated=0;
				}
				
				Bitmap myImg;
				try {
					if(updated_path.equals(""))
					{
						myImg = BitmapFactory.decodeStream(new FileInputStream(path));
					}
					else
					{
						myImg = BitmapFactory.decodeStream(new FileInputStream(updated_path));
					}
					
					Matrix matrix =new Matrix();
					matrix.reset();
					//matrix.setRotate(currnet_rotated);
					matrix.postRotate(currnet_rotated);
					
					 rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
					        matrix, true);
					 System.gc();
					 upd_img.setImageBitmap(rotated_b);

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (Exception e) {
					
				}
				catch (OutOfMemoryError e) {
					System.out.println("comes in to out ot mem exception");
					System.gc();
					try {
						myImg=null;
						System.gc();
						Matrix matrix =new Matrix();
						matrix.reset();
						//matrix.setRotate(currnet_rotated);
						matrix.postRotate(currnet_rotated);
						myImg= cf.ShrinkBitmap(path, 800, 800);
						rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
						
						System.gc();
						upd_img.setImageBitmap(rotated_b); 

					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					 catch (OutOfMemoryError e1) {
							// TODO Auto-generated catch block
						 cf.ShowToast("You cannot rotate this image. Image size exceeds 2MB.",0);
					}
				}

			}
		});
		rotateleft.setOnClickListener(new OnClickListener() { 
 			
			public void onClick(View v) {

				// TODO Auto-generated method stub
			
				System.gc();
				currnet_rotated-=90;
				if(currnet_rotated<0)
				{
					currnet_rotated=270;
				}

				
				Bitmap myImg;
				try {
					if(updated_path.equals(""))
					{
						myImg = BitmapFactory.decodeStream(new FileInputStream(path));
					}
					else
					{
						myImg = BitmapFactory.decodeStream(new FileInputStream(updated_path));
					}
					//myImg = BitmapFactory.decodeStream(new FileInputStream(arrpath[selid]));
					Matrix matrix =new Matrix();
					matrix.reset();
					//matrix.setRotate(currnet_rotated);
					System.out.println("Ther is no more issues top ");
					matrix.postRotate(currnet_rotated);
					
					 rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
					        matrix, true);
					 
					 upd_img.setImageBitmap(rotated_b);

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (Exception e) {
					
				}
				catch (OutOfMemoryError e) {
					System.out.println("comes in to out ot mem exception");
					System.gc();
					try {
						myImg=null;
						System.gc();
						Matrix matrix =new Matrix();
						matrix.reset();
						//matrix.setRotate(currnet_rotated);
						matrix.postRotate(currnet_rotated);
						myImg= cf.ShrinkBitmap(path, 800, 800);
						rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
						System.gc();
						upd_img.setImageBitmap(rotated_b); 

					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					 catch (OutOfMemoryError e1) {
							// TODO Auto-generated catch block
						 cf.ShowToast("You cannot rotate this image. Image size exceeds 2MB.",0);
					}
				}

			
			}
		});
		btn_up.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String updstr = upd_Ed.getText().toString();
				String path_n=path;
				if(!updated_path.equals(""))
				{
					path_n=updated_path;
				}
				if(!upd_Ed.getText().toString().trim().equals(""))
				{
				try
				{
					System.out.println("Update "+cf.Roofcoverimages+" SET RIM_Caption='"+cf.encode(updstr)+"',RIM_Path='"+cf.encode(path_n)+"' WHERE id='"+selid+"'");
					cf.arr_db.execSQL("Update "+cf.Roofcoverimages+" SET RIM_Caption='"+cf.encode(updstr)+"',RIM_Path='"+cf.encode(path_n)+"' WHERE id='"+selid+"'");
					
					/**Save the rotated value in to the external stroage place **/
					if(currnet_rotated>0)
					{ 

						try
						{
							/**Create the new image with the rotation **/
							String current=MediaStore.Images.Media.insertImage(getContentResolver(), rotated_b, "My bitmap", "My rotated bitmap");
							  ContentValues values = new ContentValues();
							  values.put(MediaStore.Images.Media.ORIENTATION, 0);
							  RoofImagePicking.this.getContentResolver().update(Uri.parse(current), values, MediaStore.Images.Media.DATA+ "=?", new String[] { current } );
							
							
							if(current!=null)
							{
							String path1=getPath(Uri.parse(current));
							if(updated_path.equals(""))
							{
							File fout = new File(path);
							fout.delete();
							/** delete the selected image **/
							File fin = new File(path1);
							/** move the newly created image in the slected image pathe ***/
							fin.renameTo(new File(path));
							}
							else
							{
								
								File fout = new File(updated_path);
								fout.delete();
								/** delete the selected image **/
								
								File fin = new File(path1);
								/** move the newly created image in the slected image pathe ***/
								fin.renameTo(new File(updated_path));
							}
							
							
							
							
						}
						} catch(Exception e)
						{
							System.out.println("Error occure while rotate the image "+e.getMessage());
						}
					}
					
					
					
				
				
				} catch (Exception e) {
					System.out.println("erre " + e.getMessage());
					
				}
				cf.ShowToast("Image updated successfully.", 1);
				dialog1.setCancelable(true);
				dialog1.dismiss();
				show_savedValue();
			}
			else
			{
				cf.ShowToast("Please enter the photo caption.", 1);
				upd_Ed.requestFocus();
			}
				
				/**Save the rotated value in to the external stroage place ends **/
			}
		});
		dialog1.show();
	}
/** Showing the image for the Update starts **/
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		String selectedImagePath = "", picname = "";
		int imageorder=c.getCount(),max_c=1;
		try
		{
			if (resultCode == RESULT_OK) {
				if (requestCode == 0) {
					String[] projection = { MediaStore.Images.Media.DATA };
					Cursor cursor = managedQuery(CapturedImageURI, projection,
							null, null, null);
					int column_index_data = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					cursor.moveToFirst();
					path  = cursor.getString(column_index_data);
					String caption;
					String master_id="";
					String sql="";
					Cursor master=cf.SelectTablefunction(cf.Roof_master, " Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+insp+"' and RM_Covering='"+cf.encode(cover)+"' and RM_module_id='0'");
					/*System.out.println("the inser  query "+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+insp+"' and RM_Covering='"+cover+"' and RM_module_id='0'");
					System.out.println(" master count"+master.getCount());*/
					if(master.getCount()<=0)
					{
						
						cf.arr_db.execSQL("INSERT INTO "+cf.Roof_master+" (RM_inspectorid,RM_srid,RM_insp_id,RM_module_id,RM_Covering) Values ('"+cf.Insp_id+"','"+cf.selectedhomeid+"','"+cf.encode(insp)+"','0','"+cf.encode(cover)+"')");
						//System.out.println(" master sql=INSERT INTO "+cf.Roof_master+" (RM_inspectorid,RM_srid,RM_insp_id,RM_module_id,RM_Covering) Values ('"+cf.Insp_id+"','"+cf.selectedhomeid+"','"+cf.encode(insp)+"','0','"+cf.encode(cover)+"')");
					}
					
						
							
							imageorder+=1;
							caption=cover+" "+imageorder;
							cf.arr_db.execSQL("INSERT INTO "+cf.Roofcoverimages +"(RIM_masterid,RIM_module_id,RIM_Path,RIM_Order,RIM_Caption) VALUES ((SELECT RM_masterid as RIM_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(insp)+"' and RM_Covering='"+cf.encode(cover)+"' and RM_module_id='0' ) ," +
									"'0','"+cf.encode(path)+"','"+imageorder+"','"+cf.encode(caption)+"')");
							show_savedValue();
							cf.ShowToast("Image uploaded successfully.",0);
				}
				else if(requestCode == 121)
				{
					System.out.println("result shown");
					/** we get the result from the Idma page for this elevation **/
				String[] value=	data.getExtras().getStringArray("Selected_array"); /**We pass the array of tje value from the IDAM Select page **/
					if(value.length>0)
					{
						
						//System.out.println("result shown1"+c.getCount());
						
						String caption;
						String master_id="";
						String sql="";
						Cursor master=cf.SelectTablefunction(cf.Roof_master, " Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+insp+"' and RM_Covering='"+cf.encode(cover)+"' and RM_module_id='0'");
						/*System.out.println("the inser  query "+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+insp+"' and RM_Covering='"+cover+"' and RM_module_id='0'");
						System.out.println(" master count"+master.getCount());*/
						if(master.getCount()<=0)
						{
							
							cf.arr_db.execSQL("INSERT INTO "+cf.Roof_master+" (RM_inspectorid,RM_srid,RM_insp_id,RM_module_id,RM_Covering) Values ('"+cf.Insp_id+"','"+cf.selectedhomeid+"','"+cf.encode(insp)+"','0','"+cf.encode(cover)+"')");
							//System.out.println(" master sql=INSERT INTO "+cf.Roof_master+" (RM_inspectorid,RM_srid,RM_insp_id,RM_module_id,RM_Covering) Values ('"+cf.Insp_id+"','"+cf.selectedhomeid+"','"+cf.encode(insp)+"','0','"+cf.encode(cover)+"')");
						}
						
						for(int i=0;i<value.length;i++)
						{
							System.out.println("result value"+value[i]);
							if(!value[i].equals(""))
							{
								path=value[i];
								imageorder+=1;
								caption=cover+" "+imageorder;
								
								if(sql.equals(""))
								{
									sql="INSERT INTO "+cf.Roofcoverimages +" SELECT (Select (case when ((SELECT MAX(id) FROM "+cf.Roofcoverimages+") is NULL ) then '0' else (SELECT MAX(id) FROM "+cf.Roofcoverimages+") end))+"+max_c+"  as id,(SELECT RM_masterid as RIM_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(insp)+"' and RM_Covering='"+cf.encode(cover)+"' and RM_module_id='0' ) ,'0' as RIM_module_id,'"+cf.encode(path)+"' as RIM_Path,'"+imageorder+"' as RIM_Order,'"+cf.encode(caption)+"' as RIM_Caption";
									
									max_c++;
								}
								else
								{
									sql+=" UNION SELECT  (Select (case when ((SELECT MAX(id) FROM "+cf.Roofcoverimages+") is NULL ) then '0' else (SELECT MAX(id) FROM "+cf.Roofcoverimages+") end))+"+max_c+",(SELECT RM_masterid as RIM_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(insp)+"' and RM_Covering='"+cf.encode(cover)+"' and RM_module_id='0' ),'0','"+cf.encode(path)+"','"+imageorder+"','"+cf.encode(caption)+"'";
									
									max_c++;
								}
								
							}
						}
						System.out.println("the sql ="+sql);
						cf.arr_db.execSQL(sql);
						show_savedValue();
						cf.ShowToast("Image uploaded successfully.",0);
					}
						
				}
				else if(requestCode==125)
				{
					String[] value=	data.getExtras().getStringArray("Selected_array"); /**We pass the array of tje value from the IDAM Select page **/
					updated_path=value[0];
					System.out.println("method "+updated_path);
					if(((File) new File(updated_path)).exists())
					{
						upd_img.setImageBitmap(cf.ShrinkBitmap(updated_path, 150, 150));
						currnet_rotated=0;
					}
				}
			} else {
				//path = "";
				// edbrowse.setText("");
			}
		
		}
		catch(Exception e)
		{
			path="";
		}
		
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if((!path.trim().equals("") || clear))
			{
				Intent n= new Intent();
				n.putExtra("path", path);
				n.putExtra("id", id);
				setResult(RESULT_OK,n);
				finish();
			}
			else
			{
				Intent n1= new Intent();
				n1.putExtra("id", id);
				setResult(RESULT_CANCELED,n1);
				finish();
			}
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}
}
