package idsoft.inspectiondepot.ARR;



import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketTimeoutException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Html;
import android.util.Log;

public class Accessbymainapplication extends Activity {
	
	private static final String TAG = null;
	CommonFunctions cf;
	ProgressDialog pd,pd1;
	int verify,show_hanlder;
	String Ins_Id,Ins_FirstName,Ins_MiddleName,Ins_LastName,Ins_Address,Ins_CompanyName,Ins_CompanyId,Ins_UserName,Ins_Password,Ins_PhotoExtn
	,Ins_Email,Insp_sign,Insp_PrimaryLicenseNumber,Insp_PrimaryLicenseType;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle extras = getIntent().getExtras();
		
		cf = new CommonFunctions(this);
		if (extras != null) {
			     Ins_Id=extras.getString("Ins_Id");
			     Ins_FirstName=extras.getString("Ins_FirstName");
			     Ins_MiddleName=extras.getString("Ins_MiddleName");
			     Ins_LastName=extras.getString("Ins_LastName");
			     Ins_Address=extras.getString("Ins_Address");
			     Ins_CompanyName=extras.getString("Ins_CompanyName");
			     Ins_CompanyId= extras.getString("Ins_CompanyId");
			     Ins_UserName=extras.getString("Ins_UserName");
			     Ins_Password= extras.getString("Ins_Password");
			     Ins_PhotoExtn= extras.getString("Ins_PhotoExtn");
			     Ins_Email= extras.getString("Ins_Email");
			     Insp_sign= extras.getString("Insp_sign");System.out.println("Insp_sign"+Insp_sign);
			     Insp_PrimaryLicenseNumber= extras.getString("Insp_PrimaryLicenseNumber");System.out.println("PRIM="+Insp_PrimaryLicenseNumber);
			     Insp_PrimaryLicenseType= extras.getString("Insp_PrimaryLicenseType");System.out.println("LICTYPE="+Insp_PrimaryLicenseType);
			    int V_code=0;
			    
			    
			   
			    
			    try {
					Cursor cur = cf.arr_db.rawQuery("select * from " + cf.policyholder, null);
					cur.moveToFirst();
					int result = cur.getColumnIndex("Workorderno");
					int result1 = cur.getColumnIndex("iscustominspection");
					if (result1 == -1) 
					{
						cf.arr_db.execSQL("ALTER TABLE " + cf.policyholder +" ADD COLUMN iscustominspection Integer default '0'");
					}
					if (result == -1) 
					{
						//cf.db.execSQL("ALTER TABLE " + cf.inspectorlogin +" ADD COLUMN Ins_ImageName VARCHAR");
						
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.policyholder );
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.cloud_tbl );
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.quesorigmiti_tbl );
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.master_tbl );
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.spinnertbl );
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.GCH_PoolPrsenttbl );
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.GCH_PoolFencetbl );
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.GCH_SummaryHazardstbl );
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.GCH_FireProtecttbl );
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.GCH_SpecHazardstbl );
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.GCH_Hvactbl );
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.GCH_Electrictbl );
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.GCH_HoodDuct );
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.GCH_FireProtection );
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.GCH_Misc );
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.Roofcoverimages );
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.GCH_commenttbl );
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.SINK_Summarytbl );
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.SINK_Obs1tbl );
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.SINK_Obs2tbl );
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.SINK_Obs3tbl );
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.SINK_Obs4tbl );
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.SINK_Obstreetbl );
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.DRY_sumcond );
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.DRY_home  );
						cf.arr_db.execSQL("drop table IF EXISTS " +  cf.DRY_attic);
						cf.arr_db.execSQL("drop table IF EXISTS " +  cf.DRY_hvac);
						cf.arr_db.execSQL("drop table IF EXISTS " +  cf.DRY_appliance);
						cf.arr_db.execSQL("drop table IF EXISTS " +  cf.DRY_room);
						cf.arr_db.execSQL("drop table IF EXISTS " +  cf.Four_HVAC_Unit);
						cf.arr_db.execSQL("drop table IF EXISTS " + cf. Four_HVAC);
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.Four_Electrical_General );
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.Four_Electrical_Panel );
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.Four_Electrical_Receptacles );
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.Four_Electrical_Details );
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.Four_Electrical_GO );
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.Four_Electrical_Wiring );
						cf.arr_db.execSQL("drop table IF EXISTS " + cf. Four_Electrical_MainPanel);
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.Four_Electrical_SubPanel );
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.Four_Electrical_SubPanelPresent );
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.Four_Electrical_Attic );// "tbl_Four_Electrical_Attic";
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.Four_Electrical_Plumbing );// "tbl_Four_Electrical_Plumbing";
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.Plumbing_waterheater );// "tbl_Four_Electrical_Plumbing_WH";
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.Roof_cover_type);//"tbl_RoofCoverType";
					 	cf.arr_db.execSQL("drop table IF EXISTS " + cf.Rct_table);//"tbl_RoofCoverType_new";
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.Roof_additional);//"tbl_Roofadditioanl_new";
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.RCN_table);//"tbl_Roofconditonnoted_new";
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.RCN_table_dy);//"tbl_Roofconditonnoted_dynamic";
						
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.General_Roof_information);//"tbl_General_Roof_Information";
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.option_list);//"tbl_roofoption_list";
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.ImageTable );// "tbl_Photos";
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.Photo_caption );// "tbl_Photos_caption";
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.FeedBackInfoTable );// "tbl_FeedbackInfo";
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.FeedBackDocumentTable );// "tbl_Feedbackdocument";
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.MailingPolicyHolder );// "MailingPolicyHolder";
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.Agent_tabble );// "Agent_information";
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.Relator_table );// "Relator_information";	
						cf.arr_db.execSQL("drop table IF EXISTS " + cf.Additional_table );// "Additional_information";
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.Wind_Questiontbl );// "tbl_wind_buildcode";
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.Wind_QuesCommtbl );// "tbl_wind_quescomm";	 
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.quesroofcover );// "tbl_quesroofcover";
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.WindMit_WallCons);//"tbl_wallconstruction";
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.Comm_AuxBuilding);//"tbl_comm_auxbuilding";
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.Wind_Commercial_Comm);//"tbl_commercial_comments";
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.Comm_RestSuppAppl);//"tbl_commercial_restsupp_appl";
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.Comm_RestSupplement);//"tbl_commercial_restsupp";
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.Communal_Areas);//"tbl_communal_areas";
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.Communal_AreasElevation);//"tbl_communal_areaselevation";
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.Commercial_Building);//"tbl_Comm_Buidingno";
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.WeatherCondition);//"tbl_WeatherCondition";
					     cf.arr_db.execSQL("drop table IF EXISTS " + cf.inspectorlogin );// "tbl_inspectorlogin";
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.Select_insp );// "Select_inspection";	 
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.WindDoorSky_NA);//"tbl_na_winddoorsky";	 
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.IDMAVersion);//"ID_Version";
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.General_roof_view);//"General_roof_view";
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.Comments_lib);//"tbl_commentsLibrary";
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.Commercial_Wind);//"tbl_commercialwind";
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.BI_Skylights);//"tbl_buildskylights";
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.BI_Dooropenings);//"tbl_builddooropen";
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.BI_Windopenings);//"tbl_buildwindopen";
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.BI_General);//"tbl_buildgeneral";
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.policyholder);//"Policy_holder";
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.dyninspname);//"tbl_dynm_inspname";
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.inspnamelist);//"tbl_inspnamelist";
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.Comm_Aux_Master);//"tbl_comm_aux";
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.OnlineTable );// "OnlineDataTable";
						 cf.arr_db.execSQL("drop table IF EXISTS " + cf.BI_GeneralDup );// "OnlineDataTable";
						 System.out.println(" deleted success fully ");
						
						 if(cf.application_sta)
							{
							 Intent loginpage;
								 loginpage = new Intent(Intent.ACTION_MAIN);
								  loginpage.setComponent(new ComponentName("idsoft.inspectiondepot.IDMA","idsoft.inspectiondepot.IDMA.ApplicationMenu"));
								  startActivity(loginpage);
							}
						 
					}
				} catch (Exception e) {
					System.out.println("e" + e.getMessage());
				}
			    
			    try
			    {
			    	cf.CreateARRTable(83);
			    	Cursor c1 = cf.arr_db.rawQuery("select * from " + cf.FeedBackInfoTable, null);
					c1.moveToFirst();
					int result = c1.getColumnIndex("ARR_ISCUSTOM");
					if (result == -1) 
					{
						cf.arr_db.execSQL("ALTER TABLE " + cf.FeedBackInfoTable +" ADD COLUMN ARR_ISCUSTOM int default '0'");
					}
			    }
			    catch (Exception e) {
					// TODO: handle exception
				}			    
			    try
			    {
			    	cf.CreateARRTable(84);
			    	Cursor c2 = cf.arr_db.rawQuery("select * from " + cf.FeedBackDocumentTable, null);
					c2.moveToFirst();
					int result3 = c2.getColumnIndex("ARR_ISCUSTOM");
					if (result3 == -1) 
					{
						cf.arr_db.execSQL("ALTER TABLE " + cf.FeedBackDocumentTable +" ADD COLUMN ARR_ISCUSTOM int default '0'");
					}
			    }
			    catch (Exception e) {
					// TODO: handle exception
				}
			    
			    
			   
			    
			    try
			    {
			    	cf.CreateARRTable(8);
			    	Cursor c2 = cf.arr_db.rawQuery("select * from " + cf.WeatherCondition, null);
					c2.moveToFirst();
					if (c2.getColumnIndex("Wea_NA") == -1) 
					{
						cf.arr_db.execSQL("ALTER TABLE " + cf.WeatherCondition +" ADD COLUMN Wea_NA varchar default ''");
					}
					
			    }
			    catch (Exception e) {
					// TODO: handle exception
				}
			    
			    try
			    {
			    	cf.CreateARRTable(26);
			    	Cursor c = cf.arr_db.rawQuery("select * from " + cf.Four_HVAC, null);
					c.moveToFirst();
					if (c.getColumnIndex("fld_ageofmainpanel") == -1) 
					{
						cf.arr_db.execSQL("ALTER TABLE " + cf.Four_HVAC +" ADD COLUMN fld_ageofmainpanel varchar default ''");
					}
					
					if (c.getColumnIndex("fld_ageofmainpanelother") == -1) 
					{
						cf.arr_db.execSQL("ALTER TABLE " + cf.Four_HVAC +" ADD COLUMN fld_ageofmainpanelother varchar default ''");
					}
					
					if (c.getColumnIndex("fld_yearofinstconfdience") == -1) 
					{
						cf.arr_db.execSQL("ALTER TABLE " + cf.Four_HVAC +" ADD COLUMN fld_yearofinstconfdience varchar default ''");
					}
					
			    }
			    catch (Exception e) {
					// TODO: handle exception
				}
			    
			    try
			    {
			    	cf.CreateARRTable(8);
			    	Cursor c2 = cf.arr_db.rawQuery("select * from " + cf.BI_General, null);
					c2.moveToFirst();
					if (c2.getColumnIndex("BI_GEN_NA") == -1) 
					{
						cf.arr_db.execSQL("ALTER TABLE " + cf.BI_General +" ADD COLUMN BI_GEN_NA varchar default ''");
					}
					
			    }
			    catch (Exception e) {
					// TODO: handle exception
				}
			    
			    
			    String V_name="0";
			    try
			    {
			     V_code= extras.getInt("IDMA_Version_code");
			    
			     V_name= extras.getString("IDMA_Version_name");
			     
			    }
			    catch(Exception e)
			    {
			    	V_code=0;
			    	V_name="0";
			    	
			    }
			     if(V_name==null)
			     {
			    	 V_name="";
			     }
			    String inspext =  Ins_PhotoExtn;
			    String  FILENAME=Ins_Id+inspext;
			   
				
			    
				
			    try
			    {
			    	/**update the version of the IDMA application for getting updated version **/
			    	cf.CreateARRTable(20);
			    	Cursor cs=cf.SelectTablefunction(cf.IDMAVersion," Where  ID_VersionType='IDMA_Old' ");
			    	
			    	if(cs.getCount()>=1)
			    	{
			    		cf.arr_db.execSQL("UPDATE "+cf.IDMAVersion+" set ID_VersionCode='"+V_code+"' ,ID_VersionName='"+V_name+"' Where  ID_VersionType='IDMA_Old' "); 
			    	}
			    	else
			    	{
			    		
			    		cf.arr_db.execSQL("INSERT INTO "+ cf.IDMAVersion+" (ID_VersionCode,ID_VersionName,ID_VersionType) VALUES ('" + V_code + "','"+V_name + "','IDMA_Old')");
			    		
			    	}
			    	/**update the version of the IDMA application for getting updated version  Ends**/
			    FileOutputStream fos = openFileOutput(FILENAME, this.MODE_WORLD_READABLE);
				File outputFile = new File("/data/data/idsoft.inspectiondepot.IDMA/files"+"/"+Ins_Id+inspext);
				//byte b[]=outputFile.to
						if (outputFile.exists()) 
						{
				    		
									
									InputStream fis = new FileInputStream(outputFile);
									long length = outputFile.length();
									byte[] bytes = new byte[(int) length];
									int offset = 0;
									int numRead = 0;
									while (offset < bytes.length && (numRead = fis.read(bytes, offset, bytes.length - offset)) >= 0)
									{
										offset += numRead;
									}									
								fos.write(bytes);
					}
			    }
			    catch(Exception e)
			    {
			    	System.out.println("error was indma"+e.getMessage());
			    }
			    try
			    {
			    	FileOutputStream fos = openFileOutput(Insp_sign, this.MODE_WORLD_READABLE);
			    	File outputFile = new File("/data/data/idsoft.inspectiondepot.IDMA/files"+"/"+Insp_sign);
			    	System.out.println("outputFile"+outputFile);
				//byte b[]=outputFile.to
						if (outputFile.exists()) 
						{
				    		
									
									InputStream fis = new FileInputStream(outputFile);
									long length = outputFile.length();
									byte[] bytes = new byte[(int) length];
									int offset = 0;
									int numRead = 0;
									while (offset < bytes.length && (numRead = fis.read(bytes, offset, bytes.length - offset)) >= 0)
									{
										offset += numRead;
									}									
								fos.write(bytes);
					}
			    }
			    catch(Exception e)
			    {
			    	System.out.println("error was indma"+e.getMessage());
			    }
			       
		    	  	
			    cf.CreateARRTable(21);
	    	  	Cursor c1 = cf.arr_db.rawQuery("select * from " + cf.allinspnamelist, null);
	    	  	System.out.println("con="+c1.getCount());
	    	  	if(c1.getCount()==0)
	    	  	{
	    	  		System.out.println("coinside");
					    if (cf.isInternetOn()==true) {													
							 String source = "<font color=#FFFFFF>Processing Please wait..."	+ "</font>";
					         pd = ProgressDialog.show(this,"", Html.fromHtml(source), true);
							new Thread() {
								public void run()  {
									try {
										System.out.println("coinside try");					
										
										String ID,Name,insp_local = "";
										SoapObject request = new SoapObject(cf.NAMESPACE,"LOADALLINSPECTIONTYPE ");
										SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
										envelope.dotNet = true;
										envelope.setOutputSoapObject(request);
										HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL_ARR);
										androidHttpTransport.call(cf.NAMESPACE+"LOADALLINSPECTIONTYPE ",envelope);
										SoapObject inspnameinfo = (SoapObject) envelope.getResponse();
										System.out.println("LOADALLINSPECTIONTYPE"+inspnameinfo);				
										if(String.valueOf(inspnameinfo).equals("null") || String.valueOf(inspnameinfo).equals(null) || String.valueOf(inspnameinfo).equals("anytype{}"))
										{
											verify = 5;
											handler.sendEmptyMessage(0);											
										}
										else
										{
										
											 int Cnt = inspnameinfo.getPropertyCount();
											 for(int i=0;i<inspnameinfo.getPropertyCount();i++)
											 {
													
													SoapObject temp_result=(SoapObject) inspnameinfo.getProperty(i);
													ID = (String.valueOf(temp_result.getProperty("ID")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("ID")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("ID")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("ID"));
													Name=(String.valueOf(temp_result.getProperty("Name")).equals("anyType{}"))?"":(String.valueOf(temp_result.getProperty("Name")).equals("NA"))?"":(String.valueOf(temp_result.getProperty("Name")).equals("N/A"))?"":String.valueOf(temp_result.getProperty("Name"));
													
														
														if(ID.equals("13"))		  {insp_local="1";}
														else if(ID.equals("750")) {insp_local="2";}
														else if(ID.equals("28"))  {insp_local="3";}
														else if(ID.equals("9"))	  {insp_local="4";}
														else if(ID.equals("62"))  {insp_local="5";}
														else if(ID.equals("90"))  {insp_local="6";}
														else if(ID.equals("11"))  {insp_local="7";}
														else if(ID.equals("18"))  {insp_local="8";}
														else if(ID.equals("751")) {insp_local="9";}
														else { insp_local = "10"; }
	
	
														System.out.println("Insert into "+cf.allinspnamelist+" (ARR_Insp_ID,ARR_Insp_Name,ARR_Custom_ID) values" +
																			"('"+ID+"','"+cf.encode(Name).trim()+"','"+insp_local+"') ");
														cf.arr_db.execSQL("Insert into "+cf.allinspnamelist+" (ARR_Insp_ID,ARR_Insp_Name,ARR_Custom_ID) values" +
																"('"+ID+"','"+cf.encode(Name).trim()+"','"+insp_local+"') ");
														
											}
											 verify = 1;
											 handler.sendEmptyMessage(0);
									}	
										
									} catch (SocketTimeoutException s) {
										verify = 5;
										handler.sendEmptyMessage(0);
									} catch (IOException io) {
										verify = 5;
										handler.sendEmptyMessage(0);
									} catch (XmlPullParserException x) {
										verify = 5;
										handler.sendEmptyMessage(0);
									} catch (Exception e) {
										verify = 5;
										handler.sendEmptyMessage(0);
									}
								}
		
		
								private Handler handler = new Handler() {
									@Override
									public void handleMessage(
											Message msg) {
										pd.dismiss();
										if (verify == 1) 
										{	
											System.out.println("completed ");
											insertinspectionnames();
																						
										} else if (verify == 5)
										{
											cf.ShowToast("You have problem in your server connection please contact papperless inspection admin.",1);
											Intent intent = new Intent(Intent.ACTION_MAIN);
											intent.setComponent(new ComponentName("idsoft.inspectiondepot.IDMA","idsoft.inspectiondepot.IDMA.ApplicationMenu"));
											startActivity(intent);
										}
									}

									
								};
							}.start();
							
						} else {
							//cf.ShowToast("Internet connection not available.", 0);
							AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(Accessbymainapplication.this);
							alertDialog1
						.setMessage("Internet Connection is not available. Please enable Internet connection to Proceed further.");
						
				      	alertDialog1.setPositiveButton("Ok",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								
								Intent intent = new Intent(Intent.ACTION_MAIN);
								intent.setComponent(new ComponentName("idsoft.inspectiondepot.IDMA","idsoft.inspectiondepot.IDMA.ApplicationMenu"));
								startActivity(intent);


							}
						});
				      
				      	alertDialog1.setCancelable(false);
				      	alertDialog1.show();
						}
	    	  	}
	    	  	else
	    	  	{
	    	  		redirect();
	    	  	}

	    	  	 
	    	  /*	if (cf.isInternetOn()==true) {													
	   			 String source = "<font color=#FFFFFF>Processing Please wait..."	+ "</font>";
	   	         pd1 = ProgressDialog.show(this,"", Html.fromHtml(source), true);
	   			new Thread() {
	   				public void run()  {
	   					try {
	   						System.out.println("coinside try");					
	   						String HomeId,PH_InspectionTypeId;
	   						SoapObject objInsert = cf.Calling_WS1(cf.Insp_id,"UPDATEMOBILEDB_SCHEASSIGN");

	   						System.out.println("UPDATEMOBILEDB_SCHEASSIGN"+objInsert);				
	   						if(String.valueOf(objInsert).equals("null") || String.valueOf(objInsert).equals(null) || String.valueOf(objInsert).equals("anytype{}"))
	   						{
	   							show_hanlder=5;
	   							handler1.sendEmptyMessage(0);											
	   						}
	   						else
	   						{
	   						
	   							 int Cnt = objInsert.getPropertyCount();
	   							 for(int i=0;i<objInsert.getPropertyCount();i++)
	   							 {
	   									
	   									SoapObject obj=(SoapObject) objInsert.getProperty(i);
	   									HomeId = (String.valueOf(obj.getProperty("SRID")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("SRID")).equals("NA"))?"":(String.valueOf(obj.getProperty("SRID")).equals("N/A"))?"":String.valueOf(obj.getProperty("SRID"));
	   								     PH_InspectionTypeId = (String.valueOf(obj.getProperty("InspectionIds")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectionIds")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectionIds")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectionIds"));
	   								     Cursor c2 = cf.SelectTablefunction(cf.Select_insp, " where SI_InspectorId='" + cf.encode(cf.Insp_id) + "' and SI_srid='"+HomeId+"'");
	   									 if(c2.getCount()>0)
	   									 {
	   										 System.out.println(" UPDATE  "+cf.Select_insp+" SET SI_InspectionNames='"+cf.encode(PH_InspectionTypeId)+"' WHERE SI_srid='"+HomeId+"' and SI_InspectorId='" + cf.encode(cf.Insp_id) + "'");
	   										 cf.arr_db.execSQL(" UPDATE  "+cf.Select_insp+" SET SI_InspectionNames='"+cf.encode(PH_InspectionTypeId)+"' WHERE SI_srid='"+HomeId+"' and SI_InspectorId='" + cf.encode(cf.Insp_id) + "'");
	   									 } 													}
	   							 show_hanlder = 1;
	   							 handler1.sendEmptyMessage(0);
	   					}	
	   						
	   					} catch (SocketTimeoutException s) {
	   						show_hanlder = 5;
	   						handler1.sendEmptyMessage(0);
	   					} catch (IOException io) {
	   						show_hanlder = 5;
	   						handler1.sendEmptyMessage(0);
	   					} catch (XmlPullParserException x) {
	   						show_hanlder = 5;
	   						handler1.sendEmptyMessage(0);
	   					} catch (Exception e) {
	   						show_hanlder = 5;
	   						handler1.sendEmptyMessage(0);
	   					}
	   				}


	   				private Handler handler1 = new Handler() {
	   					@Override
	   					public void handleMessage(
	   							Message msg) {
	   						pd1.dismiss();
	   						if (show_hanlder == 1) {				
	   							redirect();
	   																		
	   						} else if (show_hanlder == 5) {
	   							cf.ShowToast("You have problem in your server connection please contact papperless inspection admin.",1);
	   						}
	   					}
	   				};
	   			}.start();
	   			
	   		} else {
	   			cf.ShowToast("Internet connection not available.", 0);
	   		}	*/	
			    
	    	  	
	    	  	
	    	  	
			}
		else
		{
			if(cf.application_sta)
			{
				Intent intent = new Intent(Intent.ACTION_MAIN);
				intent.setComponent(new ComponentName("idsoft.inspectiondepot.IDMA","idsoft.inspectiondepot.IDMA.ApplicationMenu"));
				startActivity(intent);
			}else
			{
				Intent Star = new Intent(Accessbymainapplication.this,AllRiskReportsActivity.class);
				Star.putExtra("keyName", 1);
				startActivity(Star);
			}
		}		
	}
	private void redirect()
	{
		cf.CreateARRTable(1);
	    System.out.println(Ins_Id);
	    Cursor log= cf.SelectTablefunction(cf.inspectorlogin, " where Fld_InspectorId='"+Ins_Id+"'");
		cf.arr_db.execSQL("UPDATE "+cf.inspectorlogin+" set Fld_InspectorFlag=0"); // set the flag zero  for the inspector to clear all hte login
		log.moveToFirst();
		if(log.getCount()>=1)
		{
			if(log.getString(log.getColumnIndex("Fld_InspectorFlag")).equals("1"))
			{
				
			 
				cf.arr_db.execSQL("UPDATE "+cf.inspectorlogin+" set Fld_InspectorFlag=1 where Fld_InspectorId='"+Ins_Id+"'"); // set the flag one for the inspector who logined new 
				
				Intent Star = new Intent(Accessbymainapplication.this,
						HomeScreen.class);
				Star.putExtra("keyName", 1);
				startActivity(Star);
				
				
			}
			else 
			{
				
				cf.arr_db.execSQL("UPDATE "+cf.inspectorlogin+" set Fld_InspectorFlag=1 where Fld_InspectorId='"+Ins_Id+"'"); // set the flag one for the inspector
				Intent Star = new Intent(Accessbymainapplication.this,
						HomeScreen.class);
				Star.putExtra("keyName", 1);
				startActivity(Star);
				
				
			}
			
			cf.arr_db.execSQL("UPDATE  "
					+ cf.inspectorlogin
					+ " SET  Fld_InspectorFirstName='"+Ins_FirstName + "',Fld_InspectorMiddleName='"+ Ins_MiddleName + "',Fld_InspectorLastName='"+ Ins_LastName + "'" +
							",Fld_InspectorAddress='"+ Ins_Address + "',Fld_InspectorCompanyName='"+ Ins_CompanyName + "',Fld_InspectorCompanyId='"+Ins_CompanyId + "'," +
							"Fld_InspectorUserName='"+ Ins_UserName.toLowerCase()+ "',Fld_InspectorPassword='" + Ins_Password + "',Fld_InspectorFlag='1',Android_status='true',Fld_Remember_pass=''," +
									"Fld_InspectorPhotoExtn='"+Ins_PhotoExtn+"',Fld_InspectorEmail='"+Ins_Email+"',Fld_signature='"+Insp_sign+"',Fld_licenceno='"+Insp_PrimaryLicenseNumber +"',Fld_licencetype='"+Insp_PrimaryLicenseType+"' WHERE Fld_InspectorId='"+Ins_Id+"'");
					 
			
			
		}
		else
		{
		    try {
		    	
				cf.arr_db.execSQL("INSERT INTO "
						+ cf.inspectorlogin
						+ " (Fld_InspectorId,Fld_InspectorFirstName,Fld_InspectorMiddleName,Fld_InspectorLastName,Fld_InspectorAddress,Fld_InspectorCompanyName,Fld_InspectorCompanyId,Fld_InspectorUserName,Fld_InspectorPassword,Fld_InspectorFlag,Android_status,Fld_Remember_pass,Fld_InspectorPhotoExtn,Fld_InspectorEmail,Fld_signature,Fld_licenceno,Fld_licencetype)"
						+ " VALUES ('" + Ins_Id + "','"
						+Ins_FirstName + "','"
						+ Ins_MiddleName + "','"
						+ Ins_LastName + "','"
						+ Ins_Address + "','"
						+ Ins_CompanyName + "','"
						+Ins_CompanyId + "','"
						+ Ins_UserName.toLowerCase()
						+ "','" + Ins_Password + "','"
						+ "1" + "','" +"true"+ "','','"+Ins_PhotoExtn+"','"+Ins_Email+"','"+Insp_sign +"','"+Insp_PrimaryLicenseNumber +"','"+Insp_PrimaryLicenseType+"')"); // insert the inspector and set the flag as login 
				System.out.println("herr qury INSERT INTO "
						+ cf.inspectorlogin
						+ " (Fld_InspectorId,Fld_InspectorFirstName,Fld_InspectorMiddleName,Fld_InspectorLastName,Fld_InspectorAddress,Fld_InspectorCompanyName,Fld_InspectorCompanyId,Fld_InspectorUserName,Fld_InspectorPassword,Fld_InspectorFlag,Android_status,Fld_Remember_pass,Fld_InspectorPhotoExtn,Fld_InspectorEmail,Fld_signature,Fld_licenceno,Fld_licencetype)"
						+ " VALUES ('" + Ins_Id + "','"
						+Ins_FirstName + "','"
						+ Ins_MiddleName + "','"
						+ Ins_LastName + "','"
						+ Ins_Address + "','"
						+ Ins_CompanyName + "','"
						+Ins_CompanyId + "','"
						+ Ins_UserName.toLowerCase()
						+ "','" + Ins_Password + "','"
						+ "1" + "','" +"true"+ "','','"+Ins_PhotoExtn+"','"+Ins_Email+"','"+Insp_sign +"','"+Insp_PrimaryLicenseNumber +"','"+Insp_PrimaryLicenseType+"')");
				Intent Star = new Intent(Accessbymainapplication.this,
						HomeScreen.class);
				Star.putExtra("keyName", 1);
				startActivity(Star);
				
				
			} catch (Exception e) {
				Log.i(TAG, "categorytableerror=" + e.getMessage());
				if(cf.application_sta)
				{
					Intent intent = new Intent(Intent.ACTION_MAIN);
					intent.setComponent(new ComponentName("idsoft.inspectiondepot.IDMA","idsoft.inspectiondepot.IDMA.ApplicationMenu"));
					startActivity(intent);
				}
				else
				{
					Intent Star = new Intent(Accessbymainapplication.this,AllRiskReportsActivity.class);
					Star.putExtra("keyName", 1);
					startActivity(Star);
				}
			}		   
		}	
	}
	private void insertinspectionnames()
	{
		if (cf.isInternetOn()==true) {													
  			 String source = "<font color=#FFFFFF>Processing Please wait..."	+ "</font>";
  	         pd1 = ProgressDialog.show(this,"", Html.fromHtml(source), true);
  			new Thread() {
  				public void run()  {
  					try {
  						cf.CreateARRTable(2);
  						String HomeId,PH_InspectionTypeId,tmp; String s="";int isCustomInspection;
  						SoapObject objInsert = cf.Calling_WS1(Ins_Id,"UPDATEMOBILEDB_SCHEASSIGN");

  						System.out.println("UPDATEMOBILEDB_SCHEASSIGN"+objInsert);				
  						if(String.valueOf(objInsert).equals("null") || String.valueOf(objInsert).equals(null) || String.valueOf(objInsert).equals("anytype{}"))
  						{
  							show_hanlder=5;
  							handler1.sendEmptyMessage(0);											
  						}
  						else
  						{
  						
  							 int Cnt = objInsert.getPropertyCount();
  							 for(int i=0;i<objInsert.getPropertyCount();i++)
  							 {  									
  									SoapObject obj=(SoapObject) objInsert.getProperty(i);
  									HomeId = (String.valueOf(obj.getProperty("SRID")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("SRID")).equals("NA"))?"":(String.valueOf(obj.getProperty("SRID")).equals("N/A"))?"":String.valueOf(obj.getProperty("SRID"));
  								     PH_InspectionTypeId = (String.valueOf(obj.getProperty("InspectionIds")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("InspectionIds")).equals("NA"))?"":(String.valueOf(obj.getProperty("InspectionIds")).equals("N/A"))?"":String.valueOf(obj.getProperty("InspectionIds"));
  								     Cursor c2 = cf.SelectTablefunction(cf.Select_insp, " where SI_InspectorId='" + Ins_Id + "' and SI_srid='"+HomeId+"'");
  									 System.out.println("HomeId="+HomeId+" c2="+c2.getCount() + " TypeID="+PH_InspectionTypeId+"Ins_Id"+Ins_Id);
  								     if(c2.getCount()>0)
  									 {
  										 System.out.println(" UPDATE  "+cf.Select_insp+" SET SI_InspectionNames='"+cf.encode(PH_InspectionTypeId)+"' WHERE SI_srid='"+HomeId+"' and SI_InspectorId='" + Ins_Id + "'");
  										 cf.arr_db.execSQL(" UPDATE  "+cf.Select_insp+" SET SI_InspectionNames='"+cf.encode(PH_InspectionTypeId)+"' WHERE SI_srid='"+HomeId+"' and SI_InspectorId='" + Ins_Id+ "'");
  									 }
  								     
  								     
  								   tmp = PH_InspectionTypeId;
  								   if(tmp.contains(","))
  								   {
  									 String id[] = tmp.split(",");
  									 for(int k=0;k<id.length;k++)
  									 {
  										 System.out.println("Klenth="+id.length+" K ="+id[k]);
  										 if(id[k].matches("9") || id[k].matches("11") || id[k].matches("13") || id[k].matches("18") || id[k].matches("28")
  												 || id[k].matches("750") || id[k].matches("751"))
  										 {
  												 s += "true"+"~";
  										 }
  										 else
  										 {
  												 s += "false"+"~";
  										 }
  									 }			
  								 }
  								 else
  								 {
  									 if(tmp.matches("9") || tmp.matches("11") || tmp.matches("13") || tmp.matches("18") || tmp.matches("28")
  											 || tmp.matches("750") || tmp.matches("751"))
  									 {
  										 s = "true";
  									 }
  									 else
  									 {
  										 s = "false";
  									 }
  								 }
  								 
  								 System.out.println("NEed s="+s);
  								 
  								 if(s.contains("false"))
  								 {
  									 isCustomInspection=1;
  								 }
  								 else
  								 {
  									 isCustomInspection=0;
  								 }	  
  								cf.CreateARRTable(3); 
  								Cursor c1 = cf.SelectTablefunction(cf.policyholder," where ARR_PH_InspectorId='" + Ins_Id + "' and ARR_PH_SRID='"+HomeId+"'");
  								 if(c1.getCount() >= 1)
  								 {
  									 System.out.println(" UPDATE  "+cf.policyholder+" SET iscustominspection='"+isCustomInspection+"' WHERE ARR_PH_SRID='"+HomeId+"' and ARR_PH_InspectorId='" + Ins_Id + "'");
										 cf.arr_db.execSQL(" UPDATE  "+cf.policyholder+" SET iscustominspection='"+isCustomInspection+"' WHERE ARR_PH_SRID='"+HomeId+"' and ARR_PH_InspectorId='" + Ins_Id+ "'");
									
  								 }
  							 
  							 }
  							 show_hanlder = 1;
  							 handler1.sendEmptyMessage(0);
  					}	
  						
  					} catch (SocketTimeoutException s) {
  						show_hanlder = 5;
  						handler1.sendEmptyMessage(0);
  					} catch (IOException io) {
  						show_hanlder = 5;
  						handler1.sendEmptyMessage(0);
  					} catch (XmlPullParserException x) {
  						show_hanlder = 5;
  						handler1.sendEmptyMessage(0);
  					} catch (Exception e) {
  						show_hanlder = 5;
  						handler1.sendEmptyMessage(0);
  					}
  				}


  				private Handler handler1 = new Handler() {
  					@Override
  					public void handleMessage(
  							Message msg) {
  						pd1.dismiss();
  						if (show_hanlder == 1) {				
  							redirect();
  																		
  						} else if (show_hanlder == 5) {
  							cf.ShowToast("You have problem in your server connection please contact papperless inspection admin.",1);
  							Intent intent = new Intent(Intent.ACTION_MAIN);
							intent.setComponent(new ComponentName("idsoft.inspectiondepot.IDMA","idsoft.inspectiondepot.IDMA.ApplicationMenu"));
							startActivity(intent);
  						}
  					}
  				};
  			}.start();
  			
  		} else {
  			cf.ShowToast("Internet connection not available.", 0);
  		}
	}
}