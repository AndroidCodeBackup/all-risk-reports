package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.MyOnclickListener.clicker;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class HdrOnclickListener extends LinearLayout {
	CommonFunctions cf;
	public Context context;
	public int maininsp,subinsp,app;
	public String string,maintab,subtab,innersubtab;
	public Button btn_general,btn_four,btn_rfsurvey,btn_wind,btn_gch,btn_sinkhole,
	              btn_chinese,btn_photos,btn_feedback,btn_map,btn_submit;
	
	public HdrOnclickListener(Context context, int appname,
			String string2, String string3, int i, int j, CommonFunctions cf) {
		// TODO Auto-generated constructor stub
		super(context);
		this.context = context; /** actiivty name **/
		this.maintab = string2;
		this.subtab = string3;
		this.cf = cf;
		this.app=appname;
		this.maininsp = i;
		this.subinsp = j;
		create();
	}
	
	public HdrOnclickListener(Context context, int i,
			String string2, String string3, String string4, int j, int k,
			CommonFunctions cf) {
		// TODO Auto-generated constructor stub
		super(context);
		this.context = context; /** actiivty name **/
		this.maintab = string2;
		this.subtab = string3;
		this.innersubtab = string4;
		this.cf = cf;
		this.app=i;
		this.maininsp = j;System.out.println("k="+k);
		this.subinsp = k;System.out.println("this.subinsp="+this.subinsp);
		create();
	}

	private void create() {
		LayoutInflater layoutInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutInflater.inflate(R.layout.header, this, true);
		
		((TextView)findViewById(R.id.maintxt)).setText(maintab);
		
		System.out.println("cf.URL="+cf.URL_ARR+"cf.apkrc"+cf.apkrc);
		if(cf.URL_ARR.contains("72.15.221.151:81"))
		{
			System.out.println("ca");
   			((TextView)findViewById(R.id.rcdate)).setText(cf.apkrc);
		}
   		else
   		{
   			System.out.println("ca no");
   			((TextView)findViewById(R.id.rcdate)).setText(cf.apkrc.replace("U", "L"));
   		}
		if(this.subinsp==2)
		{
			((TextView)findViewById(R.id.maintxt1)).setVisibility(GONE);
		}
		((TextView)findViewById(R.id.maintxt1)).setText(" >> ");
		((TextView)findViewById(R.id.secondtxt)).setText(subtab);
		((RelativeLayout)findViewById(R.id.rel)).setVisibility(VISIBLE);
		
		if(this.subinsp==1)
		{
			((TextView)findViewById(R.id.thirdtxt)).setVisibility(VISIBLE);
			((TextView)findViewById(R.id.sectxt1)).setVisibility(VISIBLE);
			((TextView)findViewById(R.id.thirdtxt)).setText(innersubtab);
			((TextView)findViewById(R.id.sectxt1)).setText(" >> ");
		}
		
		 ((ImageView)findViewById(R.id.head_policy_info)).setVisibility(View.VISIBLE);
		 ((ImageView)findViewById(R.id.head_take_image)).setVisibility(View.VISIBLE);
		 ((ImageView)findViewById(R.id.head_abbreviation)).setVisibility(View.VISIBLE);
		 ((ImageView)findViewById(R.id.head_insp_info)).setVisibility(View.VISIBLE);
		 ((ImageView)findViewById(R.id.head_insp_info)).setImageDrawable(getResources().getDrawable(R.drawable.header_inspector));
		 
		 switch(this.maininsp)
		 {
			case 1:
				((ImageView)findViewById(R.id.head_take_image)).setVisibility(View.GONE);
				break;
			case 2:
				((ImageView)findViewById(R.id.head_take_image)).setVisibility(View.VISIBLE);
				break;
			case 29:
				((ImageView)findViewById(R.id.head_take_image)).setVisibility(View.VISIBLE);
				break;
			case 3:
				((ImageView)findViewById(R.id.head_take_image)).setVisibility(View.VISIBLE);
				break;
			case 4:
				((ImageView)findViewById(R.id.head_take_image)).setVisibility(View.VISIBLE);
				break;
			case 5:
				((ImageView)findViewById(R.id.head_take_image)).setVisibility(View.VISIBLE);
				break;
			case 6:
				((ImageView)findViewById(R.id.head_take_image)).setVisibility(View.VISIBLE);
				break;
		 }
		 ((ImageView)findViewById(R.id.head_policy_info)).setOnClickListener(new clicker());
		 ((ImageView)findViewById(R.id.head_take_image)).setOnClickListener(new clicker());
		 ((ImageView)findViewById(R.id.head_abbreviation)).setOnClickListener(new clicker());
		 ((ImageView)findViewById(R.id.head_insp_info)).setOnClickListener(new clicker());
		 
		}
		
	
	class clicker implements OnClickListener {
         public void onClick(View v)  {
            switch (v.getId()) {
            case R.id.head_take_image:
            	Intent s= new Intent(context,TakeCamera.class);
				s.putExtra("homeid", cf.selectedhomeid);
				((Activity) cf.con).startActivityForResult(s, cf.Takeimage_rquest_code);
            	break;
            case R.id.head_policy_info:
            	Intent s1= new Intent(context,PolicyholdeInfoHead.class);
				s1.putExtra("homeid", cf.selectedhomeid);
				s1.putExtra("Type", "Policyholder");
				s1.putExtra("insp_id", cf.Insp_id);				
				((Activity) cf.con).startActivityForResult(s1, cf.info_requestcode);
            	break;
            case R.id.head_insp_info:
            	Intent s2= new Intent(context,PolicyholdeInfoHead.class);
				Bundle b1 = new Bundle();
				s2.putExtra("homeid", cf.selectedhomeid);
				s2.putExtra("Type", "Inspector");
				s2.putExtra("insp_id", cf.Insp_id);
				((Activity) cf.con).startActivityForResult(s2, cf.info_requestcode);
            	break;
            case R.id.head_abbreviation:
            	Intent s3= new Intent(context,PolicyholdeInfoHead.class);
				Bundle b = new Bundle();
				s3.putExtra("homeid", cf.selectedhomeid);
				s3.putExtra("Type", "Abbreviation");
				s3.putExtra("insp_id", cf.Insp_id);				
				((Activity) cf.con).startActivityForResult(s3, cf.info_requestcode);
            	break;
            
            }
         }
	}

}
