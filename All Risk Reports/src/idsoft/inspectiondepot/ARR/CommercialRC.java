/*
************************************************************
* Project: ALL RISK REPORT
* Module : WindMitCommareas.java
* Creation History:
Created By : Nalini on 10/24/2012
* Modification History:
Last Modified By : Gowri on 04/04/2013
************************************************************ 
*/

package idsoft.inspectiondepot.ARR;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import idsoft.inspectiondepot.ARR.BIGeneralData.RadioArrayclickerInsured;
import idsoft.inspectiondepot.ARR.WindMitCommAuxbuildings.Touch_Listener;
import idsoft.inspectiondepot.ARR.WindMitCommAuxbuildings.textwatcher;
import idsoft.inspectiondepot.ARR.WindMitCommRestsupp.RestSupp_EditClick;
import idsoft.inspectiondepot.ARR.WindMitCommRestsupp.Spin_Selectedlistener;
import idsoft.inspectiondepot.ARR.WindMitCommRestsupp.checklistenetr;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class CommercialRC extends Activity{
	CommonFunctions cf;
	String rcvalue="",levelBvalue="",roofcovering="",tmp="";
	RadioButton roofcoveringrdio[] = new RadioButton[5];
	int[] rcid = {R.id.rdio_chk1,R.id.rdio_chk2,R.id.rdio_chk3,R.id.rdio_chk4,R.id.rdio_chk5};
	boolean load_comment=true;
	String c2_q1[] = {"Level A (Non FBC Equivalent) -- Type II or III","Level B (FBC Equivalent) -- Type II or III","Not Applicable","Not Inspected","Unknown","Adequately secured"};
	CheckBox levelBchk[] = new CheckBox[9];
	int[] levelBchkid = {R.id.rclevelB1,R.id.rclevelB2,R.id.rclevelB3,R.id.rclevelB4,R.id.rclevelB5,R.id.rclevelB6,R.id.rclevelB7,R.id.rclevelB8,R.id.rclevelB9};
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cf=new CommonFunctions(this);	 
        Bundle extras = getIntent().getExtras();
  		if (extras != null) {
  			cf.getExtras(extras);				
  	 	}
  		  setContentView(R.layout.commwindroofcover);
          cf.getDeviceDimensions();
          if(cf.identityval==32){ cf.typeidentity=309;}
          else if(cf.identityval==33){cf.typeidentity=310;}
          else if(cf.identityval==34){cf.typeidentity=310;}
          
          LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
          if(cf.identityval==33)
          {
            hdr_layout.addView(new HdrOnclickListener(this,1,"Commercial Type II","Commercial Wind","Roof Coverings",3,1,cf));
          }
          else if(cf.identityval==34)
          {
              hdr_layout.addView(new HdrOnclickListener(this,1,"Commercial Type III","Commercial Wind","Roof Coverings",3,1,cf));
            }
          cf.mainmenu_layout = (LinearLayout) findViewById(R.id.header); //** HEADER MENU **//
          cf.mainmenu_layout.setMinimumWidth(cf.wd);
          cf.mainmenu_layout.addView(new MyOnclickListener(this, 3, 0,0, cf));
         
          cf.type_submenulayout =(LinearLayout) findViewById(R.id.typesubmenu); //** WINDMIT TYPE SUBMENU **//
          cf.type_submenulayout.addView(new MyOnclickListener(this, 300, 1,0, cf));
          cf.windmittypechk(cf.identityval,((RadioButton)findViewById(R.id.b1802_chk)),((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((TextView)findViewById(R.id.commtxt)),((ImageView)findViewById(R.id.editbno))); // To check the checkbox of choosen inspection type
          cf.uncheckcommercial(((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((RadioButton)findViewById(R.id.b1802_chk))); 
      	  cf.submenu_layout = (LinearLayout) findViewById(R.id.submenu);//** WINDMIT TYPE SUBMENU **//
          cf.submenu_layout.addView(new MyOnclickListener(this,cf.identityval,1,314, cf));
          cf.innersubmenu_layout = (LinearLayout) findViewById(R.id.innersubmenu);//** WINDMIT TYPE SUBMENU **//
          cf.innersubmenu_layout.addView(new MyOnclickListener(this,cf.identityval,1,124, cf));
          
          cf.mainlayout = (LinearLayout)findViewById(R.id.bc_table);
          cf.save = (Button)findViewById(R.id.save);
          cf.tblrw = (TableRow)findViewById(R.id.row2);
          cf.tblrw.setMinimumHeight(cf.ht);			
          Declaration(); cf.CreateARRTable(40);setValue();
         
    }
	private void setValue() {
		// TODO Auto-generated method stub
		try
		{
			Cursor c1 = cf.arr_db.rawQuery("SELECT * FROM " + cf.Commercial_Wind + " WHERE fld_srid='"+ cf.selectedhomeid + "'  and insp_typeid='"+cf.identityval+"'", null);				
			if(c1.getCount()>0)
			{
				c1.moveToFirst();
				String roofcovervalue = cf.decode(c1.getString(c1.getColumnIndex("fld_roofcoverings")));
				System.out.println("roofoce="+roofcovervalue);
				
				if(!roofcovervalue.equals(""))
				{
					String rcsplit[] = roofcovervalue.split("\\~",2);
					System.out.println("rcsplit"+rcsplit[0]);
					//test.split("\\(",2);
					if(!String.valueOf(rcsplit[0]).equals(""))
					{
						System.out.println("rcsplit inside for"+rcsplit[0]);
						for(int i=0;i<roofcoveringrdio.length;i++)
						{
							System.out.println("rcsplit for"+rcsplit[0]+"TAG="+roofcoveringrdio[i].getTag().toString());
							if(String.valueOf(rcsplit[0]).equals(roofcoveringrdio[i].getTag().toString()))
							{
								roofcoveringrdio[i].setChecked(true);
							}
						}
					}
					System.out.println("tetststss"+rcsplit[0]);
					 if(rcsplit[0].equals("B"))
					 {
						 System.out.println("insde BBB");
					 	((LinearLayout)findViewById(R.id.optBlin)).setVisibility(cf.v1.VISIBLE);
					 	((LinearLayout)findViewById(R.id.optClin)).setVisibility(cf.v1.VISIBLE);
					 	((TextView)findViewById(R.id.rctxt)).setVisibility(cf.v1.VISIBLE);
					 	cf.setvaluechk1(rcsplit[1],levelBchk,((EditText)findViewById(R.id.otherrfcover)));
					 }
					 ((TextView)findViewById(R.id.savetxtrc)).setVisibility(cf.v1.VISIBLE);
				}
				else
				{
					((TextView)findViewById(R.id.savetxtrc)).setVisibility(cf.v1.GONE);
				}
					 String rccomments = cf.decode(c1.getString(c1.getColumnIndex("fld_rccomments")));
				     if(!rccomments.equals(""))
				     {
					   ((EditText)findViewById(R.id.rccomment)).setText(rccomments);
				     }
			}
			else
			{
				((TextView)findViewById(R.id.savetxtrc)).setVisibility(cf.v1.GONE);
			}
		}catch(Exception e)
		{
			System.out.println("catch 1"+e.getMessage());
		}
	}
	private void Declaration() {
		// TODO Auto-generated method stub
		cf.setupUI((ScrollView) findViewById(R.id.scr));
		 for(int i=0;i<roofcoveringrdio.length;i++)
	   	 {
			 roofcoveringrdio[i] = (RadioButton)findViewById(rcid[i]); 
			 roofcoveringrdio[i].setOnClickListener(new RCrdioclicker());	   		
	   	 }	
		 for(int i=0;i<levelBchk.length;i++)
	   	 {
			 levelBchk[i] = (CheckBox)findViewById(levelBchkid[i]);    		
	   	 }	
		 
		 ((EditText)findViewById(R.id.rccomment)).addTextChangedListener(new textwatcher());
	}
	class textwatcher implements TextWatcher
    {
	         public int type;
	         textwatcher()
	    	{
	        	
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {


	    		// TODO Auto-generated method stub
	    		cf.showing_limit(s.toString(),(TextView)findViewById(R.id.rc_tv_type1),500); 
		    		if(s.toString().startsWith(" "))
		    		{
		    			((EditText)findViewById(R.id.rccomment)).setText("");
		    		}
	    		}

	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,

	    			int after) { 
	    		// TODO Auto-generated method stub
	     	}

	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,

	    			int count) {
	    		// TODO Auto-generated method stub
	    	}
	   }	
	class  RCrdioclicker implements OnClickListener
	{
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			for(int i=0;i<roofcoveringrdio.length;i++)
			{
				if(v.getId()==roofcoveringrdio[i].getId())
				{
					roofcoveringrdio[i].setChecked(true);
					if(v.getId()==roofcoveringrdio[1].getId())
					{
						((LinearLayout)findViewById(R.id.optBlin)).setVisibility(cf.v1.VISIBLE);
						((LinearLayout)findViewById(R.id.optClin)).setVisibility(cf.v1.VISIBLE);
						((TextView)findViewById(R.id.rctxt)).setVisibility(cf.v1.VISIBLE);
					}
					else
					{
						((LinearLayout)findViewById(R.id.optBlin)).setVisibility(cf.v1.GONE);
						((LinearLayout)findViewById(R.id.optClin)).setVisibility(cf.v1.GONE);
						((TextView)findViewById(R.id.rctxt)).setVisibility(cf.v1.GONE);
						clearBchk();
					}
				}
				else
				{
					roofcoveringrdio[i].setChecked(false);
				}
			}
		}
	}
	private void clearBchk()
	{
		 for(int i=0;i<levelBchk.length;i++)
	   	 {
			 levelBchk[i].setChecked(false);		
	   	 }	
	}
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.rclevelB9:/** TYPE CHECKBOX OTHER **/
			  if(levelBchk[levelBchk.length-1].isChecked())
			  { 
				  ((EditText)findViewById(R.id.otherrfcover)).setVisibility(v.VISIBLE);
				  cf.setFocus(((EditText)findViewById(R.id.otherrfcover)));
			  }
			  else
			  {
				  cf.clearother(((EditText)findViewById(R.id.otherrfcover)));
			  }
			  break;
		case R.id.loadcomments:
			/***Call for the comments***/
			cf.findinspectionname(cf.identityval);RC_suboption();
			int len=((EditText)findViewById(R.id.rccomment)).getText().toString().length();			
			if(!tmp.equals(""))
			{
					if(load_comment)
					{
						load_comment=false;
						int loc1[] = new int[2];
						v.getLocationOnScreen(loc1);
						Intent in = cf.loadComments(tmp,loc1);
						in.putExtra("insp_name", cf.inspname);
						in.putExtra("insp_ques", "Roof Cover");
						in.putExtra("length", len);
						in.putExtra("max_length", 500);
						startActivityForResult(in, cf.loadcomment_code);
					}
			}
			else
			{
				cf.ShowToast("Please select the option for Roof Covering.",1);
			}	
			break;
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.save:			
				rcvalue=cf.getselecctedtag_radio(roofcoveringrdio);	
				if(rcvalue.equals(""))
				{
					cf.ShowToast("Please select the option for Roof Covering." , 0);
				}
				else
				{
					if(roofcoveringrdio[1].isChecked())
					{
						levelBvalue= cf.getselected_chk(levelBchk);
						String levelBothrval =(levelBchk[levelBchk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.otherrfcover)).getText().toString():""; 
						levelBvalue+=levelBothrval;
						 if(levelBvalue.equals(""))
						 {
							 cf.ShowToast("Please check atleast one option under Level B (FBC Equivalent) -- Type II or III ." , 0);
						 }
						 else
						 {
							 if(levelBchk[levelBchk.length-1].isChecked())
							 {	 
								 if(levelBothrval.trim().equals("&#40;"))
								 {
									 cf.ShowToast("Please enter the other text for Level B (FBC Equivalent) -- Type II or III ." , 0);
									 cf.setFocus(((EditText)findViewById(R.id.otherrfcover)));
								 }
								 else
								 {
									 InsertRoofCover();	
								 }
							 
							 }
							 else
							 {
								 InsertRoofCover();
							 }
						 }						
					}
					else
					{
						InsertRoofCover();
					}
				}			
			break;
		}
	}
	private void RC_suboption() 
	{
		// TODO Auto-generated method stub
		 for(int i=0;i<roofcoveringrdio.length;i++)
    	 {
			 if(roofcoveringrdio[i].isChecked())
			 {
				tmp = c2_q1[i];
			 }
    	 }
	}
	private void InsertRoofCover() {
		// TODO Auto-generated method stub
		if(((EditText)findViewById(R.id.rccomment)).getText().toString().trim().equals(""))
		{
			cf.ShowToast("Please enter the Roof Covering Comments.",1);
			cf.setFocus(((EditText)findViewById(R.id.rccomment)));
		}
		else
		{
			System.out.println("levelBvalue"+levelBvalue);
			
		roofcovering = rcvalue+"~"+levelBvalue;
		System.out.println("roofcovering"+roofcovering);
		try
		{
			Cursor c1 =cf.SelectTablefunction(cf.Commercial_Wind, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
			if(c1.getCount()==0)
			{
				cf.arr_db.execSQL("INSERT INTO "
						+ cf.Commercial_Wind
						+ " (fld_srid,insp_typeid,fld_roofcoverings,fld_rccomments)"
						+ "VALUES ('"+cf.selectedhomeid+"','"+cf.identityval+"','"+cf.encode(roofcovering)+"','"+cf.encode(((EditText)findViewById(R.id.rccomment)).getText().toString().trim())+"')");
				System.out.println("roofcovering"+"INSERT INTO "
						+ cf.Commercial_Wind
						+ " (fld_srid,insp_typeid,fld_roofcoverings)"
						+ "VALUES ('"+cf.selectedhomeid+"','"+cf.identityval+"','"+cf.encode(roofcovering)+"')");
			}
			else
			{
				System.out.println("UPDATE "
					+ cf.Commercial_Wind
					+ " SET fld_roofcoverings='" + cf.encode(roofcovering) + "',fld_rccomments='"+cf.encode(((EditText)findViewById(R.id.rccomment)).getText().toString().trim())+"'"
					+ " WHERE fld_srid ='"+ cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'");
				
				cf.arr_db.execSQL("UPDATE "
					+ cf.Commercial_Wind
					+ " SET fld_roofcoverings='" + cf.encode(roofcovering) + "',fld_rccomments='"+cf.encode(((EditText)findViewById(R.id.rccomment)).getText().toString().trim())+"'"
					+ " WHERE fld_srid ='"+ cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'");			
			
			}
			cf.ShowToast("Roof Covering saved successfully",1);
			cf.gotoclass(cf.identityval, CommercialRD.class);
		}
		catch(Exception e)
		{
			System.out.println("catch inside "+e.getMessage());
		}
		}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
 		// replaces the default 'Back' button action
 		if (keyCode == KeyEvent.KEYCODE_BACK) {
 			cf.gotoclass(cf.identityval, CommercialWind.class);
 			return true;
 		}
 		
 		return super.onKeyDown(keyCode, event);
 	}
	 /**on activity result for the load comments**/
	 @Override
	 		protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	 		try
	 		{
	 			if(requestCode==cf.loadcomment_code)
	 			{
	 				load_comment=true;
	 				if(resultCode==RESULT_OK)
	 				{
	 					String rccomm = ((EditText)findViewById(R.id.rccomment)).getText().toString();	 
	 					((EditText)findViewById(R.id.rccomment)).setText(rccomm +" "+data.getExtras().getString("Comments"));
		 			}
	 			}
	 		}
	 		catch (Exception e) {
	 			// TODO: handle exception
	 			System.out.println("the erro was "+e.getMessage());
	 		}
	 	}/**on activity result for the load comments ends**/
}