/*
   ************************************************************
   * Project: ALL RISK REPORT
   * Module : Observation3.java
   * Creation History:
      Created By : Gowri on 
   * Modification History:
      Last Modified By : Gowri on 04/04/2013
   ************************************************************  
*/
package idsoft.inspectiondepot.ARR;

import java.lang.reflect.Field;

import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.Observation4.Touch_Listener;
import idsoft.inspectiondepot.ARR.Observation4.checklistenetr;
import idsoft.inspectiondepot.ARR.Observation4.textwatcher;
import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class Observation3 extends Activity{
	CommonFunctions cf;
	RadioGroup obs14rdgval,obs15rdgval,obs16rdgval,obs17rdgval,obs18rdgval,obs19rdgval,
	obs20rdgval,obs21rdgval,obs22rdgval;
	String obs14rdgtxt="",obs15rdgtxt="",obs16rdgtxt="",obs17rdgtxt="",obs18rdgtxt="",
			obs19rdgtxt="",obs20rdgtxt="",obs21rdgtxt="",obs22rdgtxt="",obs14chk_val="",
			obs15chk_val="",obs16chk_val="",obs17chk_val="",obs18chk_val="",obs19chk_val="",
			obs20chk_val="",obs21chk_val="",obs22chk_val="";
	public CheckBox obs14chk[] = new CheckBox[25];
	public CheckBox obs15chk[] = new CheckBox[25];
	public CheckBox obs16chk[] = new CheckBox[25];
	public CheckBox obs17chk[] = new CheckBox[25];
	public CheckBox obs18chk[] = new CheckBox[25];
	public CheckBox obs19chk[] = new CheckBox[25];
	public CheckBox obs20chk[] = new CheckBox[25];
	public CheckBox obs21chk[] = new CheckBox[25];
	public CheckBox obs22chk[] = new CheckBox[25];
	
	public int[] chkbx14yes = {R.id.obs14chk1,R.id.obs14chk2,R.id.obs14chk3,R.id.obs14chk4,R.id.obs14chk5,
			                 R.id.obs14chk6,R.id.obs14chk7,R.id.obs14chk8,R.id.obs14chk9,R.id.obs14chk10,
			                 R.id.obs14chk11,R.id.obs14chk12,R.id.obs14chk13,R.id.obs14chk14,R.id.obs14chk15,
			                 R.id.obs14chk16,R.id.obs14chk17,R.id.obs14chk18,R.id.obs14chk19,R.id.obs14chk20,
			                 R.id.obs14chk21,R.id.obs14chk22,R.id.obs14chk23,R.id.obs14chk24,R.id.obs14chk25};
	public int[] chkbx15yes = {R.id.obs15chk1,R.id.obs15chk2,R.id.obs15chk3,R.id.obs15chk4,R.id.obs15chk5,
            R.id.obs15chk6,R.id.obs15chk7,R.id.obs15chk8,R.id.obs15chk9,R.id.obs15chk10,
            R.id.obs15chk11,R.id.obs15chk12,R.id.obs15chk13,R.id.obs15chk14,R.id.obs15chk15,
            R.id.obs15chk16,R.id.obs15chk17,R.id.obs15chk18,R.id.obs15chk19,R.id.obs15chk20,
            R.id.obs15chk21,R.id.obs15chk22,R.id.obs15chk23,R.id.obs15chk24,R.id.obs15chk25};
	public int[] chkbx16yes = {R.id.obs16chk1,R.id.obs16chk2,R.id.obs16chk3,R.id.obs16chk4,R.id.obs16chk5,
            R.id.obs16chk6,R.id.obs16chk7,R.id.obs16chk8,R.id.obs16chk9,R.id.obs16chk10,
            R.id.obs16chk11,R.id.obs16chk12,R.id.obs16chk13,R.id.obs16chk14,R.id.obs16chk15,
            R.id.obs16chk16,R.id.obs16chk17,R.id.obs16chk18,R.id.obs16chk19,R.id.obs16chk20,
            R.id.obs16chk21,R.id.obs16chk22,R.id.obs16chk23,R.id.obs16chk24,R.id.obs16chk25};
	public int[] chkbx17yes = {R.id.obs17chk1,R.id.obs17chk2,R.id.obs17chk3,R.id.obs17chk4,R.id.obs17chk5,
            R.id.obs17chk6,R.id.obs17chk7,R.id.obs17chk8,R.id.obs17chk9,R.id.obs17chk10,
            R.id.obs17chk11,R.id.obs17chk12,R.id.obs17chk13,R.id.obs17chk14,R.id.obs17chk15,
            R.id.obs17chk16,R.id.obs17chk17,R.id.obs17chk18,R.id.obs17chk19,R.id.obs17chk20,
            R.id.obs17chk21,R.id.obs17chk22,R.id.obs17chk23,R.id.obs17chk24,R.id.obs17chk25};
	public int[] chkbx18yes = {R.id.obs18chk1,R.id.obs18chk2,R.id.obs18chk3,R.id.obs18chk4,R.id.obs18chk5,
            R.id.obs18chk6,R.id.obs18chk7,R.id.obs18chk8,R.id.obs18chk9,R.id.obs18chk10,
            R.id.obs18chk11,R.id.obs18chk12,R.id.obs18chk13,R.id.obs18chk14,R.id.obs18chk15,
            R.id.obs18chk16,R.id.obs18chk17,R.id.obs18chk18,R.id.obs18chk19,R.id.obs18chk20,
            R.id.obs18chk21,R.id.obs18chk22,R.id.obs18chk23,R.id.obs18chk24,R.id.obs18chk25};
	public int[] chkbx19yes = {R.id.obs19chk1,R.id.obs19chk2,R.id.obs19chk3,R.id.obs19chk4,R.id.obs19chk5,
            R.id.obs19chk6,R.id.obs19chk7,R.id.obs19chk8,R.id.obs19chk9,R.id.obs19chk10,
            R.id.obs19chk11,R.id.obs19chk12,R.id.obs19chk13,R.id.obs19chk14,R.id.obs19chk15,
            R.id.obs19chk16,R.id.obs19chk17,R.id.obs19chk18,R.id.obs19chk19,R.id.obs19chk20,
            R.id.obs19chk21,R.id.obs19chk22,R.id.obs19chk23,R.id.obs19chk24,R.id.obs19chk25};
	public int[] chkbx20yes = {R.id.obs20chk1,R.id.obs20chk2,R.id.obs20chk3,R.id.obs20chk4,R.id.obs20chk5,
            R.id.obs20chk6,R.id.obs20chk7,R.id.obs20chk8,R.id.obs20chk9,R.id.obs20chk10,
            R.id.obs20chk11,R.id.obs20chk12,R.id.obs20chk13,R.id.obs20chk14,R.id.obs20chk15,
            R.id.obs20chk16,R.id.obs20chk17,R.id.obs20chk18,R.id.obs20chk19,R.id.obs20chk20,
            R.id.obs20chk21,R.id.obs20chk22,R.id.obs20chk23,R.id.obs20chk24,R.id.obs20chk25};
	public int[] chkbx21yes = {R.id.obs21chk1,R.id.obs21chk2,R.id.obs21chk3,R.id.obs21chk4,R.id.obs21chk5,
            R.id.obs21chk6,R.id.obs21chk7,R.id.obs21chk8,R.id.obs21chk9,R.id.obs21chk10,
            R.id.obs21chk11,R.id.obs21chk12,R.id.obs21chk13,R.id.obs21chk14,R.id.obs21chk15,
            R.id.obs21chk16,R.id.obs21chk17,R.id.obs21chk18,R.id.obs21chk19,R.id.obs21chk20,
            R.id.obs21chk21,R.id.obs21chk22,R.id.obs21chk23,R.id.obs21chk24,R.id.obs21chk25};
	public int[] chkbx22yes = {R.id.obs22chk1,R.id.obs22chk2,R.id.obs22chk3,R.id.obs22chk4,R.id.obs22chk5,
            R.id.obs22chk6,R.id.obs22chk7,R.id.obs22chk8,R.id.obs22chk9,R.id.obs22chk10,
            R.id.obs22chk11,R.id.obs22chk12,R.id.obs22chk13,R.id.obs22chk14,R.id.obs22chk15,
            R.id.obs22chk16,R.id.obs22chk17,R.id.obs22chk18,R.id.obs22chk19,R.id.obs22chk20,
            R.id.obs22chk21,R.id.obs22chk22,R.id.obs22chk23,R.id.obs22chk24,R.id.obs22chk25};
		@Override
	   public void onCreate(Bundle savedInstanceState) {

	        super.onCreate(savedInstanceState);
	        cf=new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);
				
		 	}
	        setContentView(R.layout.sinkobservation3);
	        cf.getDeviceDimensions();
	        
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
			hdr_layout.addView(new HdrOnclickListener(this,1,"Sinkhole Inspection","Observations(14 - 22)",5,0,cf));
			
	        LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
	        mainmenu_layout.setMinimumWidth(cf.wd);
	        mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 5, 0,0,cf));
	        LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 54, 1,0,cf));
			TableRow tblrw = (TableRow)findViewById(R.id.row2);
			tblrw.setMinimumHeight(cf.ht);
			cf.CreateARRTable(54);
			declarations();
			SinkObs3_setvalue();
	}
	  private void SinkObs3_setvalue() {

			// TODO Auto-generated method stub
		  try
			{
			   Cursor Obs3_retrive=cf.SelectTablefunction(cf.SINK_Obs3tbl, " where fld_srid='"+cf.selectedhomeid+"'");
			   if(Obs3_retrive.getCount()>0)
			   {  
				   Obs3_retrive.moveToFirst();
				   ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
				    obs14rdgtxt=Obs3_retrive.getString(Obs3_retrive.getColumnIndex("InteriorWallCrackNoted"));
				   ((RadioButton) obs14rdgval.findViewWithTag(obs14rdgtxt)).setChecked(true);
				    obs14chk_val=cf.decode(Obs3_retrive.getString(Obs3_retrive.getColumnIndex("InteriorWallCrackOption")));
				    cf.setvaluechk1(obs14chk_val,obs14chk,((EditText)findViewById(R.id.obs14_etother)));
	     		   ((EditText)findViewById(R.id.obs14comment)).setText(cf.decode(Obs3_retrive.getString(Obs3_retrive.getColumnIndex("InteriorWallCrackComments"))));
	     		   
	     		   obs15rdgtxt=Obs3_retrive.getString(Obs3_retrive.getColumnIndex("WallSlabCrackNoted"));
				   ((RadioButton) obs15rdgval.findViewWithTag(obs15rdgtxt)).setChecked(true);
				    obs15chk_val=cf.decode(Obs3_retrive.getString(Obs3_retrive.getColumnIndex("WallSlabCrackOption")));
				    cf.setvaluechk1(obs15chk_val,obs15chk,((EditText)findViewById(R.id.obs15_etother)));
	     		   ((EditText)findViewById(R.id.obs15comment)).setText(cf.decode(Obs3_retrive.getString(Obs3_retrive.getColumnIndex("WallSlabCrackComments"))));
	     		 
	     		  obs16rdgtxt=Obs3_retrive.getString(Obs3_retrive.getColumnIndex("InteriorCeilingCrackNoted"));
				   ((RadioButton) obs16rdgval.findViewWithTag(obs16rdgtxt)).setChecked(true);
				   obs16chk_val=cf.decode(Obs3_retrive.getString(Obs3_retrive.getColumnIndex("InteriorCeilingCrackOption")));
				    cf.setvaluechk1(obs16chk_val,obs16chk,((EditText)findViewById(R.id.obs16_etother)));
	     		   ((EditText)findViewById(R.id.obs16comment)).setText(cf.decode(Obs3_retrive.getString(Obs3_retrive.getColumnIndex("InteriorCeilingCrackComments"))));
	     		   
	     		  obs17rdgtxt=Obs3_retrive.getString(Obs3_retrive.getColumnIndex("InteriorFloorCrackNoted"));
				   ((RadioButton) obs17rdgval.findViewWithTag(obs17rdgtxt)).setChecked(true);
				   obs17chk_val=cf.decode(Obs3_retrive.getString(Obs3_retrive.getColumnIndex("InteriorFloorCrackOption")));
				    cf.setvaluechk1(obs17chk_val,obs17chk,((EditText)findViewById(R.id.obs17_etother)));
	     		   ((EditText)findViewById(R.id.obs17comment)).setText(cf.decode(Obs3_retrive.getString(Obs3_retrive.getColumnIndex("InteriorFloorCrackComments"))));
	     		 
	     		  obs18rdgtxt=Obs3_retrive.getString(Obs3_retrive.getColumnIndex("WindowFrameOutofSquare"));
				   ((RadioButton) obs18rdgval.findViewWithTag(obs18rdgtxt)).setChecked(true);
				   obs18chk_val=cf.decode(Obs3_retrive.getString(Obs3_retrive.getColumnIndex("WindowFrameOutofSquareOption")));
				    cf.setvaluechk1(obs18chk_val,obs18chk,((EditText)findViewById(R.id.obs18_etother)));
	     		   ((EditText)findViewById(R.id.obs18comment)).setText(cf.decode(Obs3_retrive.getString(Obs3_retrive.getColumnIndex("WindowFrameOutofSquareComments"))));
	     		 
	     		  obs19rdgtxt=Obs3_retrive.getString(Obs3_retrive.getColumnIndex("FloorSlopingNoted"));
				   ((RadioButton) obs19rdgval.findViewWithTag(obs19rdgtxt)).setChecked(true);
				   obs19chk_val=cf.decode(Obs3_retrive.getString(Obs3_retrive.getColumnIndex("FloorSlopingOption")));
				    cf.setvaluechk1(obs19chk_val,obs19chk,((EditText)findViewById(R.id.obs19_etother)));
	     		   ((EditText)findViewById(R.id.obs19comment)).setText(cf.decode(Obs3_retrive.getString(Obs3_retrive.getColumnIndex("FloorSlopingComments"))));
	     		 
	     		  obs20rdgtxt=Obs3_retrive.getString(Obs3_retrive.getColumnIndex("CrackedGlazingNoted"));
				   ((RadioButton) obs20rdgval.findViewWithTag(obs20rdgtxt)).setChecked(true);
				   obs20chk_val=cf.decode(Obs3_retrive.getString(Obs3_retrive.getColumnIndex("CrackedGlazingOption")));
				    cf.setvaluechk1(obs20chk_val,obs20chk,((EditText)findViewById(R.id.obs20_etother)));
	     		   ((EditText)findViewById(R.id.obs20comment)).setText(cf.decode(Obs3_retrive.getString(Obs3_retrive.getColumnIndex("CrackedGlazingComments"))));
	     		 
	     		  obs21rdgtxt=Obs3_retrive.getString(Obs3_retrive.getColumnIndex("PreviousCorrectiveMeasureNoted"));
				   ((RadioButton) obs21rdgval.findViewWithTag(obs21rdgtxt)).setChecked(true);
				   obs21chk_val=cf.decode(Obs3_retrive.getString(Obs3_retrive.getColumnIndex("PreviousCorrectiveMeasureOption")));
				    cf.setvaluechk1(obs21chk_val,obs21chk,((EditText)findViewById(R.id.obs21_etother)));
	     		   ((EditText)findViewById(R.id.obs21comment)).setText(cf.decode(Obs3_retrive.getString(Obs3_retrive.getColumnIndex("PreviousCorrectiveMeasureComments"))));
	     		 
	     		  obs22rdgtxt=Obs3_retrive.getString(Obs3_retrive.getColumnIndex("BindingDoorOpeningNoted"));
				   ((RadioButton) obs22rdgval.findViewWithTag(obs22rdgtxt)).setChecked(true);
				   obs22chk_val=cf.decode(Obs3_retrive.getString(Obs3_retrive.getColumnIndex("BindingDoorOpeningOption")));
				    cf.setvaluechk1(obs22chk_val,obs22chk,((EditText)findViewById(R.id.obs22_etother)));
	     		   ((EditText)findViewById(R.id.obs22comment)).setText(cf.decode(Obs3_retrive.getString(Obs3_retrive.getColumnIndex("BindingDoorOpeningComments"))));
	     		 
	     		 
			   }
			   else
			   {
				   ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.GONE);
			   }
			}
		    catch (Exception E)
			{
				String strerrorlog="Retrieving Observation3 - Sink";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			}
		}
	private void declarations() {


		// TODO Auto-generated method stub
		 ((EditText)findViewById(R.id.obs14comment)).setOnTouchListener(new Touch_Listener(1));
	     ((EditText)findViewById(R.id.obs14comment)).addTextChangedListener(new textwatcher(1));
	     
	     ((EditText)findViewById(R.id.obs15comment)).setOnTouchListener(new Touch_Listener(2));
	     ((EditText)findViewById(R.id.obs15comment)).addTextChangedListener(new textwatcher(2));
	     
	     ((EditText)findViewById(R.id.obs16comment)).setOnTouchListener(new Touch_Listener(3));
	     ((EditText)findViewById(R.id.obs16comment)).addTextChangedListener(new textwatcher(3));
	     
	     ((EditText)findViewById(R.id.obs17comment)).setOnTouchListener(new Touch_Listener(4));
	     ((EditText)findViewById(R.id.obs17comment)).addTextChangedListener(new textwatcher(4));
	     
	     ((EditText)findViewById(R.id.obs18comment)).setOnTouchListener(new Touch_Listener(5));
	     ((EditText)findViewById(R.id.obs18comment)).addTextChangedListener(new textwatcher(5));
	     
	     ((EditText)findViewById(R.id.obs19comment)).setOnTouchListener(new Touch_Listener(6));
	     ((EditText)findViewById(R.id.obs19comment)).addTextChangedListener(new textwatcher(6));
	     
	     ((EditText)findViewById(R.id.obs20comment)).setOnTouchListener(new Touch_Listener(7));
	     ((EditText)findViewById(R.id.obs20comment)).addTextChangedListener(new textwatcher(7));
	     
	     ((EditText)findViewById(R.id.obs21comment)).setOnTouchListener(new Touch_Listener(8));
	     ((EditText)findViewById(R.id.obs21comment)).addTextChangedListener(new textwatcher(8));
	     
	     ((EditText)findViewById(R.id.obs22comment)).setOnTouchListener(new Touch_Listener(9));
	     ((EditText)findViewById(R.id.obs22comment)).addTextChangedListener(new textwatcher(9));
	     
	     ((EditText)findViewById(R.id.obs14_etother)).setOnTouchListener(new Touch_Listener(10));
	     ((EditText)findViewById(R.id.obs15_etother)).setOnTouchListener(new Touch_Listener(11));
	     ((EditText)findViewById(R.id.obs16_etother)).setOnTouchListener(new Touch_Listener(12));
	     ((EditText)findViewById(R.id.obs17_etother)).setOnTouchListener(new Touch_Listener(13));
	     ((EditText)findViewById(R.id.obs18_etother)).setOnTouchListener(new Touch_Listener(14));
	     ((EditText)findViewById(R.id.obs19_etother)).setOnTouchListener(new Touch_Listener(15));
	     ((EditText)findViewById(R.id.obs20_etother)).setOnTouchListener(new Touch_Listener(16));
	     ((EditText)findViewById(R.id.obs21_etother)).setOnTouchListener(new Touch_Listener(17));
	     ((EditText)findViewById(R.id.obs22_etother)).setOnTouchListener(new Touch_Listener(18));
	     
	     obs14rdgval = (RadioGroup)findViewById(R.id.obs14_rdg);
    	 obs14rdgval.setOnCheckedChangeListener(new checklistenetr(1));
    	 obs15rdgval = (RadioGroup)findViewById(R.id.obs15_rdg);
    	 obs15rdgval.setOnCheckedChangeListener(new checklistenetr(2));
    	 obs16rdgval = (RadioGroup)findViewById(R.id.obs16_rdg);
    	 obs16rdgval.setOnCheckedChangeListener(new checklistenetr(3));
    	 obs17rdgval = (RadioGroup)findViewById(R.id.obs17_rdg);
    	 obs17rdgval.setOnCheckedChangeListener(new checklistenetr(4));
    	 obs18rdgval = (RadioGroup)findViewById(R.id.obs18_rdg);
    	 obs18rdgval.setOnCheckedChangeListener(new checklistenetr(5));
    	 obs19rdgval = (RadioGroup)findViewById(R.id.obs19_rdg);
    	 obs19rdgval.setOnCheckedChangeListener(new checklistenetr(6));
    	 obs20rdgval = (RadioGroup)findViewById(R.id.obs20_rdg);
    	 obs20rdgval.setOnCheckedChangeListener(new checklistenetr(7));
    	 obs21rdgval = (RadioGroup)findViewById(R.id.obs21_rdg);
    	 obs21rdgval.setOnCheckedChangeListener(new checklistenetr(8));
    	 obs22rdgval = (RadioGroup)findViewById(R.id.obs22_rdg);
    	 obs22rdgval.setOnCheckedChangeListener(new checklistenetr(9));
    	 
         for(int i=0;i<25;i++)
    	 {
    		try{
    			obs14chk[i] = (CheckBox)findViewById(chkbx14yes[i]);
    			obs15chk[i] = (CheckBox)findViewById(chkbx15yes[i]);
    			obs16chk[i] = (CheckBox)findViewById(chkbx16yes[i]);
    			obs17chk[i] = (CheckBox)findViewById(chkbx17yes[i]);
    			obs18chk[i] = (CheckBox)findViewById(chkbx18yes[i]);
    			obs19chk[i] = (CheckBox)findViewById(chkbx19yes[i]);
    			obs20chk[i] = (CheckBox)findViewById(chkbx20yes[i]);
    			obs21chk[i] = (CheckBox)findViewById(chkbx21yes[i]);
    			obs22chk[i] = (CheckBox)findViewById(chkbx22yes[i]);
    			
    		}
			catch(Exception e)
			{
				System.out.println("error= "+e.getMessage());
			}
    	 }
  
 		
	}
	  class checklistenetr implements OnCheckedChangeListener
		{
	    	int i;
			public checklistenetr(int i) {
				// TODO Auto-generated constructor stub
			   this.i=i;
			}

			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				   RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
			        // This puts the value (true/false) into the variable
			        boolean isChecked = checkedRadioButton.isChecked();
			        // If the radiobutton that has changed in check state is now checked...
			        if (isChecked)
			        {
			          switch (i) {
						case 1:
							obs14rdgtxt= checkedRadioButton.getText().toString().trim();
							if(obs14rdgtxt.equals("No"))
							{	
								cf.hidekeyboard();((EditText)findViewById(R.id.obs14comment)).setText(R.string.novalueobs);
							    cf.Set_UncheckBox(obs14chk, ((EditText)findViewById(R.id.obs14_etother)));
							    ((TableLayout)findViewById(R.id.obs14yes)).setVisibility(cf.v1.GONE);}
							else if(obs14rdgtxt.equals("Not Applicable")){cf.hidekeyboard();
								((EditText)findViewById(R.id.obs14comment)).setText("");
								cf.Set_UncheckBox(obs14chk, ((EditText)findViewById(R.id.obs14_etother)));
							    ((TableLayout)findViewById(R.id.obs14yes)).setVisibility(cf.v1.GONE);}
							else if(obs14rdgtxt.equals("Yes")){cf.hidekeyboard();
								((EditText)findViewById(R.id.obs14comment)).setText("");
							    ((TableLayout)findViewById(R.id.obs14yes)).setVisibility(cf.v1.VISIBLE);}
						  break;
						case 2:
							obs15rdgtxt= checkedRadioButton.getText().toString().trim();
							if(obs15rdgtxt.equals("No"))
							{
								cf.hidekeyboard();((EditText)findViewById(R.id.obs15comment)).setText(R.string.novalueobs);
							    cf.Set_UncheckBox(obs15chk, ((EditText)findViewById(R.id.obs15_etother)));
							    ((TableLayout)findViewById(R.id.obs15yes)).setVisibility(cf.v1.GONE);}
							else if(obs15rdgtxt.equals("Not Applicable")){cf.hidekeyboard();
								((EditText)findViewById(R.id.obs15comment)).setText("");
								cf.Set_UncheckBox(obs15chk, ((EditText)findViewById(R.id.obs15_etother)));
							    ((TableLayout)findViewById(R.id.obs15yes)).setVisibility(cf.v1.GONE);}
							else if(obs15rdgtxt.equals("Yes")){cf.hidekeyboard();
								((EditText)findViewById(R.id.obs15comment)).setText("");
							    ((TableLayout)findViewById(R.id.obs15yes)).setVisibility(cf.v1.VISIBLE);}
						  break;
						case 3:
							obs16rdgtxt= checkedRadioButton.getText().toString().trim();
							if(obs16rdgtxt.equals("No"))
							{	cf.hidekeyboard();((EditText)findViewById(R.id.obs16comment)).setText(R.string.novalueobs);
							    cf.Set_UncheckBox(obs16chk, ((EditText)findViewById(R.id.obs16_etother)));
							    ((TableLayout)findViewById(R.id.obs16yes)).setVisibility(cf.v1.GONE);}
							else if(obs16rdgtxt.equals("Not Applicable")){cf.hidekeyboard();
								((EditText)findViewById(R.id.obs16comment)).setText("");
								cf.Set_UncheckBox(obs16chk, ((EditText)findViewById(R.id.obs16_etother)));
							    ((TableLayout)findViewById(R.id.obs16yes)).setVisibility(cf.v1.GONE);}
							else if(obs16rdgtxt.equals("Yes")){cf.hidekeyboard();
								((EditText)findViewById(R.id.obs16comment)).setText("");
							    ((TableLayout)findViewById(R.id.obs16yes)).setVisibility(cf.v1.VISIBLE);}
						  break;
						case 4:
							obs17rdgtxt= checkedRadioButton.getText().toString().trim();
							if(obs17rdgtxt.equals("No"))
							{	cf.hidekeyboard();((EditText)findViewById(R.id.obs17comment)).setText(R.string.novalueobs);
							     cf.Set_UncheckBox(obs17chk, ((EditText)findViewById(R.id.obs17_etother)));
							    ((TableLayout)findViewById(R.id.obs17yes)).setVisibility(cf.v1.GONE);}
							else if(obs17rdgtxt.equals("Not Applicable")){cf.hidekeyboard();
								((EditText)findViewById(R.id.obs17comment)).setText("");
								cf.Set_UncheckBox(obs17chk, ((EditText)findViewById(R.id.obs17_etother)));
							    ((TableLayout)findViewById(R.id.obs17yes)).setVisibility(cf.v1.GONE);}
							else if(obs17rdgtxt.equals("Yes")){cf.hidekeyboard();
								((EditText)findViewById(R.id.obs17comment)).setText("");
							    ((TableLayout)findViewById(R.id.obs17yes)).setVisibility(cf.v1.VISIBLE);}
						  break;
						case 5:
							obs18rdgtxt= checkedRadioButton.getText().toString().trim();
							if(obs18rdgtxt.equals("No"))
							{	cf.hidekeyboard();((EditText)findViewById(R.id.obs18comment)).setText(R.string.novalueobs);
							    cf.Set_UncheckBox(obs18chk, ((EditText)findViewById(R.id.obs18_etother)));
							    ((TableLayout)findViewById(R.id.obs18yes)).setVisibility(cf.v1.GONE);}
							else if(obs18rdgtxt.equals("Not Applicable")){cf.hidekeyboard();
								((EditText)findViewById(R.id.obs18comment)).setText("");
								cf.Set_UncheckBox(obs18chk, ((EditText)findViewById(R.id.obs18_etother)));
							    ((TableLayout)findViewById(R.id.obs18yes)).setVisibility(cf.v1.GONE);}
							else if(obs18rdgtxt.equals("Yes")){cf.hidekeyboard();
								((EditText)findViewById(R.id.obs18comment)).setText("");
							    ((TableLayout)findViewById(R.id.obs18yes)).setVisibility(cf.v1.VISIBLE);}
						  break;
						case 6:
							obs19rdgtxt= checkedRadioButton.getText().toString().trim();
							if(obs19rdgtxt.equals("No"))
							{	cf.hidekeyboard();((EditText)findViewById(R.id.obs19comment)).setText(R.string.novalueobs);
							    cf.Set_UncheckBox(obs19chk, ((EditText)findViewById(R.id.obs19_etother)));
							    ((TableLayout)findViewById(R.id.obs19yes)).setVisibility(cf.v1.GONE);}
							else if(obs19rdgtxt.equals("Not Applicable")){cf.hidekeyboard();
								((EditText)findViewById(R.id.obs19comment)).setText("");
								 cf.Set_UncheckBox(obs19chk, ((EditText)findViewById(R.id.obs19_etother)));
							    ((TableLayout)findViewById(R.id.obs19yes)).setVisibility(cf.v1.GONE);}
							else if(obs19rdgtxt.equals("Yes")){cf.hidekeyboard();
								((EditText)findViewById(R.id.obs19comment)).setText("");
							    ((TableLayout)findViewById(R.id.obs19yes)).setVisibility(cf.v1.VISIBLE);}
						  break;
						case 7:
							obs20rdgtxt= checkedRadioButton.getText().toString().trim();
							if(obs20rdgtxt.equals("No"))
							{	cf.hidekeyboard();((EditText)findViewById(R.id.obs20comment)).setText(R.string.novalueobs);
							    cf.Set_UncheckBox(obs20chk, ((EditText)findViewById(R.id.obs20_etother)));
							    ((TableLayout)findViewById(R.id.obs20yes)).setVisibility(cf.v1.GONE);}
							else if(obs20rdgtxt.equals("Not Applicable")){cf.hidekeyboard();
								((EditText)findViewById(R.id.obs20comment)).setText("");
								cf.Set_UncheckBox(obs20chk, ((EditText)findViewById(R.id.obs20_etother)));
							    ((TableLayout)findViewById(R.id.obs20yes)).setVisibility(cf.v1.GONE);}
							else if(obs20rdgtxt.equals("Yes")){cf.hidekeyboard();
								((EditText)findViewById(R.id.obs20comment)).setText("");
							    ((TableLayout)findViewById(R.id.obs20yes)).setVisibility(cf.v1.VISIBLE);}
						  break;
						case 8:
							obs21rdgtxt= checkedRadioButton.getText().toString().trim();
							if(obs21rdgtxt.equals("No"))
							{	cf.hidekeyboard();((EditText)findViewById(R.id.obs21comment)).setText(R.string.novalueobs);
							    cf.Set_UncheckBox(obs21chk, ((EditText)findViewById(R.id.obs21_etother)));
							    ((TableLayout)findViewById(R.id.obs21yes)).setVisibility(cf.v1.GONE);}
							else if(obs21rdgtxt.equals("Not Applicable")){cf.hidekeyboard();
								((EditText)findViewById(R.id.obs21comment)).setText("");
								cf.Set_UncheckBox(obs21chk, ((EditText)findViewById(R.id.obs21_etother)));
							    ((TableLayout)findViewById(R.id.obs21yes)).setVisibility(cf.v1.GONE);}
							else if(obs21rdgtxt.equals("Yes")){cf.hidekeyboard();
								((EditText)findViewById(R.id.obs21comment)).setText("");
							    ((TableLayout)findViewById(R.id.obs21yes)).setVisibility(cf.v1.VISIBLE);}
						  break;
						case 9:
							obs22rdgtxt= checkedRadioButton.getText().toString().trim();
							if(obs22rdgtxt.equals("No"))
							{	cf.hidekeyboard();((EditText)findViewById(R.id.obs22comment)).setText(R.string.novalueobs);
							     cf.Set_UncheckBox(obs22chk, ((EditText)findViewById(R.id.obs22_etother)));
							    ((TableLayout)findViewById(R.id.obs22yes)).setVisibility(cf.v1.GONE);}
							else if(obs22rdgtxt.equals("Not Applicable")){cf.hidekeyboard();
								((EditText)findViewById(R.id.obs22comment)).setText("");
								cf.Set_UncheckBox(obs22chk, ((EditText)findViewById(R.id.obs22_etother)));
							    ((TableLayout)findViewById(R.id.obs22yes)).setVisibility(cf.v1.GONE);}
							else if(obs22rdgtxt.equals("Yes")){cf.hidekeyboard();
								((EditText)findViewById(R.id.obs22comment)).setText("");
							    ((TableLayout)findViewById(R.id.obs22yes)).setVisibility(cf.v1.VISIBLE);}
						  break;
						default:
							break;
						}
			        }
			}
        }
	  class Touch_Listener implements OnTouchListener

	    {
	    	   public int type;
	    	   Touch_Listener(int type)
	    		{
	    			this.type=type;
	    			
	    		}
	    	    @Override
	    		public boolean onTouch(View v, MotionEvent event) {

	    			// TODO Auto-generated method stub
	    	    	((EditText) findViewById(v.getId())).setFocusableInTouchMode(true);
			    	((EditText) findViewById(v.getId())).requestFocus();
	    			return false;
	    	    }
	    }
	  class textwatcher implements TextWatcher



	    {
	         public int type;
	         textwatcher(int type)
	    	{
	    		this.type=type;
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {


	    		// TODO Auto-generated method stub
	    		if(this.type==1)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs14_tv_type1),250);
	    			if(s.toString().startsWith(" "))
	    			{
	    			     ((EditText)findViewById(R.id.obs14comment)).setText("");
	    			}
	    		}
	    		else if(this.type==2)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs15_tv_type1),250);
	    			if(s.toString().startsWith(" "))
	    			{
	    			     ((EditText)findViewById(R.id.obs15comment)).setText("");
	    			}
	    		}
	    		else if(this.type==3)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs16_tv_type1),250); 
	    			if(s.toString().startsWith(" "))
	    			{
	    			     ((EditText)findViewById(R.id.obs16comment)).setText("");
	    			}
	    		}
	    		else if(this.type==4)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs17_tv_type1),250); 
	    			if(s.toString().startsWith(" "))
	    			{
	    			     ((EditText)findViewById(R.id.obs17comment)).setText("");
	    			}
	    		}
	    		else if(this.type==5)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs18_tv_type1),250);
	    			if(s.toString().startsWith(" "))
	    			{
	    			     ((EditText)findViewById(R.id.obs18comment)).setText("");
	    			}
	    		}
	    		else if(this.type==6)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs19_tv_type1),250); 
	    			if(s.toString().startsWith(" "))
	    			{
	    			     ((EditText)findViewById(R.id.obs19comment)).setText("");
	    			}
	    		}
	    		else if(this.type==7)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs20_tv_type1),250); 
	    			if(s.toString().startsWith(" "))
	    			{
	    			     ((EditText)findViewById(R.id.obs20comment)).setText("");
	    			}
	    		}
	    		else if(this.type==8)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs21_tv_type1),250);
	    			if(s.toString().startsWith(" "))
	    			{
	    			     ((EditText)findViewById(R.id.obs21comment)).setText("");
	    			}
	    		}
	    		else if(this.type==9)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs22_tv_type1),250);
	    			if(s.toString().startsWith(" "))
	    			{
	    			     ((EditText)findViewById(R.id.obs22comment)).setText("");
	    			}
	    		}
	    		
	    	}

	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,

	    			int after) {
	    		// TODO Auto-generated method stub
	     	}

	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,

	    			int count) {
	    		// TODO Auto-generated method stub
	    	}
	   }	
	public void clicker(View v)

      {
	    	switch(v.getId())
	    	{
	    	case R.id.hme:
	    		cf.gohome();
	    		break;
	    	case R.id.save:
	    		if(obs14rdgtxt.equals(""))
	    		{
	    			cf.ShowToast("Please select the option for Question No.14.", 0);
	    		}
	    		else
	    		{
	    			 if(obs14rdgtxt.equals("Yes"))
					 {
	    				 obs14chk_val= cf.getselected_chk(obs14chk);
	    		    	 String chk_Value14other =(obs14chk[obs14chk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.obs14_etother)).getText().toString():""; // append the other text value in to the selected option
	    		    	 obs14chk_val+=chk_Value14other;
	    		    	 if(obs14chk_val.equals(""))
						 {
							 cf.ShowToast("Please select Observations for Question No.14." , 0);
						 }
	    		    	 else
	    		    	 {
	    		    		 if(obs14chk[obs14chk.length-1].isChecked())
							 {
								 if(chk_Value14other.trim().equals("&#40;"))
								 {
									 cf.ShowToast("Please enter the Other text for Question No.14.", 0);
									 cf.setFocus(((EditText)findViewById(R.id.obs14_etother)));
								 }
								 else
								 {
									chk_obs14comments();	
								 }
							 }
							 else
							 {
								 chk_obs14comments();
							 }
	    		    	 }
					 }
	    			 else
	    			 {
	    				 chk_obs14comments();
	    			 }
	    		}
	    		break;
	    	case R.id.obs14chk25: /** CHECK BOX - OTHER 14 **/
	    		 if(obs14chk[24].isChecked())
				  {
	    			  ((EditText)findViewById(R.id.obs14_etother)).setVisibility(v.VISIBLE);
	    			  ((EditText)findViewById(R.id.obs14_etother)).requestFocus();
				  }
				  else
				  {
					  cf.clearother(((EditText)findViewById(R.id.obs14_etother)));
				  }
				break;
	    	case R.id.obs15chk25: /** CHECK BOX - OTHER 15 **/
	    		if(obs15chk[24].isChecked())
	    		{  ((EditText)findViewById(R.id.obs15_etother)).setVisibility(v.VISIBLE);
	    		  ((EditText)findViewById(R.id.obs15_etother)).requestFocus();}
				else
					cf.clearother(((EditText)findViewById(R.id.obs15_etother)));
			
	    		break;
	    	case R.id.obs16chk25: /** CHECK BOX - OTHER 16 **/
	    		if(obs16chk[24].isChecked()){
				  ((EditText)findViewById(R.id.obs16_etother)).setVisibility(v.VISIBLE);
				  ((EditText)findViewById(R.id.obs16_etother)).requestFocus();}
				else
					cf.clearother(((EditText)findViewById(R.id.obs16_etother)));
			
	    		break;
	    	case R.id.obs17chk25: /** CHECK BOX - OTHER 17 **/
	    		if(obs17chk[24].isChecked()){
				  ((EditText)findViewById(R.id.obs17_etother)).setVisibility(v.VISIBLE);
	    	    	((EditText)findViewById(R.id.obs17_etother)).requestFocus();}
				else
					cf.clearother(((EditText)findViewById(R.id.obs17_etother)));
	    		break;
	    	case R.id.obs18chk25: /** CHECK BOX - OTHER 18 **/
	    		if(obs18chk[24].isChecked()){
				  ((EditText)findViewById(R.id.obs18_etother)).setVisibility(v.VISIBLE);
				  ((EditText)findViewById(R.id.obs18_etother)).requestFocus();}
				else
					cf.clearother(((EditText)findViewById(R.id.obs18_etother)));
			
	    		break;
	    	case R.id.obs19chk25: /** CHECK BOX - OTHER 19 **/
	    		if(obs19chk[24].isChecked()){
				  ((EditText)findViewById(R.id.obs19_etother)).setVisibility(v.VISIBLE);
				  ((EditText)findViewById(R.id.obs19_etother)).requestFocus();}
				else
					cf.clearother(((EditText)findViewById(R.id.obs19_etother)));
			
	    		break;
	    	case R.id.obs20chk25: /** CHECK BOX - OTHER 20 **/
	    		if(obs20chk[24].isChecked()){
				  ((EditText)findViewById(R.id.obs20_etother)).setVisibility(v.VISIBLE);
				  ((EditText)findViewById(R.id.obs20_etother)).requestFocus();}
				else
					cf.clearother(((EditText)findViewById(R.id.obs20_etother)));
			
	    		break;
	    	case R.id.obs21chk25: /** CHECK BOX - OTHER 21 **/
	    		if(obs21chk[24].isChecked()){
				  ((EditText)findViewById(R.id.obs21_etother)).setVisibility(v.VISIBLE);
				  ((EditText)findViewById(R.id.obs21_etother)).requestFocus();}
				else
					cf.clearother(((EditText)findViewById(R.id.obs21_etother)));
			
	    		break;
	    	case R.id.obs22chk25: /** CHECK BOX - OTHER 22 **/
	    		if(obs22chk[24].isChecked()){
				  ((EditText)findViewById(R.id.obs22_etother)).setVisibility(v.VISIBLE);
				  ((EditText)findViewById(R.id.obs22_etother)).requestFocus();}
				else
					cf.clearother(((EditText)findViewById(R.id.obs22_etother)));
			
	    		break;
			  case R.id.obs14_help: /** HELP FOR OBSERVATION14 **/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>Inspectors must analyze " +
				  		"all interior areas, examining all visible wall surfaces looking for " +
				  		"evidence or clues of any settlement or cracking associated with " +
				  		"shrinkage, sinkhole or other structural issue.</p><p>Interior wall " +
				  		"cracks relating to serious structural settlement typically can be found " +
				  		"in a diagonal flashing above openings along the drywall.  Inspectors " +
				  		"should pay close attention in these areas for displacement of adjoining " +
				  		"floor ceiling areas to determine if any previous cracking or repairs " +
				  		"were carried out, including recent painting.</p><p>Inspectors should " +
				  		"comment and photograph all areas where cracking is noted during the " +
				  		"internal inspection.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
						  break;
			  case R.id.obs15_help: /** HELP FOR OBSERVATION15 **/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>Inspectors need to pay " +
				  		"close attention to all visible and accessible areas of wall and floor " +
				  		"junctions to determine if any separation or movement has occurred " +
				  		"between the slab and the main exterior and interior wall " +
				  		"structures.</p><p>Slab separation from the wall structure is indicative " +
				  		"of settlement of some nature and must be documented and photographed by " +
				  		"all inspectors.  Limitations to this inspection should be clearly " +
				  		"outlined, particularly where carpets, furniture and other " +
				  		"owners\'; belongings are present.</p><p>Inspectors should separate " +
				  		"typical shrinkage and separation cracks along the baseboards  to that of " +
				  		" settling slabs or building movement.</p><p>Where any separation is " +
				  		"noted, Inspectors are required to photograph using a 4\" spirit " +
				  		"level, the floor area involved clearly identifying on the photograph the " +
				  		"4\"; spirit level and its findings in relation to a level and " +
				  		"plumb floor.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
						  break;
			  case R.id.obs16_help: /** HELP FOR OBSERVATION16 **/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>Inspectors need to pay " +
				  		"attention to the ceiling and wall junctions throughout the interior " +
				  		"survey.  Separation between ceiling and walls is indicative of potential " +
				  		"movement and should be photographed and recorded.  Where noted, ceiling " +
				  		"and wall separation is required to be photographed using a 4\" " +
				  		"spirit level, with the 4\" spirit level in place on the " +
				  		"photograph provided.  Ample commentary should be made to ensure auditors " +
				  		"and underwriters are fully versed on the severity of the conditions " +
				  		"found.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
						  break;
			  case R.id.obs17_help: /** HELP FOR OBSERVATION17 **/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>While most of the floor " +
				  		"areas will in essence be covered with floor covering, Inspectors need to " +
				  		"pay particularly attention to areas of the floor that are accessible or " +
				  		"visible such as the garage floor, tiled flooring or linoleum covered " +
				  		"areas.  Floor cracking typically is evident along the grout joints, can " +
				  		"be seen under linoleum-type finishes and visible to unfinished areas " +
				  		"such as garages.</p><p>Inspectors should photograph and document any " +
				  		"cracking evident, ensuring that a 4\" spirit level is included " +
				  		"within the photographs provided.</p><p>Inspectors should follow the line " +
				  		"of cracking in any area to the exterior wall to determine if any " +
				  		"corresponding cracks are evident on the exterior foundation wall.  The " +
				  		"examination of the interior drywall at the crack termination with the " +
				  		"exterior walls or surrounding locations should be conducted and reported " +
				  		"within the report.</p><p>Inspectors should comment and photograph all " +
				  		"relevant findings.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
						  break;
			  case R.id.obs18_help: /** HELP FOR OBSERVATION18 **/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>Inspectors need to pay close attention to all openings and accessible windows, " +
				  		"doors, frames and associated hardware to determine if any openings, frames, or hardware is damaged " +
				  		"as a result of compression or abnormal settlement from sinkhole or " +
				  		"other settlement issues</p><p>All areas found should be properly photographed " +
				  		"and commented.</p><p>Photographs should be provided with at least 1 photograph of " +
				  		"windows and openings that have been analyzed using a small torpedo level ensuring findings " +
				  		"are clearly depicted within the picture annotations and comment area.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
						  break;
			  case R.id.obs19_help: /** HELP FOR OBSERVATION19 **/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>Inspectors need to pay " +
				  		"close attention as they survey the interior of the home for any evidence " +
				  		"of uneven flooring or out of square flooring.</p><p>At least one " +
				  		"photograph will be required from the inspector of a floor area where a " +
				  		"4\" spirit level was used, clearly depicting the 4\" " +
				  		"spirit level and slope of the floor.</p><p>Any adverse situations should " +
				  		"be properly cataloged, photographed, annotated and commented on.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
						  break;
			  case R.id.obs20_help: /** HELP FOR OBSERVATION20 **/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>Inspectors are required " +
				  		"to identify and record all cracked glazing and clearly state within the " +
				  		"report that the cracked glazing is a result of impact or other normal " +
				  		"occurrence.</p><p>Settlement or sinkhole activities relating to glazing, " +
				  		"cracked glass found which is associated to compression or settlement as " +
				  		"a result of abnormal structural activity needs to be " +
				  		"reported.</p><p>Compression breakage will have other ancillary visible " +
				  		"issues such as damaged or buckled framing, cracking around openings, " +
				  		"binding windows, etc. which should all be analyzed and reported on as " +
				  		"part of the inspection service if cracked glazing is " +
				  		"found.</p><p>Cracked glazing associated with normal wear and tear, will " +
				  		"also need to be recorded by the inspector, in addition to confirming " +
				  		"that all openings were working properly and no other issues were " +
				  		"present.</p><p>Photographs are required to back up the verification " +
				  		"process.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
						  break;
			  case R.id.obs21_help: /** HELP FOR OBSERVATION21 **/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>Inspectors, as part of " +
				  		"this inspection should attempt to identify any previous structural " +
				  		"repairs or issues that have occurred and report accordingly.</p><p>Any " +
				  		"evidence of structural repairs, rebuilding, extensive cracking or " +
				  		"remodeling should all be recorded and identified.  The policyholder " +
				  		"should also be questioned as to the extent of repairs or any other " +
				  		"issues that were conducted over the history of the home.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
						  break;
			  case R.id.obs22_help: /** HELP FOR OBSERVATION22 **/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>Any evidence of " +
				  		"settlement around window openings requires inspectors to open and " +
				  		"operate all doors and other hardware during the inspection to confirm " +
				  		"proper operation.  Any separation, binding or large gaps present or " +
				  		"found during the inspection should be properly photographed and " +
				  		"recorded.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
						  break;
			  case R.id.shwobs14:
				  hidelayouts();
				  ((ImageView)findViewById(R.id.shwobs14)).setVisibility(cf.v1.GONE);
				  ((ImageView)findViewById(R.id.hdobs14)).setVisibility(cf.v1.VISIBLE);
				  ((LinearLayout)findViewById(R.id.obs14lin)).setVisibility(cf.v1.VISIBLE);
				  ((TextView)findViewById(R.id.fsttxt)).requestFocus();
				  break;
			case R.id.hdobs14:
				((ImageView)findViewById(R.id.shwobs14)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdobs14)).setVisibility(cf.v1.GONE);
				((LinearLayout)findViewById(R.id.obs14lin)).setVisibility(cf.v1.GONE);
				break;
			  case R.id.shwobs15:
				  hidelayouts();
				  ((ImageView)findViewById(R.id.shwobs15)).setVisibility(cf.v1.GONE);
				  ((ImageView)findViewById(R.id.hdobs15)).setVisibility(cf.v1.VISIBLE);
				  ((LinearLayout)findViewById(R.id.obs15lin)).setVisibility(cf.v1.VISIBLE);
				  ((TextView)findViewById(R.id.sectxt)).requestFocus();
				  break;
			  case R.id.hdobs15:
					((ImageView)findViewById(R.id.shwobs15)).setVisibility(cf.v1.VISIBLE);
					((ImageView)findViewById(R.id.hdobs15)).setVisibility(cf.v1.GONE);
					((LinearLayout)findViewById(R.id.obs15lin)).setVisibility(cf.v1.GONE);
					break;
			  case R.id.shwobs16:
				  hidelayouts();
				  ((ImageView)findViewById(R.id.shwobs16)).setVisibility(cf.v1.GONE);
				  ((ImageView)findViewById(R.id.hdobs16)).setVisibility(cf.v1.VISIBLE);
				  ((LinearLayout)findViewById(R.id.obs16lin)).setVisibility(cf.v1.VISIBLE);
				  ((TextView)findViewById(R.id.thdtxt)).requestFocus();
				  break;
			  case R.id.hdobs16:
					((ImageView)findViewById(R.id.shwobs16)).setVisibility(cf.v1.VISIBLE);
					((ImageView)findViewById(R.id.hdobs16)).setVisibility(cf.v1.GONE);
					((LinearLayout)findViewById(R.id.obs16lin)).setVisibility(cf.v1.GONE);
					break;
			  case R.id.shwobs17:
				  hidelayouts();
				  ((ImageView)findViewById(R.id.shwobs17)).setVisibility(cf.v1.GONE);
				  ((ImageView)findViewById(R.id.hdobs17)).setVisibility(cf.v1.VISIBLE);
				  ((LinearLayout)findViewById(R.id.obs17lin)).setVisibility(cf.v1.VISIBLE);
				  ((TextView)findViewById(R.id.fourtxt)).requestFocus();
				  break;
			  case R.id.hdobs17:
					((ImageView)findViewById(R.id.shwobs17)).setVisibility(cf.v1.VISIBLE);
					((ImageView)findViewById(R.id.hdobs17)).setVisibility(cf.v1.GONE);
					((LinearLayout)findViewById(R.id.obs17lin)).setVisibility(cf.v1.GONE);
					break;
			  case R.id.shwobs18:
				  hidelayouts();
				  ((ImageView)findViewById(R.id.shwobs18)).setVisibility(cf.v1.GONE);
				  ((ImageView)findViewById(R.id.hdobs18)).setVisibility(cf.v1.VISIBLE);
				  ((LinearLayout)findViewById(R.id.obs18lin)).setVisibility(cf.v1.VISIBLE);
				  ((TextView)findViewById(R.id.fivtxt)).requestFocus();
				  break;
			  case R.id.hdobs18:
					((ImageView)findViewById(R.id.shwobs18)).setVisibility(cf.v1.VISIBLE);
					((ImageView)findViewById(R.id.hdobs18)).setVisibility(cf.v1.GONE);
					((LinearLayout)findViewById(R.id.obs18lin)).setVisibility(cf.v1.GONE);
					break;
			  case R.id.shwobs19:
				  hidelayouts();
				  ((ImageView)findViewById(R.id.shwobs19)).setVisibility(cf.v1.GONE);
				  ((ImageView)findViewById(R.id.hdobs19)).setVisibility(cf.v1.VISIBLE);
				  ((LinearLayout)findViewById(R.id.obs19lin)).setVisibility(cf.v1.VISIBLE);
				  ((TextView)findViewById(R.id.sixtxt)).requestFocus();
				  break;
			  case R.id.hdobs19:
					((ImageView)findViewById(R.id.shwobs19)).setVisibility(cf.v1.VISIBLE);
					((ImageView)findViewById(R.id.hdobs19)).setVisibility(cf.v1.GONE);
					((LinearLayout)findViewById(R.id.obs19lin)).setVisibility(cf.v1.GONE);
					break;
			  case R.id.shwobs20:
				  hidelayouts();
				  ((ImageView)findViewById(R.id.shwobs20)).setVisibility(cf.v1.GONE);
				  ((ImageView)findViewById(R.id.hdobs20)).setVisibility(cf.v1.VISIBLE);
				  ((LinearLayout)findViewById(R.id.obs20lin)).setVisibility(cf.v1.VISIBLE);
				  ((TextView)findViewById(R.id.sevtxt)).requestFocus();
				  break;
			  case R.id.hdobs20:
					((ImageView)findViewById(R.id.shwobs20)).setVisibility(cf.v1.VISIBLE);
					((ImageView)findViewById(R.id.hdobs20)).setVisibility(cf.v1.GONE);
					((LinearLayout)findViewById(R.id.obs20lin)).setVisibility(cf.v1.GONE);
					break;
			  case R.id.shwobs21:
				  hidelayouts();
				  ((ImageView)findViewById(R.id.shwobs21)).setVisibility(cf.v1.GONE);
				  ((ImageView)findViewById(R.id.hdobs21)).setVisibility(cf.v1.VISIBLE);
				  ((LinearLayout)findViewById(R.id.obs21lin)).setVisibility(cf.v1.VISIBLE);
				  ((TextView)findViewById(R.id.eigtxt)).requestFocus();
				  break;
			  case R.id.hdobs21:
					((ImageView)findViewById(R.id.shwobs21)).setVisibility(cf.v1.VISIBLE);
					((ImageView)findViewById(R.id.hdobs21)).setVisibility(cf.v1.GONE);
					((LinearLayout)findViewById(R.id.obs21lin)).setVisibility(cf.v1.GONE);
					break;
			  case R.id.shwobs22:
				  hidelayouts();
				  ((ImageView)findViewById(R.id.shwobs22)).setVisibility(cf.v1.GONE);
				  ((ImageView)findViewById(R.id.hdobs22)).setVisibility(cf.v1.VISIBLE);
				  ((LinearLayout)findViewById(R.id.obs22lin)).setVisibility(cf.v1.VISIBLE);
				  ((TextView)findViewById(R.id.nintxt)).requestFocus();
				  break;
			  case R.id.hdobs22:
					((ImageView)findViewById(R.id.shwobs22)).setVisibility(cf.v1.VISIBLE);
					((ImageView)findViewById(R.id.hdobs22)).setVisibility(cf.v1.GONE);
					((LinearLayout)findViewById(R.id.obs22lin)).setVisibility(cf.v1.GONE);
					break;
			}
	    }
	private void chk_obs14comments() {
		// TODO Auto-generated method stub
		if(((EditText)findViewById(R.id.obs14comment)).getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.14 */
		{
			cf.ShowToast("Please enter the comments for Question No.14.", 0);
			cf.setFocus(((EditText)findViewById(R.id.obs14comment)));
		}
		else
		{
			if(obs15rdgtxt.equals(""))
    		{
    			cf.ShowToast("Please select the option for Question No.15.", 0);
    		}
    		else
    		{
    			 if(obs15rdgtxt.equals("Yes"))
				 {
    				 obs15chk_val= cf.getselected_chk(obs15chk);
    		    	 String chk_Value15other =(obs15chk[obs15chk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.obs15_etother)).getText().toString():""; // append the other text value in to the selected option
    		    	 obs15chk_val+=chk_Value15other;
    		    	 if(obs15chk_val.equals(""))
					 {
						 cf.ShowToast("Please select the Observations for Question No.15." , 0);
					 }
    		    	 else
    		    	 {
    		    		 if(obs15chk[obs15chk.length-1].isChecked())
						 {
							 if(chk_Value15other.trim().equals("&#40;"))
							 {
								 cf.ShowToast("Please enter the Other text for Question No.15 Observation", 0);
								 cf.setFocus(((EditText)findViewById(R.id.obs15_etother)));
							 }
							 else
							 {
								 chk_obs15comments();	
							 }
						 }
						 else
						 {
							 chk_obs15comments();
						 }
    		    	 }
				 }
    			 else
    			 {
    				 chk_obs15comments();
    			 }
    		}
		}
	}
	private void chk_obs15comments() {

		// TODO Auto-generated method stub
		if(((EditText)findViewById(R.id.obs15comment)).getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.15 */
		{
			cf.ShowToast("Please enter the comments for Question No.15.", 0);
			cf.setFocus(((EditText)findViewById(R.id.obs15comment)));
		}
		else
		{
			if(obs16rdgtxt.equals(""))
    		{
    			cf.ShowToast("Please select the option for Question No.16.", 0);
    		}
    		else
    		{
    			 if(obs16rdgtxt.equals("Yes"))
				 {
    				 obs16chk_val= cf.getselected_chk(obs16chk);
    		    	 String chk_Value16other =(obs16chk[obs16chk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.obs16_etother)).getText().toString():""; // append the other text value in to the selected option
    		    	 obs16chk_val+=chk_Value16other;
    		    	 if(obs16chk_val.equals(""))
					 {
						 cf.ShowToast("Please select the Observations for Question No.16." , 0);
					 }
    		    	 else
    		    	 {
    		    		 if(obs16chk[obs16chk.length-1].isChecked())
						 {
							 if(chk_Value16other.trim().equals("&#40;"))
							 {
								 cf.ShowToast("Please enter the Other text for Question No.16.", 0);
								 cf.setFocus(((EditText)findViewById(R.id.obs16_etother)));
							 }
							 else
							 {
								chk_obs16comments();	
							 }
						 }
						 else
						 {
							 chk_obs16comments();
						 }
    		    	 }
				 }
    			 else
    			 {
    				 chk_obs16comments();
    			 }
    		}
		}
	}
	private void chk_obs16comments() {
		// TODO Auto-generated method stub
		if(((EditText)findViewById(R.id.obs16comment)).getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.16 */
		{
			cf.ShowToast("Please enter the comments for Question No.16.", 0);
			cf.setFocus(((EditText)findViewById(R.id.obs16comment)));
		}
		else
		{
			if(obs17rdgtxt.equals(""))
    		{
    			cf.ShowToast("Please select the option for Question No.17.", 0);
    		}
    		else
    		{
    			 if(obs17rdgtxt.equals("Yes"))
				 {
    				 obs17chk_val= cf.getselected_chk(obs17chk);
    		    	 String chk_Value17other =(obs17chk[obs17chk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.obs17_etother)).getText().toString():""; // append the other text value in to the selected option
    		    	 obs17chk_val+=chk_Value17other;
    		    	 if(obs17chk_val.equals(""))
					 {
						 cf.ShowToast("Please select the Observations for Question No.17." , 0);
					 }
    		    	 else
    		    	 {
    		    		 if(obs17chk[obs17chk.length-1].isChecked())
						 {
							 if(chk_Value17other.trim().equals("&#40;"))
							 {
								 cf.ShowToast("Please enter the Other text for Question No.17 Observation", 0);
								 cf.setFocus(((EditText)findViewById(R.id.obs17_etother)));
							 }
							 else
							 {
								chk_obs17comments();	
							 }
						 }
						 else
						 {
							 chk_obs17comments();
						 }
    		    	 }
				 }
    			 else
    			 {
    				 chk_obs17comments();
    			 }
    		}
		}
	}
	private void chk_obs17comments() {
		// TODO Auto-generated method stub
		if(((EditText)findViewById(R.id.obs17comment)).getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.17 */
		{
			cf.ShowToast("Please enter the comments for Question No.17.", 0);
			cf.setFocus(((EditText)findViewById(R.id.obs17comment)));
		}
		else
		{
			if(obs18rdgtxt.equals(""))
    		{
    			cf.ShowToast("Please select the option for Question No.18.", 0);
    		}
    		else
    		{
    			 if(obs18rdgtxt.equals("Yes"))
				 {
    				 obs18chk_val= cf.getselected_chk(obs18chk);
    		    	 String chk_Value18other =(obs18chk[obs18chk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.obs18_etother)).getText().toString():""; // append the other text value in to the selected option
    		    	 obs18chk_val+=chk_Value18other;
    		    	 if(obs18chk_val.equals(""))
					 {
						 cf.ShowToast("Please select the Observtaions for Question No.18." , 0);
					 }
    		    	 else
    		    	 {
    		    		 if(obs18chk[obs18chk.length-1].isChecked())
						 {
							 if(chk_Value18other.trim().equals("&#40;"))
							 {
								 cf.ShowToast("Please enter the Other text for Question No.18.", 0);
								 cf.setFocus(((EditText)findViewById(R.id.obs18_etother)));
							 }
							 else
							 {
								chk_obs18comments();	
							 }
						 }
						 else
						 {
							 chk_obs18comments();
						 }
    		    	 }
				 }
    			 else
    			 {
    				 chk_obs18comments();
    			 }
    		}
		}
	}
	private void chk_obs18comments() {
		// TODO Auto-generated method stub
		if(((EditText)findViewById(R.id.obs18comment)).getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.18 */
		{
			cf.ShowToast("Please enter the comments for Question No.18.", 0);
			cf.setFocus(((EditText)findViewById(R.id.obs18comment)));
		}
		else
		{
			if(obs19rdgtxt.equals(""))
    		{
    			cf.ShowToast("Please select the option for Question No.19.", 0);
    		}
    		else
    		{
    			 if(obs19rdgtxt.equals("Yes"))
				 {
    				 obs19chk_val= cf.getselected_chk(obs19chk);
    		    	 String chk_Value19other =(obs19chk[obs19chk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.obs19_etother)).getText().toString():""; // append the other text value in to the selected option
    		    	 obs19chk_val+=chk_Value19other;
    		    	 if(obs19chk_val.equals(""))
					 {
						 cf.ShowToast("Please select the Observations for Question No.19." , 0);
					 }
    		    	 else
    		    	 {
    		    		 if(obs19chk[obs19chk.length-1].isChecked())
						 {
							 if(chk_Value19other.trim().equals("&#40;"))
							 {
								 cf.ShowToast("Please enter the Other text for Question No.19 Observation", 0);
								 cf.setFocus(((EditText)findViewById(R.id.obs19_etother)));
							 }
							 else
							 {
								 chk_obs19comments();	
							 }
						 }
						 else
						 {
							 chk_obs19comments();
						 }
    		    	 }
				 }
    			 else
    			 {
    				 chk_obs19comments();
    			 }
    		}
		}
	}
	private void chk_obs19comments() {

		// TODO Auto-generated method stub
		if(((EditText)findViewById(R.id.obs19comment)).getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.19 */
		{
			cf.ShowToast("Please enter the comments for Question No.19.", 0);
			cf.setFocus(((EditText)findViewById(R.id.obs19comment)));
		}
		else
		{
			if(obs20rdgtxt.equals(""))
    		{
    			cf.ShowToast("Please select the option for Question No.20.", 0);
    		}
    		else
    		{
    			 if(obs20rdgtxt.equals("Yes"))
				 {
    				 obs20chk_val= cf.getselected_chk(obs20chk);
    		    	 String chk_Value20other =(obs20chk[obs20chk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.obs20_etother)).getText().toString():""; // append the other text value in to the selected option
    		    	 obs20chk_val+=chk_Value20other;
    		    	 if(obs20chk_val.equals(""))
					 {
						 cf.ShowToast("Please select the Observations for Question No.20." , 0);
					 }
    		    	 else
    		    	 {
    		    		 if(obs20chk[obs20chk.length-1].isChecked())
						 {
							 if(chk_Value20other.trim().equals("&#40;"))
							 {
								 cf.ShowToast("Please enter the Other text for Question No.20.", 0);
								 cf.setFocus(((EditText)findViewById(R.id.obs20_etother)));
							 }
							 else
							 {
								 chk_obs20comments();	
							 }
						 }
						 else
						 {
							 chk_obs20comments();
						 }
    		    	 }
				 }
    			 else
    			 {
    				 chk_obs20comments();
    			 }
    		}
		}
	}
	private void chk_obs20comments() {


		// TODO Auto-generated method stub
		if(((EditText)findViewById(R.id.obs20comment)).getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.20 */
		{
			cf.ShowToast("Please enter the comments for Question No.20.", 0);
			cf.setFocus(((EditText)findViewById(R.id.obs20comment)));
		}
		else
		{
			if(obs21rdgtxt.equals(""))
    		{
    			cf.ShowToast("Please select the option for Question No.21.", 0);
    		}
    		else
    		{
    			 if(obs21rdgtxt.equals("Yes"))
				 {
    				 obs21chk_val= cf.getselected_chk(obs21chk);
    		    	 String chk_Value21other =(obs21chk[obs21chk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.obs21_etother)).getText().toString():""; // append the other text value in to the selected option
    		    	 obs21chk_val+=chk_Value21other;
    		    	 if(obs21chk_val.equals(""))
					 {
						 cf.ShowToast("Please select the Observations for Question No.21." , 0);
					 }
    		    	 else
    		    	 {
    		    		 if(obs21chk[obs21chk.length-1].isChecked())
						 {
							 if(chk_Value21other.trim().equals("&#40;"))
							 {
								 cf.ShowToast("Please enter the Other text for Question No.21.", 0);
								 cf.setFocus(((EditText)findViewById(R.id.obs21_etother)));
							 }
							 else
							 {
								chk_obs21comments();	
							 }
						 }
						 else
						 {
							 chk_obs21comments();
						 }
    		    	 }
				 }
    			 else
    			 {
    				 chk_obs21comments();
    			 }
    		}
		}
	}
	private void chk_obs21comments() {
		// TODO Auto-generated method stub
		if(((EditText)findViewById(R.id.obs21comment)).getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.21 */
		{
			cf.ShowToast("Please enter the comments for Question No.21.", 0);
			cf.setFocus(((EditText)findViewById(R.id.obs21comment)));
		}
		else
		{
			if(obs22rdgtxt.equals(""))
    		{
    			cf.ShowToast("Please select the option for Question No.22.", 0);
    		}
    		else
    		{
    			 if(obs22rdgtxt.equals("Yes"))
				 {
    				 obs22chk_val= cf.getselected_chk(obs22chk);
    		    	 String chk_Value22other =(obs22chk[obs22chk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.obs22_etother)).getText().toString():""; // append the other text value in to the selected option
    		    	 obs22chk_val+=chk_Value22other;
    		    	 if(obs22chk_val.equals(""))
					 {
						 cf.ShowToast("Please select the Observtaions for Question No.22." , 0);
					 }
    		    	 else
    		    	 {
    		    		 if(obs22chk[obs22chk.length-1].isChecked())
						 {
							 if(chk_Value22other.trim().equals("&#40;"))
							 {
								 cf.ShowToast("Please enter the Other text for Question No.22.", 0);
								 cf.setFocus(((EditText)findViewById(R.id.obs22_etother)));
							 }
							 else
							 {
								chk_obs22comments();	
							 }
						 }
						 else
						 {
							 chk_obs22comments();
						 }
    		    	 }
				 }
    			 else
    			 {
    				 chk_obs22comments();
    			 }
    		}
		}
	}
	private void chk_obs22comments() {
		// TODO Auto-generated method stub
		if(((EditText)findViewById(R.id.obs22comment)).getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.22 */
		{
			cf.ShowToast("Please enter the comments for Question No.22.", 0);
			cf.setFocus(((EditText)findViewById(R.id.obs22comment)));
		}
		else
		{
			Obs3_Insert();
		}
	}
	private void Obs3_Insert() {
		// TODO Auto-generated method stub
		Cursor OBS3_save=null;
		try
		{
			OBS3_save=cf.SelectTablefunction(cf.SINK_Obs3tbl, " where fld_srid='"+cf.selectedhomeid+"'");
			if(OBS3_save.getCount()>0)
			{
				try
				{
					cf.arr_db.execSQL("UPDATE "+cf.SINK_Obs3tbl+ " set InteriorWallCrackNoted='"+obs14rdgtxt+"',InteriorWallCrackOption='"+cf.encode(obs14chk_val)+"',"+
				                         "InteriorWallCrackComments='"+cf.encode(((EditText)findViewById(R.id.obs14comment)).getText().toString())+"',"+
							             "WallSlabCrackNoted='"+obs15rdgtxt+"',WallSlabCrackOption='"+cf.encode(obs15chk_val)+"',"+
				                         "WallSlabCrackComments='"+cf.encode(((EditText)findViewById(R.id.obs15comment)).getText().toString())+"',"+
							             "InteriorCeilingCrackNoted='"+obs16rdgtxt+"',InteriorCeilingCrackOption='"+cf.encode(obs16chk_val)+"',"+
				                         "InteriorCeilingCrackComments='"+cf.encode(((EditText)findViewById(R.id.obs16comment)).getText().toString())+"',"+
							             "InteriorFloorCrackNoted='"+obs17rdgtxt+"',InteriorFloorCrackOption='"+cf.encode(obs17chk_val)+"',"+
				                         "InteriorFloorCrackComments='"+cf.encode(((EditText)findViewById(R.id.obs17comment)).getText().toString())+"',"+
							             "WindowFrameOutofSquare='"+obs18rdgtxt+"',WindowFrameOutofSquareOption='"+cf.encode(obs18chk_val)+"',"+
						                 "WindowFrameOutofSquareComments='"+cf.encode(((EditText)findViewById(R.id.obs18comment)).getText().toString())+"',"+
							             "FloorSlopingNoted='"+obs19rdgtxt+"',FloorSlopingOption='"+cf.encode(obs19chk_val)+"',"+
						                 "FloorSlopingComments='"+cf.encode(((EditText)findViewById(R.id.obs19comment)).getText().toString())+"',"+
							             "CrackedGlazingNoted='"+obs20rdgtxt+"',CrackedGlazingOption='"+cf.encode(obs20chk_val)+"',"+
						                 "CrackedGlazingComments='"+cf.encode(((EditText)findViewById(R.id.obs20comment)).getText().toString())+"',"+
							             "PreviousCorrectiveMeasureNoted='"+obs21rdgtxt+"',PreviousCorrectiveMeasureOption='"+cf.encode(obs21chk_val)+"',"+
						                 "PreviousCorrectiveMeasureComments='"+cf.encode(((EditText)findViewById(R.id.obs21comment)).getText().toString())+"',"+
							             "BindingDoorOpeningNoted='"+obs22rdgtxt+"',BindingDoorOpeningOption='"+cf.encode(obs22chk_val)+"',"+
						                 "BindingDoorOpeningComments='"+cf.encode(((EditText)findViewById(R.id.obs22comment)).getText().toString())+"' where fld_srid='"+cf.selectedhomeid+"'");
					cf.ShowToast("Observations(14-22) updated successfully.", 1);
				    ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
				     cf.goclass(55);
				}
				catch (Exception E)
				{
					String strerrorlog="Updating the Observation3 - Sinkhole";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
			}
			else
			{
				try
				{
					cf.arr_db.execSQL("INSERT INTO "
							+ cf.SINK_Obs3tbl
							+ " (fld_srid,InteriorWallCrackNoted,InteriorWallCrackOption,InteriorWallCrackComments,"+
							             "WallSlabCrackNoted,WallSlabCrackOption,WallSlabCrackComments,"+
							             "InteriorCeilingCrackNoted,InteriorCeilingCrackOption,InteriorCeilingCrackComments,"+
							             "InteriorFloorCrackNoted,InteriorFloorCrackOption,InteriorFloorCrackComments,"+
							             "WindowFrameOutofSquare,WindowFrameOutofSquareOption,WindowFrameOutofSquareComments,"+
							             "FloorSlopingNoted,FloorSlopingOption,FloorSlopingComments,"+
							             "CrackedGlazingNoted,CrackedGlazingOption,CrackedGlazingComments,"+
							             "PreviousCorrectiveMeasureNoted,PreviousCorrectiveMeasureOption,PreviousCorrectiveMeasureComments,"+
							             "BindingDoorOpeningNoted,BindingDoorOpeningOption,BindingDoorOpeningComments)"
							+ "VALUES ('"+cf.selectedhomeid+"','"+obs14rdgtxt+"','"+cf.encode(obs14chk_val)+"','"+cf.encode(((EditText)findViewById(R.id.obs14comment)).getText().toString())+"',"+
							             "'"+obs15rdgtxt+"','"+cf.encode(obs15chk_val)+"','"+cf.encode(((EditText)findViewById(R.id.obs15comment)).getText().toString())+"',"+
							             "'"+obs16rdgtxt+"','"+cf.encode(obs16chk_val)+"','"+cf.encode(((EditText)findViewById(R.id.obs16comment)).getText().toString())+"',"+
							             "'"+obs17rdgtxt+"','"+cf.encode(obs17chk_val)+"','"+cf.encode(((EditText)findViewById(R.id.obs17comment)).getText().toString())+"',"+
							             "'"+obs18rdgtxt+"','"+cf.encode(obs18chk_val)+"','"+cf.encode(((EditText)findViewById(R.id.obs18comment)).getText().toString())+"',"+
							             "'"+obs19rdgtxt+"','"+cf.encode(obs19chk_val)+"','"+cf.encode(((EditText)findViewById(R.id.obs19comment)).getText().toString())+"',"+
							             "'"+obs20rdgtxt+"','"+cf.encode(obs20chk_val)+"','"+cf.encode(((EditText)findViewById(R.id.obs20comment)).getText().toString())+"',"+
							             "'"+obs21rdgtxt+"','"+cf.encode(obs21chk_val)+"','"+cf.encode(((EditText)findViewById(R.id.obs21comment)).getText().toString())+"',"+
							             "'"+obs22rdgtxt+"','"+cf.encode(obs22chk_val)+"','"+cf.encode(((EditText)findViewById(R.id.obs22comment)).getText().toString())+"')");

					 cf.ShowToast("Observations(14-22) saved successfully.", 1);
					 ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE); 
					 cf.goclass(55);
			 	}
				catch (Exception E)
				{
					String strerrorlog="Inserting the Observation3 - Sinkhole";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
			}
		}
		catch (Exception E)
		{
			String strerrorlog="Checking the rows inserted in the Observation3 - Sinkhole table.";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+"table at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
	private void hidelayouts() {
		// TODO Auto-generated method stub
		((LinearLayout)findViewById(R.id.obs14lin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.obs15lin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.obs16lin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.obs17lin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.obs18lin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.obs19lin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.obs20lin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.obs21lin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.obs22lin)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdobs14)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdobs15)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdobs16)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdobs17)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdobs18)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdobs19)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdobs20)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdobs21)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdobs22)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.shwobs14)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwobs15)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwobs16)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwobs17)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwobs18)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwobs19)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwobs20)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwobs21)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwobs22)).setVisibility(cf.v1.VISIBLE);
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
		     cf.goclass(53);
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}
}
