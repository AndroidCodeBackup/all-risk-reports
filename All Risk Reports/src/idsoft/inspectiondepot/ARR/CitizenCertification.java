package idsoft.inspectiondepot.ARR;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.R.string;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class CitizenCertification extends Activity {
TextView tv[]=new TextView[20];
ImageView im;
RadioGroup rg[] = new RadioGroup[3];
CommonFunctions cf;
String insp_id="2";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.citizencertification);
		cf=new CommonFunctions(this);
		 Bundle extras = getIntent().getExtras();
		if (extras != null) {
			
			cf.selectedhomeid = extras.getString("homeid");
			cf.onlinspectionid = extras.getString("InspectionType");
		    cf.onlstatus = extras.getString("status");
		    insp_id=extras.getString("insp_id");
		   
    	}
		
		tv[0]=(TextView)  findViewById(R.id.appli_name);
		tv[1]=(TextView)  findViewById(R.id.appli_no);
		tv[2]=(TextView)  findViewById(R.id.appli_add);
		tv[3]=(TextView)  findViewById(R.id.appli_date);
		tv[4]=(TextView)  findViewById(R.id.roof_cover);
		tv[5]=(TextView)  findViewById(R.id.roof_approximate);
		tv[6]=(TextView)  findViewById(R.id.roof_age);
		tv[7]=(TextView)  findViewById(R.id.roof_dateLast);
		tv[8]=(TextView)  findViewById(R.id.roof_comp);
		tv[9]=(TextView)  findViewById(R.id.roof_visible_sign);
		tv[10]=(TextView)  findViewById(R.id.roof_visible_sign_desc);
		tv[11]=(TextView)  findViewById(R.id.roof_visible_leaks);
		tv[12]=(TextView)  findViewById(R.id.roof_visible_leaks_desc);
		tv[13]=(TextView)  findViewById(R.id.insp_name);
		tv[14]=(TextView)  findViewById(R.id.insp_ph);
		tv[15]=(TextView)  findViewById(R.id.insp_Ln);
		tv[16]=(TextView)  findViewById(R.id.insp_date);		
		tv[17]=(TextView)  findViewById(R.id.insp_LT);
		
		tv[18]=(TextView)  findViewById(R.id.damagetxt);		
		tv[19]=(TextView)  findViewById(R.id.visibletxt);
		im =(ImageView) findViewById(R.id.insp_sig);
		/*rg[0]=(RadioGroup) findViewById(R.id.Roof_update);
		rg[1]=(RadioGroup) findViewById(R.id.Roof_visible);
		rg[2]=(RadioGroup) findViewById(R.id.Roof_leaks);*/
		for(int i=0;i<tv.length;i++)
		{
			tv[i].setPaintFlags(tv[i].getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);
		}
		
		((TextView) findViewById(R.id.ffs)).setPaintFlags(((TextView) findViewById(R.id.ffs)).getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);
		show_savedvalue();
		
		((Button) findViewById(R.id.citi_back)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
	}
	public void clicker(View v)
	{
		
	}
	public void show_savedvalue()
	{
		String sql=" Select ph.ARR_PH_FirstName||' '||ph.ARR_PH_LastName as ph_Name, ph.ARR_PH_Address1||' '||ph.ARR_PH_Address2 as ph_Address,ph.ARR_PH_Policyno,ph.ARR_Schedule_ScheduledDate From  "+cf.policyholder+" as ph WHERE ARR_PH_SRID='"+cf.selectedhomeid+"'";
		Cursor cs=cf.arr_db.rawQuery(sql, null);
		System.out.println(sql);
		cs.moveToFirst();
		if(cs.getCount()>0)
		{
		tv[0].setText(cf.decode(cs.getString(cs.getColumnIndex("ph_Name"))));
		tv[2].setText(cf.decode(cs.getString(cs.getColumnIndex("ph_Address"))));
		tv[1].setText(cf.decode(cs.getString(cs.getColumnIndex("ARR_PH_Policyno"))));
		tv[3].setText(cf.decode(cs.getString(cs.getColumnIndex("ARR_Schedule_ScheduledDate"))));
		}
		/***Roof covering type value **/
		/*sql=" Select GRI.*,M.mst_text from "+cf.Roof_cover_type+" GRI LEFT JOIN "+cf.master_tbl+" M on GRI.RCT_TypeValue=m.mst_custom_id and M.mst_module_id='2' WHERE RCT_masterid=(SELECT RCT_masterid from "+cf.Roof_cover_type+" Where RCT_TypeValue='11' and RCT_masterid in " +
				"(SELECT RM_masterid from "+cf.Roof_master+" where RM_insp_id='2' and RM_srid='"+cf.selectedhomeid+"' and RM_module_id='0') and RCT_FieldVal='true' ) ";*/
		sql=" SELECT * FROM "+cf.Rct_table+" WHERE RCT_Ssrid='"+cf.selectedhomeid+"' and RCT_insp_id='"+insp_id+"' and RCT_pre='true'";
		System.out.println("the sql1"+sql);
		cs=cf.arr_db.rawQuery(sql, null);
		System.out.println(sql);
		cs.moveToFirst();
		String roof_cover="",appr="",roof_age="",date="";
		if(cs.getCount()>0)
		{
			tv[4]=(TextView)  findViewById(R.id.roof_cover);
			tv[5]=(TextView)  findViewById(R.id.roof_approximate);
			tv[6]=(TextView)  findViewById(R.id.roof_age);
			tv[7]=(TextView)  findViewById(R.id.roof_dateLast);
			tv[4].setText(roof_cover.trim());
			roof_cover=cf.decode(cs.getString(cs.getColumnIndex("RCT_rct")));
			date=cf.decode(cs.getString(cs.getColumnIndex("RCT_PAD")));
			date=(date.equals(""))?"N/A":date;
			roof_age=cf.decode(cs.getString(cs.getColumnIndex("RCT_yoi")));
			appr=cf.decode(cs.getString(cs.getColumnIndex("RCT_arfl")));
			if(roof_cover.equals("Other"))
			{
				roof_cover=cf.decode(cs.getString(cs.getColumnIndex("RCT_other_rct")));
			}
			if(roof_age.equals("Other"))
			{
				roof_age=cf.decode(cs.getString(cs.getColumnIndex("RCT_other_yoi")));
			}
			if(appr.equals("Other"))
			{
				appr=cf.decode(cs.getString(cs.getColumnIndex("RCT_other_aRFL")));
			}
			
			System.out.println("PERC="+cs.getString(cs.getColumnIndex("RCT_Rsh_per")));
			if(cs.getString(cs.getColumnIndex("RCT_Rsh_per")).equals(""))
			{
				tv[4].setText(roof_cover.trim());
			}
			else
			{
				tv[4].setText(roof_cover.trim()+" ("+cs.getString(cs.getColumnIndex("RCT_Rsh_per"))+"%)");	
			}
			
			
			tv[7].setText(date);
			tv[6].setText(roof_age);
			tv[5].setText(appr);
		}
		
			/*do
			{
				tv[4]=(TextView)  findViewById(R.id.roof_cover);
				tv[5]=(TextView)  findViewById(R.id.roof_approximate);
				tv[6]=(TextView)  findViewById(R.id.roof_age);
				tv[7]=(TextView)  findViewById(R.id.roof_dateLast);
				
				if(cs.getString(cs.getColumnIndex("RCT_TypeValue")).equals("1"))
				{
					roof_cover=cf.decode(cs.getString(cs.getColumnIndex("RCT_FieldVal")));
					tv[4].setText(roof_cover.trim());
				}
				if(cs.getString(cs.getColumnIndex("RCT_TypeValue")).equals("2"))
				{
					date=cf.decode(cs.getString(cs.getColumnIndex("RCT_FieldVal")));
					tv[7].setText(date);
					
				}
				if(cs.getString(cs.getColumnIndex("RCT_TypeValue")).equals("3"))
				{
					roof_age=cf.decode(cs.getString(cs.getColumnIndex("RCT_FieldVal")));
					if(roof_age.equals("Other"))
					{
						tv[6].setText(roof_age+ " ("+cf.decode(cs.getString(cs.getColumnIndex("RCT_Other")))+")");
					}
					else
					{
						tv[6].setText(roof_age);
					}
				}
				if(cs.getString(cs.getColumnIndex("RCT_TypeValue")).equals("10"))
				{
					appr=cf.decode(cs.getString(cs.getColumnIndex("RCT_FieldVal")));
					tv[5].setText(appr);
				}
			}
			while(cs.moveToNext());
		}*/
		/***Roof covering type value  **/
		/**Roof condition noted value **/
		/*sql=" Select GRI.*,M.mst_text from "+cf.General_Roof_information+" GRI LEFT JOIN "+cf.master_tbl+" M on GRI.RC_Condition_type=m.mst_custom_id and M.mst_module_id='2' WHERE RC_masterid=" +
				"(SELECT RM_masterid from "+cf.Roof_master+" where RM_insp_id='2' and RM_srid='"+cf.selectedhomeid+"' and RM_module_id='1') and (RC_Condition_type='16' or RC_Condition_type='18')  ";*/
		sql=" SELECT R_RCN_VSL,R_RCN_VSCBS FROM "+cf.RCN_table+" WHERE R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id='"+insp_id+"'";
		System.out.println("the sql1"+sql);
		cs=cf.arr_db.rawQuery(sql, null);
		cs.moveToFirst();
		if(cs.getCount()>0)
		{
			String VSL=cf.decode(cs.getString(cs.getColumnIndex("R_RCN_VSL"))),VSL_o="",
			VSCBS=cf.decode(cs.getString(cs.getColumnIndex("R_RCN_VSCBS"))),VSCBS_o="";
			if(!VSL.equals(""))
			{
				if(VSL.contains("#&45"))
				{
					String[]tem=VSL.split("#&45");
					VSL=tem[0];
					VSL_o=tem[1];
					tv[12].setVisibility(cf.v1.VISIBLE);
					tv[12].setText(VSL_o);
					tv[19].setText(VSL);
					//((RadioButton)findViewById(R.id.Roof_leaks).findViewWithTag(VSL)).setChecked(true);
				}
				else
				{
					tv[19].setText(VSL);
					//((RadioButton)findViewById(R.id.Roof_leaks).findViewWithTag(VSL)).setChecked(true);
				}
			}
			if(!VSCBS.equals(""))
			{
				if(VSCBS.contains("#&45"))
				{
					String[]tem=VSCBS.split("#&45");
					VSCBS=tem[0];
					VSCBS_o=tem[1];
					tv[10].setVisibility(cf.v1.VISIBLE);
					tv[10].setText(VSCBS_o);
					tv[18].setText(VSCBS);
					//((RadioButton)findViewById(R.id.Roof_visible).findViewWithTag(VSCBS)).setChecked(true);
				}else
				{
					//((RadioButton)findViewById(R.id.Roof_visible).findViewWithTag(VSCBS)).setChecked(true);
					tv[18].setText(VSCBS);
				}
			}
			/*do
			{
				String val=cf.decode(cs.getString(cs.getColumnIndex("RC_Condition_type_val"))),
						desc=cf.decode(cs.getString(cs.getColumnIndex("RC_Condition_type_DESC"))),
						tit=cf.decode(cs.getString(cs.getColumnIndex("RC_Condition_type")));
				if(tit.trim().equals("18"))
				{
					System.out.println("the 18="+val);
					((RadioButton)findViewById(R.id.Roof_visible).findViewWithTag(val)).setChecked(true);
					//tv[9].setText(val);
					System.out.println("the 18="+desc);
					if(val.equals("Yes"))
					{
						tv[10].setVisibility(cf.v1.VISIBLE);
						tv[10].setText(desc);
					}
					else
					{
						tv[10].setVisibility(cf.v1.GONE);
					}
				}
				if(tit.trim().equals("16"))
				{
					System.out.println("the 16="+val);
					((RadioButton)findViewById(R.id.Roof_leaks).findViewWithTag(val)).setChecked(true);
					//tv[11].setText(val);
					System.out.println("the 16="+desc);
					if(val.equals("Yes"))
					{
						tv[12].setVisibility(cf.v1.VISIBLE);
						tv[12].setText(desc);
					}
					else
					{
						tv[12].setVisibility(cf.v1.GONE);
					}
					
					
				}
			}while(cs.moveToNext());*/
		}
		/**Roof condition noted value ends **/
		/**INsepctor information **/
		cf.CreateARRTable(1);
		cs=cf.arr_db.rawQuery(" Select * from "+cf.inspectorlogin+" WHERE Fld_InspectorId='"+cf.Insp_id+"'", null);
		cs.moveToFirst();
		String inspext = cf.decode(cs.getString(cs.getColumnIndex("Fld_signature")));
		String inspfname = cf.decode(cs.getString(cs.getColumnIndex("Fld_InspectorFirstName")));
		String insplname = cf.decode(cs.getString(cs.getColumnIndex("Fld_InspectorLastName")));
		System.out.println("the path="+getFilesDir()+"/"+inspext); 
		File f =new File(getFilesDir()+"/"+inspext);
		System.out.println("the file "+f.exists());
/*		if(f.exists())
		{
			Bitmap b =cf.ShrinkBitmap(getFilesDir()+"/"+inspext, 100, 100);System.out.println("b="+b);
			im.setImageBitmap(b);			
		}
*/		tv[13].setText(inspfname+" "+insplname);
		tv[15].setText(cf.decode(cs.getString(cs.getColumnIndex("Fld_licenceno"))));
		tv[17].setText(cf.decode(cs.getString(cs.getColumnIndex("Fld_licencetype"))));
		try {
			cf.getInspectorId();
			File outputFile = new File(this.getFilesDir()+"/Signature"+cf.Insp_id.toString()+".jpg");System.out.println("tsse"+outputFile);
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(outputFile),null, o);			
			final int REQUIRED_SIZE = 200;
			int width_tmp = o.outWidth, height_tmp = o.outHeight;
			int scale = 1;
			while (true) 
			{
				if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE)
				break;
				width_tmp /= 2;
				height_tmp /= 2;
				scale *= 2;
			}
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(
					outputFile), null, o2);
			BitmapDrawable bmd = new BitmapDrawable(bitmap);
			im.setImageDrawable(bmd);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				System.out.println("file not found"+e.getMessage());
				e.printStackTrace();
			}
		String timeStamp = new SimpleDateFormat("MM/dd/yyyy").format(Calendar.getInstance().getTime());
		tv[16].setText(timeStamp);
		tv[14].setText(cf.decode(cs.getString(cs.getColumnIndex("Fld_Phone"))));
	}
	
}
