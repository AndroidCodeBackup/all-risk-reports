package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;

import java.io.File;
import java.io.FileInputStream;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ShowInspector extends Activity{
	CommonFunctions cf;
	@Override
	   public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.inspectorprofile);
	        cf = new CommonFunctions(this);
	       final Animation animTranslate = AnimationUtils.loadAnimation(this, R.anim.lefttoright);
		     ((LinearLayout)findViewById(R.id.showlin)).setAnimation(animTranslate);
	        cf.getInspectorId();
	        try
	        {
	        	Cursor PH_retrive=cf.SelectTablefunction(cf.inspectorlogin, " where Fld_InspectorId='"+cf.Insp_id+"'");
				if(PH_retrive.getCount()>0)
				{  
					PH_retrive.moveToFirst();
					String firstname =cf.decode(PH_retrive.getString(PH_retrive.getColumnIndex("Fld_InspectorFirstName")));
					String lastname =cf.decode(PH_retrive.getString(PH_retrive.getColumnIndex("Fld_InspectorLastName")));
					String address =cf.decode(PH_retrive.getString(PH_retrive.getColumnIndex("Fld_InspectorAddress")));					
					String compname = cf.decode(PH_retrive.getString(PH_retrive.getColumnIndex("Fld_InspectorCompanyName")));					
					String email = cf.decode(PH_retrive.getString(PH_retrive.getColumnIndex("Fld_InspectorEmail")));
					String licenceno = cf.decode(PH_retrive.getString(PH_retrive.getColumnIndex("Fld_licenceno")));
					String lictype = cf.decode(PH_retrive.getString(PH_retrive.getColumnIndex("Fld_licencetype")));
					
					
					
					((TextView)findViewById(R.id.txtfname)).setText(firstname+" "+lastname);					
					if(address.equals(""))
					{
						((TextView)findViewById(R.id.txtaddress)).setText("NA");	
					}
					else
					{
						((TextView)findViewById(R.id.txtaddress)).setText(address);	
					}
					if(compname.equals("NULL") || compname.equals(""))
					{
						((TextView)findViewById(R.id.txtcname)).setText("NA");
					}
					else
					{
						((TextView)findViewById(R.id.txtcname)).setText(compname);	
					}
					if(email.equals(""))
					{
						((TextView)findViewById(R.id.txtemail)).setText("NA");	
					}
					else
					{
						((TextView)findViewById(R.id.txtemail)).setText(email);
					}
					if(licenceno.equals(""))
					{
						((TextView)findViewById(R.id.txtlno)).setText("NA");
					}
					else
					{
						((TextView)findViewById(R.id.txtlno)).setText(licenceno);
					}
					if(lictype.equals(""))
					{
						((TextView)findViewById(R.id.txtltyp)).setText("NA");
					}
					else
					{
						((TextView)findViewById(R.id.txtltyp)).setText(lictype);
					}
					
					String inspext = cf.decode(PH_retrive.getString(PH_retrive.getColumnIndex("Fld_InspectorPhotoExtn")));
					File outputFile = new File(this.getFilesDir()+"/"+cf.Insp_id.toString()+inspext);
						
					BitmapFactory.Options o = new BitmapFactory.Options();
					o.inJustDecodeBounds = true;
					BitmapFactory.decodeStream(new FileInputStream(outputFile),
						null, o);
					final int REQUIRED_SIZE = 200;
					int width_tmp = o.outWidth, height_tmp = o.outHeight;
					int scale = 1;
					while (true) {
						if (width_tmp / 2 < REQUIRED_SIZE
								|| height_tmp / 2 < REQUIRED_SIZE)
							break;
						width_tmp /= 2;
						height_tmp /= 2;
						scale *= 2;
					}
					BitmapFactory.Options o2 = new BitmapFactory.Options();
					o2.inSampleSize = scale;
					Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(
							outputFile), null, o2);
					BitmapDrawable bmd = new BitmapDrawable(bitmap);
					((ImageView)findViewById(R.id.inspphoto)).setImageDrawable(bmd);
				}
	        }
	        catch (Exception e) {
				// TODO: handle exception
			}
	}
	public void clicker(View v)
	{
		switch(v.getId())
		{
		  case R.id.close:
			   startActivity(new Intent(ShowInspector.this,HomeScreen.class));
			   break;
			   
		}
	}
}
