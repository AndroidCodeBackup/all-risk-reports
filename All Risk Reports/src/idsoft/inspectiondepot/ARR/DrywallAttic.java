/*
   ************************************************************
   * Project: ALL RISK REPORT
   * Module : ChineseDrywall.java
   * Creation History:
      Created By : Gowri on 1/16/2013
   * Modification History:
      Last Modified By : Gowri on 04/04/2013
   ************************************************************  
*/package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class DrywallAttic extends Activity {
	CommonFunctions cf;
	boolean load_comment=true;
	ArrayAdapter atcnoadapter,atcspcadapter;
	Spinner atticnospin,atticspcspin;
	String num[] = {"--Select--","1","2","3","4"};
	String spc[] = {"--Select--","Garage","Hall","Master Closet","Hall Closet","Bathroom","Other"};
	String atticnostr="",acessstr="";
	int atint=0,Current_select_id;
	Cursor Dryattic_save,Drysum_save;
	String[] attic_in_DB;
	TableLayout attictblshow;
		
	@Override
	   public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        cf=new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);
				
		 	}
	        setContentView(R.layout.drywallattic);
	        cf.getDeviceDimensions();
	        
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
			hdr_layout.addView(new HdrOnclickListener(this,1,"Chinese Drywall","Attic",6,0,cf));
	        
	        LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
	        mainmenu_layout.setMinimumWidth(cf.wd);
	        mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 6, 0,0,cf));
	        LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 65, 1,0,cf));
	        TableRow tblrw = (TableRow)findViewById(R.id.row2);
			tblrw.setMinimumHeight(cf.ht);
			cf.CreateARRTable(7);
			cf.CreateARRTable(71);
			declarations();
			setComments();
			Showatticvalue();
	}
	private void setComments() {
		// TODO Auto-generated method stub
		try
		{
		   Cursor SCf_retrive=cf.SelectTablefunction(cf.DRY_sumcond, " where fld_srid='"+cf.selectedhomeid+"'");
		   
		   if(SCf_retrive.getCount()>0)
		   {  
			   SCf_retrive.moveToFirst();
			   ((EditText)findViewById(R.id.atticcomment)).setText(cf.decode(SCf_retrive.getString(SCf_retrive.getColumnIndex("atticcomments"))));
		   }
		   
		}catch (Exception e) {
			// TODO: handle exception
		}
	}
	private void declarations() {
		// TODO Auto-generated method stub
		cf.setupUI((ScrollView) findViewById(R.id.scr));
		((EditText)findViewById(R.id.atticcomment)).addTextChangedListener(new textwatcher(1));
		
		atticnospin=(Spinner) findViewById(R.id.atticno_spin);
		atcnoadapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, num);
		atcnoadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		atticnospin.setAdapter(atcnoadapter);
		atticnospin.setOnItemSelectedListener(new  Spin_Selectedlistener(1));
		
		atticspcspin=(Spinner) findViewById(R.id.acsspac_spin);
		atcspcadapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, spc);
		atcspcadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		atticspcspin.setAdapter(atcspcadapter);
		atticspcspin.setOnItemSelectedListener(new  Spin_Selectedlistener(2));
		
		((EditText)findViewById(R.id.locobs_etxt)).addTextChangedListener(new textwatcher(2));
		
	    attictblshow =(TableLayout)findViewById(R.id.atticvalue);
	}
	class Spin_Selectedlistener implements OnItemSelectedListener
	{
		int i;
		public Spin_Selectedlistener(int i) {
			// TODO Auto-generated constructor stub
			this.i=i;
		}

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			if(i==1){
				atticnostr = atticnospin.getSelectedItem().toString();
	            if(atticnostr.equals("--Select--")){
					
				}else{
					if(atint==0){defaultattic();}
				}
	        }
			else if(i==2)
			{
				acessstr = atticspcspin.getSelectedItem().toString();
				if(acessstr.equals("Other"))
				{
					((EditText)findViewById(R.id.accesspt_etxt)).setVisibility(arg1.VISIBLE);
				}
				else
				{
					((EditText)findViewById(R.id.accesspt_etxt)).setText("");
					((EditText)findViewById(R.id.accesspt_etxt)).setVisibility(arg1.INVISIBLE);
				}
			}
			
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	class textwatcher implements TextWatcher
    {
	         public int type;
	         textwatcher(int type)
	    	{
	    		this.type=type;
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
	    		if(this.type==1)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.attic_tv_type1),500); 
	    			if(s.toString().startsWith(" "))
					{
	    				((EditText)findViewById(R.id.atticcomment)).setText("");
					}
	    		}
	    		else if(this.type==2)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.atticloc_tv_type1),250); 
	    			if(s.toString().startsWith(" "))
					{
	    				((EditText)findViewById(R.id.locobs_etxt)).setText("");
					}
	    		}
	    	
	         }

	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,int after) {
	    		// TODO Auto-generated method stub
	     	}

	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,int count) {
	    		// TODO Auto-generated method stub
	    	}
	}	
	public void clicker(View v)
	{
		switch (v.getId()) {
			case R.id.hme:
				cf.gohome();
				break;
			case R.id.loadcomments:/***Call for the comments***/
				int len=((EditText)findViewById(R.id.atticcomment)).getText().toString().length();
				if(load_comment )
				{
						load_comment=false;
						int loc[] = new int[2];
						v.getLocationOnScreen(loc);
						Intent in1 = cf.loadComments("Attic Comments",loc);
						in1.putExtra("length", len);
						in1.putExtra("max_length", 500);
						startActivityForResult(in1, cf.loadcomment_code);
				}
				break;
			case R.id.save:
				chk_values();
				if(Dryattic_save.getCount()==0)
				{
					cf.ShowToast("Atleast one Attic space need to be added and saved.", 0);
				}
				else
				{
					/*if(((EditText)findViewById(R.id.atticcomment)).getText().toString().trim().equals(""))
					{
						cf.ShowToast("Please enter Attic Comments.", 0);
						cf.setFocus(((EditText)findViewById(R.id.atticcomment)));
					}
					else
					{*/
						if(Drysum_save.getCount()>0)
						{
							try
							{
								cf.arr_db.execSQL("UPDATE "+cf.DRY_sumcond+ " set atticcomments='"+cf.encode(((EditText)findViewById(R.id.atticcomment)).getText().toString())+"' where fld_srid='"+cf.selectedhomeid+"'");
								cf.ShowToast("Attic saved successfully.", 1);
							}
							catch (Exception E)
							{
								String strerrorlog="Updating the Attic - Drywall";
								cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
							}
						}
						else
						{
							try
							{
								cf.arr_db.execSQL("INSERT INTO "
										+ cf.DRY_sumcond
										+ " (fld_srid,presofsulfur,obscopper,presofdry,addcomments,drywallcomments,atticcomments,"+
										   "applianccomments,hvaccomments,assesmentcomments)"
										+ "VALUES('"+cf.selectedhomeid+"','','',"+
										"'','','',"+
										"'"+cf.encode(((EditText)findViewById(R.id.atticcomment)).getText().toString())+"','','','')");
								 cf.ShowToast("Attic saved successfully.", 1);
							}
							catch (Exception E)
							{
								String strerrorlog="Inserting the Attic - Drywall";
								cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
							}
						}
						((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
					    cf.goclass(66);
					//}
				}
				break;
			case R.id.dryatticclear:
				atint=0;defaultattic();
				atticnospin.setSelection(0);atticnospin.setEnabled(true);
		        ((Button)findViewById(R.id.addattic)).setText("Save/Add More Attic");
				break;
			case R.id.addattic:
				atint=2;
				if(atticnostr.equals("--Select--")){
					cf.ShowToast("Please select Attic Space.", 0);
				}
				else
				{
					if(acessstr.equals("--Select--")){
						cf.ShowToast("Please select Access Point.", 0);
					}
					else
					{
						if(acessstr.equals("Other")){
							if(((EditText)findViewById(R.id.accesspt_etxt)).getText().toString().trim().equals(""))
							{
								cf.ShowToast("Please enter the Other text for Access Point.", 0);
								cf.setFocus(((EditText)findViewById(R.id.accesspt_etxt)));
							}
							else
							{
								next();
							}
						}
						else
						{
							next();
						}
					}
				}
				break;
				
	   }
	}
	private void next() {
		// TODO Auto-generated method stub
		if(((EditText)findViewById(R.id.locobs_etxt)).getText().toString().trim().equals(""))
		{
			cf.ShowToast("Please enter the Locations Observed.", 0);
			cf.setFocus(((EditText)findViewById(R.id.locobs_etxt)));
		}
		else
		{
			try
			{
				if(((Button)findViewById(R.id.addattic)).getText().toString().equals("Update")){
					cf.arr_db.execSQL("UPDATE  "+cf.DRY_attic+" SET atticnum='"+atticnostr+"',"+
				           "Accesspoint='"+acessstr+"',"+
				           "Accesspointotr='"+cf.encode(((EditText)findViewById(R.id.accesspt_etxt)).getText().toString())+"',"+
						   "Locationsobserved='"+cf.encode(((EditText)findViewById(R.id.locobs_etxt)).getText().toString())+"' Where atticId='"+Current_select_id+"' ");
					defaultattic();atticnospin.setSelection(0);atticnospin.setEnabled(true);
					cf.ShowToast("Attic updated successfully.", 0);((Button)findViewById(R.id.addattic)).setText("Save/Add More Attic");
					Showatticvalue();atint=0;
				}else{									
				chk_values();
				boolean[] b = {false};
				if(Dryattic_save.getCount()==0){
					
				}else{
				if(attic_in_DB!=null)
				{
					for(int i=0;i<attic_in_DB.length;i++)
					{
						if((attic_in_DB[i].equals(atticnostr)))
						{
							b[0]=true;
						}
					}
				}
				}
				if(Dryattic_save.getCount()==4){
					defaultattic();atticnospin.setSelection(0);atticnospin.setEnabled(true);
					cf.ShowToast("Limit Exceeds! You have already added 4 Attic spaces.", 0);
				}else{
					if(!b[0]){cf.arr_db.execSQL("INSERT INTO "
							+ cf.DRY_attic
							+ " (fld_srid,atticnum,Accesspoint,Accesspointotr,Locationsobserved)"
							+ "VALUES('"+cf.selectedhomeid+"','"+atticnostr+"','"+acessstr+"',"+
							"'"+cf.encode(((EditText)findViewById(R.id.accesspt_etxt)).getText().toString())+"',"+
							"'"+cf.encode(((EditText)findViewById(R.id.locobs_etxt)).getText().toString())+"')");
					 cf.ShowToast("Attic added Successfully.", 1);
					 defaultattic();atticnospin.setSelection(0);atticnospin.setEnabled(true);
					
					}else{cf.ShowToast("Already exists! Please select another Attic space.", 0);
					 defaultattic();atticnospin.setSelection(0);atticnospin.setEnabled(true);
					}
						Showatticvalue();
					
				}
			  }
			}
			catch(Exception e){}
		}
	}
	private void Showatticvalue() {
		// TODO Auto-generated method stub
		   
		   Cursor attc_retrieve= cf.SelectTablefunction(cf.DRY_attic, " Where fld_srid='"+cf.selectedhomeid+"' ");
	       if(attc_retrieve.getCount()>0)
			{
	    	   ((LinearLayout)findViewById(R.id.atticlin)).setVisibility(cf.v1.VISIBLE);
	    	   if(!((EditText)findViewById(R.id.atticcomment)).getText().toString().trim().equals("")){((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);}
	    	   attc_retrieve.moveToFirst();
				try
				{
					attictblshow.removeAllViews();
				}
				catch (Exception e) {
					// TODO: handle exception
				}
				LinearLayout h1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvac_listview_header, null); 
				TableRow th = (TableRow)h1.findViewById(R.id.attic);th.setVisibility(cf.v1.VISIBLE);
				TableRow thv = (TableRow)h1.findViewById(R.id.hvac);thv.setVisibility(cf.v1.GONE);
				TableLayout.LayoutParams lp = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
			 	lp.setMargins(2, 0, 2, 2); h1.removeAllViews(); 
			 	th.setPadding(10, 0, 0, 0);
			 	
			 	attictblshow.addView(th,lp);
				attic_in_DB=new String[attc_retrieve.getCount()];
				attictblshow.setVisibility(View.VISIBLE); 
				
				for(int i=0;i<attc_retrieve.getCount();i++)
				{
					TextView no,acspt,locobsvd,aticspace;
					ImageView edit,delete;
					String acs="",loc="",atsp="",acsotr="";
					atsp=attc_retrieve.getString(2);
					acs=attc_retrieve.getString(3);
					acsotr=cf.decode(attc_retrieve.getString(4));
					loc=cf.decode(attc_retrieve.getString(5));
					
					attic_in_DB[i]=atsp;
					
					LinearLayout t1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvac_listview, null);
					TableRow hv = (TableRow)t1.findViewById(R.id.hvac);hv.setVisibility(cf.v1.GONE);
					TableRow t = (TableRow)t1.findViewById(R.id.attic);t.setVisibility(cf.v1.VISIBLE);
					t.setId(44444+i);/// Set some id for further use
				 	
				 	no= (TextView) t.findViewWithTag("AtticNo");
				 	no.setText(String.valueOf(i+1));
				 	aticspace= (TextView) t.findViewWithTag("AtticSpace");
				 	aticspace.setText(atsp);
				 	acspt= (TextView) t.findViewWithTag("Accesspoint");
				 	if(acs.equals("Other"))
				 	{
				 		acspt.setText(acs+"("+acsotr+")");
				 	}
				 	else
				 	{
				 		acspt.setText(acs);
				 	}
				 	
				 	locobsvd= (TextView) t.findViewWithTag("LocationsObserved");
				 	locobsvd.setText(loc);
				 	
				 	edit= (ImageView) t.findViewWithTag("edit");
				 	edit.setId(789456+i);
				 	edit.setTag(attc_retrieve.getString(0));
	                edit.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
						try
						{
						int i=Integer.parseInt(v.getTag().toString());
						  atint=1;updateattic(i);
						}
						catch (Exception e) {
							// TODO: handle exception
						}
						}
	                });
						
				 	delete=(ImageView) t.findViewWithTag("del");
				 	delete.setTag(attc_retrieve.getString(0));
	                delete.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(final View v) {
							// TODO Auto-generated method stub
							
							AlertDialog.Builder builder = new AlertDialog.Builder(DrywallAttic.this);
							builder.setMessage("Do you want to delete the selected Attic Space?")
									.setCancelable(false)
									.setPositiveButton("Yes",
											new DialogInterface.OnClickListener() {

												public void onClick(DialogInterface dialog,
														int id) {
													
													try
													{
													int i=Integer.parseInt(v.getTag().toString());
													if(i==Current_select_id)
													{
														Current_select_id=0;
													}
													cf.arr_db.execSQL("Delete from "+cf.DRY_attic+" Where atticId='"+i+"'");
													cf.ShowToast("Attic unit deleted successfully.", 0);
													defaultattic();atticnospin.setSelection(0);atticnospin.setEnabled(true);
											         ((Button)findViewById(R.id.addattic)).setText("Save/Add More Attic");
													Showatticvalue();atint=0;
													}
													catch (Exception e) {
														// TODO: handle exception
													}
												}
											})
									.setNegativeButton("No",
											new DialogInterface.OnClickListener() {
												public void onClick(DialogInterface dialog,
														int id) {

													dialog.cancel();
												}
											});
							AlertDialog al=builder.create();
							al.setTitle("Confirmation");
							al.setIcon(R.drawable.alertmsg);
							al.setCancelable(false);
							al.show();
							
						}
					});
				 	t1.removeAllViews();
					t.setPadding(10, 0, 0, 0);
					attictblshow.addView(t,lp);
					attc_retrieve.moveToNext();
				}
			}else{ ((LinearLayout)findViewById(R.id.atticlin)).setVisibility(cf.v1.GONE);
			((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.GONE); }

	}
	protected void updateattic(int i) {
		// TODO Auto-generated method stub
		Current_select_id = i;atticnospin.setEnabled(false);
		try{Cursor atticret=cf.SelectTablefunction(cf.DRY_attic, " Where atticId='"+i+"'");
		if(atticret.getCount()>0)
		{
			atticret.moveToFirst();
			String acs="",loc="",atsp="",acsotr="";
			atsp=atticret.getString(2);
			acs=atticret.getString(3);
			acsotr=cf.decode(atticret.getString(4));
			loc=cf.decode(atticret.getString(5));
			
			atticnospin.setSelection(atcnoadapter.getPosition(atsp));
			atticspcspin.setSelection(atcspcadapter.getPosition(acs));
			if(acs.equals("Other"))
			{
				((EditText)findViewById(R.id.accesspt_etxt)).setVisibility(cf.v1.VISIBLE);
				((EditText)findViewById(R.id.accesspt_etxt)).setText(acsotr);
			}
			else
			{
				((EditText)findViewById(R.id.accesspt_etxt)).setVisibility(cf.v1.INVISIBLE);
			}
			((EditText)findViewById(R.id.locobs_etxt)).setText(loc);
			((Button)findViewById(R.id.addattic)).setText("Update");
		}
		}catch(Exception e){}
	}
	private void chk_values() {
		// TODO Auto-generated method stub
		try
		{
			Drysum_save=cf.SelectTablefunction(cf.DRY_sumcond, " where fld_srid='"+cf.selectedhomeid+"'");
		   
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		
		try{
			Dryattic_save=cf.SelectTablefunction(cf.DRY_attic, " where fld_srid='"+cf.selectedhomeid+"'");
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}
	public void defaultattic() {
		// TODO Auto-generated method stub
		atticspcspin.setSelection(0);
		((EditText)findViewById(R.id.accesspt_etxt)).setText("");
		((EditText)findViewById(R.id.accesspt_etxt)).setVisibility(cf.v1.INVISIBLE);
		((EditText)findViewById(R.id.locobs_etxt)).setText("");
		
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			cf.goclass(64);
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		  if(requestCode==cf.loadcomment_code)
		  {
			load_comment=true;
			if(resultCode==RESULT_OK)
			{
				 cf.ShowToast("Comments added successfully.", 0);
				 String atccomts = ((EditText)findViewById(R.id.atticcomment)).getText().toString();
				 ((EditText)findViewById(R.id.atticcomment)).setText((atccomts+" "+data.getExtras().getString("Comments")).trim());
				
			}
			
		}
    }
}
