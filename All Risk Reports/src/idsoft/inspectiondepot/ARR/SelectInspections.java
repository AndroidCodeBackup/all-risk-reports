package idsoft.inspectiondepot.ARR;


import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.LoadComments.liClick;
import idsoft.inspectiondepot.ARR.MultiSpinner.MultiSpinnerListener;

import java.util.ArrayList;


import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.TextView;

public class SelectInspections extends Activity{
	CommonFunctions cf;
	String sel_inspection;String inspectionname[],localinspid[];
	CheckedTextView [] chk=new CheckedTextView[7];
	String [] chk_txt=new String[7];
	Cursor c=null;
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        cf=new CommonFunctions(this);
		    Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.onlinspectionid = extras.getString("InspectionType");
				cf.onlstatus = extras.getString("status");
				cf.selectedhomeid = extras.getString("homeid");
		 	} 
	        setContentView(R.layout.selctinspection);
	        cf.getInspectorId();
	        chk[0]=(CheckedTextView) findViewById(R.id.select_insp_chk1);
	        chk[1]=(CheckedTextView) findViewById(R.id.select_insp_chk2);
	        chk[2]=(CheckedTextView) findViewById(R.id.select_insp_chk3);
	        chk[3]=(CheckedTextView) findViewById(R.id.select_insp_chk4);
	        chk[4]=(CheckedTextView) findViewById(R.id.select_insp_chk7);
	        chk[5]=(CheckedTextView) findViewById(R.id.select_insp_chk8);
	        chk[6]=(CheckedTextView) findViewById(R.id.select_insp_chk9);
	       
	        try
	        {
		        cf.CreateARRTable(14);
		        Cursor c1  =cf.SelectTablefunction(cf.inspnamelist, " ORDER BY ARR_Custom_ID");
		        inspectionname=new String[c1.getCount()];
		        localinspid=new String[c1.getCount()];
		        if(c1.getCount()>0)
		        {
		        	c1.moveToFirst();
		        	int i=0;
		        	do
		        	{
		        		inspectionname[i] = cf.decode(c1.getString(c1.getColumnIndex("ARR_Insp_Name")));
		        		localinspid[i] = cf.decode(c1.getString(c1.getColumnIndex("ARR_Custom_ID")));
		        		i++;
		        	}while(c1.moveToNext());
		        }
	        }
	        catch(Exception e)
	        {
	        
	        }
	        
	        for(int i=0;i<chk_txt.length;i++)
	        {
	        	chk[i].setText(inspectionname[i]);
	        	chk_txt[i]=localinspid[i];
	        	
	        }
	        
	        for(int j=0;j<chk.length;j++)
	        {
	        	chk[j].setOnClickListener(new liClick());
	        }
	       
	        c =cf.SelectTablefunction(cf.Select_insp, " Where SI_srid='"+cf.selectedhomeid+"' and SI_InspectorId='"+cf.Insp_id+"'");
	        c.moveToFirst();
	        if(c.getCount()>0)
	        {
	        
	        	String s=cf.decode(c.getString(c.getColumnIndex("SI_InspectionNames")));
	        	System.out.println("the encr "+s);	        	
	        	//s=s.replace("^", ",");
	        	
	        	String Temp[]=s.split(",");
	        	if(Temp.length>1)
	        	{
	        		for(int i=0;i<Temp.length;i++)
	        		{
	        			for(int j=0;j<chk_txt.length;j++)
	        			{
	        				

	        				
	        			/*	if(Temp[i].contains("Commercial Type I") || Temp[i].contains("Commercial Type II") || Temp[i].contains("Commercial Type III"))
	        	        	{
	        	        		Temp[i]="Commercial Inspection";
	        	        	}*/
	        				System.out.println("chk="+chk_txt[j]+"tmep"+Temp[i]);
	        				if(Temp[i].trim().equals(chk_txt[j].trim()))
	        				{
	        					chk[j].setChecked(true);
	        					break;
	        				}
	        			}
	        			
	        		}
	        	}
	        	else
	        	{
	        			for(int j=0;j<chk_txt.length;j++)
	        			{
	        				/*if(s.contains("Commercial Type I") || s.contains("Commercial Type II") || s.contains("Commercial Type III"))
	        	        	{
	        	        		s="Commercial Inspection";
	        	        	}*/
	        				System.out.println("s="+s+"chk_txt"+chk_txt[j]);
	        				s= s.replace(",","");
	        				if(s.trim().equals(chk_txt[j].trim()))
	        				{
	        					chk[j].setChecked(true);
	        					break;	        		
	        				}
	        			}
	        	}
	        	
	        	
	        } 
	        else
	        {
	        	
	        }
	        ImageView im =(ImageView) findViewById(R.id.head_insp_info);
			im.setVisibility(View.VISIBLE);
			im.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					    Intent s2= new Intent(SelectInspections.this,PolicyholdeInfoHead.class);
						Bundle b2 = new Bundle();
						s2.putExtra("homeid", cf.selectedhomeid);
						s2.putExtra("Type", "Inspector");
						s2.putExtra("insp_id", cf.Insp_id);
						((Activity) cf.con).startActivityForResult(s2, cf.info_requestcode);
				}
			});
	    }
	 public void clicker(View v)
	 {
		switch (v.getId()) {
		case R.id.start:
			sel_inspection="";cf.CreateARRTable(3);cf.getPolicyholderInformation(cf.selectedhomeid);
			System.out.println("cf.s"+cf.Stories);
			
			try
			{
				if(!cf.Stories.equals(""))
				{
					if(cf.Stories.equals("1") || cf.Stories.equals("2")  || cf.Stories.equals("3"))
					{
						chk_txt[3]="Commercial Type I";		
					}
					else if(cf.Stories.equals("4") || cf.Stories.equals("5")  || cf.Stories.equals("6"))
					{
						chk_txt[3]="Commercial Type II";		
					}
					else if(Integer.parseInt(cf.Stories)>7)
					{	
						chk_txt[3]="Commercial Type III";
					}
				}
			}
			catch(Exception e)
			{
				
			}
			
			for(int i=0;i<chk.length;i++)
			{
				if(chk[i].isChecked())
				{
					sel_inspection+=localinspid[i]+",";
				} 
			}
			System.out.println("the encrfipetedvfdsgd +"+sel_inspection);
			if(!sel_inspection.trim().equals("") && !sel_inspection.trim().equals("--Select--"))
			{	
				String sel_inspection1 = sel_inspection.substring(0, sel_inspection.length() - 1);System.out.println("sel_inspection1"+sel_inspection1);	
			     if(sel_inspection1.equals("--Select--"))
				{
					cf.ShowToast("Please select atleast one Inspection.", 1);
				}
				else
				{
					cf.CreateARRTable(2);
					
					try
					{
						Cursor SI_c=cf.SelectTablefunction(cf.Select_insp, " WHERE SI_srid ='"+cf.selectedhomeid+"'");
						System.out.println("the encr in save "+cf.decode(sel_inspection1));
						if(SI_c.getCount()>0)
						{
							cf.arr_db.execSQL(" UPDATE  "+cf.Select_insp+" SET SI_InspectionNames='"+cf.encode(sel_inspection1)+"' WHERE SI_srid='"+cf.selectedhomeid+"'");
							System.out.println(" UPDATE  "+cf.Select_insp+" SET SI_InspectionNames='"+cf.encode(sel_inspection1)+"' WHERE SI_srid='"+cf.selectedhomeid+"'");
						}
						else
						{
							cf.arr_db.execSQL(" INSERT INTO "+cf.Select_insp +" (SI_InspectorId,SI_srid,SI_InspectionNames) VALUES('"+cf.Insp_id+"','"+cf.selectedhomeid+"','"+cf.encode(sel_inspection1)+"')");
							System.out.println(" INSERT INTO "+cf.Select_insp +" (SI_InspectorId,SI_srid,SI_InspectionNames) VALUES('"+cf.Insp_id+"','"+cf.selectedhomeid+"','"+cf.encode(sel_inspection1)+"')");
						}
					    Intent intimg = new Intent(this,PolicyHolder.class);
						intimg.putExtra("homeid", cf.selectedhomeid);
						intimg.putExtra("InspectionType", cf.onlinspectionid);
						intimg.putExtra("status", cf.onlstatus);
						startActivity(intimg);
					}catch (Exception e) {
						// TODO: handle exception
						
					}
				}
			}
			else
			{
				cf.ShowToast("Please select the Inspection.", 1);
			}
			break;
		case R.id.back:
			Intent intimg = new Intent(this,HomeOwnerList.class);
			intimg.putExtra("homeid", cf.selectedhomeid);
			intimg.putExtra("InspectionType", cf.onlinspectionid);
			intimg.putExtra("status", cf.onlstatus);
			startActivity(intimg);
			break;

		default:
			break;
		}
	 }
	 
	 public boolean onKeyDown(int keyCode, KeyEvent event) {
			if (keyCode == KeyEvent.KEYCODE_BACK) {
				Intent intimg = new Intent(this,HomeOwnerList.class);
				intimg.putExtra("homeid", cf.selectedhomeid);
				intimg.putExtra("InspectionType", cf.onlinspectionid);
				intimg.putExtra("status", cf.onlstatus);
				startActivity(intimg);
				return true;
			}
			return super.onKeyDown(keyCode, event);
		}
	 class liClick implements android.view.View.OnClickListener
		{
			
			@Override
			public void onClick(View v) {
				if(((CheckedTextView) v).isChecked())
					((CheckedTextView) v).setChecked(false);
				else
					((CheckedTextView) v).setChecked(true);
				}
			
		}
}
