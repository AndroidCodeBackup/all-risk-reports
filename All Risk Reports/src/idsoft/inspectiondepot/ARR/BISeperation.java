package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.text.style.BulletSpan;
import android.view.KeyEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;

public class BISeperation extends Activity {
	CommonFunctions cf;
	Cursor c1;
	String buildingsep_value="",BuildSep_NA="",BuildSep="",bsnotappl="",ws_value="",BI_SEPERATION="",buildseprdgtxt="";
	public CheckBox buildingsepchk[] = new CheckBox[8];
	public int[] chkbuildingsepid = {R.id.bildsep_chk1,R.id.bildsep_chk2,R.id.bildsep_chk3,R.id.bildsep_chk4,R.id.bildsep_chk5,R.id.bildsep_chk6,R.id.bildsep_chk7,R.id.bildsep_chk8};
	public CheckBox buildsepwschk[] = new CheckBox[5];
	public int[] chkwallstructureid = {R.id.bildsep_chk9,R.id.bildsep_chk10,R.id.bildsep_chk11,R.id.bildsep_chk12,R.id.bildsep_chk13};
	RadioGroup buildseprdgpval;
	boolean bschk=false,eptybool=false;
	public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	       
	        cf = new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);				
		 	}
	        setContentView(R.layout.buildseperation);
	        cf.getDeviceDimensions();
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
         	hdr_layout.addView(new HdrOnclickListener(this,1,"General","Building Info","Building Separation",1,1,cf));
         	LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
         	main_layout.setMinimumWidth(cf.wd);
         	main_layout.addView(new MyOnclickListener(getApplicationContext(), 1, 0,0,cf));
            LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
            submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 16, 1,0,cf));
	        LinearLayout submenu2_layout = (LinearLayout) findViewById(R.id.submenu1); 
	        submenu2_layout.addView(new MyOnclickListener(getApplicationContext(), 457, 1,0,cf));
	       /* TableRow tblrw = (TableRow)findViewById(R.id.row2);
	        tblrw.setMinimumHeight(cf.ht); */
	        cf.CreateARRTable(61);
	        Declaration();
	        setValue();
	    }
	private void Declaration() {
		// TODO Auto-generated method stub
		cf.setupUI((ScrollView) findViewById(R.id.scr));
		 for(int i=0;i<buildingsepchk.length;i++)
	   	 {
	   		buildingsepchk[i] = (CheckBox)findViewById(chkbuildingsepid[i]);   	
	   		
	   	 }	
		 for(int i=0;i<buildsepwschk.length;i++)
	   	 {
			 buildsepwschk[i] = (CheckBox)findViewById(chkwallstructureid[i]);   		
	   	 }	
		 buildseprdgpval = (RadioGroup)findViewById(R.id.buildsep_rdgp);
		  ((EditText)findViewById(R.id.edbuildsepother)).addTextChangedListener(new CustomTextWatcherSpace(((EditText)findViewById(R.id.edbuildsepother))));
			 
	}
	private void setValue() 
	{		
		selecttbl();
		if(c1.getCount()==0)
		{
			((CheckBox)findViewById(R.id.buildsepnotappl)).setChecked(true);
			((LinearLayout)findViewById(R.id.buildseplin)).setVisibility(cf.v1.GONE);
		}
		else
		{
			if(BuildSep_NA.equals("0") || BuildSep_NA.equals(""))
			{
				((LinearLayout)findViewById(R.id.buildseplin)).setVisibility(cf.v1.VISIBLE);
				if(!BuildSep.equals(""))
				{
					String BuildSep_split[] = BuildSep.split("&#126;");
					cf.setvaluechk1(BuildSep_split[0],buildingsepchk,((EditText)findViewById(R.id.edbuildsepother)));
					cf.setvaluechk1(BuildSep_split[1],buildsepwschk,((EditText)findViewById(R.id.edbuildsepother)));
					((RadioButton) buildseprdgpval.findViewWithTag(BuildSep_split[2])).setChecked(true);
					((TextView)findViewById(R.id.savetxtbuildsep)).setVisibility(cf.v1.VISIBLE);
				}
				else
				{
					((LinearLayout)findViewById(R.id.buildseplin)).setVisibility(cf.v1.GONE);
					((TextView)findViewById(R.id.savetxtbuildsep)).setVisibility(cf.v1.GONE);
					((CheckBox)findViewById(R.id.buildsepnotappl)).setChecked(true);
				}
			}
			else
			{
				((CheckBox)findViewById(R.id.buildsepnotappl)).setChecked(true);
				((LinearLayout)findViewById(R.id.buildseplin)).setVisibility(cf.v1.GONE);
				((TextView)findViewById(R.id.savetxtbuildsep)).setVisibility(cf.v1.VISIBLE);
			}
		}
	}
	private void selecttbl() {
		c1= cf.SelectTablefunction(cf.BI_General, " where fld_srid='"+cf.selectedhomeid+"'");
		if(c1.getCount()>0)
		{
			c1.moveToFirst();
			 BuildSep = cf.decode(c1.getString(c1.getColumnIndex("BI_BUILD_SEP")));
			 BuildSep_NA = c1.getString(c1.getColumnIndex("BI_BUILDSEP_NA"));
		}
	}
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.bildsep_chk8:
			if(buildingsepchk[7].isChecked())
			{
				buildsepwschk[3].setChecked(true);
				((RadioButton)buildseprdgpval.findViewWithTag("Not Applicable")).setChecked(true);
			}
			else
			{
				buildsepwschk[3].setChecked(false);
				eptybool = true;
				 try{
						if(eptybool){buildseprdgpval.clearCheck();}
					 }catch(Exception e){}
				
			}
			
			break;
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.mouseoverid:
			cf.ShowToast("Use for Commercial General Conditions  and Hazards and Replacement cost only.",0);
			break;
		case R.id.bildsep_chk13:
			if(((CheckBox)findViewById(R.id.bildsep_chk13)).isChecked())
			{
				((EditText)findViewById(R.id.edbuildsepother)).setVisibility(cf.v1.VISIBLE);
				cf.setFocus(((EditText)findViewById(R.id.edbuildsepother)));
			}
			else{
				((EditText)findViewById(R.id.edbuildsepother)).setVisibility(cf.v1.GONE);
				((EditText)findViewById(R.id.edbuildsepother)).setText("");
			}		
			break;
		case R.id.buildsepsave:
		 selecttbl();
		 if(((CheckBox)findViewById(R.id.buildsepnotappl)).isChecked()==false)
		 {
			buildingsep_value= cf.getselected_chk(buildingsepchk);
			 ws_value= cf.getselected_chk(buildsepwschk);
			 String buildingsepotrval =(buildsepwschk[buildsepwschk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.edbuildsepother)).getText().toString():""; 
			 ws_value+=buildingsepotrval;
				if(buildingsep_value.equals(""))
				 {
					 cf.ShowToast("Please select the option for Building Separation." , 0);
				 }
				 else
				 {
					 if(ws_value.equals(""))
					 {
						 cf.ShowToast("Please select the option for Wall Structure." , 0);
					 }
					 else
					 {
						 if(buildsepwschk[buildsepwschk.length-1].isChecked())
						 {	 
							 if(buildingsepotrval.trim().equals("&#40;"))
							 {
								 cf.ShowToast("Please enter the other text for Wall Structure.", 0);
								 cf.setFocus((EditText)findViewById(R.id.edbuildsepother));
							 }
							 else
							 {
								 PartywallValidation();
							 }							 
						 }
						 else
						 {
							 PartywallValidation();	
						 }
					 }
				 }
			}
		else
		{
			InsertBuildingSep();
		}
		 break;
		case R.id.buildsepnotappl:			
			if(((CheckBox)findViewById(R.id.buildsepnotappl)).isChecked())
			{
				bsnotappl="1";
				if(!BuildSep.equals(""))
				{
					AlertDialog.Builder builder = new AlertDialog.Builder(BISeperation.this);
					builder.setTitle("Confirmation");
					builder.setIcon(R.drawable.alertmsg);
					builder.setMessage("Do you want to clear the Building Separation data?").setCancelable(false)
							.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,int id1) 
										{
												buildsepclear();
												((CheckBox)findViewById(R.id.buildsepnotappl)).setChecked(true);
												((LinearLayout)findViewById(R.id.buildseplin)).setVisibility(cf.v1.GONE);
												((TextView)findViewById(R.id.savetxtbuildsep)).setVisibility(cf.v1.GONE);
										}
									})
							.setNegativeButton("No",new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,	int id) {
											
											((CheckBox)findViewById(R.id.buildsepnotappl)).setChecked(false);
											dialog.cancel();											
									}
							});
					builder.show();
				}
				else
				{
					((TextView)findViewById(R.id.savetxtbuildsep)).setVisibility(cf.v1.GONE);
					((LinearLayout)findViewById(R.id.buildseplin)).setVisibility(cf.v1.GONE);
				}
				
			}
			else 
			{
				bsnotappl="0";buildsepclear();
				((LinearLayout)findViewById(R.id.buildseplin)).setVisibility(cf.v1.VISIBLE);
				((TextView)findViewById(R.id.savetxtbuildsep)).setVisibility(cf.v1.GONE);
			}
			break;
				
		}		
	}
	
	protected void buildsepclear() {
		// TODO Auto-generated method stub		
		 for(int i=0;i<buildingsepchk.length;i++)
	   	 {
			 buildingsepchk[i].setChecked(false);		
	   	 }	
		 for(int i=0;i<buildsepwschk.length;i++)
	   	 {
			 buildsepwschk[i].setChecked(false);   		
	   	 }	
		((EditText)findViewById(R.id.edbuildsepother)).setText("");
		((EditText)findViewById(R.id.edbuildsepother)).setVisibility(cf.v1.GONE);			
		bschk=true;
		try{if(bschk){buildseprdgpval.clearCheck();}}catch(Exception e){}BuildSep="";
	}
	
	private void PartywallValidation() {
		// TODO Auto-generated method stub
		int buildseprdgpid= buildseprdgpval.getCheckedRadioButtonId();	
		buildseprdgtxt = (buildseprdgpid==-1)? "":((RadioButton) findViewById(buildseprdgpid)).getText().toString();
		if(buildseprdgtxt.equals(""))
		{
			cf.ShowToast("Please select the option for Party Wall extends beyond Front.",1);
		}
		else
		{
			InsertBuildingSep();
		}
	}
	private void InsertBuildingSep() {
		if(!buildseprdgtxt.equals("") && !ws_value.equals("") && !buildingsep_value.equals(""))
		{
			BI_SEPERATION = buildingsep_value+"&#126;"+ws_value+"&#126;"+buildseprdgtxt;
		}
		else
		{
			BI_SEPERATION="";
		}
		if(((CheckBox)findViewById(R.id.buildsepnotappl)).isChecked()==true)
		{
			bsnotappl="1";BI_SEPERATION="";
		}
		else
		{
			bsnotappl="0";
		}
		
		updatenotappl();
		cf.ShowToast("Building Separation saved successfully",1);
		//((TextView)findViewById(R.id.savetxtbuildsep)).setVisibility(cf.v1.VISIBLE);
		//cf.goclass(461);
	}
	private void updatenotappl()
	{
		try
		{			
			selecttbl();
			if(c1.getCount()==0)
			{
				cf.arr_db.execSQL("INSERT INTO " + cf.BI_General + " (fld_srid,BI_BUILD_SEP,BI_BUILDSEP_NA)"
						+ "VALUES ('"+cf.selectedhomeid+"','"+cf.encode(BI_SEPERATION)+"','"+bsnotappl+"')");
			}
			else
			{
				cf.arr_db.execSQL("UPDATE "+ cf.BI_General+ " SET BI_BUILD_SEP='"+cf.encode(BI_SEPERATION)+"',BI_BUILDSEP_NA='"+bsnotappl+"'"
						+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
			}
		}
		catch(Exception e)
		{
			System.out.println("ins "+e.getMessage());
		}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			cf.goclass(456);
			return true;
		}		
		return super.onKeyDown(keyCode, event);
	}
}