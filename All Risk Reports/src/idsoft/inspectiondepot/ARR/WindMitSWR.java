/*
************************************************************
* Project: ALL RISK REPORT
* Module : WindMitSWR.java
* Creation History:
Created By : Nalini on 10/24/2012
* Modification History:
Last Modified By : Gowri on 11/28/2012
************************************************************ 
*/
package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class WindMitSWR extends Activity {
	CommonFunctions cf;
	boolean load_comment=true;
	RadioButton swroptA,swroptB,swroptC;

	String rdiochk="",updatecnt="",tmp="";
	EditText swrcomment;
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         cf=new CommonFunctions(this);
       
	 	Bundle extras = getIntent().getExtras();
		if (extras != null) {
			cf.getExtras(extras);				
	 	}
		setContentView(R.layout.windmitswr);
        cf.getDeviceDimensions();
        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
        if(cf.identityval==31)
        {
          hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","B1 1802(Rev.01/12)","SWR",3,1,cf));
        }
        else if(cf.identityval==32)
        { 
        	hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","Commercial Type I","SWR",3,1,cf));
        }
		LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
     	main_layout.setMinimumWidth(cf.wd);
     	main_layout.addView(new MyOnclickListener(getApplicationContext(), 3, 0,0,cf));
       
        cf.type_submenulayout =(LinearLayout) findViewById(R.id.typesubmenu); //** WINDMIT TYPE SUBMENU **//
        cf.type_submenulayout.addView(new MyOnclickListener(this, 300, 1,0, cf));
        cf.windmittypechk(cf.identityval,((RadioButton)findViewById(R.id.b1802_chk)),((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((TextView)findViewById(R.id.commtxt)),((ImageView)findViewById(R.id.editbno))); // To check the checkbox of choosen inspection type
        cf.uncheckcommercial(((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((RadioButton)findViewById(R.id.b1802_chk))); 
    	           
        cf.submenu_layout = (LinearLayout) findViewById(R.id.submenu);//** WINDMIT TYPE SUBMENU **//	        
        cf.submenu_layout .addView(new MyOnclickListener(this,cf.identityval,1,306, cf));
        
        cf.mainlayout = (LinearLayout)findViewById(R.id.bc_table);
        cf.save = (Button)findViewById(R.id.save);
        cf.tblrw = (TableRow)findViewById(R.id.row2);
        cf.tblrw.setMinimumHeight(cf.ht);			
		cf.CreateARRTable(31);
		cf.CreateARRTable(32);
		Declarations();
		SWR_setValue();
    }
	private void SWR_setValue() {
		// TODO Auto-generated method stub
		try
		{
		   Cursor c1=cf.SelectTablefunction(cf.Wind_Questiontbl, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
		  if(c1.getCount()>0)
		   {  
			   c1.moveToFirst();
			   int swrvalue = c1.getInt(c1.getColumnIndex("fld_swr"));
			   rdiochk=String.valueOf(swrvalue);
			   if(swrvalue==1 || swrvalue==2 || swrvalue==3)
			   {
				   if(swrvalue==1)   	  {  swroptA.setChecked(true);  }
				   else if(swrvalue==2)   {  swroptB.setChecked(true);  }
				   else if(swrvalue==3)   {  swroptC.setChecked(true);  }
				   ((TextView) findViewById(R.id.savetxtswr)).setVisibility(cf.v1.VISIBLE);
			   }
			   else
			   {
				   ((TextView) findViewById(R.id.savetxtswr)).setVisibility(cf.v1.GONE); 
			   }
		   }	
		  else
		  {
			  ((TextView) findViewById(R.id.savetxtswr)).setVisibility(cf.v1.GONE);			  
		  }
		   Cursor c2=cf.SelectTablefunction(cf.Wind_QuesCommtbl, " where fld_srid='"+cf.selectedhomeid+"'  and insp_typeid='"+cf.identityval+"'");
		   if(c2.getCount()>0)
		   {  
			   c2.moveToFirst();
			   ((EditText)findViewById(R.id.swrcomment)).setText(cf.decode(c2.getString(c2.getColumnIndex("fld_swrcomments"))));
			}		   
		}
	    catch (Exception E)
		{
	    	System.out.println("swr "+E.getMessage());
			String strerrorlog="Retrieving SWR - WINDMIT";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
	private void Declarations() {
		// TODO Auto-generated method stub
		cf.setupUI((ScrollView) findViewById(R.id.scr));
		((TextView)findViewById(R.id.swrheader)).setText(Html
				.fromHtml("(Standard Underlayments or hot mopped felts are not SWR)"));
		
		swroptA = (RadioButton) findViewById(R.id.SWRoptionA);	swroptA.setOnClickListener(OnClickListener);
		swroptB = (RadioButton) findViewById(R.id.SWRoptionB);	swroptB.setOnClickListener(OnClickListener);
		swroptC = (RadioButton) findViewById(R.id.SWRoptionC);	swroptC.setOnClickListener(OnClickListener);
		swrcomment = (EditText) findViewById(R.id.swrcomment);
		swrcomment.addTextChangedListener(new textwatcher());
		
		TextView helptxt = (TextView) findViewById(R.id.help);
		helptxt.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if (rdiochk==""){
					cf.alertcontent = "To see help comments, please select SWR options.";
				}
				else if (rdiochk.equals("1") || swroptA.isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of selection A, �SWR verified� of the OIR B1 -1802 Question 6 Secondary Water Resistance (SWR).";
				} else if (rdiochk.equals("2") || swroptB.isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of selection B, �No SWR verified�  of the OIR B1 -1802 Question 6 Secondary Water Resistance (SWR).";
				} else if (rdiochk.equals("3") || swroptC.isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of selection C, �unable to verify/unknown� of the OIR B1 -1802 Question 6 Secondary Water Resistance (SWR).";
				} else {
					cf.alertcontent = "To see help comments, please select SWR options.";
				}
				cf.showhelp("HELP",cf.alertcontent);
			}
		});
	}
	class textwatcher implements TextWatcher
    {
	         public int type;
	         textwatcher()
	    	{
	        	
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {


	    		// TODO Auto-generated method stub
	    		cf.showing_limit(s.toString(),(TextView)findViewById(R.id.swr_tv_type1),500); 
	}

	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,

	    			int after) { 
	    		// TODO Auto-generated method stub
	     	}

	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,

	    			int count) {
	    		// TODO Auto-generated method stub
	    	}
	   }	
	RadioButton.OnClickListener OnClickListener = new RadioButton.OnClickListener() {

		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.SWRoptionA:
				rdiochk = "1";
				swrcomment.setText("This home was verified as meeting the requirements of selection A, �SWR verified� of the OIR B1 -1802 Question 6 Secondary Water Resistance (SWR).");
				
				AlertDialog.Builder builder = new AlertDialog.Builder(WindMitSWR.this);
				builder.setMessage("Please confirm this home has a SWR protection that compiles with the 1802 requirements. Proper verification and documentation must be provided.")
				       .setCancelable(false)
				       .setPositiveButton("OK", new DialogInterface.OnClickListener() {
				           public void onClick(DialogInterface dialog, int id) {
				                //do things
				        	   dialog.cancel();
				           }
				       });
				
				builder.show();
				showhideoption();
				swroptA.setChecked(true);
				break;
			case R.id.SWRoptionB:
				rdiochk = "2";
				swrcomment.setText("This home was verified as meeting the requirements of selection B, �No SWR verified� of the OIR B1 -1802 Question 6 Secondary Water Resistance (SWR).");
				showhideoption();
				swroptB.setChecked(true);				
				break;
			case R.id.SWRoptionC:
				rdiochk = "3";
				swrcomment.setText("This home was verified as meeting the requirements of selection C, �unable to verify/unknown� of the OIR B1 -1802 Question 6 Secondary Water Resistance (SWR).");
				showhideoption();
				swroptC.setChecked(true);				
				break;
			}
		}
	};
	private void showhideoption()
	{
		swroptA.setChecked(false);
		swroptB.setChecked(false);
		swroptC.setChecked(false);
	}	
	public void clicker(View v) {
		switch(v.getId())
		{	
		case R.id.loadcomments:
		cf.findinspectionname(cf.identityval);SWRoption();
		int len=((EditText)findViewById(R.id.swrcomment)).getText().toString().length();			
		if(!tmp.equals(""))
		{
				if(load_comment)
				{
					load_comment=false;
					int loc1[] = new int[2];
					v.getLocationOnScreen(loc1);
					Intent in = cf.loadComments(tmp,loc1);
					in.putExtra("insp_name", cf.inspname);
					in.putExtra("insp_ques", "SWR");
					in.putExtra("length", len);
					in.putExtra("max_length", 500);
					startActivityForResult(in, cf.loadcomment_code);
				}
		}
		else
		{
			cf.ShowToast("Please select the Option for SWR.",0);
		}	
		break;
		case R.id.helpoptA:
			  cf.alerttitle="A - SWR";
			  cf.alertcontent="SWR (also called sealed roof deck) self-adhering, polymer-modified bitumen roofing underlayment applied directly to the sheathing or foam adhesive SWR barrier (not foamed-on insulation), applied as a supplemental means to protect the dwelling from water intrusion in the event of roof covering loss";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
			
		case R.id.helpoptB:
			  cf.alerttitle="B � No SWR";
			  cf.alertcontent="No SWR";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;		
			
		case R.id.helpoptC:
			  cf.alerttitle="C - Unknown";
			  cf.alertcontent="Unknown or undetermined";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		case R.id.hme:
    		cf.gohome();
    		break;
		
		case R.id.save:
			if (!swroptA.isChecked() && (!swroptB.isChecked())	&& (!swroptC.isChecked())) {
				cf.ShowToast("Please select the option for SWR.",0);

			} else if (swrcomment.getText().toString().trim().equals("")) {
				cf.ShowToast("Please enter the Comments for SWR.",0);
				cf.setFocus(swrcomment);
			} else {				
				 try
				 {
					 Cursor c1=cf.SelectTablefunction(cf.Wind_Questiontbl, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
					 if(c1.getCount()==0)
					 {
						 
						 cf.arr_db.execSQL("INSERT INTO "
									+ cf.Wind_Questiontbl
									+ " (fld_srid,insp_typeid,fld_bcyearbuilt,fld_bcpermitappdate,fld_buildcode,fld_roofdeck,fld_rdothertext," +
									"	fld_roofwall,fld_roofwallsub,fld_roofwallclipsminval,fld_roofwallclipssingminval,fld_roofwallclipsdoubminval,fld_rwothertext," +
									"	fld_roofgeometry ,fld_geomlength ,fld_roofgeomtotalarea ,fld_roofgeomothertext ,fld_swr,fld_optype,fld_openprotect,fld_opsubvalue,fld_oplevelchart," +
									"   fld_opGOwindentdoorval,fld_opGOgardoorval,fld_opGOskylightsval,fld_opGOglassblockval,fld_opNGOentrydoorval,fld_opNGOgaragedoorval, " +
									"	fld_wcvalue,fld_wcreinforcement,fld_quesmodifydate)"
									
									+ "VALUES ('" + cf.selectedhomeid + "','"+cf.identityval+"','','','','" + 0 + "','','" +
									+ 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0	+ "','','"+
									+ 0 + "','" + 0 + "','" + 0 + "','','" + rdiochk + "','"+ 0 +"','"+ 0 + "','" + 0 + "','" + 0 + "','" +
									+ 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','"+ 0 +"','" +
									+ 0 + "','" + 0 + "','"+ cf.datewithtime + "')");						 
					 }
					 else
					 {
						 cf.arr_db.execSQL("UPDATE " + cf.Wind_Questiontbl
									+ " SET fld_swr='" + rdiochk
									+ "',fld_quesmodifydate='"
									+ cf.datewithtime + "' WHERE fld_srid ='" + cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'");
					 }
					 c1.close();
					 	Cursor c2 = cf.SelectTablefunction(cf.Wind_QuesCommtbl, "where fld_srid='"+ cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'");
						if (c2.getCount() == 0) 
						{
							cf.arr_db.execSQL("INSERT INTO "
									+ cf.Wind_QuesCommtbl
									+ " (fld_srid,insp_typeid,fld_buildcodecomments,fld_roofcovercomments,fld_roofdeckcomments,fld_roofwallcomments," +
									"fld_roofgeometrycomments,fld_swrcomments,fld_openprotectioncomments,fld_wallconscomments,fld_insoverallcomments )"
									+ "VALUES('"+cf.selectedhomeid+"','"+cf.identityval+"','','','','','','"+cf.encode(swrcomment.getText().toString())+"','','','')");
						}
						else
						{
							cf.arr_db.execSQL("UPDATE " + cf.Wind_QuesCommtbl
									+ " SET fld_swrcomments='"+cf.encode(swrcomment.getText().toString())+ "' WHERE fld_srid ='" + cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'");
						
					}
						cf.ShowToast("SWR Details saved successfully.",0);
						cf.gotoclass(cf.identityval, WindMitWindowopenings.class);
				 }
				 catch (Exception E)
				 {
					String strerrorlog="Updating Questions Buildcode - FourPoint";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				 }						
			}
			
			break;
		}
	}
	private void SWRoption() {
		// TODO Auto-generated method stub
		if(swroptA.isChecked()){tmp ="SWR";}
		else if(swroptB.isChecked()){tmp ="No SWR";}
		else if(swroptC.isChecked()){tmp ="Unknown";}
		else {tmp="";}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
 		// replaces the default 'Back' button action
 		if (keyCode == KeyEvent.KEYCODE_BACK) {
 			cf.gotoclass(cf.identityval, WindMitRoofGeometry.class);
 			return true;
 		} 		
 		return super.onKeyDown(keyCode, event);
 	}
	 /**on activity result for the load comments**/
	 @Override
	 		protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	 			try
	 			{
	 				if(requestCode==cf.loadcomment_code)
	 				{
	 					load_comment=true;
	 					if(resultCode==RESULT_OK)
	 					{
	 						String swrcomm = swrcomment.getText().toString();	 
	 						swrcomment.setText(swrcomm +" "+data.getExtras().getString("Comments"));
	 					}
	 				}
	 			}
	 			catch (Exception e) {
		 			// TODO: handle exception
		 			System.out.println("the erro was "+e.getMessage());
	 			}
	 	}/**on activity result for the load comments ends**/
}