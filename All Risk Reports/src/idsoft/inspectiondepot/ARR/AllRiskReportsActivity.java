 /* Project: ALL RISK REPORT
   * Module : Submit.java
   * Creation History:
      Created By : Gowri on 
   * Modification History:
      Last Modified By : Gowri on 3/25/2013
   ************************************************************  
*/

package idsoft.inspectiondepot.ARR;


import idsoft.inspectiondepot.ARR.R;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketException;
import java.util.concurrent.TimeoutException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class AllRiskReportsActivity extends Activity {
	
	CommonFunctions cf;
	Cursor sel_logcur;
	int show_handler,remid=0;
	String chkstatus="";
	CheckBox remchk;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       
       
		cf = new CommonFunctions(this);
		 setContentView(R.layout.main);
        cf.CreateARRTable(1);
       
        /*
        
    	cf.arr_db.execSQL("INSERT INTO "
				+ cf.inspectorlogin
				+ " (Fld_InspectorId,Fld_InspectorFirstName,Fld_InspectorMiddleName,Fld_InspectorLastName,"+
				     "Fld_InspectorAddress,Fld_InspectorCompanyName,Fld_InspectorCompanyId,Fld_InspectorUserName,"+
				     "Fld_InspectorPassword,Fld_InspectorPhotoExtn,Android_status,Fld_InspectorFlag,"+
				     "Fld_Remember_pass,Fld_InspectorEmail,Fld_InspectorPEmail,Fld_signature,Fld_licenceno,Fld_licencetype,Fld_Phone)"
				+ " VALUES ('2445'," + 
				"'teslivetinspector1','','teslivetinspector1',"+
				"'','','2',"+
				"'teslivetinspector1','"+ cf.encode("Test1!!!!!1") + "','',"+
				"'true','1','',"+
				"'"+cf.encode("ksatheeshbabu@yahoo.com")+"','',"+
				"'','','','')");
		System.out.println("inserted succ");*/
		
		//  cf.CreateARRTable(3);
		
		
		//Call_NextLayout();
        try {
            Cursor c_chkinsp = cf.SelectTablefunction(cf.inspectorlogin,
					"where Fld_InspectorFlag='1'");
			int r_chkinsp = c_chkinsp.getCount();
			System.out.println("r_chkinsp="+r_chkinsp);
			if (r_chkinsp == 1) {
				Call_NextLayout();

			} else if (r_chkinsp > 1) {
				//** More than 0ne user logged in, so we log out the users **//*
				cf.arr_db.execSQL("update " + cf.inspectorlogin
						+ " set Fld_InspectorFlag=0 where Fld_InspectorFlag=1");
			}
		} catch (Exception e) {
		}
        
        ((AutoCompleteTextView) this.findViewById(R.id.eusername)).addTextChangedListener(new AF_watcher());
        ((EditText) this.findViewById(R.id.epwd)).setTransformationMethod(PasswordTransformationMethod.getInstance());
        
    }
    class AF_watcher implements TextWatcher
	{

		@Override
		public void afterTextChanged(Editable arg0) {
			// TODO Auto-generated method stub
			try {
				Cursor cur1 = cf.arr_db.rawQuery("select * from "+ cf.inspectorlogin + " where Fld_InspectorUserName like '" + cf.encode(((AutoCompleteTextView)findViewById(R.id.eusername)).getText().toString()) + "%'",null);
			    String[] autousername = new String[cur1.getCount()];
				cur1.moveToFirst();
				if(cur1.getCount()!=0)
				{
					if (cur1 != null) {
						int i = 0;
						do {
							autousername[i] = cf.decode(cur1.getString(cur1.getColumnIndex("Fld_InspectorUserName")));
							
							if (autousername[i].contains("null")) {
								autousername[i] = autousername[i].replace("null", "");
							}
							 
							i++;
						} while (cur1.moveToNext());
					}
					cur1.close();
				}
				 ArrayAdapter<String> adapter = new ArrayAdapter<String>(AllRiskReportsActivity.this,R.layout.loginnamelist,autousername);
				 ((AutoCompleteTextView)findViewById(R.id.eusername)).setThreshold(1);
				 ((AutoCompleteTextView)findViewById(R.id.eusername)).setAdapter(adapter);
				 Cursor cur2 = cf.arr_db.rawQuery("select * from "+ cf.inspectorlogin + " where Fld_InspectorUserName = '" + cf.encode(((AutoCompleteTextView)findViewById(R.id.eusername)).getText().toString()) + "' and Fld_Remember_pass='1'",null);
				 /** Set the password for the remember option enable**/
				 cur2.moveToFirst();
				 if(cur2.getCount()>0)
				 {
					if(cf.decode(cur2.getString(cur2.getColumnIndex("Fld_InspectorUserName"))).equals(((AutoCompleteTextView)findViewById(R.id.eusername)).getText().toString()) && cur2.getString(cur2.getColumnIndex("Fld_Remember_pass")).equals("1"))
					{
						((EditText)findViewById(R.id.epwd)).setText(cf.decode(cur2.getString(cur2.getColumnIndex("Fld_InspectorPassword"))));
						((Button)findViewById(R.id.clear)).setVisibility(View.VISIBLE);
					}
					else
					{
						((EditText)findViewById(R.id.epwd)).setText("");
						((Button)findViewById(R.id.clear)).setVisibility(View.GONE);
					}
					/** Set the password for the remember option enable Ends here **/
				 }
				 else
					{
					 ((EditText)findViewById(R.id.epwd)).setText("");
					 ((Button)findViewById(R.id.clear)).setVisibility(View.GONE);
					}
			}
			catch(Exception e)
			{
				
			}
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			
		}		 
	 }
    public void clicker(View v)
    {
    	    switch(v.getId())
    	    {
    	    case R.id.login:
    	    	//startActivity(new Intent(getApplicationContext(),HomeScreen.class));
    	    
    	    	if (!"".equals(((AutoCompleteTextView) this.findViewById(R.id.eusername)).getText().toString().trim())
    	    		|| !"".equals(((EditText) this.findViewById(R.id.epwd)).getText().toString().trim())) {
    	    		if (!"".equals(((AutoCompleteTextView) this.findViewById(R.id.eusername)).getText().toString().trim())) {
    	    			if (!"".equals(((EditText) this.findViewById(R.id.epwd)).getText().toString().trim())) {
    	    				
							try{
								sel_logcur = cf.SelectTablefunction(cf.inspectorlogin,
											"where Fld_InspectorUserName='"+cf.encode(((AutoCompleteTextView) this.findViewById(R.id.eusername)).getText().toString())+ "'");
                          	    int rws = sel_logcur.getCount();
                          	    if(rws==0)
                          	    {
                          	    	if(cf.isInternetOn())
									{
                          	    		retrieve_login_from_server();
									}
                          	    	else
                          	    	{
                          	    		cf.ShowToast("Internet connection not available.", 0);
                          	    	}
                          	    }
                          	    else
                          	    {
                          	    	sel_logcur.moveToFirst();
                          	    	if (((AutoCompleteTextView) this.findViewById(R.id.eusername)).getText().toString().equals(cf.decode(sel_logcur.getString(sel_logcur.getColumnIndex("Fld_InspectorUserName"))))
											&& ((EditText) this.findViewById(R.id.epwd)).getText().toString().equals(cf.decode(sel_logcur.getString(sel_logcur.getColumnIndex("Fld_InspectorPassword")))))
                      	    		{
                          	    		try
                          	    		{ 
                          	    			cf.arr_db.execSQL("UPDATE "
    												+ cf.inspectorlogin
    												+ " SET Fld_InspectorFlag=1"
    												+ " WHERE Fld_InspectorId ='"
    												+ sel_logcur.getString(sel_logcur.getColumnIndex("Fld_InspectorId")) + "'");
                          	    			Call_RememberPassword();
                          	    			
                          	    		}
                          	    		 catch (Exception e) {
											// TODO: handle exception
										}
										
                      	    		}
                          	    	else
                          	    	{
                          	    		cf.ShowToast("Invalid Username or Password.", 0);
                        				((AutoCompleteTextView)findViewById(R.id.eusername)).setText("");
                        				((EditText)findViewById(R.id.epwd)).setText("");
                        				((AutoCompleteTextView)findViewById(R.id.eusername)).requestFocus();
                          	    	}
                          	    }
							}catch (Exception e) {
								// TODO: handle exception
							}
        	    		}
        	    		else
        	    		{
        	    			cf.ShowToast("Please enter the Password.", 0);
        	    			((EditText) this.findViewById(R.id.epwd)).setText("");
            	    		((EditText) this.findViewById(R.id.epwd)).requestFocus();
        	    		}
    	    		}
    	    		else
    	    		{
    	    			cf.ShowToast("Please enter the Username.", 0);
    	    			((AutoCompleteTextView) this.findViewById(R.id.eusername)).setText("");
        	    		((AutoCompleteTextView) this.findViewById(R.id.eusername)).requestFocus();
    	    		}
    	    	}
    	    	else
    	    	{
    	    		cf.ShowToast("Please enter the Username and Password.", 0);
    	    		((AutoCompleteTextView) this.findViewById(R.id.eusername)).requestFocus();
    	    	}
    	    	break;
    	    case R.id.cancel:
    	    	Intent intent = new Intent(Intent.ACTION_MAIN);
				intent.addCategory(Intent.CATEGORY_HOME);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
				break;
    	  
    	    case R.id.clear:
    	    	try{
    	    		cf.arr_db.execSQL("UPDATE "
						+ cf.inspectorlogin
						+ " SET Fld_Remember_pass=0"
						+ " WHERE Fld_InspectorUserName ='"
						+ cf.encode(((AutoCompleteTextView) this.findViewById(R.id.eusername)).getText().toString())+ "'");
				 ((Button)findViewById(R.id.clear)).setVisibility(View.GONE);
				((EditText) this.findViewById(R.id.epwd)).setText("");
    	    	}
    	    	catch (Exception e) {
					// TODO: handle exception
				}
    	    	break;
    	    }
    }
    private void retrieve_login_from_server() throws TimeoutException,NetworkErrorException {
		// TODO Auto-generated method stub
    	cf.show_ProgressDialog("Processing ");
    	new Thread() {
			public void run(){
				Looper.prepare();
				try
				{
					SoapObject request = new SoapObject(cf.NAMESPACE,"CHECKUSERSTAUSAUTHENTICATION");
					SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
					envelope.dotNet = true;
					request.addProperty("UserName",((AutoCompleteTextView)findViewById(R.id.eusername)).getText().toString());
					request.addProperty("Password",((EditText)findViewById(R.id.epwd)).getText().toString());
					envelope.setOutputSoapObject(request);;
					HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL_ARR);System.out.println("request"+request);
					androidHttpTransport.call(cf.NAMESPACE+"CHECKUSERSTAUSAUTHENTICATION",envelope);
					SoapObject chklogin =(SoapObject) envelope.getResponse(); 
					System.out.println("chklogin="+chklogin);
					SoapObject obj = (SoapObject) chklogin.getProperty(0);
					String chkauth = String.valueOf(obj.getProperty("Loginstatus"));
					chkstatus = String.valueOf(obj.getProperty("Logindescription"));
					System.out.println("chkauth="+chkauth+"chkstatus="+chkstatus);
					if(chkauth.equals("true")) 
					{
						    String inspid = String.valueOf(obj.getProperty("UserId"));
						    
							try{
								Cursor cur = cf.SelectTablefunction(cf.inspectorlogin,
									" where Fld_InspectorId = '" + inspid
											+ "' and Fld_InspectorFlag='1'");
								if(cur.getCount()>0)
								{
									
								}
								else
								{
									try
									{
										SoapObject inspreq = new SoapObject(cf.NAMESPACE,"INSPECTORDETAILS");
										SoapSerializationEnvelope inspenv = new SoapSerializationEnvelope(SoapEnvelope.VER11);
										inspenv.dotNet = true;
										inspreq.addProperty("inspectorid",inspid);
										inspenv.setOutputSoapObject(inspreq);;
										HttpTransportSE androidHttpTransport1 = new HttpTransportSE(cf.URL_ARR);
										androidHttpTransport1.call(cf.NAMESPACE+"INSPECTORDETAILS",inspenv);
										SoapObject chkinsp = (SoapObject) inspenv.getResponse(); 
										String Inspector_id = String.valueOf(chkinsp.getProperty("Inspectorid"));
										String phoneno= String.valueOf(chkinsp.getProperty("Inspectorphone"));
										String Insp_fname = String.valueOf(chkinsp.getProperty("Inspectorfirstname"));
										String Insp_mname = String.valueOf(chkinsp.getProperty("Inspectormiddlename"));
										String Insp_lname = String.valueOf(chkinsp.getProperty("Inspectorlastname"));
										String Insp_address = String.valueOf(chkinsp.getProperty("Inspectoraddress"));
										String Insp_cname = String.valueOf(chkinsp.getProperty("Inspectorcompanyname"));
										String Insp_cid = String.valueOf(chkinsp.getProperty("InspectorcompanyId"));
										String Insp_uname = String.valueOf(chkinsp.getProperty("Inspectorusername"));
										String Insp_pwd = String.valueOf(chkinsp.getProperty("Inspectorpassword"));
										System.out.println("Insp_pwd="+Insp_pwd+Inspector_id);
										String Insp_phto = String.valueOf(chkinsp.getProperty("InspectorPhoto"));System.out.println("Insp_phto"+Insp_phto);
										String Insp_phtoext = String.valueOf(chkinsp.getProperty("InspectorPhotoExt"));
										String Insp_status = String.valueOf(chkinsp.getProperty("AndroidStatus"));
										String Insp_pemail = String.valueOf(chkinsp.getProperty("InspectorPersonalEmail"));
										String Insp_email = String.valueOf(chkinsp.getProperty("InspectorEmail"));
										String Insp_sign = String.valueOf(chkinsp.getProperty("InspectorSign"));
										String Insp_pltype = String.valueOf(chkinsp.getProperty("PrimaryLicenseType"));System.out.println("INSPL="+Insp_pltype);
										String Insp_plno= String.valueOf(chkinsp.getProperty("PrimaryLicenseNumber"));
										String headshot= String.valueOf(chkinsp.getProperty("Headshot"));
										
										String Insp_falg="0";										
										byte[] dec_inspphto = Base64.decode(Insp_phto, 0);
										byte[] dec_inspsign = Base64.decode(Insp_sign, 0);
										try
										{
											String FILENAME = Inspector_id + Insp_phtoext;
											FileOutputStream fos = openFileOutput(FILENAME, Context.MODE_WORLD_READABLE);
											fos.write(dec_inspphto);
											fos.close();	
										}
										catch (IOException e)
										{
											System.out.println("dec_inspphto"+e.getMessage());
										}
										String SFILENAME="";
										try
										{
											SFILENAME = "Signature"+ Inspector_id +".jpg";
											FileOutputStream fos1 = openFileOutput(SFILENAME, Context.MODE_WORLD_READABLE);
											fos1.write(dec_inspsign);
											fos1.close();	
										}
										catch (IOException e)
										{
											System.out.println("signature="+e.getMessage());
										}
										try {

											cf.arr_db.execSQL("INSERT INTO "
													+ cf.inspectorlogin
													+ " (Fld_InspectorId,Fld_InspectorFirstName,Fld_InspectorMiddleName,Fld_InspectorLastName,"+
													     "Fld_InspectorAddress,Fld_InspectorCompanyName,Fld_InspectorCompanyId,Fld_InspectorUserName,"+
													     "Fld_InspectorPassword,Fld_InspectorPhotoExtn,Android_status,Fld_InspectorFlag,"+
													     "Fld_Remember_pass,Fld_InspectorEmail,Fld_InspectorPEmail,Fld_signature,Fld_licenceno,Fld_licencetype,Fld_Phone)"
													+ " VALUES ('" + Inspector_id + "'," + 
													"'"+cf.encode(Insp_fname) + "','"+cf.encode(Insp_mname)+"','"+cf.encode(Insp_lname)+"',"+
													"'"+cf.encode(Insp_address) + "','"+ cf.encode(Insp_cname) + "','"+ cf.encode(Insp_cid) + "',"+
													"'"+cf.encode(Insp_uname) + "','"+ cf.encode(Insp_pwd) + "','"+cf.encode(Insp_phtoext)+ "',"+
													"'" +Insp_status + "','"+Insp_falg+"','"+remid+"',"+
													"'"+cf.encode(Insp_email)+"','" +cf.encode(Insp_pemail) + "',"+
													"'"+cf.encode(SFILENAME)+"','"+cf.encode(Insp_plno)+"','"+cf.encode(Insp_pltype)+"','"+cf.encode(phoneno)+"')");
											System.out.println("inserted succ");
											
											
											cf.arr_db.execSQL("UPDATE " + cf.inspectorlogin + " SET Fld_InspectorFlag=1"
    												+ " WHERE Fld_InspectorId ='" + Inspector_id+ "'");
											System.out.println("policyholder succ");
											
											
											Call_RememberPassword();
										} catch (Exception e) {
										  
									     }
									}
									catch (Exception e) {
										// TODO: handle exception
										show_handler=3;
										handler.sendEmptyMessage(0);
									}
									}
								    show_handler=1;
								    handler.sendEmptyMessage(0); 
							
							}
							catch (Exception e) {
								// TODO: handle exception
							}
								
					
					}
					else 
					{
						show_handler=2;
						handler.sendEmptyMessage(0);
					}
					
				}
				catch (SocketException e) {
					// TODO Auto-generated catch block
					show_handler=3;
					handler.sendEmptyMessage(0);
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					show_handler=3;
					handler.sendEmptyMessage(0);
					
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					show_handler=3;
					handler.sendEmptyMessage(0);
					
				}
				catch (Exception e) {
					// TODO: handle exception
					show_handler=4;
					handler.sendEmptyMessage(0);
				}
			}
    	}.start();
	}
    protected void Call_RememberPassword() {
		// TODO Auto-generated method stub
    	try
    	{
    		Cursor	sel_logcur = cf.SelectTablefunction(cf.inspectorlogin,
    				"where Fld_InspectorUserName='"+ cf.encode(((AutoCompleteTextView)findViewById(R.id.eusername)).getText().toString())+ "'");
    		if(sel_logcur!=null)
    		{
    			if(sel_logcur.getCount()>=0)
    			{
    				sel_logcur.moveToFirst();
    				if(sel_logcur.getString(sel_logcur.getColumnIndex("Fld_Remember_pass")).equals("0"))
    				{
    					Show_alert(sel_logcur);
    				}
    				else
    				{
    					Call_NextLayout();
    				}
    			}
    			else
    			{
    				Call_NextLayout();
    			}
    		}
    		else
    		{
    			Call_NextLayout();
    		}
    		
    	}catch (Exception e) {
			// TODO: handle exception
		}
    	
	}
    private void Show_alert(final Cursor sel_logcur) {
		// TODO Auto-generated method stub
    	final Dialog dialog1 = new Dialog(AllRiskReportsActivity.this,android.R.style.Theme_Translucent_NoTitleBar);
		dialog1.getWindow().setContentView(R.layout.alert);
		LinearLayout curtbl=(LinearLayout) dialog1.findViewById(R.id.remember_password);
		curtbl.setVisibility(View.VISIBLE);	
	  	Button RP_yes = (Button) dialog1.findViewById(R.id.RP_yes);	
	  	Button RP_no = (Button) dialog1.findViewById(R.id.RP_no);
	  	ImageView btn_helpclose_t = (ImageView) dialog1.findViewById(R.id.RP_close);
			
			btn_helpclose_t.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialog1.setCancelable(true);
					dialog1.dismiss();
					Call_NextLayout();
			
				}
				
				
			});
			RP_yes.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialog1.setCancelable(true);
					dialog1.dismiss();
					cf.arr_db.execSQL("UPDATE "
							+ cf.inspectorlogin
							+ " SET Fld_Remember_pass=1"
							+ " WHERE Fld_InspectorId ='"
							+ sel_logcur.getString(sel_logcur.getColumnIndex("Fld_InspectorId")) + "'");
					cf.ShowToast("Password has been saved sucessfully.", 1);
					Call_NextLayout();
				
				}	
			});
			RP_no.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialog1.setCancelable(true);
					dialog1.dismiss();
					Call_NextLayout();
				
				}	
			});
			dialog1.setCancelable(false);
			dialog1.show();System.out.println("dialog1");
    }
	protected void Call_NextLayout() {
		// TODO Auto-generated method stub
		startActivity(new Intent(getApplicationContext(),HomeScreen.class));
	}
	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			cf.pd.dismiss();
			if(show_handler == 1)
			{
				show_handler=0;
				
				Call_RememberPassword();
			}
			else if(show_handler == 2)
			{
				cf.ShowToast(chkstatus, 0);
				((AutoCompleteTextView)findViewById(R.id.eusername)).setText("");
				((EditText)findViewById(R.id.epwd)).setText("");
				((AutoCompleteTextView)findViewById(R.id.eusername)).requestFocus();
				show_handler=0;
			}
			else if(show_handler == 3)
			{
				cf.ShowToast("There is a problem on your Network. Please try again later with better Network.", 0);
				show_handler=0;
			}
			else if(show_handler == 4)
			{
				cf.ShowToast("There is a problem on your application. Please contact Paperless administrator.", 0);
				show_handler=0;
			}
			
		}
    };
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent startMain = new Intent(Intent.ACTION_MAIN);
			startMain.addCategory(Intent.CATEGORY_HOME);
			startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(startMain);
			return true;
		}

		return super.onKeyDown(keyCode, event);
	}
}