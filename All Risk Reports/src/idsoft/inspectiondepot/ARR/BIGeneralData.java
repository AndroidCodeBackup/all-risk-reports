package idsoft.inspectiondepot.ARR;


import java.util.ArrayList;
import java.util.List;

import android.R.color;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class BIGeneralData extends Activity {
	CommonFunctions cf;
	String wallconstvalue[] = new String[10];
	String SCOPE="",neighbourval="",permitconftxt="",buildingtypevalue="",addistrucrdgtxt="",incioccutxt="",BI_INSURED="",BI_BUILDING_OCCU="",BOCCU_SURV="",BI_BUILDOCCU="",BI_NEIGHBOUROPTION="",BI_NEIGHBOUR="",BI_GeneralData="",BI_YOC="",surveyothrval="",
			BI_OCCU_SURV="",BI_PERMIT="",BI_Inci="",BI_Addistructures="",BI_OCCU_TYPE="",BI_OTHEROCCU="",buildingsize="",BI_SCOPEOFINSP="",BI_OTHERSURV="",
			yrbuiltrdgtxt="",permitconfrdgtxt="",balconiespresrdgtxt="",incioccurdgtxt="",BI_INCIDENTAL="",additionalrdgtxt="",BI_YEARBUILT="", addistrucothrval="",
			BI_OCCU_PERCENT="",BI_VACANT_PERCENT="",BI_ADDISTRUC1="",BI_ADDISTRUC2="",yocstringother="",
					General_Desc="",YOC="",Permit_Confirmed="",Balcony_present="",Inci_Occu="",Addi_Stru="",gennotappl="0";
	
	/* String spnstories[] = {"--Select--","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65","66","67","68","69","70","71","72","73","74","75","76","77","78","79","80","81","82","83","84","85","86","87","88","89","90","91","92","93","94","95","96","97","98","99","100"};
	 String spnstories1[]= {"--Select--","1","2","3"};
	 String spnstories2[]= {"--Select--","4","5","6"};
	 String spnstories3[]= {"--Select--","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65","66","67","68","69","70","71","72","73","74","75","76","77","78","79","80","81","82","83","84","85","86","87","88","89","90","91","92","93","94","95","96","97","98","99","100"};
	*/ 
	 private List<String> items= new ArrayList<String>();
	Spinner spin_yoc,nostoriesspin;ArrayAdapter spinyoc_adap,nostoriesadap;
	Cursor c2,curbuilgen;ScrollView scr1;
	int m=0,rwselev,occuval=0,percentval=100,vacantval=0,buildingval=0,nobuild=0,nounits=0,chkvl=2,yoc=0;
	boolean builoccu[] = {true,true,true,true,true,true,true,true,true,true,true};
	int type;
	String[] elevation,percentage,storarr,arrwallid,arrinsptypeid;
	
	public RadioButton insuredchk[] = new RadioButton[7];
	public int[] chkinsuredid = {R.id.insured_chk1,R.id.insured_chk2,R.id.insured_chk3,R.id.insured_chk4,R.id.insured_chk5,R.id.insured_chk6,R.id.insured_chk7};
	public RadioButton neighbouroptionchk[] = new RadioButton[4];
	public int[] chkneighbouroptionid = {R.id.neigh_chk5,R.id.neigh_chk6,R.id.neigh_chk7,R.id.neigh_chk8};
	
	public RadioButton neighbourchk[] = new RadioButton[4];
	public int[] chkneighbourid = {R.id.neigh_chk1,R.id.neigh_chk2,R.id.neigh_chk3,R.id.neigh_chk4};
	
	
	public CheckBox incidentalchk[] = new CheckBox[7];
	public int[] chkincidentalid = {R.id.incioccu_chk1,R.id.incioccu_chk2,R.id.incioccu_chk3,R.id.incioccu_chk4,R.id.incioccu_chk5,R.id.incioccu_chk6,R.id.incioccu_chk7};
	
	public CheckBox scopeinschk[] = new CheckBox[5];
	public int[] scopeinspchkid = {R.id.scopeofinsp_chk1,R.id.scopeofinsp_chk2,R.id.scopeofinsp_chk3,R.id.scopeofinsp_chk4,R.id.scopeofinsp_chk5};

	
	public RadioButton resrdio[] = new RadioButton[8];
	public int[] resrdioid = {R.id.occu_chk4,R.id.occu_chk5,R.id.occu_chk6,R.id.occu_chk7,R.id.occu_chk8,R.id.occu_chk16,R.id.occu_chk20,R.id.occu_chk21};
	
	public RadioButton commrdio[] = new RadioButton[14];
	public int[] comrdioid = {R.id.occu_chk1,R.id.occu_chk2,R.id.occu_chk3,R.id.occu_chk9,R.id.occu_chk10,R.id.occu_chk11,R.id.occu_chk12,R.id.occu_chk13,R.id.occu_chk14,R.id.occu_chk15,R.id.occu_chk17,R.id.occu_chk18,R.id.occu_chk19,R.id.occu_chk21};
	
	EditText ednostories;
	public RadioButton occuchk[] = new RadioButton[21];
	public int[] chkoccuid = {R.id.occu_chk1,R.id.occu_chk2,R.id.occu_chk3,R.id.occu_chk4,R.id.occu_chk5,R.id.occu_chk6,R.id.occu_chk7,
			R.id.occu_chk8,R.id.occu_chk9,R.id.occu_chk10,R.id.occu_chk11,R.id.occu_chk12,R.id.occu_chk13,R.id.occu_chk14,R.id.occu_chk15,
			R.id.occu_chk16,R.id.occu_chk17,R.id.occu_chk18,R.id.occu_chk19,R.id.occu_chk20,R.id.occu_chk21};
	public CheckBox buildingoccuchk[] = new CheckBox[7];
	public int[] chkibuildoccuid = {R.id.occu_chk,R.id.vacant_chk,R.id.ocuupancy_chk1,R.id.ocuupancy_chk2,R.id.ocuupancy_chk3,R.id.ocuupancy_chk4,R.id.ocuupancy_chk5};
	public RadioButton yrbuiltoption[] = new RadioButton[3];
	public RadioButton balcony_rd[] = new RadioButton[2];int toast=0,viewX,viewY,viewX1,viewY1;
	RadioGroup addistrucrdgval,permitconfirmedval,incidentoccuval,neighbourhoodval;
	boolean b1=true,b2=true;	
	public CheckBox BI_ADDI1[] = new CheckBox[3];
	public CheckBox BI_ADDI2[] = new CheckBox[11];
	TextWatcher watcherocccu,watchervacant;
	boolean b[]=new boolean[32];
	String  wcvalue1[]=new String[12];
	public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);	       
	        cf = new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);				
		 	}
	        setContentView(R.layout.generaldata);
	        cf.getDeviceDimensions();
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
         	hdr_layout.addView(new HdrOnclickListener(this,1,"General","Building Info","General Data",1,1,cf));
         	LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
         	main_layout.setMinimumWidth(cf.wd);
         	main_layout.addView(new MyOnclickListener(getApplicationContext(), 1, 0,0,cf));
	        LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
	        submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 16, 1,0,cf));
	        LinearLayout submenu2_layout = (LinearLayout) findViewById(R.id.submenu1); 
	        submenu2_layout.addView(new MyOnclickListener(getApplicationContext(), 450, 1,0,cf));
	        TableRow tblrw = (TableRow)findViewById(R.id.row2);
	        tblrw.setMinimumHeight(cf.ht);
	        cf.CreateARRTable(61); cf.CreateARRTable(34);
	        cf.getBIGeneraldata(cf.selectedhomeid);
	        
	        Declaration(); 
	        setValue();
	        cf.getPolicyholderInformation(cf.selectedhomeid);
	        cf.getBIGeneraldata(cf.selectedhomeid);
	 }
	private void defaultbi()
	{
		System.out.println("cf.Stories"+cf.Stories);
		ednostories.setText(cf.Stories);
		((EditText)findViewById(R.id.edbuildingsize)).setText(cf.BuildingSize);
		System.out.println("cf.Stories"+cf.YearPH);
		int spinnerPosition2 = spinyoc_adap.getPosition(cf.YearPH);
		spin_yoc.setSelection(spinnerPosition2);
		
		for(int i=0;i<yrbuiltoption.length;i++)
		{
			  yrbuiltoption[i].setChecked(false);
		}
					
			if(cf.YearPH.equals("Unknown"))
			{	 
	 			 yrbuiltoption[1].setChecked(true);
	 		}
			else if(cf.YearPH.equals("Other"))
			{
				 yrbuiltoption[2].setChecked(true);
			}
			else
			{
				yrbuiltoption[1].setChecked(true);
			}
					
			noofbuildings();
			
			
	}
	private void  noofbuildings()
	{
		if(cf.CommercialFlag.equals("0"))
		{
			((EditText)findViewById(R.id.ednoofbuildings)).setText("1");
		}
		else
		{
			try
			{
				Cursor c =cf.SelectTablefunction(cf.policyholder, " WHERE Commercial_Flag='"+cf.CommercialFlag+"'");
				((EditText)findViewById(R.id.ednoofbuildings)).setText(c.getCount());
			}
			catch (Exception e) {
				// TODO: handle exception
				System.out.println("EE="+e.getMessage());
			}
			
		}	
	}
	private void setValue() {
		// TODO Auto-generated method stub
		 SelectBItbl();
		 System.out.println("setvalue="+c2.getCount());
		  if(c2.getCount()==0)
		  {			    
			  ((LinearLayout)findViewById(R.id.generaldatalin)).setVisibility(cf.v1.GONE);
			  ((CheckBox)findViewById(R.id.genchk)).setChecked(true);
			  scopechk();
			  /*defaultbi();
			  scopechk();
			  ((RadioButton) addistrucrdgval.findViewWithTag("No")).setChecked(true);
			  ((RadioButton) incidentoccuval.findViewWithTag("No")).setChecked(true);
			  if(cf.CommercialFlag.equals("0"))
			  {
				((CheckBox)findViewById(R.id.typ_chk4)).setChecked(true);
			  }*/
			 			  
		  }
		  else
		  {
			  
				if(cf.GEN_NA.equals("0") || cf.GEN_NA.equals(""))
				{
			
					((LinearLayout)findViewById(R.id.generaldatalin)).setVisibility(cf.v1.VISIBLE);
			 if(!General_Desc.equals(""))
			 {	
				((TextView)findViewById(R.id.savegen)).setVisibility(cf.v1.VISIBLE);
				String General_Data_Split[] = General_Desc.split("&#39;");
				String insuredsplit[] = General_Data_Split[0].split("&#94;");
				insuredchk[0].setChecked(false);
				cf.setRadioBtnValue(insuredsplit[0],insuredchk);
				
				if(!insuredsplit[0].equals("Other"))
				{
					((EditText)findViewById(R.id.insuredis_other)).setVisibility(cf.v1.INVISIBLE);
				}
				else
				{
					((EditText)findViewById(R.id.insuredis_other)).setVisibility(cf.v1.VISIBLE);
			    	((EditText)findViewById(R.id.insuredis_other)).setText(insuredsplit[1]);
				}
				
				String neighsplit[] = General_Data_Split[1].split("&#94;");	
				neighbouroptionchk[2].setChecked(false);
				
				if(neighsplit[0].equals("Commercial"))
				{
					for(int i=0;i<commrdio.length;i++)
					{
						commrdio[i].setTextColor(Color.BLACK);
						commrdio[i].setEnabled(true);	
					}
					for(int i=0;i<resrdio.length;i++)
					{
						resrdio[i].setTextColor(Color.GRAY);
						resrdio[i].setEnabled(false);	
					}
				}
				else if(neighsplit[0].equals("Residential"))
				{
					for(int i=0;i<resrdio.length;i++)
					{
						resrdio[i].setTextColor(Color.BLACK);
						resrdio[i].setEnabled(true);	
					}
					for(int i=0;i<commrdio.length;i++)
					{
						commrdio[i].setTextColor(Color.GRAY);
						commrdio[i].setEnabled(false);	
					}
				}
				
				cf.setRadioBtnValue(neighsplit[0],neighbourchk);
				String neighsuboptionsplit[] = neighsplit[1].split("&#33;");	
				cf.setRadioBtnValue(neighsuboptionsplit[0],neighbouroptionchk);
				
				if(!neighsuboptionsplit[0].equals("Other"))
				{
					((EditText)findViewById(R.id.neigh_other)).setVisibility(cf.v1.INVISIBLE);
				}
				else
				{
					((EditText)findViewById(R.id.neigh_other)).setVisibility(cf.v1.VISIBLE);
			    	((EditText)findViewById(R.id.neigh_other)).setText(neighsuboptionsplit[1]);
				}
				
				String occupancysplit[] = General_Data_Split[2].split("&#94;");
				occuchk[3].setChecked(false);
				cf.setRadioBtnValue(occupancysplit[0],occuchk);
				if(!occupancysplit[0].equals("Other Building Type"))
				{
					((EditText)findViewById(R.id.occupancy_other)).setVisibility(cf.v1.INVISIBLE);
				}
				else
				{
					((EditText)findViewById(R.id.occupancy_other)).setVisibility(cf.v1.VISIBLE);
			    	((EditText)findViewById(R.id.occupancy_other)).setText(occupancysplit[1]);
				}
				occuchk[20].setEnabled(true);
				occuchk[20].setTextColor(Color.BLACK);
				System.out.println("General_Data_Split[4]"+General_Data_Split[4]);
				if(!General_Data_Split[4].equals(""))
				{ 
					ednostories.setText(General_Data_Split[4]);
				}
				else
				{
					System.out.println("else"+cf.Stories);
				}
				if(neighsplit[0].equals("Commercial") || neighsplit[0].equals("Industrial") || neighsplit[0].equals("Rural"))
				{
					((LinearLayout)findViewById(R.id.noofbuillin)).setVisibility(cf.v1.VISIBLE);	
					((LinearLayout)findViewById(R.id.noofbuillin)).setVisibility(cf.v1.VISIBLE);	
					//((LinearLayout)findViewById(R.id.inciocculin)).setVisibility(cf.v1.VISIBLE);
					//((LinearLayout)findViewById(R.id.btypelin)).setVisibility(cf.v1.VISIBLE);
					
					noofbuildings();
					if(!Inci_Occu.equals(""))
					{
						String Inci_Occu_split[] = Inci_Occu.split("&#126;");
						((RadioButton) incidentoccuval.findViewWithTag(Inci_Occu_split[0])).setChecked(true);	
						if(Inci_Occu_split[0].equals("Yes")){
							((LinearLayout)findViewById(R.id.incioccu_layt1)).setVisibility(cf.v1.VISIBLE);
							((LinearLayout)findViewById(R.id.incioccu_layt2)).setVisibility(cf.v1.VISIBLE);
							cf.setvaluechk1(Inci_Occu_split[1],incidentalchk,((EditText)findViewById(R.id.neigh_other)));
						}
						else
						{
							((LinearLayout)findViewById(R.id.incioccu_layt1)).setVisibility(cf.v1.GONE);
							((LinearLayout)findViewById(R.id.incioccu_layt2)).setVisibility(cf.v1.GONE);
						}				
					}	
				}
				else
				{
					((LinearLayout)findViewById(R.id.noofbuillin)).setVisibility(cf.v1.GONE);	
					((LinearLayout)findViewById(R.id.noofbuillin)).setVisibility(cf.v1.GONE);	
					//((LinearLayout)findViewById(R.id.inciocculin)).setVisibility(cf.v1.GONE);
					//((LinearLayout)findViewById(R.id.btypelin)).setVisibility(cf.v1.GONE);
				}
				
			/** Settting value for commercial and Residential starts**/
				
				/*if(neighsplit[0].equals("Commercial") || neighsplit[0].equals("Residential"))
				{*/
					((LinearLayout)findViewById(R.id.noofunitslin)).setVisibility(cf.v1.VISIBLE);
					((LinearLayout)findViewById(R.id.bsizelin)).setVisibility(cf.v1.VISIBLE);
					((LinearLayout)findViewById(R.id.bocculin1)).setVisibility(cf.v1.VISIBLE);
					((LinearLayout)findViewById(R.id.bocculin2)).setVisibility(cf.v1.VISIBLE);
					((LinearLayout)findViewById(R.id.balconieslin)).setVisibility(cf.v1.VISIBLE);
					((LinearLayout)findViewById(R.id.addstruclin)).setVisibility(cf.v1.VISIBLE);
					
					System.out.println("General_Data_Split[5]"+General_Data_Split[5]);
					
					if(!General_Data_Split[5].equals(""))
					{ 
						((EditText)findViewById(R.id.ednoofunits)).setText(General_Data_Split[5]);
					}
					else
					{
						((EditText)findViewById(R.id.ednoofunits)).setText("1");
					}
					
					
					if(!General_Data_Split[6].equals(""))
					{
						((EditText)findViewById(R.id.edbuildingsize)).setText(General_Data_Split[6]);
					}
					
					
					
					
				/*}
				else
				{
					((LinearLayout)findViewById(R.id.noofunitslin)).setVisibility(cf.v1.GONE);
					((LinearLayout)findViewById(R.id.bsizelin)).setVisibility(cf.v1.GONE);
					((LinearLayout)findViewById(R.id.bocculin1)).setVisibility(cf.v1.GONE);
					((LinearLayout)findViewById(R.id.bocculin2)).setVisibility(cf.v1.GONE);
					((LinearLayout)findViewById(R.id.balconieslin)).setVisibility(cf.v1.GONE);
					((LinearLayout)findViewById(R.id.addstruclin)).setVisibility(cf.v1.GONE);
				}*/
				/** Settting value for commercial and Residential ends**/
			}
			else
			{
				 ((CheckBox)findViewById(R.id.genchk)).setChecked(true);
				 ((LinearLayout)findViewById(R.id.generaldatalin)).setVisibility(cf.v1.GONE);
				
				System.out.println("wtast up");
				((TextView)findViewById(R.id.savegen)).setVisibility(cf.v1.GONE);
				defaultbi();
				
				buildingtype();
				noofbuildings();
				additionalstruc();
				buildingoccu();
				neighbourchk();
				
				
			}	System.out.println("BOCCU_SURV"+BOCCU_SURV);
			 if(!BOCCU_SURV.equals(""))
				{
					String Perc_split[] = BOCCU_SURV.split("&#33;");
					if(!Perc_split[0].equals("") && !Perc_split[0].equals("0"))
					{
						((EditText)findViewById(R.id.edoccupercent)).setText(Perc_split[0]);
						((EditText)findViewById(R.id.edoccupercent)).setEnabled(true);
					}
					else
					{
						((EditText)findViewById(R.id.edoccupercent)).setText("");
						((EditText)findViewById(R.id.edvacantpercent)).setEnabled(false);
					}
					if(!Perc_split[1].equals("") && !Perc_split[1].equals("0")){((EditText)findViewById(R.id.edvacantpercent)).setText(Perc_split[1]);((EditText)findViewById(R.id.edvacantpercent)).setEnabled(true);}else{((EditText)findViewById(R.id.edvacantpercent)).setText("");((EditText)findViewById(R.id.edvacantpercent)).setEnabled(false);}
					cf.setvaluechk1(Perc_split[2],buildingoccuchk,((EditText)findViewById(R.id.edoccuchkother)));
				}
				if(!Balcony_present.equals(""))
				{ 
					cf.setRadioBtnValue(Balcony_present,balcony_rd);
				}	System.out.println("Addi_Stru"+Addi_Stru);
				if(!Addi_Stru.equals(""))
				{
					String Addi_Struc_split[] = Addi_Stru.split("&#126;");
					((RadioButton) addistrucrdgval.findViewWithTag(Addi_Struc_split[0])).setChecked(true);
					if( Addi_Struc_split[0].equals("Yes"))
					{
						((LinearLayout)findViewById(R.id.addistrcu_layt1)).setVisibility(cf.v1.VISIBLE);
						((LinearLayout)findViewById(R.id.addistrcu_layt2)).setVisibility(cf.v1.VISIBLE);
						((LinearLayout)findViewById(R.id.addistrcu_layt3)).setVisibility(cf.v1.VISIBLE);
						((LinearLayout)findViewById(R.id.addistrcu_layt4)).setVisibility(cf.v1.VISIBLE);
						if(Addi_Struc_split.length>1){cf.setvaluechk1(Addi_Struc_split[1],BI_ADDI1,((EditText)findViewById(R.id.edaddfencesother)));}
						if(Addi_Struc_split.length>2){cf.setvaluechk1(Addi_Struc_split[2],BI_ADDI2,((EditText)findViewById(R.id.edaddfencesother)));}
						
					}
					else
					{
						((LinearLayout)findViewById(R.id.addistrcu_layt1)).setVisibility(cf.v1.GONE);
						((LinearLayout)findViewById(R.id.addistrcu_layt2)).setVisibility(cf.v1.GONE);
						((LinearLayout)findViewById(R.id.addistrcu_layt3)).setVisibility(cf.v1.GONE);
						((LinearLayout)findViewById(R.id.addistrcu_layt4)).setVisibility(cf.v1.GONE);
					}			
				}
				else
				{
					((RadioButton) addistrucrdgval.findViewWithTag("No")).setChecked(true);
				}
			 System.out.println("SCOPE="+SCOPE);
			 		if(!SCOPE.equals(""))
			 		{
			 			cf.setvaluechk1(SCOPE,scopeinschk,((EditText)findViewById(R.id.scope_other)));
			 		}
			 		else
			 		{
			 			scopechk();
			 		}
			 
					if(!YOC.equals(""))
					{
						//yoc=1;
						String yrsplit[] = YOC.split("&#94;");
						System.out.println("yOC]"+YOC);
						String yrothersplit[] = yrsplit[0].split("&#126;");			
						if(yrothersplit[0].equals("Other"))	
						{
							 
							 ((EditText)findViewById(R.id.edyocother)).setVisibility(cf.v1.VISIBLE);
							 ((EditText)findViewById(R.id.edyocother)).setText(yrothersplit[1]);
						}		
						int spinnerPosition2 = spinyoc_adap.getPosition(yrothersplit[0]);
						spin_yoc.setSelection(spinnerPosition2);
						System.out.println("yrspli="+yrsplit[1]);
						cf.setRadioBtnValue(yrsplit[1],yrbuiltoption);
					}
					System.out.println("permitco="+Permit_Confirmed);
					if(!Permit_Confirmed.equals(""))
					{
						String permit_conf_split[] = Permit_Confirmed.split("&#94;");
						if(permit_conf_split[0].equals("No"))
						{
							((RadioButton) permitconfirmedval.findViewWithTag("No")).setChecked(true);
							((LinearLayout)findViewById(R.id.permitconfirmedlin)).setVisibility(cf.v1.GONE);
							//((EditText)findViewById(R.id.etpermitconfirmed)).setText("Beyond Scope of Inspection");
						}
						else
						{
							System.out.println("permit"+permit_conf_split[1]);
							((LinearLayout)findViewById(R.id.permitconfirmedlin)).setVisibility(cf.v1.VISIBLE);
							((EditText)findViewById(R.id.etpermitconfirmed)).setText(permit_conf_split[1]);
						}
							((RadioButton) permitconfirmedval.findViewWithTag(permit_conf_split[0])).setChecked(true);		
					}
					else
					{
						System.out.println("nsisdeee");
						//((EditText)findViewById(R.id.etpermitconfirmed)).setText("Beyond Scope of Inspection");
						((RadioButton) permitconfirmedval.findViewWithTag("No")).setChecked(true);
						((LinearLayout)findViewById(R.id.permitconfirmedlin)).setVisibility(cf.v1.GONE);
					}
					
				buildingtype();
		  }
			else
			{
				defaultbi();
				if(!SCOPE.equals(""))
		 		{
		 			cf.setvaluechk1(SCOPE,scopeinschk,((EditText)findViewById(R.id.scope_other)));
		 		}
		 		else
		 		{
		 			scopechk();
		 		}
				((CheckBox)findViewById(R.id.genchk)).setChecked(true);
				((LinearLayout)findViewById(R.id.generaldatalin)).setVisibility(cf.v1.GONE);
				((TextView)findViewById(R.id.savegen)).setVisibility(cf.v1.VISIBLE);
			}
		  }
	}
	
	private void buildingtype() {
		// TODO Auto-generated method stub
		if(cf.CommercialFlag.equals("0"))
		{
			((CheckBox)findViewById(R.id.typ_chk4)).setChecked(true);		
		}
		else
		{
			if(cf.Stories.equals("0") || cf.Stories.equals(""))
			{
				typechkvalue();
			}
			else if(cf.Stories.equals("1") || cf.Stories.equals("2")  || cf.Stories.equals("3"))
			{
				typechkvalue();
				((CheckBox)findViewById(R.id.typ_chk1)).setChecked(true);		
			}
			else if(cf.Stories.equals("4") || cf.Stories.equals("5")  || cf.Stories.equals("6"))
			{
				typechkvalue();
				((CheckBox)findViewById(R.id.typ_chk2)).setChecked(true);		
			}
			else 
			{	
				typechkvalue();
				((CheckBox)findViewById(R.id.typ_chk3)).setChecked(true);			
			}	
		}	
	}
	private void neighbourchk() {
		// TODO Auto-generated method stub
		
		if(!cf.Neighbourhoodis.equals(""))
		{
			String neighbourmain[] = cf.Neighbourhoodis.split(",");
			cf.setRadioBtnValue(neighbourmain[0],neighbourchk);
			
			insuredchk[0].setChecked(false);
			neighbouroptionchk[2].setChecked(false);
			
			if(neighbourmain[1].contains("Other"))
			{
				
				if(neighbourmain[1].equals("Other()"))
				{
					neighbourmain[1]= neighbourmain[1].replace("(", "&#40;");
					if(neighbourmain[1].endsWith(")"))
					{
			    		neighbourmain[1] = neighbourmain[1].replace(")", "");
					}
					
			    	String neighoptionsplit[] = neighbourmain[1].split("&#40;");
			    	
					cf.setRadioBtnValue(neighoptionsplit[0],neighbouroptionchk);
					System.out.println("neighbourmain[1]"+neighbourmain[1]);
					
					((EditText)findViewById(R.id.neigh_other)).setVisibility(cf.v1.VISIBLE);					
				}
				else
				{
					neighbourmain[1]= neighbourmain[1].replace("(", "&#40;");
					if(neighbourmain[1].endsWith(")"))
					{
			    		neighbourmain[1] = neighbourmain[1].replace(")", "");
					}
					
			    	String neighoptionsplit[] = neighbourmain[1].split("&#40;");
			    	
					cf.setRadioBtnValue(neighoptionsplit[0],neighbouroptionchk);
					System.out.println("neighbourmain[1]"+neighbourmain[1]);
					((EditText)findViewById(R.id.neigh_other)).setVisibility(cf.v1.VISIBLE);
			    	((EditText)findViewById(R.id.neigh_other)).setText(neighoptionsplit[1]);
					
				}
				
				
				
			}
			else
			{
				cf.setRadioBtnValue(neighbourmain[1],neighbouroptionchk);
				((EditText)findViewById(R.id.neigh_other)).setVisibility(cf.v1.INVISIBLE);
			}
								
			if(neighbourmain[0].equals("Commercial"))
			{
				for(int i=0;i<commrdio.length;i++)
				{
					commrdio[i].setTextColor(Color.BLACK);
					commrdio[i].setEnabled(true);	
				}
				for(int i=0;i<resrdio.length;i++)									
				{
					resrdio[i].setTextColor(Color.GRAY);
					resrdio[i].setEnabled(false);	
				}
			}
			else if(neighbourmain[0].equals("Residential"))
			{
				for(int i=0;i<resrdio.length;i++)
				{
					resrdio[i].setTextColor(Color.BLACK);
					resrdio[i].setEnabled(true);	
				}
				for(int i=0;i<commrdio.length;i++)
				{
					commrdio[i].setTextColor(Color.GRAY);
					commrdio[i].setEnabled(false);	
				}
			}
		}
		else
		{
			if(!cf.CommercialFlag.equals("0"))
			{
				neighbourchk[0].setChecked(true);
			}
			else
			{
				neighbourchk[1].setChecked(true);
			}
		}
		
		if(!cf.InsuredIS.equals(""))
		{
			if(cf.InsuredIS.contains("Other"))
			{
				if(cf.InsuredIS.equals("Other()"))
				{
				
					cf.InsuredIS = cf.InsuredIS.replace("(", "&#40;");
					if(cf.InsuredIS.endsWith(")"))
					{
						cf.InsuredIS = cf.InsuredIS.replace(")", "");
					}
			    	String insuredsplit[] = cf.InsuredIS.split("&#40;");
			    	
					insuredchk[0].setChecked(false);
					cf.setRadioBtnValue(insuredsplit[0],insuredchk);
					
					((EditText)findViewById(R.id.insuredis_other)).setVisibility(cf.v1.VISIBLE);
				}
				else
				{
					cf.InsuredIS = cf.InsuredIS.replace("(", "&#40;");
					if(cf.InsuredIS.endsWith(")"))
					{
						cf.InsuredIS = cf.InsuredIS.replace(")", "");
					}
			    	String insuredsplit[] = cf.InsuredIS.split("&#40;");
			    	
					insuredchk[0].setChecked(false);
					cf.setRadioBtnValue(insuredsplit[0],insuredchk);
					
					((EditText)findViewById(R.id.insuredis_other)).setVisibility(cf.v1.VISIBLE);
			    	((EditText)findViewById(R.id.insuredis_other)).setText(insuredsplit[1]);
				}
				
		    	
			}
			else
			{
				cf.setRadioBtnValue(cf.InsuredIS,insuredchk);
				((EditText)findViewById(R.id.insuredis_other)).setVisibility(cf.v1.INVISIBLE);
			}
		}				
		
	
		
		
		if(!cf.OccupancyType.equals(""))
		{
			occuchk[3].setChecked(false);
			
			System.out.println("cf.OccupancyType"+cf.OccupancyType);
			if(cf.OccupancyType.contains("Other Building Type"))
			{
				if(cf.OccupancyType.contains("Other Building Type()"))
				{
				
				cf.OccupancyType = cf.OccupancyType.replace("(", "&#40;");
				if(cf.OccupancyType.endsWith(")"))
				{
					cf.OccupancyType = cf.OccupancyType.replace(")", "");
				}
		    	String occupancysplit[] = cf.OccupancyType.split("&#40;");
		    	
		    	
				cf.setRadioBtnValue(occupancysplit[0],occuchk);
				
				((EditText)findViewById(R.id.occupancy_other)).setVisibility(cf.v1.VISIBLE);
				}
				else
				{
					cf.OccupancyType = cf.OccupancyType.replace("(", "&#40;");
					if(cf.OccupancyType.endsWith(")"))
					{
						cf.OccupancyType = cf.OccupancyType.replace(")", "");
					}
			    	String occupancysplit[] = cf.OccupancyType.split("&#40;");
			    	
			    	
					cf.setRadioBtnValue(occupancysplit[0],occuchk);
					
					((EditText)findViewById(R.id.occupancy_other)).setVisibility(cf.v1.VISIBLE);
			    	((EditText)findViewById(R.id.occupancy_other)).setText(occupancysplit[1]);
				}
		    	
			}
			else
			{
				cf.setRadioBtnValue(cf.OccupancyType,occuchk);
				((EditText)findViewById(R.id.occupancy_other)).setVisibility(cf.v1.INVISIBLE);
			}
		}
		else
		{
			((CheckBox)findViewById(R.id.ocuupancy_chk2)).setChecked(true);
		}
		System.out.println("cfff"+cf.Nofunits);
		if(cf.Nofunits.equals("") || cf.Nofunits.equals("0"))
		{
			((EditText)findViewById(R.id.ednoofunits)).setText("1");
		}
		else
		{
			((EditText)findViewById(R.id.ednoofunits)).setText(cf.Nofunits);
		}
		System.out.println("what is ="+cf.PermitConfirmed);
		if(!cf.PermitConfirmed.equals(""))
		{
			if(cf.PermitConfirmed.equals("No"))
			{
				((RadioButton) permitconfirmedval.findViewWithTag(cf.PermitConfirmed)).setChecked(true);				
			}			
			else
			{	
				cf.PermitConfirmed = cf.PermitConfirmed.replace(" (", "&#94;");
				if(cf.PermitConfirmed.endsWith(")"))
				{
					cf.PermitConfirmed = cf.PermitConfirmed.replace(")", "");
				}				
				System.out.println(" cf.PermitConfirmed="+ cf.PermitConfirmed);
				
				String permitsplit[] = cf.PermitConfirmed.split("&#94;",-1);
				
				System.out.println("permitsplit="+permitsplit[0]);
				System.out.println("permitsplit111="+permitsplit[1]);
				
				((RadioButton) permitconfirmedval.findViewWithTag(permitsplit[0])).setChecked(true);
				((LinearLayout)findViewById(R.id.permitconfirmedlin)).setVisibility(cf.v1.VISIBLE);
				((EditText)findViewById(R.id.etpermitconfirmed)).setText(permitsplit[1]);				
			}
		}
	}
	private void scopechk() {
		// TODO Auto-generated method stub
		
		if(!cf.ScopeofInspection.equals(""))
		{
			cf.ScopeofInspection = cf.ScopeofInspection.replace(" ,", ",");
			cf.ScopeofInspection = cf.ScopeofInspection.replace(",", "&#94;");					
			cf.ScopeofInspection = cf.ScopeofInspection.replace("(", "&#40;");
			if(cf.ScopeofInspection.endsWith(")"))
			{
				cf.ScopeofInspection = cf.ScopeofInspection.replace(")", "");
			}
			cf.setvaluechk1(cf.ScopeofInspection,scopeinschk,((EditText)findViewById(R.id.scope_other)));
		}	
		else
		{
			 ((CheckBox)findViewById(R.id.scopeofinsp_chk3)).setChecked(true);
		}
	}
	private void additionalstruc() {
		// TODO Auto-generated method stub
		System.out.println("AdditionalStructures"+cf.AdditionalStructures);
		if(!cf.AdditionalStructures.equals(""))
		{
			((RadioButton) addistrucrdgval.findViewWithTag(cf.AdditionalStructures)).setChecked(true);	
		}
		
		if(cf.AdditionalStructures.equals("Yes"))
		{
			((LinearLayout)findViewById(R.id.addistrcu_layt1)).setVisibility(cf.v1.VISIBLE);
			((LinearLayout)findViewById(R.id.addistrcu_layt2)).setVisibility(cf.v1.VISIBLE);
			((LinearLayout)findViewById(R.id.addistrcu_layt3)).setVisibility(cf.v1.VISIBLE);
			((LinearLayout)findViewById(R.id.addistrcu_layt4)).setVisibility(cf.v1.VISIBLE);
			System.out.println("LightPoles"+cf.LightPoles);
			
			if(!cf.LightPoles.equals(""))
			{				
				cf.LightPoles = cf.LightPoles.replace(",", "&#94;");					
				cf.LightPoles = cf.LightPoles.replace("(", "&#40;");				
				cf.setvaluechk1(cf.LightPoles,BI_ADDI1,((EditText)findViewById(R.id.edaddfencesother)));
			}
			System.out.println("FencesPrepLine"+cf.FencesPrepLine);
			if(!cf.FencesPrepLine.equals(""))
			{				
				cf.FencesPrepLine = cf.FencesPrepLine.replace(",", "&#94;");					
				cf.FencesPrepLine = cf.FencesPrepLine.replace("(", "&#40;");
				if(cf.FencesPrepLine.endsWith(")"))
				{
					cf.FencesPrepLine = cf.FencesPrepLine.replace(")", "");
				}
				if(cf.FencesPrepLine.contains("&gt;"))
				{
					cf.FencesPrepLine = cf.FencesPrepLine.replace("&gt;", ">");
				}
				if(cf.FencesPrepLine.contains("&lt;"))
				{
					cf.FencesPrepLine = cf.FencesPrepLine.replace("&lt;", "<");
				}
				System.out.println("cf.FencesPrepLine"+cf.FencesPrepLine);
				setvaluechk(cf.FencesPrepLine,BI_ADDI2,((EditText)findViewById(R.id.edaddfencesother)));
				//cf.setvaluechk1(cf.FencesPrepLine,BI_ADDI2,((EditText)findViewById(R.id.edaddfencesother)));
			}
		}
		else
		{
			((LinearLayout)findViewById(R.id.addistrcu_layt1)).setVisibility(cf.v1.GONE);
			((LinearLayout)findViewById(R.id.addistrcu_layt2)).setVisibility(cf.v1.GONE);
			((LinearLayout)findViewById(R.id.addistrcu_layt3)).setVisibility(cf.v1.GONE);
			((LinearLayout)findViewById(R.id.addistrcu_layt4)).setVisibility(cf.v1.GONE);
		}		
	}
	private void setvaluechk(String value, CheckBox[] checkbox,EditText ed1) {
		// TODO Auto-generated method stub
		String val = value.replace(",", "&#94;");
		val = val.replace("(", "&#40;");
		if(val.endsWith(")"))
		{
			val = val.replace(")", "");
		}
		String valspilt[] = val.split("&#94;");
		for(int i=0;i<checkbox.length;i++)
		{
			for(int j=0;j<valspilt.length;j++)
			{
				if(checkbox[i].getText().toString().equals(valspilt[j].trim()))
				{
					checkbox[i].setChecked(true);
				}
			}
		}System.out.println("valu="+value);
		if(value.contains("Other"))
		{
			String othervalspilt[] = val.split("&#40;");
			checkbox[checkbox.length-1].setChecked(true);
			
			ed1.setVisibility(cf.v1.VISIBLE);
			System.out.println("value="+value);
			System.out.println("value1="+othervalspilt[0]);
			System.out.println("value2="+othervalspilt[1]);
			
			ed1.setText(othervalspilt[1]);
	    	
		}
		
	}
	private void buildingoccu() {
		// TODO Auto-generated method stub
		if(!cf.BuildingOccu.equals(""))
		{			
			String occupval[] = cf.BuildingOccu.split("\\~",-1);	
			if(!cf.BuiloccuPerc.equals("0") && !cf.Builvacaperc.equals("0"))
			{
				 cf.BuildingOccu = "Occupied,Vacant," +occupval[2];
			}
			else if(cf.BuiloccuPerc.equals("0") && !cf.Builvacaperc.equals("0"))
			{
				 cf.BuildingOccu = "Vacant," +occupval[2];
			}
			else if(!cf.BuiloccuPerc.equals("0") && cf.Builvacaperc.equals("0"))
			{
				 cf.BuildingOccu = "Occupied," +occupval[2];
			}
			setvaluechk(occupval[2],buildingoccuchk,((EditText)findViewById(R.id.edoccuchkother)));				
		}	
		
		if(!cf.BuiloccuPerc.equals("0"))
		{
			((EditText)findViewById(R.id.edoccupercent)).setText(cf.BuiloccuPerc);
			((EditText)findViewById(R.id.edoccupercent)).setEnabled(true);
		}
		
		if(!cf.Builvacaperc.equals("0"))
		{
			((EditText)findViewById(R.id.edvacantpercent)).setText(cf.Builvacaperc);
			((EditText)findViewById(R.id.edvacantpercent)).setEnabled(true);
		}		
	}
	private void SelectBItbl() {
		// TODO Auto-generated method stub
		c2=cf.SelectTablefunction(cf.BI_General, " where fld_srid='"+cf.selectedhomeid+"'");
		if(c2.getCount()>0)
		{
			c2.moveToFirst();
			 General_Desc = cf.decode(c2.getString(c2.getColumnIndex("BI_INS_NEI_OCC_NOB_NOS_NOU_BS")));
			 YOC = cf.decode(c2.getString(c2.getColumnIndex("BI_YOC")));
			 Permit_Confirmed = cf.decode(c2.getString(c2.getColumnIndex("BI_PERM_CONFIRMED")));
			 Balcony_present = cf.decode(c2.getString(c2.getColumnIndex("BI_BALCONYPRES")));
			 Inci_Occu = cf.decode(c2.getString(c2.getColumnIndex("BI_INCI_OCCU")));
			 Addi_Stru = cf.decode(c2.getString(c2.getColumnIndex("BI_ADDI_STRU")));		 
			 BOCCU_SURV= cf.decode(c2.getString(c2.getColumnIndex("BI_BOCCU_STYPE")));
			 SCOPE= cf.decode(c2.getString(c2.getColumnIndex("BI_SCOPE")));	
		}
	}
	private void Declaration() {
		// TODO Auto-generated method stub
		cf.setupUI((ScrollView) findViewById(R.id.scr));		
		scr1 = ((ScrollView)findViewById(R.id.scr));
		((RelativeLayout)findViewById(R.id.LinearLayout01)).requestFocus();		
		spin_yoc=(Spinner) findViewById(R.id.spin_yoc);		
		spinyoc_adap = new ArrayAdapter(this,android.R.layout.simple_spinner_item,cf.yearbuilt);
		spinyoc_adap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spin_yoc.setAdapter(spinyoc_adap);
		spin_yoc.setOnItemSelectedListener(new  Spin_Selectedlistener(1));		
		 
		
		ednostories=(EditText) findViewById(R.id.noofstories);
		/* nostoriesspin=(Spinner) findViewById(R.id.spin_noofstories);
		 nostoriesadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item,spnstories);
		 nostoriesadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 nostoriesspin.setAdapter(nostoriesadap);*/
		 //nostoriesspin.setEnabled(false);
		 
		// nostoriesspin.setOnItemSelectedListener(new  Spin_Selectedlistener(2)); 
		 for(int i=0;i<insuredchk.length;i++)
	   	 {
	   		insuredchk[i] = (RadioButton)findViewById(chkinsuredid[i]);
	   		insuredchk[i].setOnClickListener(new RadioArrayclickerInsured());	   		
	   	 } 
		 for(int i=0;i<neighbourchk.length;i++)
	   	 {
	   		neighbourchk[i] = (RadioButton)findViewById(chkneighbourid[i]);	   		
	   	 }
		 for(int i=0;i<neighbouroptionchk.length;i++)
	   	 {
	   		neighbouroptionchk[i] = (RadioButton)findViewById(chkneighbouroptionid[i]);
	   		neighbouroptionchk[i].setOnClickListener(new RadioArrayclickerNeighbour());	   		
	   	 }
		 for(int i=0;i<occuchk.length;i++)
	   	 {
	   		occuchk[i] = (RadioButton)findViewById(chkoccuid[i]);
	   		occuchk[i].setOnClickListener(new RadioArrayclickerOccupancy());
	   		occuchk[i].setTextColor(Color.BLACK);
	   	 } 
		 for(int i=0;i<buildingoccuchk.length;i++)
	   	 {
	   		buildingoccuchk[i] = (CheckBox)findViewById(chkibuildoccuid[i]);
	   	 }
		 for(int i=0;i<scopeinschk.length;i++)
	   	 {
			 scopeinschk[i] = (CheckBox)findViewById(scopeinspchkid[i]);
	   	 }
		 for(int i=0;i<resrdio.length;i++)
	   	 {
			 resrdio[i] = (RadioButton)findViewById(resrdioid[i]);
	   	 }
		 for(int i=0;i<commrdio.length;i++)
	   	 {
			 commrdio[i] = (RadioButton)findViewById(comrdioid[i]);
	   	 }		
		 for(int i=0;i<incidentalchk.length;i++)
	   	 {
	   		incidentalchk[i] = (CheckBox)findViewById(chkincidentalid[i]);
	   	 }
		
		 //((EditText)findViewById(R.id.ednoofbuildings)).addTextChangedListener(new textwatcher(1));
		 ((EditText)findViewById(R.id.ednoofunits)).addTextChangedListener(new textwatcher(2,((EditText)findViewById(R.id.ednoofunits))));	
		 ednostories.addTextChangedListener(new textwatcher(6,ednostories));
		// ((EditText)findViewById(R.id.edbuildingsize)).setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
		  yrbuiltoption[0] = (RadioButton)findViewById(R.id.yrbuiltrdio1);
			 yrbuiltoption[1] = (RadioButton)findViewById(R.id.yrbuiltrdio2);
			 yrbuiltoption[2] = (RadioButton)findViewById(R.id.yrbuiltrdio3);
			 
			 balcony_rd[0] = (RadioButton)findViewById(R.id.balconies_rd1);
			 balcony_rd[1] = (RadioButton)findViewById(R.id.balconies_rd2);
			 
			 permitconfirmedval = (RadioGroup)findViewById(R.id.rdgpermitconfirmed);
			 permitconfirmedval.setOnCheckedChangeListener(new checklistenetr(1));
			
			 addistrucrdgval = (RadioGroup)findViewById(R.id.rdgaddistru);
			 addistrucrdgval.setOnCheckedChangeListener(new checklistenetr(2));
			 
			 incidentoccuval = (RadioGroup)findViewById(R.id.rdgincioccup);
			 incidentoccuval.setOnCheckedChangeListener(new checklistenetr(3)); 
			 
			 neighbourhoodval = (RadioGroup)findViewById(R.id.neigh_rdgp1);
			 neighbourhoodval.setOnCheckedChangeListener(new checklistenetr(4)); 
			 
			 
			 BI_ADDI1[0]=(CheckBox) findViewById(R.id.addstruopt1_chk1);
			 BI_ADDI1[1]=(CheckBox) findViewById(R.id.addstruopt1_chk2);	
			 BI_ADDI1[2]=(CheckBox) findViewById(R.id.addstruopt1_chk3);	
			 
			 BI_ADDI2[0]=(CheckBox) findViewById(R.id.addstruopt2_chk1);
			 BI_ADDI2[1]=(CheckBox) findViewById(R.id.addstruopt2_chk2);
			 BI_ADDI2[2]=(CheckBox) findViewById(R.id.addstruopt2_chk3);
			 BI_ADDI2[3]=(CheckBox) findViewById(R.id.addstruopt2_chk4);
			 BI_ADDI2[4]=(CheckBox) findViewById(R.id.addstruopt2_chk5);
			 BI_ADDI2[5]=(CheckBox) findViewById(R.id.addstruopt2_chk6);
			 BI_ADDI2[6]=(CheckBox) findViewById(R.id.addstruopt2_chk7);
			 BI_ADDI2[7]=(CheckBox) findViewById(R.id.addstruopt2_chk8);
			 BI_ADDI2[8]=(CheckBox) findViewById(R.id.addstruopt2_chk9);
			 BI_ADDI2[9]=(CheckBox) findViewById(R.id.addstruopt2_chk10);
			 BI_ADDI2[10]=(CheckBox) findViewById(R.id.addstruopt2_chk11);
		 
		 watcherocccu = new TextWatcher() {
				public void onTextChanged(CharSequence s, int start, int before,
						int count) {
					// TODO Auto-generated method stub
					try{if(Integer.parseInt(String.valueOf(s))>100){((EditText)findViewById(R.id.edoccupercent)).setText("100");}
					}catch(Exception e){}
				}
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
					
					// TODO Auto-generated method stub
				}
				public void afterTextChanged(Editable e) {
					// TODO Auto-generated method stub	
					if(!e.toString().trim().equals(""))
					{
						try 
						{
							occuval = Integer.parseInt(e.toString().trim()); 
						}catch(Exception e1) {
							occuval =0;
						}
							String te = occuval+"";
							if(te.trim().length()>2)
							{
								if(b1)
								{
									b2=false;
									buildingoccuchk[1].setChecked(false);
									((EditText)findViewById(R.id.edvacantpercent)).setEnabled(false);	
									((EditText)findViewById(R.id.edvacantpercent)).setText("");
								}
								else
								{
									b1=true;
								}
							}
							else
							{
								int occ = percentval - occuval;
								if(b1)
								{
									b2=false;	
									buildingoccuchk[1].setChecked(true);
									((EditText)findViewById(R.id.edvacantpercent)).setEnabled(true);
									((EditText)findViewById(R.id.edvacantpercent)).setText(String.valueOf(occ));
								}
								else
								{
									b1=true;
								}							
							}
					}
					else
					{
						if(b1)
						{
							b2=false;
							buildingoccuchk[1].setChecked(false);
							((EditText)findViewById(R.id.edvacantpercent)).setEnabled(false);	
							((EditText)findViewById(R.id.edvacantpercent)).setText("");
						}
					}
				}
			};
		 watchervacant = new TextWatcher() {
				public void onTextChanged(CharSequence s1, int start, int before,
						int count) {
					// TODO Auto-generated method stub
					try{ if(Integer.parseInt(String.valueOf(s1))>100){ ((EditText)findViewById(R.id.edvacantpercent)).setText("100");}}
					catch(Exception e){}
				}

				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
					// TODO Auto-generated method stub
				}

				public void afterTextChanged(Editable e1) {
					// TODO Auto-generated method stub		
					if(!e1.toString().trim().equals(""))
					{
						try 
						{	 
							vacantval = Integer.parseInt(e1.toString().trim()); 
						}
						catch(Exception e2) 
						{
							vacantval=0; 
						}
							String te1 = vacantval+"";
							if(te1.trim().length()>2)
							{
								if(b2)
								{
									b1=false;
									buildingoccuchk[0].setChecked(false);	
									((EditText)findViewById(R.id.edoccupercent)).setEnabled(false);		
									((EditText)findViewById(R.id.edoccupercent)).setText("");
								}
								else
								{
									b2=true;
								}
							}
							else
							{
								int vacant = percentval - vacantval;								
								if(b2)
								{
									b1=false;
									buildingoccuchk[0].setChecked(true);
									((EditText)findViewById(R.id.edoccupercent)).setEnabled(true);
									((EditText)findViewById(R.id.edoccupercent)).setText(String.valueOf(vacant));
								}
								else
								{
									b2=true;
								}
							}
					}
					else
					{
						if(b2)
						{
							b1=false;
							buildingoccuchk[0].setChecked(false);
							((EditText)findViewById(R.id.edoccupercent)).setEnabled(false);	
							((EditText)findViewById(R.id.edoccupercent)).setText("");
						}
					}
				}
			};
			((EditText)findViewById(R.id.edoccupercent)).addTextChangedListener(watcherocccu);
			((EditText)findViewById(R.id.edvacantpercent)).addTextChangedListener(watchervacant);
			((RadioButton) permitconfirmedval.findViewWithTag("No")).setChecked(true);
			
			  ((EditText)findViewById(R.id.edbuildingsize)).addTextChangedListener(new textwatcher(3,((EditText)findViewById(R.id.edbuildingsize))));
			  ((EditText)findViewById(R.id.edyocother)).addTextChangedListener(new textwatcher(4, ((EditText)findViewById(R.id.edyocother))));
			  
			  ((EditText)findViewById(R.id.scope_other)).addTextChangedListener(new CustomTextWatcherSpace(((EditText)findViewById(R.id.scope_other))));
			  ((EditText)findViewById(R.id.insuredis_other)).addTextChangedListener(new CustomTextWatcherSpace(((EditText)findViewById(R.id.insuredis_other))));
			  ((EditText)findViewById(R.id.neigh_other)).addTextChangedListener(new CustomTextWatcherSpace(((EditText)findViewById(R.id.neigh_other))));
			  
			  ((EditText)findViewById(R.id.occupancy_other)).addTextChangedListener(new CustomTextWatcherSpace(((EditText)findViewById(R.id.occupancy_other))));
			  ((EditText)findViewById(R.id.edoccuchkother)).addTextChangedListener(new CustomTextWatcherSpace(((EditText)findViewById(R.id.edoccuchkother))));
			  ((EditText)findViewById(R.id.edaddfencesother)).addTextChangedListener(new CustomTextWatcherSpace(((EditText)findViewById(R.id.edaddfencesother))));
			  ((EditText)findViewById(R.id.etpermitconfirmed)).addTextChangedListener(new textwatcher(5,((EditText)findViewById(R.id.etpermitconfirmed))));
			
	}
	class checklistenetr implements OnCheckedChangeListener
	{
    	int i;
		public checklistenetr(int i) {
			// TODO Auto-generated constructor stub
		 this.i=i;
		}

		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			   RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
		        // This puts the value (true/false) into the variable
		        boolean isChecked = checkedRadioButton.isChecked();
		        // If the radiobutton that has changed in check state is now checked...
		        if (isChecked)
		        {
		          switch (i) {
					case 1:
						permitconftxt= checkedRadioButton.getText().toString().trim();
						if(permitconftxt.equals("No")){
							((LinearLayout)findViewById(R.id.permitconfirmedlin)).setVisibility(cf.v1.GONE);
							((EditText)findViewById(R.id.etpermitconfirmed)).setText("");}						
						else if(permitconftxt.equals("Yes")){
							((LinearLayout)findViewById(R.id.permitconfirmedlin)).setVisibility(cf.v1.VISIBLE);
							SelectBItbl();
							System.out.println("cf.per="+Permit_Confirmed);
							if(Permit_Confirmed.equals("") || Permit_Confirmed.contains("No"))
							{
								((EditText)findViewById(R.id.etpermitconfirmed)).setText("Beyond Scope of Inspection");
							}
						}
						 break;
					case 2:
						addistrucrdgtxt= checkedRadioButton.getText().toString().trim();
						if(addistrucrdgtxt.equals("Yes")){
							((LinearLayout)findViewById(R.id.addistrcu_layt1)).setVisibility(cf.v1.VISIBLE);
						((LinearLayout)findViewById(R.id.addistrcu_layt2)).setVisibility(cf.v1.VISIBLE);
						((LinearLayout)findViewById(R.id.addistrcu_layt3)).setVisibility(cf.v1.VISIBLE);
						((LinearLayout)findViewById(R.id.addistrcu_layt4)).setVisibility(cf.v1.VISIBLE);}
						
						else if(addistrucrdgtxt.equals("No")){
							 clearadddtstruc();
							((LinearLayout)findViewById(R.id.addistrcu_layt1)).setVisibility(cf.v1.GONE);
						((LinearLayout)findViewById(R.id.addistrcu_layt2)).setVisibility(cf.v1.GONE);
						((LinearLayout)findViewById(R.id.addistrcu_layt3)).setVisibility(cf.v1.GONE);
						((LinearLayout)findViewById(R.id.addistrcu_layt4)).setVisibility(cf.v1.GONE);
						}
						 break;
					case 3:						
						incioccutxt= checkedRadioButton.getText().toString().trim();
						if(incioccutxt.equals("Yes")){
							((LinearLayout)findViewById(R.id.incioccu_layt1)).setVisibility(cf.v1.VISIBLE);
							((LinearLayout)findViewById(R.id.incioccu_layt2)).setVisibility(cf.v1.VISIBLE);
						}
						else if(incioccutxt.equals("No")){
							 for(int i=0;i<incidentalchk.length;i++)
						   	 {
						   		incidentalchk[i].setChecked(false);
						   	 }
							((LinearLayout)findViewById(R.id.incioccu_layt1)).setVisibility(cf.v1.GONE);
						((LinearLayout)findViewById(R.id.incioccu_layt2)).setVisibility(cf.v1.GONE);}
						 break;
					case 4:						
						neighbourval = checkedRadioButton.getText().toString().trim();
						break;
		          }
		        }
		}
	 }
		        
	class textwatcher implements TextWatcher
	 {
		 public int type;
	       public EditText ed;
	         textwatcher(int type,EditText ed)
	    	{
	        	 System.out.println("what is type="+type);
	        	 this.type=type;
	    		this.ed = ed;
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {
	    		// TODO Auto-generated method stub
	    		System.out.println("type="+type);
	    		if(this.type==1)
	    		{
	    			if(neighbourval.equals("Commercial") || neighbourval.equals("Industrial") ||  neighbourval.equals("Rural"))
	    			{
		    			 if(!s.toString().equals("")){
		    			     if(Character.toString(s.toString().charAt(0)).equals("0"))
			    			 {
			    				 cf.ShowToast("Invalid Entry! Please enter the valid Number of Buildings.", 0);
			    				// ((EditText)findViewById(R.id.ednoofbuildings)).setText("1");cf.hidekeyboard();
			    			 }
		    			 }
		    			 if (ed.getText().toString().startsWith(" "))
		    		        {
		    		            // Not allowed
		    		        	ed.setText("");
		    		        }
	    			}
	    		}	
	    		else if(this.type==2)
	    		{
	    				 if(!s.toString().equals("")){
		    			     if(Character.toString(s.toString().charAt(0)).equals("0"))
			    			 {
		    			    	 cf.ShowToast("Invalid Entry! Please enter the valid Number of Units.", 0);
			    				 ((EditText)findViewById(R.id.ednoofunits)).setText("");cf.hidekeyboard();
			    			 }
		    			 }
	    				 if (ed.getText().toString().startsWith(" "))
		    		        {
		    		            // Not allowed
		    		        	ed.setText("");
		    		        }
	    		}	
	    		else if(this.type==3)
	    		{
	    			 if(!s.toString().equals("")){
	    			     if(Character.toString(s.toString().charAt(0)).equals("0"))
		    			 {
	    			    	 cf.ShowToast("Invalid Entry! Please enter the valid Building Size.", 0);
		    				 ((EditText)findViewById(R.id.edbuildingsize)).setText("");cf.hidekeyboard();
		    			 }
	    			 }
	    			 if (ed.getText().toString().startsWith(" "))
	    		        {
	    		            // Not allowed
	    		        	ed.setText("");
	    		        }
	    		}
	    		else if(this.type==4)
	    		{
	    			 if(!s.toString().equals("")){
	    			     if(Character.toString(s.toString().charAt(0)).equals("0"))
		    			 {
	    			    	 cf.ShowToast("Invalid Entry! Please enter the valid Year of Home.", 0);
		    				 ((EditText)findViewById(R.id.edyocother)).setText("");cf.hidekeyboard();
		    			 }
	    			 } 
	    			 if (ed.getText().toString().startsWith(" "))
	    		        {
	    		            // Not allowed
	    		        	ed.setText("");
	    		        }
	    		}
	    		else if(this.type==5)
	    		{
	    			System.out.println("came here="+s.toString());
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.permit_tv_type1),250); 
	    		}
	    		else if(this.type==6)
	    		{
	    			 if(!s.toString().equals("")){
	    			     if(Character.toString(s.toString().charAt(0)).equals("0"))
		    			 {
	    			    	 cf.ShowToast("Invalid Entry! Please enter the valid No of Stories.", 0);
		    				 ((EditText)findViewById(R.id.noofstories)).setText("");cf.hidekeyboard();
		    			 }
	    			     /*else if(Integer.parseInt(s.toString())>100)
	    			     {
	    			    	 cf.ShowToast("Please enter the No of Stories less than 100.", 0);
		    				 ((EditText)findViewById(R.id.noofstories)).setText("");cf.hidekeyboard();
	    			     }
	    			     else if(!cf.CommercialFlag.equals("0"))
	    			     {
	    			    	 if(cf.Stories.equals("1") || cf.Stories.equals("2")  || cf.Stories.equals("3"))
	    					 {
	    						if(Integer.parseInt(s.toString())>3)
	    						{
	    							cf.ShowToast("Please enter the No of Stories between 1-3.", 0);
	   		    				   ((EditText)findViewById(R.id.noofstories)).setText("");cf.hidekeyboard();
	    						}
	    					 }
	    					 else if(cf.Stories.equals("4") || cf.Stories.equals("5")  || cf.Stories.equals("6"))
	    					 {
	    						 if(Integer.parseInt(s.toString())<4 || Integer.parseInt(s.toString())>6)
		    					 {
	    							 cf.ShowToast("Please enter the No of Stories between 4-6.", 0);
		   		    				 ((EditText)findViewById(R.id.noofstories)).setText("");cf.hidekeyboard();
		    					 }	
	    					 }
	    					 else 
	    					 {
	    						 if(Integer.parseInt(s.toString())<7 || Integer.parseInt(s.toString())>100)
		    					 {
	    							 cf.ShowToast("Please enter the No of Stories between 7-100.", 0);
		   		    				 ((EditText)findViewById(R.id.noofstories)).setText("");cf.hidekeyboard();
		    					 }	
	    					 }
	    			     }*/
	    			 } 
	    		}
	    	}
	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,

	    			int after) {
	    		// TODO Auto-generated method stub
	    	}
	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,int count) {
	    		// TODO Auto-generated method stub
	    		 if (ed.getText().toString().startsWith(" "))
	    	     {
	    	            // Not allowed
	    			 ed.setText("");
	    	     }
	    	}	    	
	    }
	class  RadioArrayclickerInsured implements OnClickListener
	{
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			for(int i=0;i<insuredchk.length;i++)
			{
				if(v.getId()==insuredchk[i].getId())
				{	
					 if(insuredchk[insuredchk.length-1].isChecked())
					{
						setvisible(((EditText)findViewById(R.id.insuredis_other)));
					}
					else
					{
						setInVisible(((EditText)findViewById(R.id.insuredis_other)));
					}
					insuredchk[i].setChecked(true);
				}
				else
				{
					insuredchk[i].setChecked(false);
					setInVisible(((EditText)findViewById(R.id.insuredis_other)));
				}
			}
		}
	}
	class  RadioArrayclickerOccupancy implements OnClickListener
	{
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			for(int i=0;i<occuchk.length;i++)
			{
				if(v.getId()==occuchk[i].getId())
				{	
					 if(occuchk[occuchk.length-1].isChecked())
					{
						setvisible(((EditText)findViewById(R.id.occupancy_other)));
					}
					else
					{
						setInVisible(((EditText)findViewById(R.id.occupancy_other)));
					}
					 occuchk[i].setChecked(true);
				}
				else
				{
					occuchk[i].setChecked(false);
					setInVisible(((EditText)findViewById(R.id.occupancy_other)));
				}
			}
		}
	}
	class  RadioArrayclickerNeighbour implements OnClickListener
	{
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			for(int i=0;i<neighbouroptionchk.length;i++)
			{
				if(v.getId()==neighbouroptionchk[i].getId())
				{	
					 if(neighbouroptionchk[neighbouroptionchk.length-1].isChecked())
					{
						setvisible(((EditText)findViewById(R.id.neigh_other)));
					}
					else
					{
						setInVisible(((EditText)findViewById(R.id.neigh_other)));
					}
					 neighbouroptionchk[i].setChecked(true);
				}
				else
				{
					neighbouroptionchk[i].setChecked(false);
					setInVisible(((EditText)findViewById(R.id.neigh_other)));
				}
			}
		}
	}
	private void setvisible(EditText editText) {
		// TODO Auto-generated method stub
		editText.setVisibility(cf.v1.VISIBLE);
		cf.setFocus(editText);			
	}
	public void clearadddtstruc() {
		// TODO Auto-generated method stub
		BI_ADDI1[0].setChecked(false);
		 BI_ADDI1[1].setChecked(false);	 
		 BI_ADDI1[2].setChecked(false);
		 BI_ADDI2[0].setChecked(false);
		 BI_ADDI2[1].setChecked(false);
		 BI_ADDI2[2].setChecked(false);
		 BI_ADDI2[3].setChecked(false);
		 BI_ADDI2[4].setChecked(false);
		 
		 BI_ADDI2[5].setChecked(false);
		 BI_ADDI2[6].setChecked(false);
		 BI_ADDI2[7].setChecked(false);
		 BI_ADDI2[8].setChecked(false);
		 BI_ADDI2[9].setChecked(false);
		 BI_ADDI2[10].setChecked(false);
		 ((EditText)findViewById(R.id.edaddfencesother)).setText("");
		 ((EditText)findViewById(R.id.edaddfencesother)).setVisibility(cf.v1.GONE);
	}
	private void setInVisible(EditText editText) {
		// TODO Auto-generated method stub
		editText.setVisibility(cf.v1.GONE);
		editText.setText("");
	}
	class Spin_Selectedlistener implements OnItemSelectedListener
	{
		int i;
		public Spin_Selectedlistener(int i) {
			// TODO Auto-generated constructor stub
			this.i=i;
		}

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
			// TODO Auto-generated method stub
			if(i==1){
				yocstringother = spin_yoc.getSelectedItem().toString();chkvl=0;
				 if(!yocstringother.equals("--Select--"))
				 {
						if(yocstringother.equals("Other"))
						{
							((EditText)findViewById(R.id.edyocother)).setVisibility(cf.v1.VISIBLE);
							 yrbuiltoption[0].setChecked(false);
							 yrbuiltoption[1].setChecked(false);
			 				 yrbuiltoption[2].setChecked(true);
			 				
		                }
						else if(yocstringother.equals("Unknown"))
						{
							((EditText)findViewById(R.id.edyocother)).setText("");
							((EditText)findViewById(R.id.edyocother)).setVisibility(cf.v1.GONE);
						
							
		 					yrbuiltoption[0].setChecked(false);
		 					yrbuiltoption[1].setChecked(true);
		 					yrbuiltoption[2].setChecked(false);
						}
						else
						{
							((EditText)findViewById(R.id.edyocother)).setText("");
							((EditText)findViewById(R.id.edyocother)).setVisibility(cf.v1.GONE);
							 yrbuiltoption[1].setChecked(false);
		 					yrbuiltoption[0].setChecked(true);
		 					yrbuiltoption[2].setChecked(false);
						}
				 }
				 else
				 {
					 System.out.println("yocstringothe"+yoc);
					 ((EditText)findViewById(R.id.edyocother)).setText("");
						((EditText)findViewById(R.id.edyocother)).setVisibility(cf.v1.GONE);
				
					 if(yoc==1)
					 {
						 yrbuiltoption[2].setChecked(true);
					 }
					 else
					 {
						 yrbuiltoption[2].setChecked(false);
					 }
				 }
			}
			/*else if(i==2){
				buildingtypevalue = nostoriesspin.getSelectedItem().toString();
				System.out.println("buildingtypevalue"+buildingtypevalue);
				if(!buildingtypevalue.equals("--Select--"))
				{
					if(buildingtypevalue.equals("1") || buildingtypevalue.equals("2")  || buildingtypevalue.equals("3"))
					{
						typechkvalue();
						((CheckBox)findViewById(R.id.typ_chk1)).setChecked(true);		
						((CheckBox)findViewById(R.id.typ_chk2)).setEnabled(false);
						((CheckBox)findViewById(R.id.typ_chk3)).setEnabled(false);
					}
					else if(buildingtypevalue.equals("4") || buildingtypevalue.equals("5")  || buildingtypevalue.equals("6"))
					{
						typechkvalue();
						((CheckBox)findViewById(R.id.typ_chk2)).setChecked(true);		
						((CheckBox)findViewById(R.id.typ_chk3)).setEnabled(false);		
						((CheckBox)findViewById(R.id.typ_chk1)).setEnabled(false);
					}
					else if(Integer.parseInt(buildingtypevalue)>6)
					{
						typechkvalue();
						((CheckBox)findViewById(R.id.typ_chk3)).setChecked(true);
						((CheckBox)findViewById(R.id.typ_chk2)).setEnabled(false);		
						((CheckBox)findViewById(R.id.typ_chk1)).setEnabled(false);	
					}
					else 
					{	
						typechkvalue();
					}
				}
				else
				{
					typechkvalue();
				}
			}*/
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
	}
	private void typechkvalue()
	{
		((CheckBox)findViewById(R.id.typ_chk1)).setChecked(false);	
		((CheckBox)findViewById(R.id.typ_chk2)).setChecked(false);
		((CheckBox)findViewById(R.id.typ_chk3)).setChecked(false);
		((CheckBox)findViewById(R.id.typ_chk1)).setEnabled(false);	
		((CheckBox)findViewById(R.id.typ_chk2)).setEnabled(false);
		((CheckBox)findViewById(R.id.typ_chk3)).setEnabled(false);
	}
	
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.gensave:
			savevaldiation();
			break;
		case R.id.genchk:			
			if(((CheckBox)findViewById(R.id.genchk)).isChecked())
			{
				gennotappl="1";
				if(!cf.Gendata_val.equals(""))
				{
					AlertDialog.Builder builder = new AlertDialog.Builder(BIGeneralData.this);
					builder.setTitle("Confirmation");
					builder.setIcon(R.drawable.alertmsg);
					builder.setMessage("Do you want to clear the General data Information?").setCancelable(false)
							.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,int id1) 
										{
												defaultbi();
												((RadioButton) addistrucrdgval.findViewWithTag("No")).setChecked(true);
												((RadioButton) permitconfirmedval.findViewWithTag("No")).setChecked(true);
												((CheckBox)findViewById(R.id.genchk)).setChecked(true);
												((LinearLayout)findViewById(R.id.generaldatalin)).setVisibility(cf.v1.GONE);
												((TextView)findViewById(R.id.savegen)).setVisibility(cf.v1.GONE);
										}
									})
							.setNegativeButton("No",new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,	int id) {
											
											((CheckBox)findViewById(R.id.genchk)).setChecked(false);
											dialog.cancel();											
									}
							});
					builder.show();
				}
				else
				{
					((TextView)findViewById(R.id.savegen)).setVisibility(cf.v1.GONE);
					((LinearLayout)findViewById(R.id.generaldatalin)).setVisibility(cf.v1.GONE);
				}
				
			}
			else
			{
				gennotappl="0";
				defaultbi();gendataclear();
				((LinearLayout)findViewById(R.id.generaldatalin)).setVisibility(cf.v1.VISIBLE);
				((TextView)findViewById(R.id.savegen)).setVisibility(cf.v1.GONE);
			}
			break;
		case R.id.scopeofinsp_chk5:
			if(((CheckBox)findViewById(R.id.scopeofinsp_chk5)).isChecked())
			{
				((EditText)findViewById(R.id.scope_other)).setVisibility(cf.v1.VISIBLE);
				cf.setFocus((EditText)findViewById(R.id.scope_other));
			}
			else
			{
				((EditText)findViewById(R.id.scope_other)).setVisibility(cf.v1.GONE);
				((EditText)findViewById(R.id.scope_other)).setText("");
			}				
			break;
		case R.id.addstruopt2_chk11:
			if(((CheckBox)findViewById(R.id.addstruopt2_chk11)).isChecked())
			{
				((EditText)findViewById(R.id.edaddfencesother)).setVisibility(cf.v1.VISIBLE);
				cf.setFocus((EditText)findViewById(R.id.edaddfencesother));
			}
			else
			{
				((EditText)findViewById(R.id.edaddfencesother)).setVisibility(cf.v1.GONE);
				((EditText)findViewById(R.id.edaddfencesother)).setText("");
			}				
			break;
		case R.id.neigh_chk1:
			if(((RadioButton)findViewById(R.id.neigh_chk1)).isChecked())
			{
				showlayout();
				for(int i=0;i<commrdio.length;i++)
				{
					commrdio[i].setTextColor(Color.BLACK);
					commrdio[i].setEnabled(true);	
				}
				((LinearLayout)findViewById(R.id.noofbuillin)).setVisibility(cf.v1.VISIBLE);
			//	((EditText)findViewById(R.id.ednoofbuildings)).setText("1");
				//((LinearLayout)findViewById(R.id.neighb_option)).setVisibility(cf.v1.VISIBLE);
				//((LinearLayout)findViewById(R.id.btypelin)).setVisibility(cf.v1.VISIBLE);
				//((LinearLayout)findViewById(R.id.inciocculin)).setVisibility(cf.v1.VISIBLE);
			}
			else
			{
				hidelayout();
			}
			break;
		case R.id.neigh_chk2:
			if(((RadioButton)findViewById(R.id.neigh_chk2)).isChecked())
			{
				showlayout();				
				for(int i=0;i<resrdio.length;i++)
				{
					resrdio[i].setTextColor(Color.BLACK);
					resrdio[i].setEnabled(true);	
				}				
				((LinearLayout)findViewById(R.id.noofbuillin)).setVisibility(cf.v1.GONE);
				//((LinearLayout)findViewById(R.id.btypelin)).setVisibility(cf.v1.GONE);
				//((LinearLayout)findViewById(R.id.inciocculin)).setVisibility(cf.v1.GONE);
				((LinearLayout)findViewById(R.id.incioccu_layt1)).setVisibility(cf.v1.GONE);
				((LinearLayout)findViewById(R.id.incioccu_layt2)).setVisibility(cf.v1.GONE);
			}
			else
			{
				hidelayout();
			}
			break;
		case R.id.neigh_chk3:
			if(((RadioButton)findViewById(R.id.neigh_chk3)).isChecked())
			{				
				industshowlayout();				
			}
			break;
		case R.id.neigh_chk4:
			if(((RadioButton)findViewById(R.id.neigh_chk4)).isChecked())
			{
				industshowlayout();
			}
			break;
		case R.id.ocuupancy_chk5:
			if(((CheckBox)findViewById(R.id.ocuupancy_chk5)).isChecked())
			{setvisible(((EditText)findViewById(R.id.edoccuchkother)));}
			else{setInVisible(((EditText)findViewById(R.id.edoccuchkother)));}	
			break;	
		case R.id.occu_chk:
			if(((CheckBox)findViewById(R.id.ocuupancy_chk2)).isChecked()==false)
			{
				if(((CheckBox)findViewById(R.id.occu_chk)).isChecked())
				{
					if(((EditText)findViewById(R.id.edvacantpercent)).getText().toString().equals("100"))
					{
						((EditText)findViewById(R.id.edoccupercent)).setText("0");
					}
					else
					{
						((EditText)findViewById(R.id.edoccupercent)).setText("100");
					}
					 
					((CheckBox)findViewById(R.id.occu_chk)).setChecked(true);
					((EditText)findViewById(R.id.edoccupercent)).setEnabled(true);
				}
				else
				{
					((EditText)findViewById(R.id.edvacantpercent)).setText("100");
					((CheckBox)findViewById(R.id.vacant_chk)).setChecked(true);
					((CheckBox)findViewById(R.id.occu_chk)).setChecked(false);
					b1=false;
					((EditText)findViewById(R.id.edoccupercent)).setText("");	
					((EditText)findViewById(R.id.edoccupercent)).setEnabled(false);
				}
			}
			else
			{
				((CheckBox)findViewById(R.id.occu_chk)).setChecked(false);
				((EditText)findViewById(R.id.edoccupercent)).setEnabled(false);
			}
			break;
		case R.id.vacant_chk:
			if(((CheckBox)findViewById(R.id.ocuupancy_chk2)).isChecked()==false)
			{
				if(((CheckBox)findViewById(R.id.vacant_chk)).isChecked())
				{
					if(((EditText)findViewById(R.id.edoccupercent)).getText().toString().equals("100"))
					{
						((EditText)findViewById(R.id.edvacantpercent)).setText("0");
					}
					else
					{
						((EditText)findViewById(R.id.edvacantpercent)).setText("100");
					}
					
					((CheckBox)findViewById(R.id.vacant_chk)).setChecked(true);
					((EditText)findViewById(R.id.edvacantpercent)).setEnabled(true);
				}
				else
				{
					((EditText)findViewById(R.id.edoccupercent)).setText("100");
					((CheckBox)findViewById(R.id.occu_chk)).setChecked(true);
					((CheckBox)findViewById(R.id.vacant_chk)).setChecked(false);
					b2=false;
					((EditText)findViewById(R.id.edvacantpercent)).setText("");	
					((EditText)findViewById(R.id.edvacantpercent)).setEnabled(false);
				}
			}
			else
			{
				((CheckBox)findViewById(R.id.vacant_chk)).setChecked(false);
				((EditText)findViewById(R.id.edvacantpercent)).setEnabled(false);
			}
			break;
		case R.id.ocuupancy_chk2:
			if(((CheckBox)findViewById(R.id.ocuupancy_chk2)).isChecked())
			{
				((CheckBox)findViewById(R.id.occu_chk)).setChecked(false);((CheckBox)findViewById(R.id.occu_chk)).setEnabled(false);
				((CheckBox)findViewById(R.id.vacant_chk)).setChecked(false);((CheckBox)findViewById(R.id.vacant_chk)).setEnabled(false);
				((EditText)findViewById(R.id.edoccupercent)).setEnabled(false);
				((EditText)findViewById(R.id.edvacantpercent)).setEnabled(false);
				 b2=false;b1=false;
				((EditText)findViewById(R.id.edoccupercent)).setText("");	
				 b2=false;b1=false;
				((EditText)findViewById(R.id.edvacantpercent)).setText("");					
			}
			else
			{
				((CheckBox)findViewById(R.id.occu_chk)).setEnabled(true);
				((CheckBox)findViewById(R.id.vacant_chk)).setEnabled(true);
			}
			break;
		case R.id.yrbuiltrdio1:		
			if(spin_yoc.getSelectedItem().toString().equals("Unknown") || spin_yoc.getSelectedItem().toString().equals("Other"))
			{
				((EditText)findViewById(R.id.edyocother)).setVisibility(cf.v1.GONE);				
				spin_yoc.setSelection(0);
			}
			yoc=0;
			yearofhomedefault();			
			yrbuiltoption[0].setChecked(true);
			break;
		case R.id.yrbuiltrdio2:
			int spnrpos1 =spinyoc_adap.getPosition("Unknown");
			spin_yoc.setSelection(spnrpos1);
			yearofhomedefault();
			yrbuiltoption[1].setChecked(true);			
			break;
		case R.id.yrbuiltrdio3:		
			spin_yoc.setSelection(0);
			yoc=1;
			yearofhomedefault();
			yrbuiltoption[2].setChecked(true);			
			break;
		}		
	}
	private void gendataclear() {
		// TODO Auto-generated method stub
		  ((RadioButton) addistrucrdgval.findViewWithTag("No")).setChecked(true);
		  ((RadioButton) incidentoccuval.findViewWithTag("No")).setChecked(true);
		  ((CheckBox)findViewById(R.id.typ_chk4)).setChecked(true);
		  ((CheckBox)findViewById(R.id.ocuupancy_chk2)).setChecked(true);
		  if(!cf.CommercialFlag.equals("0"))
			{
				neighbourchk[0].setChecked(true);
			}
			else
			{
				neighbourchk[1].setChecked(true);
			}
	}
	private void yearofhomedefault() {
		// TODO Auto-generated method stub
		for(int i=0;i<yrbuiltoption.length;i++)
		{
			yrbuiltoption[i].setChecked(false);
		}
	}
	private void industshowlayout() {
		// TODO Auto-generated method stub
		//occupancyunchk();
		for(int i=0;i<occuchk.length;i++)
		{
			occuchk[i].setChecked(false);
			occuchk[i].setTextColor(Color.BLACK);
			occuchk[i].setEnabled(true);
		}
		//occuchk[20].setEnabled(false);
		((LinearLayout)findViewById(R.id.noofbuillin)).setVisibility(cf.v1.VISIBLE);
		//((EditText)findViewById(R.id.ednoofbuildings)).setText("1");
	//	((LinearLayout)findViewById(R.id.neighb_option)).setVisibility(cf.v1.VISIBLE);
		//((LinearLayout)findViewById(R.id.btypelin)).setVisibility(cf.v1.VISIBLE);
		((LinearLayout)findViewById(R.id.noofunitslin)).setVisibility(cf.v1.VISIBLE);
		((LinearLayout)findViewById(R.id.bsizelin)).setVisibility(cf.v1.VISIBLE);
		((LinearLayout)findViewById(R.id.bocculin1)).setVisibility(cf.v1.VISIBLE);
		((LinearLayout)findViewById(R.id.bocculin2)).setVisibility(cf.v1.VISIBLE);
		//((LinearLayout)findViewById(R.id.btypelin)).setVisibility(cf.v1.VISIBLE);
		//((LinearLayout)findViewById(R.id.inciocculin)).setVisibility(cf.v1.VISIBLE);
		//((LinearLayout)findViewById(R.id.incioccu_layt1)).setVisibility(cf.v1.VISIBLE);
		//((LinearLayout)findViewById(R.id.incioccu_layt2)).setVisibility(cf.v1.VISIBLE);
		((LinearLayout)findViewById(R.id.balconieslin)).setVisibility(cf.v1.VISIBLE);
		((EditText)findViewById(R.id.occupancy_other)).setVisibility(cf.v1.GONE);
		((EditText)findViewById(R.id.occupancy_other)).setText("");
	}
	private void showlayout() {
		// TODO Auto-generated method stub
		occupancyunchk();
		//((LinearLayout)findViewById(R.id.neighb_option)).setVisibility(cf.v1.VISIBLE);
		((LinearLayout)findViewById(R.id.noofunitslin)).setVisibility(cf.v1.VISIBLE);
		((LinearLayout)findViewById(R.id.bsizelin)).setVisibility(cf.v1.VISIBLE);		
		((LinearLayout)findViewById(R.id.bocculin1)).setVisibility(cf.v1.VISIBLE);
		((LinearLayout)findViewById(R.id.bocculin2)).setVisibility(cf.v1.VISIBLE);
		((LinearLayout)findViewById(R.id.balconieslin)).setVisibility(cf.v1.VISIBLE);
		((LinearLayout)findViewById(R.id.addstruclin)).setVisibility(cf.v1.VISIBLE);
	}
	private void occupancyunchk()
	{
		for(int i=0;i<occuchk.length;i++)
		{
			occuchk[i].setChecked(false);
			occuchk[i].setTextColor(Color.GRAY);
			occuchk[i].setEnabled(false);
		}
		((EditText)findViewById(R.id.occupancy_other)).setVisibility(cf.v1.GONE);
		((EditText)findViewById(R.id.occupancy_other)).setText("");
	}
	private void hidelayout() {
		// TODO Auto-generated method stub
		for(int i=0;i<occuchk.length;i++)
		{
			occuchk[i].setTextColor(Color.BLACK);
			occuchk[i].setEnabled(true);
		}
		((LinearLayout)findViewById(R.id.noofunitslin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.bsizelin)).setVisibility(cf.v1.GONE);
		//((LinearLayout)findViewById(R.id.btypelin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.bocculin1)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.bocculin2)).setVisibility(cf.v1.GONE);

		
	}

	private void savevaldiation() {
		// TODO Auto-generated method stub
				BI_SCOPEOFINSP= cf.getselected_chk(scopeinschk);
				String scopeothrval =(scopeinschk[scopeinschk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.scope_other)).getText().toString():""; 
				BI_SCOPEOFINSP+=scopeothrval;System.out.println("BI__="+BI_SCOPEOFINSP);
				 if(BI_SCOPEOFINSP.equals(""))
				 {
					 cf.ShowToast("Please select the option for Scope of Inspection." , 0);
				 }
				 else
				 {
					 if(scopeinschk[scopeinschk.length-1].isChecked())
					 {	 
						 if(scopeothrval.trim().equals("&#40;"))
						 {
							 cf.ShowToast("Please enter the other text for Scope of Inspection.", 0);
							 cf.setFocus(((EditText)findViewById(R.id.scope_other)));
						 }
						 else
						 {
							 BInotapplvalidation();	
						 }					 
					 }
					 else
					 {
						 BInotapplvalidation();
					 }
				 }
	}
	private void BInotapplvalidation()
	{
		if(((CheckBox)findViewById(R.id.genchk)).isChecked()==false)
		{
			insuresisvalidation();
		}
		else
		{
			BI_InsertGeneralData();
		}
	}
	private void insuresisvalidation()
	{
		BI_INSURED=cf.getslected_radio(insuredchk);	
		if(BI_INSURED.equals(""))
		{
			cf.ShowToast("Please select the option for Insured/Client Is." , 0);
		}
		else
		{
			 if(insuredchk[insuredchk.length-1].isChecked())
			 {	 
				 if(((EditText)findViewById(R.id.insuredis_other)).getText().toString().trim().equals(""))
				 {
					cf.ShowToast("Please enter the other text for Insured/Client Is.", 0);
					cf.setFocus(((EditText)findViewById(R.id.insuredis_other)));
				 }
				 else
				 {
					 NeighBourhoodvalidation();	
				 }			 
			 }
			 else
			 {
				 NeighBourhoodvalidation();	
			 }
		}
	}
	private void NeighBourhoodvalidation() {
		// TODO Auto-generated method stub	
		BI_NEIGHBOUR=cf.getslected_radio(neighbourchk);	
		if(BI_NEIGHBOUR.equals(""))
		{
			cf.ShowToast("Please select the option for Neighbourhood Is." , 0);
		}
		else
		{
			BI_NEIGHBOUROPTION=cf.getslected_radio(neighbouroptionchk);
			if(BI_NEIGHBOUROPTION.equals(""))
			{
				cf.ShowToast("Please select the sub option for Neighbourhood Is." , 0);
			}
			else
			{
				if(neighbouroptionchk[neighbouroptionchk.length-1].isChecked())
				 {	 
					 if(((EditText)findViewById(R.id.neigh_other)).getText().toString().trim().equals(""))
					 {
						cf.ShowToast("Please enter the other text for Neighbourhood Is Sub-Sets.", 0);
						 cf.setFocus(((EditText)findViewById(R.id.neigh_other)));
					 }
					 else
					 {
						 Occupancyvalidation();	
					 }			 
				 }
				 else
				 {
					 Occupancyvalidation();	
				 }	
			}
		}		
	}
	private void Occupancyvalidation() {
		// TODO Auto-generated method stub	
		BI_OCCU_TYPE=cf.getslected_radio(occuchk);	
		if(BI_OCCU_TYPE.equals(""))
		{
			 cf.ShowToast("Please select the option for Occupancy Type." , 0);
		}
		else
		{
				//if(neighbourval.equals("Commercial") || neighbourval.equals("Residential") || neighbourval.equals("Rural") || neighbourval.equals("Industrial"))
				//{
					if(occuchk[occuchk.length-1].isChecked())
					{	 
						if(((EditText)findViewById(R.id.occupancy_other)).getText().toString().trim().equals(""))
						 {
							 cf.ShowToast("Please enter the other text for Occupancy Type.", 0);
							 cf.setFocus(((EditText)findViewById(R.id.occupancy_other)));
						 }
						 else
						 {
							 noofbuildingsvalidation();	
						 }			 
					}
					else
					{
						noofbuildingsvalidation();	
					}
				/*}
				else
				{
						if(!"--Select--".equals(nostoriesspin.getSelectedItem().toString()))
						{
							yearofconstvalidation();
						}
						else
						{
							cf.ShowToast("Please Select Number of Stories", 0);
						}
				}*/
		}		
	}
	private void noofbuildingsvalidation() 
	{
		if(neighbourval.equals("Commercial") || neighbourval.equals("Rural") || neighbourval.equals("Industrial"))
		{
			String noofbuildings = ((EditText)findViewById(R.id.ednoofbuildings)).getText().toString().trim();
			if(!"".equals(noofbuildings))
			{
				if(Integer.parseInt(noofbuildings)>0)		
				{
					noofstoriesvalidation();
				}
				else
				{
					cf.ShowToast("Invalid Entry! Please enter the valid Number of Buildings.", 0);
					//((EditText)findViewById(R.id.ednoofbuildings)).setText("1");
					cf.setFocus(((EditText)findViewById(R.id.ednoofbuildings)));cf.hidekeyboard();
				}
			}
			else
			{
				cf.ShowToast("Please enter the Number of Buildings", 0);
				cf.setFocus(((EditText)findViewById(R.id.ednoofbuildings)));
			}
		}
		else
		{
			noofstoriesvalidation();
		}
	}
	private void noofstoriesvalidation() 
	{
		//if(!"--Select--".equals(nostoriesspin.getSelectedItem().toString()))
		if(!"".equals(ednostories.getText().toString().trim()))
		{				
				  int story = Integer.parseInt(ednostories.getText().toString());
				  if(!cf.CommercialFlag.equals("0"))
			      {
				    	 if(cf.Stories.equals("1") || cf.Stories.equals("2")  || cf.Stories.equals("3"))
						 {
								if(story>3)
								{
									cf.ShowToast("Please enter the No of Stories between 1-3.", 0);
				    				   ((EditText)findViewById(R.id.noofstories)).setText("");cf.hidekeyboard(); return;
								}
								else
								{
									NoofUnitsValidation();
								}
						 }
						 else if(cf.Stories.equals("4") || cf.Stories.equals("5")  || cf.Stories.equals("6"))
						 {
								 if(story<4 || story>6)
								 {
									 cf.ShowToast("Please enter the No of Stories between 4-6.", 0);
					    				 ((EditText)findViewById(R.id.noofstories)).setText("");cf.hidekeyboard(); return;
								 }	
								 else
								 {
										NoofUnitsValidation();
								 }
						 }
						 else 
						 {
								 if(story<7)
								 {
									 cf.ShowToast("Please enter the No of Stories between 7-999.", 0);
					    				 ((EditText)findViewById(R.id.noofstories)).setText("");cf.hidekeyboard();
					    				 return;
								 }
								 else
								 {
										NoofUnitsValidation();
								 }
						 }
			      }
				  else
				  {
					   /* if(Integer.parseInt(ednostories.getText().toString())>100)
						{
					    	cf.ShowToast("Please enter Number of Stories less than 999.", 0); return;					  
						}
						else
						{*/
							NoofUnitsValidation();
						//}	
				  }
				
			}
			else
			{
				cf.ShowToast("Please enter Number of Stories", 0);
				cf.setFocus((EditText)findViewById(R.id.noofstories));
			}		
	}
	private void NoofUnitsValidation()
	{
		String noofunits = ((EditText)findViewById(R.id.ednoofunits)).getText().toString().trim();
		if(!"".equals(((EditText)findViewById(R.id.ednoofunits)).getText().toString().trim()))
		{
			if(Integer.parseInt(noofunits)>0)		
			{
				BuildingSizeValidation();
			}
			else
			{
				cf.ShowToast("Invalid Entry! Please enter the valid Number of Units.", 0);
				((EditText)findViewById(R.id.ednoofunits)).setText("");
				cf.setFocus(((EditText)findViewById(R.id.ednoofunits)));cf.hidekeyboard();
			
			}
		}
		else
		{
			cf.ShowToast("Please enter the Number of Units", 0);
			cf.setFocus(((EditText)findViewById(R.id.ednoofunits)));
		}
	}
	private void BuildingSizeValidation() {
		// TODO Auto-generated method stub
		try
		{
			buildingsize = ((EditText)findViewById(R.id.edbuildingsize)).getText().toString().trim();
			b[3]=(Integer.parseInt(buildingsize)>399 && Integer.parseInt(buildingsize)<100000)? true:false;
		}
		catch(Exception e)
		{
			
		}
		
		if(!buildingsize.trim().equals("")) 
		{
			BuildingOccupancyValidation();
			/*if(b[3])
			{
				BuildingOccupancyValidation();
			}
			else
			{
				cf.ShowToast("Please enter the Building Size greater than 399 and less than 1,00,000", 0);
				((EditText)findViewById(R.id.edbuildingsize)).setText("");
				cf.setFocus( ((EditText)findViewById(R.id.edbuildingsize)));
			}*/
		}
		else
		{
			cf.ShowToast("Please enter the Building Size", 0);
			cf.setFocus(((EditText)findViewById(R.id.edbuildingsize)));
		
		}
	}
	private void BuildingOccupancyValidation() {
		// TODO Auto-generated method stub
		BI_BUILDOCCU= cf.getselected_chk(buildingoccuchk);
		surveyothrval =(buildingoccuchk[buildingoccuchk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.edoccuchkother)).getText().toString():""; 
		BI_BUILDOCCU+=surveyothrval;System.out.println("BuildingOccupancyValidation="+BI_BUILDOCCU);
		BI_OCCU_PERCENT = ((EditText)findViewById(R.id.edoccupercent)).getText().toString().trim(); 
		BI_VACANT_PERCENT = ((EditText)findViewById(R.id.edvacantpercent)).getText().toString().trim();
		try
		{

			if(buildingoccuchk[0].isChecked())
			{
				if(!BI_OCCU_PERCENT.equals("")){
					builoccu[0]=true;
				}
				else
				{
					builoccu[0]=false;
				}
			} else{ builoccu[0]=true; }		
			
			
			if(buildingoccuchk[1].isChecked())
			{
				if(!BI_VACANT_PERCENT.equals("")){
					builoccu[1]=true;
				}
				else
				{
					builoccu[1]=false;
				}
			} else{ builoccu[1]=true; }	
			if(!BI_VACANT_PERCENT.equals("") && !BI_OCCU_PERCENT.equals("") )
			{
			builoccu[6]=((Integer.parseInt(BI_VACANT_PERCENT)+(Integer.parseInt(BI_OCCU_PERCENT)))==100)?true:false;
			}
			else
			{
				builoccu[6]=true;
			}
		
			if(buildingoccuchk[0].isChecked()==true && buildingoccuchk[1].isChecked()==false){
				if(!BI_OCCU_PERCENT.equals(""))	
				{
					if(Integer.parseInt(BI_OCCU_PERCENT)<100)
					{
						builoccu[9]=false;
					}
					else
					{
						builoccu[9]=true;
					}
				}
			 } 
			 else
			 { 
				 builoccu[9]=true; 
			 }
				
			 if(buildingoccuchk[1].isChecked()==true && buildingoccuchk[0].isChecked()==false){
				if(!BI_VACANT_PERCENT.equals("")) { if(Integer.parseInt(BI_VACANT_PERCENT)<100){builoccu[10]=false;}else{builoccu[10]=true;}}
			 }else{	builoccu[10]=true; }

		}
		catch(Exception e)
		{
			
		}	
		
		 if(BI_BUILDOCCU.equals(""))
		 {
			 cf.ShowToast("Please select the option for Building Occupancy." , 0);
		 }		 
		 else
		 {
			 if(buildingoccuchk[0].isChecked() || buildingoccuchk[1].isChecked())
			 {
				 if(builoccu[0] && builoccu[1] && builoccu[9] && builoccu[10])
				 {
					BuildOccuotherValid();	
				 }
				 else
				 {
					 if(builoccu[0]==false) { cf.ShowToast("Please enter the Occupied Percentage under Building Occupancy", 0);
						((EditText)findViewById(R.id.edoccupercent)).setEnabled(true);cf.setFocus(((EditText)findViewById(R.id.edoccupercent)));}
					 else if(builoccu[1]==false) { cf.ShowToast("Please enter the Vacant Percentage under Building Occupancy", 0);
						((EditText)findViewById(R.id.edvacantpercent)).setText("");cf.setFocus(((EditText)findViewById(R.id.edvacantpercent)));}
					 else  if(builoccu[9]==false) { 
						 	buildingoccuchk[1].setChecked(true);
							String occuval = ((EditText)findViewById(R.id.edoccupercent)).getText().toString();
							int  occ = percentval - (Integer.parseInt(occuval));
							((EditText)findViewById(R.id.edvacantpercent)).setText(String.valueOf(occ));
							cf.setFocus(((EditText)findViewById(R.id.edoccupercent)));builoccu[9]=true;BuildOccuotherValid();}
					 else if(builoccu[10]==false) {
						 	buildingoccuchk[0].setChecked(true);
							String vacantval = ((EditText)findViewById(R.id.edvacantpercent)).getText().toString();
							int vacant = percentval - (Integer.parseInt(vacantval));
							((EditText)findViewById(R.id.edoccupercent)).setText(String.valueOf(vacant));
							cf.setFocus(((EditText)findViewById(R.id.edoccupercent)));builoccu[10]=true;BuildOccuotherValid();}
				 }
			 }
			 else
			 {
				 BuildOccuotherValid();	
			 }
		 }
		
	}
	private void BuildOccuotherValid() {
		// TODO Auto-generated method stub
		 if(buildingoccuchk[buildingoccuchk.length-1].isChecked())
		 {	 
			 if(surveyothrval.trim().equals("&#40;"))
			 {
				 cf.ShowToast("Please enter the other text for Building Occupancy.", 0);
				 cf.setFocus(((EditText)findViewById(R.id.edoccuchkother)));
			 }
			 else
			 {
				// BI_BUILDOCCU +=(buildingoccuchk[buildingoccuchk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.edoccuchkother)).getText().toString():""; 
				 incidentalvalidation();	
			 }
		 
		 }
		 else
		 {
			 incidentalvalidation();	
		 }
	}
	private void incidentalvalidation() {
		// TODO Auto-generated method stub
		if(neighbourval.equals("Commercial") || neighbourval.equals("Rural") || neighbourval.equals("Industrial"))
		{
			int incioccurdgpid= incidentoccuval.getCheckedRadioButtonId();	
			incioccurdgtxt = (incioccurdgpid==-1)? "":((RadioButton) findViewById(incioccurdgpid)).getText().toString();
			if(!incioccurdgtxt.equals(""))
			{
				if(incioccurdgtxt.equals("Yes"))
				{
					BI_INCIDENTAL=cf.getselected_chk(incidentalchk);	
					if(!BI_INCIDENTAL.equals(""))
					{
						yearofconstvalidation();
					}
					else
					{
						cf.ShowToast("Please select the option for Incidental Occupancy - Yes.", 0); 
					}
				}
				else
				{
					yearofconstvalidation();
				}
			}
			else
			{
				cf.ShowToast("Please select the option for Incidental Occupancy.", 0);
			}
		}
		else
		{
			yearofconstvalidation();
		}
	}	
	private void yearofconstvalidation()
	{	
		yrbuiltrdgtxt=cf.getslected_radio(yrbuiltoption);
		BI_YEARBUILT= spin_yoc.getSelectedItem().toString();
		if(!BI_YEARBUILT.equals("--Select--"))
		{
			if(!yrbuiltrdgtxt.equals(""))
			{
				if(BI_YEARBUILT.equals("Other"))
				{
					String yrothervalue = ((EditText)findViewById(R.id.edyocother)).getText().toString().trim();
					if(!yrothervalue.equals(""))
					{
						if(yrothervalue.length()<4)
						{
							cf.ShowToast("Please enter the year in four digits.", 0);
							cf.setFocus(((EditText)findViewById(R.id.edyocother)));
						}
						else if(cf.yearValidation(((EditText)findViewById(R.id.edyocother)).getText().toString()).equals("true"))
						{
							cf.ShowToast("Year should not exceed the current year.", 0);
							cf.setFocus(((EditText)findViewById(R.id.edyocother)));
						}
						else if(cf.yearValidation(((EditText)findViewById(R.id.edyocother)).getText().toString()).equals("zero"))
						{
							cf.ShowToast("Invalid Entry! Please enter the valid year.", 0);
							((EditText)findViewById(R.id.edyocother)).setText("");
							cf.setFocus(((EditText)findViewById(R.id.edyocother)));cf.hidekeyboard();
						}
						else
						{
							permitconfvalidation();
						}
					}
					else
					{
						cf.ShowToast("Please select the other option for Year of Home", 0);
						cf.setFocus(((EditText)findViewById(R.id.edyocother)));
					}
				}
				else
				{
					permitconfvalidation();
				}	
			}
			else
			{
				cf.ShowToast("Please select the option for Year of Home.", 0);
			}
		}
		else
		{
			cf.ShowToast("Please select Year of Home.", 0);
			cf.setFocus(((EditText)findViewById(R.id.edyocother)));
		}
	}
	private void permitconfvalidation() {
		// TODO Auto-generated method stub
		int premitconfrdgpid= permitconfirmedval.getCheckedRadioButtonId();	
		permitconfrdgtxt = (premitconfrdgpid==-1)? "":((RadioButton) findViewById(premitconfrdgpid)).getText().toString();
		b[6]=(permitconfrdgtxt.equals("Yes"))? ((((EditText)findViewById(R.id.etpermitconfirmed)).getText().toString().trim().equals(""))? false:true):true;
		balconiespresrdgtxt =cf.getslected_radio(balcony_rd);
		
		if(!permitconfrdgtxt.equals("") && b[6]==true)
		{
			if(!balconiespresrdgtxt.equals(""))
			{
				addstructvalidation();
			}
			else
			{													
				cf.ShowToast("Please select the option for Balconies Present", 0);
			}
		}
		else
		{
			if(permitconfrdgtxt.equals(""))
			{
				cf.ShowToast("Please select Permit Confirmed", 0); 
			}
			else
			{
				cf.ShowToast("Please enter the Permit Confirmed Comments", 0);
				cf.setFocus(((EditText)findViewById(R.id.etpermitconfirmed)));
			}	
		}	
	}	
	private void addstructvalidation() {
		// TODO Auto-generated method stub
		if(neighbourval.equals("Commercial") || neighbourval.equals("Residential") || neighbourval.equals("Rural") || neighbourval.equals("Industrial"))
		{			
			int additionalrdgpid= addistrucrdgval.getCheckedRadioButtonId();	
			additionalrdgtxt = (additionalrdgpid==-1)? "":((RadioButton) findViewById(additionalrdgpid)).getText().toString();
			if(!additionalrdgtxt.equals(""))
			{
				if(additionalrdgtxt.equals("Yes"))
				{
					BI_ADDISTRUC1=cf.getselected_chk(BI_ADDI1);
						if(!BI_ADDISTRUC1.equals(""))
						{
							AddiStruc2Valdiation();					
						}
						else
						{
							cf.ShowToast("Please select the option for Light Poles.", 0);	
						}
				}
				else
				{
					BI_InsertGeneralData();
				}
			}
			else
			{
				cf.ShowToast("Please select Additional Structures.", 0); 
			}
		}
		else
		{
			BI_InsertGeneralData();
		}
	}
	private void AddiStruc2Valdiation() {
		// TODO Auto-generated method stub
			// TODO Auto-generated method stub
		BI_ADDISTRUC2= cf.getselected_chk(BI_ADDI2);
		 addistrucothrval =(BI_ADDI2[BI_ADDI2.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.edaddfencesother)).getText().toString().trim():""; 
			BI_ADDISTRUC2+=addistrucothrval;
			 if(BI_ADDISTRUC2.equals(""))
			 {
				 cf.ShowToast("Please select the option for Fences/Boundary Wall." , 0);
			 }
			 else
			 {
				 if(BI_ADDI2[BI_ADDI2.length-1].isChecked())
				 {
					 if(addistrucothrval.trim().equals("&#40;"))
					 {
						 cf.ShowToast("Please enter the other text for Fences/Boundary Wall.", 0);
						 cf.setFocus(((EditText)findViewById(R.id.edaddfencesother)));
					 }
					 else
					 {
						 BI_InsertGeneralData();	
					 }
				 
				 }
				 else
				 {
					 BI_InsertGeneralData();
				 }
			 }			
	}
	private void BI_InsertGeneralData() {
		// TODO Auto-generated method stub
		
		String insuredval="",neighbourval="",occupancyval="";
		//String buildingsize = ((EditText)findViewById(R.id.edbuildingsize)).getText().toString();
		String occuper = ((EditText)findViewById(R.id.edoccupercent)).getText().toString().trim();
		String vacantper = ((EditText)findViewById(R.id.edvacantpercent)).getText().toString().trim();
		String noofbuild = ((EditText)findViewById(R.id.ednoofbuildings)).getText().toString().trim();
		String noofunits = ((EditText)findViewById(R.id.ednoofunits)).getText().toString().trim();
	
			
		if(!occuper.equals("")){occuval = Integer.parseInt(occuper);}else{occuval=0;}
		if(!vacantper.equals("")){vacantval = Integer.parseInt(vacantper);}else{vacantval=0;}
		if(!noofbuild.equals("")){nobuild = Integer.parseInt(noofbuild);}else{nobuild=0;}
		if(!noofunits.equals("")){nounits = Integer.parseInt(noofunits);}else{nounits=0;}
		
		insuredval =BI_INSURED+"&#94;"+((EditText)findViewById(R.id.insuredis_other)).getText().toString().trim();
		neighbourval =BI_NEIGHBOUR+"&#94;"+BI_NEIGHBOUROPTION+"&#33;"+((EditText)findViewById(R.id.neigh_other)).getText().toString().trim();
		occupancyval =BI_OCCU_TYPE+"&#94;"+((EditText)findViewById(R.id.occupancy_other)).getText().toString().trim();
		BI_GeneralData = insuredval+"&#39;"+neighbourval+"&#39;"+occupancyval+"&#39;"+nobuild+"&#39;"+ednostories.getText().toString()+"&#39;"+nounits+"&#39;"+((EditText)findViewById(R.id.edbuildingsize)).getText().toString();
		BI_YOC = spin_yoc.getSelectedItem().toString()+"&#126;"+((EditText)findViewById(R.id.edyocother)).getText().toString()+"&#94;"+yrbuiltrdgtxt;
		
		if(BI_BUILDOCCU.equals(""))
		{
			BI_OCCU_SURV ="";
		}
		else
		{
			BI_OCCU_SURV =occuval+"&#33;"+vacantval+"&#33;"+BI_BUILDOCCU;
		}
		
		if(permitconfrdgtxt.equals(""))
		{
			BI_PERMIT ="";
		}
		else
		{
			BI_PERMIT = permitconfrdgtxt+"&#94;"+((EditText)findViewById(R.id.etpermitconfirmed)).getText().toString();	
		}
		
		
		if(!incioccurdgtxt.equals(""))
		{
			BI_Inci = incioccurdgtxt+"&#126;"+BI_INCIDENTAL;
		}
		else
		{
			BI_Inci = "";
		}
	
		if(!additionalrdgtxt.equals(""))
		{
			BI_Addistructures=additionalrdgtxt+"&#126;"+BI_ADDISTRUC1+"&#126;"+BI_ADDISTRUC2;
		}
		else
		{
			BI_Addistructures = "";
		}
		
		if(((CheckBox)findViewById(R.id.genchk)).isChecked()){gennotappl="1";}else{gennotappl="0";}
		try{
			Cursor c1 =cf.SelectTablefunction(cf.BI_General, " where fld_srid='"+cf.selectedhomeid+"'");
			if(c1.getCount()==0)
			{
				cf.arr_db.execSQL("INSERT INTO "
						+ cf.BI_General
						+ " (fld_srid,BI_INS_NEI_OCC_NOB_NOS_NOU_BS,BI_YOC,BI_BOCCU_STYPE,BI_PERM_CONFIRMED,BI_BALCONYPRES,BI_INCI_OCCU,BI_ADDI_STRU,BI_SCOPE,BI_GEN_NA)"
						+ "VALUES ('"+cf.selectedhomeid+"','"+cf.encode(BI_GeneralData)+"','"+cf.encode(BI_YOC)+"','"+cf.encode(BI_OCCU_SURV)+"','"+cf.encode(BI_PERMIT)+"','"+balconiespresrdgtxt+"','"+cf.encode(BI_Inci)+"','"+cf.encode(BI_Addistructures)+"','"+cf.encode(BI_SCOPEOFINSP)+"','"+gennotappl+"')");
			}
			else
			{
				System.out.println("UPDATE "
					+ cf.BI_General
					+ " SET BI_INS_NEI_OCC_NOB_NOS_NOU_BS='" + cf.encode(BI_GeneralData) + "',BI_YOC='"+cf.encode(BI_YOC)+"',BI_BOCCU_STYPE='"+cf.encode(BI_OCCU_SURV)+"',BI_PERM_CONFIRMED='"+cf.encode(BI_PERMIT)+"',BI_BALCONYPRES='"+balconiespresrdgtxt+"',BI_INCI_OCCU='"+cf.encode(BI_Inci)+"',BI_ADDI_STRU='"+cf.encode(BI_Addistructures)+"',BI_SCOPE='"+cf.encode(BI_SCOPEOFINSP)+"',BI_GEN_NA='"+gennotappl+"'"
					+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
				cf.arr_db.execSQL("UPDATE "
					+ cf.BI_General
					+ " SET BI_INS_NEI_OCC_NOB_NOS_NOU_BS='" + cf.encode(BI_GeneralData) + "',BI_YOC='"+cf.encode(BI_YOC)+"',BI_BOCCU_STYPE='"+cf.encode(BI_OCCU_SURV)+"',BI_PERM_CONFIRMED='"+cf.encode(BI_PERMIT)+"',BI_BALCONYPRES='"+balconiespresrdgtxt+"',BI_INCI_OCCU='"+cf.encode(BI_Inci)+"',BI_ADDI_STRU='"+cf.encode(BI_Addistructures)+"',BI_SCOPE='"+cf.encode(BI_SCOPEOFINSP)+"',BI_GEN_NA='"+gennotappl+"'"
					+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");			
			
			}
			String yoc ="";
			if(spin_yoc.getSelectedItem().toString().equals("Other"))
			{
				yoc = spin_yoc.getSelectedItem().toString()+"&#126;"+((EditText)findViewById(R.id.edyocother)).getText().toString();
			}
			else
			{
				yoc = spin_yoc.getSelectedItem().toString();
			}
			
			cf.arr_db.execSQL("UPDATE " + cf.policyholder + " SET ARR_PH_NOOFSTORIES='" + ednostories.getText().toString() + "',ARR_YearBuilt='"+yoc+"'  WHERE ARR_PH_SRID ='"+ cf.selectedhomeid + "'");cf.ShowToast("General Data saved successfully",1);		
		//((TextView)findViewById(R.id.savegen)).setVisibility(cf.v1.VISIBLE);
		WallConstruction();
		cf.goclass(461);
		
		}
		catch(Exception e){
			System.out.println("ccc "+e.getMessage());
		}
	}
	private void WallConstruction()
	{
		try
		{
			cf.getPolicyholderInformation(cf.selectedhomeid);
			System.out.println("CF>S"+cf.Stories);
			for(int j=0;j<Integer.parseInt(cf.Stories);j++)
			{
				items.add(cf.spnstories[j]);
			} 
			System.out.println("CCCtry");
			Cursor c1 = cf.arr_db.rawQuery("SELECT * FROM " + cf.WindMit_WallCons + " WHERE fld_srid='"+ cf.selectedhomeid + "'", null);
			System.out.println("CCCCCCC"+c1.getCount());
			// Cursor c1=cf.SelectTablefunction(cf.WindMit_WallCons, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
			 System.out.println("SelectTablefunction"+c1.getCount());
			 rwselev = c1.getCount();
			 elevation = new String[rwselev];
			 percentage = new String[rwselev];
			 storarr = new String[rwselev];
			 arrwallid= new String[rwselev];
			 arrinsptypeid= new String[rwselev];
				 if(c1.getCount()>0)
				 {
					 c1.moveToFirst();				
					 int i=0;
						 do {
							 	arrwallid[i] = c1.getString(c1.getColumnIndex("WCID"));
							 	arrinsptypeid[i]= c1.getString(c1.getColumnIndex("insp_typeid"));
							 	elevation[i]= c1.getString(c1.getColumnIndex("fld_Elevation"));		
							 	percentage[i]= c1.getString(c1.getColumnIndex("fld_Percentage"));	
							 	storarr[i]= c1.getString(c1.getColumnIndex("fld_Stories"));
								String Stories_Split[] = storarr[i].split("~",-1);
								System.out.println("SSSSS"+storarr[i]);
								for(int k=0;k<Stories_Split.length;k++)
							   	{
									System.out.println("Stories_Split"+Stories_Split[k]);
							   		if(!Stories_Split[k].trim().equals(""))
							     	{
							   			System.out.println("trim"+Stories_Split[k]);
							   			
							   			String storiessplit[] = Stories_Split[k].split(",");
							       		for(int j=0;j<storiessplit.length;j++)
							       		{
							       			if(!items.contains(storiessplit[j].trim()))
							       			{
							       				if(m==0)
							   	    			{
							       					wallconstvalue[k] = Stories_Split[k].replace(storiessplit[j],"");m++;
							   	    			}
							   	    			else
							   	    			{
							   	    				wallconstvalue[k] = wallconstvalue[k].replace(storiessplit[j],"");
							   	    			}
							       			}
							       			else
							       			{
							       				if(m==0){
							       					if(wallconstvalue[k]==null) {wallconstvalue[k]="";
							   						wallconstvalue[k] =Stories_Split[k];
							   						}
							   						else{wallconstvalue[k] =Stories_Split[k];}
							   					}
							       			}       				
							       		}m=0;
							       	}
							   		else
							       	{
							   			wallconstvalue[k]="";
							       	}	  
							   		
							   			if(!wallconstvalue[k].equals(""))
							       		{	   				
							       			if(wallconstvalue[k].contains(",,"))
							   	    		{
							       				wallconstvalue[k]=wallconstvalue[k].replace(",,", "");
							   	    		}
							       			if(!wallconstvalue[k].equals(""))
							       			{
							       				String lastChar = wallconstvalue[k].substring(wallconstvalue[k].length()-1);
							       				if (lastChar.contains(",")) {
							       					wallconstvalue[k] = wallconstvalue[k].substring(0,wallconstvalue[k].length()-1);
							       				}
							       			}
							       		}
							   			
							   			System.out.println("wcvalue1="+wcvalue1[i]);
							   			if(wcvalue1[i]==null){
							   				wcvalue1[i]="";
							   				wcvalue1[i] +=wallconstvalue[k]+"~";
							   			}
							   			else
							   			{
							   				wcvalue1[i] +=wallconstvalue[k]+"~";
							   			}	
							   			System.out.println("wcvalueafter="+wcvalue1[i]);
							   		}
								System.out.println("WCVALUE="+wcvalue1[i]);
								if(wcvalue1[i].substring(wcvalue1[i].lastIndexOf("~")).equals("~"))
						   		{
					   				wcvalue1[i]= wcvalue1[i].substring(0,wcvalue1[i].lastIndexOf("~"));
						   		}	  
					   			cf.arr_db.execSQL("UPDATE "
											+ cf.WindMit_WallCons
											+ " SET fld_Stories='" + wcvalue1[i] + "'"
											+ " WHERE fld_srid ='"
											+ cf.selectedhomeid + "' and fld_Elevation='"+elevation[i]+"' and insp_typeid='"+arrinsptypeid[i]+"' and WCID='"+arrwallid[i]+"'");
								
					   			
					   			System.out.println("UPDATE "
										+ cf.WindMit_WallCons
										+ " SET fld_Stories='" + wcvalue1[i] + "'"
										+ " WHERE fld_srid ='"
										+ cf.selectedhomeid + "' and fld_Elevation='"+elevation[i]+"' and insp_typeid='"+arrinsptypeid[i]+"' and WCID='"+arrwallid[i]+"'");
					   			i++;
							} while (c1.moveToNext());						
				 }
				 else
				 {
					
				 }
			 }
			 catch(Exception e)
			 {
				 System.out.println("wc erro "+e.getMessage());
			 }
		
	}

	 public boolean onKeyDown(int keyCode, KeyEvent event) {
	 		// replaces the default 'Back' button action
	 		if (keyCode == KeyEvent.KEYCODE_BACK) {
	 			cf.goclass(12);
				return true;
	 		}
	 	return super.onKeyDown(keyCode, event);
	}
}