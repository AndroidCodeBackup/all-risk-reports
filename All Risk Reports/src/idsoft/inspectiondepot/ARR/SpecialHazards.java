/*
   ************************************************************
   * Project: ALL RISK REPORT
   * Module : SpecialHazards.java
   * Creation History:
      Created By : Gowri on 
   * Modification History:
      Last Modified By : Gowri on 04/04/2013
   ************************************************************  
*/
package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.FireProtection.checklistenetr;
import idsoft.inspectiondepot.ARR.WindMitWallCons.WC_EditClick;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Html;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class SpecialHazards extends Activity{
	CommonFunctions cf;
	int shzchkval=0;
	String fgrd_val="No",flrd_val="No",clrd_val="No",cmrd_val="No",ccord_val="No",wcrd_val="No",spfrd_val="No",dtord_val="No",
		   exprd_val="No",asrd_val="No",incird_val="No",iford_val="No",cgprd_val="No",agfsrd_val="No",ufsrd_val="No",
		   sorrd_val="No",expdrd_val="No",wwrd_val="No",hpsrd_val="No",pbrd_val="No",ccookrd_val="No",retfgrdtxt="";
	RadioGroup fgrdgval,flrdgval,clrdgval,cmrdgval,ccordgval,wcrdgval,spfrdgval,dtordgval,exprdgval,asrdgval,incirdgval,
	           ifordgval,cgprdgval,agfsrdgval,ufsrdgval,sorrdgval,expdrdgval,wwrdgval,hpsrdgval,pbrdgval,ccookrdgval;
	
	@Override
	   public void onCreate(Bundle savedInstanceState) {

	        super.onCreate(savedInstanceState);
	        cf=new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);
				
		 	}
	        setContentView(R.layout.specailhazards);
	        cf.getDeviceDimensions();
	        
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
			hdr_layout.addView(new HdrOnclickListener(this,1,"General Conditions & Hazards","Special Hazards",4,0,cf));
	        LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
	        mainmenu_layout.setMinimumWidth(cf.wd);
	        mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 4, 0,0,cf));
	        LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 46, 1,0,cf));
			TableRow tblrw = (TableRow)findViewById(R.id.row2);
			tblrw.setMinimumHeight(cf.ht);
			cf.CreateARRTable(46);
			declarations();
			Hazards_SetValue();
	}
	private void Hazards_SetValue() {
		// TODO Auto-generated method stub
		try
		{
			chk_values();
			
		   Cursor SHZ_retrive=cf.SelectTablefunction(cf.GCH_SpecHazardstbl, " where fld_srid='"+cf.selectedhomeid+"' AND (fld_shzchk='1' OR (fld_shzchk='0' AND fld_fgases<>''))");
		   if(SHZ_retrive.getCount()>0)
		   {  
			   SHZ_retrive.moveToFirst();
			   ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
			   if(SHZ_retrive.getInt(SHZ_retrive.getColumnIndex("fld_shzchk"))==1)
			   {
				   ((CheckBox)findViewById(R.id.sh_na)).setChecked(true);
				   ((LinearLayout)findViewById(R.id.shz_table)).setVisibility(cf.v1.GONE);
			   }
			   else
			   {
				   ((CheckBox)findViewById(R.id.sh_na)).setChecked(false);
				   ((LinearLayout)findViewById(R.id.shz_table)).setVisibility(cf.v1.VISIBLE);
				   
				   fgrd_val=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_fgases"));
				    ((RadioButton) fgrdgval.findViewWithTag(fgrd_val)).setChecked(true);
				    
				    flrd_val=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_fliquids"));
				    ((RadioButton) flrdgval.findViewWithTag(flrd_val)).setChecked(true);
				    
				    clrd_val=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_cliquids"));
				    ((RadioButton) clrdgval.findViewWithTag(clrd_val)).setChecked(true);
				    
				    cmrd_val=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_cmetals"));
				    ((RadioButton) cmrdgval.findViewWithTag(cmrd_val)).setChecked(true);
				    
				    ccord_val=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_cchim"));
				    ((RadioButton) ccordgval.findViewWithTag(ccord_val)).setChecked(true);
				    
				    
				    wcrd_val=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_welding"));
				    ((RadioButton) wcrdgval.findViewWithTag(wcrd_val)).setChecked(true);
				    
				    spfrd_val=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_spaint"));
				    ((RadioButton) spfrdgval.findViewWithTag(spfrd_val)).setChecked(true);
				    
				    dtord_val=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_dip"));
				    ((RadioButton) dtordgval.findViewWithTag(dtord_val)).setChecked(true);
				    
				    exprd_val=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_exp"));
				    ((RadioButton) exprdgval.findViewWithTag(exprd_val)).setChecked(true);
				    
				    asrd_val=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_astorage"));
				    ((RadioButton) asrdgval.findViewWithTag(asrd_val)).setChecked(true);
				    
				    incird_val=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_incin"));
				    ((RadioButton) incirdgval.findViewWithTag(incird_val)).setChecked(true);
				    
				    iford_val=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_ifovens"));
				    ((RadioButton) ifordgval.findViewWithTag(iford_val)).setChecked(true);
				    
				    cgprd_val=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_cgp"));
				    ((RadioButton) cgprdgval.findViewWithTag(cgprd_val)).setChecked(true);
				    
				    agfsrd_val=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_agfstorage"));
				    ((RadioButton) agfsrdgval.findViewWithTag(agfsrd_val)).setChecked(true);
				    
				    ufsrd_val=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_ufstorage"));
				    ((RadioButton) ufsrdgval.findViewWithTag(ufsrd_val)).setChecked(true);
				    
				    sorrd_val=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_soilyrages"));
				    ((RadioButton) sorrdgval.findViewWithTag(sorrd_val)).setChecked(true);
				    
				    expdrd_val=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_expdust"));
				    ((RadioButton) expdrdgval.findViewWithTag(expdrd_val)).setChecked(true);
				    
				    wwrd_val=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_wworking"));
				    ((RadioButton) wwrdgval.findViewWithTag(wwrd_val)).setChecked(true);
				    
				    hpsrd_val=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_hpiled"));
				    ((RadioButton) hpsrdgval.findViewWithTag(hpsrd_val)).setChecked(true);
				    
				    pbrd_val=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_pbaler"));
				    ((RadioButton) pbrdgval.findViewWithTag(pbrd_val)).setChecked(true);
				    
				    ccookrd_val=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_ccook"));
				    ((RadioButton) ccookrdgval.findViewWithTag(ccookrd_val)).setChecked(true);
				    System.out.println(" yjr balues "+sorrd_val+"/"+expdrd_val+"/"+wwrd_val+"/"+hpsrd_val+"/"+pbrd_val+"/"+ccookrd_val);
				    
			   }
			 
		   }
		   
		   else
		   {
			    cf.getinspectioname(cf.selectedhomeid);
			    if(cf.selinspname.equals("4"))
				{
					((CheckBox)findViewById(R.id.sh_na)).setChecked(false);
					((LinearLayout)findViewById(R.id.shz_table)).setVisibility(cf.v1.VISIBLE);
				}
				else
				{
					((CheckBox)findViewById(R.id.sh_na)).setChecked(true);
					((LinearLayout)findViewById(R.id.shz_table)).setVisibility(cf.v1.GONE);
				}
			   
		   }
		}
	    catch (Exception E)
		{
			String strerrorlog="Retrieving Specail Hazards - GCH";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
	private void declarations() {
		// TODO Auto-generated method stub
		fgrdgval = (RadioGroup)findViewById(R.id.fg_rdg);
		fgrdgval.setOnCheckedChangeListener(new checklistenetr(1));
		flrdgval = (RadioGroup)findViewById(R.id.fl_rdg);
		flrdgval.setOnCheckedChangeListener(new checklistenetr(2));
		clrdgval = (RadioGroup)findViewById(R.id.cl_rdg);
		clrdgval.setOnCheckedChangeListener(new checklistenetr(3));
		cmrdgval = (RadioGroup)findViewById(R.id.cm_rdg);
		cmrdgval.setOnCheckedChangeListener(new checklistenetr(4));
		ccordgval = (RadioGroup)findViewById(R.id.cco_rdg);
		ccordgval.setOnCheckedChangeListener(new checklistenetr(5));
		wcrdgval = (RadioGroup)findViewById(R.id.wc_rdg);
		wcrdgval.setOnCheckedChangeListener(new checklistenetr(6));
		spfrdgval = (RadioGroup)findViewById(R.id.spf_rdg);
		spfrdgval.setOnCheckedChangeListener(new checklistenetr(7));
		dtordgval = (RadioGroup)findViewById(R.id.dto_rdg);
		dtordgval.setOnCheckedChangeListener(new checklistenetr(8));
		exprdgval = (RadioGroup)findViewById(R.id.exp_rdg);
		exprdgval.setOnCheckedChangeListener(new checklistenetr(9));
		asrdgval = (RadioGroup)findViewById(R.id.as_rdg);
		asrdgval.setOnCheckedChangeListener(new checklistenetr(10));
		incirdgval = (RadioGroup)findViewById(R.id.inci_rdg);
		incirdgval.setOnCheckedChangeListener(new checklistenetr(11));
		ifordgval = (RadioGroup)findViewById(R.id.ifo_rdg);
		ifordgval.setOnCheckedChangeListener(new checklistenetr(12));
		cgprdgval = (RadioGroup)findViewById(R.id.cgp_rdg);
		cgprdgval.setOnCheckedChangeListener(new checklistenetr(13));
		agfsrdgval = (RadioGroup)findViewById(R.id.agfs_rdg);
		agfsrdgval.setOnCheckedChangeListener(new checklistenetr(14));
		ufsrdgval = (RadioGroup)findViewById(R.id.ufs_rdg);
		ufsrdgval.setOnCheckedChangeListener(new checklistenetr(15));
		sorrdgval = (RadioGroup)findViewById(R.id.sor_rdg);
		sorrdgval.setOnCheckedChangeListener(new checklistenetr(16));
		expdrdgval = (RadioGroup)findViewById(R.id.expd_rdg);
		expdrdgval.setOnCheckedChangeListener(new checklistenetr(17));
		wwrdgval = (RadioGroup)findViewById(R.id.ww_rdg);
		wwrdgval.setOnCheckedChangeListener(new checklistenetr(18));
		hpsrdgval = (RadioGroup)findViewById(R.id.hps_rdg);
		hpsrdgval.setOnCheckedChangeListener(new checklistenetr(19));
		pbrdgval = (RadioGroup)findViewById(R.id.pb_rdg);
		pbrdgval.setOnCheckedChangeListener(new checklistenetr(20));
		ccookrdgval = (RadioGroup)findViewById(R.id.ccook_rdg);
		ccookrdgval.setOnCheckedChangeListener(new checklistenetr(21));
		
		set_defaultchk();	
	}
	private void set_defaultchk() {
		// TODO Auto-generated method stub
		((RadioButton) fgrdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) flrdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) clrdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) cmrdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) ccordgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) wcrdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) spfrdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) dtordgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) exprdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) asrdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) incirdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) ifordgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) cgprdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) agfsrdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) ufsrdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) sorrdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) expdrdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) wwrdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) hpsrdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) pbrdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) ccookrdgval.findViewWithTag("No")).setChecked(true);
		fgrd_val="No";flrd_val="No";clrd_val="No";cmrd_val="No";ccord_val="No";wcrd_val="No";spfrd_val="No";dtord_val="No";
				   exprd_val="No";asrd_val="No";incird_val="No";iford_val="No";cgprd_val="No";agfsrd_val="No";ufsrd_val="No";
				   sorrd_val="No";expdrd_val="No";wwrd_val="No";hpsrd_val="No";pbrd_val="No";ccookrd_val="No";
	}
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.sh_na:/** SPECIAL HAZARDS NOT APPLICABLE CHECKBOX **/
			if(((CheckBox)findViewById(R.id.sh_na)).isChecked())
			{
				//chk_values();System.out.println("retfgrdtxt="+retfgrdtxt);
    			if(!retfgrdtxt.equals("")){
 				    showunchkalert(1);
 			    }
    			else
    			{
    				clearhz();
    				set_defaultchk();
    				((LinearLayout)findViewById(R.id.shz_table)).setVisibility(v.GONE);
 			    	((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.INVISIBLE);
 			    }
				
			}
			else
			{
				 set_defaultchk();
				((LinearLayout)findViewById(R.id.shz_table)).setVisibility(v.VISIBLE);
				((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.INVISIBLE);
				((CheckBox)findViewById(R.id.sh_na)).setChecked(false);
			}
			
			break;
		case R.id.helpid:
			cf.ShowToast("Use for Commercial GCH report only.",0);
			break;
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.save:
			if(((CheckBox)findViewById(R.id.sh_na)).isChecked())
			{
				shzchkval=1;SpecialHz_Insert();
				((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
				cf.ShowToast("Special Hazards saved successfully.", 0);
				cf.goclass(45);
			}
			else
			{
				shzchkval=0;
				SpecialHz_Insert();
				((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
				cf.ShowToast("Special Hazards saved successfully.", 0);
				cf.goclass(45);
			}
			
			
			break;

		}
	}
	private void SpecialHz_Insert() {
		// TODO Auto-generated method stub
		Cursor SHZ_save=null;
		try
		{
			SHZ_save=cf.SelectTablefunction(cf.GCH_SpecHazardstbl, " where fld_srid='"+cf.selectedhomeid+"'");
			if(SHZ_save.getCount()>0)
			{
				try
				{
					cf.arr_db.execSQL("UPDATE "+cf.GCH_SpecHazardstbl+ " set fld_shzchk='"+shzchkval+"',fld_fgases='"+fgrd_val+"',fld_fliquids='"+flrd_val+"',"+
				                                "fld_cliquids='"+clrd_val+"',fld_cmetals='"+cmrd_val+"',fld_cchim='"+ccord_val+"',fld_welding='"+wcrd_val+"',"+
							                    "fld_spaint='"+spfrd_val+"',fld_dip='"+dtord_val+"',fld_exp='"+exprd_val+"',fld_astorage='"+asrd_val+"',"+
				                                "fld_incin='"+incird_val+"',fld_ifovens='"+iford_val+"',fld_cgp='"+cgprd_val+"',fld_agfstorage='"+agfsrd_val+"',"+
							                    "fld_ufstorage='"+ufsrd_val+"',fld_soilyrages='"+sorrd_val+"',fld_expdust='"+expdrd_val+"',fld_wworking='"+wwrd_val+"',"+
						                        "fld_hpiled='"+hpsrd_val+"',fld_pbaler='"+pbrd_val+"',fld_ccook='"+ccookrd_val+"' where fld_srid='"+cf.selectedhomeid+"'");
					System.out.println("UPDATE "+cf.GCH_SpecHazardstbl+ " set fld_shzchk='"+shzchkval+"',fld_fgases='"+fgrd_val+"',fld_fliquids='"+flrd_val+"',"+
                            "fld_cliquids='"+clrd_val+"',fld_cmetals='"+cmrd_val+"',fld_cchim='"+ccord_val+"',fld_welding='"+wcrd_val+"',"+
		                    "fld_spaint='"+spfrd_val+"',fld_dip='"+dtord_val+"',fld_exp='"+exprd_val+"',fld_astorage='"+asrd_val+"',"+
                            "fld_incin='"+incird_val+"',fld_ifovens='"+iford_val+"',fld_cgp='"+cgprd_val+"',fld_agfstorage='"+agfsrd_val+"',"+
		                    "fld_ufstorage='"+ufsrd_val+"',fld_soilyrages='"+sorrd_val+"',fld_expdust='"+expdrd_val+"',fld_wworking='"+wwrd_val+"',"+
	                        "fld_hpiled='"+hpsrd_val+"',fld_pbaler='"+pbrd_val+"',fld_ccook='"+ccookrd_val+"' where fld_srid='"+cf.selectedhomeid+"'");
					/*((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
					cf.ShowToast("Special Hazards saved successfully.", 0);
					cf.goclass(47);*/
				}
				catch (Exception E)
				{
					String strerrorlog="Updating the Fire Protection/Survey - GCH";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
			}
			else
			{
				try
				{
					cf.arr_db.execSQL("INSERT INTO "
						+ cf.GCH_SpecHazardstbl
						+ " (fld_srid,fld_shzchk,fld_fgases,fld_fliquids,fld_cliquids,fld_cmetals,fld_cchim,fld_welding,fld_spaint,fld_dip,fld_exp,fld_astorage,"+
				                                "fld_incin,fld_ifovens,fld_cgp,fld_agfstorage,fld_ufstorage,fld_soilyrages,fld_expdust,fld_wworking,"+
						                        "fld_hpiled,fld_pbaler,fld_ccook)"
						+ "VALUES ('"+cf.selectedhomeid+"','"+shzchkval+"','"+fgrd_val+"','"+flrd_val+"','"+clrd_val+"','"+cmrd_val+"','"+ccord_val+"','"+wcrd_val+"',"+
	                    "'"+spfrd_val+"','"+dtord_val+"','"+exprd_val+"','"+asrd_val+"','"+incird_val+"','"+iford_val+"','"+cgprd_val+"','"+agfsrd_val+"',"+
	                    "'"+ufsrd_val+"','"+sorrd_val+"','"+expdrd_val+"','"+wwrd_val+"','"+hpsrd_val+"','"+pbrd_val+"','"+ccookrd_val+"')");

					/*((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
					cf.ShowToast("Special Hazards saved successfully.", 0);
					cf.goclass(47);*/
				}
				catch (Exception E)
				{
					
					String strerrorlog="Inserting the Fire Protection/Survey - GCH";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
					
				}
				
			}
		}
		catch (Exception E)
		{
				String strerrorlog="Checking the rows inserted in the GCH Special Hazards table.";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			cf.goclass(44);
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}
	class checklistenetr implements OnCheckedChangeListener
	{
    	int i;
		public checklistenetr(int i) {
			// TODO Auto-generated constructor stub
		   this.i=i;
		}

		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			   RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
		        boolean isChecked = checkedRadioButton.isChecked();
		        if (isChecked)
		        {
		          switch (i) {
					case 1:
						fgrd_val= checkedRadioButton.getText().toString().trim();
						break;
					case 2:
						flrd_val= checkedRadioButton.getText().toString().trim();
						break;
					case 3:
						clrd_val= checkedRadioButton.getText().toString().trim();
						break;
					case 4:
						cmrd_val= checkedRadioButton.getText().toString().trim();
						break;
					case 5:
						ccord_val= checkedRadioButton.getText().toString().trim();
						break;
					case 6:
						wcrd_val= checkedRadioButton.getText().toString().trim();
						break;
					case 7:
						spfrd_val= checkedRadioButton.getText().toString().trim();
						break;
					case 8:
						dtord_val= checkedRadioButton.getText().toString().trim();
						break;
					case 9:
						exprd_val= checkedRadioButton.getText().toString().trim();
						break;
					case 10:
						asrd_val= checkedRadioButton.getText().toString().trim();
						break;
					case 11:
						incird_val= checkedRadioButton.getText().toString().trim();
						break;
					case 12:
						iford_val= checkedRadioButton.getText().toString().trim();
						break;
					case 13:
						cgprd_val= checkedRadioButton.getText().toString().trim();
						break;
					case 14:
						agfsrd_val= checkedRadioButton.getText().toString().trim();
						break;
					case 15:
						ufsrd_val= checkedRadioButton.getText().toString().trim();
						break;
					case 16:
						sorrd_val= checkedRadioButton.getText().toString().trim();
						break;
					case 17:
						expdrd_val= checkedRadioButton.getText().toString().trim();
						break;
					case 18:
						wwrd_val= checkedRadioButton.getText().toString().trim();
						break;
					case 19:
						hpsrd_val= checkedRadioButton.getText().toString().trim();
						break;
					case 20:
						pbrd_val= checkedRadioButton.getText().toString().trim();
						break;
					case 21:
						ccookrd_val= checkedRadioButton.getText().toString().trim();
						break;
		          }
		       }
		}
	}
	private void chk_values() {
		// TODO Auto-generated method stub
		try
		{
		   Cursor sh_retrive=cf.SelectTablefunction(cf.GCH_SpecHazardstbl, " where fld_srid='"+cf.selectedhomeid+"'");
		   int rws = sh_retrive.getCount();
			if(rws>0){
				sh_retrive.moveToFirst();
				retfgrdtxt = sh_retrive.getString(sh_retrive.getColumnIndex("fld_fgases"));
				System.out.println("the value is= "+retfgrdtxt);
			}
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}
	public void showunchkalert(final int i) {
		// TODO Auto-generated method stub
		AlertDialog.Builder bl = new Builder(SpecialHazards.this);
		bl.setTitle("Confirmation");
		bl.setMessage(Html.fromHtml("Do you want to clear the Special Hazards data?"));
		bl.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog,
											int id) {
										 switch(i){
										 case 1:
											 ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.GONE);
											 set_defaultchk();
							    			 clearhz();//SpecialHz_Insert();
							    			// cf.arr_db.execSQL("Delete from "+cf.GCH_SpecHazardstbl+" Where fld_srid='"+cf.selectedhomeid+"'");
											 
							    			
							    			 ((LinearLayout)findViewById(R.id.shz_table)).setVisibility(cf.v1.GONE);
							    			 break;
		                                 
										 }
										 	 
										 
									}
		});
		bl.setNegativeButton("No",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog,
							int id) {
						switch(i){
						 case 1:
							 ((CheckBox)findViewById(R.id.sh_na)).setChecked(false);
							 break;
					
						 }
					}
        });
		AlertDialog al=bl.create();
		al.setIcon(R.drawable.alertmsg);
		al.setCancelable(false);
		al.show();
		
		
	}
	protected void clearhz() {
		// TODO Auto-generated method stub
		System.out.println("completed su");
		 shzchkval=0;retfgrdtxt="";
		 fgrd_val="";flrd_val="";clrd_val="";cmrd_val="";ccord_val="";
		 wcrd_val="";spfrd_val="";dtord_val="";exprd_val="";asrd_val="";retfgrdtxt="";
		 incird_val="";iford_val="";cgprd_val="";agfsrd_val="";ufsrd_val="";
		 sorrd_val="";expdrd_val="";wwrd_val="";hpsrd_val="";pbrd_val="";ccookrd_val="";
	}
}