package idsoft.inspectiondepot.ARR;

public interface IDisposable {

	void dispose();
}
