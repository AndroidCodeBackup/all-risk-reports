package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import android.app.Activity;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WeblinkOpen extends Activity {
	private static final String TAG = null;
	WebView webview1;
	int value, Count;
	Intent intimg;
	String InspectionType, status, homeId, identity, weburl, Page;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle bunhomeId = getIntent().getExtras();
		if (bunhomeId != null) {
			weburl = bunhomeId.getString("weburl");
			homeId = bunhomeId.getString("homeid");
			identity = bunhomeId.getString("iden");
			InspectionType = bunhomeId.getString("InspectionType");
			status = bunhomeId.getString("status");
			value = bunhomeId.getInt("keyName");
			Count = bunhomeId.getInt("Count");
			Page = bunhomeId.getString("page");

		}

		setContentView(R.layout.weblink);

		webview1 = (WebView) findViewById(R.id.webview);
		webview1.getSettings().setJavaScriptEnabled(true);
		webview1.loadUrl(weburl);

	}
}
