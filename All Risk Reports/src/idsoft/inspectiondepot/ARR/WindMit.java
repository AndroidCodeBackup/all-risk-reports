/*
************************************************************
* Project: ALL RISK REPORT
* Module : WindMit.java
* Creation History:
Created By : Nalini on 10/24/2012
* Modification History:
Last Modified By : Gowri on 10/25/2012
************************************************************ 
*/
package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TableRow;

public class WindMit extends Activity {
	CommonFunctions cf;

	@Override
	   public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        cf=new CommonFunctions(this);	  
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);				
		 	}        
	        setContentView(R.layout.windmit);
	        cf.getDeviceDimensions();
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
			hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","",3,2,cf));
         	LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
         	main_layout.setMinimumWidth(cf.wd);
         	main_layout.addView(new MyOnclickListener(getApplicationContext(), 3, 0,0,cf));
	        
	       cf.type_submenulayout =(LinearLayout) findViewById(R.id.typesubmenu);
	       cf.type_submenulayout.addView(new MyOnclickListener(this, 300, 1,0, cf));
	       cf.uncheckcommercial(((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((RadioButton)findViewById(R.id.b1802_chk))); 
	   
	       cf.getPolicyholderInformation(cf.selectedhomeid);System.out.println(cf.CommercialFlag);
	       if(!cf.CommercialFlag.equals("0"))
	       {
	    	  System.out.println("ctstss"+cf.Stories);
	    	   if(cf.Stories.equals("1") || cf.Stories.equals("2")  || cf.Stories.equals("3"))
			   {
	    		   if(cf.comm1_buildno.equals(""))
				   {
	    			   cf.getcommercialbno(1,32,"Commercial Type I");
				   }   
			   }

	    	   else if(cf.Stories.equals("4") || cf.Stories.equals("5")  || cf.Stories.equals("6"))
			   {
	    		   if(cf.comm2_buildno.equals(""))
				   {
	    			   cf.getcommercialbno(2,33,"Commercial Type II");
				   }   
			   }

	    	   else
			   {
	    		   if(cf.comm3_buildno.equals(""))
				   {
	    			   cf.getcommercialbno(3,34,"Commercial Type III");
				   }   
			   }
	       }
  		 
  		 
  		 
	}
	public void clicker(View v) {
		switch(v.getId())
		{
		case R.id.hme:
			cf.gohome();
			break;		
		}
	}	
	 public boolean onKeyDown(int keyCode, KeyEvent event) {
	 		// replaces the default 'Back' button action
	 		if (keyCode == KeyEvent.KEYCODE_BACK) {
	 			cf.isaccess= cf.chk_InspTypeQuery(cf.selectedhomeid,"Roof Survey");
				if(cf.isaccess==true){
				cf.Roof=cf.Roof_insp[1];
	 			cf.goback(2);}
				else{
					cf.goback(0);}
	 			return true;
	 		}
	 		
	 		return super.onKeyDown(keyCode, event);
	 	}
}