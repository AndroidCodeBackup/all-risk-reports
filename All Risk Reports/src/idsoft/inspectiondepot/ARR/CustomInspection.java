package idsoft.inspectiondepot.ARR;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Html;
import android.text.InputFilter;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.TableRow.LayoutParams;


public class CustomInspection extends Activity 
{
	CommonFunctions cf;
	Cursor c2;
	int  currnet_rotated=0,fbchk=0;
	String maininspectionid="";
	ImageView upd_img;
	TableLayout tbl_custom;
	EditText edittitle,ed;
	ImageView img;
	Bitmap rotated_b;
	Button del,edit,clear;
	TextView tit_tv;
	String maininspname="",addiinspname="";
	String s="",saveadditional="";
	int arraylistfilepahlength=0,inspid=0;
	int strnotreq=0,fbnotreq=0;
	String filePath="",updated_path="",pathforzoom="pathforzoom";
	View v1;
	String[] arrfilepath;
	int[] arrimagename;
	Spinner insp_sp;
	private String[] selected_insp;
	CheckBox chknotreq,chkfdback;
	ArrayList<String> arraylistfilepah = new ArrayList<String>();
	protected void onCreate(Bundle savedInstanceState) 
	{

		super.onCreate(savedInstanceState);
		cf = new CommonFunctions(this);
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			cf.getExtras(extras);				
	 	}
		setContentView(R.layout.custom);
		LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
		hdr_layout.addView(new HdrOnclickListener(this,1,"Custom Inspection","",1,0,cf));
		
		  LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
		  mainmenu_layout.setMinimumWidth(cf.wd);
	      mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 69, 0,0,cf));
	      tbl_custom =   (TableLayout) findViewById(R.id.custom_DY_tbl);
	      cf.CreateARRTable(90);
	      chknotreq = (CheckBox) findViewById(R.id.chknotrequired);
	      chkfdback = (CheckBox) findViewById(R.id.chkfeedback);
	      
	      cf.CreateARRTable(3);
	      cf.CreateARRTable(90);
	      
	      show_layouts();
	      show_maininspreport();
	      show_addiservreport();
	      show_values();  //checkforimage_dup();
	}
	private void checkforimage_dup() {
		// TODO Auto-generated method stub
		
		Cursor c2 = cf.arr_db.rawQuery("select * from " + cf.Custom_report +" Where SRID='"+cf.selectedhomeid+"'", null);
    	if(c2.getCount()>0)
		{
			c2.moveToFirst();
			arrfilepath = new String[c2.getCount()];
			for(int i=0;i<c2.getCount();i++,c2.moveToNext())
			{				
				arrfilepath[i]=cf.decode(c2.getString(c2.getColumnIndex("ReportImage")));
				System.out.println("arrfile="+arrfilepath[i]);
				if(!arraylistfilepah.contains(arrfilepath[i]))
				{
					arraylistfilepah.add(arrfilepath[i]);
				}
				
			}
		}
	}
	private void show_maininspreport() {
		// TODO Auto-generated method stub

	      Cursor c2 = cf.arr_db.rawQuery("select * from " + cf.Custom_report +" Where SRID='"+cf.selectedhomeid+"' and IsMainInspection='1'", null);
	      if(c2.getCount()>0)
		  {
				c2.moveToFirst();
				String imagename1=cf.decode(c2.getString(c2.getColumnIndex("ReportImage")));
				if(imagename1.equals(""))
				{
					((EditText) findViewById(R.id.edtmaininsp)).setVisibility(v1.VISIBLE);
					((Button) findViewById(R.id.browsemaininsp)).setVisibility(v1.VISIBLE);
					((ImageView) findViewById(R.id.maininspdelete)).setVisibility(v1.GONE);
					((ImageView) findViewById(R.id.maininspimgdisp)).setVisibility(v1.GONE);
				}
				else
				{
					if(imagename1.endsWith(".pdf"))
					{
						((ImageView) findViewById(R.id.maininspimgdisp)).setImageDrawable(getResources().getDrawable(R.drawable.pdficon));
					}
					else if(imagename1.endsWith(".doc") || imagename1.endsWith(".docx"))
					{
						((ImageView) findViewById(R.id.maininspimgdisp)).setImageDrawable(getResources().getDrawable(R.drawable.wd));
					}
					else if(imagename1.endsWith(".xls") || imagename1.endsWith(".xlsx"))
					{
						((ImageView) findViewById(R.id.maininspimgdisp)).setImageDrawable(getResources().getDrawable(R.drawable.xl));
					}
					else
					{
						Bitmap bm = decodeFile(imagename1);
						((ImageView) findViewById(R.id.maininspimgdisp)).setImageBitmap(bm);	
					}			
					
					((ImageView) findViewById(R.id.maininspimgdisp)).setTag(imagename1);
					((EditText) findViewById(R.id.edtmaininsp)).setVisibility(v1.GONE);
					((Button) findViewById(R.id.browsemaininsp)).setVisibility(v1.GONE);
					((ImageView) findViewById(R.id.maininspdelete)).setVisibility(v1.VISIBLE);
					((ImageView) findViewById(R.id.maininspimgdisp)).setVisibility(v1.VISIBLE);
				}
		  }
	}
	private void show_layouts() {
		// TODO Auto-generated method stub
		 Cursor c1 = cf.arr_db.rawQuery("select * from " + cf.policyholder+" Where ARR_PH_SRID='"+cf.selectedhomeid+"'", null);
	      if(c1.getCount()>0)
		  {
				c1.moveToFirst();
				maininspectionid =c1.getString(c1.getColumnIndex("ARR_PH_InspectionTypeId"));
				String additionalinspids =cf.decode(c1.getString(c1.getColumnIndex("fld_inspectionids")));
				
				if(maininspectionid.matches("9") || maininspectionid.matches("11") || maininspectionid.matches("13") || 
						 maininspectionid.matches("18") || maininspectionid.matches("28") || maininspectionid.matches("750") ||
						 maininspectionid.matches("751"))
				 {
					 ((RelativeLayout) findViewById(R.id.maininspheader)).setVisibility(v1.GONE);
					 ((LinearLayout) findViewById(R.id.maininspectionlin)).setVisibility(v1.GONE);
				 }
				 else
				 {
					  setinspname(((TextView) findViewById(R.id.maininsptitle)),maininspectionid);
					 ((TextView) findViewById(R.id.maininsptitle)).setTag(maininspectionid);
					 ((RelativeLayout) findViewById(R.id.maininspheader)).setVisibility(v1.VISIBLE);
					 ((LinearLayout) findViewById(R.id.maininspectionlin)).setVisibility(v1.VISIBLE);
				 }
				 
				 if(additionalinspids.contains(","))
				 {
					 String id[] = additionalinspids.split(",");
					 for(int k=1;k<id.length;k++)
					 {
						 if(id[k].matches("9") || id[k].matches("11") || id[k].matches("13") || id[k].matches("18") || id[k].matches("28")
								 || id[k].matches("750") || id[k].matches("751"))
						 {
								 s += "true"+"~";
						 }
						 else
						 {
							s += "false"+"~";
						 }
					 }			
				 }
				 if(s.contains("false"))
				 {
					 ((RelativeLayout) findViewById(R.id.addiinspheader)).setVisibility(v1.VISIBLE);
					 ((TableLayout) findViewById(R.id.additionalservlin)).setVisibility(v1.VISIBLE);
				 }
		  }
	}
	private void setinspname(TextView tv,String addiinspname)
	{
		 cf.CreateARRTable(14);
		 Cursor c2 = cf.arr_db.rawQuery("select * from " + cf.allinspnamelist+" Where ARR_Insp_ID='"+addiinspname+"'", null);
	      if(c2.getCount()>0)
		  {
				c2.moveToFirst();
				tv.setText(cf.decode(c2.getString(c2.getColumnIndex("ARR_Insp_Name"))));
		  }
	}
	private void show_addiservreport()
	{		
		Cursor s=cf.SelectTablefunction(cf.Select_insp, " WHERE SI_srid='"+cf.selectedhomeid+"'");
    	s.moveToFirst();
    	if(s.getCount()>0)
    	{
    		String tmp=cf.decode(s.getString(s.getColumnIndex("SI_InspectionNames"))).trim();	
    		System.out.println("tmp="+tmp);
    		if(tmp.contains(","))
    		{
    			tmp = tmp.replace(",","~");
    			String arr[] = tmp.split("~");
    			for(int i=0;i<arr.length;i++)
    			{
    				if(arr[i].contentEquals("9") || arr[i].contentEquals("11") || arr[i].contentEquals("13") || arr[i].contentEquals("18")
    						|| arr[i].contentEquals("28") || arr[i].contentEquals("750") || arr[i].contentEquals("751"))
    				{
    					 tmp = tmp.replaceAll("\\b"+arr[i]+"\\b", "");
    					 if(tmp.contains("~~"))
    					 {
    						tmp = tmp.replaceAll("~~","~");	
    					 }
    			
    				}
    			}
    			if(tmp.startsWith("~"))
				{
					tmp = tmp.substring(1, tmp.length());
				}
    			if(tmp.endsWith("~"))
				{
					tmp = tmp.substring(0, tmp.length() - 1);
				}
    			tmp = tmp.replace("~",",");
    			System.out.println("tmpcustom="+tmp);
    		}
    		
    		if(tmp.length()>0)
    		{		    			
    			try
    			{
    				System.out.println("maininspectionid="+maininspectionid);
    				
        			String mainidsplit[] = tmp.split(",");
        			for(int i=0;i<mainidsplit.length;i++)
        			{
        				if(mainidsplit[i].contentEquals(maininspectionid))
        				{
        					 tmp = tmp.replaceAll("\\b"+mainidsplit[i]+"\\b", "");
        					 if(tmp.contains(",,"))
        					 {
        						tmp = tmp.replaceAll(",,",",");	
        					 }
        			
        				}
        			}
        			if(tmp.startsWith(","))
    				{
    					tmp = tmp.substring(1, tmp.length());
    				}
        			if(tmp.endsWith(","))
    				{
    					tmp = tmp.substring(0, tmp.length() - 1);
    				}
        			
        			System.out.println("what is tmp ="+tmp);
    				
	    			Cursor c =cf.SelectTablefunction(cf.allinspnamelist, " Where  ARR_Insp_ID in ("+tmp+")  order by ARR_Custom_ID");
	    			System.out.println("tmp count="+c.getCount());
	    			if(c.getCount()>0)
	    			{	    				
	    				c.moveToFirst();
	    				
	    				selected_insp=new String[c.getCount()+1];System.out.println("length="+selected_insp.length);
	    				selected_insp[0]="--Select--";System.out.println("selected_insp[0]="+selected_insp[0]);
	    				for(int i=1;i<c.getCount()+1;i++,c.moveToNext())
	    				{	    					
	    					System.out.println("i="+i);
	    					selected_insp[i]=cf.decode(c.getString(c.getColumnIndex("ARR_Insp_Name"))).trim();
	    					
	    					System.out.println("v for ="+selected_insp[i]);
	    				}	    				
	    			}
    			}
    			catch (Exception e) {
					// TODO: handle exception
    				System.out.println("second issues="+e.getMessage());
				}
    			
    			try
    		    {
    			 ArrayAdapter adapter_insp = new ArrayAdapter(CustomInspection.this,android.R.layout.simple_spinner_item, selected_insp);
    			 adapter_insp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    			 insp_sp=(Spinner) findViewById(R.id.addinsptitle);
    				insp_sp.setAdapter(adapter_insp);
    				
    				insp_sp.setOnItemSelectedListener(new OnItemSelectedListener() {

    					@Override
    					public void onItemSelected(AdapterView<?> arg0, View arg1,
    							int arg2, long arg3) {
    						// TODO Auto-generated method stub
    						if(insp_sp.getSelectedItem().toString().equals("--Select--"))
    						{
    							((LinearLayout) findViewById(R.id.addilayoutlin)).setVisibility(v1.GONE);	
    							((Button) findViewById(R.id.addiservsave)).setVisibility(v1.GONE);	
    						}
    						else
    						{
    							((LinearLayout) findViewById(R.id.addilayoutlin)).setVisibility(v1.VISIBLE);
    							((Button) findViewById(R.id.addiservsave)).setVisibility(v1.VISIBLE);	
    							((Button) findViewById(R.id.browsetxt)).setEnabled(true);
    							getinspid(insp_sp.getSelectedItem().toString());
    							Cursor c1 = cf.arr_db.rawQuery("select * from " + cf.Custom_report +" Where SRID='"+cf.selectedhomeid+"' and InspectionID='"+inspid+"'", null);
    							 if(c1.getCount()>0)
    							  {
    									c1.moveToFirst();
    									String imagename=cf.decode(c1.getString(c1.getColumnIndex("ReportImage")));
    									if(imagename.equals(""))
    									{
    										((EditText) findViewById(R.id.editreport)).setVisibility(v1.VISIBLE);
    										((Button) findViewById(R.id.browsetxt)).setVisibility(v1.VISIBLE);
    										((ImageView) findViewById(R.id.delete)).setVisibility(v1.GONE);
    										((ImageView) findViewById(R.id.imgdisp)).setVisibility(v1.GONE);
    									}
    									else
    									{
    										if(imagename.endsWith(".pdf"))
    										{
    											((ImageView) findViewById(R.id.imgdisp)).setImageDrawable(getResources().getDrawable(R.drawable.pdficon));
    										}
    										else if(imagename.endsWith(".doc") || imagename.endsWith(".docx"))
    										{
    											((ImageView) findViewById(R.id.imgdisp)).setImageDrawable(getResources().getDrawable(R.drawable.wd));
    										}
    										else if(imagename.endsWith(".xls") || imagename.endsWith(".xlsx"))
    										{
    											((ImageView) findViewById(R.id.imgdisp)).setImageDrawable(getResources().getDrawable(R.drawable.xl));
    										}
    										else
    										{
    											Bitmap bm = decodeFile(imagename);
    											((ImageView) findViewById(R.id.imgdisp)).setImageBitmap(bm);	
    										}
    										
    										((ImageView) findViewById(R.id.imgdisp)).setTag(imagename);
    										((EditText) findViewById(R.id.editreport)).setText("");
    										((EditText) findViewById(R.id.editreport)).setVisibility(v1.GONE);
    										((Button) findViewById(R.id.browsetxt)).setVisibility(v1.GONE);
    										((ImageView) findViewById(R.id.delete)).setVisibility(v1.VISIBLE);
    										((ImageView) findViewById(R.id.imgdisp)).setVisibility(v1.VISIBLE);
    									}
    									if(c1.getString(c1.getColumnIndex("NotRequired")).equals("1"))
    									{
    										chknotreq.setChecked(true);chkfdback.setChecked(true);chkfdback.setEnabled(false);
    										((Button) findViewById(R.id.browsetxt)).setEnabled(false);
    										((TextView) findViewById(R.id.reportidcol)).setVisibility(cf.v1.GONE);
    										
    									}
    									else
    									{
    										chknotreq.setChecked(false);chkfdback.setEnabled(true);
    										((Button) findViewById(R.id.browsetxt)).setEnabled(true);
    										((TextView) findViewById(R.id.reportidcol)).setVisibility(cf.v1.VISIBLE);
    									}
    									if(c1.getString(c1.getColumnIndex("FBNotRequired")).equals("1"))
    									{
    										chkfdback.setChecked(false);
    									}
    									else
    									{
    										chkfdback.setChecked(true);
    									}	
    							  }
    						      else
    						      {
    						    	  chkfdback.setChecked(false);
    						    	  chknotreq.setChecked(false);chkfdback.setEnabled(true);
    						    	  ((EditText) findViewById(R.id.editreport)).setText("");
    						    	  ((ImageView) findViewById(R.id.delete)).setVisibility(v1.GONE);
    						    	  ((ImageView) findViewById(R.id.imgdisp)).setVisibility(v1.GONE);
    						    	  ((EditText) findViewById(R.id.editreport)).setVisibility(v1.VISIBLE);
										((Button) findViewById(R.id.browsetxt)).setVisibility(v1.VISIBLE);
										((TextView) findViewById(R.id.reportidcol)).setVisibility(cf.v1.VISIBLE);
    						      }
    						}    						    						 
    					}

    					@Override
    					public void onNothingSelected(AdapterView<?> arg0) {
    						// TODO Auto-generated method stub
    						
    					}
    				});
    		    }
    		    catch (Exception e) {
    				// TODO: handle exception
    			}
    		}		    		
    	}
	}
	private void getinspid(String inspname) {
		// TODO Auto-generated method stub
		  System.out.println("dsdsd"+inspname);
		  Cursor c =cf.SelectTablefunction(cf.allinspnamelist, " Where  ARR_Insp_Name='"+cf.encode(inspname)+"'  order by ARR_Custom_ID");
		  System.out.println( " Where  ARR_Insp_Name='"+cf.encode(inspname)+"'");
			System.out.println("some ieeuse"+c.getCount());
			if(c.getCount()>0)
			{
				c.moveToFirst();
				inspid =	c.getInt(c.getColumnIndex("ARR_Insp_ID"));				
			}
	 }
	private  void show_values() {
			// TODO Auto-generated method stub
	   
		  	tbl_custom.removeAllViews();
	    	c2 = cf.arr_db.rawQuery("select * from " + cf.Custom_report +" Where SRID='"+cf.selectedhomeid+"' and IsMainInspection='0' and InspectionID='0'", null);
	    	if(c2.getCount()>0)
			{
				c2.moveToFirst();
				//arrfilepath = new String[c2.getCount()];
				for(int i=0;i<c2.getCount();i++,c2.moveToNext())
				{				
					/*arrfilepath[i]=cf.decode(c2.getString(c2.getColumnIndex("ReportImage")));
					if(!arraylistfilepah.contains(arrfilepath[i]))
					{
						arraylistfilepah.add(arrfilepath[i]);
					}*/
					
					
					dynamicreport(c2);
					
				}
				((Button) findViewById(R.id.save)).setVisibility(v1.VISIBLE);
			}
	    	else
	    	{
	    		((Button) findViewById(R.id.save)).setVisibility(v1.GONE);
	    	}
		}
	  private void dynamicreport(Cursor c) 
	  {
			// TODO Auto-generated method stub
		  System.out.println("curr"+c);
		  System.out.println("dynamicreport"+c.getCount());
			if(c.getCount()>0)
			{		
				//c.moveToFirst();
					int id=c.getInt(c.getColumnIndex("Ques_Id"));System.out.println("ID="+id);
					String title=c.getString(c.getColumnIndex("ReportTitle"));System.out.println("title="+title);
					String Path=cf.decode(c.getString(c.getColumnIndex("ReportImage")));System.out.println("Path="+Path);
					TableRow tbl=new TableRow(this);
		    		tbl.setId(id);
		    		System.out.println("TBL="+tbl.getId());
		    		TableLayout.LayoutParams tableRowParams=
		    				  new TableLayout.LayoutParams
		    				  (TableLayout.LayoutParams.FILL_PARENT,TableLayout.LayoutParams.WRAP_CONTENT);
		    		tableRowParams.setMargins(20,10,0,0);
		    		tbl.setLayoutParams(tableRowParams);
		    		
					tit_tv=new TextView(this);
		    		tit_tv.setWidth(0);
		    		System.out.println("what is titl"+title+tit_tv.getId());
		    		tit_tv.setTag("etitle");
		    		tit_tv.setText(title);
		    		tbl.addView(tit_tv,220,LayoutParams.WRAP_CONTENT);
		    		
		    		edittitle =new EditText(this);
		    		edittitle.setVisibility(View.GONE);
		    		edittitle.setTag("edtitle");
		    		tbl.addView(edittitle,220,LayoutParams.WRAP_CONTENT);
		    		InputFilter[] FilterArray;
		    		FilterArray = new InputFilter[1];
		    		FilterArray[0] = new InputFilter.LengthFilter(50);
		    		edittitle.setFilters(FilterArray);	    		
		    		
		    		
		    		ed=new EditText(this);
		    		ed.setWidth(0);
		    		ed.setEnabled(false);
		    		ed.setTag("edbrowse");
		    		ed.setId(id);
		    		System.out.println("Imagename="+ed.getText().toString());
		    		
		    		
		    		tbl.addView(ed,200, LayoutParams.WRAP_CONTENT);
		    		
		    		LayoutParams paramsbrowse = new LayoutParams(100, LayoutParams.WRAP_CONTENT);
		    		paramsbrowse.setMargins(10, 0, 0, 0);
		    		
		    		Button btn=new Button(this);
		    		btn.setText("Browse");
		    		btn.setTag("edbrowse");
		    		tbl.addView(btn,paramsbrowse);
		    		
		    		LayoutParams params = new LayoutParams(75,75);
		    	    params.setMargins(4, 4, 4, 4);
		    		
		    		img = new ImageView(this);
		    		img.setTag("edimg");
		    		tbl.addView(img,params);
		    		
		    		LayoutParams params1 = new LayoutParams(100, LayoutParams.WRAP_CONTENT);
		    	    params1.setMargins(10, 10, 10, 0);		    		
		    		edit = new Button(this);
		    		edit.setText("Edit");	    		
		    		edit.setTag("edit");
		    		tbl.addView(edit,params1);
		    		
		    		LayoutParams params2 = new LayoutParams(100, LayoutParams.WRAP_CONTENT);
		    		params2.setMargins(10, 10, 10, 0);		    		
					del = new Button(this);
		    		del.setText("Delete");		    		
		    		del.setTag("delimg");	  
		    		tbl.addView(del,params2);
		    		
		    		
		    		LayoutParams params3 = new LayoutParams(32, 32);	
		    	    params3.setMargins(10, 0, 10, 0);	
		    	    clear = new Button(this);
		    		clear.setTag("clearimg");
		    	    clear.setBackgroundResource(R.drawable.cancelicon);
		    		tbl.addView(clear,params3);
		    		
		    		if(!Path.equals(""))
		    		{
		    			if(Path.endsWith("pdf"))
		    			{
		    				img.setImageDrawable(getResources().getDrawable(R.drawable.pdficon));
		    			}
		    			else if(Path.endsWith("xls") || Path.endsWith("xlsx"))
		    			{
		    				img.setImageDrawable(getResources().getDrawable(R.drawable.xl));
		    			}
		    			else if(Path.endsWith("doc") || Path.endsWith("docx"))
		    			{
		    				img.setImageDrawable(getResources().getDrawable(R.drawable.wd));
		    			}
		    			else
		    			{
		    				 Bitmap bm = decodeFile(Path);
			    			 img.setImageBitmap(bm);
		    			}
		    			 ed.setVisibility(View.GONE);
		    			 btn.setVisibility(View.GONE);
		    			 del.setVisibility(View.VISIBLE);
		    			 edit.setVisibility(View.VISIBLE);
		    		}
		    		else
		    		{
		    			System.out.println("cam finally here");
		    			 ed.setVisibility(View.VISIBLE);
		    			 del.setVisibility(View.GONE);
		    			 btn.setVisibility(View.VISIBLE);
		    			 edit.setVisibility(View.GONE);
		    			 img.setVisibility(View.GONE);
		    			 
		    		}
		    		clear.setVisibility(View.GONE);
		    		tbl_custom.addView(tbl);
		    		
		    		btn.setOnClickListener(new browseonclick(id));
		    		del.setOnClickListener(new delclick(id));
		    		clear.setOnClickListener(new clearimge(id));
		    		img.setOnClickListener(new imgclick(id,Path,tit_tv.getText().toString()));
		    		edit.setOnClickListener(new editclick(id,title));
		    	
			}
		}
	   class clearimge implements OnClickListener
			{
				int id;
				public clearimge(int id) {
					// TODO Auto-generated constructor stub
					this.id=id;
				}
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					cf.ShowToast("Image Cleared successfully", 0);
					((EditText)(findViewById(id).findViewWithTag("edbrowse"))).setText("");
					((Button) (findViewById(id)).findViewWithTag("clearimg")).setVisibility(cf.v1.GONE);	
				}
				
			}
	  class imgclick implements OnClickListener
		{
			int id;
			String path,title;
			public imgclick(int id,String path,String title) {
				// TODO Auto-generated constructor stub
				this.id=id;
				this.path=path;
				this.title=title;
			}

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				System.out.println("this.aot="+this.path);
				if(this.path.endsWith(".pdf") || this.path.endsWith(".xls") || this.path.endsWith(".doc") || this.path.endsWith(".docx"))
				{
					viewdocument(this.id,this.path,title);					
				}
				else
				{
					System.out.println("ddsdfddfdsF");
					updateimage(this.id,this.path,title);

				}
			
			}
		}
	    class editclick implements OnClickListener
	  	{
	  		int id;String title;

	  		public editclick(int id,String title) {
	  			// TODO Auto-generated constructor stub
	  			this.id=id;
	  			this.title = title;
	  		}

	  		@Override
	  		public void onClick(View v) {
	  			// TODO Auto-generated method stub
	  			 ((TextView)(findViewById(this.id).findViewWithTag("etitle"))).setVisibility(View.GONE);
	  			((EditText)(findViewById(this.id).findViewWithTag("edtitle"))).setVisibility(View.VISIBLE);
	  			((EditText)(findViewById(this.id).findViewWithTag("edtitle"))).setText(title);
	  		
	  		}
	  		
	  	}
	    class delclick implements OnClickListener
		{
			int id;

			public delclick(int id) {
				// TODO Auto-generated constructor stub
				this.id=id;
			}

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder b =new AlertDialog.Builder(CustomInspection.this);
				b.setTitle("Confirmation");
				b.setMessage("Are you sure? Do you want to delete the selected Report?");
				b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
					System.out.println("testtest="+id);
					
						cf.arr_db.execSQL(" DELETE FROM "+cf.Custom_report+" WHERE Ques_Id='"+id+"'");
						show_values();
						
						
					}
				});
				b.setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
					}
				});
				AlertDialog al=b.create();
				al.setCancelable(false);
				al.show();
			}
		}
	    class browseonclick implements OnClickListener
		{
			int id;

			public browseonclick(int id) {
				// TODO Auto-generated constructor stub
				this.id=id;
			}

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try
				{
					
					Cursor c11 = cf.arr_db.rawQuery("SELECT * FROM " + cf.Custom_report+ " WHERE Ques_Id='" + this.id + "'", null);
		 			System.out.println("imgonclick"+c11.getCount());
		 			/***We call the centralized image selection part **/
					String[] selected_paht=null;
					if(c11.getCount()>0)
					{
		 				c11.moveToFirst();
		 				selected_paht = new String[c11.getCount()];
		 				
		 				for(int i=0;i<c11.getCount();i++)
		 				{
		 					selected_paht[i]=cf.decode(c11.getString(c11.getColumnIndex("ReportImage"))); 
		 					
		 					c11.moveToNext();
		 				}
					
					}
					System.out.println("ccccc");
				Intent reptoit1 = new Intent(CustomInspection.this,Select_CustomPhoto.class);
				Bundle b=new Bundle();
				reptoit1.putExtra("Selectedvalue", selected_paht); /**Send the already selected image **/
				reptoit1.putExtra("Maximumcount", 0);/**Total count of image in the database **/
				reptoit1.putExtra("Total_Maximumcount", 1); /***Total count of image we need to accept**/
				reptoit1.putExtra("ok", "true"); /***Total count of image we need to accept**/
				//reptoit1.setClassName("com.idinspection","com.idinspection.Select_phots");
				startActivityForResult(reptoit1,this.id); /** Call the Select image page in the idma application  image ***/
				}		
				catch ( Exception e) {
					// TODO: handle exception
					System.out.println("sds"+e.getMessage());
				}

	}
			
		}
	  private static Bitmap decodeFile(String file) {
		    try {
		    	
		    	File f=new File(file);
		    	
		        // Decode image size
		        BitmapFactory.Options o = new BitmapFactory.Options();
		        o.inJustDecodeBounds = true;
		        BitmapFactory.decodeStream(new FileInputStream(f), null, o);

		        // The new size we want to scale to
		        final int REQUIRED_SIZE = 150;

		        // Find the correct scale value. It should be the power of 2.
		        int scale = 1;
		        while (o.outWidth / scale / 2 >= REQUIRED_SIZE
		                && o.outHeight / scale / 2 >= REQUIRED_SIZE)
		            scale *= 2;

		        // Decode with inSampleSize
		        BitmapFactory.Options o2 = new BitmapFactory.Options();
		        o2.inSampleSize = scale;
		        o.inJustDecodeBounds = false;
		        return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
		    } catch (FileNotFoundException e) {
		    }

		    return null;
		}
	  public void viewdocument(int id, final String path, final String title) {
		// TODO Auto-generated method stub

	      final Dialog dialog = new Dialog(CustomInspection.this);
            dialog.getWindow().setContentView(R.layout.maindialog);
            dialog.setTitle(Html.fromHtml("<font color='#FFFFFF'>"+title+"</font>"));//.pri
            
            dialog.setCancelable(true);
            final ImageView img = (ImageView) dialog.findViewById(R.id.ImageView01);
            EditText ed1 = (EditText) dialog.findViewById(R.id.TextView01);
            ed1.setVisibility(v1.GONE);img.setVisibility(v1.GONE);
            
            Button button_close = (Button) dialog.findViewById(R.id.Button01);
    		button_close.setText("Close");//button_close.setVisibility(v2.GONE);
    		
    		
            Button button_d = (Button) dialog.findViewById(R.id.Button03);
    		button_d.setText("Delete");
    		button_d.setVisibility(v1.VISIBLE);
    		
    		final Button button_view = (Button) dialog.findViewById(R.id.Button02);
    		button_view.setText("View");
    		button_view.setVisibility(v1.VISIBLE);
    		
    		
    		final Button button_saveimage= (Button) dialog.findViewById(R.id.Button05);
    		button_saveimage.setVisibility(v1.GONE);
    		final LinearLayout linrotimage = (LinearLayout) dialog.findViewById(R.id.linrotation);
    		linrotimage.setVisibility(v1.GONE);
    		
    		final Button rotateleft= (Button) dialog.findViewById(R.id.rotateleft);
    		rotateleft.setVisibility(v1.GONE);
    		final Button rotateright= (Button) dialog.findViewById(R.id.rotateright);
    		rotateright.setVisibility(v1.GONE);
    		
    		
    		
    		
    		
    		button_d.setOnClickListener(new OnClickListener() {
	            public void onClick(View v) {
	            
	            AlertDialog.Builder builder = new AlertDialog.Builder(CustomInspection.this);
	   			builder.setMessage("Are you sure? Do you want to delete the selected document?")
	   				.setTitle("Confirmation")
	   				.setIcon(R.drawable.alertmsg)
   			       .setCancelable(false)
   			       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
   			           public void onClick(DialogInterface dialog, int id) {
   			        	 cf.arr_db.execSQL("UPDATE " +  cf.Custom_report + "  set ReportImage ='' where ReportTitle='"+title+"'");
   			        	cf.ShowToast("Document has been deleted successfully.",1);
 					   show_values();
   			           }
   			       })
   			       .setNegativeButton("No", new DialogInterface.OnClickListener() {
   			           public void onClick(DialogInterface dialog, int id) {
   			                dialog.cancel();
   			           }
   			       });
   			 builder.show();
            
   			 dialog.cancel(); 
        }
	           	
	  	 });
    		
    		
    		
    		button_view.setOnClickListener(new OnClickListener() {
	            public void onClick(View v) {
	            currnet_rotated=0;
	            	if(path.endsWith(".pdf") || path.endsWith(".doc") || path.endsWith(".docx") || path.endsWith(".xls") || path.endsWith(".xlsx"))
				     {
	            		img.setVisibility(v1.GONE);
	            		String tempstr ;
	            		if(path.contains("file://"))
	    		    	{
	    		    		 tempstr = path.replace("file://","");		    		
	    		    	}
	            		else
	            		{
	            			tempstr = path;
	            		}
	            		 File file = new File(tempstr);
					 
		                 if (file.exists()) 
		                 {
		                	 
		                	Uri path1 = Uri.fromFile(file);
		                    Intent intent = new Intent(Intent.ACTION_VIEW);
		                    if(path.endsWith("pdf"))
		                    { 	
		                    	intent.setDataAndType(path1, "application/pdf");
		                    }
		                    else if(path.endsWith("doc") || path.endsWith("docx"))
		                    {
		                    	intent.setDataAndType(path1, "application/msword");
		                    }
		                    else if(path.endsWith("xls") || path.endsWith("xlsx"))
		                    {
		                    	intent.setDataAndType(path1, "application/vnd.ms-excel");
		                    }

		                    
		                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		  
		                    try {
		                        startActivity(intent);
		                    } 
		                    catch (ActivityNotFoundException e) {
		                    	cf.ShowToast("No application available to view PDF.",1);
		                       
		                    }
		               
		                }
				     }
	            	else  
	            	{
	            		Bitmap bitmap2=cf.ShrinkBitmap(path,250,250);
	            		BitmapDrawable bmd2 = new BitmapDrawable(bitmap2); 
    					img.setImageDrawable(bmd2);
    					img.setVisibility(v1.VISIBLE);
    					linrotimage.setVisibility(v1.VISIBLE);
    					rotateleft.setVisibility(v1.VISIBLE);
    					rotateright.setVisibility(v1.VISIBLE);
			            button_view.setVisibility(v1.GONE);
			            button_saveimage.setVisibility(v1.VISIBLE);
			           
	            		
	            	}
	            }
    		});
    		rotateleft.setOnClickListener(new OnClickListener() {  			
    			public void onClick(View v) {

    				// TODO Auto-generated method stub
    			
    				System.gc();
    				currnet_rotated-=90;
    				if(currnet_rotated<0)
    				{
    					currnet_rotated=270;
    				}

    				
    				Bitmap myImg;
    				try {
    					myImg = BitmapFactory.decodeStream(new FileInputStream(path));
    					Matrix matrix =new Matrix();
    					matrix.reset();
    					//matrix.setRotate(currnet_rotated);
    					
    					matrix.postRotate(currnet_rotated);
    					
    					 rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
    					        matrix, true);
    					 
    					 img.setImageBitmap(rotated_b);

    				} catch (FileNotFoundException e) {
    					// TODO Auto-generated catch block
    					e.printStackTrace();
    				}
    				catch (Exception e) {
    					
    				}
    				catch (OutOfMemoryError e) {
    					
    					System.gc();
    					try {
    						myImg=null;
    						System.gc();
    						Matrix matrix =new Matrix();
    						matrix.reset();
    						//matrix.setRotate(currnet_rotated);
    						matrix.postRotate(currnet_rotated);
    						myImg= cf.ShrinkBitmap(path, 800, 800);
    						rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
    						System.gc();
    						img.setImageBitmap(rotated_b); 

    					} catch (Exception e1) {
    						// TODO Auto-generated catch block
    						e1.printStackTrace();
    					}
    					 catch (OutOfMemoryError e1) {
    							// TODO Auto-generated catch block
    						 cf.ShowToast("You cannot rotate this image. Image size is too large.",1);
    					}
    				}

    			
    			}
    		});
    		rotateright.setOnClickListener(new OnClickListener() {
    		    public void onClick(View v) {
    		    	
    		    	currnet_rotated+=90;
    				if(currnet_rotated>=360)
    				{
    					currnet_rotated=0;
    				}
    				
    				Bitmap myImg;
    				try {
    					myImg = BitmapFactory.decodeStream(new FileInputStream(path));
    					Matrix matrix =new Matrix();
    					matrix.reset();
    					//matrix.setRotate(currnet_rotated);
    					
    					matrix.postRotate(currnet_rotated);
    					
    					 rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
    					        matrix, true);
    					 
    					 img.setImageBitmap(rotated_b);  

    				} catch (FileNotFoundException e) {
    					//.println("FileNotFoundException "+e.getMessage()); 
    					// TODO Auto-generated catch block
    					e.printStackTrace();
    				}
    				catch(Exception e){}
    				catch (OutOfMemoryError e) {
    					//.println("comes in to out ot mem exception");
    					System.gc();
    					try {
    						myImg=null;
    						System.gc();
    						Matrix matrix =new Matrix();
    						matrix.reset();
    						//matrix.setRotate(currnet_rotated);
    						matrix.postRotate(currnet_rotated);
    						myImg= cf.ShrinkBitmap(path, 800, 800);
    						rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
    						System.gc();
    						img.setImageBitmap(rotated_b); 

    					} catch (Exception e1) {
    						// TODO Auto-generated catch block
    						e1.printStackTrace();
    					}
    					 catch (OutOfMemoryError e1) {
    							// TODO Auto-generated catch block
    						 cf.ShowToast("You cannot rotate this image. Image size is too large.",1);
    					}
    				}

    		    }
    		});
    		button_saveimage.setOnClickListener(new OnClickListener() {
				
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					
					if(currnet_rotated>0)
					{ 

						try
						{
							/**Create the new image with the rotation **/
					
							String current=MediaStore.Images.Media.insertImage(getContentResolver(), rotated_b, "My bitmap", "My rotated bitmap");
							 ContentValues values = new ContentValues();
							  values.put(MediaStore.Images.Media.ORIENTATION, 0);
							  CustomInspection.this.getContentResolver().update(Uri.parse(current), values, MediaStore.Images.Media.DATA+ "=?", new String[] { current } );
							
							if(current!=null)
							{
							String path=getPath(Uri.parse(current));
							File fout = new File(path);
							fout.delete();
							/** delete the selected image **/
							File fin = new File(path);
							/** move the newly created image in the slected image pathe ***/
							fin.renameTo(new File(path));
							cf.ShowToast("Document saved successfully.",1);dialog.cancel();
							show_values();
							
							
						}
						} catch(Exception e)
						{
							//.println("Error occure while rotate the image "+e.getMessage());
						}
						
					}
					else
					{
						cf.ShowToast("Document saved successfully.",1);
						dialog.cancel();
						show_values();
					}
					
				}
			});
    		button_close.setOnClickListener(new OnClickListener() {
	            public void onClick(View v) {
	            	dialog.cancel();
	            }
    		});
    		dialog.show();
	}
	public void updateimage(final int id,final String path,final String title) {
		// TODO Auto-generated method stub
		
System.out.println("path="+path);
			 System.gc();
			currnet_rotated=0;
			

			String k;
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;

			final Dialog dialog1 = new Dialog(CustomInspection.this,
					android.R.style.Theme_Translucent_NoTitleBar);
			dialog1.getWindow().setContentView(R.layout.alert);
			/**
			 * get the help and update relative layou and set the visbilitys for the
			 * respective relative layout
			 */
			LinearLayout Re = (LinearLayout) dialog1.findViewById(R.id.maintable);
			Re.setVisibility(View.GONE);
			LinearLayout Reup = (LinearLayout) dialog1
					.findViewById(R.id.updateimage);
			Reup.setVisibility(View.VISIBLE);
			/**
			 * get the help and update relative layou and set the visbilitys for the
			 * respective relative layout
			 */
			

			upd_img = (ImageView) dialog1.findViewById(R.id.firstimg);
			final EditText upd_Ed = (EditText) dialog1.findViewById(R.id.firsttxt);
			upd_Ed.setFocusable(false);upd_Ed.setVisibility(View.GONE);
			
			ImageView btn_helpclose = (ImageView) dialog1.findViewById(R.id.imagehelpclose);
			Button btn_up = (Button) dialog1.findViewById(R.id.update);
			Button btn_del = (Button) dialog1.findViewById(R.id.delete);
			Button rotateleft = (Button) dialog1.findViewById(R.id.rotateleft);rotateleft.setVisibility(View.GONE);
			Button rotateright = (Button) dialog1.findViewById(R.id.rotateright);rotateright.setVisibility(View.GONE);
	        Button zoom = (Button) dialog1.findViewById(R.id.zoom);
	        zoom.setVisibility(View.GONE);
			
	        btn_del.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					
					 cf.arr_db.execSQL("UPDATE " +  cf.Custom_report + "  set ReportImage ='' where ReportTitle='"+title+"'");
			         cf.ShowToast("Document has been deleted successfully.",1);
					 show_values();
					 upd_img=null;
					 dialog1.setCancelable(true);
					 dialog1.dismiss();				
					
				}
			});
	        
	        btn_up.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					System.out.println("updated_path="+updated_path);

					boolean filecheck=false;
					arraylistfilepahlength=arraylistfilepah.size();
					if(arraylistfilepahlength==0)
					{
						filecheck=true;
					}
					else
					{
						if(arraylistfilepah.contains(updated_path))
						{
							System.out.println("inside arraylist contains if");
							filecheck=false;
						}
						else
						{
							System.out.println("inside arraylist contains else");
							filecheck=true;
						}
					}
					System.out.println("filecheck value is "+filecheck);
					
					if(filecheck)
					{
						try {
							
							dialog1.setCancelable(true);
							dialog1.dismiss();
							if(updated_path.equals(""))
							{
								updated_path=path;
							}
							
							cf.arr_db.execSQL("UPDATE " + cf.Custom_report									
									+ " SET  ReportImage='"+cf.encode(updated_path)+"' WHERE SRID ='"+ cf.selectedhomeid+ "' and ReportTitle='" + title + "'");
							System.out.println("buttonUPDATE " + cf.Custom_report									
									+ " SET  ReportImage='"+cf.encode(updated_path)+"' WHERE SRID ='"+ cf.selectedhomeid+ "' and ReportTitle='" + title + "'");
							cf.ShowToast("Saved successfully.", 0);
						
					
						
					} catch (Exception e) {
						System.out.println("erre " + e.getMessage());
						
					}
					/**Save the rotated value in to the external stroage place ends **/
					updated_path="";
					upd_img=null;
					show_values();
					}
					else
					{
						cf.ShowToast("File already selected, Please choose different one",0);

						
					}
					
				}

			});
			
			upd_img.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					try{
					Cursor c11 = cf.arr_db.rawQuery("SELECT * FROM " + cf.Custom_report+ " WHERE Ques_Id='" + id + "'", null);
		 			System.out.println("imgonclick"+c11.getCount());
		 			/***We call the centralized image selection part **/
					String[] selected_paht=null;
					if(c11.getCount()>0)
					{
		 				c11.moveToFirst();
		 				selected_paht = new String[c11.getCount()];
		 				
		 				for(int i=0;i<c11.getCount();i++)
		 				{
		 					selected_paht[i]=cf.decode(c11.getString(c11.getColumnIndex("ReportImage"))); 
		 					
		 					c11.moveToNext();
		 				}
					
					}
					System.out.println("ccccc");
				Intent reptoit1 = new Intent(CustomInspection.this,Select_CustomPhoto.class);
				Bundle b=new Bundle();
				reptoit1.putExtra("Selectedvalue", selected_paht); /**Send the already selected image **/
				reptoit1.putExtra("Maximumcount", 0);/**Total count of image in the database **/
				reptoit1.putExtra("Total_Maximumcount", 1); /***Total count of image we need to accept**/
				reptoit1.putExtra("ok", "true"); /***Total count of image we need to accept**/
				//reptoit1.setClassName("com.idinspection","com.idinspection.Select_phots");
				startActivityForResult(reptoit1,id); /** Call the Select image page in the idma application  image ***/
				}		
				
				catch ( Exception e) {
					// TODO: handle exception
					System.out.println("sds"+e.getMessage());
				
				}
				}
			});
			

		
			
			btn_helpclose.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog1.setCancelable(true);
					dialog1.dismiss();
				}
			});
			
					// update and delete button function ends
					try {
						System.out.println("path="+path);
							BitmapFactory.decodeStream(new FileInputStream(path),null, o);
						k = "1";
		
					} catch (FileNotFoundException e) {
						k = "2";
					}
					if (k.equals("1")) {
						final int REQUIRED_SIZE = 400;
						int width_tmp = o.outWidth, height_tmp = o.outHeight;
						int scale = 1;
						while (true) {
							if (width_tmp / 2 < REQUIRED_SIZE
									|| height_tmp / 2 < REQUIRED_SIZE)
								break;
							width_tmp /= 2;
							height_tmp /= 2;
							scale *= 2;
						}
		
						// Decode with inSampleSize
						BitmapFactory.Options o2 = new BitmapFactory.Options();
						o2.inSampleSize = scale;
						Bitmap bitmap = null;
						
						try {
							bitmap = BitmapFactory.decodeStream(new FileInputStream(path), null, o2);
							System.out.println("bbbbitmap="+bitmap);
						} catch (FileNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						BitmapDrawable bmd = new BitmapDrawable(bitmap);
						
						upd_img.setImageDrawable(bmd);
		
						dialog1.setCancelable(false);
						dialog1.show();
					} else {
						rotateright.setVisibility(View.GONE);
						rotateleft.setVisibility(View.GONE);
						dialog1.setCancelable(false);
						dialog1.show();
					}
					upd_Ed.clearFocus();
			      //cf.hidekeyboard();
				
			
	}
	 public String getPath(Uri uri) {
			String[] projection = { MediaStore.Images.Media.DATA };
			Cursor cursor = managedQuery(uri, projection, null, null, null);
			int column_index = cursor
					.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		}
	private void showdynamic1()
	  {
	  	
	  		// TODO Auto-generated method stub
	  		final Dialog dialog = new Dialog(CustomInspection.this,android.R.style.Theme_Translucent_NoTitleBar);
	  		dialog.getWindow().setContentView(R.layout.alert);
	  		dialog.findViewById(R.id.Add_caption).setVisibility(View.VISIBLE);
	  		((TextView) dialog.findViewById(R.id.add_caption_tit)).setText("Report title");
	  		final EditText input =(EditText)dialog.findViewById(R.id.caption_text);
	  		((TextView) dialog.findViewById(R.id.EN_txtid1)).setText("Please enter Report title");
	  		
	  		InputFilter[] FilterArray;
	  		FilterArray = new InputFilter[1];
	  		FilterArray[0] = new InputFilter.LengthFilter(50);
	  		input.setFilters(FilterArray);
	  		Button save=(Button)dialog.findViewById(R.id.caption_save);
	  		Button can=(Button)dialog.findViewById(R.id.caption_cancel);
	  		ImageView cls=(ImageView)dialog.findViewById(R.id.caption_close);
	  		cls.setOnClickListener(new OnClickListener() {
	  			
	  			@Override
	  			public void onClick(View v) {
	  				// TODO Auto-generated method stub
	  				dialog.setCancelable(true);
	  				dialog.cancel();

	  			}
	  		});
	  		save.setOnClickListener(new OnClickListener() {							
	  			@Override
	  			public void onClick(View v) {
	  				String title=input.getText().toString().trim();
	  				if(title.equals(""))
	  				{
	  					cf.ShowToast("Please enter the Report Title.", 0);
	  				}
	  				else
	  				{
	  					Cursor c1 = cf.arr_db.rawQuery("select * from " + cf.Custom_report + " where SRID='"+cf.selectedhomeid+"' and ReportTitle='"+title+"'", null);
		  				if(c1.getCount()<=0)
		  				{
		  					cf.arr_db.execSQL(" INSERT INTO "+cf.Custom_report+" (SRID,ReportImage,ReportTitle,NotRequired,FBNotRequired,IsMainInspection,InspectionID) VALUES" +
		  							" ('"+cf.selectedhomeid+"','','"+title+"','0','0','0','0')");
		  					
		  				}
		  				else
		  				{
		  					cf.ShowToast("Already report title exists.", 0);
		  				}
		  				c2 = cf.arr_db.rawQuery("select * from " + cf.Custom_report +" Where SRID='"+cf.selectedhomeid+"' and IsMainInspection='0' and InspectionID='0' and ReportTitle='"+title+"'", null);
		  				System.out.println("select * from " + cf.Custom_report +" Where SRID='"+cf.selectedhomeid+"' and IsMainInspection='0' and InspectionID='0' and ReportTitle='"+title+"'");
		  				c2.moveToFirst();
		  				dynamicreport(c2);
		  				((Button) findViewById(R.id.save)).setVisibility(v1.VISIBLE);
		  				dialog.cancel();
		  				
	  				}
	  			}
	  		});
	  		
	  		can.setOnClickListener(new OnClickListener() {
	  			
	  			@Override
	  			public void onClick(View v) {
	  				// TODO Auto-generated method stub
	  						dialog.setCancelable(true);
	  						dialog.cancel();
	  						
	  					}
	  				});
	  		dialog.setCancelable(false);
	  		dialog.show();
	  	}
	public void clicker(View v) {
		switch (v.getId()) {
		case R.id.maininspimgdisp:
			String imagepath = ((ImageView) findViewById(R.id.maininspimgdisp)).getTag().toString();			
			viewdocument(imagepath);
			break;
		case R.id.imgdisp:
			String imagepath1 = ((ImageView) findViewById(R.id.imgdisp)).getTag().toString();			
			viewdocument(imagepath1);
			break;
		case R.id.browsemaininsp:			
			Intent reptoit2= new Intent(CustomInspection.this,Select_CustomPhoto.class);
			Bundle b1=new Bundle();
			reptoit2.putExtra("Selectedvalue", ""); /**Send the already selected image **/
			reptoit2.putExtra("Maximumcount", 0);/**Total count of image in the database **/
			reptoit2.putExtra("Total_Maximumcount", 1); /***Total count of image we need to accept**/
			reptoit2.putExtra("ok", "true"); /***Total count of image we need to accept**/
			//reptoit1.setClassName("com.idinspection","com.idinspection.Select_phots");
			startActivityForResult(reptoit2,127); /** Call the Select image page in the idma application  image ***/			
			break;
		case R.id.browsetxt:			
			Intent reptoit1 = new Intent(CustomInspection.this,Select_CustomPhoto.class);
			Bundle b=new Bundle();
			reptoit1.putExtra("Selectedvalue", ""); /**Send the already selected image **/
			reptoit1.putExtra("Maximumcount", 0);/**Total count of image in the database **/
			reptoit1.putExtra("Total_Maximumcount", 1); /***Total count of image we need to accept**/
			reptoit1.putExtra("ok", "true"); /***Total count of image we need to accept**/
			//reptoit1.setClassName("com.idinspection","com.idinspection.Select_phots");
			startActivityForResult(reptoit1,126); /** Call the Select image page in the idma application  image ***/			
			break;
		case R.id.clicknewrepot:
			showdynamic1();
			break;
		case R.id.save:
			saveval();
			break;
		case R.id.chknotrequired:
			 if(chknotreq.isChecked())
			 {
				
				 if(!((EditText) findViewById(R.id.editreport)).getText().toString().equals("") ||
						 ((ImageView) findViewById(R.id.imgdisp)).getVisibility()==v1.VISIBLE)
				 {
					  AlertDialog.Builder builder = new AlertDialog.Builder(CustomInspection.this);
			   			builder.setMessage("Are you sure? Do you want to delete the selected document?")
			   				.setTitle("Confirmation")
			   				.setIcon(R.drawable.alertmsg)
		 			       .setCancelable(false)
		 			       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
		 			           public void onClick(DialogInterface dialog, int id) {
		 			        	   getinspid(((Spinner)findViewById(R.id.addinsptitle)).getSelectedItem().toString());
		 			        	 cf.arr_db.execSQL("Update " +  cf.Custom_report + " SET ReportImage='' WHERE SRID ='" + cf.selectedhomeid + "' and IsMainInspection='0' and InspectionID='"+inspid+"'");
		 			        	cf.ShowToast("Document has been deleted successfully.",1);
		 			        	((EditText) findViewById(R.id.editreport)).setVisibility(cf.v1.VISIBLE);
		 			        	((EditText) findViewById(R.id.editreport)).setText("");
		 			        	((ImageView) findViewById(R.id.imgdisp)).setVisibility(cf.v1.GONE);
		 			        	((Button) findViewById(R.id.browsetxt)).setVisibility(cf.v1.VISIBLE);
		 			        	
		 			        	 //show_addiservreport();
		 			           }
		 			       })
		 			       .setNegativeButton("No", new DialogInterface.OnClickListener() {
		 			           public void onClick(DialogInterface dialog, int id) {
		 			        	   
		 			        	  chknotreq.setChecked(false);
		 			        	   
		 			                dialog.cancel();
		 			           }
		 			       });
		 			 builder.show();
		          
				 }
				 ((Button) findViewById(R.id.browsetxt)).setEnabled(false);
				 chkfdback.setEnabled(false);chkfdback.setChecked(true);((EditText) findViewById(R.id.editreport)).setText("");
				 ((TextView) findViewById(R.id.reportidcol)).setVisibility(v1.GONE);
			 }
			 else
			 {
				 System.out.println("fbchk="+fbchk);
				 if(fbchk==1)
				 {
					 chkfdback.setChecked(true); 
				 }
				 else
				 {
					 chkfdback.setChecked(false);
				 }
				
				 chkfdback.setEnabled(true);	 
				 ((Button) findViewById(R.id.browsetxt)).setEnabled(true);
				 
				 ((TextView) findViewById(R.id.reportidcol)).setVisibility(v1.VISIBLE);	
			 }
			break;
		case R.id.chkfeedback:
			if(chkfdback.isChecked()==true)
			{
				fbchk=1;
			}
			else
			{
				fbchk=2;
			}
			break;
		case R.id.maininspdelete:
			  AlertDialog.Builder builder1 = new AlertDialog.Builder(CustomInspection.this);
	   			builder1.setMessage("Are you sure? Do you want to delete the selected document?")
	   				.setTitle("Confirmation")
	   				.setIcon(R.drawable.alertmsg)
			       .setCancelable(false)
			       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			           public void onClick(DialogInterface dialog, int id) {
			        	 cf.arr_db.execSQL("Update " +  cf.Custom_report + " SET ReportImage='' WHERE SRID ='" + cf.selectedhomeid + "' and IsMainInspection='1'");
			        	cf.ShowToast("Document has been deleted successfully.",1);
			        	((EditText) findViewById(R.id.edtmaininsp)).setText("");	
			        	show_maininspreport();
			           }
			       })
			       .setNegativeButton("No", new DialogInterface.OnClickListener() {
			           public void onClick(DialogInterface dialog, int id) {
			                dialog.cancel();
			           }
			       });
			 builder1.show();
        
			break;
		case R.id.delete:
			  AlertDialog.Builder builder = new AlertDialog.Builder(CustomInspection.this);
	   			builder.setMessage("Are you sure? Do you want to delete the selected document?")
	   				.setTitle("Confirmation")
	   				.setIcon(R.drawable.alertmsg)
 			       .setCancelable(false)
 			       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
 			           public void onClick(DialogInterface dialog, int id) {
 			        	   getinspid(((Spinner)findViewById(R.id.addinsptitle)).getSelectedItem().toString());
 			        	 cf.arr_db.execSQL("Update " +  cf.Custom_report + " SET ReportImage='' WHERE SRID ='" + cf.selectedhomeid + "' and IsMainInspection='0' and InspectionID='"+inspid+"'");
 			        	cf.ShowToast("Document has been deleted successfully.",1);
 			        	((EditText) findViewById(R.id.editreport)).setText("");	
 			        	 show_addiservreport();
 			           }
 			       })
 			       .setNegativeButton("No", new DialogInterface.OnClickListener() {
 			           public void onClick(DialogInterface dialog, int id) {
 			                dialog.cancel();
 			           }
 			       });
 			 builder.show();
          
			break;
		case R.id.oervallsave:
			if(((LinearLayout) findViewById(R.id.maininspectionlin)).getVisibility()==v1.VISIBLE)
			{
				if(((EditText) findViewById(R.id.edtmaininsp)).getText().toString().equals("") && ((ImageView) findViewById(R.id.maininspimgdisp)).getVisibility()==v1.GONE)
				 {
					 cf.ShowToast("Please upload file for "+((TextView) findViewById(R.id.maininsptitle)).getText().toString(), 0);
				 }
				 else
				 {
					additionalservalid();
				 }	
			}
			else
			{
				additionalservalid();
			}
			
			
			break;
		case R.id.addiservsave:
			 if(!((Spinner) findViewById(R.id.addinsptitle)).getSelectedItem().toString().equals("--Select--"))
			 {
				 if(chknotreq.isChecked())
				 {
					 saveaddiservcereport();
				 }
				 else
				 {
					 if(((EditText) findViewById(R.id.editreport)).getText().toString().equals("") && ((ImageView) findViewById(R.id.imgdisp)).getVisibility()==v1.GONE)
					 {
						 cf.ShowToast("Please upload file for "+((Spinner) findViewById(R.id.addinsptitle)).getSelectedItem().toString(), 0);
					 }
					 else
					 {
						 saveaddiservcereport();
					 }
				 }
			 }
			 else
			 {
				 saveaddiservcereport();
			 }
			break;
			
		}
	}			
	private void viewdocument(String imagepath) {
		// TODO Auto-generated method stub
		if(imagepath.endsWith(".pdf") || imagepath.endsWith(".doc") || imagepath.endsWith(".docx") || imagepath.endsWith(".xls") || imagepath.endsWith(".xlsx"))
		{
			String tempstr ;
    		if(imagepath.contains("file://"))
	    	{
	    		 tempstr = imagepath.replace("file://","");		    		
	    	}
    		else
    		{
    			tempstr = imagepath;
    		}
    		 File file = new File(tempstr);
		 
             if (file.exists()) 
             {
            	 
            	Uri path1 = Uri.fromFile(file);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                if(imagepath.endsWith("pdf"))
                { 	
                	intent.setDataAndType(path1, "application/pdf");
                }
                else if(imagepath.endsWith("doc") || imagepath.endsWith("docx"))
                {
                	intent.setDataAndType(path1, "application/msword");
                }
                else if(imagepath.endsWith("xls") || imagepath.endsWith("xlsx"))
                {
                	intent.setDataAndType(path1, "application/vnd.ms-excel");
                }

                
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                try {
                    startActivity(intent);
                } 
                catch (ActivityNotFoundException e) {
                	cf.ShowToast("No application available to view PDF.",1);
                   
                }
           
            }
	     
		}
	}
	private void additionalservalid() {
		// TODO Auto-generated method stub
		for(int i=1;i<selected_insp.length;i++)
		{
			Cursor c = cf.arr_db.rawQuery("select * from " + cf.Custom_report +" Where SRID='"+cf.selectedhomeid+"' and  ReportTitle='"+selected_insp[i]+"'", null);
		 	if(c.getCount()==0)
		 	{
		 		cf.ShowToast("Please upload file for "+selected_insp[i]+" in Additional services", 0);
		 		return;
		 	}	
		}

		addiservicevalidation();
	}
	private void addiservicevalidation() {
		// TODO Auto-generated method stub
		
		Cursor c = cf.arr_db.rawQuery("select * from " + cf.Custom_report +" Where SRID='"+cf.selectedhomeid+"' and  IsMainInspection='0' and InspectionID='0'", null);
	 	System.out.println("sdsd"+c.getCount());
	 	if(c.getCount()>0)
		{
				c.moveToFirst();
				for(int i=0;i<c.getCount();i++,c.moveToNext())
				{
					
							int id=c.getInt(c.getColumnIndex("Ques_Id"));
							String path=cf.decode(c.getString(c.getColumnIndex("ReportImage")));
							System.out.println("id="+id);
							String value=((EditText)(findViewById(id).findViewWithTag("edbrowse"))).getText().toString();
							System.out.println("value="+value);
							System.out.println("oath="+path);
							
							
							 if(value.trim().equals("") && path.equals(""))
							 {
									cf.ShowToast("Please upload image for Other Reports.", 0);
									return;
							
							 }
						  
				}
				
		}
	 	savereport();
	}
	private void savereport() {
		// TODO Auto-generated method stub
		try
		{
			cf.CreateARRTable(91);
			if(((LinearLayout) findViewById(R.id.maininspectionlin)).getVisibility()==v1.VISIBLE)
			{
				Cursor c1 = cf.arr_db.rawQuery("select * from " + cf.Custom_report +" Where SRID='"+cf.selectedhomeid+"' and IsMainInspection='1'", null);
				if(c1.getCount()==0)
				{
					cf.arr_db.execSQL(" INSERT INTO "+cf.Custom_report+" (SRID,ReportImage,ReportTitle,NotRequired,FBNotRequired,IsMainInspection,InspectionID) VALUES" +
						" ('"+cf.selectedhomeid+"','"+cf.encode(((EditText) findViewById(R.id.edtmaininsp)).getText().toString())+"','"+((TextView) findViewById(R.id.maininsptitle)).getText().toString()+"','0','1','1','"+((TextView) findViewById(R.id.maininsptitle)).getTag().toString()+"')");
				}
				else
				{
					if(((EditText) findViewById(R.id.edtmaininsp)).getVisibility()==v1.VISIBLE)
					{
						cf.arr_db.execSQL(" UPDATE "+cf.Custom_report+" SET  ReportImage='"+cf.encode(((EditText) findViewById(R.id.edtmaininsp)).getText().toString())+"' WHERE SRID='"+cf.selectedhomeid+"' and IsMainInspection='1'");
					}
				}
			}			
			
		 	cf.ShowToast("Custom Inspection Report saved successfully.", 0);
		 	show_maininspreport();
		 	show_addiservreport();
		}
		catch(Exception e)
		{
			System.out.println("tetserror="+e.getMessage());
		}	 	
	}
	private void saveaddiservcereport()
	{
		if(((TableLayout) findViewById(R.id.additionalservlin)).getVisibility()==v1.VISIBLE)
		{
			if(chknotreq.isChecked()==true) { strnotreq =1;	}	else { strnotreq=0; }				 	
		 	
		 	if(chknotreq.isChecked()==false && chkfdback.isChecked()==false) 
		 	{ 
		 		fbnotreq =1;  
		 	} 	
		 	else 
		 	{
		 		fbnotreq =0;			 					 	
		 	}		 	
		 	getinspid(((Spinner) findViewById(R.id.addinsptitle)).getSelectedItem().toString());
			
			Cursor c2 = cf.arr_db.rawQuery("select * from " + cf.Custom_report +" Where SRID='"+cf.selectedhomeid+"' and IsMainInspection='0' and InspectionID='"+inspid+"'", null);
			if(c2.getCount()==0)
			{
				System.out.println(" INSERT INTO "+cf.Custom_report+" (SRID,ReportImage,ReportTitle,NotRequired,FBNotRequired,IsMainInspection,InspectionID) VALUES" +
					" ('"+cf.selectedhomeid+"','"+cf.encode(((EditText) findViewById(R.id.editreport)).getText().toString())+"','"+((Spinner) findViewById(R.id.addinsptitle)).getSelectedItem().toString()+"','"+strnotreq+"','"+fbnotreq+"','0','"+inspid+"')");
				
				cf.arr_db.execSQL(" INSERT INTO "+cf.Custom_report+" (SRID,ReportImage,ReportTitle,NotRequired,FBNotRequired,IsMainInspection,InspectionID) VALUES" +
					" ('"+cf.selectedhomeid+"','"+cf.encode(((EditText) findViewById(R.id.editreport)).getText().toString())+"','"+((Spinner) findViewById(R.id.addinsptitle)).getSelectedItem().toString()+"','"+strnotreq+"','"+fbnotreq+"','0','"+inspid+"')");
			}
			else
			{
				if(((EditText) findViewById(R.id.editreport)).getVisibility()==v1.VISIBLE || ((ImageView) findViewById(R.id.imgdisp)).getVisibility()==v1.VISIBLE)
				{
					if(((EditText) findViewById(R.id.editreport)).getVisibility()==v1.VISIBLE)
					{
						System.out.println(" UPDATE "+cf.Custom_report+" SET  ReportImage='"+((EditText) findViewById(R.id.editreport)).getText().toString()+"',FBNotRequired='"+fbnotreq+"',NotRequired='"+strnotreq+"' WHERE SRID='"+cf.selectedhomeid+"' and InspectionID='"+inspid+"'");
						cf.arr_db.execSQL(" UPDATE "+cf.Custom_report+" SET  ReportImage='"+cf.encode(((EditText) findViewById(R.id.editreport)).getText().toString())+"',FBNotRequired='"+fbnotreq+"',NotRequired='"+strnotreq+"' WHERE SRID='"+cf.selectedhomeid+"' and InspectionID='"+inspid+"'");
					}
					else
					{
						System.out.println("elesse"+" UPDATE "+cf.Custom_report+" SET  FBNotRequired='1',NotRequired='"+strnotreq+"' WHERE SRID='"+cf.selectedhomeid+"' and InspectionID='"+inspid+"'");
						cf.arr_db.execSQL(" UPDATE "+cf.Custom_report+" SET  FBNotRequired='"+fbnotreq+"',NotRequired='"+strnotreq+"' WHERE SRID='"+cf.selectedhomeid+"' and InspectionID='"+inspid+"'");
					}
				}
			}
			cf.ShowToast("Saved Successfully.", 0);
			show_values();show_addiservreport();
		}
	}
	protected void saveval() {
		// TODO Auto-generated method stub
		Cursor c = cf.arr_db.rawQuery("select * from " + cf.Custom_report +" Where SRID='"+cf.selectedhomeid+"' and  IsMainInspection='0' and InspectionID='0'", null);
	 	System.out.println("sdsd"+c.getCount());
	 	if(c.getCount()>0)
		{
				c.moveToFirst();
				for(int i=0;i<c.getCount();i++,c.moveToNext())
				{
					
							int id=c.getInt(c.getColumnIndex("Ques_Id"));
							String path=cf.decode(c.getString(c.getColumnIndex("ReportImage")));
							System.out.println("id="+id);
							String value=((EditText)(findViewById(id).findViewWithTag("edbrowse"))).getText().toString();
							if(((EditText)(findViewById(id).findViewWithTag("edtitle"))).getVisibility()==v1.VISIBLE && 
									((EditText)(findViewById(id).findViewWithTag("edtitle"))).getText().toString().trim().equals(""))
							{
								Toast.makeText(getApplicationContext(), "Please enter report title",Toast.LENGTH_LONG).show();
								return;
							}
							else
							{							
								if(value.trim().equals("") && path.equals(""))
								{
									Toast.makeText(getApplicationContext(), "Please upload image for "+c.getString(c.getColumnIndex("ReportTitle")),
											   Toast.LENGTH_LONG).show();
									
									return;
								}
							}
				}
				savereportimage();
		}
	}
	private void savereportimage() {
		// TODO Auto-generated method stub
		System.out.println("inside save repor");
		String imagepath="";
		Cursor c1 = cf.arr_db.rawQuery("select * from " + cf.Custom_report +" Where SRID='"+cf.selectedhomeid+"' and  IsMainInspection='0' and InspectionID='0'", null);
		if(c1.getCount()>=0)
		{
			c1.moveToFirst();
			for(int i=0;i<c1.getCount();i++,c1.moveToNext())
			{
				int id=c1.getInt(c1.getColumnIndex("Ques_Id"));
				String title=c1.getString(c1.getColumnIndex("ReportTitle"));
				String dbpath=cf.decode(c1.getString(c1.getColumnIndex("ReportImage")));
				if(dbpath.equals(""))
				{
					imagepath =((EditText)(findViewById(id).findViewWithTag("edbrowse"))).getText().toString(); 
				}
				else
				{
					imagepath = dbpath;
				}		
				System.out.println("impa="+imagepath);
				if(((EditText)(findViewById(id).findViewWithTag("edtitle"))).getVisibility()==v1.VISIBLE)
				{
					System.out.println("edtitle=");
					Cursor c = cf.arr_db.rawQuery("select * from " + cf.Custom_report +" Where SRID='"+cf.selectedhomeid+"' and ReportTitle='"+((EditText)(findViewById(id).findViewWithTag("edtitle"))).getText().toString().trim()+"'", null);
					System.out.println("c="+c.getCount());
					if(c.getCount()==0)
					{
						cf.arr_db.execSQL(" UPDATE "+cf.Custom_report+" SET  ReportImage='"+cf.encode(imagepath)+"',ReportTitle='"+((EditText)(findViewById(id).findViewWithTag("edtitle"))).getText().toString()+"' WHERE ReportTitle='"+title+"'");
						System.out.println(" UPDATE "+cf.Custom_report+" SET  ReportImage='"+imagepath+"',ReportTitle='"+((EditText)(findViewById(id).findViewWithTag("edtitle"))).getText().toString()+"' WHERE ReportTitle='"+title+"'");
					}
					else
					{
						cf.ShowToast("Already Exists.Please enter different Report title.",0);
					}
				}
				else
				{
					System.out.println(" UPDATE "+cf.Custom_report+" SET  ReportImage='"+cf.encode(imagepath)+"' WHERE ReportTitle='"+title+"'");
					cf.arr_db.execSQL(" UPDATE "+cf.Custom_report+" SET  ReportImage='"+cf.encode(imagepath)+"' WHERE ReportTitle='"+title+"'");
				}				
				
				((EditText)(findViewById(id).findViewWithTag("edtitle"))).setVisibility(v1.GONE);
				((TextView)(findViewById(id).findViewWithTag("etitle"))).setVisibility(v1.VISIBLE);		
				
			}
			cf.ShowToast("Custom Report Saved Successfully.",0);
			upd_img=null;
			show_values();
		}
	}
	  @Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
			// TODO Auto-generated method stub
			if (resultCode == RESULT_OK) {
				String[] value=	data.getExtras().getStringArray("Selected_array");
				System.out.println("resulcode"+requestCode);
				if(requestCode==126)
				{
					for(int i=0;i<value.length;i++)
				    {
						 File f = new File(value[i]);
						 double length = f.length();
							double kb = length / 1024;
							double mb = kb / 1024;							
							if (mb >= 2) {
								cf.ShowToast(
										"File size exceeds!! Too large to attach",0);
							} else {
								
								try
								{
									/*boolean filecheck=false;
									arraylistfilepahlength=arraylistfilepah.size();
									System.out.println("array list file length "+arraylistfilepahlength);
									if(arraylistfilepahlength==0)
									{
										filecheck=true;
									}
									else
									{
										if(arraylistfilepah.contains(value[i]))
										{
											System.out.println("inside arraylist contains if");
											filecheck=false;
										}
										else
										{
											System.out.println("inside arraylist contains else");
											filecheck=true;
										}
									}
									
									System.out.println("filecheck value is "+filecheck);
									
									if(filecheck)
									{*/
									System.out.println("value="+value[i]);
										((EditText) findViewById(R.id.editreport)).setText(value[i]);
										((ImageView) findViewById(R.id.delete)).setVisibility(cf.v1.VISIBLE);
										((ImageView) findViewById(R.id.maininspdelete)).setVisibility(cf.v1.VISIBLE);
										
										/*if ((value[i].toLowerCase().endsWith(".jpg"))  ||(value[i].toLowerCase().endsWith(".jpeg")) ||(value[i].toLowerCase().endsWith(".png")) ||(value[i].toLowerCase().endsWith(".gif")))
										{
											Bitmap bm = decodeFile(value[i]);
											 if(bm==null)
											 {
													cf.ShowToast("File corrupted!! Cant able to attach",0);
											 }
											else
											{
												((EditText) findViewById(R.id.edtimg)).setText(value[i]);
											}	
										}
										else if (value[i].toLowerCase().endsWith(".pdf") || value[i].toLowerCase().endsWith(".xls") || value[i].toLowerCase().endsWith(".doc") ||  value[i].toLowerCase().endsWith(".docx"))
										{
											System.out
													.println("inside pdf");
											if (value[i].toLowerCase().endsWith(".pdf"))
											{
												((ImageView)(findViewById(requestCode).findViewWithTag("edimg"))).setImageDrawable(getResources().getDrawable(R.drawable.pdficon));
											}
											else if (value[i].toLowerCase().endsWith(".xls"))
											{
												System.out
												.println("inside xls");
												((ImageView)(findViewById(requestCode).findViewWithTag("edimg"))).setImageDrawable(getResources().getDrawable(R.drawable.xl));
											}
											else if (value[i].toLowerCase().endsWith(".doc") || value[i].toLowerCase().endsWith(".docx"))
											{
												((ImageView)(findViewById(requestCode).findViewWithTag("edimg"))).setImageDrawable(getResources().getDrawable(R.drawable.wd));
											}
											((EditText)(findViewById(requestCode).findViewWithTag("edbrowse"))).setText(value[i]);
										}*/
									/*}
									else
									{
										cf.ShowToast("File already selected, Please choose different one",0);
									}*/
							}
							catch (OutOfMemoryError e) {
								// TODO: handle exception
								System.out.println("Out of memory error "+e.getMessage());
								//cf.show_toast("File size exceeds!! Too large to attach",1);
							}
								
								
								}
				    }
				}
				else if(requestCode==127)
				{
					for(int i=0;i<value.length;i++)
				    {
						 File f = new File(value[i]);
						 double length = f.length();
							double kb = length / 1024;
							double mb = kb / 1024;							
							if (mb >= 2) {
								cf.ShowToast(
										"File size exceeds!! Too large to attach",0);
							} else {
								
								try
								{
								/*	
									boolean filecheck=false;
									arraylistfilepahlength=arraylistfilepah.size();
									System.out.println("array list file length "+arraylistfilepahlength);
									if(arraylistfilepahlength==0)
									{
										filecheck=true;
									}
									else
									{
										if(arraylistfilepah.contains(value[i]))
										{
											System.out.println("inside arraylist contains if");
											filecheck=false;
										}
										else
										{
											System.out.println("inside arraylist contains else");
											filecheck=true;
										}
									}
									System.out.println("filecheck value is "+filecheck);
									
									if(filecheck)
									{*/
										((EditText) findViewById(R.id.edtmaininsp)).setText(value[i]);
										((ImageView) findViewById(R.id.maininspdelete)).setVisibility(cf.v1.VISIBLE);
										((ImageView) findViewById(R.id.delete)).setVisibility(cf.v1.VISIBLE);
									/*}
									else
									{
										cf.ShowToast("File already selected, Please choose different one",0);
									}*/
							}
							catch (OutOfMemoryError e) {
								// TODO: handle exception
								System.out.println("Out of memory error "+e.getMessage());
							}
						}
				    }
				}
				else
				{				
					Bitmap bitmapdb=null;
					System.out.println("dsfffgdf"+upd_img);
					
					if(upd_img!=null)
					{
						try
						{
							
							if(value.length>0)
							{
								System.out.println("value"+value[0]);
								if(!value[0].equals(""))
								{
									
									updated_path=value[0];System.out.println("updated_path"+updated_path);
									 if(updated_path.endsWith(".pdf") || updated_path.endsWith(".doc") || updated_path.endsWith(".docx") || updated_path.endsWith(".xls") || updated_path.endsWith(".xlsx"))
									 {
										 if(updated_path.endsWith(".pdf"))
										 {
											 upd_img.setImageDrawable(getResources().getDrawable(R.drawable.pdficon));
										 }
										 else if(updated_path.endsWith(".doc") || updated_path.endsWith(".docx"))
										 {
											 upd_img.setImageDrawable(getResources().getDrawable(R.drawable.wd));
										 }
										 else if(updated_path.endsWith(".xls") || updated_path.endsWith(".xlsx"))
										 {
											 upd_img.setImageDrawable(getResources().getDrawable(R.drawable.xl));
										 }
								 
										 
										 
									 }
									 else
									 {
										 Bitmap b1=cf.ShrinkBitmap(updated_path, 400, 400);
										 System.out.println("b1="+b1);
											if(b1!=null)
											{
												System.out.println("updimag path");
												upd_img.setImageBitmap(b1);
											}
									 }
								}
							}
						
						}
						catch (Exception e) {
							System.out.println("ncide catch="+e.getMessage());
							// TODO: handle exception
						}
					}
					else
					{
					
								System.out.println("inside else");
								/*Cursor c1 = cf.arr_db.rawQuery("select * from " + cf.Custom_report , null);
								if(c1.getCount()>=0)
								{
									c1.moveToFirst();
									for(int j=0;j<c1.getCount();j++,c1.moveToNext())
									{
										String Path=c1.getString(c1.getColumnIndex("ReportImage"));
										if(!arraylistfilepah.contains(filePath))
										{
											arraylistfilepah.add(Path);
										}
									}
								}*/
								for(int i=0;i<value.length;i++)
							    {
									
									 System.out.println("vau="+value[i]);
									 
									 File f = new File(value[i]);
									 double length = f.length();
										double kb = length / 1024;
										double mb = kb / 1024;
										
										
										
										if (mb >= 2) {
											cf.ShowToast(
													"File size exceeds!! Too large to attach",0);
										} else {
											try
											{
												/*boolean filecheck=false;
												checkforimage_dup();
												arraylistfilepahlength=arraylistfilepah.size();
												System.out.println("array list file length second "+arraylistfilepahlength);
												if(arraylistfilepahlength==0)
												{
													filecheck=true;
												}
												else
												{
													
														System.out.println("TTTTTT="+arraylistfilepah+"conn"+value[i]);
													if(arraylistfilepah.contains(value[i]))
													{
														System.out.println("inside arraylist contains if");
														filecheck=false;
													}
													else
													{
														System.out.println("inside arraylist contains else");
														filecheck=true;
													}
												}												
												System.out.println("filecheck value is second="+filecheck);
												
												if(filecheck)
												{*/
													if ((value[i].toLowerCase().endsWith(".jpg"))  ||(value[i].toLowerCase().endsWith(".jpeg")) ||(value[i].toLowerCase().endsWith(".png")) ||(value[i].toLowerCase().endsWith(".gif")))
													{
														Bitmap bm = decodeFile(value[i]);
														System.out
																.println("bm="+bm);
														 if(bm==null)
														 {
																cf.ShowToast("File corrupted!! Cant able to attach",0);
														 }
														else
														{
															System.out.println("value="+value[i]+"requestCode"+requestCode);
															
															((ImageView)(findViewById(requestCode).findViewWithTag("edimg"))).setVisibility(cf.v1.GONE);
															((Button)(findViewById(requestCode).findViewWithTag("delimg"))).setVisibility(cf.v1.GONE);
															((Button)(findViewById(requestCode).findViewWithTag("clearimg"))).setVisibility(cf.v1.VISIBLE);
															
															 ((EditText)(findViewById(requestCode).findViewWithTag("edbrowse"))).setText(value[i]);
														}	
													}
													else if (value[i].toLowerCase().endsWith(".pdf") || value[i].toLowerCase().endsWith(".xls") || value[i].toLowerCase().endsWith(".doc") ||  value[i].toLowerCase().endsWith(".docx"))
													{
														
														/*if (value[i].toLowerCase().endsWith(".pdf"))
														{
															((ImageView)(findViewById(requestCode).findViewWithTag("edimg"))).setImageDrawable(getResources().getDrawable(R.drawable.pdficon));
														}
														else if (value[i].toLowerCase().endsWith(".xls"))
														{
															
															((ImageView)(findViewById(requestCode).findViewWithTag("edimg"))).setImageDrawable(getResources().getDrawable(R.drawable.xl));
														}
														else if (value[i].toLowerCase().endsWith(".doc") || value[i].toLowerCase().endsWith(".docx"))
														{
															((ImageView)(findViewById(requestCode).findViewWithTag("edimg"))).setImageDrawable(getResources().getDrawable(R.drawable.wd));
														}*/
														
														((ImageView)(findViewById(requestCode).findViewWithTag("edimg"))).setVisibility(cf.v1.GONE);
														((Button)(findViewById(requestCode).findViewWithTag("delimg"))).setVisibility(cf.v1.GONE);
														((Button)(findViewById(requestCode).findViewWithTag("clearimg"))).setVisibility(cf.v1.VISIBLE);														
														 ((EditText)(findViewById(requestCode).findViewWithTag("edbrowse"))).setText(value[i]);
													}
												/*}
												else
												{
													cf.ShowToast("File already selected, Please choose different one",0);
												}*/
										}
										catch (OutOfMemoryError e) {
											// TODO: handle exception
											System.out.println("Out of memory error "+e.getMessage());
										}
										}
							    }
					}
				}
			}
				 
		}
	  
}
