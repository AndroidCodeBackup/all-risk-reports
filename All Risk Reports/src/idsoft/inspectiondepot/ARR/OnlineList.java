package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.R.color;
import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView; 
import android.widget.TextView;
import android.widget.Toast;

public class OnlineList extends Activity
{
	int rws;
	ScrollView sv;String status,insptype,substatus;
	String strtit,res = "",inspdata,anytype = "anyType{}",sql,statusname;
	EditText search_text;
	LinearLayout onlinspectionlist;
	TextView tvstatus[];
	Button deletebtn[];
	public String[] data,countarr;
	CommonFunctions cf;
	CommonDeleteInsp del;
	ProgressDialog pd;
	String headernote="";

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			status = extras.getString("status");
			substatus = extras.getString("substatus");
			insptype = extras.getString("InspectionType");
			statusname = extras.getString("statusname");
		}
		setContentView(R.layout.online);
		
		cf = new CommonFunctions(this);
		del = new CommonDeleteInsp(this); 
		cf.getInspectorId();
		cf.CreateARRTable(3);
		onlinspectionlist = (LinearLayout) findViewById(R.id.linlayoutdyn);
		search_text = (EditText) findViewById(R.id.edtsearch);
		cf.CreateARRTable(66);
		TextView note  = (TextView) findViewById(R.id.note);
		
		if("Assign".equals(statusname))
		{
			headernote="POLICY HOLDER NAME | POLICY NUMBER | ADDRESS | CITY | STATE | COUNTY | ZIP | INSPECTION NAME";
		}
		else
		{
			headernote="POLICY HOLDER NAME | POLICY NUMBER | ADDRESS | CITY | STATE | COUNTY | ZIP | INSPECTION DATE | INSPECTION START TIME  - END TIME | INSPECTION NAME | Stories | Building Number";
		}
		
		((TextView) findViewById(R.id.status)).setText(statusname);
		((TextView) findViewById(R.id.inspectiontype)).setText(cf.strret);
		((TextView) findViewById(R.id.note)).setText(headernote);
		String source = "<font color=#FFFFFF>Loading data. Please wait..."	+ "</font>";
		pd = ProgressDialog.show(OnlineList.this,"", Html.fromHtml(source), true);
		 new Thread(new Runnable() {
                public void run() {
                	gethomeownerlist();
                	finishedHandler.sendEmptyMessage(0);
                }
            }).start();		 
		 ImageView im =(ImageView) findViewById(R.id.head_insp_info);
			im.setVisibility(View.VISIBLE);
			im.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					    Intent s2= new Intent(OnlineList.this,PolicyholdeInfoHead.class);
						Bundle b2 = new Bundle();
						s2.putExtra("homeid", cf.selectedhomeid);
						s2.putExtra("Type", "Inspector");
						s2.putExtra("insp_id", cf.Insp_id);
						((Activity) cf.con).startActivityForResult(s2, cf.info_requestcode);
				}
			});
	}
	private void gethomeownerlist()
	{
		SoapObject result = null;
		SoapObject request = new SoapObject(cf.NAMESPACE, "ONLINESYNCPOLICYINFO");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("InspectionStatus",status);
		request.addProperty("SubStatus",substatus);		
		//request.addProperty("InspectiontypeID",insptype);
		request.addProperty("inspectorid",cf.Insp_id);
		request.addProperty("compflag",insptype);
		
		System.out.println("REQUESTT "+request);
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL_ARR);
		
		try
		{
			androidHttpTransport.call(cf.NAMESPACE+"ONLINESYNCPOLICYINFO",envelope);
			result = (SoapObject) envelope.getResponse();
			System.out.println("result inside"+result);
			if (result.equals(anytype)) {
				cf.ShowToast("Server is busy. Please try again later",1);
			} else {
					InsertDate(result);
			}
		} catch (Exception e) {
			try {
				throw e;
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	private void InsertDate(SoapObject objInsert) throws NetworkErrorException,IOException, XmlPullParserException, SocketTimeoutException 
	{
		 cf.dropTable(66);	
		 cf.CreateARRTable(66);
	     int Cnt = objInsert.getPropertyCount();
	    
	     for (int i = 0; i < Cnt; i++) 
	     {
			SoapObject obj = (SoapObject) objInsert.getProperty(i);
			try {
					String  email, cperson,inspectionName, IsInspected = "0", insurancecompanyname, IsUploaded = "0", homeid, firstname, lastname, middlename, addr1, addr2, city, state, country, zip, homephone, cellphone, workphone, ownerpolicyno, companyid, inspectorid, wId, cId, inspectorfirstname, inspectorlastname, scheduleddate, yearbuilt, nstories, inspectionstarttime, inspectionendtime, inspectioncomment, inspectiontypeid;
					homeid = String.valueOf(obj.getProperty("SRID"));
					firstname = String.valueOf(obj.getProperty("FirstName"));
					lastname = String.valueOf(obj.getProperty("LastName"));
					addr1 = String.valueOf(obj.getProperty("Address1"));
					city = String.valueOf(obj.getProperty("City"));
					state = String.valueOf(obj.getProperty("State"));
					country = String.valueOf(obj.getProperty("Country"));
					zip = String.valueOf(obj.getProperty("Zip"));
					ownerpolicyno = String
							.valueOf(obj.getProperty("OwnerPolicyNo"));
					scheduleddate = String
							.valueOf(obj.getProperty("ScheduledDate"));
			    	inspectionstarttime = String.valueOf(obj
							.getProperty("InspectionStartTime"));
					inspectionendtime = String.valueOf(obj
							.getProperty("InspectionEndTime"));
				    inspectionName = String.valueOf(obj.getProperty("InspectionName")).trim();
				   cf.arr_db.execSQL("INSERT INTO "
							+ cf.OnlineTable
							+ " (s_SRID,InspectorId,Status,SubStatus,InspectionTypeId,s_OwnersNameFirst,s_OwnersNameLast,s_propertyAddress,s_City,s_State,s_County,s_ZipCode,s_OwnerPolicyNumber,ScheduleDate,InspectionStartTime,InspectionEndTime,InspectionName)"
							+ " VALUES ('" + homeid + "','"+ cf.Insp_id+"','"
							+ status+"','"
							+ substatus+"','"+insptype+"','"
							+ cf.encode(firstname) + "','"
							+ cf.encode(lastname) + "','"
							+ cf.encode(addr1) + "','"
							+ cf.encode(city) + "','"
							+ cf.encode(state) + "','"
							+ cf.encode(country) + "','" + zip
							+ "','" + cf.encode(ownerpolicyno) + "','"
							+ scheduleddate + "','" + inspectionstarttime + "','"
							+ inspectionendtime+"','"+inspectionName+"')");
					
				} 
			    catch (Exception e) 
			    {
					System.out.println("e=" + e.getMessage());
			    }
	     }
	  }
	private Handler finishedHandler = new Handler() {
	    @Override 
	    public void handleMessage(Message msg) {
	        pd.dismiss();
	         dbquery();
	    }
	};
	private void dbquery() {
		data = null;		
		countarr = null;
		rws = 0;
		sql = "select * from " + cf.OnlineTable;
		if (!res.equals("")) {

			sql += " where (s_OwnersNameFirst like '%" + res
					+ "%' or s_OwnersNameLast like '%" + res
					+ "%' or s_OwnerPolicyNumber like '%" + res
					+ "%') and InspectorId='" + cf.Insp_id + "'";
			if (status.equals("110")&& substatus.equals("111")) {
				sql += " and  Status=110 and SubStatus=111";
			} else if (status.equals("90")&& substatus.equals("0")) {
				sql += " and Status=90 and SubStatus=0";
			} else if (status.equals("40")&& substatus.equals("41")) {
				sql += " and Status=40 and SubStatus='41'";
			}
			 else if (status.equals("40")&& substatus.equals("0")) {
					sql += " and Status=40 and SubStatus='0'";
				}
			 else if (status.equals("30")&& substatus.equals("0")) {
					sql += " and Status=30 and SubStatus='0'";
				}
			 else if (statusname.equals("RR")) {
					sql += " and ((Status='2' and SubStatus='0') or (Status='5' and SubStatus='0') or (Status='2' and SubStatus='121') or (Status='2' and SubStatus='122') or (Status='140'))";
				}

		} else {
			if (status.equals("110")&& substatus.equals("111")) {
				sql += " where Status=110 and SubStatus=111 and InspectorId='" + cf.Insp_id + "'";
			} else if (status.equals("90")&& substatus.equals("0")) {
				sql += " where Status=90  and SubStatus=0   and InspectorId='" + cf.Insp_id + "'";
			}  else if (status.equals("40")&& substatus.equals("41")) {
				sql += " where Status=40 and SubStatus='41' and InspectorId='" + cf.Insp_id + "'";
			}
			 else if (status.equals("40")&& substatus.equals("0")) {
					sql += " where Status=40 and SubStatus='0' and InspectorId='" + cf.Insp_id + "'";
				}
			 else if (status.equals("30")&& substatus.equals("0")) {
					sql += " where Status=30 and SubStatus='0' and  InspectorId='" + cf.Insp_id + "'";
				}
			 else if (statusname.equals("RR")) {
					sql += " where ((Status='2' and SubStatus='0') or (Status='5' and SubStatus='0') or (Status='2' and SubStatus='121') or (Status='2' and SubStatus='122') or (Status='140')) and  InspectorId='" + cf.Insp_id + "'";
				}
		}
		System.out.println("sql "+sql);
		Cursor cur = cf.arr_db.rawQuery(sql, null);
		rws = cur.getCount();
		countarr= new String[rws];
		data = new String[rws];
		int j = 0;
		cur.moveToFirst();
		if (cur.getCount() >0) {
			((TextView) findViewById(R.id.noofrecords)).setText("No of Records : "+rws);
			((LinearLayout) findViewById(R.id.dynamiclayout)).setVisibility(cf.v1.VISIBLE);
			((TextView) findViewById(R.id.norecordstxt)).setVisibility(cf.v1.GONE);
			do {
				
				String s =(cf.decode(cur.getString(cur.getColumnIndex("s_OwnersNameFirst"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("s_OwnersNameFirst")));
				data[j] =  s+ " ";
				s=(cf.decode(cur.getString(cur.getColumnIndex("s_OwnersNameLast"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("s_OwnersNameLast")));
				this.data[j] += s + " | ";
				s=(cf.decode(cur.getString(cur.getColumnIndex("s_OwnerPolicyNumber"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("s_OwnerPolicyNumber")));
				this.data[j] += s+ " | ";
				s=(cf.decode(cur.getString(cur.getColumnIndex("s_propertyAddress"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("s_propertyAddress")));
				this.data[j]+= s + " | ";
				s=(cf.decode(cur.getString(cur.getColumnIndex("s_City"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("s_City")));
				this.data[j] += s + " | ";
				s=(cf.decode(cur.getString(cur.getColumnIndex("s_State"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("s_State")));
				this.data[j] += s + " | ";
				s=(cf.decode(cur.getString(cur.getColumnIndex("s_County"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("s_County")));
				this.data[j] += s + " | ";
				s=(cf.decode(cur.getString(cur.getColumnIndex("s_ZipCode"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("s_ZipCode")));
				this.data[j] += s+ " | ";
				if(!statusname.equals("Assign")) {
					s=(cf.decode(cur.getString(cur.getColumnIndex("ScheduleDate"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("ScheduleDate")));
					this.data[j] += s+ " | ";
					s=(cf.decode(cur.getString(cur.getColumnIndex("InspectionStartTime"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("InspectionStartTime")));
					this.data[j] += s+ " - ";
					s=(cf.decode(cur.getString(cur.getColumnIndex("InspectionEndTime"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("InspectionEndTime")));
					this.data[j] += s+ " | ";
				}
				s=(cf.decode(cur.getString(cur.getColumnIndex("InspectionName"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("InspectionName"))).trim();
				this.data[j] += s +" | ";
				
				s=(cf.decode(cur.getString(cur.getColumnIndex("InspectionName"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("InspectionName"))).trim();
				this.data[j] += s+" | ";
				
				s=(cf.decode(cur.getString(cur.getColumnIndex("InspectionName"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("InspectionName"))).trim();
				this.data[j] += s;
					
				countarr[j]=cur.getString(cur.getColumnIndex("s_SRID"));
				j++;
				
			} while (cur.moveToNext());
			search_text.setText("");
			display();
			cf.hidekeyboard(search_text);
		} else {
			cf.ShowToast("Sorry, No results found for your search criteria.",1);
			onlinspectionlist.removeAllViews();
			((LinearLayout) findViewById(R.id.dynamiclayout)).setVisibility(cf.v1.GONE);
			((TextView) findViewById(R.id.norecordstxt)).setVisibility(cf.v1.VISIBLE);
			cf.hidekeyboard(search_text);
		}

	}
	
	private void display() {
		
		onlinspectionlist.removeAllViews();
		sv = new ScrollView(this);
		onlinspectionlist.addView(sv);

		final LinearLayout l1 = new LinearLayout(this);
		l1.setOrientation(LinearLayout.VERTICAL);
		sv.addView(l1);
		
		View v = new View(this);
		v.setBackgroundResource(R.color.black);
		l1.addView(v,LayoutParams.FILL_PARENT,1);
		if (data.length>=1)
		{				
			for (int i = 0; i < data.length; i++) 
			{		
				tvstatus = new TextView[rws];
				deletebtn = new Button[rws];
				LinearLayout l2 = new LinearLayout(this);
				l2.setOrientation(LinearLayout.HORIZONTAL);
				l1.addView(l2);
	
				LinearLayout lchkbox = new LinearLayout(this);
				LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(900, ViewGroup.LayoutParams.WRAP_CONTENT);
				paramschk.topMargin = 8;
				paramschk.leftMargin = 20;
				paramschk.bottomMargin = 10;
				l2.addView(lchkbox);
	
				tvstatus[i] = new TextView(this);
				tvstatus[i].setTag("textbtn" + i);
				tvstatus[i].setText(data[i]);
				tvstatus[i].setTextColor(Color.WHITE);
				tvstatus[i].setTextSize(TypedValue.COMPLEX_UNIT_PX,14);
				lchkbox.addView(tvstatus[i], paramschk);
				
				
				 if (i % 2 == 0) {
					 l2.setBackgroundColor(Color.parseColor("#13456d"));
				    	} else {
				    	l2.setBackgroundColor(Color.parseColor("#386588"));
				    	}
				 if(data.length!=(i+1))
					{
						 View v1 = new View(this);
						 v1.setBackgroundResource(R.color.white);
						 l1.addView(v1,LayoutParams.FILL_PARENT,1);	 
					}
			}
			View v2 = new View(this);
			v2.setBackgroundResource(R.color.black);
			onlinspectionlist.addView(v2,LayoutParams.FILL_PARENT,1);
		}
		else
		{
		
		}
	}
	public void clicker(View v) {
		switch (v.getId()) {
	
		case R.id.home:
			cf.gohome();
			break;
		case R.id.search:
			String temp = cf.encode(search_text.getText().toString().trim());
			if (temp.equals("")) {
				cf.ShowToast("Please enter the Name or Policy Number to search.", 1);
				search_text.requestFocus();
			} 
			else
			{
				res = temp;
				dbquery();
			}
			break;
		case R.id.clear:
			search_text.setText("");
			res = "";
			dbquery();
			cf.hidekeyboard();
			break;
				
		}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			startActivity(new Intent(OnlineList.this,Dashboard.class));
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}