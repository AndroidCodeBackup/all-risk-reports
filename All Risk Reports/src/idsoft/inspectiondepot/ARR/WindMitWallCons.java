/*
************************************************************
* Project: ALL RISK REPORT
* Module : WindMitWallCons.java
* Creation History:
Created By : Nalini on 10/24/2012
* Modification History:
Last Modified By : Gowri on 11/28/2012
************************************************************ 
*/
package idsoft.inspectiondepot.ARR;


import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.BIWallstructure.multi;
import idsoft.inspectiondepot.ARR.MultiSpinner.MultiSpinnerListener;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
public class WindMitWallCons extends Activity {
	String[] arrwallid,Wall_Split=new String[10],arrelevothertxt,percentageval,storiesval,otherwall,spiltstorarr=new String[15],splitpercarr,elevation,percentage,arrinsptypeid,percarr,storiesarr,storarr;
	String elevspinnertxt="",wallstru="",floorchoosen="",s="",spn1="",spn2="",spn3="",spn4="",spn5="",spn6="",spn7="",elev="",strstories="",elevtype="",
			woodframeval = "0", reinmasonryval = "0", unreinmasonryval = "0",updatecnt="",sel_inspection,strdisplay="",reinforccement_val="",
			pouredconcreteval = "0", otherwallval = "0",concreteblockval="0",solidconcreteval="0",reinforce_value="",s1="";
	LinearLayout subpnlshw,headingelev;TableLayout myTable;
	Dialog dialog1;
	String wallconstvalue[] = new String[10];
	int m=0;
	Spinner spnelevation;TextView tv[] = new TextView[101];
	TableLayout dynviewelevation;
	CommonFunctions cf;	
	public CheckBox reinforcechk[] = new CheckBox[5];
	public int[] reinforceid = {R.id.reinfor_chk1,R.id.reinfor_chk2,R.id.reinfor_chk3,R.id.reinfor_chk4,R.id.reinfor_chk5};	
	boolean load_comment=true;
	String  wcvalue1[]=new String[12];
	int i1=0,i2=0;
	public TextView tvwstrudetails[] = new TextView[7];
	public int[] wallstrudetailsid = {R.id.tvwsdetails1,R.id.tvwsdetails2,R.id.tvwsdetails3,R.id.tvwsdetails4,R.id.tvwsdetails5,R.id.tvwsdetails6,R.id.tvwsdetails7};
		
	public TextView dummytext[] = new TextView[7];
	public int[] tvid = {R.id.tv1,R.id.tv2,R.id.tv3,R.id.tv4,R.id.tv5,R.id.tv6,R.id.tv7};
		
	public TextView tvstoriesdisplay[] = new TextView[7];
	public int[] tvstoriesid = {R.id.tvstor1,R.id.tvstor2,R.id.tvstor3,R.id.tvstor4,R.id.tvstor5,R.id.tvstor6,R.id.tvstor7};	
	
	public MultiSpinner spnnoofstor[] = new MultiSpinner[7];	
	public int[] spnstoriesid = {R.id.spnstories1,R.id.spnstories2,R.id.spnstories3,R.id.spnstories4,R.id.spnstories5,R.id.spnstories6,R.id.spnstories7};
	
	public Spinner spnpercent[] = new Spinner[7];
	public int[] spnpercid = {R.id.spnperc1,R.id.spnperc2,R.id.spnperc3,R.id.spnperc4,R.id.spnperc5,R.id.spnperc6,R.id.spnperc7};
	
	public TextView tvwstruothertxt[] = new TextView[7];
	public int[] wallstruothersid = {R.id.tvwsothertxt1,R.id.tvwsothertxt2,R.id.tvwsothertxt3,R.id.tvwsothertxt4,R.id.tvwsothertxt5,R.id.tvwsothertxt6,R.id.tvwsothertxt7};

	
	TableRow tr;
	ArrayAdapter spnAadap,spnBadap;
	TextView[] txtconcblockunrein,txtsolidconc,txtwoodmetalframe,txtunreinmas,txtreinmas,txtpouredconc,txtother,txtelevation;
	Button[] editimg,delimg;
	private List<String> items= new ArrayList<String>();
	String spnwallstruc[] = new String[9];
	int identity=0,elevid,rwselev,per1 = 0,per2 = 0,per3 = 0,per4 = 0,per5 = 0,per6 = 0,per7 = 0,stories1=0,stories2=0,stories3=0,stories4=0,stories5=0,stories6=0,stories7=0,perctotal=0,storiestot=0;
	EditText edwccomments;
	String spnelevtype[] = { "--Select--", "Front Elevation","Right Elevation","Left Elevation","Back Elevation"};
	public TextView tvwctitle[] = new TextView[7];
	public int[] wallcladdingtitleid = {R.id.txtwc1,R.id.txtwc2,R.id.txtwc3,R.id.txtwc4,R.id.txtwc5,R.id.txtwc6,R.id.txtwc7};
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cf=new CommonFunctions(this);  
        Bundle extras = getIntent().getExtras();
		if (extras != null) {
			cf.getExtras(extras);				
	 	}
		setContentView(R.layout.windmitwallconstruction);
        cf.getDeviceDimensions();
        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
        if(cf.identityval==31)
        {
          hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","B1 1802(Rev.01/12)","Wall Construction",3,1,cf));
        }
        else if(cf.identityval==32)
        { 
        	hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","Commercial Type I","Wall Construction",3,1,cf));
        }
		LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
     	main_layout.setMinimumWidth(cf.wd);
     	main_layout.addView(new MyOnclickListener(getApplicationContext(), 3, 0,0,cf));
       
        cf.type_submenulayout =(LinearLayout) findViewById(R.id.typesubmenu); //** WINDMIT TYPE SUBMENU **//
        cf.type_submenulayout.addView(new MyOnclickListener(this, 300, 1,0, cf));
        cf.windmittypechk(cf.identityval,((RadioButton)findViewById(R.id.b1802_chk)),((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((TextView)findViewById(R.id.commtxt)),((ImageView)findViewById(R.id.editbno))); // To check the checkbox of choosen inspection type
        cf.uncheckcommercial(((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((RadioButton)findViewById(R.id.b1802_chk))); 
    	          
        cf.submenu_layout = (LinearLayout) findViewById(R.id.submenu);//** WINDMIT TYPE SUBMENU **//	        
        cf.submenu_layout .addView(new MyOnclickListener(this,cf.identityval,1,308, cf));
        
        cf.mainlayout = (LinearLayout)findViewById(R.id.bc_table);
        cf.save = (Button)findViewById(R.id.save);
        cf.tblrw = (TableRow)findViewById(R.id.row2);
        cf.tblrw.setMinimumHeight(cf.ht);		
        cf.CreateARRTable(3);
		cf.CreateARRTable(31);
		cf.CreateARRTable(32);
		cf.CreateARRTable(34);
		cf.getInspectorId();
		cf.getPolicyholderInformation(cf.selectedhomeid);
		Declarations();
		cf.CreateARRTable(31);cf.CreateARRTable(34);
		//Wallconstories();
		getSavedelev_value();
		WallConctrcution_setValue();
    }
	private void Wallconstories()
	{
		try
		{
			 Cursor c1=cf.SelectTablefunction(cf.WindMit_WallCons, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
			 int rwselev1 = c1.getCount(); System.out.println("CCCC"+c1.getCount());
			 String elevation1[] = new String[rwselev1];
			 String percentage1[] = new String[rwselev1];
			 String storarr1[] = new String[rwselev1];
			 String arrwallid1[]= new String[rwselev1];
			 String arrinsptypeid1[]= new String[rwselev1];
			 
				 if(c1.getCount()>0)
				 {
					 c1.moveToFirst();				
					 int i=0;
						 do {
							 	arrwallid1[i] = c1.getString(c1.getColumnIndex("WCID"));
							 	arrinsptypeid1[i]= c1.getString(c1.getColumnIndex("insp_typeid"));
							 	elevation1[i]= c1.getString(c1.getColumnIndex("fld_Elevation"));		
							 	percentage1[i]= c1.getString(c1.getColumnIndex("fld_Percentage"));	
							 	storarr1[i]= c1.getString(c1.getColumnIndex("fld_Stories"));
								String Stories_Split[] = storarr1[i].split("~");System.out.println("Stories_Split"+c1.getCount());
								for(int k=0;k<Stories_Split.length;k++)
							   	{
									System.out.println("Stories_Split"+Stories_Split.length);
							   		if(!Stories_Split[k].trim().equals(""))
							     	{
							   			System.out.println("Stories="+Stories_Split[k]);
							   			String storiessplit[] = Stories_Split[k].split(",");
							       		for(int j=0;j<storiessplit.length;j++)
							       		{
							       			System.out.println("storiessplit="+storiessplit.length);
							       			if(!items.contains(storiessplit[j].trim()))
							       			{
							       				if(m==0)
							   	    			{
							       					wallconstvalue[k] = Stories_Split[k].replace(storiessplit[j],"");m++;
							   	    			}
							   	    			else
							   	    			{
							   	    				wallconstvalue[k] = wallconstvalue[k].replace(storiessplit[j],"");
							   	    			}
							       			}
							       			else
							       			{
							       				if(m==0){
							       					if(wallconstvalue[k]==null) {wallconstvalue[k]="";
							   						wallconstvalue[k] =Stories_Split[k];
							   						}
							   						else{wallconstvalue[k] =Stories_Split[k];}
							   					}
							       			}       				
							       		}m=0;
							       	}
							   		else
							       	{
							   			wallconstvalue[k]="";
							       	}	  
							   		
							   			if(!wallconstvalue[k].equals(""))
							       		{	   				
							       			if(wallconstvalue[k].contains(",,"))
							   	    		{
							       				wallconstvalue[k]=wallconstvalue[k].replace(",,", "");
							   	    		}
							       			if(!wallconstvalue[k].equals(""))
							       			{
							       				String lastChar = wallconstvalue[k].substring(wallconstvalue[k].length()-1);
							       				if (lastChar.contains(",")) {
							       					wallconstvalue[k] = wallconstvalue[k].substring(0,wallconstvalue[k].length()-1);
							       				}
							       			}
							       		}
							   			System.out.println("testtesttest"+wallconstvalue[i]);
							   			spnnoofstor[k].setItems1(items,"--Select--",new multi(),wallconstvalue[k]);	    	
								    	
							   			if(wcvalue1[i]==null){
							   				wcvalue1[i]="";
							   				wcvalue1[i] +=wallconstvalue[k]+"~";
							   			}
							   			else
							   			{
							   				wcvalue1[i] +=wallconstvalue[k]+"~";
							   			}
							   			System.out.println("RD="+wcvalue1[i]);
							   		}
								System.out.println("RDRED="+wcvalue1[i]);
								if(wcvalue1[i].substring(wcvalue1[i].lastIndexOf("~")).equals("~"))
						   		{
					   				wcvalue1[i]= wcvalue1[i].substring(0,wcvalue1[i].lastIndexOf("~"));
						   		}	  
								System.out.println("wcvalue="+wcvalue1[i]);
					   			cf.arr_db.execSQL("UPDATE "
											+ cf.WindMit_WallCons
											+ " SET fld_Stories='" + wcvalue1[i] + "'"
											+ " WHERE fld_srid ='"
											+ cf.selectedhomeid + "' and fld_Elevation='"+elevation1[i]+"' and insp_typeid='"+arrinsptypeid1[i]+"' and WCID='"+arrwallid1[i]+"'");
					   			
					   			System.out.println("UPDATE "
										+ cf.WindMit_WallCons
										+ " SET fld_Stories='" + wcvalue1[i] + "'"
										+ " WHERE fld_srid ='"
										+ cf.selectedhomeid + "' and fld_Elevation='"+elevation1[i]+"' and insp_typeid='"+arrinsptypeid1[i]+"' and WCID='"+arrwallid1[i]+"'");
					   			i++;
							} while (c1.moveToNext());						
				 }
				 else
				 {
					
				 }
			 }
			 catch(Exception e)
			 {
				 System.out.println("wc erro "+e.getMessage());
			 }
		
	}

	private void getSavedelev_value() {
		// TODO Auto-generated method stub
		try
		{
		 Cursor c1=cf.SelectTablefunction(cf.WindMit_WallCons, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
		 System.out.println("how many rws "+c1.getCount());
		 rwselev = c1.getCount();
		 elevation = new String[rwselev];
		 percentage = new String[rwselev];
		 storarr = new String[rwselev];
		 otherwall = new String[rwselev];
		 arrwallid= new String[rwselev];
		 arrelevothertxt=new String[rwselev];
			 if(c1.getCount()>0)
			 {
				 c1.moveToFirst();				
				 int i=0;
					 do {
						 	arrwallid[i] = c1.getString(c1.getColumnIndex("WCID"));
						 	elevation[i]= c1.getString(c1.getColumnIndex("fld_Elevation"));		
						 	percentage[i]= c1.getString(c1.getColumnIndex("fld_Percentage"));	
						 	storarr[i]= c1.getString(c1.getColumnIndex("fld_Stories"));System.out.println("STORIES="+storarr[i]);
						 	otherwall[i]= cf.decode(c1.getString(c1.getColumnIndex("fld_WallconsOther")));			
						 	arrelevothertxt[i] = cf.decode(c1.getString(c1.getColumnIndex("fld_elevation_othertxt")));
							i++;
						} while (c1.moveToNext());
					 subpnlshw.setVisibility(cf.v1.VISIBLE);headingelev.setVisibility(cf.v1.VISIBLE);
					
					((TableLayout)findViewById(R.id.tableLayoutNote)).setVisibility(cf.v1.VISIBLE);
					 ((TextView)findViewById(R.id.txtbriefexplanation)).setText(Html.fromHtml("CF."+cf.redcolor+"  - Chosen Floors" + "<br/>"
										+ "W/MF "+cf.redcolor+"  - Wood/Light Metal Frame " + "<br/>"));
					 show_wcelev_Value();
			 }
			 else
			 {
				 subpnlshw.setVisibility(cf.v1.GONE);headingelev.setVisibility(cf.v1.GONE);
				 ((TableLayout)findViewById(R.id.tableLayoutNote)).setVisibility(cf.v1.GONE);
			 }
		 }
		 catch(Exception e)
		 {
			 System.out.println("wc erro "+e.getMessage());
		 }
	}
	private void WallConctrcution_setValue() {
		// TODO Auto-generated method stub
		try
		{
				 Cursor c2=cf.SelectTablefunction(cf.Wind_Questiontbl, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
				 if(c2.getCount()>0)
				 {
					 c2.moveToFirst();	
					 String reinforccement_val = cf.decode(c2.getString(c2.getColumnIndex("fld_wcreinforcement")));
					
					 if(!reinforccement_val.equals(""))
					 {
						 if(!reinforccement_val.equals("0"))
						 {
							 cf.setvaluechk1(reinforccement_val,reinforcechk,((EditText)findViewById(R.id.edreinforceother)));
						 }
					 }
				 }
				 
				 Cursor c12=cf.SelectTablefunction(cf.Wind_QuesCommtbl, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
				 if(c12.getCount()>0)
				 {	
					 c12.moveToFirst();
					  edwccomments.setText(cf.decode(c12.getString(c12.getColumnIndex("fld_wallconscomments"))));
				 } 
		 }
		 catch (Exception E)
		 {
		    	String strerrorlog="Retrieving Wall construction - WINDMIT";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		 }
	}
	private void show_wcelev_Value() {
		// TODO Auto-generated method stub
		dynviewelevation.removeAllViews();
		LinearLayout h1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvac_listview_header, null); 
		LinearLayout th= (LinearLayout)h1.findViewById(R.id.wallconstruction_hdr);th.setVisibility(cf.v1.VISIBLE);
		TableLayout.LayoutParams lp = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
	 	lp.setMargins(2, 0, 2, 2); h1.removeAllViews();
	 	th.setPadding(10, 0, 0, 0);	 	
	 	dynviewelevation.addView(th,lp);
		dynviewelevation.setVisibility(View.VISIBLE); 
		for (int i = 0; i < rwselev; i++) 
		{	System.out.println("rwselev"+rwselev);
				TextView number,elev,txtconcblock,txtsolidconc,txtwoodlightmetal,txtunrein,txtrein,txtpourconc,txtother;
				ImageView edit,delete;
				splitpercarr = percentage[i].split("~");
				spiltstorarr = storarr[i].split("\\~",-1);System.out.println("spiltstorarr="+storarr[i]);
				System.out.println("rwselev"+rwselev);
				LinearLayout t1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvac_listview, null);
				LinearLayout t = (LinearLayout)t1.findViewById(R.id.wallcons);t.setVisibility(cf.v1.VISIBLE);
				t.setId(44444+i);/// Set some id for further use
			 	
				number= (TextView) t.findViewWithTag("#");	
				elev= (TextView) t.findViewWithTag("WC_Elev");			
			 	txtconcblock= (TextView) t.findViewWithTag("WC_ConcUnRein"); 			
			 	txtsolidconc= (TextView) t.findViewWithTag("WC_SoldConc");   			txtconcblock.setMovementMethod(LinkMovementMethod.getInstance());
			 	txtwoodlightmetal= (TextView) t.findViewWithTag("WC_WoodMetalFrame");	txtsolidconc.setMovementMethod(LinkMovementMethod.getInstance());
			 	txtunrein= (TextView) t.findViewWithTag("WC_UnRein");					txtwoodlightmetal.setMovementMethod(LinkMovementMethod.getInstance());
			 	txtrein= (TextView) t.findViewWithTag("WC_Rein");						txtunrein.setMovementMethod(LinkMovementMethod.getInstance());
			 	txtpourconc= (TextView) t.findViewWithTag("WC_PourConc");				txtrein.setMovementMethod(LinkMovementMethod.getInstance());
			 	txtother= (TextView) t.findViewWithTag("WC_Other");						txtpourconc.setMovementMethod(LinkMovementMethod.getInstance());
			 	txtother.setMovementMethod(LinkMovementMethod.getInstance());
			 	number.setText(arrwallid[i]);
			 	if(arrelevothertxt[i].equals(""))
				{
			 		elev.setText(elevation[i]);
				}
				else
				{
					elev.setText("Other Elevation("+arrelevothertxt[i]+")");	
				}
			 		

			 	System.out.println("111"+spiltstorarr[0]);
			 	System.out.println("222"+spiltstorarr[1]);
			 	System.out.println("333"+spiltstorarr[2]);
			 	System.out.println("444"+spiltstorarr[3]);
			 	System.out.println("555"+spiltstorarr[4]);
			 	System.out.println("666"+spiltstorarr[5]);
			 	System.out.println("777"+spiltstorarr[6]);
			 	
			 	if(!spiltstorarr[0].trim().equals("")){
			 		txtconcblock.setText(Html.fromHtml("<font color='#000000'>"+ splitpercarr[0]+"%</font>"+" - "+"<font color='#0000FF'><u>Click here</u></font>"));	
			 		txtconcblock.setTag("0~"+spiltstorarr[0]);
			 		commonspan(txtconcblock);			 		
			 	}
			 	else
			 	{
					txtconcblock.setText(Html.fromHtml("<font color='#000000'>"+ splitpercarr[0]+"%</font>"));	
				}
			 	
			 	if(!spiltstorarr[1].trim().equals("")){
			 		txtsolidconc.setText(Html.fromHtml("<font color='#000000'>"+ splitpercarr[1]+"%</font>"+" - "+"<font color='#0000FF'><u>Click here</u></font>"));	
			 		txtsolidconc.setTag("1~"+spiltstorarr[1]);
			 		commonspan(txtsolidconc);
			 	}
			 	else
			 	{
			 		txtsolidconc.setText(Html.fromHtml("<font color='#000000'>"+ splitpercarr[1]+"%</font>"));	
				}
			 	
			 	if(!spiltstorarr[2].trim().equals("")){
			 		txtwoodlightmetal.setText(Html.fromHtml("<font color='#000000'>"+ splitpercarr[2]+"%</font>"+" - "+"<font color='#0000FF'><u>Click here</u></font>"));	
			 		txtwoodlightmetal.setTag("2~"+spiltstorarr[2]);
			 		commonspan(txtwoodlightmetal);
			 	}
			 	else
			 	{
			 		txtwoodlightmetal.setText(Html.fromHtml("<font color='#000000'>"+ splitpercarr[2]+"%</font>"));	
				}
			 	
			 	if(!spiltstorarr[3].trim().equals("")){
			 		txtunrein.setText(Html.fromHtml("<font color='#000000'>"+ splitpercarr[3]+"</font>%"+" - "+"<font color='#0000FF'><u>Click here</u></font>"));	
			 		txtunrein.setTag("3~"+spiltstorarr[3]);		 	
			 		commonspan(txtunrein);	 
			 	}
			 	else
			 	{
			 		txtunrein.setText(Html.fromHtml("<font color='#000000'>"+ splitpercarr[3]+"%</font>"));	
				}
				
			 	if(!spiltstorarr[4].trim().equals("")){
			 		txtrein.setText(Html.fromHtml("<font color='#000000'>"+ splitpercarr[4]+"</font>%"+" - "+"<font color='#0000FF'><u>Click here</u></font>"));	
			 		txtrein.setTag("4~"+spiltstorarr[4]);
			 		commonspan(txtrein);
			 	}
			 	else
			 	{
			 		txtrein.setText(Html.fromHtml("<font color='#000000'>"+ splitpercarr[4]+"</font>%"));	
				}
			 	
			 	if(!spiltstorarr[5].trim().equals("")){
			 		txtpourconc.setText(Html.fromHtml("<font color='#000000'>"+ splitpercarr[5]+"</font>%"+" - "+"<font color='#0000FF'><u>Click here</u></font>"));	
			 		txtpourconc.setTag("5~"+spiltstorarr[5]);
			 		commonspan(txtpourconc);
			 	}
			 	else
			 	{
			 		txtpourconc.setText(Html.fromHtml("<font color='#000000'>"+ splitpercarr[5]+"</font>%"));	
				}
			 	
			 	if(!spiltstorarr[6].trim().equals(""))
			 	{
			 		if(!otherwall[i].equals(""))
			 		{
			 			txtother.setText(Html.fromHtml("<font color='#000000'>"+ splitpercarr[6]+"%("+otherwall[i]+")"+"</font>"+" - "+"<font color='#0000FF'><u>Click here</u></font>"));	
					}
			 		else
			 		{
			 			txtother.setText(Html.fromHtml("<font color='#000000'>"+ splitpercarr[6]+"</font>%"+" - "+"<font color='#0000FF'><u>Click here</u></font>"));
			 		}
			 		
			 		txtother.setTag("6~"+spiltstorarr[6]);
				 	commonspan(txtother);
				 		
			 	}
			 	else
			 	{
			 		if(!otherwall[i].equals(""))
		 			{
			 			txtother.setText(Html.fromHtml("<font color='#000000'>"+ splitpercarr[6]+"%("+otherwall[i]+")"+"</font>"));
		 			}
			 		else
			 		{
			 			txtother.setText(Html.fromHtml("<font color='#000000'>"+ splitpercarr[6]+"</font>%"));
			 		}	
				}

			 	
			 	edit= (ImageView) t.findViewWithTag("WC_edit");	
			 	edit.setTag(arrwallid[i]);
			 	edit.setOnClickListener(new WC_EditClick(1,arrwallid[i]));
			 	delete= (ImageView) t.findViewWithTag("WC_delete");
			 	delete.setTag(arrwallid[i]);
			 	delete.setOnClickListener(new WC_EditClick(2,arrwallid[i]));
			 	t1.removeAllViews();
				t.setPadding(10, 0, 0, 0);
				dynviewelevation.addView(t,lp);
			}			 	
	}
	private void commonspan(TextView tv1) {
		// TODO Auto-generated method stub	
		
		 int i1 = tv1.getText().toString().length()-10; 
		 int i2 = tv1.getText().toString().length()-1;  
		Spannable mySpannable = (Spannable)tv1.getText();
		ClickableSpan myClickableSpan = new ClickableSpan()
		{
		  @Override
		  public void onClick(View widget) 
		  {
			  String storarr[] = widget.getTag().toString().split("~");
			  showalert(1,Integer.parseInt(storarr[0]),storarr[1]);
		  }
		 };
		 mySpannable.setSpan(myClickableSpan, i1, i2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
	}
	class WC_EditClick implements OnClickListener
	{
		int type,edit; 
		WC_EditClick(int edit,String id1)
		{
			this.edit=edit;
			this.type=Integer.parseInt(id1);
		}
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if(edit==1)
			{	
				identity=1;
				((Button)findViewById(R.id.elevsave)).setText("Update");
				for(int i=0;i<spnnoofstor.length;i++)
				{
					spnnoofstor[i].setItems1(items,"--Select--",new multi(),"");
					tvwstrudetails[i].setVisibility(cf.v1.GONE);
					tvwstruothertxt[i].setVisibility(cf.v1.GONE);
					dummytext[i].setText("");
				}		
				updateWC(type);
			}
			else
			{
				AlertDialog.Builder builder = new AlertDialog.Builder(WindMitWallCons.this);
						builder.setMessage("Are you sure, Do you want to delete the selected Elevation?")
								.setCancelable(false)
								.setPositiveButton("Yes",
										new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog, int id) {												
												try
												{
													cf.arr_db.execSQL("Delete from "+cf.WindMit_WallCons+" Where WCID='"+type+"'");
													cf.ShowToast("Deleted Successfully.", 0);
													getSavedelev_value();
													clearelevdatas();
												}
												catch (Exception e) {
													// TODO: handle exception
												}
											}
										})
								.setNegativeButton("No",
										new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog,int id) {
												dialog.cancel();
											}
										});
						builder.setTitle("Confirmation");
						builder.setIcon(R.drawable.alertmsg);
						builder.show();											
			}
		}
	}
	private void updateWC(int elevation) {
		// TODO Auto-generated method stub
		elevid=elevation;String floorchoosen="";
		try
		{
		Cursor c2=cf.SelectTablefunction(cf.WindMit_WallCons, " WHERE WCID='"+elevid+"' and insp_typeid='"+cf.identityval+"' and fld_srid='"+cf.selectedhomeid+"'");
		if(c2.getCount()>0)
		{
			c2.moveToFirst();
			String elev_value = c2.getString(c2.getColumnIndex("fld_Elevation"));
			String perc_value = c2.getString(c2.getColumnIndex("fld_Percentage"));	
			String stories_value = c2.getString(c2.getColumnIndex("fld_Stories"));
			String wallother =  cf.decode(c2.getString(c2.getColumnIndex("fld_WallconsOther")));
			
			int spinnerPosition1 = spnBadap.getPosition(elev_value);
			spnelevation.setSelection(spinnerPosition1);
			spnelevation.setEnabled(false);				
			
			if(elev_value.equals("Other Elevation"))
			{
				((EditText)findViewById(R.id.edtelevother)).setVisibility(cf.v1.VISIBLE);
				((EditText)findViewById(R.id.edtelevother)).setText(cf.decode(c2.getString(c2.getColumnIndex("fld_elevation_othertxt"))));
			}
			else
			{
				((EditText)findViewById(R.id.edtelevother)).setVisibility(cf.v1.GONE);
				((EditText)findViewById(R.id.edtelevother)).setText("");
			}
			percarr = perc_value.split("~");
			storiesarr = stories_value.split("~");				
			
			for(int i=0;i<percarr.length;i++)
			{				
				int spinnerPosition = spnAadap.getPosition(percarr[i]);
				if(spinnerPosition!=-1)
				{
					spnpercent[i].setSelection(spinnerPosition);
				}
				else 
				{ 
					spnpercent[i].setSelection(0); 
				}
			}
			
			for(int k=0;k<storiesarr.length;k++)
			{			
				spnnoofstor[k].setItems1(items,"--Select--",new multi(),storiesarr[k]);	
				spnnoofstor[k].setSelectedtext(dummytext[k]);
				if(spnnoofstor[k].getSelectedtext().toString().equals("You have selected "))
				{				
					spnwallstruc[k] =  spnnoofstor[k].getSelectedtext().toString().replace("You have selected ","");
					
				}
				else
				{
					spnwallstruc[k] =  spnnoofstor[k].getSelectedtext().toString();
				}
					if(!spnwallstruc[k].equals(""))
					{
						System.out.println("for else");
						
						if(spnwallstruc[k].contains(","))
						{
							floorchoosen = " to view the selected Floors";
						}
						else
						{
							floorchoosen = " to view the selected Floor";
						}							
						tvwstrudetails[k].setVisibility(cf.v1.VISIBLE);
						tvwstruothertxt[k].setVisibility(cf.v1.VISIBLE);
						tvwstrudetails[k].setText(Html.fromHtml("<font color='#0000FF'><u>Click here</u></font>"));
						tvwstruothertxt[k].setText(Html.fromHtml("<font color='#D11C1C'>"+floorchoosen+"</font>"));
						
					}
					else
					{
						tvwstrudetails[k].setVisibility(cf.v1.GONE);
						tvwstruothertxt[k].setVisibility(cf.v1.GONE);
					}	
					
			}
			if(!wallother.equals(""))
			{
				((EditText)findViewById(R.id.edroofwallother)).setText(wallother);
			}
			else
			{
				((EditText)findViewById(R.id.edroofwallother)).setText("");
			}		
				
		}((Button)findViewById(R.id.elevsave)).setText("Update");
	}	

	catch(Exception e)
	{
		System.out.println("catch "+e.getMessage());
	}
	}
	private void Declarations() {
		// TODO Auto-generated method stub
		cf.setupUI((ScrollView) findViewById(R.id.scr));System.out.println("scr");
		if(cf.identityval==32)
		{
			cf.save.setText("Save & Next");
		}
		else
		{
			cf.save.setText("Save");
			
		}
		dynviewelevation = (TableLayout)findViewById(R.id.dynamicviewelevation);
		subpnlshw = (LinearLayout)findViewById(R.id.subpnlshow);
		headingelev = (LinearLayout)findViewById(R.id.savedelev);	
		
		spnelevation = (Spinner)findViewById(R.id.spnelevtype);
		spnBadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, cf.spnelevation);
		spnBadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spnelevation.setAdapter(spnBadap);
		spnelevation.setOnItemSelectedListener(new  Spin_Selectedlistener(1));
		try
		{		
			items.clear();
			cf.getPolicyholderInformation(cf.selectedhomeid);
			for(int j=0;j<Integer.parseInt(cf.Stories);j++)
			{
				items.add(cf.spnstories[j]);
			}  
		}
		catch(Exception e){}
		for(int i=0;i<reinforcechk.length;i++)
   	    {
	   		reinforcechk[i] = (CheckBox)findViewById(reinforceid[i]); 	   			
	    }
		
		for(int i=0;i<spnpercent.length;i++)
   	    {
			spnpercent[i] = (Spinner)findViewById(spnpercid[i]); 
	   		spnAadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, cf.spnpercentage);
	   		spnAadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	   		spnpercent[i].setAdapter(spnAadap);
	   		spnpercent[i].setOnItemSelectedListener(new MyOnItemSelectedListenerpercent(spnpercent[i].getId()));	
   	    }	
		for(int i=0;i<tvwctitle.length;i++)
	   	{
			tvwctitle[i] = (TextView)findViewById(wallcladdingtitleid[i]);
		}
		for(int i=0;i<tvwstrudetails.length;i++)
   	    {
			tvwstrudetails[i] = (TextView)findViewById(wallstrudetailsid [i]);
	   		tvwstrudetails[i].setOnClickListener(new OnClickListener1(i));	   		
   	    }		
		for(int i=0;i<tvwstruothertxt.length;i++)
		{
			tvwstruothertxt[i] = (TextView)findViewById(wallstruothersid[i]);			
	   	}
		for(int i=0;i<dummytext.length;i++)
   	    {
	   		dummytext[i] = (TextView)findViewById(tvid[i]);	   		
   	    }
		for(int i=0;i<tvstoriesdisplay.length;i++)
   	    {
	   		tvstoriesdisplay[i] = (TextView)findViewById(tvstoriesid[i]);
   	    }
		for(int i=0;i<spnnoofstor.length;i++)
   	    {
			spnnoofstor[i] = (MultiSpinner)findViewById(spnstoriesid[i]);
	   		spnnoofstor[i].setItems(items,"--Select--",new multi());	   			
	   		spnnoofstor[i].setSelectedtext(dummytext[i]);			
   	    }
		edwccomments=(EditText) findViewById(R.id.wccomment);
	    edwccomments.addTextChangedListener(new textwatcher());	  
	     
	}
	 class OnClickListener1 implements OnClickListener {
		   int id,iden;
		   OnClickListener1(int id1)
			{
				this.id=id1;
			}
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showalert(2,id,"");
			}
	    }
	 public void showalert(int i, int id,String storiesarr) {
			// TODO Auto-generated method stub
			 dialog1 = new Dialog(WindMitWallCons.this,android.R.style.Theme_Translucent_NoTitleBar);
				dialog1.getWindow().setContentView(R.layout.alertwallstories);
				
				
				if(tvwctitle[id].getText().toString().equals("Other"))
				{
					((TextView)dialog1.findViewById(R.id.txtheading)).setText(tvwctitle[id].getText().toString()+" ("+((EditText)findViewById(R.id.edroofwallother)).getText().toString()+")");
				}
				else
				{
					((TextView)dialog1.findViewById(R.id.txtheading)).setText(tvwctitle[id].getText().toString());
				}	
				
				myTable = (TableLayout)dialog1.findViewById(R.id.mainLayout);
				alertwallstories(i,id,storiesarr);
				((ImageView)dialog1.findViewById(R.id.imagehelpclose)).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog1.setCancelable(true);
						dialog1.dismiss();
					}
				});				
				((Button)dialog1.findViewById(R.id.okbtn)).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog1.setCancelable(true);
						dialog1.dismiss();
					}
				});
				dialog1.show();
		}
	 protected void alertwallstories(int diff,int val,String stories) {
			// TODO Auto-generated method stub
		 int k=0;
			cf.getPolicyholderInformation(cf.selectedhomeid);
			System.out.println("diff"+diff);
			if(diff==1)
			{
				System.out.println("stor"+stories);
				spnwallstruc[val] = stories;
				Wall_Split = spnwallstruc[val].split(",");
			}
			else
			{	
				System.out.println("spnwallstruc");
				if(spnwallstruc[val]==null || spnwallstruc[val]=="")
				{
					System.out.println("spnwallstrunulllllc");
					wallstru= spnnoofstor[val].getSelectedtext().toString().replace("You have selected ","");			
					Wall_Split = wallstru.trim().split(",");
				}
				else
				{
					System.out.println("spnwallstrunulllllcnotnull"+spnwallstruc[val]);
					spnwallstruc[val] = spnwallstruc[val].replace("You have selected ", "");System.out.println("111"+spnwallstruc[val]);
					Wall_Split = spnwallstruc[val].trim().split(",");
				}
				
			}
			
				for(int i=0;i<10;i++)
		        {
					tr =new TableRow(this);
			        TableRow.LayoutParams params = new TableRow.LayoutParams(55,30);//tbl.setBackgroundColor(Color.BLACK);
			        params.setMargins(1, 1, 0, 0);	
			        tr.setOrientation(tr.HORIZONTAL);
			        for (int j=1;j<=10;j++)
			        {
			        	 tv[j] = new TextView(this);
			        	 tv[j].setTypeface(null, Typeface.BOLD);
			        	 tv[j].setGravity(Gravity.CENTER);
			        	 tv[j].setLayoutParams(params);
			        	 tv[j].setTextColor(Color.BLACK);
			        	 tv[j].setBackgroundResource(R.color.Wall_Light);
			        	 String l = ((i*10)+j)+"";	 //System.out.println("l="+l);
			 	         if(Integer.parseInt(l)<=Integer.parseInt(cf.Stories))
			        	 {
			        		 tv[j].setText(l);
			        		 tv[j].setBackgroundResource(R.color.Wall_selected);		 
			        	 }
			        	 else
			        	 {
			        		 tv[j].setText(l); 
			        		 tv[j].setTextColor(Color.parseColor("#bbd3e8"));
			        	 }
			 	         if(k<Wall_Split.length)
			 	         {
			 	        	System.out.println("Wall_Split="+Wall_Split[k]+"l="+l);
			 	        	 if(Wall_Split[k].trim().equals(l))
			 	        	 {
			 	        		 tv[j].setBackgroundResource(R.color.Wall_dark);	
			 	        		 tv[j].setTextColor(Color.WHITE);k++;
			 	        	 }
			 	        	/* System.out.println("LENGTH"+Wall_Split.length);
			 	        	 if(Wall_Split[k]==null)
			 	        	 {
			 	        		System.out.println("nulll"+Wall_Split.length);
			 	        	 }
			 	        	 else
			 	        	 {
			 	        		System.out.println("LENGTH=="+Wall_Split[k]);
				 	        	 if(Wall_Split[k].trim().equals(l))
				 	        	 {
				 	        		 tv[j].setBackgroundResource(R.color.Wall_dark);	
				 	        		 tv[j].setTextColor(Color.WHITE);k++;
				 	        	 }
			 	        	 }*/
			 	         }
			 	         	
			 	        tr.addView(tv[j]);
			        }   
			        myTable.addView(tr); 
		        }
		}
	 
	class multi implements MultiSpinnerListener{

			public void onItemsSelected(boolean[] selected) {
				// TODO Auto-generated method stub
				try
				{System.out.println("spnnoofstor");
					for(int i=0;i<spnnoofstor.length;i++)
					{
						if(!"You have selected --Select--".equals(spnnoofstor[i].getSelectedtext().toString()))
						{	
							System.out.println("spnnoofstor.length");
							if(spnnoofstor[i].getSelectedtext().toString().contains(","))
							{
								floorchoosen = " to view the selected Floors";
							}
							else
							{
								floorchoosen = " to view the selected Floor";
							}
							System.out.println("spnnoofstor.length"+spnnoofstor[i].getSelectedtext().toString());
							if(!spnnoofstor[i].getSelectedtext().toString().equals(""))
							{								
								tvstoriesdisplay[i].setText("You have selected " +spnnoofstor[i].getSelectedtext().toString());
								tvwstrudetails[i].setVisibility(cf.v1.VISIBLE);
								tvwstruothertxt[i].setVisibility(cf.v1.VISIBLE);
								tvwstrudetails[i].setText(Html.fromHtml("<font color='#0000FF'><u>Click here</u></font>"));
								tvwstruothertxt[i].setText(Html.fromHtml("<font color='#D11C1C'>"+floorchoosen+"</font>"));
								
							}else
							{
								tvstoriesdisplay[i].setVisibility(cf.v1.GONE);
								tvwstrudetails[i].setVisibility(cf.v1.INVISIBLE);
								tvwstruothertxt[i].setVisibility(cf.v1.INVISIBLE);
							}
						}
					}
				}
				catch(Exception e){
					System.out.println("sds dasd "+e.getMessage());
				}
			}
	    	
	    }
	
	class Spin_Selectedlistener implements OnItemSelectedListener
	{
		int i;
		public Spin_Selectedlistener(int i) {
			// TODO Auto-generated constructor stub
			this.i=i;
		}
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			if(i==1){
				System.out.println("identity"+identity);
				System.out.println("identityelse="+identity);
				elevspinnertxt = spnelevation.getSelectedItem().toString();
				System.out.println("elee"+elevspinnertxt);
				if(elevspinnertxt.equals("Other Elevation"))
				{
					((EditText)findViewById(R.id.edtelevother)).setVisibility(cf.v1.VISIBLE);
				}
				else
				{
					((EditText)findViewById(R.id.edtelevother)).setText("");
					((EditText)findViewById(R.id.edtelevother)).setVisibility(cf.v1.GONE);
				}
				if(identity==0)
				{
					System.out.println("identityif="+identity);
					for(int i=0;i<spnpercent.length;i++)
			   	    {
						spnpercent[i].setSelection(0);
				   		spnnoofstor[i].setSelection(0);
				   		dummytext[i].setText("");
				   		spnnoofstor[i].setItems1(items,"--Select--",new multi(),"");	   			
				   		spnnoofstor[i].setSelectedtext(dummytext[i]);				   		
				   		dummytext[i].setVisibility(cf.v1.GONE);
				   		tvwstrudetails[i].setVisibility(cf.v1.GONE);
			   	    }
					((EditText)findViewById(R.id.edroofwallother)).setText("");
					((EditText)findViewById(R.id.edroofwallother)).setFocusable(false);
				}
				else
				{
					
				}
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
		}
		
	}

	public class MyOnItemSelectedListenerpercent implements OnItemSelectedListener {
		int type1;
		public MyOnItemSelectedListenerpercent(int id) {
			// TODO Auto-generated constructor stub
			type1=id;
		}

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {			
			String storiesvalue = ((Spinner)findViewById(type1)).getSelectedItem().toString();
			if (storiesvalue.equals("")) {
				storiesvalue = "0";
			}
			
			switch(type1)
			{
			
				case R.id.spnperc1:
					
					try {
						if(!storiesvalue.equals("--Select--"))
						{
							per1 = Integer.parseInt(storiesvalue);	
							spnnoofstor[0].setEnabled(true);
						}
						else
						{
							per1=0;
							uncheckfloors(0);
						}
					} catch (Exception e) {
						per1 = 0;
					}
					break;
				case R.id.spnperc2:
					try {
						if(!storiesvalue.equals("--Select--"))
						{
							per2 = Integer.parseInt(storiesvalue);	
							spnnoofstor[1].setEnabled(true);
						}
						else
						{
							per2=0;
							uncheckfloors(1);
						}
					} catch (Exception e) {
						per2 = 0;
					}
					break;
				case R.id.spnperc3:
					try {
						if(!storiesvalue.equals("--Select--"))
						{
							per3 = Integer.parseInt(storiesvalue);	
							spnnoofstor[2].setEnabled(true);
						}
						else
						{
							per3=0;
							uncheckfloors(2);
						}
					} catch (Exception e) {
						per3 = 0;
					}
					break;
				case R.id.spnperc4:
					try {
						if(!storiesvalue.equals("--Select--"))
						{
							per4 = Integer.parseInt(storiesvalue);	
							spnnoofstor[3].setEnabled(true);
						}
						else
						{
							per4=0;
							uncheckfloors(3);
						}
					} catch (Exception e) {
						per4 = 0;
					}
					break;
				case R.id.spnperc5:
					try {
						if(!storiesvalue.equals("--Select--"))
						{
							per5 = Integer.parseInt(storiesvalue);	
							spnnoofstor[4].setEnabled(true);
						}
						else
						{
							per5=0;
							uncheckfloors(4);
						}
					} catch (Exception e) {
						per5 = 0;
					}
					break;
				case R.id.spnperc6:
					try {
						if(!storiesvalue.equals("--Select--"))
						{
							per6 = Integer.parseInt(storiesvalue);	
							spnnoofstor[5].setEnabled(true);
						}
						else
						{
							per6=0;
							uncheckfloors(5);
						}
					} catch (Exception e) {
						per6 = 0;
					}
					break;
				case R.id.spnperc7:
					try {
						if(!storiesvalue.equals("--Select--"))
						{
							per7 = Integer.parseInt(storiesvalue);	
							spnnoofstor[6].setEnabled(true);
						}
						else
						{
							per7=0;
							((EditText)findViewById(R.id.edroofwallother)).setText("");
							uncheckfloors(6);
						}
					} catch (Exception e) {
						per7 = 0;
					}
					break;
					
			}
			}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
	}
	public void uncheckfloors(int i) {
		// TODO Auto-generated method stub
		//System.out.println("EEE"+spnnoofstor[i].getSelectedtext().toString());
		spnnoofstor[i].setEnabled(false);
		
		
		dummytext[i].setText("");
   		spnnoofstor[i].setItems1(items,"--Select--",new multi(),"");	   			
   		spnnoofstor[i].setSelectedtext(dummytext[i]);
		
		tvwstrudetails[i].setVisibility(cf.v1.GONE);
		tvwstruothertxt[i].setVisibility(cf.v1.GONE);
		System.out.println("setItems1"+spnnoofstor[i].getSelectedtext().toString());
	}
	class textwatcher implements TextWatcher
    {
	         public int type;
	         textwatcher()
	    	{
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {


	    		// TODO Auto-generated method stub
	    		cf.showing_limit(s.toString(),(TextView)findViewById(R.id.wc_tv_type1),500); 
	    		
	}

	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,

	    			int after) {
	    		// TODO Auto-generated method stub
	     	}

	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,

	    			int count) {
	    		// TODO Auto-generated method stub
	    	}
	   }	
	
	public void clicker(View v) {
		switch(v.getId())
		{
		case R.id.loadcomments:
			/***Call for the comments***/
			cf.findinspectionname(cf.identityval);
			int len=((EditText)findViewById(R.id.wccomment)).getText().toString().length();			
					if(load_comment)
					{
						load_comment=false;
						int loc1[] = new int[2];
						v.getLocationOnScreen(loc1);
						Intent in = cf.loadComments("Wall Construction Comments",loc1);
						in.putExtra("insp_name", cf.inspname);
						in.putExtra("insp_ques", "Wall Construction");
						in.putExtra("length", len);
						in.putExtra("max_length", 500);
						startActivityForResult(in, cf.loadcomment_code);
					}
			break;
		case R.id.hme:
    		cf.gohome();
    		break;
		case R.id.reinfor_chk5:
			if (((CheckBox) v).isChecked()) {
				((EditText)findViewById(R.id.edreinforceother)).setVisibility(v.VISIBLE);
			}
			else{
				((EditText)findViewById(R.id.edreinforceother)).setVisibility(v.GONE);
				((EditText)findViewById(R.id.edreinforceother)).setText("");
			}
    		break;
		case R.id.elevcancel:
			clearelevdatas();
			break;
		case R.id.elevsave:
			boolean b=((spnelevation.getSelectedItem().toString().equals("Other Elevation")) && ((EditText)findViewById(R.id.edtelevother)).getText().toString().trim().equals("")) ? false:true;
			boolean chkglobal = false;
			int totp = per1 + per2 + per3 + per4 + per5 + per6 +per7;
			if(!spnelevation.getSelectedItem().toString().equals("--Select--") && b==true)
			{
				if(totp==0)
				{
					cf.ShowToast("Please select Percentage Value.",0);			
				}
				else if ((totp > 100))
				{
					cf.ShowToast("Percentage exceeds 100% and should sum to 100.", 1);					
				}
				else if ((totp < 100)) 
				{
					cf.ShowToast("Percentage should sum to 100.",0);						
				}
				else if ((totp == 100)) 
				{				
					if(per7!=0) 
					{
						s = ((EditText)findViewById(R.id.edroofwallother)).getText().toString().trim();	
						if(s.equals(""))
						{
							cf.ShowToast("Please enter the other text for Wall Construction.",0);
							cf.setFocus(((EditText)findViewById(R.id.edroofwallother)));							
						}
						else 
						{
							insertdata();
						}
					}
					else
					{
						insertdata();
					}
				}
			}else
			{
				if(spnelevation.getSelectedItem().toString().equals("--Select--"))
				{
					cf.ShowToast("Please select the Elevation.", 0);
				}
				else if(b==false)
				{
					cf.ShowToast("Please select the Other Elevation text.", 0);
					cf.setFocus(((EditText)findViewById(R.id.edtelevother)));
				}
			}
			break;
		case R.id.save:
			Cursor c1 = cf.arr_db.rawQuery("SELECT * FROM "
					+ cf.WindMit_WallCons + " WHERE fld_srid='"
					+ cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'", null);				
			if(c1.getCount()==0)
			{
				cf.ShowToast("Please save atleast one Wall construction Elevation Type.",0);	
			}
			else
			{
				reinforce_value= cf.getselected_chk(reinforcechk);
				String reinforceotherval =(reinforcechk[reinforcechk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.edreinforceother)).getText().toString():""; 
				reinforce_value+=reinforceotherval;
		    	if(reinforce_value.equals(""))
				{
					 cf.ShowToast("Please select the option for Reinforcement Check." , 0);
				}
		    	else
		    	{
		    		 if(reinforcechk[reinforcechk.length-1].isChecked())
					 {
		    			 if(reinforceotherval.trim().equals("&#40;"))
						 {
							 cf.ShowToast("Please enter the other text for Reinforcement Check.", 0);
							 cf.setFocus(((EditText)findViewById(R.id.edreinforceother)));
						 }
						 else
						 {
							WC_Insert();	
						 }
					 }
					 else
					 {
						 WC_Insert();
					 }
	    	   }
			}
			break;
		}
	}
	private void clearelevdatas() {
		// TODO Auto-generated method stub
		spnelevation.setSelection(0);spnelevation.setEnabled(true);
		for(int i=0;i<spnpercent.length;i++)
   	    {
	   		spnpercent[i].setSelection(0);
	   		spnnoofstor[i].setSelection(0);
	   		dummytext[i].setText("");
	   		spnnoofstor[i].setItems1(items,"--Select--",new multi(),"");	   			
	   		spnnoofstor[i].setSelectedtext(dummytext[i]);				   		
	   		dummytext[i].setVisibility(cf.v1.GONE);
	   		tvwstrudetails[i].setVisibility(cf.v1.GONE);
	   		tvwstruothertxt[i].setVisibility(cf.v1.GONE);
   	    }
		((EditText)findViewById(R.id.edroofwallother)).setText("");
		((EditText)findViewById(R.id.edroofwallother)).setFocusable(false);
		((Button)findViewById(R.id.elevsave)).setText("Save/Add More Elevation");
	}


	private void WC_Insert() {
		// TODO Auto-generated method stub		
		if(((EditText)findViewById(R.id.wccomment)).getText().toString().trim().equals(""))
		{
			cf.ShowToast("Please enter the comments for Wall Construction.",0);	
			cf.setFocus(((EditText)findViewById(R.id.wccomment)));
		}
		else
		{
			 try
			 {
				 Cursor c1=cf.SelectTablefunction(cf.Wind_Questiontbl, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
				 if(c1.getCount()==0)
				 {
					 cf.arr_db.execSQL("INSERT INTO "
								+ cf.Wind_Questiontbl
								+ " (fld_srid,insp_typeid,fld_bcyearbuilt,fld_bcpermitappdate,fld_buildcode,fld_roofdeck,fld_rdothertext," +
								"	fld_roofwall,fld_roofwallsub,fld_roofwallclipsminval,fld_roofwallclipssingminval,fld_roofwallclipsdoubminval,fld_rwothertext," +
								"	fld_roofgeometry ,fld_geomlength ,fld_roofgeomtotalarea ,fld_roofgeomothertext ,fld_swr,fld_optype,fld_openprotect,fld_opsubvalue,fld_oplevelchart," +
								"   fld_opGOwindentdoorval,fld_opGOgardoorval,fld_opGOskylightsval,fld_opGOglassblockval,fld_opNGOentrydoorval,fld_opNGOgaragedoorval, " +
								"	fld_wcvalue,fld_wcreinforcement,fld_quesmodifydate)"								
								+ "VALUES ('" + cf.selectedhomeid + "','"+cf.identityval+"','','','','" + 0 + "','','" +
								+ 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0	+ "','','"+
								+ 0 + "','" + 0 + "','" + 0 + "','','" +  0 + "','"+ 0 +"','"+ 0 + "','" + 0 + "','" + 0 + "','" +
								+ 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','"+ 0 +"','" +
								+ 0 + "','" + cf.encode(reinforce_value) + "','"+ cf.datewithtime + "')");	
				 }
				 else
				 {
					  cf.arr_db.execSQL("UPDATE " + cf.Wind_Questiontbl
								+ " SET fld_wcreinforcement='" + cf.encode(reinforce_value)+ "' WHERE fld_srid ='" + cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'");
				 }
				 
					Cursor c2 = cf.SelectTablefunction(cf.Wind_QuesCommtbl, "where fld_srid='"+ cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'");
					if (c2.getCount() == 0) 
					{
						cf.arr_db.execSQL("INSERT INTO "
								+ cf.Wind_QuesCommtbl
								+ " (fld_srid,insp_typeid,fld_buildcodecomments,fld_roofcovercomments,fld_roofdeckcomments,fld_roofwallcomments," +
								"fld_roofgeometrycomments,fld_swrcomments,fld_openprotectioncomments,fld_wallconscomments,fld_insoverallcomments )"
								+ "VALUES('"+cf.selectedhomeid+"','"+cf.identityval+"','','','','','','','','"+cf.encode(edwccomments.getText().toString())+"','')");
					}
					else
					{
						cf.arr_db.execSQL("UPDATE " + cf.Wind_QuesCommtbl
								+ " SET fld_wallconscomments='"+ cf.encode((edwccomments).getText().toString())	+ "' WHERE fld_srid ='" + cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'");
					}
					cf.ShowToast("Wall Construction saved successfully.",0);
					cf.gotoclass(cf.identityval, WindAddendumcomm.class);
					/*if(cf.identityval==32 && cf.onlinspectionid.equals("12"))
					{
						cf.gotoclass(cf.identityval, WindMitCommareas.class);
					}	*/
			 }
			 catch (Exception E)
			 {
				String strerrorlog="Updating Questions Wall Construction - Wind";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			 }			
		}
	}
	private void insertdata() {
		// TODO Auto-generated method stub	
		spn1=spnnoofstor[0].getSelectedtext().toString();System.out.println("SPN1"+spn1);
		spn2=spnnoofstor[1].getSelectedtext().toString();System.out.println("SPN2"+spn2);
		spn3=spnnoofstor[2].getSelectedtext().toString();System.out.println("SPN3"+spn3);
		spn4=spnnoofstor[3].getSelectedtext().toString();System.out.println("SPN4"+spn4);
		spn5=spnnoofstor[4].getSelectedtext().toString();System.out.println("SPN5"+spn5);
		spn6=spnnoofstor[5].getSelectedtext().toString();System.out.println("SPN6"+spn6);
		spn7=spnnoofstor[6].getSelectedtext().toString();System.out.println("SPN7"+spn7);
		
		if(!spn1.equals("")){spn1=spn1.replace("You have selected ", "");spn1=spn1.replace(" ", "");}else{spn1=" ";}
		if(!spn2.equals("")){spn2=spn2.replace("You have selected ", "");spn2=spn2.replace(" ", "");}else{spn2=" ";}
		if(!spn3.equals("")){spn3=spn3.replace("You have selected ", "");spn3=spn3.replace(" ", "");}else{spn3=" ";}
		if(!spn4.equals("")){spn4=spn4.replace("You have selected ", "");spn4=spn4.replace(" ", "");}else{spn4=" ";}
		if(!spn5.equals("")){spn5=spn5.replace("You have selected ", "");spn5=spn5.replace(" ", "");}else{spn5=" ";}
		if(!spn6.equals("")){spn6=spn6.replace("You have selected ", "");spn6=spn6.replace(" ", "");}else{spn6=" ";}
		if(!spn7.equals("")){spn7=spn7.replace("You have selected ", "");spn7=spn7.replace(" ", "");}else{spn7=" ";}
			
		String totper=per1+"~"+per2+"~"+per3+"~"+per4+"~"+per5+"~"+per6+"~"+per7;
		String totstories=spn1+"~"+spn2+"~"+spn3+"~"+spn4+"~"+spn5+"~"+spn6+"~"+spn7;
		System.out.println("tot"+totstories);
		
		if(per7==0 && spn7.trim().equals(""))
		{
			s1 = ""; 
		}
		else
		{
			s1=cf.encode(((EditText)findViewById(R.id.edroofwallother)).getText().toString().trim());
		}
		try
		{
			if(((Button)findViewById(R.id.elevsave)).getText().toString().equals("Update")){
				cf.arr_db.execSQL("UPDATE "
						+ cf.WindMit_WallCons
						+ " SET fld_Percentage='" + totper + "',fld_Stories='" + totstories + "',fld_WallconsOther='"+s1+"',fld_elevation_othertxt='"+cf.encode(((EditText)findViewById(R.id.edtelevother)).getText().toString().trim())+"'"
						+ " WHERE fld_srid ='"
						+ cf.selectedhomeid + "' and fld_Elevation='"+spnelevation.getSelectedItem().toString()+"' and insp_typeid='"+cf.identityval+"' and WCID='"+elevid+"'");
				((Button)findViewById(R.id.elevsave)).setText("Save/Add More Elevation");
			}
			else
			{
				Cursor c1=cf.SelectTablefunction(cf.WindMit_WallCons, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
				cf.arr_db.execSQL("INSERT INTO "
						+ cf.WindMit_WallCons
						+ " (fld_srid,insp_typeid,fld_Elevation,fld_Percentage,fld_Stories,fld_elevation_othertxt,fld_WallconsOther)"
						+ "VALUES ('" + cf.selectedhomeid + "','"+cf.identityval+"','"+ spnelevation.getSelectedItem().toString() + "','"+totper+"','"+totstories+"','"+cf.encode(((EditText)findViewById(R.id.edtelevother)).getText().toString().trim())+"','"+s1+"')");
			
				
			}
			cf.ShowToast("Wall Construction saved successfully.",0);
			((Button)findViewById(R.id.elevsave)).setText("Save/Add More Elevation");
			getSavedelev_value();	
			clearelevdatas();
		}
		catch (Exception E)
		{
			String strerrorlog="Updating Questions Buildcode - FourPoint";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		} 
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
 		// replaces the default 'Back' button action
 		if (keyCode == KeyEvent.KEYCODE_BACK) {
 			cf.gotoclass(cf.identityval, WindMitOpenProt.class);
 			return true;
 		}
 		
 		return super.onKeyDown(keyCode, event);
 	}
	/**on activity result for the load comments**/
	 @Override
	 		protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	 		try
	 		{
	 			if(requestCode==cf.loadcomment_code)
	 			{
	 				load_comment=true;
	 				if(resultCode==RESULT_OK)
	 				{
		 				String wccomm = edwccomments.getText().toString();	 
		 				edwccomments.setText(wccomm +" "+data.getExtras().getString("Comments"));	 				
	 				}
	 			}
	 		}
	 		catch (Exception e) {
	 			// TODO: handle exception
	 			System.out.println("the erro was "+e.getMessage());
	 		}
	 	}/**on activity result for the load comments ends**/
}