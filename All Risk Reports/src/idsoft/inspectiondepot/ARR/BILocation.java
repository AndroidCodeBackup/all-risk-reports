package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.BIObservations.textwatcher;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;

public class BILocation extends Activity {
	CommonFunctions cf;
	public Spinner spinnerloc[] = new Spinner[4];
	public int[] spnlocationid = {R.id.spinloc1,R.id.spinloc2,R.id.spinloc3,R.id.spinloc4};
	ArrayAdapter spinlocadap;
	Cursor c1;
	public String spnlocation[] = { "--Select--", "Yes","No","Not Determined","Not Applicable","Beyond Scope of Inspection"};
	String BI_LOC="",locchkval="",locnotappl="0";
	public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);	       
	        cf = new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);				
		 	}
	        setContentView(R.layout.location);
	        cf.getDeviceDimensions();
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
         	hdr_layout.addView(new HdrOnclickListener(this,1,"General","Building Info","Location - Proximity Information",1,1,cf));
         	LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
         	main_layout.setMinimumWidth(cf.wd);
         	main_layout.addView(new MyOnclickListener(getApplicationContext(), 1, 0,0,cf));
         	LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
	        submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 16, 1,0,cf));
	        LinearLayout submenu2_layout = (LinearLayout) findViewById(R.id.submenu1); 
	        submenu2_layout.addView(new MyOnclickListener(getApplicationContext(), 451, 1,0,cf));
	        TableRow tblrw = (TableRow)findViewById(R.id.row2);
	        tblrw.setMinimumHeight(cf.ht); 
	        Declaration();
	        cf.CreateARRTable(61);
	        setValue();
	    }
	private void setValue() {
		// TODO Auto-generated method stub
		cf.Check_BI();
		if(cf.C_GEN.getCount()==0)
		{
			((CheckBox)findViewById(R.id.locnotapplicable)).setChecked(true);
			((LinearLayout)findViewById(R.id.locationlin)).setVisibility(cf.v1.GONE);
		}
		else
		{
			if(cf.Loc_NA.equals("0") || cf.Loc_NA.equals(""))
			{
				if(!cf.Loc_val.equals(""))
				{
					((LinearLayout)findViewById(R.id.locationlin)).setVisibility(cf.v1.VISIBLE);
					String Loc_Split[] = cf.Loc_val.split("&#94;");
					int spinnerloc1 = spinlocadap.getPosition(Loc_Split[0]);
					spinnerloc[0].setSelection(spinnerloc1);
					
					int spinnerloc2 = spinlocadap.getPosition(Loc_Split[1]);
					spinnerloc[1].setSelection(spinnerloc2);
					
					int spinnerloc3 = spinlocadap.getPosition(Loc_Split[2]);
					spinnerloc[2].setSelection(spinnerloc3);
					
					int spinnerloc4 = spinlocadap.getPosition(Loc_Split[3]);
					spinnerloc[3].setSelection(spinnerloc4);
					
					if(Loc_Split.length>4)
					{
						((EditText)findViewById(R.id.loc_other)).setText(Loc_Split[4]);				
					}
					((TextView)findViewById(R.id.savetxtloc)).setVisibility(cf.v1.VISIBLE);
				}
				else
				{
					((LinearLayout)findViewById(R.id.locationlin)).setVisibility(cf.v1.GONE);
					((TextView)findViewById(R.id.savetxtloc)).setVisibility(cf.v1.GONE);
					((CheckBox)findViewById(R.id.locnotapplicable)).setChecked(true);
				}
			}
			else
			{
				((CheckBox)findViewById(R.id.locnotapplicable)).setChecked(true);
				((LinearLayout)findViewById(R.id.locationlin)).setVisibility(cf.v1.GONE);
				((TextView)findViewById(R.id.savetxtloc)).setVisibility(cf.v1.VISIBLE);
			}
		}
	}
	private void Declaration() {
		// TODO Auto-generated method stub
		cf.setupUI((ScrollView) findViewById(R.id.scr));
		for(int i=0;i<=3;i++)
	   	{
			spinnerloc[i] = (Spinner)findViewById(spnlocationid[i]);
		   	spinlocadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, spnlocation);
		   	spinlocadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		   	spinnerloc[i].setAdapter(spinlocadap);		   	
	   	}
		 ((EditText)findViewById(R.id.loc_other)).addTextChangedListener(new textwatcher(1));
	}
	
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.mouseoverid:
			cf.ShowToast("Use for General Conditions Report only.",0);
			break;
		case R.id.locsave:			
			if(((CheckBox)findViewById(R.id.locnotapplicable)).isChecked()==false)
				{
					if(!"--Select--".equals(spinnerloc[0].getSelectedItem().toString()))
					{
						if(!"--Select--".equals(spinnerloc[1].getSelectedItem().toString()))
						{
							if(!"--Select--".equals(spinnerloc[2].getSelectedItem().toString()))
							{
								if(!"--Select--".equals(spinnerloc[3].getSelectedItem().toString()))
								{
									if(((EditText)findViewById(R.id.loc_other)).getText().toString().length()>1)
									{
										if(((EditText)findViewById(R.id.loc_other)).getText().toString().trim().equals(""))
										{
										
											cf.ShowToast("Please enter the Other text.", 0);
											cf.setFocus((EditText)findViewById(R.id.loc_other));
										}
										else
										{
											InsertLocation();	
										}
									}
									else
									{
										InsertLocation();	
									}										
								}
								else
								{
									cf.ShowToast("Please select value under Subject to Brush / Forest Fires.", 0);	
								}
							}
							else
							{
								cf.ShowToast("Please select value under Within 40 FT of Commercial Structure.", 0);	
							}
						}
						else
						{
							cf.ShowToast("Please select value under Within 5 Miles of Salt Water.", 0);	
						}
					}
					else
					{
						cf.ShowToast("Please select value under Within 1,500 FT of Salt Water.", 0);	
					}
				}
				else
				{
					InsertLocation();
				}
			break;
		case R.id.locnotapplicable:			
			if(((CheckBox)findViewById(R.id.locnotapplicable)).isChecked())
			{
				locnotappl="1";
				if(!cf.Loc_val.equals(""))
				{
					AlertDialog.Builder builder = new AlertDialog.Builder(BILocation.this);
					builder.setTitle("Confirmation");
					builder.setIcon(R.drawable.alertmsg);
					builder.setMessage("Do you want to clear the Location Proximity data?").setCancelable(false)
							.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,int id1) 
										{
												locationclear();
												((CheckBox)findViewById(R.id.locnotapplicable)).setChecked(true);
												((LinearLayout)findViewById(R.id.locationlin)).setVisibility(cf.v1.GONE);
												((TextView)findViewById(R.id.savetxtloc)).setVisibility(cf.v1.GONE);
										}
									})
							.setNegativeButton("No",new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,	int id) {
											
											((CheckBox)findViewById(R.id.locnotapplicable)).setChecked(false);
											dialog.cancel();											
									}
							});
					builder.show();
				}
				else
				{
					((TextView)findViewById(R.id.savetxtloc)).setVisibility(cf.v1.GONE);
					((LinearLayout)findViewById(R.id.locationlin)).setVisibility(cf.v1.GONE);
				}
				
			}
			else
			{
				locnotappl="0";
				locationclear();
				//int spinnerloc1 = spinlocadap.getPosition(Loc_Split[0]);
				spinnerloc[0].setSelection(2);
				
				//int spinnerloc2 = spinlocadap.getPosition(Loc_Split[1]);
				spinnerloc[1].setSelection(2);
				
				//int spinnerloc3 = spinlocadap.getPosition(Loc_Split[2]);
				spinnerloc[2].setSelection(2);
				
				//int spinnerloc4 = spinlocadap.getPosition(Loc_Split[3]);
				spinnerloc[3].setSelection(2);
				((LinearLayout)findViewById(R.id.locationlin)).setVisibility(cf.v1.VISIBLE);
				((TextView)findViewById(R.id.savetxtloc)).setVisibility(cf.v1.GONE);
			}
			break;
		}		
	}	
	class textwatcher implements TextWatcher
	 {
	       public int type;
	         textwatcher(int type)
	    	{
	    		this.type=type;
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {
	    		// TODO Auto-generated method stub
	    		if(this.type==1)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.locother_txt),250); 
	    			if(s.toString().startsWith(" "))
	    			{
	    				 ((EditText)findViewById(R.id.loc_other)).setText("");
	    			}
	    		}	    		
	    	}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
			}
	 }
	protected void locationclear() {
		// TODO Auto-generated method stub
		for(int i=0;i<spinnerloc.length;i++)
		{
			spinnerloc[i].setSelection(0);
		}
		((EditText)findViewById(R.id.loc_other)).setText("");cf.Loc_val="";
	}
	
	private void InsertLocation() {
		// TODO Auto-generated method stub
		BI_LOC =spinnerloc[0].getSelectedItem().toString()+"&#94;"+spinnerloc[1].getSelectedItem().toString()+"&#94;"+spinnerloc[2].getSelectedItem().toString()+"&#94;"
				+spinnerloc[3].getSelectedItem().toString()+"&#94;"+((EditText)findViewById(R.id.loc_other)).getText().toString();
		if(((CheckBox)findViewById(R.id.locnotapplicable)).isChecked()){locnotappl="1";BI_LOC="";}else{locnotappl="0";}
		
		updatenotappl();
		cf.ShowToast("Location - Proximity Information saved successfully",1);
		//((TextView)findViewById(R.id.savetxtloc)).setVisibility(cf.v1.VISIBLE);
		cf.goclass(452);
	}
	private void updatenotappl() {
		// TODO Auto-generated method stub
		try
		{
			cf.Check_BI();
			if(cf.C_GEN.getCount()==0)
			{
				cf.arr_db.execSQL("INSERT INTO " + cf.BI_General + " (fld_srid,BI_LOC_DESC,BI_LOC_NA)"
						+ "VALUES ('"+cf.selectedhomeid+"','"+cf.encode(BI_LOC)+"','"+locnotappl+"')");
			}
			else
			{
				cf.arr_db.execSQL("UPDATE "	+ cf.BI_General	+ " SET BI_LOC_DESC='"+cf.encode(BI_LOC)+"',BI_LOC_NA='"+locnotappl+"' WHERE fld_srid ='"+ cf.selectedhomeid + "'");	
			}			
		}
		catch(Exception e)
		{
			System.out.println("sdfdfds"+e.getMessage());
		}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			cf.goclass(450);
			return true;
		}		
		return super.onKeyDown(keyCode, event);
	}
}