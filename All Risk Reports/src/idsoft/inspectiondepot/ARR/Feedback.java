package idsoft.inspectiondepot.ARR;
/*
 ************************************************************
 * Project: ALL RISK REPORT
 * Module : Feedback.java
 * Creation History:
    Created By : Rakki s on 10/18/2012
 * Modification History:
    Last Modified By : Gowri on 04/04/2013
 ************************************************************  
*/
import idsoft.inspectiondepot.ARR.Electrical.ServiceMainclicker;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Feedback extends Activity{
	private static final int SELECT_PICTURE = 0;
	private static final int SELECT_PDF = 1;
	private static final int CAPTURE_PICTURE_INTENT = 0;
	private ListView lv1;View v2;public int focus=0;
	private static final String TAG = null;
	protected static final int visibility = 0;Bitmap rotated_b;
	ArrayList<String> listItems=new ArrayList<String>();
    private File[] imagelist;
    String[] pdflist=null;
    SummaryFunctions sf;
	int o=0,documentothertextsupp=0,documentothertextoff=0,commentlength;
	Cursor curfeed1,curfeed2;
	TextWatcher watcher;
	ListAdapter adapter,adapter11;ArrayAdapter<String> adapter1;
	View v1;File images;
	ListView doclst,offlst;
	String[] items = {"gallery","pdf"};
	Cursor c1;
	String[] items1 = {"pdf"};
	static String[] docnamearr;static String[] docidarr; static String[] offdocidarr;
	static String[] docpatharr;
	static String[] offnamearr;
	TableLayout tbl;
	static String[] offpatharr;
	String[] array_who,array_doc;String selected_id,offselected_id;
	Uri mCapturedImageURI;int ImgOrder =1;
	String doc_id,offdoc_id,strdoctitle,strdoctitle1,strdoctitleoff,strdoctitlesupp,homeid,strpresatins="",strfbcoment,strfbaddcoment="",strdocname,selectedImagePath="empty",selectedImagePath1,capturedImageFilePath,
	       docname,docpath,offname,offpath,InspectionType,status,inspectiontype,picname,strother="";
	RadioButton cusyrd,cusnrd,insyrd,insnrd,insntrd,infyrd,infnrd,infntrd,insnard;
	String isCSC="",dupflag="",eownerchk_value="";
	//String isInsavail="";
	int rws,t,c,docrws,offrws,rws1,rws2,flagstr,inspid,value,Count,feedrws1,feedrws2;
	String isManf="",inspectionid="",strwhopresent="";
	EditText edfbcoment,offedittitle,docpathedit,othrtxt,othrtxtsupp,othrtxtoff; 
	android.text.format.DateFormat df;CharSequence cd,md;Cursor c11;
	TableLayout offtbl,tblsupp,tbloffice;
	LinearLayout doctbllst,offtbllst,linpar,lintyp;
	Button docbrws,offbrws,docupd,viewsummary;
	private static final String mbtblInspectionList = "Retail_inpgeneralinfo";
	Spinner s,s1,s2; TextView policyholderinfo;
	ScrollView pscr,cscr;
	int currnet_rotated=0;
	TableLayout exceedslimit1;
	TextView lintxt,CSEform,Whowaspresent,didhomeowner,didhomeownermanu,txtfeedbackcomments;
	String inspection="B1 1802 Inspection";
	
	
	public int[] ownerrdioid = {R.id.insyes,R.id.insno,R.id.insnot,R.id.insna};
	public RadioButton ownerchk[] = new RadioButton[4];
	
	public CheckBox whopresentchk[] = new CheckBox[4];
	public int[] whopresid = {R.id.whop_chk1,R.id.whop_chk2,R.id.whop_chk3,R.id.whop_chk4};
	CommonFunctions cf;
	Spinner  insp_sp;
	private String[] selected_insp,selected_inspid,selected_onlineid;
	public void onCreate(Bundle SavedInstanceState) {
		super.onCreate(SavedInstanceState);
		
		cf = new CommonFunctions(this);
		sf = new SummaryFunctions(this);
		 /** Creating feedback table **/
		
		cf.CreateARRTable(83);
		cf.CreateARRTable(84);
		cf.CreateARRTable(90);
		
		/*Cursor cur = cf.arr_db.rawQuery("select * from " + cf.FeedBackInfoTable, null);
		cur.moveToFirst();
		int result1 = cur.getColumnIndex("ARR_FI_FeedbackComments_addendum");
		
		if (result1 == -1) 
		{
			cf.arr_db.execSQL("ALTER TABLE " + cf.FeedBackInfoTable +" ADD COLUMN ARR_FI_FeedbackComments_addendum VARCHAR(1000)");				
		}*/
		Bundle bunQ1extras1 = getIntent().getExtras();        
		  if (bunQ1extras1 != null) {
			cf.selectedhomeid= bunQ1extras1.getString("homeid");
			cf.onlinspectionid = bunQ1extras1.getString("InspectionType");
		    cf.onlstatus = bunQ1extras1.getString("status");
			
		  }

		 
		   setContentView(R.layout.feedbackdocument); 
		   /**Used for hide he key board when it clik out side**/

			cf.setupUI((ScrollView) findViewById(R.id.scr));

		/**Used for hide he key board when it clik out side**/
		   focus=1;
		   cf.getInspectorId();//cf.getinspectiondate();
		   cf.getDeviceDimensions();
		   /** menu **/    
		  
		   LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
			hdr_layout.addView(new HdrOnclickListener(this,1,"Feedback","",1,2,cf));
	        
		   LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
			  mainmenu_layout.setMinimumWidth(cf.wd);
		      mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 8, 0,0,cf));
		      TableRow tblrw = (TableRow)findViewById(R.id.row2);
		      tblrw.setMinimumHeight(cf.ht);
		     
		      /**hide the take camera icon **/
				((ImageView) findViewById(R.id.head_take_image)).setVisibility(View.GONE);
				/**hide the take camera icon ends **/
	        
	       df = new android.text.format.DateFormat();
		   cd = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		   md = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		  
		   for(int i=0;i<whopresentchk.length;i++)
		   {
			   whopresentchk[i] = (CheckBox)findViewById(whopresid[i]);
		   }
		   
			cusyrd = (RadioButton)findViewById(R.id.customeryes);
			cusyrd.setOnClickListener(OnClickListener);
			cusnrd = (RadioButton)findViewById(R.id.customerno);
			cusnrd.setOnClickListener(OnClickListener);
			/*insyrd = (RadioButton)findViewById(R.id.insyes);
			insyrd.setOnClickListener(OnClickListener);
			insnrd = (RadioButton)findViewById(R.id.insno);
			insnrd.setOnClickListener(OnClickListener);
			insntrd = (RadioButton)findViewById(R.id.insnot);
			insntrd.setOnClickListener(OnClickListener);
			insnard = (RadioButton)findViewById(R.id.insna);
			insnard.setOnClickListener(OnClickListener);*/
			

			for(int i=0;i<ownerchk.length;i++)
		   	 {
		   		try{
		   			ownerchk[i] = (RadioButton)findViewById(ownerrdioid[i]);
		   			ownerchk[i].setOnClickListener(new OwnerrdioClicker());
		   		}
					catch(Exception e)
					{
						System.out.println("error= "+e.getMessage());
					}
		   	 }
			
			
			infyrd = (RadioButton)findViewById(R.id.infyes);
			infyrd.setOnClickListener(OnClickListener);
			infnrd = (RadioButton)findViewById(R.id.infno);
			infnrd.setOnClickListener(OnClickListener);
			infntrd = (RadioButton)findViewById(R.id.infnot);
			infntrd.setOnClickListener(OnClickListener);
			
			 //s = (Spinner)findViewById(R.id.Spinner01);
			 viewsummary = (Button)findViewById(R.id.viewsummary);
			
			 ((TextView)findViewById(R.id.CSEform)).setText(Html.fromHtml(cf.redcolor+" Customer service evaluation form completed and signed by homeowner"));
			 ((TextView)findViewById(R.id.Whowaspresent)).setText(Html.fromHtml(cf.redcolor+" Who was present at inspection?"));
			 ((TextView)findViewById(R.id.didhomeowner)).setText(Html.fromHtml(cf.redcolor+" Owner/Representative satisfied with inspection "));
			 ((TextView)findViewById(R.id.didhomeownermanu)).setText(Html.fromHtml(cf.redcolor+" Customer service evaluation form completed and signed by owner / representative "));
			 ((TextView)findViewById(R.id.header_sup)).setText(Html.fromHtml("Supplemental Documents to Attach with Inspection Report"));
			
			 
			 txtfeedbackcomments=(TextView)findViewById(R.id.txtfeedbackcomments);
			 txtfeedbackcomments.setText(Html.fromHtml("Feedback Comments"));
			 othrtxt=(EditText)findViewById(R.id.other);
			 othrtxtsupp=(EditText)findViewById(R.id.othersupp);
			 othrtxtoff=(EditText)findViewById(R.id.otheroff);
			 

			
			 s1 = (Spinner)findViewById(R.id.Spinner02);
			 s2 = (Spinner)findViewById(R.id.Spinner03);
			
				  linpar = (LinearLayout) findViewById(R.id.SQ_ED_type1_parrant);
				  lintyp = (LinearLayout) findViewById(R.id.SQ_ED_type1);
				  lintxt = (TextView) findViewById(R.id.SQ_TV_type1);
			edfbcoment = (EditText)findViewById(R.id.comment);
			//edfbaddcoment= (EditText)findViewById(R.id.comment_addendum);
			//edfbcoment.setOnTouchListener(new Touch_Listener(4));
			edfbcoment.addTextChangedListener(new FB_textwatcher(1));
			//edfbaddcoment.addTextChangedListener(new FB_textwatcher(2));
			
		
			docbrws = (Button)findViewById(R.id.browsetxt);
			offbrws = (Button)findViewById(R.id.offbrowsetxt);
			docpathedit = (EditText)findViewById(R.id.pathtxt);
			
		//	docupd = (Button)findViewById(R.id.updtxt);
			
			doclst = (ListView)findViewById(R.id.ListView01);
			offlst = (ListView)findViewById(R.id.ListView02);
			doctbllst= (LinearLayout)findViewById(R.id.uploadeddoc1);
			offtbllst= (LinearLayout)findViewById(R.id.uploadeddoc2);
			
		   /* array_who=new String[5];
		    array_who[0]="--Select--";
		    array_who[1]="Owner";
		    array_who[2]="Representative";
		    array_who[3]="Agent";
		    array_who[4]="Other";*/
			
	        
		    array_doc=new String[14];
		    array_doc[0]="--Select--";
		    array_doc[1]="Acknowledgement Form";
		    array_doc[2]="CSE Form";
		    array_doc[3]="OIR 1802 Form";	
		    array_doc[4]="Roof Permit";
		    array_doc[5]="Sketch";
		    array_doc[6]="Building Permit";
		    array_doc[7]="Property Appraisal Information";
		    array_doc[8]="Field Inspection Report";
		    array_doc[9]="Field Notes";
		    array_doc[10]="Manufacturers Information Form";
		    array_doc[11]="Sinkhole Risk Proximity Report";
		    array_doc[12]="Owners Paperwork";
		    array_doc[13]="Other Information";
		    
		    
		    String arr[]=null;
		    
		    /*ArrayAdapter adapter2 = new ArrayAdapter(this,android.R.layout.simple_spinner_item, array_who);
			adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

	        s.setAdapter(adapter2);
	        
	        s.setOnItemSelectedListener(new MyOnItemSelectedListenerdata());*/
	        ArrayAdapter adapter1 = new ArrayAdapter(this,android.R.layout.simple_spinner_item, array_doc);
	        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	        s1.setAdapter(adapter1);
	        s1.setOnItemSelectedListener(new MyOnItemSelectedListenerdata1());
	        s2.setAdapter(adapter1);
	        s2.setOnItemSelectedListener(new MyOnItemSelectedListenerdata2());
		    try
		    {
		    	Cursor s=cf.SelectTablefunction(cf.Select_insp, " WHERE SI_srid='"+cf.selectedhomeid+"'");
		    	s.moveToFirst();
		    	if(s.getCount()>0)
		    	{
		    		String tmp=cf.decode(s.getString(s.getColumnIndex("SI_InspectionNames"))).trim();
		    		if(tmp.length()>0)
		    		{		    			
		    			Cursor c1 =cf.SelectTablefunction(cf.Custom_report, " Where  SRID ='"+cf.selectedhomeid+"' and IsMainInspection='0' and InspectionID!='0'");
		    			System.out.println("c1="+c1.getCount());
		    			if(c1.getCount()>0)
		    			{
		    				c1.moveToFirst();
		    				int i=0;
		    				do
		    				{
		    					String fbflag = c1.getString(c1.getColumnIndex("FBNotRequired"));
		    					System.out.println("flasg="+fbflag);
		    					tmp=tmp.replace(",","~");
		    					if(fbflag.equals("0"))
    		    				{
		    						
		    							System.out.println("what is tmp"+tmp +" matches = "+c1.getString(c1.getColumnIndex("FBNotRequired")));
			    						tmp = tmp.replaceAll("\\b"+c1.getString(c1.getColumnIndex("InspectionID"))+"\\b", "");
			    						System.out.println("final ="+tmp);
			    						if(tmp.contains("~~"))
			       					 {
			       						tmp = tmp.replaceAll("~~","~");	
			       					 }
			    						System.out.println("final last="+tmp);
		    						
    		    				}
		    					i++;		    				
		    				}while(c1.moveToNext());
		    				tmp = tmp.replace("~",",");System.out.println("te="+tmp);
		    				if(tmp.endsWith(","))
		    				{
		    					tmp = tmp.substring(0, tmp.length() - 1);
		    				}
		    			}
		    			System.out.println("tmpafetr="+tmp);
		    			
		    			Cursor c =cf.SelectTablefunction(cf.allinspnamelist, " Where  ARR_Insp_ID in ("+tmp+")  order by ARR_Custom_ID");
		    			System.out.println("some ieeuse"+c.getCount());
		    			if(c.getCount()>0)
		    			{
		    				c.moveToFirst();
		    				arr=new String[c.getCount()+1];
		    				selected_insp=new String[c.getCount()];
		    				selected_inspid=new String[c.getCount()];
		    				selected_onlineid=new String[c.getCount()];
		    				arr[0]="-Select-";
		    				for(int i=1;i<c.getCount()+1;i++,c.moveToNext())
		    				{
		    					System.out.println("some ieeuse va;llue="+cf.decode(c.getString(c.getColumnIndex("ARR_Insp_Name"))));
		    					arr[i]=cf.decode(c.getString(c.getColumnIndex("ARR_Insp_Name"))).trim();
		    					selected_insp[i-1]=cf.decode(c.getString(c.getColumnIndex("ARR_Insp_Name"))).trim();
		    					selected_inspid[i-1]=cf.decode(c.getString(c.getColumnIndex("ARR_Custom_ID"))).trim();
		    					if(selected_inspid[i-1].equals("4"))
		   			        	{
									 cf.getPolicyholderInformation(cf.selectedhomeid);
									 if(cf.Stories.equals("1") || cf.Stories.equals("2")  || cf.Stories.equals("3"))
									 {
										selected_inspid[i-1]="4";		
									 }
									 else if(cf.Stories.equals("4") || cf.Stories.equals("5")  || cf.Stories.equals("6"))
									 {
										 selected_inspid[i-1]="5";
									 }
									 else if(Integer.parseInt(cf.Stories)>=7)
									 {	
										 selected_inspid[i-1]="6";		
									 }
		   			            }
		    					selected_onlineid[i-1]=cf.decode(c.getString(c.getColumnIndex("ARR_Insp_ID"))).trim();
		    				}
		    			}
		    			/*tmp="-Select-^"+tmp;
		    			tmp=tmp.replace("^","~");
		    		
		    			arr=tmp.split("~");*/
		    		}
		    		else
		    		{
		    			arr=cf.All_insp_list;//getResources().getStringArray(R.array.Inspection_types);
		    		}
		    	}
		    	else
		    	{
		    		arr=getResources().getStringArray(R.array.Inspection_types);
		    	}
		    }
		    catch (Exception e) {
				// TODO: handle exception
			}
		    try
		    {
			 ArrayAdapter adapter_insp = new ArrayAdapter(Feedback.this,android.R.layout.simple_spinner_item, arr);
			 adapter_insp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			 insp_sp=(Spinner) findViewById(R.id.fb_insp_sp);
				insp_sp.setAdapter(adapter_insp);
				
				insp_sp.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						// TODO Auto-generated method stub
						inspection="0";
						String selected =insp_sp.getSelectedItem().toString();
						s1.setSelection(0);
						s2.setSelection(0);
						if(!selected.equals("-Select-"))
						{
							  for(int i=0;i<whopresentchk.length;i++)
			            	   {
			            		   whopresentchk[i].setChecked(false);
			            	   }
							  othrtxt.setVisibility(cf.v1.GONE);
							Cursor c =cf.SelectTablefunction(cf.allinspnamelist, " Where  ARR_Insp_Name='"+cf.encode(selected)+"'");
							if(c.getCount()>0)
							{
								c.moveToFirst();
								inspection = c.getString(c.getColumnIndex("ARR_Custom_ID"));
								inspid = c.getInt(c.getColumnIndex("ARR_Insp_ID"));
								
								
							}
							/*for(int i=0;i<cf.All_insp_list.length;i++)
							{
								System.out.println("cf.All_insp_list"+cf.All_insp_list[i]+"SEEL"+selected);
								
								if(cf.All_insp_list[i].equals(selected))
								{
									System.out.println("both equals="+cf.All_insp_list[i]);
									
									inspection=i+"";
								}
							}*/
							
							
							((View) findViewById(R.id.fb_lin)).setVisibility(View.VISIBLE);
							show_fb_info();
							 show_list();
							 showoffice_list();
							
						}
						else
						{
							for(int i=0;i<whopresentchk.length;i++)
			            	   {
			            		   whopresentchk[i].setChecked(false);
			            	   }
							  othrtxt.setVisibility(cf.v1.GONE);
							//((View) findViewById(R.id.fb_lin)).setVisibility(View.GONE);
						}
						
					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {
						// TODO Auto-generated method stub
						
					}
				});
		    }
		    catch (Exception e) {
				// TODO: handle exception
			}
			   
				
			
			
			 offlst.setOnItemClickListener(new OnItemClickListener() {
				  public void onItemClick(AdapterView<?> a, View v, int position, long id) {
					
				        final String offselarr = offnamearr[position];
				        offselected_id=offdocidarr[position];
				     
				        final Dialog dialog = new Dialog(Feedback.this);
			            dialog.getWindow().setContentView(R.layout.maindialog);
			            dialog.setTitle(offpatharr[position]);  
			            dialog.setCancelable(true);
			            final ImageView img = (ImageView) dialog.findViewById(R.id.ImageView01);
			            EditText ed1 = (EditText) dialog.findViewById(R.id.TextView01);
			            ed1.setVisibility(v1.GONE);
			            img.setVisibility(v1.GONE);
			            Button button_close = (Button) dialog.findViewById(R.id.Button01);
			    		button_close.setText("Close");//button_close.setVisibility(v2.GONE);
			            Button button_d = (Button) dialog.findViewById(R.id.Button03);
			    		button_d.setText("Delete");
			    		button_d.setVisibility(v2.VISIBLE);
			    		final Button button_view = (Button) dialog.findViewById(R.id.Button02);
			    		button_view.setText("View");
			    		button_view.setVisibility(v2.VISIBLE);
			    		
			    		final Button button_saveimage= (Button) dialog.findViewById(R.id.Button05);
			    		button_saveimage.setVisibility(v2.GONE);
			    		final LinearLayout linrotimage = (LinearLayout) dialog.findViewById(R.id.linrotation);
			    		linrotimage.setVisibility(v2.GONE);
			    		final Button rotateright= (Button) dialog.findViewById(R.id.rotateright);
			    		rotateright.setVisibility(v2.GONE);
			    		final Button rotateleft= (Button) dialog.findViewById(R.id.rotateleft);
			    		rotateleft.setVisibility(v2.GONE);
			    		
			    		button_d.setOnClickListener(new OnClickListener() {
				            public void onClick(View v) {
				            
				            AlertDialog.Builder builder = new AlertDialog.Builder(Feedback.this);
				   			builder.setMessage("Do you want to delete the selected document?")
			   			       .setCancelable(false)
			   			       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			   			           public void onClick(DialogInterface dialog, int id) {
			   			        	System.out.println("insss111="+inspection);
			   			        	   /*if(inspection.equals("4"))
			   			        	   {
			   			        		   System.out.println("inside 111");
			   			        		   inspection = getinspnumber(inspection);
			   			        	   }*/
			   			        	   
			   			        	   cf.arr_db.execSQL("Delete From " +cf. FeedBackDocumentTable + "  WHERE ARR_FI_D_SRID ='" + cf.selectedhomeid + "' and ARR_FI_DocumentId='"+offselected_id+"' and ARR_FI_D_Inspection='"+inspection+"' and ARR_FI_D_IsOfficeUse=1");
			   			        	cf.ShowToast("Document has been deleted successfully.",1);
			   			        	
			   					   
			   					    showoffice_list();
			   			           }
			   			       })
			   			       .setNegativeButton("No", new DialogInterface.OnClickListener() {
			   			           public void onClick(DialogInterface dialog, int id) {
			   			                dialog.cancel();
			   			           }
			   			       });
			   			 builder.show();
			            
			   			 dialog.cancel();
		            }
	    		           	
				  	 });
			    		button_view.setOnClickListener(new OnClickListener() {
				            public void onClick(View v) {
				            	currnet_rotated=0;
				            	
				            	if(offselarr.equals(""))
				            	{
				            		img.setVisibility(v2.VISIBLE);
				            	}
				            	else if(offselarr.endsWith(".pdf"))
							     {
				            		img.setVisibility(v2.GONE);
				            		String tempstr ;
				            		if(offselarr.contains("file://"))
				    		    	{
				    		    		 tempstr = offselarr.replace("file://","");		    		
				    		    	}
				            		else
				            		{
				            			tempstr = offselarr;
				            		}
				            		File file = new File(tempstr);
								 
					                 if (file.exists()) {
					                	Uri path = Uri.fromFile(file);
					                    Intent intent = new Intent(Intent.ACTION_VIEW);
					                    intent.setDataAndType(path, "application/pdf");
					                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					 
					                    try {
					                        startActivity(intent);
					                    } 
					                    catch (ActivityNotFoundException e) {
					                    	cf.ShowToast("No Application available to view PDF.",1);
					                       
					                    }
					               
					                }
							     }
				            	else
				            	{
				            		Bitmap bitmap2=ShrinkBitmap(offselarr,400,400);
				            		BitmapDrawable bmd2 = new BitmapDrawable(bitmap2);
				            		img.setImageDrawable(bmd2);
			    					img.setVisibility(v2.VISIBLE);
			    					rotateright.setVisibility(v2.VISIBLE);
			    					rotateleft.setVisibility(v2.VISIBLE);
			    					button_view.setVisibility(v2.GONE);
						            button_saveimage.setVisibility(v2.VISIBLE);
						            linrotimage.setVisibility(View.VISIBLE);
				            	}
				            	
				            }
			    		});
			    		rotateleft.setOnClickListener(new OnClickListener() {  			
			    			public void onClick(View v) {

			    				// TODO Auto-generated method stub
			    			
			    				System.gc();
			    				currnet_rotated-=90;
			    				if(currnet_rotated<0)
			    				{
			    					currnet_rotated=270;
			    				}

			    				
			    				Bitmap myImg;
			    				try {
			    					myImg = BitmapFactory.decodeStream(new FileInputStream(offselarr));
			    					Matrix matrix =new Matrix();
			    					matrix.reset();
			    					//matrix.setRotate(currnet_rotated);
			    					
			    					matrix.postRotate(currnet_rotated);
			    					
			    					 rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
			    					        matrix, true);
			    					 
			    					 img.setImageBitmap(rotated_b);

			    				} catch (FileNotFoundException e) {
			    					// TODO Auto-generated catch block
			    					e.printStackTrace();
			    				}
			    				catch (Exception e) {
			    					
			    				}
			    				catch (OutOfMemoryError e) {
			    					
			    					System.gc();
			    					try {
			    						myImg=null;
			    						System.gc();
			    						Matrix matrix =new Matrix();
			    						matrix.reset();
			    						//matrix.setRotate(currnet_rotated);
			    						matrix.postRotate(currnet_rotated);
			    						myImg= cf.ShrinkBitmap(offselarr, 800, 800);
			    						rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
			    						System.gc();
			    						img.setImageBitmap(rotated_b); 

			    					} catch (Exception e1) {
			    						// TODO Auto-generated catch block
			    						e1.printStackTrace();
			    					}
			    					 catch (OutOfMemoryError e1) {
			    							// TODO Auto-generated catch block
			    						 cf.ShowToast("You cannot rotate this image. Image size is too large.",1);
			    					}
			    				}

			    			
			    			}
			    		});
			    		rotateright.setOnClickListener(new OnClickListener() {
			    		    public void onClick(View v) {
			    		    	
			    		    	currnet_rotated+=90;
			    				if(currnet_rotated>=360)
			    				{
			    					currnet_rotated=0;
			    				}
			    				
			    				Bitmap myImg;
			    				try {
			    					myImg = BitmapFactory.decodeStream(new FileInputStream(offselarr));
			    					Matrix matrix =new Matrix();
			    					matrix.reset();
			    					//matrix.setRotate(currnet_rotated);
			    					
			    					matrix.postRotate(currnet_rotated);
			    					
			    					 rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
			    					        matrix, true);
			    					 
			    					 img.setImageBitmap(rotated_b); 

			    				} catch (FileNotFoundException e) {
			    					//.println("FileNotFoundException "+e.getMessage()); 
			    					// TODO Auto-generated catch block
			    					e.printStackTrace();
			    				}
			    				catch(Exception e){}
			    				catch (OutOfMemoryError e) {
			    				//	.println("comes in to out ot mem exception");
			    					System.gc();
			    					try {
			    						myImg=null;
			    						System.gc();
			    						Matrix matrix =new Matrix();
			    						matrix.reset();
			    						//matrix.setRotate(currnet_rotated);
			    						matrix.postRotate(currnet_rotated);
			    						myImg= cf.ShrinkBitmap(offselarr, 800, 800);
			    						rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
			    						System.gc();
			    						img.setImageBitmap(rotated_b); 

			    					} catch (Exception e1) {
			    						// TODO Auto-generated catch block
			    						e1.printStackTrace();
			    					}
			    					 catch (OutOfMemoryError e1) {
			    							// TODO Auto-generated catch block
			    						 cf.ShowToast("You cannot rotate this image. Image size is too large.",1);
			    					}
			    				}

			    		    }
			    		});
			    		
			    		button_saveimage.setOnClickListener(new OnClickListener() {
							
							public void onClick(View v) {
								// TODO Auto-generated method stub
								
								if(currnet_rotated>0)
								{ 

									try
									{
										/**Create the new image with the rotation **/
								
										String current=MediaStore.Images.Media.insertImage(getContentResolver(), rotated_b, "My bitmap", "My rotated bitmap");
										 ContentValues values = new ContentValues();
										  values.put(MediaStore.Images.Media.ORIENTATION, 0);
										  Feedback.this.getContentResolver().update(Uri.parse(current), values, MediaStore.Images.Media.DATA+ "=?", new String[] { current } );
										if(current!=null)
										{
										String path=getPath(Uri.parse(current));
										File fout = new File(offselarr);
										fout.delete();
										/** delete the selected image **/
										File fin = new File(path);
										/** move the newly created image in the slected image pathe ***/
										fin.renameTo(new File(offselarr));
										cf.ShowToast("Document saved successfully.",1);dialog.cancel();
										show_list();
										
										
									}
									} catch(Exception e)
									{
										//.println("Error occure while rotate the image "+e.getMessage());
									}
									
								}
								else
								{
									dialog.cancel();
									show_list();
								}
								
							}
						});
			    		button_close.setOnClickListener(new OnClickListener() {
				            public void onClick(View v) {
				            	dialog.cancel();
				            }
			    		});
			    		dialog.show();
				     
				  }  
		   });
			 adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_row, items) {
			        
			        ViewHolder holder;
			        Drawable icon;
			 
			        class ViewHolder {
			                ImageView icon;
			                TextView title;
			        }
			 
			        public View getView(int position, View convertView,ViewGroup parent) {
			                final LayoutInflater inflater = (LayoutInflater) getApplicationContext()
			                                .getSystemService(
			                                                Context.LAYOUT_INFLATER_SERVICE);
			 
			                if (convertView == null) {
			                        convertView = inflater.inflate(
			                                        R.layout.list_row, null);
			 
			                        holder = new ViewHolder();
			                        holder.icon = (ImageView) convertView
			                                        .findViewById(R.id.icon);
			                        holder.title = (TextView) convertView
			                                        .findViewById(R.id.title);
			                        convertView.setTag(holder);
			                } else {
			                        // view already defined, retrieve view holder
			                        holder = (ViewHolder) convertView.getTag();
			                }              
			  
			                holder.title.setText(items[position]);
			                if(items[position].equals("gallery"))
			                {
			                	 Drawable tile = getResources().getDrawable(R.drawable.gallery);
			                	 holder.icon.setImageDrawable(tile);
			                }
			                else  if(items[position].equals("camera"))
			                {
			                	 Drawable tile1 = getResources().getDrawable(R.drawable.iconphoto);
			                	 holder.icon.setImageDrawable(tile1);
			                }
			       
	                else  if(items[position].equals("pdf"))
	                {
	                	 Drawable tile2 = getResources().getDrawable(R.drawable.pdficon);
	                	 holder.icon.setImageDrawable(tile2);
	                }
			                
			 
			                return convertView;
			        }
			};
			 adapter11 = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_row, items) {
			        
			        ViewHolder holder;
			        Drawable icon;
			 
			        class ViewHolder {
			                ImageView icon;
			                TextView title;
			        }
			
			        public View getView(int position, View convertView,ViewGroup parent) {
			                final LayoutInflater inflater = (LayoutInflater) getApplicationContext()
			                                .getSystemService(
			                                                Context.LAYOUT_INFLATER_SERVICE);
			 
			                if (convertView == null) {
			                        convertView = inflater.inflate(
			                                        R.layout.list_row, null);
			 
			                        holder = new ViewHolder();
			                        holder.icon = (ImageView) convertView
			                                        .findViewById(R.id.icon);
			                        holder.title = (TextView) convertView
			                                        .findViewById(R.id.title);
			                        convertView.setTag(holder);
			                } else {
			                        // view already defined, retrieve view holder
			                        holder = (ViewHolder) convertView.getTag();
			                }              
			  
			                holder.title.setText(items[position]);
			                if(items[position].equals("pdf"))
	                {
	                	 Drawable tile2 = getResources().getDrawable(R.drawable.pdficon);
	                	 holder.icon.setImageDrawable(tile2);
	                }
			                
			 
			                return convertView;
			        }
			};
			viewsummary.setOnClickListener(new OnClickListener() {
	            public void onClick(View v) {
	            	sf.displayphotosummary(cf.selectedhomeid);
	            }
    		});
	}
	
	class  OwnerrdioClicker implements OnClickListener
	{
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			for(int i=0;i<ownerchk.length;i++)
			{
				if(v.getId()==ownerchk[i].getId())
				{	
					ownerchk[i].setChecked(true);
				}
				else
				{
					ownerchk[i].setChecked(false);	
				}
			}
		}
	}
	
	
	private String getinspnumber(String inspection)
	{
		cf.getPolicyholderInformation(cf.selectedhomeid);
		System.out.println(cf.Stories+"Stories");
		if(cf.Stories.equals("1") || cf.Stories.equals("2")  || cf.Stories.equals("3"))
		{
			inspection="4";		
		}
		else if(cf.Stories.equals("4") || cf.Stories.equals("5")  || cf.Stories.equals("6"))
		{
			inspection="5";			
		}
		else if(Integer.parseInt(cf.Stories)>=7)
		{	
			inspection="6";		
		}
		System.out.println(inspection+"inspec");
		return inspection;
	}
	private void show_fb_info() {
		// TODO Auto-generated method stub


		
		 try {System.out.println("insss2222="+inspection);
			
			 
			 if(inspection.equals("10"))
			 {
				    Cursor c2 =cf.SelectTablefunction(cf.allinspnamelist, " Where  ARR_Insp_Name='"+cf.encode(insp_sp.getSelectedItem().toString())+"'");
					if(c2.getCount()>0)
					{
						c2.moveToFirst();
						inspectionid = c2.getString(c2.getColumnIndex("ARR_Insp_ID"));
					} 
					c1 = cf.arr_db.rawQuery("SELECT * FROM "+ cf.FeedBackInfoTable	+ " WHERE ARR_FI_Srid='"+cf.selectedhomeid+"' and ARR_ISCUSTOM='"+inspectionid+"'",null);
		              
			 }
			 else
			 {
				 if(inspection.equals("4"))
		         {
					  inspection = getinspnumber(inspection);
		         }
				 
				   c1 = cf.arr_db.rawQuery("SELECT * FROM "+ cf.FeedBackInfoTable	+ " WHERE ARR_FI_Srid='"+cf.selectedhomeid+"' and ARR_FI_Inspection='"+inspection+"'",null);
	              
			 }
			 
			 
			 rws = c1.getCount(); 			    
             System.out.println("rws="+rws);  
             if(rws==0)
  			 {
            	 for(int i=0;i<whopresentchk.length;i++)
          	     {
            		 ownerchk[i].setChecked(false);
          	     }
            	 
            	 
            	 /*  insyrd.setChecked(false);insnrd.setChecked(false);insntrd.setChecked(false);insnard.setChecked(false);
            	   infyrd.setChecked(false);infnrd.setChecked(false);infntrd.setChecked(false);*/
            	   othrtxt.setText("");
            	   othrtxt.setVisibility(cf.v1.GONE);
            	   for(int i=0;i<whopresentchk.length;i++)
            	   {
            		   whopresentchk[i].setChecked(false);
            	   }
            	   edfbcoment.setText("");
            	
            	   ((RadioButton) findViewById(R.id.insna)).setChecked(true); 
            	  // edfbaddcoment.setText("");
            	  // s.setSelection(0);
  			 }
  			 else
  			 {
  				     int Column1=c1.getColumnIndex("ARR_FI_IsCusServiceCompltd"); 
    		     	 int Column2=c1.getColumnIndex("ARR_FI_PresentatInspection"); 
    		     	 int Column3=c1.getColumnIndex("ARR_FI_IsInspectionPaperAvbl");
    		     	 int Column4=c1.getColumnIndex("ARR_FI_IsManufacturerInfo");
    		     	 int Column5=c1.getColumnIndex("ARR_FI_FeedbackComments");
    		     	 int Column6=c1.getColumnIndex("ARR_FI_othertxt");
    		     	 c1.moveToFirst();
    		     	 if(c1!=null)   
    		     	 {  
    		     		do{
    		     			 String csc=cf.decode(c1.getString(Column1)); 
    		     			 String pati=cf.decode(c1.getString(Column2));
    		     			 String ipa=cf.decode(c1.getString(Column3));
    		     			 String imi=cf.decode(c1.getString(Column4));
    		     			 String fc=cf.decode(c1.getString(Column5));
    		     			 if(csc.equals("1"))
    						 {
    							 cusyrd.setChecked(true);cusnrd.setChecked(false);isCSC="Yes";
    						 }
    						 else
    						 {
    							 cusyrd.setChecked(false);cusnrd.setChecked(true);isCSC="No";
    						 }
    		     			 
    		     			cf.setvaluechk1(pati,whopresentchk,((EditText)findViewById(R.id.other)));
    		     			 
    		     			 /*int spinnerPosition = adapter2.getPosition(pati);
    				         s.setSelection(spinnerPosition);*/
    		     			 /*if(ipa.equals("Yes"))
    						 {
    							 insyrd.setChecked(true);insnrd.setChecked(false);insntrd.setChecked(false);insnard.setChecked(false);
    							 isInsavail="Yes";
    						 }
    						 else if(ipa.equals("No"))
    						 {
    							 insyrd.setChecked(false);insnrd.setChecked(true);insntrd.setChecked(false);insnard.setChecked(false);
    							 isInsavail="No";
    						 }
    						 else if(ipa.equals(getResources().getString(R.string.nd)))
    						 {
    							 insyrd.setChecked(false);insnrd.setChecked(false);insntrd.setChecked(true);insnard.setChecked(false);
    							 isInsavail=getResources().getString(R.string.nd);
    						 }
    						 else if(ipa.equals(getResources().getString(R.string.na)))
    						 {
    							 insyrd.setChecked(false);insnrd.setChecked(false);insntrd.setChecked(false);insnard.setChecked(true);
    							 isInsavail=getResources().getString(R.string.na);
    						 }*/
    		     			 
    		     			cf.setRadioBtnValue(ipa,ownerchk);
    		     			 
    		     			 
    		     			 if(imi.equals("Yes"))
   						     {
    		     				infyrd.setChecked(true);infnrd.setChecked(false);infntrd.setChecked(false);
    		     				isManf="Yes";
   						     }
    		     			 else if(imi.equals("No"))
   						     {
     		     				infyrd.setChecked(false);infnrd.setChecked(true);infntrd.setChecked(false);
     		     				isManf="No";
    						 }
    		     			 else if(imi.equals(getResources().getString(R.string.nd)))
  						     {
    		     				infyrd.setChecked(false);infnrd.setChecked(false);infntrd.setChecked(true);
    		     				isManf=getResources().getString(R.string.nd);
   						     }
    		     			else if(imi.equals(getResources().getString(R.string.na)))
  						     {
    		     				infyrd.setChecked(false);infnrd.setChecked(false);infntrd.setChecked(false);
    		     				isManf=getResources().getString(R.string.na);
   						     }
    		     			cf.showing_limit(fc,linpar,lintyp,lintxt,"1498");	
    		     			edfbcoment.setMaxWidth(cf.wd-160);
    		     			edfbcoment.setText(fc);
    		     			/*edfbaddcoment.setMaxWidth(cf.wd-160);
    		     			edfbaddcoment.setText(cf.decode(c1.getString(c1.getColumnIndex("ARR_FI_FeedbackComments_addendum"))));*/
    		     			
    		     			
    		     			if(!c1.getString(c1.getColumnIndex("ARR_FI_othertxt")).equals(""))
    		     			{
    		     				othrtxt.setText(cf.decode(c1.getString(c1.getColumnIndex("ARR_FI_othertxt"))));
    		     				othrtxt.setVisibility(cf.v1.VISIBLE);
    		     			}
    		     			
    		     		}while(c1.moveToNext());   
    		     	 }c1.close(); 
  			 }
		 }
		 catch(Exception e)
		 {
			 Log.i(TAG,"error= "+e.getMessage());
		 }
	}
	class FB_textwatcher implements TextWatcher
	{
         public int type;
         FB_textwatcher(int type)
		{
			this.type=type;
		}
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			if(this.type==1)
			{
				cf.showing_limit(s.toString(),lintxt,500);
			}
			/*if(this.type==2)
			{
				cf.showing_limit(s.toString(), ((TextView)findViewById(R.id.SQ_TV_type2)), 500);//(,linpar,lintyp,lintxt,"500");
			}*/
				
		}

		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}

		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			
		}
		
	}		 

	RadioButton.OnClickListener OnClickListener =  new RadioButton.OnClickListener()
	{

	  public void onClick(View v) {
		    switch(v.getId())
			 {
		    case R.id.customeryes:
		    	isCSC="yes";cusyrd.setChecked(true);cusnrd.setChecked(false);
		    	break;
		    case R.id.customerno:
		    	isCSC="No";cusyrd.setChecked(false);cusnrd.setChecked(true);
		    	break;
		    /*case R.id.insyes:
		    	isInsavail="Yes";insyrd.setChecked(true);insnrd.setChecked(false);insntrd.setChecked(false);insnard.setChecked(false);
		    	break;
		    case R.id.insno:
		    	isInsavail="No";insyrd.setChecked(false);insnrd.setChecked(true);insntrd.setChecked(false);insnard.setChecked(false);
		    	break;
		    case R.id.insnot:
		    	isInsavail=getResources().getString(R.string.nd);insyrd.setChecked(false);insnrd.setChecked(false);insntrd.setChecked(true);insnard.setChecked(false);
		    	break;
		    case R.id.insna:
		    	isInsavail=getResources().getString(R.string.na);insyrd.setChecked(false);insnrd.setChecked(false);insntrd.setChecked(false);insnard.setChecked(true);		    	
		    	break;*/
		    case R.id.infyes:
		    	isManf="Yes";infyrd.setChecked(true);infnrd.setChecked(false);infntrd.setChecked(false);
		    	break;
		    case R.id.infno:
		    	isManf="No";infyrd.setChecked(false);infnrd.setChecked(true);infntrd.setChecked(false);
		    	break;
		    case R.id.infnot:
		    	isManf=getResources().getString(R.string.nd);infyrd.setChecked(false);infnrd.setChecked(false);infntrd.setChecked(true);
		    	break;
			 }
	  }
	  };
	private int ii;
	private String[] pdfsample;
	private boolean load_comment=true;	          
	public void clicker(View v) {
		String selected =insp_sp.getSelectedItem().toString();
		if(!selected.equals("-Select-"))
		{
		switch (v.getId()) {
		
		 case R.id.save:System.out.println("insss33333="+inspection);
			/* if(inspection.equals("4"))
	         {
				 System.out.println("inside 333");
	           inspection = getinspnumber(inspection);
	         }*/
			 try
			 {
				 curfeed1 = cf.arr_db.rawQuery("SELECT * FROM "+ cf.FeedBackDocumentTable	+ " WHERE ARR_FI_D_SRID='"+cf.selectedhomeid+"' and ARR_FI_D_Inspection='"+inspection+"' and ARR_FI_D_IsOfficeUse=0",null);
				 feedrws1 = curfeed1.getCount();
				 
				 curfeed2 = cf.arr_db.rawQuery("SELECT * FROM "+ cf.FeedBackDocumentTable	+ " WHERE ARR_FI_D_SRID='"+cf.selectedhomeid+"' and ARR_FI_D_Inspection='"+inspection+"' and ARR_FI_D_IsOfficeUse=1",null);
				 feedrws2 = curfeed2.getCount();
			 }
			 catch(Exception e)
			 {
				// .println(" Error  save = "+e.getMessage());	 
			 }

		    	/*strfbcoment=edfbcoment.getText().toString();
		    	if(s.getSelectedItem().toString().trim().equals("Other"))
		    	{
		    		strother = othrtxt.getText().toString();
		    		
		    	}
		    	else
		    	{
		    		strother="";
		    	}*/
		    	
		    	strwhopresent= cf.getselected_chk(whopresentchk);
				String whopresotherval =(whopresentchk[whopresentchk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.other)).getText().toString():""; 
				strwhopresent+=whopresotherval;
				 if(strwhopresent.equals(""))
				 {
					 cf.ShowToast("Please select the option for Who was present at inspection?" , 0);
				 }
				 else
				 {
					 if(whopresentchk[whopresentchk.length-1].isChecked())
					 {	 
						 if(whopresotherval.trim().equals("&#40;"))
						 {
							 cf.ShowToast("Please enter the other text for Who was present at inspection?", 0);
							 cf.setFocus(((EditText)findViewById(R.id.other)));
						 }
						 else
						 {
							 feeddocvalidation();	
						 }					 
					 }
					 else
					 {
						 feeddocvalidation();
					 }
				 }
		    	break;
		 	case R.id.whop_chk4:
				if(((CheckBox)findViewById(R.id.whop_chk4)).isChecked())
				{
					((EditText)findViewById(R.id.other)).setVisibility(cf.v1.VISIBLE);
					cf.setFocus((EditText)findViewById(R.id.other));
				}
				else
				{
					((EditText)findViewById(R.id.other)).setVisibility(cf.v1.GONE);
					((EditText)findViewById(R.id.other)).setText("");
				}				
				break;
		    case R.id.hme:
		    	cf.gohome();
		    	break;
		    case R.id.browsetxt:
		    	c=1;
		    	if(!"--Select--".equals(s1.getSelectedItem().toString())&& (!array_doc[13].equals(s1.getSelectedItem().toString()) || !othrtxtsupp.getText().toString().equals("") ))
    			{
		 				try
		 				{
		  					Cursor c11 = cf.arr_db.rawQuery("SELECT * FROM " + cf.FeedBackDocumentTable
				 					+ " WHERE ARR_FI_D_SRID='" + cf.selectedhomeid + "' and ARR_FI_D_IsOfficeUse='0' and ARR_FI_D_Inspection='"+inspection+"' order by ARR_FI_D_ImageOrder", null);
		 					
		 					String[] selected_paht =null;
		 					int rws=0;
		 		 			/***We call the centralized image selection part **/
				 			if(c11.getCount()>0)
				 			{
				 				rws=c11.getCount();
				 				c11.moveToFirst();
				 				selected_paht = new String[c11.getCount()];
				 				
				 				
				 				for(int i=0;i<c11.getCount();i++)
				 				{
				 					selected_paht[i]=cf.decode(c11.getString(c11.getColumnIndex("ARR_FI_D_FileName"))); 
				 					
				 					c11.moveToNext();
				 				}
				 			}
		 					Intent reptoit1 = new Intent(this,Select_phots.class);
		 					Bundle b=new Bundle();
		 					
		 					reptoit1.putExtra("Selectedvalue",selected_paht); /**Send the already selected image **/
		 					reptoit1.putExtra("office_use","false"); /**Send the is for office use **/
		 					reptoit1.putExtra("Db_count",rws); /**SendDatabase count ofr the image order**/
		 					reptoit1.putExtra("Maximumcount", 0);/**Total count of image in the database **/
		 					reptoit1.putExtra("Total_Maximumcount", 0); /***Total count of image we need to accept**/
		 					reptoit1.putExtra("unlimeted", "true"); /***Total count of image we need to accept**/
		 					reptoit1.putExtra("PDF", "true"); /***Total count of image we need to accept**/
		 					startActivityForResult(reptoit1,121); /** Call the Select image page in the idma application  image ***/
		 				}  
		 				catch (ActivityNotFoundException e) {
		 					// TODO: handle exception
		 					cf.ShowToast("Your current Inspection Depot Mobile Application not support this feature", 0);
						}
		 				catch (Exception e) {
							// TODO: handle exception
						}
    		      //  show();
    			}
		    	else
		    	{
		    		if("--Select--".equals(s1.getSelectedItem().toString()))
		    		{
		    			cf.ShowToast("Please select the Document Title.",1);
		    		}
		    		else
		    		{
		    			
		    			cf.ShowToast("Please enter the Other text for Other Information.",1);
		    			cf.setFocus(othrtxtsupp);
		    		}
		    		
		    	}
		    	break;
		    case R.id.offbrowsetxt:
		    	c=2;selectedImagePath="";
		    	if(!"--Select--".equals(s2.getSelectedItem().toString()) && (!array_doc[13].equals(s2.getSelectedItem().toString()) || !othrtxtoff.getText().toString().equals("") ) )
    			{
		    		try
		    		{
		    		Cursor c11 = cf.arr_db.rawQuery("SELECT * FROM " + cf.FeedBackDocumentTable
		 					+ " WHERE ARR_FI_D_SRID='" + cf.selectedhomeid + "' and ARR_FI_D_IsOfficeUse='1' and ARR_FI_D_Inspection='"+inspection+"' order by ARR_FI_D_ImageOrder", null);
 					String[] selected_paht =null;
		 			/***We call the centralized image selection part **/
		 			if(c11.getCount()>0)
		 			{
		 				c11.moveToFirst();
		 				selected_paht = new String[c11.getCount()];
		 				
		 				
		 				for(int i=0;i<c11.getCount();i++)   
		 				{
		 					
		 					selected_paht[i]=cf.decode(c11.getString(c11.getColumnIndex("ARR_FI_D_FileName"))); 
		 					c11.moveToNext();
		 				}
		 			}
 					Intent reptoit1 = new Intent(this,Select_phots.class);
 					Bundle b=new Bundle();
 					
 					reptoit1.putExtra("Selectedvalue",selected_paht); /**Send the already selected image **/
 					reptoit1.putExtra("office_use","true"); /**Send the is for office use **/
 					reptoit1.putExtra("Db_count",rws); /**SendDatabase count ofr the image order**/
 					reptoit1.putExtra("Maximumcount", 0);/**Total count of image in the database **/
 					reptoit1.putExtra("Total_Maximumcount", 0); /***Total count of image we need to accept**/
 					reptoit1.putExtra("unlimeted", "true"); /***Total count of image we need to accept**/
 					reptoit1.putExtra("PDF", "true"); /***Total count of image we need to accept**/
 				//reptoit1.setClassName("com.idinspection","com.idinspection.Select_phots");
 					startActivityForResult(reptoit1,121); /** Call the Select image page in the idma application  image ***/
 				}  
 				catch (ActivityNotFoundException e) {
 					// TODO: handle exception
 					cf.ShowToast("Your current Inspection Depot Mobile Application not support this feature", 0);
				}
 				catch (Exception e) {
					// TODO: handle exception
				}
    			}
		    	else
		    	{
		    		if("--Select--".equals(s2.getSelectedItem().toString()))
		    		{
		    			cf.ShowToast("Please select the Document Title.",1);
		    		}
		    		else
		    		{
		    			
		    			cf.ShowToast("Please enter the Other text for Other Information.",1);
		    			cf.setFocus(othrtxtoff);
		    		}
		    		
		    	}
		    	break;
		    case R.id.browse_clear:
		    	Clear_all_docu(0);
		    break;
		    case R.id.offbrowse_clear:
		    	Clear_all_docu(1);
			break;
		    case R.id.load_comments:
				int len=edfbcoment.getText().toString().length();
				
				if(load_comment )
				{
					load_comment=false;
					int loc[] = new int[2];
					v.getLocationOnScreen(loc);
					cf.loadinsp_n="General Information";
					cf.loadinsp_q=cf.getResourcesvalue(R.string.gen_8);
					Intent in1 = cf.loadComments("Feedback Comments",loc);
					in1.putExtra("length", len);
					in1.putExtra("id",R.id.load_comments );
					in1.putExtra("max_length", 500);
					startActivityForResult(in1, cf.loadcomment_code);
				}
				break;
		}
		}
		else
		{
			if(v.getId()==R.id.hme)
			{
				Intent homepage = new Intent(Feedback.this,PolicyHolder.class);
				startActivity(homepage);
			}
			else
			{
				cf.ShowToast("Please select Inspection Type ", 0);
			}
		}
		
	}
	private void feeddocvalidation() {
		// TODO Auto-generated method stub
		eownerchk_value= cf.getslected_radio(ownerchk);
    	// String eservmainchk_valueotr=(servmainchk[servmainchk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.sm_otr)).getText().toString():""; // append the other text value in to the selected option
    	 //eservmainchk_value+=eservmainchk_valueotr;

		
		
		if(eownerchk_value.equals(""))
		{
			cf.ShowToast("Please select Owner/Representative Satisfied with Inspection .",1);
		}
		else if(isManf.equals(""))
    	{
    		cf.ShowToast("Please select Customer Service Evaluation Form Completed and Signed by Owner / Representative .",1);
    		
    	}
    	else
    	{
    				if(rws==0)
		    		{
		    			try
		    			{
		    			
		    				for(int l=0;l<selected_insp.length;l++)
						    {
								
		    					cf.arr_db.execSQL("INSERT INTO "
										+  cf.FeedBackInfoTable
										+ " (ARR_FI_Srid,ARR_FI_IsCusServiceCompltd,ARR_FI_PresentatInspection,ARR_FI_IsInspectionPaperAvbl,ARR_FI_IsManufacturerInfo,ARR_FI_FeedbackComments,ARR_FI_CreatedOn,ARR_FI_othertxt,ARR_FI_Inspection,ARR_ISCUSTOM)"
										+ " VALUES ('" + cf.selectedhomeid + "','" + cf.encode(isCSC)+"','"+cf.encode(strwhopresent)+"','"+cf.encode(eownerchk_value) + "','"
										+ cf.encode(isManf) + "','" + cf.encode(strfbcoment)+"','"+cd+"','"+cf.encode(((EditText)findViewById(R.id.other)).getText().toString().trim())+"','"+selected_inspid[l]+"','"+selected_onlineid[l]+"')");
		    				
						    System.out.println("INSERT INTO "
									+  cf.FeedBackInfoTable
									+ " (ARR_FI_Srid,ARR_FI_IsCusServiceCompltd,ARR_FI_PresentatInspection,ARR_FI_IsInspectionPaperAvbl,ARR_FI_IsManufacturerInfo,ARR_FI_FeedbackComments,ARR_FI_CreatedOn,ARR_FI_othertxt,ARR_FI_Inspection,ARR_ISCUSTOM)"
									+ " VALUES ('" + cf.selectedhomeid + "','" + cf.encode(isCSC)+"','"+cf.encode(strwhopresent)+"','"+cf.encode(eownerchk_value) + "','"
									+ cf.encode(isManf) + "','" + cf.encode(strfbcoment)+"','"+cd+"','"+cf.encode(((EditText)findViewById(R.id.other)).getText().toString().trim())+"','"+selected_inspid[l]+"','"+selected_onlineid[l]+"')");
						    
						    }

		    				
		    				
		    			/*	cf.arr_db.execSQL("INSERT INTO "
								+  cf.FeedBackInfoTable
								+ " (ARR_FI_Srid,ARR_FI_IsCusServiceCompltd,ARR_FI_PresentatInspection,ARR_FI_IsInspectionPaperAvbl,ARR_FI_IsManufacturerInfo,ARR_FI_FeedbackComments,ARR_FI_CreatedOn,ARR_FI_othertxt,ARR_FI_Inspection,ARR_ISCUSTOM)"
								+ " VALUES ('" + cf.selectedhomeid + "','" + cf.encode(isCSC)+"','"+cf.encode(strpresatins)+"','"+cf.encode(isInsavail) + "','"
								+ cf.encode(isManf) + "','" + cf.encode(strfbcoment)+"','"+cd+"','"+cf.encode(strother)+"','"+inspection+"','"+inspid+"')");
		    				*/		    					
		    		    		cf.ShowToast("Feedback saved successfully.",1);
		    		    		 inspection="-Select-";
				    			 show_fb_info();
								 show_list();
								 showoffice_list();
								 insp_sp.setSelection(0);
								 edfbcoment.clearFocus();
		    		    		 //cf.netalert("MAP");
		    		    		//Intent myintent=new Intent(Feedback.this,Maps.class);
		    		    		  //cf.putExtras(myintent);
		    				 		// startActivity(myintent);
		    				}
		    				catch(Exception e)
		    				{
		    					cf.ShowToast("There is a problem in saving data.",1);
		    				}
		    		}
		    		else
		    		{
		    			try
		    			{
		    				
		    			 cf.arr_db.execSQL("UPDATE " +  cf.FeedBackInfoTable + " SET ARR_FI_IsCusServiceCompltd='"+ cf.encode(isCSC) + "',ARR_FI_PresentatInspection='"+ cf.encode(strwhopresent)+"',ARR_FI_othertxt='"+cf.encode(((EditText)findViewById(R.id.other)).getText().toString().trim())+"',ARR_FI_IsInspectionPaperAvbl='"+cf.encode(eownerchk_value)+"',ARR_FI_IsManufacturerInfo='"+cf.encode(isManf)+"',ARR_FI_FeedbackComments='"+cf.encode(strfbcoment)+"',ARR_FI_CreatedOn='"+cd+"' WHERE ARR_FI_Srid ='" + cf.selectedhomeid + "' and ARR_FI_Inspection='"+inspection+"' and ARR_ISCUSTOM='"+inspid+"' ");
		    			 flagstr=0;
		    			 cf.ShowToast("Feedback saved successfully.",1);
		    			 inspection="-Select-";
		    			 show_fb_info();
						 show_list();
						 showoffice_list();
						 insp_sp.setSelection(0);
						 
						 edfbcoment.clearFocus();
		    			 //cf.netalert("MAP");
		    			// Intent myintent=new Intent(Feedback.this,Maps.class);
    		    		//  cf.putExtras(myintent);
    				 		// startActivity(myintent);
				 			
		    		}
    				catch(Exception e)
    				{
    					cf.ShowToast("There is a problem in saving your data.",1);
    				}
		    			
		        	}
    		
    		
    	}
	}
	private void Clear_all_docu(final int i) {
		// TODO Auto-generated method stub
		 String s="",offuse="0";
		if(i==0)
		{
		 s="Supplemental Document";
		 
		}
		else if(i==1)
		{
			 s="For Office Document";
			 offuse="1";
			 
		}
		final String of_u=offuse;
		AlertDialog.Builder b =new Builder(Feedback.this); 
		b.setIcon(R.drawable.alertmsg);
		b.setTitle("Confirmation");
		b.setMessage("Are you sure? Do you want to delete all the documents in "+s+"?");
		b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			  
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				String delinsp_id="";
				if(inspection.equals("10"))
				{
					Cursor c2 =cf.SelectTablefunction(cf.allinspnamelist, " Where  ARR_Insp_Name='"+cf.encode(insp_sp.getSelectedItem().toString())+"'");
					if(c2.getCount()>0)
					{
						c2.moveToFirst();
						delinsp_id = c2.getString(c2.getColumnIndex("ARR_Insp_ID"));
					} 
					cf.arr_db.execSQL("DELETE  FROM " + cf.FeedBackDocumentTable + " WHERE ARR_FI_D_SRID='" + cf.selectedhomeid + "' and ARR_FI_D_IsOfficeUse='"+of_u+"' and ARR_ISCUSTOM='"+delinsp_id+"'");
				}
				else
				{
					cf.arr_db.execSQL("DELETE  FROM " + cf.FeedBackDocumentTable + " WHERE ARR_FI_D_SRID='" + cf.selectedhomeid + "' and ARR_FI_D_IsOfficeUse='"+of_u+"' and ARR_FI_D_Inspection='"+inspection+"'");
				}
				
				if(i==0)
				{
					show_list();
				 
				}
				else if(i==1)
				{
					showoffice_list();
					 
				}
				
			}
		});
		b.setNegativeButton("No", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				
			}
		});
		AlertDialog a=b.create();
		a.show();
	}
	
	public class MyOnItemSelectedListenerdata implements OnItemSelectedListener {

	    public void onItemSelected(AdapterView<?> parent,
	        View view, int pos, long id) {	  
	    	strpresatins = parent.getItemAtPosition(pos).toString();
	    	if(strpresatins.equals("Other"))
	    	{
	    		othrtxt.setVisibility(visibility);
	    		o=1;
	    	}
	    	else
	    	{
	    		othrtxt.setVisibility(view.GONE);
	    		othrtxt.setText("");
	    		o=0;
	    	}
	     }

	    public void onNothingSelected(AdapterView parent) {
	      // Do nothing.
	    }
	}
	public class MyOnItemSelectedListenerdata1 implements OnItemSelectedListener {

	    public void onItemSelected(AdapterView<?> parent,
	        View view, int pos, long id) {	  
	    	strdoctitle = parent.getItemAtPosition(pos).toString();
	    	if(strdoctitle.equals("Other Information"))
	    	{
	    		
	    		AlertDialog.Builder builder = new AlertDialog.Builder(Feedback.this);
	   			builder.setMessage("Do you want to Duplicate the Supplemental documents of Other Information to all the Inspections?")
   			       .setCancelable(false)
   			       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
   			           public void onClick(DialogInterface dialog, int id) {
   			        	dupflag="1";
   			        	  
   			           }
   			       })
   			       .setNegativeButton("No", new DialogInterface.OnClickListener() {
   			           public void onClick(DialogInterface dialog, int id) {
   			        	dupflag="0";
   			                dialog.cancel();
   			           }
   			       });
	   			builder.show();
            
	    		
	    		othrtxtsupp.setVisibility(visibility);	    
	    		
	    		documentothertextsupp=1;
	    	}
	    	else
	    	{
	    		othrtxtsupp.setVisibility(view.GONE);
	    		documentothertextsupp=0;
	    	}

	     }

	    public void onNothingSelected(AdapterView parent) {
	      // Do nothing.
	    }
	}

	public class MyOnItemSelectedListenerdata2 implements OnItemSelectedListener {

    public void onItemSelected(AdapterView<?> parent,
        View view, int pos, long id) {	  
    	strdoctitle = parent.getItemAtPosition(pos).toString();
    	if(strdoctitle.equals("Other Information"))
    	{
    		AlertDialog.Builder builder = new AlertDialog.Builder(Feedback.this);
   			builder.setMessage("Do you want to Duplicate the Office documents of Other Information to all the Inspections?")
			       .setCancelable(false)
			       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			           public void onClick(DialogInterface dialog, int id) {
			        	dupflag="1";
			        	  
			           }
			       })
			       .setNegativeButton("No", new DialogInterface.OnClickListener() {
			           public void onClick(DialogInterface dialog, int id) {
			        	dupflag="0";
			                dialog.cancel();
			           }
			       });
   			builder.show();
    		othrtxtoff.setVisibility(visibility);	    	 
    		documentothertextoff=1;
    	}
    	else
    	{
    		othrtxtoff.setVisibility(view.GONE);
    		documentothertextoff=0;
    	}

     }

    public void onNothingSelected(AdapterView parent) {
      // Do nothing.
    }
}
	
	public static class EfficientAdapter extends BaseAdapter {
		private static final int IMAGE_MAX_SIZE = 0;
		private LayoutInflater mInflater;
		ViewHolder holder;
		View v2;String j;

		public EfficientAdapter(Context context) {
			mInflater = LayoutInflater.from(context);
		}

		public int getCount() {
			int arrlen = 0;
			arrlen = docpatharr.length;
			

			return arrlen;
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {

				holder = new ViewHolder();

				convertView = mInflater.inflate(R.layout.listview, null);
				holder.text2 = (ImageView) convertView
						.findViewById(R.id.TextView02);
				holder.text3 = (TextView) convertView
						.findViewById(R.id.TextView03);
				holder.text4 = (TextView) convertView
				.findViewById(R.id.TextView04);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			try {

				if (docpatharr[position].contains("null")
						|| docnamearr[position].contains("null")||docidarr[position].contains("null")) {
					docpatharr[position] = docpatharr[position].replace("null", "");
					docnamearr[position] = docnamearr[position].replace("null", "");
				
					docidarr[position] = docidarr[position].replace("null", "");
				
				}
				holder.text2.setBackgroundResource(R.drawable.allfilesicon);

				  holder.text3.setText(docpatharr[position]);
				  holder.text4.setText(docnamearr[position]);
				

			} catch (Exception e) {
				Log.i(TAG, "error:  efficiant" + e.getMessage());
			}

			return convertView;
		}

		static class ViewHolder {
			TextView text3,text4;
			ImageView text2;

		}

	}
	public static class EfficientAdapter1 extends BaseAdapter {
		private static final int IMAGE_MAX_SIZE = 0;
		private LayoutInflater mInflater;
		ViewHolder holder;
		View v2;String j;

		public EfficientAdapter1(Context context) {
			mInflater = LayoutInflater.from(context);
		}

		public int getCount() {
			int arrlen = 0;
			
				arrlen = offpatharr.length;
			
			return arrlen;
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {

				holder = new ViewHolder();

				convertView = mInflater.inflate(R.layout.listview, null);
				holder.text2 = (ImageView) convertView
						.findViewById(R.id.TextView02);
				holder.text3 = (TextView) convertView
						.findViewById(R.id.TextView03);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			try {

				if (offpatharr[position].contains("null")
						|| offnamearr[position].contains("null")||offdocidarr[position].contains("null")) {
					offpatharr[position] = offpatharr[position].replace("null", "");
					offnamearr[position] = offnamearr[position].replace("null", "");
					offdocidarr[position] = offdocidarr[position].replace("null", "");
				
				}
				holder.text2.setBackgroundResource(R.drawable.allfilesicon);
            	holder.text3.setText(offpatharr[position]);
				

			} catch (Exception e) {
				Log.i(TAG, "error: efficiaNT2 " + e.getMessage());
			}

			return convertView;
		}

		static class ViewHolder {
			TextView text3;
			ImageView text2;

		}

	}
	
	protected void pickpdf() {
		// TODO Auto-generated method stub
		PackageManager packageManager = getPackageManager();
		Intent testIntent = new Intent(Intent.ACTION_VIEW);
		testIntent.setType("application/pdf");
		startActivityForResult(Intent.createChooser(testIntent, "Select pdf"), SELECT_PDF);

}
	protected void startCameraActivity() {
		String fileName = "temp.jpg";
		ContentValues values = new ContentValues();
		values.put(MediaStore.Images.Media.TITLE, fileName);
		mCapturedImageURI = getContentResolver().insert(
				MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
		startActivityForResult(intent, CAPTURE_PICTURE_INTENT);

	}
	protected void pickfromgallery() {	
		Intent intent = new Intent();
		intent.setType("image/*");
		intent.setAction(Intent.ACTION_GET_CONTENT);
		startActivityForResult(Intent.createChooser(intent, "Select Picture"),
				SELECT_PICTURE);
	}
	
	public int getImageOrder(String srid,String inspname)
	{
		Cursor c11;
		if(inspection.equals("10"))
		{
			
			Cursor c =cf.SelectTablefunction(cf.allinspnamelist, " Where  ARR_Insp_Name='"+cf.encode(inspname)+"'");
			if(c.getCount()>0)
			{
				c.moveToFirst();
				inspid = c.getInt(c.getColumnIndex("ARR_Insp_ID"));
			}
			
			c11 =  cf.arr_db.rawQuery("SELECT * FROM "+  cf.FeedBackDocumentTable	+ " WHERE ARR_FI_D_SRID='"+srid+"' and ARR_ISCUSTOM='"+inspid+"' order by ARR_FI_D_ImageOrder desc",null);
		   	
		}
		else
		{
			c11 =  cf.arr_db.rawQuery("SELECT * FROM "+  cf.FeedBackDocumentTable	+ " WHERE ARR_FI_D_SRID='"+srid+"' and ARR_FI_D_Inspection='"+inspection+"' order by ARR_FI_D_ImageOrder desc",null);
		}
	
		 int imgrws = c11.getCount();
	     if(imgrws==0){
	    	 ImgOrder=1;
	     }
	     else
	     {
	    	 c11.moveToFirst();
	    	 int imgordr=c11.getInt(c11.getColumnIndex("ARR_FI_D_ImageOrder"));
	    	 ImgOrder=imgordr+1;
	    	 
	     }
	     return ImgOrder;
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode==121)
		{
			if(resultCode==RESULT_OK)
			{
				
			String[] value=	data.getExtras().getStringArray("Selected_array"); /**We pass the array of tje value from the IDAM Select page **/
			flagstr=(data.getExtras().getString("office_use").equals("false"))?0:1;
			ImgOrder=data.getExtras().getInt("Db_count");
			for(int i=0;i<value.length;i++ )
			{
				
				String title="";
				String documents = "";
				if(flagstr==0)
				{
					documents = s1.getSelectedItem().toString();
					title=cf.encode((s1.getSelectedItem().toString().equals("Other Information"))? othrtxtsupp.getText().toString():s1.getSelectedItem().toString().trim());
				}
				else
				{
					documents = s2.getSelectedItem().toString();
					title=cf.encode((s2.getSelectedItem().toString().equals("Other Information"))? othrtxtoff.getText().toString():s2.getSelectedItem().toString().trim());
				}
				
				
				if(documents.equals("CSE Form") || documents.equals("Acknowledgement Form")  || documents.equals("Sketch") ||  documents.equals("Building Permit")  ||  documents.equals("Owners Paperwork"))
				{
					for(int l=0;l<selected_insp.length;l++)
				    {
							getImageOrder(cf.selectedhomeid,selected_insp[l]);
						    cf.arr_db.execSQL("INSERT INTO "
					     					+  cf.FeedBackDocumentTable
					     					+ " (ARR_FI_D_SRID,ARR_FI_D_DocumentTitle,ARR_FI_D_FileName,ARR_FI_D_Nameext,ARR_FI_D_ImageOrder,ARR_FI_D_IsOfficeUse,ARR_FI_D_CreatedOn,ARR_FI_D_ModifiedDate,ARR_FI_D_Inspection,ARR_ISCUSTOM)"
					     					+ " VALUES ('" + cf.selectedhomeid + "','"+title+"','"+cf.encode(value[i])+"','"+cf.encode(picname)+"','"+ImgOrder+"','"+flagstr+ "','"+ cd+"','"+md+"','"+selected_inspid[l]+"','"+selected_onlineid[l]+"')");
						 		
								 ImgOrder++;
				    }
				}
				
				else if(documents.equals("Roof Permit"))
				{
					
					
					for(int l=0;l<selected_insp.length;l++)
				    {
						
						if(selected_inspid[l].equals("1") || selected_inspid[l].equals("2") || selected_inspid[l].equals("3") || selected_inspid[l].equals("4") || selected_inspid[l].equals("7") || selected_insp[l].equals(insp_sp.getSelectedItem().toString())) 
						{
							getImageOrder(cf.selectedhomeid,selected_insp[l]);

									cf.arr_db.execSQL("INSERT INTO "
				     					+  cf.FeedBackDocumentTable
				     					+ " (ARR_FI_D_SRID,ARR_FI_D_DocumentTitle,ARR_FI_D_FileName,ARR_FI_D_Nameext,ARR_FI_D_ImageOrder,ARR_FI_D_IsOfficeUse,ARR_FI_D_CreatedOn,ARR_FI_D_ModifiedDate,ARR_FI_D_Inspection,ARR_ISCUSTOM)"
				     					+ " VALUES ('" + cf.selectedhomeid + "','"+title+"','"+cf.encode(value[i])+"','"+cf.encode(picname)+"','"+ImgOrder+"','"+flagstr+ "','"+ cd+"','"+md+"','"+selected_inspid[l]+"','"+selected_onlineid[l]+"')");
					 		
							 ImgOrder++;
						}
				    }
				}		 
				
				else if(documents.equals("Manufacturers Information Form"))
				{
					
					for(int l=0;l<selected_insp.length;l++)
				    {
						
						if(selected_inspid[l].equals("1") || selected_inspid[l].equals("2") || selected_inspid[l].equals("3") ||  selected_insp[l].equals(insp_sp.getSelectedItem().toString())) 
						{
							getImageOrder(cf.selectedhomeid,selected_insp[l]);

									cf.arr_db.execSQL("INSERT INTO "
				     					+  cf.FeedBackDocumentTable
				     					+ " (ARR_FI_D_SRID,ARR_FI_D_DocumentTitle,ARR_FI_D_FileName,ARR_FI_D_Nameext,ARR_FI_D_ImageOrder,ARR_FI_D_IsOfficeUse,ARR_FI_D_CreatedOn,ARR_FI_D_ModifiedDate,ARR_FI_D_Inspection,ARR_ISCUSTOM)"
				     					+ " VALUES ('" + cf.selectedhomeid + "','"+title+"','"+cf.encode(value[i])+"','"+cf.encode(picname)+"','"+ImgOrder+"','"+flagstr+ "','"+ cd+"','"+md+"','"+selected_inspid[l]+"','"+selected_onlineid[l]+"')");
					 		
							 ImgOrder++;
						}
				    }
				}
				 
				else if(documents.equals("Sinkhole Risk Proximity Report"))
				{
					for(int l=0;l<selected_insp.length;l++)
				    {
						
						if(selected_inspid[l].equals("8") || selected_insp[l].equals(insp_sp.getSelectedItem().toString())) 
						{
							getImageOrder(cf.selectedhomeid,selected_insp[l]);

									cf.arr_db.execSQL("INSERT INTO "
				     					+  cf.FeedBackDocumentTable
				     					+ " (ARR_FI_D_SRID,ARR_FI_D_DocumentTitle,ARR_FI_D_FileName,ARR_FI_D_Nameext,ARR_FI_D_ImageOrder,ARR_FI_D_IsOfficeUse,ARR_FI_D_CreatedOn,ARR_FI_D_ModifiedDate,ARR_FI_D_Inspection,ARR_ISCUSTOM)"
				     					+ " VALUES ('" + cf.selectedhomeid + "','"+title+"','"+cf.encode(value[i])+"','"+cf.encode(picname)+"','"+ImgOrder+"','"+flagstr+ "','"+ cd+"','"+md+"','"+selected_inspid[l]+"','"+selected_onlineid[l]+"')");
					 		
							 ImgOrder++;
						}
				    }
									
				}				
				else if(documents.equals("Other Information"))
				{
					if(dupflag.equals("1"))
					{
						
						for(int l=0;l<selected_insp.length;l++)
					    {
							
								getImageOrder(cf.selectedhomeid,selected_insp[l]);

										cf.arr_db.execSQL("INSERT INTO "
					     					+  cf.FeedBackDocumentTable
					     					+ " (ARR_FI_D_SRID,ARR_FI_D_DocumentTitle,ARR_FI_D_FileName,ARR_FI_D_Nameext,ARR_FI_D_ImageOrder,ARR_FI_D_IsOfficeUse,ARR_FI_D_CreatedOn,ARR_FI_D_ModifiedDate,ARR_FI_D_Inspection,ARR_ISCUSTOM)"
					     					+ " VALUES ('" + cf.selectedhomeid + "','"+title+"','"+cf.encode(value[i])+"','"+cf.encode(picname)+"','"+ImgOrder+"','"+flagstr+ "','"+ cd+"','"+md+"','"+selected_inspid[l]+"','"+selected_onlineid[l]+"')");
						 		
								 ImgOrder++;
							
					    }
					}
					else if(dupflag.equals("0"))
					{					
							getImageOrder(cf.selectedhomeid,insp_sp.getSelectedItem().toString());
							
							cf.arr_db.execSQL("INSERT INTO "
					     					+  cf.FeedBackDocumentTable
					     					+ " (ARR_FI_D_SRID,ARR_FI_D_DocumentTitle,ARR_FI_D_FileName,ARR_FI_D_Nameext,ARR_FI_D_ImageOrder,ARR_FI_D_IsOfficeUse,ARR_FI_D_CreatedOn,ARR_FI_D_ModifiedDate,ARR_FI_D_Inspection,ARR_ISCUSTOM)"
					     					+ " VALUES ('" + cf.selectedhomeid + "','"+title+"','"+cf.encode(value[i])+"','"+cf.encode(picname)+"','"+ImgOrder+"','"+flagstr+ "','"+ cd+"','"+md+"','"+inspection+"','"+inspid+"')");
						 	ImgOrder++;						
					}
				}
				else
				{
					
					getImageOrder(cf.selectedhomeid,insp_sp.getSelectedItem().toString());
					
					cf.arr_db.execSQL("INSERT INTO "
		     					+  cf.FeedBackDocumentTable
		     					+ " (ARR_FI_D_SRID,ARR_FI_D_DocumentTitle,ARR_FI_D_FileName,ARR_FI_D_Nameext,ARR_FI_D_ImageOrder,ARR_FI_D_IsOfficeUse,ARR_FI_D_CreatedOn,ARR_FI_D_ModifiedDate,ARR_FI_D_Inspection,ARR_ISCUSTOM)"
		     					+ " VALUES ('" + cf.selectedhomeid + "','"+title+"','"+cf.encode(value[i])+"','"+cf.encode(picname)+"','"+ImgOrder+"','"+flagstr+ "','"+ cd+"','"+md+"','"+inspection+"','"+inspid+"')");
			 		
					ImgOrder++;
				}
	 					
			}
			if(flagstr==1)
			 {
				try
				{
				showoffice_list();
    			othrtxtsupp.setText("");
    			othrtxtoff.setText("");
   		  		othrtxtsupp.setVisibility(v1.GONE);
   		  		othrtxtoff.setVisibility(v1.GONE);
     			offbrws.setVisibility(visibility);
     			s2.setSelection(0);
				}
				catch (Exception e) {
					// TODO: handle exception
				//	.println("there is issues"+e.getMessage());
				}
			 }
			 else 
			 {
				 try
				 {
					 show_list();
					othrtxtsupp.setText("");othrtxtoff.setText("");
	        	    othrtxtsupp.setVisibility(v1.GONE);
	        	    othrtxtoff.setVisibility(v1.GONE);
	        	    docbrws.setVisibility(visibility);
	        		s1.setSelection(0);
			 }
				catch (Exception e) {
					// TODO: handle exception
					//.println("there is issues"+e.getMessage());
				}
			 }
			cf.ShowToast("Files added successfully.", 1);
			//show_savedvalue();
			}
			else
			{
				cf.ShowToast("You have cancelled file uploading", 1);
			}
        }
		else if(requestCode==cf.loadcomment_code)
		{
			load_comment=true;
			if(resultCode==RESULT_OK)
			{
				if(data.getExtras().getInt("id")== R.id.load_comments)
					edfbcoment.setText((edfbcoment.getText().toString()+" "+data.getExtras().getString("Comments")).trim());
				/*else if(data.getExtras().getInt("id")== R.id.load_comments_addendum)
					edfbaddcoment.setText((edfbaddcoment.getText().toString()+" "+data.getExtras().getString("Comments")).trim());*/
			}
			/*else if(resultCode==RESULT_CANCELED)
			{
				cf.ShowToast("You have canceled  the comments selction ",0);
			}*/
	}		 		
		
		/*if(t==0)
			{
				if (resultCode == RESULT_OK) {
					if (requestCode == SELECT_PICTURE) {
						Uri selectedImageUri = data.getData();
						selectedImagePath = getPath(selectedImageUri);
						 String[] bits = selectedImagePath.split("/");
						 picname = bits[bits.length - 1];
					}
				}
			}
			else if (t == 1) {
					switch (resultCode) {
					case 0:
						break;
					case -1:
						String[] projection = { MediaStore.Images.Media.DATA };
						Cursor cursor = managedQuery(mCapturedImageURI, projection,
								null, null, null);
						int column_index_data = cursor
								.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
						cursor.moveToFirst();
						capturedImageFilePath = cursor.getString(column_index_data);
						selectedImagePath = capturedImageFilePath;
						
						 String[] bits = selectedImagePath.split("/");
						 picname = bits[bits.length - 1];
						break;

					}
				}
			if(!selectedImagePath.trim().equals(""))
			{
			Bitmap b= cf.ShrinkBitmap(selectedImagePath, 400, 400);
		
			if(b==null)
			{
				cf.ShowToast("This image is not a supported format.You can not upload.",1);
			}
			else if(c==1 && !selectedImagePath.equals("") )
			    {
			    	docpathedit.setText(selectedImagePath);
				    docbrws.setVisibility(v1.GONE);
				    docupd.setVisibility(visibility);
			    }
			    else if(c==2 && !selectedImagePath.equals("") )
			    {
			    	offpathedit.setText(selectedImagePath);
				    offbrws.setVisibility(v1.GONE);
				    offupd.setVisibility(visibility);
			    }
			}
			else
			{
				cf.ShowToast("you have not selected any images.",1);
			}*/
		
	}
	public String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}
	protected void offshowcommon() {
		
		offtbl.setVisibility(visibility);offedittitle.setVisibility(visibility);
	}

	
	private void show_list()
	{
		Cursor c2;
		docname="";docpath="";doc_id="";
System.out.println("inspe="+inspection);
		 try {
			 
			if(inspection.equals("10"))
			 {
				    Cursor c3 =cf.SelectTablefunction(cf.allinspnamelist, " Where  ARR_Insp_Name='"+cf.encode(insp_sp.getSelectedItem().toString())+"'");
					if(c3.getCount()>0)
					{
						c3.moveToFirst();
						inspectionid = c3.getString(c3.getColumnIndex("ARR_Insp_ID"));
					} 
					c2 = cf.arr_db.rawQuery("SELECT * FROM "+ cf.FeedBackDocumentTable	+ " WHERE ARR_FI_D_SRID='"+cf.selectedhomeid+"' and ARR_ISCUSTOM='"+inspectionid+"' and  ARR_FI_D_IsOfficeUse=0",null);
		             ((LinearLayout)findViewById(R.id.l2)).setVisibility(cf.v1.GONE); 
		             ((LinearLayout)findViewById(R.id.supplin)).setVisibility(cf.v1.GONE);
		             
			 }
			 else
			 {
				 if(inspection.equals("4"))
		         {
					  inspection = getinspnumber(inspection);
		         }
				 ((LinearLayout)findViewById(R.id.l2)).setVisibility(cf.v1.VISIBLE);
				 ((LinearLayout)findViewById(R.id.supplin)).setVisibility(cf.v1.VISIBLE);
				   c2 = cf.arr_db.rawQuery("SELECT * FROM "+ cf.FeedBackDocumentTable	+ " WHERE ARR_FI_D_SRID='"+cf.selectedhomeid+"' and ARR_FI_D_Inspection='"+inspection+"' and  ARR_FI_D_IsOfficeUse=0",null);
	              
			 }
			 
			 
			 
			/* if(inspection.equals("4"))
	         {
			   inspection = getinspnumber(inspection);
	          }
			   Cursor	c2 =  cf.arr_db.rawQuery("SELECT * FROM "+  cf.FeedBackDocumentTable	+ " WHERE ARR_FI_D_SRID='"+cf.selectedhomeid+"' and ARR_FI_D_Inspection='"+inspection+"' and  ARR_FI_D_IsOfficeUse=0",null);*/
			   docrws = c2.getCount(); 
			   if(docrws!=0)        
			   {
				   int Column1=c2.getColumnIndex("ARR_FI_D_FileName");
	     	        int Column2=c2.getColumnIndex("ARR_FI_D_DocumentTitle");
	     	        c2.moveToFirst();
	     	        int i=0;docnamearr=new String[docrws];docpatharr=new String[docrws];docidarr=new String[docrws];
			     	if(c2!=null)
			     	{
			     		do{
			     			docnamearr[i]=cf.decode(c2.getString(Column1));
			     			docpatharr[i]=cf.decode(c2.getString(Column2));
			     			docidarr[i]=c2.getString(c2.getColumnIndex("ARR_FI_DocumentId"));
			     			// docnamearr = docname.split("~");
			     			// docpatharr=docpath.split("~"); 
			     			// docidarr=doc_id.split("~");
		     				doctbllst.setVisibility(visibility);
		     				ColorDrawable sage = new ColorDrawable(this.getResources().getColor(R.color.sage));
		     				doclst.setDivider(sage);
		     				doclst.setVisibility(visibility);doclst.setDividerHeight(2);
		     			   // doclst.setAdapter(new EfficientAdapter(this));
		     				findViewById(R.id.browse_clear).setVisibility(View.VISIBLE);		     			   
		     		i++;
                     }while(c2.moveToNext());
			     		TableRow.LayoutParams mParam = new TableRow.LayoutParams((int)(cf.wd-150),(int)(docrws * 50)+20);
			            doclst.setLayoutParams(mParam);
			            doclst.setAdapter(new EfficientAdapter(this));
	              }
			   }
			   else
			   {
				   doclst.setVisibility(v2.GONE);
				   doctbllst.setVisibility(v2.GONE);
				   findViewById(R.id.browse_clear).setVisibility(View.GONE);
			   }
       }
		 catch(Exception e)
		 {
			 Log.i(TAG,"error= show list "+e.getMessage());
		 } 
		 doclst.setOnItemClickListener(new OnItemClickListener() {
			  public void onItemClick(AdapterView<?> a, View v, int position, long id) {
			      final String selarr = docnamearr[position];
			      selected_id=docidarr[position];
			   
			      final Dialog dialog = new Dialog(Feedback.this);
		            dialog.getWindow().setContentView(R.layout.maindialog);
		            dialog.setTitle(Html.fromHtml("<font color='#FFFFFF'>"+docpatharr[position]+"</font>"));//.pri
		            
		            dialog.setCancelable(true);
		            final ImageView img = (ImageView) dialog.findViewById(R.id.ImageView01);
		            EditText ed1 = (EditText) dialog.findViewById(R.id.TextView01);
		            ed1.setVisibility(v1.GONE);img.setVisibility(v1.GONE);
		            
		            Button button_close = (Button) dialog.findViewById(R.id.Button01);
		    		button_close.setText("Close");//button_close.setVisibility(v2.GONE);
		    		
		    		
		            Button button_d = (Button) dialog.findViewById(R.id.Button03);
		    		button_d.setText("Delete");
		    		button_d.setVisibility(v2.VISIBLE);
		    		
		    		final Button button_view = (Button) dialog.findViewById(R.id.Button02);
		    		button_view.setText("View");
		    		button_view.setVisibility(v2.VISIBLE);
		    		
		    		
		    		final Button button_saveimage= (Button) dialog.findViewById(R.id.Button05);
		    		button_saveimage.setVisibility(v2.GONE);
		    		final LinearLayout linrotimage = (LinearLayout) dialog.findViewById(R.id.linrotation);
		    		linrotimage.setVisibility(v2.GONE);
		    		
		    		final Button rotateleft= (Button) dialog.findViewById(R.id.rotateleft);
		    		rotateleft.setVisibility(v2.GONE);
		    		final Button rotateright= (Button) dialog.findViewById(R.id.rotateright);
		    		rotateright.setVisibility(v2.GONE);
		    		
		    		
		    		
		    		
		    		
		    		button_d.setOnClickListener(new OnClickListener() {
			            public void onClick(View v) {
			            
			            AlertDialog.Builder builder = new AlertDialog.Builder(Feedback.this);
			   			builder.setMessage("Are you sure? Do you want to delete the selected document?")
			   				.setTitle("Confirmation")
			   				.setIcon(R.drawable.alertmsg)
		   			       .setCancelable(false)
		   			       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
		   			           public void onClick(DialogInterface dialog, int id) {
		   			        	 cf.arr_db.execSQL("Delete From " +  cf.FeedBackDocumentTable + "  WHERE ARR_FI_D_SRID ='" + cf.selectedhomeid + "' and ARR_FI_DocumentId='"+selected_id+"' and ARR_FI_D_Inspection='"+inspection+"' and ARR_FI_D_IsOfficeUse=0");
		   			        	cf.ShowToast("Document has been deleted successfully.",1);
  	   					   show_list();
		   			           }
		   			       })
		   			       .setNegativeButton("No", new DialogInterface.OnClickListener() {
		   			           public void onClick(DialogInterface dialog, int id) {
		   			                dialog.cancel();
		   			           }
		   			       });
		   			 builder.show();
		            
		   			 dialog.cancel(); 
	            }
   		           	
			  	 });
		    		
		    		
		    		
		    		button_view.setOnClickListener(new OnClickListener() {
			            public void onClick(View v) {
			            currnet_rotated=0;
			            	if(selarr.endsWith(".pdf"))
						     {
			            		img.setVisibility(v2.GONE);
			            		String tempstr ;
			            		if(selarr.contains("file://"))
			    		    	{
			    		    		 tempstr = selarr.replace("file://","");		    		
			    		    	}
			            		else
			            		{
			            			tempstr = selarr;
			            		}
			            		 File file = new File(tempstr);
							 
				                 if (file.exists()) {
				                	Uri path = Uri.fromFile(file);
				                    Intent intent = new Intent(Intent.ACTION_VIEW);
				                    intent.setDataAndType(path, "application/pdf");
				                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				  
				                    try {
				                        startActivity(intent);
				                    } 
				                    catch (ActivityNotFoundException e) {
				                    	cf.ShowToast("No application available to view PDF.",1);
				                       
				                    }
				               
				                }
						     }
			            	else  
			            	{
			            		Bitmap bitmap2=ShrinkBitmap(selarr,250,250);
			            		BitmapDrawable bmd2 = new BitmapDrawable(bitmap2); 
		    					img.setImageDrawable(bmd2);
		    					img.setVisibility(v2.VISIBLE);
		    					linrotimage.setVisibility(v2.VISIBLE);
		    					rotateleft.setVisibility(v2.VISIBLE);
		    					rotateright.setVisibility(v2.VISIBLE);
					            button_view.setVisibility(v2.GONE);
					            button_saveimage.setVisibility(v2.VISIBLE);
					           
			            		
			            	}
			            }
		    		});
		    		rotateleft.setOnClickListener(new OnClickListener() {  			
		    			public void onClick(View v) {

		    				// TODO Auto-generated method stub
		    			
		    				System.gc();
		    				currnet_rotated-=90;
		    				if(currnet_rotated<0)
		    				{
		    					currnet_rotated=270;
		    				}

		    				
		    				Bitmap myImg;
		    				try {
		    					myImg = BitmapFactory.decodeStream(new FileInputStream(selarr));
		    					Matrix matrix =new Matrix();
		    					matrix.reset();
		    					//matrix.setRotate(currnet_rotated);
		    					
		    					matrix.postRotate(currnet_rotated);
		    					
		    					 rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
		    					        matrix, true);
		    					 
		    					 img.setImageBitmap(rotated_b);

		    				} catch (FileNotFoundException e) {
		    					// TODO Auto-generated catch block
		    					e.printStackTrace();
		    				}
		    				catch (Exception e) {
		    					
		    				}
		    				catch (OutOfMemoryError e) {
		    					
		    					System.gc();
		    					try {
		    						myImg=null;
		    						System.gc();
		    						Matrix matrix =new Matrix();
		    						matrix.reset();
		    						//matrix.setRotate(currnet_rotated);
		    						matrix.postRotate(currnet_rotated);
		    						myImg= cf.ShrinkBitmap(selarr, 800, 800);
		    						rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
		    						System.gc();
		    						img.setImageBitmap(rotated_b); 

		    					} catch (Exception e1) {
		    						// TODO Auto-generated catch block
		    						e1.printStackTrace();
		    					}
		    					 catch (OutOfMemoryError e1) {
		    							// TODO Auto-generated catch block
		    						 cf.ShowToast("You cannot rotate this image. Image size is too large.",1);
		    					}
		    				}

		    			
		    			}
		    		});
		    		rotateright.setOnClickListener(new OnClickListener() {
		    		    public void onClick(View v) {
		    		    	
		    		    	currnet_rotated+=90;
		    				if(currnet_rotated>=360)
		    				{
		    					currnet_rotated=0;
		    				}
		    				
		    				Bitmap myImg;
		    				try {
		    					myImg = BitmapFactory.decodeStream(new FileInputStream(selarr));
		    					Matrix matrix =new Matrix();
		    					matrix.reset();
		    					//matrix.setRotate(currnet_rotated);
		    					
		    					matrix.postRotate(currnet_rotated);
		    					
		    					 rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
		    					        matrix, true);
		    					 
		    					 img.setImageBitmap(rotated_b);  

		    				} catch (FileNotFoundException e) {
		    					//.println("FileNotFoundException "+e.getMessage()); 
		    					// TODO Auto-generated catch block
		    					e.printStackTrace();
		    				}
		    				catch(Exception e){}
		    				catch (OutOfMemoryError e) {
		    					//.println("comes in to out ot mem exception");
		    					System.gc();
		    					try {
		    						myImg=null;
		    						System.gc();
		    						Matrix matrix =new Matrix();
		    						matrix.reset();
		    						//matrix.setRotate(currnet_rotated);
		    						matrix.postRotate(currnet_rotated);
		    						myImg= cf.ShrinkBitmap(selarr, 800, 800);
		    						rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
		    						System.gc();
		    						img.setImageBitmap(rotated_b); 

		    					} catch (Exception e1) {
		    						// TODO Auto-generated catch block
		    						e1.printStackTrace();
		    					}
		    					 catch (OutOfMemoryError e1) {
		    							// TODO Auto-generated catch block
		    						 cf.ShowToast("You cannot rotate this image. Image size is too large.",1);
		    					}
		    				}

		    		    }
		    		});
		    		button_saveimage.setOnClickListener(new OnClickListener() {
						
						public void onClick(View v) {
							// TODO Auto-generated method stub
							
							
							if(currnet_rotated>0)
							{ 

								try
								{
									/**Create the new image with the rotation **/
							
									String current=MediaStore.Images.Media.insertImage(getContentResolver(), rotated_b, "My bitmap", "My rotated bitmap");
									 ContentValues values = new ContentValues();
									  values.put(MediaStore.Images.Media.ORIENTATION, 0);
									  Feedback.this.getContentResolver().update(Uri.parse(current), values, MediaStore.Images.Media.DATA+ "=?", new String[] { current } );
									
									if(current!=null)
									{
									String path=getPath(Uri.parse(current));
									File fout = new File(selarr);
									fout.delete();
									/** delete the selected image **/
									File fin = new File(path);
									/** move the newly created image in the slected image pathe ***/
									fin.renameTo(new File(selarr));
									cf.ShowToast("Document saved successfully.",1);dialog.cancel();
									show_list();
									
									
								}
								} catch(Exception e)
								{
									//.println("Error occure while rotate the image "+e.getMessage());
								}
								
							}
							else
							{
								cf.ShowToast("Document saved successfully.",1);
								dialog.cancel();
								show_list();
							}
							
						}
					});
		    		button_close.setOnClickListener(new OnClickListener() {
			            public void onClick(View v) {
			            	dialog.cancel();
			            }
		    		});
		    		dialog.show();

		  }
			  });
	}
	void showoffice_list()
	{
		try {
			Cursor c3;
			 
			 if(inspection.equals("10"))
			 {
				    Cursor c1 =cf.SelectTablefunction(cf.allinspnamelist, " Where  ARR_Insp_Name='"+cf.encode(insp_sp.getSelectedItem().toString())+"'");
					if(c1.getCount()>0)
					{
						c1.moveToFirst();
						inspectionid = c1.getString(c1.getColumnIndex("ARR_Insp_ID"));
					} 
					c3 = cf.arr_db.rawQuery("SELECT * FROM "+ cf.FeedBackDocumentTable	+ " WHERE ARR_FI_D_SRID='"+cf.selectedhomeid+"' and ARR_ISCUSTOM='"+inspectionid+"' and  ARR_FI_D_IsOfficeUse=1",null);
		             ((LinearLayout)findViewById(R.id.l3)).setVisibility(cf.v1.GONE); 
		             ((LinearLayout)findViewById(R.id.offlin)).setVisibility(cf.v1.GONE);
		             
			 }
			 else
			 {
				 if(inspection.equals("4"))
		         {
					  inspection = getinspnumber(inspection);
		         }
				 ((LinearLayout)findViewById(R.id.l3)).setVisibility(cf.v1.VISIBLE);
				 ((LinearLayout)findViewById(R.id.offlin)).setVisibility(cf.v1.VISIBLE);
				   c3 = cf.arr_db.rawQuery("SELECT * FROM "+ cf.FeedBackDocumentTable	+ " WHERE ARR_FI_D_SRID='"+cf.selectedhomeid+"' and ARR_FI_D_Inspection='"+inspection+"' and  ARR_FI_D_IsOfficeUse=1",null);
	              
			 }
			 
			 
			   offrws = c3.getCount();
			   if(offrws!=0)
			   {
				  int Column1=c3.getColumnIndex("ARR_FI_D_FileName");
	     	       int Column2=c3.getColumnIndex("ARR_FI_D_DocumentTitle");
	     	      c3.moveToFirst();
	     	    offnamearr= new String[c3.getCount()];
	     	   offpatharr= new String[c3.getCount()];
	     	  offdocidarr= new String[c3.getCount()];
			     	if(c3!=null)
			     	{int i=0;
			     		do{
			     			offnamearr[i] = cf.decode(c3.getString(Column1));
			     			 offpatharr[i]=cf.decode(c3.getString(Column2)); 
			     			offdocidarr[i]=c3.getString(c3.getColumnIndex("ARR_FI_DocumentId"));
			     			
			     			offtbllst.setVisibility(visibility);
		     			    ColorDrawable sage = new ColorDrawable(this.getResources().getColor(R.color.sage));
			     			offlst.setDivider(sage);
			     		    offlst.setVisibility(visibility);offlst.setDividerHeight(2);
		     			   // offlst.setAdapter(new EfficientAdapter1(this));
			     		   findViewById(R.id.offbrowse_clear).setVisibility(View.VISIBLE);           
		     			    i++;
                    }while(c3.moveToNext());
			     		
			     		TableRow.LayoutParams mParam = new TableRow.LayoutParams((int)(cf.wd-150),(int)((i*50)+20));
			     		offlst.setLayoutParams(mParam);
			            offlst.setAdapter(new EfficientAdapter1(this));
			            
	       }
			   }
			   else
			   {
				    offtbllst.setVisibility(v2.GONE);
				    offlst.setVisibility(v2.GONE);
				    findViewById(R.id.offbrowse_clear).setVisibility(View.GONE);
			   }
      }
		 catch(Exception e)
		 {
			 Log.i(TAG,"error lastr= "+e.getMessage());
		 }
		
	}
	
	 public void walkdir(File dir) {
		    String pdfPattern = ".pdf";
		    File listFile[] = dir.listFiles();
           if (listFile != null) {
		    	  for (int i = 0; i < listFile.length; i++) {

		            if (listFile[i].isDirectory()) {
		                walkdir(listFile[i]);
		            } else  {
		            if (listFile[i].getName().endsWith(pdfPattern)){
		                   
		            	 pdfsample=dynamicarraysetting(dir+"/"+listFile[i].getName(),pdfsample);
		            	  ii++;
		              }
		            }
		        }
		    }    
		}
	 private String[] dynamicarraysetting(String ErrorMsg,String[] pieces3) {
			// TODO Auto-generated method stub
			
			
			 try
			 {
			if(pieces3==null)
			{
				pieces3=new String[1];
				
				pieces3[0]=ErrorMsg;
			}
			else
			{
				
				String tmp[]=new String[pieces3.length+1];
				int i;
				for(i =0;i<pieces3.length;i++)
				{
					
					tmp[i]=pieces3[i];
				}
			
				tmp[tmp.length-1]=ErrorMsg;
				pieces3=null;
				pieces3=tmp.clone();
			}
			 }
			 catch(Exception e)
			 {
				// .println("Exception "+e.getMessage());
				 //return pieces3;
			 }
			return pieces3;
		}
	 Bitmap ShrinkBitmap(String file, int width, int height) {
		 Bitmap bitmap =null;
			try {
				BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
				bmpFactoryOptions.inJustDecodeBounds = true;
			    bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);

				int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight
						/ (float) height);
				int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth
						/ (float) width);
                
				if (heightRatio > 1 || widthRatio > 1) {
					if (heightRatio > widthRatio) {
						bmpFactoryOptions.inSampleSize = heightRatio;
					} else {
						bmpFactoryOptions.inSampleSize = widthRatio;
					}
				}

				bmpFactoryOptions.inJustDecodeBounds = false;
				bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
				
				
				return bitmap;
			} catch (Exception e) {
			
				return bitmap;
			}

		}
	 public void onWindowFocusChanged(boolean hasFocus) {

		    super.onWindowFocusChanged(hasFocus);
		    if(focus==1)
		    {
		    	focus=0;
		    	edfbcoment.setText(edfbcoment.getText().toString());    
		    }
		 }      
		
	public boolean onKeyDown(int keyCode, KeyEvent event)
		 {
		 if(keyCode==KeyEvent.KEYCODE_BACK)
		 {
			 cf.goback(7);
		      return true;
		 }
		 return super.onKeyDown(keyCode, event);
		 }
}
