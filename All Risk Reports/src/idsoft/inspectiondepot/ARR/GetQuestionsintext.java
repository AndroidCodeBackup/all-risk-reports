package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import android.content.Context;


public class GetQuestionsintext {
	

	String b1_q1[] = { "-Select-", "Meets 2001 FBC", "Meets SFBC -94","Unknown - Does Not Meet" };
	String b1_q2[] = { "-Select-","Meets FBC(PD -03/2/02) or Original (after 2004)","Meets SFBC (PD 09/01/1994) or Post 1997","One or more does not meet A or B", "None Meet A or B" };
	String b1_q3[] = { "-Select-", "Mean Uplift less than B and C","103 PSF", "182 PSF", "Concrete", "Other", "Unknown", "No Attic" };
	String b1_q4[] = { "-Select-", "Toe Nail", "Clips", "Single Wraps","Double Wraps", "Structural", "Other", "Unknown", "No Attic" };
	String b1_q5[] = { "-Select-", "Hip Roof", "Flat Roof","Other Roof Type" };
	String b1_q6[] = { "-Select-", "SWR", "No SWR", "Unknown" };
	String b1_q7[] = { "-Select-", "FBC Plywood","All Glazed Openings 4 � 8 lb Large Missile lb.","All Glazed Openings 4 - 8 lb Large Missile","Opening Protection Not Verified", "None / Some Not Protected" };
	String b1_q8[] = { "-Select-", "Wall Construction Comments" };
	String s4[]={"-Select-","All"};
	
	Context con;
	public GetQuestionsintext(Context con) 
	{
		this.con =con;
	}
	public String getsuboptiontext(int optionId,int quesID) {
		// TODO Auto-generated method stub
		String tmp[] = s4;
		switch(quesID)
		{ 
			case 1:
				tmp=b1_q1;
			break;
			case 2:
				tmp=b1_q2;
			break;
			case 3:
				tmp=b1_q3;
			break;
			case 4:
				tmp=b1_q4;
			break;
			case 5:
				tmp=b1_q5;
			break;
			case 6:
				tmp=b1_q6;
			break;
			case 7:
				tmp=b1_q7;
			break;
			case 8:
				tmp=b1_q8;
			break;
			
			
		}
		return tmp[optionId];
				
	}
	public String getquestiontext(int questionID) {
		// TODO Auto-generated method stub
		String tmp = null;
		String[] temp=con.getResources().getStringArray(R.array.b11802_suboption);
		switch(questionID)
		{ 
			case 1:
				tmp=temp[0];
			break;
			case 2:
				tmp=temp[1];
			break;
			case 3:
				tmp=temp[2];
			break;
			case 4:
				tmp=temp[3];
			break;
			case 5:
				tmp=temp[4];
			break;
			case 6:
				tmp=temp[5];
			break;
			case 7:
				tmp=temp[6];;
			break;
			case 8:
				tmp=temp[7];
			break;
			
			
		}
		return tmp;
		
	}
}
