package idsoft.inspectiondepot.ARR;
/***   	Page Information
Version :1.0
VCode:1
Created Date:19/10/2012
Purpose:For Schedule Inspection 
Created By:Rakki S
Last Updated:25/10/2012   
Last Updatedby:Rakki s
***/
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.Calendar;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

public class General extends Activity{
	private static final int DATE_DIALOG_ID = 0;
	private static final int SELECT_PICTURE = 0;
	CommonFunctions cf;
	public String strhomeid,commercialflag; 
	String isinspected;
	String startspin,endspin;
	Cursor c2;
	int spin1,spin2,verify,SP_ST,SP_ET;
	String homeid[];
	private ArrayAdapter<CharSequence> mRangeadapter;
	TextView assigndate;
	private EditText et_commenttxt,et_inspdate;
	private CheckBox schchk;
	private Button get_inspectiondate;
	private Spinner Sch_Spinnerstart,Sch_Spinnerend;
	private TextView SQ_TV_type2;
///	private LinearLayout SQ_ED_type2;
	private String Sh_assignedDate;
	private boolean load_comment=true;
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	       
	        cf = new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.selectedhomeid = strhomeid= extras.getString("homeid");
				cf.onlinspectionid = extras.getString("InspectionType");
			    cf.onlstatus = extras.getString("status");
			}
			
			setContentView(R.layout.inspectionschedule);
			/**Used for hide he key board when it clik out side**/

			cf.setupUI((ScrollView) findViewById(R.id.scr));

		/**Used for hide he key board when it clik out side**/
			
			cf.getDeviceDimensions();
			LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
	     	hdr_layout.addView(new HdrOnclickListener(this,1,"General","Schedule Info",1,0,cf));
	     	LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
         	main_layout.setMinimumWidth(cf.wd);
         	main_layout.addView(new MyOnclickListener(getApplicationContext(), 1, 0,0,cf));
	        LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
	        submenu_layout.addView(new MyOnclickListener(this, 12, 1,0,cf));
	        LinearLayout submenu2_layout = (LinearLayout) findViewById(R.id.submenu1); System.out.println("test submenu1");
		    submenu2_layout.addView(new MyOnclickListener(getApplicationContext(), 59, 1,0,cf));System.out.println("test 56");
			
			ScrollView scr = (ScrollView)findViewById(R.id.scr);
		    scr.setMinimumHeight(cf.ht);		   
			
			schchk = (CheckBox) findViewById(R.id.reschedule);
			et_inspdate = (EditText)findViewById(R.id.etinspectiondate);    
			get_inspectiondate = (Button)findViewById(R.id.getinspectiondate);
			Sch_Spinnerstart = (Spinner)findViewById(R.id.spinstarttime);
			Sch_Spinnerend = (Spinner)findViewById(R.id.spinendtime);
			Spinner_SetStarttime();
			Sch_Spinnerstart.setOnItemSelectedListener(new MyOnItemSelectedListenerstart());			
			et_commenttxt = (EditText)findViewById(R.id.etcommenttxt);
			SQ_TV_type2 = (TextView) findViewById(R.id.SH_TV_ED);
			assigndate = (TextView) findViewById(R.id.viewassigndate);
			
			
			
		    et_commenttxt.addTextChangedListener(new SH_textwatcher(1));
		    
		    et_commenttxt.setOnTouchListener(new OnTouchListener() {
				
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					((ScrollView) findViewById(R.id.scr)).requestDisallowInterceptTouchEvent(true);
					return false;
				}
			});
		    
		    
		    cf.getInspectorId();
			cf.getCalender();
			Spinner_SetEndtime();
			Sch_Spinnerend.setOnItemSelectedListener(new MyOnItemSelectedListenerend());
			
			if (cf.onlstatus.equals("Assign")) {schchk.setEnabled(false);}
			else
			{
				schchk.setEnabled(true);
			}
			schchk.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {

					// Perform action on clicks, depending on whether it's now
					// checked
				if (((CheckBox) v).isChecked()) {
						//et_inspdate.setEnabled(true);
						get_inspectiondate.setEnabled(true);
						Sch_Spinnerstart.setEnabled(true);
						Sch_Spinnerend.setEnabled(true);
						et_commenttxt.setEnabled(true);
						//((TextView)findViewById(R.id.txtid)).setText("RE-SCHEDULE AN INSPECTION");
					} else if (!et_inspdate.getText().toString().equals("")) {
						et_inspdate.setEnabled(false);
						get_inspectiondate.setEnabled(false);
						Sch_Spinnerstart.setEnabled(false);
						Sch_Spinnerend.setEnabled(false);
						et_commenttxt.setEnabled(false);
						//((TextView)findViewById(R.id.txtid)).setText("SCHEDULE AN INSPECTION");
					}else{
						//((TextView)findViewById(R.id.txtid)).setText("SCHEDULE AN INSPECTION");
					}
				}
			});
			get_inspectiondate.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					cf.showDialogDate(et_inspdate);
					
				}
			});
			try {
				c2 = cf.arr_db.rawQuery("SELECT * FROM "
						+ cf.policyholder + " WHERE ARR_PH_SRID='" + cf.encode(cf.selectedhomeid)
						+ "' and ARR_PH_InspectorId='" + cf.encode(cf.Insp_id) + "'", null);
				int rws = c2.getCount();
				c2.moveToFirst();
				if (c2 != null) {
						et_inspdate.setText(cf.decode(c2.getString(c2.getColumnIndex("ARR_Schedule_ScheduledDate"))));
						Sh_assignedDate=cf.decode(c2.getString(c2.getColumnIndex("ARR_Schedule_AssignedDate")));
						SP_ST = mRangeadapter.getPosition(cf.decode(c2.getString(c2.getColumnIndex("ARR_Schedule_InspectionStartTime"))));
						Sch_Spinnerstart.setSelection(SP_ST);
         			    SP_ET = mRangeadapter.getPosition(cf.decode(c2.getString(c2.getColumnIndex("ARR_Schedule_InspectionEndTime"))));
						Sch_Spinnerend.setSelection(SP_ET);
						et_commenttxt.setText(cf.decode(c2.getString(c2.getColumnIndex("ARR_Schedule_Comments"))));
						assigndate.setText(cf.decode(c2.getString(c2.getColumnIndex("ARR_Schedule_AssignedDate"))));

						if (!cf.decode(c2.getString(c2.getColumnIndex("ARR_Schedule_ScheduledDate"))).equals("") && !cf.onlstatus.equals("Assign"))
						{
							et_inspdate.setEnabled(false);
							get_inspectiondate.setEnabled(false);
							Sch_Spinnerstart.setEnabled(false);
							Sch_Spinnerend.setEnabled(false);
							et_commenttxt.setEnabled(false);

						} else {
							//et_inspdate.setEnabled(true);
							get_inspectiondate.setEnabled(true);
							Sch_Spinnerstart.setEnabled(true);
							Sch_Spinnerend.setEnabled(true);
							
							et_commenttxt.setEnabled(true);
						}
				}

			} catch (Exception e) {
				cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ General.this +" "+" in the processing stage of retrieving data for schedule from PH table  at "+" "+ cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				
			}
			
		
	 } 
	 class SH_textwatcher implements TextWatcher
		{
	         public int type;
	        
	         SH_textwatcher(int type)
			{
				this.type=type; 
			}
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				cf.showing_limit(s.toString(),SQ_TV_type2,999);
			 
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				
			}
			
		}
		private ProgressDialog pd;
		

		public void clicker(View v) {
			switch (v.getId()) {

			  case R.id.hme:
						  cf.gohome();
						  break;
			  case R.id.save:
				if(!cf.onlstatus.trim().equals("CIT"))
				{
				if (!"".equals(et_inspdate.getText().toString())) {
					 // if (Validation_TodayDate(et_inspdate.getText().toString()) == "true") {
						  //if (Validation_Assigndate(Sh_assignedDate, et_inspdate.getText().toString()) == "true") {
							  if (!"Select".equals(Sch_Spinnerstart.getSelectedItem().toString())) {
								  if (!"Select".equals(Sch_Spinnerend.getSelectedItem().toString())) {
									  if (spinvalidation(spin1, spin2) == "true") {
										  //||  schchk.isChecked())
										  /*if (et_commenttxt.getText().toString().trim().equals("") || !et_commenttxt.isEnabled()) {
											  if(et_commenttxt.getText().toString().trim().equals(""))
											  {
												cf.ShowToast("Please enter the Scheduling Comments.", 0);
												 et_commenttxt.requestFocus();
											  }
											  else if(!et_commenttxt.isEnabled())
											  {
													cf.ShowToast("Please check the reschedule option ", 0);
											  }
										  	   	
											}
											else
											{*/
												
												try
												{
												Cursor c12 = cf.arr_db.rawQuery("SELECT * FROM "
														+ cf.policyholder + " WHERE ARR_PH_SRID='" + cf.encode(cf.selectedhomeid)
														+ "' and ARR_PH_InspectorId='" + cf.encode(cf.Insp_id) + "'", null);												
												c12.moveToFirst();
												 isinspected = c12.getString(c12.getColumnIndex("ARR_PH_IsInspected"));
												 commercialflag = c12.getString(c12.getColumnIndex("Commercial_Flag"));
												
												}
												catch(Exception e)
												{
													
												}
												
											if(isinspected!=null && isinspected.equals("1"))
											{ 
												cf.ShowToast("You cannot schedule this record. This inspection has been moved to CIT status.", 0);
											}
											else
											{
													if (cf.isInternetOn()==true) {													
													 String source = "<font color=#FFFFFF>Scheduling..."	+ "</font>";
											         pd = ProgressDialog.show(this,"", Html.fromHtml(source), true);
													new Thread() {
														public void run() {
															try {
																String Chk_Inspector=cf.IsCurrentInspector(cf.Insp_id,cf.selectedhomeid,"ISCURRENTINSPECTOR");
																if(Chk_Inspector.equals("true"))
																{  	
																	 if (sendschedule() == "true") 
																	 {
																			if(commercialflag.equals("0"))
																			{
																				cf.arr_db.execSQL("UPDATE "
																						+ cf.policyholder
																						+ " SET ARR_PH_Status='40',ARR_Schedule_ScheduledDate='"
																						+ cf.encode(et_inspdate.getText().toString())
																						+ "',"
																						+ "ARR_Schedule_Comments='"
																						+ cf.encode(et_commenttxt.getText().toString())
																						+ "',ARR_Schedule_InspectionStartTime='"
																						+ cf.encode(Sch_Spinnerstart.getSelectedItem().toString())
																						+ "',"
																						+ "ARR_Schedule_InspectionEndTime='"
																						+ cf.encode(Sch_Spinnerend.getSelectedItem().toString())
																						+ "',ARR_ScheduleFlag='1' WHERE ARR_PH_InspectorId ='"
																						+ cf.encode(cf.Insp_id)
																						+ "' and ARR_PH_SRID='"
																						+ cf.encode(cf.selectedhomeid)
																						+ "'");
																			}
																			else
																			{
																				System.out
																						.println("UPDATE "
																						+ cf.policyholder
																						+ " SET ARR_PH_Status='40',ARR_Schedule_ScheduledDate='"
																						+ cf.encode(et_inspdate.getText().toString())
																						+ "',"
																						+ "ARR_Schedule_Comments='"
																						+ cf.encode(et_commenttxt.getText().toString())
																						+ "',ARR_Schedule_InspectionStartTime='"
																						+ cf.encode(Sch_Spinnerstart.getSelectedItem().toString())
																						+ "',"
																						+ "ARR_Schedule_InspectionEndTime='"
																						+ cf.encode(Sch_Spinnerend.getSelectedItem().toString())
																						+ "',ARR_ScheduleFlag='1' WHERE ARR_PH_InspectorId ='"
																						+ cf.encode(cf.Insp_id)
																						+ "' and Commercial_Flag='"+commercialflag+ "'");
																				
																				cf.arr_db.execSQL("UPDATE "
																						+ cf.policyholder
																						+ " SET ARR_PH_Status='40',ARR_Schedule_ScheduledDate='"
																						+ cf.encode(et_inspdate.getText().toString())
																						+ "',"
																						+ "ARR_Schedule_Comments='"
																						+ cf.encode(et_commenttxt.getText().toString())
																						+ "',ARR_Schedule_InspectionStartTime='"
																						+ cf.encode(Sch_Spinnerstart.getSelectedItem().toString())
																						+ "',"
																						+ "ARR_Schedule_InspectionEndTime='"
																						+ cf.encode(Sch_Spinnerend.getSelectedItem().toString())
																						+ "',ARR_ScheduleFlag='1' WHERE ARR_PH_InspectorId ='"
																						+ cf.encode(cf.Insp_id)
																						+ "' and Commercial_Flag='"+commercialflag+ "'");
																			}
																			verify = 0;
																			handler.sendEmptyMessage(0);
																			pd.dismiss();
																			if (cf.onlstatus.equals("Assign")) {																				
																				startActivity(new Intent(General.this,Dashboard.class));
																			}
																	 }
																	 else
																	 {
																		 showerror();
																	 }
																}
																else
																{
																	verify = 2;
																	handler.sendEmptyMessage(0);
																	pd.dismiss();
																	
																}
															} catch (SocketTimeoutException s) {
																verify = 5;
																handler.sendEmptyMessage(0);
																pd.dismiss();
															} catch (NetworkErrorException n) {
																verify = 5;
																handler.sendEmptyMessage(0);
																pd.dismiss();
															} catch (IOException io) {
																verify = 5;
																handler.sendEmptyMessage(0);
																pd.dismiss();
															} catch (XmlPullParserException x) {
																verify = 5;
																handler.sendEmptyMessage(0);
																pd.dismiss();
															} catch (Exception e) {
																verify = 5;
																handler.sendEmptyMessage(0);
																pd.dismiss();
															}
														}

														private void showerror() {
															verify = 1;
															handler.sendEmptyMessage(0);
															pd.dismiss();
														}

														private Handler handler = new Handler() {
															@Override
															public void handleMessage(
																	Message msg) {
																if (verify == 0) {
																	if(schchk.isChecked()){
																	cf.ShowToast("Your inspection has been successfully re-scheduled.", 0);
																	}
																	else
																	{
																		cf.ShowToast("Your inspection has been successfully scheduled.", 0);
																	}
																} else if (verify == 1) {
																	cf.ShowToast("Your inspection has not been scheduled.", 0);
																} else if (verify == 5) {
																	cf.ShowToast("Please check your network connection and try again.", 0);
																}
																else if(verify == 2)
																{
																	cf.ShowToast("Sorry. This record has been reallocated to another inspector.", 0);
																}
															}
														};
													}.start();
													
												} else {
													cf.ShowToast("Internet connection not available.", 0);
													cf.arr_db.execSQL("UPDATE "
															+ cf.policyholder
															+ " SET ARR_PH_Status='40',ARR_Schedule_ScheduledDate='"
															+ cf.encode(et_inspdate.getText().toString())
															+ "',"
															+ "ARR_Schedule_Comments='"
															+ cf.encode(et_commenttxt.getText().toString())
															+ "',ARR_Schedule_InspectionStartTime='"
															+ cf.encode(Sch_Spinnerstart.getSelectedItem().toString())
															+ "',"
															+ "ARR_Schedule_InspectionEndTime='"
															+ cf.encode(Sch_Spinnerend.getSelectedItem().toString())
															+ "',ARR_ScheduleFlag='0' WHERE ARR_PH_InspectorId ='"
															+ cf.encode(cf.Insp_id)
															+ "' and ARR_PH_SRID='"
															+ cf.encode(cf.selectedhomeid) + "'");
													if (cf.onlstatus.equals("Assign")) {
														startActivity(new Intent(
																General.this,Dashboard.class));

													}
												}
											}
											//}
										} else {
											 cf.ShowToast("End Time should be greater than Start Time.", 0);
									      }
								  } else {
									   cf.ShowToast("Please select End Time.", 0);
									   Sch_Spinnerend.requestFocus();
									}
							  }
							  else {
								  cf.ShowToast("Please select Start Time.", 0);
								  Sch_Spinnerstart.requestFocus();
								}
							  /* }
						  else {
							  cf.ShowToast("Schedule Date should be greater than or equal to Assigned Date.", 0);
							  et_inspdate.setText("");
							  et_inspdate.requestFocus();
							}
					  }
					  else {
						    cf.ShowToast("Schedule Date should be greater than or equal to today date.", 0);
						    et_inspdate.setText("");
						    et_inspdate.requestFocus();
						}
*/				  }
				  else {
					  cf.ShowToast("Please enter the Inspection Date.", 0);
					 et_inspdate.requestFocus();
					}
				}
				else
				{
					cf.ShowToast("This record in Completed in Tablet Status so you can not schedule .", 0);
					 et_inspdate.requestFocus();
				}
				  break;
			  case R.id.load_comments:
					int len=et_commenttxt.getText().toString().length();
					
					if(load_comment )
					{
						load_comment=false;
						int loc[] = new int[2];
						v.getLocationOnScreen(loc);
						cf.loadinsp_n="General Information";
						cf.loadinsp_q=cf.getResourcesvalue(R.string.gen_4);
						Intent in1 = cf.loadComments("Scheduling Comments",loc);
						in1.putExtra("length", len);
						in1.putExtra("max_length", 999);
						startActivityForResult(in1, cf.loadcomment_code);
					}
					break;
			}
		}
	private String sendschedule() throws NetworkErrorException, IOException,SocketTimeoutException, XmlPullParserException {
			// TODO Auto-generated method stub
		   SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);		    
			envelope.dotNet = true;
			SoapObject ad_property = new SoapObject(cf.NAMESPACE, "EXPORTSCHEDULEINFO");
			
			ad_property.addProperty("Srid", cf.selectedhomeid);
			ad_property.addProperty("InspectorID", cf.Insp_id);
			ad_property.addProperty("ScheduleDate", et_inspdate.getText().toString());
			int spinnerstatPosition = mRangeadapter.getPosition(startspin);
			ad_property.addProperty("ScheduleStartTime", spinnerstatPosition);
			int spinnerendPosition = mRangeadapter.getPosition(endspin);
			ad_property.addProperty("ScheduleEndTime", spinnerendPosition);
			ad_property.addProperty("Comments", et_commenttxt.getText().toString());
			ad_property.addProperty("Scheduleflag",commercialflag);
			
			
			envelope.setOutputSoapObject(ad_property);
			System.out.println("the property "+ad_property);
			HttpTransportSE androidHttpTransport1 = new HttpTransportSE(cf.URL_ARR);
			androidHttpTransport1.call(cf.NAMESPACE+"EXPORTSCHEDULEINFO", envelope);
			Object response = envelope.getResponse();System.out.println("responsee"+response);
			
			if (response == null || response.toString().equals("")
					|| response.toString().equals("null")) {
				verify = 1;
			    return "false";
			} else if (response.toString().equals("true")) {
				verify = 0;
				return "true";
			} else {
				verify = 1;
				return "false";
			}
		}
	private String spinvalidation(int spin12, int spin22) {
			// TODO Auto-generated method stub
			if (spin22 > spin12) {
				return "true";
			} else {
				return "false";
			}
		
		}
	private String Validation_Assigndate(String sh_assignedDate,
				String string) {
			// TODO Auto-generated method stub
		 if (!sh_assignedDate.trim().equals("")
					|| sh_assignedDate.equals("N/A")
					|| sh_assignedDate.equals("Not Available")
					|| sh_assignedDate.equals("anytype")
					|| sh_assignedDate.equals("Null")) {
				String chkdate = null;
				int i1 = sh_assignedDate.indexOf("/");
				String result = sh_assignedDate.substring(0, i1);
				int i2 = sh_assignedDate.lastIndexOf("/");
				String result1 = sh_assignedDate.substring(i1 + 1, i2);
				String result2 = sh_assignedDate.substring(i2 + 1);
				result2 = result2.trim();
				int j1 = Integer.parseInt(result);
				int j2 = Integer.parseInt(result1);
				int j = Integer.parseInt(result2);

				int i3 = string.indexOf("/");
				String result3 = string.substring(0, i3);
				int i4 = string.lastIndexOf("/");
				String result4 = string.substring(i3 + 1, i4);
				String result5 = string.substring(i4 + 1);
				result5 = result5.trim();
				int k1 = Integer.parseInt(result3);
				int k2 = Integer.parseInt(result4);
				int k = Integer.parseInt(result5);
				if (j > k) {
					chkdate = "false";
				} else if (j < k) {
					chkdate = "true";
				} else if (j == k) {

					if (j1 > k1) {

						chkdate = "false";
					} else if (j1 < k1) {
						chkdate = "true";
					} else if (j1 == k1) {
						if (j2 > k2) {
							chkdate = "false";
						} else if (j2 < k2) {
							chkdate = "true";
						} else if (j2 == k2) {

							chkdate = "true";
						}

					}
		}

				return chkdate;
			} else {
				return "true";
			}
		}
	private String Validation_TodayDate(String string) {
			// TODO Auto-generated method stub
		  	int i1 = string.indexOf("/");
			String result = string.substring(0, i1);
			int i2 = string.lastIndexOf("/");
			String result1 = string.substring(i1 + 1, i2);
			String result2 = string.substring(i2 + 1);
			result2 = result2.trim();
			int j1 = Integer.parseInt(result);
			int j2 = Integer.parseInt(result1);
			int j = Integer.parseInt(result2);
			final Calendar c = Calendar.getInstance();
			int thsyr = c.get(Calendar.YEAR);
			int curmnth = c.get(Calendar.MONTH);
			int curdate = c.get(Calendar.DAY_OF_MONTH);
			int day = c.get(Calendar.DAY_OF_WEEK);
			curmnth = curmnth + 1;

			if (j > thsyr || (j1 > curmnth && j >= thsyr)
					|| (j2 >= curdate && j1 >= curmnth && j >= thsyr)) {
				return "true";
			} else {
				return "false";
			}

		}
	public class MyOnItemSelectedListenerstart implements
		OnItemSelectedListener {

		   	public void onItemSelected(AdapterView<?> parent, View view, int pos,
					long id) {
				startspin = parent.getItemAtPosition(pos).toString();
				spin1 = parent.getSelectedItemPosition();
		    }
		
			public void onNothingSelected(AdapterView parent) {
				// Do nothing.
			}
     }

   public class MyOnItemSelectedListenerend implements OnItemSelectedListener {

	     public void onItemSelected(AdapterView<?> parent, View view, int pos,
			long id) {
			endspin = parent.getItemAtPosition(pos).toString();
			spin2 = parent.getSelectedItemPosition();
        }
	
		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}
	private void Spinner_SetEndtime() {
		// TODO Auto-generated method stub
		mRangeadapter = new ArrayAdapter<CharSequence>(this,
				android.R.layout.simple_spinner_item);
		mRangeadapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		Sch_Spinnerend.setAdapter(mRangeadapter);
		mRangeadapter.add("Select");
		mRangeadapter.add("8:00 AM");
		mRangeadapter.add("8:30 AM");
		mRangeadapter.add("9:00 AM");
		mRangeadapter.add("9:30 AM");
		mRangeadapter.add("10:00 AM");
		mRangeadapter.add("10:30 AM");
		mRangeadapter.add("11:00 AM");
		mRangeadapter.add("11:30 AM");
		mRangeadapter.add("12:00 PM");
		mRangeadapter.add("12:30 PM");
		mRangeadapter.add("1:00 PM");
		mRangeadapter.add("1:30 PM");
		mRangeadapter.add("2:00 PM");
		mRangeadapter.add("2:30 PM");
		mRangeadapter.add("3:00 PM");
		mRangeadapter.add("3:30 PM");
		mRangeadapter.add("4:00 PM");
		mRangeadapter.add("4:30 PM");
		mRangeadapter.add("5:00 PM");
		mRangeadapter.add("5:30 PM");
		mRangeadapter.add("6:00 PM");
		mRangeadapter.add("6:30 PM");
		mRangeadapter.add("7:00 PM");
	}
	private void Spinner_SetStarttime() {
		// TODO Auto-generated method stub
		mRangeadapter = new ArrayAdapter<CharSequence>(this,
				android.R.layout.simple_spinner_item);
		mRangeadapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		Sch_Spinnerstart.setAdapter(mRangeadapter);
		mRangeadapter.add("Select");
		mRangeadapter.add("8:00 AM");
		mRangeadapter.add("8:30 AM");
		mRangeadapter.add("9:00 AM");
		mRangeadapter.add("9:30 AM");
		mRangeadapter.add("10:00 AM");
		mRangeadapter.add("10:30 AM");
		mRangeadapter.add("11:00 AM");
		mRangeadapter.add("11:30 AM");
		mRangeadapter.add("12:00 PM");
		mRangeadapter.add("12:30 PM");
		mRangeadapter.add("1:00 PM");
		mRangeadapter.add("1:30 PM");
		mRangeadapter.add("2:00 PM");
		mRangeadapter.add("2:30 PM");
		mRangeadapter.add("3:00 PM");
		mRangeadapter.add("3:30 PM");
		mRangeadapter.add("4:00 PM");
		mRangeadapter.add("4:30 PM");
		mRangeadapter.add("5:00 PM");
		mRangeadapter.add("5:30 PM");
		mRangeadapter.add("6:00 PM");
		mRangeadapter.add("6:30 PM");
		mRangeadapter.add("7:00 PM");
	}
	 public boolean onKeyDown(int keyCode, KeyEvent event) {
			// replaces the default 'Back' button action
			if (keyCode == KeyEvent.KEYCODE_BACK) {
				cf.goback(12);
				return true;
			}
			if (keyCode == KeyEvent.KEYCODE_MENU) {

			}
			return super.onKeyDown(keyCode, event);
		}
	 protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		 if(requestCode==cf.loadcomment_code)
			{
					load_comment=true;
					if(resultCode==RESULT_OK)
					{
						et_commenttxt.setText((et_commenttxt.getText().toString()+" "+data.getExtras().getString("Comments")).trim());
						
					}
			}		 
	 	}	
}