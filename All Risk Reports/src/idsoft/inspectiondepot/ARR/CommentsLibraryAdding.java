/*
   ************************************************************
   * Project: ALL RISK REPORT
   * Module : Submit.java
   * Creation History:
      Created By : Rakki s on 11/20/2012
   * Modification History:
      Last Modified By : Gowri on 2/08/2013
   ************************************************************  
*/

package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class CommentsLibraryAdding extends  Activity {
CommonFunctions cf;
ArrayAdapter adapter2;
Spinner sp1,sp2,sp3,sp4;
String[] s2={"-Select-","Retail Order"},s3,s4={"-Select-","All"},s1,tmp1,inspectionname,localinspid;
String resmsg="";
EditText comments;
TableLayout tbl_lis;
CheckBox c1,c2;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		cf=new CommonFunctions(this);
		setContentView(R.layout.comments_library_adding);
		cf.CreateARRTable(9);
		
		sp1= (Spinner) findViewById(R.id.lib_comm_insp_sp);
		//sp2= (Spinner) findViewById(R.id.lib_comm_inspT_sp);
		sp3= (Spinner) findViewById(R.id.lib_comm_inspQ_sp);
		sp4= (Spinner) findViewById(R.id.lib_comm_inspO_sp);
		//c2=(CheckBox) findViewById(R.id.ADD_carrier);
		//c1=(CheckBox) findViewById(R.id.ADD_retail);
		comments=(EditText) findViewById(R.id.lib_comments);
		comments.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				cf.showing_limit(s.toString(), ((TextView)findViewById(R.id.comment_lib_limit)), 500);
			}
		});
		tbl_lis=(TableLayout) findViewById(R.id.lib_com_list);
		
		try
		{
			cf.CreateARRTable(14);
		    Cursor c1  =cf.SelectTablefunction(cf.inspnamelist, " ORDER BY ARR_Custom_ID");
		    tmp1=new String[c1.getCount()];System.out.println("dtme"+c1.getCount());
		    localinspid=new String[c1.getCount()];
	        if(c1.getCount()>0)
	        {
	        	c1.moveToFirst();
	        	int i=0;
	        	do
	        	{
	        		tmp1[i] = cf.decode(c1.getString(c1.getColumnIndex("ARR_Insp_Name"))).trim();
	        		localinspid[i] = cf.decode(c1.getString(c1.getColumnIndex("ARR_Custom_ID")));
	        		i++;
	        	}while(c1.moveToNext());
	        }
	        else
	        {
	        	cf.ShowToast("Please import first then try add comments", 1);
	        	cf.gohome();
	        }
	        
		}
		catch(Exception e)
		{
			System.out.println("EEEcomm="+e.getMessage());
		}
		s1=new String[tmp1.length+2];
		s1[0]="--Select--";
		for(int i=1;i<s1.length-1;i++)
		{			
			s1[i]=tmp1[i-1];
			
		}	
		
		s1[s1.length-1]="General Information";
		adapter2 = new ArrayAdapter(CommentsLibraryAdding.this,android.R.layout.simple_spinner_item, s1);
		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp1.setAdapter(adapter2);
			
		sp1.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,int arg2, long arg3) {
				// TODO Auto-generated method stub
				String s=sp1.getSelectedItem().toString().trim();	
				
				if(!s1[0].equals(s))
				{
					String[] tmp=sets3value(sp1.getSelectedItem().toString());
					s3=new String[tmp.length+1];
					s3[0]="-Select-";
					for(int i=1;i<s3.length;i++)
					{
						s3[i]=tmp[i-1];
					}
					adapter2 = new ArrayAdapter(CommentsLibraryAdding.this,android.R.layout.simple_spinner_item, s3);
					adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					sp3.setAdapter(adapter2);
					
					sp3.setSelection(0);
					((TableRow) findViewById(R.id.lib_comm_inspQ_rw)).setVisibility(View.VISIBLE);

					
					/*adapter2 = new ArrayAdapter(CommentsLibraryAdding.this,android.R.layout.simple_spinner_item, s2);
					adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					sp2.setAdapter(adapter2);
					
					sp2.setSelection(0);
					
					((TableRow) findViewById(R.id.lib_comm_inspT_rw)).setVisibility(View.VISIBLE);
					
					((Button) findViewById(R.id.View_comments)).setVisibility(View.VISIBLE);
					try
					{
					showSavedComments();
					}catch (Exception e) {
						// TODO: handle exception
						//System.out.println("this issues not avai");
					}*/
				}
				else
				{
					((TableRow) findViewById(R.id.lib_comm_inspQ_rw)).setVisibility(View.GONE);
					((TableRow) findViewById(R.id.lib_comm_inspC_rw)).setVisibility(View.GONE);
					((LinearLayout) findViewById(R.id.lib_comm_inspB_rw)).setVisibility(View.GONE);
					((Button) findViewById(R.id.Add_comments)).setVisibility(View.GONE);
					((TableRow) findViewById(R.id.lib_comm_inspO_rw)).setVisibility(View.GONE);
					try
					{
						sp3.setSelection(0);
					}catch (Exception e) {
						// TODO: handle exception
						
					}

					/*try
					{
						sp2.setSelection(0);
					}
					catch (Exception e) {
						// TODO: handle exception
					}
					((TableRow) findViewById(R.id.lib_comm_inspT_rw)).setVisibility(View.GONE);
					((TableRow) findViewById(R.id.lib_comm_inspQ_rw)).setVisibility(View.GONE);
					((TableRow) findViewById(R.id.lib_comm_inspC_rw)).setVisibility(View.GONE);
					((Button) findViewById(R.id.Add_comments)).setVisibility(View.GONE);
					
					((TableRow) findViewById(R.id.lib_comm_inspO_rw)).setVisibility(View.GONE);
					((Button) findViewById(R.id.View_comments)).setVisibility(View.GONE);*/
					
				}
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		/*sp2.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				if((sp2.getSelectedItem().toString().trim().equals(s2[1])))
				{
					c2.setVisibility(View.VISIBLE);
					c1.setVisibility(View.GONE);
					c2.setChecked(false);
					c1.setChecked(false);
					
					
				}
				else
				{
					c2.setVisibility(View.GONE);
					c1.setVisibility(View.VISIBLE);
					c2.setChecked(false);
					c1.setChecked(false);
				}
			
				if(sp2.getSelectedItemPosition()!=0)
				{
					String[] tmp=sets3value(sp1.getSelectedItem().toString());
					s3=new String[tmp.length+1];
					s3[0]="-Select-";
					for(int i=1;i<s3.length;i++)
					{
						s3[i]=tmp[i-1];
					}
					adapter2 = new ArrayAdapter(CommentsLibraryAdding.this,android.R.layout.simple_spinner_item, s3);
					adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					sp3.setAdapter(adapter2);
					
					sp3.setSelection(0);
					((TableRow) findViewById(R.id.lib_comm_inspQ_rw)).setVisibility(View.VISIBLE);
				}
				else
				{
					((TableRow) findViewById(R.id.lib_comm_inspQ_rw)).setVisibility(View.GONE);
					((TableRow) findViewById(R.id.lib_comm_inspC_rw)).setVisibility(View.GONE);
					((Button) findViewById(R.id.Add_comments)).setVisibility(View.GONE);
					((TableRow) findViewById(R.id.lib_comm_inspO_rw)).setVisibility(View.GONE);
					try
					{
						sp3.setSelection(0);
					}catch (Exception e) {
						// TODO: handle exception
						
					}
					
				}
				try
				{
				showSavedComments();
				}catch (Exception e) {
					// TODO: handle exception
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});*/System.out.println("sdfdsfdsdsfasdsad");
		sp3.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				String[] tmp=s4;
				if(sp3.getSelectedItemPosition()!=0)
				{
						tmp=sets4value(sp1.getSelectedItem().toString());
		
					adapter2 = new ArrayAdapter(CommentsLibraryAdding.this,android.R.layout.simple_spinner_item, tmp);
					adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					sp4.setAdapter(adapter2);
				
					sp4.setSelection(0);
					((TableRow) findViewById(R.id.lib_comm_inspO_rw)).setVisibility(View.VISIBLE);
				}
				else
				{
					try
					{
						sp4.setSelection(0);
					}catch (Exception e) {
						// TODO: handle exception
						
					}
					((TableRow) findViewById(R.id.lib_comm_inspC_rw)).setVisibility(View.GONE);
					((LinearLayout) findViewById(R.id.lib_comm_inspB_rw)).setVisibility(View.GONE);
					((Button) findViewById(R.id.Add_comments)).setVisibility(View.GONE);
					((TableRow) findViewById(R.id.lib_comm_inspO_rw)).setVisibility(View.GONE);
				
				}
				try
				{
				showSavedComments();
				}catch (Exception e) {
					// TODO: handle exception
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		sp4.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
						
				if(sp4.getSelectedItemPosition()!=0)
				{
					if(sp3.getSelectedItem().toString().equals(cf.getResourcesvalue(R.string.b11802_8)))
					{
						/*if(s2[1].equals(sp2.getSelectedItem().toString()))
						{*/
							/*c1.setVisibility(View.GONE);
							c2.setVisibility(View.GONE);*/
						//}
						
					}
					else
					{
						/*if(sp2.getSelectedItem().toString().equals(s2[1]))
						{
							c2.setVisibility(View.VISIBLE);
						}*/
						/*else if(sp2.getSelectedItem().toString().equals(s2[2]))
						{
							c1.setVisibility(View.VISIBLE);
						}*/
					}
					set_breadcum();
					((TableRow) findViewById(R.id.lib_comm_inspC_rw)).setVisibility(View.VISIBLE);
					((LinearLayout) findViewById(R.id.lib_comm_inspB_rw)).setVisibility(View.VISIBLE);
					((Button) findViewById(R.id.Add_comments)).setVisibility(View.VISIBLE);
					clear_all();
				}
				else
				{
					((TableRow) findViewById(R.id.lib_comm_inspC_rw)).setVisibility(View.GONE);
					((LinearLayout) findViewById(R.id.lib_comm_inspB_rw)).setVisibility(View.GONE);
					((Button) findViewById(R.id.Add_comments)).setVisibility(View.GONE);
					clear_all();
				}
				
				try
				{
				showSavedComments();
				}catch (Exception e) {
					// TODO: handle exception
				}
			}
  
			

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		cf.setupUI(findViewById(R.id.widget54));
		
		ImageView im =(ImageView) findViewById(R.id.head_insp_info);
		im.setVisibility(View.VISIBLE);
		im.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				    Intent s2= new Intent(CommentsLibraryAdding.this,PolicyholdeInfoHead.class);
					Bundle b2 = new Bundle();
					s2.putExtra("homeid", cf.selectedhomeid);
					s2.putExtra("Type", "Inspector");
					s2.putExtra("insp_id", cf.Insp_id);
					((Activity) cf.con).startActivityForResult(s2, cf.info_requestcode);
			}
		});
	}
	private void set_breadcum() {
		// TODO Auto-generated method stub
		((TextView) findViewById(R.id.LC_cum_insp)).setText(sp1.getSelectedItem().toString());
		//((TextView) findViewById(R.id.LC_cum_typ_insp)).setText(sp2.getSelectedItem().toString());
		((TextView) findViewById(R.id.LC_cum_qes)).setText(sp3.getSelectedItem().toString());
		((TextView) findViewById(R.id.LC_cum_typ_qesopt)).setText(sp4.getSelectedItem().toString());
	}
	public void clicker(View v)
	{
		switch(v.getId())
		{
		case R.id.hme:
			cf.gohome();	
		break;
		case R.id.Add_comments:
			
			if(sp1.getSelectedItem().toString().trim().equals("-Select-"))
			{
				cf.ShowToast("Please select the Inspection.", 0);
			}
/*			else if(sp2.getSelectedItem().toString().trim().equals("-Select-"))
			{
				cf.ShowToast("Please select the Type of Inspection.", 0);
			}
*/			else if(sp3.getSelectedItem().toString().trim().equals("-Select-"))
			{
				cf.ShowToast("Please select the Inspection Question.", 0);
			}
			else if(sp4.getSelectedItem().toString().trim().equals("-Select-"))
			{
				cf.ShowToast("Please select the Question option.", 0);
			}
			else if(comments.getText().toString().trim().equals(""))
			{
				
				cf.ShowToast("Please enter the comments.", 0);
				comments.requestFocus();
			}
			else
			{
				save_comments();
			}
		break;
		case R.id.clear:
			try
			{
				/*if(sp2.getSelectedItem().toString().trim().equals(s2[1]))
				{
					c2.setVisibility(View.VISIBLE);
				}
				else if(sp2.getSelectedItem().toString().trim().equals(s2[2]))
				{
					c1.setVisibility(View.VISIBLE);
				}*/
				clear_all();
			}
			catch(Exception e)
			{
				
			}
		break;
		/*case R.id.back_up:
			try
			{
			Cursor c2=cf.SelectTablefunction(cf.Comments_lib, "");
			
			SQLiteDatabase Secondry_db = this.openOrCreateDatabase("Temp", 1, null);
			
			Secondry_db.execSQL("CREATE TABLE IF NOT EXISTS "+cf.Comments_lib+" (_id INTEGER PRIMARY KEY AUTOINCREMENT,CL_inspid VARCHAR( 5 ) DEFAULT ( '' )," +
			 		"CL_inspectionName VARCHAR( 40 )  DEFAULT ( '' ),CL_type VARCHAR( 20 )  DEFAULT ( '' ),CL_question VARCHAR( 20 )  DEFAULT ( '' )," +
			 		"CL_option  VARCHAR( 30 )  DEFAULT ( '' ),CL_Comments    VARCHAR( 250 )  DEFAULT ( '' ),CL_Active    BOOLEAN  DEFAULT false,CL_date     datetime default current_timestamp," +
			 		"CL_Mdate datetime default current_timestamp);");
			String s="";
			c2.moveToFirst();
			for(int i=0;i<c2.getColumnCount();i++)
			{
				if(i+1<c2.getColumnCount())
				 s+="'"+c2.getString(i)+"' as "+c2.getColumnName(i)+" , ";
				else
					s+="'"+c2.getString(i)+"' as "+c2.getColumnName(i)+"";
			}
			for(int j=0;j<c2.getCount();j++)
			{
				s+=" UNION SELECT  ";
				for(int i=0;i<c2.getColumnCount();i++)
				{
					if(i+1<c2.getColumnCount())
					 s+="'"+c2.getString(i)+"' , ";
					else
						s+="'"+c2.getString(i)+"' ";
				}
				c2.moveToNext();
			}
			
			System.out.println("the quesry s="+s);
			Secondry_db.execSQL("INSERT into "+cf.Comments_lib+" Select "+s);
			 try {
			        
			        File data = Environment.getDataDirectory();
			        File sd = Environment.getExternalStorageDirectory();
			        File backupDB = new File(sd, "Temp.txt");
			        if(backupDB.exists())
			        {
			        	backupDB.delete();
			        }
			        if (sd.canWrite()) {
			            String currentDBPath = "\\data\\inspectiondepot.allriskreports\\databases\\"+cf.MY_DATABASE_NAME;
			            String backupDBPath = "Temp";
			            File currentDB = new File(data, currentDBPath);
			            backupDB = new File(sd, backupDBPath);
			            
			            if (currentDB.exists()) {
			                FileChannel src = new FileInputStream(currentDB).getChannel();
			                FileChannel dst = new FileOutputStream(backupDB).getChannel();
			                dst.transferFrom(src, 0, src.size());
			                src.close();
			                dst.close();
			                cf.ShowToast("The back up created successfully", 1);
			            }
			        }
			    } catch (Exception e) {
			    }
			}catch (Exception e) {
				// TODO: handle exception
				cf.ShowToast("the error was="+e.getMessage(), 0);
			}
			ScrollView scr=(ScrollView)findViewById(R.id.widget54);
			View v1 = scr.getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap b = v1.getDrawingCache();             
            String extr = Environment.getExternalStorageDirectory().toString();
            File myPath = new File(extr, "Screen.jpg");
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(myPath);
                b.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                fos.flush();
                fos.close();
               String s= MediaStore.Images.Media.insertImage(getContentResolver(), b, "Screen", "screen");
               System.out.println("the saved path was"+s);
            }catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
            	System.out.println("the error was file not availabel "+e.getMessage());
                e.printStackTrace();
            } catch (Exception e) {
                // TODO Auto-generated catch block
            	System.out.println("the error was"+e.getMessage());
                e.printStackTrace();
            }
		break;*/
		}
	}
	private void showSavedComments() {
		// TODO Auto-generated method stub
		
		String s1_val,s2_val,s3_val,s4_val,commnents_val;
		s1_val=sp1.getSelectedItem().toString().trim();
		//s2_val=sp2.getSelectedItem().toString().trim();
		/*if(s2_val.equals(s2[1]))
		{
			s2_val="11";
		}
		else if(s2_val.equals(s2[2]))
		{
			s2_val="12";
		}
		else
		{
			s2_val="0";
		}*/
		int s5_val=0;
		if(s1_val.trim().equals(s1[s1.length-1]))
		{
			s5_val=10;
		}
		else
		{
			for(int i =0;i<cf.All_insp_list.length;i++)
			{
				System.out.println("It crossed al"+cf.All_insp_list[i]+s1_val+"end");
				if(s1_val.trim().equals(cf.All_insp_list[i]))
				{
					s5_val=i;
				}
			}
		}
		s3_val=sp3.getSelectedItem().toString().trim();
		s4_val=sp4.getSelectedItem().toString().trim();
		System.out.println(" Where CL_inspid='"+cf.Insp_id+"' and CL_inspectionName='"+s5_val+"' " +
				"and CL_question='"+cf.encode(s3_val)+"' and CL_option='"+cf.encode(s4_val)+"' order by CL_Mdate");
		Cursor c=cf.SelectTablefunction(cf.Comments_lib, " Where CL_inspid='"+cf.Insp_id+"' and CL_inspectionName='"+s5_val+"' " +
				"and CL_question='"+cf.encode(s3_val)+"' and CL_option='"+cf.encode(s4_val)+"' order by CL_Mdate");
		
		if(c.getCount()>0)
		{
			
			tbl_lis.removeAllViews();
			TableRow tbl= new TableRow(this);
			TableLayout.LayoutParams lp= new TableLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
			tbl.setBackgroundColor(getResources().getColor(R.color.sage));
			TextView tv;
			View v=new View(this);
			v.setBackgroundColor(Color.BLACK);
			tv = new TextView(this);
			tv.setGravity(Gravity.CENTER_HORIZONTAL);
			tv.setText("No");
			tv.setTextColor(Color.WHITE);
			tv.setPadding(0, 10, 0, 10);
			tbl.addView(tv,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
			v=new View(this);
			v.setBackgroundColor(Color.BLACK);
			tbl.addView(v,1,LayoutParams.FILL_PARENT);
			
			tv = new TextView(this);
			tv.setGravity(Gravity.CENTER_HORIZONTAL);
			tv.setPadding(0, 10, 0, 10);
			tv.setText("Active");
			tv.setTextColor(Color.WHITE);
			tbl.addView(tv,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
			v=new View(this);
			v.setBackgroundColor(Color.BLACK);
			tbl.addView(v,1,LayoutParams.FILL_PARENT);
			
			tv = new TextView(this);
			tv.setTextColor(Color.WHITE);
			tv.setPadding(10, 10, 0, 10);
			tv.setText("Comments");
			tbl.addView(tv,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
			v=new View(this);
			v.setBackgroundColor(Color.BLACK);
			tbl.addView(v,1,LayoutParams.FILL_PARENT);
			
			tv = new TextView(this);
			tv.setText("Edit/Delete");
			tv.setGravity(Gravity.CENTER_HORIZONTAL);
			tv.setPadding(0, 10, 0, 10);
			tv.setTextColor(Color.WHITE);
			tbl.addView(tv,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
			tbl_lis.addView(tbl,lp);
			
			c.moveToFirst();
			for(int i=1;i<=c.getCount();i++,c.moveToNext())
			{
				v=new View(this);
				v.setBackgroundColor(Color.BLACK);
				tbl_lis.addView(v,LayoutParams.FILL_PARENT,1);
				tbl= new TableRow(this);
				tbl.setId(c.getInt(c.getColumnIndex("_id")));
				String status=c.getString(c.getColumnIndex("CL_Active"));
				String cmt=cf.decode(c.getString(c.getColumnIndex("CL_Comments")));
				v.setBackgroundColor(Color.BLACK);
				tv = new TextView(this);
				tv.setGravity(Gravity.CENTER_HORIZONTAL);
				tv.setText(i+"");
				tbl.addView(tv,50,LayoutParams.WRAP_CONTENT);
				v=new View(this);
				v.setBackgroundColor(Color.BLACK);
				tbl.addView(v,1,LayoutParams.FILL_PARENT);
				
				LinearLayout chli=new LinearLayout(this);
				CheckBox ch = new CheckBox(this);
				ch.setOnClickListener(new list_listener(tbl.getId()));
				chli.setGravity(Gravity.CENTER_HORIZONTAL);
				chli.addView(ch,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
				chli.setPadding(0, 10, 10, 10);
				if(status.equals("true"))
				{
					ch.setChecked(true);
				}
			
				ch.setTag("Active");
				tbl.addView(chli,100,LayoutParams.WRAP_CONTENT);
				v=new View(this);
				v.setBackgroundColor(Color.BLACK);
				tbl.addView(v,1,LayoutParams.FILL_PARENT);
				
				tv = new TextView(this);
				tv.setText(cmt);
				tv.setPadding(10, 10, 0, 10);
				tbl.addView(tv,500,LayoutParams.WRAP_CONTENT);
				v=new View(this);
				v.setBackgroundColor(Color.BLACK);
				tbl.addView(v,1,LayoutParams.FILL_PARENT);
				
				ImageView im1 =new ImageView(this,null,R.attr.Roof_RCN_Tit_img);
				ImageView im2 =new ImageView(this,null,R.attr.Roof_RCN_opt_img);
				im1.setTag("Edit");
				im2.setTag("Delete");
				im1.setVisibility(View.VISIBLE);
				im2.setVisibility(View.VISIBLE);
				im1.setPadding(10, 10, 10, 10);
				im1.setOnClickListener(new list_listener(tbl.getId()));
				im2.setOnClickListener(new list_listener(tbl.getId()));
				im2.setPadding(10, 10, 10, 10);
				LinearLayout li=new LinearLayout(this);
				li.addView(im1,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
				li.addView(im2,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
				tbl.addView(li,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
				
				tbl_lis.addView(tbl,lp);
			}
			findViewById(R.id.breadcum).setVisibility(View.VISIBLE);
		}
		else
		{
			tbl_lis.removeAllViews();
			findViewById(R.id.breadcum).setVisibility(View.GONE);
			//cf.ShowToast(" Sorry no records found for this option", 0);
			
		}
		
	}
	
	
	
	class  list_listener implements OnClickListener{ 
		int id;
		list_listener(int id)
		{
			this.id=id;
		}
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if(v.getTag().equals("Active"))
			{
				final CheckBox chk= (CheckBox) v;
				String msg="";
				String Status="false";
				if(chk.isChecked())
				{
					
					msg="Are you sure, Do you want to Activate the selected comments?";
					Status="true";
					resmsg="Activated successfully.";
				}
				else
				{
					msg="Are you sure, Do you want to De-activate the selected comments?";
					Status="false";
					resmsg="De-activated successfully.";
				}
				final String sta=Status;
				AlertDialog.Builder b =new AlertDialog.Builder(CommentsLibraryAdding.this);
				b.setTitle("Confirmation");
				b.setMessage(msg);
				b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						cf.arr_db.execSQL(" UPDATE "+cf.Comments_lib+" Set CL_Active='"+sta+"' WHERE _id="+id);
						
						cf.ShowToast(resmsg, 1);
						
					}
				});
				b.setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						if(sta.equals("true"))
						{
							chk.setChecked(false);
						}else
						{
							chk.setChecked(true);
						}
					}
				});
				AlertDialog al=b.create();al.setIcon(R.drawable.alertmsg);
				al.setCancelable(false);
				al.show();
			}
			else if(v.getTag().equals("Edit"))
			{
				edit_comments(id);
			}
		else if(v.getTag().equals("Delete"))
			{
				
				AlertDialog.Builder b =new AlertDialog.Builder(CommentsLibraryAdding.this);
				b.setTitle("Confirmation");
				b.setMessage("Are you sure, Do you want to delete the selected comment?");
				b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						cf.arr_db.execSQL(" Delete from "+cf.Comments_lib+"  WHERE _id="+id);
						clear_all();
						if(!sp3.getSelectedItem().toString().equals(cf.getResourcesvalue(R.string.b11802_8)))
						{
							/*if(sp2.getSelectedItem().toString().equals(s2[1]))
							{
								c2.setVisibility(View.VISIBLE);
							}
							else if(sp2.getSelectedItem().toString().equals(s2[2]))
							{
								c1.setVisibility(View.VISIBLE);
							}*/
						}
						cf.ShowToast("Deleted successfully.", 1);
						showSavedComments();
					}
				});
				b.setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
					
					}
				});   
				AlertDialog al=b.create();
				al.setIcon(R.drawable.alertmsg);
				al.setCancelable(false);
				al.show();    
			}
		}
	};
		
	private void save_comments() {
		// TODO Auto-generated method stub
		String s1_val,s2_val,s3_val,s4_val,commnents_val;
		s1_val=sp1.getSelectedItem().toString().trim();
		int s5_val=0;
		System.out.println("It crossed al");
		if(s1_val.equals(s1[s1.length-1].trim()))
		{
			s5_val=10;
		}
		else
		{
			for(int i =0;i<cf.All_insp_list.length;i++)
			{
				System.out.println("s1_val"+s1_val+"insp="+cf.All_insp_list[i]);
				if(s1_val.equals(cf.All_insp_list[i]))
				{
					
					System.out.println("comes equals="+cf.All_insp_list[i]+"sss="+s1_val);
					s5_val=i;
				}
			}
		}
		System.out.println("It crossed ajcbvhbxcvhxvhbhjl"+s5_val);
		//int s5_val=sp1.getSelectedItemPosition();
		/*if(s2_val.equals(s2[1]))
		{
			s2_val="11";
		}
		else if(s2_val.equals(s2[2]))
		{
			s2_val="12";
		}
		*/
		
		s3_val=sp3.getSelectedItem().toString().trim();
		s4_val=sp4.getSelectedItem().toString().trim();
		commnents_val=comments.getText().toString().trim();
		String s=((View) findViewById(R.id.Add_comments)).getTag().toString().trim();
		if(s.equals("Add_comments"))
		{
 			/*
			String sql="Select (Case when Count(*)>0 then '11' When  (Select Count(*)  from tbl_commentsLibrary as com1 Where CL_inspid='"+cf.Insp_id+"' and CL_inspectionName='"+cf.encode(s1_val)+"' " +
					"and CL_type='12' and CL_question='"+cf.encode(s3_val)+"' and CL_option='"+cf.encode(s4_val)+"' and CL_Comments='"+cf.encode(commnents_val)+"') >0 then '12'" +
							" Else '0' end ) as type from tbl_commentsLibrary as com Where CL_inspid='"+cf.Insp_id+"' and CL_inspectionName='"+cf.encode(s1_val)+"' " +
					"and CL_type='11' and CL_question='"+cf.encode(s3_val)+"' and CL_option='"+cf.encode(s4_val)+"' and CL_Comments='"+cf.encode(commnents_val)+"'";
			*/
			
			/*if(c1.isChecked()||c2.isChecked())
			{
				c=cf.SelectTablefunction(cf.Comments_lib, " Where CL_inspid='"+cf.Insp_id+"' and CL_inspectionName='"+cf.encode(s1_val)+"' " +
						" and CL_question='"+cf.encode(s3_val)+"' and CL_option='"+cf.encode(s4_val)+"' and CL_Comments='"+cf.encode(commnents_val)+"'");
			}
			else
			{*/
			Cursor c=null;
			c=cf.SelectTablefunction(cf.Comments_lib, " Where CL_inspid='"+cf.Insp_id+"' and CL_inspectionName='"+s5_val+"' " +
					"and CL_question='"+cf.encode(s3_val)+"' and CL_option='"+cf.encode(s4_val)+"' and CL_Comments='"+cf.encode(commnents_val)+"'");
			/*System.out.println("Where CL_inspid='"+cf.Insp_id+"' and CL_inspectionName='"+cf.encode(s1_val)+"' " +
					"and CL_question='"+cf.encode(s3_val)+"' and CL_option='"+cf.encode(s4_val)+"' and CL_Comments='"+cf.encode(commnents_val)+"'");
			System.out.println("coun="+c.getCount());*/
			if(c.getCount()>0)
			{
				cf.ShowToast("Comments already exists", 0);
			}
			else
			{
				//System.out.println("INSERT INTO "+cf.Comments_lib+" (CL_inspid,CL_inspectionName,CL_type ,CL_question ,CL_option ,CL_Comments,CL_Active,CL_date,CL_Mdate) VALUES ('"+cf.Insp_id+"','"+s5_val+"','28','"+cf.encode(s3_val)+"','"+cf.encode(s4_val)+"','"+cf.encode(commnents_val)+"','true',datetime('now'),datetime('now'))");	
				cf.arr_db.execSQL("INSERT INTO "+cf.Comments_lib+" (CL_inspid,CL_inspectionName,CL_type ,CL_question ,CL_option ,CL_Comments,CL_Active,CL_date,CL_Mdate) VALUES ('"+cf.Insp_id+"','"+s5_val+"','28','"+cf.encode(s3_val)+"','"+cf.encode(s4_val)+"','"+cf.encode(commnents_val)+"','true',datetime('now'),datetime('now'))");
				try
				{
					/*if(c1.isChecked() || c2.isChecked())
					{
						String err="";
						if(c1.isChecked())
						{
							s2_val="11";
							err=s2[1];
						}
						else if(c2.isChecked())
						{
							s2_val="12";
							err=s2[2];
						}
						
						Cursor c2=cf.SelectTablefunction(cf.Comments_lib, " Where CL_inspid='"+cf.Insp_id+"' and CL_inspectionName='"+cf.encode(s1_val)+"' " +
								"and CL_type='"+cf.encode(s2_val)+"' and CL_question='"+cf.encode(s3_val)+"' and CL_option='"+cf.encode(s4_val)+"' and CL_Comments='"+cf.encode(commnents_val)+"'");
						System.out.println("the count was "+c2.getCount());
						if(c2.getCount()<=0)
						{
							cf.arr_db.execSQL("INSERT INTO "+cf.Comments_lib+" (CL_inspid,CL_inspectionName,CL_type ,CL_question ,CL_option ,CL_Comments,CL_Active,CL_date,CL_Mdate) VALUES ('"+cf.Insp_id+"','"+cf.encode(s1_val)+"','"+cf.encode(s2_val)+"','"+cf.encode(s3_val)+"','"+cf.encode(s4_val)+"','"+cf.encode(commnents_val)+"','true',datetime('now'),datetime('now'))");
							cf.ShowToast("Comments added successfully in "+s2[1]+" and "+s2[2]+".", 1);
							
						}
						else 
						{
							cf.ShowToast("Comments added successfully in "+sp2.getSelectedItem().toString()+".The same comments available in "+err, 1);
						}
						
					}
					else
					{
						cf.ShowToast("Comments added successfully in "+sp2.getSelectedItem().toString(),1);
					}*/
				}
				catch (Exception e) {
					// TODO: handle exception
					
				}
				
					//c1.setChecked(false);
					//c2.setChecked(false);
			}
			
		}
		else
		{
		
			Cursor avi_c=cf.SelectTablefunction(cf.Comments_lib," Where  CL_inspectionName='"+s5_val+"' and CL_question='"+cf.encode(s3_val)+"' and CL_option='"+cf.encode(s4_val)+"' and CL_Comments='"+cf.encode(commnents_val)+"' and _id<>"+s);
		
			if(avi_c.getCount()<=0)
			{
			cf.arr_db.execSQL("UPDATE  "+cf.Comments_lib+" SET CL_inspectionName='"+s5_val+"',CL_question='"+cf.encode(s3_val)+"',CL_option='"+cf.encode(s4_val)+"',CL_Comments='"+cf.encode(commnents_val)+"',CL_Mdate=datetime('now') WHERE _id="+s);
			((View) findViewById(R.id.Add_comments)).setTag("Add_comments");
			((Button) findViewById(R.id.Add_comments)).setText("Add Comments");
			cf.ShowToast("Comments updated successfully.", 1);
			
			}
		}
		showSavedComments();
		clear_all();
		
	}
	public void edit_comments(int id) {
		// TODO Auto-generated method stub
		Cursor c=cf.SelectTablefunction(cf.Comments_lib, " WHERE _id="+id);
		c.moveToFirst();
		if(c.getCount()>0)
		{
		 String s1_val,s2_val,s3_val,s4_val,comm_val;
		
		 s1_val=cf.decode(c.getString(c.getColumnIndex("CL_inspectionName")));
		// s2_val=cf.decode(c.getString(c.getColumnIndex("CL_type")));
		 s3_val=cf.decode(c.getString(c.getColumnIndex("CL_question")));
		 s4_val=cf.decode(c.getString(c.getColumnIndex("CL_option")));
		 comm_val=cf.decode(c.getString(c.getColumnIndex("CL_Comments")));
		 
		 if(!comm_val.equals(""))
		 { int pos=0;
			
			 comments.setText(comm_val);
		 }
		 ((View) findViewById(R.id.Add_comments)).setTag(id);
		 ((Button) findViewById(R.id.Add_comments)).setText("Update Comments");
		/* c1.setVisibility(View.GONE);
		 c2.setVisibility(View.GONE);
		 c1.setChecked(false);
		 c2.setChecked(false);*/
		}
	}
	public String[] sets3value(String s)
	{
		if(s1[1].equals(s))
		{
			s3=getResources().getStringArray(R.array.four_point_suboption);	
		}
		else if(s1[2].equals(s))
		{
			s3=new String[1];
			s3[0]=getResources().getString(R.string.roof_1);	
		}
		else if(s1[3].equals(s))
		{
			s3=getResources().getStringArray(R.array.b11802_suboption);
				//s3= new String[temp.length-1];System.out.println("SDftes"+s3);
				
				
				
				/*for(int j=0,i=0;i<temp.length;i++)
				{
					if(!cf.getResourcesvalue(R.string.b11802_8).equals(temp[i]))
					{
						s3[j]=temp[i];
						j++;
					}
				}*/
				
		}
		else if(s1[4].equals(s))
		{
			
			
			s3=getResources().getStringArray(R.array.com1_suboption);System.out.println("asd");
				/*s3= new String[temp.length-1];System.out.println("com1_suboption");
				
				for(int j=0,i=0;i<temp.length;i++)
				{
					if(!cf.getResourcesvalue(R.string.b11802_8).equals(temp[i]))
					{
						s3[j]=temp[i];
						j++;
					}
				}*/
				
		}
		else if(s1[5].equals(s))
		{
			//s3=getResources().getStringArray(R.array.com2_suboption);	
			s3=getResources().getStringArray(R.array.gch_suboption);	
		}
		else if(s1[6].equals(s))
		{
			//s3=getResources().getStringArray(R.array.com2_suboption);	
			s3=getResources().getStringArray(R.array.sink_suboption);	
		}
		else if(s1[7].equals(s))
		{
			//s3=getResources().getStringArray(R.array.gch_suboption);
			s3=getResources().getStringArray(R.array.cd_suboption);	
		}
		else if(s1[8].equals(s))
		{
			//s3=getResources().getStringArray(R.array.sink_suboption);
			s3=getResources().getStringArray(R.array.gen_suboption);	
		}
		/*else if(cf.All_insp_list[9].equals(s))
		{
			//s3=new String[1];
			//s3[0]=getResources().getString(R.string.chin);
			s3=getResources().getStringArray(R.array.cd_suboption);	
		}
		else if(cf.All_insp_list[10].equals(s))
		{
			s3=getResources().getStringArray(R.array.gen_suboption);	
		}*/
		return s3;
	}
	
	public String[] sets4value(String s)
	{
		
		
		String[] tmp = s4;
		if(s.equals(s1[3]))
		{
			String b1_q1[] = { "-Select-", "Meets 2001 FBC", "Meets SFBC -94","Unknown - Does Not Meet" };
			String b1_q2[] = { "-Select-","Meets FBC(PD -03/2/02) or Original (after 2004)","Meets SFBC (PD 09/01/1994) or Post 1997","One or more does not meet A or B", "None Meet A or B" };
			String b1_q3[] = { "-Select-", "Mean Uplift less than B and C","103 PSF", "182 PSF", "Concrete", "Other", "Unknown", "No Attic" };
			String b1_q4[] = { "-Select-", "Toe Nail", "Clips", "Single Wraps","Double Wraps", "Structural", "Other", "Unknown", "No Attic" };
			String b1_q5[] = { "-Select-", "Hip Roof", "Flat Roof","Other Roof Type" };
			String b1_q6[] = { "-Select-", "SWR", "No SWR", "Unknown" };
			String b1_q7[] = { "-Select-", "FBC Plywood","All Glazed Openings 4 � 8 lb Large Missile lb.","All Glazed Openings 4 - 8 lb Large Missile","Opening Protection Not Verified", "None / Some Not Protected" };
			String b1_q8[] = { "-Select-", "Wall Construction Comments" };
			String b1_q9[] = { "-Select-", "Addendum Comments" };
			
			int pos=sp3.getSelectedItemPosition();
				switch(pos)
				{ 
					case 0:
						tmp=s4;
					break;
					case 1:
						tmp=b1_q1;
					break;
					case 2:
						tmp=b1_q2;
					break;
					case 3:
						tmp=b1_q3;
					break;
					case 4:
						tmp=b1_q4;
					break;
					case 5:
						tmp=b1_q5;
					break;
					case 6:
						tmp=b1_q6;
					break;
					case 7:
						tmp=b1_q7;
					break;
					case 8:
						tmp=b1_q8;
					break;
					case 9:
						tmp=b1_q9;
					break;
				}
				return tmp;
			
		}
		else if(s.equals(s1[1]))
		{
			String four_q1[] = { "-Select-", "Overall Roof Comments"};
			String four_q2[] = { "-Select-", "Overall Attic Comments"};
			String four_q3[] = { "-Select-", "Overall Plumbing Comments"};
			String four_q4[] = { "-Select-", "Electrical Issues/Hazards Present Comments","Overall Electrical Comments"};
			String four_q5[] = { "-Select-", "Overall HVAC Comments"};
			String four_q6[] = { "-Select-", "Addendum Comments"};
			
				int pos=sp3.getSelectedItemPosition();
			
			
			switch(pos)
			{ 
				case 0:
					tmp=s4;
				break;
				case 1:
					tmp=four_q1;
				break;
				case 2:
					tmp=four_q2;
				break;
				case 3:
					tmp=four_q3;
				break;
				case 4:
					tmp=four_q4;
				break;
				case 5:
					tmp=four_q5;
				break;
				case 6:
					tmp=four_q6;
				break;
			}
			return tmp;
		}
		else if(s.equals(s1[2]))
		{
			String q1[] = { "-Select-", "Overall Roof Comments"};
			
				int pos=sp3.getSelectedItemPosition();
			
			
			switch(pos)
			{ 
				case 0:
					tmp=s4;
				break;
				case 1:
					tmp=q1;
				break;
				
			}
			return tmp;
		}
		else if(s.equals(s1[4]))
		{
			System.out.println("ssssss"+s1[4]);
			String b1_q1[] = { "-Select-", "Meets 2001 FBC", "Meets SFBC -94","Unknown - Does Not Meet" };
			String b1_q2[] = { "-Select-","Meets FBC(PD -03/2/02) or Original (after 2004)","Meets SFBC (PD 09/01/1994) or Post 1997","One or more does not meet A or B", "None Meet A or B","Level A (Non FBC Equivalent) -- Type II or III","Level B (FBC Equivalent) -- Type II or III","Not Applicable","Not Inspected","Unknown" };
			String b1_q3[] = { "-Select-", "Mean Uplift less than B and C","103 PSF", "182 PSF", "Concrete", "Other", "Unknown", "No Attic","Level A -- Wood OR Other Deck Type II only","Level B -- Metal Deck Type II or III","Level C -- Reinforced Concrete Roof Deck Type I, II or III","Not Applicable","Not Inspected" };
			String b1_q4[] = { "-Select-", "Toe Nail", "Clips", "Single Wraps","Double Wraps", "Structural", "Other", "Unknown", "No Attic" };
			String b1_q5[] = { "-Select-", "Hip Roof", "Flat Roof","Other Roof Type" };
			String b1_q6[] = { "-Select-", "SWR", "No SWR", "Unknown","Underlayment","Foamed Adhesive","Not Applicable","Not Inspected"};
			String b1_q7[] = { "-Select-", "FBC Plywood","All Glazed Openings 4 � 8 lb Large Missile lb.","All Glazed Openings 4 - 8 lb Large Missile","Opening Protection Not Verified", "None / Some Not Protected","Class A (Hurricane Impact)","Class B (Basic Impact)","Not Applicable","Not Inspected","Unknown" };
			String b1_q8[] = { "-Select-", "Wall Construction Comments" };
			String b1_q9[] = { "-Select-", "Elevator Comments","Overall Communal Areas Comments"};
			String b1_q10[] = { "-Select-", "Overall Roof Comments"};
			String b1_q11[] = { "-Select-", "Auxilary Building Comments"};
			String b1_q12[] = { "-Select-", "Restaurant Supplement Comments"};
			String b1_q13[] = { "-Select-", "Addendum Comments"};
			int pos=sp3.getSelectedItemPosition();
			switch(pos)
			{ 
					case 0:
						tmp=s4;
					break;
					case 1:
						tmp=b1_q1;
					break;
					case 2:
						tmp=b1_q2;
					break;
					case 3:
						tmp=b1_q3;
					break;
					case 4:
						tmp=b1_q4;
					break;
					case 5:
						tmp=b1_q5;
					break;
					case 6:
						tmp=b1_q6;
					break;
					case 7:
						tmp=b1_q7;
					break;
					case 8:
						tmp=b1_q8;
						/*if(s2[1].equals(sp2.getSelectedItem().toString()))
						{
							tmp=b1_q9;
						}
						else
						{
							tmp=b1_q8;
						}*/
						
					break;
					case 9:
						tmp=b1_q9;
						/*if(s2[1].equals(sp2.getSelectedItem().toString()))
						{
							tmp=b1_q10;
						}
						else
						{
							tmp=b1_q9;
						}*/
						
					break;
					case 10:
						tmp=b1_q10;
					break;
					case 11:
						tmp=b1_q11;
					break;
					case 12:
						tmp=b1_q12;
					break;
					case 13:
						System.out.println("inaii");
						tmp=b1_q13;
					break;
				}
				return tmp;
		}
		/*else if(s.equals(s1[5]))
		{
			String q1[] = { "-Select-", "Level A (Non FBC Equivalent) -- Type II or III","Level B (FBC Equivalent) -- Type II or III","Not Applicable","Not Inspected","Unknown"};
			String q2[] = { "-Select-", "Level A -- Wood OR Other Deck Type II only","Level B -- Metal Deck Type II or III","Level C -- Reinforced Concrete Roof Deck Type I, II or III","Not Applicable","Not Inspected","Unknown"};
			String q3[] = { "-Select-", "Underlayment","Foamed Adhesive","Not Applicable","Not Inspected","Unknown"};
			String q4[] = { "-Select-", "Class A (Hurricane Impact)","Class B (Basic Impact)","Not Applicable","Not Inspected","Unknown"};
			String q5[] = { "-Select-", "Elevator Comments","Overall Communal Areas Comments",};
			String q6[] = { "-Select-", "Overall Roof Comments"};
			String q7[] = { "-Select-", "Auxilary Building Comments"};
			String q8[] = { "-Select-", "Restaurant Supplement Comments"};
			int pos=sp3.getSelectedItemPosition();
			switch(pos)
			{ 
					case 0:
						tmp=s4;
					break;
					case 1:
						tmp=q1;
					break;
					case 2:
						tmp=q2;
					break;
					case 3:
						tmp=q3;
					break;
					case 4:
						tmp=q4;
					break;
					case 5:
						tmp=q5;
					break;
					case 6:
						tmp=q6;
					break;
					case 7:
						tmp=q7;
					break;
					case 8:
						tmp=q8;
					break;
					
				}
				return tmp;
		}
		else if(s.equals(s1[6]))
		{
			String q1[] = { "-Select-", "Level A (Non FBC Equivalent) -- Type II or III","Level B (FBC Equivalent) -- Type II or III","Not Applicable","Not Inspected","Unknown"};
			String q2[] = { "-Select-", "Level A -- Wood OR Other Deck Type II only","Level B -- Metal Deck Type II or III","Level C -- Reinforced Concrete Roof Deck Type I, II or III","Not Applicable","Not Inspected","Unknown"};
			String q3[] = { "-Select-", "Underlayment","Foamed Adhesive","Not Applicable","Not Inspected","Unknown"};
			String q4[] = { "-Select-", "Class A (Hurricane Impact)","Class B (Basic Impact)","Not Applicable","Not Inspected","Unknown"};
			String q5[] = { "-Select-", "Elevator Comments","Overall Communal Areas Comments",};
			String q6[] = { "-Select-", "Overall Roof Comments"};
			String q7[] = { "-Select-", "Auxilary Building Comments"};
			String q8[] = { "-Select-", "Restaurant Supplement Comments"};
			int pos=sp3.getSelectedItemPosition();
			switch(pos)
			{ 
					case 0:
						tmp=s4;
					break;
					case 1:
						tmp=q1;
					break;
					case 2:
						tmp=q2;
					break;
					case 3:
						tmp=q3;
					break;
					case 4:
						tmp=q4;
					break;
					case 5:
						tmp=q5;
					break;
					case 6:
						tmp=q6;
					break;
					case 7:
						tmp=q7;
					break;
					case 8:
						tmp=q8;
					break;
					
				}
				return tmp;
		}*/
        else if(s.equals(s1[5]))
		{
			String q1[] = { "-Select-", "Overall Roof Comments"};
			String q2[] = { "-Select-", "Summary of Hazards and Concerns Comments"};
			String q3[] = { "-Select-", "HVAC Comments"};
			//String q4[] = { "-Select-", "Electric Comments"};
			String q4[] = { "-Select-", "Overall GCH Comments"};
			int pos=sp3.getSelectedItemPosition();
			switch(pos)
			{ 
					case 0:
						tmp=s4;
					break;
					case 1:
						tmp=q1;
					break;
					case 2:
						tmp=q2;
					break;
					case 3:
						tmp=q3;
						break;
					case 4:
						tmp=q4;
						break;
					/*case 5:
						tmp=q5;*/
					
				}
				return tmp;
		}
		else if(s.equals(s1[6]))
		{
			String q1[] = { "-Select-", "Summary Comments","HomeOwner Comments"};
			
			int pos=sp3.getSelectedItemPosition();
			switch(pos)
			{ 
					case 0:
						tmp=s4;
					break;
					case 1:
						tmp=q1;
					break;
				
				}
				return tmp;
		}
		else if(s.equals(s1[7]))
		{
			String q1[] = { "-Select-", "Summary of Conditions/Inspectors Findings Comments"};
			String q2[] = { "-Select-", "Visual Assessment Room Comments"};
			String q3[] = { "-Select-", "Internal HVAC System(s) Comments"};
			String q4[] = { "-Select-", "Internal Appliance System(s) Comments"};
			String q5[] = { "-Select-", "Attic Comments"};
			String q6[] = { "-Select-", "Overall Chinese Drywall Comments"};
			
			int pos=sp3.getSelectedItemPosition();
			
			switch(pos)
			{ 
				case 0:
					tmp=s4;
				break;
				case 1:
					tmp=q1;
				break;
				case 2:
					tmp=q2;
				break;
				case 3:
					tmp=q3;
				break;
				case 4:
					tmp=q4;
				break;
				case 5:
					tmp=q5;
				break;
				case 6:
					tmp=q6;
				break;
			}
			return tmp;
		}
		else if(s.equals(s1[8]))
		{
			String q1[] = { "-Select-", "Initials/Comments"};
			String q2[] = { "-Select-", "Scheduling Comments"};
			String q3[] = { "-Select-", "Overall Building Comments"};
			String q4[] = { "-Select-", "Feedback Comments"};
			int pos=sp3.getSelectedItemPosition();
			switch(pos)
			{ 
				case 0:
					tmp=s4;
				break;
				case 1:
					tmp=q1;
				break;
				case 2:
					tmp=q2;
				break;
				case 3:
					tmp=q3;
				break;
				case 4:
					tmp=q4;
				break;
			}
			return tmp;
		}
		else
		{
			return s4;
		}
		
	}
	public void clear_all()
	{
		comments.setText("");
		((Button) findViewById(R.id.Add_comments)).setTag("Add_comments");
		((Button) findViewById(R.id.Add_comments)).setText("Add Comments");
		
	}
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			startActivity(new Intent(this,HomeScreen.class));
			return true;
		}
		return super.onKeyDown(keyCode, event);
		
	}

	
}
