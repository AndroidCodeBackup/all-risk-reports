package idsoft.inspectiondepot.ARR;

/***   	Page Information
Version :1.0
VCode:1
Created Date:19/10/2012
Purpose:For Policy holder information 
Created By:Rakki S
Last Updated:25/10/2012   
Last Updatedby:Rakki s
***/

import idsoft.inspectiondepot.ARR.BIComments.textwatcher;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.webkit.URLUtil;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class PolicyHolder extends Activity{
	CommonFunctions cf;
	String strcounty2,strstate2;
	String[] arraystateid, arraystatename,
	arraycountyid, arraycountyname,
	arraystateid2, arraystatename2, arraycountyid2, arraycountyname2;
	boolean countysetselection=false,countysetselection2=false,call_county2 = true;
	String statevalue = "false",zipidentifier,state,county,county2,city,stateid,countyid,strstate,strstateid, strstateid2,
			strcounty, strcity = "",strcountyid,
					strcountyid2;
	DataBaseHelper1 dbh1;
	int mc=0,ml=0;
	ArrayAdapter<String> stateadapter,stateadapter2, countyadapter, countyadapter2;
	Spinner spnrstate,spnrcounty,spnstate2,spncounty2;
	boolean ph[]={true,true,true};
	EditText et_firstname,et_lastname,et_address1,et_address2,et_zip,et_hmephn,et_website,
    et_city,et_wrkphn,et_state,et_cellphn,et_county,et_email,et_policyno,et_contactperson,
    et_inspdate,et_commenttxt,et_mailaddress1,et_mailaddress2,et_mailcity,et_mailzip;
    TextView et_companyname;
	CheckBox cb_mailing,cb_email;
    @Override
	    public void onCreate(Bundle savedInstanceState) 
    	{
	        super.onCreate(savedInstanceState);System.out.println("inside policy");
	        cf = new CommonFunctions(this);	      
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.selectedhomeid = extras.getString("homeid");
				cf.onlinspectionid = extras.getString("InspectionType");
			    cf.onlstatus = extras.getString("status");
         	}
         	setContentView(R.layout.policyholder);System.out.println("inside policyholder");
         	/**Used for hide he key board when it clik out side**/
         	cf.setupUI((ScrollView) findViewById(R.id.scr));
        	cf.CreateARRTable(4);
        	cf.CreateARRTable(3);
        	 cf.getInspectorId();
        /**Used for hide he key board when it clik out side**/
         	cf.getDeviceDimensions();
         
         	LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
         	hdr_layout.addView(new HdrOnclickListener(this,1,"General","Policy Holder Info",1,0,cf));
         	LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
         	main_layout.setMinimumWidth(cf.wd);
         	main_layout.addView(new MyOnclickListener(getApplicationContext(), 1, 0,0,cf));System.out.println("test policy");
 			LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);System.out.println("test policy submenu");
			submenu_layout.addView(new MyOnclickListener(this, 12, 1,0,cf));System.out.println("test main");
		    LinearLayout submenu2_layout = (LinearLayout) findViewById(R.id.submenu1); System.out.println("test submenu1");
		    submenu2_layout.addView(new MyOnclickListener(getApplicationContext(), 56, 1,0,cf));System.out.println("test 56");
			Declarations();System.out.println("test devclea");
			set_policyholdervalue();System.out.println("test se policy");

			if (cf.onlinspectionid.equals("11")) 
			{
				et_policyno.setEnabled(true);					
			}
			else
			{
				et_policyno.setEnabled(false);
			}
			System.out.println("Dfdfgdf");
	 }
	private void set_policyholdervalue() {
		// TODO Auto-generated method stub
		/****Show the saved policy holder personal information **/
		try {
			Cursor c2 = cf.arr_db.rawQuery("SELECT * FROM "
					+ cf.policyholder + " WHERE ARR_PH_SRID='"+cf.selectedhomeid+"' and ARR_PH_InspectorId='" + cf.encode(cf.Insp_id) + "'", null);
			System.out.println("SELECT * FROM "
					+ cf.policyholder + " WHERE ARR_PH_SRID='"+cf.selectedhomeid+"' and ARR_PH_InspectorId='" + cf.encode(cf.Insp_id) + "'"+c2.getCount());
			int rws = c2.getCount();
			c2.moveToFirst();
			if (c2 != null) {
				do {
					
					et_firstname.setText(cf.decode(c2.getString(c2.getColumnIndex("ARR_PH_FirstName"))));
					et_lastname.setText(cf.decode(c2.getString(c2.getColumnIndex("ARR_PH_LastName"))));
					et_address1.setText(cf.decode(c2.getString(c2.getColumnIndex("ARR_PH_Address1"))));
					et_address2.setText(cf.decode(c2.getString(c2.getColumnIndex("ARR_PH_Address2"))));
					et_city.setText(cf.decode(c2.getString(c2.getColumnIndex("ARR_PH_City"))));
					
					spnrstate.setSelection(stateadapter.getPosition(cf.decode(c2.getString(c2.getColumnIndex("ARR_PH_State")))));
					
					spnrcounty.setEnabled(true);
					
					countysetselection=true;
					county =cf.decode(c2.getString(c2.getColumnIndex("ARR_PH_County")));
					
					LoadCounty(arraystateid[stateadapter.getPosition(cf.decode(c2.getString(c2.getColumnIndex("ARR_PH_State"))))]);
					
					et_policyno.setText(cf.decode(c2.getString(c2.getColumnIndex("ARR_PH_Policyno"))));
					
					if(c2.getString(c2.getColumnIndex("ARR_PH_InsuranceCompany")).equals("")|| 
							c2.getString(c2.getColumnIndex("ARR_PH_InsuranceCompany")).equals("0") ||
							c2.getString(c2.getColumnIndex("ARR_PH_InsuranceCompany")).equals("NULL"))
					{
						et_companyname.setText("N/A");
					}
					else
					{
						et_companyname.setText(cf.decode(c2.getString(c2.getColumnIndex("ARR_PH_InsuranceCompany"))));	
					}
					
					et_policyno.setText(cf.decode(c2.getString(c2.getColumnIndex("ARR_PH_Policyno"))));
					
					
					et_zip.setText(cf.decode(c2.getString(c2.getColumnIndex("ARR_PH_Zip"))));
					et_contactperson.setText(cf.decode(c2.getString(c2.getColumnIndex("ARR_ContactPerson"))));
					
					System.out.println("ARR_PH_CellPhone"+cf.decode(c2.getString(c2.getColumnIndex("ARR_PH_HomePhone"))));
					if (cf.decode(c2.getString(c2.getColumnIndex("ARR_PH_HomePhone"))).equals("")) {
						et_hmephn.setText("");
					}else{
					et_hmephn.setText(cf.Set_phoneno(cf.decode(c2.getString(c2.getColumnIndex("ARR_PH_HomePhone")))));
					}
					System.out.println("ARR_PH_WorkPhone"+cf.decode(c2.getString(c2.getColumnIndex("ARR_PH_WorkPhone"))));
					if (cf.decode(c2.getString(c2.getColumnIndex("ARR_PH_WorkPhone"))).equals("")) {
						et_wrkphn.setText("");
					}else{
					et_wrkphn.setText(cf.Set_phoneno(cf.decode(c2.getString(c2.getColumnIndex("ARR_PH_WorkPhone")))));
					}
					
					System.out.println("ARR_PH_CellPhone"+cf.decode(c2.getString(c2.getColumnIndex("ARR_PH_CellPhone"))));
					if (cf.decode(c2.getString(c2.getColumnIndex("ARR_PH_CellPhone"))).equals("")) {
						et_cellphn.setText("");
					}else{
					et_cellphn.setText(cf.Set_phoneno(cf.decode(c2.getString(c2.getColumnIndex("ARR_PH_CellPhone")))));
					}
					
					//et_website.setText(cf.decode(c2.getString(c2.getColumnIndex("ARR_PH_WEBSITE"))));System.out.println("ARR_PH_WEBSITE");
					et_email.setText(cf.decode(c2.getString(c2.getColumnIndex("ARR_PH_Email"))));System.out.println("ARR_PH_Email");
					if (cf.decode(c2.getString(c2.getColumnIndex("ARR_PH_EmailChkbx"))).equals("1")) {
						mc=1;
						cb_email.setChecked(true);
						et_email.setEnabled(false);
					} else {
					   mc=0;
						cb_email.setChecked(false);
						et_email.setEnabled(true);
					}
					
					if(cf.decode(c2.getString(c2.getColumnIndex("ARR_PH_Email"))).equals("") && cf.decode(c2.getString(c2.getColumnIndex("ARR_PH_EmailChkbx"))).equals("0"))
					{
						cb_email.setChecked(true);
						et_email.setEnabled(false);
					}
					
					if(!cf.decode(c2.getString(c2.getColumnIndex("ARR_Schedule_Comments"))).equals(""))
					{
						((EditText)findViewById(R.id.policyholdercomm)).setText(cf.decode(c2.getString(c2.getColumnIndex("ARR_Schedule_Comments"))));
						((EditText)findViewById(R.id.policyholdercomm)).setEnabled(false);
					}
					else
					{
						((EditText)findViewById(R.id.policyholdercomm)).setEnabled(true);
					}
					
					
					cf.ScheduledDate=cf.decode(c2.getString(c2.getColumnIndex("ARR_Schedule_ScheduledDate")));						
					et_mailaddress1.setText(cf.decode(c2.getString(c2.getColumnIndex("MailingAddress"))));
					et_mailaddress2.setText(cf.decode(c2.getString(c2.getColumnIndex("MailingAddress2"))));
					et_mailcity.setText(cf.decode(c2.getString(c2.getColumnIndex("Mailingcity"))));
					
					spnstate2.setSelection(stateadapter2.getPosition(cf.decode(c2.getString(c2.getColumnIndex("MailingState"))))); 
					county2 =cf.decode(c2.getString(c2.getColumnIndex("MailingCounty")));

					et_mailzip.setText(cf.decode(c2.getString(c2.getColumnIndex("Mailingzip"))));
				
					
					
				} while (c2.moveToNext());
			}
			
	  } catch (Exception e) {
		 	cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ PolicyHolder.this +" "+" in the processing stage of retrieving data from PH table  at "+" "+ cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
	  }
		
		/****Show the saved mailling address **/
	    try {
			Cursor c2 = cf.arr_db.rawQuery("SELECT * FROM "
					+ cf.MailingPolicyHolder + " WHERE ARR_ML_PH_SRID='" + cf.encode(cf.selectedhomeid)
					+ "' and ARR_ML_PH_InspectorId='" + cf.encode(cf.Insp_id) + "'", null);
			int rws = c2.getCount();
			c2.moveToFirst();
			if (c2 != null) {
				do {
					et_mailaddress1.setText(cf.decode(c2.getString(c2.getColumnIndex("ARR_ML_PH_Address1"))));
					et_mailaddress2.setText(cf.decode(c2.getString(c2.getColumnIndex("ARR_ML_PH_Address2"))));
					et_mailcity.setText(cf.decode(c2.getString(c2.getColumnIndex("ARR_ML_PH_City"))));
					et_mailzip.setText(cf.decode(c2.getString(c2.getColumnIndex("ARR_ML_PH_Zip"))));
					System.out.println("mailing state");
					
					System.out.println("mailing statetest"+cf.decode(c2.getString(c2.getColumnIndex("ARR_ML_PH_State"))));
					
					spnstate2.setSelection(stateadapter2.getPosition(cf.decode(c2.getString(c2.getColumnIndex("ARR_ML_PH_State")))));
					
					spncounty2.setEnabled(true);
					
					countysetselection2=true;
					county2 =cf.decode(c2.getString(c2.getColumnIndex("ARR_ML_PH_County")));
					System.out.println("county2="+county2);

					LoadCounty2(arraystateid[stateadapter2.getPosition(cf.decode(c2.getString(c2.getColumnIndex("ARR_ML_PH_State"))))]);
					
					
					if (cf.decode(c2.getString(c2.getColumnIndex("ARR_ML"))).equals("1")) {
						ml=1;
						cb_mailing.setChecked(true);
					} else {
						ml=0;
						cb_mailing.setChecked(false);
					}
				} while (c2.moveToNext());
				//((EditText)findViewById(R.id.policyholdercomm)).addTextChangedListener(new textwatcher());
			}
			
	  } catch (Exception e) {
				cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ PolicyHolder.this +" "+" in the processing stage of retrieving data from ML table  at "+" "+ cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
	  }
	}
	private void Declarations() {
		// TODO Auto-generated method stub
		et_firstname  = (EditText) findViewById(R.id.etfirstname);et_firstname.addTextChangedListener(new CustomTextWatcherSpace(et_firstname));
		et_lastname  = (EditText) findViewById(R.id.etlastname);et_lastname.addTextChangedListener(new CustomTextWatcherSpace(et_lastname));
		et_address1  = (EditText) findViewById(R.id.etaddress1);et_address1.addTextChangedListener(new CustomTextWatcherSpace(et_address1));
		et_address2  = (EditText) findViewById(R.id.etaddress2);et_address2.addTextChangedListener(new CustomTextWatcherSpace(et_address2));
		et_zip  = (EditText) findViewById(R.id.etzip);
		et_hmephn  = (EditText) findViewById(R.id.ethmephn1);et_hmephn.addTextChangedListener(new CustomTextWatcher(et_hmephn));
		et_city  = (EditText) findViewById(R.id.etcity);et_address2.addTextChangedListener(new CustomTextWatcherSpace(et_address2));
		et_wrkphn  = (EditText) findViewById(R.id.etwrkphn);et_wrkphn.addTextChangedListener(new CustomTextWatcher(et_wrkphn));		
		spnrstate= (Spinner) findViewById(R.id.spntate);
		spnstate2= (Spinner) findViewById(R.id.spnmailstate);
		
		
		//et_state  = (EditText) findViewById(R.id.etstate);et_address2.addTextChangedListener(new CustomTextWatcherSpace(et_address2));
		et_cellphn  = (EditText) findViewById(R.id.etcellphn);et_cellphn.addTextChangedListener(new CustomTextWatcher(et_cellphn));
		spnrcounty= (Spinner) findViewById(R.id.spncounty);
		spncounty2= (Spinner) findViewById(R.id.spnmailcounty);
		
		//et_county  = (EditText) findViewById(R.id.etcounty);et_county.addTextChangedListener(new CustomTextWatcherSpace(et_county));
		
		statevalue = LoadState();

		if (statevalue == "true") {
			stateadapter = new ArrayAdapter<String>(PolicyHolder.this,
					android.R.layout.simple_spinner_item, arraystatename);
			stateadapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spnrstate.setAdapter(stateadapter);

			stateadapter2 = new ArrayAdapter<String>(PolicyHolder.this,
					android.R.layout.simple_spinner_item, arraystatename);
			stateadapter2
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spnstate2.setAdapter(stateadapter2);
		}

		spnrstate.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				strstate = spnrstate.getSelectedItem().toString();
				int stateid = spnrstate.getSelectedItemPosition();
				strstateid = arraystateid[stateid];
				if (!strstate.equals("--Select--")) {
					System.out.println("not select");
					LoadCounty(strstateid);
					spnrcounty.setEnabled(true);
					
				} else {
					System.out.println("inside spinner state else");
					// spnrcounty.setAdapter(null);
					spnrcounty.setEnabled(false);
					arraycountyname = new String[0];
					countyadapter = new ArrayAdapter<String>(
							PolicyHolder.this,
							android.R.layout.simple_spinner_item,
							arraycountyname);
					countyadapter
							.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					spnrcounty.setAdapter(countyadapter);
					
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
		spnstate2.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				call_county2 = true;
				return false;
			}
		});
		spnstate2.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				strstate2 = spnstate2.getSelectedItem().toString();
				int stateid = spnstate2.getSelectedItemPosition();
				strstateid2 = arraystateid[stateid];
				if (call_county2 == true) {
					if (!strstate2.equals("--Select--")) {
						LoadCounty2(strstateid2);
						spncounty2.setEnabled(true);

					} else {
						// spinnercounty2.setAdapter(null);
						spncounty2.setEnabled(false);
						arraycountyname2 = new String[0];
						countyadapter2 = new ArrayAdapter<String>(
								PolicyHolder.this,
								android.R.layout.simple_spinner_item,
								arraycountyname2);
						countyadapter2
								.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						spncounty2.setAdapter(countyadapter2);
					}
				} else {
					spncounty2.setEnabled(true);
					spncounty2.setSelection(spncounty2.getSelectedItemPosition());
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
		spnrcounty.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				strcounty = spnrcounty.getSelectedItem().toString();
				int countyid = spnrcounty.getSelectedItemPosition();
				strcountyid = arraycountyid[countyid];
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
	
		spncounty2.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				strcounty2 = spncounty2.getSelectedItem().toString();
				int countyid = spncounty2.getSelectedItemPosition();
				strcountyid2 = arraycountyid2[countyid];

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
		
		et_email  = (EditText) findViewById(R.id.etemail);et_email.addTextChangedListener(new CustomTextWatcherSpace(et_email));
		et_policyno  = (EditText) findViewById(R.id.etpolicyno);
		et_mailaddress1  = (EditText) findViewById(R.id.etmailaddress1);et_mailaddress1.addTextChangedListener(new CustomTextWatcherSpace(et_mailaddress1));
		//et_mailstate  = (EditText) findViewById(R.id.etmailstate);et_mailstate.addTextChangedListener(new CustomTextWatcherSpace(et_mailstate));
		et_mailaddress2  = (EditText) findViewById(R.id.etmailaddress2);et_mailaddress2.addTextChangedListener(new CustomTextWatcherSpace(et_mailaddress2));
		//et_mailcounty  = (EditText) findViewById(R.id.etmailcounty);et_mailcounty.addTextChangedListener(new CustomTextWatcherSpace(et_mailcounty));
		et_mailcity  = (EditText) findViewById(R.id.etmailcity);et_mailcity.addTextChangedListener(new CustomTextWatcherSpace(et_mailcity));
		et_mailzip  = (EditText) findViewById(R.id.etmailzip);
		et_contactperson  = (EditText) findViewById(R.id.etcontactperson);et_contactperson.addTextChangedListener(new CustomTextWatcherSpace(et_contactperson));
		cb_mailing = (CheckBox)findViewById(R.id.chkmailing);
		et_companyname  = (TextView) findViewById(R.id.etinsurancecompany);
		cb_email = (CheckBox)findViewById(R.id.chkemail);
		//et_website = (EditText)findViewById(R.id.etwebsite);et_website.addTextChangedListener(new CustomTextWatcherSpace(et_website));
		((EditText)findViewById(R.id.policyholdercomm)).addTextChangedListener(new textwatcher());
	}
	public String LoadState() {
		try {
			dbh1 = new DataBaseHelper1(PolicyHolder.this);

			dbh1.createDataBase();
			SQLiteDatabase newDB = dbh1.openDataBase();
			dbh1.getReadableDatabase();
			Cursor cur = newDB.rawQuery("select * from State_Table order by statename",
					null);
			cur.moveToFirst();
			int rows = cur.getCount();
			arraystateid = new String[rows + 1];
			arraystatename = new String[rows + 1];
			arraystateid[0] = "0";
			arraystatename[0] = "--Select--";
			cur.moveToFirst();
			if (cur.getCount() >= 1) {
				int i = 1;
				do {
					String ID = cf.decode(cur.getString(cur
							.getColumnIndex("stateid")));
					String Category = cf.decode(cur.getString(cur
							.getColumnIndex("statename")));
					arraystateid[i] = ID;
					arraystatename[i] = Category;
					i++;

				} while (cur.moveToNext());
			}
			cur.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(getClass().getSimpleName(),
					"Could not create or Open the database1");
		} catch (SQLiteException e) {
			// TODO: handle exception
			System.out.println("Exception " + e.getMessage());
		}
		
		return "true";
	}
	public void LoadCounty(final String stateid) {
		try {
			dbh1 = new DataBaseHelper1(PolicyHolder.this);

			dbh1.createDataBase();
			SQLiteDatabase newDB = dbh1.openDataBase();
			dbh1.getReadableDatabase();
			Cursor cur = newDB.rawQuery("select * from County_Table"
					+ " where stateid='" + cf.encode(stateid) + "' order by countyname", null);
			cur.moveToFirst();
			int rows = cur.getCount();System.out.println("inside load county");
			arraycountyid = new String[rows + 1];
			arraycountyname = new String[rows + 1];
			arraycountyid[0] = "--Select--";
			arraycountyname[0] = "--Select--";
			System.out.println("LoadCounty count is " + rows);
			cur.moveToFirst();
			if (cur.getCount() >= 1) {
				int i = 1;
				do {
					String id = cf.decode(cur.getString(cur
							.getColumnIndex("countyid")));
					String Name = cf.decode(cur.getString(cur
							.getColumnIndex("countyname")));
					arraycountyid[i] = id;
					arraycountyname[i] = Name;
					i++;
				} while (cur.moveToNext());

			}
			LoadCountyData();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(getClass().getSimpleName(),
					"Could not create or Open the database1");
		} catch (SQLiteException e) {
			// TODO: handle exception
			System.out.println("Exception " + e.getMessage());
		}
		
	}
	public void LoadCounty2(final String stateid) {
		try {
			dbh1 = new DataBaseHelper1(PolicyHolder.this);

			dbh1.createDataBase();
			SQLiteDatabase newDB = dbh1.openDataBase();
			dbh1.getReadableDatabase();
			Cursor cur = newDB.rawQuery("select * from County_Table"
					+ " where stateid='" + cf.encode(stateid) + "' order by countyname", null);
			cur.moveToFirst();
			int rows = cur.getCount();
			arraycountyid2 = new String[rows + 1];
			arraycountyname2 = new String[rows + 1];
			arraycountyid2[0] = "--Select--";
			arraycountyname2[0] = "--Select--";
			System.out.println("LoadCounty2 count is " + rows);
			cur.moveToFirst();
			if (cur.getCount() >= 1) {
				int i = 1;
				do {
					String id = cf.decode(cur.getString(cur
							.getColumnIndex("countyid")));
					String Name = cf.decode(cur.getString(cur
							.getColumnIndex("countyname")));
					arraycountyid2[i] = id;
					arraycountyname2[i] = Name;
					i++;
				} while (cur.moveToNext());

			}
			LoadCountyData2();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(getClass().getSimpleName(),
					"Could not create or Open the database1");
		} catch (SQLiteException e) {
			// TODO: handle exception
			System.out.println("Exception " + e.getMessage());
		}
		
	}
	private void LoadCountyData() {
		System.out.println("inside Load County data");
		countyadapter = new ArrayAdapter<String>(PolicyHolder.this,
				android.R.layout.simple_spinner_item, arraycountyname);
		countyadapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spnrcounty.setAdapter(countyadapter);
		System.out.println("Countysele"+countysetselection);
		if(countysetselection)
		{
		System.out.println("what is couny="+county);
			spnrcounty.setSelection(countyadapter.getPosition(county));
			countysetselection=false;
		}
		
	}
	private void LoadCountyData2() {
		System.out.println("inside LoadCountyData2");
		countyadapter2 = new ArrayAdapter<String>(PolicyHolder.this,
				android.R.layout.simple_spinner_item, arraycountyname2);
		countyadapter2
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spncounty2.setAdapter(countyadapter2);
		
		System.out.println("countysetselection2"+countysetselection2);
		if(countysetselection2)
		{
		System.out.println("what is countttttttt="+county2);
			spncounty2.setSelection(countyadapter2.getPosition(county2));
			countysetselection2=false;
		}
	}

	class textwatcher implements TextWatcher
    {
	        public int type;
	        textwatcher()
	    	{

	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {
	    		// TODO Auto-generated method stub
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.phccom_tv_type1),999);	    		
	    	}

	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,

	    			int after) {
	    		// TODO Auto-generated method stub
	     	}

	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,int count) {
	    		// TODO Auto-generated method stub
	    	}
	   }	
	public void clicker(View v) {
			switch (v.getId()) 
			{
				case R.id.save:
					Save_PHData();
					break;
			    case R.id.home:
				  cf.gohome();
				   break;
			    case R.id.chkmailing:
			    	if(cb_mailing.isChecked())
					{	
						 ml = 1;
						et_mailaddress1.setText(et_address1.getText().toString());
						et_mailaddress2.setText(et_address2.getText().toString());
						et_mailcity.setText(et_city.getText().toString());
						
						spnstate2.setSelection(stateadapter2.getPosition(spnrstate.getSelectedItem().toString())); 
						countysetselection2=true;
						county2 =spnrcounty.getSelectedItem().toString();
						
						//et_mailstate.setText(et_state.getText().toString());
						//et_mailcounty.setText(et_county.getText().toString());
						et_mailzip.setText(et_zip.getText().toString());
						
		  		    }
					else
					{	ml=0;
						et_mailaddress1.setText("");
						et_mailaddress2.setText("");
						et_mailcity.setText("");
						spnstate2.setSelection(0);
						spncounty2.setSelection(0);
						//et_mailstate.setText("");
						//et_mailcounty.setText("");
						et_mailzip.setText("");
					}
			   	break;
			    case R.id.chkemail:
				    if(cb_email.isChecked())
					{
						 mc = 1;
						 et_email.setEnabled(false);
						// et_email.setText("");
						 
					}
					else
					{
						mc = 0;
						 et_email.setEnabled(true);
					}
			    break;
			    case R.id.shwph:
			    	hideotherlayouts();
					((LinearLayout)findViewById(R.id.phlin)).setVisibility(cf.v1.VISIBLE);
					((ImageView)findViewById(R.id.hdwph)).setVisibility(cf.v1.VISIBLE);
					((ImageView)findViewById(R.id.shwph)).setVisibility(cf.v1.GONE);
				break;
			    case R.id.hdwph:
					((ImageView)findViewById(R.id.shwph)).setVisibility(cf.v1.VISIBLE);
					((ImageView)findViewById(R.id.hdwph)).setVisibility(cf.v1.GONE);
					((LinearLayout)findViewById(R.id.phlin)).setVisibility(cf.v1.GONE);
					break;
			    case R.id.shwinspaddr:
			    	countysetselection=true;
			    	hideotherlayouts();
					((LinearLayout)findViewById(R.id.inspaddrlin)).setVisibility(cf.v1.VISIBLE);
					((ImageView)findViewById(R.id.hdwinspaddr)).setVisibility(cf.v1.VISIBLE);
					((ImageView)findViewById(R.id.shwinspaddr)).setVisibility(cf.v1.GONE);
				break;
			    case R.id.hdwinspaddr:
					((ImageView)findViewById(R.id.shwinspaddr)).setVisibility(cf.v1.VISIBLE);
					((ImageView)findViewById(R.id.hdwinspaddr)).setVisibility(cf.v1.GONE);
					((LinearLayout)findViewById(R.id.inspaddrlin)).setVisibility(cf.v1.GONE);
					break;
			    case R.id.shwmailaddr:
			    	countysetselection2=true;
			    	hideotherlayouts();
					((LinearLayout)findViewById(R.id.mailingin)).setVisibility(cf.v1.VISIBLE);
					((ImageView)findViewById(R.id.hdwmailaddr)).setVisibility(cf.v1.VISIBLE);
					((ImageView)findViewById(R.id.shwmailaddr)).setVisibility(cf.v1.GONE);
				break;
			    case R.id.hdwmailaddr:
					((ImageView)findViewById(R.id.shwmailaddr)).setVisibility(cf.v1.VISIBLE);
					((ImageView)findViewById(R.id.hdwmailaddr)).setVisibility(cf.v1.GONE);
					((LinearLayout)findViewById(R.id.mailingin)).setVisibility(cf.v1.GONE);
					break;
			}
		}
	private void hideotherlayouts() {
		// TODO Auto-generated method stub
		((LinearLayout)findViewById(R.id.phlin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.inspaddrlin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.mailingin)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.shwph)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwinspaddr)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwmailaddr)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.hdwph)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdwinspaddr)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdwmailaddr)).setVisibility(cf.v1.GONE);
	}
	private void Save_PHData() { 
		// TODO Auto-generated method stub
		ph[0] =(!et_wrkphn.getText().toString().equals(""))? ((cf.PhoneNo_validation(et_wrkphn.getText().toString())!="Yes")? false:true):true;		/****CHECKING WORKPHONE VALIDATION**/
		ph[1] =(!et_cellphn.getText().toString().equals(""))? ((cf.PhoneNo_validation(et_cellphn.getText().toString())!="Yes")? false:true):true;	/***CHECKING CELLPHONE VALIDATION***/
		//ph[2] =(!et_website.getText().toString().equals(""))? ((!Website_validation(et_website.getText().toString()).equals("Valid"))? false:true):true;/**CHECKING WEBSITE VALIDATION**/
		
		/****VALIDATIONS FOR SAVE BUTTON***/

	      if (!"".equals(et_firstname.getText().toString().trim()))  { /***CHECKING FIRSTNAME IS AVAILABLE OR NOT*/
	    	  if (!"".equals(et_lastname.getText().toString().trim())) {/****CHECKING LASTNAME IS AVAILABLE OR NOT*/
	    		  if (!"".equals(et_hmephn.getText().toString().trim())) { /****CHECKING HOMEPHONE IS AVAILABLE OR NOT*/	    			 
	    			  if (cf.PhoneNo_validation(et_hmephn.getText().toString().trim()).equals("Yes")) { /****VALIDATION FOR HOME PHONE*/
	                    if(ph[0]==true) {/****CHECKING WORKPHONE IS AVAILABLE OR NOT*/
	                    	 if(ph[1]==true) {/****CHECKING CELLPHONE IS AVAILABLE OR NOT*/
	                    		 //if(ph[2]==true) {/****CHECKING WEBSITE IS AVAILABLE OR NOT*/
			                    	if (et_email.getText().toString().trim().equals("")&& (mc == 0)) { /****VALIDATION FOR EMAIL*/
										 cf.ShowToast("Please enter the E-mail.", 0);
										 cf.setFocus(et_email);
									 } 
			                    	 else 
			                    	 {
										if (mc == 1) 
										{
											inspectionaddressvalidation();										
										} 
										else if (mc == 0) 
										{
											if (Email_Validation(et_email.getText().toString().trim()) != "Yes") 
											{
												cf.ShowToast("Please enter the valid E-mail.", 0);
												et_email.setText(""); cf.setFocus(et_email);
											} 
											else 
											{
												inspectionaddressvalidation();								
											}
										}
									}
	                    		 /*}
	                    		 else
	                    		 {
	                    			 cf.ShowToast("Please enter the valid Website.", 0);
	                 				et_website.setText("");cf.setFocus(et_website);
	                    		 }*/
	                    	 }
	                    	 else
	                    	 {
	                    		 cf.ShowToast("Cell Phone - requires 10 digit numbers.", 0);
	                    		 et_cellphn.setText(""); cf.setFocus(et_cellphn);
	                    	 }
	                    }
	                    else
	                    {
	                    	cf.ShowToast("Work Phone - requires 10 digit numbers.", 0);
	                    	et_wrkphn.setText(""); cf.setFocus(et_wrkphn);
	                    }
	    			  }
	    			  else 
	    			  {
							cf.ShowToast("Home Phone - requires 10 digit numbers.", 0);
						    et_hmephn.setText(""); cf.setFocus(et_hmephn);
	    			  }
	    		  } 
	    		  else 
	    		  {
	    			  cf.ShowToast("Please enter the Home Phone Number.", 0);
	    			  cf.setFocus(et_hmephn);
	    		  }
	    	  }
	    	  else 
		      {
	    		  	cf.ShowToast("Please enter the Last Name.", 0);
	    		  	cf.setFocus(et_lastname);
		      }
	      }
	      else 
	      {
				cf.ShowToast("Please enter the First Name.", 0);
				cf.setFocus(et_firstname);
	      }
	   
	}
	
	private void inspectionaddressvalidation() {
		// TODO Auto-generated method stub
		//countysetselection=true;
		System.out.println("spnrcounty"+ findViewById(R.id.spncounty));
		if (!"".equals(et_address1.getText().toString().trim())) {/***CHECKING ADDRESS1 IS AVAILABLE OR NOT*/

			 if (!"".equals(et_city.getText().toString().trim())) {/****CHECKING CITY IS AVAILABLE OR NOT*/				 
				 
				   if (et_zip.getText().length() == 5) { /****CHECKING LENGTH OF ZIPCODE*/
					   
					  System.out.println("spnrstate.getSelectedItem().toString().trim()"+spnrstate.getSelectedItem().toString().trim());
					   
					  if (!"--Select--".equals(spnrstate.getSelectedItem().toString().trim()))
					  {//****CHECKING STATE IS AVAILABLE OR NOT*//*
						  /*System.out.println("test"+ spnrcounty);
						  
						  System.out.println("staet="+stateid);
						  String strstate = arraystateid[spnrstate.getSelectedItemPosition()];
						  LoadCounty(strstate);
						  System.out.println("test"+spnrstate.getSelectedItemPosition());*/
						/*   countyadapter = new ArrayAdapter<String>(PolicyHolder.this,android.R.layout.simple_spinner_item,arraycountyname);
							countyadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
							spnrcounty.setAdapter(countyadapter);
						*/  
						 /* System.out.println("test"+ spnrcounty.getAdapter());
						  System.out.println("test"+ spnrcounty.getSelectedItem());
						  System.out.println("test"+ spnrcounty);*/
						  System.out.println("spnrcounty.getSelectedItem().toString().trim()"+spnrcounty.getSelectedItem().toString().trim());
						  if (!"--Select--".equals(spnrcounty.getSelectedItem().toString().trim())) 
						  {//****CHECKING COUNTY IS AVAILABLE OR NOT*//*
							  System.out.println("inside eee");
							  Mailing_Validation();
						   }
						   else
						   {
							   cf.ShowToast("Please enter the Inspection County.", 0);
							  
						   }
					   }
					   else
					   {
						   cf.ShowToast("Please enter the Inspection State.", 0);
						   
					   }
				   }
				   else
				   {
					   cf.ShowToast("Please enter the valid Inspection ZipCode.",0);
					   et_zip.setText("");
					   cf.setFocus(et_zip);
				   }
			 }
			 else
			 {
				 cf.ShowToast("Please enter the Inspection City.",0);
				 cf.setFocus(et_city);
			 }
		}
		else 
		{
				cf.ShowToast("Please enter the Inspection Address.", 0);
				cf.setFocus(et_address1);
		}	
	}
	public void Mailing_Validation()
	{
		//countysetselection2=true;
		if (!"".equals(et_mailaddress1.getText().toString().trim())) {  /*CHECKING MAILING ADRESS1 IS AVAILABLE OR NOT*/
				 if (!"".equals(et_mailcity.getText().toString().trim())) {  /*CHECKING MAILING CITY IS AVAILABLE OR NOT*/
					 if (!"--Select--".equals(spnstate2.getSelectedItem().toString().trim())) {  /*CHECKING MAILING STATE IS AVAILABLE OR NOT*/
						 
					/*	 System.out.println("staet="+stateid);
						  String strstate2 = arraystateid[spnstate2.getSelectedItemPosition()];
						  LoadCounty2(strstate2);
						  System.out.println("test"+spnstate2.getSelectedItemPosition());
						*/ 
						  
							  if (!"".equals(et_mailzip.getText().toString().trim())) {/*CHECKING ZIP IS AVAILABLE OR NOT*/
					                if (et_mailzip.getText().length() == 5) { /*CHECKING LENGTH OF ZIPCODE*/					                      
					                	if (!"--Select--".equals(spncounty2.getSelectedItem().toString().trim()))   /*CHECKING MAILING COUNTY IS AVAILABLE OR NOT*/
				                		{
					                		/*if(((EditText)findViewById(R.id.policyholdercomm)).getText().toString().trim().equals(""))
					                		{
					                			cf.ShowToast("Please enter the Scheduling Comments.", 0);
					                			cf.setFocus(((EditText)findViewById(R.id.policyholdercomm)));
					                		}
					                		else
					                		{*/
				                			  db_update();
					                		//}
				                		}
				                		else
				                		{
				                			cf.ShowToast("Please enter the Mailing County.", 0);
				            
				                		}
					                } else {
										cf.ShowToast("Please enter the valid ZipCode.",0);
										et_mailzip.setText("");cf.setFocus(et_mailzip);
									}
				        	  } else {
									cf.ShowToast("Please enter the ZipCode.", 0);
									cf.setFocus(et_mailzip);
							  }
					 }
			         else {
							cf.ShowToast("Please enter the Mailing State.", 0);
							
						}
				 }
		         else {
						cf.ShowToast("Please enter the Mailing City.", 0);
						cf.setFocus(et_mailcity);
				}
		 }
         else {
				cf.ShowToast("Please enter the Mailing Address1.", 0);
				cf.setFocus(et_mailaddress1);
			}
	}
	private void db_update() {
		// TODO Auto-generated method stub
		try{
						cf.arr_db.execSQL("UPDATE " + cf.policyholder
							+ " SET ARR_PH_FirstName='" + cf.encode(et_firstname.getText().toString())
							+ "',ARR_PH_LastName='"
							+ cf.encode(et_lastname.getText().toString())
							+ "',ARR_PH_Address1='"+ cf.encode(et_address1.getText().toString())+ "',ARR_PH_Address2='"+ cf.encode(et_address2.getText().toString())+ "',ARR_PH_City='"
							+ cf.encode(et_city.getText().toString()) + "',ARR_PH_State='"
							+ cf.encode(spnrstate.getSelectedItem().toString()) + "',ARR_PH_County='"
							+ cf.encode(spnrcounty.getSelectedItem().toString()) + "',ARR_PH_Policyno='"
							+ cf.encode(et_policyno.getText().toString())
							+ "',ARR_PH_InsuranceCompany='" + cf.encode((et_companyname.getText().toString().trim().equals("Not Available"))? "0":et_companyname.getText().toString().trim())
							+ "',ARR_PH_Zip='"
							+ cf.encode(et_zip.getText().toString())
							+ "',ARR_PH_HomePhone='" + cf.encode(et_hmephn.getText().toString())
							+ "',ARR_PH_WorkPhone='" + cf.encode(et_wrkphn.getText().toString())
							+ "',ARR_PH_CellPhone='" + cf.encode(et_cellphn.getText().toString())
							+ "',ARR_PH_WEBSITE='',ARR_PH_Email='"
							+ cf.encode(et_email.getText().toString()) + "',ARR_PH_EmailChkbx='"
							+ cf.encode(Integer.toString(mc)) + "'," +
							"ARR_ContactPerson='"+cf.encode(et_contactperson.getText().toString())+"',ARR_Schedule_Comments='"+cf.encode(((EditText)findViewById(R.id.policyholdercomm)).getText().toString().trim())+"' WHERE ARR_PH_SRID ='" + cf.encode(cf.selectedhomeid)+ "'");
					
					/*CHECKING MAILING ADDRESS TABLE*/
					Cursor c2 = cf.arr_db.rawQuery("SELECT * FROM "
							+ cf.MailingPolicyHolder + " WHERE ARR_ML_PH_SRID='" + cf.encode(cf.selectedhomeid)
							+ "'", null);
					System.out.println("ccc"+c2.getCount());
					if (c2.getCount() == 0) {
							cf.arr_db.execSQL("INSERT INTO "
									+ cf.MailingPolicyHolder
									+ " (ARR_ML_PH_SRID,ARR_ML_PH_InspectorId,ARR_ML,ARR_ML_PH_Address1,ARR_ML_PH_Address2,ARR_ML_PH_City,ARR_ML_PH_Zip,ARR_ML_PH_State,ARR_ML_PH_County)"
									+ "VALUES ('"+cf.encode(cf.selectedhomeid)+"','"+cf.encode(cf.Insp_id)+"','"+cf.encode(Integer.toString(ml))+"','"+cf.encode(et_mailaddress1.getText().toString())+"','"
								    + cf.encode(et_mailaddress2.getText().toString())+"','"+cf.encode(et_mailcity.getText().toString())+"','"
								    + cf.encode(et_mailzip.getText().toString())+"','"+cf.encode(spnstate2.getSelectedItem().toString())+"','"
								    + cf.encode(spncounty2.getSelectedItem().toString())+"')");
					}
					else
					{
						cf.arr_db.execSQL("UPDATE " + cf.MailingPolicyHolder
								+ " SET ARR_ML='"+ml+"',ARR_ML_PH_Address1='" + cf.encode(et_mailaddress1.getText().toString())
								+ "',ARR_ML_PH_Address2='"
								+ cf.encode(et_mailaddress2.getText().toString())
								+ "',ARR_ML_PH_City='"
								+ cf.encode(et_mailcity.getText().toString())
								+ "',ARR_ML_PH_Zip='"
								+ cf.encode(et_mailzip.getText().toString()) + "',ARR_ML_PH_State='"
								+ cf.encode(spnstate2.getSelectedItem().toString()) + "',ARR_ML_PH_County='"
								+ cf.encode(spncounty2.getSelectedItem().toString()) + "' WHERE ARR_ML_PH_SRID ='" + cf.encode(cf.selectedhomeid)
								+ "'");
					}
					cf.ShowToast("Policyholder Information has been updated", 0);
				}
				
				catch(Exception e)
				{
					System.out.println("Policy holder exception="+e.getMessage());
					cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ PolicyHolder.this +" "+" in the processing stage of updating data into PH table  at "+" "+ cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
	}
	 public boolean onKeyDown(int keyCode, KeyEvent event) {
			// replaces the default 'Back' button action
			if (keyCode == KeyEvent.KEYCODE_BACK) {
				Intent  myintent = new Intent(PolicyHolder.this,HomeOwnerList.class);
			 	myintent.putExtra("InspectionType", cf.onlinspectionid);
				myintent.putExtra("status", cf.onlstatus);
				myintent.putExtra("homeid", cf.selectedhomeid);
				startActivity(myintent);
				return true;
			}
			return super.onKeyDown(keyCode, event);
		}
	 
		public String Email_Validation(String string) {
			// TODO Auto-generated method stub
			Pattern pattern;
		    Matcher matcher;
		    final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		    pattern = Pattern.compile(EMAIL_PATTERN);
		    matcher = pattern.matcher(string);
		    if (matcher.matches()) {
	              return "Yes";
	    		} else {
	    			return "No";
	    		}
		}
		public String Website_validation(String string) {
			// TODO Auto-generated method stub
			if(URLUtil.isValidUrl(string))
			{
				return "Valid";
			}
			else{ return "Invalid";}
			
		}
}