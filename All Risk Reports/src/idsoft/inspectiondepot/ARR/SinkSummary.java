/*
   ************************************************************
   * Project: ALL RISK REPORT
   * Module : SinkSummary.java
   * Creation History:
      Created By : Gowri on 
   * Modification History:
      Last Modified By : Gowri on 04/04/2013
   ************************************************************  
*/
package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.SummaryHazards.textwatcher;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class SinkSummary extends Activity {
	CommonFunctions cf;
	RadioGroup plrdgval,aprdgval,ho1rdgval,ho2rdgval,ho3rdgval,ho4rdgval,ho5rdgval,ho6rdgval,ho7rdgval;
	String plrdgtxt="",aprdgtxt="",ho1rdgtxt="",ho2rdgtxt="",ho3rdgtxt="",ho4rdgtxt="",ho5rdgtxt="",ho6rdgtxt="",ho7rdgtxt="";
	int hopchkval=0,k;
	boolean load_comment=true,ho1chk=false,ho2chk=false,ho3chk=false,ho4chk=false,ho5chk=false,ho6chk=false,ho7chk=false;
	
	@Override
	   public void onCreate(Bundle savedInstanceState) {

	        super.onCreate(savedInstanceState);
	        cf=new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);
				
		 	}
	        setContentView(R.layout.sinksummary);
	        cf.getDeviceDimensions();
	        
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
			hdr_layout.addView(new HdrOnclickListener(this,1,"Sinkhole Inspection","Summary Questions",5,0,cf));
			
	        LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
	        mainmenu_layout.setMinimumWidth(cf.wd);
	        mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 5, 0,0,cf));
	        LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 51, 1,0,cf));
			TableRow tblrw = (TableRow)findViewById(R.id.row2);
			tblrw.setMinimumHeight(cf.ht);
			declarations();
			cf.CreateARRTable(51);
			SinkSummary_setvalue();
	}
	private void SinkSummary_setvalue() {
		// TODO Auto-generated method stub
		try
		{
		   Cursor SS_retrive=cf.SelectTablefunction(cf.SINK_Summarytbl, " where fld_srid='"+cf.selectedhomeid+"'");
		   System.out.println("SS_retrive="+SS_retrive.getCount());
		   if(SS_retrive.getCount()>0)
		   {  
			    SS_retrive.moveToFirst();
			   ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
			    plrdgtxt=SS_retrive.getString(SS_retrive.getColumnIndex("fld_plocated"));
			    if(!plrdgtxt.equals("")){
			      ((RadioButton) plrdgval.findViewWithTag(plrdgtxt)).setChecked(true);
			    }
     			aprdgtxt=SS_retrive.getString(SS_retrive.getColumnIndex("fld_aproperty"));
     			if(!aprdgtxt.equals("")){((RadioButton) aprdgval.findViewWithTag(aprdgtxt)).setChecked(true);}
			   ((EditText)findViewById(R.id.sumcomment)).setText(cf.decode(SS_retrive.getString(SS_retrive.getColumnIndex("fld_sumcomments"))));
			    if(SS_retrive.getInt(SS_retrive.getColumnIndex("fld_hopresent"))==0)
			   {
				   ((CheckBox)findViewById(R.id.hop_chk)).setChecked(false);
				   ((TableLayout)findViewById(R.id.vwtbl)).setVisibility(cf.v1.VISIBLE);
				   
				   ho1rdgtxt=SS_retrive.getString(SS_retrive.getColumnIndex("fld_hoq1"));
				   ((RadioButton) ho1rdgval.findViewWithTag(ho1rdgtxt)).setChecked(true);
				   ho2rdgtxt=SS_retrive.getString(SS_retrive.getColumnIndex("fld_hoq2"));
				   ((RadioButton) ho2rdgval.findViewWithTag(ho2rdgtxt)).setChecked(true);
				   ho3rdgtxt=SS_retrive.getString(SS_retrive.getColumnIndex("fld_hoq3"));
				   ((RadioButton) ho3rdgval.findViewWithTag(ho3rdgtxt)).setChecked(true);
				   ho4rdgtxt=SS_retrive.getString(SS_retrive.getColumnIndex("fld_hoq4"));
				   ((RadioButton) ho4rdgval.findViewWithTag(ho4rdgtxt)).setChecked(true);
				   ho5rdgtxt=SS_retrive.getString(SS_retrive.getColumnIndex("fld_hoq5"));
				   ((RadioButton) ho5rdgval.findViewWithTag(ho5rdgtxt)).setChecked(true);
				   ho6rdgtxt=SS_retrive.getString(SS_retrive.getColumnIndex("fld_hoq6"));
				   ((RadioButton) ho6rdgval.findViewWithTag(ho6rdgtxt)).setChecked(true);
				   ho7rdgtxt=SS_retrive.getString(SS_retrive.getColumnIndex("fld_hoq7"));
				   ((RadioButton) ho7rdgval.findViewWithTag(ho7rdgtxt)).setChecked(true);
				   ((EditText)findViewById(R.id.hocomment)).setText(cf.decode(SS_retrive.getString(SS_retrive.getColumnIndex("fld_hocomments"))));
			   }
			   else
			   {
				   ((CheckBox)findViewById(R.id.hop_chk)).setChecked(true);
				   ((TableLayout)findViewById(R.id.vwtbl)).setVisibility(cf.v1.GONE);
			   }
				   
     			
		   }
		   else
		   {
			   ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.GONE);
		   }
		}
	    catch (Exception E)
		{
			String strerrorlog="Retrieving Summary - Sink";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
	private void declarations() {
		// TODO Auto-generated method stub
		 ((EditText)findViewById(R.id.sumcomment)).setOnTouchListener(new Touch_Listener(1));
    	 ((EditText)findViewById(R.id.sumcomment)).addTextChangedListener(new textwatcher(1));
    	 
    	 ((EditText)findViewById(R.id.hocomment)).setOnTouchListener(new Touch_Listener(2));
    	 ((EditText)findViewById(R.id.hocomment)).addTextChangedListener(new textwatcher(2));
    	 
    	 plrdgval = (RadioGroup)findViewById(R.id.pl_rdg);
    	 plrdgval.setOnCheckedChangeListener(new checklistenetr(plrdgval,1));
    	 
    	 aprdgval = (RadioGroup)findViewById(R.id.ap_rdg);
    	 aprdgval.setOnCheckedChangeListener(new checklistenetr(aprdgval,2));
    	 
    	 ho1rdgval = (RadioGroup)findViewById(R.id.ho_rdg1);
    	 ho1rdgval.setOnCheckedChangeListener(new checklistenetr(ho1rdgval,3));
    	 
    	 ho2rdgval = (RadioGroup)findViewById(R.id.ho_rdg2);
    	 ho2rdgval.setOnCheckedChangeListener(new checklistenetr(ho2rdgval,4));
    	 
    	 ho3rdgval = (RadioGroup)findViewById(R.id.ho_rdg3);
    	 ho3rdgval.setOnCheckedChangeListener(new checklistenetr(ho3rdgval,5));
    	 
    	 ho4rdgval = (RadioGroup)findViewById(R.id.ho_rdg4);
    	 ho4rdgval.setOnCheckedChangeListener(new checklistenetr(ho4rdgval,6));
    	 
    	 ho5rdgval = (RadioGroup)findViewById(R.id.ho_rdg5);
    	 ho5rdgval.setOnCheckedChangeListener(new checklistenetr(ho5rdgval,7));
    	 
    	 ho6rdgval = (RadioGroup)findViewById(R.id.ho_rdg6);
    	 ho6rdgval.setOnCheckedChangeListener(new checklistenetr(ho6rdgval,8));
    	 
    	 ho7rdgval = (RadioGroup)findViewById(R.id.ho_rdg7);
    	 ho7rdgval.setOnCheckedChangeListener(new checklistenetr(ho7rdgval,9));
    	
    	
	}
	
	class checklistenetr implements OnCheckedChangeListener
	{
    	RadioGroup aprdgval;
    	int i;
		public checklistenetr(RadioGroup aprdgval, int i) {
			// TODO Auto-generated constructor stub
			this.aprdgval=aprdgval;this.i=i;
		}

		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			   RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
		        // This puts the value (true/false) into the variable
		        boolean isChecked = checkedRadioButton.isChecked();
		        // If the radiobutton that has changed in check state is now checked...
		        if (isChecked)
		        {
		          switch (i) {
					case 1:
						plrdgtxt= checkedRadioButton.getText().toString().trim();
			        	break;
					case 2:
						aprdgtxt= checkedRadioButton.getText().toString().trim();
			        	break;
					case 3:
						ho1rdgtxt= checkedRadioButton.getText().toString().trim();
			        	break;
					case 4:
						ho2rdgtxt= checkedRadioButton.getText().toString().trim();
			        	break;
					case 5:
						ho3rdgtxt= checkedRadioButton.getText().toString().trim();
			        	break;
					case 6:
						ho4rdgtxt= checkedRadioButton.getText().toString().trim();
			        	break;
					case 7:
						ho5rdgtxt= checkedRadioButton.getText().toString().trim();
			        	break;
					case 8:
						ho6rdgtxt= checkedRadioButton.getText().toString().trim();
			        	break;
					case 9:
						ho7rdgtxt= checkedRadioButton.getText().toString().trim();
			        	break;
					default:
						break;
					}
		        }
		}
	}
	public void clicker(View v) {
		switch(v.getId())
		{
		case R.id.sumloadcomments:
			/***Call for the comments***/
			 k=1;
			 int len=((EditText)findViewById(R.id.sumcomment)).getText().toString().length();
			 /*if(len<500)
			 {*/
				if(load_comment )
				{
					load_comment=false;
					int loc[] = new int[2];
					v.getLocationOnScreen(loc);
					Intent in1 = cf.loadComments("Summary Comments",loc);
					in1.putExtra("length", len);
					in1.putExtra("max_length", 500);
					startActivityForResult(in1, cf.loadcomment_code);
				}
			 /*}
			 else
			 {
				cf.ShowToast("You have exceeded the maximum length.", 0);
			 }*/
			
			 break;
		case R.id.hmeloadcomments:
			/***Call for the comments***/
			 k=2;
			 int len1=((EditText)findViewById(R.id.hocomment)).getText().toString().length();
			 /*if(len1<500)
			 {*/
				if(load_comment )
				{
					load_comment=false;
					int loc[] = new int[2];
					v.getLocationOnScreen(loc);
					Intent in1 = cf.loadComments("HomeOwner Comments",loc);
					in1.putExtra("length", len1);
					in1.putExtra("max_length", 500);
					startActivityForResult(in1, cf.loadcomment_code);
				}
			 /*}
			 else
			 {
				cf.ShowToast("You have exceeded the maximum length.", 0);
			 }*/
			 break;
		case R.id.save:
			if(((EditText)findViewById(R.id.sumcomment)).getText().toString().length()>0 && ((EditText)findViewById(R.id.sumcomment)).getText().toString().trim().equals(""))
			{
				  cf.ShowToast("Please enter the valid Summary of Inspectors Findings/Conditions comments.", 0);
				  ((EditText)findViewById(R.id.sumcomment)).setText("");
				  cf.setFocus(((EditText)findViewById(R.id.sumcomment)));
			
			}
			else
			{
				if(((CheckBox)findViewById(R.id.hop_chk)).isChecked())
				{
					Sinksummary_Insert();
				
				}
				else
				{
					if(ho1rdgtxt.equals(""))
					{
						cf.ShowToast("Please select Homeowner/Policy Holder Disclosure Question No.1.", 0);
					}
					else if(ho2rdgtxt.equals(""))
					{
						cf.ShowToast("Please select Homeowner/Policy Holder Disclosure Question No.2.", 0);
					}
					else if(ho3rdgtxt.equals(""))
					{
						cf.ShowToast("Please select Homeowner/Policy Holder Disclosure Question No.3.", 0);
					}
					else if(ho4rdgtxt.equals(""))
					{
						cf.ShowToast("Please select Homeowner/Policy Holder Disclosure Question No.4.", 0);
					}
					else if(ho5rdgtxt.equals(""))
					{
						cf.ShowToast("Please select Homeowner/Policy Holder Disclosure Question No.5.", 0);
					}
					else if(ho6rdgtxt.equals(""))
					{
						cf.ShowToast("Please select Homeowner/Policy Holder Disclosure Question No.6.", 0);
					}
					else if(ho7rdgtxt.equals(""))
					{
						cf.ShowToast("Please select Homeowner/Policy Holder Disclosure Question No.7.", 0);
					}
					else
					{
						if(((EditText)findViewById(R.id.hocomment)).getText().toString().trim().equals(""))
						{
							  cf.ShowToast("Please enter the HomeOwner Comments.", 0);
							  ((EditText)findViewById(R.id.hocomment)).setText("");
						      cf.setFocus(((EditText)findViewById(R.id.hocomment)));
						}
						else
						{
						    Sinksummary_Insert();
						}
					} 
					
				}
			}
			
			break;
		case R.id.hop_chk:
			if(((CheckBox)findViewById(R.id.hop_chk)).isChecked())
			{
				hopchkval=1;((TableLayout)findViewById(R.id.vwtbl)).setVisibility(v.GONE);
				ho1chk = true;ho2chk = true;ho3chk = true;ho4chk = true;ho5chk = true;ho6chk = true;ho7chk = true;
				try{
					if(ho1chk)
				    {
						ho1rdgval.clearCheck();
					}
				}catch (Exception e) {
					// TODO: handle exception
			    }
				try{
					if(ho2chk)
				    {
						ho2rdgval.clearCheck();
					}
			    }catch (Exception e) {
				// TODO: handle exception
		        }
				try{
					if(ho3chk)
				    {
						ho3rdgval.clearCheck();
					}
				 }catch (Exception e) {
					// TODO: handle exception
			        }
				try{
					if(ho4chk)
				    {
						ho4rdgval.clearCheck();
					}
				}catch (Exception e) {
					// TODO: handle exception
			        }
				try{
					if(ho5chk)
				    {
						ho5rdgval.clearCheck();
					}
				}catch (Exception e) {
					// TODO: handle exception
				}
				try{
					if(ho6chk)
				    {
						ho6rdgval.clearCheck();
					}
				}catch (Exception e) {
					// TODO: handle exception
			    }
				try{
					if(ho7chk)
				    {
						ho7rdgval.clearCheck();
					}
				}catch (Exception e) {
						// TODO: handle exception
				}
				((EditText)findViewById(R.id.hocomment)).setText("");
			}
    		else
    		{	
    			hopchkval=0;((TableLayout)findViewById(R.id.vwtbl)).setVisibility(v.VISIBLE);
    		}
			break;
		case R.id.hme:
			cf.gohome();
			break;
		}
	}
	 private void Sinksummary_Insert() {
		// TODO Auto-generated method stub
		 Cursor SS_save=null;
			try
			{
				
				if(((CheckBox)findViewById(R.id.hop_chk)).isChecked())
				{
					hopchkval=1;
				}
				else
				{
					hopchkval=0;
				}
				
				SS_save=cf.SelectTablefunction(cf.SINK_Summarytbl, " where fld_srid='"+cf.selectedhomeid+"'");
				if(SS_save.getCount()>0)
				{
					try
					{
						cf.arr_db.execSQL("UPDATE "+cf.SINK_Summarytbl+ " set fld_plocated='"+plrdgtxt+"',fld_aproperty='"+aprdgtxt+"',"+
					                     "fld_sumcomments='"+cf.encode(((EditText)findViewById(R.id.sumcomment)).getText().toString().trim())+"',"+
								         "fld_hopresent='"+hopchkval+"',fld_hoq1='"+ho1rdgtxt+"',fld_hoq2='"+ho2rdgtxt+"',fld_hoq3='"+ho3rdgtxt+"',"+
					                     "fld_hoq4='"+ho4rdgtxt+"',fld_hoq5='"+ho5rdgtxt+"',fld_hoq6='"+ho6rdgtxt+"',fld_hoq7='"+ho7rdgtxt+"',"+
								         "fld_hocomments='"+cf.encode(((EditText)findViewById(R.id.hocomment)).getText().toString().trim())+"' where fld_srid='"+cf.selectedhomeid+"'");
						cf.ShowToast("Summary Questions updated successfully.", 1);
					    ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
					     cf.goclass(52);
					}
					catch (Exception E)
					{
						String strerrorlog="Updating the Summary of Hazards & Concerns - GCH";
						cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
					}
				}
				else
				{
					try
					{
						cf.arr_db.execSQL("INSERT INTO "
								+ cf.SINK_Summarytbl
								+ " (fld_srid,fld_plocated,fld_aproperty,fld_sumcomments,fld_hopresent,fld_hoq1,fld_hoq2,fld_hoq3,"+
					                     "fld_hoq4,fld_hoq5,fld_hoq6,fld_hoq7,fld_hocomments)"
								+ "VALUES ('"+cf.selectedhomeid+"','"+plrdgtxt+"','"+aprdgtxt+"','"+cf.encode(((EditText)findViewById(R.id.sumcomment)).getText().toString().trim())+"',"+
								         "'"+hopchkval+"','"+ho1rdgtxt+"','"+ho2rdgtxt+"','"+ho3rdgtxt+"','"+ho4rdgtxt+"','"+ho5rdgtxt+"','"+ho6rdgtxt+"','"+ho7rdgtxt+"',"+
								         "'"+cf.encode(((EditText)findViewById(R.id.hocomment)).getText().toString().trim())+"')");

						 cf.ShowToast("Summary Questions saved successfully.", 1);
						 ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE); 
						 cf.goclass(52);
				 	}
					catch (Exception E)
					{
						String strerrorlog="Inserting the summary table - Sink ";
						cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
					}
				}
			}
			catch (Exception E)
			{
				String strerrorlog="Checking the rows inserted in the Sink summary table.";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+"table at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			}
		
	}
	class Touch_Listener implements OnTouchListener

	    {
	    	   public int type;
	    	   Touch_Listener(int type)
	    		{
	    			this.type=type;
	    			
	    		}
	    	    @Override
	    		public boolean onTouch(View v, MotionEvent event) {

	    			// TODO Auto-generated method stub
	    	    	((EditText) findViewById(v.getId())).setFocusableInTouchMode(true);
			    	((EditText) findViewById(v.getId())).requestFocus();
	    			return false;
	    	    }
	    }
	    class textwatcher implements TextWatcher


	    {
	         public int type;
	         textwatcher(int type)
	    	{
	    		this.type=type;
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {


	    		// TODO Auto-generated method stub
	    		if(this.type==1)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.sum_tv_type1),500); 
	    		}
	    		else if(this.type==2)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.ho_tv_type1),500); 
	    		}
	    	}

	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,

	    			int after) {
	    		// TODO Auto-generated method stub
	     	}

	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,

	    			int count) {
	    		// TODO Auto-generated method stub
	    	}
	   }	
	    public boolean onKeyDown(int keyCode, KeyEvent event) {
	 		// replaces the default 'Back' button action
	 		if (keyCode == KeyEvent.KEYCODE_BACK) {
	 			cf.isaccess= cf.chk_InspTypeQuery(cf.selectedhomeid,"Roof Survey");
				if(cf.isaccess==true)
	 			cf.goback(4);
				else
					cf.goback(0);
	 			return true;
	 		}
	 		
	 		return super.onKeyDown(keyCode, event);
	 	}
	    @Override
		protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
			  if(requestCode==cf.loadcomment_code)
			  {
				  load_comment=true;
				if(resultCode==RESULT_OK)
				{
					  if(k==1){
						 
						  String sumcomts = ((EditText)findViewById(R.id.sumcomment)).getText().toString();
					      ((EditText)findViewById(R.id.sumcomment)).setText((sumcomts+" "+data.getExtras().getString("Comments")).trim());
					  }
					  else {
						 
						  String hocomts = ((EditText)findViewById(R.id.hocomment)).getText().toString();
					      ((EditText)findViewById(R.id.hocomment)).setText((hocomts+" "+data.getExtras().getString("Comments")).trim());
					  }
					  cf.ShowToast("Comments added successfully.", 0);
				}
				
			}

		}
}
