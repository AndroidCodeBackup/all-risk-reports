package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView; 
import android.widget.TextView;
import android.widget.Toast;

public class DashboardList extends Activity
{
	int rws;
	ScrollView sv;
	String status,insptype,substatus,colomnname,onlstatus,onlinspectionid,alerttitle,statusofdata;
	String strtit,res = "",headernote,inspdata,anytype = "anyType{}",sql;
	EditText search_text;
	LinearLayout onlinspectionlist;
	TextView tvstatus[];
	Button deletebtn[];
	public String[] data,countarr;
	CommonFunctions cf;
	CommonDeleteInsp del;
	ProgressDialog pd;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle extras = getIntent().getExtras();
		if (extras != null) 
		{
			onlinspectionid = extras.getString("InspectionType");
			onlstatus = extras.getString("status");
	 	}
		setContentView(R.layout.inspectionlist);
		
		cf = new CommonFunctions(this);
		del = new CommonDeleteInsp(this); 
		cf.getInspectorId();
		cf.CreateARRTable(3);
		((TextView) findViewById(R.id.status)).setText(onlstatus);			
		((TextView) findViewById(R.id.inspectiontype)).setText(insptype);
	
		
		if("UTS".equals(status)) 
		{
			headernote="POLICY HOLDER NAME | POLICY NUMBER | ADDRESS | CITY | STATE | COUNTY | ZIP";
		}
		else
		{
			headernote="POLICY HOLDER NAME | POLICY NUMBER | ADDRESS | CITY | STATE | COUNTY | ZIP | INSPECTION DATE | INSPECTION START TIME  - END TIME | Stories | Building No";
		}
		((TextView) findViewById(R.id.note)).setText(headernote);
		
		if (onlstatus.equals("CIO")) {
			alerttitle="Are you sure want to delete all the inspections in the CIO status?";
			statusofdata="41";
			colomnname = "ARR_PH_SubStatus";
		} 
		else 	if (onlstatus.equals("UTS")) {
			alerttitle="Are you sure want to delete all the inspections in the UTS status?";
			statusofdata="110";
			colomnname = "ARR_PH_Status";
		} 
		else 	if (onlstatus.equals("CAN")) 
		{
			alerttitle="Are you sure want to delete all the inspections in the CAN status?";
			statusofdata="90";
			colomnname = "ARR_PH_Status";
		}
		((TextView) findViewById(R.id.status)).setText(onlstatus);	
		((TextView) findViewById(R.id.inspectiontype)).setText(cf.strret);
		onlinspectionlist = (LinearLayout) findViewById(R.id.linlayoutdyn);
		search_text = (EditText)findViewById(R.id.search_text);
		dbquery();
		
		ImageView im =(ImageView) findViewById(R.id.head_insp_info);
		im.setVisibility(View.VISIBLE);
		im.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				    Intent s2= new Intent(DashboardList.this,PolicyholdeInfoHead.class);
					Bundle b2 = new Bundle();
					s2.putExtra("homeid", cf.selectedhomeid);
					s2.putExtra("Type", "Inspector");
					s2.putExtra("insp_id", cf.Insp_id);
					((Activity) cf.con).startActivityForResult(s2, cf.info_requestcode);
			}
		});
		
	}
	private void dbquery() {
		int k = 1;
		data = null;
		
		sql = "select * from " + cf.policyholder;
		if (!res.equals("")) {

			sql += " where (ARR_PH_FirstName like '%" + res
					+ "%' or ARR_PH_LastName like '%" + res
					+ "%' or ARR_PH_Policyno like '%" + res
					+ "%')  and ARR_PH_InspectorId='" + cf.Insp_id + "'";
			if (onlstatus.equals("UTS")) {
				sql += " and  ARR_PH_Status=110";
			} else if (onlstatus.equals("CAN")) {
				sql += " and ARR_PH_Status=90 ";
			//} else if (cf.onlstatus.equals("pretab")) {
				//sql += " and Status=40";
			} else if (onlstatus.equals("CIO")) {
				sql += " and ARR_PH_SubStatus='41' ";
			}
			else if (onlstatus.equals("RR")) {
				sql += " and ((ARR_PH_Status='2' and ARR_PH_SubStatus='0') or (ARR_PH_Status='5' and ARR_PH_SubStatus='0') or (ARR_PH_Status='2' and ARR_PH_SubStatus='121') or (ARR_PH_Status='2' and ARR_PH_SubStatus='122') or (ARR_PH_Status='140'))";
			}

		} else {
			if (onlstatus.equals("UTS")) {
				sql += " where ARR_PH_Status=110  and ARR_PH_InspectorId='" + cf.Insp_id + "'";
			} else if (onlstatus.equals("CAN")) {
				sql += " where ARR_PH_Status=90 ARR_PH_InspectorId='" + cf.Insp_id + "'";
			//} else if (cf.onlstatus.equals("pretab")) {
				//sql += " where Status=40 and  InspectorId='" + cf.Insp_id + "'";
			} else if (onlstatus.equals("CIO")) {
				sql += " where ARR_PH_SubStatus='41' and ARR_PH_InspectorId='" + cf.Insp_id + "'";
			}
			else if (onlstatus.equals("RR")) {
				sql += " where ((ARR_PH_Status='2' and ARR_PH_SubStatus='0') or (ARR_PH_Status='5' and ARR_PH_SubStatus='0') or (ARR_PH_Status='2' and ARR_PH_SubStatus='121') or (ARR_PH_Status='2' and ARR_PH_SubStatus='122') or (ARR_PH_Status='140')) and ARR_PH_InspectorId='" + cf.Insp_id + "'";
			}
		}
		System.out.println("sql="+sql);
		Cursor cur = cf.arr_db.rawQuery(sql, null);
		rws = cur.getCount();

		if (cur.getCount() >= 1) 
		{
			((TextView) findViewById(R.id.noofrecords)).setText("No of Records : "+rws);
			((LinearLayout) findViewById(R.id.dynamiclayout)).setVisibility(cf.v1.VISIBLE);
			((TextView) findViewById(R.id.norecordstxt)).setVisibility(cf.v1.GONE);
		
			data = new String[rws];
			countarr = new String[rws];
			int j = 0;
			cur.moveToFirst();System.out.println("rws count"+rws);
				do {
					String s =(cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_FirstName"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_FirstName")));
					data[j] = " "+ s+ " ";
					s=(cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_LastName"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_LastName")));
					this.data[j] += s + " | ";
					s=(cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_Policyno"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_Policyno")));
					this.data[j] += s+ " | ";
					s=(cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_Address1"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_Address1")));
					this.data[j]+= s + " | ";
					s=(cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_City"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_City")));
					this.data[j] += s + " | ";
					s=(cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_State"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_State")));
					this.data[j] += s + " | ";
					s=(cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_County"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_County")));
					this.data[j] += s + " | ";
					s=(cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_Zip"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_Zip")));
					this.data[j] += s+ " | ";
					if(!onlstatus.equals("Assign")) 
					{
						s=(cf.decode(cur.getString(cur.getColumnIndex("ARR_Schedule_ScheduledDate"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("ARR_Schedule_ScheduledDate")));
						this.data[j] += s + " | ";
						s=(cf.decode(cur.getString(cur.getColumnIndex("ARR_Schedule_InspectionStartTime"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("ARR_Schedule_InspectionStartTime")));
						this.data[j] += s + " - ";
						s=(cf.decode(cur.getString(cur.getColumnIndex("ARR_Schedule_InspectionEndTime"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("ARR_Schedule_InspectionEndTime")));
						this.data[j] += s +" | ";
					}
					
					s=(cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_NOOFSTORIES"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_NOOFSTORIES")));
					this.data[j] += s+ " | ";
					
					s=(cf.decode(cur.getString(cur.getColumnIndex("fld_noofbuildings"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("fld_noofbuildings")));
					this.data[j] += s+ " | ";
					
					countarr[j] = cur.getString(cur.getColumnIndex("ARR_PH_SRID"));
					j++;
	
				} while (cur.moveToNext());
				search_text.setText("");
				display();
		     }
			else 
			{
			
				onlinspectionlist.removeAllViews();
				((LinearLayout) findViewById(R.id.dynamiclayout)).setVisibility(cf.v1.GONE);
				((TextView) findViewById(R.id.norecordstxt)).setVisibility(cf.v1.VISIBLE);
				if(res.equals(""))
				{
					cf.gohome();
				}
				else
				{
					cf.ShowToast("Sorry, No results found.", 1);
					cf.hidekeyboard();
				}
			}
	}
	
	private void display() {
		
		onlinspectionlist.removeAllViews();
		sv = new ScrollView(this);
		onlinspectionlist.addView(sv);

		final LinearLayout l1 = new LinearLayout(this);
		l1.setOrientation(LinearLayout.VERTICAL);
		sv.addView(l1);
		
		View v = new View(this);
		v.setBackgroundResource(R.color.black);
		l1.addView(v,LayoutParams.FILL_PARENT,1);
		if (data.length>=1)
		{				
			for (int i = 0; i < data.length; i++) 
			{		
				tvstatus = new TextView[rws];
				deletebtn = new Button[rws];
				LinearLayout l2 = new LinearLayout(this);
				LinearLayout.LayoutParams mainparamschk = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
				l2.setLayoutParams(mainparamschk);
				l2.setOrientation(LinearLayout.HORIZONTAL);
				l1.addView(l2);

				LinearLayout lchkbox = new LinearLayout(this);
				LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
				paramschk.topMargin = 8;
				paramschk.leftMargin = 20;
				paramschk.bottomMargin = 10;
				l2.addView(lchkbox);
				tvstatus[i] = new TextView(this);
				
				tvstatus[i].setTag("textbtn" + i);
				tvstatus[i].setWidth(600);
				tvstatus[i].setText(data[i]);
				tvstatus[i].setTextColor(Color.BLACK);
				tvstatus[i].setTextSize(TypedValue.COMPLEX_UNIT_PX,14);

			    lchkbox.addView(tvstatus[i], paramschk);

				LinearLayout ldelbtn = new LinearLayout(this);
				LinearLayout.LayoutParams paramsdelbtn = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		        paramsdelbtn.setMargins(0, 10, 10, 0); //left, top, right, bottom
				ldelbtn.setLayoutParams(mainparamschk);
				ldelbtn.setGravity(Gravity.RIGHT);
			    l2.addView(ldelbtn);
				deletebtn[i] = new Button(this);
				deletebtn[i].setBackgroundResource(R.drawable.deletebtn1);
				deletebtn[i].setTag("deletebtn" + i);
				deletebtn[i].setPadding(30, 0, 0, 0);
				ldelbtn.addView(deletebtn[i], paramsdelbtn);
				deletebtn[i].setOnClickListener(new View.OnClickListener() {
					public void onClick(final View v) {
						String getidofselbtn = v.getTag().toString();    
						final String repidofselbtn = getidofselbtn.replace("deletebtn", "");
						final int cvrtstr = Integer.parseInt(repidofselbtn);
						final String dt = countarr[cvrtstr];
						 final Dialog dialog1 = new Dialog(DashboardList.this,android.R.style.Theme_Translucent_NoTitleBar);
							dialog1.getWindow().setContentView(R.layout.alertsync);
							TextView txttitle = (TextView) dialog1.findViewById(R.id.txthelp);
							txttitle.setText("Confirmation");
							TextView txt = (TextView) dialog1.findViewById(R.id.txtid);
							txt.setText(Html.fromHtml("Do you want to delete this inspections?"));
							Button btn_yes = (Button) dialog1.findViewById(R.id.yes);
							Button btn_cancel = (Button) dialog1.findViewById(R.id.cancel);
							btn_yes.setOnClickListener(new OnClickListener()
							{
								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub
									dialog1.dismiss();
									del.delinsp_byrecord(dt);System.out.println("dt"+dt);
									dbquery();				
								}
							});
							btn_cancel.setOnClickListener(new OnClickListener()
							{
								public void onClick(View arg0) {
									// TODO Auto-generated method stub
									dialog1.setCancelable(true);
									dialog1.dismiss();
								}			
							});
							dialog1.setCancelable(false);		
							dialog1.show();
					} 
				});
				

				 if (i % 2 == 0) 
				 {
					 l2.setBackgroundColor(Color.parseColor("#13456d"));
				 } 
				 else 
				 {
				   	l2.setBackgroundColor(Color.parseColor("#386588"));
				  	}
				 if(data.length!=(i+1))
					{
						 View v1 = new View(this);
						 v1.setBackgroundResource(R.color.white);
						 l1.addView(v1,LayoutParams.FILL_PARENT,1);	 
					}
			}
			View v2 = new View(this);
			 v2.setBackgroundResource(R.color.black);
			 onlinspectionlist.addView(v2,LayoutParams.FILL_PARENT,1);
		}
		else
		{
		
		}
		

	}

	public void clicker(View v) {
		switch (v.getId()) {
	
		case R.id.deleteall:			
			 String temp1="(";
			 Cursor c=cf.arr_db.rawQuery(" Select * from "+cf.policyholder+" where "+colomnname+" ="+statusofdata+" and ARR_PH_InspectorId='"+cf.Insp_id+"'",null);
		 	 if( c.getCount()>=1)
			 {
				 c.moveToFirst();
				 for(int i=0;i<c.getCount();i++)
				 {
					 temp1+="'"+c.getString(c.getColumnIndex("ARR_PH_SRID"))+"'";
					 if((i+1)==(c.getCount()))
					 {
						 temp1+=")";
					 }
					 else
					 {
						 temp1+=",";
						 c.moveToNext();
					 }
				 }
				
				 del.delinsp_bystatus(temp1);
			 }else
			 {
				 cf.ShowToast("Record unavailable", 0);
			 }
			  
			break;
		
		  case R.id.home:
			  cf.gohome();
			  break;
		case R.id.search:
			String temp = cf.encode(search_text.getText().toString().trim());
			if (temp.equals("")) {
				cf.ShowToast("Please enter the Name or Policy Number to search.", 1);
				search_text.requestFocus();
			} 
			else
			{
				res = temp;
				dbquery();
			}
			break;
		case R.id.clear:
			search_text.setText("");
			res = "";
			dbquery();
			cf.hidekeyboard();
			break;
				
		}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			startActivity(new Intent(DashboardList.this,Dashboard.class));
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}