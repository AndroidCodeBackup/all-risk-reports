/*
   ************************************************************
   * Project: ALL RISK REPORT
   * Module : GCHElectric.java
   * Creation History:
      Created By : Gowri on 
   * Modification History:
      Last Modified By : Gowri on 04/04/2012
   ************************************************************  
*/
package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

public class GCHHoodduct extends Activity {
	CommonFunctions cf;
	Cursor c1;int chk=0;
	boolean hack[]={false,false,false};
	boolean hoodductchk1=false,hoodductchk2=false,hoodductchk3=false;
	int hoodrdgp_id1,hoodrdgp_id2,hoodrdgp_id3,hoodrdgp_id4;
	RadioGroup hoodductrdgp1,hoodductrdgp2,hoodductrdgp3,hoodductrdgp4;
	boolean hoodchk1=false,hoodchk2=false,hoodchk3=false,hoodchk4=false;
	String filterstxt="",hoodduct_na="",hoodnamecont="",retieve_Hoodduct_val="",hoodductnotappl="0",hoodductrdgpval1="",hoodductrdgpval2="",hoodductrdgpval3="",hoodductrdgpval4="",hoodductval="",hoodchkval="";
	@Override
	   public void onCreate(Bundle savedInstanceState) {

	        super.onCreate(savedInstanceState);
	        cf=new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);				
		 	}
	        setContentView(R.layout.gchhoodduct);
	        cf.getDeviceDimensions();
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
			hdr_layout.addView(new HdrOnclickListener(this,1,"General Conditions & Hazards","Hood and Duct work",4,0,cf));
	        LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
	        mainmenu_layout.setMinimumWidth(cf.wd);
	        mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 4, 0,0,cf));
	        LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 416, 1,0,cf));
			TableRow tblrw = (TableRow)findViewById(R.id.row2);
			tblrw.setMinimumHeight(cf.ht);
			cf.CreateARRTable(416);
			declarations();
			Hoodduct_SetValue();
	}
	private void declarations() 
	{
		cf.setupUI((ScrollView) findViewById(R.id.scr));
		 hoodductrdgp1 = (RadioGroup)findViewById(R.id.hood_rdgp1);hoodductrdgp1.setOnCheckedChangeListener(new checklistenetr(1));
		 hoodductrdgp2 = (RadioGroup)findViewById(R.id.hood_rdgp2);
		 hoodductrdgp3 = (RadioGroup)findViewById(R.id.hood_rdgp3);
		 hoodductrdgp4 = (RadioGroup)findViewById(R.id.hood_rdgp4);
		 ((EditText)findViewById(R.id.edhoodnumbberofcont)).addTextChangedListener(new CustomTextWatcherSpace(((EditText)findViewById(R.id.edhoodnumbberofcont))));
			
	}	
	
	private void Hoodduct_SetValue()	
	{
		chk_values();
		if(c1.getCount()==0)
		{
			((CheckBox)findViewById(R.id.hood_chk)).setChecked(true);
			((LinearLayout)findViewById(R.id.hood_table)).setVisibility(cf.v1.GONE);
		}
		else
		{
		if(hoodduct_na.equals("0") || hoodduct_na.equals(""))
		{
			((LinearLayout)findViewById(R.id.hood_table)).setVisibility(cf.v1.VISIBLE);
			 if(!retieve_Hoodduct_val.equals(""))
			 {
				 String Hood_Split[]= retieve_Hoodduct_val.split("&#94;");			
				((RadioButton) hoodductrdgp1.findViewWithTag(Hood_Split[0])).setChecked(true);	
				
				if(Hood_Split[1].equals("Not Applicable"))
				{
					chk=1;
					((CheckBox)findViewById(R.id.namecontnotappl)).setChecked(true);	
					((EditText)findViewById(R.id.edhoodnumbberofcont)).setEnabled(false);
					((EditText)findViewById(R.id.edhoodnumbberofcont)).setText("Not Applicable");
					
				}
				else
				{
					chk=0;
					((CheckBox)findViewById(R.id.namecontnotappl)).setChecked(false);	
					((EditText)findViewById(R.id.edhoodnumbberofcont)).setText(Hood_Split[1]);
				}
				
				((RadioButton) hoodductrdgp2.findViewWithTag(Hood_Split[2])).setChecked(true);
				((RadioButton) hoodductrdgp3.findViewWithTag(Hood_Split[3])).setChecked(true);	
				
				((RadioButton) hoodductrdgp4.findViewWithTag(Hood_Split[4])).setChecked(true);
				((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
			}
		    else
		    {
		    	((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.GONE);	
		    }				 
		}
		else
		{				
			((CheckBox)findViewById(R.id.hood_chk)).setChecked(true);
			((LinearLayout)findViewById(R.id.hood_table)).setVisibility(cf.v1.GONE);
			((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);			
		}
		}
	}
	class checklistenetr implements OnCheckedChangeListener
	{
    	int i;
		public checklistenetr(int i) {
			// TODO Auto-generated constructor stub
		   this.i=i;
		}

		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			   RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
		        boolean isChecked = checkedRadioButton.isChecked();
		        if (isChecked)
		        {
		          switch (i) 
		          {					
					case 1:
						filterstxt= checkedRadioButton.getText().toString().trim();	
						//chk_values();
						if(!retieve_Hoodduct_val.equals(""))
						{
							
						}
						else
						{
							if(filterstxt.equals("No") || filterstxt.equals("Not Determined"))
							{
								chk=1;
								((EditText)findViewById(R.id.edhoodnumbberofcont)).setText("Not Applicable");
								((EditText)findViewById(R.id.edhoodnumbberofcont)).setEnabled(false);
								((CheckBox)findViewById(R.id.namecontnotappl)).setChecked(true);
								((RadioButton) hoodductrdgp2.findViewWithTag("Not Applicable")).setChecked(true);	
								((RadioButton) hoodductrdgp3.findViewWithTag("Not Applicable")).setChecked(true);
								((RadioButton) hoodductrdgp4.findViewWithTag("Not Applicable")).setChecked(true);								
							}
							else
							{
								chk=0;
								 ((EditText)findViewById(R.id.edhoodnumbberofcont)).setText("");
									hoodductchk1=true;hoodductchk2=true;hoodductchk3=true;
									((CheckBox)findViewById(R.id.namecontnotappl)).setChecked(false);
									((EditText)findViewById(R.id.edhoodnumbberofcont)).setEnabled(true);
									try{if(hoodductchk1){hoodductrdgp2.clearCheck();}}catch(Exception e){}
									try{if(hoodductchk2){hoodductrdgp3.clearCheck();}}catch(Exception e){}
									try{if(hoodductchk3){hoodductrdgp4.clearCheck();}}catch(Exception e){}									
							}
						}
						break;
		          }
		        }
		}
	}

	public void clicker(View v)
	{
		switch (v.getId()) {
		/*case R.id.namecontnotappl:
			if(((RadioButton)findViewById(R.id.namecontnotappl)).isChecked())
			{
				
				((EditText)findViewById(R.id.edhoodnumbberofcont)).setText("Not Applicable");
				((RadioButton)findViewById(R.id.namecontnotappl)).setChecked(true);
			}
			else
			{
				
			}
			break;*/
		case R.id.namecontnotappl:
			if(((CheckBox)findViewById(R.id.namecontnotappl)).isChecked())
			{
				chk=1;
				System.out.println("checked");
				((EditText)findViewById(R.id.edhoodnumbberofcont)).setText("Not Applicable");
				((EditText)findViewById(R.id.edhoodnumbberofcont)).setEnabled(false);
				((RadioButton) hoodductrdgp2.findViewWithTag("Not Applicable")).setChecked(true);
				((RadioButton) hoodductrdgp3.findViewWithTag("Not Applicable")).setChecked(true);
				((RadioButton) hoodductrdgp4.findViewWithTag("Not Applicable")).setChecked(true);	
			}
			else
			{
				chk=0;hack[0] = true;hack[1] = true;hack[2] = true;
				System.out.println("notchecked");
				((EditText)findViewById(R.id.edhoodnumbberofcont)).setEnabled(true);
				((EditText)findViewById(R.id.edhoodnumbberofcont)).setText("");
				 try{
					if(hack[0]){hoodductrdgp2.clearCheck();}
				 }catch(Exception e){}
				 try{
						if(hack[1]){hoodductrdgp3.clearCheck();}
					 }catch(Exception e){}
				 try{
						if(hack[2]){hoodductrdgp4.clearCheck();}
					 }catch(Exception e){}
				
			}
			break;
		case R.id.hood_chk:
			if(((CheckBox)findViewById(R.id.hood_chk)).isChecked())
			{
				
				if(!retieve_Hoodduct_val.equals(""))
				{
					showunchkalert();
				}
				else
				{
					UncheckHoodDuct();
					((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.GONE);
					((LinearLayout)findViewById(R.id.hood_table)).setVisibility(cf.v1.GONE);
				}				
			}
			else
			{
				UncheckHoodDuct();
				 ((LinearLayout)findViewById(R.id.hood_table)).setVisibility(cf.v1.VISIBLE);
				((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.GONE);
				((CheckBox)findViewById(R.id.hood_chk)).setChecked(false);
			}
			break;
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.save:
			if(((CheckBox)findViewById(R.id.hood_chk)).isChecked())
			{
				hoodductnotappl="1";inserthoodductvalues();
				cf.ShowToast("Hood and Duct Work saved successfully", 0);
				((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
				cf.goclass(43);
				
			}
			else
			{
				hoodductnotappl="0";
				hoodrdgp_id1= hoodductrdgp1.getCheckedRadioButtonId();	
				hoodductrdgpval1 = (hoodrdgp_id1==-1)? "":((RadioButton) findViewById(hoodrdgp_id1)).getText().toString();
				hoodrdgp_id2= hoodductrdgp2.getCheckedRadioButtonId();	
				hoodductrdgpval2 = (hoodrdgp_id2==-1)? "":((RadioButton) findViewById(hoodrdgp_id2)).getText().toString();
				hoodrdgp_id3= hoodductrdgp3.getCheckedRadioButtonId();	
				hoodductrdgpval3 = (hoodrdgp_id3==-1)? "":((RadioButton) findViewById(hoodrdgp_id3)).getText().toString();
				hoodrdgp_id4= hoodductrdgp4.getCheckedRadioButtonId();	
				hoodductrdgpval4 = (hoodrdgp_id4==-1)? "":((RadioButton) findViewById(hoodrdgp_id4)).getText().toString();
				
				if(!hoodductrdgpval1.trim().equals(""))
				 {
					 if(!((EditText)findViewById(R.id.edhoodnumbberofcont)).getText().toString().trim().equals("") || ((CheckBox)findViewById(R.id.namecontnotappl)).isChecked()==true)
					 {
						 if(!hoodductrdgpval2.trim().equals(""))
						 {
							 if(!hoodductrdgpval3.trim().equals(""))
							 {
								 if(!hoodductrdgpval4.trim().equals(""))
								 {
									 inserthoodductvalues();
									 cf.ShowToast("Hood and Duct Work saved successfully", 0);
										((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
										cf.goclass(43);
								 }
								 else
								 {
									 cf.ShowToast("Please select the option for Is 18 in. clearance provided between all combustibles?", 0);
								 }
							 }
							 else
							 {
								 cf.ShowToast("Please select the option for Are Hoods, Ducts and Filters Clean?", 0);
							 }
						 }
						 else
						 {
							 cf.ShowToast("Please select the option for If by Contractor, is there a contract?", 0);
						 }
					 }
					 else
					 {
						 cf.ShowToast("Please enter the Name of Contractor.", 0);
						 cf.setFocus(((EditText)findViewById(R.id.edhoodnumbberofcont)));	
					 }
				 }
				else
				{
					cf.ShowToast("Please select the option for Filters Frequently Cleaned.", 0);
				}
			}
			break;
		}
	}
	protected void UncheckHoodDuct() {
		// TODO Auto-generated method stub
		retieve_Hoodduct_val="";
		hoodchk1=true;hoodchk2=true;hoodchk3=true;hoodchk4=true;
		try{if(hoodchk1){hoodductrdgp1.clearCheck();}}catch(Exception e){}	
		try{if(hoodchk2){hoodductrdgp2.clearCheck();}}catch(Exception e){}	
		try{if(hoodchk3){hoodductrdgp3.clearCheck();}}catch(Exception e){}	
		try{if(hoodchk4){hoodductrdgp4.clearCheck();}}catch(Exception e){}	
		((EditText)findViewById(R.id.edhoodnumbberofcont)).setText("");
		//((RadioButton)findViewById(R.id.namecontnotappl)).setChecked(false);		
		((CheckBox)findViewById(R.id.namecontnotappl)).setChecked(false);
		hoodductrdgpval1="";hoodductrdgpval2="";hoodductrdgpval3="";hoodductrdgpval4="";retieve_Hoodduct_val="";
	}
	
	private void chk_values()
	{
		try
		{
			c1 = cf.arr_db.rawQuery("SELECT * FROM "+ cf.GCH_HoodDuct + " WHERE fld_srid='"+ cf.selectedhomeid + "'", null);				
			if(c1.getCount()>0)
			{
				c1.moveToFirst();
				hoodduct_na = c1.getString(c1.getColumnIndex("fld_hoodduct_na"));
				retieve_Hoodduct_val = cf.decode(c1.getString(c1.getColumnIndex("fld_hoodandductwork")));
			}
		}
		catch(Exception e){
			System.out.println("EEe"+e.getMessage());
			
		}
	}
	public void showunchkalert() {
		// TODO Auto-generated method stub
		AlertDialog.Builder bl = new Builder(GCHHoodduct.this);
		bl.setTitle("Confirmation");
		bl.setMessage(Html.fromHtml("Do you want to clear the Hood and Duct Work data?"));
		bl.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog,
											int id) {
										((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.GONE);
										UncheckHoodDuct();//inserthoodductvalues();
										((LinearLayout)findViewById(R.id.hood_table)).setVisibility(cf.v1.GONE);
										
									}
		});
		bl.setNegativeButton("No",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog,
							int id) {
						
							 ((CheckBox)findViewById(R.id.hood_chk)).setChecked(false);
					
						 }
        });
		AlertDialog al=bl.create();
		al.setIcon(R.drawable.alertmsg);
		al.setCancelable(false);
		al.show();
	}
	
	private void inserthoodductvalues() {
		// TODO Auto-generated method stub		
		if(!hoodductrdgpval1.equals("") && (!((EditText)findViewById(R.id.edhoodnumbberofcont)).getText().toString().trim().equals("") || ((CheckBox)findViewById(R.id.namecontnotappl)).isChecked()==true) && !hoodductrdgpval2.equals("") && !hoodductrdgpval3.equals("") && !hoodductrdgpval4.equals(""))
		{
			if(((EditText)findViewById(R.id.edhoodnumbberofcont)).getText().toString().trim().equals(""))
			{
				hoodnamecont = ((CheckBox)findViewById(R.id.namecontnotappl)).getText().toString();
			}
			else
			{
				hoodnamecont = 	((EditText)findViewById(R.id.edhoodnumbberofcont)).getText().toString();	
			}
			
			
			hoodductval = hoodductrdgpval1+"&#94;"+cf.encode(hoodnamecont)+"&#94;"
						+hoodductrdgpval2+"&#94;"+hoodductrdgpval3+"&#94;"+hoodductrdgpval4;
		}
		else
		{
			hoodductval="";
		}
		try
		{
			chk_values();
			if(c1.getCount()==0)
			{
				cf.arr_db.execSQL("INSERT INTO " + cf.GCH_HoodDuct + " (fld_srid,fld_hoodduct_na,fld_hoodandductwork)"
						+ "VALUES ('"+cf.selectedhomeid+"','"+hoodductnotappl+"','"+hoodductval+"')");
			}
			else
			{
				cf.arr_db.execSQL("UPDATE "	+ cf.GCH_HoodDuct	+ " SET fld_hoodduct_na='"+hoodductnotappl+"',fld_hoodandductwork='"+hoodductval+"' WHERE fld_srid ='"+ cf.selectedhomeid + "'");	
			}	
			 
		}
		catch(Exception e)
		{
			System.out.println("sdfdfds"+e.getMessage());
		}
		
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
 		// replaces the default 'Back' button action
 		if (keyCode == KeyEvent.KEYCODE_BACK) {
 			//cf.goclass(419);
 			cf.gotoclass(35, WindMitCommRestsupp.class);	
 			return true;
 		}
 		
 		return super.onKeyDown(keyCode, event);
 	}
}