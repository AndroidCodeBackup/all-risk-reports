package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView; 
import android.widget.TextView;
import android.widget.Toast;

public class Deleteinspection extends Activity
{
	int rws;
	ScrollView sv;
	String res = "";
	EditText search_text;
	LinearLayout onlinspectionlist;
	TextView tvstatus[];
	Button deletebtn[];
	public String[] data,countarr;
	CommonFunctions cf;
	CommonDeleteInsp del;
	

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.deleteinspection);
		cf = new CommonFunctions(this);
		del = new CommonDeleteInsp(this); 
		cf.getInspectorId();
		cf.CreateARRTable(3);
		onlinspectionlist = (LinearLayout) findViewById(R.id.linlayoutdyn);
		search_text = (EditText) findViewById(R.id.edtsearch);
		
		ImageView im =(ImageView) findViewById(R.id.head_insp_info);
		im.setVisibility(View.VISIBLE);
		im.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				    Intent s2= new Intent(Deleteinspection.this,PolicyholdeInfoHead.class);
					Bundle b2 = new Bundle();
					s2.putExtra("homeid", cf.selectedhomeid);
					s2.putExtra("Type", "Inspector");
					s2.putExtra("insp_id", cf.Insp_id);
					((Activity) cf.con).startActivityForResult(s2, cf.info_requestcode);
			}
		});
		dbquery();
		
	}
	
	private void dbquery() {
		data = null;
		countarr = null;
		rws = 0;
		try
		{
			String sql = "select * from " + cf.policyholder
					+ " where ARR_PH_InspectorId = '" + cf.encode(cf.Insp_id) + "' and ARR_PH_Status!='90'";
			if (!res.equals("")) {
				sql += " and (ARR_PH_FirstName like '%" + res
						+ "%' or ARR_PH_LastName like '%" + res
						+ "%' or ARR_PH_Policyno like '%" + res + "%')";
			}
			
			Cursor cur = cf.arr_db.rawQuery(sql, null);
			rws = cur.getCount();
			
			data = new String[rws];
			countarr = new String[rws];
			int j = 0;
			if (cur.getCount() >= 1) {
				
				((TextView) findViewById(R.id.noofrecords)).setText("No of Records : "+rws);
				((LinearLayout) findViewById(R.id.dynamiclayout)).setVisibility(cf.v1.VISIBLE);
				((TextView) findViewById(R.id.norecordstxt)).setVisibility(cf.v1.GONE);
				
				cur.moveToFirst();
				do {
					//if (cur.getInt(cur.getColumnIndex("ARR_PH_IsUploaded"))==0) {
						
						data[j]=  (cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_FirstName"))).trim().equals(""))? "N/A | ":cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_FirstName")))
										+ " ";
						data[j] += (cf.decode(cur.getString(cur
										.getColumnIndex("ARR_PH_LastName"))).trim().equals("")) ? "N/A | ":cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_LastName"))) + " | ";
						data[j] += (cf.decode(cur.getString(cur
										.getColumnIndex("ARR_PH_Policyno"))).trim().equals("")) ? "N/A | ":cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_Policyno")))
										+ " | ";
						data[j] += (cf.decode(cur.getString(cur
										.getColumnIndex("ARR_PH_Address1"))).trim().equals("")) ? "N/A | ":cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_Address1"))) + " | ";
						data[j] += (cf.decode(cur.getString(cur
										.getColumnIndex("ARR_PH_City"))).trim().equals("")) ? "N/A | ":cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_City"))) + " | ";
						data[j] += (cf.decode(cur.getString(cur
										.getColumnIndex("ARR_PH_State"))) .trim().equals("")) ? "N/A | ":cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_State")))+ " | ";
						data[j] += (cf.decode(cur.getString(cur
										.getColumnIndex("ARR_PH_County"))).trim().equals("")) ? "N/A | ":cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_County"))) + " | ";
						data[j] += (cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_Zip"))) .trim().equals("")) ? "N/A | ":cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_Zip")))+ " | ";
										
						if(!cf.onlstatus.equals("Assign")) 
						{
							data[j] += (cf.decode(cur.getString(cur
											.getColumnIndex("ARR_Schedule_ScheduledDate"))) .trim().equals("")) ? "N/A | ":cf.decode(cur.getString(cur.getColumnIndex("ARR_Schedule_ScheduledDate")))+ " | ";
							data[j] += (cf.decode(cur.getString(cur
											.getColumnIndex("ARR_Schedule_InspectionStartTime"))).trim().equals("")) ? "N/A - ":cf.decode(cur.getString(cur.getColumnIndex("ARR_Schedule_InspectionStartTime"))) + " - ";
							data[j] += (cf.decode(cur.getString(cur
											.getColumnIndex("ARR_Schedule_InspectionEndTime"))) .trim().equals("")) ? "N/A | ":cf.decode(cur.getString(cur.getColumnIndex("ARR_Schedule_InspectionEndTime")))+ " | ";
						}
						
						data[j] += (cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_Status"))).trim().equals("")) ? "N/A | ":cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_Status")))+ " | ";
						
						
						cf.getinspectioname(cur.getString(cur.getColumnIndex("ARR_PH_SRID")));
						String insptext ="";
						if(!cf.selinspname.equals(""))
						{
							String inspid[]= cf.selinspname.split(",");
							for(int i=0;i<inspid.length;i++)
					    	{
								Cursor c =cf.SelectTablefunction(cf.inspnamelist, " Where ARR_Custom_ID='"+inspid[i]+"'");
					    		if(c.getCount()>0)
					    	    {
					    	   		 c.moveToFirst();
					    	   		insptext += cf.decode(c.getString(c.getColumnIndex("ARR_Insp_Name"))).trim()+","; 
					    	    }
					    	 }	
								if(insptext.trim().length()>1)
								{
									insptext = insptext.substring(0, insptext.length() - 1);System.out.println("insptext"+insptext);		
								}
																	
							this.data[j] += insptext ;
						}
						
						
								if (data[j].contains("| 90")) {
									data[j] =data[j].replace("| 90", "| Cancelled Inspection");
								}
								if (data[j].contains("| 110")) {
									data[j] = data[j].replace("| 110", "| Unable to Scheduled");
								}
								if (data[j].contains("| 40")) {
									String ss = cf.decode(cur.getString(cur
											.getColumnIndex("ARR_PH_SubStatus")));
									if (!ss.equals("0")) {
										data[j] += ss;
										if (data[j].contains("| 4041")) {
											data[j] = data[j].replace("| 4041",
													"| Completed Inspections in Online");
										}
									} else {
										data[j] = data[j].replace("| 40", "| Schedule");
									}
								}
								if (data[j].contains("| 30")) {
									data[j] = data[j].replace("| 30", "| Assign");
								}
								if(cur.getString(cur.getColumnIndex("ARR_PH_Status")).equals("1"))
								{
									data[j] = data[j].replace("| 1", "| Completed Inspections in Tablet");
								}
								
								String s=(cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_NOOFSTORIES"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_NOOFSTORIES")));
								data[j] += " | " + s+ " | ";
								
								if(cf.decode(cur.getString(cur.getColumnIndex("Commercial_Flag"))).equals("0"))
								{
									s=(cf.decode(cur.getString(cur.getColumnIndex("fld_noofbuildings"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("fld_noofbuildings")));
									this.data[j] += s;
								}
								else
								{
									cf.CreateARRTable(70);
									Cursor curbuilding= cf.SelectTablefunction(cf.Comm_Building, " where fld_srid='"+cur.getString(cur.getColumnIndex("ARR_PH_SRID"))+"'");
									if(curbuilding.getCount()==0)
									{
										s=(cf.decode(cur.getString(cur.getColumnIndex("fld_noofbuildings"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("fld_noofbuildings")));
										this.data[j] += "1 of "+ s;
									}
									else
									{
										curbuilding.moveToFirst();
										
										
										s=(cf.decode(cur.getString(cur.getColumnIndex("fld_noofbuildings"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("fld_noofbuildings")));
										this.data[j] += curbuilding.getString(curbuilding.getColumnIndex("BUILDING_NO"))+" of "+ s;
									}
								}
								
								
								countarr[j] = cur.getString(2);
								
	
								if (data[j].contains("null")) {
									data[j]=data[j].replace("null", "N/A");
								}													
						j++;
					//}
				} while (cur.moveToNext());
				search_text.setText("");
				display();
				cf.hidekeyboard();
			} else {
				
				onlinspectionlist.removeAllViews();
				((LinearLayout) findViewById(R.id.dynamiclayout)).setVisibility(cf.v1.GONE);
				((TextView) findViewById(R.id.norecordstxt)).setVisibility(cf.v1.VISIBLE);
				if(res.equals(""))
				{
					cf.gohome();
				}
				else
				{
					cf.ShowToast("Sorry, No results found.", 1);
					cf.hidekeyboard();
				}
				
			}
		}
		catch(Exception e)
		{
			System.out.println("exception in delete"+e.getMessage());
		}

	}
	
	private void display() {
		
		onlinspectionlist.removeAllViews();
		sv = new ScrollView(this);
		onlinspectionlist.addView(sv);

		final LinearLayout l1 = new LinearLayout(this);
		l1.setOrientation(LinearLayout.VERTICAL);
		sv.addView(l1);
		
		View v = new View(this);
		v.setBackgroundResource(R.color.black);
		l1.addView(v,LayoutParams.FILL_PARENT,1);
		
		if (data!=null && data.length>0) {
			
			for (int i = 0; i < data.length; i++) {
				tvstatus = new TextView[rws];
				deletebtn = new Button[rws];
				LinearLayout l2 = new LinearLayout(this);
				LinearLayout.LayoutParams mainparamschk = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
				l2.setLayoutParams(mainparamschk);
				l2.setOrientation(LinearLayout.HORIZONTAL);
				l1.addView(l2);

				LinearLayout lchkbox = new LinearLayout(this);
				LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
				paramschk.topMargin = 8;
				paramschk.leftMargin = 20;
				paramschk.bottomMargin = 10;
				l2.addView(lchkbox);
				tvstatus[i] = new TextView(this);
				tvstatus[i].setTag("textbtn" + i);
				tvstatus[i].setWidth(700);
				tvstatus[i].setText(data[i]);				
				tvstatus[i].setTextColor(Color.WHITE);
				tvstatus[i].setTextSize(TypedValue.COMPLEX_UNIT_SP,14);

			    lchkbox.addView(tvstatus[i], paramschk);

				LinearLayout ldelbtn = new LinearLayout(this);
				LinearLayout.LayoutParams paramsdelbtn = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		        paramsdelbtn.setMargins(0, 10, 10, 0); //left, top, right, bottom
				ldelbtn.setLayoutParams(mainparamschk);
				ldelbtn.setGravity(Gravity.RIGHT);
			    l2.addView(ldelbtn);
				deletebtn[i] = new Button(this);
				deletebtn[i].setBackgroundResource(R.drawable.deletebtn1);
				deletebtn[i].setTag("deletebtn" + i);
				deletebtn[i].setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
				deletebtn[i].setPadding(30, 0, 0, 0);
				ldelbtn.addView(deletebtn[i], paramsdelbtn);
				deletebtn[i].setOnClickListener(new View.OnClickListener() {
					public void onClick(final View v) {
						String getidofselbtn = v.getTag().toString();    
						final String repidofselbtn = getidofselbtn.replace("deletebtn", "");
						final int cvrtstr = Integer.parseInt(repidofselbtn);
						final String dt = countarr[cvrtstr];
						 final Dialog dialog1 = new Dialog(Deleteinspection.this,android.R.style.Theme_Translucent_NoTitleBar);
							dialog1.getWindow().setContentView(R.layout.alertsync);
							TextView txttitle = (TextView) dialog1.findViewById(R.id.txthelp);
							txttitle.setText("Confirmation");
							TextView txt = (TextView) dialog1.findViewById(R.id.txtid);
							txt.setText(Html.fromHtml("Do you want to delete this inspection?"));
							Button btn_yes = (Button) dialog1.findViewById(R.id.yes);
							Button btn_cancel = (Button) dialog1.findViewById(R.id.cancel);
							btn_yes.setOnClickListener(new OnClickListener()
							{
								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub
									dialog1.dismiss();
									del.delinsp_byrecord(dt);
									dbquery();				
								}
							});
							btn_cancel.setOnClickListener(new OnClickListener()
							{
								public void onClick(View arg0) {
									// TODO Auto-generated method stub
									dialog1.setCancelable(true);
									dialog1.dismiss();
								}			
							});
							dialog1.setCancelable(false);		
							dialog1.show();
					} 
				});
				
			
				 if (i % 2 == 0) {
					 l2.setBackgroundColor(Color.parseColor("#13456d"));
				    	} else {
				    	l2.setBackgroundColor(Color.parseColor("#386588"));
				    	}
				 if(data.length!=(i+1))
					{
						 View v1 = new View(this);
						 v1.setBackgroundResource(R.color.white);
						 l1.addView(v1,LayoutParams.FILL_PARENT,1);	 
					}
			}
			View v2 = new View(this);
			 v2.setBackgroundResource(R.color.black);
			 onlinspectionlist.addView(v2,LayoutParams.FILL_PARENT,1);
		}
		else
		{
		
		}
	}
	public void clicker(View v) {
		switch (v.getId()) {
	
		case R.id.deleteall:			
			del.delinsp_all();
			break;		
		case R.id.home:
			cf.gohome();
			break;
		case R.id.search:
				String temp = cf.encode(search_text.getText().toString().trim());
				if (temp.equals("")) {
					cf.ShowToast("Please enter the Name or Policy Number to search.", 1);
					search_text.requestFocus();
				} 
				else
				{
					res = temp;
					dbquery();
				}
			break;
		case R.id.clear:
			search_text.setText("");
			res = "";
			dbquery();
			cf.hidekeyboard();
			break;
			
		}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			startActivity(new Intent(Deleteinspection.this,HomeScreen.class));
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}