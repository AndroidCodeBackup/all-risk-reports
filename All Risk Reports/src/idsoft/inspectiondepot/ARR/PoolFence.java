/*
   ************************************************************
   * Project: ALL RISK REPORT
   * Module : PoolFence.java
   * Creation History:
      Created By : Gowri on 
   * Modification History:
      Last Modified By : Gowri on 04/04/2013
   ************************************************************  
*/
package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.GCH.checklistenetr;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Html;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class PoolFence extends Activity {
	CommonFunctions cf;
	String ppfchk_vlaue="",slrdgtxt="No",pirdgtxt="No",pfdrdgtxt="No",retppfrdtxt="";
	int ppfchkval=0;
	public RadioButton[] ppf_rd = new RadioButton[7];
	RadioGroup slrdgval,pirdgval,pfdrdgval;
	public int[] ppfid = {R.id.ppf_rd1,R.id.ppf_rd2,R.id.ppf_rd3,R.id.ppf_rd4,R.id.ppf_rd5,R.id.ppf_rd6,R.id.ppf_rd7};
	public CheckBox ppfchk[] = new CheckBox[7];
	boolean slck=false,pick=false,pfdck=false;
	
	@Override
	   public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        cf=new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);
				
		 	}
	        setContentView(R.layout.poolfence);
	        cf.getDeviceDimensions();
	        
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
			hdr_layout.addView(new HdrOnclickListener(this,1,"General Conditions & Hazards","Perimeter Pool Fence",4,0,cf));
	        LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
	        mainmenu_layout.setMinimumWidth(cf.wd);
	        mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 4, 0,0,cf));
	        LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 42, 1,0,cf));
			TableRow tblrw = (TableRow)findViewById(R.id.row2);
			tblrw.setMinimumHeight(cf.ht);
			declarations();
			cf.CreateARRTable(42);
			PoolFence_SetValue();
		 }
	private void PoolFence_SetValue() {
		// TODO Auto-generated method stub
		
		try
		{
			chk_values();
		   Cursor PPF_retrive=cf.SelectTablefunction(cf.GCH_PoolFencetbl, " where fld_srid='"+cf.selectedhomeid+"' AND (fld_ppfchk='1' OR (fld_ppfchk='0' AND fld_ppfence<>''))");
		   if(PPF_retrive.getCount()>0)
		   {  
			   PPF_retrive.moveToFirst();
			   ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
			   if(PPF_retrive.getInt(PPF_retrive.getColumnIndex("fld_ppfchk"))==1)
			   {
				   ((CheckBox)findViewById(R.id.pf_na)).setChecked(true);
				   ((LinearLayout)findViewById(R.id.pf_table)).setVisibility(cf.v1.GONE);
			   }
			   else
			   {
			       ((CheckBox)findViewById(R.id.pf_na)).setChecked(false);
				   ((LinearLayout)findViewById(R.id.pf_table)).setVisibility(cf.v1.VISIBLE);
				    ppfchk_vlaue=cf.decode(PPF_retrive.getString(PPF_retrive.getColumnIndex("fld_ppfence")));
				    cf.setvaluechk1(ppfchk_vlaue,ppfchk,((EditText)findViewById(R.id.ppf7_otr)));
				   
				    slrdgtxt=PPF_retrive.getString(PPF_retrive.getColumnIndex("fld_selflatches"));
				   ((RadioButton) slrdgval.findViewWithTag(slrdgtxt)).setChecked(true);
				    pirdgtxt=PPF_retrive.getString(PPF_retrive.getColumnIndex("fld_pinstalled"));
				    ((RadioButton) pirdgval.findViewWithTag(pirdgtxt)).setChecked(true);
				    pfdrdgtxt=PPF_retrive.getString(PPF_retrive.getColumnIndex("fld_pfdisrepair"));
				    ((RadioButton) pfdrdgval.findViewWithTag(pfdrdgtxt)).setChecked(true);
				    
			   }
			 
		   }
		   else
		   {
			   ((CheckBox)findViewById(R.id.pf_na)).setChecked(true);
				((LinearLayout)findViewById(R.id.pf_table)).setVisibility(cf.v1.GONE);
		   }
		}
	    catch (Exception E)
		{
			String strerrorlog="Retrieving Pool Fence data - GCH";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
	private void declarations() 
{
		// TODO Auto-generated method stub
		 cf.setupUI((ScrollView) findViewById(R.id.scr));
		((TextView)findViewById(R.id.rowtxt1)).setText(Html.fromHtml(cf.redcolor+" Perimeter Pool Fence"));
		((TextView)findViewById(R.id.rowtxt2)).setText(Html.fromHtml(cf.redcolor+" Self Latches/Locks to Gate"));
		((TextView)findViewById(R.id.rowtxt3)).setText(Html.fromHtml(cf.redcolor+" Professionally Installed"));
		((TextView)findViewById(R.id.rowtxt4)).setText(Html.fromHtml(cf.redcolor+" Pool Fence in Disrepair"));
		  for(int i=0;i<7;i++)
	   	 {
	   		try{
	   			ppfchk[i] = (CheckBox)findViewById(ppfid[i]);
	   		}
				catch(Exception e)
				{
					System.out.println("error= "+e.getMessage());
				}
	   	 }
				
		slrdgval = (RadioGroup)findViewById(R.id.sl_rdg);
		slrdgval.setOnCheckedChangeListener(new checklistenetr(1));
		pirdgval = (RadioGroup)findViewById(R.id.pi_rdg);
		pirdgval.setOnCheckedChangeListener(new checklistenetr(2));
		pfdrdgval = (RadioGroup)findViewById(R.id.pfd_rdg);
		pfdrdgval.setOnCheckedChangeListener(new checklistenetr(3));
		
		set_defaultchk();
	}
	class checklistenetr implements OnCheckedChangeListener
	{
    	int i;
		public checklistenetr(int i) {
			// TODO Auto-generated constructor stub
		   this.i=i;
		}

		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			   RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
		        boolean isChecked = checkedRadioButton.isChecked();
		        if (isChecked)
		        {
		          switch (i) {
					case 1:
						slrdgtxt= checkedRadioButton.getText().toString().trim();slck=false;
						break;
					case 2:
						pirdgtxt= checkedRadioButton.getText().toString().trim();pick=false;
						break;
					case 3:
						pfdrdgtxt= checkedRadioButton.getText().toString().trim();pfdck=false;
					  break;
		          }
		       }
		}
	}
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.pf_na:/** POOL FENCE NOT APPLICABLE CHECKBOX **/
			if(((CheckBox)findViewById(R.id.pf_na)).isChecked())
			{
				
    			if(!retppfrdtxt.equals("")){
 				    showunchkalert(1);
 			    }
    			else
    			{
    				clearfence();
    				set_defaultchk();
    				((LinearLayout)findViewById(R.id.pf_table)).setVisibility(v.GONE);
 			    	((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.INVISIBLE);
 			    }
				
			}
			else
			{
				((LinearLayout)findViewById(R.id.pf_table)).setVisibility(v.VISIBLE);
				((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.INVISIBLE);
				((CheckBox)findViewById(R.id.pf_na)).setChecked(false);
			}
			
			break;
	
		 case R.id.ppf_rd7:/** PERIMETER POOL FENCE CHECKBOX OTHER **/
			  if(ppfchk[ppfchk.length-1].isChecked())
			  { 
				  ((EditText)findViewById(R.id.ppf7_otr)).setVisibility(v.VISIBLE);
			  }
			  else
			  {
				  ((EditText)findViewById(R.id.ppf7_otr)).setVisibility(v.GONE);
				  ((EditText)findViewById(R.id.ppf7_otr)).setText("");
			  }
			  break;
		
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.save:
			if(((CheckBox)findViewById(R.id.pf_na)).isChecked())
			{
				ppfchkval=1;poolfence_insert();
				((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
				cf.ShowToast("Perimeter Pool Fence saved successfully.", 0);//}
			    cf.goclass(44);
			}
			else
			{
				ppfchkval=0;
				ppfchk_vlaue= cf.getselected_chk(ppfchk);
		    	String ppfchk_vlaueotr=(ppfchk[ppfchk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.ppf7_otr)).getText().toString():""; // append the other text value in to the selected option
		    	ppfchk_vlaue+=ppfchk_vlaueotr;
		    	if(ppfchk_vlaue.equals(""))
				{
					 cf.ShowToast("Please select the option for Perimeter Pool Fence." , 0);
				}
		    	else
		    	{
		    		 if(ppfchk[ppfchk.length-1].isChecked())
					 {
						 if(ppfchk_vlaueotr.trim().equals("&#40;"))
						 {
							 cf.ShowToast("Please enter the Other text for Perimeter Pool Fence.", 0);
							 cf.setFocus(((EditText)findViewById(R.id.ppf7_otr)));
						 }
						 else
						 {
							poolfence_insert();
							((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
							cf.ShowToast("Perimeter Pool Fence saved successfully.", 0);//}
						    cf.goclass(44);
						 }
					 }
					 else
					 {
						 poolfence_insert();
						 ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
						 cf.ShowToast("Perimeter Pool Fence saved successfully.", 0);//}
						 cf.goclass(44);
					 }
		    	 }
			}
			
			
			break;
		
		}
	}
	private void poolfence_insert() {
		// TODO Auto-generated method stub
		Cursor PPF_save=null;
		try
		{
			PPF_save=cf.SelectTablefunction(cf.GCH_PoolFencetbl, " where fld_srid='"+cf.selectedhomeid+"'");
			if(PPF_save.getCount()>0)
			{
				try
				{
					cf.arr_db.execSQL("UPDATE "+cf.GCH_PoolFencetbl+ " set fld_ppfchk='"+ppfchkval+"',fld_ppfence='"+cf.encode(ppfchk_vlaue)+"',"+
				                               "fld_ppfenceother='"+cf.encode(((EditText)findViewById(R.id.ppf7_otr)).getText().toString())+"',"+
							                   "fld_selflatches='"+slrdgtxt+"',fld_pinstalled='"+pirdgtxt+"',"+
				                               "fld_pfdisrepair='"+pfdrdgtxt+"' where fld_srid='"+cf.selectedhomeid+"'");
					/*if(ppfchkval==0){ ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.GONE);}
					else{ *//*((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
					cf.ShowToast("Perimeter Pool Fence saved successfully.", 0);//}
				     cf.goclass(43);*/
				}
				catch (Exception E)
				{
					String strerrorlog="Updating the Pool Fence data - GCH";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
				
			}
			else
			{
				try
				{
					cf.arr_db.execSQL("INSERT INTO "
							+ cf.GCH_PoolFencetbl
							+ " (fld_srid,fld_ppfchk,fld_ppfence,fld_ppfenceother,fld_selflatches,fld_pinstalled,fld_pfdisrepair)"
							+ "VALUES ('"+cf.selectedhomeid+"','"+ppfchkval+"','"+cf.encode(ppfchk_vlaue)+"','"+cf.encode(((EditText)findViewById(R.id.ppf7_otr)).getText().toString())+"','"+slrdgtxt+"','"+pirdgtxt+"','"+pfdrdgtxt+"')");

					/*if(ppfchkval==0){ ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.GONE);}
					else{*/ /*((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
					cf.ShowToast("Perimeter Pool Fence saved successfully.", 0);//}
					 cf.goclass(43);*/
			 	}
				catch (Exception E)
				{
					String strerrorlog="Inserting the Pool Fence data - GCH ";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
				
			}
		}
		catch (Exception E)
		{
			String strerrorlog="Checking the rows inserted in the GCH Pool Fence table.";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of checking rows of GCH Pool Fence table at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			cf.goclass(41);
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}
	private void chk_values() {
		// TODO Auto-generated method stub
		try
		{
		   Cursor Pf_retrive=cf.SelectTablefunction(cf.GCH_PoolFencetbl, " where fld_srid='"+cf.selectedhomeid+"'");
		   int rws = Pf_retrive.getCount();
			if(rws>0){
				Pf_retrive.moveToFirst();
				retppfrdtxt = Pf_retrive.getString(Pf_retrive.getColumnIndex("fld_ppfence"));
			}
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}
	public void showunchkalert(final int i) {
		// TODO Auto-generated method stub
		AlertDialog.Builder bl = new Builder(PoolFence.this);
		bl.setTitle("Confirmation");
		bl.setMessage(Html.fromHtml("Do you want to clear the Perimeter Pool Fence data?"));
		bl.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog,
											int id) {
										 switch(i){
										 case 1:
											 ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.GONE);
							    			 clearfence();//poolfence_insert();
							    			// cf.arr_db.execSQL("Delete from "+cf.GCH_PoolFencetbl+" Where fld_srid='"+cf.selectedhomeid+"'");
											((LinearLayout)findViewById(R.id.pf_table)).setVisibility(cf.v1.GONE);
							    			 break;
										 
										
										 }
										 	 
										 
									}
		});
		bl.setNegativeButton("No",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog,
							int id) {
						switch(i){
						 case 1:
							((CheckBox)findViewById(R.id.pf_na)).setChecked(false);
							 break;
					
						 }
					}
        });
		AlertDialog al=bl.create();
		al.setIcon(R.drawable.alertmsg);
		al.setCancelable(false);
		al.show();
		
		
	}
	protected void clearfence() {
		// TODO Auto-generated method stub
		 ppfchkval=0;ppfchk_vlaue="";slck=true;slrdgtxt="";
		 pick=true;pirdgtxt="";pfdck=true;pfdrdgtxt="";retppfrdtxt="";
		 try{if(slck){slrdgval.clearCheck();}}catch(Exception e){}
		 try{if(pick){pirdgval.clearCheck();}}catch(Exception e){}
		 try{if(pfdck){pfdrdgval.clearCheck();}}catch(Exception e){}
		 cf.Set_UncheckBox(ppfchk, ((EditText)findViewById(R.id.ppf7_otr)));
		 set_defaultchk();
	}
	protected void set_defaultchk() {
		// TODO Auto-generated method stub
		((RadioButton) slrdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) pirdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) pfdrdgval.findViewWithTag("No")).setChecked(true);
		slrdgtxt="No";pirdgtxt="No";pfdrdgtxt="No";
	}
}
