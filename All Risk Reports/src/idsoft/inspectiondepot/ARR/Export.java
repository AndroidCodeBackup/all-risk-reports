/*
 ************************************************************
update * Project: ALL RISK REPORT
 * Module : Submit.java
 * Creation History:
      Created By : Gowri on 2/5/2013
 * Modification History:
      Last Modified By : Rakki on 3/29/2013
 ************************************************************  
 */
package idsoft.inspectiondepot.ARR;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.nio.channels.FileChannel;
import java.util.concurrent.TimeoutException;

import org.kobjects.base64.Base64;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;


import android.R.bool;
import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class Export extends Activity implements Runnable {
	CommonFunctions cf;
	TextView title;
	EditText search_text;
	Button completed_insp, export;
	LinearLayout onlinspectionlist;
	public String[] data, countarr, s1;
	public Cursor cur;
	public int crrws, show_handler, inspectionid = 0;
	public String dt = "", wind_NA = "", door_NA = "", sky_NA = "",
			aux_NA = "", inspdata = "", errorlog, strwhtrval="", sql = "",
			photos_missingfiles = "", feed_missingfiles = "", chksppval="";
	public TextView tvstatus[];
	public ScrollView sv;
	public Button deletebtn[];
	public Bitmap bitmap, bitmap1;

	/*** For progress dialogue **/
	ProgressDialog pd;
	double Total = 0.0;
	double sub_total = 0;
	double PerIns_inc = 0;
	double genperModule_in = 0;
	double fourperModule_in = 0;
	double surveyperModule_in = 0;
	double b11802perModule_in = 0;
	double com1perModule_in = 0;
	double com2perModule_in = 0;
	double com3perModule_in = 0;
	double gchperModule_in = 0;
	double sinkperModule_in = 0;
	double dryperModule_in = 0;
	double customperModule_in = 0;
	int mState;
	private String Exportbartext = "", sub_insp = "", res = "";
	PowerManager.WakeLock wl = null;
	public ProgressThread progThread;
	Dialog dialog = null;
	TextView tv_head, tv_child, tv_child_per, tv_head_per, tvheader;
	LinearLayout li_head, li_child, back_li_head, back_li_child;
	int li_inc_unit_chi = 0, li_inc_unit_head = 0;
	LayoutParams lp;
	LayoutParams ch_lp;
	int delay = 1000; // Milliseconds of delay in the update loop
	int maxBarValue = 0, show_handler1; // Maximum value of horizontal progress
										// bar
	int typeBar = 1;
	int RUNNING = 1;
	/*** For progress dialogue ends **/
	/*** For Error message handling ***/
	Cursor current_val_cur = null;
	String curr_Request = "";
	int current_pos = 0;
	String Web_mehtod = "";
	String result_elev = "";
	String status = "Uploading inspection completed ";
	/*** For Error message handling ends ***/
	public boolean[] EX_CHKGCH = { false, false, false, false, false, false,
			false, false, false, false, false, false };
	public boolean[] EX_CHKSH = { false, false, false, false, false, false };
	public boolean[] EX_CHKCD = { false, false, false, false, false, false };
	public boolean[] EX_CHKFP = { false, false, false, false, false, false };
	public boolean[] EX_B1802 = { false, false, false, false, false, false,false };
	public boolean[] EX_Comm1 = { false, false, false, false, false, false,false };
	public boolean[] EX_Comm2 = { false, false, false, false, false, false,false };
	public boolean[] EX_CHKFP_EL = { false, false, false, false, false, false,
			false, false,false };
	public boolean[] EX_survey = { false, false, false };
	public boolean[] EX_BI = { false, false };
	public boolean[] EX_Custom = { false, false };
	public boolean photos = false, feedback = false, exportweather = false,exportbuilding = false,
			bno = false,addendum=false, fouraddendum=false,b1802addendum=false;
	public String Error_tracker = "",exportcustom="false";

	/** check the selected inspection and its type **/
	String insp_boo[] = { "", "", "", "", "", "", "", "", "", "" };
	String four_boo[] = { "", "", "", "", "", "" };
	String general_boo[] = { "", "", "" };
	String survey_boo[] = { "", "", "" };
	String B11802_boo[] = { "", "", "", "", "", "" };
	String COM1_boo[] = { "", "", "", "", "", "", "", "" };
	String COM2_boo[] = { "", "", "", "", "", "", "" };
	String COM3_boo[] = { "", "", "", "", "", "", "" };
	String GCH_boo[] = { "", "", "", "", "", "", "", "", "", "", "", "", "",
			"", "" };
	String SINK_boo[] = { "", "", "", "", "", "", "" };
	String DRYWALL_boo[] = { "", "", "", "", "", "", "", "" };
	String CUSTOM_boo[] = { "",""};
	/*** check the selected inspection and its type ends ***/
	String custom_sta[] = { "", ""};
	String four_sta[] = { "", "", "", "", "", ""};
	String general_sta[] = { "", "", "" };
	
	String survey_sta[] = { "", "", "" };
	String B11802_sta[] = { "", "", "", "", "", "","" };
	String COM1_sta[] = { "", "", "", "", "", "", "", "" };
	String COM2_sta[] = { "", "", "", "", "", "", "" };
	String COM3_sta[] = { "", "", "", "", "", "", "" };
	String GCH_sta[] = { "", "", "", "", "", "", "", "", "", "", "", "", "",
			"", "" };
	String SINK_sta[] = { "", "", "", "", "", "", "" };
	String DRYWALL_sta[] = { "", "", "", "", "", "", "", "" };
	String CUSTOM_sta[] = { "",""};
	/**
	 * This is used to know the result of the each inspection type exporting
	 * data
	 **/
	public boolean[] EX_Result = { false, false, false, false, false, false,
			false, false, false, false, false,false };
	public String[] PP_infos, PF_infos, SH_infos, FP_infos, SPH_infos,
			HV_infos, ELE_infos, B1802_infos, RC_infos, Comm_infos, CA_infos;
	private boolean exportph;
	boolean reexported = false;
	MarshalBase64 marshal,marshal1;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		cf = new CommonFunctions(this);
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			cf.onlstatus = extras.getString("type");

		}
		cf.getInspectorId();
		setContentView(R.layout.completedinspectionlist);

		tvheader = (TextView) findViewById(R.id.tvheader);
		completed_insp = (Button) findViewById(R.id.completed_insp);
		export = (Button) findViewById(R.id.export_insp);
		completed_insp.setVisibility(cf.v1.VISIBLE);
		export.setVisibility(cf.v1.VISIBLE);

		search_text = (EditText) findViewById(R.id.search_text);
		onlinspectionlist = (LinearLayout) findViewById(R.id.linlayoutdyn);
		cf.getInspectorId();

		if (cf.onlstatus.equals("export")) {
			tvheader.setText("Export inspection");
			// export.setBackgroundResource(R.drawable.button_back_blue);
		} else if (cf.onlstatus.equals("Reexport")) {
			tvheader.setText("Reexport inspection");
			reexported = true;
			// completed_insp.setBackgroundResource(R.drawable.button_back_blue);
		}

		/*
		 * cf.arr_db.execSQL("UPDATE " + cf.policyholder +
		 * " SET ARR_PH_IsInspected='',ARR_PH_Status='40' WHERE ARR_PH_SRID ='RI12348'  and ARR_PH_InspectorId = '"
		 * +cf.Insp_id + "'");
		 */
		cf.CreateARRTable(21);
		if (cf.doesTblExist(cf.policyholder)) {
			dbquery();
		} else {

		}
		ImageView im =(ImageView) findViewById(R.id.head_insp_info);
		im.setVisibility(View.VISIBLE);
		im.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				    Intent s2= new Intent(Export.this,PolicyholdeInfoHead.class);
					Bundle b2 = new Bundle();
					s2.putExtra("homeid", cf.selectedhomeid);
					s2.putExtra("Type", "Inspector");
					s2.putExtra("insp_id", cf.Insp_id);
					((Activity) cf.con).startActivityForResult(s2, cf.info_requestcode);
			}
		});
	}

	private void dbquery() {
		int k = 1;
		sql = "select * from " + cf.policyholder;
		if (!res.equals("")) {

			sql += " where (ARR_PH_FirstName like '%" + cf.encode(res)
					+ "%' or ARR_PH_LastName like '%" + cf.encode(res)
					+ "%' or ARR_PH_Policyno like '%" + cf.encode(res)
					+ "%') and ARR_PH_InspectorId='" + cf.encode(cf.Insp_id)
					+ "'";
			if (cf.onlstatus.equals("export")) {
				sql += " and ARR_PH_IsInspected='1' ";
			} else if (cf.onlstatus.equals("Reexport")) {
				sql += " and ARR_PH_IsInspected='2' and ARR_PH_IsUploaded='1' ";
			}

		} else {
			if (cf.onlstatus.equals("export")) {
				sql += " where ARR_PH_IsInspected='1'  and ARR_PH_InspectorId='"
						+ cf.encode(cf.Insp_id) + "' ";
				// sql += " where ARR_PH_InspectorId='" + cf.encode(cf.Insp_id)
				// + "' ";
			} else if (cf.onlstatus.equals("Reexport")) {
				sql += " where ARR_PH_IsInspected='2' and ARR_PH_IsUploaded='1'  and ARR_PH_InspectorId='"
						+ cf.encode(cf.Insp_id) + "' ";
			}

		}

		cur = cf.arr_db.rawQuery(sql, null);
		crrws = cur.getCount();
		data = new String[crrws];
		countarr = new String[crrws];
		int i = 0, j = 0;
		cur.moveToFirst();
		this.data = new String[crrws];
		if (cur.getCount() >= 1) {
			((TextView) findViewById(R.id.noofrecords))
					.setText("No of Records : " + crrws);
			((LinearLayout) findViewById(R.id.dynamiclayout))
					.setVisibility(cf.v1.VISIBLE);
			((TextView) findViewById(R.id.norecordstxt))
					.setVisibility(cf.v1.GONE);

			do {
				this.data[i] += " "
						+ cf.decode(cur.getString(cur
								.getColumnIndex("ARR_PH_FirstName"))) + " ";
				this.data[i] += cf.decode(cur.getString(cur
						.getColumnIndex("ARR_PH_LastName"))) + " | ";
				this.data[i] += cf.decode(cur.getString(cur
						.getColumnIndex("ARR_PH_Policyno"))) + " \n ";
				this.data[i] += cf.decode(cur.getString(cur
						.getColumnIndex("ARR_PH_Address1"))) + " | ";
				this.data[i] += cf.decode(cur.getString(cur
						.getColumnIndex("ARR_PH_City"))) + " | ";
				this.data[i] += cf.decode(cur.getString(cur
						.getColumnIndex("ARR_PH_State"))) + " | ";
				this.data[i] += cf.decode(cur.getString(cur
						.getColumnIndex("ARR_PH_County"))) + " | ";
				this.data[i] += cf.decode(cur.getString(cur
						.getColumnIndex("ARR_PH_Zip"))) + " | " + " \n ";
				this.data[i] += cf.decode(cur.getString(cur
						.getColumnIndex("ARR_Schedule_ScheduledDate"))) + " | ";
				this.data[i] += cf.decode(cur.getString(cur
						.getColumnIndex("ARR_Schedule_InspectionStartTime")))
						+ " - ";
				this.data[i] += cf.decode(cur.getString(cur
						.getColumnIndex("ARR_Schedule_InspectionEndTime")))
						+ " | ";

				cf.getinspectioname(cur.getString(cur
						.getColumnIndex("ARR_PH_SRID")));
				String insptext = "";
				if (!cf.selinspname.equals("")) {
					String inspid[] = cf.selinspname.split(",");
					for (int l = 0; l < inspid.length; l++) {
						Cursor c = cf.SelectTablefunction(cf.allinspnamelist,
								" Where ARR_Insp_ID='" + inspid[l] + "'");
						if (c.getCount() > 0) {
							c.moveToFirst();
							insptext += cf.decode(
									c.getString(c
											.getColumnIndex("ARR_Insp_Name")))
									.trim()
									+ ",";
						}
					}
					if (insptext.trim().length() > 1) {
						insptext = insptext.substring(0, insptext.length() - 1);
					}

					this.data[j] += insptext;
				}

				String s=(cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_NOOFSTORIES"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("ARR_PH_NOOFSTORIES")));
				data[j] += " | " + s+ " | ";
				
				if(cf.decode(cur.getString(cur.getColumnIndex("Commercial_Flag"))).equals("0"))
				{
					s=(cf.decode(cur.getString(cur.getColumnIndex("fld_noofbuildings"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("fld_noofbuildings")));
					this.data[j] += s;
				}
				else
				{
					cf.CreateARRTable(70);
					Cursor curbuilding= cf.SelectTablefunction(cf.Comm_Building, " where fld_srid='"+cur.getString(cur.getColumnIndex("ARR_PH_SRID"))+"'");
					if(curbuilding.getCount()==0)
					{
						s=(cf.decode(cur.getString(cur.getColumnIndex("fld_noofbuildings"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("fld_noofbuildings")));
						this.data[j] += "1 of "+ s;
					}
					else
					{
						curbuilding.moveToFirst();
						
						
						s=(cf.decode(cur.getString(cur.getColumnIndex("fld_noofbuildings"))).trim().equals("")) ? "N/A":cf.decode(cur.getString(cur.getColumnIndex("fld_noofbuildings")));
						this.data[j] += curbuilding.getString(curbuilding.getColumnIndex("BUILDING_NO"))+" of "+ s;
					}
				}
				
				countarr[j] = cf.decode(cur.getString(cur
						.getColumnIndex("ARR_PH_SRID")));
				j++;

				if (this.data[i].contains("null")) {
					this.data[i] = this.data[i].replace("null", "");
				}
				if (this.data[i].contains("N/A |")) {
					this.data[i] = this.data[i].replace("N/A |", "");
				}
				if (this.data[i].contains("N/A - N/A")) {
					this.data[i] = this.data[i].replace("N/A - N/A", "");
				}
				i++;
			} while (cur.moveToNext());
			display();
		} else {
			onlinspectionlist.removeAllViews();
			((LinearLayout) findViewById(R.id.dynamiclayout))
					.setVisibility(cf.v1.GONE);
			((TextView) findViewById(R.id.norecordstxt))
					.setVisibility(cf.v1.VISIBLE);

		}

	}

	private void display() {
		onlinspectionlist.removeAllViews();
		sv = new ScrollView(this);
		onlinspectionlist.addView(sv);

		final LinearLayout l1 = new LinearLayout(this);
		l1.setOrientation(LinearLayout.VERTICAL);
		sv.addView(l1);

		View v = new View(this);
		v.setBackgroundResource(R.color.black);
		l1.addView(v, LayoutParams.FILL_PARENT, 1);
		tvstatus = new TextView[crrws];
		deletebtn = new Button[crrws];

		if (data.length >= 1) {
			for (int i = 0; i < data.length; i++) {
				
				LinearLayout l2 = new LinearLayout(this);
				LinearLayout.LayoutParams mainparamschk = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
				
				l2.setLayoutParams(mainparamschk);
				l2.setOrientation(LinearLayout.HORIZONTAL);
				l1.addView(l2);			

				LinearLayout lchkbox = new LinearLayout(this);
				LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.FILL_PARENT,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				paramschk.topMargin = 8;
				paramschk.leftMargin = 20;
				paramschk.rightMargin = 20;
				paramschk.bottomMargin = 10;
				l2.addView(lchkbox);

				tvstatus[i] = new TextView(this);
				//tvstatus[i].setWidth();
				tvstatus[i].setTag("textbtn" + i);
				tvstatus[i].setText(data[i]);
				tvstatus[i].setTextColor(Color.WHITE);
				tvstatus[i].setTextSize(TypedValue.COMPLEX_UNIT_SP,14);
				lchkbox.addView(tvstatus[i], paramschk);

				LinearLayout ldelbtn = new LinearLayout(this);
				LinearLayout.LayoutParams paramsdelbtn = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
				paramsdelbtn.rightMargin = 10;
				paramsdelbtn.bottomMargin = 10;
				ldelbtn.setLayoutParams(mainparamschk);
				l2.addView(ldelbtn);

				/**
				 * DELETE BUTTON NO NEED FOR THE EXPORT AND ONCLICK EVENT SETTIN
				 * INSPECTION SO I JUST HIDE IT NOW
				 **/
				tvstatus[i].setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						/* try{
						 File sd = Environment.getExternalStorageDirectory();
						 String data ="/data/"+getPackageName()+"/databases/"+cf.MY_DATABASE_NAME; 
						 File f=new File(sd,"databases_with_all_inspected data.sql");
						 if(!f.exists()) { f.createNewFile(); } 
						 File data1 = Environment.getDataDirectory(); 
						 File currentDB = new File(data1, data);
						 
						  if(currentDB.exists()) { if (f.canWrite()) {
						  
						  String currentDBPath = cf.MY_DATABASE_NAME; 
						  String backupDBPath = "databases_with_all_inspected data.sql"; 
						  currentDB = new File(data1, data); 
						  File backupDB = new File(sd, backupDBPath);
						 
						  FileChannel src = new FileInputStream(currentDB).getChannel(); 
						  FileChannel dst = new FileOutputStream(f).getChannel();
						  dst.transferFrom(src, 0, src.size()); src.close();
						  dst.close(); } else {  } } else {
						  } } catch (Exception e) { 
						  }*/
						String getidofselbtn = v.getTag().toString();
						final String repidofselbtn = getidofselbtn.replace(
								"textbtn", "");
						final int cvrtstr = Integer.parseInt(repidofselbtn);
						dt = countarr[cvrtstr];
						
						Intent in = new Intent(Export.this, ExportAlert.class);
						in.putExtra("Srid", dt);
						startActivityForResult(in, 825);
						// fn_export();
					}

				});

				if (i % 2 == 0) {
					l2.setBackgroundColor(Color.parseColor("#13456d"));
				} else {
					l2.setBackgroundColor(Color.parseColor("#386588"));
				}
				if (data.length != (i + 1)) {
					View v1 = new View(this);
					v1.setBackgroundResource(R.color.white);
					l1.addView(v1, LayoutParams.FILL_PARENT, 1);
				}
			}
			View v2 = new View(this);
			v2.setBackgroundResource(R.color.black);
			onlinspectionlist.addView(v2, LayoutParams.FILL_PARENT, 1);
		}

	}

	protected void fn_export() {
		// TODO Auto-generated method stub
		ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = conMgr.getActiveNetworkInfo();
		if (info != null && info.isConnected()) {
			dialog = new Dialog(this);
			dialog.setContentView(R.layout.progressbar);
			dialog.setTitle(Html
					.fromHtml("<font color=white>Please Wait</font>"));

			tv_head = (TextView) dialog.findViewById(R.id.header_txt);
			tv_child = (TextView) dialog.findViewById(R.id.child_txt);

			tv_head_per = (TextView) dialog.findViewById(R.id.head_percent);
			tv_head_per.setText("0%");
			tv_child_per = (TextView) dialog.findViewById(R.id.child_percent);
			tv_child_per.setText("0%");
			li_head = (LinearLayout) dialog.findViewById(R.id.head_progress);

			li_child = (LinearLayout) dialog.findViewById(R.id.child_progress);
			back_li_head = (LinearLayout) dialog
					.findViewById(R.id.head_back_prog);
			back_li_child = (LinearLayout) dialog
					.findViewById(R.id.child_back_prog);
			tv_head.setText(Html
					.fromHtml("Please be patient as we export your file. When complete you must log into www.paperlessinspectors.com and submit this file for auditing.  \n"
							+ Exportbartext));
			dialog.show();
			lp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, 10);
			ch_lp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
					10);
			Total = 0;
			lp.width = 0;
			li_head.setLayoutParams(lp);
			ch_lp.width = 0;
			li_child.setLayoutParams(ch_lp);
			progThread = new ProgressThread(handler1);
			progThread.start();

			/*
			 * String source =
			 * "<b><font color=#00FF33>Exporting. Please wait..." +
			 * "</font></b>";
			 */
			PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
			wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
			wl.acquire();

			/*
			 * pd = ProgressDialog.show(Export.this, "", Html.fromHtml(source),
			 * true);
			 */
			Thread thread = new Thread(Export.this);
			thread.start();

		} else {
			cf.ShowToast("Internet Connection not available.", 0);
		}

	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {

		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			startActivity(new Intent(Export.this, HomeScreen.class));
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		 Looper.prepare();
		cf.selectedhomeid = dt;
		boolean GCHisaccess = cf.chk_InspTypeQuery(cf.selectedhomeid, "11");
		boolean SHisaccess = cf.chk_InspTypeQuery(cf.selectedhomeid, "18");
		boolean CDisaccess = cf.chk_InspTypeQuery(cf.selectedhomeid, "751");
		boolean ROOFSurveyisaccess = cf.chk_InspTypeQuery(cf.selectedhomeid,"750");
		boolean Fourptisaccess = cf.chk_InspTypeQuery(cf.selectedhomeid, "13");
		boolean B1802isaccess = cf.chk_InspTypeQuery(cf.selectedhomeid, "28");
		boolean Comm1isaccess = cf.chk_InspTypeQuery(cf.selectedhomeid, "9");
		boolean Comm2isaccess = cf.chk_InspTypeQuery(cf.selectedhomeid, "9");
		boolean Comm3isaccess = cf.chk_InspTypeQuery(cf.selectedhomeid, "9");

		try {

			if (insp_boo[0].equals("true")) {
				Exportbartext = "Exporting General Information";
				Export_general();

			} else {
				EX_Result[9] = true;
			}

			if (Fourptisaccess && insp_boo[1].equals("true")) {
				Exportbartext = "Exporting " + cf.All_insp_list[1];
				Export_FourPointData();

			} else {
				EX_Result[4] = true;
			}
			if (ROOFSurveyisaccess && insp_boo[2].equals("true")) {
				Exportbartext = "Exporting " + cf.All_insp_list[2];
				Export_RoofSurveyData();

			} else {
				EX_Result[3] = true;
			}
			if (B1802isaccess && insp_boo[3].equals("true")) {
				Exportbartext = "Exporting " + cf.All_insp_list[3];
				Export_B1802Data();
			} else {
				EX_Result[5] = true;
			}
			System.out.println("comm1acc"+Comm1isaccess);
			System.out.println("comm2acc"+Comm2isaccess);
			System.out.println("comm3acc"+Comm3isaccess);
			if (Comm1isaccess && insp_boo[4].equals("true")) {
				//Exportbartext = "Exporting " + cf.All_insp_list[4];
				Exportbartext = "Exporting Commercial Type I Inspection ";
				Export_Comm1Data();

			} else {
				EX_Result[6] = true;
			}

			if (Comm2isaccess && insp_boo[5].equals("true")) {
				//Exportbartext = "Exporting " + cf.All_insp_list[4];
				Exportbartext = "Exporting Commercial Type II Inspection ";
				Export_Comm2Data();

			} else {
				EX_Result[7] = true;
			}

			if (Comm3isaccess && insp_boo[6].equals("true")) {
				//Exportbartext = "Exporting " + cf.All_insp_list[4];
				Exportbartext = "Exporting Commercial Type III Inspection ";
				Export_Comm3Data();

			} else {
				EX_Result[8] = true;
			}

			if (GCHisaccess && insp_boo[7].equals("true")) {
				Exportbartext = "Exporting " + cf.All_insp_list[7];
				Export_GCHData();
			} else {
				EX_Result[0] = true;
			}
			if (SHisaccess && insp_boo[8].equals("true")) {
				Exportbartext = "Exporting " + cf.All_insp_list[8];
				Export_SHData();
			} else {
				EX_Result[1] = true;
			}

			if (CDisaccess && insp_boo[9].equals("true")) {
				Exportbartext = "Exporting " + cf.All_insp_list[9];
				Export_ChineseDrywallData();

			} else {
				EX_Result[2] = true;
			}
			
			if (insp_boo[10].equals("true")) {
				System.out.println("custom inse");
				Exportbartext = "Exporting Custom Inspection";
				Export_CustomData();

			} else {
				EX_Result[11] = true;
			}
			
			show_handler=movetopreinspected();// change the status to pre inspected
			 
		} catch (SocketException e) {
			// TODO Auto-generated catch block

			call_erro_service("Socket Exception :" + e.getMessage().toString());

			show_handler = 2;
			e.printStackTrace();

		} catch (NetworkErrorException e) {
			// TODO Auto-generated catch block
			call_erro_service("NetworkErrorException :"
					+ e.getMessage().toString());
			show_handler = 2;
			e.printStackTrace();
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			call_erro_service("TimeoutException :" + e.getMessage().toString());
			show_handler = 2;
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			call_erro_service("IOException :" + e.getMessage().toString());
			show_handler = 2;
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			call_erro_service("XmlPullParserException :"
					+ e.getMessage().toString());
			show_handler = 2;
			e.printStackTrace();
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			call_erro_service("NullPointerException :"
					+ " This may happen due to methode un availalbe or the wrong port");
			show_handler = 2;
			e.printStackTrace();
		}

		handler.sendEmptyMessage(0);
	}

	private int movetopreinspected() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, NullPointerException {
		// TODO Auto-generated method stub
		
		System.out.println("EX_Result[0]"+EX_Result[0]+"EX_Result[1]"+EX_Result[1]+"EX_Result[2]"+EX_Result[2]+"EX_Result[3]"+EX_Result[3]+"EX_Result[4]"+EX_Result[4]+"EX_Result[5]"+EX_Result[5]+"EX_Result[6]"+EX_Result[6]+"EX_Result[7]"+EX_Result[7]+"EX_Result[8]"+EX_Result[8]+"EX_Result[9]"+EX_Result[9]);
		if (EX_Result[0] == true && EX_Result[1] == true
				&& EX_Result[2] == true && EX_Result[4] == true
				&& EX_Result[5] == true && EX_Result[6] == true
				&& EX_Result[7] == true && EX_Result[8] == true
				&& EX_Result[9] == true && EX_Result[3] == true && EX_Result[11] == true) {
			cf.request = cf.SoapRequest("UpdateInspectionStatus");
			cf.request.addProperty("InspectorID", cf.Insp_id);
			cf.request.addProperty("SRID", cf.selectedhomeid);
			boolean result = cf.SoapResponse("UpdateInspectionStatus",
					cf.request);
			
			System.out.println("resultUpdateInspectionStatus"+result);
			if (result) {
				System.out.println("inside statu="+result);
				try
				{
					 SoapObject onlresult=cf.Calling_WS1(cf.Insp_id, "UPDATEMOBILEDBCOUNT");System.out.println("onres="+onlresult);
					 retrieveonlinedata(onlresult);System.out.println("inside retrie");
				}
				catch (Exception e) {
					// TODO: handle exception
					System.out.println("onrescatch"+e.getMessage());
				}
				
				System.out.println("inside status change");
				status = "This inspection was uploaded completely to your online profile under \"pre-inspected\" status. \nPlease log into  www.paperlessinspectors.com and audit and submit this file for QA.";
				
				cf.arr_db
						.execSQL("UPDATE "
								+ cf.policyholder
								+ " SET ARR_PH_IsInspected=2,ARR_PH_IsUploaded=1,ARR_PH_SubStatus=41 WHERE ARR_PH_SRID ='"
							+ cf.selectedhomeid + "'");
				return 1;
			}
			else 
			{
				status = "Exporting inspection completed. Inspection in scheduled  status";
				return 1;

			}
		} else {
			System.out.println("inside pre status false");
			return 2;
			
		}
	}
	 private void retrieveonlinedata(SoapObject result) {
			
			int Cnt = result.getPropertyCount();
			int onpre=0,onuts=0,oncan=0,onrr=0,ontyppre=0,ontyputs=0,ontypcan=0,ontyprr=0;
			if (Cnt >= 1) {
				SoapObject obj = (SoapObject) result;

				for (int i = 0; i < obj.getPropertyCount(); i++) {
					SoapObject obj1 = (SoapObject) obj.getProperty(i);
					if (!obj1.getProperty("i_maininspectiontype").toString()
							.equals("")) {
						
							ontyppre += Integer.parseInt(obj1.getProperty(
									"Comp_Ins_in_Online").toString());
							ontyputs += Integer.parseInt(obj1.getProperty("UTS")
									.toString());
							ontypcan += Integer.parseInt(obj1.getProperty("Can")
									.toString());
							ontyprr +=  Integer.parseInt(obj1.getProperty("Completed")
															.toString());
							
						
					}
				}
				try
				{
					//insp_id varchar(50),C_28_pre varchar(5) Default('0'),C_28_uts varchar(5) Default('0'),C_28_can varchar(5) Default('0'),C_28_RR varchar(5) Default('0')
					Cursor c=cf.SelectTablefunction(cf.count_tbl, " WHERE insp_id='"+cf.Insp_id+"'");
					if(c.getCount()>0)
					{
						cf.arr_db.execSQL(" UPDATE "+cf.count_tbl+" SET C_28_pre='"+ontyppre+"',C_28_uts='"+ontyputs+"',C_28_can='"+ontypcan+"',C_28_RR='"+ontyprr+"',C_29_pre='"+onpre+"',C_29_uts='"+onuts+"',C_29_can='"+oncan+"',C_29_RR='"+onrr+"' Where  insp_id='"+cf.Insp_id+"'");
					}
					else
					{
						cf.arr_db.execSQL(" INSERT INTO  "+cf.count_tbl+" (insp_id,C_28_pre,C_28_uts,C_28_can,C_28_RR,C_29_pre,C_29_uts,C_29_can,C_29_RR) VALUES " +
								"('"+cf.Insp_id+"','"+ontyppre+"',"+ontyputs+","+ontypcan+","+ontyprr+","+onpre+","+onuts+","+oncan+","+onrr+")");
					}
					c.close();
				}
				catch (Exception e) {
					// TODO: handle exception
					System.out.println("onratch"+e.getMessage());
				}
			}
	}
	/**** Start Exporting function ****/
	/*** Inspection wise export main function **/
	private void Export_general() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, NullPointerException {

		// TODO Auto-generated method stub
		/*** Get teh number of insepction selected ***/

		genperModule_in = getmoduleperinc(general_boo);
		/*** Get teh number of insepction selected ends ***/
		if (general_boo[0].equals("true")) {
			general_sta[0] = "false";
			general_sta[0] = Export_PH() + "";// Policy Holder information
												// export
			sub_insp = "Policyholder information";
			sub_total = 0;
			Total += genperModule_in;
			sub_total = 100;
			setincreament();
			;
		}
		if (general_boo[1].equals("true")) {
			sub_insp = "Building information";
			sub_total = 0;
			general_sta[1] = "false";
			general_sta[1] = Export_BI() + "";// Building information export
			Total += genperModule_in;
			sub_total = 100;
			setincreament();
			;
		}

		if (general_boo[2].equals("true")) {
			sub_insp = "Weather Condition";
			sub_total = 0;
			general_sta[2] = "false";
			general_sta[2] = Export_Weather() + "";// Weather Condition export
			Total += genperModule_in;
			sub_total = 100;
			setincreament();
			;
		}
		System.out.println("general_sta[0]"+general_sta[0]+"general_sta[1]"+general_sta[1]+"general_sta[2]"+general_sta[2]);
		
		if((general_sta[0].equals("") || general_sta[0].equals("true")) && (general_sta[1].equals("") || general_sta[1].equals("true"))
				&& (general_sta[2].equals("") || general_sta[2].equals("true")))
		{
			System.out.println("came inside 9 true");
			EX_Result[9] = true;
		}
		else
		{
			EX_Result[9] = false;
		
		}
		
	}

	private void Export_FourPointData() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, NullPointerException {

		try {
			/*** Get teh number of insepction selected ***/
			fourperModule_in = getmoduleperinc(four_boo);
			/*** Get teh number of insepction selected ends ***/
			System.out.println("first inside");
			Export_FourpointAddendum();
			
			if (four_boo[0].equalsIgnoreCase("true")) {
				sub_insp = "Roof system";
				four_sta[0] = "false";
				four_sta[0] = EX_FP_Roof() + "";
				Total += fourperModule_in;
			}
		/*	if (four_boo[1].equalsIgnoreCase("true")) {
				four_sta[1] = "false";
				four_sta[1] = EX_FP_Attic() + "";
				Total += fourperModule_in;
			}*/
			if (four_boo[1].equalsIgnoreCase("true")) {
				four_sta[1] = "false";
				EX_CHKFP[1] = EX_FP_Plumbing();
				System.out.println("strwhtrval" + strwhtrval);
				if (strwhtrval.equals("0")) {
					EX_CHKFP[5] = Ex_FP_WHeater();
				} else {
					EX_CHKFP[5] = true;
				}
				System.out.println("EX_CHKFP[5]" + EX_CHKFP[5]);
				if (EX_CHKFP[1] && EX_CHKFP[5]) {
					four_sta[1] = "true";
				} else {
					four_sta[1] = "false";
				}

				Total += fourperModule_in;
			}

			if (four_boo[2].equalsIgnoreCase("true")) {
				four_sta[2] = "false";
				four_sta[2] = EX_FP_Electrical() + "";
				Total += fourperModule_in;
			}
			if (four_boo[3].equalsIgnoreCase("true")) {
				four_sta[3] = "false";
				if (EX_FP_HVAC() && EX_FP_HVACUnit()) {
					four_sta[3] = "true";
				} else {
					four_sta[3] = "false";
				}
				Total += fourperModule_in;

			}
			if (four_boo[4].equals("true")) {
				four_sta[4] = "false";
				four_sta[4] = Expot_photos("1") + "";// Add false variable in
														// your EX_CHKFP and add
														// response to that
														// variable
				Total += fourperModule_in;
			}
			if (four_boo[5].equals("true")) {
				four_sta[5] = "false";
				four_sta[5] = Expot_feedback("1") + "";// Add false variable in
														// your EX_CHKFP and add
														// response to that
														// variable
				Total += fourperModule_in;
			}
			
			System.out.println("four_sta[0]="+four_sta[0]+"four_sta[1]"+four_sta[1]+"four_sta[2]"+four_sta[2]);
			System.out.println("four_sta[3]="+four_sta[3]+"four_sta[4]"+four_sta[4]+"four_sta[5]"+four_sta[5]);


			if (!four_sta[0].equals("false") && !four_sta[1].equals("false")
					&& !four_sta[2].equals("false")
					&& !four_sta[3].equals("false")
					&& !four_sta[4].equals("false")
					&& !four_sta[5].equals("false")					
					) {
				EX_Result[4] = true;
			} else {
				EX_Result[4] = false;
			}
			
			System.out.println("EX_Result[4]"+EX_Result[4]);			
			
			
		} catch (SocketException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting Four Point data,Server may be down so please contact Paperless admin. @";
			throw e;

		} catch (NetworkErrorException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting Four Point data,Server may be down so please contact Paperless admin. @";
			throw e;

		} catch (TimeoutException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting Four Point data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (IOException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting Four Point data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting Four Point data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting Four Point data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (Exception e) {

			System.out.println("exception " + e.getMessage());
			// TODO: handle exception
			Error_tracker += "You have problem in exporting  Four Point  data please try again later. @";

		}
		// System.out.println("correct 6");
	}
	private boolean Export_FourpointAddendum() throws SocketException,
	NetworkErrorException, TimeoutException, IOException,
	XmlPullParserException, Exception, NullPointerException {
		sub_total = 0;
		fouraddendum = false;
		try {
			cf.CreateARRTable(50);
			cf.request = cf.SoapRequest("FOURPOINTADDENDUMCOMMENTS");
			Cursor C_ADD = cf.SelectTablefunction(cf.Addendum,
					" where fld_srid='" + cf.selectedhomeid + "' and insp_typeid='1'");System.out.println("Addendum inside"+C_ADD.getCount());
			System.out.println("Four="+C_ADD.getCount());
			if (C_ADD.getCount() >= 1) {
				C_ADD.moveToFirst();
				System.out.println("Fourmove");
				cf.request.addProperty("SRID", C_ADD.getString(C_ADD.getColumnIndex("fld_srid")));
				cf.request.addProperty("Addendumcomments",cf.decode(C_ADD.getString(C_ADD.getColumnIndex("addendumcomment"))));
				
				
					System.out.println("FOURPOINTADDENDUMCOMMENTS=" + cf.request);
					sub_total = 15;
					fouraddendum = cf.SoapResponse("FOURPOINTADDENDUMCOMMENTS", cf.request);
					System.out.println("fouraddendum=" + fouraddendum);
					C_ADD.close();
			} 
			else 
			{
				fouraddendum = true;
			}

			} catch (Exception e) {
				// TODO: handle exception
				System.out.println("ctach in FOURPOINTADDENDUMCOMMENTS" + e.getMessage());
				fouraddendum = false;
				throw e;
			}
			sub_total = 100;
			return fouraddendum;
	}
	private void Export_RoofSurveyData() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, NullPointerException {
		// TODO Auto-generated method stub
		try {
			surveyperModule_in = getmoduleperinc(survey_boo);
			System.out.println("the per inc was " + surveyperModule_in);
			if (survey_boo[0].equals("true")) {
				sub_total = 0;
				sub_insp = "Roof system";
				survey_sta[0] = "false";

				if (EX_Roof_RCT_new("2")) {
					if (EX_ROOF_RCN_new("2")) {
						survey_sta[0] = "true";
					}
				}
				Total += surveyperModule_in;
				sub_total = 100;
				setincreament();
				;
			}
			if (survey_boo[1].equals("true")) {
				survey_sta[1] = "false";
				sub_total = 0;
				sub_insp = "Photos";
				survey_sta[1] = Expot_photos("2") + "";
				Total += surveyperModule_in;
				sub_total = 100;
				setincreament();
				;
			}
			if (survey_boo[2].equals("true")) {
				sub_total = 0;
				sub_insp = "Feedback information";
				survey_sta[2] = "false";
				survey_sta[2] = Expot_feedback("2") + "";
				Total += surveyperModule_in;
				sub_total = 100;
				setincreament();
				;
			}
			
			if (survey_sta[0].equals("true") && survey_sta[1].equals("true") && survey_sta[2].equals("true")) {
				EX_Result[3] = true;
			} else {
				EX_Result[3] = false;
			}
		} catch (SocketException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting Roof Survey data.Please contact Paperless admin. @";
			throw e;

		} catch (NetworkErrorException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting  Roof Survey data,Server may be down so please contact Paperless admin. @";
			throw e;

		} catch (TimeoutException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting  Roof Survey data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (IOException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting  Roof Survey data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting  Roof Survey data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting  Roof Survey data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (Exception e) {
			// TODO: handle exception
			Error_tracker += "You have problem in exporting  Roof Survey data please try again later. @";
			// return false;
		}
	}

	private void Export_B1802Data() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, NullPointerException {

		b11802perModule_in = getmoduleperinc(B11802_boo);
		try {
			if (B11802_boo[0].equals("true")) {
				sub_insp = "Question";
				sub_total = 0;
				B11802_sta[0] = "false";
				if (EX_B1802_Questions(31) && EX_B1802_RoofCover(31)
						&& EX_B1802_Comments(31)
						&& EX_B1802_WallConstruction(31)) {
					B11802_sta[0] = "true";
				} else {
					B11802_sta[0] = "false";
				}
				sub_total = 100;
				setincreament();
				;
				Total += b11802perModule_in;
			}

			if (B11802_boo[1].equals("true")) {
				sub_insp = "Window Openings";
				sub_total = 0;
				B11802_sta[1] = "false";
				B11802_sta[1] = EX_B1802_WindowOpenings(31) + "";
				Total += b11802perModule_in;
				sub_total = 100;
				setincreament();
				;
			}

			if (B11802_boo[2].equals("true")) {
				sub_insp = "Door Openings";
				sub_total = 0;
				B11802_sta[2] = "false";
				B11802_sta[2] = EX_B1802_DoorOpenings(31) + "";
				Total += b11802perModule_in;
				sub_total = 100;
				setincreament();
				;
			}
			if (B11802_boo[3].equals("true")) {
				sub_insp = "Skylights Openings";
				sub_total = 0;
				B11802_sta[3] = "false";
				B11802_sta[3] = EX_B1802_Skylights(31) + "";
				Total += b11802perModule_in;
				sub_total = 100;
				setincreament();
			}
			if (B11802_boo[4].equals("true")) {
				sub_insp = "Photos";
				sub_total = 0;
				B11802_sta[4] = "false";

				B11802_sta[4] = Expot_photos("3") + "";
				Total += b11802perModule_in;

				sub_total = 100;
				setincreament();
			}
			if (B11802_boo[5].equals("true")) {
				sub_insp = "Feedback";
				sub_total = 0;
				B11802_sta[5] = "false";

				B11802_sta[5] = Expot_feedback("3") + "";
				Total += b11802perModule_in;

				sub_total = 100;
				setincreament();
			}
			

			if (!B11802_sta[0].equals("false")
					&& !B11802_sta[1].equals("false")
					&& !B11802_sta[2].equals("false")
					&& !B11802_sta[3].equals("false")
					&& !B11802_sta[4].equals("false")
					&& !B11802_sta[5].equals("false") && !B11802_sta[6].equals("false")) {
				EX_Result[5] = true;
			} else {
				EX_Result[5] = false;
			}
			
			EX_B1802_Addendum(31);
			
		} catch (SocketException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting Wind mit B11802 data,Server may be down so please contact Paperless admin. @";
			throw e;

		} catch (NetworkErrorException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting Wind mit B11802 data,Server may be down so please contact Paperless admin. @";
			throw e;

		} catch (TimeoutException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting Wind mit B11802 data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (IOException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting Wind mit B11802 data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting Wind mit B11802 data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting Wind mit B11802 data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (Exception e) {
			// TODO: handle exception

			Error_tracker += "You have problem in exporting Wind mit B11802 data please try again later. @";
			// return false;
		}
	}
	private boolean EX_B1802_Addendum(int typeid) throws SocketException,
	NetworkErrorException, TimeoutException, IOException,
	XmlPullParserException, Exception, NullPointerException {
		sub_total = 0;
		b1802addendum = false;
		try {
			cf.request = cf.SoapRequest("WINDADDENDUMCOMMENTS");
			Cursor C_ADD = cf.SelectTablefunction(cf.Addendum," where fld_srid='" + cf.selectedhomeid + "' and (insp_typeid='31' or insp_typeid='32')");
			if (C_ADD.getCount() >= 1)
			{
				C_ADD.moveToFirst();
				cf.request.addProperty("SRID", C_ADD.getString(C_ADD.getColumnIndex("fld_srid")));
				
				findinspectionid(typeid);
				
				cf.request.addProperty("InspectionID", inspectionid);
				cf.request.addProperty("Addendumcomments",cf.decode(C_ADD.getString(C_ADD.getColumnIndex("addendumcomment"))));
					System.out.println("WINDADDENDUMCOMMENTS=" + cf.request);
					sub_total = 15;
					b1802addendum = cf.SoapResponse("WINDADDENDUMCOMMENTS", cf.request);
					System.out.println("b1802addendum=" + b1802addendum);					
			} 
			else 
			{
				b1802addendum = true;
			}

			} catch (Exception e) {
				// TODO: handle exception
				System.out.println("ctach in WINDADDENDUMCOMMENTS" + e.getMessage());
				b1802addendum = false;
				throw e;
			}
			sub_total = 100;
			return b1802addendum;
	}
	
	private void Export_Comm1Data() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, NullPointerException {
		com1perModule_in = getmoduleperinc(COM1_boo);
		try {
			
			EX_B1802_Addendum(32);
			//Export_CommAddendum(32);
			Export_BNo(32);
			if (COM1_boo[0].equals("true")) {
				COM1_sta[0] = "false";
				sub_insp = "Questions";
				if (EX_B1802_Questions(32) && EX_B1802_RoofCover(32)
						&& EX_B1802_Comments(32)
						&& EX_B1802_WallConstruction(32)) {
					COM1_sta[0] = "true";
				} else {
					COM1_sta[0] = "false";
				}
				Total += com1perModule_in;
			}
			if (COM1_boo[1].equals("true")) {
				COM1_sta[1] = "false";
				COM1_sta[1] = EX_B1802_WindowOpenings(32) + "";
				Total += com1perModule_in;
			}
			if (COM1_boo[2].equals("true")) {
				COM1_sta[2] = "false";
				COM1_sta[2] = EX_B1802_DoorOpenings(32) + "";
				Total += com1perModule_in;
			}
			if (COM1_boo[3].equals("true")) {
				COM1_sta[3] = "false";
				COM1_sta[3] = EX_B1802_Skylights(32) + "";
				Total += com1perModule_in;
			}
			if (COM1_boo[4].equals("true")) {
				COM1_sta[4] = "false";
				sub_insp = "Communal Areas";
				if (EX_Comm_CommunalAreas(32)
						&& EX_Comm_CommunalAreasElevators(32)) {
					COM1_sta[4] = "true";
				} else {
					COM1_sta[4] = "false";
				}
				Total += com1perModule_in;
			}
			if (COM1_boo[5].equals("true")) {
				COM1_sta[5] = "false";
				COM1_sta[5] = EX_CommercialRoof("4") + "";
				Total += com1perModule_in;
			}
			if (COM1_boo[6].equals("true")) {

				COM1_sta[6] = "false";
				COM1_sta[6] = Expot_photos("4") + "";
				Total += com1perModule_in;

			}
			if (COM1_boo[7].equals("true")) {

				COM1_sta[7] = "false";
				COM1_sta[7] = Expot_feedback("4") + "";
				Total += com1perModule_in;

			}
			if (!COM1_sta[0].equals("false") && !COM1_sta[1].equals("false")
					&& !COM1_sta[2].equals("false")
					&& !COM1_sta[3].equals("false")
					&& !COM1_sta[4].equals("false")
					&& !COM1_sta[5].equals("false")
					&& !COM1_sta[6].equals("false")
					&& !COM1_sta[7].equals("false") ) {
				EX_Result[6] = true;
			} else {
				EX_Result[6] = false;
			}
			

		} catch (SocketException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting Wind mit Commercial-1 data,Server may be down so please contact Paperless admin. @";
			throw e;

		} catch (NetworkErrorException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting Wind mit Commercial-1 data,Server may be down so please contact Paperless admin. @";
			throw e;

		} catch (TimeoutException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting Wind mit Commercial-1 data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (IOException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting Wind mit Commercial-1 data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting Wind mit Commercial-1 data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting Wind mit Commercial-1 data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (Exception e) {
			// TODO: handle exception
			Error_tracker += "You have problem in exporting Wind mit Commercial-1 data please try again later. @";
			// return false;
		}
	}
	
	
	private void Export_Comm2Data() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, NullPointerException {
		com2perModule_in = getmoduleperinc(COM2_boo);

		try {
			Export_CommAddendum(33);
			Export_BNo(33);
			if (COM2_boo[4].equals("true")) {
				sub_insp = "Commercial Wind";
				COM2_sta[4] = "false";
				COM2_sta[4] = EX_Comm_CommWind(33) + "";
				Total += com2perModule_in;
			}
			if (COM2_boo[0].equals("true")) {
				sub_insp = "Communal Areas";
				COM2_sta[0] = "false";
				if (EX_Comm_CommunalAreas(33)
						&& EX_Comm_CommunalAreasElevators(33)) {

					COM2_sta[0] = "true";
				} else {
					COM2_sta[0] = "false";
				}
				Total += com2perModule_in;
			}
			if (COM2_boo[1].equals("true")) {
				sub_insp = "Roof system";
				COM2_sta[1] = "false";
				COM2_sta[1] = EX_CommercialRoof("5") + "";
				Total += com2perModule_in;
			}
			if (COM2_boo[2].equals("true")) {
				sub_insp = "Auxiliary building";
				COM2_sta[2] = "false";
				if (EX_Comm_Aux(33) && EX_Comments(33)) {
					COM2_sta[2] = "true";
				} else {
					COM2_sta[2] = "false";
				}
				Total += com2perModule_in;
			}
			if (COM2_boo[3].equals("true")) {
				sub_insp = "Appliances";
				COM2_sta[3] = "false";
				if (EX_Comm_Appl(33) && EX_Comments(33)) {
					COM2_sta[3] = "true";
				} else {
					COM2_sta[3] = "false";
				}
				Total += com2perModule_in;

			}
			if (COM2_boo[5].equals("true")) {
				
				COM2_sta[5] = "false";

				COM2_sta[5] = Expot_photos("5") + "";

				Total += com2perModule_in;
			}
			if (COM2_boo[6].equals("true")) {
				
				System.out.println("its commercial 22");
				COM2_sta[6] = "false";

				COM2_sta[6] = Expot_feedback("5") + "";

				Total += com2perModule_in;
			}
			if (!COM2_sta[0].equals("false") && !COM2_sta[1].equals("false")
					&& !COM2_sta[2].equals("false")
					&& !COM2_sta[3].equals("false")
					&& !COM2_sta[4].equals("false")
					&& !COM2_sta[5].equals("false")
					&& !COM2_sta[6].equals("false")) {
				EX_Result[7] = true;
			} else {
				EX_Result[7] = false;
			}
			

		} catch (SocketException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting Wind mit Commercial-2 data,Server may be down so please contact Paperless admin. @";
			throw e;

		} catch (NetworkErrorException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting Wind mit Commercial-2 data,Server may be down so please contact Paperless admin. @";
			throw e;

		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			System.out.println("got some erro tim " + e.getMessage());
			Error_tracker += "You have problem in exporting Wind mit Commercial-2 data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (IOException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting Wind mit Commercial-2 data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting Wind mit Commercial-2 data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting Wind mit Commercial-2 data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (Exception e) {
			// TODO: handle exception

			Error_tracker += "You have problem in exporting Wind mit Commercial-2 data please try again later. @";
			// return false;
		}
	}


	private void Export_Comm3Data() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, NullPointerException {
		com3perModule_in = getmoduleperinc(COM3_boo);
		try {
			System.out.println("ceme inside comm3");
			Export_CommAddendum(34);
			Export_BNo(34);
			if (COM3_boo[4].equals("true")) {
				COM3_sta[4] = EX_Comm_CommWind(34) + "";
				Total += com3perModule_in;
			}
			if (COM3_boo[0].equals("true")) {
				COM3_sta[0] = "false";
				if (EX_Comm_CommunalAreas(34)
						&& EX_Comm_CommunalAreasElevators(34)) {
					COM3_sta[0] = "true";
				} else {
					COM3_sta[0] = "false";
				}
				Total += com3perModule_in;
			}
			if (COM3_boo[1].equals("true")) {
				sub_insp = "Roof information";
				COM3_sta[1] = "false";
				COM3_sta[1] = EX_CommercialRoof("6") + "";

				Total += com3perModule_in;
			}
			if (COM3_boo[2].equals("true")) {
				COM3_sta[2] = "false";
				sub_insp = "Auxiliary building";
				if (EX_Comm_Aux(34) && EX_Comments(34)) {
					COM3_sta[2] = "true";
				} else {
					COM3_sta[2] = "false";
				}
				Total += com3perModule_in;
			}
			if (COM3_boo[3].equals("true")) {
				COM3_sta[3] = "false";
				sub_insp = "Restaurant Supplement";
				if (EX_Comm_Appl(34) && EX_Comments(34)) {
					COM3_sta[3] = "true";
				} else {
					COM3_sta[3] = "false";
				}
				Total += com3perModule_in;
			}
			if (COM3_boo[5].equals("true")) {
				COM3_sta[5] = "false";

				COM3_sta[5] = Expot_photos("6") + "";

				Total += com3perModule_in;
			}
			if (COM3_boo[6].equals("true")) {
				COM3_sta[6] = "false";

				COM3_sta[6] = Expot_feedback("6") + "";

				Total += com3perModule_in;
			}
			System.out.println("COM3_sta[0]"+COM3_sta[0]+" "+COM3_sta[1]+" "+COM3_sta[2]+" "+COM3_sta[3]+" "+COM3_sta[4]+" "+COM3_sta[5]+" "+COM3_sta[6]);
			if (!COM3_sta[0].equals("false") && !COM3_sta[1].equals("false")
					&& !COM3_sta[2].equals("false")
					&& !COM3_sta[3].equals("false")
					&& !COM3_sta[4].equals("false")
					&& !COM3_sta[5].equals("false")
					&& !COM3_sta[6].equals("false")) {
				System.out.println("cmh er");
				EX_Result[8] = true;
			} else {
				EX_Result[8] = false;
			}

			
			
		} catch (SocketException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting Wind mit Commercial-3 data,Server may be down so please contact Paperless admin. @";
			throw e;

		} catch (NetworkErrorException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting Wind mit Commercial-4 data,Server may be down so please contact Paperless admin. @";
			throw e;

		} catch (TimeoutException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting Wind mit Commercial-4 data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (IOException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting Wind mit Commercial-4 data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting Wind mit Commercial-4 data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting Wind mit Commercial-4 data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (Exception e) {
			// TODO: handle exception
			// Error_tracker+="You have problem in exporting Wind mit Commercial-4 data please try again later. @";
			// return false;
		}
	}
	private boolean Export_CommAddendum(int typeid) throws SocketException,
	NetworkErrorException, TimeoutException, IOException,
	XmlPullParserException, Exception, NullPointerException {
		sub_total = 0;
		addendum = false;
		try {
			//cf.request = cf.SoapRequest("COMMERCIALADDENDUMCOMMENTS");
			cf.request = cf.SoapRequest("WINDADDENDUMCOMMENTS");
			Cursor C_ADD = cf.SelectTablefunction(cf.Addendum,
					" where fld_srid='" + cf.selectedhomeid + "' and insp_typeid='"+typeid+"'");
			if (C_ADD.getCount() >= 1) {
				C_ADD.moveToFirst();

				cf.request.addProperty("SRID", C_ADD.getString(C_ADD.getColumnIndex("fld_srid")));
				findinspectionid(typeid);
				cf.request.addProperty("InspectionID", inspectionid);
				
				cf.request.addProperty("Addendumcomments",cf.decode(C_ADD.getString(C_ADD.getColumnIndex("addendumcomment"))));

					System.out.println("COMMERCIALADDENDUMCOMMENTS=" + cf.request);
					sub_total = 15;
					addendum = cf.SoapResponse("WINDADDENDUMCOMMENTS", cf.request);
					System.out.println("addendum=" + addendum);
					C_ADD.close();
			} 
			else 
			{
				addendum = true;
			}

			} catch (Exception e) {
				// TODO: handle exception
				System.out.println("ctach in COMMERCIALADDENDUMCOMMENTS" + e.getMessage());
				addendum = false;
				throw e;
			}
			sub_total = 100;
			return addendum;
	}
	
	
	private boolean Export_BNo(int typeid) throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		sub_total = 0;
		bno = false;
		System.out.println("Export_BNo");
		try {
			findinspectionid(typeid);
			cf.request = cf.SoapRequest("UPDATEBUILDINGVALUE");
			System.out.println("sd");
			Cursor C_BUIL = cf.SelectTablefunction(cf.Comm_Building,
					" where fld_srid='" + cf.selectedhomeid + "' and insp_typeid='"+typeid+"'");
			System.out.println("sasdfdsf" + C_BUIL.getCount());
			if (C_BUIL.getCount() >= 1) {
				C_BUIL.moveToFirst();
				cf.request.addProperty("InspectorID", cf.Insp_id);
				/*if(inspectionid==9 || inspectionid==62 || inspectionid==90)
				{
					cf.request.addProperty("Inspectionid", "9");	
				}
				else
				{
					cf.request.addProperty("Inspectionid", "0");	
				}*/
				cf.request.addProperty("Inspectionid", inspectionid);
				cf.request.addProperty("SRID", cf.selectedhomeid);

					cf.request
							.addProperty("Buildingvalue", C_BUIL.getInt(C_BUIL
									.getColumnIndex("BUILDING_NO")));
					
				System.out.println("UPDATEBUILDINGVALUE=" + cf.request);
				sub_total = 15;
				bno = cf.SoapResponse("UPDATEBUILDINGVALUE", cf.request);
				System.out.println("bno=" + bno);
				C_BUIL.close();
			} else {
				bno = true;
			}

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("ctach in b1802" + e.getMessage());
			bno = false;
			throw e;
		}
		sub_total = 100;
		return bno;
	}

	private void Export_GCHData() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, NullPointerException {
		try {
			gchperModule_in = getmoduleperinc(GCH_sta);

			if (GCH_boo[0].equals("true")) {
				GCH_sta[0] = "false";
				GCH_sta[0] = EX_GCH_PoolPresent() + "";
				Total += gchperModule_in;
			}
			if (GCH_boo[1].equals("true")) {
				GCH_sta[1] = "false";
				GCH_sta[1] = EX_GCH_PoolFence() + "";
				Total += gchperModule_in;
			}
			if (GCH_boo[2].equals("true")) {
				GCH_sta[2] = "false";
				GCH_sta[2] = EX_GCH_Roof() + "";
				Total += gchperModule_in;
			}

			if (GCH_boo[3].equals("true")) {
				GCH_sta[3] = "false";
				GCH_sta[3] = EX_GCH_SummaryHazards() + "";
				Total += gchperModule_in;
			}
			if (GCH_boo[4].equals("true")) {
				GCH_sta[4] = "false";
				GCH_sta[4] = EX_GCH_Fire() + "";
				Total += gchperModule_in;
			}
			if (GCH_boo[5].equals("true")) {
				GCH_sta[5] = "false";
				GCH_sta[5] = EX_GCH_SpecialHazards() + "";
				Total += gchperModule_in;
			}
			if (GCH_boo[6].equals("true")) {
				GCH_sta[6] = "false";
				GCH_sta[6] = EX_GCH_HVAC() + "";
				Total += gchperModule_in;
			}
			if (GCH_boo[7].equals("true")) {
				GCH_sta[7] = "false";
				GCH_sta[7] = EX_GCH_Electric() + "";
				Total += gchperModule_in;
			}
			if (GCH_boo[8].equals("true")) {
				GCH_sta[8] = "false";
				GCH_sta[8] = EX_GCH_Hood() + "";
				Total += gchperModule_in;
			}
			if (GCH_boo[9].equals("true")) {
				GCH_sta[9] = "false";
				GCH_sta[9] = EX_GCH_FireProtection() + "";
				Total += gchperModule_in;
			}
			if (GCH_boo[10].equals("true")) {
				GCH_sta[10] = "false";
				GCH_sta[10] = EX_GCH_Misc() + "";
				Total += gchperModule_in;
			}
			if (GCH_boo[11].equals("true")) {
				GCH_sta[11] = "false";
				GCH_sta[11] = EX_GCH_Comments() + "";
				Total += gchperModule_in;
			}
			if (GCH_boo[14].equals("true")) {
				System.out.println("insidebool14");
				GCH_sta[14] = "false";
				if (EX_Comm_Appl(35) && EX_Comments(35)) {
					GCH_sta[14] = "true";
				} else {
					GCH_sta[14] = "false";
				}
				Total += gchperModule_in;
				System.out.println("stat result GCh ="+GCH_sta[14] );
			}
			if (GCH_boo[12].equals("true")) {
				GCH_sta[12] = "false";
				GCH_sta[12] = Expot_photos("7") + "";
				Total += gchperModule_in;
			}
			if (GCH_boo[13].equals("true")) {
				GCH_sta[13] = "false";
				GCH_sta[13] = Expot_feedback("7") + "";
				Total += gchperModule_in;
			}

			if (!GCH_sta[0].equals("false") && !GCH_sta[1].equals("false")
					&& !GCH_sta[2].equals("false")
					&& !GCH_sta[3].equals("false")
					&& !GCH_sta[4].equals("false")
					&& !GCH_sta[5].equals("false")
					&& !GCH_sta[6].equals("false")
					&& !GCH_sta[7].equals("false")
					&& !GCH_sta[8].equals("false")
					&& !GCH_sta[9].equals("false")
					&& !GCH_sta[10].equals("false")
					&& !GCH_sta[11].equals("false")
					&& !GCH_sta[12].equals("false")
					&& !GCH_sta[13].equals("false")
					&& !GCH_sta[14].equals("false")) {
				EX_Result[0] = true;
			} else {
				EX_Result[0] = false;
			}
		} catch (SocketException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting  General Condition & Hazards data,Server may be down so please contact Paperless admin. @";
			throw e;

		} catch (NetworkErrorException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting  General Condition & Hazards data,Server may be down so please contact Paperless admin. @";
			throw e;

		} catch (TimeoutException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting  General Condition & Hazards data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (IOException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting  General Condition & Hazards data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting  General Condition & Hazards data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting  General Condition & Hazards data,Server may be down so please contact Paperless admin. @";
			throw e;
		}

		catch (Exception e) {
			// TODO: handle exception
			// Error_tracker+="You have problem in exporting  General Condition & Hazards data please try again later. @";
			// return false;
		}
	}

	private void Export_SHData() throws SocketException, NetworkErrorException,
			TimeoutException, IOException, XmlPullParserException,
			NullPointerException {
		try {
			sinkperModule_in = getmoduleperinc(SINK_boo);
			if (SINK_boo[0].equals("true")) {
				SINK_sta[0] = "false";
				SINK_sta[0] = EX_SH_SinkSummary() + "";
				Total += sinkperModule_in;

			}
			System.out.println("SINK_boo[1]" + SINK_boo[1]);
			if (SINK_boo[1].equals("true")) {
				SINK_sta[1] = "false";

				if (EX_SH_OBS1() && EX_SH_OBS1Tree()) {
					System.out.println("SINK_sta[1]" + SINK_sta[1]);
					SINK_sta[1] = "true";
				} else {
					System.out.println("SINK_sta[1]false=" + SINK_sta[1]);
					SINK_sta[1] = "false";
				}
				Total += sinkperModule_in;

			}
			if (SINK_boo[2].equals("true")) {
				SINK_sta[2] = "false";
				SINK_sta[2] = EX_SH_OBS2() + "";
				Total += sinkperModule_in;
			}
			if (SINK_boo[3].equals("true")) {
				SINK_sta[3] = "false";
				SINK_sta[3] = EX_SH_OBS3() + "";
				Total += sinkperModule_in;
			}
			if (SINK_boo[4].equals("true")) {
				SINK_sta[4] = "false";
				SINK_sta[4] = EX_SH_OBS4() + "";
				Total += sinkperModule_in;
			}
			if (SINK_boo[5].equals("true")) {
				SINK_sta[5] = "false";

				SINK_sta[5] = Expot_photos("8") + "";// Add false variable in
														// your EX_CHKFP and add
														// response to that
														// variable

				Total += sinkperModule_in;
			}
			if (SINK_boo[6].equals("true")) {
				SINK_sta[6] = "false";
				SINK_sta[6] = Expot_feedback("8") + "";// Add false variable in
														// your EX_CHKFP and add
														// response to that
														// variable
				Total += sinkperModule_in;
			}

			if (!SINK_sta[0].equals("false") && !SINK_sta[1].equals("false")
					&& !SINK_sta[2].equals("false")
					&& !SINK_sta[3].equals("false")
					&& !SINK_sta[4].equals("false")
					&& !SINK_sta[5].equals("false")
					&& !SINK_sta[6].equals("false")) {
				EX_Result[1] = true;
			} else {
				EX_Result[1] = false;
			}
		} catch (SocketException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting  Sinkhole information data,Server may be down so please contact Paperless admin. @";
			throw e;

		} catch (NetworkErrorException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting  Sinkhole information data,Server may be down so please contact Paperless admin. @";
			throw e;

		} catch (TimeoutException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting  Sinkhole information data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (IOException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting  Sinkhole information data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting  Sinkhole information data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting  Sinkhole information data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (Exception e) {
			// TODO: handle exception
			// Error_tracker+="You have problem in exporting  Sinkhole information data please try again later. @";
			// return false;
		}
	}

	private void Export_ChineseDrywallData() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, NullPointerException {
		try {
			dryperModule_in = getmoduleperinc(DRYWALL_boo);
			if (DRYWALL_boo[0].equals("true")) {
				DRYWALL_sta[0] = "false";
				DRYWALL_sta[0] = EX_CD_SummaryCond() + "";
				Total += dryperModule_in;
			}
			if (DRYWALL_boo[1].equals("true")) {
				DRYWALL_sta[1] = "false";
				DRYWALL_sta[1] = EX_CD_Room() + "";
				Total += dryperModule_in;
			}
			if (DRYWALL_boo[2].equals("true")) {
				DRYWALL_sta[2] = "false";
				DRYWALL_sta[2] = EX_CD_HVAC() + "";
				Total += dryperModule_in;
			}
			if (DRYWALL_boo[3].equals("true")) {
				DRYWALL_sta[3] = "false";
				DRYWALL_sta[3] = EX_CD_Appliance() + "";
				Total += dryperModule_in;
			}
			if (DRYWALL_boo[4].equals("true")) {
				DRYWALL_sta[4] = "false";
				DRYWALL_sta[4] = EX_CD_Attic() + "";
				Total += dryperModule_in;
			}
			if (DRYWALL_boo[5].equals("true")) {
				DRYWALL_sta[5] = "false";
				DRYWALL_sta[5] = Expot_photos("9") + "";
				Total += dryperModule_in;
			}
			if (DRYWALL_boo[6].equals("true")) {
				DRYWALL_sta[6] = "false";
				DRYWALL_sta[6] = Expot_feedback("9") + "";// Add false variable
															// in your EX_CHKFP
															// and add response
															// to that variable
				Total += dryperModule_in;
			}
/*for(int i=0;i<DRYWALL_sta.length;i++)
{
	Log.v("DRYWALL_sta", DRYWALL_sta[i]);
}*/
			if (!DRYWALL_sta[0].equals("false")
					&& !DRYWALL_sta[1].equals("false")
					&& !DRYWALL_sta[2].equals("false")
					&& !DRYWALL_sta[3].equals("false")
					&& !DRYWALL_sta[4].equals("false")
					&& !DRYWALL_sta[5].equals("false")
					&& !DRYWALL_sta[6].equals("false")) {
				EX_Result[2] = true;
			} else {
				EX_Result[2] = false;
			}
		} catch (SocketException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting  Chinese Drywall data,Server may be down so please contact Paperless admin. @";
			throw e;

		} catch (NetworkErrorException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting  Chinese Drywall data,Server may be down so please contact Paperless admin. @";
			throw e;

		} catch (TimeoutException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting  Chinese Drywall data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (IOException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting  Chinese Drywall data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting  Chinese Drywall data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block

			Error_tracker += "You have problem in exporting  Chinese Drywall data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (Exception e) {
			// TODO: handle exception
			// Error_tracker+="You have problem in exporting  Chinese Drywall data please try again later. @";
			// return false;
		}
	}

	/*** Inspection wise export main function ends **/
	/*** Module wise export function starts ***/
	/*** General tab export **/
	private boolean Export_BI() throws SocketException, NetworkErrorException,
			TimeoutException, IOException, XmlPullParserException,
			NullPointerException {
		try {
System.out.println("inside BII");
			sub_total = 0;
			EX_BI[0] = EX_BIGENERAL();
			sub_total = 50;

			EX_BI[1] = EX_BUILDINGINFORMATION();
			sub_total = 100;
			setincreament();
			System.out.println("Ex_BI0="+EX_BI[0]);
			System.out.println("Ex_BI1="+EX_BI[1]);
			
			if (EX_BI[0] == true && EX_BI[1] == true)
			{
				exportbuilding = true;

			} else {
				exportbuilding = false;
			}
			return exportbuilding;
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			exportbuilding = false;
			Error_tracker += "You have problem in exporting Building Information,Server may be down so please contact Paperless admin. @";
			throw e;

		} catch (NetworkErrorException e) {
			// TODO Auto-generated catch block
			exportbuilding = false;
			Error_tracker += "You have problem in exporting Building Information,Server may be down so please contact Paperless admin. @";
			throw e;

		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			exportbuilding = false;
			Error_tracker += "You have problem in exporting Building Information,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			exportbuilding = false;
			Error_tracker += "You have problem in exporting Building Information,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			exportbuilding= false;
			Error_tracker += "You have problem in exporting Building Information,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (Exception e) {
			// TODO: handle exception
			exportbuilding = false;
			Error_tracker += "You have problem in exporting Building Information please try again later@";
			return false;
		}

	}

	private boolean Export_PH() throws SocketException, NetworkErrorException,
			TimeoutException, IOException, XmlPullParserException,
			NullPointerException {
		try {

			sub_total = 0;
			Cursor c1 = cf.SelectTablefunction(cf.policyholder,
					" where ARR_PH_SRID='" + cf.selectedhomeid + "' ");
			if (c1.getCount() > 0) {
				c1.moveToFirst();

				cf.request = cf.SoapRequest("EXPORTPOLICYHOLDERDATA");
				cf.request.addProperty("InspectorID", cf.Insp_id);
				cf.request.addProperty("SRID", c1.getString(2));
				cf.request.addProperty("OwnerFirstName",
						cf.decode(c1.getString(3)));
				cf.request.addProperty("OwnerLastName",
						cf.decode(c1.getString(4)));
				cf.request.addProperty("Address", cf.decode(c1.getString(5)));
				cf.request.addProperty("Address2", cf.decode(c1.getString(6)));
				cf.request.addProperty("City", cf.decode(c1.getString(7)));
				cf.request.addProperty("Zip", c1.getString(8));
				cf.request.addProperty("State", cf.decode(c1.getString(9)));
				cf.request.addProperty("County", cf.decode(c1.getString(10)));

				if (!c1.getString(33).equals("")) {
					cf.request.addProperty("YearBuilt", c1.getString(33));
				} else {
					cf.request.addProperty("YearBuilt", "");
				}

				cf.request.addProperty("InspectionDate", cf.decode(c1.getString(c1.getColumnIndex("ARR_Schedule_ScheduledDate"))));
				cf.request.addProperty("ContactPerson",
						cf.decode(c1.getString(34)));
				cf.request
						.addProperty("HomePhone", cf.decode(c1.getString(14)));
				cf.request
						.addProperty("WorkPhone", cf.decode(c1.getString(15)));
				cf.request
						.addProperty("CellPhone", cf.decode(c1.getString(16)));
				cf.request.addProperty("Nstories", cf.decode(c1.getString(17)));

				cf.request.addProperty("Email", cf.decode(c1.getString(19)));
				cf.request.addProperty("SchedulingComments",
						cf.decode(c1.getString(29)));
				System.out.println("exportph request=" + cf.request);

				cf.request.addProperty("Description1", cf.decode(c1
						.getString(c1.getColumnIndex("fld_homewonercaption"))));
				cf.request.addProperty("Description2", cf.decode(c1
						.getString(c1.getColumnIndex("fld_paperworkcaption"))));

				/*
				 * cf.request.addProperty("MailingAddress",cf.decode(c1.getString
				 * (c1.getColumnIndex("MailingAddress"))));
				 * System.out.println("exportph request="+cf.request);
				 * cf.request
				 * .addProperty("MailingAddress2",cf.decode(c1.getString
				 * (c1.getColumnIndex("MailingAddress2"))));
				 * cf.request.addProperty
				 * ("MailingCity",cf.decode(c1.getString(c1
				 * .getColumnIndex("MailingCity"))));
				 * System.out.println("exportph request="+cf.request);
				 * cf.request
				 * .addProperty("MailingZip",cf.decode(c1.getString(c1.
				 * getColumnIndex("MailingZip"))));
				 * cf.request.addProperty("MailingState"
				 * ,cf.decode(c1.getString(c1.getColumnIndex("MailingState"))));
				 * cf
				 * .request.addProperty("MailingCounty",cf.decode(c1.getString(
				 * c1.getColumnIndex("MailingCounty"))));System.out.println(
				 * "exportph request="+cf.request);
				 */

				Cursor C_policymail = cf.SelectTablefunction(
						cf.MailingPolicyHolder, " where ARR_ML_PH_SRID='"
								+ cf.selectedhomeid
								+ "' and ARR_ML_PH_InspectorId='" + cf.Insp_id
								+ "'");
				C_policymail.moveToFirst();
				if (C_policymail.getCount() > 0) {
					cf.request.addProperty("MailingAddress", cf
							.decode(C_policymail.getString(C_policymail
									.getColumnIndex("ARR_ML_PH_Address1"))));
					cf.request.addProperty("MailingAddress2", cf
							.decode(C_policymail.getString(C_policymail
									.getColumnIndex("ARR_ML_PH_Address2"))));
					cf.request.addProperty("MailingCity", cf
							.decode(C_policymail.getString(C_policymail
									.getColumnIndex("ARR_ML_PH_City"))));
					cf.request.addProperty("MailingZip", cf.decode(C_policymail
							.getString(C_policymail
									.getColumnIndex("ARR_ML_PH_Zip"))));
					cf.request.addProperty("MailingState", cf
							.decode(C_policymail.getString(C_policymail
									.getColumnIndex("ARR_ML_PH_State"))));
					cf.request.addProperty("MailingCounty", cf
							.decode(C_policymail.getString(C_policymail
									.getColumnIndex("ARR_ML_PH_County"))));
				} else {
					cf.request.addProperty("MailingAddress", "");
					cf.request.addProperty("MailingAddress2", "");
					cf.request.addProperty("MailingCity", "");
					cf.request.addProperty("MailingZip", "");
					cf.request.addProperty("MailingState", "");
					cf.request.addProperty("MailingCounty", "");
				}

				String homesignpath = cf.decode(c1.getString(c1
						.getColumnIndex("fld_homeownersign")));
				String papersignpath = cf.decode(c1.getString(c1
						.getColumnIndex("fld_paperworksign")));
				if (!homesignpath.equals("")) {
					/*** Start converting image to byte ***/
					File f = new File(homesignpath);

					if (f.exists()) {
						bitmap = ShrinkBitmap(homesignpath, 400, 400);
						MarshalBase64 marshal = new MarshalBase64();
						marshal.register(cf.envelope);
						ByteArrayOutputStream out = new ByteArrayOutputStream();
						bitmap.compress(CompressFormat.PNG, 100, out);
						byte[] raw = out.toByteArray();

						cf.request.addProperty("Signatureimg1", raw);
					} else {
						Error_tracker += "Please check the following Image is available in "
								+ homesignpath;
						cf.request.addProperty("Signatureimg1", "");
						exportph = false;
					}

				} else {
					cf.request.addProperty("Signatureimg1", "");
				}
				if (!papersignpath.equals("")) {
					/*** Start converting image to byte ***/
					File f1 = new File(papersignpath);

					if (f1.exists()) {

						bitmap1 = ShrinkBitmap(papersignpath, 400, 400);
						MarshalBase64 marshal = new MarshalBase64();
						marshal.register(cf.envelope);
						ByteArrayOutputStream out1 = new ByteArrayOutputStream();
						bitmap1.compress(CompressFormat.PNG, 100, out1);
						byte[] raw1 = out1.toByteArray();
						cf.request.addProperty("Signatureimg2", raw1);
					} else {
						Error_tracker += "Please check the following Image is available in "
								+ homesignpath;
						cf.request.addProperty("Signatureimg2", "");
						exportph = false;
					}

				} else {
					cf.request.addProperty("Signatureimg2", "");
				}
				System.out.println("1stimage");
				cf.envelope.setOutputSoapObject(cf.request);
				MarshalBase64 marshal = new MarshalBase64();
				marshal.register(cf.envelope);
				cf.envelope.setOutputSoapObject(cf.request);
				exportph = cf.SoapResponse("EXPORTPOLICYHOLDERDATA",
						cf.envelope);// System.out.println("export"+exportph);
				System.out.println("property " + cf.request);
				// exportph=
				// cf.SoapResponse("EXPORTPOLICYHOLDERDATA",cf.request);System.out.println("export"+exportph);
				System.out.println("status=" + exportph);
				sub_total = 100;

			} else {
				System.out.println("insid ph else");
				exportph=true;
				sub_total = 100;
			}
			return exportph;
		}

		catch (SocketException e) {
			// TODO Auto-generated catch block
			EX_Result[10] = false;
			Error_tracker += "You have problem in exporting Policy Holder Information,Server may be down so please contact Paperless admin. @";
			throw e;

		} catch (NetworkErrorException e) {
			// TODO Auto-generated catch block
			EX_Result[10] = false;
			Error_tracker += "You have problem in exporting Policy Holder Information,Server may be down so please contact Paperless admin. @";
			throw e;

		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			EX_Result[10] = false;
			Error_tracker += "You have problem in exporting Policy Holder Information,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			EX_Result[10] = false;
			Error_tracker += "You have problem in exporting Policy Holder Information,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			EX_Result[10] = false;
			Error_tracker += "You have problem in exporting Policy Holder Information,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			EX_Result[10] = false;
			Error_tracker += "You have problem in exporting Policy Holder Information,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (Exception e) {
			// TODO: handle exception
			EX_Result[10] = false;
			Error_tracker += "You have problem in exporting Policy Holder Information please try again later@";
			return false;
		}

	}

	
	private void Export_CustomData() throws SocketException,NetworkErrorException, TimeoutException, IOException,XmlPullParserException, NullPointerException {
			try {
				customperModule_in = getmoduleperinc(CUSTOM_boo);
			
			if (CUSTOM_boo[0].equals("true")) {
				CUSTOM_sta[0] = "false";
				CUSTOM_sta[0] = Export_Custom() + "";
				Total += dryperModule_in;
			}

			if (CUSTOM_boo[1].equals("true")) {
				CUSTOM_sta[1] = "false";
				CUSTOM_sta[1] = Expot_feedback("10") + "";
				Total += dryperModule_in;
			}
		System.out.println("custom insp11="+CUSTOM_sta[0]);
		System.out.println("custom insp22="+CUSTOM_sta[1]);
			
			if (!CUSTOM_sta[0].equals("false")
					&& !CUSTOM_sta[1].equals("false")) {
				EX_Result[11] = true;
			} else {
				EX_Result[11] =false;
			}
		} catch (SocketException e) {
			// TODO Auto-generated catch block
		
			Error_tracker += "You have problem in exporting  Chinese Drywall data,Server may be down so please contact Paperless admin. @";
			throw e;
		
		} catch (NetworkErrorException e) {
			// TODO Auto-generated catch block
		
			Error_tracker += "You have problem in exporting  Chinese Drywall data,Server may be down so please contact Paperless admin. @";
			throw e;
		
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
		
			Error_tracker += "You have problem in exporting  Chinese Drywall data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (IOException e) {
			// TODO Auto-generated catch block
		
			Error_tracker += "You have problem in exporting  Chinese Drywall data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
		
			Error_tracker += "You have problem in exporting  Chinese Drywall data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
		
			Error_tracker += "You have problem in exporting  Chinese Drywall data,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (Exception e) {
			// TODO: handle exception
			// Error_tracker+="You have problem in exporting  Chinese Drywall data please try again later. @";
			// return false;
		}
		}

	
	
	private String Export_Custom() throws SocketException,NetworkErrorException, TimeoutException, IOException,XmlPullParserException {
		// TODO Auto-generated method stub
		String name="";
		String result_val = "";
		sub_total=0;
		sub_insp = "Custom Inspection";
		String typeid="";
		cf.CreateARRTable(90);
		Cursor c1 = cf.arr_db.rawQuery("Select * from " + cf.Custom_report + " Where SRID='" + cf.selectedhomeid+ "'", null);
		System.out.println("c1="+c1.getCount());
		if(c1.getCount()>0)
		{
			boolean com = false;
			c1.moveToFirst();
			int roof_inc = 50 / c1.getCount();
			for (int m = 0; m < c1.getCount(); m++, c1.moveToNext()) {
				cf.request = cf.SoapRequest("CUSTIONMAININSPECTION");
				
				String path = cf.decode(c1.getString(c1.getColumnIndex("ReportImage")));
				name = path.substring(path.lastIndexOf("/") + 1);

				cf.request.addProperty("InspectorID", cf.Insp_id);				
				cf.request.addProperty("Srid", cf.selectedhomeid);
				cf.request.addProperty("InspectionID", c1.getString(c1.getColumnIndex("InspectionID")));				
				cf.request.addProperty("Documentname",name);
				cf.request.addProperty("Reportname",cf.decode(c1.getString(c1.getColumnIndex("ReportTitle"))));
				System.out.println("Notrequired="+c1.getString(c1.getColumnIndex("NotRequired")));
				if(c1.getString(c1.getColumnIndex("NotRequired")).equals("1"))
				{
					cf.request.addProperty("Notrequiredreport",true);
				}
				else if(c1.getString(c1.getColumnIndex("NotRequired")).equals("0"))
				{
					cf.request.addProperty("Notrequiredreport",false);
				}
				System.out.println("FBNotRequired="+c1.getString(c1.getColumnIndex("FBNotRequired")));
				if(c1.getString(c1.getColumnIndex("FBNotRequired")).equals("1"))
				{
					cf.request.addProperty("Notrequirefeedbackreport",true);
				}
				else if(c1.getString(c1.getColumnIndex("FBNotRequired")).equals("0"))
				{
					cf.request.addProperty("Notrequirefeedbackreport",false);
				}
				
				typeid = c1.getString(c1.getColumnIndex("IsMainInspection"));
				if(typeid.equals("0"))
				{
					if(c1.getString(c1.getColumnIndex("InspectionID")).equals("0"))
					{
						typeid = "3";
					}
					else
					{
						typeid = "2";
					}
				}
				cf.request.addProperty("Inspectiontype",Integer.parseInt(typeid));
				cf.request.addProperty("Androidid",c1.getInt(c1.getColumnIndex("Ques_Id")));
				
				
			/*	if (name.endsWith(".pdf") || name.endsWith(".docx") || name.endsWith(".doc") || name.endsWith(".xls") || name.endsWith(".xlsx")) 
				{
					System.out.println("sindie Other");
					File dir = Environment.getExternalStorageDirectory();
					if (path.startsWith("file:///")) {
						path = path.substring(11);
					}
					if(!path.equals(""))
					{
					
						File assist = new File(path);
						if (assist.exists()) {
							try {
								String mypath = path;
								String temppath[] = mypath.split("/");
								int ss = temppath[temppath.length - 1]
										.lastIndexOf(".");
								String tests = temppath[temppath.length - 1]
										.substring(ss);
								String namedocument;
								InputStream fis = new FileInputStream(assist);
								long length = assist.length();
								byte[] bytes = new byte[(int) length];
								int offset = 0;
								int numRead = 0;
								while (offset < bytes.length
										&& (numRead = fis.read(bytes, offset,
												bytes.length - offset)) >= 0) {
									offset += numRead;
								}
								Object strBase64 = Base64.encode(bytes);
								cf.request.addProperty("UploadedDocument", strBase64);
	
								System.out.println("custom info rsult strBase64"+strBase64+"Resul"+ result_val);
								com = true;
							} catch (Exception e) {
								// TODO: handle exception.
								Error_tracker += "You have problem uploadin custom inspection document in "+path;
							}
							if (com) {
								result_val += cf.SoapResponse(
										"CUSTIONMAININSPECTION", cf.request) + "~";
							} else {
								result_val += "false~";
							}
	
						} else {
							Error_tracker += "Please check the following Image/Document is available in "+path;
							 feed_missingfiles+=tit+"&#94;"; 
							result_val += "false~";
							 System.out.println("file missing"); 
						}
					}
					else
					{
						result_val += "true~";
					}

				} else {
					System.out.println("sindie JPG"+path);
					bitmap = null;
					if(!path.equals(""))
					{
						File f = new File(path);System.out.println("file exist="+f.exists());
						if (f.exists()) {
							*//***
							 * Check the file size compress the based on the
							 * size
							 **//*
							*//***
							 * Check the file size compress the based on the
							 * size ends
							 **//*
							
							try {
								bitmap = ShrinkBitmap(path, 400, 400);// BitmapFactory.decodeFile(path);//ShrinkBitmap(path,
																		// 800,
																		// 800);
								MarshalBase64 marshal = new MarshalBase64();
								marshal.register(cf.envelope);
								ByteArrayOutputStream out = null;
								out = new ByteArrayOutputStream();
								bitmap.compress(CompressFormat.PNG, 100, out);
								byte[] raw = out.toByteArray();
	
								cf.request.addProperty("UploadedDocument", raw);
								System.out.println("custom request"+cf.request);
								
								com = true;
							} catch (Exception e) {
								// TODO: handle exception
								Error_tracker += "You have problem uploadin custom insepction image/document in path "+path + "@";
							}
							if (com) {
								result_val += cf.SoapResponse(
										"CUSTIONMAININSPECTION", cf.request) + "~";
							} else {
								result_val += "false";
							}
							System.out.println("Feed doc rsult" + result_val);
						} else {
							// feedback_fileexits=true;
							Error_tracker += "Please check the following Image/Document is available "
									+" Path=" + path + " @";
							result_val += "false~";
						}
					}
					else
					{
						result_val += "true~";
					}
				}
				System.out.println("Report"+ cf.request);*/
				if (name.endsWith(".pdf") || name.endsWith(".docx") || name.endsWith(".doc") || name.endsWith(".xls") || name.endsWith(".xlsx")) 
				{				
					File dir = Environment.getExternalStorageDirectory();
					if (path.startsWith("file:///")) {
						path = path.substring(11);
					}
					if(!path.equals(""))
					{
						File assist = new File(path);
						if (assist.exists()) {
							try {System.out.println("file =");
								String mypath = path;
	
								String temppath[] = mypath.split("/");
								int ss = temppath[temppath.length - 1]
										.lastIndexOf(".");
								String tests = temppath[temppath.length - 1]
										.substring(ss);
								String namedocument;System.out.println("tests ="+tests);
								InputStream fis = new FileInputStream(assist);
								long length = assist.length();System.out.println("length ="+length);
								byte[] bytes = new byte[(int) length];
								int offset = 0;
								int numRead = 0;
								while (offset < bytes.length
										&& (numRead = fis.read(bytes, offset,
												bytes.length - offset)) >= 0) {
									offset += numRead;
								}
								Object strBase64 = Base64.encode(bytes);System.out.println("strBase64 ="+strBase64);
								cf.request.addProperty("UploadedDocument", strBase64);System.out.println("UploadedDocument"+strBase64);
							} catch (Exception e) {
								// TODO: handle exception.
								Error_tracker += "Please check the following Document is available in Custom Inspection"+ " Path="
										+ cf.decode(c1.getString(c1.getColumnIndex("ReportImage"))) + " @";
							}
						}
						else{
							System.out.println("not exsits");
						}
					}
					else
					{
						cf.request.addProperty("UploadedDocument", "");
					}
				}
				else
				{				
				
						try {
							if(!path.equals(""))
							{
								File f=new File(path);
								System.out.println("Filename="+cf.decode(c1.getString(c1.getColumnIndex("ReportImage"))));
								if (f.exists()) 
								{
									System.out.println("file exists");
									byte[] raw = null;
									Bitmap bitmap = cf.ShrinkBitmap(cf.decode(c1.getString(c1.getColumnIndex("ReportImage"))),400, 400);
									MarshalBase64 marshal = new MarshalBase64();
									marshal.register(cf.envelope);
									ByteArrayOutputStream out = new ByteArrayOutputStream();
									bitmap.compress(CompressFormat.PNG, 100, out);
									raw = out.toByteArray();
									cf.request.addProperty("UploadedDocument", raw);
								} else {
									System.out.println("file not exists");
									Error_tracker += "Please check the following Document is available in Custom Inspection"+ " Path="
											+ cf.decode(c1.getString(c1.getColumnIndex("ReportImage"))) + " @";
									result_val += "~false";
								}
							}
							else
							{
								cf.request.addProperty("UploadedDocument", "");
							}
						} catch (Exception e) {
							// TODO: handle exception
							System.out.println("comes in the catch "+e.getMessage());
							Error_tracker += "Please check the following Document is available in Custom Inspection"+ " Path="
									+ cf.decode(c1.getString(c1.getColumnIndex("ReportImage"))) + " @";
							cf.request.addProperty("UploadedDocument", "");
						}
				}
				
				
				cf.envelope.setOutputSoapObject(cf.request);
				MarshalBase64 marshal = new MarshalBase64();
				marshal.register(cf.envelope);

				cf.envelope.setOutputSoapObject(cf.request);

				

				HttpTransportSE androidHttpTransport1 = new HttpTransportSE(
						cf.URL_ARR);
				androidHttpTransport1.call(cf.NAMESPACE + "CUSTIONMAININSPECTION",
						cf.envelope);
				System.out.println("reeeee"+cf.envelope.getResponse().toString());
				
				result_val += cf.envelope.getResponse().toString() + "~";
				System.out.println("Report image  " + result_val);
				cf.request = null;
				sub_total += roof_inc;

			}
		}
		if(result_val.contains("false"))
		{
			exportcustom ="false";
		}
		else 
		{
			exportcustom ="true";
		}
		
		sub_total = 100;

		return exportcustom;
	}

	private boolean Export_Weather() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, NullPointerException {
		try {
			sub_total = 0;
			Cursor c1 = cf.SelectTablefunction(cf.WeatherCondition,
					" where Wea_srid='" + cf.selectedhomeid + "' ");
			if (c1.getCount() > 0) {
				c1.moveToFirst();
				cf.request = cf.SoapRequest("WHEATERINFO_ANDROID");
				System.out.println("WHEATERINFO_ANDROID");
				cf.request.addProperty("SRID",
						c1.getString(c1.getColumnIndex("Wea_srid")));
				cf.request.addProperty("OutdoorTemp", cf.decode(c1.getString(c1
						.getColumnIndex("Wea_OutDoor"))));
				cf.request.addProperty("IndoorTemp", cf.decode(c1.getString(c1
						.getColumnIndex("Wea_InDoor"))));
				cf.request.addProperty("WeatherCondition", cf.decode(c1
						.getString(c1.getColumnIndex("Wea_WeatherCondi"))));
				cf.request.addProperty("RecentCondition", cf.decode(c1
						.getString(c1.getColumnIndex("Wea_RecentCondi"))));

				if (c1.getString(c1.getColumnIndex("Wea_R_outTemp")).equals("")
						&& c1.getString(c1.getColumnIndex("Wea_R_inTemp"))
								.equals("")) {
					cf.request.addProperty("RelativeHumidity", "1");
				} else {
					cf.request.addProperty("RelativeHumidity", "0");
				}
				if (!c1.getString(c1.getColumnIndex("Wea_R_outTemp"))
						.equals("")) {
					cf.request.addProperty("RelOutdoorTemp", cf.decode(c1
							.getString(c1.getColumnIndex("Wea_R_outTemp"))));
				} else {
					cf.request.addProperty("RelOutdoorTemp", 0);
				}

				if (!c1.getString(c1.getColumnIndex("Wea_R_inTemp")).equals(""))

				{
					cf.request.addProperty("RelIndoorTemp", cf.decode(c1
							.getString(c1.getColumnIndex("Wea_R_inTemp"))));
				} else {
					cf.request.addProperty("RelIndoorTemp", 0);
				}

				System.out.println("WHEATERINFO_ANDROID=" + cf.request);
				exportweather = cf.SoapResponse("WHEATERINFO_ANDROID",
						cf.request);
				System.out.println("exportweather" + exportweather);
				sub_total = 100;

			} else {
				sub_total = 100;
				exportweather = true;
			}
			//return exportweather;
			return true;
		}

		catch (SocketException e) {
			// TODO Auto-generated catch block
			EX_Result[11] = false;
			Error_tracker += "You have problem in exporting weather Information,Server may be down so please contact Paperless admin. @";
			throw e;

		} catch (NetworkErrorException e) {
			// TODO Auto-generated catch block
			EX_Result[11] = false;
			Error_tracker += "You have problem in exporting weather Information,Server may be down so please contact Paperless admin. @";
			throw e;

		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			EX_Result[11] = false;
			Error_tracker += "You have problem in exporting weatherInformation,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			EX_Result[11] = false;
			Error_tracker += "You have problem in exporting weather Information,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			EX_Result[11] = false;
			Error_tracker += "You have problem in exporting weather Information,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			EX_Result[11] = false;
			Error_tracker += "You have problem in exporting weather Information,Server may be down so please contact Paperless admin. @";
			throw e;
		} catch (Exception e) {
			// TODO: handle exception
			EX_Result[11] = true;
			Error_tracker += "You have problem in exporting weather Information please try again later@";
			return false;
		}

	}

	/*** General tab export ends **/
	/*** Four point data */
	private boolean Ex_FP_WHeater() throws Exception, SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, NullPointerException {
		// TODO Auto-generated method stub
		String result = "";
		try {
			sub_total = 50;
			sub_insp = "Plumbing System";
			Cursor C_FP_WH = cf.SelectTablefunction(cf.Plumbing_waterheater,
					" where fld_srid='" + cf.selectedhomeid + "' ");
			if (C_FP_WH.getCount() >= 1) {
				current_val_cur = C_FP_WH;

				int cc = 50 / C_FP_WH.getCount();
				C_FP_WH.moveToFirst();
				for (int i = 0; i < C_FP_WH.getCount(); i++) {

					current_pos = i;
					cf.request = cf.SoapRequest("FourpointWaterHeater");

					//String temppath[] = cf.decode(C_FP_WH.getString(C_FP_WH.getColumnIndex("whtrimg"))).split("/");
					//String tests = temppath[temppath.length - 1];
					/*if (new File(cf.decode(C_FP_WH.getString(C_FP_WH
							.getColumnIndex("whtrimg")))).exists()) {
						if (new File(cf.decode(C_FP_WH.getString(C_FP_WH
								.getColumnIndex("whtrmodimg")))).exists()) {*/
					/*String temppath[] = cf
							.decode(C_FP_WH.getString(C_FP_WH
									.getColumnIndex("whtrimg"))).split("/");
					String tests = temppath[temppath.length - 1];
							String temppath1[] = cf.decode(
									C_FP_WH.getString(C_FP_WH
											.getColumnIndex("whtrmodimg")))
									.split("/");
							String tests1 = temppath1[temppath1.length - 1];

							bitmap = ShrinkBitmap(cf.decode(C_FP_WH
									.getString(C_FP_WH
											.getColumnIndex("whtrimg"))), 400,
									400);
							MarshalBase64 marshal = new MarshalBase64();
							ByteArrayOutputStream out = new ByteArrayOutputStream();
							bitmap.compress(CompressFormat.PNG, 100, out);
							byte[] raw = out.toByteArray();
							System.out.println("raw=" + raw);

							bitmap1 = ShrinkBitmap(cf.decode(C_FP_WH
									.getString(C_FP_WH
											.getColumnIndex("whtrmodimg"))),
									400, 400);
							MarshalBase64 marshal1 = new MarshalBase64();
							ByteArrayOutputStream out1 = new ByteArrayOutputStream();
							bitmap1.compress(CompressFormat.PNG, 100, out1);
							byte[] raw1 = out1.toByteArray();
							System.out.println("raw1=" + raw1);*/

							SoapObject request = new SoapObject(cf.NAMESPACE,
									"FourpointWaterHeater");
							SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
									SoapEnvelope.VER11);
							envelope.dotNet = true;

							cf.request.addProperty("Fld_Srid", C_FP_WH
									.getString(C_FP_WH
											.getColumnIndex("fld_srid")));
							cf.request.addProperty("Watr_Heater_Typ", cf
									.decode(C_FP_WH.getString(C_FP_WH
											.getColumnIndex("whtrtype"))));
							if (cf.decode(
									C_FP_WH.getString(C_FP_WH
											.getColumnIndex("whtrage")))
									.contains("~")) {
								String[] s = cf.decode(
										C_FP_WH.getString(C_FP_WH
												.getColumnIndex("whtrage")))
										.split("~");
								cf.request.addProperty("Apprx_Age", s[0]);
								cf.request
										.addProperty("Other_Approx_Age", s[1]);
							} else {
								cf.request.addProperty("Apprx_Age", cf
										.decode(C_FP_WH.getString(C_FP_WH
												.getColumnIndex("whtrage"))));
								cf.request.addProperty("Other_Approx_Age", "");
							}
							cf.request.addProperty("Loctd_Ovr_Finhd_Celing", cf
									.decode(C_FP_WH.getString(C_FP_WH
											.getColumnIndex("whtrloc"))));
							cf.request.addProperty("DrainPan_Prsnt", cf
									.decode(C_FP_WH.getString(C_FP_WH
											.getColumnIndex("whtrdp"))));
							cf.request.addProperty("DrainLine_Prsnt", cf
									.decode(C_FP_WH.getString(C_FP_WH
											.getColumnIndex("whtrdl"))));
							cf.request.addProperty("Leakge_Evident", cf
									.decode(C_FP_WH.getString(C_FP_WH
											.getColumnIndex("whtrle"))));
							
							String path1 = cf.decode(C_FP_WH.getString(C_FP_WH.getColumnIndex("whtrimg")));
							String name1 = path1.substring(path1.lastIndexOf("/") + 1);
							File f1 = new File(path1);
							
							if (f1.exists()) 
							{
								try 
								{
									
									bitmap = ShrinkBitmap(path1, 400,400);
									marshal = new MarshalBase64();
									ByteArrayOutputStream out = new ByteArrayOutputStream();
									bitmap.compress(CompressFormat.PNG, 100, out);
									byte[] raw = out.toByteArray();
									cf.request.addProperty("WHimage", raw);
									cf.request.addProperty("WHimagename", name1);
								} 
								catch (Exception e) 
								{
										// TODO: handle exception
									Error_tracker += "Please check the following Image is available in "
											+ cf.All_insp_list[1]
											+ "  WaterHeater type "
											+ cf.decode(C_FP_WH.getString(C_FP_WH
													.getColumnIndex("whtrtype")))
											+ ", image Path="
											+ cf.decode(C_FP_WH.getString(C_FP_WH
													.getColumnIndex("whtrmodimg")))
											+ " @";
									result += "false ~";
								}
							}
							
							String path2 = cf.decode(C_FP_WH.getString(C_FP_WH.getColumnIndex("whtrmodimg")));
							
							String name2 = path1.substring(path1.lastIndexOf("/") + 1);
							File f2 = new File(path2);
							if (f2.exists()) 
							{
								try 
								{
									marshal1 = new MarshalBase64();
									ByteArrayOutputStream out = new ByteArrayOutputStream();
									bitmap1 = ShrinkBitmap(path2,400, 400);
									bitmap1.compress(CompressFormat.PNG, 100, out);
									byte[] raw1 = out.toByteArray();
									System.out.println("raw1=" + raw1);
									//cf.request.addProperty("WHimage", raw1);
									
									
									cf.request.addProperty("WHModelimagename", name2);
									cf.request.addProperty("WHModelimage", raw1);
									//cf.request.addProperty("WHModelimagename", tests1);
								} 
								catch (Exception e) 
								{
										// TODO: handle exception
									Error_tracker += "Please check the following Image is available in "
											+ cf.All_insp_list[1]
											+ "  WaterHeater type "
											+ cf.decode(C_FP_WH.getString(C_FP_WH
													.getColumnIndex("whtrtype")))
											+ ", image Path="
											+ cf.decode(C_FP_WH.getString(C_FP_WH
													.getColumnIndex("whtrmodimg")))
											+ " @";
									result += "false ~";
								}
							}

							//String temppath[] = cf.decode(C_FP_WH.getString(C_FP_WH.getColumnIndex("whtrimg"))).split("/");
							//String tests = temppath[temppath.length - 1];
							/*if (new File(cf.decode(C_FP_WH.getString(C_FP_WH
									.getColumnIndex("whtrimg")))).exists()) {
								if (new File(cf.decode(C_FP_WH.getString(C_FP_WH
										.getColumnIndex("whtrmodimg")))).exists()) {*/
							/*String temppath[] = cf
									.decode(C_FP_WH.getString(C_FP_WH
											.getColumnIndex("whtrimg"))).split("/");
							String tests = temppath[temppath.length - 1];
									String temppath1[] = cf.decode(
											C_FP_WH.getString(C_FP_WH
													.getColumnIndex("whtrmodimg")))
											.split("/");
									String tests1 = temppath1[temppath1.length - 1];

									bitmap = ShrinkBitmap(cf.decode(C_FP_WH
											.getString(C_FP_WH
													.getColumnIndex("whtrimg"))), 400,
											400);
									MarshalBase64 marshal = new MarshalBase64();
									ByteArrayOutputStream out = new ByteArrayOutputStream();
									bitmap.compress(CompressFormat.PNG, 100, out);
									byte[] raw = out.toByteArray();
									System.out.println("raw=" + raw);

									bitmap1 = ShrinkBitmap(cf.decode(C_FP_WH
											.getString(C_FP_WH
													.getColumnIndex("whtrmodimg"))),
											400, 400);
									MarshalBase64 marshal1 = new MarshalBase64();
									ByteArrayOutputStream out1 = new ByteArrayOutputStream();
									bitmap1.compress(CompressFormat.PNG, 100, out1);
									byte[] raw1 = out1.toByteArray();
									System.out.println("raw1=" + raw1);*/
							
							
							
							
							//cf.request.addProperty("WHimage", raw);
							//cf.request.addProperty("WHimagename", tests1);
							if (cf.decode(
									C_FP_WH.getString(C_FP_WH
											.getColumnIndex("whtrcap")))
									.contains("~")) {
								String[] s = cf.decode(
										C_FP_WH.getString(C_FP_WH
												.getColumnIndex("whtrcap")))
										.split("~");
								cf.request.addProperty("WaterHeaterCapacity",
										s[0]);
								cf.request.addProperty(
										"WaterHeaterCapacity_Other", s[1]);
							} else {
								cf.request.addProperty("WaterHeaterCapacity",
										cf.decode(C_FP_WH.getString(C_FP_WH
												.getColumnIndex("whtrcap"))));
								cf.request.addProperty(
										"WaterHeaterCapacity_Other", "");
							}
							if (cf.decode(
									C_FP_WH.getString(C_FP_WH
											.getColumnIndex("whtrloct")))
									.contains("~")) {
								String[] s = cf.decode(
										C_FP_WH.getString(C_FP_WH
												.getColumnIndex("whtrloct")))
										.split("~");
								cf.request.addProperty("WaterHeaterLocation",
										s[0]);
								cf.request.addProperty(
										"WaterHeaterLocation_Other", s[1]);
							} else {
								cf.request.addProperty("WaterHeaterLocation",
										cf.decode(C_FP_WH.getString(C_FP_WH
												.getColumnIndex("whtrloct"))));
								cf.request.addProperty(
										"WaterHeaterLocation_Other", "");
							}

							//cf.request.addProperty("WHModelimage", raw1);
							
							cf.request.addProperty("ROWID", C_FP_WH
									.getString(C_FP_WH
											.getColumnIndex("pwhidId")));
							System.out.println("EXPORT WH"+cf.request);
							envelope.setOutputSoapObject(cf.request);
							marshal.register(envelope);
							marshal1.register(envelope);

							System.out.println("waterheaterresult= "
									+ cf.request);
							envelope.setOutputSoapObject(cf.request);System.out.println("reqq="+request);
							HttpTransportSE androidHttpTransport = new HttpTransportSE(
									cf.URL_ARR);System.out.println("http tran");
							androidHttpTransport.call(cf.NAMESPACE
									+ "FourpointWaterHeater", envelope);System.out.println("ctransport call");
							result += envelope.getResponse().toString() + "~";System.out.println("REQSULT="+result);
							System.out.println("wresult=" + result);
							// EX_CHKFP[6]= Boolean.parseBoolean(result);

							// System.out.println("EX_CHKFP[6]= "+EX_CHKFP[6]);

						/*} else {
							Error_tracker += "Please check the following Image is available in "
									+ cf.All_insp_list[1]
									+ "  WaterHeater type "
									+ cf.decode(C_FP_WH.getString(C_FP_WH
											.getColumnIndex("whtrtype")))
									+ ", image Path="
									+ cf.decode(C_FP_WH.getString(C_FP_WH
											.getColumnIndex("whtrmodimg")))
									+ " @";
							result += "false ~";
						}
					} else {
						Error_tracker += "Please check the following Image is available in "
								+ cf.All_insp_list[1]
								+ "  WaterHeater type "
								+ cf.decode(C_FP_WH.getString(C_FP_WH
										.getColumnIndex("whtrtype")))
								+ ", image Path="
								+ cf.decode(C_FP_WH.getString(C_FP_WH
										.getColumnIndex("whtrimg"))) + " @";
						result += "false ~";
					}*/
					C_FP_WH.moveToNext();
					sub_total += cc;

				}
				C_FP_WH.close();

			}
		} catch (Exception e) {
			// TODO: handle exception
			EX_CHKFP[5] = false;
			throw e;
		}
		sub_total = 100;
		setincreament();
		;
		if (result.contains("false")) {
			EX_CHKFP[5] = false;
			return false;
		} else {
			EX_CHKFP[5] = true;
			return true;
		}

		// return EX_CHKFP[6];
	}

	private boolean EX_FP_Electrical() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		// TODO Auto-generated method stub
		sub_insp = "Electrical System";
		double ec = 100.00 / 9.00;
		sub_total = 0;
		System.out.println("elecinside");
		EX_CHKFP_EL[0] = EX_FP_GEN_ELE();
		sub_total += ec;

		EX_CHKFP_EL[1] = EX_FP_MPanel();
		sub_total += ec;

		EX_CHKFP_EL[2] = EX_FP_SPanel();
		sub_total += ec;
		System.out.println("chksppval"+chksppval);
		if (chksppval.equals("Yes")) {
			EX_CHKFP_EL[3] = Ex_FP_SPP();
		} else {
			EX_CHKFP_EL[3] = true;
		}
		sub_total += ec;

		EX_CHKFP_EL[4] = EX_FP_VEPanel();
		sub_total += ec;

		EX_CHKFP_EL[5] = EX_FP_Wiring();
		sub_total += ec;

		EX_CHKFP_EL[6] = EX_FP_Receptacles();
		sub_total += ec;

		EX_CHKFP_EL[7] = EX_FP_VBranch();
		sub_total += ec;

		EX_CHKFP_EL[8] = EX_FP_GO();
		sub_total += ec;
		System.out.println("EX_CHKFP_EL[0]"+EX_CHKFP_EL[0]+"EX_CHKFP_EL[1]"+EX_CHKFP_EL[1]+"EX_CHKFP_EL[2]"+EX_CHKFP_EL[2]+"EX_CHKFP_EL[3]"+EX_CHKFP_EL[3]+"EX_CHKFP_EL[4]"+EX_CHKFP_EL[4]+"EX_CHKFP_EL[5]"+EX_CHKFP_EL[5]+"EX_CHKFP_EL[6]"+EX_CHKFP_EL[6]+"EX_CHKFP_EL[7]"+EX_CHKFP_EL[7]+"EX_CHKFP_EL[8]"+EX_CHKFP_EL[8]);
		
		if (EX_CHKFP_EL[0] == true && EX_CHKFP_EL[1] == true
				&& EX_CHKFP_EL[2] == true && EX_CHKFP_EL[3] == true
				&& EX_CHKFP_EL[4] == true && EX_CHKFP_EL[5] == true
				&& EX_CHKFP_EL[6] == true && EX_CHKFP_EL[7] == true
				&& EX_CHKFP_EL[8] == true) {
			EX_CHKFP[3] = true;
		}
		sub_total = 100;
		setincreament();
		;
		return EX_CHKFP[3];
	}

	private boolean EX_FP_GO() throws SocketException, NetworkErrorException,
			TimeoutException, IOException, XmlPullParserException, Exception,
			NullPointerException {
		// TODO Auto-generated method stub
		try {
			cf.request = cf.SoapRequest("FourpointGeneralObservations");
			cf.CreateARRTable(245);
			Cursor C_FP_GO = cf.SelectTablefunction(cf.Four_Electrical_GO,
					" where fld_srid='" + cf.selectedhomeid + "' ");
			if (C_FP_GO.getCount() >= 1) {
				C_FP_GO.moveToFirst();
				String[] FP_ELE_GOinfos = cf.setvalueToArray(C_FP_GO);

				cf.request.addProperty("Fld_Srid", FP_ELE_GOinfos[1]);
				cf.request.addProperty("fld_goinc", FP_ELE_GOinfos[2]);
				cf.request.addProperty("haz", FP_ELE_GOinfos[3]);
				if (FP_ELE_GOinfos[4]
						.contains("Excessing Use of Extension Cords")
						|| FP_ELE_GOinfos[4]
								.contains("Missing Covers/Open Junction Boxes")
						|| FP_ELE_GOinfos[4]
								.contains("Damaged Fixtures/Fittings")
						|| FP_ELE_GOinfos[4]
								.contains("Overheating/Scorching Noted")
						|| FP_ELE_GOinfos[4]
								.contains("Loose/Exposed/Unsafe Wiring")) {
					FP_ELE_GOinfos[4] = FP_ELE_GOinfos[4].replace(
							"Excessing Use of Extension Cords",
							"Excessing Use of Extension Cords");
					FP_ELE_GOinfos[4] = FP_ELE_GOinfos[4].replace(
							"Missing Covers/Open Junction Boxes",
							"Missing Covers/Open Junction Boxes");
					FP_ELE_GOinfos[4] = FP_ELE_GOinfos[4].replace(
							"Damaged Fixtures/Fittings",
							"Damaged Fixtures/Fittings");
					FP_ELE_GOinfos[4] = FP_ELE_GOinfos[4].replace(
							"Overheating/Scorching Noted",
							"Overheating/Scorching Noted");
					FP_ELE_GOinfos[4] = FP_ELE_GOinfos[4].replace(
							"Loose/Exposed/Unsafe Wiring",
							"Loose/Exposed/Unsafe Wiring");

				}
				if (FP_ELE_GOinfos[4].contains("&#40;")) {
					String[] s = FP_ELE_GOinfos[4].split("&#40;");
					s[0] = s[0].replace("&#94;", "&#44;");
					cf.request.addProperty("hazyes", s[0] + "&#44;Other("
							+ s[1] + ")");
				} else {
					FP_ELE_GOinfos[4] = FP_ELE_GOinfos[4].replace("&#94;",
							"&#44;");
					cf.request.addProperty("hazyes", FP_ELE_GOinfos[4]);
				}
				cf.request.addProperty("haznocomt", FP_ELE_GOinfos[5]);
				if (FP_ELE_GOinfos[6].contains("Beyond Scope of Inspection")) {
					FP_ELE_GOinfos[6] = FP_ELE_GOinfos[6].replace(
							"Beyond Scope of Inspection",
							"Beyond Scope of Inspection");
				}
				cf.request.addProperty("recp", FP_ELE_GOinfos[6]);
				cf.request.addProperty("recpyescomt", FP_ELE_GOinfos[7]);
				if (FP_ELE_GOinfos[8].contains("Beyond Scope of Inspection")) {
					FP_ELE_GOinfos[8] = FP_ELE_GOinfos[8].replace(
							"Beyond Scope of Inspection",
							"Beyond Scope of Inspection");
				}
				cf.request.addProperty("lconn", FP_ELE_GOinfos[8]);
				cf.request.addProperty("lconnyescomt", FP_ELE_GOinfos[9]);

				System.out.println("cf.request" + cf.request);
				EX_CHKFP_EL[8] = cf.SoapResponse(
						"FourpointGeneralObservations", cf.request);

				System.out.println("EX_CHKFP_EL[8]= " + EX_CHKFP_EL[8]);
				C_FP_GO.close();
			}
			else
			{
				return true;
			}
		} catch (Exception e) {
			// TODO: handle exception
			EX_CHKFP_EL[8] = false;
			throw e;
		}

		return EX_CHKFP_EL[8];
	}

	private boolean EX_FP_VBranch() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		// TODO Auto-generated method stub
		try {
			cf.request = cf.SoapRequest("FourpointVisibleBranchCircuity");
			cf.CreateARRTable(244);
			Cursor C_FP_VB = cf.SelectTablefunction(cf.Four_Electrical_Details,
					" where fld_srid='" + cf.selectedhomeid + "' ");
			System.out.println("C_FP_VB" + C_FP_VB.getCount());
			if (C_FP_VB.getCount() >= 1) {
				C_FP_VB.moveToFirst();
				String[] FP_ELE_VBinfos = cf.setvalueToArray(C_FP_VB);

				cf.request.addProperty("Fld_Srid", FP_ELE_VBinfos[1]);
				System.out.println(FP_ELE_VBinfos[1]);
				
				Cursor C_FP_BW1 = cf.SelectTablefunction(cf.Four_Electrical_Wiring," where fld_srid='" + cf.selectedhomeid + "' ");
				if(C_FP_BW1.getCount()>=1)
				{
					C_FP_BW1.moveToFirst();
					cf.request.addProperty("fld_vbcinc", C_FP_BW1.getString(2));
				}
				else
				{
					cf.request.addProperty("fld_vbcinc", 0);
				}
				
				
				
				System.out.println(FP_ELE_VBinfos[2]);
				System.out.println(FP_ELE_VBinfos[3]);
				if (FP_ELE_VBinfos[3].contains("Typical Maintenance Needed")) {
					FP_ELE_VBinfos[3] = FP_ELE_VBinfos[3].replace(
							"Typical Maintenance Needed",
							"Typical Maintenance Needed");
				}
				if (FP_ELE_VBinfos[3].contains("&#94;")) {
					FP_ELE_VBinfos[3] = FP_ELE_VBinfos[3].replace("&#94;",
							"&#44;");
					cf.request.addProperty("vbc", FP_ELE_VBinfos[3]);
				} else {
					cf.request.addProperty("vbc", FP_ELE_VBinfos[3]);
				}
				cf.request.addProperty("fld_lfrin", FP_ELE_VBinfos[4]);
				if (FP_ELE_VBinfos[5].contains("Typical Maintenance Needed")) {
					FP_ELE_VBinfos[5] = FP_ELE_VBinfos[5].replace(
							"Typical Maintenance Needed",
							"Typical Maintenance Needed");
				}
				if (FP_ELE_VBinfos[5].contains("&#94;")) {
					FP_ELE_VBinfos[5] = FP_ELE_VBinfos[5].replace("&#94;",
							"&#44;");
					cf.request.addProperty("lfr", FP_ELE_VBinfos[5]);
				} else {
					cf.request.addProperty("lfr", FP_ELE_VBinfos[5]);
				}
				cf.request.addProperty("lfrotr", "");
				Cursor C_FP_GOBS = cf.SelectTablefunction(cf.Four_Electrical_GO," where fld_srid='" + cf.selectedhomeid + "' ");
				System.out.println("C_FP_GOBS" + C_FP_GOBS.getCount());
				if (C_FP_GOBS.getCount() >= 1) 
				{
					C_FP_GOBS.moveToFirst();
				
					cf.request.addProperty("fld_oesinc", C_FP_GOBS.getString(C_FP_GOBS.getColumnIndex("fld_goinc")));
				}
				else
				{
					cf.request.addProperty("fld_oesinc","");
				}
				/*if (FP_ELE_VBinfos[8]
						.contains("Yes, based on visual observations at the time of inspection")
						|| FP_ELE_VBinfos[8]
								.contains("No-Some deficiencies observed")) {
					FP_ELE_VBinfos[8] = FP_ELE_VBinfos[8]
							.replace(
									"Yes, based on visual observations at the time of inspection",
									"Yes, based on visual observations at the time of inspection");
					FP_ELE_VBinfos[8] = FP_ELE_VBinfos[8].replace(
							"No-Some deficiencies observed",
							"No-Some deficiencies >observed");
				}*/
				cf.request.addProperty("oes", FP_ELE_VBinfos[8]);
				cf.request.addProperty("oescomment", FP_ELE_VBinfos[9]);
				cf.request.addProperty("addcomment", FP_ELE_VBinfos[11]);

				System.out.println("cf.request" + cf.request);
				EX_CHKFP_EL[7] = cf.SoapResponse(
						"FourpointVisibleBranchCircuity", cf.request);

				System.out.println("EX_CHKFP_EL[7]= " + EX_CHKFP_EL[7]);
				C_FP_VB.close();
			}
			else
			{
				System.out.println("else inside");
				return true;
			}
		} catch (Exception e) {
			// TODO: handle exception
			EX_CHKFP_EL[7] = false;
			throw e;
		}
		return EX_CHKFP_EL[7];
	}

	private boolean EX_FP_Receptacles() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		// TODO Auto-generated method stub
		try {
			cf.request = cf.SoapRequest("FourpointReceptaclesOutlets");
			cf.CreateARRTable(243);
			Cursor C_FP_REC = cf.SelectTablefunction(
					cf.Four_Electrical_Receptacles, " where fld_srid='"
							+ cf.selectedhomeid + "' ");
			if (C_FP_REC.getCount() >= 1) {
				C_FP_REC.moveToFirst();
				String[] FP_ELE_RECinfos = cf.setvalueToArray(C_FP_REC);

				cf.request.addProperty("Fld_Srid", FP_ELE_RECinfos[1]);
				cf.request.addProperty("fld_inc", FP_ELE_RECinfos[2]);
				if (FP_ELE_RECinfos[3].contains("&#94;")) {
					FP_ELE_RECinfos[3] = FP_ELE_RECinfos[3].replace("&#94;",
							"&#44;");
					cf.request.addProperty("receptacle", FP_ELE_RECinfos[3]);
				} else {
					cf.request.addProperty("receptacle", FP_ELE_RECinfos[3]);
				}
				if (FP_ELE_RECinfos[4].contains("Beyond Scope of Inspection")) {
					FP_ELE_RECinfos[4] = FP_ELE_RECinfos[4].replace(
							"Beyond Scope of Inspection",
							"Beyond Scope of Inspection");
				}
				if (FP_ELE_RECinfos[4].contains("&#94;")) {
					FP_ELE_RECinfos[4] = FP_ELE_RECinfos[4].replace("&#94;",
							"&#44;");
					cf.request
							.addProperty("gfcireceptacle", FP_ELE_RECinfos[4]);
				} else {
					cf.request
							.addProperty("gfcireceptacle", FP_ELE_RECinfos[4]);
				}

				System.out.println("cf.request" + cf.request);
				EX_CHKFP_EL[6] = cf.SoapResponse("FourpointReceptaclesOutlets",
						cf.request);

				System.out.println("EX_CHKFP_EL[6]= " + EX_CHKFP_EL[6]);
				C_FP_REC.close();
			}
			else
			{
				return true;
			}
		} catch (Exception e) {
			// TODO: handle exception
			EX_CHKFP_EL[6] = false;
			throw e;
		}
		return EX_CHKFP_EL[6];
	}

	private boolean EX_FP_Wiring() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		// TODO Auto-generated method stub
		try {
			cf.request = cf.SoapRequest("FourpointWiringType");
			cf.CreateARRTable(246);
			Cursor C_FP_Wir = cf.SelectTablefunction(cf.Four_Electrical_Wiring,
					" where fld_srid='" + cf.selectedhomeid + "' ");
			if (C_FP_Wir.getCount() >= 1) {
				C_FP_Wir.moveToFirst();
				String[] FP_ELE_wirinfos = cf.setvalueToArray(C_FP_Wir);

				cf.request.addProperty("Fld_Srid", FP_ELE_wirinfos[1]);
				cf.request.addProperty("fld_wirinc", FP_ELE_wirinfos[2]);
				if (FP_ELE_wirinfos[3]
						.contains("Active Knob and Tube or Cloth wiring")) {
					FP_ELE_wirinfos[3] = FP_ELE_wirinfos[3].replace(
							"Active Knob and Tube or Cloth wiring",
							"Active Knob and Tube or Cloth wiring");
				}
				if (FP_ELE_wirinfos[3].contains("&#40;")) {
					String[] s = FP_ELE_wirinfos[3].split("&#40;");
					s[0] = s[0].replace("&#94;", "&#44;");
					cf.request.addProperty("wirtyp", s[0] + "&#44;Other("
							+ s[1] + ")");
				} else {
					FP_ELE_wirinfos[3] = FP_ELE_wirinfos[3].replace("&#94;",
							"&#44;");
					cf.request.addProperty("wirtyp", FP_ELE_wirinfos[3]);
				}
				if (FP_ELE_wirinfos[4].contains("&#94;")) {
					FP_ELE_wirinfos[4] = FP_ELE_wirinfos[4].replace("&#94;",
							"&#44;");
					cf.request.addProperty("aluminopt", FP_ELE_wirinfos[4]);
				} else {
					cf.request.addProperty("aluminopt", FP_ELE_wirinfos[4]);
				}
				if (FP_ELE_wirinfos[5].contains("&#94;")) {
					FP_ELE_wirinfos[5] = FP_ELE_wirinfos[5].replace("&#94;",
							"&#44;");
					cf.request.addProperty("aluminsubopt1", FP_ELE_wirinfos[5]);
				} else {
					cf.request.addProperty("aluminsubopt1", FP_ELE_wirinfos[5]);
				}
				if (FP_ELE_wirinfos[6].contains("&#94;")) {
					FP_ELE_wirinfos[6] = FP_ELE_wirinfos[6].replace("&#94;",
							"&#44;");
					cf.request.addProperty("aluminsubopt2", FP_ELE_wirinfos[6]);
				} else {
					cf.request.addProperty("aluminsubopt2", FP_ELE_wirinfos[6]);
				}
				if (FP_ELE_wirinfos[7].contains("&#94;")) {
					FP_ELE_wirinfos[7] = FP_ELE_wirinfos[7].replace("&#94;",
							"&#44;");
					cf.request.addProperty("aluminsubopt3", FP_ELE_wirinfos[7]);
				} else {
					cf.request.addProperty("aluminsubopt3", FP_ELE_wirinfos[7]);
				}

				System.out.println("cf.request" + cf.request);
				EX_CHKFP_EL[5] = cf.SoapResponse("FourpointWiringType",
						cf.request);

				System.out.println("EX_CHKFP_EL[5]= " + EX_CHKFP_EL[5]);
				C_FP_Wir.close();
			}
			else
			{
				return true;
			}
		} catch (Exception e) {
			// TODO: handle exception
			EX_CHKFP_EL[5] = false;
			throw e;
		}
		return EX_CHKFP_EL[5];
	}

	private boolean EX_FP_VEPanel() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		// TODO Auto-generated method stub
		try {
			
			cf.request = cf.SoapRequest("FourpointVisibleElectricalPanel");
			cf.CreateARRTable(242);
			Cursor C_FP_VEpanel = cf.SelectTablefunction(
					cf.Four_Electrical_Panel, " where fld_srid='"
							+ cf.selectedhomeid + "' ");
			if (C_FP_VEpanel.getCount() >= 1) {
				C_FP_VEpanel.moveToFirst();
				String[] FP_ELE_VEPinfos = cf.setvalueToArray(C_FP_VEpanel);

				cf.request.addProperty("Fld_Srid", FP_ELE_VEPinfos[1]);
				cf.request.addProperty("fld_inc", FP_ELE_VEPinfos[2]);
				cf.request.addProperty("visiblepanel", FP_ELE_VEPinfos[3]);
				if (FP_ELE_VEPinfos[4].contains("Blowing Fuses or Breakers")) {
					FP_ELE_VEPinfos[4] = FP_ELE_VEPinfos[4].replace(
							"Blowing Fuses or Breakers",
							"Blowing Fuses or Breakers");
				}
				if (FP_ELE_VEPinfos[4].contains("&#40;")) {
					String[] s = FP_ELE_VEPinfos[4].split("&#40;");
					s[0] = s[0].replace("&#94;", "&#44;");
					cf.request.addProperty("type", s[0] + "(" + s[1] + ")");
				} else {
					FP_ELE_VEPinfos[4] = FP_ELE_VEPinfos[4].replace("&#94;",
							"&#44;");
					cf.request.addProperty("type", FP_ELE_VEPinfos[4]);
				}

				System.out.println("cf.request" + cf.request);
				EX_CHKFP_EL[4] = cf.SoapResponse(
						"FourpointVisibleElectricalPanel", cf.request);

				System.out.println("EX_CHKFP_EL[4]= " + EX_CHKFP_EL[4]);
				C_FP_VEpanel.close();
			}
			else
			{
				return true;
			}
		} catch (Exception e) {
			// TODO: handle exception
			EX_CHKFP_EL[4] = false;
			throw e;
		}
		return EX_CHKFP_EL[4];
	}

	private boolean Ex_FP_SPP() throws SocketException, NetworkErrorException,
			TimeoutException, IOException, XmlPullParserException, Exception,
			NullPointerException {
		// TODO Auto-generated method stub
		try {
			cf.CreateARRTable(249);
			Cursor C_FP_SPP = cf.SelectTablefunction(
					cf.Four_Electrical_SubPanelPresent, " where fld_srid='"
							+ cf.selectedhomeid + "' ");
			if (C_FP_SPP.getCount() >= 1) {
				C_FP_SPP.moveToFirst();
				for (int i = 0; i < C_FP_SPP.getCount(); i++) {
					cf.request = cf.SoapRequest("FourpointSubPanelList");

					cf.request.addProperty("Fld_Srid", C_FP_SPP
							.getString(C_FP_SPP.getColumnIndex("fld_srid")));
					String sptyp = cf.decode(C_FP_SPP.getString(C_FP_SPP
							.getColumnIndex("sptype")));
					if (sptyp.contains("&#94;")) {
						sptyp = sptyp.replace("&#94;", "&#44;");
					}
					cf.request.addProperty("SubPanelType", sptyp);
					cf.request.addProperty("SubPanelLocation", cf
							.decode(C_FP_SPP.getString(C_FP_SPP
									.getColumnIndex("splocation"))));
					String spamp = cf.decode(C_FP_SPP.getString(C_FP_SPP
							.getColumnIndex("spamps")));
					if (spamp.contains("~")) {
						String[] s = spamp.split("~");
						cf.request.addProperty("SubpanelAmps", s[0]);
						cf.request.addProperty("SubpanelAmpsOther", s[1]);
					} else {
						cf.request.addProperty("SubpanelAmps", spamp);
						cf.request.addProperty("SubpanelAmpsOther", "");
					}

					cf.request.addProperty("ROWID", cf.decode(C_FP_SPP
							.getString(C_FP_SPP.getColumnIndex("sppId"))));

					System.out.println("sppresult= " + cf.request);
					EX_CHKFP_EL[3] = cf.SoapResponse("FourpointSubPanelList",
							cf.request);
					System.out.println("EX_CHKFP_EL[3]= " + EX_CHKFP_EL[3]);

					C_FP_SPP.moveToNext();
				}
				C_FP_SPP.close();
			}
		} catch (Exception e) {
			// TODO: handle exception
			EX_CHKFP_EL[3] = false;
			throw e;
		}
		return EX_CHKFP_EL[3];
	}

	private boolean EX_FP_SPanel() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		// TODO Auto-generated method stub
		try {
			cf.request = cf.SoapRequest("FourpointSubPanel");
			cf.CreateARRTable(248);
			Cursor C_FP_Spanel = cf.SelectTablefunction(
					cf.Four_Electrical_SubPanel, " where fld_srid='"
							+ cf.selectedhomeid + "' ");
			if (C_FP_Spanel.getCount() >= 1) {
				C_FP_Spanel.moveToFirst();
				String[] FP_ELE_SPinfos = cf.setvalueToArray(C_FP_Spanel);

				cf.request.addProperty("Fld_Srid", FP_ELE_SPinfos[1]);
				cf.request.addProperty("fld_electinc", FP_ELE_SPinfos[2]);
				cf.request.addProperty("spp", FP_ELE_SPinfos[3]);
				chksppval = FP_ELE_SPinfos[3];

				cf.request
						.addProperty("nosub",
								(FP_ELE_SPinfos[4].equals("") ? "0"
										: FP_ELE_SPinfos[4]));

				System.out.println("cf.request" + cf.request);
				EX_CHKFP_EL[2] = cf.SoapResponse("FourpointSubPanel",
						cf.request);

				System.out.println("EX_CHKFP_EL[2]= " + EX_CHKFP_EL[2]);
				C_FP_Spanel.close();
			}
			else
			{
				return true;
			}
		} catch (Exception e) {
			// TODO: handle exception
			EX_CHKFP_EL[2] = false;
			throw e;
		}
		return EX_CHKFP_EL[2];
	}

	private boolean EX_FP_MPanel() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		// TODO Auto-generated method stub
		try {
			cf.request = cf.SoapRequest("FourpointElectricalPanel_Updated");
			cf.CreateARRTable(247);
			Cursor C_FP_Mpanel = cf.SelectTablefunction(
					cf.Four_Electrical_MainPanel, " where fld_srid='"
							+ cf.selectedhomeid + "' ");
			if (C_FP_Mpanel.getCount() >= 1) {
				C_FP_Mpanel.moveToFirst();
				String[] FP_ELE_MPinfos = cf.setvalueToArray(C_FP_Mpanel);

				cf.request.addProperty("Fld_Srid", FP_ELE_MPinfos[1]);
				cf.request.addProperty("fld_electinc", FP_ELE_MPinfos[2]);
				cf.request.addProperty("mmpcond", FP_ELE_MPinfos[3]);
				if (FP_ELE_MPinfos[4].contains("&#40;")) {
					String[] s = FP_ELE_MPinfos[4].split("&#40;");
					s[0] = s[0].replace("&#94;", "&#44;");
					//cf.request.addProperty("mptype", s[0] + "&#44;"+ s[1]);
					cf.request.addProperty("mptype", s[0]);
					cf.request.addProperty("mptype_text",s[1]);
				} else {
					FP_ELE_MPinfos[4] = FP_ELE_MPinfos[4].replace("&#94;","&#44;");
					cf.request.addProperty("mptype", FP_ELE_MPinfos[4]);
					cf.request.addProperty("mptype_text","");
				}
				
				
				
				if (FP_ELE_MPinfos[5].contains("&#40;")) {
					String[] s = FP_ELE_MPinfos[5].split("&#40;");
					s[0] = s[0].replace("&#94;", "&#44;");
					cf.request.addProperty("mptypopt", s[0] + "&#44;Other("
							+ s[1] + ")");
				} else {
					FP_ELE_MPinfos[5] = FP_ELE_MPinfos[5].replace("&#94;",
							"&#44;");
					cf.request.addProperty("mptypopt", FP_ELE_MPinfos[5]);
				}
				cf.request.addProperty("agempl", FP_ELE_MPinfos[6]);
				cf.request.addProperty("agemplotr", FP_ELE_MPinfos[7]);
				cf.request.addProperty("yiconf", FP_ELE_MPinfos[8]);
				cf.request.addProperty("mplocation", FP_ELE_MPinfos[9]);
				cf.request.addProperty("mplocationother", FP_ELE_MPinfos[10]);
				cf.request.addProperty("dslocation", FP_ELE_MPinfos[11]);
				cf.request.addProperty("dslocationotr", FP_ELE_MPinfos[12]);
				cf.request.addProperty("recupd", FP_ELE_MPinfos[13]);
				if (FP_ELE_MPinfos[14].equals("")) {
					FP_ELE_MPinfos[14] = "0";
				}
				cf.request.addProperty("recupdyes", FP_ELE_MPinfos[14]);
				if (FP_ELE_MPinfos[15].contains("Service Entrance Conductors")
						|| FP_ELE_MPinfos[15]
								.contains("Meter to Main Panel Conductors")
						|| FP_ELE_MPinfos[15]
								.contains("Branch Circuits within Main Panel")
						|| FP_ELE_MPinfos[15]
								.contains("Main Panel/Panel Enclosure(s)")) {
					FP_ELE_MPinfos[15] = FP_ELE_MPinfos[15].replace(
							"Service Entrance Conductors",
							"Service Entrance Conductors");
					FP_ELE_MPinfos[15] = FP_ELE_MPinfos[15].replace(
							"Meter to Main Panel Conductors",
							"Meter to Main Panel Conductors");
					FP_ELE_MPinfos[15] = FP_ELE_MPinfos[15].replace(
							"Branch Circuits within Main Panel",
							"Branch Circuits within Main Panel");
					FP_ELE_MPinfos[15] = FP_ELE_MPinfos[15].replace(
							"Main Panel/Panel Enclosure(s)",
							"Main Panel/Panel Enclosure(s)");
				}
				if (FP_ELE_MPinfos[15].contains("&#94;")) {
					FP_ELE_MPinfos[15] = FP_ELE_MPinfos[15].replace("&#94;",
							"&#44;");
					cf.request.addProperty("rectyp", FP_ELE_MPinfos[15]);
				} else {
					cf.request.addProperty("rectyp", FP_ELE_MPinfos[15]);
				}
				cf.request.addProperty("recdate", FP_ELE_MPinfos[16]);
				cf.request.addProperty("recpc", FP_ELE_MPinfos[17]);

				System.out.println("cf.request" + cf.request);
				EX_CHKFP_EL[1] = cf.SoapResponse("FourpointElectricalPanel_Updated",
						cf.request);

				System.out.println("EX_CHKFP_EL[1]= " + EX_CHKFP_EL[1]);
				C_FP_Mpanel.close();
			}
			else
			{
				return true;
			}

		} catch (Exception e) {
			// TODO: handle exception
			EX_CHKFP_EL[1] = false;
			throw e;
		}
		return EX_CHKFP_EL[1];
	}

	private boolean EX_FP_GEN_ELE() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		// TODO Auto-generated method stub
		try {
			System.out.println("elecinside1");
			cf.CreateARRTable(24);
			cf.request = cf.SoapRequest("FourpointGeneralElectricalSystem");
			Cursor C_FPgenele = cf.SelectTablefunction(
					cf.Four_Electrical_General, " where fld_srid='"
							+ cf.selectedhomeid + "' ");
			System.out.println("C_FPgenele" + C_FPgenele.getCount());
			if (C_FPgenele.getCount() >= 1) {
				C_FPgenele.moveToFirst();
				String[] FP_GEN_ELE_infos = cf.setvalueToArray(C_FPgenele);System.out.println("after this");
				//System.out.println("FP_GEN_ELE_infos" + FP_GEN_ELE_infos);
				cf.request = cf.SoapRequest("FourpointGeneralElectricalSystem");
				System.out.println("ytr");
				cf.request.addProperty("Fld_Srid", FP_GEN_ELE_infos[1]);
				cf.request.addProperty("fld_inc", FP_GEN_ELE_infos[2]);
				cf.request
						.addProperty("electricalservice", FP_GEN_ELE_infos[3]);
				cf.request.addProperty("servicetype", FP_GEN_ELE_infos[4]);
				cf.request.addProperty("electricalmeter", FP_GEN_ELE_infos[5]);
				cf.request.addProperty("location", FP_GEN_ELE_infos[6]);
				cf.request.addProperty("locationother", FP_GEN_ELE_infos[7]);
				cf.request.addProperty("servicemain", FP_GEN_ELE_infos[8]);
				cf.request.addProperty("servicemainotr", FP_GEN_ELE_infos[9]);
				System.out.println("cf.request" + cf.request);
				EX_CHKFP_EL[0] = cf.SoapResponse(
						"FourpointGeneralElectricalSystem", cf.request);
				System.out.println("EX_CHKFP_EL[0]= " + EX_CHKFP_EL[0]);
				C_FPgenele.close();
			}
			else
			{
				return true;
			}
		} catch (Exception e) {
			// TODO: handle exception
			EX_CHKFP_EL[0] = false;
			throw e;
		}
		return EX_CHKFP_EL[0];
	}
	private boolean EX_FP_HVACUnit() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		// TODO Auto-generated method stub
		String result = "";
		try {
			sub_total = 50;
			sub_insp = "HVAC System";
			cf.CreateARRTable(25);
			Cursor C_FP_HVU = cf.SelectTablefunction(cf.Four_HVAC_Unit,
					" where fld_srid='" + cf.selectedhomeid + "' ");System.out.println("C_FP_HVU"+C_FP_HVU.getCount());
			if (C_FP_HVU.getCount() >= 1) {
				current_val_cur = C_FP_HVU;

				int hcc = 50 / C_FP_HVU.getCount();
				C_FP_HVU.moveToFirst();
				for (int i = 0; i < C_FP_HVU.getCount(); i++) {
					current_pos = i;
					cf.request = cf.SoapRequest("FourpointHVACUNITDetails_Updated");
					//cf.request = cf.SoapRequest("FourpointHVACUNITDetails_Updated");System.out.println("unitdetails");

					/*String temppath[] = cf.decode(
							C_FP_HVU.getString(C_FP_HVU
									.getColumnIndex("appimg"))).split("/");
					String tests = temppath[temppath.length - 1];

					bitmap = ShrinkBitmap(cf.decode(C_FP_HVU.getString(C_FP_HVU
							.getColumnIndex("appimg"))), 400, 400);
					if (new File(cf.decode(C_FP_HVU.getString(C_FP_HVU
							.getColumnIndex("appimg")))).exists()) {
						MarshalBase64 marshal = new MarshalBase64();
						marshal.register(cf.envelope);
						ByteArrayOutputStream out = new ByteArrayOutputStream();
						bitmap.compress(CompressFormat.PNG, 100, out);
						byte[] raw = out.toByteArray();
						System.out.println("raw=" + raw);*/

						SoapObject request = new SoapObject(cf.NAMESPACE,"FourpointHVACUNITDetails_Updated");
						SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
								SoapEnvelope.VER11);
						envelope.dotNet = true;System.out.println("sopenveloper");

						cf.request
								.addProperty("Fld_Srid", C_FP_HVU
										.getString(C_FP_HVU
												.getColumnIndex("fld_srid")));
						cf.request.addProperty("unttype", C_FP_HVU
								.getString(C_FP_HVU.getColumnIndex("unttype")));
						cf.request.addProperty("untnum", C_FP_HVU
								.getString(C_FP_HVU.getColumnIndex("untnum")));
						cf.request.addProperty("approxage",
								C_FP_HVU.getString(C_FP_HVU
										.getColumnIndex("approxage")));
						cf.request.addProperty("approxageother", cf
								.decode(C_FP_HVU.getString(C_FP_HVU
										.getColumnIndex("approxageother"))));
						cf.request
								.addProperty("upgraded", C_FP_HVU
										.getString(C_FP_HVU
												.getColumnIndex("upgraded")));
						if (cf.decode(
								C_FP_HVU.getString(C_FP_HVU
										.getColumnIndex("upgradedper")))
								.equals("")) {
							cf.request.addProperty("upgradedper", "0");

						} else {
							cf.request.addProperty("upgradedper", cf
									.decode(C_FP_HVU.getString(C_FP_HVU
											.getColumnIndex("upgradedper"))));

						}
						
						cf.request
						.addProperty("compupgrateval", C_FP_HVU
								.getString(C_FP_HVU
										.getColumnIndex("cupgraded")));
				if (cf.decode(
						C_FP_HVU.getString(C_FP_HVU
								.getColumnIndex("cupgradedper")))
						.equals("")) {
					cf.request.addProperty("compupgrateper", "0");

				} else {
					cf.request.addProperty("compupgrateper", cf
							.decode(C_FP_HVU.getString(C_FP_HVU
									.getColumnIndex("cupgradedper"))));

				}
						cf.request.addProperty("hsystem", C_FP_HVU
								.getString(C_FP_HVU.getColumnIndex("hsystem")));
						cf.request.addProperty("hsystemotr", cf.decode(C_FP_HVU
								.getString(C_FP_HVU
										.getColumnIndex("hsystemotr"))));
						if(C_FP_HVU.getString(C_FP_HVU.getColumnIndex("zone")).equals("--Select--"))
						{
							cf.request.addProperty("zone", "");
						}
						else
						{
							cf.request.addProperty("zone", C_FP_HVU.getString(C_FP_HVU.getColumnIndex("zone")));	
						}
						
						cf.request
								.addProperty("location", C_FP_HVU
										.getString(C_FP_HVU
												.getColumnIndex("location")));
						cf.request.addProperty("locationother", C_FP_HVU
								.getString(C_FP_HVU
										.getColumnIndex("ceilingother")));
						cf.request.addProperty("overallcond", C_FP_HVU
								.getString(C_FP_HVU
										.getColumnIndex("overallcond")));
						
						if(C_FP_HVU.getString(C_FP_HVU.getColumnIndex("ceiling")).equals("--Select--"))
						{
							cf.request.addProperty("ceiling", "");
						}
						else
						{
							cf.request.addProperty("ceiling", C_FP_HVU
									.getString(C_FP_HVU.getColumnIndex("ceiling")));	
						}
						
						
						
						
						
						cf.request.addProperty("ceilingother", C_FP_HVU
								.getString(C_FP_HVU
										.getColumnIndex("ceilingother")));
						
						if(C_FP_HVU.getString(C_FP_HVU.getColumnIndex("dripplan")).equals("--Select--"))
						{
							cf.request.addProperty("dripplan", "");
						}
						else
						{
							cf.request
							.addProperty("dripplan", C_FP_HVU
									.getString(C_FP_HVU
											.getColumnIndex("dripplan")));	
						}
						
					
						cf.request.addProperty("Coverage", C_FP_HVU.getString(C_FP_HVU.getColumnIndex("upgraded")));
						
						String path1 = cf.decode(C_FP_HVU.getString(C_FP_HVU.getColumnIndex("appimg")));System.out.println("path1"+path1);
						String name1 = path1.substring(path1.lastIndexOf("/") + 1);
						File f1 = new File(path1);System.out.println("ssss"+f1.exists());
						
						if (f1.exists()) 
						{
							try 
							{
								
								bitmap = ShrinkBitmap(path1, 400,400);
								marshal = new MarshalBase64();
								marshal.register(envelope);
								ByteArrayOutputStream out = new ByteArrayOutputStream();
								bitmap.compress(CompressFormat.PNG, 100, out);
								byte[] raw = out.toByteArray();
								cf.request.addProperty("unitphoto", raw);
								cf.request.addProperty("photoname", name1);
								
							} 
							catch (Exception e) 
							{
									// TODO: handle exception
								Error_tracker += "Please check the following Image is available in "
										+ cf.All_insp_list[1]
										+ "  HVAC UNIT "
										+ cf.decode(C_FP_HVU.getString(C_FP_HVU
												.getColumnIndex("unttype")))
										+ ", image Path="
										+ cf.decode(C_FP_HVU.getString(C_FP_HVU
												.getColumnIndex("appimg")))
										+ " @";
								result += "false ~";
							}
						}
						else
						{
							cf.request.addProperty("unitphoto", "");
							cf.request.addProperty("photoname", "");
						}
						
						/*cf.request.addProperty("unitphoto", raw);
						cf.request.addProperty("photoname", tests);*/
						/*cf.request.addProperty("compupgrateval", tests);
						cf.request.addProperty("compupgrateper", tests);*/
						cf.request.addProperty("ROWID", i+1);

						envelope.setOutputSoapObject(cf.request);
						//marshal.register(envelope);System.out.println();
						System.out.println("FourpointHVACUNITDetails_Updated" + cf.request);
						envelope.setOutputSoapObject(cf.request);
						HttpTransportSE androidHttpTransport = new HttpTransportSE(
								cf.URL_ARR);
						androidHttpTransport.call(cf.NAMESPACE
								+ "FourpointHVACUNITDetails_Updated", envelope);
						result += envelope.getResponse().toString() + "~";
						System.out.println("hvuntresult=" + result);
						// EX_CHKFP[5]= Boolean.parseBoolean(result);

						// System.out.println("EX_CHKFP[5]= "+EX_CHKFP[5]);

					/*} else {
						Error_tracker += "Please check the following Image is available in "
								+ cf.All_insp_list[1]
								+ "  HVAC Units "
								+ C_FP_HVU.getString(C_FP_HVU
										.getColumnIndex("unttype"))
								+ ", image Path="
								+ cf.decode(C_FP_HVU.getString(C_FP_HVU
										.getColumnIndex("appimg"))) + " @";
						result += "false ~";
					}*/
					C_FP_HVU.moveToNext();
					sub_total += hcc;
				}
			} else {
				return true;
			}
		} catch (Exception e) {
			// TODO: handle exception
			EX_CHKFP[5] = false;
			result += "false ~";
			throw e;
		}
		sub_total = 100;
		setincreament();
		;
		if (result.contains("false")) {
			return false;
		} else {
			return true;
		}
	}

	private boolean EX_FP_HVAC() throws SocketException, NetworkErrorException,
			TimeoutException, IOException, XmlPullParserException, Exception,
			NullPointerException {
		// TODO Auto-generated method stub
		try {
			sub_total = 0;
			sub_insp = "HVAC System";
			cf.request = cf.SoapRequest("FourpointHVACSystem_Updated");
			cf.CreateARRTable(26);
			Cursor C_FPHV = cf.SelectTablefunction(cf.Four_HVAC,
					" where fld_srid='" + cf.selectedhomeid + "' ");
			if (C_FPHV.getCount() >= 1) {
				C_FPHV.moveToFirst();
				String[] FPHV_infos = cf.setvalueToArray(C_FPHV);

				cf.request.addProperty("Fld_Srid", FPHV_infos[1]);
				cf.request.addProperty("fld_inchvac", FPHV_infos[2]);
				if (FPHV_infos[3]
						.contains("Central Air - Cool/Air Conditioning Only")
						|| FPHV_infos[3]
								.contains("Packaged Central Air - Heat Pump")
						|| FPHV_infos[3]
								.contains("Packaged Central Air - Cool/Air Conditioning Only")) {
					FPHV_infos[3] = FPHV_infos[3].replace(
							"Packaged Central Air-Cool/Air Conditioning Only",
							"Packaged Central Air-Cool/Air Conditioning Only");
					FPHV_infos[3] = FPHV_infos[3].replace(
							"Central Air-Cool/Air Conditioning Only",
							"Central Air-Cool/Air Conditioning Only");
					FPHV_infos[3] = FPHV_infos[3].replace(
							"Packaged Central Air-Heat Pump",
							"Packaged Central Air-Heat Pump");

				}
				if (FPHV_infos[3].contains("&#40;")) {
					String[] s = FPHV_infos[3].split("&#40;");
					s[0] = s[0].replace("&#94;", "&#44;");
					cf.request.addProperty("heatsource", s[0] + "&#44;Other("
							+ s[1] + ")");
				} else {
					FPHV_infos[3] = FPHV_infos[3].replace("&#94;", "&#44;");
					cf.request.addProperty("heatsource", FPHV_infos[3]);
				}
				cf.request.addProperty("noofzones", FPHV_infos[4]);
				cf.request.addProperty("tankloc", FPHV_infos[5]);
				if (FPHV_infos[6].equals("")) {
					cf.request.addProperty("distance", "0");
				} else {
					cf.request.addProperty("distance", FPHV_infos[6]);
				}

				cf.request.addProperty("openflame", FPHV_infos[7]);
				cf.request.addProperty("factor", FPHV_infos[8]);
				cf.request.addProperty("type", FPHV_infos[9]);
				cf.request.addProperty("typeotr", FPHV_infos[10]);
				cf.request.addProperty("porheater", FPHV_infos[11]);
				if (FPHV_infos[12].contains("&#40;")) {
					String[] s = FPHV_infos[12].split("&#40;");
					s[0] = s[0].replace("&#94;", "&#44;");
					cf.request.addProperty("poroption", s[0] + "&#44;Other("
							+ s[1] + ")");
				} else {
					FPHV_infos[12] = FPHV_infos[12].replace("&#94;", "&#44;");
					cf.request.addProperty("poroption", FPHV_infos[12]);
				}
				
				
				if (FPHV_infos[16].equals("Other")) {
					cf.request.addProperty("Age_HVAC", FPHV_infos[16] + "("	+ FPHV_infos[17] + ")");
				} else {
					cf.request.addProperty("Age_HVAC", FPHV_infos[16]);
				}

				cf.request.addProperty("Year_Confidence", FPHV_infos[18]);
				cf.request.addProperty("working", FPHV_infos[13]);
				cf.request.addProperty("wrkcomments", FPHV_infos[14]);
				cf.request.addProperty("hvaccomments", FPHV_infos[15]);

				System.out.println("FourpointHVACSystem_Updated" + cf.request);
				sub_total = 25;
				EX_CHKFP[4] = cf
						.SoapResponse("FourpointHVACSystem_Updated", cf.request);
				System.out.println("EX_CHKFP[4]= " + EX_CHKFP[4]);
				C_FPHV.close();
			}
			else
			{
				return true;
			}
		} catch (Exception e) {
			// TODO: handle exception
			EX_CHKFP[4] = false;
			throw e;
		}
		sub_total = 50;
		return EX_CHKFP[4];
	}

	private boolean EX_FP_Plumbing() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		// TODO Auto-generated method stub
		try {
			sub_total = 0;
			sub_insp = "Plumbing System";
			cf.request = cf.SoapRequest("FourpointPlumbingSystem");
			cf.CreateARRTable(23);
			Cursor C_FPPL = cf.SelectTablefunction(cf.Four_Electrical_Plumbing,
					" where fld_srid='" + cf.selectedhomeid + "' ");System.out.println("C_FPPL"+C_FPPL.getCount());
			if (C_FPPL.getCount() >= 1) {
				C_FPPL.moveToFirst();
				String[] FPPL_infos = cf.setvalueToArray(C_FPPL);
				cf.request.addProperty("Fld_Srid", FPPL_infos[1]);
				cf.request.addProperty("Water_Service", FPPL_infos[2]);

				if (FPPL_infos[3].contains("&#94;")) {
					s1 = FPPL_infos[3].split("&#94;");
					cf.request.addProperty("WtrService_Type", s1[0]);
					if(s1.length>1)
					{
						if(!s1[1].equals(""))
						{
							cf.request.addProperty("Comments_WaterService", s1[1]);
						}
						else
						{
							cf.request.addProperty("Comments_WaterService", "");
						}
						
					}
					else
					{
						cf.request.addProperty("Comments_WaterService", "");
					}
				} else {
					cf.request.addProperty("WtrService_Type", "");
					cf.request.addProperty("Comments_WaterService", "");
				}

				if (FPPL_infos[4].contains("&#40;")) {
					String[] s = FPPL_infos[4].split("&#40;");
					s[0] = s[0].replace("&#94;", "&#44;");
					cf.request.addProperty("PipingType", s[0] + "&#44;Other("
							+ s[1] + ")");
				} else {
					FPPL_infos[4] = FPPL_infos[4].replace("&#94;", "&#44;");
					cf.request.addProperty("PipingType", FPPL_infos[4]);
				}
				if (FPPL_infos[5].contains("&#94;")) {
					String[] s = FPPL_infos[5].split("&#94;");
					cf.request.addProperty("Age", s[0]);
					cf.request.addProperty("Other_Age_Water_Service", s[1]);
				} else {
					cf.request.addProperty("Age", FPPL_infos[5]);
					cf.request.addProperty("Other_Age_Water_Service", "");
				}
				cf.request.addProperty("Year_Instl_Confid1", FPPL_infos[6]);
				cf.request.addProperty("Main_ShutOff_Prsnt", FPPL_infos[7]);
				cf.request.addProperty("Location", FPPL_infos[8]);
				if (FPPL_infos[9].contains("&#94;")) {
					String[] s = FPPL_infos[9].split("&#94;");
					cf.request.addProperty("Confrmed_By", s[0]);
					cf.request.addProperty("txtOtherConfirm", s[1]);
				} else {
					cf.request.addProperty("Confrmed_By", FPPL_infos[9]);
					cf.request.addProperty("txtOtherConfirm", "");
				}
				if (FPPL_infos[10].contains("Yes~")) {
					if (FPPL_infos[10].contains("~")) {
						String[] s = FPPL_infos[10].split("~");
						cf.request.addProperty("Reventy_Upd", s[0]);
						cf.request.addProperty("UpdtPercentage", s[1]);
					} else {
						cf.request.addProperty("Reventy_Upd", FPPL_infos[10]);
						cf.request.addProperty("UpdtPercentage", 0);
					}
					if (FPPL_infos[11].contains("Service from meter to home")
							|| FPPL_infos[11]
									.contains("Distributed system in home")) {
						FPPL_infos[11] = FPPL_infos[11].replace(
								"Service from meter to home",
								"Service from meter to home");
						FPPL_infos[11] = FPPL_infos[11].replace(
								"Distributed system in home",
								"Distributed system in home");
					}
					cf.request.addProperty("Upd_List", FPPL_infos[11]);
					cf.request.addProperty("Permt_Confrmd", FPPL_infos[13]);
					if (FPPL_infos[12].equals("Not Determined")) {
						cf.request.addProperty("Date_Lstupd", "");
						cf.request.addProperty("DatupdNotDetermnd",
								FPPL_infos[12]);
					} else {
						cf.request.addProperty("Date_Lstupd", FPPL_infos[12]);
						cf.request.addProperty("DatupdNotDetermnd", "");
					}

				} else {
					cf.request.addProperty("Reventy_Upd", FPPL_infos[10]);
					cf.request.addProperty("UpdtPercentage", 0);
					cf.request.addProperty("Upd_List", "");
					cf.request.addProperty("Permt_Confrmd", "");
					cf.request.addProperty("Date_Lstupd", "");
					cf.request.addProperty("DatupdNotDetermnd", "");
				}

				cf.request.addProperty("Sewer_Service", FPPL_infos[14]);
				cf.request.addProperty("Leakge_Evident", FPPL_infos[23]);
				cf.request.addProperty("Full_Leakge_Review", FPPL_infos[25]);
				cf.request.addProperty("SewerOdorsNoted", FPPL_infos[24]);
				cf.request.addProperty("Sewer_Servce_Type", FPPL_infos[15]);
				if (FPPL_infos[16].contains("&#40;")) {
					String[] s = FPPL_infos[16].split("&#40;");
					s[0] = s[0].replace("&#94;", "&#44;");
					cf.request.addProperty("Piping_Type", s[0] + "&#44;Other("
							+ s[1] + ")");
				} else {
					FPPL_infos[16] = FPPL_infos[16].replace("&#94;", "&#44;");
					cf.request.addProperty("Piping_Type", FPPL_infos[16]);
				}
				if (FPPL_infos[17].contains("&#94;")) {
					String[] s = FPPL_infos[17].split("&#94;");
					cf.request.addProperty("Approx_Age", s[0]);
					cf.request.addProperty("Other_Age_Sewer_Service", s[1]);
				} else {
					cf.request.addProperty("Approx_Age", FPPL_infos[17]);
					cf.request.addProperty("Other_Age_Sewer_Service", "");
				}

				cf.request.addProperty("Year_Instl_Confid", FPPL_infos[18]);
				
				
				if(FPPL_infos[19].contains("Other"))
				{
					String[] sewertext = FPPL_infos[19].split("&#94;");
					cf.request.addProperty("Sewer_Cap_Found", sewertext[0]);
					cf.request.addProperty("Sewer_Cap_FoundOther", sewertext[1]);
				}
				else
				{
					cf.request.addProperty("Sewer_Cap_Found", FPPL_infos[19]);
					cf.request.addProperty("Sewer_Cap_FoundOther", "");
				}
				
				
				
				if (FPPL_infos[20]
						.contains("Yes-Based on Visual Observations~")) {
					if (FPPL_infos[20].contains("~")) {
						String[] s = FPPL_infos[20].split("~");
						if (s[0].contains("Yes-Based on Visual Observations")) {
							s[0] = s[0].replace(
									"Yes-Based on Visual Observations",
									"Yes-Based on Visual Observations");
						}
						cf.request.addProperty("Recnty_Updtd", s[0]);
						cf.request.addProperty("Recnty_Updtd_Percnt", s[1]);
					} else {
						cf.request.addProperty("Recnty_Updtd", FPPL_infos[20]);
						cf.request.addProperty("Recnty_Updtd_Percnt", 0);
					}
					cf.request.addProperty("Permit_Confrmd", FPPL_infos[22]);
					if (FPPL_infos[21].equals("Not Determined")) {
						cf.request.addProperty("Date_lastupdatd", "");
						cf.request.addProperty("DatupdNotDetermined",
								FPPL_infos[21]);
					} else {
						cf.request.addProperty("Date_lastupdatd",
								FPPL_infos[21]);
						cf.request.addProperty("DatupdNotDetermined", "");
					}
				} else {
					if (FPPL_infos[20]
							.contains("No - Based on Visual Observations")) {
						FPPL_infos[20] = FPPL_infos[20].replace(
								"No - Based on Visual Observations",
								"No - Based on Visual Observations");
					}
					cf.request.addProperty("Recnty_Updtd", FPPL_infos[20]);
					cf.request.addProperty("Recnty_Updtd_Percnt", 0);
					cf.request.addProperty("Permit_Confrmd", "");
					cf.request.addProperty("Date_lastupdatd", "");
					cf.request.addProperty("DatupdNotDetermined", "");
				}
				cf.request.addProperty("Rently_Upd_List", "");
				cf.request.addProperty("Full_Leakge_Review_Part", 0);
				cf.request.addProperty("Sewer_Comnts", FPPL_infos[26]);

				cf.request.addProperty("Bathroom", FPPL_infos[27]);
				cf.request.addProperty("Total_Bathrom", FPPL_infos[28]);
				cf.request.addProperty("bathrm_Above_Finshd_Sealing",
						FPPL_infos[29]);
				cf.request.addProperty("Tile_Shower", FPPL_infos[30]);
				if (FPPL_infos[31]
						.equals("None Apparent at Time of Inspection")) {
					FPPL_infos[31] = FPPL_infos[31].replace(
							"None Apparent at Time of Inspection",
							"None Apparent at Time of Inspection");
				}
				cf.request.addProperty("Posble_Tile_Reprd_Rqurd",
						FPPL_infos[31]);

				cf.request.addProperty("Loundry", FPPL_infos[32]);
				cf.request.addProperty("LoundryRoom_Prsnt", FPPL_infos[33]);
				cf.request.addProperty("Drain_Pan_Wsher", FPPL_infos[34]);
				cf.request.addProperty("Loundry_Sink_Prsnt", FPPL_infos[35]);
				cf.request.addProperty("Leakage_Evident_LoundryRoom",
						FPPL_infos[36]);
				cf.request.addProperty("Washr_Above_Fnshd_Celing",
						FPPL_infos[37]);
				cf.request.addProperty("Washr_Connction_leaking",
						FPPL_infos[40]);

				//cf.request.addProperty("OtherDeficiancy", FPPL_infos[46]);
				cf.request.addProperty("OtherDeficiancy", 0);
				cf.request.addProperty("Active_Age_Noted", FPPL_infos[38]);
				cf.request.addProperty("Indication_Prior_Leakage",
						FPPL_infos[39]);
				cf.request.addProperty("Water_Flow_Restricted", FPPL_infos[41]);
				if (FPPL_infos[42].contains("&#94;")) {
					String[] s = FPPL_infos[42].split("&#94;");
					cf.request.addProperty("OtherConditn_Comnts", s[0] + "("
							+ s[1] + ")");
				} else {
					cf.request.addProperty("OtherConditn_Comnts",
							FPPL_infos[42]);
				}
				cf.request.addProperty("ConditionofOveroll", FPPL_infos[43]);
				if (FPPL_infos[44].contains("&#94;")) {
					String[] s = FPPL_infos[44].split("&#94;");
					cf.request.addProperty("Does_Plumb_Working", s[0]);
					cf.request.addProperty("Does_Plumb_Working_Cmnts", s[1]);
				} else {
					if (FPPL_infos[44].contains("")) {
						FPPL_infos[44] = FPPL_infos[44]
								.replace(
										"Yes, based on visual observations at the time of inspection",
										"Yes, based on visual observations at the time of inspection");
					}
					cf.request
							.addProperty("Does_Plumb_Working", FPPL_infos[44]);
					cf.request.addProperty("Does_Plumb_Working_Cmnts", "");
				}
				sub_total = 25;
				cf.request
						.addProperty("Overoll_Plubming_Cmnts", FPPL_infos[45]);
				strwhtrval = FPPL_infos[47];
				cf.request.addProperty("WaterHeater", FPPL_infos[47]);
				System.out.println("cf.request" + cf.request);
				EX_CHKFP[2] = cf.SoapResponse("FourpointPlumbingSystem",
						cf.request);
				System.out.println("EX_CHKFP[2]= " + EX_CHKFP[2]);
				C_FPPL.close();
			}
			else
			{
				return true;
			}

		} catch (Exception e) {
			// TODO: handle exception
			EX_CHKFP[2] = false;
			System.out.println("e=" + e.getMessage());
			throw e;
		}
		sub_total = 50;
		return EX_CHKFP[2];

	}

	private boolean EX_FP_Attic() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		// TODO Auto-generated method stub
		try {
			sub_total = 0;
			sub_insp = "Attic System";
			cf.request = cf.SoapRequest("FourpointAtticSystem");
			cf.CreateARRTable(22);
			Cursor C_FPAT = cf.SelectTablefunction(cf.Four_Electrical_Attic,
					" where fld_srid='" + cf.selectedhomeid + "' ");
			if (C_FPAT.getCount() >= 1) {
				C_FPAT.moveToFirst();
				String[] FPAT_infos = cf.setvalueToArray(C_FPAT);

				cf.request.addProperty("Fld_Srid", FPAT_infos[1]);
				if(C_FPAT.getString(C_FPAT.getColumnIndex("Attic_Na")).equals("true"))
						cf.request.addProperty("Attic", 1);
				else
					cf.request.addProperty("Attic", 0);
				
				cf.request.addProperty("atcinc", FPAT_infos[2]);
				if (FPAT_infos[3].contains("&#94;")) {
					FPAT_infos[3] = FPAT_infos[3].replace("&#94;", "&#44;");
					cf.request.addProperty("atcoptions", FPAT_infos[3]);
				} else {
					cf.request.addProperty("atcoptions", FPAT_infos[3]);
				}
				cf.request.addProperty("atccomments", FPAT_infos[4]);
				
				if(FPAT_infos[5].equals(""))
				{
					cf.request.addProperty("rfdcinc", 0);	
				}
				else
				{
					cf.request.addProperty("rfdcinc", FPAT_infos[5]);
				}
				
				
				
				if (FPAT_infos[6].contains("Planks/Dimensional Lumber")
						|| FPAT_infos[6].contains(">6")
						|| FPAT_infos[6].contains("<6")) {
					FPAT_infos[6] = FPAT_infos[6].replace(
							"Planks/Dimensional Lumber",
							"Planks/Dimensional Lumber");
					FPAT_infos[6] = FPAT_infos[6].replace(">6\"", "&gt;6\"");
					FPAT_infos[6] = FPAT_infos[6].replace("<6\"", "&lt;6\"");
				}
				if (FPAT_infos[6].contains("&#40;")) {
					String[] s = FPAT_infos[6].split("&#40;");
					s[0] = s[0].replace("&#94;", "&#44;");
					cf.request.addProperty("rfdcmat", s[0] + "&#44;Other("
							+ s[1] + ")");
				} else {
					FPAT_infos[6] = FPAT_infos[6].replace("&#94;", "&#44;");
					cf.request.addProperty("rfdcmat", FPAT_infos[6]);
				}
				if (FPAT_infos[7].contains("&#40;")) {
					String[] s = FPAT_infos[7].split("&#40;");
					s[0] = s[0].replace("&#94;", "&#44;");
					cf.request.addProperty("rfstmat", s[0] + "&#44;Other("
							+ s[1] + ")");
				} else {
					FPAT_infos[7] = FPAT_infos[7].replace("&#94;", "&#44;");
					cf.request.addProperty("rfstmat", FPAT_infos[7]);
				}
				if (FPAT_infos[8].contains("N/A")) {
					FPAT_infos[8] = FPAT_infos[8].replace("N/A",
							"Not Applicable");
				}
				if (FPAT_infos[8].contains("&#40;")) {
					String[] s = FPAT_infos[8].split("&#40;");
					s[0] = s[0].replace("&#94;", "&#44;");
					cf.request.addProperty("rfdcthi", s[0] + "&#44;Other("
							+ s[1] + ")");
				} else {
					FPAT_infos[8] = FPAT_infos[8].replace("&#94;", "&#44;");
					cf.request.addProperty("rfdcthi", FPAT_infos[8]);
				}
				if (FPAT_infos[9].contains("N/A")) {
					FPAT_infos[9] = FPAT_infos[9].replace("N/A",
							"Not Applicable");
				}
				if (FPAT_infos[9].contains("&#40;")) {
					String[] s = FPAT_infos[9].split("&#40;");
					s[0] = s[0].replace("&#94;", "&#44;");
					cf.request.addProperty("rfdcatt", s[0] + "&#44;Other("
							+ s[1] + ")");
				} else {
					FPAT_infos[9] = FPAT_infos[9].replace("&#94;", "&#44;");
					cf.request.addProperty("rfdcatt", FPAT_infos[9]);
				}
				if (FPAT_infos[10].contains("N/A")
						|| FPAT_infos[10].contains("N/D")) {
					FPAT_infos[10] = FPAT_infos[10].replace("N/A",
							"Not Applicable");
					FPAT_infos[10] = FPAT_infos[10].replace("N/D",
							"Not Determined");
				}
				if (FPAT_infos[10].contains(">24\"")) {
					FPAT_infos[10] = FPAT_infos[10]
							.replace(">24\"", "&gt;24\"");
				}
				if (FPAT_infos[10].contains("&#94;")) {
					FPAT_infos[10] = FPAT_infos[10].replace("&#94;", "&#44;");
					cf.request.addProperty("rftruss", FPAT_infos[10]);
				} else {
					cf.request.addProperty("rftruss", FPAT_infos[10]);
				}
				if (FPAT_infos[11].contains("N/A")
						|| FPAT_infos[11].contains("N/D")) {
					FPAT_infos[11] = FPAT_infos[11].replace("N/A",
							"Not Applicable");
					FPAT_infos[11] = FPAT_infos[11].replace("N/D",
							"Not Determined");
				}
				if (FPAT_infos[11].contains(">6\"/12\"")) {
					FPAT_infos[11] = FPAT_infos[11].replace(">6\"/12\"",
							"&gt;6\"/12\"");
				}
				cf.request.addProperty("nailspac", FPAT_infos[11]);
				if (FPAT_infos[12].contains("N/A")
						|| FPAT_infos[12].contains("N/D")) {
					FPAT_infos[12] = FPAT_infos[12].replace("N/A",
							"Not Applicable");
					FPAT_infos[12] = FPAT_infos[12].replace("N/D",
							"Not Determined");
				}
				cf.request.addProperty("missednails", FPAT_infos[12]);
				cf.request.addProperty("rfwallinc", FPAT_infos[13]);
				if (FPAT_infos[14]
						.contains("Strap/Clip - Insufficient Nailing")) {
					FPAT_infos[14] = FPAT_infos[14].replace(
							"Strap/Clip - Insufficient Nailing",
							"Strap/Clip-Insufficient Nailing");

				}
				if (FPAT_infos[14].contains("&#40;")) {
					String[] s = FPAT_infos[14].split("&#40;");
					s[0] = s[0].replace("&#94;", "&#44;");
					cf.request.addProperty("rtowopt", s[0] + "&#44;Other("
							+ s[1] + ")");
				} else {
					FPAT_infos[14] = FPAT_infos[14].replace("&#94;", "&#44;");
					cf.request.addProperty("rtowopt", FPAT_infos[14]);
				}
				cf.request.addProperty("nail3", FPAT_infos[15]);
				cf.request.addProperty("nail4", FPAT_infos[16]);
				cf.request.addProperty("fournail", FPAT_infos[17]);
				cf.request.addProperty("truss", FPAT_infos[18]);
				cf.request.addProperty("truss1", FPAT_infos[19]);
				cf.request.addProperty("gablewallinc", FPAT_infos[20]);
				if (FPAT_infos[21].contains("&#40;")) {
					String[] s = FPAT_infos[21].split("&#40;");
					s[0] = s[0].replace("&#94;", "&#44;");
					cf.request.addProperty("gablewallopt", s[0] + "&#44;Other("
							+ s[1] + ")");
				} else {
					FPAT_infos[21] = FPAT_infos[21].replace("&#94;", "&#44;");
					cf.request.addProperty("gablewallopt", FPAT_infos[21]);
				}
				
				if(FPAT_infos[22].equals(""))
				{
					cf.request.addProperty("atccondinc", 0);
				}
				else
				{
					cf.request.addProperty("atccondinc", FPAT_infos[22]);	
				}
				
				
				if (FPAT_infos[23].contains("N/A")
						|| FPAT_infos[23].equals("BSI")) {
					FPAT_infos[23] = FPAT_infos[23].replace("N/A",
							"Not Applicable");
					FPAT_infos[23] = FPAT_infos[23].replace("BSI",
							"Beyond Scope of Inspection");
				}
				if (FPAT_infos[23].contains("&#126;")) {
					String[] s = FPAT_infos[23].split("&#126;");
					cf.request.addProperty("moisture", s[0]);
					cf.request.addProperty("moisturecmds", s[1]);
				} else {
					cf.request.addProperty("moisture", FPAT_infos[23]);
					cf.request.addProperty("moisturecmds", "");
				}
				if (FPAT_infos[24].contains("N/A")
						|| FPAT_infos[24].equals("BSI")) {
					FPAT_infos[24] = FPAT_infos[24].replace("N/A",
							"Not Applicable");
					FPAT_infos[24] = FPAT_infos[24].replace("BSI",
							"Beyond Scope of Inspection");
				}
				if (FPAT_infos[24].contains("&#126;")) {
					String[] s = FPAT_infos[24].split("&#126;");
					cf.request.addProperty("dsheath", s[0]);
					cf.request.addProperty("dsheathcmds", s[1]);
				} else {
					cf.request.addProperty("dsheath", FPAT_infos[24]);
					cf.request.addProperty("dsheathcmds", "");
				}
				if (FPAT_infos[25].contains("N/A")
						|| FPAT_infos[25].equals("BSI")) {
					FPAT_infos[25] = FPAT_infos[25].replace("N/A",
							"Not Applicable");
					FPAT_infos[25] = FPAT_infos[25].replace("BSI",
							"Beyond Scope of Inspection");
				}
				if (FPAT_infos[25].contains("&#126;")) {
					String[] s = FPAT_infos[25].split("&#126;");
					cf.request.addProperty("droof", s[0]);
					cf.request.addProperty("droofcmds", s[1]);
				} else {
					cf.request.addProperty("droof", FPAT_infos[25]);
					cf.request.addProperty("droofcmds", "");
				}
				if (FPAT_infos[26].contains("N/A")
						|| FPAT_infos[26].equals("BSI")) {
					FPAT_infos[26] = FPAT_infos[26].replace("N/A",
							"Not Applicable");
					FPAT_infos[26] = FPAT_infos[26].replace("BSI",
							"Beyond Scope of Inspection");
				}
				if (FPAT_infos[26].contains("&#126;")) {
					String[] s = FPAT_infos[26].split("&#126;");
					cf.request.addProperty("nsalt", s[0]);
					cf.request.addProperty("nsaltcmds", s[1]);
				} else {
					cf.request.addProperty("nsalt", FPAT_infos[26]);
					cf.request.addProperty("nsaltcmds", "");
				}

				cf.request.addProperty("othertxt", FPAT_infos[27]);
				if (FPAT_infos[28].contains("N/A")
						|| FPAT_infos[28].equals("BSI")) {
					FPAT_infos[28] = FPAT_infos[28].replace("N/A",
							"Not Applicable");
					FPAT_infos[28] = FPAT_infos[28].replace("BSI",
							"Beyond Scope of Inspection");
				}
				if (FPAT_infos[28].contains("&#126;")) {
					String[] s = FPAT_infos[28].split("&#126;");
					cf.request.addProperty("other", s[0]);
					cf.request.addProperty("othercmds", s[1]);
				} else {
					cf.request.addProperty("other", FPAT_infos[28]);
					cf.request.addProperty("othercmds", "");
				}
				System.out.println("fourpointatticresult= " + cf.request);
				sub_total = 50;
				EX_CHKFP[1] = cf.SoapResponse("FourpointAtticSystem",
						cf.request);
				System.out.println("EX_CHKFP[1]= " + EX_CHKFP[1]);
				C_FPAT.close();
			}
			else
			{
				return true;
			}

		} catch (Exception e) {
			// TODO: handle exception
			EX_CHKFP[1] = false;
			throw e;
		}
		sub_total = 100;
		setincreament();
		;
		return EX_CHKFP[1];

	}

	private boolean EX_FP_Roof() throws SocketException, NetworkErrorException,
			TimeoutException, IOException, XmlPullParserException, Exception,
			NullPointerException {
		// TODO Auto-generated method stub
		if (EX_Roof_RCT_new("1")) {
			if (EX_ROOF_RCN_new("1")) {
				return true;
			}
		}
		return false;
	}

	/*** Four point data ends */
	/*** Wind mitigation data */
	private boolean EX_CommercialRoof(String inspetype) throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		// TODO Auto-generated method stub
		if (EX_Roof_RCT_new(inspetype)) {
			if (EX_ROOF_RCN_new(inspetype)) {
				EX_Comm1[2] = true;
			}
		}
		return EX_Comm1[2];
	}

	private boolean EX_B1802_Questions(int insptypeid) throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		sub_total = 0;
		EX_B1802[0] = false;

		try {
			findinspectionid(insptypeid);
			cf.request = cf.SoapRequest("WINDOVERALLQUESTIONS");
			cf.CreateARRTable(31);
			Cursor C_WND = cf.SelectTablefunction(cf.Wind_Questiontbl,
					" where fld_srid='" + cf.selectedhomeid
							+ "' and insp_typeid='" + insptypeid + "'");
			if (C_WND.getCount() >= 1) {
				B1802_infos = cf.setvalueToArray(C_WND);
				cf.request.addProperty("Fld_Srid", cf.selectedhomeid);
				cf.request.addProperty("InspectionID", inspectionid);

				cf.request.addProperty("fld_buildcode", B1802_infos[3]);
				cf.request.addProperty("fld_bcyearbuilt", B1802_infos[4]);
				cf.request.addProperty("fld_bcpermitappdate", B1802_infos[5]);

				cf.request.addProperty("fld_roofdeck", B1802_infos[6]);
				cf.request.addProperty("fld_rdothertext", B1802_infos[7]);

				cf.request.addProperty("fld_roofwall", B1802_infos[8]);
				cf.request.addProperty("fld_roofwallsub", B1802_infos[9]);

				cf.request.addProperty("fld_roofwallclipsminval",
						B1802_infos[10]);
				cf.request.addProperty("fld_roofwallclipssingminval",
						B1802_infos[11]);
				cf.request.addProperty("fld_roofwallclipsdoubminval",
						B1802_infos[12]);
				cf.request.addProperty("fld_rwothertext", B1802_infos[13]);

				cf.request.addProperty("fld_roofgeometry", B1802_infos[14]);
				if (!B1802_infos[15].equals("")) {
					cf.request.addProperty("fld_geomlength", B1802_infos[15]);
				} else {
					cf.request.addProperty("fld_geomlength", 0);
				}

				if (!B1802_infos[16].equals("")) {
					cf.request.addProperty("fld_roofgeomtotalarea",
							B1802_infos[16]);
				} else {
					cf.request.addProperty("fld_roofgeomtotalarea", 0);
				}
				cf.request
						.addProperty("fld_roofgeomothertext", B1802_infos[17]);

				cf.request.addProperty("fld_swr", B1802_infos[18]);

				cf.request.addProperty("fld_openprotect", B1802_infos[19]);
				cf.request.addProperty("fld_optype", 0);

				if (B1802_infos[21].equals("")) {
					cf.request.addProperty("fld_opsubvalue", 0);
				} else {
					cf.request.addProperty("fld_opsubvalue", B1802_infos[21]);
				}

				cf.request.addProperty("fld_oplevelchart", "");

				cf.request.addProperty("fld_opGOwindentdoorval",
						B1802_infos[23]);
				cf.request.addProperty("fld_opGOgardoorval", B1802_infos[24]);
				cf.request.addProperty("fld_opGOskylightsval", B1802_infos[25]);
				cf.request
						.addProperty("fld_opGOglassblockval", B1802_infos[26]);
				cf.request
						.addProperty("fld_opNGOentrydoorval", B1802_infos[27]);
				cf.request.addProperty("fld_opNGOgaragedoorval",
						B1802_infos[28]);

				if (B1802_infos[30].contains("&#40;")) {
					String[] s = B1802_infos[30].split("&#40;");
					cf.request.addProperty("fld_wcvalue",
							s[0].replace("&#94;", ",") + ",Other");
					cf.request.addProperty("fld_wcreinforcement", s[1]);
				} else {
					cf.request.addProperty("fld_wcvalue",
							B1802_infos[30].replace("&#94;", ","));
					cf.request.addProperty("fld_wcreinforcement", "");
				}

				cf.request.addProperty("fld_quesmodifydate", B1802_infos[31]);

				System.out.println("Questions=" + cf.request);
				sub_total = 15;
				EX_B1802[0] = cf.SoapResponse("WINDOVERALLQUESTIONS",
						cf.request);
				System.out.println("EX_B1802[0]=" + EX_B1802[0]);
				C_WND.close();
			} else {
				EX_B1802[0] = true;
			}

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("ctach in b1802" + e.getMessage());
			EX_B1802[0] = false;
			throw e;
		}
		sub_total = 25;
		return EX_B1802[0];
	}

	private boolean EX_B1802_RoofCover(int insptypeid) throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		sub_total = 25;
		EX_B1802[1] = false;
		try {
			findinspectionid(insptypeid);
			cf.CreateARRTable(33);
			cf.request = cf.SoapRequest("WINDROOFCOVERTYPE ");
			Cursor C_RC = cf.SelectTablefunction(cf.quesroofcover,
					" where SRID='" + cf.selectedhomeid + "' and INSP_TYPEID='"
							+ insptypeid + "'");
			if (C_RC.getCount() >= 1) {
				RC_infos = cf.setvalueToArray(C_RC);
				cf.request.addProperty("Fld_Srid", cf.selectedhomeid);
				cf.request.addProperty("InspectionID", inspectionid);

				cf.request.addProperty("RoofCoverType", RC_infos[3]);
				cf.request.addProperty("RoofCoverValue", RC_infos[4]);
				cf.request.addProperty("PermitApplnDate1", RC_infos[5]);
				cf.request.addProperty("PermitApplnDate2", RC_infos[6]);
				cf.request.addProperty("PermitApplnDate3", RC_infos[7]);
				cf.request.addProperty("PermitApplnDate4", RC_infos[8]);
				cf.request.addProperty("PermitApplnDate5", RC_infos[9]);
				cf.request.addProperty("PermitApplnDate6", RC_infos[10]);

				cf.request.addProperty("RoofCoverTypeOther", RC_infos[11]);

				cf.request.addProperty("ProdApproval1", RC_infos[12]);
				cf.request.addProperty("ProdApproval2", RC_infos[13]);
				cf.request.addProperty("ProdApproval3", RC_infos[14]);
				cf.request.addProperty("ProdApproval4", RC_infos[15]);
				cf.request.addProperty("ProdApproval5", RC_infos[16]);
				cf.request.addProperty("ProdApproval6", RC_infos[17]);

				cf.request.addProperty("InstallYear1", RC_infos[18]);
				cf.request.addProperty("InstallYear2", RC_infos[19]);
				cf.request.addProperty("InstallYear3", RC_infos[20]);
				cf.request.addProperty("InstallYear4", RC_infos[21]);
				cf.request.addProperty("InstallYear5", RC_infos[22]);
				cf.request.addProperty("InstallYear6", RC_infos[23]);

				cf.request.addProperty("NoInfo1", RC_infos[24]);
				cf.request.addProperty("NoInfo2", RC_infos[25]);
				cf.request.addProperty("NoInfo3", RC_infos[26]);
				cf.request.addProperty("NoInfo4", RC_infos[27]);
				cf.request.addProperty("NoInfo5", RC_infos[28]);
				cf.request.addProperty("NoInfo6", RC_infos[29]);

				cf.request.addProperty("RoofPreDominant", RC_infos[30]);

				System.out.println("Roofcover=" + cf.request);
				sub_total = 35;
				EX_B1802[1] = cf.SoapResponse("WINDROOFCOVERTYPE ", cf.request);
				System.out.println("EX_B1802[1]=" + EX_B1802[1]);
				C_RC.close();
			} else {
				EX_B1802[1] = true;
			}

		} catch (Exception e) {
			// TODO: handle exception
			EX_B1802[1] = false;
			throw e;
		}
		sub_total = 50;
		return EX_B1802[1];
	}

	private boolean EX_B1802_Comments(int insptypeid) throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		sub_total = 50;

		EX_B1802[2] = false;
		findinspectionid(insptypeid);
		
		try {
			cf.request = cf.SoapRequest("WINDQUESTIONCOMMETS");
			cf.CreateARRTable(32);
			Cursor C_Comm = cf.SelectTablefunction(cf.Wind_QuesCommtbl,
					" where fld_srid='" + cf.selectedhomeid
							+ "' and insp_typeid='" + insptypeid + "'");
			if (C_Comm.getCount() > 0) {
				Comm_infos = cf.setvalueToArray(C_Comm);
				cf.request.addProperty("Fld_Srid", cf.selectedhomeid);
				cf.request.addProperty("InspectionID", inspectionid);

				cf.request.addProperty("fld_buildcodecomments", Comm_infos[3]);
				cf.request.addProperty("fld_roofcovercomments", Comm_infos[4]);
				cf.request.addProperty("fld_roofdeckcomments", Comm_infos[5]);
				cf.request.addProperty("fld_roofwallcomments", Comm_infos[6]);
				cf.request.addProperty("fld_roofgeometrycomments",
						Comm_infos[7]);
				cf.request.addProperty("fld_swrcomments", Comm_infos[8]);
				System.out.println("Comm_infos[9]" + Comm_infos[9]);
				cf.request.addProperty("fld_openprotectioncomments",
						Comm_infos[9]);
				cf.request.addProperty("fld_wallconscomments", Comm_infos[10]);

				cf.request.addProperty("fld_insoverallcomments", "");
				System.out.println("comments " + cf.request);
				sub_total = 65;
				EX_B1802[2] = cf
						.SoapResponse("WINDQUESTIONCOMMETS", cf.request);
				System.out.println("EX_B1802[2]=" + EX_B1802[2]);
				C_Comm.close();
			} else {
				EX_B1802[2] = true;
			}

		} catch (Exception e) {
			// TODO: handle exception
			EX_B1802[2] = false;
			throw e;
		}
		sub_total = 75;
		return EX_B1802[2];
	}

	private boolean EX_B1802_WindowOpenings(int insptypeid)
			throws SocketException, NetworkErrorException, TimeoutException,
			IOException, XmlPullParserException, Exception,
			NullPointerException {
		sub_total = 0;
		sub_insp = "Window Openings";
		EX_B1802[3] = false;
		findinspectionid(insptypeid);
		cf.CreateARRTable(312);
		try {
			Cursor C_NA = cf.SelectTablefunction(cf.WindDoorSky_NA,
					" where fld_srid='" + cf.selectedhomeid
							+ "' and insp_typeid='" + insptypeid + "'");
			if (C_NA.getCount() > 0) {
				C_NA.moveToFirst();
				if (!C_NA.getString(C_NA.getColumnIndex("fld_windowopenNA"))
						.equals("")) {
					wind_NA = C_NA.getString(C_NA
							.getColumnIndex("fld_windowopenNA"));
				} else {
					wind_NA = "0";
				}
			} else {
				wind_NA = "0";
			}
			C_NA.close();
			cf.CreateARRTable(62);
			Cursor C_WIN = cf.SelectTablefunction(cf.BI_Windopenings,
					" where fld_srid='" + cf.selectedhomeid
							+ "' and insptypeid='" + insptypeid + "'");
			if (C_WIN.getCount() > 0) {
				int cc = 100 / C_WIN.getCount();
				C_WIN.moveToFirst();
				for (int i = 0; i < C_WIN.getCount(); i++) {
					cf.request = cf.SoapRequest("WINDWINDOWOPENINGS");
					cf.request.addProperty("Fld_Srid", cf.selectedhomeid);
					cf.request.addProperty("InspectionID", inspectionid);
					cf.request.addProperty("WindowOpeningChecked", 0);
					cf.request.addProperty("ElevationType", C_WIN.getString(3));
					cf.request.addProperty("ElevationOther",
							cf.decode(C_WIN.getString(5)));

					String Wind_open_split[] = cf.decode(C_WIN.getString(4))
							.split("&#94;");
					cf.request.addProperty("NoofQty", Wind_open_split[0]);
					System.out.println("Wind_open_split[1]"+Wind_open_split[1]);
					
					
					if(Wind_open_split[1].equals("Other"))
					{
						cf.request.addProperty("Window", Wind_open_split[1]+"("+cf.decode(C_WIN.getString(6))+")");	
					}
					else
					{
						cf.request.addProperty("Window", Wind_open_split[1]);	
					}
					
					cf.request.addProperty("Floor", Wind_open_split[2]);
					cf.request.addProperty("ProtectCode", Wind_open_split[3]);

					cf.request.addProperty("ROWID", i+1);
					System.out.println("windowopenings = " + cf.request);

					EX_B1802[3] = cf.SoapResponse("WINDWINDOWOPENINGS",
							cf.request);
					System.out.println("EX_B1802[3] = " + EX_B1802[3]);
					C_WIN.moveToNext();
					sub_total += cc;
				}
				C_WIN.close();
			} else {
				cf.request = cf.SoapRequest("WINDWINDOWOPENINGS");
				cf.request.addProperty("Fld_Srid", cf.selectedhomeid);
				cf.request.addProperty("InspectionID", inspectionid);
				cf.request.addProperty("WindowOpeningChecked", wind_NA);
				cf.request.addProperty("ElevationType", "");
				cf.request.addProperty("ElevationOther", "");
				cf.request.addProperty("NoofQty", 0);
				cf.request.addProperty("Window", "");
				cf.request.addProperty("Floor", "");
				cf.request.addProperty("ProtectCode", "");
				cf.request.addProperty("ROWID", 0);
				sub_total = 90;
				EX_B1802[3] = cf.SoapResponse("WINDWINDOWOPENINGS", cf.request);

				System.out.println("EX_B1802[3] else = " + EX_B1802[3]);
			}

		} catch (Exception e) {
			// TODO: handle exception
			EX_B1802[3] = false;
			throw e;
		}
		sub_total = 100;
		setincreament();
		;
		return EX_B1802[3];
	}

	private boolean EX_B1802_DoorOpenings(int insptypeid)
			throws SocketException, NetworkErrorException, TimeoutException,
			IOException, XmlPullParserException, Exception,
			NullPointerException {
		sub_total = 0;
		sub_insp = "Door Openings";
		EX_B1802[4] = false;
		cf.CreateARRTable(312);
		findinspectionid(insptypeid);
		try {
			Cursor C_NA = cf.SelectTablefunction(cf.WindDoorSky_NA,
					" where fld_srid='" + cf.selectedhomeid
							+ "' and insp_typeid='" + insptypeid + "'");
			if (C_NA.getCount() > 0) {
				C_NA.moveToFirst();
				if (!C_NA.getString(4).equals("")) {
					door_NA = C_NA.getString(4);
				} else {
					door_NA = "0";
				}
			} else {
				door_NA = "0";
			}
			C_NA.close();
			cf.CreateARRTable(63);
			Cursor C_Door = cf.SelectTablefunction(cf.BI_Dooropenings,
					" where fld_srid='" + cf.selectedhomeid
							+ "' and insptypeid='" + insptypeid + "'");
			System.out.println("C_Door" + C_Door.getCount());
			if (C_Door.getCount() > 0) {
				int cc = 100 / C_Door.getCount();
				C_Door.moveToFirst();
				for (int i = 0; i < C_Door.getCount(); i++) {
					cf.request = cf.SoapRequest("WINDDOOROPENING");
					cf.request.addProperty("Fld_Srid", cf.selectedhomeid);
					cf.request.addProperty("InspectionID", inspectionid);
					System.out.println("door1="+cf.request);
					cf.request
							.addProperty("ElevationType", C_Door.getString(3));
					cf.request.addProperty("ElevationOther",
							cf.decode(C_Door.getString(5)));
					cf.request.addProperty("DoorOpeningChecked", 0);

					System.out.println(cf.decode(C_Door.getString(4))+"Doorrequ");
					
					
					String dooropen_split[] = cf.decode(C_Door.getString(4))
							.split("&#39;");
					cf.request.addProperty("Noofqty", dooropen_split[0]);System.out.println("door2="+cf.request);
					
					
					String doortypesplit[] = dooropen_split[1].split("&#94;");
					if(doortypesplit[0].equals("Other"))
					{
						cf.request.addProperty("DoorType", doortypesplit[0]+"("+doortypesplit[1]+")");	
					}
					else
					{
						cf.request.addProperty("DoorType", doortypesplit[0]);	
					}
					
					cf.request.addProperty("Floor", dooropen_split[2]);
					cf.request.addProperty("ProtectCode", dooropen_split[3]);System.out.println("door3="+cf.request);

					String yrinstalled[] = dooropen_split[4].split("&#94;");

					if (yrinstalled[0].equals("Other")) {
						cf.request.addProperty("YearInst", yrinstalled[0]
								+ " (" + yrinstalled[1] + ")");
					} else {
						cf.request.addProperty("YearInst", yrinstalled[0]);
					}

					cf.request.addProperty("YearProvide", "");System.out.println("door4="+cf.request);
					if (dooropen_split[5].equals("1")) {
						cf.request.addProperty("Glazes",
								Integer.parseInt(dooropen_split[5]));
					} else {
						cf.request.addProperty("Glazes",
								Integer.parseInt(dooropen_split[5]));
					}System.out.println("door5="+cf.request);
					cf.request.addProperty("ROWID", i+1);
					System.out.println("dooropen=" + cf.request);

					EX_B1802[4] = cf
							.SoapResponse("WINDDOOROPENING", cf.request);
					System.out.println("EX_B1802[4]=" + EX_B1802[4]);
					C_Door.moveToNext();
					sub_total += cc;
				}
				C_Door.close();
			} else {
				cf.request = cf.SoapRequest("WINDDOOROPENING");
				cf.request.addProperty("Fld_Srid", cf.selectedhomeid);
				cf.request.addProperty("InspectionID", inspectionid);
				cf.request.addProperty("ElevationType", "");
				cf.request.addProperty("ElevationOther", "");
				cf.request.addProperty("DoorOpeningChecked", door_NA);
				cf.request.addProperty("Noofqty", 0);
				cf.request.addProperty("DoorType", "");
				cf.request.addProperty("Floor", "");
				cf.request.addProperty("ProtectCode", "");
				cf.request.addProperty("YearInst", "");
				cf.request.addProperty("YearProvide", "");
				cf.request.addProperty("Glazes", 0);
				cf.request.addProperty("ROWID", 0);
				System.out.println("dooropen else =" + cf.request);
				sub_total = 90;
				EX_B1802[4] = cf.SoapResponse("WINDDOOROPENING", cf.request);
			}
		} catch (Exception e) {
			// TODO: handle exception
			EX_B1802[4] = false;
			throw e;
		}
		sub_total = 100;
		setincreament();
		;
		return EX_B1802[4];
	}

	private boolean EX_B1802_Skylights(int insptypeid) throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		sub_total = 0;
		sub_insp = "Skylights";
		EX_B1802[5] = false;
		try {
			findinspectionid(insptypeid);
			cf.CreateARRTable(312);
			Cursor C_NA = cf.SelectTablefunction(cf.WindDoorSky_NA,
					" where fld_srid='" + cf.selectedhomeid
							+ "' and insp_typeid='" + insptypeid + "'");
			System.out.println("C_NA" + C_NA.getCount());
			if (C_NA.getCount() > 0) {
				C_NA.moveToFirst();
				if (!C_NA.getString(5).equals("")) {
					sky_NA = C_NA.getString(5);
				} else {
					sky_NA = "0";
				}
			} else {
				sky_NA = "0";
			}
			C_NA.close();

			cf.CreateARRTable(64);
			Cursor C_Sky = cf.SelectTablefunction(cf.BI_Skylights,
					" where fld_srid='" + cf.selectedhomeid
							+ "' and insptypeid='" + insptypeid + "'");
			System.out.println("C_Sky" + C_Sky.getCount());
			if (C_Sky.getCount() > 0) {
				int cc = 100 / C_Sky.getCount();
				C_Sky.moveToFirst();
				for (int i = 0; i < C_Sky.getCount(); i++) {
					cf.request = cf.SoapRequest("WINDSKYLIGHTS");
					cf.request.addProperty("Fld_Srid", cf.selectedhomeid);
					cf.request.addProperty("InspectionID", inspectionid);

					cf.request.addProperty("Skylightschecked", 0);
					cf.request.addProperty("ElevationType", C_Sky.getString(3));
					cf.request
							.addProperty("ElevationOther", cf.decode(C_Sky.getString(5)));

					String skylights_split[] = cf.decode(C_Sky.getString(4))
							.split("&#39;");
					cf.request.addProperty("NoofQty", skylights_split[0]);
					cf.request.addProperty("ProtectCode", skylights_split[1]);
					cf.request.addProperty("Daylightbuck", skylights_split[2]);

					cf.request.addProperty("ROWID", i+1);
					System.out.println("skylights=" + cf.request);

					EX_B1802[5] = cf.SoapResponse("WINDSKYLIGHTS", cf.request);
					System.out.println("EX_B1802[5]=" + EX_B1802[5]);
					C_Sky.moveToNext();
					sub_total += cc;

				}
				C_Sky.close();
			} else {
				cf.request = cf.SoapRequest("WINDSKYLIGHTS");
				cf.request.addProperty("Fld_Srid", cf.selectedhomeid);
				cf.request.addProperty("InspectionID", inspectionid);
				cf.request.addProperty("Skylightschecked", sky_NA);
				cf.request.addProperty("ElevationType", "");
				cf.request.addProperty("ElevationOther", "");
				cf.request.addProperty("NoofQty", 0);
				cf.request.addProperty("ProtectCode", "");
				cf.request.addProperty("Daylightbuck", "");
				cf.request.addProperty("ROWID", 0);

				System.out.println("skylights else =" + cf.request);
				sub_total = 90;
				EX_B1802[5] = cf.SoapResponse("WINDSKYLIGHTS", cf.request);
				System.out.println("EX_B1802[5] else=" + EX_B1802[5]);
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("exceee" + e.getMessage());
			EX_B1802[5] = false;
			throw e;
		}
		sub_total = 100;
		setincreament();
		;
		return EX_B1802[5];
	}

	private boolean EX_B1802_WallConstruction(int insptypeid)
			throws SocketException, NetworkErrorException, TimeoutException,
			IOException, XmlPullParserException, Exception,
			NullPointerException {

		sub_total = 75;
		EX_B1802[6] = false;
		try {
			findinspectionid(insptypeid);
			cf.CreateARRTable(31);
			Cursor C_WC = cf.SelectTablefunction(cf.Wind_Questiontbl,
					" where fld_srid='" + cf.selectedhomeid
							+ "' and insp_typeid='" + cf.identityval + "'");
			if (C_WC.getCount() > 0) {
				C_WC.moveToFirst();
				cf.request.addProperty("Reinforcement", C_WC.getString(C_WC
						.getColumnIndex("fld_wcreinforcement")));
			}
			C_WC.close();
			cf.CreateARRTable(34);
			Cursor C_Wall = cf.SelectTablefunction(cf.WindMit_WallCons,
					" where fld_srid='" + cf.selectedhomeid
							+ "' and insp_typeid='" + insptypeid + "'");
			if (C_Wall.getCount() > 0) {
				int cc = 25 / C_Wall.getCount();
				C_Wall.moveToFirst();

				for (int i = 0; i < C_Wall.getCount(); i++) {
					cf.request = cf.SoapRequest("WINDWALLCONSTRUCTION");
					cf.request.addProperty("Fld_Srid", cf.selectedhomeid);
					cf.request.addProperty("InspectionID", inspectionid);

					cf.request.addProperty("Typeid", C_Wall.getString(3));
					cf.request.addProperty("ElevationOther",
							cf.decode(C_Wall.getString(6)));
System.out.println("test"+C_Wall.getString(4));
					String percsplit[] = C_Wall.getString(5).split("~");
					//String Stories_Split[] = C_Wall.getString(4).split("\\~", -1);
					String Stories_Split[] = C_Wall.getString(4).split("~");
					
					
					System.out.println("ssss"+Stories_Split[0]);
					
					
					if (!Stories_Split[0].trim().equals("")) {
					cf.request.addProperty("Concreteblock", percsplit[0]+ "~" + Stories_Split[0]);
					}
					else
					{
						cf.request.addProperty("Concreteblock", percsplit[0]+ "~" +"");
					}
					if (!Stories_Split[1].trim().equals("")) 
					{
						cf.request.addProperty("Solid", percsplit[1]+ "~" + Stories_Split[1]);
					}
					else
					{
						cf.request.addProperty("Solid", percsplit[1]+ "~" + "");
					}
					if(!Stories_Split[2].trim().equals("")) {
					cf.request.addProperty("Woodlightframe", percsplit[2]+ "~" + Stories_Split[2]);
					}
					else
					{
						cf.request.addProperty("Woodlightframe", percsplit[2]+ "~" + "");
					}
					
					if (!Stories_Split[3].trim().equals("")) {
					cf.request.addProperty("UnReinMasonry", percsplit[3]+ "~" + Stories_Split[3]);
					}
					else
					{
						cf.request.addProperty("UnReinMasonry", percsplit[3]+ "~" + "");
					}
					
					if (!Stories_Split[4].trim().equals("")) {
					cf.request.addProperty("ReinMasonry", percsplit[4]+ "~" + Stories_Split[4]);
					}
					else
					{
						cf.request.addProperty("ReinMasonry", percsplit[4]+ "~" + "");
					}
					if (!Stories_Split[5].trim().equals("")) {
					cf.request.addProperty("PouredConcrete", percsplit[5]+ "~" + Stories_Split[5]);
					}
					else
					{
						cf.request.addProperty("PouredConcrete", percsplit[5]+ "~" + "");
					}
					if (!Stories_Split[6].trim().equals("")) {
					cf.request.addProperty("Other", percsplit[6]+ "~" + Stories_Split[6]);
					}
					else
					{
						cf.request.addProperty("Other", percsplit[6]+ "~" +"");
					}

					cf.request.addProperty("OtherText",
							cf.decode(C_Wall.getString(7)));
					// cf.request.addProperty("Stories",C_Wall.getString(4));

					cf.request.addProperty("ROWID", i+1);

					System.out.println("WallConstruction = " + cf.request);

					EX_B1802[6] = cf.SoapResponse("WINDWALLCONSTRUCTION",
							cf.request);
					System.out.println("EX_B1802[6]=" + EX_B1802[6]);

					C_Wall.moveToNext();
					sub_total += cc;
				}
				C_Wall.close();
			} else {
				cf.request = cf.SoapRequest("WINDWALLCONSTRUCTION");
				cf.request.addProperty("Fld_Srid", cf.selectedhomeid);
				cf.request.addProperty("InspectionID", inspectionid);
				cf.request.addProperty("Typeid", "");
				cf.request.addProperty("ElevationOther", "");
				cf.request.addProperty("Concreteblock", "");
				cf.request.addProperty("Solid", "");
				cf.request.addProperty("Woodlightframe", "");
				cf.request.addProperty("UnReinMasonry", "");
				cf.request.addProperty("ReinMasonry", "");
				cf.request.addProperty("PouredConcrete", "");
				cf.request.addProperty("Other", "");
				cf.request.addProperty("OtherText", "");
				cf.request.addProperty("ROWID", 0);

				System.out.println("skylights else =" + cf.request);
				sub_total = 90;
				EX_B1802[6] = cf.SoapResponse("WINDWALLCONSTRUCTION",
						cf.request);
				System.out.println("EX_B1802[6] else=" + EX_B1802[6]);
			}
		} catch (Exception e) {
			// TODO: handle exception
			EX_B1802[6] = false;
			throw e;
		}
		sub_total = 100;
		setincreament();
		;
		return EX_B1802[6];
	}

	private boolean EX_Comm_CommunalAreas(int insptypeid)
			throws SocketException, NetworkErrorException, TimeoutException,
			IOException, XmlPullParserException, Exception,
			NullPointerException {
		sub_total = 0;
		findinspectionid(insptypeid);
		EX_Comm1[0] = false;
		cf.CreateARRTable(311);
		cf.CreateARRTable(39);
		try {
			cf.request = cf.SoapRequest("CommercialCommunalAreas");
			Cursor C_Comm = cf.SelectTablefunction(cf.Communal_Areas,
					" where fld_srid='" + cf.selectedhomeid
							+ "' and insp_typeid='" + insptypeid + "'");
			if (C_Comm.getCount() >= 1) {
				CA_infos = cf.setvalueToArray(C_Comm);

				cf.request.addProperty("Fld_Srid", cf.selectedhomeid);
				cf.request.addProperty("InspectionID", inspectionid);

				if (CA_infos[3].equals("0")) {
					String lobbyval = cf.decode(C_Comm.getString(C_Comm
							.getColumnIndex("fld_lobby")));
					String lobbytxt_splt[] = lobbyval.split("&#39;");
					if (lobbytxt_splt[0].contains("&#40;")) {
						String[] s = lobbytxt_splt[0].split("&#40;");
						cf.request.addProperty("WallsValue",
								s[0].replace("&#94;", ",") + ",Other");
						cf.request.addProperty("WallsOthertext", s[1]);
					} else {
						cf.request.addProperty("WallsValue",
								lobbytxt_splt[0].replace("&#94;", ","));
						cf.request.addProperty("WallsOthertext", "");
					}
					if (lobbytxt_splt[1].contains("&#40;")) {
						String[] s = lobbytxt_splt[1].split("&#40;");
						cf.request.addProperty("FloorValue",
								s[0].replace("&#94;", ",") + ",Other");
						cf.request.addProperty("FloorOthertext", s[1]);
					} else {
						cf.request.addProperty("FloorValue",
								lobbytxt_splt[1].replace("&#94;", ","));
						cf.request.addProperty("FloorOthertext", "");
					}
					if (lobbytxt_splt[2].contains("&#40;")) {
						String[] s = lobbytxt_splt[2].split("&#40;");
						cf.request.addProperty("CeilingsValue",
								s[0].replace("&#94;", ",") + ",Other");
						cf.request.addProperty("CeilingsOthertext", s[1]);
					} else {
						cf.request.addProperty("CeilingsValue",
								lobbytxt_splt[2].replace("&#94;", ","));
						cf.request.addProperty("CeilingsOthertext", "");
					}
					if (lobbytxt_splt[3].contains("&#40;")) {
						String[] s3 = lobbytxt_splt[3].split("&#40;");
						cf.request.addProperty("DoorsValue",
								s3[0].replace("&#94;", ",") + ",Other(" + s3[1]
										+ ")");
					} else {
						cf.request.addProperty("DoorsValue",
								lobbytxt_splt[3].replace("&#94;", ","));
					}
					if (lobbytxt_splt[3].contains("&#40;")) {
						String[] s4 = lobbytxt_splt[4].split("&#40;");
						cf.request.addProperty("WindowsValue",
								s4[0].replace("&#94;", ",") + ",Other(" + s4[1]
										+ ")");					} else {
						cf.request.addProperty("WindowsValue",
								lobbytxt_splt[4].replace("&#94;", ","));
					}
				} else {
					cf.request.addProperty("WallsValue", "");
					cf.request.addProperty("WallsOthertext", "");
					cf.request.addProperty("FloorValue", "");
					cf.request.addProperty("FloorOthertext", "");
					cf.request.addProperty("CeilingsValue", "");
					cf.request.addProperty("CeilingsOthertext", "");
					cf.request.addProperty("DoorsValue", "");
					cf.request.addProperty("WindowsValue", "");
				}

				if (CA_infos[5].equals("0")) {
					String stairwells_val = cf.decode(C_Comm.getString(C_Comm
							.getColumnIndex("fld_stairwells")));System.out.println("stairwells_val="+stairwells_val);
					String stairwellstxt_splt[] = stairwells_val.split("&#39;");
					if (stairwellstxt_splt[0].contains("&#40;")) {
						String[] s = stairwellstxt_splt[0].split("&#40;");
						cf.request.addProperty("StairConst",
								s[0].replace("&#94;", ",") + ",Other");
						cf.request.addProperty("StairConstOther", s[1]);
					} else {
						cf.request.addProperty("StairConst",
								stairwellstxt_splt[0].replace("&#94;", ","));
						cf.request.addProperty("StairConstOther", "");
					}
					if (stairwellstxt_splt[1].contains("&#40;")) {
						String[] s = stairwellstxt_splt[1].split("&#40;");
						cf.request.addProperty("FloorType",
								s[0].replace("&#94;", ",") + ",Other");
						cf.request.addProperty("FloorTypeOthertext", s[1]);
					} else {
						cf.request.addProperty("FloorType",
								stairwellstxt_splt[1].replace("&#94;", ","));
						cf.request.addProperty("FloorTypeOthertext", "");
					}

					cf.request.addProperty("MetalRailingsvalue",
							stairwellstxt_splt[2]);
					if (stairwellstxt_splt[2].equals("Yes")) {
						if (stairwellstxt_splt[3].contains("&#40;")) {
							String[] s = stairwellstxt_splt[3].split("&#40;");
							cf.request.addProperty("RailingValue",
									s[0].replace("&#94;", ",") + ",Other");
							cf.request.addProperty("RailingValueOther", s[1]);
						} else {
							cf.request
									.addProperty("RailingValue",
											stairwellstxt_splt[3].replace(
													"&#94;", ","));
							cf.request.addProperty("RailingValueOther", "");
						}
					} else {
						cf.request.addProperty("RailingValue", "");
						cf.request.addProperty("RailingValueOther", "");
					}

					cf.request.addProperty("StairWellsValue",
							stairwellstxt_splt[4].replace("&#94;", ","));

					if (stairwellstxt_splt[5].equals("Yes")) {
						cf.request.addProperty("AllFloorsValue", true);
					} else if (stairwellstxt_splt[5].equals("No")) {
						cf.request.addProperty("AllFloorsValue", false);
					}
				} else {
					cf.request.addProperty("StairConst", "");
					cf.request.addProperty("StairConstOther", "");
					cf.request.addProperty("FloorType", "");
					cf.request.addProperty("FloorTypeOthertext", "");
					cf.request.addProperty("MetalRailingsvalue", "");
					cf.request.addProperty("RailingValue", "");
					cf.request.addProperty("RailingValueOther", "");
					cf.request.addProperty("StairWellsValue", "");
					cf.request.addProperty("AllFloorsValue", false);
				}
				cf.request.addProperty("ServingAllFloorValue", false);
				cf.request.addProperty("DateofLastInspected", "");

				if (CA_infos[7].equals("0")) {
					cf.request.addProperty("ElevatorComments", CA_infos[8]);
				} else {
					cf.request.addProperty("ElevatorComments", "");
				}
				if (CA_infos[9].equals("0")) {
					String poolareas_split[] = CA_infos[11].split("\\^");
					String Pool_area_include[] = poolareas_split[0].split("~");
					String Firness_area_include[] = poolareas_split[1]
							.split("~");
					String Common_area_include[] = poolareas_split[2]
							.split("~");
					String Other_area_include[] = poolareas_split[3].split("~");

					cf.request.addProperty("PoolAreas", Pool_area_include[1]
							.equals("--Select--") ? "" : Pool_area_include[1]);
					cf.request.addProperty("FitnessAreas",
							Firness_area_include[1].equals("--Select--") ? ""
									: Firness_area_include[1]);
					cf.request.addProperty("Commbathroom",
							Common_area_include[1].equals("--Select--") ? ""
									: Common_area_include[1]);
					cf.request.addProperty("OtherText", CA_infos[10]);
					cf.request.addProperty("OtherTextValue",
							(Other_area_include[1].equals("--Select--")) ? ""
									: Other_area_include[1]);

				} else {
					cf.request.addProperty("PoolAreas", "");
					cf.request.addProperty("FitnessAreas", "");
					cf.request.addProperty("Commbathroom", "");
					cf.request.addProperty("OtherText", "");
					cf.request.addProperty("OtherTextValue", "");
				}

				cf.CreateARRTable(36);
				Cursor c2 = cf.SelectTablefunction(cf.Wind_Commercial_Comm,
						" where fld_srid='" + cf.selectedhomeid
								+ "' and insp_typeid='" + insptypeid + "'");
				if (c2.getCount() > 0) {
					c2.moveToFirst();
					cf.request
							.addProperty(
									"CommAreaComments",
									cf.decode(c2.getString(c2
											.getColumnIndex("fld_commercialareascomments"))));
				} else {
					cf.request.addProperty("CommAreaComments", "");
				}
				cf.request.addProperty("MainLobbyChecked", CA_infos[3]);
				cf.request.addProperty("StairWellsChecked", CA_infos[5]);
				cf.request.addProperty("ElevatorChecked", CA_infos[7]);
				cf.request.addProperty("OtherAreaChecked", CA_infos[9]);

				System.out.println("COMMUNALA REAS=" + cf.request);
				sub_total = 40;

				EX_Comm1[0] = cf.SoapResponse("CommercialCommunalAreas",
						cf.request);
				System.out.println("EX_Comm1[0]=" + EX_Comm1[0]);
				C_Comm.close();
			} else {
				EX_Comm1[0] = true;
			}

		} catch (Exception e) {
			// TODO: handle exception
			EX_Comm1[0] = false;
			throw e;
		}
		sub_total = 10;
		return EX_Comm1[0];
	}

	private boolean EX_Comm_CommunalAreasElevators(int insptypeid)
			throws SocketException, NetworkErrorException, TimeoutException,
			IOException, XmlPullParserException, Exception,
			NullPointerException {
		sub_total = 10;
		EX_Comm1[1] = false;
		findinspectionid(insptypeid);
		cf.CreateARRTable(311);
		try {
			String result_val = "";
			Cursor C_CEle = cf.SelectTablefunction(cf.Communal_AreasElevation,
					" where fld_srid='" + cf.selectedhomeid
							+ "' and insp_typeid='" + insptypeid + "'");
			System.out.println("C_CEle" + C_CEle.getCount());
			if (C_CEle.getCount() > 0) {
				current_val_cur = C_CEle;
				C_CEle.moveToFirst();				
				int cc = 50 / C_CEle.getCount();
				for (int i = 0; i < C_CEle.getCount(); i++, C_CEle.moveToNext()) {
					current_pos = i;					

					String path = cf.decode(C_CEle.getString(7));
					String name = path.substring(path.lastIndexOf("/") + 1);
					try {
							cf.request = cf
									.SoapRequest("CommunalAreasElevators");

							cf.request.addProperty("Fld_Srid",
									cf.selectedhomeid);
							cf.request
									.addProperty("InspectionID", inspectionid);
							cf.request.addProperty("fld_elevationno",
									C_CEle.getString(3));
							cf.request.addProperty("fld_model",
									cf.decode(C_CEle.getString(4)));
							cf.request.addProperty("fld_servingallfloors",
									C_CEle.getString(5));
							cf.request.addProperty("fld_datelastinsp",
									C_CEle.getString(6));
							cf.request.addProperty("fld_uploadimg", "");
							cf.request.addProperty("imgname", name);
							cf.request.addProperty("fld_manufacturer",
									cf.decode(C_CEle.getString(8)));
							cf.request.addProperty("ROWID", i+1);						
							
							System.out.println("request" + cf.request);
							cf.envelope.setOutputSoapObject(cf.request);
							
						} catch (Exception e) {
							// TODO: handle exception
							Error_tracker += "You have problem in exporting Communal areas dynamic elevators in "
									+ insptypeid + "  Path=" + path + " @";
						}

						///if (c) {
							HttpTransportSE androidHttpTransport = new HttpTransportSE(
									cf.URL_ARR);
							androidHttpTransport.call(cf.NAMESPACE
									+ "CommunalAreasElevators", cf.envelope);
							System.out.println("REOPSNE"
									+ cf.envelope.getResponse().toString());							
							
							String result= cf.envelope.getResponse().toString();
							
							cf.request = null;
							String insp_online="";
							
							System.out.println("insos"+insptypeid);
							if (insptypeid == 32)
							{
								insp_online = "9";
							}
							else if (insptypeid == 33){
								insp_online = "62";
								
							}else if (insptypeid == 34){
								insp_online = "90";
							}
							System.out.println("res="+result);
							
							send_elev_image(cf.selectedhomeid,C_CEle.getString(0),result, insptypeid, insp_online);
							
							
							sub_total += C_CEle.getCount();	
				}	
			} 
			else
			{			
				sub_total = 90;
				EX_Comm1[1] = true;
			}

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("communal araes=" + e.getMessage());
			EX_Comm1[1] = false;
			throw e;
		}
		System.out.println("result_elec"+result_elev);
		sub_total = 50;
		if (result_elev.equals("")) {
			EX_Comm1[1]= true;
		} else if (result_elev.contains("false")) {
			EX_Comm1[1]= false;
		} else {
			EX_Comm1[1] =true;
		}

		
		System.out.println("CommunalAreasElevators outide" + EX_Comm1[1]);
		
		return EX_Comm1[1];
	}
	private void send_elev_image(String selectedhomeid, String masterid,String elevatorno,
			int insp, String insp_online) throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException {
		
		/*** Start uploading the roof cover image section ***/
		cf.CreateARRTable(436);
		/*Cursor image = cf.arr_db.rawQuery("Select * from " + cf.Elevatorimages
				+ " Where Elev_masterid=(Select ELev_masterid from "
				+ cf.Elev_master + " Where Elev_srid='" + cf.selectedhomeid + "' and Elev_insp_id='"
				+ insp + "') order by Elev_Order", null);		
		System.out.println("Select * from " + cf.Elevatorimages
				+ " Where Elev_masterid=(Select ELev_masterid from "
				+ cf.Elev_master + " Where Elev_srid='" + cf.selectedhomeid + "' and Elev_insp_id='"
				+ insp + "' ) order by Elev_Order"+"what is count="+image.getCount());*/

		Cursor image = cf.arr_db.rawQuery("Select * from " + cf.Elevatorimages
				+ " Where Elev_masterid='"+masterid+"' order by Elev_Order", null);
		System.out.println("imagecount="+image.getCount()+"Select * from " + cf.Elevatorimages
				+ " Where Elev_masterid='"+masterid+"' order by Elev_Order");
		image.moveToFirst();

		if (image.getCount() > 0) {
			int roof_inc = 10 / image.getCount();
			for (int im = 0; im < image.getCount(); im++, image.moveToNext()) {
				cf.request = cf.SoapRequest("ELEVATORPHOTOSECTION");
				try {
					if (new File(cf.decode(image.getString(image
							.getColumnIndex("Elev_Path")))).exists()) {
						byte[] raw = null;
						Bitmap bitmap = cf.ShrinkBitmap(cf.decode(image
								.getString(image.getColumnIndex("Elev_Path"))),
								400, 400);
						MarshalBase64 marshal = new MarshalBase64();
						marshal.register(cf.envelope);
						ByteArrayOutputStream out = new ByteArrayOutputStream();
						bitmap.compress(CompressFormat.PNG, 100, out);
						raw = out.toByteArray();
						cf.request.addProperty("image", raw);
					} else {
						Error_tracker += "Please check the following Image is available in "
								+ cf.All_insp_list[insp]
								+ ", Elevation Image ="+ elevatorno
								+ ", image order = "
								+ image.getInt(image
										.getColumnIndex("Elev_Order"))
								+ " Path="
								+ cf.decode(image.getString(image
										.getColumnIndex("Elev_Path"))) + " @";
						result_elev += "~false";
					}
				} catch (Exception e) {
					// TODO: handle exception
					System.out.println("comes in the catch ");
					Error_tracker += "Please check the following Image is available in "
							+ cf.All_insp_list[insp]
							+ ", Elevator No ="
							+ elevatorno
							+ ", image order = "
							+ image.getInt(image.getColumnIndex("Elev_Order"))
							+ " Path="
							+ cf.decode(image.getString(image
									.getColumnIndex("Elev_Path"))) + " @";
					cf.request.addProperty("image", "");
				}
				String name = cf.decode(image.getString(image
						.getColumnIndex("Elev_Path")));
				name = name.substring(name.lastIndexOf("/") + 1, name.length());
				cf.request.addProperty("Fld_Srid", cf.selectedhomeid);
				cf.request.addProperty("InspectionID", insp_online);
				cf.request.addProperty("ElevationTypeid", 28);// Default for the Roof cove image 
				
			//	cf.request.addProperty("Coveringtype", "Commmual Area Elevator");
				cf.request.addProperty("ImageName", name);
				cf.request.addProperty("Description", cf.decode(image
						.getString(image.getColumnIndex("Elev_Caption"))));
				cf.request.addProperty("Order",
						image.getInt(image.getColumnIndex("Elev_Order")));
				cf.request.addProperty("ROWID",
						image.getInt(image.getColumnIndex("Elev_Order")));
				
				cf.request.addProperty("PrimaryID",Integer.parseInt(elevatorno));
				//cf.request.addProperty("Flag", "");
			//	System.out.println(" the request"+cf.request);
				cf.envelope.setOutputSoapObject(cf.request);
				MarshalBase64 marshal = new MarshalBase64();
				marshal.register(cf.envelope);

				cf.envelope.setOutputSoapObject(cf.request);

				System.out.println("Elevator Image=" + elevatorno + "="	+ cf.request);

				HttpTransportSE androidHttpTransport1 = new HttpTransportSE(
						cf.URL_ARR);
				androidHttpTransport1.call(cf.NAMESPACE + "ELEVATORPHOTOSECTION",
						cf.envelope);
				result_elev += cf.envelope.getResponse().toString() + "~";
				System.out.println("Elevator image  " + im + result_elev);
				cf.request = null;
				sub_total += roof_inc;

			}
		}
		/*** uploading the roof cover image section ends ***/
		// TODO Auto-generated method stub
	
	}
	private boolean EX_Comm_CommWind(int insptypeid) throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		EX_Comm2[0] = false;
		String option = "";
		try {
			cf.CreateARRTable(40);
			findinspectionid(insptypeid);
			cf.request = cf.SoapRequest("COMMERCIALWIND");
			Cursor C_CW = cf.SelectTablefunction(cf.Commercial_Wind,
					" where fld_srid='" + cf.selectedhomeid
							+ "' and insp_typeid='" + insptypeid + "'");System.out.println("test comm wind"+C_CW.getCount());
			if (C_CW.getCount() > 0) {
				C_CW.moveToFirst();
				cf.request.addProperty("Fld_Srid", cf.selectedhomeid);
				cf.request.addProperty("InspectionID", inspectionid);System.out.println("test sdsd wind"+C_CW.getCount());
				cf.request.addProperty("TerrainExposureNA", 0);System.out.println("test ctteomm wind"+C_CW.getString(3));

				if(!C_CW.getString(3).equals(""))
				{
					String terrainaplit[] = C_CW.getString(3).split("~");
					cf.request.addProperty("TerrainExposureCat", terrainaplit[0]);
					cf.request.addProperty("CertificationofWindNA", 0);
					cf.request.addProperty("CertofWindSpeed", terrainaplit[1]);
					cf.request.addProperty("CertofWindDesign", terrainaplit[2]);
				}
				else
				{
					cf.request.addProperty("TerrainExposureCat", "");
					cf.request.addProperty("CertificationofWindNA", 0);
					cf.request.addProperty("CertofWindSpeed", "");
					cf.request.addProperty("CertofWindDesign", "");
				}
				
				
				cf.request.addProperty("RoofCoveringsNA", 0);System.out.println("req comm wind"+cf.request);
		
				String roofcovervalue =  cf.decode(C_CW.getString(4));System.out.println("roof"+roofcovervalue);
				String rcsplit[] = roofcovervalue.split("~");System.out.println("roofsplit"+rcsplit[0]);
				System.out.println("RoofCoveringOption"+rcsplit[0]);
				cf.request.addProperty("RoofCoveringOption", rcsplit[0]);
				if (rcsplit[0].equals("B")) {
					System.out.println("inside B");
					if (rcsplit[1].contains("&#40;")) {
						System.out.println("insidecontains 40B");
						String[] s = rcsplit[1].split("&#40;");
						s[0] = s[0].replace("&#94;", ",");
						
						System.out.println("insS22"+s[0]);
						System.out.println("insiS!"+s[1]);
						cf.request.addProperty("RoofCoveringLevel", s[0] + ",Other("+ s[1] + ")");
					} else {
						rcsplit[0] = rcsplit[0].replace("&#94;",",");
						cf.request.addProperty("RoofCoveringLevel", rcsplit[1]);
					}		
					
				} else {
					cf.request.addProperty("RoofCoveringLevel", "");
				}
				cf.request.addProperty("RoofCoveringComments",
						cf.decode(C_CW.getString(9)));

				cf.request.addProperty("RoofDeckAttachNA", 0);
				String rdsplit[] = C_CW.getString(5).split("~");
				if (rdsplit[0].equals("A")) {
					cf.request.addProperty("RoofDeckAttachOption", rdsplit[0]
							+ "," + rdsplit[1]);
				} else {
					cf.request.addProperty("RoofDeckAttachOption", rdsplit[0]);
				}
				cf.request.addProperty("RoofDeckAttachComments",
						cf.decode(C_CW.getString(10)));

				cf.request.addProperty("SWRNA", 0);
				cf.request.addProperty("SWROption", C_CW.getString(6));
				cf.request.addProperty("CommSWRComments",
						cf.decode(C_CW.getString(11)));

				cf.request.addProperty("OpenProtectionNA", 0);
				String opsplit[] = C_CW.getString(7).split("~");
				if (opsplit[0].equals("A") || opsplit[0].equals("B")) {
					cf.request.addProperty("OpenProtectionOption", opsplit[0]
							+ "(" + opsplit[1].replace("&#94;", ",") + ")");
				} else {
					cf.request.addProperty("OpenProtectionOption", opsplit[0]);
				}

				cf.request.addProperty("OpenProtectComments",
						cf.decode(C_CW.getString(12)));
				cf.request.addProperty("CertificationNA", 0);
				cf.request
						.addProperty("CertificationOption", C_CW.getString(8));
				System.out.println("COMMERCIALWIND = " + cf.request);
				EX_Comm2[0] = cf.SoapResponse("COMMERCIALWIND", cf.request);

				System.out.println("EX_Comm2[8]=" + EX_Comm2[0]);
				C_CW.close();
			} else {
				EX_Comm2[0] = true;
			}
		} catch (Exception e) {
			// TODO: handle exception
			EX_Comm2[0] = false;
			throw e;
		}
		return EX_Comm2[0];
	}

	private boolean EX_Comm_Aux(int insptypeid) throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		sub_total = 0;
		sub_insp="Auxiliary Buildings";
		EX_Comm2[1] = false;
		findinspectionid(insptypeid);
		cf.CreateARRTable(355);
		cf.CreateARRTable(35);
		try {
			Cursor C_NA = cf.SelectTablefunction(cf.Comm_Aux_Master,
					" where fld_srid='" + cf.selectedhomeid
							+ "' and insp_typeid='" + insptypeid + "'");
			if (C_NA.getCount() > 0) {
				C_NA.moveToFirst();
				aux_NA = C_NA.getString(C_NA.getColumnIndex("fld_aux_NA"));

			}
			C_NA.close();

			Cursor C_AUX = cf.arr_db.rawQuery("SELECT * FROM "
					+ cf.Comm_AuxBuilding + " WHERE fld_srid='"
					+ cf.selectedhomeid + "' and insp_typeid='" + insptypeid
					+ "'", null);
			if (C_AUX.getCount() > 0) {
				C_AUX.moveToFirst();
				int cc = 60 / C_AUX.getCount();
				System.out.println("moverto");
				for (int i = 0; i < C_AUX.getCount(); i++) {
					cf.request = cf.SoapRequest("AuxiliaryBuildings");
					cf.request.addProperty("Fld_Srid", cf.selectedhomeid);

					cf.request.addProperty("InspectionID", inspectionid);
					cf.request.addProperty("Auxilliarybuilding", cf
							.decode(C_AUX.getString(C_AUX
									.getColumnIndex("fld_aux_Building_type"))));
					cf.request
							.addProperty(
									"AuxilliaryOthers",
									cf.decode(C_AUX.getString(C_AUX
											.getColumnIndex("fld_aux_Building_other"))));

					System.out.println("size=="+cf.decode(C_AUX.getString(C_AUX.getColumnIndex("fld_aux_Ins_Size"))));
					
					
					String aux_Ins_Size[] = cf.decode(
							C_AUX.getString(C_AUX
									.getColumnIndex("fld_aux_Ins_Size")))
							.split("&#94;");
					if (!"".equals(aux_Ins_Size[0])) {
						String aux_Ins_Split[] = aux_Ins_Size[0].split("&#39;");
						if (aux_Ins_Split[0].equals("Yes")) {
							cf.request
									.addProperty(
											"InsurancePolicy",
											aux_Ins_Split[0]
													+ "("
													+ cf.decode(aux_Ins_Split[1])
													+ ")");
						} else {
							cf.request.addProperty("InsurancePolicy",
									aux_Ins_Split[0]);
						}
					}

					String aux_Size_Split[] = aux_Ins_Size[1].split("&#39;");

					cf.request.addProperty("Size", aux_Size_Split[1]);
					cf.request.addProperty("Type", aux_Size_Split[0]);

					String aux_Cons_RC_3yrs[] = cf
							.decode(C_AUX.getString(C_AUX
									.getColumnIndex("fld_aux_Cons__RC_3Years")))
							.split("&#94;");
					String RCT_Split[] = aux_Cons_RC_3yrs[1].split("&#39;");

					cf.request.addProperty("Constructiontype",
							aux_Cons_RC_3yrs[0]);
					cf.request.addProperty("RoofCoverType", RCT_Split[0]);

					if (RCT_Split[0].equals("Other")) {
						cf.request.addProperty("RoofCoverOther", RCT_Split[1]);
					} else {
						cf.request.addProperty("RoofCoverOther", "");
					}
					System.out.println("request" + cf.request);
					String WS_RC_OGN_DD[] = cf.decode(
							C_AUX.getString(C_AUX
									.getColumnIndex("fld_aux_WS_RC_OGN_DD")))
							.split("&#94;");
					String WC_Split[] = WS_RC_OGN_DD[0].split("&#39;");
					String RC_Split[] = WS_RC_OGN_DD[1].split("&#39;");
					String OGN_Split[] = WS_RC_OGN_DD[2].split("&#39;");
					String Def_Split[] = WS_RC_OGN_DD[3].split("&#39;");
					String Dam_Split[] = WS_RC_OGN_DD[4].split("&#39;");

					cf.request.addProperty("Wallstructure", WC_Split[0]);

					if (RC_Split[0].equals("Poor")) {
						cf.request.addProperty("Roofcovering", RC_Split[0]
								+ "(" + cf.decode(RC_Split[1]) + ")");
					} else {
						cf.request.addProperty("Roofcovering", RC_Split[0]);
					}

					if (OGN_Split[0].equals("Poor")) {
						cf.request.addProperty("OverallGencondition",
								OGN_Split[0] + "(" + cf.decode(OGN_Split[1])
										+ ")");
					} else {
						cf.request.addProperty("OverallGencondition",
								OGN_Split[0]);
					}

					if (Def_Split[0].equals("Yes")) {
						cf.request.addProperty("DefectNoted", Def_Split[0]
								+ "(" + cf.decode(Def_Split[1]) + ")");
					} else {
						cf.request.addProperty("DefectNoted", Def_Split[0]);
					}

					if (Dam_Split[0].equals("Yes")) {
						cf.request.addProperty("DemageNoted", Dam_Split[0]
								+ "(" + cf.decode(Dam_Split[1]) + ")");
					} else {
						cf.request.addProperty("DemageNoted", Dam_Split[0]);
					}

					cf.request.addProperty("ThreeYrRepProb",
							aux_Cons_RC_3yrs[2]);
					cf.request.addProperty("Comments", "tesr");
					cf.request.addProperty("ROWID", i+1);

					System.out.println("AuxiliaryBuildings=" + cf.request);

					EX_Comm2[1] = cf.SoapResponse("AuxiliaryBuildings",
							cf.request);
					System.out.println("EX_Comm2[1]=" + EX_Comm2[1]);

					C_AUX.moveToNext();
					sub_total += cc;
				}
				C_AUX.close();
			} else {
				
				  cf.request=cf.SoapRequest("AuxiliaryBuildings");
				  cf.request.addProperty("Fld_Srid",cf.selectedhomeid);
				  cf.request.addProperty("InspectionID",inspectionid);
				  cf.request.addProperty("Auxilliarybuilding","");
				  cf.request.addProperty("AuxilliaryOthers","");
				  cf.request.addProperty("InsurancePolicy","");
				  cf.request.addProperty("Size",0);
				  cf.request.addProperty("Type","");
				  cf.request.addProperty("Constructiontype","");
				  cf.request.addProperty("RoofCoverType","");
				  cf.request.addProperty("RoofCoverOther","");
				  cf.request.addProperty("Wallstructure","");
				  cf.request.addProperty("Roofcovering","");
				  cf.request.addProperty("OverallGencondition","");
				  cf.request.addProperty("DefectNoted","");
				  cf.request.addProperty("DemageNoted","");
				  cf.request.addProperty("ThreeYrRepProb", "");
				  cf.request.addProperty("ROWID",0);
				 System.out.println("aux="+cf.request);
				 EX_Comm2[1] = cf.SoapResponse("AuxiliaryBuildings", cf.request);
				 
				sub_total = 60;
				EX_Comm2[1] = true;
				System.out.println("EX_Comm2[1]" + EX_Comm2[1]);
			}
		} catch (Exception e) {
			System.out.println("catch inside" + e.getMessage());
			EX_Comm2[1] = false;
			throw e;
		}
		sub_total = 75;
		return EX_Comm2[1];
	}

	private boolean EX_Comm_Appl(int insptypeid) throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		sub_total = 0;
		sub_insp = "Restaurant Supplement";
		EX_Comm2[2] = false;
		String result="";
		try {
			findinspectionid(insptypeid);
			cf.CreateARRTable(37);
			
			Cursor C_APP = cf.arr_db.rawQuery("SELECT * FROM "
					+ cf.Comm_RestSuppAppl + " WHERE fld_srid='"
					+ cf.selectedhomeid + "' and insp_typeid='" + insptypeid
					+ "'", null);
			System.out.println("C_APP" + C_APP.getCount());
			if (C_APP.getCount() > 0) {
				C_APP.moveToFirst();
				int cc = 60 / C_APP.getCount();
				for (int i = 0; i < C_APP.getCount(); i++) {
					cf.request = cf.SoapRequest("RESTAURANTSUPPLEMENTAPPLIANCES");

					cf.request.addProperty("Fld_Srid", cf.selectedhomeid);
					cf.request.addProperty("InspectionID", inspectionid);
					cf.request.addProperty("Appliances", cf.decode(C_APP.getString(C_APP.getColumnIndex("fld_appliancetype"))));
					cf.request.addProperty("AppliancesOther", cf.decode(C_APP.getString(C_APP.getColumnIndex("fld_applianceother"))));

					String restsuppdataarr[] = cf.decode(C_APP.getString(C_APP.getColumnIndex("fld_appldata"))).split("&#94;");
					String typefuelspliarr[] = restsuppdataarr[0].split("&#39;");
					
					if(restsuppdataarr[0].contains("Other"))
					{
						
						cf.request.addProperty("Typeoffuel", typefuelspliarr[0]+"("+typefuelspliarr[1]+")");	
					}
					else
					{
						cf.request.addProperty("Typeoffuel", typefuelspliarr[0]);
					}
					
					
					
					cf.request.addProperty("Numbers", restsuppdataarr[1]);
					cf.request.addProperty("Noofburners", restsuppdataarr[2]);
					
					String  voltspliarr[] = restsuppdataarr[3].split("&#39;");
					if (restsuppdataarr[3].contains("Other"))
					{
							cf.request.addProperty("NoofVolts", "Other");
							cf.request.addProperty("NoofVoltsother", voltspliarr[1]);							
					}
					else
					{
						cf.request.addProperty("NoofVolts", voltspliarr[0]);
						cf.request.addProperty("NoofVoltsother", "");
					}
					
					if(inspectionid==62 || inspectionid==90)
					{
						if(restsuppdataarr[4].equals("N/A"))
						{
							cf.request.addProperty("NoofDoors",10);	
						}
						else
						{
							int s = Integer.parseInt(restsuppdataarr[4]);
							int k= s-2;System.out.println("K="+k);
							cf.request.addProperty("NoofDoors", k);	
						}
					}
					else
					{
						cf.request.addProperty("NoofDoors", restsuppdataarr[4]);	
					}
					

					cf.request.addProperty("Domestic", restsuppdataarr[5]);
					cf.request.addProperty("Commercial", restsuppdataarr[6]);
					cf.request.addProperty("Underhoodwithfilters",
							restsuppdataarr[7]);
					cf.request.addProperty("SurfaceProtection",
							restsuppdataarr[8]);

					
					String age_split[] = restsuppdataarr[9].split("&#39;");
					if (age_split[0].equals("Other")) {
						cf.request.addProperty("Age", age_split[0] + "("
								+ age_split[1] + ")");
					} else {
						cf.request.addProperty("Age", age_split[0]);
					}
					cf.request.addProperty("AgeType", age_split[2]);
					
					cf.request.addProperty("ROWID", i+1);

					System.out.println("RESTAURANTSUPPLEMENTAPPLIANCES="
							+ cf.request);

					result += cf.SoapResponse(
							"RESTAURANTSUPPLEMENTAPPLIANCES", cf.request)+"~";
					System.out.println("RESTAURANTSUPPLEMENTAPPLIANCES="+result);

					C_APP.moveToNext();
					sub_total += cc;
				}
				C_APP.close();
			} else {
				cf.request = cf.SoapRequest("RESTAURANTSUPPLEMENTAPPLIANCES");
				cf.request.addProperty("Fld_Srid", cf.selectedhomeid);
				cf.request.addProperty("InspectionID", inspectionid);
				cf.request.addProperty("Appliances", "");
				cf.request.addProperty("AppliancesOther", "");
				cf.request.addProperty("Numbers", 0);
				cf.request.addProperty("Typeoffuel", "");
				cf.request.addProperty("Domestic", "");
				cf.request.addProperty("Commercial", "");
				cf.request.addProperty("Noofburners", 0);
				cf.request.addProperty("NoofVolts", 0);
				cf.request.addProperty("NoofVoltsother", "");				
				cf.request.addProperty("NoofDoors", 0);
				cf.request.addProperty("Underhoodwithfilters", "");
				cf.request.addProperty("SurfaceProtection", "");
				cf.request.addProperty("Age", "");
				cf.request.addProperty("AgeType", "");
				cf.request.addProperty("ROWID", 0);
				sub_total = 60;
				result = cf.SoapResponse("RESTAURANTSUPPLEMENTAPPLIANCES", cf.request)+"~";
				
			}
			if(result.contains("false"))
			{
				EX_Comm2[2]=false;
				//return false;
			}
			else
			{
				EX_Comm2[2]=true;
				//return true;
			}
		} catch (Exception e) {
			EX_Comm2[2] = false;
			throw e;
		}
		
		sub_total = 75;
		return EX_Comm2[2];
	}
	private boolean EX_Comments(int insptypeid) throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		sub_total = 75;
		EX_Comm2[3] = false;
		try {
			cf.CreateARRTable(36);
			findinspectionid(insptypeid);
			Cursor C_COM = cf.arr_db.rawQuery("SELECT * FROM "
					+ cf.Wind_Commercial_Comm + " WHERE fld_srid='"
					+ cf.selectedhomeid + "' and insp_typeid='" + insptypeid
					+ "'", null);
			if (C_COM.getCount() > 0) {
				C_COM.moveToFirst();
				cf.request = cf.SoapRequest("COMMERCIALTYPECOMMENTS");
				cf.request.addProperty("Fld_Srid", cf.selectedhomeid);
				cf.request.addProperty("InspectionID", inspectionid);
				cf.request.addProperty("AuxillaryComments",
						cf.decode(C_COM.getString(4)));
				cf.request.addProperty("RestaurantComments",
						cf.decode(C_COM.getString(5)));

				System.out.println("request=" + cf.request);
				sub_total = 80;
				EX_Comm2[3] = cf.SoapResponse("COMMERCIALTYPECOMMENTS",
						cf.request);
				System.out.println("EX_Comm2[3]=" + EX_Comm2[3]);
			} else {
				EX_Comm2[3] = true;
			}
		} catch (Exception e) {
			EX_Comm2[3] = false;
			throw e;
		}
		sub_total = 100;
		setincreament();
		;
		return EX_Comm2[3];
	}

	private boolean EX_BIGENERAL() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException {
		// TODO Auto-generated method stub
		cf.Check_BI();
		System.out.println("BIGERNA");
		String buildoccupancytxt = "";
		if (cf.C_GEN.getCount() > 0) {
			
			System.out.println("inside graet");
			
			cf.request = cf.SoapRequest("BUILDINGGENERALDATA");
			cf.request.addProperty("Fld_Srid", cf.selectedhomeid);
			
			if(cf.GEN_NA.equals(""))
			{
				cf.request.addProperty("BuildingInfnchecked",0);	
			}
			else
			{
				cf.request.addProperty("BuildingInfnchecked",cf.GEN_NA);
			}
			System.out.println("cf.Gendata_val"+cf.Gendata_val);
			

			
			if(!cf.Gendata_val.equals(""))
			{
				String General_Data_Split[] = cf.Gendata_val.split("&#39;");System.out.println("General_Data_Split"+General_Data_Split.length);
				String insuredsplit[] = General_Data_Split[0].split("&#94;");System.out.println("insuredsplit"+insuredsplit.length);
				
				
				
				if(insuredsplit.length>1)
				{
					if (insuredsplit[0].equals("Other")) {
						cf.request.addProperty("InsuredIs", insuredsplit[0] + "("
								+ insuredsplit[1] + ")");
					} else {
						cf.request.addProperty("InsuredIs", insuredsplit[0]);
					}
				}
				else
				{
					cf.request.addProperty("InsuredIs","");
				}
				System.out.println("test=");
				String neighsplit[] = General_Data_Split[1].split("&#94;");System.out.println("neighsplit"+neighsplit.length);
				String neighsuboptionsplit[] = neighsplit[1].split("&#33;");System.out.println("neighsuboptionsplit"+neighsuboptionsplit.length);
				
				if(neighsuboptionsplit.length>1)
				{
					if (neighsuboptionsplit[0].equals("Other")) {
						cf.request.addProperty("NeighborhoodIs", neighsplit[0] + ","
								+ neighsuboptionsplit[0] + "(" + neighsuboptionsplit[1]
								+ ")");
					} else {
						cf.request.addProperty("NeighborhoodIs", neighsplit[0] + ","
								+ neighsuboptionsplit[0]);
					}
				}
				else
				{
					cf.request.addProperty("NeighborhoodIs", "");
				}
				
				System.out.println("re graet"+cf.request);

				
				System.out.println("General_Data_Split[2]"+General_Data_Split[2]);
				String occupancysplit[] = General_Data_Split[2].split("&#94;");
				
				if(occupancysplit.length>1)
				{				
					if (occupancysplit[0].equals("Other Building Type")) {
						cf.request.addProperty("OccupancyType", occupancysplit[0] + "("
								+ occupancysplit[1] + ")");
					} else {
						cf.request.addProperty("OccupancyType", occupancysplit[0]);
					}
				}
				else
				{
					cf.request.addProperty("OccupancyType", "");	
				}
				System.out.println("General_Data_Split="+General_Data_Split[4]);
				int btype = Integer.parseInt(General_Data_Split[4]);
				if (btype <= 3) {
					cf.request.addProperty("BuildingType", "Type-1 (1-3 stories)");
				} else if (btype <= 6) {
					cf.request.addProperty("BuildingType", "Type-ll (4-6 stories)");
				} else if (btype >= 7) {
					cf.request.addProperty("BuildingType",
							"Type-lll (7 or more stories)");
				}
				cf.request.addProperty("NoofBuilding", General_Data_Split[3]);
				cf.request.addProperty("NoofStories", General_Data_Split[4]);
				cf.request.addProperty("NoofUnits", General_Data_Split[5]);				
				cf.request.addProperty("BuildingSize", General_Data_Split[6]);
			}
			else
			{
				cf.getPolicyholderInformation(cf.selectedhomeid);
				cf.request.addProperty("InsuredIs", "");
				cf.request.addProperty("NeighborhoodIs",""); 
				cf.request.addProperty("OccupancyType", "");
				cf.request.addProperty("NoofBuilding", 0);
				cf.request.addProperty("NoofStories",cf.Stories); 
				cf.request.addProperty("NoofUnits", 0);
				cf.request.addProperty("BuildingSize", cf.BuildingSize);
				cf.request.addProperty("BuildingType", "");
			}
			
			
			System.out.println("re "+cf.request);
			
			if(!cf.BOCCU_SURV.equals(""))
			{
				String Perc_split[] = cf.BOCCU_SURV.split("&#33;");
				if (Perc_split[2].contains("&#40;")) {
					String[] s = Perc_split[2].split("&#40;");
					buildoccupancytxt = s[0].replace("&#94;", ",") + ",Other("
							+ s[1] + ")";
				} else {
					buildoccupancytxt = Perc_split[2].replace("&#94;", ",");
				}

				if (!Perc_split[0].equals("") && !Perc_split[0].equals("0")
						&& !Perc_split[1].equals("") && !Perc_split[1].equals("0")) {
					cf.request.addProperty("BuildingOccupancy", "Occupied("
							+ Perc_split[0] + ")~Vacant(" + Perc_split[1] + ")~"
							+ buildoccupancytxt);
				} else if (!Perc_split[0].equals("") && !Perc_split[0].equals("0")) {
					cf.request.addProperty("BuildingOccupancy", "Occupied("
							+ Perc_split[0] + ")~" + buildoccupancytxt);
				} else if (!Perc_split[1].equals("") && !Perc_split[1].equals("0")) {
					cf.request.addProperty("BuildingOccupancy", "Vacant("
							+ Perc_split[1] + ")~" + buildoccupancytxt);
				} else {
					cf.request.addProperty("BuildingOccupancy", buildoccupancytxt);
				}
			}
			else
			{
				cf.request.addProperty("BuildingOccupancy", "");
			}
			
			
			System.out.println(" graet"+cf.request);
			
			if(!cf.SCOPE.equals(""))
			{
				if (cf.SCOPE.contains("&#40;")) {
					String[] s = cf.SCOPE.split("&#40;");
					cf.request.addProperty("SurveyType", s[0].replace("&#94;", ",")
							+ ",Other(" + s[1] + ")");
				} else {
					cf.request.addProperty("SurveyType",
							cf.SCOPE.replace("&#94;", ","));
				}	
			}
			else
			{
				cf.request.addProperty("SurveyType","");
			}
			System.out.println("cf.SCOPE"+cf.SCOPE);
			System.out.println("cf.YOC"+cf.YOC);
			if(!cf.YOC.equals(""))
			{
				String yrsplit[] = cf.YOC.split("&#94;");
				String yrothersplit[] = yrsplit[0].split("&#126;");
				if (yrothersplit[0].equals("Other")) {
					cf.request.addProperty("YearConst", yrothersplit[0] + "("
							+ yrothersplit[1] + ")," + yrsplit[1]);
				} else {
					System.out.println("yrsplit.length"+yrsplit.length);
					if(yrsplit.length>1)
					{
						cf.request.addProperty("YearConst", yrothersplit[0] + ","+ yrsplit[1]);
					}
					else
					{
						cf.request.addProperty("YearConst", yrothersplit[0]);
					}
				}
				System.out.println("re zsis"+cf.request);
			}
			else
			{
				cf.request.addProperty("YearConst","");
			}System.out.println("cf.YOC");
			System.out.println("cf.Permit_Confirmed"+cf.Permit_Confirmed);
			if(!cf.Permit_Confirmed.equals(""))
			{
				if(cf.Permit_Confirmed.equals("&#94;"))
				{
					cf.request.addProperty("PermitConfirm", "");
				}
				else
				{
					String permit_conf_split[] = cf.Permit_Confirmed.split("&#94;");
					if (permit_conf_split[0].equals("Yes")) {
						cf.request.addProperty("PermitConfirm", permit_conf_split[0]
								+ "(" + permit_conf_split[1] + ")");
					} else {
						cf.request.addProperty("PermitConfirm", permit_conf_split[0]);
					}
				}				
				
			}
			else
			{
				cf.request.addProperty("PermitConfirm", "");
			}
			System.out.println("cf.BalconiesPresent"+cf.Balcony_present);
			if(!cf.Balcony_present.equals(""))
			{
				cf.request.addProperty("BalconiesPresent", cf.Balcony_present);
			}
			else
			{
				cf.request.addProperty("BalconiesPresent", "");
			}
			System.out.println("cf.Inci_Occu"+cf.Inci_Occu);
			String Inci_Occu_split[] = cf.Inci_Occu.split("&#126;");
			if (Inci_Occu_split[0].equals("Yes")) {
				cf.request.addProperty("IncidentOccupancy", Inci_Occu_split[0]
						+ "~" + Inci_Occu_split[1].replace("&#94;", ","));
			} else {
				cf.request.addProperty("IncidentOccupancy", Inci_Occu_split[0]);
			}
			System.out.println("cf.Addi_Stru"+cf.Addi_Stru);
			if(!cf.Addi_Stru.equals("")){
				String Addi_Struc_split[] = cf.Addi_Stru.split("&#126;");
				cf.request.addProperty("Addlstructure", Addi_Struc_split[0]);
				if (Addi_Struc_split.length > 1) {
					cf.request.addProperty("LightPoles",
							Addi_Struc_split[1].replace("&#94;", ","));
				} else {
					cf.request.addProperty("LightPoles", "");
				}
				if (Addi_Struc_split.length > 2) {
					if (Addi_Struc_split[2].contains("&#40;")) {
						String[] s = Addi_Struc_split[2].split("&#40;");
						cf.request
								.addProperty("FencesPrepLine",
										s[0].replace("&#94;", ",") + ",Other("
												+ s[1] + ")");
					} else {
						cf.request.addProperty("FencesPrepLine",
								Addi_Struc_split[2].replace("&#94;", ","));
					}
				} else {
					cf.request.addProperty("FencesPrepLine", "");
				}
			}
			else
			{
				cf.request.addProperty("Addlstructure", "");
				cf.request.addProperty("LightPoles", "");
				cf.request.addProperty("FencesPrepLine", "");
			}
			
			

			System.out.println("Buidling  Information=" + cf.request);
			EX_BI[0] = cf.SoapResponse("BUILDINGGENERALDATA", cf.request);
			System.out.println("EX_BI[0]=" + EX_BI[0]);
		} else {
			EX_BI[0] = true;
		}
		System.out.println("BIGERNAEX_BI[0]="+EX_BI[0]);
		return EX_BI[0];
	}

	private boolean EX_BUILDINGINFORMATION() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException {
		// TODO Auto-generated method stub
		cf.Check_BI();
		String isotxt = "";
		System.out.println("came here");
		if (cf.C_GEN.getCount() > 0) {
			cf.request = cf.SoapRequest("BUILDINGINFORMATION");
			cf.request.addProperty("Fld_Srid", cf.selectedhomeid);
			cf.request.addProperty("LocationNAchecked", cf.Loc_NA);
			System.out.println("cf.Loc_NA"+cf.Loc_NA);
			if (cf.Loc_NA.equals("0")) {
				String Loc_Split[] = cf.Loc_val.split("&#94;");
				cf.request.addProperty("SaltWater1", Loc_Split[0]);
				cf.request.addProperty("SaltWater2", Loc_Split[1]);
				cf.request.addProperty("Commercialstruct", Loc_Split[2]);
				cf.request.addProperty("BrushForestFires", Loc_Split[3]);

				if (Loc_Split.length > 4) {
					cf.request.addProperty("LocationOther", Loc_Split[4]);
				}
			} else {
				cf.request.addProperty("SaltWater1", "");
				cf.request.addProperty("SaltWater2", "");
				cf.request.addProperty("Commercialstruct", "");
				cf.request.addProperty("BrushForestFires", "");
				cf.request.addProperty("LocationOther", "");
			}
			System.out.println("cf.ObservationNAchecked"+cf.OBS_NA);
			cf.request.addProperty("ObservationNAchecked", cf.OBS_NA);
			if (cf.OBS_NA.equals("0")) {

				String Observ1_split[] = cf.Observ1.split("&#94;");
				if (Observ1_split[0].equals("No")) {
					cf.request.addProperty("ConstructionOrigin",
							Observ1_split[0] + " (" + Observ1_split[1] + ")");
				} else {
					cf.request.addProperty("ConstructionOrigin",
							Observ1_split[0]);
				}

				String Observ2_split[] = cf.Observ2.split("&#94;");
				if (Observ2_split[0].equals("Yes")) {
					cf.request.addProperty("RemodelOrigin", Observ2_split[0]
							+ "(" + Observ2_split[1] + ")");
				} else {
					cf.request.addProperty("RemodelOrigin", Observ2_split[0]);
				}

				String Observ3_split[] = cf.Observ3.split("&#94;");
				if (Observ3_split[0].equals("No")) {
					cf.request.addProperty("Professionconst", Observ3_split[0]
							+ "(" + Observ3_split[1] + ")");
				} else {
					cf.request.addProperty("Professionconst", Observ3_split[0]);
				}
				cf.request.addProperty("PermitConfirm", "");

				String Observ4_split[] = cf.Observ4.split("&#94;");
				if (Observ4_split[0].equals("Yes")) {
					cf.request.addProperty("Visiblenonstandard",
							Observ4_split[0] + "(" + Observ4_split[1] + ")");
				} else {
					cf.request.addProperty("Visiblenonstandard",
							Observ4_split[0]);
				}

				String Observ5_split[] = cf.Observ5.split("&#94;");
				if (Observ5_split[0].equals("Yes")) {
					cf.request.addProperty("PermitInformation",
							Observ5_split[0] + "(" + Observ5_split[1] + ")");
				} else {
					cf.request.addProperty("PermitInformation",
							Observ5_split[0]);
				}

				cf.request.addProperty("ObservationDesc", "");
			} else {
				cf.request.addProperty("ConstructionOrigin", "");
				cf.request.addProperty("RemodelOrigin", "");
				cf.request.addProperty("Professionconst", "");
				cf.request.addProperty("PermitConfirm", "");
				cf.request.addProperty("Visiblenonstandard", "");
				cf.request.addProperty("PermitInformation", "");
				cf.request.addProperty("ObservationDesc", "");
			}

			cf.request.addProperty("ISOClassificationNAchk", cf.ISO_NA);
			if (cf.ISO_NA.equals("0")) {
				String ISO_split[] = cf.Iso_val.split("~");
				if (ISO_split[0].length() > 1) {
					isotxt += "Frame (" + ISO_split[0].trim() + ")~";
				}
				else
				{
					isotxt += " ( )~";
				}
				if (ISO_split[2].length() > 1) {
					isotxt += "Mixed (" + ISO_split[2].trim() + ")~";
				}
				else
				{
					isotxt += " ( )~";
				}
				if (ISO_split[5].length() > 1) {
					isotxt += "Superior Masonry (" + ISO_split[5].trim() + ")~";
				}
				else
				{
					isotxt += " ( )~";
				}
				if (ISO_split[1].length() > 1) {
					isotxt += "Masonry Non-Combustible ("+ ISO_split[1].trim() + ")~";
				}
				else
				{
					isotxt += " ( )~";
				}
				if (ISO_split[7].length() > 1) {
					System.out.println("DDS");
					isotxt += "Fire Resistive (" + ISO_split[7].trim() + ")~";
				}
				else
				{
					isotxt += " ( )~";
				}
				
				if (ISO_split[3].length() > 1) {
					isotxt += "Joisted Masonry (" + ISO_split[3].trim() + ")~";
				}
				else
				{
					isotxt += " ( )~";
					
				}
				if (ISO_split[8].length() > 1) {
					isotxt += "Heavy Lumber (" + ISO_split[8].trim() + ")~";
				}
				else
				{
					isotxt += " ( )~";
				}
				if (ISO_split[6].length() > 1) {
					isotxt += "Non-Combustible (" + ISO_split[6].trim() + ")~";
				}
				else
				{
					isotxt += " ( )~";
				}
				if (ISO_split[4].length() > 1) {
					isotxt += "Modified Fire Resistive ("
							+ ISO_split[4].trim() + ")~";
				}
				else
				{
					isotxt += " ( )~";
				}
				if (ISO_split[10].length() > 1) {
					isotxt += "Superior Masonry Non-Combustible ("
							+ ISO_split[10].trim() + ")";
				}
				else
				{
					isotxt += " ( )~";
				}
				if (ISO_split[9].length() > 1) {
					isotxt += "~Superior Non-Combustible ("
							+ ISO_split[9].trim() + ")";
				}
				else
				{
					isotxt += " ( )";
				}
				
				cf.request.addProperty("ISOClassification",isotxt);
				
			} else {
				cf.request.addProperty("ISOClassification", "");
			}
			cf.request.addProperty("FoundationNAchecked", cf.Foun_NA);

			if (cf.Foun_NA.equals("0")) {
				if (cf.Foundation_val.contains("&#40;")) {
					String[] s = cf.Foundation_val.split("&#40;");
					cf.request
							.addProperty("Foundation",
									s[0].replace("&#94;", ",") + ",Other("
											+ s[1] + ")");
				} else {
					cf.request.addProperty("Foundation",
							cf.Foundation_val.replace("&#94;", ","));
				}
			} else {
				cf.request.addProperty("Foundation", "");
			}
			cf.request.addProperty("WallStructureNAchecked", cf.WS_NA);
			System.out.println("Wallstru_val" + cf.Wallstru_val);
			if (cf.WS_NA.equals("0")) {
				String WSPer_Split[] = cf.Wallstru_val.split("\\^", -1);
				String WSStories_Split[] = cf.WS_Stories.split("\\^", -1);

				if (WSStories_Split[0].trim().equals("")) {
					cf.request.addProperty("UnReinforceMasonry", WSPer_Split[0]
							+ "~0");
				} else {
					cf.request.addProperty("UnReinforceMasonry", WSPer_Split[0]
							+ "~" + WSStories_Split[0]);
				}
				if (WSStories_Split[1].trim().equals("")) {
					cf.request.addProperty("ReinforceMasonry", WSPer_Split[1]
							+ "~0");
				} else {
					cf.request.addProperty("ReinforceMasonry", WSPer_Split[1]
							+ "~" + WSStories_Split[1]);
				}
				if (WSStories_Split[2].trim().equals("")) {
					cf.request.addProperty("ReinforceConcrete", WSPer_Split[2]
							+ "~0");
				} else {
					cf.request.addProperty("ReinforceConcrete", WSPer_Split[2]
							+ "~" + WSStories_Split[2]);
				}
				if (WSStories_Split[3].trim().equals("")) {
					cf.request.addProperty("WoodLightFrame", WSPer_Split[3]
							+ "~0");
				} else {
					cf.request.addProperty("WoodLightFrame", WSPer_Split[3]
							+ "~" + WSStories_Split[3]);
				}
				if (WSStories_Split[4].trim().equals("")) {
					cf.request.addProperty("HeavySteelFrame", WSPer_Split[4]
							+ "~0");
				} else {
					cf.request.addProperty("HeavySteelFrame", WSPer_Split[4]
							+ "~" + WSStories_Split[4]);
				}
				if (WSStories_Split[5].trim().equals("")) {
					cf.request.addProperty("ReinConcreteFrame", WSPer_Split[5]
							+ "~0");
				} else {
					cf.request.addProperty("ReinConcreteFrame", WSPer_Split[5]
							+ "~" + WSStories_Split[5]);
				}
				if (WSStories_Split[6].trim().equals("")) {
					cf.request.addProperty("SteelFrame", WSPer_Split[6] + "~0");
				} else {
					cf.request.addProperty("SteelFrame", WSPer_Split[6] + "~"
							+ WSStories_Split[6]);
				}
				if (WSStories_Split[7].trim().equals("")) {
					cf.request
							.addProperty("TiltUpConst", WSPer_Split[7] + "~0");
				} else {
					cf.request.addProperty("TiltUpConst", WSPer_Split[7] + "~"
							+ WSStories_Split[7]);
				}
				if (!WSPer_Split[8].trim().equals("")) {
					if (WSStories_Split[8].trim().equals("")) {
						cf.request.addProperty("OtherWall", cf.WS_Other + "("
								+ WSPer_Split[8] + "~0)");
					} else {
						cf.request.addProperty("OtherWall", cf.WS_Other + "("
								+ WSPer_Split[8] + "~" + WSStories_Split[8]
								+ ")");
					}
				} else {
					cf.request.addProperty("OtherWall", WSPer_Split[8] + ",0");
				}
			} else {
				cf.request.addProperty("UnReinforceMasonry", "");
				cf.request.addProperty("ReinforceMasonry", "");
				cf.request.addProperty("ReinforceConcrete", "");
				cf.request.addProperty("WoodLightFrame", "");
				cf.request.addProperty("HeavySteelFrame", "");
				cf.request.addProperty("ReinConcreteFrame", "");
				cf.request.addProperty("SteelFrame", "");
				cf.request.addProperty("TiltUpConst", "");
				cf.request.addProperty("OtherWall", "");
			}

			cf.request.addProperty("WallCladdingNAchecked", cf.WC_NA);

			if (cf.WC_NA.equals("0")) {
				String WcStories_Split[] = cf.Wallclad_val.split("\\^", -1);

				cf.request.addProperty("AluminiumSiding", WcStories_Split[0]);
				cf.request.addProperty("Stucco", WcStories_Split[1]);
				cf.request.addProperty("VinylSiding", WcStories_Split[2]);
				cf.request.addProperty("BrickSiding", WcStories_Split[3]);
				cf.request.addProperty("WoodSiding", WcStories_Split[4]);
				cf.request.addProperty("Paintedblock", WcStories_Split[5]);
				cf.request.addProperty("BrickVeneer", WcStories_Split[6]);
				cf.request.addProperty("MetalPanels", WcStories_Split[7]);
				cf.request.addProperty("PaintedConcrete", WcStories_Split[8]);
				cf.request.addProperty("Painted", WcStories_Split[9]);
				cf.request.addProperty("PreCastPanels", WcStories_Split[10]);
				cf.request.addProperty("CementFiber", WcStories_Split[11]);
				cf.request.addProperty("Glass", WcStories_Split[12]);
				System.out.println("WcStories_Split[13]" + WcStories_Split[13]
						+ "DSF");
				if (!WcStories_Split[13].trim().equals("")) {
					cf.request.addProperty("OtherWallcladding", cf.WC_Other
							+ "(" + WcStories_Split[13] + ")");
				} else {
					cf.request.addProperty("OtherWallcladding", "");
				}
			} else {
				cf.request.addProperty("AluminiumSiding", "");
				cf.request.addProperty("Stucco", "");
				cf.request.addProperty("VinylSiding", "");
				cf.request.addProperty("BrickSiding", "");
				cf.request.addProperty("WoodSiding", "");
				cf.request.addProperty("Paintedblock", "");
				cf.request.addProperty("BrickVeneer", "");
				cf.request.addProperty("MetalPanels", "");
				cf.request.addProperty("PaintedConcrete", "");
				cf.request.addProperty("Painted", "");
				cf.request.addProperty("PreCastPanels", "");
				cf.request.addProperty("CementFiber", "");
				cf.request.addProperty("Glass", "");
				cf.request.addProperty("OtherWallcladding", "");
			}

			cf.request.addProperty("BuildingseperationNAchk", cf.BISEP_NA);

			if (cf.BISEP_NA.equals("0")) {
				String BuildSep_split[] = cf.Buildsep_val.split("&#126;");
				cf.request.addProperty("BuildingSeperation",
						BuildSep_split[0].replace("&#94;", ","));
				if (BuildSep_split[1].contains("&#40;")) {
					String[] s = BuildSep_split[1].split("&#40;");
					cf.request
							.addProperty("BuildingWallStructure",
									s[0].replace("&#94;", ",") + ",Other("
											+ s[1] + ")");
				} else {
					cf.request.addProperty("BuildingWallStructure",
							BuildSep_split[1].replace("&#94;", ","));
				}
				cf.request
						.addProperty("ElevationWallextend", BuildSep_split[2]);
			} else {
				cf.request.addProperty("BuildingSeperation", "");
				cf.request.addProperty("BuildingWallStructure", "");
				cf.request.addProperty("ElevationWallextend", "");
			}

			cf.request.addProperty("BuildingInfocomments", cf.comments);
			cf.request.addProperty("SchedulingComments", "");

			System.out.println("Buidling  Information11==" + cf.request);
			EX_BI[1] = cf.SoapResponse("BUILDINGINFORMATION", cf.request);
			System.out.println("EX_BI[1]=" + EX_BI[1]);

		} else {
			EX_BI[1] = true;
		}
		return EX_BI[1];
	}

	private boolean EX_SH_OBS4() throws SocketException, NetworkErrorException,
			TimeoutException, IOException, XmlPullParserException, Exception,
			NullPointerException {
		// TODO Auto-generated method stub
		try {
			sub_total = 0;
			cf.CreateARRTable(55);
			sub_insp = "Observations(23 - 27)";
			cf.request = cf.SoapRequest("SinkholeOBSERVATION4");
			Cursor C_SOB4 = cf.SelectTablefunction(cf.SINK_Obs4tbl,
					" where fld_srid='" + cf.selectedhomeid + "' ");
			if (C_SOB4.getCount() >= 1) {
				C_SOB4.moveToFirst();
				String[] SOB4_infos = cf.setvalueToArray(C_SOB4);

				cf.request.addProperty("Fld_Srid", SOB4_infos[1]);
				cf.request.addProperty("CrawlSpacePresent", SOB4_infos[2]);
				cf.request.addProperty("CrawlSpaceComments", SOB4_infos[3]);
				cf.request.addProperty("StandingWaterNoted", SOB4_infos[4]);
				cf.request.addProperty("StandingWaterComments", SOB4_infos[5]);
				cf.request.addProperty("LeakingPlumbNoted", SOB4_infos[6]);
				cf.request.addProperty("LeakingPlumbComments", SOB4_infos[7]);
				cf.request.addProperty("VentilationNoted", SOB4_infos[8]);
				cf.request.addProperty("VentilationComments", SOB4_infos[9]);
				cf.request.addProperty("CrawlSpaceAccessible", SOB4_infos[10]);
				cf.request.addProperty("CrawlSpaceAccessibleComments",
						SOB4_infos[11]);
				cf.request.addProperty("RoofSagNoted", SOB4_infos[12]);
				cf.request.addProperty("RoofSagComments", SOB4_infos[13]);
				cf.request.addProperty("RoofLeakageEvidence", SOB4_infos[14]);
				cf.request.addProperty("RoofLeakageComments", SOB4_infos[15]);
				cf.request.addProperty("DeferredMaintanenceIssuesNoted",
						SOB4_infos[16]);
				cf.request.addProperty("DeferredMaintanenceComments",
						SOB4_infos[17]);
				cf.request.addProperty("DeterioratedNoted", SOB4_infos[18]);
				cf.request.addProperty("DeterioratedComments", SOB4_infos[19]);
				cf.request.addProperty("DeterioratedExteriorFinishesNoted",
						SOB4_infos[20]);
				cf.request.addProperty("DeterioratedExteriorFinishesComments",
						SOB4_infos[21]);
				cf.request.addProperty("DeterioratedExteriorSidingNoted",
						SOB4_infos[22]);
				cf.request.addProperty("DeterioratedExteriorSidingComments",
						SOB4_infos[23]);
				cf.request.addProperty("SafetyNoted", SOB4_infos[24]);
				cf.request.addProperty("SafetyComments", SOB4_infos[25]);
				cf.request.addProperty("OverallComments", SOB4_infos[26]);
				cf.request.addProperty("Observation23chk", 1);
				cf.request.addProperty("Observation24chk", 1);
				cf.request.addProperty("Observation25chk", 1);
				cf.request.addProperty("Observation26chk", 1);
				cf.request.addProperty("Observation27chk", 1);
				System.out.println("SinkObservation4result= " + cf.request);
				sub_total = 50;
				EX_CHKSH[5] = cf.SoapResponse("SinkholeOBSERVATION4",
						cf.request);
				System.out.println("EX_CHKSH[5]= " + EX_CHKSH[5]);
				C_SOB4.close();
			}
			else
			{
				EX_CHKSH[5]= true;
			}

		} catch (Exception e) {
			// TODO: handle exception
			EX_CHKSH[5] = false;
			throw e;
		}
		sub_total = 100;
		setincreament();
		;
		return EX_CHKSH[5];
	}

	/*** Wind mitigation data ends */
	/*** GCH export Starts */
	private boolean EX_GCH_Misc() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		// TODO Auto-generated method stub
		try {
			sub_total = 0;
			sub_insp = "Miscellaneous";
			cf.CreateARRTable(418);
			cf.request = cf.SoapRequest("ExportGCHMiscellaneous");
			Cursor C_M = cf.SelectTablefunction(cf.GCH_Misc,
					" where fld_srid='" + cf.selectedhomeid + "'");
			if (C_M.getCount() >= 1) {
				String[] M_infos = cf.setvalueToArray(C_M);
				System.out.println("M_infos= " + M_infos[4]);
				if (!M_infos[5].equals("")) {
					cf.request.addProperty("Disposalofash", M_infos[5]);
				} else {
					cf.request.addProperty("Disposalofash", M_infos[4]);
				}
				cf.request.addProperty("Fld_Srid", M_infos[1]);

				cf.request.addProperty("Miscellaneouschecked", M_infos[2]);

				cf.request.addProperty("TrashRemove", M_infos[3]);

				cf.request.addProperty("Soiledlinens", M_infos[6]);

				System.out.println("request10" + cf.request);
				sub_total = 50;
				EX_CHKGCH[10] = cf.SoapResponse("ExportGCHMiscellaneous",
						cf.request);
				System.out.println("EX_CHKGCH[10]=" + EX_CHKGCH[10]);
				C_M.close();
			}
			else
			{
				EX_CHKGCH[10] = true;
			}
		} catch (Exception e) {
			// TODO: handle exception
			EX_CHKGCH[10] = false;
			throw e;
		}
		sub_total = 100;
		setincreament();
		;
		return EX_CHKGCH[10];

	}

	private boolean EX_GCH_FireProtection() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		// TODO Auto-generated method stub
		try {
			sub_total = 0;
			sub_insp = "Fire Protection";
			cf.request = cf.SoapRequest("SaveFirePro");
			cf.CreateARRTable(417);
			Cursor C_FR = cf.SelectTablefunction(cf.GCH_FireProtection,
					" where fld_srid='" + cf.selectedhomeid + "' ");
			if (C_FR.getCount() >= 1) {
				String[] FR_infos = cf.setvalueToArray(C_FR);

				cf.request.addProperty("Fld_Srid", FR_infos[1]);
				cf.request.addProperty("FireProtectchecked", FR_infos[2]);
				if (FR_infos[3].equals("")) {
					cf.request.addProperty("autofire", "");
					cf.request.addProperty("NameModel", "");
					cf.request.addProperty("Lastservice", "");
					cf.request.addProperty("Oftenservice", "");
					cf.request.addProperty("Servicecomp", "");
					cf.request.addProperty("Undercontract", "");
					cf.request.addProperty("manualshut", "");
					cf.request.addProperty("Porablefire", "");
					cf.request.addProperty("autofireext", "");
				} else {
					String FIRE_Split[] = FR_infos[3].split("&#94;");
					cf.request.addProperty("autofire", FIRE_Split[0]);
					cf.request.addProperty("NameModel", FIRE_Split[1]);
					cf.request.addProperty("Lastservice", FIRE_Split[2]);
					cf.request.addProperty("Oftenservice", FIRE_Split[4]);
					cf.request.addProperty("Servicecomp", FIRE_Split[5]);
					String FIRE_PROT1_SPLIT1[] = FIRE_Split[3].split("&#39;");
					if (FIRE_PROT1_SPLIT1[0].equals("Other")) {
						cf.request.addProperty("Undercontract",
								FIRE_PROT1_SPLIT1[0] + "^BRK"
										+ FIRE_PROT1_SPLIT1[1]);
					} else {
						cf.request.addProperty("Undercontract",
								FIRE_PROT1_SPLIT1[0]);
					}

					String FIRE_PROT2_SPLIT[] = FIRE_Split[6].split("&#39;");
					if (FIRE_PROT2_SPLIT[0].equals("Other")) {
						cf.request.addProperty("manualshut",
								FIRE_PROT2_SPLIT[0] + "^BRK"
										+ FIRE_PROT2_SPLIT[1]);
					} else {
						cf.request.addProperty("manualshut",
								FIRE_PROT2_SPLIT[0]);
					}

					String FIRE_PROT3_SPLIT[] = FIRE_Split[7].split("&#39;");
					if (FIRE_PROT3_SPLIT[0].equals("Other")) {
						cf.request.addProperty("Porablefire",
								FIRE_PROT3_SPLIT[0] + "^BRK"
										+ FIRE_PROT3_SPLIT[1]);
					} else {
						cf.request.addProperty("Porablefire",
								FIRE_PROT3_SPLIT[0]);
					}

					cf.request.addProperty("autofireext", FIRE_Split[8]);
				}

				System.out.println("request9" + cf.request);
				sub_total = 50;
				EX_CHKGCH[9] = cf.SoapResponse("SaveFirePro", cf.request);
				System.out.println("EX_CHKGCH[9]=" + EX_CHKGCH[9]);
				C_FR.close();
			}
			else
			{
				EX_CHKGCH[9] = true;
			}
		} catch (Exception e) {
			// TODO: handle exception
			EX_CHKGCH[9] = false;
			throw e;
		}
		sub_total = 100;
		setincreament();
		;
		return EX_CHKGCH[9];

	}

	private boolean EX_GCH_Hood() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		// TODO Auto-generated method stub
		try {
			sub_total = 0;
			sub_insp = "Hood and Duct Work";
			cf.CreateARRTable(416);
			cf.request = cf.SoapRequest("ExportGCHHoodduckwork");
			Cursor C_HD = cf.SelectTablefunction(cf.GCH_HoodDuct,
					" where fld_srid='" + cf.selectedhomeid + "' ");
			if (C_HD.getCount() >= 1) {
				String[] HD_infos = cf.setvalueToArray(C_HD);
				cf.request.addProperty("Fld_Srid", HD_infos[1]);
				cf.request.addProperty("hoodduckchk", HD_infos[2]);
				if (HD_infos[3].equals("")) {
					cf.request.addProperty("Frequency", "");
					cf.request.addProperty("contractName", "");
					cf.request.addProperty("isContractor", "");
					cf.request.addProperty("HDFclean", "");
					cf.request.addProperty("clearprovide", "");
				} else {
					String Hood_Split[] = HD_infos[3].split("&#94;");
					cf.request.addProperty("Frequency", Hood_Split[0]);
					cf.request.addProperty("contractName", Hood_Split[1]);
					cf.request.addProperty("isContractor", Hood_Split[2]);
					cf.request.addProperty("HDFclean", Hood_Split[3]);
					cf.request.addProperty("clearprovide", Hood_Split[4]);
				}

				System.out.println("request8" + cf.request);
				sub_total = 50;
				EX_CHKGCH[8] = cf.SoapResponse("ExportGCHHoodduckwork",
						cf.request);
				System.out.println("EX_CHKGCH[8]=" + EX_CHKGCH[8]);
				C_HD.close();
			}
			else
			{
				EX_CHKGCH[8] = true;
			}

		} catch (Exception e) {
			// TODO: handle exception
			EX_CHKGCH[8] = false;
			throw e;
		}
		sub_total = 100;
		setincreament();
		;
		return EX_CHKGCH[8];

	}

	private boolean EX_GCH_Comments() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		// TODO Auto-generated method stub
		try {
			sub_total = 0;
			sub_insp = "Overall GCH Comments";
			cf.CreateARRTable(49);
			cf.request = cf.SoapRequest("ExportGCHOverallComments");
			Cursor C_COM = cf.SelectTablefunction(cf.GCH_commenttbl,
					" where fld_srid='" + cf.selectedhomeid + "' ");
			if (C_COM.getCount() >= 1) {
				String[] COM_infos = cf.setvalueToArray(C_COM);

				cf.request.addProperty("Fld_Srid", COM_infos[1]);
				cf.request.addProperty("gchcomment", COM_infos[2]);
				System.out.println("request11" + cf.request);

				EX_CHKGCH[11] = cf.SoapResponse("ExportGCHOverallComments",
						cf.request);
				System.out.println("EX_CHKGCH[11]=" + EX_CHKGCH[11]);
				C_COM.close();
			}
			else
			{
				EX_CHKGCH[11] = true;
			}

		} catch (Exception e) {
			// TODO: handle exception
			EX_CHKGCH[11] = false;
			throw e;
		}
		sub_total = 100;
		setincreament();
		
		return EX_CHKGCH[11];
	}

	private boolean EX_GCH_Electric() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		// TODO Auto-generated method stub
		try {
			sub_total = 0;
			cf.CreateARRTable(48);
			sub_insp = "Electric System";
			cf.request = cf.SoapRequest("ExportGCElectricSystem");
			Cursor C_ELE = cf.SelectTablefunction(cf.GCH_Electrictbl,
					" where fld_srid='" + cf.selectedhomeid + "' ");
			if (C_ELE.getCount() >= 1) {
				ELE_infos = cf.setvalueToArray(C_ELE);
				for (int i = 0; i < ELE_infos.length; i++) {
					if (ELE_infos[i].equals("--Select--")
							|| ELE_infos[i].equals("null")) {
						ELE_infos[i] = ELE_infos[i].replace("--Select--", "");
						ELE_infos[i] = ELE_infos[i].replace("null", "");
					}
				}
				cf.request.addProperty("Fld_Srid", ELE_infos[1]);
				cf.request.addProperty("fld_elecchk", ELE_infos[2]);
				cf.request.addProperty("fld_mdisconnect", ELE_infos[3]);
				if(ELE_infos[4].equals("Other"))
				{
					cf.request.addProperty("fld_mpamp", ELE_infos[4]);
					cf.request.addProperty("fld_mpampotr", ELE_infos[5]);
				}
				else
				{
					cf.request.addProperty("fld_mpamp", ELE_infos[4]);
					cf.request.addProperty("fld_mpampotr", "");
				}

				/*if (ELE_infos[6].contains("Knob & Tube")) {
					ELE_infos[6] = ELE_infos[6].replace("Knob & Tube",
							"Knob ~ Tube");
				}*/

				if (ELE_infos[6].contains("&#40;")) {
					String[] s = ELE_infos[6].split("&#40;");
					cf.request.addProperty("fld_electype", s[0] + "&#94;Other("
							+ s[1] + ")");
				} else {
					cf.request.addProperty("fld_electype", ELE_infos[6]);
				}
				
				if (ELE_infos[7].contains("&#40;")) {
					String[] s = ELE_infos[7].split("&#40;");
					cf.request.addProperty("fld_voprotect", s[0] + "&#94;Other("
							+ s[1] + ")");
				} else {
					cf.request.addProperty("fld_voprotect", ELE_infos[7]);
				}
				
				//cf.request.addProperty("fld_electype", ELE_infos[6]);
				//cf.request.addProperty("fld_voprotect", ELE_infos[7]);
				if (ELE_infos[8].contains("Open Junction/Service Box")) {
					ELE_infos[8] = ELE_infos[8].replace(
							"Open Junction/Service Box",
							"Open Junction/Service Box");
				}
				
				if (ELE_infos[8].contains("&#40;")) {
					String[] s = ELE_infos[8].split("&#40;");
					cf.request.addProperty("fld_oehz", s[0] + "&#94;Other("
							+ s[1] + ")");
				} else {
					cf.request.addProperty("fld_oehz", ELE_infos[8]);
				}
				
				//cf.request.addProperty("fld_oehz", ELE_infos[8]);
				cf.request.addProperty("fld_csatis", ELE_infos[9]);
				cf.request.addProperty("fld_updated", ELE_infos[10]);
				cf.request.addProperty("fld_age", ELE_infos[11]);
				cf.request.addProperty("fld_Electriccomments", ELE_infos[12]);
				
				System.out.println("request8" + cf.request);
				sub_total = 50;
				EX_CHKGCH[7] = cf.SoapResponse("ExportGCElectricSystem",
						cf.request);
				System.out.println("EX_CHKGCH[7]=" + EX_CHKGCH[7]);
				C_ELE.close();
			}
			else
			{
				EX_CHKGCH[7] = true;
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			EX_CHKGCH[7] = false;
			throw e;
		}
		sub_total = 100;
		setincreament();
		;
		return EX_CHKGCH[7];
	}

	private boolean EX_GCH_HVAC() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		// TODO Auto-generated method stub
		try {
			sub_total = 0;
			sub_insp = "HVAC System";
			cf.CreateARRTable(47);
			cf.request = cf.SoapRequest("ExportGCHHVACSystem");
			Cursor C_HV = cf.SelectTablefunction(cf.GCH_Hvactbl,
					" where fld_srid='" + cf.selectedhomeid + "' ");
			if (C_HV.getCount() >= 1) {
				HV_infos = cf.setvalueToArray(C_HV);
				for (int i = 0; i < HV_infos.length; i++) {
					if (HV_infos[i].equals("--Select--")
							|| HV_infos[i].equals("null")) {
						HV_infos[i] = HV_infos[i].replace("--Select--", "");
						HV_infos[i] = HV_infos[i].replace("null", "");
					}
				}
				cf.request.addProperty("Fld_Srid", HV_infos[1]);
				cf.request.addProperty("fld_hvchk", HV_infos[2]);
				cf.request.addProperty("fld_hvacsystem", HV_infos[3]);
				if (HV_infos[4].contains("&#40;")) {
					String[] s = HV_infos[4].split("&#40;");
					cf.request.addProperty("fld_type", s[0] + "&#94;Other("
							+ s[1] + ")");
				} else {
					cf.request.addProperty("fld_type", HV_infos[4]);
				}
				if (HV_infos[5].contains("&#40;")) {
					String[] fl = HV_infos[5].split("&#40;");
					cf.request.addProperty("fld_fuel", fl[0] + "&#94;Other("
							+ fl[1] + ")");
				} else {
					cf.request.addProperty("fld_fuel", HV_infos[5]);
				}
				if (HV_infos[6].contains("&#40;")) {
					String[] ar = HV_infos[6].split("&#40;");
					cf.request.addProperty("fld_air", ar[0] + "&#94;Other("
							+ ar[1] + ")");
				} else {
					cf.request.addProperty("fld_air", HV_infos[6]);
				}
				System.out.println("fld_condsatis" + HV_infos[7]);

				if (HV_infos[7].contains("&#94;")) {
					String[] sp = HV_infos[7].split("&#94;");
					cf.request.addProperty("fld_condsatis", sp[0] + "^BRK"
							+ sp[1]);
				} else {
					cf.request.addProperty("fld_condsatis", HV_infos[7]);
				}

				cf.request.addProperty("fld_updated", HV_infos[8]);
				cf.request.addProperty("fld_sdlupd", HV_infos[9]);
				cf.request.addProperty("fld_sconf", HV_infos[10]);
				cf.request.addProperty("fld_age", HV_infos[11]);
				cf.request.addProperty("fld_appearsop", HV_infos[12]);
				cf.request.addProperty("fld_other", HV_infos[13]);
				cf.request.addProperty("fld_comments", HV_infos[14]);
				System.out.println("ExportGCHHVACSystem=" + cf.request);
				sub_total = 50;
				EX_CHKGCH[6] = cf.SoapResponse("ExportGCHHVACSystem",
						cf.request);
				System.out.println("EX_CHKGCH[6]=" + EX_CHKGCH[6]);
				C_HV.close();
			}
			else
			{
				EX_CHKGCH[6]= true;
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			EX_CHKGCH[6] = false;
			throw e;
		}
		sub_total = 100;
		setincreament();
		;
		return EX_CHKGCH[6];
	}

	private boolean EX_GCH_SpecialHazards() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		// TODO Auto-generated method stub
		try {
			sub_total = 0;
			cf.CreateARRTable(46);
			sub_insp = "Special Hazards";
			cf.request = cf.SoapRequest("ExportGCHSpecialHazards");
			Cursor C_SPH = cf.SelectTablefunction(cf.GCH_SpecHazardstbl,
					" where fld_srid='" + cf.selectedhomeid + "' ");
			if (C_SPH.getCount() >= 1) {
				SPH_infos = cf.setvalueToArray(C_SPH);

				cf.request.addProperty("Fld_Srid", SPH_infos[1]);
				cf.request.addProperty("fld_shzchk", SPH_infos[2]);
				cf.request.addProperty("fld_fgases", SPH_infos[3]);
				cf.request.addProperty("fld_fliquids", SPH_infos[4]);
				cf.request.addProperty("fld_cliquids", SPH_infos[5]);
				cf.request.addProperty("fld_cmetals", SPH_infos[6]);
				cf.request.addProperty("fld_cchim", SPH_infos[7]);
				cf.request.addProperty("fld_welding", SPH_infos[8]);
				cf.request.addProperty("fld_spaint", SPH_infos[9]);
				cf.request.addProperty("fld_dip", SPH_infos[10]);
				cf.request.addProperty("fld_exp", SPH_infos[11]);
				cf.request.addProperty("fld_astorage", SPH_infos[12]);
				cf.request.addProperty("fld_incin", SPH_infos[13]);
				cf.request.addProperty("fld_ifovens", SPH_infos[14]);
				cf.request.addProperty("fld_cgp", SPH_infos[15]);
				cf.request.addProperty("fld_agfstorage", SPH_infos[16]);
				cf.request.addProperty("fld_ufstorage", SPH_infos[17]);
				cf.request.addProperty("fld_soilyrages", SPH_infos[18]);
				cf.request.addProperty("fld_expdust", SPH_infos[19]);
				cf.request.addProperty("fld_wworking", SPH_infos[20]);
				cf.request.addProperty("fld_hpiled", SPH_infos[21]);
				cf.request.addProperty("fld_pbaler", SPH_infos[22]);
				cf.request.addProperty("fld_ccook", SPH_infos[23]);
				System.out.println("ExportGCHSpecialHazards=" + cf.request);
				sub_total = 50;
				EX_CHKGCH[5] = cf.SoapResponse("ExportGCHSpecialHazards",
						cf.request);
				System.out.println("EX_CHKGCH[5]=" + EX_CHKGCH[5]);
				C_SPH.close();
			}
			else
			{
				EX_CHKGCH[5] =true;
			}

		} catch (Exception e) {
			// TODO: handle exception
			EX_CHKGCH[5] = false;
			throw e;
		}
		sub_total = 100;
		setincreament();
		;
		return EX_CHKGCH[5];
	}

	private boolean EX_GCH_Fire() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		// TODO Auto-generated method stub
		try {
			sub_total = 0;
			cf.CreateARRTable(45);
			sub_insp = "Fire Protection/Survey";
			cf.request = cf.SoapRequest("ExportGCHFireProtectionSurvey");
			Cursor C_FP = cf.SelectTablefunction(cf.GCH_FireProtecttbl,
					" where fld_srid='" + cf.selectedhomeid + "' ");
			if (C_FP.getCount() >= 1) {
				FP_infos = cf.setvalueToArray(C_FP);

				cf.request.addProperty("Fld_Srid", FP_infos[1]);
				cf.request.addProperty("fld_fpschk", FP_infos[2]);
				if (FP_infos[3].equals("Yes-Based on Floors Inspected")) {
					FP_infos[3] = FP_infos[3].replace(
							"Yes-Based on Floors Inspected",
							"Yes-Based on Floors Inspected");
				}
				cf.request.addProperty("fld_fireext", FP_infos[3]);
				cf.request.addProperty("fld_firedet", FP_infos[4]);
				cf.request.addProperty("fld_size", FP_infos[5]);
				cf.request.addProperty("fld_type", FP_infos[6]);
				cf.request.addProperty("fld_wservice", FP_infos[7]);
				cf.request.addProperty("fld_taged", FP_infos[8]);
				if (FP_infos[8].equals("Yes")) {
					cf.request.addProperty("fld_tagdate", FP_infos[9]);
				} else {
					cf.request.addProperty("fld_tagdate", "");
				}
				if (FP_infos[10].equals("Yes")) {
					cf.request.addProperty("fld_smokedet", FP_infos[10] + "^"
							+ FP_infos[11]);
				} else {
					cf.request.addProperty("fld_smokedet", FP_infos[10]);
				}

				cf.request.addProperty("fld_ssys", FP_infos[12]);
				cf.request.addProperty("fld_scover", FP_infos[13]);
				cf.request.addProperty("fld_salarm", FP_infos[14]);
				if (FP_infos[15].equals("Yes-Based on Areas Inspected")) {
					FP_infos[15] = FP_infos[15].replace(
							"Yes-Based on Areas Inspected",
							"Yes-Based on Areas Inspected");
				}
				cf.request.addProperty("fld_sign", FP_infos[15]);
				System.out
						.println("ExportGCHFireProtectionSurvey" + cf.request);
				sub_total = 50;
				EX_CHKGCH[4] = cf.SoapResponse("ExportGCHFireProtectionSurvey",
						cf.request);
				System.out.println("EX_CHKGCH[4]=" + EX_CHKGCH[4]);
				C_FP.close();
			}
			else
			{
				EX_CHKGCH[4]=true;
			}
		} catch (Exception e) {
			// TODO: handle exception
			EX_CHKGCH[4] = false;
			throw e;
		}
		sub_total = 100;
		setincreament();
		;
		return EX_CHKGCH[4];
	}

	private boolean EX_GCH_SummaryHazards() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		// TODO Auto-generated method stub
		try {
			sub_total = 0;
			cf.CreateARRTable(44);
			sub_insp = "Summary of Hazards & Concerns";
			cf.request = cf.SoapRequest("ExportGCHSummaryHazardsConcerns");
			Cursor C_SH = cf.SelectTablefunction(cf.GCH_SummaryHazardstbl,
					" where fld_srid='" + cf.selectedhomeid + "' ");
			if (C_SH.getCount() >= 1) {
				SH_infos = cf.setvalueToArray(C_SH);

				cf.request.addProperty("Fld_Srid", SH_infos[1]);
				cf.request.addProperty("fld_shzchk", SH_infos[2]);
				cf.request.addProperty("fld_vicious", SH_infos[3]);
				cf.request.addProperty("fld_viciousytxt", SH_infos[4]);
				cf.request.addProperty("fld_horses", SH_infos[5]);
				cf.request.addProperty("fld_horseytxt", SH_infos[6]);
				cf.request.addProperty("fld_overhanging", SH_infos[7]);
				cf.request.addProperty("fld_trampoline", SH_infos[8]);
				cf.request.addProperty("fld_skateboard", SH_infos[9]);
				cf.request.addProperty("fld_bicycle", SH_infos[10]);
				cf.request.addProperty("fld_triphz", SH_infos[11]);
				cf.request.addProperty("fld_triphztxt", SH_infos[12]);
				cf.request.addProperty("fld_unsafe", SH_infos[13]);
				cf.request.addProperty("fld_porchdeck", SH_infos[14]);
				cf.request.addProperty("fld_nonconst", SH_infos[15]);
				cf.request.addProperty("fld_odoor", SH_infos[16]);
				cf.request.addProperty("fld_openfound", SH_infos[17]);
				cf.request.addProperty("fld_wood", SH_infos[18]);
				cf.request.addProperty("fld_excessdb", SH_infos[19]);
				cf.request.addProperty("fld_bop", SH_infos[20]);
				cf.request.addProperty("fld_boptxt", SH_infos[21]);
				cf.request.addProperty("fld_bgdisrepair", SH_infos[22]);
				cf.request.addProperty("fld_visualpd", SH_infos[23]);
				cf.request.addProperty("fld_structp", SH_infos[24]);
				cf.request.addProperty("fld_inoper", SH_infos[25]);
				cf.request.addProperty("fld_recentdw", SH_infos[26]);
				cf.request.addProperty("fld_possiblecdw", SH_infos[27]);
				cf.request.addProperty("fld_ownerccdw", SH_infos[28]);
				cf.request.addProperty("fld_nsssystem", SH_infos[29]);
				cf.request.addProperty("fld_nwsdedic", SH_infos[30]);
				cf.request.addProperty("fld_summaryhzcomments", SH_infos[31]);
				System.out.println("ExportGCHSummaryHazardsConcerns[3]="
						+ cf.request);
				sub_total = 50;
				EX_CHKGCH[3] = cf.SoapResponse(
						"ExportGCHSummaryHazardsConcerns", cf.request);
				System.out.println("EX_CHKGCH[3]=" + EX_CHKGCH[3]);
				C_SH.close();
			}
			else
			{
				EX_CHKGCH[3] =true;
			}

		} catch (Exception e) {
			// TODO: handle exception
			EX_CHKGCH[3] = false;
			throw e;
		}
		sub_total = 100;
		setincreament();
		;
		return EX_CHKGCH[3];
	}

	private boolean EX_GCH_Roof() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		// TODO Auto-generated method stub
		if (EX_Roof_RCT_new("7")) {
			if (EX_ROOF_RCN_new("7")) {
				EX_CHKGCH[2] = true;
			}

		}
		return EX_CHKGCH[2];

	}

	private boolean EX_GCH_PoolFence() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		// TODO Auto-generated method stub
		try {
			sub_total = 0;
			sub_insp = "Perimeter Pool Fence";
			cf.CreateARRTable(42);
			SoapObject pfrequest = cf
					.SoapRequest("ExportGCHPerimeterpoolfence");
			Cursor C_PF = cf.SelectTablefunction(cf.GCH_PoolFencetbl,
					" where fld_srid='" + cf.selectedhomeid + "' ");
			System.out.println("C_PF=" + C_PF.getCount());
			if (C_PF.getCount() >= 1) {
				C_PF.moveToFirst();
				PF_infos = cf.setvalueToArray(C_PF);
				System.out.println("PF_infos=" + PF_infos[0]);
				pfrequest.addProperty("Fld_Srid", PF_infos[1]);
				pfrequest.addProperty("fld_ppfchk", PF_infos[2]);
				if (PF_infos[3].contains("Enclosure > 4FT")
						|| PF_infos[3].contains("Enclosure < 4FT")) {
					PF_infos[3] = PF_infos[3].replace("Enclosure > 4FT",
							"Enclosure GRT 4FT");
					PF_infos[3] = PF_infos[3].replace("Enclosure < 4FT",
							"Enclosure LRT 4FT");
				}
				if (PF_infos[3].contains("&#40;")) {
					String[] s = PF_infos[3].split("&#40;");
					pfrequest.addProperty("fld_ppfence", s[0] + "&#94;Other("
							+ s[1] + ")");
				} else {
					pfrequest.addProperty("fld_ppfence", PF_infos[3]);
				}
				pfrequest.addProperty("fld_ppfenceother", "");
				pfrequest.addProperty("fld_selflatches", PF_infos[5]);
				pfrequest.addProperty("fld_pinstalled", PF_infos[6]);
				pfrequest.addProperty("fld_pfdisrepair", PF_infos[7]);
				System.out.println("pfrequest=" + pfrequest);
				sub_total = 50;
				EX_CHKGCH[1] = cf.SoapResponse("ExportGCHPerimeterpoolfence",
						pfrequest);
				System.out.println("EX_CHKGCH[1]=" + EX_CHKGCH[1]);
				C_PF.close();
			}
			else
			{
				EX_CHKGCH[1] = true;
			}

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("e+" + e.getMessage());
			EX_CHKGCH[1] = false;
			throw e;
		}
		sub_total = 100;
		setincreament();
		;
		return EX_CHKGCH[1];
	}

	private boolean EX_GCH_PoolPresent() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		// TODO Auto-generated method stub
		try {
			sub_total = 0;
			sub_insp = "Swimming Pool Present";
			cf.CreateARRTable(41);
			cf.request = cf.SoapRequest("ExportGCHSwimmingpoolpresent");
			Cursor C_SPP = cf.SelectTablefunction(cf.GCH_PoolPrsenttbl,
					" where fld_srid='" + cf.selectedhomeid + "' ");
			System.out.println("C_PF=" + C_SPP.getCount());
			if (C_SPP.getCount() >= 1) {
				PP_infos = cf.setvalueToArray(C_SPP);
				System.out.println("PF_infos=" + PP_infos[1]);
				cf.request.addProperty("Fld_Srid", PP_infos[1]);
				cf.request.addProperty("Fld_ppchk", PP_infos[2]);
				cf.request.addProperty("Fld_ppground", PP_infos[3]);
				
				if(PP_infos[4].equals(""))
				{
					cf.request.addProperty("Fld_htpresent", "No");
				}
				else
				{
					cf.request.addProperty("Fld_htpresent", PP_infos[4]);
				}
				
				
				
				if (PP_infos[4].equals("Yes")) {
					cf.request.addProperty("Fld_htcovered", PP_infos[5]);
					cf.request.addProperty("Fld_ppstructure", PP_infos[6]);

				} else {
					cf.request.addProperty("Fld_htcovered", "");
					cf.request.addProperty("Fld_ppstructure", "");
				}
				cf.request.addProperty("Fld_egppresent", PP_infos[7]);
				cf.request.addProperty("Fld_pspresent", PP_infos[8]);
				cf.request.addProperty("Fld_dbpresent", PP_infos[9]);
				cf.request.addProperty("Fld_ppenclosure", PP_infos[10]);
				cf.request.addProperty("Fld_pedisrepair", PP_infos[11]);
				sub_total = 50;
				System.out.println("GCH="+cf.request);
				EX_CHKGCH[0] = cf.SoapResponse("ExportGCHSwimmingpoolpresent",
						cf.request);
				System.out.println("EX_CHKGCH[0]=" + EX_CHKGCH[0]);
				C_SPP.close();
			}
			else
			{
				EX_CHKGCH[0] = true;
			}

		} catch (Exception e) {
			// TODO: handle exception
			EX_CHKGCH[0] = false;
			throw e;
		}
		sub_total = 100;
		setincreament();
		;
		return EX_CHKGCH[0];
	}

	/*** GCH export ends */
	/*** Sinkhole export starts */
	private boolean EX_SH_OBS3() throws SocketException, NetworkErrorException,
			TimeoutException, IOException, XmlPullParserException, Exception,
			NullPointerException {
		// TODO Auto-generated method stub
		try {
			sub_total = 0;
			sub_insp = "Observations(14 - 22)";
			cf.CreateARRTable(54);
			cf.request = cf.SoapRequest("SinkholeOBSERVATION3");
			Cursor C_SOB3 = cf.SelectTablefunction(cf.SINK_Obs3tbl,
					" where fld_srid='" + cf.selectedhomeid + "' ");
			if (C_SOB3.getCount() >= 1) {
				C_SOB3.moveToFirst();
				String[] SOB3_infos = cf.setvalueToArray(C_SOB3);

				cf.request.addProperty("Fld_Srid", SOB3_infos[1]);
				cf.request.addProperty("InteriorWallCrackNoted", SOB3_infos[2]);
				if (SOB3_infos[3].contains("&#40;")) {
					String[] o1 = SOB3_infos[3].split("&#40;");
					cf.request.addProperty("InteriorWallCrackOption", o1[0]
							+ "&#94;Other(" + o1[1] + ")");
				} else {
					cf.request.addProperty("InteriorWallCrackOption",
							SOB3_infos[3]);
				}
				cf.request.addProperty("InteriorWallCrackComments",
						SOB3_infos[4]);
				cf.request.addProperty("WallSlabCrackNoted", SOB3_infos[5]);
				if (SOB3_infos[6].contains("&#40;")) {
					String[] o2 = SOB3_infos[6].split("&#40;");
					cf.request.addProperty("WallSlabCrackOption", o2[0]
							+ "&#94;Other(" + o2[1] + ")");
				} else {
					cf.request
							.addProperty("WallSlabCrackOption", SOB3_infos[6]);
				}
				cf.request.addProperty("WallSlabCrackComments", SOB3_infos[7]);
				cf.request.addProperty("InteriorCeilingCrackNoted",
						SOB3_infos[8]);
				if (SOB3_infos[9].contains("&#40;")) {
					String[] o3 = SOB3_infos[9].split("&#40;");
					cf.request.addProperty("InteriorCeilingCrackOption", o3[0]
							+ "&#94;Other(" + o3[1] + ")");
				} else {
					cf.request.addProperty("InteriorCeilingCrackOption",
							SOB3_infos[9]);
				}
				cf.request.addProperty("InteriorCeilingCrackComments",
						SOB3_infos[10]);
				cf.request.addProperty("InteriorFloorCrackNoted",
						SOB3_infos[11]);
				if (SOB3_infos[12].contains("&#40;")) {
					String[] o4 = SOB3_infos[12].split("&#40;");
					cf.request.addProperty("InteriorFloorCrackOption", o4[0]
							+ "&#94;Other(" + o4[1] + ")");
				} else {
					cf.request.addProperty("InteriorFloorCrackOption",
							SOB3_infos[12]);
				}
				cf.request.addProperty("InteriorFloorCrackComments",
						SOB3_infos[13]);
				cf.request
						.addProperty("WindowFrameOutofSquare", SOB3_infos[14]);
				if (SOB3_infos[15].contains("&#40;")) {
					String[] o5 = SOB3_infos[15].split("&#40;");
					cf.request.addProperty("WindowFrameOutofSquareOption",
							o5[0] + "&#94;Other(" + o5[1] + ")");
				} else {
					cf.request.addProperty("WindowFrameOutofSquareOption",
							SOB3_infos[15]);
				}
				cf.request.addProperty("WindowFrameOutofSquareComments",
						SOB3_infos[16]);
				cf.request.addProperty("FloorSlopingNoted", SOB3_infos[17]);
				if (SOB3_infos[18].contains("&#40;")) {
					String[] o6 = SOB3_infos[18].split("&#40;");
					cf.request.addProperty("FloorSlopingOption", o6[0]
							+ "&#94;Other(" + o6[1] + ")");
				} else {
					cf.request
							.addProperty("FloorSlopingOption", SOB3_infos[18]);
				}
				cf.request.addProperty("FloorSlopingComments", SOB3_infos[19]);
				cf.request.addProperty("CrackedGlazingNoted", SOB3_infos[20]);
				if (SOB3_infos[21].contains("&#40;")) {
					String[] o7 = SOB3_infos[21].split("&#40;");
					cf.request.addProperty("CrackedGlazingOption", o7[0]
							+ "&#94;Other(" + o7[1] + ")");
				} else {
					cf.request.addProperty("CrackedGlazingOption",
							SOB3_infos[21]);
				}
				cf.request
						.addProperty("CrackedGlazingComments", SOB3_infos[22]);
				cf.request.addProperty("PreviousCorrectiveMeasureNoted",
						SOB3_infos[23]);
				if (SOB3_infos[24].contains("&#40;")) {
					String[] o8 = SOB3_infos[24].split("&#40;");
					cf.request.addProperty("PreviousCorrectiveMeasureOption",
							o8[0] + "&#94;Other(" + o8[1] + ")");
				} else {
					cf.request.addProperty("PreviousCorrectiveMeasureOption",
							SOB3_infos[24]);
				}
				cf.request.addProperty("PreviousCorrectiveMeasureComments",
						SOB3_infos[25]);
				cf.request.addProperty("BindingDoorOpeningNoted",
						SOB3_infos[26]);
				if (SOB3_infos[27].contains("&#40;")) {
					String[] o9 = SOB3_infos[27].split("&#40;");
					cf.request.addProperty("BindingDoorOpeningOption", o9[0]
							+ "&#94;Other(" + o9[1] + ")");
				} else {
					cf.request.addProperty("BindingDoorOpeningOption",
							SOB3_infos[27]);
				}
				cf.request.addProperty("BindingDoorOpeningComments",
						SOB3_infos[28]);
				cf.request.addProperty("Observation14chk", 1);
				cf.request.addProperty("Observation15chk", 1);
				cf.request.addProperty("Observation16chk", 1);
				cf.request.addProperty("Observation17chk", 1);
				cf.request.addProperty("Observation18chk", 1);
				cf.request.addProperty("Observation19chk", 1);
				cf.request.addProperty("Observation20chk", 1);
				cf.request.addProperty("Observation21chk", 1);
				cf.request.addProperty("Observation22chk", 1);
				System.out.println("SinkObservation3result= " + cf.request);
				sub_total = 50;
				EX_CHKSH[4] = cf.SoapResponse("SinkholeOBSERVATION3",
						cf.request);
				System.out.println("EX_CHKSH[4]= " + EX_CHKSH[4]);
				C_SOB3.close();
			}
			else
			{
				EX_CHKSH[4] =true;
			}

		} catch (Exception e) {
			// TODO: handle exception
			EX_CHKSH[4] = false;
			throw e;
		}
		sub_total = 100;
		setincreament();
		;
		return EX_CHKSH[4];
	}

	private boolean EX_SH_OBS2() throws SocketException, NetworkErrorException,
			TimeoutException, IOException, XmlPullParserException, Exception,
			NullPointerException {
		// TODO Auto-generated method stub
		try {
			sub_total = 0;
			sub_insp = "Observations(8 - 13)";
			cf.CreateARRTable(53);
			cf.request = cf.SoapRequest("SinkholeOBSERVATION2");
			Cursor C_SOB2 = cf.SelectTablefunction(cf.SINK_Obs2tbl,
					" where fld_srid='" + cf.selectedhomeid + "' ");
			if (C_SOB2.getCount() >= 1) {
				C_SOB2.moveToFirst();
				String[] SOB2_infos = cf.setvalueToArray(C_SOB2);

				cf.request.addProperty("Fld_Srid", SOB2_infos[1]);
				cf.request.addProperty("PoolDeckSlabNoted", SOB2_infos[2]);
				;
				cf.request.addProperty("PoolDeckSlabComments", SOB2_infos[3]);
				cf.request.addProperty("PoolShellPlumbLevel", SOB2_infos[4]);
				cf.request.addProperty("PoolShellPlumbLevelComments",
						SOB2_infos[5]);
				cf.request.addProperty("ExcessiveSettlement", SOB2_infos[6]);
				if (SOB2_infos[7].contains("&#40;")) {
					String[] o1 = SOB2_infos[7].split("&#40;");
					cf.request.addProperty("ExcessiveSettlementTypeofCrack",
							o1[0] + "&#94;Other(" + o1[1] + ")");
				} else {
					cf.request.addProperty("ExcessiveSettlementTypeofCrack",
							SOB2_infos[7]);
				}
				if (SOB2_infos[8].contains("&#40;")) {
					String[] o2 = SOB2_infos[8].split("&#40;");
					cf.request.addProperty("ExcessiveSettlementLocation", o2[0]
							+ "&#94;Other(" + o2[1] + ")");
				} else {
					cf.request.addProperty("ExcessiveSettlementLocation",
							SOB2_infos[8]);
				}
				if (SOB2_infos[9].contains("&#40;")) {
					String[] o3 = SOB2_infos[9].split("&#40;");
					cf.request.addProperty("ExcessiveSettlementWidthofCrack",
							o3[0] + "&#94;Other(" + o3[1] + ")");
				} else {
					cf.request.addProperty("ExcessiveSettlementWidthofCrack",
							SOB2_infos[9]);
				}
				cf.request.addProperty("ExcessiveSettlementLengthofCrack",
						SOB2_infos[10]);
				cf.request.addProperty("ExcessiveSettlementCleanliness",
						SOB2_infos[11]);
				if (SOB2_infos[12].contains("&#40;")) {
					String[] o4 = SOB2_infos[12].split("&#40;");
					cf.request.addProperty("ExcessiveSettlementProbableCause",
							o4[0] + "&#94;Other(" + o4[1] + ")");
				} else {
					cf.request.addProperty("ExcessiveSettlementProbableCause",
							SOB2_infos[12]);
				}
				cf.request.addProperty("ExcessiveSettlementComments",
						SOB2_infos[13]);
				cf.request.addProperty("WallLeaningNoted", SOB2_infos[14]);
				cf.request.addProperty("WallLeaningComments", SOB2_infos[15]);
				cf.request.addProperty("WallsVisiblyNotLevel", SOB2_infos[16]);
				cf.request.addProperty("WallsVisiblyNotLevelComments",
						SOB2_infos[17]);
				cf.request.addProperty("WallsVisiblyBulgingNoted",
						SOB2_infos[18]);
				cf.request.addProperty("WallsVisiblyBulgingComments",
						SOB2_infos[19]);
				cf.request.addProperty("OpeningsOutofSquare", SOB2_infos[20]);
				cf.request.addProperty("OpeningsOutofSquareComments",
						SOB2_infos[21]);
				cf.request.addProperty("DamagedFinishesNoted", SOB2_infos[22]);
				cf.request.addProperty("DamagedFinishesComments",
						SOB2_infos[23]);
				cf.request.addProperty("SeperationCrackNoted", SOB2_infos[24]);
				if (SOB2_infos[25].contains("&#40;")) {
					String[] o5 = SOB2_infos[25].split("&#40;");
					cf.request.addProperty("SeperationCrackTypeofCrack", o5[0]
							+ "&#94;Other(" + o5[1] + ")");
				} else {
					cf.request.addProperty("SeperationCrackTypeofCrack",
							SOB2_infos[25]);
				}
				cf.request.addProperty("SeperationCrackComments",
						SOB2_infos[26]);
				cf.request.addProperty("ExteriorOpeningCracksNoted",
						SOB2_infos[27]);
				if (SOB2_infos[28].contains("&#40;")) {
					String[] o6 = SOB2_infos[28].split("&#40;");
					cf.request.addProperty("ExteriorOpeningCracksTypeofCrack",
							o6[0] + "&#94;Other(" + o6[1] + ")");
				} else {
					cf.request.addProperty("ExteriorOpeningCracksTypeofCrack",
							SOB2_infos[28]);
				}
				if (SOB2_infos[29].contains("&#40;")) {
					String[] o7 = SOB2_infos[29].split("&#40;");
					cf.request.addProperty("ExteriorOpeningCracksLocation",
							o7[0] + "&#94;Other(" + o7[1] + ")");
				} else {
					cf.request.addProperty("ExteriorOpeningCracksLocation",
							SOB2_infos[29]);
				}
				if (SOB2_infos[30].contains("&#40;")) {
					String[] o8 = SOB2_infos[30].split("&#40;");
					cf.request.addProperty("ExteriorOpeningCracksWidthofCrack",
							o8[0] + "&#94;Other(" + o8[1] + ")");
				} else {
					cf.request.addProperty("ExteriorOpeningCracksWidthofCrack",
							SOB2_infos[30]);
				}
				cf.request.addProperty("ExteriorOpeningCracksLengthofCrack",
						SOB2_infos[31]);
				cf.request.addProperty("ExteriorOpeningCracksCleanliness",
						SOB2_infos[32]);
				if (SOB2_infos[33].contains("&#40;")) {
					String[] o9 = SOB2_infos[33].split("&#40;");
					cf.request.addProperty(
							"ExteriorOpeningCracksProbableCause", o9[0]
									+ "&#94;Other(" + o9[1] + ")");
				} else {
					cf.request.addProperty(
							"ExteriorOpeningCracksProbableCause",
							SOB2_infos[33]);
				}
				cf.request.addProperty("ExteriorOpeningCracksComments",
						SOB2_infos[34]);
				cf.request.addProperty("ObservationSettlementNoted",
						SOB2_infos[35]);
				if (SOB2_infos[36].contains("&#40;")) {
					String[] o10 = SOB2_infos[36].split("&#40;");
					cf.request.addProperty("ObservationSettlementTypeofCrack",
							o10[0] + "&#94;Other(" + o10[1] + ")");
				} else {
					cf.request.addProperty("ObservationSettlementTypeofCrack",
							SOB2_infos[36]);
				}
				if (SOB2_infos[37].contains("&#40;")) {
					String[] o11 = SOB2_infos[37].split("&#40;");
					cf.request.addProperty("ObservationSettlementLocation",
							o11[0] + "&#94;Other(" + o11[1] + ")");
				} else {
					cf.request.addProperty("ObservationSettlementLocation",
							SOB2_infos[37]);
				}
				if (SOB2_infos[38].contains("&#40;")) {
					String[] o12 = SOB2_infos[38].split("&#40;");
					cf.request.addProperty("ObservationSettlementWidthofCrack",
							o12[0] + "&#94;Other(" + o12[1] + ")");
				} else {
					cf.request.addProperty("ObservationSettlementWidthofCrack",
							SOB2_infos[38]);
				}
				cf.request.addProperty("ObservationSettlementLengthofCrack",
						SOB2_infos[39]);
				cf.request.addProperty("ObservationSettlementCleanliness",
						SOB2_infos[40]);
				if (SOB2_infos[41].contains("&#40;")) {
					String[] o13 = SOB2_infos[41].split("&#40;");
					cf.request.addProperty(
							"ObservationSettlementProbableCause", o13[0]
									+ "&#94;Other(" + o13[1] + ")");
				} else {
					cf.request.addProperty(
							"ObservationSettlementProbableCause",
							SOB2_infos[41]);
				}
				cf.request.addProperty("ObservationSettlementComments",
						SOB2_infos[42]);
				cf.request.addProperty("Observation8checked", 1);
				cf.request.addProperty("Observation9checked", 1);
				cf.request.addProperty("Observation10checked", 1);
				cf.request.addProperty("Observation11checked", 1);
				cf.request.addProperty("Observation12checked", 1);
				cf.request.addProperty("Observation13checked", 1);
				System.out.println("SinkObservation2result= " + cf.request);
				sub_total = 50;
				EX_CHKSH[3] = cf.SoapResponse("SinkholeOBSERVATION2",
						cf.request);
				System.out.println("EX_CHKSH[3]= " + EX_CHKSH[3]);
				C_SOB2.close();
			}
			else
			{
				EX_CHKSH[3]= true;
			}

		} catch (Exception e) {
			// TODO: handle exception
			EX_CHKSH[3] = false;
			throw e;
		}
		sub_total = 100;
		setincreament();
		;
		return EX_CHKSH[3];
	}

	private boolean EX_SH_OBS1Tree() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		// TODO Auto-generated method stub
		if (cf.doesTblExist(cf.SINK_Obstreetbl)) {
			try {
				sub_total = 50;
				sub_insp = "Observations(1 - 7)";
				Cursor C_SOB1T = cf.SelectTablefunction(cf.SINK_Obstreetbl,
						" where fld_srid='" + cf.selectedhomeid + "' ");
				System.out.println("tree= " + C_SOB1T.getCount());
				if (C_SOB1T.getCount() >= 1) {
					int sob1 = 50 / C_SOB1T.getCount();
					C_SOB1T.moveToFirst();
					for (int i = 0; i < C_SOB1T.getCount(); i++) {
						cf.request = cf.SoapRequest("SinkholeLARGETREE");
						SoapObject request = new SoapObject(cf.NAMESPACE,
								"SinkholeLARGETREE");
						SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
								SoapEnvelope.VER11);
						envelope.dotNet = true;

						cf.request
								.addProperty("Fld_Srid", cf.decode(C_SOB1T
										.getString(C_SOB1T
												.getColumnIndex("fld_srid"))));
						cf.request.addProperty("Width", cf.decode(C_SOB1T
								.getString(C_SOB1T.getColumnIndex("Width"))));
						cf.request.addProperty("Height", cf.decode(C_SOB1T
								.getString(C_SOB1T.getColumnIndex("Height"))));
						if (cf.decode(
								C_SOB1T.getString(C_SOB1T
										.getColumnIndex("Location"))).contains(
								"&#40;")) {
							String[] o1 = cf.decode(
									C_SOB1T.getString(C_SOB1T
											.getColumnIndex("Location")))
									.split("&#40;");
							cf.request.addProperty("Location", o1[0]
									+ "&#94;Other(" + o1[1] + ")");
						} else {
							String str = cf.decode(
								C_SOB1T.getString(C_SOB1T
										.getColumnIndex("Location")));
						if(!str.contains("Front Elevation") && !str.contains("Rear Elevation")
								&& !str.contains("Right Elevation")&& !str.contains("Left Elevation"))
						{
							cf.request.addProperty("Location","Other("+str+")");
						}
						else
						{
						  cf.request.addProperty("Location", cf
								.decode(C_SOB1T.getString(C_SOB1T
										.getColumnIndex("Location"))));
						}}
						cf.request.addProperty("ROWID", C_SOB1T
								.getString(C_SOB1T.getColumnIndex("OB1TId")));

						envelope.setOutputSoapObject(cf.request);

						System.out.println("SinkholeLARGETREE= " + cf.request);
						envelope.setOutputSoapObject(cf.request);
						HttpTransportSE androidHttpTransport = new HttpTransportSE(
								cf.URL_ARR);
						androidHttpTransport.call(cf.NAMESPACE
								+ "SinkholeLARGETREE", envelope);
						String result = envelope.getResponse().toString();
						EX_CHKSH[2] = Boolean.parseBoolean(result);

						System.out.println("EX_CHKCD[2]= " + EX_CHKCD[2]);

						C_SOB1T.moveToNext();
						sub_total += sob1;
					}
					C_SOB1T.close();
				} else {
					EX_CHKSH[2] = true;
				}

			} catch (Exception e) {
				// TODO: handle exception
				EX_CHKSH[2] = false;
				throw e;
			}
		} else {
			EX_CHKSH[2] = true;
		}
		sub_total = 100;
		setincreament();
		;
		return EX_CHKSH[2];
	}

	private boolean EX_SH_OBS1() throws SocketException, NetworkErrorException,
			TimeoutException, IOException, XmlPullParserException, Exception,
			NullPointerException {
		// TODO Auto-generated method stub
		try {
			sub_total = 0;
			sub_insp = "Observations(1 - 7)";
			cf.request = cf.SoapRequest("SinkholeOBSERVATION1");
			cf.CreateARRTable(52);
			Cursor C_SOB1 = cf.SelectTablefunction(cf.SINK_Obs1tbl,
					" where fld_srid='" + cf.selectedhomeid + "' ");
			if (C_SOB1.getCount() >= 1) {
				C_SOB1.moveToFirst();
				String[] SOB1_infos = cf.setvalueToArray(C_SOB1);
				cf.request.addProperty("Fld_Srid", SOB1_infos[1]);

				cf.request.addProperty("IrregularLandSurface", SOB1_infos[2]);
				if (SOB1_infos[3].contains("&#40;")) {
					String[] o1 = SOB1_infos[3].split("&#40;");
					cf.request.addProperty("IrregularLandSurfaceLocation",
							o1[0] + "&#94;Other(" + o1[1] + ")");
				} else {
					cf.request.addProperty("IrregularLandSurfaceLocation",
							SOB1_infos[3]);
				}
				cf.request.addProperty("IrregularLandSurfaceComments",
						SOB1_infos[4]);

				cf.request.addProperty("VisibleBuriedDebris", SOB1_infos[5]);
				if (SOB1_infos[6].contains("&#40;")) {
					String[] o2 = SOB1_infos[6].split("&#40;");
					cf.request.addProperty("VisibleBuriedDebrisLocation", o2[0]
							+ "&#94;Other(" + o2[1] + ")");
				} else {
					cf.request.addProperty("VisibleBuriedDebrisLocation",
							SOB1_infos[6]);
				}
				cf.request.addProperty("VisibleBuriedDebrisComments",
						SOB1_infos[7]);

				cf.request.addProperty("IrregularLandSurfaceAdjacent",
						SOB1_infos[8]);
				if (SOB1_infos[9].contains("&#40;")) {
					String[] o3 = SOB1_infos[9].split("&#40;");
					cf.request.addProperty(
							"IrregularLandSurfaceAdjacentLocation", o3[0]
									+ "&#94;Other(" + o3[1] + ")");
				} else {
					cf.request.addProperty(
							"IrregularLandSurfaceAdjacentLocation",
							SOB1_infos[9]);
				}
				cf.request.addProperty("IrregularLandSurfaceAdjacentComments",
						SOB1_infos[10]);

				cf.request.addProperty("SoilCollapse", SOB1_infos[11]);
				if (SOB1_infos[12].contains("&#40;")) {
					String[] o4 = SOB1_infos[12].split("&#40;");
					cf.request.addProperty("SoilCollapseLocation", o4[0]
							+ "&#94;Other(" + o4[1] + ")");
				} else {
					cf.request.addProperty("SoilCollapseLocation",
							SOB1_infos[12]);
				}
				cf.request.addProperty("SoilCollapseComments", SOB1_infos[13]);
				cf.request.addProperty("SoilErosionAroundFoundation",
						SOB1_infos[14]);
				if (SOB1_infos[15].contains("&#40;")) {
					String[] o5 = SOB1_infos[15].split("&#40;");
					cf.request.addProperty("SoilErosionAroundFoundationOption",
							o5[0] + "&#94;Other(" + o5[1] + ")");
				} else {
					cf.request.addProperty("SoilErosionAroundFoundationOption",
							SOB1_infos[15]);
				}
				cf.request.addProperty("SoilErosionAroundFoundationComments",
						SOB1_infos[16]);
				cf.request.addProperty("DrivewaysCracksNoted", SOB1_infos[17]);
				if (SOB1_infos[18].contains("&#40;")) {
					String[] o6 = SOB1_infos[18].split("&#40;");
					cf.request.addProperty("DrivewaysCracksOption", o6[0]
							+ "&#94;Other(" + o6[1] + ")");
				} else {
					cf.request.addProperty("DrivewaysCracksOption",
							SOB1_infos[18]);
				}
				cf.request.addProperty("DrivewaysCracksComments",
						SOB1_infos[19]);
				cf.request
						.addProperty("UpliftDrivewaysSurface", SOB1_infos[20]);
				if (SOB1_infos[21].contains("&#40;")) {
					String[] o7 = SOB1_infos[21].split("&#40;");
					cf.request.addProperty("UpliftDrivewaysSurfaceOption",
							o7[0] + "&#94;Other(" + o7[1] + ")");
				} else {
					cf.request.addProperty("UpliftDrivewaysSurfaceOption",
							SOB1_infos[21]);
				}
				cf.request.addProperty("UpliftDrivewaysSurfaceComments",
						SOB1_infos[22]);
				cf.request.addProperty("LargeTree", SOB1_infos[23]);
				cf.request.addProperty("LargeTreeComments", SOB1_infos[24]);
				cf.request.addProperty("CypressTree", SOB1_infos[25]);
				if (SOB1_infos[26].contains("&#40;")) {
					String[] o8 = SOB1_infos[26].split("&#40;");
					cf.request.addProperty("CypressTreeLocation", o8[0]
							+ "&#94;Other(" + o8[1] + ")");
				} else {
					cf.request.addProperty("CypressTreeLocation",
							SOB1_infos[26]);
				}
				cf.request.addProperty("CypressTreeComments", SOB1_infos[27]);
				cf.request.addProperty("LargeTreesRemoved", SOB1_infos[28]);
				if (SOB1_infos[29].contains("&#40;")) {
					String[] o9 = SOB1_infos[29].split("&#40;");
					cf.request.addProperty("LargeTreesRemovedLocation", o9[0]
							+ "&#94;Other(" + o9[1] + ")");
				} else {
					cf.request.addProperty("LargeTreesRemovedLocation",
							SOB1_infos[29]);
				}
				cf.request.addProperty("LargeTreesRemovedComments",
						SOB1_infos[30]);
				cf.request.addProperty("FoundationCrackNoted", SOB1_infos[31]);
				if (SOB1_infos[32].contains("&#40;")) {
					String[] o10 = SOB1_infos[32].split("&#40;");
					cf.request.addProperty("FoundationTypeofCrack", o10[0]
							+ "&#94;Other(" + o10[1] + ")");
				} else {
					cf.request.addProperty("FoundationTypeofCrack",
							SOB1_infos[32]);
				}
				if (SOB1_infos[33].contains("&#40;")) {
					String[] o11 = SOB1_infos[33].split("&#40;");
					cf.request.addProperty("FoundationLocation", o11[0]
							+ "&#94;Other(" + o11[1] + ")");
				} else {
					cf.request
							.addProperty("FoundationLocation", SOB1_infos[33]);
				}
				if (SOB1_infos[34].contains("&#40;")) {
					String[] o12 = SOB1_infos[34].split("&#40;");
					cf.request.addProperty("FoundationWidthofCrack", o12[0]
							+ "&#94;Other(" + o12[1] + ")");
					System.out.println("foundation width cracks" + o12[0]
							+ "&#94;Other(" + o12[1] + ")");
				} else {
					cf.request.addProperty("FoundationWidthofCrack",
							SOB1_infos[34]);
					System.out.println("foundation width cracks="
							+ SOB1_infos[34]);
				}
				cf.request.addProperty("FoundationLengthofCrack",
						SOB1_infos[35]);
				cf.request.addProperty("FoundationCleanliness", SOB1_infos[36]);
				if (SOB1_infos[37].contains("&#40;")) {
					String[] o13 = SOB1_infos[37].split("&#40;");
					cf.request.addProperty("FoundationProbableCause", o13[0]
							+ "&#94;Other(" + o13[1] + ")");
				} else {
					cf.request.addProperty("FoundationProbableCause",
							SOB1_infos[37]);
				}
				cf.request.addProperty("FoundationComments", SOB1_infos[38]);
				cf.request.addProperty("RetainingWallsServiceable",
						SOB1_infos[39]);
				cf.request.addProperty("RetainingWallsServiceOption",
						SOB1_infos[40]);
				cf.request.addProperty("RetainingWallsServiceComments",
						SOB1_infos[41]);
				cf.request.addProperty("Observation1checked", 1);
				cf.request.addProperty("Observation2checked", 1);
				cf.request.addProperty("Observation3checked", 1);
				cf.request.addProperty("Observation4checked", 1);
				cf.request.addProperty("Observation5checked", 1);
				cf.request.addProperty("Observation6checked", 1);
				cf.request.addProperty("Observation7checked", 1);
				System.out.println("SinkObservation1result= " + cf.request);
				sub_total = 25;
				EX_CHKSH[1] = cf.SoapResponse("SinkholeOBSERVATION1",
						cf.request);
				System.out.println("EX_CHKSH[1]= " + EX_CHKSH[1]);
				C_SOB1.close();
			}
			else
			{
				EX_CHKSH[1] = true;
			}

		} catch (Exception e) {
			// TODO: handle exception
			EX_CHKSH[1] = false;
			throw e;
		}
		sub_total = 50;
		return EX_CHKSH[1];
	}

	private boolean EX_SH_SinkSummary() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		// TODO Auto-generated method stub
		try {
			sub_total = 0;
			sub_insp = "Summary Questions";
			cf.CreateARRTable(51);
			cf.request = cf.SoapRequest("SinkholeSUMMARYQUESTIONS");
			Cursor C_SS = cf.SelectTablefunction(cf.SINK_Summarytbl,
					" where fld_srid='" + cf.selectedhomeid + "' ");
			if (C_SS.getCount() >= 1) {
				C_SS.moveToFirst();
				String[] SS_infos = cf.setvalueToArray(C_SS);

				cf.request.addProperty("Fld_Srid", SS_infos[1]);
				cf.request.addProperty("fld_plocated", SS_infos[2]);
				cf.request.addProperty("fld_aproperty", SS_infos[3]);
				cf.request.addProperty("fld_sumcomments", SS_infos[4]);
				cf.request.addProperty("fld_hopresent", SS_infos[5]);
				cf.request.addProperty("fld_hoq1", SS_infos[6]);
				cf.request.addProperty("fld_hoq2", SS_infos[7]);
				cf.request.addProperty("fld_hoq3", SS_infos[8]);
				cf.request.addProperty("fld_hoq4", SS_infos[9]);
				cf.request.addProperty("fld_hoq5", SS_infos[10]);
				cf.request.addProperty("fld_hoq6", SS_infos[11]);
				cf.request.addProperty("fld_hoq7", SS_infos[12]);
				cf.request.addProperty("fld_hocomments", SS_infos[13]);
				System.out.println("SinkSummaryresult= " + cf.request);
				sub_total = 50;
				EX_CHKSH[0] = cf.SoapResponse("SinkholeSUMMARYQUESTIONS",
						cf.request);
				System.out.println("EX_CHKSH[0]= " + EX_CHKSH[0]);
				C_SS.close();
			}
			else
			{
				EX_CHKSH[0]=true;
			}

		} catch (Exception e) {
			// TODO: handle exception
			EX_CHKSH[0] = false;
			throw e;
		}
		sub_total = 100;
		setincreament();
		;
		return EX_CHKSH[0];
	}

	/*** Sinkhole export ends */
	/*** Chinese drywall export starts */
	private boolean EX_CD_Attic() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		// TODO Auto-generated method stub
		String result = "";
		try {
			sub_total = 0;
			sub_insp = "Attic System";
			cf.CreateARRTable(71);
			Cursor C_CDAT = cf.SelectTablefunction(cf.DRY_attic,
					" where fld_srid='" + cf.selectedhomeid + "' ");
			if (C_CDAT.getCount() >= 1) {
				int atc = 100 / C_CDAT.getCount();
				C_CDAT.moveToFirst();
				for (int i = 0; i < C_CDAT.getCount(); i++) {
					cf.request = cf.SoapRequest("ChinesDryWallAttic");

					cf.request
							.addProperty("Fld_Srid", C_CDAT.getString(C_CDAT
									.getColumnIndex("fld_srid")));
					cf.request.addProperty("OrderID",(i+1));
					cf.request
							.addProperty("atticnum", C_CDAT.getString(C_CDAT
									.getColumnIndex("atticnum")));
					cf.request.addProperty("Accesspoint", C_CDAT
							.getString(C_CDAT.getColumnIndex("Accesspoint")));
					cf.request.addProperty("Accesspointotr",
							cf.decode(C_CDAT.getString(C_CDAT
									.getColumnIndex("Accesspointotr"))));
					cf.request.addProperty("Locationsobserved", cf
							.decode(C_CDAT.getString(C_CDAT
									.getColumnIndex("Locationsobserved"))));

					System.out.println("Atticresult= " + cf.request);
					result += cf.SoapResponse("ChinesDryWallAttic", cf.request)
							+ "~";

					System.out.println("EX_CHKCD[4]= " + result);

					C_CDAT.moveToNext();
					sub_total += atc;
				}
				C_CDAT.close();
			}
			else
			{
				return true;
			}
		} catch (Exception e) {
			// TODO: handle exception
			EX_CHKCD[4] = false;
			result += "false~";
			throw e;
		}
		sub_total = 100;
		setincreament();
		;
		if (result.contains("false"))
			return false;
		else
			return true;

		// return EX_CHKCD[4];
	}

	private boolean EX_CD_Appliance() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		// TODO Auto-generated method stub
		String result = "";
		try {
			sub_total = 0;
			sub_insp = "Internal Appliance System(s)";
			cf.CreateARRTable(73);
			Cursor C_CDA = cf.SelectTablefunction(cf.DRY_appliance,
					" where fld_srid='" + cf.selectedhomeid + "' ");
			if (C_CDA.getCount() >= 1) {
				int iac = 100 / C_CDA.getCount();
				current_val_cur = C_CDA;

				C_CDA.moveToFirst();
				for (int i = 0; i < C_CDA.getCount(); i++) {
					current_pos = i;
					cf.request = cf.SoapRequest("InternalAppliance");
					Boolean file_ex=true;
					if(!cf.decode(C_CDA.getString(C_CDA.getColumnIndex("appimg"))).equals("") )
					{
						File f=new File(cf.decode(C_CDA.getString(C_CDA	.getColumnIndex("appimg"))));
						if(f!=null)
						{
						if(f.exists())
							file_ex=true;
						else
							file_ex=false;
						}
						else
							file_ex=false;
					}
					else
					{
						file_ex=true;
					}
					if ( file_ex) {
						String temppath[] = cf
								.decode(C_CDA.getString(C_CDA
										.getColumnIndex("appimg"))).split("/");
						String tests = temppath[temppath.length - 1];

						bitmap = ShrinkBitmap(cf.decode(C_CDA.getString(C_CDA
								.getColumnIndex("appimg"))), 400, 400);
						
						MarshalBase64 marshal = new MarshalBase64();
						marshal.register(cf.envelope);
						
						
						//System.out.println("raw=" + raw);

						SoapObject request = new SoapObject(cf.NAMESPACE,
								"InternalAppliance");
						SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
								SoapEnvelope.VER11);
						envelope.dotNet = true;
						if(bitmap!=null)
						{
							ByteArrayOutputStream out = new ByteArrayOutputStream();
							bitmap.compress(CompressFormat.PNG, 100, out);
							byte[] raw = out.toByteArray();
							cf.request.addProperty("appimg", raw);
						}
						else{
							cf.request.addProperty("appimg", "");	
						}
						cf.request.addProperty("Fld_Srid", C_CDA
								.getString(C_CDA.getColumnIndex("fld_srid")));
						cf.request.addProperty("OrderID",(i+1));
						cf.request
								.addProperty("appnum", C_CDA.getString(C_CDA
										.getColumnIndex("appnum")));
						cf.request
								.addProperty("apptyp", C_CDA.getString(C_CDA
										.getColumnIndex("apptyp")));
						cf.request.addProperty("apptypotr", cf.decode(C_CDA
								.getString(C_CDA.getColumnIndex("apptypotr"))));
						cf.request.addProperty("unitacess", C_CDA
								.getString(C_CDA.getColumnIndex("unitacess")));
						cf.request.addProperty("obscorrosion",
								C_CDA.getString(C_CDA
										.getColumnIndex("obscorrosion")));
						cf.request.addProperty("recentrepairs", C_CDA
								.getString(C_CDA
										.getColumnIndex("recentrepairs")));
						cf.request
								.addProperty("coppercorrs", C_CDA
										.getString(C_CDA
												.getColumnIndex("coppercorrs")));
						cf.request.addProperty("darkstains", C_CDA
								.getString(C_CDA.getColumnIndex("darkstains")));
						
						cf.request.addProperty("imgname", tests);

						envelope.setOutputSoapObject(cf.request);
						marshal.register(envelope);

						System.out.println("APPlianceresult= " + cf.request);
						envelope.setOutputSoapObject(cf.request);
						HttpTransportSE androidHttpTransport = new HttpTransportSE(
								cf.URL_ARR);
						androidHttpTransport.call(cf.NAMESPACE
								+ "InternalAppliance", envelope);
						result += envelope.getResponse().toString() + "~";
						// EX_CHKCD[3] = Boolean.parseBoolean(result);

						System.out.println("EX_CHKCD[3]= " + result);

					} else {
						Error_tracker += "Please check the following Image is available in "
								+ cf.All_insp_list[9]
								+ "  Application type "
								+ C_CDA.getString(C_CDA
										.getColumnIndex("apptyp"))
								+ ", image Path="
								+ cf.decode(C_CDA.getString(C_CDA
										.getColumnIndex("appimg"))) + " @";
						result = "false ~";
					}
					C_CDA.moveToNext();
					sub_total += iac;
				}
				C_CDA.close();
			}
			else
			{
				return true;
			}
		} catch (Exception e) {
			// TODO: handle exception
			EX_CHKCD[3] = false;
			result += "false~";
			throw e;
		}
		sub_total = 100;
		setincreament();
		;
		if (result.contains("false"))
			return false;
		else
			return true;
	}

	private boolean EX_CD_HVAC() throws SocketException, NetworkErrorException,
			TimeoutException, IOException, XmlPullParserException, Exception,
			NullPointerException {
		// TODO Auto-generated method stub
		String result = "";
		try {
			sub_total = 0;
			sub_insp = "Internal HVAC System(s)";
			cf.CreateARRTable(72);
			Cursor C_CDH = cf.SelectTablefunction(cf.DRY_hvac,
					" where fld_srid='" + cf.selectedhomeid + "' ");
			if (C_CDH.getCount() >= 1) {
				int ihc = 100 / C_CDH.getCount();
				C_CDH.moveToFirst();
				for (int i = 0; i < C_CDH.getCount(); i++) {
					cf.request = cf.SoapRequest("InternalHVACSystem");

					cf.request.addProperty("Fld_Srid",
							C_CDH.getString(C_CDH.getColumnIndex("fld_srid")));
					cf.request.addProperty("OrderID",(i+1));
					cf.request.addProperty("hvacnum",
							C_CDH.getString(C_CDH.getColumnIndex("hvacnum")));
					cf.request.addProperty("unitloc", cf.decode(C_CDH
							.getString(C_CDH.getColumnIndex("unitloc"))));
					cf.request.addProperty("manufacturer", cf.decode(C_CDH
							.getString(C_CDH.getColumnIndex("manufacturer"))));
					cf.request.addProperty("unitacess",
							C_CDH.getString(C_CDH.getColumnIndex("unitacess")));
					cf.request.addProperty("recentrepairs", C_CDH
							.getString(C_CDH.getColumnIndex("recentrepairs")));
					cf.request.addProperty("coppercorrs", C_CDH.getString(C_CDH
							.getColumnIndex("coppercorrs")));
					cf.request
							.addProperty("darkstains", C_CDH.getString(C_CDH
									.getColumnIndex("darkstains")));

					System.out.println("HVACresult= " + cf.request);
					result += cf.SoapResponse("InternalHVACSystem", cf.request)
							+ "~";

					System.out.println("EX_CHKCD[2]= " + result);

					C_CDH.moveToNext();
					sub_total += ihc;
				}
				C_CDH.close();
			}
			else
			{
				return true;
			}
		} catch (Exception e) {
			// TODO: handle exception
			EX_CHKCD[2] = false;
			result += "false ~";
			throw e;
		}
		sub_total = 100;
		setincreament();
		;
		if (result.contains("false"))
			return false;
		else
			return true;
	}

	private boolean EX_CD_Room() throws SocketException, NetworkErrorException,
			TimeoutException, IOException, XmlPullParserException, Exception,
			NullPointerException {
		// TODO Auto-generated method stub
		String result = "";
		try {
			sub_total = 0;
			sub_insp = "Visual Assessment Room";
			cf.CreateARRTable(74);
			Cursor C_CDR = cf.SelectTablefunction(cf.DRY_room,
					" where fld_srid='" + cf.selectedhomeid + "' ");
			if (C_CDR.getCount() >= 1) {
				int varc = 100 / C_CDR.getCount();
				C_CDR.moveToFirst();
				for (int i = 0; i < C_CDR.getCount(); i++) {
					cf.request = cf.SoapRequest("VisualAssessmentRoom");
					cf.request.addProperty("Fld_Srid",
							C_CDR.getString(C_CDR.getColumnIndex("fld_srid")));
					cf.request.addProperty("OrderID",(i+1));
					cf.request.addProperty("roomnum",
							C_CDR.getString(C_CDR.getColumnIndex("roomnum")));
					cf.request.addProperty("roomname",
							C_CDR.getString(C_CDR.getColumnIndex("roomname")));
					if (C_CDR.getString(C_CDR.getColumnIndex("roomname"))
							.equals("Other")) {
						cf.request.addProperty("rmnameotr", cf.decode(C_CDR
								.getString(C_CDR.getColumnIndex("rmnameotr"))));
					} else {
						cf.request.addProperty("rmnameotr", "");

					}
					cf.request.addProperty("outletsaccess", C_CDR
							.getString(C_CDR.getColumnIndex("outletsaccess")));
					cf.request.addProperty("outletseval", C_CDR.getString(C_CDR
							.getColumnIndex("outletseval")));
					cf.request.addProperty("switchaccess", C_CDR
							.getString(C_CDR.getColumnIndex("switchaccess")));
					cf.request
							.addProperty("switcheval", C_CDR.getString(C_CDR
									.getColumnIndex("switcheval")));
					cf.request.addProperty("invasive",
							C_CDR.getString(C_CDR.getColumnIndex("invasive")));
					cf.request.addProperty("sulfur",
							C_CDR.getString(C_CDR.getColumnIndex("sulfur")));
					cf.request.addProperty("confpresence", C_CDR
							.getString(C_CDR.getColumnIndex("confpresence")));
					cf.request
							.addProperty("obscopcorrosion", C_CDR
									.getString(C_CDR
											.getColumnIndex("obscopcorrosion")));
					cf.request
							.addProperty("obsmetcorrosion", C_CDR
									.getString(C_CDR
											.getColumnIndex("obsmetcorrosion")));
					cf.request
							.addProperty("darkstains", C_CDR.getString(C_CDR
									.getColumnIndex("darkstains")));
					System.out.println("roomresult= " + cf.request);
					sub_total += varc;
					result += cf.SoapResponse("VisualAssessmentRoom",
							cf.request) + "~";
					System.out.println("EX_CHKCD[1]= " + result);

					cf.request = null;
					C_CDR.moveToNext();
				}
				C_CDR.close();
			}
			else
			{
				return true;
			}

		} catch (Exception e) {
			// TODO: handle exception
			EX_CHKCD[1] = false;
			result += "false ~";
			throw e;
		}
		sub_total = 100;
		setincreament();
		;
		if (result.contains("false"))
			return false;
		else
			return true;
	}

	private boolean EX_CD_SummaryCond() throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		// TODO Auto-generated method stub
		try {
			sub_total = 0;
			sub_insp = "Summary of Conditions/Inspectors Findings";
			cf.request = cf.SoapRequest("SummaryofConditionsIns");
			cf.CreateARRTable(7);
			Cursor C_CDS = cf.SelectTablefunction(cf.DRY_sumcond,
					" where fld_srid='" + cf.selectedhomeid + "' ");
			if (C_CDS.getCount() >= 1) {
				String[] CDS_infos = cf.setvalueToArray(C_CDS);

				cf.request.addProperty("Fld_Srid", cf.selectedhomeid);
				cf.request.addProperty("presofsulfur", CDS_infos[2]);
				cf.request.addProperty("obscopper", CDS_infos[3]);
				cf.request.addProperty("presofdry", CDS_infos[4]);
				cf.request.addProperty("addcomments", CDS_infos[5]);
				cf.request.addProperty("Overallchinese_Comment", CDS_infos[6]);
				cf.request.addProperty("Attic_Comment", CDS_infos[7]);
				cf.request.addProperty("Internalappliance_Comment",
						CDS_infos[8]);
				cf.request.addProperty("I_HVAC_Comment", CDS_infos[9]);
				cf.request
						.addProperty("VisualAssesment_Comment", CDS_infos[10]);
				
				Cursor c =cf.SelectTablefunction(cf.DRY_home, " WHERE fld_srid='"+cf.selectedhomeid+"'");
				c.moveToFirst();
				if(c.getCount()>0)
				{
					cf.request.addProperty("HomeOwner_Persent_time",cf.decode(c.getString(c.getColumnIndex("home_pre"))));
					cf.request.addProperty("Reason_Chinesedrywall_Assesment",cf.decode(c.getString(c.getColumnIndex("home_assessment"))));
					cf.request.addProperty("Homeowner_experiencing_Sulfurlike",cf.decode(c.getString(c.getColumnIndex("home_odor"))));
					cf.request.addProperty("Homeowner_experiencing_black_coating",cf.decode(c.getString(c.getColumnIndex("home_furnishing"))));
					cf.request.addProperty("Homeowner_experiencing_Mechnical_Equipment",cf.decode(c.getString(c.getColumnIndex("home_breakdown"))));
					cf.request.addProperty("Homeowner_Knowingly_Unknowingly_Cleared",cf.decode(c.getString(c.getColumnIndex("home_cleanevidence"))));
					cf.request.addProperty("Material_testing_Request",cf.decode(c.getString(c.getColumnIndex("home_test_requ"))));
					cf.request.addProperty("Signed_Copy_Preinspection",cf.decode(c.getString(c.getColumnIndex("home_signed"))));
					cf.request.addProperty("Homeowner_Agreed_Material",cf.decode(c.getString(c.getColumnIndex("home_testing_iden"))));
					cf.request.addProperty("Homeowner_Repair_Comments",cf.decode(c.getString(c.getColumnIndex("home_comments"))));
					
				}
				System.out.println("Summaryresult= " + cf.request);
				sub_total = 50;
				EX_CHKCD[0] = cf.SoapResponse("SummaryofConditionsIns",
						cf.request);
				System.out.println("EX_CHKCD[0]= " + EX_CHKCD[0]);
				C_CDS.close();
			}
			else
			{
				return true;
			}
		} catch (Exception e) {
			// TODO: handle exception
			EX_CHKCD[0] = false;
			throw e;
		}
		sub_total = 100;
		setincreament();
		;
		return EX_CHKCD[0];
	}

	/*** Chinese drywall export Ends */
	/*** Common roof export starts */
	private boolean EX_ROOF_RCN_new(String insp) throws SocketException,
	NetworkErrorException, TimeoutException, IOException,
	XmlPullParserException, Exception, NullPointerException {
// TODO Auto-generated method stub
		String RCT_type = "", result_val = "", insp_online = "";
		if (insp.equals("1"))
			insp_online = "13";
		else if (insp.equals("2"))
			insp_online = "750";
		else if (insp.equals("4"))
			insp_online = "9";
		else if (insp.equals("5"))
			insp_online = "62";
		else if (insp.equals("6"))
			insp_online = "90";
		else if (insp.equals("7"))
			insp_online = "11";
		
		// RM_srid as RCT_srid,RM_page as RCT_page,RM_insp_id as
		// RCT_insp_id,RM_Covering as
		// RCT_FielsName,RCT_TypeValue,RCT_FieldVal,RCT_Other,RM_Enable as
		// RCT_Enable
		cf.request = cf.SoapRequest("RoofConditionsNoted_Type1");
		cf.request.addProperty("Fld_Srid", cf.selectedhomeid);// "RI12348");
		cf.request.addProperty("InspectionID", insp_online);
		
		sql=" SELECT * FROM "+cf.Roof_additional+" LEFT JOIN "+cf.RCN_table+" ON R_RCN_Ssrid='"+cf.selectedhomeid+"' and R_RCN_insp_id='"+insp+"' WHERE R_ADD_Ssrid='"+cf.selectedhomeid+"' and R_ADD_insp_id='"+insp+"'";
		Cursor C_Roof = cf.arr_db.rawQuery(sql, null);
		C_Roof.moveToFirst();
		if (C_Roof.getCount() >= 1) {
			C_Roof.moveToFirst();
			int roof_inc = 50 / C_Roof.getCount();
			// System.out.println("count "+C_Roof.getCount());
			
			cf.request.addProperty("Roof_Survey", cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_ADD_scope"))));
			cf.request.addProperty("Additional_Roof_Chk", cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_ADD_na"))));
			cf.request.addProperty("FullLeakage_Review", cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_ADD_full"))));
			cf.request.addProperty("FullLeakage_Review_Sub", "");
			cf.request.addProperty("Apprx_remaning_Life", 0);
			cf.request.addProperty("Ageof_Roof", "");
			cf.request.addProperty("Date_LastUpdt", "");
			cf.request.addProperty("wereany_updt_Compld", cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_ADD_WUWC"))));
			cf.request.addProperty("WereAny_upd_Cmnts", "");
			
			
			cf.request.addProperty("Roof_Condition_Noted", cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_RCN_na"))));
			if (cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_RCN_na"))).equals("true"))
				cf.request.addProperty("RoofConditionchecked", 1);
			else
				cf.request.addProperty("RoofConditionchecked", 0);
			
			cf.request.addProperty("Recent_Rpr_Noted", add_RCN_other(cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_RCN_RRN")))));
			cf.request.addProperty("Roof_Leakage_Noted", "");
			cf.request.addProperty("Excessive_Roof_Aging_evident", "");
			cf.request.addProperty("Visble_Sign_Leakage", add_RCN_other(cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_RCN_VSL")))));
			cf.request.addProperty("Visble_Sign_Damaged", add_RCN_other(cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_RCN_VSTDS")))));
			cf.request.addProperty("Visble_Sign_Tiling", add_RCN_other(cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_RCN_VSDTS")))));
			cf.request.addProperty("Visble_Sign_Curling", add_RCN_other(cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_RCN_VSCBS")))));
			cf.request.addProperty("Visble_Sign_RoofStruct", add_RCN_other(cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_RCN_VDRS")))));
			cf.request.addProperty("Other_Damaged_Notd", add_RCN_other(cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_RCN_ODN")))));
			cf.request.addProperty("Overoll_Condition", add_RCN_other(cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_RCN_OCR")))));
			cf.request.addProperty("Overoll_RoofComments", cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_ADD_comment"))));
			if (cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_ADD_na"))).equals("true"))
				cf.request.addProperty("AddlRoofchecked", 1);
			else
				cf.request.addProperty("AddlRoofchecked", 0);
			
			
			cf.request.addProperty("Roof_MountStruct", add_RCN_other(cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_RCN_HRMS")))));
			cf.request.addProperty("Roof_FlashCoping", add_RCN_other(cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_RCN_FC")))));
			cf.request.addProperty("General_Roofappears", add_RCN_other(cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_RCN_GRAC")))));
			cf.request.addProperty("Roof_Leakage", add_RCN_other(cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_RCN_RLN")))));
			cf.request.addProperty("Roof_Ponding", add_RCN_other(cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_RCN_RPN")))));
			cf.request.addProperty("Roof_evr_repl", add_RCN_other(cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_RCN_HREBR")))));
			cf.request.addProperty("Old_roof_remove", add_RCN_other(cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_RCN_WORR")))));
			cf.request.addProperty("Roof_rec_maintain", add_RCN_other(cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_RCN_RMRA")))));
			cf.request.addProperty("Specific_fund", add_RCN_other(cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_RCN_SFPE")))));
			cf.request.addProperty("Amount_fund", add_RCN_other(cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_RCN_Amount")))));
			cf.request.addProperty("Water_demage", add_RCN_other(cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_RCN_EVPWD")))));
			cf.request.addProperty("Roofing_Material", add_RCN_other(cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_RCN_RME")))));
			cf.request.addProperty("Base_Flashing", add_RCN_other(cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_RCN_BFP")))));
			cf.request.addProperty("Stretch_Membrane", add_RCN_other(cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_RCN_ESM")))));
			cf.request.addProperty("Avoid_seepage", add_RCN_other(cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_RCN_SOSAS")))));
			cf.request.addProperty("Roof_Demage", add_RCN_other(cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_RCN_DRS")))));
			
			cf.request.addProperty("Roofupd_confirm", cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_ADD_PDRUC"))));
			cf.request.addProperty("Roof_PermitNo", cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_ADD_PN"))));
			cf.request.addProperty("Roof_PermitDate", cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_ADD_PD"))));
			cf.request.addProperty("Roof_Mounted_Euip", cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_ADD_RME"))));
			String Roof_mountstr="",Roof_mount_loc="";
			String R_ADD_V_HVAC_E=cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_ADD_HVAC_E")));
			if(R_ADD_V_HVAC_E.contains("#&45"))
			{
				Roof_mountstr+=cf.getResourcesvalue(R.string.hvacE);
				Roof_mount_loc+=cf.getResourcesvalue(R.string.hvacE);
				String[] temp=R_ADD_V_HVAC_E.split("#&45");
				Roof_mountstr+="^BRK"+temp[1]+"^BRK";
				Roof_mount_loc+="^BRK"+temp[2]+"^BRK";
				
			}
			R_ADD_V_HVAC_E=cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_ADD_VEN_E")));
			if(R_ADD_V_HVAC_E.contains("#&45"))
			{
				Roof_mountstr+=cf.getResourcesvalue(R.string.VentalationE);
				Roof_mount_loc+=cf.getResourcesvalue(R.string.VentalationE);
				String[] temp=R_ADD_V_HVAC_E.split("#&45");
				Roof_mountstr+="^BRK"+temp[1]+"^BRK";
				Roof_mount_loc+="^BRK"+temp[2]+"^BRK";
				
			}
			R_ADD_V_HVAC_E=cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_ADD_STOR_E")));
			if(R_ADD_V_HVAC_E.contains("#&45"))
			{
				Roof_mountstr+=cf.getResourcesvalue(R.string.StorageE);
				Roof_mount_loc+=cf.getResourcesvalue(R.string.StorageE);
				String[] temp=R_ADD_V_HVAC_E.split("#&45");
				Roof_mountstr+="^BRK"+temp[1]+"^BRK";
				Roof_mount_loc+="^BRK"+temp[2]+"^BRK";
				
			}
			R_ADD_V_HVAC_E=cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_ADD_PIP")));
			if(R_ADD_V_HVAC_E.contains("true"))
			{
				Roof_mountstr+=cf.getResourcesvalue(R.string.PipingD)+"^BRK";
			
				//String[] temp=R_ADD_V_HVAC_E.split("#&45");
				
				
			}
			R_ADD_V_HVAC_E=cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_ADD_clean_E")));
			if(R_ADD_V_HVAC_E.contains("true"))
			{
				Roof_mountstr+=cf.getResourcesvalue(R.string.CleaningE)+"^BRK";
				
				//String[] temp=R_ADD_V_HVAC_E.split("#&45");
				
				
			}
			R_ADD_V_HVAC_E=cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_ADD_ANT_P")));
			if(R_ADD_V_HVAC_E.contains("true"))
			{
				Roof_mountstr+=cf.getResourcesvalue(R.string.AntennaP)+"^BRK";
				
				//String[] temp=R_ADD_V_HVAC_E.split("#&45");
				
				
			}
			R_ADD_V_HVAC_E=cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_ADD_LSE")));
			if(R_ADD_V_HVAC_E.contains("true"))
			{
				Roof_mountstr+=cf.getResourcesvalue(R.string.LooseS)+"^BRK";
				
				//String[] temp=R_ADD_V_HVAC_E.split("#&45");
				
				
			}
			R_ADD_V_HVAC_E=cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_ADD_O_E")));
			//System.out.println(" the other value"+R_ADD_V_HVAC_E);
			if(R_ADD_V_HVAC_E.contains("true"))
			{
				Roof_mountstr+=cf.getResourcesvalue(R.string.OtherE);
				String[] temp=R_ADD_V_HVAC_E.split("#&45");
				Roof_mountstr+="("+temp[1]+")";
				
				//String[] temp=R_ADD_V_HVAC_E.split("#&45");
				
				
			}
			if(!Roof_mountstr.endsWith(")"))
			{
			if (Roof_mountstr.length() > 1) {
				Roof_mountstr = Roof_mountstr.substring(0,
						Roof_mountstr.lastIndexOf("^"));
			}
			}
			if (Roof_mount_loc.length() > 1) {
				Roof_mount_loc = Roof_mount_loc.substring(0,
						Roof_mount_loc.lastIndexOf("^"));
			}
			cf.request.addProperty("Roof_Mounted_Struct",Roof_mountstr );
			cf.request.addProperty("Roof_Mounted_Location", Roof_mount_loc);
			cf.request.addProperty("Roof_Mounted_Loose", cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_ADD_RMEL"))));
			cf.request.addProperty("Demage_Roof_mount", cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_ADD_DRMS"))));
			cf.request.addProperty("Roof_Other", cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_ADD_other_I"))));
			if(cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_ADD_citizen"))).equals(""))
			{
				cf.request.addProperty("Include_CitizenFrm", false);	
			}
			else
			{
				cf.request.addProperty("Include_CitizenFrm", cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_ADD_citizen"))));	
			}
			
			cf.request.addProperty("RCT_NA", cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_RCT_na"))));
			
			System.out.println(" the request "+cf.request);
			
			result_val += "~"
					+ cf.SoapResponse("RoofConditionsNoted_Type1", cf.request);
			System.out.println("the overal roof result=" + result_val);
			
			C_Roof=cf.SelectTablefunction(cf.RCN_table_dy, " WHERE R_RCN_D_Ssrid='"+cf.selectedhomeid+"' and R_RCN_D_insp_id='"+insp+"'");
			if(C_Roof.getCount()>0)
			{
				C_Roof.moveToFirst();
			for(int i=0;i<C_Roof.getCount();i++,C_Roof.moveToNext())
			{
			
			SoapObject req = cf.SoapRequest("RoofConditionsNoted_Type2");
			req.addProperty("Fld_Srid", cf.selectedhomeid);// "RI12348");
			req.addProperty("InspectionID", insp_online);
			req.addProperty("ROWID",(i+1));
			// req.addProperty("InspectionID",insp);
			req.addProperty("Roof_Title", cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_RCN_D_title"))));
			req.addProperty("Roof_Value", convefromabreviation(cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_RCN_D_option")))));
			req.addProperty("Roof_Description", cf.decode(C_Roof.getString(C_Roof.getColumnIndex("R_RCN_D_Other"))));
			System.out.println("called the other RCN proper" + req);
			result_val += "~"+ cf.SoapResponse("RoofConditionsNoted_Type2", req);
			sub_total += roof_inc;
			}
			}
			/** Start adding the default values **/


		}
		else
		{
			return true;
		}
		sub_total = 100;
		setincreament();
		
		if (result_val.equals("")) {
			return false;
		} else if (result_val.contains("false")) {
			return false;
		} else {
			return true;
		}
		
	}
	private Object add_RCN_other(String decode) {
		// TODO Auto-generated method stub
		return convefromabreviation(decode.replace("#&45","^BRK"));
		
		
	}
	private boolean EX_Roof_RCT_new(String insp) throws SocketException,
	NetworkErrorException, TimeoutException, IOException,
	XmlPullParserException, Exception, NullPointerException {
// TODO Auto-generated method stub
		Exportbartext = "Exporting Roof System";
		sub_total = 0;
		String RCT_type = "", result_val = "", insp_online = "";
		if (insp.equals("1"))
			insp_online = "13";
		else if (insp.equals("2"))
			insp_online = "750";
		else if (insp.equals("4"))
			insp_online = "9";
		else if (insp.equals("5"))
			insp_online = "62";
		else if (insp.equals("6"))
			insp_online = "90";
		else if (insp.equals("7"))
			insp_online = "11";
		cf.CreateARRTable(435);
		cf.CreateARRTable(431);
		
		
		
		String sql = " SELECT * FROM "+cf.Rct_table+" WHERE RCT_Ssrid='"+cf.selectedhomeid+"' and RCT_insp_id='"+insp+"' ";
		Cursor C_Roof = cf.arr_db.rawQuery(sql, null);
		C_Roof.moveToFirst();
		if (C_Roof.getCount() >= 1) {
		
			int roof_inc = 40 / C_Roof.getCount();
		
			for (int i = 0; i < C_Roof.getCount(); i++, C_Roof.moveToNext()) {
				
				
				cf.request = cf.SoapRequest("RoofCoveringType");
				cf.request.addProperty("Fld_Srid", cf.selectedhomeid);// "RI12348");
				cf.request.addProperty("InspectionID", insp_online);
				cf.request.addProperty("Predmnt", convefromabreviation(cf.decode(C_Roof.getString(C_Roof.getColumnIndex("RCT_pre")))));
				cf.request.addProperty("CoveringType", cf.decode(C_Roof.getString(C_Roof.getColumnIndex("RCT_rct"))));
				cf.request.addProperty("RoofOtherText", cf.decode(C_Roof.getString(C_Roof.getColumnIndex("RCT_other_rct"))));
				cf.request.addProperty("PrmtDate", cf.decode(C_Roof.getString(C_Roof.getColumnIndex("RCT_PAD"))));
				String yer=cf.decode(C_Roof.getString(C_Roof.getColumnIndex("RCT_yoi")));
				if(yer.equals("Other"))
				{
					yer="Other("+cf.decode(C_Roof.getString(C_Roof.getColumnIndex("RCT_other_yoi")))+")";
				}
				cf.request.addProperty("YrInst", yer);
				String sh=cf.decode(C_Roof.getString(C_Roof.getColumnIndex("RCT_rsh")));
				if(sh.equals("Other"))
				{
					sh="Other("+cf.decode(C_Roof.getString(C_Roof.getColumnIndex("RCT_other_rsh")))+")";
				}
				cf.request.addProperty("RoofShap",sh);
				cf.request.addProperty("RoofShapPercent", cf.decode(C_Roof.getString(C_Roof.getColumnIndex("RCT_Rsh_per"))));
				cf.request.addProperty("RoofSlope", cf.decode(C_Roof.getString(C_Roof.getColumnIndex("RCT_rsl"))));
				String ARM=cf.decode(C_Roof.getString(C_Roof.getColumnIndex("RCT_arfl")));
				if(ARM.equals("Other"))
				{
					ARM="Other("+cf.decode(C_Roof.getString(C_Roof.getColumnIndex("RCT_other_aRFL")))+")";
				}
				cf.request.addProperty("ApprRemain", ARM);
				String RST=cf.decode(C_Roof.getString(C_Roof.getColumnIndex("RCT_rst")));
				if(RST.equals("Other"))
				{
					RST="Other("+cf.decode(C_Roof.getString(C_Roof.getColumnIndex("RCT_other_rst")))+")";
				}
				cf.request.addProperty("RoofStruct", RST);
				cf.request.addProperty("BuildingCode", cf.decode(C_Roof.getString(C_Roof.getColumnIndex("RCT_bc"))));
				cf.request.addProperty("PrdmntRoofCvr", cf.decode(C_Roof.getString(C_Roof.getColumnIndex("RCT_pre"))));
				cf.request.addProperty("RoofComplxty", cf.decode(C_Roof.getString(C_Roof.getColumnIndex("RCT_rc"))));
				cf.request.addProperty("Noinformation", cf.decode(C_Roof.getString(C_Roof.getColumnIndex("RCT_npv"))));
				cf.request.addProperty("ROWID", (i+1));

				cf.envelope.setOutputSoapObject(cf.request);
				MarshalBase64 marshal = new MarshalBase64();
				marshal.register(cf.envelope);
				cf.envelope.setOutputSoapObject(cf.request);
				// System.out.println("no more issues4");
				System.out.println("Roof covering type=" + RCT_type
						+ "=" + cf.request);

				HttpTransportSE androidHttpTransport = new HttpTransportSE(
						cf.URL_ARR);

				androidHttpTransport.call(cf.NAMESPACE
						+ "RoofCoveringType", cf.envelope);
				result_val += cf.envelope.getResponse().toString()
						+ "~";
				System.out.println("the reult =" + result_val);
				cf.request = null;
				send_roof_image(cf.selectedhomeid, cf.decode(C_Roof.getString(C_Roof.getColumnIndex("RCT_rct"))), insp,
						insp_online);
				sub_total += roof_inc;	
			}
			
		}
		else
		{
			return true;
		}
		
		
		sub_total = 50;
		if (result_val.equals("")) {
			return true;
		} else if (result_val.contains("false")) {
			return false;
		} else {
			return true;
		}
	}
/*	private boolean EX_ROOF_RCN(String insp) throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		// TODO Auto-generated method stub
		String RCT_type = "", result_val = "", insp_online = "";
		if (insp.equals("1"))
			insp_online = "13";
		else if (insp.equals("2"))
			insp_online = "750";
		else if (insp.equals("4"))
			insp_online = "9";
		else if (insp.equals("5"))
			insp_online = "62";
		else if (insp.equals("6"))
			insp_online = "90";
		else if (insp.equals("7"))
			insp_online = "11";

		// RM_srid as RCT_srid,RM_page as RCT_page,RM_insp_id as
		// RCT_insp_id,RM_Covering as
		// RCT_FielsName,RCT_TypeValue,RCT_FieldVal,RCT_Other,RM_Enable as
		// RCT_Enable
		cf.request = cf.SoapRequest("RoofConditionsNoted_Type1");
		cf.request.addProperty("Fld_Srid", cf.selectedhomeid);// "RI12348");
		cf.request.addProperty("InspectionID", insp_online);
		String sql = " Select * from " + cf.General_roof_view
				+ " Where RC_SRID='" + cf.selectedhomeid + "' and RC_insp_id='"
				+ insp + "'";
		System.out.println(" Select * from " + cf.General_roof_view
				+ " Where RC_SRID='" + cf.selectedhomeid + "' and RC_insp_id='"
				+ insp + "'");
		Cursor C_Roof = cf.arr_db.rawQuery(sql, null);
		C_Roof.moveToFirst();
		if (C_Roof.getCount() >= 1) {
			int roof_inc = 50 / C_Roof.getCount();
			// System.out.println("count "+C_Roof.getCount());
			for (int i = 0; i < C_Roof.getCount(); i++, C_Roof.moveToNext()) {

				String val = "", val_other = "", tit = "", id_val = "";
				tit = cf.decode(C_Roof.getString(C_Roof
						.getColumnIndex("RC_Condition_type")));
				// System.out.println("tit"+i+"="+tit);
				val = convefromabreviation(cf.decode(C_Roof.getString(C_Roof
						.getColumnIndex("RC_Condition_type_val"))));
				val_other = cf.decode(C_Roof.getString(C_Roof
						.getColumnIndex("RC_Condition_type_DESC")));
				id_val = cf.decode(C_Roof.getString(C_Roof
						.getColumnIndex("RC_Id")));
				System.out.println("the tit " + tit + " id_val=" + id_val);
				*//***
				 * RCN values added to the property this will use for 4point
				 * ,roof survey,com1,GCH
				 **//*
				if (tit.equals("22"))*//***
				 * We check the title based on the
				 * tbl_master_info table custom id
				 ***//*
				{
					if (!val_other.equals("")) {
						val += "^BRK" + val_other;
					}
					cf.request.addProperty("Overoll_Condition", val);

				} else if (tit.equals("15"))*//***
				 * We check the title based on the
				 * tbl_master_info table custom id
				 ***//*
				{
					if (!val_other.equals("")) {
						val += "^BRK" + val_other;
					}

					cf.request.addProperty("Recent_Rpr_Noted", val);

				} else if (tit.equals("16"))*//***
				 * We check the title based on the
				 * tbl_master_info table custom id
				 ***//*
				{
					if (!val_other.equals("")) {
						val += "^BRK" + val_other;
					}
					cf.request.addProperty("Visble_Sign_Leakage", val);

				} else if (tit.equals("17"))*//***
				 * We check the title based on the
				 * tbl_master_info table custom id
				 ***//*
				{
					if (!val_other.equals("")) {
						val += "^BRK" + val_other;
					}
					cf.request.addProperty("Visble_Sign_Damaged", val);

				} else if (tit.equals("18"))*//***
				 * We check the title based on the
				 * tbl_master_info table custom id
				 ***//*
				{
					if (!val_other.equals("")) {
						val += "^BRK" + val_other;
					}
					cf.request.addProperty("Visble_Sign_Curling", val);
				} else if (tit.equals("19"))*//***
				 * We check the title based on the
				 * tbl_master_info table custom id
				 ***//*
				{
					if (!val_other.equals("")) {
						val += "^BRK" + val_other;
					}
					cf.request.addProperty("Visble_Sign_Tiling", val);
					;

				} else if (tit.equals("20"))*//***
				 * We check the title based on the
				 * tbl_master_info table custom id
				 ***//*
				{
					if (!val_other.equals("")) {
						val += "^BRK" + val_other;
					}
					cf.request.addProperty("Visble_Sign_RoofStruct", val);

				} else if (tit.equals("21"))*//***
				 * We check the title based on the
				 * tbl_master_info table custom id
				 ***//*
				{
					if (!val_other.equals("")) {
						val += "^BRK" + val_other;
					}
					cf.request.addProperty("Other_Damaged_Notd", val);
				} else if (tit.equals("23"))*//***
				 * We check the title based on the
				 * tbl_master_info table custom id
				 ***//*
				{
					if (!val_other.equals("")) {
						val += "^BRK" + val_other;
					}
					cf.request.addProperty("Roof_MountStruct", val);
				} else if (tit.equals("24"))*//***
				 * We check the title based on the
				 * tbl_master_info table custom id
				 ***//*
				{
					val = val.replace("Roof Flashing Property Anchored",
							"Roof Flashing Property Anchored");// For online i
																// have hard
																// code like
																// this

					if (!val_other.equals("")) {
						val += "^BRK" + val_other;
					}
					cf.request.addProperty("Roof_FlashCoping", val);
				}

				else if (tit.equals("25"))*//***
				 * We check the title based on the
				 * tbl_master_info table custom id
				 ***//*
				{
					if (!val_other.equals("")) {
						val += "^BRK" + val_other;
					}
					cf.request.addProperty("General_Roofappears", val);
				}

				else if (tit.equals("26"))*//***
				 * We check the title based on the
				 * tbl_master_info table custom id
				 ***//*
				{
					if (!val_other.equals("")) {
						val += "^BRK" + val_other;
					}
					cf.request.addProperty("Roof_Leakage", val);
				}

				else if (tit.equals("27"))*//***
				 * We check the title based on the
				 * tbl_master_info table custom id
				 ***//*
				{
					if (!val_other.equals("")) {
						val += "^BRK" + val_other;
					}
					cf.request.addProperty("Roof_Ponding", val);
				}

				else if (tit.equals("28"))*//***
				 * We check the title based on the
				 * tbl_master_info table custom id
				 ***//*
				{
					if (!val_other.equals("")) {
						val += "^BRK" + val_other;
					}
					cf.request.addProperty("Roof_evr_repl", val);
				}

				else if (tit.equals("29"))*//***
				 * We check the title based on the
				 * tbl_master_info table custom id
				 ***//*
				{
					if (!val_other.equals("")) {
						val += "^BRK" + val_other;
					}
					cf.request.addProperty("Old_roof_remove", val);
				} else if (tit.equals("30"))*//***
				 * We check the title based on the
				 * tbl_master_info table custom id
				 ***//*
				{
					if (!val_other.equals("")) {
						val += "^BRK" + val_other;
					}
					cf.request.addProperty("Roof_rec_maintain", val);
				}

				else if (tit.equals("31"))*//***
				 * We check the title based on the
				 * tbl_master_info table custom id
				 ***//*
				{
					if (!val_other.equals("")) {
						val += "^BRK" + val_other;
					}
					cf.request.addProperty("Specific_fund", val);
				}

				else if (tit.equals("32"))*//***
				 * We check the title based on the
				 * tbl_master_info table custom id
				 ***//*
				{
					if (!val_other.equals("")) {
						val += "^BRK" + val_other;
					}
					cf.request.addProperty("Amount_fund", val);
				}

				else if (tit.equals("33"))*//***
				 * We check the title based on the
				 * tbl_master_info table custom id
				 ***//*
				{
					if (!val_other.equals("")) {
						val += "^BRK" + val_other;
					}
					cf.request.addProperty("Water_demage", val);
				} else if (tit.equals("34"))*//***
				 * We check the title based on the
				 * tbl_master_info table custom id
				 ***//*
				{
					if (!val_other.equals("")) {
						val += "^BRK" + val_other;
					}
					cf.request.addProperty("Roofing_Material", val);
				} else if (tit.equals("36"))*//***
				 * We check the title based on the
				 * tbl_master_info table custom id
				 ***//*
				{
					if (!val_other.equals("")) {
						val += "^BRK" + val_other;
					}
					cf.request.addProperty("Base_Flashing", val);
				} else if (tit.equals("35"))*//***
				 * We check the title based on the
				 * tbl_master_info table custom id
				 ***//*
				{
					if (!val_other.equals("")) {
						val += "^BRK" + val_other;
					}
					cf.request.addProperty("Stretch_Membrane", val);
				} else if (tit.equals("37"))*//***
				 * We check the title based on the
				 * tbl_master_info table custom id
				 ***//*
				{
					if (!val_other.equals("")) {
						val += "^BRK" + val_other;
					}
					cf.request.addProperty("Avoid_seepage", val);
				} else if (tit.equals("38"))*//***
				 * We check the title based on the
				 * tbl_master_info table custom id
				 ***//*
				{
					if (!val_other.equals("")) {
						val += "^BRK" + val_other;
					}
					cf.request.addProperty("Roof_Demage", val);
				}
				*//***
				 * RCN values added to the property this will use for 4point
				 * ,roof survey,com1,GCH ends
				 **//*

				*//** Additional values property starts ***//*
				else if (tit.equals(cf
						.getResourcesvalue(R.string.roof_add_4poin_txt)))*//***
				 * We
				 * check the title based on the tbl_master_info table custom id
				 ***//*
				{
					if (!val_other.equals("")) {
						val += "^BRK" + val_other;
					}
					cf.request.addProperty("FullLeakage_Review", val);
				} else if (tit.equals(cf
						.getResourcesvalue(R.string.roof_add_4poin_txt2)))*//***
				 * We
				 * check the title based on the tbl_master_info table custom id
				 ***//*
				{
					if (!val_other.equals("")) {
						val += "^BRK" + val_other;
					}
					cf.request.addProperty("Roof_Survey",
							val.replace("&#94;", ","));
				}

				*//** Additional values property ends ***//*
				else if (tit.equals("Comments")) {
					if (!val_other.equals("")) {
						val = val_other;
					}
					cf.request.addProperty("Overoll_RoofComments", val);

				} else if (tit.equals("RCN_notapplicable")) {

					cf.request.addProperty("Roof_Condition_Noted", val);
					if (val.equals("true"))
						cf.request.addProperty("RoofConditionchecked", 1);
					else
						cf.request.addProperty("RoofConditionchecked", 0);

				} else if (tit.equals("ADD_notapplicable")) {

					cf.request.addProperty("Additional_Roof_Chk", val);
					if (val.equals("true"))
						cf.request.addProperty("AddlRoofchecked", 1);
					else
						cf.request.addProperty("AddlRoofchecked", 0);

				} else if (tit.equals(cf
						.getResourcesvalue(R.string.ROOF_Survey5)))*//***
				 * We check
				 * the title based on the tbl_master_info table custom id
				 ***//*
				{
					if (!val_other.equals("")) {
						val += "^BRK" + val_other;
					}
					cf.request.addProperty("wereany_updt_Compld", val);
				} else if (tit.equals(cf.getResourcesvalue(R.string.COMER2)))*//***
				 * 
				 * We check the title based on the tbl_master_info table custom
				 * id
				 ***//*
				{
					cf.request.addProperty("Roofupd_confirm", val);
					if (!val_other.equals("")) {
						String[] tmp_val = val_other.split("#94");
						if (tmp_val.length >= 1) {
							cf.request.addProperty("Roof_PermitNo",
									tmp_val[0].replace("NO:", ""));
							cf.request.addProperty("Roof_PermitDate",
									tmp_val[1].replace("DATE:", ""));
						}
					}

				} else if (tit.equals(cf.getResourcesvalue(R.string.COMER3)))*//***
				 * 
				 * We check the title based on the tbl_master_info table custom
				 * id
				 ***//*
				{
					String Equp_val = "", Equp_loc = "";
					if (!val_other.equals("") && !val.equals("")) {
						val_other = val_other.replace("#94", " #94 ");
						String tmp[] = val_other.split("#95");
						if (tmp.length >= 2) {

							String percent[] = tmp[0].split("#94");
							String loc[] = tmp[1].split("#94");
							System.out.println("equpment =" + val);
							System.out.println("equpment other =" + val_other);
							String equp[] = val.split("&#94;");

							for (int k = 0; k < equp.length; k++) {
								System.out.println("equivfor=" + Equp_val);
								equp[k].trim();
								if (equp[k].equals(cf
										.getResourcesvalue(R.string.hvacE))) {
									Equp_val += equp[k] + "^BRK"
											+ percent[0].trim() + "^BRK";
									Equp_loc += equp[k] + "^BRK"
											+ loc[0].trim() + "^BRK";
								} else if (equp[k]
										.equals(cf
												.getResourcesvalue(R.string.VentalationE))) {
									Equp_val += equp[k] + "^BRK"
											+ percent[1].trim() + "^BRK";
									Equp_loc += equp[k] + "^BRK"
											+ loc[1].trim() + "^BRK";
								} else if (equp[k].equals(cf
										.getResourcesvalue(R.string.StorageE))) {
									Equp_val += equp[k] + "^BRK"
											+ percent[2].trim() + "^BRK";
									Equp_loc += equp[k] + "^BRK"
											+ loc[2].trim() + "^BRK";
								} else if (equp[k].equals(cf
										.getResourcesvalue(R.string.OtherE))) {
									Equp_val += equp[k] + "("
											+ percent[3].trim() + ")^BRK";
								} else {
									Equp_val += equp[k] + "^BRK";
									
									 * if(k<=2) { Equp_loc+=equp[k]+"(),"; }
									 * else { //Equp_loc+=","; }
									 
								}
								System.out.println("equiv=" + Equp_val);

							}
							if (Equp_val.length() > 1) {
								Equp_val = Equp_val.substring(0,
										Equp_val.lastIndexOf("^"));
							}
							if (Equp_loc.length() > 1) {
								Equp_loc = Equp_loc.substring(0,
										Equp_loc.lastIndexOf("^"));
							}

						}
						// val+="^BRK"+val_other;
					}
					
					cf.request.addProperty("Roof_Mounted_Struct", Equp_val);
					cf.request.addProperty("Roof_Mounted_Location", Equp_loc);
					System.out.println(" the equepment value" + Equp_val);
					System.out.println(" the equepment location" + Equp_loc);
				}

				else if (tit.equals(cf.getResourcesvalue(R.string.COMER4)))*//***
				 * We
				 * check the title based on the tbl_master_info table custom id
				 ***//*
				{
					if (!val_other.equals("")) {
						val += "^BRK" + val_other;
					}
					cf.request.addProperty("Roof_Mounted_Euip", val);
				} else if (tit
						.equals(cf.getResourcesvalue(R.string.EquipmentL)))*//***
				 * We
				 * check the title based on the tbl_master_info table custom id
				 ***//*
				{
					if (!val_other.equals("")) {
						val += "^BRK" + val_other;
					}
					cf.request.addProperty("Roof_Mounted_Loose", val);
				} else if (tit.equals(cf.getResourcesvalue(R.string.DamageR)))*//***
				 * 
				 * We check the title based on the tbl_master_info table custom
				 * id
				 ***//*
				{
					if (!val_other.equals("")) {
						val += "^BRK" + val_other;
					}
					cf.request.addProperty("Demage_Roof_mount", val);
				} else if (tit.equals(cf.getResourcesvalue(R.string.COMER5)))*//***
				 * 
				 * We check the title based on the tbl_master_info table custom
				 * id
				 ***//*
				{
					if (!val_other.equals("")) {
						val += "^BRK" + val_other;
					}
					cf.request.addProperty("Roof_Other", val);
				} else if (tit.equals("ICCF")) {
					cf.request.addProperty("Include_CitizenFrm", val);
				} else if (!tit.equals("updatedDate")) {
					SoapObject req = cf
							.SoapRequest("RoofConditionsNoted_Type2");
					req.addProperty("Fld_Srid", cf.selectedhomeid);// "RI12348");
					req.addProperty("InspectionID", insp_online);
					req.addProperty("ROWID", id_val);
					// req.addProperty("InspectionID",insp);
					req.addProperty("Roof_Title", tit);
					req.addProperty("Roof_Value", val);
					req.addProperty("Roof_Description", val_other);
					System.out.println("called the other RCN proper" + req);
					result_val += "~"
							+ cf.SoapResponse("RoofConditionsNoted_Type2", req);

				}

				sub_total += roof_inc;
			}

			*//** Start adding the default values **//*

			if (insp.equals("7")) {
				cf.request.addProperty("Additional_Roof_Chk", false);
				cf.request.addProperty("AddlRoofchecked", 0);
			}
			if (!insp.equals("2")) {
				cf.request.addProperty("Include_CitizenFrm", false);

			}
			cf.request.addProperty("Apprx_remaning_Life", 0);

			*//** Start adding the default values ends **//*
			System.out.println("called the RCN property " + cf.request);
			result_val += "~"
					+ cf.SoapResponse("RoofConditionsNoted_Type1", cf.request);
			System.out.println("the overal roof result=" + result_val);
			// System.out.println("the result over all roof"+cf.SoapResponse("RoofConditionsNoted_Type1",
			// cf.request));
		}
		sub_total = 100;
		setincreament();
		;
		if (result_val.equals("")) {
			return false;
		} else if (result_val.contains("false")) {
			return false;
		} else {
			return true;
		}
	}
*/	
/*	private boolean EX_Roof_RCT(String insp) throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		// TODO Auto-generated method stub
		sub_total = 0;
		String RCT_type = "", result_val = "", insp_online = "";
		if (insp.equals("1"))
			insp_online = "13";
		else if (insp.equals("2"))
			insp_online = "750";
		else if (insp.equals("4"))
			insp_online = "9";
		else if (insp.equals("5"))
			insp_online = "62";
		else if (insp.equals("6"))
			insp_online = "90";
		else if (insp.equals("7"))
			insp_online = "11";
		cf.CreateARRTable(435);

		// RM_srid as RCT_srid,RM_page as RCT_page,RM_insp_id as
		// RCT_insp_id,RM_Covering as
		// RCT_FielsName,RCT_TypeValue,RCT_FieldVal,RCT_Other,RM_Enable as
		// RCT_Enable
		cf.request = cf.SoapRequest("RoofCoveringType");
		String sql = "SELECT GRI.*,mst.mst_text as title,opt.tit_max_w FROM "
				+ cf.Roof_covertype_view
				+ " as GRI LEFT JOIN "
				+ cf.option_list
				+ " as opt on opt.option_tit=GRI.RCT_TypeValue and module_id='2' and insp_id='"
				+ insp
				+ "'  left join "
				+ cf.master_tbl
				+ " mst  on mst_custom_id=GRI.RCT_TypeValue and mst_module_id='2' Where RCT_srid='"
				+ cf.selectedhomeid + "' and RCT_insp_id='" + insp
				+ "' ORDER BY RCT_FielsName";
		Cursor C_Roof = cf.arr_db.rawQuery(sql, null);
		C_Roof.moveToFirst();
		if (C_Roof.getCount() >= 1) {

			int roof_inc = 40 / C_Roof.getCount();
			cf.request.addProperty("Fld_Srid", cf.selectedhomeid);// "RI12348");
			cf.request.addProperty("InspectionID", insp_online);

			for (int i = 0; i < C_Roof.getCount(); i++, C_Roof.moveToNext()) {
				if (cf.decode(C_Roof.getString(C_Roof.getColumnIndex("title")))
						.equals(cf.RCT_tit)) {

					if (!cf.decode(
							C_Roof.getString(C_Roof
									.getColumnIndex("RCT_FieldVal"))).equals(
							RCT_type)
							&& !RCT_type.equals("")) {
						cf.envelope.setOutputSoapObject(cf.request);
						MarshalBase64 marshal = new MarshalBase64();
						marshal.register(cf.envelope);
						cf.envelope.setOutputSoapObject(cf.request);
						// System.out.println("no more issues4");
						System.out.println("Roof covering type=" + RCT_type
								+ "=" + cf.request);

						HttpTransportSE androidHttpTransport = new HttpTransportSE(
								cf.URL_ARR);

						androidHttpTransport.call(cf.NAMESPACE
								+ "RoofCoveringType", cf.envelope);
						result_val += cf.envelope.getResponse().toString()
								+ "~";
						System.out.println("the reult =" + result_val);
						cf.request = null;
						*//** uploading Roof images **//*

						send_roof_image(cf.selectedhomeid, RCT_type, insp,
								insp_online);

						*//** uploading Roof images ends **//*
						cf.request = cf.SoapRequest("RoofCoveringType");
						cf.request.addProperty("Fld_Srid", cf.selectedhomeid);// "RI12348");
						cf.request.addProperty("InspectionID", insp_online);

						RCT_type = cf.decode(C_Roof.getString(C_Roof
								.getColumnIndex("RCT_FieldVal")));
					} else {
						RCT_type = cf.decode(C_Roof.getString(C_Roof
								.getColumnIndex("RCT_FieldVal")));

					}
					cf.request.addProperty("CoveringType", cf.decode(C_Roof
							.getString(C_Roof.getColumnIndex("RCT_FieldVal"))));
					cf.request.addProperty("RoofOtherText", cf.decode(C_Roof
							.getString(C_Roof.getColumnIndex("RCT_Other"))));
				} else {
					*//** Start adding the filds of the roof cover type **//*
					String val = "", val_other = "";

					if (cf.decode(
							C_Roof.getString(C_Roof
									.getColumnIndex("RCT_TypeValue"))).equals(
							"6"))*//***
					 * We check the title based on the
					 * tbl_master_info table custom id
					 ***//*
					{

						val = cf.decode(C_Roof.getString(C_Roof
								.getColumnIndex("RCT_FieldVal")));
						val_other = cf.decode(C_Roof.getString(C_Roof
								.getColumnIndex("RCT_Other")));
					} else {
						val = cf.decode(C_Roof.getString(C_Roof
								.getColumnIndex("RCT_FieldVal")));
						val_other = cf.decode(C_Roof.getString(C_Roof
								.getColumnIndex("RCT_Other")));
						if (!val_other.equals("")) {
							val = "Other(" + val_other + ")";// append the other
																// text to the
																// value with
																// some spl
																// charactor
						}
					}

					if (cf.decode(
							C_Roof.getString(C_Roof
									.getColumnIndex("RCT_TypeValue"))).equals(
							"2"))*//***
					 * We check the title based on the
					 * tbl_master_info table custom id
					 ***//*
					{
						// if()
						cf.request.addProperty("PrmtDate", val);

					} else if (cf.decode(
							C_Roof.getString(C_Roof
									.getColumnIndex("RCT_TypeValue"))).equals(
							"3"))*//***
					 * We check the title based on the
					 * tbl_master_info table custom id
					 ***//*
					{
						cf.request.addProperty("YrInst", val);
					} else if (cf.decode(
							C_Roof.getString(C_Roof
									.getColumnIndex("RCT_TypeValue"))).equals(
							"4"))*//***
					 * We check the title based on the
					 * tbl_master_info table custom id
					 ***//*
					{
						if (val.equals("true")) {
							// val="false";

							cf.request.addProperty("Noinformation", true);

						} else {

							cf.request.addProperty("Noinformation", false);
						}

					} else if (cf.decode(
							C_Roof.getString(C_Roof
									.getColumnIndex("RCT_TypeValue"))).equals(
							"5"))*//***
					 * We check the title based on the
					 * tbl_master_info table custom id
					 ***//*
					{
						cf.request.addProperty("RoofSlope", val);
					} else if (cf.decode(
							C_Roof.getString(C_Roof
									.getColumnIndex("RCT_TypeValue"))).equals(
							"6"))*//***
					 * We check the title based on the
					 * tbl_master_info table custom id
					 ***//*
					{
						System.out.println("comes inthe shape");
						int percen = 0;
						String val_tmp = "";
						if (!val.equals("")) {

							String[] tmp = val.split("~");
							if (tmp.length > 1) {
								percen = Integer.parseInt(tmp[1]);
								val = tmp[0];

							}

						}

						if (!val_other.equals("")) {
							val = "Other(" + val_other + ")";// append the other
																// text to the
																// value with
																// some spl
																// charactor
						}
						cf.request.addProperty("RoofShap", val);
						cf.request.addProperty("RoofShapPercent", percen);
					} else if (cf.decode(
							C_Roof.getString(C_Roof
									.getColumnIndex("RCT_TypeValue"))).equals(
							"7"))*//***
					 * We check the title based on the
					 * tbl_master_info table custom id
					 ***//*
					{
						cf.request.addProperty("RoofStruct", val);
					} else if (cf.decode(
							C_Roof.getString(C_Roof
									.getColumnIndex("RCT_TypeValue"))).equals(
							"8"))*//***
					 * We check the title based on the
					 * tbl_master_info table custom id
					 ***//*
					{
						val = convefromabreviation(cf.decode(C_Roof
								.getString(C_Roof
										.getColumnIndex("RCT_FieldVal"))));
						cf.request.addProperty("BuildingCode", val);
					} else if (cf.decode(
							C_Roof.getString(C_Roof
									.getColumnIndex("RCT_TypeValue"))).equals(
							"9"))*//***
					 * We check the title based on the
					 * tbl_master_info table custom id
					 ***//*
					{
						cf.request.addProperty("RoofComplxty", val);
					} else if (cf.decode(
							C_Roof.getString(C_Roof
									.getColumnIndex("RCT_TypeValue"))).equals(
							"10"))*//***
					 * We check the title based on the
					 * tbl_master_info table custom id
					 ***//*
					{
						cf.request.addProperty("ApprRemain", val);
					} else if (cf.decode(
							C_Roof.getString(C_Roof
									.getColumnIndex("RCT_TypeValue"))).equals(
							"11"))*//***
					 * We check the title based on the
					 * tbl_master_info table custom id
					 ***//*
					{
						if (val.equals("true")) {
							cf.request.addProperty("PrdmntRoofCvr", true);
							// val="false";
						} else {
							cf.request.addProperty("PrdmntRoofCvr", false);
						}

					}
					
					 * else if
					 * (cf.decode(C_Roof.getString(C_Roof.getColumnIndex(
					 * "RCT_TypeValue"))).equals("12"))
					 *//***
					 * We check the title based on the tbl_master_info table
					 * custom id
					 ***//*
					
					 * { if(val.equals("")) {
					 * cf.request.addProperty("PhotoName","");
					 * cf.request.addProperty("byteArrayIn",""); } else {
					 * 
					 * String name=val.substring(val.lastIndexOf("/")+1);
					 * cf.request.addProperty("PhotoName",name);
					 *//*** Start converting image to byte ***//*
					
					 * Bitmap bitmap = cf.ShrinkBitmap(val, 400, 400);
					 * 
					 * ByteArrayOutputStream out = new ByteArrayOutputStream();
					 * bitmap.compress(CompressFormat.PNG, 100, out); byte[] raw
					 * = out.toByteArray(); String imagetype="1";
					 * cf.request.addProperty("byteArrayIn",raw); }
					 * //byteArrayIn
					 *//*** Start converting image to byte ends ***//*
					
					 * 
					 * }
					 
					*//** Ends adding the filds of the roof cover type **//*
				}
				sub_total += roof_inc;
			}
			if (!RCT_type.equals("")) {
				cf.envelope.setOutputSoapObject(cf.request);
				MarshalBase64 marshal = new MarshalBase64();
				marshal.register(cf.envelope);
				cf.envelope.setOutputSoapObject(cf.request);

				System.out.println("Roof covering type=" + RCT_type + "="
						+ cf.request);

				HttpTransportSE androidHttpTransport = new HttpTransportSE(
						cf.URL_ARR);
				androidHttpTransport.call(cf.NAMESPACE + "RoofCoveringType",
						cf.envelope);
				result_val += cf.envelope.getResponse().toString() + "~";
				System.out.println("the result was " + result_val);
				cf.request = null;
				*//** uploading Roof images **//*

				send_roof_image(cf.selectedhomeid, RCT_type, insp, insp_online);

				*//** uploading Roof images ends **//*
				// cf.request=cf.SoapRequest("RoofCoveringType");
				// EX_CHKGCH[3]=
				// cf.SoapResponse("ExportGCHSummaryHazardsConcerns",cf.request);
			}

		}
		sub_total = 50;
		if (result_val.equals("")) {
			return false;
		} else if (result_val.contains("false")) {
			return false;
		} else {
			return true;
		}

		// return result;
	}
*/
	private void send_roof_image(String selectedhomeid, String RCT_type,
			String insp, String insp_online) throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException {
		String result_val = "";
		/*** Start uploading the roof cover image section ***/

		Cursor image = cf.arr_db.rawQuery("Select * from " + cf.Roofcoverimages
				+ " Where RIM_masterid=(Select RM_masterid from "
				+ cf.Roof_master + " Where RM_inspectorid='" + cf.Insp_id
				+ "' and RM_srid='" + cf.selectedhomeid + "' and RM_insp_id='"
				+ insp + "' and RM_Covering='" + cf.encode(RCT_type)
				+ "') order by RIM_Order", null);
		/*
		 * System.out.println("its works correctly"+"Select * from "+cf.
		 * Roofcoverimages
		 * +" Where RIM_masterid=(Select RM_masterid from "+cf.Roof_master+
		 * " Where RM_inspectorid='"
		 * +cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid
		 * +"' and RM_insp_id='"+insp
		 * +"' and RM_Covering='"+cf.encode(RCT_type)+"') order by RIM_Order");
		 */
		image.moveToFirst();

		if (image.getCount() > 0) {
			int roof_inc = 10 / image.getCount();
			for (int im = 0; im < image.getCount(); im++, image.moveToNext()) {
				cf.request = cf.SoapRequest("ELEVATIONPHOTOSECTION");
				try {
					if (new File(cf.decode(image.getString(image
							.getColumnIndex("RIM_Path")))).exists()) {
						byte[] raw = null;
						Bitmap bitmap = cf.ShrinkBitmap(cf.decode(image
								.getString(image.getColumnIndex("RIM_Path"))),
								400, 400);
						MarshalBase64 marshal = new MarshalBase64();
						marshal.register(cf.envelope);
						ByteArrayOutputStream out = new ByteArrayOutputStream();
						bitmap.compress(CompressFormat.PNG, 100, out);
						raw = out.toByteArray();
						cf.request.addProperty("image", raw);
					} else {
						Error_tracker += "Please check the following Image is available in "
								+ cf.All_insp_list[Integer
										.parseInt(insp.trim())]
								+ ", Roof cover type ="
								+ RCT_type
								+ ", image order = "
								+ image.getInt(image
										.getColumnIndex("RIM_Order"))
								+ " Path="
								+ cf.decode(image.getString(image
										.getColumnIndex("RIM_Path"))) + " @";
						result_val += "~false";
					}
				} catch (Exception e) {
					// TODO: handle exception
					System.out.println("comes in the catch ");
					Error_tracker += "Please check the following Image is available in "
							+ cf.All_insp_list[Integer.parseInt(insp.trim())]
							+ ", Roof cover type ="
							+ RCT_type
							+ ", image order = "
							+ image.getInt(image.getColumnIndex("RIM_Order"))
							+ " Path="
							+ cf.decode(image.getString(image
									.getColumnIndex("RIM_Path"))) + " @";
					cf.request.addProperty("image", "");
				}
				String name = cf.decode(image.getString(image
						.getColumnIndex("RIM_Path")));
				name = name.substring(name.lastIndexOf("/") + 1, name.length());
				cf.request.addProperty("Fld_Srid", cf.selectedhomeid);
				cf.request.addProperty("InspectionID", insp_online);
				cf.request.addProperty("ElevationTypeid", 24);// Default for the Roof cove image 
				
				cf.request.addProperty("Coveringtype", RCT_type);
				cf.request.addProperty("ImageName", name);
				cf.request.addProperty("Description", cf.decode(image
						.getString(image.getColumnIndex("RIM_Caption"))));
				cf.request.addProperty("Order",
						image.getInt(image.getColumnIndex("RIM_Order")));
				cf.request.addProperty("ROWID",
						image.getInt(image.getColumnIndex("RIM_Order")));
				//cf.request.addProperty("Flag", "");
			//	System.out.println(" the request"+cf.request);
				cf.envelope.setOutputSoapObject(cf.request);
				MarshalBase64 marshal = new MarshalBase64();
				marshal.register(cf.envelope);

				cf.envelope.setOutputSoapObject(cf.request);

				System.out.println("Roof covering type=" + RCT_type + "="
						+ cf.request);

				HttpTransportSE androidHttpTransport1 = new HttpTransportSE(
						cf.URL_ARR);
				androidHttpTransport1.call(cf.NAMESPACE + "ELEVATIONPHOTOSECTION",
						cf.envelope);
				result_val += cf.envelope.getResponse().toString() + "~";
				System.out.println("Roof cover image  " + im + result_val);
				cf.request = null;
				sub_total += roof_inc;

			}
		}
		
		/*** uploading the roof cover image section ends ***/
		// TODO Auto-generated method stub

	}

	/*** Common roof export ends */
	/** Photos and feed back starts **/
	private boolean Expot_photos(String insp_id) throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException {
		System.gc();
		sub_total = 0;
		cf.CreateARRTable(81);
		String sql = " Select * from " + cf.ImageTable + " Where ARR_IM_SRID='"
				+ cf.selectedhomeid + "' and  ARR_IM_Insepctiontype ='"
				+ insp_id + "' ";
		System.out.println("sql="+sql);
		Cursor C_photos = cf.arr_db.rawQuery(sql, null);	System.out.println("C_photos="+C_photos.getCount());
		C_photos.moveToFirst();
		String result_val = "";
		if (C_photos.getCount() >= 1) {
			current_val_cur = C_photos;

			int photo_inc = 100 / C_photos.getCount();
			C_photos.moveToFirst();
			String insp_online = "", insp = "", path = "", name = "";
			boolean b = false;
			for (int i = 0; i < C_photos.getCount(); i++, C_photos.moveToNext()) {
				current_pos = i;
				sub_insp = "Photos " + (i + 1) + "/" + C_photos.getCount();
				b = false;
				try {
					bitmap.recycle();// to release the memory
				} catch (Exception e) {
					// TODO: handle exception
				}

				insp = C_photos.getString(C_photos
						.getColumnIndex("ARR_IM_Insepctiontype"));
				insp_online = getinspection_online_no(insp);
				path = cf.decode(C_photos.getString(C_photos
						.getColumnIndex("ARR_IM_ImageName")));
				name = path.substring(path.lastIndexOf("/") + 1);
				File f = new File(path);
				if (f.exists()) {
					cf.request = cf.SoapRequest("ELEVATIONPHOTOSECTION");
					try {
						bitmap = ShrinkBitmap(path, 400, 400);// BitmapFactory.decodeFile(path);//ShrinkBitmap(path,
																// 800, 800);
						System.out.println("comes correc1");
						MarshalBase64 marshal = new MarshalBase64();
						marshal.register(cf.envelope);
						ByteArrayOutputStream out = new ByteArrayOutputStream();
						bitmap.compress(CompressFormat.PNG, 100, out);
						byte[] raw = out.toByteArray();
						cf.request.addProperty("image", raw);
					} catch (Exception e) {
						// TODO: handle exception

						Error_tracker += "You have problem in exporting Image in "
								+ cf.All_insp_list[Integer.parseInt(insp_id
										.trim())]
								+ " , Elevation= "
								+ getelvationnumber(C_photos.getInt(C_photos
										.getColumnIndex("ARR_IM_Elevation")),
										insp)
								+ " ,image order = "
								+ C_photos.getString(C_photos
										.getColumnIndex("ARR_IM_ImageOrder"))
								+ " Path=" + path + " @";
					}

					System.out.println("comes correc 2");
					// cf.request.addProperty("byteArrayIn",raw);
					System.out.println("comes correc 3");

					cf.request.addProperty("Fld_Srid", cf.selectedhomeid);// "RI12348");
					cf.request.addProperty("InspectionID", insp_online);
					//int elev_online = getelevationidforonline(Integer.parseInt(C_photos.getString(C_photos.getColumnIndex("ARR_IM_Elevation"))));// for
																			// online
																			// has
																			// changed
																			// the
																			// eleation
																			// no
																			// so
																			// ve
																			// changed
					int elev_online = Integer.parseInt(C_photos.getString(C_photos.getColumnIndex("ARR_IM_Elevation")));// for
					cf.request.addProperty("ElevationTypeid", elev_online);
					cf.request.addProperty("ElevationOther", "");

					cf.request.addProperty("ImageName", name);
					cf.request.addProperty("Description", cf.decode(C_photos
							.getString(C_photos
									.getColumnIndex("ARR_IM_Description"))));
					cf.request.addProperty("Order", C_photos.getString(C_photos
							.getColumnIndex("ARR_IM_ImageOrder")));
					cf.request.addProperty("ROWID", C_photos.getString(C_photos
							.getColumnIndex("ARR_IM_ID")));
					cf.request.addProperty("Coveringtype", "");
					cf.envelope.setOutputSoapObject(cf.request);
					MarshalBase64 marshal = new MarshalBase64();
					marshal.register(cf.envelope);
					cf.envelope.setOutputSoapObject(cf.request);
					// System.out.println("no more issues4");
					System.out.println("Photos=" + cf.request);
					b = true;

					if (b) {
						HttpTransportSE androidHttpTransport = new HttpTransportSE(
								cf.URL_ARR);
						androidHttpTransport.call(cf.NAMESPACE
								+ "ELEVATIONPHOTOSECTION", cf.envelope);
						result_val += cf.envelope.getResponse().toString()
								+ "~";
						System.out.println("the photos reult =" + result_val);
					} else {
						result_val += "false~";
					}
					// EX_CHKGCH[3]=
					// cf.SoapResponse("ExportGCHSummaryHazardsConcerns",cf.request);

				} else {
					Error_tracker += "Please check the following Image is available in "
							+ cf.All_insp_list[Integer.parseInt(insp_id.trim())]
							+ ", Elevation ="
							+ getelvationnumber(C_photos.getInt(C_photos
									.getColumnIndex("ARR_IM_Elevation")), insp)
							+ ", image order = "
							+ C_photos.getString(C_photos
									.getColumnIndex("ARR_IM_ImageOrder"))
							+ " Path=" + path + " @";
					// photos_missingfiles+=cf.decode(C_photos.getString(C_photos.getColumnIndex("ARR_IM_Description")))+"&#94;";
					result_val += "false~";
					// System.out.println("File missing");
				}

				sub_total += photo_inc;
				// }
			}
			sub_total = 100;
			setincreament();
			;
			if (result_val.contains("false"))
				return false;
			else
				return true;
		} else {
			return true;
		}

	}

	private boolean Expot_feedback(String insp) throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException {
		System.out.println("its commercial");
		
		cf.CreateARRTable(83);
		String sql = " Select * from " + cf.FeedBackInfoTable
				+ " Where ARR_FI_Srid='" + cf.selectedhomeid
				+ "' and  ARR_FI_Inspection ='" + insp + "' ";
		
		Cursor C_feed_i = cf.arr_db.rawQuery(sql, null);
		System.out.println("sql="+sql+"count="+C_feed_i.getCount());
		C_feed_i.moveToFirst();
		String result_val = "";
		sub_total = 0;
		String insp_online1="";
		if (C_feed_i.getCount() >= 1) {
			sub_insp = "Feedback Information";
			C_feed_i.moveToFirst();
			for (int i = 0; i < C_feed_i.getCount(); i++, C_feed_i.moveToNext()) {
				if(insp.equals("10"))
				{
					 insp_online1 = C_feed_i.getString(C_feed_i.getColumnIndex("ARR_ISCUSTOM"));
				}
				else
				{
					 insp_online1 = getinspection_online_no(C_feed_i.getString(C_feed_i.getColumnIndex("ARR_FI_Inspection")));	
				}
				
				cf.request = cf.SoapRequest("FEEDBACKINFORMATION");
				cf.request.addProperty("Fld_Srid", cf.selectedhomeid);
				cf.request.addProperty("InspectionID", insp_online1);
				cf.request.addProperty("IsHOSatisfied",cf.decode(C_feed_i.getString(C_feed_i.getColumnIndex("ARR_FI_IsInspectionPaperAvbl"))));
				cf.request.addProperty("IsCusServiceCompltd", cf.decode(C_feed_i.getString(C_feed_i.getColumnIndex("ARR_FI_IsManufacturerInfo"))));
				System.out.println("requ=="+cf.request);
				String whopresent = cf.decode(C_feed_i.getString(C_feed_i.getColumnIndex("ARR_FI_PresentatInspection")));
				
				
				
				if (whopresent.contains("&#40;")) {
					String[] s = whopresent.split("&#40;");
					s[0] = s[0].replace("&#94;", "&#44;");
					cf.request.addProperty("PresentatInspection", s[0] + "&#44;Other("
							+ s[1] + ")");
				} else {
					whopresent = whopresent.replace("&#94;","&#44;");
					cf.request.addProperty("PresentatInspection", whopresent);
				}
				
				
				//cf.request.addProperty("PresentatInspection",cf.decode(C_feed_i.getString(C_feed_i.getColumnIndex("ARR_FI_PresentatInspection"))));
				cf.request.addProperty("OtherPresent", cf.decode(C_feed_i.getString(C_feed_i.getColumnIndex("ARR_FI_othertxt"))));
				cf.request.addProperty("FeedbackComments", cf.decode(C_feed_i.getString(C_feed_i.getColumnIndex("ARR_FI_FeedbackComments"))));
				System.out.println("feed info request val=" + cf.request);
				result_val += cf
						.SoapResponse("FEEDBACKINFORMATION", cf.request) + "~";
				System.out.println("Feed info rsult" + result_val);
			}
			sub_total += 50;System.out.println("result=="+result_val);
			if (result_val.contains("false")) {
				return false;
			} else {

				/** Start export feedback document ***/
				String sql1 = " Select * from " + cf.FeedBackDocumentTable
						+ " Where ARR_FI_D_SRID='" + cf.selectedhomeid
						+ "' and  ARR_FI_D_Inspection ='" + insp + "' ";
				System.out.println("sql1=="+sql1);
				Cursor C_feed_d = cf.arr_db.rawQuery(sql1, null);System.out.println("C_feed_d=="+C_feed_d.getCount());
				if(C_feed_d.getCount()>0)
				{
				C_feed_d.moveToFirst();
				String result_val_d = "";
				String[] array_doc = new String[12];
				array_doc[0] = "--Select--";
				array_doc[1] = "Acknowledgement Form";
				array_doc[2] = "CSE Form";
				array_doc[3] = "OIR 1802 Form";
				array_doc[4] = "Paper Signup Sheet";
				array_doc[5] = "Other Information";
				array_doc[6] = "Roof Permit ";
				array_doc[7] = "Sketch";
				array_doc[8] = "Building Permit";
				array_doc[9] = "Property Appraisal Information";
				array_doc[10] = "Field Inspection Report";
				array_doc[11] = "Field Notes";
				boolean com = false;
				int feed_inc = 50 / C_feed_d.getCount();
				current_val_cur = C_feed_d;

				for (int i = 0; i < C_feed_d.getCount(); i++, C_feed_d
						.moveToNext()) {
					current_pos = i;
					sub_insp = "Feedback Documents " + (i + 1) + "/"
							+ C_feed_d.getCount();
					com = false;
					try {
						bitmap.recycle();// to release the memory
					} catch (Exception e) {
						// TODO: handle exception
					}

					String tit = cf.decode(C_feed_d.getString(C_feed_d
							.getColumnIndex("ARR_FI_D_DocumentTitle")));
					Boolean titb = false;
					for (int n = 0; n < array_doc.length; n++) {
						if (array_doc[n].equals(tit)) {
							titb = true;
						}
					}
					String path = cf.decode(C_feed_d.getString(C_feed_d
							.getColumnIndex("ARR_FI_D_FileName")));
					String name = path.substring(path.lastIndexOf("/") + 1);
					Boolean b = (C_feed_d.getString(C_feed_d
							.getColumnIndex("ARR_FI_D_IsOfficeUse"))
							.equals("1")) ? true : false;
					String insp_online="";

					if(C_feed_d.getString(C_feed_d.getColumnIndex("ARR_FI_D_Inspection")).equals("10"))
					{
						 insp_online = C_feed_d.getString(C_feed_d.getColumnIndex("ARR_ISCUSTOM"));
					}
					else
					{
						 insp_online = getinspection_online_no(C_feed_d.getString(C_feed_d.getColumnIndex("ARR_FI_D_Inspection")));	
					}
					
					cf.request = cf.SoapRequest("FEEDBACKDOCUMENTS");
					cf.request.addProperty("Fld_Srid", cf.selectedhomeid);
					cf.request.addProperty("InspectionID", insp_online);
					if (titb) {
						cf.request.addProperty("DocumentTitle", tit);

						cf.request.addProperty("DocumentOther", "");
					} else {
						cf.request.addProperty("DocumentTitle", array_doc[5]);
						cf.request.addProperty("DocumentOther", tit);
					}

					cf.request.addProperty("Documentsname", name);
					cf.request.addProperty("IsOfficeDocument", b);
					cf.request.addProperty("ROWID", cf.decode(C_feed_d
							.getString(C_feed_d
									.getColumnIndex("ARR_FI_DocumentId"))));
					System.out.println("REquest Feedback=="+cf.request);
					if (name.endsWith(".pdf")) {

						File dir = Environment.getExternalStorageDirectory();
						if (path.startsWith("file:///")) {
							path = path.substring(11);
						}

						File assist = new File(path);
						if (assist.exists()) {
							try {
								String mypath = path;
								String temppath[] = mypath.split("/");
								int ss = temppath[temppath.length - 1]
										.lastIndexOf(".");
								String tests = temppath[temppath.length - 1]
										.substring(ss);
								String namedocument;
								InputStream fis = new FileInputStream(assist);
								long length = assist.length();
								byte[] bytes = new byte[(int) length];
								int offset = 0;
								int numRead = 0;
								while (offset < bytes.length
										&& (numRead = fis.read(bytes, offset,
												bytes.length - offset)) >= 0) {
									offset += numRead;
								}
								Object strBase64 = Base64.encode(bytes);
								cf.request.addProperty("Documents", strBase64);

								System.out.println("Feed info rsult strBase64"+strBase64+"Resul"+ result_val);
								com = true;
							} catch (Exception e) {
								// TODO: handle exception.
								Error_tracker += "You have problem uploadin Feed back information of "
										+ cf.All_insp_list[Integer
												.parseInt(insp.trim())] + "@";
							}
							if (com) {
								result_val += cf.SoapResponse(
										"FEEDBACKDOCUMENTS", cf.request) + "~";
							} else {
								result_val += "false~";
							}

						} else {
							Error_tracker += "Please check the following Image/Document is available "
									+ cf.All_insp_list[Integer.parseInt(insp
											.trim())] + " Path=" + path + " @";
							/* feed_missingfiles+=tit+"&#94;"; */
							result_val += "false~";
							/* System.out.println("file missing"); */
						}

					} else {
						bitmap = null;
					
						File f = new File(path);
						if (f.exists()) {
							/***
							 * Check the file size compress the based on the
							 * size
							 **/
							/***
							 * Check the file size compress the based on the
							 * size ends
							 **/
							
							try {
								bitmap = ShrinkBitmap(path, 400, 400);// BitmapFactory.decodeFile(path);//ShrinkBitmap(path,
																		// 800,
																		// 800);
								MarshalBase64 marshal = new MarshalBase64();
								marshal.register(cf.envelope);
								ByteArrayOutputStream out = null;
								out = new ByteArrayOutputStream();
								bitmap.compress(CompressFormat.PNG, 100, out);
								byte[] raw = out.toByteArray();

								cf.request.addProperty("Documents", raw);
								System.out.println("feeedback request"+cf.request);
								
								com = true;
							} catch (Exception e) {
								// TODO: handle exception
								Error_tracker += "You have problem uploadin Feed back information "
										+ cf.All_insp_list[Integer
												.parseInt(insp.trim())] + "@";
							}
							if (com) {
								result_val += cf.SoapResponse(
										"FEEDBACKDOCUMENTS", cf.request) + "~";
							} else {
								result_val += "false";
							}
							System.out.println("Feed doc rsult" + result_val);
						} else {
							// feedback_fileexits=true;
							Error_tracker += "Please check the following Image/Document is available "
									+ cf.All_insp_list[Integer.parseInt(insp
											.trim())] + " Path=" + path + " @";
							result_val += "false~";

						}

					}

					// System.out.println("feed info request val="+cf.request);

					sub_total += feed_inc;
				}

				/** export feedback document ends ***/
				// }
			}
			}
			sub_total = 100;
			setincreament();
			;
			if (result_val.contains("false"))
				return false;
			else if (result_val.equals(""))
				return false;
			else
				return true;

		}
		else
		{
			return true;
		}
	}

	/** Photos and feed back ends **/
	/*** Module wise export function Ends ***/
	/**** Exporting function ends ****/
	/*** Support function for exporting **/
	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			// pd.dismiss();
			
			Total = 100;
			if (show_handler == 1) {


				if (EX_Result[0] == true && EX_Result[1] == true
						&& EX_Result[2] == true && EX_Result[4] == true
						&& EX_Result[5] == true && EX_Result[6] == true
						&& EX_Result[7] == true && EX_Result[8] == true
						&& EX_Result[9] == true && EX_Result[3] == true) {
					dialog.setCancelable(true);
					dialog.dismiss();
					AlertDialog alertDialog = new AlertDialog.Builder(Export.this).create();
					alertDialog.setMessage(status);
					alertDialog.setButton("OK",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int which) {
							Intent in = new Intent(getApplicationContext(), Export.class);
							in.putExtra("type", "export");
							startActivity(in);
													
						}
					});
					alertDialog.show();
					
				}
			} else if (show_handler == 2) {
				// cf.ShowToast(errorlog, 0);
				if (Error_tracker.length() > 2) {
					Error_tracker = Error_tracker.substring(0,
							Error_tracker.length() - 1);
				}
				Intent n = new Intent(Export.this, ExportAlert.class);
				n.putExtra("Srid", dt);
				n.putExtra("insp", insp_boo);
				n.putExtra("result", "true");
				n.putExtra("general", general_sta);
				n.putExtra("four", four_sta);
				n.putExtra("survey", survey_sta);
				n.putExtra("B11802", B11802_sta);
				n.putExtra("COM1", COM1_sta);
				n.putExtra("COM2", COM2_sta);
				n.putExtra("COM3", COM3_sta);
				n.putExtra("GCH", GCH_sta);
				n.putExtra("SINK", SINK_sta);
				n.putExtra("DRYWALL", DRYWALL_sta);
				n.putExtra("CUSTOM", custom_sta);
				n.putExtra("error", Error_tracker);
				startActivityForResult(n, 825);
				
			}
			if(wl!=null)
			{
			wl.release();
			wl=null;
			}
			/* for(int i=0;i<GCH_sta.length;i++) {
			 System.out.println("the insp val"+i+"="+GCH_sta[i]); }
			 
			*/
			
		}
	};

	public boolean check_Status(String result) {
		// TODO Auto-generated method stub
		try {
			if (result != null) {
				if (result.trim().equals("true")) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}

		} catch (Exception e) {
			return false;
		}
	}

	Bitmap ShrinkBitmap(String file, int width, int height) {

		try {
			BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
			bmpFactoryOptions.inJustDecodeBounds = true;
			Bitmap bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);

			int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight
					/ (float) height);
			int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth
					/ (float) width);

			if (heightRatio > 1 || widthRatio > 1) {
				if (heightRatio > widthRatio) {
					bmpFactoryOptions.inSampleSize = heightRatio;
				} else {
					bmpFactoryOptions.inSampleSize = widthRatio;
				}
			}

			bmpFactoryOptions.inJustDecodeBounds = false;
			bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
			return bitmap;
		} catch (Exception e) {
			return bitmap;
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			getselectedinsp(data);
		} else {
			Intent in = new Intent(this, Export.class);
			in.putExtra("type", "export");
			startActivity(in);
		}

	}

	private void getselectedinsp(Intent data) {
		// TODO Auto-generated method stub
		Error_tracker = "";
		insp_boo = data.getStringArrayExtra("insp");
		four_boo = data.getStringArrayExtra("four");
		general_boo = data.getStringArrayExtra("general");
		survey_boo = data.getStringArrayExtra("survey");
		B11802_boo = data.getStringArrayExtra("B11802");
		COM1_boo = data.getStringArrayExtra("COM1");
		COM2_boo = data.getStringArrayExtra("COM2");
		COM3_boo = data.getStringArrayExtra("COM3");
		GCH_boo = data.getStringArrayExtra("GCH");
		SINK_boo = data.getStringArrayExtra("SINK");
		DRYWALL_boo = data.getStringArrayExtra("DRYWALL");
		CUSTOM_boo = data.getStringArrayExtra("CUSTOM");

		/*
		 * for(int i=0;i<B11802_boo.length;i++) {
		 * System.out.println("the insp val"+i+"="+B11802_boo[i]); } for(int
		 * i=0;i<COM1_boo.length;i++) {
		 * System.out.println("the insp val"+i+"="+COM1_boo[i]); }
		 */
		/*** Get teh number of insepction selected ***/

		int j = 0;
		for (int i = 0; i < insp_boo.length; i++) {

			if (insp_boo[i].equals("true")) {
				j++;
			}

		}
		if (j != 0) {
			PerIns_inc = 100 / j;
		}

		/*** Get teh number of insepction selected ends ***/
		for (int i = 0; i < four_sta.length; i++) {
			four_sta[i] = "";

		}
		for (int i = 0; i < general_sta.length; i++) {
			general_sta[i] = "";

		}
		for (int i = 0; i < survey_sta.length; i++) {
			survey_sta[i] = "";

		}
		for (int i = 0; i < B11802_sta.length; i++) {
			B11802_sta[i] = "";

		}
		for (int i = 0; i < COM1_sta.length; i++) {
			COM1_sta[i] = "";

		}
		for (int i = 0; i < COM2_sta.length; i++) {
			COM2_sta[i] = "";

		}
		for (int i = 0; i < COM3_sta.length; i++) {
			COM3_sta[i] = "";

		}
		for (int i = 0; i < GCH_sta.length; i++) {
			GCH_sta[i] = "";

		}
		for (int i = 0; i < SINK_sta.length; i++) {
			SINK_sta[i] = "";

		}
		for (int i = 0; i < DRYWALL_sta.length; i++) {
			DRYWALL_sta[i] = "";

		}
		for (int i = 0; i < CUSTOM_sta.length; i++) {
			CUSTOM_sta[i] = "";

		}
		// System.out.println("comes correctly ibfr the export");
		fn_export();
	}

	public String getelvationnumber(int selected, String insp) {
		String elev = "";
		if (insp.equals("1")) {
			if (selected == 9)
				elev = "Exterior Photographs";
			if (selected == 10)
				elev = "Roof Photographs";
			if (selected == 11)
				elev = "Plumbing Photographs";
			if (selected == 12)
				elev = "Electrical Photographs";
			if (selected == 13)
				elev = "HVAC Photographs";
			if (selected == 14)
				elev = "Other Pictures";
		} else if (insp.equals("2")) {
			if (selected == 15)
				elev = "Roof Photographs";
			if (selected == 16)
				elev = "Other Pictures";
			/** need to change ***/
			if (selected == 62)
				elev = "Exterior Pictures";
			if (selected == 63)
				elev = "Attic Pictures";

		} else if (insp.equals("3")) {
			if (selected == 17)
				elev = "Owner signature";
			if (selected == 18)
				elev = "Front Elevation";
			if (selected == 19)
				elev = "Right Elevation";
			if (selected == 20)
				elev = "Left Elevation";
			if (selected == 21)
				elev = "Back Elevation";
			if (selected == 22)
				elev = "Attic Photographs";
			if (selected == 23)
				elev = "Additional Photographs";
			if (selected == 24)
				elev = "Other Pictures";
		} else if (insp.equals("4")) {
			if (selected == 25)
				elev = "Owner signature";
			if (selected == 27)
				elev = "Front Elevation";
			if (selected == 28)
				elev = "Right Elevation";
			if (selected == 29)
				elev = "Left Elevation";
			if (selected == 30)
				elev = "Back Elevation";
			if (selected == 31)
				elev = "Attic Photographs";
			if (selected == 32)
				elev = "Additional Photographs";
			if (selected == 33)
				elev = "Other Pictures";
		} else if (insp.equals("5")) {
			if (selected == 34)
				elev = "Owner signature";
			if (selected == 35)
				elev = "Front Elevation";
			if (selected == 36)
				elev = "Right Elevation";
			if (selected == 37)
				elev = "Left Elevation";
			if (selected == 38)
				elev = "Back Elevation";
			if (selected == 39)
				elev = "Attic Photographs";
			if (selected == 40)
				elev = "Additional Photographs";
			if (selected == 41)
				elev = "Other Pictures";
		} else if (insp.equals("6")) {
			if (selected == 42)
				elev = "Owner signature";
			if (selected == 43)
				elev = "Front Elevation";
			if (selected == 44)
				elev = "Right Elevation";
			if (selected == 45)
				elev = "Left Elevation";
			if (selected == 46)
				elev = "Back Elevation";
			if (selected == 47)
				elev = "Attic Photographs";
			if (selected == 48)
				elev = "Additional Photographs";
			if (selected == 49)
				elev = "Other Pictures";
		} else if (insp.equals("7")) {
			if (selected == 1)
				elev = "Front Elevation";
			if (selected == 2)
				elev = "Right Elevation";
			if (selected == 3)
				elev = "Left Elevation";
			if (selected == 4)
				elev = "Back Elevation";
			if (selected == 5)
				elev = "Attic Elevation";
			if (selected == 6)
				elev = "Additional Photographs";
			if (selected == 7)
				elev = "Roof Photographs";
			if (selected == 8)
				elev = "Other Pictures";
		} else if (insp.equals("8")) {
			if (selected == 50)
				elev = "Owner signature";
			if (selected == 51)
				elev = "External photos";
			if (selected == 52)
				elev = "Roof and attic photos";
			if (selected == 53)
				elev = "Ground and adjoining images";
			if (selected == 54)
				elev = "Internal photos";
			if (selected == 55)
				elev = "Additional Photographs";
			if (selected == 56)
				elev = "Other Pictures";
		} else if (insp.equals("9")) {
			if (selected == 57)
				elev = "Copper wiring";
			if (selected == 58)
				elev = "Copper wiring at outlets and switches";
			if (selected == 59)
				elev = "Metal Fixtures";
			if (selected == 60)
				elev = "Ceiling board stamps";
			if (selected == 61)
				elev = "Other Pictures";
		}
		return elev;
	}

	private int getelevationidforonline(int selected) {
		// TODO Auto-generated method stub
		int elev = 0;

		if (selected == 9)
			elev = 1;
		if (selected == 10)
			elev = 2;
		if (selected == 11)
			elev = 3;
		if (selected == 12)
			elev = 4;
		if (selected == 13)
			elev = 5;
		if (selected == 14)
			elev = 7;
		if (selected == 64)
			elev = 23;

		if (selected == 15)
			elev = 2;
		if (selected == 16)
			elev = 7;
		/** need to change ***/
		if (selected == 62)
			elev = 1;
		if (selected == 63)
			elev = 6;
		if (selected == 65)
			elev = 23;

		// if(selected==17) elev= "Owner signature";
		if (selected == 18)
			elev = 17;
		if (selected == 19)
			elev = 18;
		if (selected == 20)
			elev = 19;
		if (selected == 21)
			elev = 20;
		if (selected == 22)
			elev = 6;
		if (selected == 23)
			elev = 12;
		if (selected == 24)
			elev = 7;
		if (selected == 66)
			elev = 23;

		// if(selected==25) elev= "Owner signature";
		if (selected == 27)
			elev = 17;
		if (selected == 28)
			elev = 18;
		if (selected == 29)
			elev = 19;
		if (selected == 30)
			elev = 20;
		if (selected == 31)
			elev = 6;
		if (selected == 32)
			elev = 12;
		if (selected == 33)
			elev = 7;
		if (selected == 67)
			elev = 23;

		// if(selected==34) elev= "Owner signature";
		if (selected == 35)
			elev = 17;
		if (selected == 36)
			elev = 18;
		if (selected == 37)
			elev = 19;
		if (selected == 38)
			elev = 20;
		if (selected == 39)
			elev = 6;
		if (selected == 40)
			elev = 12;
		if (selected == 41)
			elev = 7;
		if (selected == 68)
			elev = 23;

		// if(selected==42) elev= "Owner signature";
		if (selected == 43)
			elev = 17;
		if (selected == 44)
			elev = 18;
		if (selected == 45)
			elev = 19;
		if (selected == 46)
			elev = 20;
		if (selected == 47)
			elev = 6;
		if (selected == 48)
			elev = 12;
		if (selected == 49)
			elev = 7;
		if (selected == 69)
			elev = 23;

		if (selected == 1)
			elev = 17;
		if (selected == 2)
			elev = 18;
		if (selected == 3)
			elev = 19;
		if (selected == 4)
			elev = 20;
		if (selected == 5)
			elev = 6;
		if (selected == 6)
			elev = 12;
		if (selected == 7)
			elev = 2;
		if (selected == 8)
			elev = 7;
		if (selected == 70)
			elev = 23;

		// if(selected==50) elev= "Owner signature";
		if (selected == 51)
			elev = 8;
		if (selected == 52)
			elev = 9;
		if (selected == 53)
			elev = 10;
		if (selected == 54)
			elev = 11;
		if (selected == 55)
			elev = 12;
		if (selected == 56)
			elev = 7;
		if (selected == 71)
			elev = 23;

		if (selected == 57)
			elev = 13;
		if (selected == 58)
			elev = 14;
		if (selected == 59)
			elev = 15;
		if (selected == 60)
			elev = 16;
		if (selected == 61)
			elev = 7;
		if (selected == 72)
			elev = 23;

		return elev;
	}

	class ProgressThread extends Thread {

		// Class constants defining state of the thread
		final static int DONE = 0;

		Handler mHandler;

		ProgressThread(Handler h) {
			mHandler = h;
		}

		@Override
		public void run() {
			mState = RUNNING;

			while (mState == RUNNING) {
				try {

					// Control speed of update (but precision of delay not
					// guaranteed)
					if (sub_total == 100 || sub_total == 0) {

						Thread.sleep(delay + delay);

					} else {

						Thread.sleep(delay);

					}

				} catch (InterruptedException e) {
					// Log.e("ERROR", "Thread was Interrupted");
				}

				Message msg = mHandler.obtainMessage();
				Bundle b = new Bundle();
				b.putDouble("total", Total);
				b.putDouble("sub_total", sub_total);
				msg.setData(b);
				mHandler.sendMessage(msg);

			}
		}

		public void setState(int state) {
			mState = state;
		}

	}

	/** Upload policy holder information Starts **/
	final Handler handler1 = new Handler() {

		public void handleMessage(Message msg) {

			double total = msg.getData().getDouble("total");
			double sub_total = msg.getData().getDouble("sub_total");
			lp.width = (int) (((double) back_li_head.getWidth() / 100.00) * total);
			li_head.setLayoutParams(lp);
			tv_head.setText(Html
					.fromHtml("Please be patient as we export your file.<br> When complete you must log into www.paperlessinspectors.com and submit this file for auditing. <br> Please don't lock your screen it may affect your export.<br> "
							+ Exportbartext));

			tv_head_per.setText((int) total + "%");
			tv_child_per.setText((int) sub_total + "%");
			tv_child.setText("Exporting  " + sub_insp);
			ch_lp.width = (int) (((double) back_li_child.getWidth() / 100.00) * sub_total);
			li_child.setLayoutParams(ch_lp);
			if (total == 100) {
				dialog.setCancelable(true);
				dialog.dismiss();
				progThread.setState(ProgressThread.DONE);

			}

		}
	};

	public void setincreament() {
		Message msg = handler1.obtainMessage();
		Bundle b = new Bundle();
		b.putDouble("total", Total);
		b.putDouble("sub_total", sub_total);
		msg.setData(b);
		handler1.sendMessage(msg);
	}

	private String convefromabreviation(String val) {
		// TODO Auto-generated method stub
		System.out.println("converbfromabbrevia"+val);
		if (val.equals("N/A")) {
			val = cf.getResourcesvalue(R.string.na);
		} else if (val.equals("N/D")) {
			val = cf.getResourcesvalue(R.string.nd);
		} else if (val.equals("N/AC")) {
			val = "Not Accessible";// cf.getResourcesvalue(R.string.nd);
		} else if (val.equals("BSI")) {
			val = cf.getResourcesvalue(R.string.BSI);
		} else if (val.contains("Yes-BVCN")) { // We use contains because comments also added with this val so only we use this 
			
			val = val.replace("Yes-BVCN", cf.getResourcesvalue(R.string.YesBVCN));
		}
		System.out.println("vali="+val);
		return val;
	}

	public String[] Device_Information() {
		String s[] = new String[5];
		s[0] = Settings.System.getString(this.getContentResolver(),
				Settings.System.ANDROID_ID);
		s[1] = android.os.Build.MODEL;
		s[2] = android.os.Build.MANUFACTURER;
		s[3] = android.os.Build.VERSION.RELEASE;
		s[4] = android.os.Build.VERSION.SDK;
		return s;
		/*
		 * WifiManager wifiManager =
		 * (WifiManager)(this.con.getSystemService(this.con.WIFI_SERVICE));
		 * WifiInfo wifiInfo = wifiManager.getConnectionInfo(); ipAddress =
		 * wifiInfo.getIpAddress();
		 */
	}

	private void findinspectionid(int insptypeid) throws SocketException,
			NetworkErrorException, TimeoutException, IOException,
			XmlPullParserException, Exception, NullPointerException {
		// TODO Auto-generated method stub
		if (String.valueOf(insptypeid).equals("31")) {
			inspectionid = 28;
		} else if (String.valueOf(insptypeid).equals("32")) {
			inspectionid = 9;
		} else if (String.valueOf(insptypeid).equals("33")) {
			inspectionid = 62;
		} else if (String.valueOf(insptypeid).equals("34")) {
			inspectionid = 90;
		} else if (String.valueOf(insptypeid).equals("35")) {
			inspectionid = 11;
		}
	}

	private String getinspection_online_no(String insp) {
		String insp_online = "0";
		if (insp.equals("1"))
			insp_online = "13";
		else if (insp.equals("2"))
			insp_online = "750";
		else if (insp.equals("3"))
			insp_online = "28";
		else if (insp.equals("4"))
			insp_online = "9";
		else if (insp.equals("5"))
			//insp_online = "9";
			insp_online="62";
		else if (insp.equals("6"))
			//insp_online = "9";
			insp_online="90";
		else if (insp.equals("7"))
			insp_online = "11";
		else if (insp.equals("8"))
			insp_online = "18";
		else if (insp.equals("9"))
			insp_online = "751";
		return insp_online;
	}

	private void call_erro_service(String string) {
		// TODO Auto-generated method stub
		// System.out.println("comes correctly in the error log");
		try {
			String[] device_info = Device_Information();

			String[] getfromcursor = { "CommunalAreasElevators",
					"FourpointWaterHeater", "FourpointHVACUNITDetails_Updated",
					"InternalAppliance", "ELEVATIONPHOTOSECTION",
					"FEEDBACKDOCUMENTS" };
			boolean cur = false;
			for (int i = 0; i < getfromcursor.length; i++) {
				if (getfromcursor[i].equals(Web_mehtod)) {
					cur = true;
				}
			}
			/** Check for the web service method which have the image bitmap **/
			if (cur) {
				if (current_val_cur != null) {
					if (current_val_cur.getCount() < current_pos)
						;
					{
						curr_Request = "";
						current_val_cur.moveToPosition(current_pos);
						for (int i = 0; i < current_val_cur.getColumnCount(); i++) {
							curr_Request += current_val_cur.getColumnName(i)
									+ "="
									+ cf.decode(current_val_cur.getString(i))
									+ ";";
						}
					}
				}
			}

			// System.out.println("the request"+curr_Request);
			/*
			 * <InspectorId>int</InspectorId> <Srid>string</Srid>
			 * <Inspectiontype>string</Inspectiontype>
			 * <ErrorMsg>string</ErrorMsg> <WebMethod>string</WebMethod>
			 * <APILevel>string</APILevel> <Manufacturer>string</Manufacturer>
			 * <Devicemodel>string</Devicemodel>
			 */
			cf.request = cf.SoapRequest("ANDROIDERRORMAIL");
			cf.request.addProperty("InspectorId", cf.Insp_id);
			cf.request.addProperty("Srid", cf.selectedhomeid);
			cf.request.addProperty("Inspectiontype", Exportbartext);
			cf.request.addProperty("ErrorMsg", string);
			cf.request.addProperty("Exportingdata", curr_Request);
			cf.request.addProperty("WebMethod", Web_mehtod);
			cf.request.addProperty("APILevel", device_info[3]);
			// cf.request.addProperty("Sdk",device_info[4]);
			cf.request.addProperty("Manufacturer", device_info[2]);
			cf.request.addProperty("Devicemodel", device_info[1]);
			System.out.println("the request " + cf.request);
			String result = cf.SoapResponse("ANDROIDERRORMAIL", cf.request)
					+ "";
			System.out.println("result " + result);

		} catch (NetworkErrorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private Double getmoduleperinc(String[] general_boo2) {
		// TODO Auto-generated method stub
		int j = 0;
		for (int i = 0; i < general_boo2.length; i++) {

			if (general_boo2[i].equals("true")) {
				j++;
			}

		}
		if (j != 0) {
			// System.out.println("the perinc "+PerIns_inc+" Per module"+PerIns_inc/j);
			return PerIns_inc / j;
		}
		return 0.0;
	}

	/*** Support function for exporting ends **/
	public void clicker(View v) {
		switch (v.getId()) {
		case R.id.search:
			String temp = cf.encode(search_text.getText().toString());
			if (temp.equals("")) {
				cf.ShowToast(
						"Please enter the Name or Policy Number to search.", 1);
				search_text.requestFocus();
			} else {
				res = temp;
				dbquery();
			}
			break;
		case R.id.clear:
			search_text.setText("");
			res = "";
			dbquery();
			break;

		case R.id.home:
			cf.gohome();
			break;
		case R.id.completed_insp:
			Intent inte = new Intent(getApplicationContext(), Export.class);

			inte.putExtra("type", "Reexport");
			startActivity(inte);
			break;
		case R.id.export_insp:
			Intent inte1 = new Intent(getApplicationContext(), Export.class);
			inte1.putExtra("type", "export");
			startActivity(inte1);
			break;

		}
	}
};