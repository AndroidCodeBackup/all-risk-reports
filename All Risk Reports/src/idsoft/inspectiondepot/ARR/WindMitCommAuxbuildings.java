/*
************************************************************
* Project: ALL RISK REPORT
* Module : WindMitCommAuxbuildings.java
* Creation History:
Created By : Nalini on 10/24/2012
* Modification History:
Last Modified By : Gowri on 04/04/2013
************************************************************ 
*/
package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.Electrical.Spin_Selectedlistener;
import idsoft.inspectiondepot.ARR.Electrical.checklistenetr;
import idsoft.inspectiondepot.ARR.SummaryHazards.textwatcher;
import idsoft.inspectiondepot.ARR.WindMitWallCons.WC_EditClick;
import idsoft.inspectiondepot.ARR.WindMitWallCons.multi;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.opengl.Visibility;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class WindMitCommAuxbuildings extends Activity{
	CommonFunctions cf;
	Spinner auxspin,rcspin,rcspintype,constypespin;
	ArrayAdapter auxbuildadap,spnconstypeadap,rcbuildadap;
	String  aux_na="",auxnotappl="0",aux_value="",insupolicyval="",aux_Insu_Size_val="",aux_Cons__RC_3Years_val="",aux_WS_RC_OGN_DD="",insunotxt="",roofcoveringtypetxt="",sizetxt="",
			wallstructuretxt="",roofcovertxt="",overgencondtxt="",defecttxt="",damagetxt="",threeyearstxt="",auxbuildtxt="",sizetxtdbval,consttypedbval;
	RadioGroup inspolicyrdgval,sizerdgval,walllstrurdgval,roofcoverdgval,overallgencondrgpval,defectrgpval,decayrgpval,damagergpval,threeyearsrgpval;
	LinearLayout subpnlshw;
	TableLayout dynviewaux;
	int identity=0,rwsauxbuil;
	boolean insuchk=false,sizechk=false,wallstruchk=false,roofcoverchk=false,ognchk=false,defectchk=false,damagechk=false,threeyearschk=false,load_comment=true;
	public Spinner spnwcstories[] = new Spinner[11];
	public int[] spnwcstoriesid = {R.id.spin_numb1,R.id.spin_numb2,R.id.spin_numb3,R.id.spin_numb4,R.id.spin_numb5,R.id.spin_numb6,R.id.spin_numb7,R.id.spin_numb8,R.id.spin_numb9,R.id.spin_numb10,R.id.spin_numb11};
	String spnauxbuildings[] ={"--Select--", "Garages","Parking Garages","Maintenance Sheds","Workshops","Tool Sheds","Pool Houses","Boiler Sheds","Pump Houses","AIR Conditioner Sheds","Storage Sheds","Guard Houses","Club Houses","Property Line Walls","Grand Stands","Bleachers","Hot Houses","Glass Houses","Green Houses","Light Poles","Sheds","Carports","Cabanas","Pool Cages","Solar Paneling","Swimming Pool Buildings","Spas","Hot Tubs","Whirlpools","Other"};
	String spnauxroofcover[] ={"--Select--", "Average","Poor","Good"};
	
	String spnauxroofcovertype[]={"--Select--", "Asphalt/Fiberglass Shingle","Concrete/Clay Tile","Metal","Built Up Tar and Gravel","Modified Bitumen","Wood Shake/Shingle","Membrane","Other"};
	String spnauxconstype[]={"--Select--", "Masonry","Frame","Concrete","Combination","Metal","Pre-Manufactured"};
	String spnstories[] = { "--Select--", "1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65","66","67","68","69","70","71","72","73","74","75","76","77","78","79","80","81","82","83","84","85","86","87","88","89","90","91","92","93","94","95","96","97","98","99","100"};
	String spnpercentage[] = { "--Select--", "10","20","30","40","50","60","70","80","90","100",};
	
	public Spinner spnwcpercentage[] = new Spinner[11];
	public int[] spnwcpercentageid = {R.id.spin_per1,R.id.spin_per2,R.id.spin_per3,R.id.spin_per4,R.id.spin_per5,R.id.spin_per6,R.id.spin_per7,R.id.spin_per8,R.id.spin_per9,R.id.spin_per10,R.id.spin_per11};
	String[] Size_Split,aux_build_txt,aux_Ins_Size_txt,aux_Cons__RC_3Years_txt,aux_WS_RC_OGN_DD_txt,aux_build_other;
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cf=new CommonFunctions(this);	 
        Bundle extras = getIntent().getExtras();
  		if (extras != null) {
  			cf.getExtras(extras);				
  	 	}
  		  setContentView(R.layout.windmitcommauxbuild);
  		  cf.setupUI((ScrollView) findViewById(R.id.scr));
          cf.getDeviceDimensions();
          
          LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
          if(cf.identityval==33)
          {
        	  hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","Commercial Type II","Auxiliary Buildings",3,1,cf));
          }
          else if(cf.identityval==34)
          {
        	  hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","Commercial Type III","Auxiliary Buildings",3,1,cf));
          }
          
          cf.mainmenu_layout = (LinearLayout) findViewById(R.id.header); //** HEADER MENU **//
          cf.mainmenu_layout.setMinimumWidth(cf.wd);
          cf.mainmenu_layout.addView(new MyOnclickListener(this, 3, 0,0, cf));
         
          cf.type_submenulayout =(LinearLayout) findViewById(R.id.typesubmenu); //** WINDMIT TYPE SUBMENU **//
          cf.type_submenulayout.addView(new MyOnclickListener(this, 300, 1,0, cf));
          cf.windmittypechk(cf.identityval,((RadioButton)findViewById(R.id.b1802_chk)),((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((TextView)findViewById(R.id.commtxt)),((ImageView)findViewById(R.id.editbno))); // To check the checkbox of choosen inspection type
          cf.uncheckcommercial(((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((RadioButton)findViewById(R.id.b1802_chk))); 
      	      
          cf.submenu_layout = (LinearLayout) findViewById(R.id.submenu);//** WINDMIT TYPE SUBMENU **//	 
          cf.submenu_layout .addView(new MyOnclickListener(this,cf.identityval,1,311, cf));
          
          cf.innersubmenu_layout = (LinearLayout) findViewById(R.id.innersubmenu);//** WINDMIT TYPE SUBMENU **//
          cf.innersubmenu_layout.addView(new MyOnclickListener(this,cf.identityval,1,145, cf));
          
          cf.mainlayout = (LinearLayout)findViewById(R.id.bc_table);
          cf.save = (Button)findViewById(R.id.save);
          cf.tblrw = (TableRow)findViewById(R.id.row2);
          cf.tblrw.setMinimumHeight(cf.ht);	
          Declarations();
          cf.CreateARRTable(35);cf.CreateARRTable(36);cf.CreateARRTable(355); 
          Set_AuxBuild_Comm();
          Comm_Aux_Buil_SetValue();          
    }
	private void Set_AuxBuild_Comm() {
		// TODO Auto-generated method stub
		try
		{
		Cursor c1 = cf.arr_db.rawQuery("SELECT * FROM "+ cf.Comm_Aux_Master + " WHERE fld_srid='"+ cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'", null);				
		if(c1.getCount()>0)
		{
			c1.moveToFirst();
			
			if(c1.getString(c1.getColumnIndex("fld_aux_NA")).equals("0") || c1.getString(c1.getColumnIndex("fld_aux_NA")).equals(""))
			{
				((TextView)findViewById(R.id.savetxtaux)).setVisibility(cf.v1.VISIBLE);
				((LinearLayout)findViewById(R.id.auxlin)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.shwaux)).setEnabled(true);
				((ImageView)findViewById(R.id.shwaux)).setVisibility(cf.v1.GONE);
				((ImageView)findViewById(R.id.hdwaux)).setVisibility(cf.v1.VISIBLE);	
				((CheckBox)findViewById(R.id.auxnotapplicable)).setChecked(false);
			}
			else
			{
				((CheckBox)findViewById(R.id.auxnotapplicable)).setChecked(true);
				((LinearLayout)findViewById(R.id.auxlin)).setVisibility(cf.v1.GONE);
				((TextView)findViewById(R.id.savetxtaux)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.shwaux)).setEnabled(false);
				((ImageView)findViewById(R.id.shwaux)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdwaux)).setVisibility(cf.v1.GONE);
			}
		}
		else
		{
			((LinearLayout)findViewById(R.id.auxlin)).setVisibility(cf.v1.GONE);
			Cursor c = cf.arr_db.rawQuery("SELECT * FROM "	+ cf.Comm_AuxBuilding + " WHERE fld_srid='"
					+ cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'", null);
			if(c.getCount()==0)
			{
				((CheckBox)findViewById(R.id.auxnotapplicable)).setChecked(true);
			}
		}
		}
		catch(Exception e)
		{
			System.out.println("EEE"+e.getMessage());
		}
		Cursor c3 = cf.arr_db.rawQuery("SELECT * FROM " + cf.Wind_Commercial_Comm + " WHERE fld_srid='" + cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'", null);				
		if(c3.getCount()>0)
		{
			c3.moveToFirst();
			if(!cf.decode(c3.getString(c3.getColumnIndex("fld_auxillarycomments"))).equals("")){
			((EditText)findViewById(R.id.auxbuilcomment)).setText(cf.decode(c3.getString(c3.getColumnIndex("fld_auxillarycomments"))));
			}
		}
	}
	private void Comm_Aux_Buil_SetValue() {
		// TODO Auto-generated method stub
		try
		{
			Cursor c1 = cf.arr_db.rawQuery("SELECT * FROM "	+ cf.Comm_AuxBuilding + " WHERE fld_srid='"
					+ cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'", null);
			rwsauxbuil = c1.getCount();
			aux_build_txt = new String[rwsauxbuil];
			aux_Ins_Size_txt = new String[rwsauxbuil];
			aux_Cons__RC_3Years_txt = new String[rwsauxbuil];
			aux_WS_RC_OGN_DD_txt = new String[rwsauxbuil];
			aux_build_other= new String[rwsauxbuil];
			 if(rwsauxbuil>0)
			 {
				 ((TableLayout)findViewById(R.id.auxlin)).setVisibility(cf.v1.VISIBLE);
					((TextView)findViewById(R.id.savetxtaux)).setVisibility(cf.v1.VISIBLE);
				 c1.moveToFirst();		
				 int i=0;
					 do {
						 aux_build_txt[i]= cf.decode(c1.getString(c1.getColumnIndex("fld_aux_Building_type")));
						 aux_Ins_Size_txt[i]= cf.decode(c1.getString(c1.getColumnIndex("fld_aux_Ins_Size")));
						 aux_Cons__RC_3Years_txt[i]= cf.decode(c1.getString(c1.getColumnIndex("fld_aux_Cons__RC_3Years")));	
						 aux_WS_RC_OGN_DD_txt[i]= cf.decode(c1.getString(c1.getColumnIndex("fld_aux_WS_RC_OGN_DD")));
						 aux_build_other[i]= cf.decode(c1.getString(c1.getColumnIndex("fld_aux_Building_other")));
					i++;
						} while (c1.moveToNext());			
					 subpnlshw.setVisibility(cf.v1.VISIBLE);
					 ((TableLayout)findViewById(R.id.tableLayoutNote)).setVisibility(cf.v1.VISIBLE);
					 ((TextView)findViewById(R.id.txtbriefexplanation)).setText(Html.fromHtml("RCT "+cf.redcolor+"  - Roof Covering Type " + "<br/>"+" "
							 			+ "WS/WC "+cf.redcolor+"  - Wall Structure/Wall Cladding" + "<br/>"
							 			+ "GRCC "+cf.redcolor+"  - General Roof Covering Condition " + "<br/>"
										+ "OGC "+cf.redcolor+"  - Overall General Condition" + "<br/>"
										+ "WD/DN "+cf.redcolor+"  - Wall Damage/Decay Noted " + "<br/>"
										+ "DC/LN "+cf.redcolor+"  - Damage Covering/Leakage Noted " + "<br/>"																				
										+ "3YRP "+cf.redcolor+" - 3 Years Replacement Probability " + "<br/>"));
					 
					 
						show_wcelev_Value();
			 }
			 else
			 {
				 subpnlshw.setVisibility(cf.v1.GONE);
				 ((TableLayout)findViewById(R.id.tableLayoutNote)).setVisibility(cf.v1.GONE);	 
			 }
		 }
		 catch(Exception e)
		 {
			 System.out.println("wc erro "+e.getMessage());
		 }
	}
	private void show_wcelev_Value() {
		// TODO Auto-generated method stub
		dynviewaux.removeAllViews();
		LinearLayout h1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvac_listview_header, null);
		LinearLayout th= (LinearLayout)h1.findViewById(R.id.auxbuild);th.setVisibility(cf.v1.VISIBLE);
		TableLayout.LayoutParams lp = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
	 	lp.setMargins(2, 0, 2, 2); 
	 	h1.removeAllViews(); 	 	
	 	dynviewaux.addView(th,lp);	
	 	dynviewaux.setVisibility(View.VISIBLE);	
	 	for (int i = 0; i < rwsauxbuil; i++) 
		{	
			TextView No,txtauxbuild,txtoninsupolicy,txtsize,txtconstype,txtRCT,txtWallstruc,txtroofcovering,txtOGN,txtdefect,txtdamage,txt3yrp;
			ImageView edit,delete;
			
			LinearLayout t1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvac_listview, null);	
			LinearLayout t = (LinearLayout)t1.findViewById(R.id.auxbuildcontent);t.setVisibility(cf.v1.VISIBLE);
			t.setId(44444+i);/// Set some id for further use
			
			
		 	No= (TextView) t.findViewWithTag("AUx_No");			
		 	txtauxbuild= (TextView) t.findViewWithTag("Aux_building");
		 	txtoninsupolicy= (TextView) t.findViewWithTag("Aux_insupolicy");	
		 	txtsize= (TextView) t.findViewWithTag("Aux_BuildSize");	
		 	txtconstype= (TextView) t.findViewWithTag("Aux_Constype");
		 	txtRCT= (TextView) t.findViewWithTag("Aux_RCtype");	
		 	txtWallstruc= (TextView) t.findViewWithTag("AUX_WS/WC");
		 	txtroofcovering= (TextView) t.findViewWithTag("Aux_RC");
		 	txtOGN= (TextView) t.findViewWithTag("Aux_OGN");	
		 	txtdefect= (TextView) t.findViewWithTag("Aux_Defect");
			txtdamage= (TextView) t.findViewWithTag("Aux_Damage");
			txt3yrp= (TextView) t.findViewWithTag("Aux_3YRP");
		 	
			String Ins_Size_Spilt[] = aux_Ins_Size_txt[i].split("&#94;"); 		 	
		 	String Insu_Split[] = Ins_Size_Spilt[0].split("&#39;");
		 	String Size_Split[] = Ins_Size_Spilt[1].split("&#39;");
		 	
			String Cons_RC_3Yrs[] = aux_Cons__RC_3Years_txt[i].split("&#94;"); 			
		 	String RCT_Split[] = Cons_RC_3Yrs[1].split("&#39;");
		 	
		 	String WS_RC_OGN_DD[] = aux_WS_RC_OGN_DD_txt[i].split("&#94;"); 
		 	String  WC_Split[] = WS_RC_OGN_DD[0].split("&#39;");
		 	String  RC_Split[] = WS_RC_OGN_DD[1].split("&#39;");
		 	String OGN_Split[] = WS_RC_OGN_DD[2].split("&#39;");
		 	String Def_Split[] = WS_RC_OGN_DD[3].split("&#39;");
		 	String Dam_Split[] = WS_RC_OGN_DD[4].split("&#39;");
		 	
			No.setText(String.valueOf(i+1));
			if(aux_build_txt[i].equals("Other"))
			{
				txtauxbuild.setText(aux_build_txt[i]+"("+aux_build_other[i]+")");
			}
			else
			{
				txtauxbuild.setText(aux_build_txt[i]);
			}
		 	
		 	
		 	if("Yes".equals(Insu_Split[0]))
		 	{  insupolicyval = Insu_Split[0] + " (No :"+Insu_Split[1]+")";}
		 	else{
		 		insupolicyval = Insu_Split[0];
		 	}
		 	
		 	txtoninsupolicy.setText(insupolicyval);		 
		 	txtsize.setText(Size_Split[1]+ " Sq.Ft "+Size_Split[0]);		 	
		 	txtconstype.setText(Cons_RC_3Yrs[0]);	
		 	txtRCT.setText(RCT_Split[0]);		
		 	txtWallstruc.setText(WC_Split[0]);
		 	
		 	txtroofcovering.setText(RC_Split[0]);
		 	txtOGN.setText(OGN_Split[0]);	
		 	txtdefect.setText(Def_Split[0]);	
		 	txtdamage.setText(Dam_Split[0]);			
		 	txt3yrp.setText(Cons_RC_3Yrs[2]);		

			edit= (ImageView) t.findViewWithTag("Aux_edit");		
		 	edit.setTag(aux_build_txt[i]);
		 	edit.setOnClickListener(new Aux_EditClick(1));	
		 	delete= (ImageView) t.findViewWithTag("Aux_delete");	
		 	delete.setTag(aux_build_txt[i]);	
		 	delete.setOnClickListener(new Aux_EditClick(2));
		 	t1.removeAllViews();
			dynviewaux.addView(t,lp);
		}
	 	
	}
	class Aux_EditClick implements OnClickListener
	{
		String type="";
		int edit; 
		Aux_EditClick(int edit)
		{
			this.edit=edit;			
		}
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			type=v.getTag().toString();
			if(edit==1)
			{
				identity=1;
				((Button)findViewById(R.id.auxsave)).setText("Update");
				updateAux(type);
			}
			else
			{
				AlertDialog.Builder builder = new AlertDialog.Builder(WindMitCommAuxbuildings.this);
						builder.setMessage("Do you want to delete the selected Auxiliary Buildings?")
								.setCancelable(false)
								.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog,int id) {												
												try
												{
													cf.arr_db.execSQL("DELETE FROM "+cf.Comm_AuxBuilding+" WHERE fld_srid='"+cf.selectedhomeid+"' and fld_aux_Building_type='"+type+"' and insp_typeid='"+cf.identityval+"'");
													cf.ShowToast("Deleted Successfully.", 0);
													Comm_Aux_Buil_SetValue();
													clear();
												}
												catch (Exception e) {
													// TODO: handle exception
												}
											}
										})
								.setNegativeButton("No",
										new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog,int id) {
												dialog.cancel();
											}
										});
						builder.setTitle("Confirmation");
						builder.setIcon(R.drawable.alertmsg);
						builder.show();					
						
			}
		}
	}
	private void updateAux(String auxvalue) {
		// TODO Auto-generated method stub		
		try
		{
		Cursor c2=cf.SelectTablefunction(cf.Comm_AuxBuilding, " WHERE fld_srid='"+cf.selectedhomeid+"' and fld_aux_Building_type='"+auxvalue+"' and insp_typeid='"+cf.identityval+"'");
		if(c2.getCount()>0)
		{
			c2.moveToFirst();
			aux_value = c2.getString(c2.getColumnIndex("fld_aux_Building_type"));
			int spinnerPosition1 = auxbuildadap.getPosition(aux_value);
			auxspin.setSelection(spinnerPosition1);
			auxspin.setEnabled(false);			
			
			if(aux_value.equals("Other"))
			{
				((EditText)findViewById(R.id.edauxbuild_otr)).setVisibility(cf.v1.VISIBLE);		
				((EditText)findViewById(R.id.edauxbuild_otr)).setText(cf.decode(c2.getString(c2.getColumnIndex("fld_aux_Building_other"))));				
			}			
			
			
			String aux_Ins_Size[] = cf.decode(c2.getString(c2.getColumnIndex("fld_aux_Ins_Size"))).split("&#94;");	
			String aux_Ins_Split[] = aux_Ins_Size[0].split("&#39;");
			String aux_Size_Split[] = aux_Ins_Size[1].split("&#39;");
			
			((RadioButton) inspolicyrdgval.findViewWithTag(aux_Ins_Split[0])).setChecked(true);
			if(aux_Ins_Split[0].equals("Yes"))
			{	
				((LinearLayout)findViewById(R.id.insupolicynolin)).setVisibility(cf.v1.VISIBLE);
				((EditText)findViewById(R.id.edinsupolicy)).setText(aux_Ins_Split[1]);
			}
			else
			{
				((LinearLayout)findViewById(R.id.insupolicynolin)).setVisibility(cf.v1.GONE);
				((EditText)findViewById(R.id.edinsupolicy)).setText("");			
			}
			
			
			
			((RadioButton) sizerdgval.findViewWithTag(aux_Size_Split[0])).setChecked(true);
			
			
			
			
			((EditText)findViewById(R.id.edsize)).setText(aux_Size_Split[1]);
			
			
			
			String aux_Cons_RC_3yrs[] = cf.decode(c2.getString(c2.getColumnIndex("fld_aux_Cons__RC_3Years"))).split("&#94;");
			String RCT_Split[] = aux_Cons_RC_3yrs[1].split("&#39;");
			
			int spinnerPosition2 = spnconstypeadap.getPosition(aux_Cons_RC_3yrs[0]);
			constypespin.setSelection(spinnerPosition2);
			
			int spinnerPosition3 = rcbuildadap.getPosition(RCT_Split[0]);
			rcspintype.setSelection(spinnerPosition3);
			
			if(RCT_Split[0].equals("Other"))
			{
				((EditText)findViewById(R.id.edroofcovertype_othr)).setVisibility(cf.v1.VISIBLE);
				((EditText)findViewById(R.id.edroofcovertype_othr)).setText(cf.decode(RCT_Split[1]));
			}				
			
			String WS_RC_OGN_DD[] = cf.decode(c2.getString(c2.getColumnIndex("fld_aux_WS_RC_OGN_DD"))).split("&#94;");			
			
		 	String WC_Split[] = WS_RC_OGN_DD[0].split("&#39;");
		 	String RC_Split[] = WS_RC_OGN_DD[1].split("&#39;");
		 	String OGN_Split[] = WS_RC_OGN_DD[2].split("&#39;");
		 	String Def_Split[] = WS_RC_OGN_DD[3].split("&#39;");
		 	String Dam_Split[] = WS_RC_OGN_DD[4].split("&#39;");
		 	
		 	((RadioButton) walllstrurdgval.findViewWithTag(WC_Split[0])).setChecked(true);
		 	((RadioButton) roofcoverdgval.findViewWithTag(RC_Split[0])).setChecked(true);
		 	((RadioButton) overallgencondrgpval.findViewWithTag(OGN_Split[0])).setChecked(true);
		 	((RadioButton) defectrgpval.findViewWithTag(Def_Split[0])).setChecked(true);
		 	((RadioButton) damagergpval.findViewWithTag(Dam_Split[0])).setChecked(true);
		 		
		 	((RadioButton) threeyearsrgpval.findViewWithTag(aux_Cons_RC_3yrs[2])).setChecked(true);
		 	
		 	if(RC_Split[0].equals("Poor"))
		 	{
		 		((LinearLayout)findViewById(R.id.rclin)).setVisibility(cf.v1.VISIBLE);
		 		((EditText)findViewById(R.id.edroofcover_othr)).setVisibility(cf.v1.VISIBLE);
		 		((EditText)findViewById(R.id.edroofcover_othr)).setText(cf.decode(RC_Split[1]));
			}
		 	else {
		 		((LinearLayout)findViewById(R.id.rclin)).setVisibility(cf.v1.GONE);
		 	}	
		 	
		 	if(OGN_Split[0].equals("Poor"))
		 	{
		 		((LinearLayout)findViewById(R.id.ognlin)).setVisibility(cf.v1.VISIBLE);
		 		((EditText)findViewById(R.id.edognotr)).setVisibility(cf.v1.VISIBLE);
		 		((EditText)findViewById(R.id.edognotr)).setText(cf.decode(OGN_Split[1]));
			}
		 	else {
		 		((LinearLayout)findViewById(R.id.ognlin)).setVisibility(cf.v1.GONE);
		 	}		
		 	
		 	if(Def_Split[0].equals("Yes"))
		 	{
		 		((LinearLayout)findViewById(R.id.defectlin)).setVisibility(cf.v1.VISIBLE);
		 		((EditText)findViewById(R.id.eddecay_otr)).setVisibility(cf.v1.VISIBLE);
		 		((EditText)findViewById(R.id.eddecay_otr)).setText(cf.decode(Def_Split[1]));
			}
		 	else {
		 		((LinearLayout)findViewById(R.id.defectlin)).setVisibility(cf.v1.GONE);
		 	}		
		 	if(Dam_Split[0].equals("Yes"))
		 	{
		 		((LinearLayout)findViewById(R.id.damagelin)).setVisibility(cf.v1.VISIBLE);
		 		((EditText)findViewById(R.id.eddamage_otr)).setVisibility(cf.v1.VISIBLE);
		 		((EditText)findViewById(R.id.eddamage_otr)).setText(cf.decode(Dam_Split[1]));
			}
		 	else {
		 		((LinearLayout)findViewById(R.id.damagelin)).setVisibility(cf.v1.GONE);
		 	}		
		 			
		}c2.close();
		}
		catch(Exception e)
		{
			System.out.println("Caa"+e.getMessage());
		}
	}
	private void Declarations() {
		// TODO Auto-generated method stub
		
		 auxspin=(Spinner) findViewById(R.id.spnaux);
		 auxbuildadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, spnauxbuildings);
		 auxbuildadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 auxspin.setAdapter(auxbuildadap);		 
		 auxspin.setOnItemSelectedListener(new  Spin_Selectedlistener(1));
		 
		 constypespin=(Spinner) findViewById(R.id.spnconstype);
		 spnconstypeadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, spnauxconstype);
		 spnconstypeadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 constypespin.setAdapter(spnconstypeadap);		 
		 
		 rcspintype=(Spinner) findViewById(R.id.spnroofcoveringtype);
		 rcbuildadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, spnauxroofcovertype);
		 rcbuildadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 rcspintype.setAdapter(rcbuildadap);
		 rcspintype.setOnItemSelectedListener(new  Spin_Selectedlistener(2));
		 
		 dynviewaux = (TableLayout)findViewById(R.id.dynamicaux);
		 subpnlshw = (LinearLayout)findViewById(R.id.subpnlshow); 
		 
		 inspolicyrdgval = (RadioGroup)findViewById(R.id.insurancepol_rdg);
		 inspolicyrdgval.setOnCheckedChangeListener(new checklistenetr(1));
		 
		 sizerdgval = (RadioGroup)findViewById(R.id.size_rdg);
		 sizerdgval.setOnCheckedChangeListener(new checklistenetr(2));
		 
		 walllstrurdgval = (RadioGroup)findViewById(R.id.wallstruc_rdgp);
		 walllstrurdgval.setOnCheckedChangeListener(new checklistenetr(3));
		 
		 defectrgpval = (RadioGroup)findViewById(R.id.defect_rdgp);
		 defectrgpval.setOnCheckedChangeListener(new checklistenetr(4));

		 roofcoverdgval = (RadioGroup)findViewById(R.id.roofcover_rdgp);
		 roofcoverdgval.setOnCheckedChangeListener(new checklistenetr(5));
		 
		 threeyearsrgpval = (RadioGroup)findViewById(R.id.threeyears_rdgp); 
		 threeyearsrgpval.setOnCheckedChangeListener(new checklistenetr(6));
		 
		 damagergpval = (RadioGroup)findViewById(R.id.damage_rdgp);
		 damagergpval.setOnCheckedChangeListener(new checklistenetr(7));
		 
		 overallgencondrgpval = (RadioGroup)findViewById(R.id.overallgencon_rdgp); 
		 overallgencondrgpval.setOnCheckedChangeListener(new checklistenetr(8));		 
		 
    	 ((EditText)findViewById(R.id.edroofcover_othr)).setOnTouchListener(new Touch_Listener(2));
    	 ((EditText)findViewById(R.id.edroofcover_othr)).addTextChangedListener(new textwatcher(2));
    	 
    	 ((EditText)findViewById(R.id.edognotr)).setOnTouchListener(new Touch_Listener(3));
    	 ((EditText)findViewById(R.id.edognotr)).addTextChangedListener(new textwatcher(3));
    	 
    	 ((EditText)findViewById(R.id.eddecay_otr)).setOnTouchListener(new Touch_Listener(4));
    	 ((EditText)findViewById(R.id.eddecay_otr)).addTextChangedListener(new textwatcher(4));
    	 
    	 ((EditText)findViewById(R.id.eddamage_otr)).setOnTouchListener(new Touch_Listener(5));
    	 ((EditText)findViewById(R.id.eddamage_otr)).addTextChangedListener(new textwatcher(5));
    	 
    	 ((EditText)findViewById(R.id.auxbuilcomment)).setOnTouchListener(new Touch_Listener(6));
    	 ((EditText)findViewById(R.id.auxbuilcomment)).addTextChangedListener(new textwatcher(6));
    	 
    	 ((EditText)findViewById(R.id.edinsupolicy)).addTextChangedListener(new textwatcher(7));
    	 ((EditText)findViewById(R.id.edsize)).addTextChangedListener(new textwatcher(8));
    	 
    	
	}
	 class Touch_Listener implements OnTouchListener

	    {
	    	   public int type;
	    	   Touch_Listener(int type)
	    		{
	    			this.type=type;
	    			
	    		}
	    	    @Override
	    		public boolean onTouch(View v, MotionEvent event) {

	    			// TODO Auto-generated method stub
	    	    	((EditText) findViewById(v.getId())).setFocusableInTouchMode(true);
			    	((EditText) findViewById(v.getId())).requestFocus();
	    			return false;
	    	    }
	    }
	    class textwatcher implements TextWatcher


	    {
	         public int type;
	        textwatcher(int type)
	    	{
	    		this.type=type;
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {


	    		// TODO Auto-generated method stub
	    		if(this.type==1)
	    		{
	    		//	cf.showing_limit(s.toString(),(TextView)findViewById(R.id.wallstruc_tv_type1),150); 
	    		}
	    		else if(this.type==2)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.rc_tv_type1),250); 
	    		}
	    		else if(this.type==3)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.ogn_tv_type1),250);  
	    		}
	    		else if(this.type==4)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.defect_tv_type1),250);  
	    		}
	    		else if(this.type==5)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.damage_tv_type1),250);   
	    		}
	    		else if(this.type==6)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.auxbuild_tv_type1),500);   
	    		}
	    		else if(this.type==7)
	    		{
	    			if(!s.toString().equals("")){
	    			     if(Character.toString(s.toString().charAt(0)).equals("0"))
		    			 {
		    				 cf.ShowToast("Invalid Entry! Please enter the valid Insurance Policy No.", 0);
		    				 ((EditText)findViewById(R.id.edinsupolicy)).setText("");
		    			 }
	    			 } 
	    		}
	    		else if(this.type==8)
	    		{
	    			if(!s.toString().equals("")){
	    			     if(Character.toString(s.toString().charAt(0)).equals("0"))
		    			 {
		    				 cf.ShowToast("Invalid Entry! Please enter the valid Size.", 0);
		    				 ((EditText)findViewById(R.id.edsize)).setText("");
		    			 }
	    			 } 
	    		}
	    	}

	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,

	    			int after) {
	    		// TODO Auto-generated method stub
	    		
	    	}

	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,

	    			int count) {
	    		// TODO Auto-generated method stub
	    		
	    	}
	    	
	    }
	
	class Spin_Selectedlistener implements OnItemSelectedListener
	{
		int i;
		public Spin_Selectedlistener(int i) {
			// TODO Auto-generated constructor stub
			this.i=i;
		}

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			if(i==1){
				auxbuildtxt = auxspin.getSelectedItem().toString();
				if(auxbuildtxt.equals("Other")){ ((EditText)findViewById(R.id.edauxbuild_otr)).setVisibility(cf.v1.VISIBLE);}
					else{((EditText)findViewById(R.id.edauxbuild_otr)).setVisibility(cf.v1.GONE);	}	
				
				//clear();
				
			}
			else if(i==2)
			{
				roofcoveringtypetxt = rcspintype.getSelectedItem().toString();
				if(roofcoveringtypetxt.equals("Other")){ ((EditText)findViewById(R.id.edroofcovertype_othr)).setVisibility(cf.v1.VISIBLE);}
					else{((EditText)findViewById(R.id.edroofcovertype_othr)).setVisibility(cf.v1.GONE);	}		
			}
		}
		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub			
		}		
	}
	class checklistenetr implements OnCheckedChangeListener
	{
    	int i;
		public checklistenetr(int i) {
			// TODO Auto-generated constructor stub
		   this.i=i;
		}
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			   RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
		        boolean isChecked = checkedRadioButton.isChecked();
		        if (isChecked)
		        {
		          switch (i) 
		          {
					case 1:
						insunotxt= checkedRadioButton.getText().toString().trim();
						if(insunotxt.equals("Yes")){							
							((LinearLayout)findViewById(R.id.insupolicynolin)).setVisibility(cf.v1.VISIBLE);
							((EditText)findViewById(R.id.edinsupolicy)).setVisibility(cf.v1.VISIBLE);
						}else{((LinearLayout)findViewById(R.id.insupolicynolin)).setVisibility(cf.v1.GONE);
						((EditText)findViewById(R.id.edinsupolicy)).setText("");}
						break;
					case 2:
						sizetxt= checkedRadioButton.getText().toString().trim();
						break;
					case 3:
						wallstructuretxt= checkedRadioButton.getText().toString().trim();						
					 break;
					case 4:
						defecttxt = checkedRadioButton.getText().toString().trim();
						if(defecttxt.equals("Yes")){
							((LinearLayout)findViewById(R.id.defectlin)).setVisibility(cf.v1.VISIBLE);
							((EditText)findViewById(R.id.eddecay_otr)).setVisibility(cf.v1.VISIBLE);
						}else{((LinearLayout)findViewById(R.id.defectlin)).setVisibility(cf.v1.GONE);
						((EditText)findViewById(R.id.eddecay_otr)).setText("");}						
					 break;
					case 5:
						roofcovertxt= checkedRadioButton.getText().toString().trim();
						if(roofcovertxt.equals("Poor")){
							((LinearLayout)findViewById(R.id.rclin)).setVisibility(cf.v1.VISIBLE);
							((EditText)findViewById(R.id.edroofcover_othr)).setVisibility(cf.v1.VISIBLE);
						}else{((LinearLayout)findViewById(R.id.rclin)).setVisibility(cf.v1.GONE);
						((EditText)findViewById(R.id.edroofcover_othr)).setText("");}						
					 break;
					case 6:
						threeyearstxt = checkedRadioButton.getText().toString().trim();
					 break;
					case 7:
						damagetxt = checkedRadioButton.getText().toString().trim();
						if(damagetxt.equals("Yes")){
							((LinearLayout)findViewById(R.id.damagelin)).setVisibility(cf.v1.VISIBLE);
							((EditText)findViewById(R.id.eddamage_otr)).setVisibility(cf.v1.VISIBLE);
						}else{((LinearLayout)findViewById(R.id.damagelin)).setVisibility(cf.v1.GONE);
						((EditText)findViewById(R.id.eddamage_otr)).setText("");}
					 break;
					case 8:
						overgencondtxt = checkedRadioButton.getText().toString().trim();
						if(overgencondtxt.equals("Poor")){
							((LinearLayout)findViewById(R.id.ognlin)).setVisibility(cf.v1.VISIBLE);
							((EditText)findViewById(R.id.edognotr)).setVisibility(cf.v1.VISIBLE);
						}else{((LinearLayout)findViewById(R.id.ognlin)).setVisibility(cf.v1.GONE);
						((EditText)findViewById(R.id.edognotr)).setText("");}
					 break;					
		          }
		        }
		}
	}
	public void clicker(View v) {
		switch(v.getId())
		{
		case R.id.shwaux:
			if(!((CheckBox)findViewById(R.id.auxnotapplicable)).isChecked()){
				hideotherlayouts();
				((CheckBox)findViewById(R.id.auxnotapplicable)).setChecked(false);
				((LinearLayout)findViewById(R.id.auxlin)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdwaux)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.shwaux)).setVisibility(cf.v1.GONE);
			}
			break;
			case R.id.hdwaux:
				((ImageView)findViewById(R.id.shwaux)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdwaux)).setVisibility(cf.v1.GONE);
				((LinearLayout)findViewById(R.id.auxlin)).setVisibility(cf.v1.GONE);
				break;
		case R.id.loadcomments:
			/***Call for the comments***/
			cf.findinspectionname(cf.identityval);
			 int len=((EditText)findViewById(R.id.auxbuilcomment)).getText().toString().length();
			 	if(load_comment )
				{
					load_comment=false;
					int loc[] = new int[2];
					v.getLocationOnScreen(loc);
					Intent in1 = cf.loadComments("Auxilary Building Comments",loc);
					in1.putExtra("insp_name", cf.inspname);
					in1.putExtra("insp_ques", "Auxilary Buildings");
					in1.putExtra("length", len);
					in1.putExtra("max_length", 500);
					startActivityForResult(in1, cf.loadcomment_code);
				}
			/***Call for the comments ends***/			
			break;

		case R.id.mouseoverid:
			cf.ShowToast("Complete for Wind Mitigation and Replacement Cost Evaluations all others default to NA.",0);
			break;
		case R.id.auxnotapplicable:
			if(((CheckBox)findViewById(R.id.auxnotapplicable)).isChecked())
			{
				auxnotappl="1";
				if(rwsauxbuil>0)
				{
							AlertDialog.Builder bl = new Builder(WindMitCommAuxbuildings.this);
							bl.setTitle("Confirmation");
							bl.setIcon(R.drawable.alertmsg);
							bl.setMessage(Html.fromHtml("Do you want to clear the Auxiliary Building data?"));
							bl.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,int id) 
								{
									clear();
									((CheckBox)findViewById(R.id.auxnotapplicable)).setChecked(true);
									((LinearLayout)findViewById(R.id.auxlin)).setVisibility(cf.v1.GONE);
									((TextView)findViewById(R.id.savetxtaux)).setVisibility(cf.v1.INVISIBLE);
									((ImageView)findViewById(R.id.shwaux)).setVisibility(cf.v1.VISIBLE);
					    			((ImageView)findViewById(R.id.hdwaux)).setVisibility(cf.v1.GONE);
					    			((ImageView)findViewById(R.id.shwaux)).setEnabled(false);
								}
							});
							bl.setNegativeButton("No",new DialogInterface.OnClickListener() 
							{
								public void onClick(DialogInterface dialog,	int id) 
								{
									 ((CheckBox)findViewById(R.id.auxnotapplicable)).setChecked(false);
									 ((ImageView)findViewById(R.id.shwaux)).setVisibility(cf.v1.GONE);
						    		 ((ImageView)findViewById(R.id.hdwaux)).setVisibility(cf.v1.VISIBLE);
								}
							 });
							AlertDialog al=bl.create();
							al.setIcon(R.drawable.alertmsg);
							al.setCancelable(false);
							al.show();
				
				}
				else
				{
					((TextView)findViewById(R.id.savetxtaux)).setVisibility(cf.v1.INVISIBLE);
					((LinearLayout)findViewById(R.id.auxlin)).setVisibility(cf.v1.GONE);
					((ImageView)findViewById(R.id.shwaux)).setVisibility(cf.v1.VISIBLE);
 			    	((ImageView)findViewById(R.id.hdwaux)).setVisibility(cf.v1.GONE);
 			    	((ImageView)findViewById(R.id.shwaux)).setEnabled(false);
				}
			}
			else
			{
				auxnotappl="0";
				clear();
				((LinearLayout)findViewById(R.id.auxlin)).setVisibility(cf.v1.VISIBLE);
				((TextView)findViewById(R.id.savetxtaux)).setVisibility(cf.v1.INVISIBLE);
				 ((CheckBox)findViewById(R.id.auxnotapplicable)).setChecked(false);
				((ImageView)findViewById(R.id.shwaux)).setVisibility(cf.v1.GONE);
			     ((ImageView)findViewById(R.id.hdwaux)).setVisibility(cf.v1.VISIBLE);
			     ((ImageView)findViewById(R.id.shwaux)).setEnabled(true);
			}
			break;
		
		case R.id.wallstructure_chk:
			if (((CheckBox) v).isChecked()) {
				((LinearLayout)findViewById(R.id.wslin)).setVisibility(cf.v1.VISIBLE);}
			else{((LinearLayout)findViewById(R.id.wslin)).setVisibility(cf.v1.GONE);}
			break;
		case R.id.save:
			Cursor c1 = cf.arr_db.rawQuery("SELECT * FROM "+ cf.Comm_AuxBuilding + " WHERE fld_srid='"+ cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'", null);				
			if(c1.getCount()==0 && ((CheckBox)findViewById(R.id.auxnotapplicable)).isChecked()==false)
			{
				cf.ShowToast("Atleast one Auxiliary Building need to be added and saved.",0);		
			}
			/*else if(((EditText)findViewById(R.id.auxbuilcomment)).getText().toString().trim().equals(""))
			{
				cf.ShowToast("Please enter the Overall Auxiliary Building Comments.",0);	
				cf.setFocus(((EditText)findViewById(R.id.auxbuilcomment)));
			}*/
			else
			{
				insertauxbuld();
			}
			break;
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.auxsave:
			aux_Insu_Size_val="";
			aux_Cons__RC_3Years_val="";
			aux_WS_RC_OGN_DD="";			
			boolean b[] =  new boolean[8];
			
			b[0]=((insunotxt.equals("Yes")) && ((EditText)findViewById(R.id.edinsupolicy)).getText().toString().trim().equals("")) ? false:true;
		//	b[1]=((wallstructuretxt.equals("Poor")) && ((EditText)findViewById(R.id.edwallstur_othr)).getText().toString().trim().equals("")) ? false:true;
			b[2]=((roofcovertxt.equals("Poor")) && ((EditText)findViewById(R.id.edroofcover_othr)).getText().toString().trim().equals("")) ? false:true;
			b[3]=((overgencondtxt.equals("Poor")) && ((EditText)findViewById(R.id.edognotr)).getText().toString().trim().equals("")) ? false:true;
			b[4]=((defecttxt.equals("Yes")) && ((EditText)findViewById(R.id.eddecay_otr)).getText().toString().trim().equals("")) ? false:true;
			b[5]=((damagetxt.equals("Yes")) && ((EditText)findViewById(R.id.eddamage_otr)).getText().toString().trim().equals("")) ? false:true;
			b[6]=((auxbuildtxt.equals("Other")) && ((EditText)findViewById(R.id.edauxbuild_otr)).getText().toString().trim().equals("")) ? false:true;
			b[7]=((roofcoveringtypetxt.equals("Other")) && ((EditText)findViewById(R.id.edroofcovertype_othr)).getText().toString().trim().equals("")) ? false:true;
			String size = ((EditText)findViewById(R.id.edsize)).getText().toString().trim();
			if(!auxbuildtxt.equals("--Select--") && b[6])
			{
				if(!insunotxt.equals("") && b[0])
				{
					if(!((EditText)findViewById(R.id.edsize)).getText().toString().trim().equals(""))
					{
						if(Integer.parseInt(size)>0)		
						{
						
						if(!sizetxt.equals(""))
						{
							if(!constypespin.getSelectedItem().toString().equals("--Select--"))
							{
								if(!rcspintype.getSelectedItem().toString().equals("--Select--") && b[7])
								{
									if(!wallstructuretxt.equals(""))
									{
										if(!defecttxt.equals("") && b[4])
										{
											if(!roofcovertxt.equals("") && b[2])
											{
												if(!threeyearstxt.equals(""))
												{
													if(!damagetxt.equals("") && b[5])
													{
														if(!overgencondtxt.equals("") && b[3])
														{
															insertdata();clear();
															 Comm_Aux_Buil_SetValue();
															 
														}
														else
														{
															if(overgencondtxt.equals("")){cf.ShowToast("Please select the option for Overall General Condition.",0);}
															else if(b[3]==false)
															{
																 cf.ShowToast("Please enter the Comments for Overall General Condition - Poor.",0);
																cf.setFocus(((EditText)findViewById(R.id.edognotr)));
															}
														}
													}
													else
													{
														if(damagetxt.equals("")){cf.ShowToast("Please select the option for Damage Covering/Leakage Noted.",0);}
														else if(b[5]==false)
														{
															 cf.ShowToast("Please enter the Comments for Damage Covering/Leakage Noted - Yes.",0);
															 cf.setFocus(((EditText)findViewById(R.id.eddamage_otr)));
														}
													}
												}
												else
												{
													cf.ShowToast("Please select the option for 3 Years Replacement Probability.",0);																
												}
											}
											else
											{
												if(roofcovertxt.equals("")){cf.ShowToast("Please select the option for General Roof Covering Condition.",0);}
												else if(b[2]==false)
												{
													 cf.ShowToast("Please enter the Comments for the  General Roof Covering Condition - Poor.",0);
													 cf.setFocus(((EditText)findViewById(R.id.edroofcover_othr)));
												}
											}
										}
										else
										{
											if(defecttxt.equals("")){cf.ShowToast("Please select the option for Wall Damage/Decay Noted.",0);}
											else if(b[4]==false)
											{
												 cf.ShowToast("Please enter the Comments for Wall Damage/Decay Noted - Yes.",0);
												 cf.setFocus(((EditText)findViewById(R.id.eddecay_otr)));
											}		
										}
									}
									else
									{
										if(wallstructuretxt.equals("")){cf.ShowToast("Please select the option for Wall Structure/Wall Cladding.",0);}
									}									
								}
								else
								{
									if(rcspintype.getSelectedItem().toString().equals("--Select--"))
									{
										cf.ShowToast("Please select the Roof Covering Type.",0);
									}else if(b[7]==false)
									{
										 cf.ShowToast("Please enter the other text for Roof Covering Type.",0);
										 cf.setFocus(((EditText)findViewById(R.id.edroofcovertype_othr)));
									}
								}
							}
							else
							{
								cf.ShowToast("Please select the Construction Type.",0);
							}
						 
					  }
					  else
					  {
						  cf.ShowToast("Please select the option for Size.",0);
					  }
					}
					else
					{
						 cf.ShowToast("Invalid Entry! Please enter the valid Size.", 0);
							((EditText)findViewById(R.id.edsize)).setText("");cf.hidekeyboard();
						
					}
				}
				else
				{
						 cf.ShowToast("Please enter the Size.",0);
						 cf.setFocus(((EditText)findViewById(R.id.edsize)));
				}						
			 }
			 else
			 {
				if(insunotxt.equals("")){cf.ShowToast("Please select the option for On Insurance Policy.",0);}
				else if(b[0]==false)
				{
						 cf.ShowToast("Please enter the Insurance Policy No.",0);
						 cf.setFocus(((EditText)findViewById(R.id.edinsupolicy)));
				}
			  }
		  }
		  else
		  {
				if(auxbuildtxt.equals("--Select--")){cf.ShowToast("Please select the Auxiliary Building.",0);}
				else if(b[6]==false){cf.ShowToast("Please enter the other text for Auxiliary Building.",0);
				cf.setFocus(((EditText)findViewById(R.id.edauxbuild_otr)));}
		   }
		break;
		case R.id.auxcancel:
			clear();
			break;
		}
	}
	protected void updatenotappl() {
		// TODO Auto-generated method stub		
		try
		{
			Cursor c2=cf.SelectTablefunction(cf.Comm_Aux_Master, " WHERE fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");			
			if(c2.getCount()==0)
			{
				cf.arr_db.execSQL("INSERT INTO " + cf.Comm_Aux_Master + " (fld_srid,insp_typeid,fld_aux_NA)"
						+ "VALUES ('"+cf.selectedhomeid+"','"+cf.identityval+"','"+auxnotappl+"')");
			}
			else
			{
				cf.arr_db.execSQL("UPDATE "	+ cf.Comm_Aux_Master	+ " SET fld_aux_NA='"+auxnotappl+"' WHERE fld_srid ='"+ cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'");	
			}			
		}
		catch(Exception e)
		{
			System.out.println("sdfdfds"+e.getMessage());
		}
	}
	private void insertauxbuld() {
		// TODO Auto-generated method stub	
		if(((CheckBox)findViewById(R.id.auxnotapplicable)).isChecked())
		 {
			auxnotappl  ="1";
			 try
			 {
				 cf.arr_db.execSQL("Delete from "+cf.Comm_AuxBuilding+" Where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
			 }
			 catch(Exception e)
			 {
					System.out.println("applnotappl="+e.getMessage());		
			 }
		 }
       else
       {
    	   auxnotappl ="0";
       }
		updatenotappl();
		Cursor c3 = cf.arr_db.rawQuery("SELECT * FROM " + cf.Wind_Commercial_Comm + " WHERE fld_srid='" + cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'", null);				
		if(c3.getCount()==0)
		{
			 cf.arr_db.execSQL("INSERT INTO "
						+ cf.Wind_Commercial_Comm
						+ " (fld_srid,insp_typeid,fld_commercialareascomments,fld_auxillarycomments,fld_restsuppcomments)"
						+ "VALUES ('"+cf.selectedhomeid+"','"+cf.identityval+"','','"+cf.encode(((EditText)findViewById(R.id.auxbuilcomment)).getText().toString().trim())+"','')");
		}
		else
		{
			cf.arr_db.execSQL("UPDATE "
					+ cf.Wind_Commercial_Comm
					+ " SET fld_auxillarycomments='"+cf.encode(((EditText)findViewById(R.id.auxbuilcomment)).getText().toString().trim())+"'"
					+ " WHERE fld_srid ='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
		}
		cf.ShowToast("Auxiliary Building saved successfully", 0);
		cf.gotoclass(cf.identityval, WindMitCommRestsupp.class);
	}
	private void insertdata() {
		// TODO Auto-generated method stub		
		aux_Insu_Size_val += insunotxt+"&#39;"+cf.encode(((EditText)findViewById(R.id.edinsupolicy)).getText().toString().trim())+"&#94;"+sizetxt+"&#39;"+Integer.parseInt(((EditText)findViewById(R.id.edsize)).getText().toString().trim()); 
		aux_Cons__RC_3Years_val += constypespin.getSelectedItem().toString()+"&#94;"+rcspintype.getSelectedItem().toString()+"&#39;"+cf.encode(((EditText)findViewById(R.id.edroofcovertype_othr)).getText().toString().trim())+"&#94;"+threeyearstxt;
		aux_WS_RC_OGN_DD +=	 wallstructuretxt+"&#94;"
							+roofcovertxt+"&#39;"+cf.encode(((EditText)findViewById(R.id.edroofcover_othr)).getText().toString().trim())+"&#94;"
							+overgencondtxt+"&#39;"+cf.encode(((EditText)findViewById(R.id.edognotr)).getText().toString().trim())+"&#94;"
							+defecttxt+"&#39;"+cf.encode(((EditText)findViewById(R.id.eddecay_otr)).getText().toString().trim())+"&#94;"
							+damagetxt+"&#39;"+cf.encode(((EditText)findViewById(R.id.eddamage_otr)).getText().toString().trim())+"&#94;";
		try
		 {
			Cursor c1 = cf.arr_db.rawQuery("SELECT * FROM "
					+ cf.Comm_AuxBuilding + " WHERE fld_srid='"
					+ cf.selectedhomeid + "' and fld_aux_Building_type='"+auxspin.getSelectedItem().toString()+"' and insp_typeid='"+cf.identityval+"'", null);				
			
			if(c1.getCount()==0)
			 {
				 cf.arr_db.execSQL("INSERT INTO "
							+ cf.Comm_AuxBuilding
							+ " (fld_srid,insp_typeid,fld_aux_Building_type,fld_aux_Building_other,fld_aux_Ins_Size,fld_aux_Cons__RC_3Years,fld_aux_WS_RC_OGN_DD)"
							+ "VALUES ('"+cf.selectedhomeid+"','"+cf.identityval+"','"+ auxspin.getSelectedItem().toString() + "','"+cf.encode(((EditText)findViewById(R.id.edauxbuild_otr)).getText().toString().trim())+"','"+aux_Insu_Size_val+"','"+cf.encode(aux_Cons__RC_3Years_val)+"','"+cf.encode(aux_WS_RC_OGN_DD)+"')");
			}
			else
			{
					cf.arr_db.execSQL("UPDATE "
							+ cf.Comm_AuxBuilding
							+ " SET fld_aux_Ins_Size='"+aux_Insu_Size_val+"',fld_aux_Cons__RC_3Years='"+cf.encode(aux_Cons__RC_3Years_val)+"',fld_aux_WS_RC_OGN_DD='"+cf.encode(aux_WS_RC_OGN_DD)+"',fld_aux_Building_other='"+cf.encode(((EditText)findViewById(R.id.edauxbuild_otr)).getText().toString().trim())+"'"
							+ " WHERE fld_srid ='"+cf.selectedhomeid+"' and fld_aux_Building_type='"+auxspin.getSelectedItem().toString()+"' and insp_typeid='"+cf.identityval+"'");
			}
				cf.ShowToast("Auxiliary Building saved successfully.",0);
				clear();
		}
		 catch (Exception E)
		 {
			String strerrorlog="Updating Auxiliary Buildings - WindMit";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		 }
	}
	private void clear()
	{
		auxspin.setSelection(0);auxspin.setEnabled(true);rwsauxbuil=0; 
		//subpnlshw.setVisibility(cf.v1.GONE);
		//((TableLayout)findViewById(R.id.tableLayoutNote)).setVisibility(cf.v1.GONE);
		insuchk=true;sizechk=true;wallstruchk=true;roofcoverchk=true;ognchk=true;defectchk=true;damagechk=true;threeyearschk=true;
		try{if(insuchk){inspolicyrdgval.clearCheck();}}catch(Exception e){}
		try{if(sizechk){sizerdgval.clearCheck();}}catch(Exception e){}
		try{if(wallstruchk){walllstrurdgval.clearCheck();}}catch(Exception e){}
		try{if(roofcoverchk){roofcoverdgval.clearCheck();}}catch(Exception e){}
		try{if(ognchk){overallgencondrgpval.clearCheck();}}catch(Exception e){}
		try{if(defectchk){defectrgpval.clearCheck();}}catch(Exception e){}
		try{if(damagechk){damagergpval.clearCheck();}}catch(Exception e){}
		try{if(threeyearschk){threeyearsrgpval.clearCheck();}}catch(Exception e){}		
		((LinearLayout)findViewById(R.id.rclin)).setVisibility(cf.v1.GONE);
		((EditText)findViewById(R.id.edroofcover_othr)).setText("");
		((EditText)findViewById(R.id.edroofcover_othr)).setVisibility(cf.v1.GONE);		
		((LinearLayout)findViewById(R.id.ognlin)).setVisibility(cf.v1.GONE);
		((EditText)findViewById(R.id.edognotr)).setText("");
		((EditText)findViewById(R.id.edognotr)).setVisibility(cf.v1.GONE);		
		((LinearLayout)findViewById(R.id.defectlin)).setVisibility(cf.v1.GONE);
		((EditText)findViewById(R.id.eddecay_otr)).setText("");
		((EditText)findViewById(R.id.eddecay_otr)).setVisibility(cf.v1.GONE);		
		((LinearLayout)findViewById(R.id.damagelin)).setVisibility(cf.v1.GONE);
		((EditText)findViewById(R.id.eddamage_otr)).setText("");
		((EditText)findViewById(R.id.eddamage_otr)).setVisibility(cf.v1.GONE);
		rcspintype.setSelection(0);
		constypespin.setSelection(0);
		((LinearLayout)findViewById(R.id.insupolicynolin)).setVisibility(cf.v1.GONE);
		((EditText)findViewById(R.id.edinsupolicy)).setText("");
		((EditText)findViewById(R.id.edsize)).setText("");
		((EditText)findViewById(R.id.edauxbuild_otr)).setText("");
		((EditText)findViewById(R.id.edauxbuild_otr)).setVisibility(cf.v1.GONE);
		((EditText)findViewById(R.id.edroofcovertype_othr)).setText("");
		((EditText)findViewById(R.id.edroofcovertype_othr)).setVisibility(cf.v1.GONE);
		((Button)findViewById(R.id.auxsave)).setText("Save/Add More Auxiliary Building");
	}	
	private void hideotherlayouts() {
		// TODO Auto-generated method stub
		((LinearLayout)findViewById(R.id.auxlin)).setVisibility(cf.v1.GONE);	
		((ImageView)findViewById(R.id.shwaux)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.hdwaux)).setVisibility(cf.v1.GONE);;		
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
 		// replaces the default 'Back' button action
 		if (keyCode == KeyEvent.KEYCODE_BACK) {
 			if(cf.identityval==33){
 				cf.Roof=cf.getResourcesvalue(R.string.insp_5);
 			}
 			else if(cf.identityval==34){
 				cf.Roof=cf.getResourcesvalue(R.string.insp_6);
 			}
 			
 			cf.gotoclass(cf.identityval, Roof_commercial_information.class);
 			return true;
 		}
 		
 		return super.onKeyDown(keyCode, event);
 	}
	/**on activity result for the load comments**/
	 @Override
	 		protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	 			try
	 			{
	 				if(requestCode==cf.loadcomment_code)
	 				{
	 					load_comment=true;
	 					if(resultCode==RESULT_OK)
	 					{
	 						String auxcomments = ((EditText)findViewById(R.id.auxbuilcomment)).getText().toString();
	 						((EditText)findViewById(R.id.auxbuilcomment)).setText(auxcomments +" "+data.getExtras().getString("Comments"));
		 				}
	 				}
	 			}
	 			catch (Exception e) {
	 			// TODO: handle exception
	 			System.out.println("the erro was "+e.getMessage());
	 			}
	 	}/**on activity result for the load comments ends**/
}