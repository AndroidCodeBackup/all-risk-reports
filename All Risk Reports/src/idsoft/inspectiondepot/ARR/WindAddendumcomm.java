/*
   ************************************************************
   * Project: ALL RISK REPORT
   * Module : GCHComments.java
   * Creation History:
      Created By : Gowri on 
   * Modification History:
      Last Modified By : Gowri on 04/04/2013
   ************************************************************  
*/
package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.Observation4.textwatcher;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TableRow;
import android.widget.TextView;

public class WindAddendumcomm extends Activity{
	CommonFunctions cf;
	boolean load_comment=true;
	@Override
	   public void onCreate(Bundle savedInstanceState) {

	        super.onCreate(savedInstanceState);
	        cf=new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);
		 	}
	        setContentView(R.layout.fourcomments);
	        cf.getDeviceDimensions();

	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
	        
	        if(cf.identityval==31)
	        {
	          hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","B1 1802(Rev.01/12)","Addendum",3,1,cf));
	        }
	        else if(cf.identityval==32)
	        { 
	        	hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","Commercial Type I","Addendum",3,1,cf));
	        }
	        else if(cf.identityval==33)
	        {
	          hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","Commercial Type II","Addendum",3,1,cf));
	        }
	        else if(cf.identityval==34)
	        { 
	        	hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","Commercial Type III","Addendum",3,1,cf));
	        }
			LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
	     	main_layout.setMinimumWidth(cf.wd);
	     	main_layout.addView(new MyOnclickListener(getApplicationContext(), 3, 0,0,cf));
	       
	        cf.type_submenulayout =(LinearLayout) findViewById(R.id.typesubmenu); //** WINDMIT TYPE SUBMENU **//
	        cf.type_submenulayout.addView(new MyOnclickListener(this, 300, 1,0, cf));
	        cf.windmittypechk(cf.identityval,((RadioButton)findViewById(R.id.b1802_chk)),((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((TextView)findViewById(R.id.commtxt)),((ImageView)findViewById(R.id.editbno))); // To check the checkbox of choosen inspection type
	        cf.uncheckcommercial(((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((RadioButton)findViewById(R.id.b1802_chk))); 
	    	       
	        cf.submenu_layout = (LinearLayout) findViewById(R.id.submenu);//** WINDMIT TYPE SUBMENU **//	        
	        cf.submenu_layout .addView(new MyOnclickListener(this,cf.identityval,1,129, cf));
	        
	        cf.mainlayout = (LinearLayout)findViewById(R.id.bc_table);
	        cf.save = (Button)findViewById(R.id.save);
	        cf.tblrw = (TableRow)findViewById(R.id.row2);
	        cf.tblrw.setMinimumHeight(cf.ht);		
	        
			
			cf.CreateARRTable(50);
			try
			{
			   Cursor c1=cf.SelectTablefunction(cf.Addendum, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
			   if(c1.getCount()>0)
			   {  
				   c1.moveToFirst();
				   ((EditText)findViewById(R.id.fourcomment)).setText(cf.decode(c1.getString(c1.getColumnIndex("addendumcomment"))));   
				  /* if(cf.identityval==31)
				   {
					   ((EditText)findViewById(R.id.fourcomment)).setText(cf.decode(c1.getString(c1.getColumnIndex("b1802addcomment"))));   
				   }
				   else  if(cf.identityval==32)
				   {
					   ((EditText)findViewById(R.id.fourcomment)).setText(cf.decode(c1.getString(c1.getColumnIndex("commer1addcomment"))));
				   }
				   else  if(cf.identityval==33)
				   {
					   ((EditText)findViewById(R.id.fourcomment)).setText(cf.decode(c1.getString(c1.getColumnIndex("commer2addcomment"))));
				   }
				   else  if(cf.identityval==34)
				   {
					   ((EditText)findViewById(R.id.fourcomment)).setText(cf.decode(c1.getString(c1.getColumnIndex("commer3addcomment"))));
				   }		*/		   
			   }
			   else
			   {
				   ((EditText)findViewById(R.id.fourcomment)).setText("No additional comments on this property.");
			   }
			}
		    catch (Exception E)
			{
				String strerrorlog="Addendum COMMETS - Fourpoint";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			}
			((EditText)findViewById(R.id.fourcomment)).addTextChangedListener(new textwatcher(1));
	}
	 class textwatcher implements TextWatcher


	    {
	         public int type;
	         textwatcher(int type)
	    	{
	    		this.type=type;
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {


	    		// TODO Auto-generated method stub
	    		if(this.type==1)
	    		{
	    			
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.addencom_tv_type1),500);
	    			if(s.toString().startsWith(" "))
	    			{
	    				((EditText)findViewById(R.id.fourcomment)).setText("");
	    			}
	    		}
	    	
	    		
	    	}

	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,

	    			int after) {
	    		// TODO Auto-generated method stub
	     	}

	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,

	    			int count) {
	    		// TODO Auto-generated method stub
	    	}
	   }	
	public void clicker(View v)
	{
		switch(v.getId())
		{
		case R.id.hme:
			cf.gohome();
				break;
		
		case R.id.save:
			if(((EditText)findViewById(R.id.fourcomment)).getText().toString().trim().equals(""))
			{
				cf.ShowToast("Please enter the Addendum Comments.", 0);
			}
			else
			{
				Cursor c1=null;
				try
				{
					c1=cf.SelectTablefunction(cf.Addendum, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
					if(c1.getCount()>0)
					{
						try
						{
							cf.arr_db.execSQL("UPDATE "+cf.Addendum+ " set addendumcomment ='"+cf.encode(((EditText)findViewById(R.id.fourcomment)).getText().toString())+"' where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
							cf.ShowToast("Addendum Comments updated successfully.", 1);
						}
						catch (Exception E)
						{
							System.out.println("dddd"+E.getMessage());
							String strerrorlog="Updating the COMMENTS - GCH";
							cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
						}
					}
					else
					{
						try
						{							
							cf.arr_db.execSQL("INSERT INTO "
									+ cf.Addendum
									+ " (fld_srid,insp_typeid,addendumcomment)"
									+ "VALUES ('"+cf.selectedhomeid+"','"+cf.identityval+"','"+cf.encode(((EditText)findViewById(R.id.fourcomment)).getText().toString())+"')");
					
							cf.ShowToast("Addendum Comments saved successfully.", 1);
						}
						catch (Exception E)
						{
							String strerrorlog="Inserting the COMMENTS- GCH";
							cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
						}
					}
				}
				catch (Exception E)
				{
						String strerrorlog="Checking the rows inserted in the Addendum COMMENTS table.";
						cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
			}
			break;
		case R.id.loadcomments:
			/***Call for the comments***/
			 int len=((EditText)findViewById(R.id.fourcomment)).getText().toString().length();
			 if(load_comment )
				{
					load_comment=false;
					int loc[] = new int[2];
					v.getLocationOnScreen(loc);
					if(cf.identityval==31)
					{
					   	cf.loadinsp_n="3";
				    }
				    else if(cf.identityval==32 || cf.identityval==33 || cf.identityval==34)
				    { 
				       	cf.loadinsp_n="4";
				    }
					Intent in1 = cf.loadComments("Addendum Comments",loc);
					in1.putExtra("insp_ques","Addendum");
					in1.putExtra("insp_name",cf.loadinsp_n);
					in1.putExtra("length", len);
					in1.putExtra("max_length", 500);
					startActivityForResult(in1, cf.loadcomment_code);
				}
			
			 break;
		}
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	
		  if(requestCode==cf.loadcomment_code)
		  {
			  load_comment=true;
			if(resultCode==RESULT_OK)
			{
				cf.ShowToast("Comments added successfully.", 0);
				 String gchcomts = ((EditText)findViewById(R.id.fourcomment)).getText().toString();
				 ((EditText)findViewById(R.id.fourcomment)).setText((gchcomts+" "+data.getExtras().getString("Comments")).trim());
				
			}
			
		}

	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
 		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) 
		{
			if(cf.identityval==31)
			{
				cf.gotoclass(cf.identityval, WindMitWallCons.class);
			}
			else if(cf.identityval==32)
			{
				/*cf.Roof=cf.getResourcesvalue(R.string.insp_4);
				cf.gotoclass(cf.identityval, Roof_section_common.class);*/
				cf.gotoclass(cf.identityval, WindMitCommareas.class);
			}
			else if(cf.identityval==33 || cf.identityval==34)
			{
				//cf.gotoclass(cf.identityval, WindMitCommRestsupp.class);
				cf.gotoclass(cf.identityval, WindMitCommareas.class);
			}
			else 
			{
				cf.goclass(25);
			}
			return true;
		}
 		return super.onKeyDown(keyCode, event);
 	}
}