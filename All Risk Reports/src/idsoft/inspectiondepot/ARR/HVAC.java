/*
************************************************************
* Project: ALL RISK REPORT
* Module : HVAC.java
* Creation History:
Created By : Gowri on 
* Modification History:
Last Modified By : Gowri on 04/09/2013
************************************************************ 
*/
package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.Electrical.Spin_Selectedlistener;
import idsoft.inspectiondepot.ARR.Electrical.checklistenetr;
import idsoft.inspectiondepot.ARR.Electrical.textwatcher;

import java.io.File;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class HVAC extends Activity {
	CommonFunctions cf;
	boolean hack[]={false,false,false,false,false};
	public int pick_img_code=24;
	public int[] chkbxheat = {R.id.typ_chk1,R.id.typ_chk2,R.id.typ_chk3,R.id.typ_chk4,R.id.typ_chk5,
			R.id.typ_chk6,R.id.typ_chk7,R.id.typ_chk8,R.id.typ_chk10};
	public CheckBox heatchk[] = new CheckBox[9];
	public static String getstr="";
	public static boolean chkbol=false;
	boolean openflame=false,portableheater=false,yicck=false,epchk = false;;
	String 	agestr1="";
	RadioGroup yicrdgval;
	String hvaczone[] = {"--Select--","1","2","3","4","5","6"};
	String heatchk_vlaue="",zonestr="--Select--",ftlrdgtxt="",ofhsptxt="",fpitxt="",typerdval="",phptxt="",yicrdgtxt="",
			porheatchk_vlaue="",unttypstr="",untnostr="",agestr="",upgdstr="",cupgdstr="",locstr="",ocstr="",
			ceilstr="",rethvacchkval,dripstr="",hvacchkval="0",upgdyesstr="",cupgdyesstr="",wotxt="",rethsrdtxt="",phsstr="--Select--",hzstr="--Select--";
	ArrayAdapter adapter,unttypadap,untnoadap,ageadap,ageadap1,upgdadap,cupgdadap,locadap,ocadap,ceiladap,dripadap,intlocadap;
	static ArrayAdapter phsadap;
	ArrayAdapter hzadap;
	RadioGroup ftlrdgval,ofhsprdgval,fpirdgval,phhprdgval,wordgval;
	Spinner agespin1,zonespin,unttypespin,untnospin,agespin,upgdspin,cupgdspin,locspin,ocspin,ceilspin,dripspin,intlocspin;
	static Spinner phsspin;
	Spinner hzspin;
	public int[] rdbtype = {R.id.type_rd1,R.id.type_rd2,R.id.type_rd3,R.id.type_rd4,R.id.type_rd5,
			R.id.type_rd6,R.id.type_rd7,R.id.type_rd8,R.id.type_rd9};
	public RadioButton typerd[] = new RadioButton[9];
	public int[] chkbxporheat = {R.id.ht_chk1,R.id.ht_chk2,R.id.ht_chk3,R.id.ht_chk4};
	public CheckBox porheatchk[] = new CheckBox[4];
	String unttype[] = {"--Select--","External System/Unit","Internal System/Unit"};
	String phsys[] = {"--Select--","Heat Pump","Electric Heat","Hydronic System","Wall Unit(s)","Window Unit(s)",
			 "Central Air Conditioning - Cool Only","Packaged Central Air-Heat Pump","Packaged Air Conditioning - Cool Only","Heat Strip",
			 "Other"};
	String zonnum[] = {"--Select--","Zone 1","Zone 2","Zone 3","Zone 4","Zone 5"};
	String pupgd[] = {"--Select--","Main System","Bonus","Master Bedroom"};
	String cupgd[] = {"--Select--","Yes","No","N/A","Yes - Partial"};
	String upgd[] = {"--Select--","Yes","No","Not Applicable"};
	
	String locationext[] = {"--Select--","Front Elevation","Right Elevation","Back Elevation","Left Elevation","Roof Top"};
	String oc[] = {"--Select--","Satisfactory","Average","Poor","Deferred Maintenance Noted","Inoperable"};
	String locationint[] = {"--Select--","Attic","Closet","Garage","Refer to Comments","Other"};
	int unitCurrent_select_id,spunt=0,edt=0,epval=0,mpo=0;
	Cursor hvacunit_save,chkhvacunit_save,chkhvac_save;
	String unit_in_DB[],unittype_in_DB[];
	TableLayout unttblshow;
	static ArrayList<String> str_arrlst = new ArrayList<String>();
	EditText et_nocomments,et_hvaccomments,dstedt,upgdedt,cupgdedt;
	public boolean load_comment=true, hv_sve=false;
	
	@Override
	   public void onCreate(Bundle savedInstanceState) {


	        super.onCreate(savedInstanceState);
	        cf=new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);
				
		 	}
	        setContentView(R.layout.fourhvac);
	        cf.getDeviceDimensions();
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
			hdr_layout.addView(new HdrOnclickListener(this,1,"Four Point Inspection","HVAC System",2,0,cf));
         	LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
         	main_layout.setMinimumWidth(cf.wd);
         	main_layout.addView(new MyOnclickListener(getApplicationContext(), 2, 0,0,cf));
	        LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 25, 1,0,cf));
			TableRow tblrw = (TableRow)findViewById(R.id.row2);
			tblrw.setMinimumHeight(cf.ht);
			declarations();
			
			cf.CreateARRTable(25);
			cf.CreateARRTable(26);
			HVAC_setValue();			
			ShowUnitvalue();
			
			
		   /* str_arrlst.clear();
		    getstr="";
		    System.out.println("gerstr after clear "+getstr);
			for(int i=0;i<heatchk.length;i++)
			{
				if(heatchk[i].isChecked())
				{
					System.out.println("check="+getstr);
					
					   if(getstr.equals(""))
			    	   {
			    		   getstr = heatchk[i].getText().toString().trim();
			    	   }
			    	   else
			    	   {
			    		   getstr += ","+heatchk[i].getText().toString().trim();
			    	   }
				}
					
				System.out.println("fistgetstr="+getstr);
				
			}*/
			/*if(getstr.length()>1)
			{
				
				getstr=getstr.substring(0,getstr.lastIndexOf(","));
				System.out.println("aftergetstr="+getstr);
				if(getstr.contains(","))
				{
					String[] arr = getstr.split(",");
					System.out.println("arrlen="+arr.length);
					str_arrlst.add("--Select--");
					for(int j=0;j<arr.length;j++)
					{
						str_arrlst.add(arr[j]);
					}
				}
				else
				{
					str_arrlst.add("--Select--");
					str_arrlst.add(getstr);
				}
			}*/
		/*	System.out.println("getstrfinal="+getstr);
			 phsadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, str_arrlst);
			 phsadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			 phsspin.setAdapter(phsadap);*/
			
	}
	
	
	private void HVAC_setValue() {
		// TODO Auto-generated method stub
		try
		{
		   chk_values();System.out.println("chekvalue");
		   Cursor HVAC_retrive=cf.SelectTablefunction(cf.Four_HVAC, " where fld_srid='"+cf.selectedhomeid+"'");System.out.println("HVAC_retrive"+HVAC_retrive.getCount());
		   if(HVAC_retrive.getCount()>0)
		   {  
			   HVAC_retrive.moveToFirst();
			   System.out.println("hvaccomment=");
			   if(HVAC_retrive.getString(HVAC_retrive.getColumnIndex("hvaccomments")).equals(""))
			   {
				   ((EditText)findViewById(R.id.hvaccomment)).setText("No Comments.");
			   }
			   else
			   {
				   ((EditText)findViewById(R.id.hvaccomment)).setText(cf.decode(HVAC_retrive.getString(HVAC_retrive.getColumnIndex("hvaccomments"))));   
			   }
			   System.out.println("HVAC="+HVAC_retrive.getString(HVAC_retrive.getColumnIndex("fld_inchvac")));
			   
			   if(HVAC_retrive.getString(HVAC_retrive.getColumnIndex("fld_inchvac")).equals("0"))
			   {
				   System.out.println("fourhvac_tablet");
				    ((TableLayout)findViewById(R.id.fourhvac_table)).setVisibility(cf.v1.VISIBLE);System.out.println("foutable");
					((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);System.out.println("fourhvsavetxiet");
					
					heatchk_vlaue=cf.decode(HVAC_retrive.getString(HVAC_retrive.getColumnIndex("heatsource")));
				    cf.setvaluechk1(heatchk_vlaue,heatchk,((EditText)findViewById(R.id.typ_otr)));System.out.println("heatch-="+heatchk_vlaue);
				   
				    
				  zonestr=cf.decode(HVAC_retrive.getString(HVAC_retrive.getColumnIndex("noofzones")));System.out.println("zonestr"+zonestr);
				  System.out.println("after= "+zonestr);
				   zonespin.setSelection(adapter.getPosition(zonestr)); 
				   ftlrdgtxt=cf.decode(HVAC_retrive.getString(HVAC_retrive.getColumnIndex("tankloc")));System.out.println("ftlrdgtxt"+ftlrdgtxt);
				   if(ftlrdgtxt.equals("Not Applicable"))
				   {
					   ((LinearLayout)findViewById(R.id.distancelin)).setVisibility(cf.v1.GONE); 
				   }
				   else
				   {
					   ((LinearLayout)findViewById(R.id.distancelin)).setVisibility(cf.v1.VISIBLE);
				   }
				   ((RadioButton) ftlrdgval.findViewWithTag(ftlrdgtxt)).setChecked(true);
				   ((EditText)findViewById(R.id.distance_ettxt)).setText(cf.decode(HVAC_retrive.getString(HVAC_retrive.getColumnIndex("distance"))));
				   
				   
				   System.out.println(HVAC_retrive.getString(HVAC_retrive.getColumnIndex("distance")));
				   
				  
				   ofhsptxt=cf.decode(HVAC_retrive.getString(HVAC_retrive.getColumnIndex("openflame"))); System.out.println("ofhsptxt"+ofhsptxt);
				   ((RadioButton) ofhsprdgval.findViewWithTag(ofhsptxt)).setChecked(true);
				   if(ofhsptxt.equals("Yes")){
					   ((LinearLayout)findViewById(R.id.showfactor)).setVisibility(cf.v1.VISIBLE);
					   ((LinearLayout)findViewById(R.id.showtype)).setVisibility(cf.v1.VISIBLE);
				   fpitxt=cf.decode(HVAC_retrive.getString(HVAC_retrive.getColumnIndex("factor")));System.out.println("fpitxt"+fpitxt);
				   ((RadioButton) fpirdgval.findViewWithTag(fpitxt)).setChecked(true);
				  
				    typerdval=cf.decode(HVAC_retrive.getString(HVAC_retrive.getColumnIndex("type")));System.out.println("typerdval"+typerdval);
				    cf.setRadioBtnValue(typerdval,typerd);
				    if(typerdval.equals("Other"))
				    {	((EditText)findViewById(R.id.type_otr)).setVisibility(cf.v1.VISIBLE);
				        ((RadioButton)findViewById(R.id.type_rd9)).setChecked(true);
				        ((EditText)findViewById(R.id.type_otr)).setText(cf.decode(HVAC_retrive.getString(HVAC_retrive.getColumnIndex("typeotr"))));
				    }else{
				    	((EditText)findViewById(R.id.type_otr)).setVisibility(cf.v1.GONE);
				    }
				   }else
				   {
					   ((LinearLayout)findViewById(R.id.showfactor)).setVisibility(cf.v1.GONE);
					   ((LinearLayout)findViewById(R.id.showtype)).setVisibility(cf.v1.GONE);
				   }
				    phptxt=cf.decode(HVAC_retrive.getString(HVAC_retrive.getColumnIndex("porheater")));System.out.println("phptxt"+phptxt);
				   ((RadioButton) phhprdgval.findViewWithTag(phptxt)).setChecked(true);
				   
				   if(phptxt.equals("Yes")){
					   ((LinearLayout)findViewById(R.id.showheater)).setVisibility(cf.v1.VISIBLE);
					   porheatchk_vlaue=cf.decode(HVAC_retrive.getString(HVAC_retrive.getColumnIndex("poroption")));
					   cf.setvaluechk1(porheatchk_vlaue,porheatchk,((EditText)findViewById(R.id.heat_otr)));
				   }else{
					   ((LinearLayout)findViewById(R.id.showheater)).setVisibility(cf.v1.GONE);
				   }
				   
				   wotxt=cf.decode(HVAC_retrive.getString(HVAC_retrive.getColumnIndex("working")));System.out.println("work="+wotxt);
				   ((RadioButton) wordgval.findViewWithTag(wotxt)).setChecked(true);
				   if(wotxt.equals("No")){
					   ((LinearLayout)findViewById(R.id.nocommentslin)).setVisibility(cf.v1.VISIBLE);
					   ((EditText)findViewById(R.id.nocomment)).setText(cf.decode(HVAC_retrive.getString(HVAC_retrive.getColumnIndex("wrkcomments"))));
				   }else{
					   ((LinearLayout)findViewById(R.id.nocommentslin)).setVisibility(cf.v1.GONE);
				   }
				   

				    
				    agestr1	=cf.decode(HVAC_retrive.getString(HVAC_retrive.getColumnIndex("fld_ageofmainpanel")));System.out.println("aget="+agestr1);
					   
					   agespin1.setSelection(ageadap1.getPosition(agestr1)); 
					   if(agestr1.equals("Other")){mpo=1;
						   ((EditText)findViewById(R.id.age_otr1)).setVisibility(cf.v1.VISIBLE);
						   ((EditText)findViewById(R.id.age_otr1)).setText(cf.decode(HVAC_retrive.getString(HVAC_retrive.getColumnIndex("fld_ageofmainpanelother"))));
					   }else{
						   ((EditText)findViewById(R.id.age_otr1)).setVisibility(cf.v1.GONE);
					   }
					   System.out.println("year");
					   yicrdgtxt=cf.decode(HVAC_retrive.getString(HVAC_retrive.getColumnIndex("fld_yearofinstconfdience")));System.out.println("yicrdgtxt"+yicrdgtxt);
					   ((RadioButton) yicrdgval.findViewWithTag(yicrdgtxt)).setChecked(true);System.out.println("yrrdip");
				   
				   try
					  {
						  cf.arr_db.execSQL("UPDATE  "+cf.Four_HVAC_Unit+" SET fld_flag='0' Where fld_srid='"+cf.selectedhomeid+"' ");

					  }catch (Exception e) {
						// TODO: handle exception
					}
				   
			   }
			   else  if(HVAC_retrive.getString(HVAC_retrive.getColumnIndex("fld_inchvac")).equals("1"))
			   {
				   ((TableLayout)findViewById(R.id.fourhvac_table)).setVisibility(cf.v1.GONE);
				   ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
				   System.out.println("executed");
				   ((CheckBox)findViewById(R.id.hvac_na)).setChecked(true);
				
				  
			   }
			   
				  
		   }
		   else
		   {
			   defaulthvac();  ((EditText)findViewById(R.id.hvaccomment)).setText("No Comments.");
		   }
		}
	    catch (Exception E)
		{
			String strerrorlog="Retrieving HVAC - FourPoint";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
	public static void getlistner(String getstr2,String selectedstring)
	{
		str_arrlst.clear();
		System.out.println("insidegetstr2= "+getstr2);
		if(getstr2.length()>1)
		{
			System.out.println("getssts="+getstr2);
			if(!selectedstring.equals(""))
			 {
				 phsspin.setSelection(phsadap.getPosition(selectedstring));	 
				 if(selectedstring.contains("Other"))
				 {
					 
				 }
				 else
				 {
					 
				 }
			 }
			else
			{
				 phsspin.setSelection(0);
			}
			
			 /*if(getstr2.contains("Heat Pump"))
			 {
				 phsspin.setSelection(phsadap.getPosition("Heat Pump"));	                                           
			 }
			 else
			 {
				 phsspin.setSelection(0);
			 }*/
			if(getstr2.contains(","))
			{
				/*getstr2=getstr2.substring(0,getstr2.lastIndexOf(","));
				System.out.println("aftergetstr="+getstr2);*/
				String[] arr = getstr2.split(",");
				System.out.println("arrlen="+arr.length);
				str_arrlst.add("--Select--");
				for(int j=0;j<arr.length;j++)
				{
					
					str_arrlst.add(arr[j]);
				}
			}
			else
			{
				str_arrlst.add("--Select--");
				str_arrlst.add(getstr2);
			}
		}
		 System.out.println("getstr2="+getstr2);
		 phsadap.notifyDataSetChanged();
		 
	}
	
	private void declarations() {
		// TODO Auto-generated method stub
		cf.setupUI((ScrollView) findViewById(R.id.scr));
		getstr="";
		 for(int i=0;i<heatchk.length;i++)
    	 {
    		try{
    			heatchk[i] = (CheckBox)findViewById(chkbxheat[i]);
    			heatchk[i].setOnTouchListener(new OnTouchListener() {
					
					@Override
					public boolean onTouch(View v, MotionEvent event) {
						
						// TODO Auto-generated method stub
						chkbol = true;
						return false;
					}
				});
    			
    				heatchk[i].setOnCheckedChangeListener(new CheckListner(heatchk[i]));
    				System.out.println( "getstr"+ getstr);
    			
    			
    		}
			catch(Exception e)
			{
				System.out.println("error= "+e.getMessage());
			}
    	 }
		 zonespin=(Spinner) findViewById(R.id.noofzones_spin);
		 adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, hvaczone);
		 adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 zonespin.setAdapter(adapter);
		 zonespin.setOnItemSelectedListener(new  Spin_Selectedlistener(1));
		
		 ftlrdgval = (RadioGroup)findViewById(R.id.ftl_rdg);
		 ftlrdgval.setOnCheckedChangeListener(new checklistenetr(1));
		 ofhsprdgval = (RadioGroup)findViewById(R.id.ofhsp_rdg);
		 ofhsprdgval.setOnCheckedChangeListener(new checklistenetr(2));
		 fpirdgval = (RadioGroup)findViewById(R.id.fpi_rdg);
		 fpirdgval.setOnCheckedChangeListener(new checklistenetr(3));
		 dstedt = (EditText)findViewById(R.id.distance_ettxt);
		 dstedt.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
		 upgdedt = (EditText)findViewById(R.id.upgd_otr);
		 cupgdedt = (EditText)findViewById(R.id.cupgd_otr);
		
		 for(int i=0;i<9;i++)
    	 {
    		try{
    			typerd[i] = (RadioButton)findViewById(rdbtype[i]);
    		}
			catch(Exception e)
			{
				System.out.println("error= "+e.getMessage());
			}
    	 }
		 phhprdgval = (RadioGroup)findViewById(R.id.php_rdg);
		 phhprdgval.setOnCheckedChangeListener(new checklistenetr(4));
		 for(int i=0;i<4;i++)
    	 {
    		try{
    			porheatchk[i] = (CheckBox)findViewById(chkbxporheat[i]);
    		}
			catch(Exception e)
			{
				System.out.println("error= "+e.getMessage());
			}
    	 }
		 unttypespin=(Spinner) findViewById(R.id.unttype_spin);
		 unttypadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, unttype);
		 unttypadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 unttypespin.setAdapter(unttypadap);
		 unttypespin.setOnItemSelectedListener(new  Spin_Selectedlistener(2));
				 
		 untnospin=(Spinner) findViewById(R.id.untno_spin);
		 untnoadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, zonnum);
		 untnoadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 untnospin.setAdapter(untnoadap);
		 untnospin.setOnItemSelectedListener(new  Spin_Selectedlistener(3));
		 untnospin.setSelection(1);
		 
		 agespin=(Spinner) findViewById(R.id.age_spin);
		 ageadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, cf.yearbuilt);
		 ageadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 agespin.setAdapter(ageadap);
		 agespin.setOnItemSelectedListener(new  Spin_Selectedlistener(4));
		 defaultage();
		 
		 upgdspin=(Spinner) findViewById(R.id.upg_spin);
		 upgdadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, pupgd);
		 upgdadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 upgdspin.setAdapter(upgdadap);
		 upgdspin.setOnItemSelectedListener(new  Spin_Selectedlistener(5));
		 
		 cupgdspin=(Spinner) findViewById(R.id.cupg_spin);
		 cupgdadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, cupgd);
		 cupgdadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 cupgdspin.setAdapter(cupgdadap);
			 
		 cupgdspin.setOnItemSelectedListener(new  Spin_Selectedlistener(13));
		 
		 phsspin=(Spinner) findViewById(R.id.phs_spin);
		 phsadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, phsys);
		 phsadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 phsspin.setAdapter(phsadap);
		
		 phsspin.setOnItemSelectedListener(new  Spin_Selectedlistener(11));
		 
		/* hzspin=(Spinner) findViewById(R.id.nhz_spin);
		 hzadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, hvaczone);
		 hzadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 hzspin.setAdapter(hzadap);
		 hzspin.setOnItemSelectedListener(new  Spin_Selectedlistener(12));
		 hzspin.setSelection(1);*/
		 
		 locspin=(Spinner) findViewById(R.id.loc_spin);
		 if(unttypespin.getSelectedItem().toString().equals("Internal System/Unit"))
		 {
			 locadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, locationint);	 
		 }
		 else
		 {
			 locadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, locationext);
		 }
		 
		 locadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 locspin.setAdapter(locadap);
		 locspin.setOnItemSelectedListener(new  Spin_Selectedlistener(6));
		 
		 ocspin=(Spinner) findViewById(R.id.oc_spin);
		 ocadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, oc);
		 ocadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 ocspin.setAdapter(ocadap);
		 ocspin.setOnItemSelectedListener(new  Spin_Selectedlistener(7));
		 
		 ceilspin=(Spinner) findViewById(R.id.ceil_spin);
		 ceiladap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, upgd);
		 ceiladap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 ceilspin.setAdapter(ceiladap);
		 ceilspin.setOnItemSelectedListener(new  Spin_Selectedlistener(8));
		 
		 dripspin=(Spinner) findViewById(R.id.dplan_spin);
		 dripadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, upgd);
		 dripadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 dripspin.setAdapter(dripadap);
		 dripspin.setOnItemSelectedListener(new  Spin_Selectedlistener(9));
		 
		 intlocspin=(Spinner) findViewById(R.id.intloc_spin);
		 intlocadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, locationint);
		 intlocadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 intlocspin.setAdapter(intlocadap);
		 intlocspin.setOnItemSelectedListener(new  Spin_Selectedlistener(10));
		 
		 agespin1=(Spinner) findViewById(R.id.age_spin1);
		 ageadap1 = new ArrayAdapter(this,android.R.layout.simple_spinner_item, cf.yearbuilt);
		 ageadap1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 agespin1.setAdapter(ageadap1);//agespin.setSelection(ageadap.getPosition("2012"));
		 agespin1.setOnItemSelectedListener(new  Spin_Selectedlistener(14));
		 ((EditText)findViewById(R.id.age_otr1)).addTextChangedListener(new textwatcher(8));
		 
		 defaultage1();
		 yicrdgval = (RadioGroup)findViewById(R.id.yic_rdg);
		 yicrdgval.setOnCheckedChangeListener(new checklistenetr(6));
		 ((RadioButton)yicrdgval.findViewWithTag("Original")).setChecked(true);
		 
		 unttblshow =(TableLayout)findViewById(R.id.unitvalue);
		 
		 wordgval = (RadioGroup)findViewById(R.id.wo_rdg);
		 wordgval.setOnCheckedChangeListener(new checklistenetr(5));
		 et_nocomments = (EditText)findViewById(R.id.nocomment);
		 et_nocomments.addTextChangedListener(new textwatcher(1));
		 et_hvaccomments = (EditText)findViewById(R.id.hvaccomment);
		 et_hvaccomments.addTextChangedListener(new textwatcher(2));
		 ((EditText)findViewById(R.id.age_otr)).addTextChangedListener(new textwatcher(3));
		 ((EditText)findViewById(R.id.upgd_otr)).addTextChangedListener(new textwatcher(4));
		 ((EditText)findViewById(R.id.cupgd_otr)).addTextChangedListener(new textwatcher(6));
		 ((EditText)findViewById(R.id.distance_ettxt)).addTextChangedListener(new textwatcher(5));
		 
		 
		 ((EditText)findViewById(R.id.typ_otr)).addTextChangedListener(new textwatcher(9));
		 
		 int cupdpos = cupgdadap.getPosition("N/A");
		 cupgdspin.setSelection(cupdpos);	
	}
	private void defaultage1()
	{
		cf.getPolicyholderInformation(cf.selectedhomeid);
		if(cf.YearPH.contains("Other"))
		{
			  String otheryear[] = cf.YearPH.split("&#126;");
			  int spinnerPosition = ageadap1.getPosition(otheryear[0]);
			   agespin1.setSelection(spinnerPosition);
			  ((EditText)findViewById(R.id.age_otr1)).setVisibility(cf.v1.VISIBLE);
			  ((EditText)findViewById(R.id.age_otr1)).setText(otheryear[1]);			   
		}
		else
		{
			 int spinnerPosition = ageadap1.getPosition(cf.YearPH);
			   agespin1.setSelection(spinnerPosition);		  
			 ((EditText)findViewById(R.id.age_otr1)).setVisibility(cf.v1.GONE);
		}		
	}
	private void defaultage()
	{
		cf.getPolicyholderInformation(cf.selectedhomeid);
		if(cf.YearPH.contains("Other"))
		{
			  String otheryear[] = cf.YearPH.split("&#126;");
			  int spinnerPosition = ageadap.getPosition(otheryear[0]);
			   agespin.setSelection(spinnerPosition);
			  ((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.VISIBLE);
			  ((EditText)findViewById(R.id.age_otr)).setText(otheryear[1]);			   
		}
		else
		{
			 int spinnerPosition = ageadap.getPosition(cf.YearPH);
			   agespin.setSelection(spinnerPosition);		  
			 ((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.GONE);
		}		
	}
	class textwatcher implements TextWatcher
    {
	         public int type;
	         textwatcher(int type)
	    	{
	    		this.type=type;
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {
System.out.println("dd"+this.type);

	    		// TODO Auto-generated method stub
	    		if(this.type==1)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.no_tv_type1),250); 
	    		}
	    		else if(this.type==2)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.hvac_tv_type1),500); 
	    		}
	    		else if(this.type==3)
	    		{
	    			 if(!s.toString().equals("")){
	    			     if(Character.toString(s.toString().charAt(0)).equals("0"))
		    			 {
		    				 cf.ShowToast("Invalid Entry! Please enter valid year.", 0);
		    				 ((EditText)findViewById(R.id.age_otr)).setText("");
		    				 cf.hidekeyboard();
		    			 }
	    			 }
	    		}
	    		else if(this.type==4)
	    		{
	    			 if(!s.toString().equals("")){
	    			     if(Character.toString(s.toString().charAt(0)).equals("0"))
		    			 {
		    				 cf.ShowToast("Percentage should be Greater than 0 and Less than or Equal to 100.", 0);
		    				 ((EditText)findViewById(R.id.upgd_otr)).setText("");
		    				 cf.hidekeyboard();
		    			 }
	    			 }
	    		}
	    		else if(this.type==6)
	    		{
	    			 if(!s.toString().equals("")){
	    			     if(Character.toString(s.toString().charAt(0)).equals("0"))
		    			 {
		    				 cf.ShowToast("Percentage should be Greater than 0 and Less than or Equal to 100.", 0);
		    				 ((EditText)findViewById(R.id.cupgd_otr)).setText("");
		    				 cf.hidekeyboard();
		    			 }
	    			 }
	    		}
	    		else if(this.type==5)
	    		{
	    			 if(!s.toString().equals("")){
	    			     if(Character.toString(s.toString().charAt(0)).equals("0"))
		    			 {
		    				 cf.ShowToast("Invalid Entry! Please enter valid Distance From Fuel Tank Location.", 0);
		    				 ((EditText)findViewById(R.id.distance_ettxt)).setText("");
		    				 cf.hidekeyboard();
		    			 }
	    			 }
	    		}
	    		/*else if(this.type==9)
	    		{
	    			 
	    		}*/
	}

	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,

	    			int after) {
	    		// TODO Auto-generated method stub
	     	}

	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,

	    			int count) {
	    		// TODO Auto-generated method stub
	    		System.out.println("iiit="+this.type);
	    		 if(this.type==9)
  			     {
  			    	 System.out.println("DDFDF"+s);
  			    	 if(!s.toString().equals(""))
  			    	 {
	    			     if(((EditText)findViewById(R.id.phs_otr)).getVisibility()==View.VISIBLE)
	    			     {
	    			    	 System.out.println("hhssj="+s);
	    			    	 ((EditText)findViewById(R.id.phs_otr)).setText(s);
	    			     }
  			    	 }
  			     }
	    	}
	   }	
	class checklistenetr implements OnCheckedChangeListener
	{
    	int i;
		public checklistenetr(int i) {
			// TODO Auto-generated constructor stub
		   this.i=i;
		}

		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			   RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
		        // This puts the value (true/false) into the variable
		        boolean isChecked = checkedRadioButton.isChecked();
		        // If the radiobutton that has changed in check state is now checked...
		        if (isChecked)
		        {
		          switch (i) {
					case 1:
						ftlrdgtxt= checkedRadioButton.getText().toString().trim();hack[0]=false;
						if(ftlrdgtxt.equals("Not Applicable"))
						{
							((LinearLayout)findViewById(R.id.distancelin)).setVisibility(cf.v1.GONE);
							((EditText)findViewById(R.id.distance_ettxt)).setText("");
							
							((RadioButton) ofhsprdgval.findViewWithTag("No")).setChecked(true);
							((RadioButton) phhprdgval.findViewWithTag("No")).setChecked(true);
							
							
						}
						else
						{
							openflame=true;portableheater=true;
							try{ if(openflame){ofhsprdgval.clearCheck();}}catch(Exception e){}
							try{ if(portableheater){phhprdgval.clearCheck();}}catch(Exception e){}
							((LinearLayout)findViewById(R.id.distancelin)).setVisibility(cf.v1.VISIBLE);
							((EditText)findViewById(R.id.distance_ettxt)).setText("");
						}
					  break;
					case 2:
						ofhsptxt = checkedRadioButton.getText().toString().trim();hack[1]=false;
						if(ofhsptxt.equals("Yes")){
							((LinearLayout)findViewById(R.id.showfactor)).setVisibility(cf.v1.VISIBLE);
							((LinearLayout)findViewById(R.id.showtype)).setVisibility(cf.v1.VISIBLE);
						}else{
							hack[2]=true;fpitxt="";typerdval="";
							try{ if(hack[2]){fpirdgval.clearCheck();}}catch(Exception e){}
							Rdbtn_Uncheck();
							((LinearLayout)findViewById(R.id.showfactor)).setVisibility(cf.v1.GONE);
							((LinearLayout)findViewById(R.id.showtype)).setVisibility(cf.v1.GONE);
						}
						break;
					case 3:
						fpitxt = checkedRadioButton.getText().toString().trim();hack[2]=false;
						break;
					case 4:
						phptxt = checkedRadioButton.getText().toString().trim();hack[3]=false;System.out.println("44444"+phptxt);
						if(phptxt.equals("Yes")){
							((LinearLayout)findViewById(R.id.showheater)).setVisibility(cf.v1.VISIBLE);
						}
						else
						{
							cf.Set_UncheckBox(porheatchk, ((EditText)findViewById(R.id.heat_otr)));
							((LinearLayout)findViewById(R.id.showheater)).setVisibility(cf.v1.GONE);
						}
						break;
					case 5:
						wotxt = checkedRadioButton.getText().toString().trim();hack[4]=false;
						if(wotxt.equals("Yes")){
							((LinearLayout)findViewById(R.id.nocommentslin)).setVisibility(cf.v1.GONE);
						}else{((LinearLayout)findViewById(R.id.nocommentslin)).setVisibility(cf.v1.VISIBLE);
						  ((EditText)findViewById(R.id.nocomment)).setText("");}
						break;
					case 6:
						yicrdgtxt= checkedRadioButton.getText().toString().trim();yicck=false;
						if(yicrdgtxt.equals("Unknown")){epval=1;
							((RadioButton)yicrdgval.findViewWithTag("Unknown")).setChecked(true);
							agespin1.setSelection(ageadap1.getPosition("Unknown"));
						}
						else
						{
							
								if(yicrdgtxt.equals("Estimate"))
								{
									agespin1.setSelection(0);
								}
								epval=2;
						}
						
						/*else if(yicrdgtxt.equals("Known")|| yicrdgtxt.equals("Estimate")){
							agespin.setSelection(0);
						}*/
					  break;
						default:
						break;
					}
		        }
		     }
    }	
	class Spin_Selectedlistener implements OnItemSelectedListener
	{
		int i;
		public Spin_Selectedlistener(int i) {
			// TODO Auto-generated constructor stub
			this.i=i;
		}

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			if(i==1){
			zonestr = zonespin.getSelectedItem().toString();}
			else if(i==2){
			unttypstr = unttypespin.getSelectedItem().toString();
			if(unttypstr.equals("Internal System/Unit")){
				//((LinearLayout)findViewById(R.id.intshow)).setVisibility(cf.v1.VISIBLE);
				((LinearLayout)findViewById(R.id.intshow1)).setVisibility(cf.v1.VISIBLE);
				((Spinner)findViewById(R.id.intloc_spin)).setVisibility(cf.v1.VISIBLE);
				if(spunt==0){((Spinner)findViewById(R.id.intloc_spin)).setSelection(0);
				((EditText)findViewById(R.id.dummyid)).setText("");
				((EditText)findViewById(R.id.dummyid)).setVisibility(cf.v1.INVISIBLE);defaultspin();}
				((Spinner)findViewById(R.id.loc_spin)).setVisibility(cf.v1.GONE);
				
				
			}else{
				//((LinearLayout)findViewById(R.id.intshow)).setVisibility(cf.v1.GONE);
				((LinearLayout)findViewById(R.id.intshow1)).setVisibility(cf.v1.GONE);
				((Spinner)findViewById(R.id.intloc_spin)).setVisibility(cf.v1.GONE);
				((Spinner)findViewById(R.id.loc_spin)).setVisibility(cf.v1.VISIBLE);
				if(spunt==0){((Spinner)findViewById(R.id.loc_spin)).setSelection(0);defaultspin();}
				
			}
			}else if(i==3){
				 System.out.println("inner");
			untnostr = untnospin.getSelectedItem().toString();
			 System.out.println("untnostr="+untnostr);
			}else if(i==4){
				agestr = agespin.getSelectedItem().toString();
				if(agestr.equals("Other"))
				{
					((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.VISIBLE);
					cf.setFocus(((EditText)findViewById(R.id.age_otr)));
				}
				else
				{
					((EditText)findViewById(R.id.age_otr)).setText("");
					((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.INVISIBLE);
				}
			}else if(i==5){
				upgdstr = upgdspin.getSelectedItem().toString();
				if(upgdstr.equals("Yes"))
				{
					((EditText)findViewById(R.id.upgd_otr)).setVisibility(cf.v1.VISIBLE);
					 if(edt!=1)
					 {
						  ((EditText)findViewById(R.id.upgd_otr)).setText("100");
					 }
					cf.setFocus(((EditText)findViewById(R.id.upgd_otr)));
					((TextView)findViewById(R.id.upgdpertxt)).setVisibility(cf.v1.VISIBLE);
				}
				else
				{
					((EditText)findViewById(R.id.upgd_otr)).setText("");
					((EditText)findViewById(R.id.upgd_otr)).setVisibility(cf.v1.INVISIBLE);
				    ((TextView)findViewById(R.id.upgdpertxt)).setVisibility(cf.v1.INVISIBLE);
				}
			}else if(i==13){
				cupgdstr = cupgdspin.getSelectedItem().toString();System.out.println("cupgdstr"+cupgdstr);
				if(cupgdstr.equals("--Select--"))
				{
					 cupgdspin.setSelection(cupgdadap.getPosition("N/A"));	
				}
				else
				{
					if(cupgdstr.equals("Yes"))
					{
						((EditText)findViewById(R.id.cupgd_otr)).setVisibility(cf.v1.VISIBLE);
						if(edt!=1)
						{
						  ((EditText)findViewById(R.id.cupgd_otr)).setText("100");
						}
						cf.setFocus(((EditText)findViewById(R.id.cupgd_otr)));
						((TextView)findViewById(R.id.cupgdpertxt)).setVisibility(cf.v1.VISIBLE);
					}
					else
					{
						((EditText)findViewById(R.id.cupgd_otr)).setText("");
						((EditText)findViewById(R.id.cupgd_otr)).setVisibility(cf.v1.INVISIBLE);
					    ((TextView)findViewById(R.id.cupgdpertxt)).setVisibility(cf.v1.INVISIBLE);
					}
				}
			}
			else if(i==6){
				locstr = locspin.getSelectedItem().toString();
				if(locstr.equals("Other"))
				{
					((EditText)findViewById(R.id.dummyid)).setVisibility(cf.v1.VISIBLE);
					cf.setFocus(((EditText)findViewById(R.id.dummyid)));
				}
				else
				{
					((EditText)findViewById(R.id.dummyid)).setText("");
					((EditText)findViewById(R.id.dummyid)).setVisibility(cf.v1.INVISIBLE);
				}
			}else if (i==7){
				ocstr = ocspin.getSelectedItem().toString();
			}else if (i==8){
				ceilstr = ceilspin.getSelectedItem().toString();
				
			}else if (i==9){
				dripstr = dripspin.getSelectedItem().toString();
			}else if(i==10){
				locstr = intlocspin.getSelectedItem().toString();
				if(locstr.equals("Other"))
				{
					((EditText)findViewById(R.id.dummyid)).setVisibility(cf.v1.VISIBLE);
					cf.setFocus(((EditText)findViewById(R.id.dummyid)));
				}
				else
				{
					((EditText)findViewById(R.id.dummyid)).setText("");
					((EditText)findViewById(R.id.dummyid)).setVisibility(cf.v1.INVISIBLE);
				}
			}
			else if(i==11)
			{
				phsstr = phsspin.getSelectedItem().toString();System.out.println("phsstr="+phsstr);
				if(phsstr.equals("Other"))
				{
					((EditText)findViewById(R.id.phs_otr)).setVisibility(cf.v1.VISIBLE);
					cf.setFocus(((EditText)findViewById(R.id.phs_otr)));
				}
				else
				{
					((EditText)findViewById(R.id.phs_otr)).setText("");
					((EditText)findViewById(R.id.phs_otr)).setVisibility(cf.v1.INVISIBLE);
				}
			}
			else if(i==12)
			{
				hzstr = hzspin.getSelectedItem().toString();
			}
			else if(i==14){
				
				agestr1 = agespin1.getSelectedItem().toString();System.out.println("agestr="+agestr1+mpo);
				if(agestr1.equals("Other")){
					((EditText)findViewById(R.id.age_otr1)).setVisibility(cf.v1.VISIBLE);
                     if(mpo==0){cf.setFocus(((EditText)findViewById(R.id.age_otr1)));}
				}else if(agestr1.equals("Unknown")){
					((EditText)findViewById(R.id.age_otr1)).setText("");
					((EditText)findViewById(R.id.age_otr1)).setVisibility(cf.v1.GONE);
					((RadioButton)yicrdgval.findViewWithTag("Unknown")).setChecked(true);
					
				}
				else{System.out.println("spinelse");
				
				if(epval==0)
				{
						((RadioButton)yicrdgval.findViewWithTag("Original")).setChecked(true);
					
					epval=1;	
				}
				else if(epval==1)
				{	
					System.out.println("inside ssval");
					epchk=true;
					try{if(epchk){yicrdgval.clearCheck();}}catch(Exception e){}
				}
				
				
					((EditText)findViewById(R.id.age_otr1)).setText("");
					((EditText)findViewById(R.id.age_otr1)).setVisibility(cf.v1.GONE);
				   }
				
			}
			
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	public void clicker(View v)
	{
		switch(v.getId())
		{
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.img_pck:
			try
			{
				String s =((EditText)findViewById(R.id.updimg_ed).findViewWithTag("ModPath")).getText().toString();
				if(s==null)
				{
					s="";
				}
				Intent in=new Intent(HVAC.this,pickSingleimage.class);
				in.putExtra("path", s);
				in.putExtra("id", 2);
				startActivityForResult(in, pick_img_code);
			}
			catch (Exception e) {
				// TODO: handle exception
			}
			break;
		case R.id.img_clr:
			cf.ShowToast("Image cleared successfully.",0);
			((EditText)findViewById(R.id.updimg_ed)).setText("");
			((Button)findViewById(R.id.img_clr)).setVisibility(cf.v1.GONE);
			break;
		case R.id.loadcomments:
			/***Call for the comments***/
			 int len=((EditText)findViewById(R.id.hvaccomment)).getText().toString().length();
			 if(load_comment )
				{
					load_comment=false;
					int loc[] = new int[2];
					v.getLocationOnScreen(loc);
					Intent in1 = cf.loadComments("Overall HVAC Comments",loc);
					in1.putExtra("length", len);
					in1.putExtra("max_length", 500);
					startActivityForResult(in1, cf.loadcomment_code);
				}
			
			 break;
		case R.id.shwhvac:
			if(!((CheckBox)findViewById(R.id.hvac_na)).isChecked()){
				((CheckBox)findViewById(R.id.hvac_na)).setChecked(false);
				//((TableLayout)findViewById(R.id.fourhvac_table)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdhvac)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.shwhvac)).setVisibility(cf.v1.GONE);
			}
			/*((TableLayout)findViewById(R.id.fourhvac_table)).setVisibility(cf.v1.VISIBLE);
			cf.layoutfocus((TableLayout)findViewById(R.id.fourhvac_table));*/
			break;
		case R.id.hdhvac:
			((ImageView)findViewById(R.id.shwhvac)).setVisibility(cf.v1.VISIBLE);
			((ImageView)findViewById(R.id.hdhvac)).setVisibility(cf.v1.GONE);
			//((TableLayout)findViewById(R.id.fourhvac_table)).setVisibility(cf.v1.GONE);
			break;
		case R.id.hvac_na:/** HVAC NOT APPLICABLE CHECKBOX **/
			 if(((CheckBox)findViewById(R.id.hvac_na)).isChecked())
			 {
				 if(hv_sve){chk_values();}
				 hvacchkval="1";
				  if(!rethsrdtxt.equals("")){
	 				 AlertDialog.Builder builder = new AlertDialog.Builder(HVAC.this);
		     			builder.setTitle("Confirmation");
		     			builder.setMessage(Html.fromHtml("Do you want to clear the HVAC data?"));
		 				builder.setPositiveButton("Yes",
		 								new DialogInterface.OnClickListener() {
	
		 									public void onClick(DialogInterface dialog,
		 											int id) {
		 										((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.INVISIBLE);
		 										clearhvac();hvacchkval="1";
		 										//HVAC_InsertUpdate();
								    			((TableLayout)findViewById(R.id.fourhvac_table)).setVisibility(cf.v1.GONE);
								    			//((RelativeLayout)findViewById(R.id.hvacrelchk)).setEnabled(false);
								    			/*((ImageView)findViewById(R.id.shwhvac)).setVisibility(cf.v1.VISIBLE);
								    			 ((ImageView)findViewById(R.id.hdhvac)).setVisibility(cf.v1.GONE);
								    			 ((ImageView)findViewById(R.id.shwhvac)).setEnabled(false);
								    			*/
								    			
		 									}
		 								});
		 				builder.setNegativeButton("No",
		 								new DialogInterface.OnClickListener() {

		 									public void onClick(DialogInterface dialog,
		 											int id) {
		 										((CheckBox)findViewById(R.id.hvac_na)).setChecked(false);
		 										/*((ImageView)findViewById(R.id.shwhvac)).setVisibility(cf.v1.GONE);
								    			((ImageView)findViewById(R.id.hdhvac)).setVisibility(cf.v1.VISIBLE);
								    			*///((TableLayout)findViewById(R.id.fourhvac_table)).setVisibility(cf.v1.VISIBLE);
		 										dialog.cancel();
		 									}
		 								});
	 				AlertDialog al=builder.create();
	 				al.setIcon(R.drawable.alertmsg);
	 				al.setCancelable(false);
	 				al.show();
	 			  }else{
	 				 
	 				 clearhvac();hvacchkval="1";
	 				/* chk_values();
	 				 if(chkhvac_save.getCount()==0)
	 				 {
		 				try
		 				{
		 					cf.arr_db.execSQL("INSERT INTO "
		 							+ cf.Four_HVAC
		 							+ " (fld_srid,fld_inchvac,heatsource,noofzones,tankloc,distance,openflame,factor,type,typeotr,"+
		 							"porheater,poroption,working,wrkcomments,hvaccomments )"
		 							+ "VALUES('"+cf.selectedhomeid+"','1','','',"+
		 							"'','',"+
		 							"'','','',"+
		 							"'',"+
		 							"'','','',"+
		 							"'',"+
		 							"'')");
		 				
		 			 	}
		 				catch (Exception E)
		 				{
		 					String strerrorlog="Inserting the HVAC  - FourPoint";
		 					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		 				}
	 				 }
	 				 else
	 				 {
	 					cf.arr_db.execSQL("UPDATE "+cf.Four_HVAC+ " set fld_inchvac='1' where fld_srid='"+cf.selectedhomeid+"'");
	 				 }*/
	 				// ((RelativeLayout)findViewById(R.id.hvacrelchk)).setEnabled(false);
 			    	 ((TableLayout)findViewById(R.id.fourhvac_table)).setVisibility(cf.v1.GONE);
		        	// ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
		        	/* ((ImageView)findViewById(R.id.shwhvac)).setVisibility(cf.v1.VISIBLE);
	 			    	((ImageView)findViewById(R.id.hdhvac)).setVisibility(cf.v1.GONE);
	 			    	((ImageView)findViewById(R.id.shwhvac)).setEnabled(false);*/
				    }
			 }
			 else
			 {
				/*try
					{
					    cf.arr_db.execSQL("UPDATE "+cf.Four_HVAC+ " set fld_inchvac='"+0+"' where fld_srid='"+cf.selectedhomeid+"'");
						 
					}
					catch (Exception E)
					{
						String strerrorlog="Updating the HVAC - FourPoint";
						cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
					}*/
				// ((RelativeLayout)findViewById(R.id.hvacrelchk)).setEnabled(true);
				 hvacchkval="0";clearhvac();
				 ((TableLayout)findViewById(R.id.fourhvac_table)).setVisibility(cf.v1.VISIBLE);
				 ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.INVISIBLE);
				/* ((ImageView)findViewById(R.id.shwhvac)).setVisibility(cf.v1.GONE);
			     ((ImageView)findViewById(R.id.hdhvac)).setVisibility(cf.v1.VISIBLE);
			     ((ImageView)findViewById(R.id.shwhvac)).setEnabled(true);*/
			 }
			break;
		case R.id.save:
			
			//if(((LinearLayout)findViewById(R.id.fourhvac_table)).isShown())
			if(!((CheckBox)findViewById(R.id.hvac_na)).isChecked())
			{
				 heatchk_vlaue= cf.getselected_chk(heatchk);
				 String chk_valueotr=(heatchk[heatchk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.typ_otr)).getText().toString():""; // append the other text value in to the selected option
		    	 heatchk_vlaue+=chk_valueotr;
		    	 if(heatchk_vlaue.equals(""))
				 {
					 cf.ShowToast("Please select Primary Heat Source under HVAC." , 0);
				 }
		    	 else
		    	 {
		    		 if(heatchk[heatchk.length-1].isChecked())
					 {
						 if(chk_valueotr.trim().equals("&#40;"))
						 {
							 cf.ShowToast("Please enter the Other text for Primary Heat Source.", 0);
							 cf.setFocus(((EditText)findViewById(R.id.typ_otr)));
						 }
						 else
						 {
							 ageofmailpanel();	
						 }
					 }
					 else
					 {
						 ageofmailpanel();
					 }
		    	 }
			}
			else
			{
				clear();
				comments();
			}
			break;
		case R.id.addunit:
			if(unttypstr.equals("--Select--")){
				cf.ShowToast("Please select Unit Type.", 0);
			}else{
				if(untnostr.equals("--Select--")){
					cf.ShowToast("Please select Zone.", 0);
				}else{
					if(agestr.equals("--Select--")){
						cf.ShowToast("Please select Year.", 0);
					}else{
						if(agestr.equals("Other")){
							if(((EditText)findViewById(R.id.age_otr)).getText().toString().trim().equals("")){
								cf.ShowToast("Please enter the other text under Year.", 0);
								cf.setFocus(((EditText)findViewById(R.id.age_otr)));
							}else{
								if(((EditText)findViewById(R.id.age_otr)).getText().toString().length()<4){
									cf.ShowToast("Please enter the valid year.", 0);
								}else{
									if(cf.yearValidation(((EditText)findViewById(R.id.age_otr)).getText().toString()).equals("true"))
									{
										cf.ShowToast("Year should not exceed the current year.", 0);
										cf.setFocus(((EditText)findViewById(R.id.age_otr)));
									}
									else if(cf.yearValidation(((EditText)findViewById(R.id.age_otr)).getText().toString()).equals("zero"))
									{
										cf.ShowToast("Please enter the valid year.", 0);
										cf.setFocus(((EditText)findViewById(R.id.age_otr)));
									}
									else{
									    partupgraded();
									}
								}
							}
						}else{
							partupgraded();
					  }
				   }
				}
			}
		  break;
		case R.id.unitclear:
			unttypespin.setSelection(0);untnospin.setEnabled(true);untnospin.setSelection(1);
			//agespin.setSelection(ageadap.getPosition("2012"));
			 cf.getPolicyholderInformation(cf.selectedhomeid);
			   int spinnerPosition = ageadap.getPosition(cf.YearPH);
			   agespin.setSelection(spinnerPosition);
	        upgdspin.setSelection(0);cupgdspin.setSelection(0);locspin.setSelection(0);ocspin.setSelection(0);locspin.setVisibility(cf.v1.VISIBLE);
	        ceilspin.setSelection(0);dripspin.setSelection(0);unttypespin.setEnabled(true);
	        phsspin.setSelection(0);((EditText)findViewById(R.id.phs_otr)).setVisibility(cf.v1.INVISIBLE);
		     //hzspin.setSelection(1);
		     ((EditText)findViewById(R.id.phs_otr)).setText("");
	        ((EditText)findViewById(R.id.age_otr)).setText("");((EditText)findViewById(R.id.upgd_otr)).setText("");
	        ((EditText)findViewById(R.id.cupgd_otr)).setText("");
		    ((EditText)findViewById(R.id.dummyid)).setText("");((Spinner)findViewById(R.id.intloc_spin)).setVisibility(cf.v1.GONE);
		    ((EditText)findViewById(R.id.updimg_ed)).setText("");
			((Button)findViewById(R.id.img_clr)).setVisibility(cf.v1.INVISIBLE);
	        ((Button)findViewById(R.id.addunit)).setText("Save/Add More Unit");spunt=0;edt=0;((EditText)findViewById(R.id.dummyid)).setVisibility(cf.v1.INVISIBLE);
			break;
		
		 case R.id.typ_chk10:/** TYPE CHECKBOX OTHER **/
			  if(heatchk[heatchk.length-1].isChecked())
			  { 
				  ((EditText)findViewById(R.id.typ_otr)).setVisibility(v.VISIBLE);
                   cf.setFocus(((EditText)findViewById(R.id.typ_otr)));
			  }
			  else
			  {
				  ((EditText)findViewById(R.id.typ_otr)).setVisibility(v.GONE);
				  ((EditText)findViewById(R.id.typ_otr)).setText("");
			  }
			  break;
			case R.id.type_rd1:
				Rdbtn_Uncheck();((RadioButton)findViewById(R.id.type_rd1)).setChecked(true);
				typerdval=cf.retrieve_radioname(((RadioButton)findViewById(R.id.type_rd1)));
				break;
			case R.id.type_rd2:
				Rdbtn_Uncheck();((RadioButton)findViewById(R.id.type_rd2)).setChecked(true);
				typerdval=cf.retrieve_radioname(((RadioButton)findViewById(R.id.type_rd2)));
				break;
			case R.id.type_rd3:
				Rdbtn_Uncheck();((RadioButton)findViewById(R.id.type_rd3)).setChecked(true);
				typerdval=cf.retrieve_radioname(((RadioButton)findViewById(R.id.type_rd3)));
				break;
			case R.id.type_rd4:
				Rdbtn_Uncheck();((RadioButton)findViewById(R.id.type_rd4)).setChecked(true);
				typerdval=cf.retrieve_radioname(((RadioButton)findViewById(R.id.type_rd4)));
				break;
			case R.id.type_rd5:
				Rdbtn_Uncheck();((RadioButton)findViewById(R.id.type_rd5)).setChecked(true);
				typerdval=cf.retrieve_radioname(((RadioButton)findViewById(R.id.type_rd5)));
				break;
			case R.id.type_rd6:
				Rdbtn_Uncheck();((RadioButton)findViewById(R.id.type_rd6)).setChecked(true);
				typerdval=cf.retrieve_radioname(((RadioButton)findViewById(R.id.type_rd6)));
				break;
			case R.id.type_rd7:
				Rdbtn_Uncheck();((RadioButton)findViewById(R.id.type_rd7)).setChecked(true);
				typerdval=cf.retrieve_radioname(((RadioButton)findViewById(R.id.type_rd7)));
				break;
			case R.id.type_rd8:
				Rdbtn_Uncheck();((RadioButton)findViewById(R.id.type_rd8)).setChecked(true);
				typerdval=cf.retrieve_radioname(((RadioButton)findViewById(R.id.type_rd8)));
				break;
			case R.id.type_rd9:
				Rdbtn_Uncheck();((RadioButton)findViewById(R.id.type_rd9)).setChecked(true);
				typerdval=cf.retrieve_radioname(((RadioButton)findViewById(R.id.type_rd9)));
				((EditText)findViewById(R.id.type_otr)).setVisibility(v.VISIBLE);
				 cf.setFocus(((EditText)findViewById(R.id.type_otr)));
				((EditText)findViewById(R.id.type_otr)).setText("");
				
				break;
			case R.id.ht_chk4:/** PORTABLE HEATER CHECKBOX OTHER **/
				  if(porheatchk[porheatchk.length-1].isChecked())
				  { 
					  ((EditText)findViewById(R.id.heat_otr)).setVisibility(v.VISIBLE);
					  cf.setFocus(((EditText)findViewById(R.id.heat_otr)));
				  }
				  else
				  {
					  ((EditText)findViewById(R.id.heat_otr)).setVisibility(v.GONE);
					  ((EditText)findViewById(R.id.heat_otr)).setText("");
				  }
				  break;
		}
	}
	private void ageofmailpanel()
	{
		if(agestr1.equals("--Select--")){
			cf.ShowToast("Please select Age of Main Panel", 0);
		}else{
			if(agestr1.equals("Other")){
				if(((EditText)findViewById(R.id.age_otr1)).getText().toString().trim().equals("")){
					cf.ShowToast("Please enter the Year under Age of Main Panel", 0);
					 cf.setFocus(((EditText)findViewById(R.id.age_otr1)));
				}else{
					if(((EditText)findViewById(R.id.age_otr1)).getText().toString().length()<4){
						cf.ShowToast("Please enter the valid year under Age of Main Panel", 0);
						 cf.setFocus(((EditText)findViewById(R.id.age_otr1)));
					}else{
						if(cf.yearValidation(((EditText)findViewById(R.id.age_otr1)).getText().toString()).equals("true"))
						{
							cf.ShowToast("Year should not exceed the current year under Age of Main Panel", 0);
							cf.setFocus(((EditText)findViewById(R.id.age_otr1)));
						}
						else if(cf.yearValidation(((EditText)findViewById(R.id.age_otr1)).getText().toString()).equals("zero"))
						{
							cf.ShowToast("Please enter the valid year under Age of Main Panel", 0);
							cf.setFocus(((EditText)findViewById(R.id.age_otr1)));
						}
						else
						{
								YIC();
						}
						
					}
				}
			}else{
				YIC();
		  }
		}
	}
	private void YIC() {
		// TODO Auto-generated method stub
		 if(yicrdgtxt.equals("")){
			 cf.ShowToast("Please select the option for Year of Installed Confidence" , 0);
		 }
		 else
		 {
			 noofzones();
		 }
	}
	protected void clearhvac() {
		// TODO Auto-generated method stub
		((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.INVISIBLE);
		 hvacchkval="";hack[0]=true;hack[1]=true;hack[2]=true;hack[3]=true;hack[4]=true;
		 cf.Set_UncheckBox(heatchk, ((EditText)findViewById(R.id.typ_otr)));
		((EditText)findViewById(R.id.distance_ettxt)).setText("");
		zonespin.setSelection(0);
			 try{
				 if(hack[0]){ftlrdgval.clearCheck();}}catch(Exception e){}
				 try{if(hack[1]){ofhsprdgval.clearCheck();}}catch(Exception e){}
				 try{ if(hack[2]){fpirdgval.clearCheck();}}catch(Exception e){}
				 try{ if(hack[3]){phhprdgval.clearCheck();}}catch(Exception e){}
				 cf.Set_UncheckBox(porheatchk, ((EditText)findViewById(R.id.heat_otr)));
				 Rdbtn_Uncheck();
				((LinearLayout)findViewById(R.id.showheater)).setVisibility(cf.v1.GONE);
				((LinearLayout)findViewById(R.id.showfactor)).setVisibility(cf.v1.GONE);
				((LinearLayout)findViewById(R.id.showtype)).setVisibility(cf.v1.GONE);
			 try{if(hack[4]){wordgval.clearCheck();}  }catch(Exception e){}
			 cf.arr_db.execSQL("UPDATE  "+cf.Four_HVAC_Unit+" SET fld_flag='1' Where fld_srid='"+cf.selectedhomeid+"' ");

			//cf.arr_db.execSQL("UPDATE from "+cf.Four_HVAC_Unit+" Where fld_srid='"+cf.selectedhomeid+"'");
			ShowUnitvalue();spunt=0;rethsrdtxt="";
			clear();defaulthvac();
    			
	}
	public void defaultspin() {
		// TODO Auto-generated method stub
		((Spinner)findViewById(R.id.untno_spin)).setSelection(1);
		 cf.getPolicyholderInformation(cf.selectedhomeid);
		   int spinnerPosition = ageadap.getPosition(cf.YearPH);
		   agespin.setSelection(spinnerPosition);
		//((Spinner)findViewById(R.id.age_spin)).setSelection(ageadap.getPosition("2012"));
		((EditText)findViewById(R.id.age_otr)).setText("");((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.INVISIBLE);
		((Spinner)findViewById(R.id.upg_spin)).setSelection(0);
		((EditText)findViewById(R.id.upgd_otr)).setText("");((EditText)findViewById(R.id.upgd_otr)).setVisibility(cf.v1.INVISIBLE);
		((Spinner)findViewById(R.id.cupg_spin)).setSelection(0);
		((EditText)findViewById(R.id.cupgd_otr)).setText("");((EditText)findViewById(R.id.cupgd_otr)).setVisibility(cf.v1.INVISIBLE);
		((Spinner)findViewById(R.id.oc_spin)).setSelection(0);
	}
	private void clear() {
		// TODO Auto-generated method stub
		heatchk_vlaue=""; zonestr="";ftlrdgtxt="";
		((EditText)findViewById(R.id.distance_ettxt)).setText("");
        ofhsptxt="";fpitxt="";typerdval="";((EditText)findViewById(R.id.type_otr)).setText("");
        phptxt="";porheatchk_vlaue="";((EditText)findViewById(R.id.heat_otr)).setText("");wotxt="";
        ((EditText)findViewById(R.id.nocomment)).setText("");
     	
	}
	private void partupgraded() {
		// TODO Auto-generated method stub
		if(upgdstr.equals("--Select--")){
			System.out.println("sdfasf");
			cf.ShowToast("Please select Coverage.", 0);
		}else{
			if(upgdstr.equals("Yes")){
				if(((EditText)findViewById(R.id.upgd_otr)).getText().toString().trim().equals("")){
					cf.ShowToast("Please enter the percentage for Coverage.", 0);
					cf.setFocus(((EditText)findViewById(R.id.upgd_otr)));
				}else{
					upgdyesstr = ((EditText)findViewById(R.id.upgd_otr)).getText().toString();
					
					if(Integer.parseInt(upgdyesstr)<=0 || Integer.parseInt(upgdyesstr)>100){
						cf.ShowToast("Percentage should be Greater than 0 and Less than or Equal to 100.", 0);
						 cf.setFocus(((EditText)findViewById(R.id.upgd_otr)));
					}else{
						completelyupgraded();
					}
					
				}
			}else{completelyupgraded();
			}
				
		}
	}
	
	
	private void completelyupgraded() {
		// TODO Auto-generated method stub
		
			if(cupgdspin.getSelectedItem().toString().equals("--Select--")){
				System.out.println("sdfasf");
				cf.ShowToast("Please select Upgraded.", 0);
			}else{
				if(cupgdspin.getSelectedItem().toString().equals("Yes")){
					if(((EditText)findViewById(R.id.cupgd_otr)).getText().toString().trim().equals("")){
						cf.ShowToast("Please enter the percentage for Upgraded.", 0);
						cf.setFocus(((EditText)findViewById(R.id.cupgd_otr)));
					}else{
						cupgdyesstr = ((EditText)findViewById(R.id.cupgd_otr)).getText().toString();
						
						if(Integer.parseInt(cupgdyesstr)<=0 || Integer.parseInt(cupgdyesstr)>100){
							cf.ShowToast("Percentage should be Greater than 0 and Less than or Equal to 100.", 0);
							 cf.setFocus(((EditText)findViewById(R.id.cupgd_otr)));
						}else{
							//hsystem();
							location();
						}
						
					}
				}else{
					location();
					//hsystem();
				}
					
			}
			
		
	}
	
	private void hsystem() {
		// TODO Auto-generated method stub
		
		
		if(phsstr.equals("--Select--")){
			cf.ShowToast("Please select Primary Heat Source.", 0);
		}
		else
		{
			if(phsstr.equals("Other"))
			{
				if(((EditText)findViewById(R.id.phs_otr)).getText().toString().equals(""))
				{
					cf.ShowToast("Please select the Other text for Primary Heat Source.", 0);
				}
				else
				{
					locationaboveceiing();
					//hzones();
				}
			}
			else
			{
				//hzones();
				locationaboveceiing();
			}
		}
	}
	
	private void location() {
		// TODO Auto-generated method stub
		if(locstr.equals("--Select--"))
		{
			cf.ShowToast("Please select Location.", 0);
		}
		else
		{
			//if(unttypstr.equals("Internal System/Unit")){
				if(locstr.equals("Other"))
				{
					if(((EditText)findViewById(R.id.dummyid)).getText().toString().trim().equals(""))
					{
						cf.ShowToast("Please enter the other text under Location.", 0);
						cf.setFocus(((EditText)findViewById(R.id.dummyid)));
					}
					else{
						overallCondition();
					}
				}else{
					overallCondition();
				}
			/*}
			else
			{
				overallCondition();
			
			}*/
	    }   
	}
	
	private void overallCondition() {
		// TODO Auto-generated method stub
		/*if(unttypstr.equals("Internal System/Unit"))
		{*/
			if(ocstr.equals("--Select--"))
			{
				cf.ShowToast("Please select Overall Condition.", 0);
			}
			else
			{
				hsystem();
				
			}
		/*}
		else
		{
			drip();
		}*/
	}
	/*private void locationaboveceiing() {
		// TODO Auto-generated method stub
		if(unttypstr.equals("Internal System/Unit"))
		{
			if(ocstr.equals("--Select--"))
			{
				cf.ShowToast("Please select Overall Condition.", 0);
			}
			else
			{
				if(unttypstr.equals("Internal System/Unit"))
				{
					if(ceilstr.equals("--Select--"))
					{
						cf.ShowToast("Please select Located Above Finished Ceiling.", 0);
					}
					else
					{
						drip();
					}
				}
				else
				{
					//Insert_Unit();
					drip();
				}
			}
		}
		else
		{
			drip();
		}
	}*/
		
		private void locationaboveceiing()
		{
			System.out.println("ceilst="+ceilstr);
			if(unttypstr.equals("Internal System/Unit"))
			{
				if(ceilstr.equals("--Select--"))
				{
					cf.ShowToast("Please select Located Above Finished Ceiling.", 0);
				}
				else
				{
					drip();
				}
			}
			else
			{
				//Insert_Unit();
				drip();
			}
		}
	/*private void overallCondition() {
		// TODO Auto-generated method stub
		if(unttypstr.equals("Internal System/Unit"))
		{
			if(ocstr.equals("--Select--"))
			{
				cf.ShowToast("Please select Overall Condition.", 0);
			}
			else
			{
				if(unttypstr.equals("Internal System/Unit"))
				{
					if(ceilstr.equals("--Select--"))
					{
						cf.ShowToast("Please select Located Above Finished Ceiling.", 0);
					}
					else
					{
						drip();
					}
				}
				else
				{
					//Insert_Unit();
					drip();
				}
			}
		}
		else
		{
			drip();
		}
	}*/
	private void next() {
		// TODO Auto-generated method stub
		if(((EditText)findViewById(R.id.updimg_ed)).getText().toString().trim().equals(""))
		{
			cf.ShowToast("Please upload Photos.",0);
			cf.setFocus(((EditText)findViewById(R.id.updimg_ed)));
		}
		else
		{
				Insert_Unit();			
		}
	}
	private void drip() {
		// TODO Auto-generated method stub
		System.out.println("cdripst="+dripstr);
		if(unttypstr.equals("Internal System/Unit"))
		{
			if(dripstr.equals("--Select--")){
				cf.ShowToast("Please select Drip Pan/Overflow Provided.", 0);
			}
			else
			{
				/*if(hzspin.getSelectedItem().toString().equals("--Select--"))
				{
					cf.ShowToast("Please select Total No of Zones.", 0);
				}
				else
				{*/
				next();	
				//}
			}
		}
		else
		{
			next();
		}
	}
	private void Insert_Unit() {
		// TODO Auto-generated method stub
		 try
		 {
			 if(((Button)findViewById(R.id.addunit)).getText().toString().equals("Update")){
			    cf.arr_db.execSQL("UPDATE  "+cf.Four_HVAC_Unit+" SET unttype='"+unttypstr+"',"+
			           "untnum='"+untnostr+"',approxage='"+agestr+"',"+
			    	   "approxageother='"+cf.encode(((EditText)findViewById(R.id.age_otr)).getText().toString())+"',"+
			           "upgraded='"+upgdstr+"',"+
			           "upgradedper='"+cf.encode(((EditText)findViewById(R.id.upgd_otr)).getText().toString())+"',"+
			           "hsystem='"+phsstr+"',"+
			           "hsystemotr='"+cf.encode(((EditText)findViewById(R.id.phs_otr)).getText().toString())+"',"+
			           "zone='"+hzstr+"',"+
			           "location='"+locstr+"',overallcond='"+ocstr+"',ceiling='"+ceilstr+"',"+
					   "ceilingother='"+cf.encode(((EditText)findViewById(R.id.dummyid)).getText().toString())+"',"+
			           "dripplan='"+dripstr+"',"+
					   "appimg='"+cf.encode(((EditText)findViewById(R.id.updimg_ed)).getText().toString())+"',"+
					   "cupgraded='"+cupgdstr+"',"+
			           "cupgradedper='"+cf.encode(((EditText)findViewById(R.id.cupgd_otr)).getText().toString())+"' Where huntId='"+unitCurrent_select_id+"' and fld_srid='"+cf.selectedhomeid+"' ");
			    unttypespin.setSelection(0);untnospin.setEnabled(true);untnospin.setSelection(1);
			    //agespin.setSelection(ageadap.getPosition("2012"));
			    cf.getPolicyholderInformation(cf.selectedhomeid);
				   int spinnerPosition = ageadap.getPosition(cf.YearPH);
				   agespin.setSelection(spinnerPosition);
		        upgdspin.setSelection(0);cupgdspin.setSelection(0);locspin.setSelection(0);ocspin.setSelection(0);phsspin.setSelection(0);
		        //hzspin.setSelection(1);
		        ((EditText)findViewById(R.id.phs_otr)).setText("");
		        ((EditText)findViewById(R.id.updimg_ed)).setText("");
				((Button)findViewById(R.id.img_clr)).setVisibility(cf.v1.INVISIBLE);
		        ceilspin.setSelection(0);dripspin.setSelection(0);unttypespin.setEnabled(true);
		        ((EditText)findViewById(R.id.age_otr)).setText("");((EditText)findViewById(R.id.upgd_otr)).setText("");
		        ((EditText)findViewById(R.id.cupgd_otr)).setText("");
			    ((EditText)findViewById(R.id.dummyid)).setText("");((EditText)findViewById(R.id.dummyid)).setVisibility(cf.v1.INVISIBLE);
		        ((Button)findViewById(R.id.addunit)).setText("Save/Add More Unit");
		        ((EditText)findViewById(R.id.phs_otr)).setVisibility(cf.v1.INVISIBLE);
		       ((Spinner)findViewById(R.id.intloc_spin)).setVisibility(cf.v1.GONE);
		        locspin.setVisibility(cf.v1.VISIBLE);
				spunt=0;edt=0;
				ShowUnitvalue();cf.ShowToast("Unit updated successfully.", 0);
			}else{
				chk_values();
				boolean[] b = {false};
				/*if(chkhvacunit_save.getCount()==0){
					
				}else{
				if(unit_in_DB!=null)
				{
					for(int i=0;i<unit_in_DB.length;i++)
					{
						if((unit_in_DB[i].equals(untnostr))&& unittype_in_DB[i].equals(unttypstr))
						{
							b[0]=true;
						}
					}
				}
				}*/
				if(hvacunit_save.getCount()==5){
				   unttypespin.setSelection(0);untnospin.setSelection(1);//agespin.setSelection(ageadap.getPosition("2012"));
				   cf.getPolicyholderInformation(cf.selectedhomeid);
				   int spinnerPosition = ageadap.getPosition(cf.YearPH);
				   agespin.setSelection(spinnerPosition);
			       upgdspin.setSelection(0);cupgdspin.setSelection(0);locspin.setSelection(0);ocspin.setSelection(0);
			       phsspin.setSelection(0);((EditText)findViewById(R.id.phs_otr)).setVisibility(cf.v1.INVISIBLE);
			        //hzspin.setSelection(1);
			        ((EditText)findViewById(R.id.phs_otr)).setText("");
			        ((EditText)findViewById(R.id.updimg_ed)).setText("");
					((Button)findViewById(R.id.img_clr)).setVisibility(cf.v1.INVISIBLE);
			        ceilspin.setSelection(0);dripspin.setSelection(0);
			        ((Spinner)findViewById(R.id.intloc_spin)).setVisibility(cf.v1.GONE);
			        locspin.setVisibility(cf.v1.VISIBLE);
			       ((EditText)findViewById(R.id.age_otr)).setText("");((EditText)findViewById(R.id.upgd_otr)).setText("");
			       ((EditText)findViewById(R.id.cupgd_otr)).setText("");
			       ((EditText)findViewById(R.id.dummyid)).setText("");((EditText)findViewById(R.id.dummyid)).setVisibility(cf.v1.INVISIBLE);
				   cf.ShowToast("Limit Exceeds! You have already added 5 "+unttypstr+".", 0);
				}else{
					if(!b[0]){ 
					cf.arr_db.execSQL("INSERT INTO "
							+ cf.Four_HVAC_Unit
							+ " (fld_srid,fld_flag,unttype,untnum,approxage,approxageother,upgraded,upgradedper,"+
							"hsystem,hsystemotr,zone,location,overallcond,ceiling,ceilingother,dripplan,appimg,cupgraded,cupgradedper)"
							+ "VALUES('"+cf.selectedhomeid+"','0','"+unttypstr+"','"+untnostr+"','"+agestr+"',"+
							"'"+cf.encode(((EditText)findViewById(R.id.age_otr)).getText().toString())+"',"+
							"'"+upgdstr+"','"+cf.encode(((EditText)findViewById(R.id.upgd_otr)).getText().toString())+"',"+
							"'"+phsstr+"','"+cf.encode(((EditText)findViewById(R.id.phs_otr)).getText().toString())+"',"+
							"'"+hzstr+"','"+locstr+"','"+ocstr+"','"+ceilstr+"','"+cf.encode(((EditText)findViewById(R.id.dummyid)).getText().toString())+"',"+
							"'"+dripstr+"','"+cf.encode(((EditText)findViewById(R.id.updimg_ed)).getText().toString())+"',"+
							"'"+cupgdstr+"','"+cf.encode(((EditText)findViewById(R.id.cupgd_otr)).getText().toString())+"')");
					 unttypespin.setSelection(0);untnospin.setSelection(1);//agespin.setSelection(ageadap.getPosition("2012"));
					 cf.getPolicyholderInformation(cf.selectedhomeid);
					   int spinnerPosition = ageadap.getPosition(cf.YearPH);
					   agespin.setSelection(spinnerPosition);
			         upgdspin.setSelection(0);cupgdspin.setSelection(0);locspin.setSelection(0);ocspin.setSelection(0);
			         phsspin.setSelection(0);((EditText)findViewById(R.id.phs_otr)).setVisibility(cf.v1.INVISIBLE);
				     //hzspin.setSelection(1);
				     ((EditText)findViewById(R.id.phs_otr)).setText("");
				     ((EditText)findViewById(R.id.updimg_ed)).setText("");
					((Button)findViewById(R.id.img_clr)).setVisibility(cf.v1.INVISIBLE);
			         ceilspin.setSelection(0);dripspin.setSelection(0);
			         ((Spinner)findViewById(R.id.intloc_spin)).setVisibility(cf.v1.GONE);
			         locspin.setVisibility(cf.v1.VISIBLE);
			         ((EditText)findViewById(R.id.age_otr)).setText("");((EditText)findViewById(R.id.upgd_otr)).setText("");
			         ((EditText)findViewById(R.id.cupgd_otr)).setText("");
				     ((EditText)findViewById(R.id.dummyid)).setText("");((EditText)findViewById(R.id.dummyid)).setVisibility(cf.v1.INVISIBLE);
				     cf.ShowToast(unttypstr+" added successfully.", 0 );
					 
					}else{cf.ShowToast("Unit already exists. Please select another unit.", 0);
					untnospin.setSelection(1);
					}
					
			        ShowUnitvalue();
					
				}
			  }
		  }
		  catch(Exception e){
		// TODO Auto-generated method stub
		
	    }
	}
	private void chk_values() {
		// TODO Auto-generated method stub
		try
		{
			hvacunit_save=cf.SelectTablefunction(cf.Four_HVAC_Unit, " where fld_srid='"+cf.selectedhomeid+"' and unttype='"+unttypstr+"'");
		   if(hvacunit_save.getCount()>0){
		   }
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		try
		{
			chkhvacunit_save=cf.SelectTablefunction(cf.Four_HVAC_Unit, " where fld_srid='"+cf.selectedhomeid+"'");
			}
		catch (Exception e) {
			// TODO: handle exception
		}
		try
		{
			chkhvac_save=cf.SelectTablefunction(cf.Four_HVAC, " where fld_srid='"+cf.selectedhomeid+"'");
			int rws = chkhvac_save.getCount();
			if(rws>0){
				chkhvac_save.moveToFirst();
				rethsrdtxt = chkhvac_save.getString(chkhvac_save.getColumnIndex("heatsource"));
				rethvacchkval = chkhvac_save.getString(chkhvac_save.getColumnIndex("fld_inchvac"));
			}
			}
		catch (Exception e) {
			// TODO: handle exception
		}
	}
	private void ShowUnitvalue() {
		// TODO Auto-generated method stub
		   Cursor unt_retrieve= cf.SelectTablefunction(cf.Four_HVAC_Unit, " Where fld_srid='"+cf.selectedhomeid+"' and fld_flag='0'");
		   System.out.println("count="+unt_retrieve.getCount());
	       if(unt_retrieve.getCount()>0)
			{
	    	   unt_retrieve.moveToFirst();
				try
				{
					unttblshow.removeAllViews();
				}
				catch (Exception e) {
					// TODO: handle exception
				}
				LinearLayout h1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvac_listview_header, null); 
				LinearLayout th = (LinearLayout)h1.findViewById(R.id.unit);th.setVisibility(cf.v1.VISIBLE);
				TableLayout.LayoutParams lp = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
			 	lp.setMargins(2, 0, 2, 2);h1.removeAllViews(); 
			 //	th.setPadding(10, 0, 0, 0);
			 	
			 	unttblshow.addView(th,lp);
				unit_in_DB=new String[unt_retrieve.getCount()];
				unittype_in_DB=new String[unt_retrieve.getCount()];
				unttblshow.setVisibility(View.VISIBLE); System.out.println("VISIBLE="+unt_retrieve.getCount());
				
				for(int i=0;i<unt_retrieve.getCount();i++)
				{
					TextView no,funttyp,funtno,fage,fupgd,fcupgd,floc,foc,fceil,fdp,fhsy,fz;
					ImageView edit,delete;
					String type="",uno="",appimg="",age="",ageotr="",upgd="",upgdyes="",loc="",oc="",ceil="",ceilotr="",drip="",
							pyhs="",pyhsotr="",zn="",cupgd="",cupgdyes="";
					type=unt_retrieve.getString(3);
					uno=unt_retrieve.getString(4);
					age=unt_retrieve.getString(5);
					ageotr=cf.decode(unt_retrieve.getString(6));
					upgd=unt_retrieve.getString(7);
					upgdyes=cf.decode(unt_retrieve.getString(8));
					pyhs = unt_retrieve.getString(9);
					pyhsotr = cf.decode(unt_retrieve.getString(10));
					zn = unt_retrieve.getString(11);
					loc=unt_retrieve.getString(12);
					oc=unt_retrieve.getString(13);
					ceil=unt_retrieve.getString(14);
					ceilotr=cf.decode(unt_retrieve.getString(15));
					drip=unt_retrieve.getString(16);
					appimg=cf.decode(unt_retrieve.getString(17));
					cupgd=unt_retrieve.getString(18);
					cupgdyes=cf.decode(unt_retrieve.getString(19));
					System.out.println("came here"+cupgd+"yes="+cupgdyes+"ceilotr"+ceilotr);
					unit_in_DB[i]=uno;unittype_in_DB[i]=type;
					
					LinearLayout t1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvac_listview, null);
					LinearLayout t = (LinearLayout)t1.findViewById(R.id.unit);t.setVisibility(cf.v1.VISIBLE);
					t.setId(44444+i);/// Set some id for further use
				 	
					no= (TextView) t.findViewWithTag("#");
				 	no.setText(String.valueOf(i+1));
				 	funttyp= (TextView) t.findViewWithTag("UType");
				 	funttyp.setText(type);
				 	funtno= (TextView) t.findViewWithTag("UNo");
				 	funtno.setText(uno);
				 	fage= (TextView) t.findViewWithTag("Age");
				 	if(age.equals("Other")){fage.setText(ageotr);}else{
				 	fage.setText(age);}
				 	fupgd= (TextView) t.findViewWithTag("UPG");
				 	if(upgd.equals("Yes")){fupgd.setText(upgd+ " ("+upgdyes+"%)");}else{
				 	fupgd.setText(upgd);}
				 	
				 	fcupgd= (TextView) t.findViewWithTag("CUPG");
				 	if(cupgd.equals("Yes")){
				 		fcupgd.setText(cupgd+ " ("+cupgdyes+"%)");
				 		}else
				 		{
				 	fcupgd.setText(cupgd);}
				 	fhsy= (TextView) t.findViewWithTag("HSYS");
				 	if(pyhs.equals("Other"))
				 	{
				 		fhsy.setText(pyhsotr);
				 	}
				 	else
				 	{
				 		fhsy.setText(pyhs);
				 	}
				 /*	fz= (TextView) t.findViewWithTag("ZN");
				 	fz.setText(zn);*/
				 	floc= (TextView) t.findViewWithTag("LOC");
					if(loc.equals("Other")){
							floc.setText(ceilotr);
						}else{
							floc.setText(loc);
						}
						
				 	foc= (TextView) t.findViewWithTag("OC");
				 	foc.setText(oc);
				 	fceil= (TextView) t.findViewWithTag("CEIL");
				 	fdp= (TextView) t.findViewWithTag("DRIP");
				 	if(type.equals("Internal System/Unit")){
					 	
					 	fceil.setText(ceil);
					 	fdp.setText(drip);
				 	}else{
				 		fceil.setText("N/A");fdp.setText("N/A");
				 	}
				 	ImageView  iv=(ImageView)t.findViewWithTag("HIMG");
					File f=new File(appimg);
					if(f.exists())
					{
						
						Bitmap b1= cf.ShrinkBitmap(appimg, 125, 125);
						if(b1!=null)
						{
							iv.setImageBitmap(b1);
							
						}
						else
						{
							iv.setImageDrawable(getResources().getDrawable(R.drawable.photonotavail));
						}
						
					}
					else
					{
						iv.setImageDrawable(getResources().getDrawable(R.drawable.photonotavail));
					}
				 	
				 	edit= (ImageView) t.findViewWithTag("edit");
				 	edit.setId(789456+i);
				 	edit.setTag(unt_retrieve.getString(0));
	                edit.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
						try
						{
						int i=Integer.parseInt(v.getTag().toString());
						
						spunt=1;edt=1;updateunit(i);
						}
						catch (Exception e) {
							// TODO: handle exception
						}
						}
	                });
						
				 	delete=(ImageView) t.findViewWithTag("del");
				 	delete.setTag(unt_retrieve.getString(0));
	                delete.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(final View v) {
							// TODO Auto-generated method stub
							
							AlertDialog.Builder builder = new AlertDialog.Builder(HVAC.this);
							builder.setMessage("Do you want to delete the selected unit?")
									.setCancelable(false)
									.setPositiveButton("Yes",
											new DialogInterface.OnClickListener() {

												public void onClick(DialogInterface dialog,
														int id) {
													
													try
													{
													int i=Integer.parseInt(v.getTag().toString());
													if(i==unitCurrent_select_id)
													{
														unitCurrent_select_id=0;
													}
													cf.arr_db.execSQL("Delete from "+cf.Four_HVAC_Unit+" Where huntId='"+i+"'");
													unttypespin.setSelection(0);untnospin.setSelection(1);//agespin.setSelection(ageadap.getPosition("2012"));
													 cf.getPolicyholderInformation(cf.selectedhomeid);
													   int spinnerPosition = ageadap.getPosition(cf.YearPH);
													   agespin.setSelection(spinnerPosition);
											        upgdspin.setSelection(0);cupgdspin.setSelection(0);locspin.setSelection(0);ocspin.setSelection(0);
											        ((Spinner)findViewById(R.id.intloc_spin)).setVisibility(cf.v1.GONE);
											        locspin.setVisibility(cf.v1.VISIBLE);
											        ceilspin.setSelection(0);dripspin.setSelection(0);untnospin.setEnabled(true);unttypespin.setEnabled(true);
											        ((Button)findViewById(R.id.addunit)).setText("Save/Add More Unit");
											        phsspin.setSelection(0);((EditText)findViewById(R.id.phs_otr)).setVisibility(cf.v1.INVISIBLE);
												     //hzspin.setSelection(1);
												     ((EditText)findViewById(R.id.phs_otr)).setText("");
												     ((EditText)findViewById(R.id.updimg_ed)).setText("");
														((Button)findViewById(R.id.img_clr)).setVisibility(cf.v1.INVISIBLE);
												     ((EditText)findViewById(R.id.age_otr)).setText("");((EditText)findViewById(R.id.upgd_otr)).setText("");
												     ((EditText)findViewById(R.id.cupgd_otr)).setText("");edt=0;
												    ((EditText)findViewById(R.id.dummyid)).setText("");((EditText)findViewById(R.id.dummyid)).setVisibility(cf.v1.INVISIBLE);
											        ShowUnitvalue();cf.ShowToast("Deleted successfully.", 0);
													
													}
													catch (Exception e) {
														// TODO: handle exception
													}
												}
											})
									.setNegativeButton("No",
											new DialogInterface.OnClickListener() {
												public void onClick(DialogInterface dialog,
														int id) {

													dialog.cancel();
												}
											});
							AlertDialog al=builder.create();
							al.setTitle("Confirmation");
							al.setIcon(R.drawable.alertmsg);
							al.setCancelable(false);
							al.show();
						
							
						}
					});
	                t1.removeAllViews();
					t.setPadding(10, 0, 0, 0);
					unttblshow.addView(t,lp);
					unt_retrieve.moveToNext();
				}
			}
	        else
			{
	        	unttblshow.setVisibility(View.GONE);
	        	((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.INVISIBLE);
	        	 chk_values();
	        	 if(chkhvac_save.getCount()>0)
	        	 {
	        		 ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE); 
	        	 }
	        }
	}
	protected void updateunit(int i) {
		// TODO Auto-generated method stub
		unitCurrent_select_id = i;untnospin.setEnabled(false);unttypespin.setEnabled(false);
		try{Cursor fhvacret=cf.SelectTablefunction(cf.Four_HVAC_Unit, " Where huntId='"+i+"'");
		if(fhvacret.getCount()>0)
		{
			fhvacret.moveToFirst();
			String type="",uno="",cupgd="",cupgdyes="",age="",appimg="",ageotr="",upgd="",upgdyes="",loc="",oc="",ceil="",ceilotr="",drip="",
					pyhs="",pyhsotr="",zn="";
			
			type=fhvacret.getString(3);
			uno=fhvacret.getString(4);
			age=fhvacret.getString(5);
			ageotr=cf.decode(fhvacret.getString(6));
			upgd=fhvacret.getString(7);
			upgdyes=cf.decode(fhvacret.getString(8));
			pyhs = fhvacret.getString(9);
			pyhsotr = cf.decode(fhvacret.getString(10));
			zn = fhvacret.getString(11);
			loc=fhvacret.getString(12);
			oc=fhvacret.getString(13);
			ceil=fhvacret.getString(14);
			ceilotr=cf.decode(fhvacret.getString(15));
			drip=fhvacret.getString(16);
			appimg=cf.decode(fhvacret.getString(17));
			cupgd=fhvacret.getString(18);
			cupgdyes=cf.decode(fhvacret.getString(19));
			
			int pos=unttypadap.getPosition(type);unttypespin.setSelection(pos);
			int untnopos=untnoadap.getPosition(uno);untnospin.setSelection(untnopos);
			int agepos=ageadap.getPosition(age);agespin.setSelection(agepos);
			if(age.equals("Other")){((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.VISIBLE);
			((EditText)findViewById(R.id.age_otr)).setText(ageotr);}
			int upgdpos=upgdadap.getPosition(upgd);upgdspin.setSelection(upgdpos);
			if(upgd.equals("Yes")){((EditText)findViewById(R.id.upgd_otr)).setVisibility(cf.v1.VISIBLE);
			((EditText)findViewById(R.id.upgd_otr)).setText(upgdyes);}
			int cupgdpos=cupgdadap.getPosition(cupgd);cupgdspin.setSelection(cupgdpos);
			if(cupgd.equals("Yes")){((EditText)findViewById(R.id.cupgd_otr)).setVisibility(cf.v1.VISIBLE);
			((EditText)findViewById(R.id.cupgd_otr)).setText(cupgdyes);}
			int phspos=phsadap.getPosition(pyhs);phsspin.setSelection(phspos);
			if(pyhs.equals("Other"))
			{
				((EditText)findViewById(R.id.phs_otr)).setText(pyhsotr);
				((EditText)findViewById(R.id.phs_otr)).setVisibility(cf.v1.VISIBLE);
			}
			//int hzpos=hzadap.getPosition(zn);hzspin.setSelection(hzpos);
			if(type.equals("Internal System/Unit")){
				locspin.setVisibility(cf.v1.GONE);
			    intlocspin.setVisibility(cf.v1.VISIBLE);
				int locpos1=intlocadap.getPosition(loc);
				intlocspin.setSelection(locpos1);
				if(loc.equals("Other")){((EditText)findViewById(R.id.dummyid)).setVisibility(cf.v1.VISIBLE);
				((EditText)findViewById(R.id.dummyid)).setText(ceilotr);}
			    }else{
			    	intlocspin.setVisibility(cf.v1.GONE);locspin.setVisibility(cf.v1.VISIBLE);
					int locpos=locadap.getPosition(loc);
					locspin.setSelection(locpos);
					if(loc.equals("Other")){((EditText)findViewById(R.id.dummyid)).setVisibility(cf.v1.VISIBLE);
					((EditText)findViewById(R.id.dummyid)).setText(ceilotr);}
					//((EditText)findViewById(R.id.dummyid)).setVisibility(cf.v1.INVISIBLE);
			}
			int ocpos=ocadap.getPosition(oc);ocspin.setSelection(ocpos);
			int ceilpos=ceiladap.getPosition(ceil);ceilspin.setSelection(ceilpos);
			
			int drippos=dripadap.getPosition(drip);dripspin.setSelection(drippos);
			((EditText)findViewById(R.id.updimg_ed)).setText(appimg);
			((Button)findViewById(R.id.img_clr)).setVisibility(cf.v1.VISIBLE);
			((Button)findViewById(R.id.addunit)).setText("Update");
		}
		}catch(Exception e){}
	}
	private void Rdbtn_Uncheck() {
		// TODO Auto-generated method stub
		((RadioButton)findViewById(R.id.type_rd1)).setChecked(false);((RadioButton)findViewById(R.id.type_rd2)).setChecked(false);
		((RadioButton)findViewById(R.id.type_rd3)).setChecked(false);((RadioButton)findViewById(R.id.type_rd4)).setChecked(false);
		((RadioButton)findViewById(R.id.type_rd5)).setChecked(false);((RadioButton)findViewById(R.id.type_rd6)).setChecked(false);
		((RadioButton)findViewById(R.id.type_rd7)).setChecked(false);((RadioButton)findViewById(R.id.type_rd8)).setChecked(false);
		((RadioButton)findViewById(R.id.type_rd9)).setChecked(false);((EditText)findViewById(R.id.type_otr)).setVisibility(cf.v1.GONE);
	}
	private void noofzones() {
		// TODO Auto-generated method stub
		if(zonestr.equals("--Select--")){
			cf.ShowToast("Please select Number of HVAC Zones.", 0);
		}else{
			zonestr = zonespin.getSelectedItem().toString();
			if(ftlrdgtxt.equals("")){
				cf.ShowToast("Please select the option for Fuel Tank Location.", 0);
			}else
			{
				if(ftlrdgtxt.equals("Above Ground")|| ftlrdgtxt.equals("Below Ground"))
				{
					if(((EditText)findViewById(R.id.distance_ettxt)).getText().toString().trim().equals(""))
					{
						  cf.ShowToast("Please enter the distance from Home - Fuel Tank.", 0);
						  cf.setFocus(((EditText)findViewById(R.id.distance_ettxt)));
					
					}
					else
					{
						openflame();
					}
				}
				else
				{
					openflame();
				}
			
			}
		}
	}
	private void openflame() {
		// TODO Auto-generated method stub
		if(ofhsptxt.equals("")){
			cf.ShowToast("Please select the option for Open Flame Heat Source Present.", 0);
		}else{
			if(ofhsptxt.equals("Yes")){
				if(fpitxt.equals("")){
					cf.ShowToast("Please select the option for Factor/Professionally installed.", 0);
				}else{
					if(typerdval.equals("")){
						cf.ShowToast("Please select the option for Appliance Type.", 0);
					}else{
						if(typerdval.equals("Other")){
							if(((EditText)findViewById(R.id.type_otr)).getText().toString().trim().equals("")){
								cf.ShowToast("Please enter the Other text under Appliance Type.", 0);
								cf.setFocus(((EditText)findViewById(R.id.type_otr)));
							}else{
								heater();
							}
						}else{
							heater();
						}
					}
				}
			}else{
				heater();
			}
		}
	}
	private void heater() {
		// TODO Auto-generated method stub
		if(phptxt.equals("")){
			cf.ShowToast("Please select Portable Heater Present.", 0);
		}else{
			if(phptxt.equals("Yes")){
				 porheatchk_vlaue= cf.getselected_chk(porheatchk);
		    	 String porchk_valueotr=(porheatchk[porheatchk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.heat_otr)).getText().toString():""; // append the other text value in to the selected option
		    	 porheatchk_vlaue+=porchk_valueotr;
		    	 if(porheatchk_vlaue.equals(""))
				 {
					 cf.ShowToast("Please select the sub options for Portable Heater Present." , 0);
				 }
		    	 else
		    	 {
		    		 if(porheatchk[porheatchk.length-1].isChecked())
					 {
						 if(porchk_valueotr.trim().equals("&#40;"))
						 {
							 cf.ShowToast("Please enter the Other text for Portable Heater Present.", 0);
							 cf.setFocus(((EditText)findViewById(R.id.heat_otr)));
						 }
						 else
						 {
							 unitValidation();
						 }
					 }
					 else
					 {
						 unitValidation();
					 }
		    	 }
			}else{
				unitValidation();
			}
		}
	}
	private void unitValidation() {
		// TODO Auto-generated method stub
		chk_values();
		if(chkhvacunit_save.getCount()==0){
			cf.ShowToast("Please add atleast one unit.", 0);
		}else{
			/*if(wotxt.equals("")){
				cf.ShowToast("Please select the option for good working order.", 0);
			}else{*/
				if(wotxt.equals("No")){
					if(((EditText)findViewById(R.id.nocomment)).getText().toString().trim().equals("")){
						cf.ShowToast("Please enter the comments under good working order.", 0);
						cf.setFocus(((EditText)findViewById(R.id.nocomment)));
					}else{
						comments();
					}
				}else{
					comments();
				}
			//}
		}
	}
	private void comments() {
		// TODO Auto-generated method stub
	/* if(et_hvaccomments.getText().toString().trim().equals("")){
		 cf.ShowToast("Please enter the Overall HVAC comments.", 0);
		cf.setFocus(et_hvaccomments);
	 }else{*/
		  HVAC_InsertUpdate();
		  
		  HVAC_setValue();
	// }
	}
	private void HVAC_InsertUpdate() {
		// TODO Auto-generated method stub
		chk_values();
		if(((CheckBox)findViewById(R.id.hvac_na)).isChecked())
		{
			hvacchkval="1";
			cf.arr_db.execSQL("Delete from "+cf.Four_HVAC_Unit+" Where fld_srid='"+cf.selectedhomeid+"'");
			
		}
		else
		{
			hvacchkval="0";
			try
			  {
				  cf.arr_db.execSQL("UPDATE  "+cf.Four_HVAC_Unit+" SET fld_flag='0' Where fld_srid='"+cf.selectedhomeid+"' ");

			  }catch (Exception e) {
				// TODO: handle exception
			}
		}
	    if(chkhvac_save.getCount()==0)
		  {
			  try
				{
					cf.arr_db.execSQL("INSERT INTO "
							+ cf.Four_HVAC
							+ " (fld_srid,fld_inchvac,heatsource,noofzones,tankloc,distance,openflame,factor,type,typeotr,"+
							"porheater,poroption,working,wrkcomments,hvaccomments,fld_ageofmainpanel,fld_ageofmainpanelother,fld_yearofinstconfdience )"
							+ "VALUES('"+cf.selectedhomeid+"','"+hvacchkval+"','"+cf.encode(heatchk_vlaue)+"','"+cf.encode(zonestr)+"',"+
							"'"+cf.encode(ftlrdgtxt)+"','"+cf.encode(((EditText)findViewById(R.id.distance_ettxt)).getText().toString())+"',"+
							"'"+cf.encode(ofhsptxt)+"','"+cf.encode(fpitxt)+"','"+cf.encode(typerdval)+"',"+
							"'"+cf.encode(((EditText)findViewById(R.id.type_otr)).getText().toString())+"',"+
							"'"+cf.encode(phptxt)+"','"+cf.encode(porheatchk_vlaue)+"','"+cf.encode(wotxt)+"',"+
							"'"+cf.encode(((EditText)findViewById(R.id.nocomment)).getText().toString())+"',"+
							"'"+cf.encode(((EditText)findViewById(R.id.hvaccomment)).getText().toString())+"','"+agestr1+"',"+
							"'"+cf.encode(((EditText)findViewById(R.id.age_otr1)).getText().toString())+"','"+cf.encode(yicrdgtxt)+"')");
					/*if(hvacchkval.equals("1")|| !heatchk_vlaue.equals(""))
					{*/
						((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
					//}
					 cf.ShowToast("HVAC system saved successfully.", 1);
					 cf.goclass(19);
					 hv_sve=true;
					
				
			 	}
				catch (Exception E)
				{
					String strerrorlog="Inserting the HVAC  - FourPoint";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
			  
				
		}
		else
		{
			System.out.println("zonestr= "+zonestr);
			try
			{
				System.out.println("UPDATE "+cf.Four_HVAC+ " set fld_inchvac='"+hvacchkval+"',heatsource='"+cf.encode(heatchk_vlaue)+"',"+
		                 "noofzones='"+cf.encode(zonestr)+"',tankloc='"+cf.encode(ftlrdgtxt)+"',"+
					     "distance='"+cf.encode(((EditText)findViewById(R.id.distance_ettxt)).getText().toString())+"',"+
		                 "openflame='"+cf.encode(ofhsptxt)+"',factor='"+cf.encode(fpitxt)+"',type='"+cf.encode(typerdval)+"',"+
					     "typeotr='"+cf.encode(((EditText)findViewById(R.id.type_otr)).getText().toString())+"',"+
						 "porheater='"+cf.encode(phptxt)+"',poroption='"+cf.encode(porheatchk_vlaue)+"',"+
					     "working='"+cf.encode(wotxt)+"',wrkcomments='"+cf.encode(((EditText)findViewById(R.id.nocomment)).getText().toString())+"',"+
						 "hvaccomments='"+cf.encode(((EditText)findViewById(R.id.hvaccomment)).getText().toString())+"'," +
						 "fld_ageofmainpanel='"+agestr1+"',fld_ageofmainpanelother='"+cf.encode(((EditText)findViewById(R.id.age_otr1)).getText().toString())+"',fld_yearofinstconfdience='"+cf.encode(yicrdgtxt)+"' where fld_srid='"+cf.selectedhomeid+"'");
				cf.arr_db.execSQL("UPDATE "+cf.Four_HVAC+ " set fld_inchvac='"+hvacchkval+"',heatsource='"+cf.encode(heatchk_vlaue)+"',"+
			                 "noofzones='"+cf.encode(zonestr)+"',tankloc='"+cf.encode(ftlrdgtxt)+"',"+
						     "distance='"+cf.encode(((EditText)findViewById(R.id.distance_ettxt)).getText().toString())+"',"+
			                 "openflame='"+cf.encode(ofhsptxt)+"',factor='"+cf.encode(fpitxt)+"',type='"+cf.encode(typerdval)+"',"+
						     "typeotr='"+cf.encode(((EditText)findViewById(R.id.type_otr)).getText().toString())+"',"+
							 "porheater='"+cf.encode(phptxt)+"',poroption='"+cf.encode(porheatchk_vlaue)+"',"+
						     "working='"+cf.encode(wotxt)+"',wrkcomments='"+cf.encode(((EditText)findViewById(R.id.nocomment)).getText().toString())+"',"+
							 "hvaccomments='"+cf.encode(((EditText)findViewById(R.id.hvaccomment)).getText().toString())+"'," +
							 "fld_ageofmainpanel='"+agestr1+"',fld_ageofmainpanelother='"+cf.encode(((EditText)findViewById(R.id.age_otr1)).getText().toString())+"',fld_yearofinstconfdience='"+cf.encode(yicrdgtxt)+"' where fld_srid='"+cf.selectedhomeid+"'");
				/*if(hvacchkval.equals("1")||heatchk_vlaue.equals(""))
				{*/
					((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);				//}
				cf.ShowToast("HVAC system saved successfully.", 1);
				cf.goclass(19);
				 hv_sve=true;
			  
			}
			catch (Exception E)
			{
				String strerrorlog="Updating the HVAC - FourPoint";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			}
		}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
 		// replaces the default 'Back' button action
 		if (keyCode == KeyEvent.KEYCODE_BACK) {
 			cf.goclass(24);
 			return true;
 		}
 		
 		return super.onKeyDown(keyCode, event);
 	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	
		  if(requestCode==cf.loadcomment_code)
		  {
			  load_comment=true;
			if(resultCode==RESULT_OK)
			{
				cf.ShowToast("Comments added successfully.", 0);
				 String hvaccomts = ((EditText)findViewById(R.id.hvaccomment)).getText().toString();
				 ((EditText)findViewById(R.id.hvaccomment)).setText((hvaccomts+" "+data.getExtras().getString("Comments")).trim());
				
			}
			
		}
		else if(requestCode==pick_img_code)
		{
				switch (resultCode) {
					case RESULT_CANCELED:
					   break;
					case RESULT_OK:
						cf.ShowToast("Image uploaded successfully.",0);
						((Button)findViewById(R.id.img_clr)).setVisibility(cf.v1.VISIBLE);
						if(data.getExtras().getInt("id")==2){
							((EditText)findViewById(R.id.updimg_ed).findViewWithTag("ModPath")).setText(data.getExtras().getString("path").trim());
						}
						
					break;
				}
		}

	}
	private void defaulthvac() {
		// TODO Auto-generated method stub
		((RadioButton) ftlrdgval.findViewWithTag("Not Applicable")).setChecked(true);	
		((RadioButton) ofhsprdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) phhprdgval.findViewWithTag("No")).setChecked(true);
		
		
		((LinearLayout)findViewById(R.id.distancelin)).setVisibility(cf.v1.GONE);
		((EditText)findViewById(R.id.distance_ettxt)).setText("");
		cf.getPolicyholderInformation(cf.selectedhomeid);
		System.out.println("build="+cf.BuildingSize);
		if(Integer.parseInt(cf.BuildingSize)<2500)
		{
			zonespin.setSelection(1);
		}
	}
}