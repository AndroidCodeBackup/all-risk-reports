package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

public class WindMitDooropenings extends Activity {
	CommonFunctions cf;
	int glazed,dooropeningid,rwsdooropen;
	Cursor c1,c2,c3,c4,c5,c6;
	String elevation[];
	LinearLayout subpnlshwdooropen;TableLayout dynviewdooropen;
	Spinner spnelevdooropen,spndoorquantity,spndoorwind,spndoorfloor,spndoorprot,spndooryrinst,spndooryrprov;
	String yrinstalledtxt="",doorspinnertxt="",yrprovidedtxt="",BI_DoorOpenings="",Dooropen_NA="",dooropennotappl="0",elevspinnertxt="";
	String[] arrdooropenings,arrdoorelev,arrdoorid,arrelevothertxt;	
	ArrayAdapter elevdooradap,spndoorquantityadap,spndoorwindadap,spndoorprotadap,spndooryrinstadap,spndooryrprovadap,spndoorflooradap;
	 public String spnofqunatity[] = { "--Select--", "1","2","3","4","5","6","7","8","9","10",};
	public String spndoortype[]={"--Select--","Single Door(out swing)","Double Door(out swing)","Single Door(in swing)","Double Door(in swing)","Siding Glass","Garage(single)","Garage(double)","Other"};
	private List<String> items1= new ArrayList<String>();
	public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	       
	        cf = new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);				
		 	}
	        setContentView(R.layout.dooropenings);
	        cf.getDeviceDimensions();
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
	        if(cf.identityval==31)
	        {
	          hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","B1 1802(Rev.01/12)","Door Openings",3,1,cf));
	        }
	        else if(cf.identityval==32)
	        { 
	        	hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","Commercial Type I","Door Openings",3,1,cf));
	        }
			LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
	     	main_layout.setMinimumWidth(cf.wd);
	     	main_layout.addView(new MyOnclickListener(getApplicationContext(), 3, 0,0,cf));
	       
	        cf.type_submenulayout =(LinearLayout) findViewById(R.id.typesubmenu); //** WINDMIT TYPE SUBMENU **//
	        cf.type_submenulayout.addView(new MyOnclickListener(this, 300, 1,0, cf));
	        
	        cf.windmittypechk(cf.identityval,((RadioButton)findViewById(R.id.b1802_chk)),((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((TextView)findViewById(R.id.commtxt)),((ImageView)findViewById(R.id.editbno))); // To check the checkbox of choosen inspection type
	        cf.uncheckcommercial(((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((RadioButton)findViewById(R.id.b1802_chk))); 
	    	        
	        cf.submenu_layout = (LinearLayout) findViewById(R.id.submenu);
	        cf.submenu_layout.addView(new MyOnclickListener(this, cf.identityval, 1,316,cf));
	        
	        cf.mainlayout = (LinearLayout)findViewById(R.id.bc_table);
	     
	        cf.tblrw = (TableRow)findViewById(R.id.row2);	
	        cf.tblrw.setMinimumHeight(cf.ht); 	
	        
	        cf.CreateARRTable(3);
	        cf.CreateARRTable(312);
	         cf.CreateARRTable(63);
	         cf.getPolicyholderInformation(cf.selectedhomeid);
	        Declaration();	   
	        Show_DoorOpenings_value();
	        setvalue();
	    }
	private void setvalue() {
		// TODO Auto-generated method stub		
		selecttbl();
		if(Dooropen_NA.equals("") || Dooropen_NA.equals("0"))
		{
			((LinearLayout)findViewById(R.id.dooropeningslin)).setVisibility(cf.v1.VISIBLE);
			
			int k=0;
			c4 =  cf.SelectTablefunction(cf.BI_Dooropenings, " where fld_srid='"+cf.selectedhomeid+"'  and insptypeid='"+cf.identityval+"' Group by fld_elevation");
			if(c4.getCount()>0)
			{
				c4.moveToFirst();
				 elevation = new String[c4.getCount()];
				int i=0;
				 do {
					 
					 elevation[i] = c4.getString(c4.getColumnIndex("fld_elevation"));
					 for(int j=0;j<cf.spnelevation.length;j++)
					 {
						 
						 if(cf.spnelevation[j].equals(elevation[i]))
						 {
							 k++;
						 }
					 }
					  i++;
				} while (c4.moveToNext());
			}
			else
			{
				//((LinearLayout)findViewById(R.id.dooropeningslin)).setVisibility(cf.v1.GONE);
			}
			
			System.out.println("kvalue="+k);
			if(k>=2)
			{
				((TextView)findViewById(R.id.savetxtdooropen)).setVisibility(cf.v1.VISIBLE);
			}
			else
			{
				((TextView)findViewById(R.id.savetxtdooropen)).setVisibility(cf.v1.GONE);
			}
		}
		else
		{
			Cursor c3 =  cf.SelectTablefunction(cf.BI_Dooropenings, " where fld_srid='"+cf.selectedhomeid+"'  and insptypeid='"+cf.identityval+"' Group by fld_elevation");
			if(c3.getCount()>0)
			{
				((CheckBox)findViewById(R.id.dooropennotappl)).setChecked(false);
				((LinearLayout)findViewById(R.id.dooropeningslin)).setVisibility(cf.v1.VISIBLE);
			}
			else
			{
				((CheckBox)findViewById(R.id.dooropennotappl)).setChecked(true);
				((LinearLayout)findViewById(R.id.dooropeningslin)).setVisibility(cf.v1.GONE);
			}
			((TextView)findViewById(R.id.savetxtdooropen)).setVisibility(cf.v1.VISIBLE);
		}
		cf.getPolicyholderInformation(cf.selectedhomeid);
		int spinnerPosition = spndooryrinstadap.getPosition(cf.YearPH);
		spndooryrinst.setSelection(spinnerPosition);
	}
	private void Declaration() {
		// TODO Auto-generated method stub
		cf.setupUI((ScrollView) findViewById(R.id.scr));
		
		 dynviewdooropen = (TableLayout)findViewById(R.id.dynamicdooropen);
		 subpnlshwdooropen = (LinearLayout)findViewById(R.id.subpnlshowdooropen);
		 
		 spnelevdooropen=(Spinner) findViewById(R.id.elevdooropen);
		 elevdooradap = new ArrayAdapter(this,android.R.layout.simple_spinner_item,cf.spnelevation);
		 elevdooradap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 spnelevdooropen.setAdapter(elevdooradap);
		 spnelevdooropen.setOnItemSelectedListener(new  Spin_Selectedlistener(3));
		
		 spndoorquantity=(Spinner) findViewById(R.id.spnquantityydoor);
		 spndoorquantityadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item,spnofqunatity);
		 spndoorquantityadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 spndoorquantity.setAdapter(spndoorquantityadap);
		 
		 spndoorwind=(Spinner) findViewById(R.id.spnrwindowdoor);
		 spndoorwindadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item,spndoortype);
		 spndoorwindadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 spndoorwind.setAdapter(spndoorwindadap);
		 spndoorwind.setOnItemSelectedListener(new  Spin_Selectedlistener(4));

		 spndoorprot=(Spinner) findViewById(R.id.spnprotectdoor);
		 spndoorprotadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item,cf.spnwindprottype);
		 spndoorprotadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 spndoorprot.setAdapter(spndoorprotadap);
		 
		 spndooryrinst=(Spinner) findViewById(R.id.spnyrinstdoor);
		 spndooryrinstadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item,cf.yearbuilt);
		 spndooryrinstadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 spndooryrinst.setAdapter(spndooryrinstadap);
		 spndooryrinst.setOnItemSelectedListener(new  Spin_Selectedlistener(1));
		 
		 /*spndooryrprov=(Spinner) findViewById(R.id.spnyrprovdoor);
		 spndooryrprovadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item,cf.yearbuilt);
		 spndooryrprovadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 spndooryrprov.setAdapter(spndooryrprovadap);
		 spndooryrprov.setOnItemSelectedListener(new  Spin_Selectedlistener(2));*/
		 
		 try
		 {		
			 items1.clear();
		  	 cf.getPolicyholderInformation(cf.selectedhomeid);
				items1.add("--Select--");
				 for(int k=0;k<Integer.parseInt(cf.Stories);k++)
				 {
					 items1.add(cf.floorarr[k]+" Floor");
			     } 
			}
			catch(Exception e){}
		 spndoorfloor=(Spinner) findViewById(R.id.spnpfloordoor);
		 spndoorflooradap = new ArrayAdapter(this,android.R.layout.simple_spinner_item,items1);
		 spndoorflooradap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 spndoorfloor.setAdapter(spndoorflooradap);
		 spndoorfloor.setSelection(1);
		 spndooryrinst.setSelection(spndooryrinstadap.getPosition(cf.YearPH));
	}
	class Spin_Selectedlistener implements OnItemSelectedListener
	{
		int i;
		public Spin_Selectedlistener(int i) {
			// TODO Auto-generated constructor stub
			this.i=i;
		}

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
			// TODO Auto-generated method stub
			if(i==1){	
				yrinstalledtxt = spndooryrinst.getSelectedItem().toString();
				if(yrinstalledtxt.equals("Other"))
				{					
					((EditText)findViewById(R.id.edotheryrinstalled)).setVisibility(cf.v1.VISIBLE);
				}
				else
				{
					((EditText)findViewById(R.id.edotheryrinstalled)).setVisibility(cf.v1.GONE);
				}
			}
			/*else if(i==2){
				yrprovidedtxt = spndooryrprov.getSelectedItem().toString();
				if(yrprovidedtxt.equals("Other"))
				{					
					((EditText)findViewById(R.id.edotheryrprovided)).setVisibility(cf.v1.VISIBLE);
				}
				else
				{
					((EditText)findViewById(R.id.edotheryrprovided)).setVisibility(cf.v1.INVISIBLE);
				}
			}*/
			else if(i==3){
				elevspinnertxt = spnelevdooropen.getSelectedItem().toString();
				if(elevspinnertxt.equals("Other Elevation"))
				{
					((EditText)findViewById(R.id.edtelevother)).setVisibility(cf.v1.VISIBLE);
				}
				else
				{
					((EditText)findViewById(R.id.edtelevother)).setText("");
					((EditText)findViewById(R.id.edtelevother)).setVisibility(cf.v1.GONE);
				}
			}
			else if(i==4){
				doorspinnertxt = spndoorwind.getSelectedItem().toString();
				if(doorspinnertxt.equals("Other"))
				{
					((EditText)findViewById(R.id.edtdoorother)).setVisibility(cf.v1.VISIBLE);
				}
				else
				{
					((EditText)findViewById(R.id.edtdoorother)).setText("");
					((EditText)findViewById(R.id.edtdoorother)).setVisibility(cf.v1.GONE);
				}
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	private void selecttbl() 
	{
		try
		{
			c1= cf.SelectTablefunction(cf.WindDoorSky_NA, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
			if(c1.getCount()>0)
			{
				c1.moveToFirst();
				Dooropen_NA = c1.getString(c1.getColumnIndex("fld_dooropenNA"));
			}
		}
		catch(Exception e)
		{
			System.out.println("sfdsdsds"+e.getMessage());
		}
		try
		{
			c2 =  cf.SelectTablefunction(cf.BI_Dooropenings, " where fld_srid='"+cf.selectedhomeid+"'  and insptypeid='"+cf.identityval+"'");
			
		}
		catch(Exception e)
		{
			System.out.println("eeeeee"+e.getMessage());
		}
	}
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.mouseoverid:
			cf.ShowToast("Use only for Commercial Type I and Residential Wind Mitigation Inspections 1802 only.",0);
			break;
		case R.id.doorsave:
			selecttbl();
			if(((CheckBox)findViewById(R.id.dooropennotappl)).isChecked()==false)
			{
				if(c2.getCount()==0)
				{
					cf.ShowToast("Please save Door Openings.", 0);	
				}
				else
				{
					int k=0;
					c4 =  cf.SelectTablefunction(cf.BI_Dooropenings, " where fld_srid='"+cf.selectedhomeid+"'  and insptypeid='"+cf.identityval+"' Group by fld_elevation");
					if(c4.getCount()>0)
					{
						c4.moveToFirst();
						 elevation = new String[c4.getCount()];
						int i=0;
						 do {
							 
							 elevation[i] = c4.getString(c4.getColumnIndex("fld_elevation"));
							 for(int j=0;j<cf.spnelevation.length;j++)
							 {
								 
								 if(cf.spnelevation[j].equals(elevation[i]))
								 {
									 k++;
								 }
							 }
							  i++;
						} while (c4.moveToNext());
					}
					System.out.println("kcheck="+k);
					
					if(k>=2)
					{
						cf.ShowToast("Door Openings saved successfully",0);
						((TextView)findViewById(R.id.savetxtdooropen)).setVisibility(cf.v1.VISIBLE);
						updatenotappl();
						cf.goclass(460);
					}
					else
					{
						cf.ShowToast("Please save atleast 2 minimum different elevation door openings",0);
					}
					
				}
			}
			else
			{
				//dooropennotappl  ="1";
				try
				{
					cf.arr_db.execSQL("Delete from "+cf.BI_Dooropenings+" Where fld_srid='"+cf.selectedhomeid+"' and insptypeid='"+cf.identityval+"'");
				}
				catch(Exception e)
				{
					System.out.println("deleting error="+e.getMessage());
				}
				updatenotappl();
				cf.goclass(460);
			}
			break;
		case R.id.dooropeningscancel:
			cleardooropenings();						
			break;
		case R.id.dooropeningssave:
		selecttbl();
		boolean b=((spnelevdooropen.getSelectedItem().toString().equals("Other Elevation")) && ((EditText)findViewById(R.id.edtelevother)).getText().toString().trim().equals("")) ? false:true;
		boolean c=((spndoorwind.getSelectedItem().toString().equals("Other")) && ((EditText)findViewById(R.id.edtdoorother)).getText().toString().trim().equals("")) ? false:true;
		
		if(((CheckBox)findViewById(R.id.dooropennotappl)).isChecked()==false)
		{
			if(!spnelevdooropen.getSelectedItem().toString().equals("--Select--") &&  b==true)
			{
				if(!spndoorquantity.getSelectedItem().toString().equals("--Select--"))
				{
					if(!spndoorwind.getSelectedItem().toString().equals("--Select--") && c==true)
					{
						if(!spndoorfloor.getSelectedItem().toString().equals("--Select--"))
						{
							if(!spndoorprot.getSelectedItem().toString().equals("--Select--"))
							{
								if(!yrinstalledtxt.equals("--Select--"))
								{
									if(yrinstalledtxt.equals("Other"))
									{
										if(!((EditText)findViewById(R.id.edotheryrinstalled)).getText().toString().trim().equals(""))
										{
											if(((EditText)findViewById(R.id.edotheryrinstalled)).getText().length()<4)
											{
												cf.ShowToast("Please enter the year in four digits.", 0);
												cf.setFocus(((EditText)findViewById(R.id.edotheryrinstalled)));
											}
											else if(cf.yearValidation(((EditText)findViewById(R.id.edotheryrinstalled)).getText().toString()).equals("true"))
											{
												cf.ShowToast("Year should not exceed the current year.", 0);
												cf.setFocus(((EditText)findViewById(R.id.edotheryrinstalled)));
											}
											else if(cf.yearValidation(((EditText)findViewById(R.id.edotheryrinstalled)).getText().toString()).equals("zero"))
											{
												cf.ShowToast("Invalid Entry! Please enter the valid year.", 0);
												((EditText)findViewById(R.id.edotheryrinstalled)).setText("");
												cf.setFocus(((EditText)findViewById(R.id.edotheryrinstalled)));cf.hidekeyboard();
											}
											else
											{
												InsertDoorOpenings();
											}
										}
										else
										{
											cf.ShowToast("Please enter the other text for Year Installed.", 0);	
											cf.setFocus((EditText)findViewById(R.id.edotheryrinstalled));
										}
									}
									else
									{
										InsertDoorOpenings();
									}
								}
								else
								{
									cf.ShowToast("Please select the Year Installed.", 0);								
								}
							}
							else
							{
								cf.ShowToast("Please select the Protection Codes.", 0);	
							}
						}
						else
						{
							cf.ShowToast("Please select the Floor.", 0);	
						}
					}
					else
					{
						if(spndoorwind.getSelectedItem().toString().equals("--Select--"))
						{
							cf.ShowToast("Please select the Door Type.", 0);	
						}
						else if(b==false)
						{
							cf.ShowToast("Please select the other text for Door Type.", 0);	
							cf.setFocus(((EditText)findViewById(R.id.edtdoorother)));
						}
						
					}
				}
				else
				{
					cf.ShowToast("Please select the No. of Quantity.", 0);	
				}
			}else
			{
				if(spnelevdooropen.getSelectedItem().toString().equals("--Select--"))
				{
					cf.ShowToast("Please select the Elevation.", 0);
				}
				else if(b==false)
				{
					cf.ShowToast("Please select the other text for Elevation.", 0);	
					cf.setFocus(((EditText)findViewById(R.id.edtelevother)));
				}
			}
		}
		else
		{
			InsertDoorOpenings();	
		}
			break;
		case R.id.dooropennotappl:			
			if(((CheckBox)findViewById(R.id.dooropennotappl)).isChecked())
			{
				dooropennotappl="1";
				if(rwsdooropen>0)
				{
					AlertDialog.Builder builder = new AlertDialog.Builder(WindMitDooropenings.this);
					builder.setTitle("Confirmation");
					builder.setIcon(R.drawable.alertmsg);
					builder.setMessage("Do you want to Clear the Door Openings Information?").setCancelable(false)
							.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,int id1) 
										{
												cleardooropenings();
												subpnlshwdooropen.setVisibility(cf.v1.GONE);
												((CheckBox)findViewById(R.id.dooropennotappl)).setChecked(true);
												((LinearLayout)findViewById(R.id.dooropeningslin)).setVisibility(cf.v1.GONE);
												((TextView)findViewById(R.id.savetxtdooropen)).setVisibility(cf.v1.GONE);
										}
									})
							.setNegativeButton("No",new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,	int id) {
											
											((CheckBox)findViewById(R.id.dooropennotappl)).setChecked(false);
											dialog.cancel();											
									}
							});
					builder.show();
				}
				else
				{
					((TextView)findViewById(R.id.savetxtdooropen)).setVisibility(cf.v1.GONE);
					((LinearLayout)findViewById(R.id.dooropeningslin)).setVisibility(cf.v1.GONE);
				}
				
			}
			else
			{
				dooropennotappl="0";cleardooropenings();
				((LinearLayout)findViewById(R.id.dooropeningslin)).setVisibility(cf.v1.VISIBLE);
				((TextView)findViewById(R.id.savetxtdooropen)).setVisibility(cf.v1.GONE);
			}
			break;
		}		
	}
	protected void updatenotappl() {
		// TODO Auto-generated method stub
		try
		{
			c1= cf.SelectTablefunction(cf.WindDoorSky_NA, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
			if(c1.getCount()==0)
			{
				cf.arr_db.execSQL("INSERT INTO " + cf.WindDoorSky_NA + " (fld_srid,insp_typeid,fld_dooropenNA)"
						+ "VALUES ('"+cf.selectedhomeid+"','"+cf.identityval+"','"+dooropennotappl+"')");
			}
			else
			{
				cf.arr_db.execSQL("UPDATE "	+ cf.WindDoorSky_NA	+ " SET fld_dooropenNA='"+dooropennotappl+"' WHERE fld_srid ='"+ cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'");	
			}			
		}
		catch(Exception e)
		{
			System.out.println("sdfdfds"+e.getMessage());
		}
	}
	private void cleardooropenings() {
		// TODO Auto-generated method stub
		spnelevdooropen.setSelection(0);rwsdooropen=0;
		spndoorquantity.setSelection(0);
		spndoorwind.setSelection(0);
		spndoorfloor.setSelection(0);
		spndoorprot.setSelection(0);
		spndooryrinst.setSelection(0);
		//spndooryrprov.setSelection(0);
		//((EditText)findViewById(R.id.edotheryrprovided)).setText("");((EditText)findViewById(R.id.edotheryrprovided)).setVisibility(cf.v1.INVISIBLE);
		((EditText)findViewById(R.id.edotheryrinstalled)).setText("");((EditText)findViewById(R.id.edotheryrinstalled)).setVisibility(cf.v1.INVISIBLE);
		((EditText)findViewById(R.id.edtdoorother)).setText("");((EditText)findViewById(R.id.edtdoorother)).setVisibility(cf.v1.GONE);
		((CheckBox)findViewById(R.id.glazed)).setChecked(false);
		((Button)findViewById(R.id.dooropeningssave)).setText("Save/Add More Door Openings");
		spnelevdooropen.setEnabled(true);
		spndoorfloor.setSelection(1);
		spndooryrinst.setSelection(spndooryrinstadap.getPosition(cf.YearPH));
	}
	
	private void InsertDoorOpenings() {
		// TODO Auto-generated method stub
		String elevation =spnelevdooropen.getSelectedItem().toString();
		String quantity =spndoorquantity.getSelectedItem().toString();
		String doors =spndoorwind.getSelectedItem().toString();
		String floor =spndoorfloor.getSelectedItem().toString();
		String protection =spndoorprot.getSelectedItem().toString();
		String yrinstalled =spndooryrinst.getSelectedItem().toString();
		//String yrprovided =spndooryrprov.getSelectedItem().toString();		
		if(((CheckBox)findViewById(R.id.glazed)).isChecked()){glazed=1;}else{glazed=0;}
		
		if(!quantity.equals("--Select--") && !doors.equals("--Select--") && !floor.equals("--Select--") && !protection.equals("--Select--") && 
				!yrinstalled.equals("--Select--"))
		{
			BI_DoorOpenings=quantity+"&#39;"+doors+"&#94;"+((EditText)findViewById(R.id.edtdoorother)).getText().toString().trim()+"&#39;"+floor+"&#39;"+protection+"&#39;"+yrinstalled+"&#94;"+((EditText)findViewById(R.id.edotheryrinstalled)).getText().toString()
					+"&#39;"+glazed;
		}
		
		
		try
		{
			if(((Button)findViewById(R.id.dooropeningssave)).getText().toString().equals("Update"))
			{
						cf.arr_db.execSQL("UPDATE "
								+ cf.BI_Dooropenings
								+ " SET BI_Dooropenings='"+cf.encode(BI_DoorOpenings)+"',fld_elevation_othertxt='"+cf.encode(((EditText)findViewById(R.id.edtelevother)).getText().toString())+"'"
								+ " WHERE fld_srid ='"+ cf.selectedhomeid + "' and BI_DoorId='"+dooropeningid+"' and insptypeid='"+cf.identityval+"'");
						((Button)findViewById(R.id.dooropeningssave)).setText("Save/Add More Door Openings");
						
			}
			else
			{
						Cursor c1=cf.SelectTablefunction(cf.BI_Dooropenings, " where fld_srid='"+cf.selectedhomeid+"' and insptypeid='"+cf.identityval+"'");
						 cf.arr_db.execSQL("INSERT INTO "
									+ cf.BI_Dooropenings
									+ " (fld_srid,insptypeid,fld_elevation,BI_DoorOpenings,fld_elevation_othertxt)"
									+ "VALUES ('"+cf.selectedhomeid+"','"+cf.identityval+"','"+elevation+"','"+cf.encode(BI_DoorOpenings)+"','"+cf.encode(((EditText)findViewById(R.id.edtelevother)).getText().toString())+"')");
						 
			}			
			 cf.ShowToast("Door Openings saved successfully",0);			
			((TextView)findViewById(R.id.savetxtdooropen)).setVisibility(cf.v1.VISIBLE);
			cleardooropenings();
			spnelevdooropen.setEnabled(true);((Button)findViewById(R.id.dooropeningssave)).setText("Save/Add More Door Openings");
			Show_DoorOpenings_value();
		}
		catch(Exception e)
		{
			System.out.println("ins "+e.getMessage());
		}
	}
	private void Show_DoorOpenings_value() {
		// TODO Auto-generated method stub
		
		 
		
		
		Cursor c1=cf.SelectTablefunction(cf.BI_Dooropenings, " where fld_srid='"+cf.selectedhomeid+"' and insptypeid='"+cf.identityval+"'");
		rwsdooropen = c1.getCount();
		if(c1.getCount()!=0)
		{
			((TextView)findViewById(R.id.savetxtdooropen)).setVisibility(cf.v1.VISIBLE);
		
			arrdooropenings=new String[rwsdooropen];
			arrdoorelev = new String[rwsdooropen];
			arrdoorid= new String[rwsdooropen];
			arrelevothertxt = new String[rwsdooropen];
			c1.moveToFirst();
			 int i=0;
			 do {
					 arrdooropenings[i] = cf.decode(c1.getString(c1.getColumnIndex("BI_DoorOpenings")));
					 arrdoorelev[i] = c1.getString(c1.getColumnIndex("fld_elevation"));
					 arrdoorid[i] = c1.getString(c1.getColumnIndex("BI_DoorId"));
					 arrelevothertxt[i] = cf.decode(c1.getString(c1.getColumnIndex("fld_elevation_othertxt")));
						i++;
				} while (c1.moveToNext());	
			 subpnlshwdooropen.setVisibility(cf.v1.VISIBLE);
			 Disp_dooropenings_value();
		}
		else
		{
			((TextView)findViewById(R.id.savetxtdooropen)).setVisibility(cf.v1.GONE);
			subpnlshwdooropen.setVisibility(cf.v1.GONE);
		}
	}
private void Disp_dooropenings_value() {
		
		dynviewdooropen.removeAllViews();
		LinearLayout h1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvac_listview_header, null);
		LinearLayout th= (LinearLayout)h1.findViewById(R.id.dooropenings_hdr);th.setVisibility(cf.v1.VISIBLE);
		TableLayout.LayoutParams lp = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
	 	lp.setMargins(2, 0, 2, 2); 
	 	h1.removeAllViews(); 
	 	
	 	dynviewdooropen.addView(th,lp);	
	 	dynviewdooropen.setVisibility(View.VISIBLE);	
	 	
	 	for (int i = 0; i < rwsdooropen; i++) 
		{	
			TextView No,txtelevation,txtnoofquantity,txtwindow,txtfloor,txtprotection,txtyrinstalled,txtyrprovided,glazed;
			ImageView edit,delete;
			
			LinearLayout t1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvac_listview, null);
			LinearLayout t = (LinearLayout)t1.findViewById(R.id.dooropenings);t.setVisibility(cf.v1.VISIBLE);
			t.setId(44444+i);/// Set some id for further use
			
		 	No= (TextView) t.findViewWithTag("No");			
		 	txtelevation= (TextView) t.findViewWithTag("BI_ElevDoor"); 	
		 	txtnoofquantity= (TextView) t.findViewWithTag("BI_DoorQuantity");
		 	txtwindow= (TextView) t.findViewWithTag("BI_Doors");	
		 	txtfloor= (TextView) t.findViewWithTag("BI_DoorFloor");	
		 	txtprotection= (TextView) t.findViewWithTag("BI_DoorProt");
		 	txtyrinstalled= (TextView) t.findViewWithTag("BI_DoorYrinst");	
		 	//txtyrprovided= (TextView) t.findViewWithTag("BI_DoorYrProv");	
		 	glazed= (TextView) t.findViewWithTag("BI_DoorGlazed");	
		 	
			No.setText(String.valueOf(i+1));		 	
				
			if(arrelevothertxt[i].equals(""))
			{
				txtelevation.setText(arrdoorelev[i]);
			}
			else
			{
				txtelevation.setText("Other Elevation("+arrelevothertxt[i]+")");		
			}
			
			
			
			String Door_open_split[] = arrdooropenings[i].split("\\&#39;"); 
			txtnoofquantity.setText(Door_open_split[0]);
			
			
			String Door_type_split[] = Door_open_split[1].split("&#94;");
			if(Door_type_split[0].equals("Other"))
			{
				txtwindow.setText(Door_type_split[0]+" ("+Door_type_split[1]+")");
			}	
			else
			{
				txtwindow.setText(Door_type_split[0]);
			}
			
			
			txtfloor.setText(Door_open_split[2]);
			txtprotection.setText(Door_open_split[3]);
			String yrinstalled[] = Door_open_split[4].split("&#94;");
			
			if(yrinstalled[0].equals("Other"))
			{
				txtyrinstalled.setText(yrinstalled[0]+" ("+yrinstalled[1]+")");
			}else
			{
				txtyrinstalled.setText(yrinstalled[0]);
			}
			
			if(Door_open_split[5].equals("1"))
			{
				glazed.setBackgroundResource(R.drawable.tick_icon);
			}
			else
			{
				glazed.setText("N/A");
			}
			
		 	edit= (ImageView) t.findViewWithTag("Dooropen_edit");			 
		 	edit.setTag(arrdoorid[i]);
		 	edit.setOnClickListener(new WindOpen_EditClick(1,arrdoorid[i]));
		 	delete= (ImageView) t.findViewWithTag("Dooropen_delete");
		 	delete.setTag(arrdoorid[i]);
		 	delete.setOnClickListener(new WindOpen_EditClick(2,arrdoorid[i]));
		 	
		 	t1.removeAllViews();
		 	dynviewdooropen.addView(t,lp);
		}
	 	
	}
class WindOpen_EditClick implements OnClickListener
{
	int type;
	int edit; 
	WindOpen_EditClick(int edit,String id1)
	{
		this.edit=edit;
		this.type=Integer.parseInt(id1);
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		//type=Integer.parseInt(v.getTag().toString());
		if(edit==1)
		{				
			((Button)findViewById(R.id.dooropeningssave)).setText("Update");
			updateDoorOpenings(type);
		}
		else if(edit==2)
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(WindMitDooropenings.this);
					builder.setMessage("Do you want to delete the selected Door Opening?").setCancelable(false)
							.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,int id) {												
											cf.arr_db.execSQL("Delete from "+cf.BI_Dooropenings+" Where BI_DoorId='"+type+"'");
											cf.ShowToast("Deleted successfully.", 0);														
											cleardooropenings();
											Show_DoorOpenings_value();
												}
									})
							.setNegativeButton("No",new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog, int id) {
											dialog.cancel();
										}
									});
					builder.setTitle("Confirmation");
					builder.setIcon(R.drawable.alertmsg);
					builder.show();		
		}
	}
}
	private void updateDoorOpenings(int type2) {
		// TODO Auto-generated method stub
		dooropeningid=type2;
		Cursor c1=cf.SelectTablefunction(cf.BI_Dooropenings, " where BI_DoorId='"+type2+"'");
		if(c1.getCount()>0)
		{
			c1.moveToFirst();
			String DoorOpening_value = cf.decode(c1.getString(c1.getColumnIndex("BI_DoorOpenings")));
			String Door_open_split[] = DoorOpening_value.split("&#39;"); 	
			
			int spinnerPosition1 = elevdooradap.getPosition(c1.getString(c1.getColumnIndex("fld_elevation")));
			spnelevdooropen.setSelection(spinnerPosition1);
			
			if(c1.getString(c1.getColumnIndex("fld_elevation")).equals("Other Elevation"))
			{
				((EditText)findViewById(R.id.edtelevother)).setVisibility(cf.v1.VISIBLE);
				((EditText)findViewById(R.id.edtelevother)).setText(cf.decode(c1.getString(c1.getColumnIndex("fld_elevation_othertxt"))));
			}
			else
			{
				((EditText)findViewById(R.id.edtelevother)).setVisibility(cf.v1.GONE);
				((EditText)findViewById(R.id.edtelevother)).setText("");
			}
			int spinnerPosition2 = spndoorquantityadap.getPosition(Door_open_split[0]);
			spndoorquantity.setSelection(spinnerPosition2);


			String Door_type_split[] = Door_open_split[1].split("&#94;");
			if(Door_type_split[0].equals("Other"))
			{
				((EditText)findViewById(R.id.edtdoorother)).setVisibility(cf.v1.VISIBLE);
				((EditText)findViewById(R.id.edtdoorother)).setText(Door_type_split[1]);
			}	
			else
			{
				((EditText)findViewById(R.id.edtdoorother)).setVisibility(cf.v1.INVISIBLE);
				((EditText)findViewById(R.id.edtdoorother)).setText("");
			}
			
			int spinnerPosition3 = spndoorwindadap .getPosition(Door_type_split[0]);
			spndoorwind.setSelection(spinnerPosition3);
			int spinnerPosition4 = spndoorflooradap.getPosition(Door_open_split[2]);
			spndoorfloor.setSelection(spinnerPosition4);
			int spinnerPosition5 = spndoorprotadap.getPosition(Door_open_split[3]);
			spndoorprot.setSelection(spinnerPosition5);	
			
			String yrinstalled[] = Door_open_split[4].split("&#94;");			
			int spinnerPosition6 = spndooryrinstadap .getPosition(yrinstalled[0]);
			spndooryrinst.setSelection(spinnerPosition6);
			
			if(yrinstalled[0].equals("Other"))
			{
				((EditText)findViewById(R.id.edotheryrinstalled)).setVisibility(cf.v1.VISIBLE);
				((EditText)findViewById(R.id.edotheryrinstalled)).setText(yrinstalled[1]);
			}
			else
			{
				((EditText)findViewById(R.id.edotheryrinstalled)).setVisibility(cf.v1.INVISIBLE);
				((EditText)findViewById(R.id.edotheryrinstalled)).setText("");
			}
			/*String yrprovided[] = Door_open_split[5].split("&#94;");			
			int spinnerPosition7 = spndooryrprovadap .getPosition(yrprovided[0]);
			spndooryrprov.setSelection(spinnerPosition7);
			
			if(yrprovided[0].equals("Other"))
			{
				((EditText)findViewById(R.id.edotheryrprovided)).setVisibility(cf.v1.VISIBLE);
				((EditText)findViewById(R.id.edotheryrprovided)).setText(yrprovided[1]);
			}
			else
			{
				((EditText)findViewById(R.id.edotheryrprovided)).setVisibility(cf.v1.INVISIBLE);
				((EditText)findViewById(R.id.edotheryrprovided)).setText("");
			}*/
			if(Door_open_split[5].equals("1")){
			((CheckBox)findViewById(R.id.glazed)).setChecked(true);
			}
			else
			{
				((CheckBox)findViewById(R.id.glazed)).setChecked(false);
			}		
			spnelevdooropen.setEnabled(false);
			((Button)findViewById(R.id.dooropeningssave)).setText("Update");
		}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			cf.gotoclass(cf.identityval, WindMitWindowopenings.class);
			return true;
		}		
		return super.onKeyDown(keyCode, event);
	}
}
