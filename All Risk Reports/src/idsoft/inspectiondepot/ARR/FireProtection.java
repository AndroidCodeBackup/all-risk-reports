/*
   ************************************************************
   * Project: ALL RISK REPORT
   * Module : FireProtection.java
   * Creation History:
      Created By : Gowri on 
   * Modification History:
      Last Modified By : Gowri on 04/04/2013
   ************************************************************  
*/
package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.Plumbing.textwatcher;
import idsoft.inspectiondepot.ARR.PoolFence.checklistenetr;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class FireProtection extends Activity {
	private static final int DATE_DIALOG_ID = 0;
	CommonFunctions cf;
	int fpschkval=0;
	String  tasrd_val="No",ferd_val="No",fdrd_val="No",wsrd_val="No",sdrd_val="",ssrd_val="No",scrd_val="",ssard_val="",
			sscrd_val="",retferdtxt="",sdord_val="",szetxt="";
	RadioGroup ferdgval,fdrdgval,wsrdgval,tasrdgval,sdrdgval,ssrdgval,scrdgval,ssardgval,sscrdgval,sdordgval;
	boolean sschk=false,scchk=false,ssachk=false,sscchk=false;
	@Override
	   public void onCreate(Bundle savedInstanceState) {

	        super.onCreate(savedInstanceState);
	        cf=new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);
				
		 	}
	        setContentView(R.layout.fireprotection);
	        cf.getDeviceDimensions();
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
			hdr_layout.addView(new HdrOnclickListener(this,1,"General Conditions & Hazards","Fire Protection/Survey",4,0,cf));
	        LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
	        mainmenu_layout.setMinimumWidth(cf.wd);
	        mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 4, 0,0,cf));
	        LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 45, 1,0,cf));
			TableRow tblrw = (TableRow)findViewById(R.id.row2);
			tblrw.setMinimumHeight(cf.ht);
			cf.CreateARRTable(45);
			declarations();cf.getCalender();
			
			FireProtect_SetValue();
	}
	private void FireProtect_SetValue() {
		// TODO Auto-generated method stub
		try
		{
			chk_values();
		   Cursor FPS_retrive=cf.SelectTablefunction(cf.GCH_FireProtecttbl, " where fld_srid='"+cf.selectedhomeid+"' AND (fld_fpschk='1' OR (fld_fpschk='0' AND fld_size<>''))");
		   if(FPS_retrive.getCount()>0)
		   {  
			   FPS_retrive.moveToFirst();
			   ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
			  if(FPS_retrive.getInt(FPS_retrive.getColumnIndex("fld_fpschk"))==1)
			  {
				  ((CheckBox)findViewById(R.id.fps_na)).setChecked(true);
				   ((LinearLayout)findViewById(R.id.fps_table)).setVisibility(cf.v1.GONE);
			  }
			  else
			  {
				   ((CheckBox)findViewById(R.id.fps_na)).setChecked(false);
				   ((LinearLayout)findViewById(R.id.fps_table)).setVisibility(cf.v1.VISIBLE);
				   
				    ferd_val=FPS_retrive.getString(FPS_retrive.getColumnIndex("fld_fireext"));
				    ((RadioButton) ferdgval.findViewWithTag(ferd_val)).setChecked(true);
				    
				    fdrd_val=FPS_retrive.getString(FPS_retrive.getColumnIndex("fld_firedet"));
				    ((RadioButton) fdrdgval.findViewWithTag(fdrd_val)).setChecked(true);
				    
				    ((EditText)findViewById(R.id.sizetxt)).setText(cf.decode(FPS_retrive.getString(FPS_retrive.getColumnIndex("fld_size"))));
				    ((EditText)findViewById(R.id.typetxt)).setText(cf.decode(FPS_retrive.getString(FPS_retrive.getColumnIndex("fld_type"))));
				    
				    wsrd_val=FPS_retrive.getString(FPS_retrive.getColumnIndex("fld_wservice"));
				    ((RadioButton) wsrdgval.findViewWithTag(wsrd_val)).setChecked(true);
				    
				    tasrd_val=FPS_retrive.getString(FPS_retrive.getColumnIndex("fld_taged"));
				    ((RadioButton) tasrdgval.findViewWithTag(tasrd_val)).setChecked(true);
				    if(tasrd_val.equals("Yes"))
				    {
				    	 ((LinearLayout)findViewById(R.id.taglin)).setVisibility(cf.v1.VISIBLE);
				    	 ((EditText)findViewById(R.id.tagedit)).setText(cf.decode(FPS_retrive.getString(FPS_retrive.getColumnIndex("fld_tagdate"))));
				    }
				    else
				    {
				    	((LinearLayout)findViewById(R.id.taglin)).setVisibility(cf.v1.GONE);
				    }
				    sdrd_val=FPS_retrive.getString(FPS_retrive.getColumnIndex("fld_smokedet"));
				    ((RadioButton) sdrdgval.findViewWithTag(sdrd_val)).setChecked(true);
				    if(sdrd_val.equals("Yes"))
				    {
				    	((LinearLayout)findViewById(R.id.sdlin)).setVisibility(cf.v1.VISIBLE);
				    	sdord_val=FPS_retrive.getString(FPS_retrive.getColumnIndex("fld_smokoptions"));
					    ((RadioButton) sdordgval.findViewWithTag(sdord_val)).setChecked(true);
				    }
				    else
				    {
				    	((LinearLayout)findViewById(R.id.sdlin)).setVisibility(cf.v1.GONE);
				    }
				    
				    ssrd_val=FPS_retrive.getString(FPS_retrive.getColumnIndex("fld_ssys"));
				    ((RadioButton) ssrdgval.findViewWithTag(ssrd_val)).setChecked(true);
				    
				    scrd_val=FPS_retrive.getString(FPS_retrive.getColumnIndex("fld_scover"));
				    ((RadioButton) scrdgval.findViewWithTag(scrd_val)).setChecked(true);
				    
				    ssard_val=FPS_retrive.getString(FPS_retrive.getColumnIndex("fld_salarm"));
				    ((RadioButton) ssardgval.findViewWithTag(ssard_val)).setChecked(true);
				    
				    sscrd_val=FPS_retrive.getString(FPS_retrive.getColumnIndex("fld_sign"));
				    ((RadioButton) sscrdgval.findViewWithTag(sscrd_val)).setChecked(true);
			  
			  }
		   }
		   else
		   {
			    cf.getinspectioname(cf.selectedhomeid);
			    if(cf.selinspname.equals("4"))
				{
					((CheckBox)findViewById(R.id.fps_na)).setChecked(false);
					((LinearLayout)findViewById(R.id.fps_table)).setVisibility(cf.v1.VISIBLE);
				}
				else
				{
					((CheckBox)findViewById(R.id.fps_na)).setChecked(true);
					((LinearLayout)findViewById(R.id.fps_table)).setVisibility(cf.v1.GONE);
				}
		   }
		}
	    catch (Exception E)
		{
			String strerrorlog="Retrieving Fire Protection/Survey - GCH";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
	private void declarations() {

		// TODO Auto-generated method stub
		cf.setupUI((ScrollView) findViewById(R.id.scr));
		((TextView)findViewById(R.id.tagdate)).setText(Html.fromHtml(cf.redcolor+"  Date"));
		ferdgval = (RadioGroup)findViewById(R.id.fe_rdg);
		ferdgval.setOnCheckedChangeListener(new checklistenetr(1));
		fdrdgval = (RadioGroup)findViewById(R.id.fd_rdg);
		fdrdgval.setOnCheckedChangeListener(new checklistenetr(2));
		wsrdgval = (RadioGroup)findViewById(R.id.ws_rdg);
		wsrdgval.setOnCheckedChangeListener(new checklistenetr(3));
		tasrdgval = (RadioGroup)findViewById(R.id.tas_rdg);
		tasrdgval.setOnCheckedChangeListener(new checklistenetr(4));
		sdrdgval = (RadioGroup)findViewById(R.id.sd_rdg);
		sdrdgval.setOnCheckedChangeListener(new checklistenetr(5));
		ssrdgval = (RadioGroup)findViewById(R.id.ss_rdg);
		ssrdgval.setOnCheckedChangeListener(new checklistenetr(6));
		scrdgval = (RadioGroup)findViewById(R.id.sc_rdg);
		scrdgval.setOnCheckedChangeListener(new checklistenetr(7));
		ssardgval = (RadioGroup)findViewById(R.id.ssa_rdg);
		ssardgval.setOnCheckedChangeListener(new checklistenetr(8));
		sscrdgval = (RadioGroup)findViewById(R.id.ssc_rdg);
		sscrdgval.setOnCheckedChangeListener(new checklistenetr(9));
		sdordgval = (RadioGroup)findViewById(R.id.sdo_rdg);
		sdordgval.setOnCheckedChangeListener(new checklistenetr(10));
		((EditText)findViewById(R.id.sizetxt)).addTextChangedListener(new textwatcher(1));
		((EditText)findViewById(R.id.sizetxt)).setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
		set_defaultchk();
		
	}
	class textwatcher implements TextWatcher
    {
	         public int type;
	         textwatcher(int type)
	    	{
	    		this.type=type;
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {


	    		// TODO Auto-generated method stub
	    		if(this.type==1)
	    		{
	    			 if(!s.toString().equals("")){
	    			     if(Character.toString(s.toString().charAt(0)).equals("0"))
		    			 {
		    				 cf.ShowToast("Invalid Entry! Please enter valid Size.", 0);
		    				 ((EditText)findViewById(R.id.sizetxt)).setText("");
		    			 }
	    			 }
	    		}
	    		
	     }

	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,

	    			int after) {
	    		// TODO Auto-generated method stub
	     	}

	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,

	    			int count) {
	    		// TODO Auto-generated method stub
	    	}
	   }	
	class checklistenetr implements OnCheckedChangeListener
	{
    	int i;
		public checklistenetr(int i) {
			// TODO Auto-generated constructor stub
		   this.i=i;
		}

		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			   RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
		        boolean isChecked = checkedRadioButton.isChecked();
		        if (isChecked)
		        {
		          switch (i) {
					case 1:
						ferd_val= checkedRadioButton.getText().toString().trim();
						break;
					case 2:
						fdrd_val= checkedRadioButton.getText().toString().trim();
						break;
					case 3:
						wsrd_val= checkedRadioButton.getText().toString().trim();
						break;
					case 4:
						tasrd_val= checkedRadioButton.getText().toString().trim();
						if(tasrd_val.equals("Yes")){
							((LinearLayout)findViewById(R.id.taglin)).setVisibility(cf.v1.VISIBLE);
						}else{
							((EditText)findViewById(R.id.tagedit)).setText("");
							((LinearLayout)findViewById(R.id.taglin)).setVisibility(cf.v1.GONE);
						}
						break;
					case 5:
						sdrd_val= checkedRadioButton.getText().toString().trim();sschk=false;
						if(sdrd_val.equals("Yes"))
						{
							((LinearLayout)findViewById(R.id.sdlin)).setVisibility(cf.v1.VISIBLE);
						}
						else
						{
							((LinearLayout)findViewById(R.id.sdlin)).setVisibility(cf.v1.GONE);
						}
						break;
					case 6:
						ssrd_val= checkedRadioButton.getText().toString().trim();
						break;
					case 7:
						scrd_val= checkedRadioButton.getText().toString().trim();scchk=false;
						break;
					case 8:
						ssard_val= checkedRadioButton.getText().toString().trim();ssachk=false;
						break;
					case 9:
						sscrd_val= checkedRadioButton.getText().toString().trim();sscchk=false;
						break;
					case 10:
						sdord_val= checkedRadioButton.getText().toString().trim();
						break;
		          }
		       }
		}
	}
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.fps_na:/**FIR PROTECTION NOT APPLICABLE CHECKBOX **/
			if(((CheckBox)findViewById(R.id.fps_na)).isChecked())
			{
		
				System.out.println("retferdtxt="+retferdtxt);
    			if(!retferdtxt.equals("")){
 				    showunchkalert(1);
 			    }
    			else
    			{
    				
    				clearfp();
    				set_defaultchk();
    				((LinearLayout)findViewById(R.id.fps_table)).setVisibility(v.GONE);
 			    	((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.INVISIBLE);
 			    }
				
			}
			else
			{
				((LinearLayout)findViewById(R.id.fps_table)).setVisibility(v.VISIBLE);
				((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.INVISIBLE);
				((CheckBox)findViewById(R.id.fps_na)).setChecked(false);
			}
			
			break;
		case R.id.helpid:
			cf.ShowToast("Use for Commercial GCH reports.",0);
			break;
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.gettagdate:
			cf.showDialogDate(((EditText)findViewById(R.id.tagedit)));
			break;
		
		case R.id.save:
			if(((CheckBox)findViewById(R.id.fps_na)).isChecked())
			{
				fpschkval=1;FireInsert();
				((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
				cf.ShowToast("Fire Protection/Survey saved successfully.", 0);
				cf.gotoclass(35, WindMitCommRestsupp.class);	
			}
			else
			{
				fpschkval=0;
				if(((EditText)findViewById(R.id.sizetxt)).getText().toString().equals(""))
				{
					cf.ShowToast("Please enter the Size.", 0);
					cf.setFocus(((EditText)findViewById(R.id.sizetxt)));
				}
				else
				{
					if(((EditText)findViewById(R.id.typetxt)).getText().toString().equals(""))
					{
						cf.ShowToast("Please enter the Type", 0);
						cf.setFocus(((EditText)findViewById(R.id.typetxt)));
					}
					else
					{
						if(tasrd_val.equals("Yes"))
						{
							if(((EditText)findViewById(R.id.tagedit)).getText().toString().trim().equals(""))
							{
								cf.ShowToast("Please select Date.", 0);
								cf.setFocus(((EditText)findViewById(R.id.tagedit)));
							}
							else
							{ 
								if(cf.checkfortodaysdate(((EditText)findViewById(R.id.tagedit)).getText().toString())=="false")
								{
									cf.ShowToast("Date should not be Greater than Current Year/Month/Date.",0);
									((EditText)findViewById(R.id.tagedit)).setText("");
									cf.setFocus(((EditText)findViewById(R.id.tagedit)));
								}
								else
								{
								  smokedetectors();
								}
							}
						}
						else
						{
							smokedetectors();
						}
					}
				}
			}
			
			
			break;
		}
	}
	private void smokedetectors() {
		// TODO Auto-generated method stub
		if(sdrd_val.equals(""))
		{
			cf.ShowToast("Please select the option for Smoke Detectors Present.", 0);
		}
		else
		{
			if(sdrd_val.equals("Yes"))
			{
				if(sdord_val.equals(""))
				{
					cf.ShowToast("Please select the option for Smoke Detectors Present - Yes.", 0);
				}
				else
				{
					next();
				}
			}
			else
			{
				next();
			}
		}
	}
	private void next() {
		// TODO Auto-generated method stub
		if(scrd_val.equals(""))
		{
			cf.ShowToast("Please select the option for Sprinkler Coverage.", 0);
		}
		else
		{
			if(ssard_val.equals(""))
			{
				cf.ShowToast("Please select the option for Sprinkler System Alarm.", 0);
			}
			else
			{
				if(sscrd_val.equals(""))
				{
					cf.ShowToast("Please select the option for Signs in Satisfactory Condition.", 0);
				}
				else
				{
					FireInsert();
					((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
					cf.ShowToast("Fire Protection/Survey saved successfully.", 0);
					cf.gotoclass(35, WindMitCommRestsupp.class);	
				}
			}
		}
	}
	

	private void FireInsert() {
		// TODO Auto-generated method stub
			Cursor FPS_save=null;
		try
		{
			FPS_save=cf.SelectTablefunction(cf.GCH_FireProtecttbl, " where fld_srid='"+cf.selectedhomeid+"'");
			if(FPS_save.getCount()>0)
			{
				try
				{
					cf.arr_db.execSQL("UPDATE "+cf.GCH_FireProtecttbl+ " set fld_fpschk='"+fpschkval+"',fld_fireext='"+ferd_val+"',fld_firedet='"+fdrd_val+"',"+
				                                "fld_size='"+cf.encode(((EditText)findViewById(R.id.sizetxt)).getText().toString())+"',"+
							                    "fld_type='"+cf.encode(((EditText)findViewById(R.id.typetxt)).getText().toString())+"',fld_wservice='"+wsrd_val+"',"+
							                    "fld_taged='"+tasrd_val+"',fld_tagdate='"+cf.encode(((EditText)findViewById(R.id.tagedit)).getText().toString())+"',"+
				                                "fld_smokedet='"+sdrd_val+"',fld_smokoptions='"+sdord_val+"',fld_ssys='"+ssrd_val+"',fld_scover='"+scrd_val+"',fld_salarm='"+ssard_val+"',"+
							                    "fld_sign='"+sscrd_val+"' where fld_srid='"+cf.selectedhomeid+"'");
					 /*((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
					cf.ShowToast("Fire Protection/Survey saved successfully.", 0);
				     cf.goclass(46);*/
				}
				catch (Exception E)
				{
					String strerrorlog="Updating the Fire Protection/Survey - GCH";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
			}
			else
			{
				try
				{
					cf.arr_db.execSQL("INSERT INTO "
						+ cf.GCH_FireProtecttbl
						+ " (fld_srid,fld_fpschk,fld_fireext,fld_firedet,fld_size,fld_type,fld_wservice,fld_taged,fld_tagdate,fld_smokedet,fld_smokoptions,fld_ssys,fld_scover,fld_salarm,fld_sign)"
						+ "VALUES ('"+cf.selectedhomeid+"','"+fpschkval+"','"+ferd_val+"','"+fdrd_val+"',"+
		                   "'"+cf.encode(((EditText)findViewById(R.id.sizetxt)).getText().toString())+"',"+
		                   "'"+cf.encode(((EditText)findViewById(R.id.typetxt)).getText().toString())+"',"+
                           "'"+wsrd_val+"','"+tasrd_val+"','"+cf.encode(((EditText)findViewById(R.id.tagedit)).getText().toString())+"',"+
		                   "'"+sdrd_val+"','"+sdord_val+"','"+ssrd_val+"','"+scrd_val+"','"+ssard_val+"','"+sscrd_val+"')");
					/*((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
					cf.ShowToast("Fire Protection/Survey saved successfully.", 0);
				 cf.goclass(46);*/
				}
				catch (Exception E)
				{
					String strerrorlog="Inserting the Fire Protection/Survey - GCH";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
				
			}
		}
		catch (Exception E)
		{
				String strerrorlog="Checking the rows inserted in the GCH Fire Protection Survey table.";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			cf.goclass(46);
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}
	private void chk_values() {
		// TODO Auto-generated method stub
		try
		{
		   Cursor fp_retrive=cf.SelectTablefunction(cf.GCH_FireProtecttbl, " where fld_srid='"+cf.selectedhomeid+"'");
		   int rws = fp_retrive.getCount();
			if(rws>0){
				fp_retrive.moveToFirst();
				retferdtxt = fp_retrive.getString(fp_retrive.getColumnIndex("fld_size"));
			}
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}
	public void showunchkalert(final int i) {
		// TODO Auto-generated method stub
		AlertDialog.Builder bl = new Builder(FireProtection.this);
		bl.setTitle("Confirmation");
		bl.setMessage(Html.fromHtml("Do you want to clear the Fire Protection/Survey data?"));
		bl.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog,
											int id) {
										 switch(i){
										 case 1:
											 ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.GONE);
							    			 clearfp();//FireInsert();
							    			 //cf.arr_db.execSQL("Delete from "+cf.GCH_FireProtecttbl+" Where fld_srid='"+cf.selectedhomeid+"'");
											 set_defaultchk();
							    			 ((LinearLayout)findViewById(R.id.fps_table)).setVisibility(cf.v1.GONE);
							    			 break;
		                                 
										 }
										 	 
										 
									}
		});
		bl.setNegativeButton("No",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog,
							int id) {
						switch(i){
						 case 1:
							 ((CheckBox)findViewById(R.id.fps_na)).setChecked(false);
							 break;
					
						 }
					}
        });
		AlertDialog al=bl.create();
		al.setIcon(R.drawable.alertmsg);
		al.setCancelable(false);
		al.show();
		
		
	}
	protected void clearfp() {
		// TODO Auto-generated method stub
		 fpschkval=0;ferd_val="";fdrd_val="";wsrd_val="";tasrd_val="";
		 sdrd_val="";ssrd_val="";scrd_val="";ssard_val="";sscrd_val="";
		 sschk=true;scchk=true;ssachk=true;sscchk=true;retferdtxt="";
		 try{if(sschk){sdrdgval.clearCheck();}}catch(Exception e){}
		 try{if(scchk){scrdgval.clearCheck();}}catch(Exception e){}
		 try{if(ssachk){ssardgval.clearCheck();}}catch(Exception e){}
		 try{if(sscchk){sscrdgval.clearCheck();}}catch(Exception e){}
		 ((EditText)findViewById(R.id.sizetxt)).setText("");szetxt="";
		 ((EditText)findViewById(R.id.typetxt)).setText("");
		 ((EditText)findViewById(R.id.tagedit)).setText("");
	}
	protected void set_defaultchk() {
		// TODO Auto-generated method stub
		((RadioButton) ferdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) fdrdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) wsrdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) tasrdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) ssrdgval.findViewWithTag("No")).setChecked(true);
		tasrd_val="No";ferd_val="No";fdrd_val="No";wsrd_val="No";sdrd_val="";ssrd_val="No";scrd_val="";ssard_val="";
				sscrd_val="";sdord_val="";szetxt="";
	
	}
}
