/*
   ************************************************************
   * Project: ALL RISK REPORT
   * Module : ChineseDrywall.java
   * Creation History:
      Created By : Gowri on 1/16/2013
   * Modification History:
      Last Modified By : Gowri on 04/04/2013
   ************************************************************  
*/package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;

import java.lang.reflect.Type;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethod;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class DrywallHVAC extends Activity{
	CommonFunctions cf;
	boolean load_comment=true;
	private static final int DLG_EXAMPLE1 = 0;
	private static final int TEXT_ID = 0;
	Spinner atticnospin,hvacnospin,hvacuntacessspin,hvacrecrepspin,hvaccopspin,hvacdarkspin;
	ArrayAdapter adapter,hvuntadap,hvacesadap,hvrecadap,hvcopadap,hvdrkkadap;
	TableLayout hvactblshow;
	String hvacunt[] = {"--Select--","1","2","3","4","5"};
    String hvacuntacess[] = {"--Select--","Full","Partial","No"};
    String hvacrecrep[] = {"--Select--","Yes","No","Not Determined","None Observed/Confirmed"};
    String hvaccop[] = {"--Select--","Yes","None Noted","Not Confirmed"};
    String hvacnostr="",hvacuntacessstr="",hvacrecrepstr="",hvaccoprstr="",hvacdarkstr="";
    int ihint=0,hvacCurrent_select_id;
    Cursor Drysum_save,Dryhvac_save;
    String[] hvac_in_DB;
    
	@Override
	   public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        cf=new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);
				
		 	}
	        setContentView(R.layout.drywallhvac);
	        cf.getDeviceDimensions();
	        
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
			hdr_layout.addView(new HdrOnclickListener(this,1,"Chinese Drywall","Internal HVAC System(s)",6,0,cf));
	        
	        LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
	        mainmenu_layout.setMinimumWidth(cf.wd);
	        mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 6, 0,0,cf));
	        LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 63, 1,0,cf));
	        TableRow tblrw = (TableRow)findViewById(R.id.row2);
			tblrw.setMinimumHeight(cf.ht);
			cf.CreateARRTable(7);
			cf.CreateARRTable(72);
			declarations();
			setComments();
			Showhvacvalue();
	}
	private void setComments() {
		// TODO Auto-generated method stub
		 try
			{
			   Cursor SCf_retrive=cf.SelectTablefunction(cf.DRY_sumcond, " where fld_srid='"+cf.selectedhomeid+"'");
			   
			   if(SCf_retrive.getCount()>0)
			   {  
				   SCf_retrive.moveToFirst();
				   ((EditText)findViewById(R.id.hvaccomment)).setText(cf.decode(SCf_retrive.getString(SCf_retrive.getColumnIndex("hvaccomments"))));
			   }
			   
			}catch (Exception e) {
				// TODO: handle exception
			}
	}
	private void declarations() {
		// TODO Auto-generated method stub
		 cf.setupUI((ScrollView) findViewById(R.id.scr));
		((EditText)findViewById(R.id.hvaccomment)).addTextChangedListener(new textwatcher(1));
		
		 hvacnospin=(Spinner) findViewById(R.id.hvacno_spin);
		 hvuntadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, hvacunt);
		 hvuntadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 hvacnospin.setAdapter(hvuntadap);
		 hvacnospin.setOnItemSelectedListener(new  Spin_Selectedlistener(1));
		 
		 hvacuntacessspin=(Spinner) findViewById(R.id.unitacess_spin);
		 hvacesadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, hvacuntacess);
		 hvacesadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 hvacuntacessspin.setAdapter(hvacesadap);
		 hvacuntacessspin.setOnItemSelectedListener(new  Spin_Selectedlistener(2));
		 
		 hvacrecrepspin=(Spinner) findViewById(R.id.recrepair_spin);
		 hvrecadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, hvacrecrep);
		 hvrecadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 hvacrecrepspin.setAdapter(hvrecadap);
		 hvacrecrepspin.setOnItemSelectedListener(new  Spin_Selectedlistener(3));
		 hvacrecrepspin.setSelection(4);
		 
		 hvaccopspin=(Spinner) findViewById(R.id.copcorosn_spin);
		 hvcopadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, hvaccop);
		 hvcopadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 hvaccopspin.setAdapter(hvcopadap);
		 hvaccopspin.setOnItemSelectedListener(new  Spin_Selectedlistener(4));
		 hvaccopspin.setSelection(2);
		 
		 hvacdarkspin=(Spinner) findViewById(R.id.dark_spin);
		 hvdrkkadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, hvaccop);
		 hvdrkkadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 hvacdarkspin.setAdapter(hvdrkkadap);
		 hvacdarkspin.setOnItemSelectedListener(new  Spin_Selectedlistener(5));
		 hvacdarkspin.setSelection(2);
		 
		 hvactblshow =(TableLayout)findViewById(R.id.hvacvalue);
	}
	class Spin_Selectedlistener implements OnItemSelectedListener
	{
		int i;
		public Spin_Selectedlistener(int i) {
			// TODO Auto-generated constructor stub
			this.i=i;
		}

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			if(i==1){
				hvacnostr = hvacnospin.getSelectedItem().toString();
	            if(hvacnostr.equals("--Select--")){
					
				}else{
					if(ihint==0){defaulthvac();}
				}
			}
			else if(i==2)
			{
				hvacuntacessstr = hvacuntacessspin.getSelectedItem().toString();
			}
			else if(i==3)
			{
				hvacrecrepstr = hvacrecrepspin.getSelectedItem().toString();	
			}
			else if(i==4)
			{
				hvaccoprstr = hvaccopspin.getSelectedItem().toString();	
			}
			else if(i==5)
			{
				hvacdarkstr = hvacdarkspin.getSelectedItem().toString();	
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	class textwatcher implements TextWatcher
    {
	         public int type;
	         textwatcher(int type)
	    	{
	    		this.type=type;
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
	    		if(this.type==1)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.hvac_tv_type1),500); 
	    			if(s.toString().startsWith(" "))
	    			{
	    				((EditText)findViewById(R.id.hvaccomment)).setText("");	
	    			}
	    		}
	    	
	         }

	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,int after) {
	    		// TODO Auto-generated method stub
	     	}

	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,int count) {
	    		// TODO Auto-generated method stub
	    	}
	}	
	public void clicker(View v)
	{
		switch (v.getId()) {
			case R.id.hme:
				cf.gohome();
				break;
			case R.id.loadcomments:/***Call for the comments***/
				int len=((EditText)findViewById(R.id.hvaccomment)).getText().toString().length();
				if(load_comment )
				{
						load_comment=false;
						int loc[] = new int[2];
						v.getLocationOnScreen(loc);
						Intent in1 = cf.loadComments("Internal HVAC System(s) Comments",loc);
						in1.putExtra("length", len);
						in1.putExtra("max_length", 500);
						startActivityForResult(in1, cf.loadcomment_code);
				}
				break;
			case R.id.dryhvacclear:
				ihint=0;defaulthvac();
		        hvacnospin.setSelection(0);//hvacnospin.setEnabled(true);
		        ((Button)findViewById(R.id.addhvac)).setText("Save/Add More HVAC Unit");
				break;
			case R.id.addhvac:
				ihint=2;
				if(hvacnostr.equals("--Select--")){
					cf.ShowToast("Please select HVAC Unit.", 0);
				}
				else
				{
					if(((EditText)findViewById(R.id.unitloc_etxt)).getText().toString().length()>0 && ((EditText)findViewById(R.id.unitloc_etxt)).getText().toString().trim().equals(""))
					{
						  cf.ShowToast("Please enter the Unit Location.", 0);
						  cf.setFocus(((EditText)findViewById(R.id.unitloc_etxt)));
						  ((EditText)findViewById(R.id.unitloc_etxt)).setText("");
					}
					else
					{
						if(((EditText)findViewById(R.id.manuf_etxt)).getText().toString().length()>0 && ((EditText)findViewById(R.id.manuf_etxt)).getText().toString().trim().equals(""))
						{
							  cf.ShowToast("Please enter Manufacturer.", 0);
							  cf.setFocus(((EditText)findViewById(R.id.manuf_etxt)));
							  ((EditText)findViewById(R.id.manuf_etxt)).setText("");
						}
						else
						{
							next();
						}
					}
				}
				break;
			case R.id.save:
				chk_values();
				if(Dryhvac_save.getCount()==0)
				{
					cf.ShowToast("Atleast one Internal HVAC System(s) should be added and saved.", 0);
				}
				else
				{
					/*if(((EditText)findViewById(R.id.hvaccomment)).getText().toString().trim().equals(""))
					{
						cf.ShowToast("Please enter Internal HVAC System Comments.", 0);
						cf.setFocus(((EditText)findViewById(R.id.hvaccomment)));
					}
					else
					{*/
						if(Drysum_save.getCount()>0)
						{
							try
							{
								cf.arr_db.execSQL("UPDATE "+cf.DRY_sumcond+ " set hvaccomments='"+cf.encode(((EditText)findViewById(R.id.hvaccomment)).getText().toString())+"' where fld_srid='"+cf.selectedhomeid+"'");
								cf.ShowToast("Internal HVAC System(s) saved successfully.", 1);
							}
							catch (Exception E)
							{
								String strerrorlog="Updating the HVAC - Drywall";
								cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
							}
						}
						else
						{
							try
							{
								cf.arr_db.execSQL("INSERT INTO "
										+ cf.DRY_sumcond
										+ " (fld_srid,presofsulfur,obscopper,presofdry,addcomments,drywallcomments,atticcomments,"+
										   "applianccomments,hvaccomments,assesmentcomments)"
										+ "VALUES('"+cf.selectedhomeid+"','','',"+
										"'','','',"+
										"'','','"+cf.encode(((EditText)findViewById(R.id.hvaccomment)).getText().toString())+"','')");
								 cf.ShowToast("Internal HVAC System(s) saved successfully.", 1);
							}
							catch (Exception E)
							{
								String strerrorlog="Inserting the HVAC - Drywall";
								cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
							}
						}
						((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
					    cf.goclass(64);
					//}
				}
				
				break;
				
	   }
		
	}
	private void next() {
		// TODO Auto-generated method stub
		if(hvacuntacessstr.equals("--Select--")){
			cf.ShowToast("Please select Unit Accessible.", 0);
		}else{
			if(hvacrecrepstr.equals("--Select--")){
				cf.ShowToast("Please select Recent Repairs to Units.", 0);
			}else{
				if(hvaccoprstr.equals("--Select--")){
					cf.ShowToast("Please select Copper Corrosion Observed.", 0);
				}else{
					if(hvacdarkstr.equals("--Select--")){
						cf.ShowToast("Please select Darkening/Stains noted on Fixtures/Fittings.", 0);
					}else{
						try
						{
							if(((Button)findViewById(R.id.addhvac)).getText().toString().equals("Update")){
                                 cf.arr_db.execSQL("UPDATE  "+cf.DRY_hvac+" SET hvacnum='"+hvacnostr+"',"+
							           "unitloc='"+cf.encode(((EditText)findViewById(R.id.unitloc_etxt)).getText().toString())+"',"+
									   "manufacturer='"+cf.encode(((EditText)findViewById(R.id.manuf_etxt)).getText().toString())+"',"+
							           "unitacess='"+hvacuntacessstr+"',recentrepairs='"+hvacrecrepstr+"',coppercorrs='"+hvaccoprstr+"',"+
									   "darkstains='"+hvacdarkstr+"' Where hvacId='"+hvacCurrent_select_id+"' ");
                                 defaulthvac();hvacnospin.setSelection(0);//hvacnospin.setEnabled(true);
								cf.ShowToast("HVAC unit updated successfully.", 0);
								((Button)findViewById(R.id.addhvac)).setText("Save/Add More HVAC Unit");
								Showhvacvalue();ihint=0;
							}else{
								chk_values();
								boolean[] b1 = {false};
								if(Dryhvac_save.getCount()==0){
									
								}else{
									/*if(hvac_in_DB!=null)
									{
										for(int i=0;i<hvac_in_DB.length;i++)
										{
											if((hvac_in_DB[i].equals(hvacnostr)))
											{
												b1[0]=true;
											}
										}
									}*/
								}
								/*if(Dryhvac_save.getCount()==5){
									defaulthvac();hvacnospin.setSelection(0);hvacnospin.setEnabled(true);
									cf.ShowToast("Limit Exceeds! You have already added 5 HVAC Unit.", 0);
								}else{*/
									/*if(!b1[0]){*/cf.arr_db.execSQL("INSERT INTO "
											+ cf.DRY_hvac
											+ " (fld_srid,hvacnum,unitloc,manufacturer,unitacess,recentrepairs,coppercorrs,darkstains)"
											+ "VALUES('"+cf.selectedhomeid+"','"+hvacnostr+"',"+
											"'"+cf.encode(((EditText)findViewById(R.id.unitloc_etxt)).getText().toString())+"',"+
											"'"+cf.encode(((EditText)findViewById(R.id.manuf_etxt)).getText().toString())+"',"+
											"'"+hvacuntacessstr+"','"+hvacrecrepstr+"','"+hvaccoprstr+"','"+hvacdarkstr+"')");
									 cf.ShowToast("Internal HVAC System added Successfully.", 1);
									 defaulthvac();hvacnospin.setSelection(0);//hvacnospin.setEnabled(true);
										
									/*}else{cf.ShowToast("HVAC Unit already exists. Please select another HVAC Unit.", 0);
									hvacnospin.setSelection(0);
									}*/
									Showhvacvalue();
								//}
									
							}
						}
						catch(Exception e){}
					}
				}
			}
		}
	}
	private void Showhvacvalue() {
		// TODO Auto-generated method stub
		Cursor hvac_retrieve= cf.SelectTablefunction(cf.DRY_hvac, " Where fld_srid='"+cf.selectedhomeid+"' ");
	       if(hvac_retrieve.getCount()>0)
			{
	    	   if(!((EditText)findViewById(R.id.hvaccomment)).getText().toString().trim().equals("")){((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);}
	    	   hvac_retrieve.moveToFirst();
				try
				{
					hvactblshow.removeAllViews();
				}
				catch (Exception e) {
					// TODO: handle exception
				}
				LinearLayout h1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvac_listview_header, null); 
				TableRow thv = (TableRow)h1.findViewById(R.id.attic);thv.setVisibility(cf.v1.GONE);
				TableRow th = (TableRow)h1.findViewById(R.id.hvac);th.setVisibility(cf.v1.VISIBLE);
				TableLayout.LayoutParams lp = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
			 	lp.setMargins(2, 0, 2, 2);h1.removeAllViews(); 
			 	th.setPadding(10, 0, 0, 0);
			 	
			 	hvactblshow.addView(th,lp);
				hvac_in_DB=new String[hvac_retrieve.getCount()];
				hvactblshow.setVisibility(View.VISIBLE); 
				
				for(int i=0;i<hvac_retrieve.getCount();i++)
				{
					TextView no,hvacunitno,hvacunitloc,hvacmanuf,hvacunitaccess,hvacrecrepairs,hvaccopper,hvacdark;
					ImageView edit,delete,duplicate;
					String manuf="",untloc="",hvsp="",untaces="",recrep="",copcor="",drkstn="";
					hvsp=hvac_retrieve.getString(2);
					untloc=cf.decode(hvac_retrieve.getString(3));
					manuf=cf.decode(hvac_retrieve.getString(4));
					untaces=hvac_retrieve.getString(5);
					recrep=hvac_retrieve.getString(6);
					copcor=hvac_retrieve.getString(7);
					drkstn=hvac_retrieve.getString(8);
					
					hvac_in_DB[i]=hvsp;
					
					LinearLayout t1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvac_listview, null);
					TableRow t = (TableRow)t1.findViewById(R.id.hvac);t.setVisibility(cf.v1.VISIBLE);
					TableRow at = (TableRow)t1.findViewById(R.id.attic);at.setVisibility(cf.v1.GONE);
					t.setId(44444+i);/// Set some id for further use
				 	
				 	no= (TextView) t.findViewWithTag("No");
				 	no.setText(String.valueOf(i+1));
				 	hvacunitno= (TextView) t.findViewWithTag("HVACUnit");
				 	hvacunitno.setText(hvsp);
				 	hvacunitloc= (TextView) t.findViewWithTag("UnitLocation");
				 	if(untloc.equals(""))
				 	{
				 		hvacunitloc.setText("N/A");
				 	}
				 	else
				 	{
				 		hvacunitloc.setText(untloc);
				 	}
				 	
				 	hvacmanuf= (TextView) t.findViewWithTag("Manufacturer");
				 	if(manuf.equals(""))
				 	{
				 		hvacmanuf.setText("N/A");
				 	}
				 	else
				 	{
				 		hvacmanuf.setText(manuf);
				 	}
				 	
				 	hvacunitaccess= (TextView) t.findViewWithTag("UnitAccessible");
				 	hvacunitaccess.setText(untaces);
				 	hvacrecrepairs= (TextView) t.findViewWithTag("RecentRepairs");
				 	hvacrecrepairs.setText(recrep);
				 	hvaccopper= (TextView) t.findViewWithTag("CopperCorrosion");
				 	hvaccopper.setText(copcor);
				 	hvacdark= (TextView) t.findViewWithTag("Darkening");
				 	hvacdark.setText(drkstn);
				 	
				 	edit= (ImageView) t.findViewWithTag("edit");
				 	edit.setId(789456+i);
				 	edit.setTag(hvac_retrieve.getString(0));
	                edit.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
						try
						{
						int i=Integer.parseInt(v.getTag().toString());
						
						 ihint=1; updatehvac(i);
						}
						catch (Exception e) {
							// TODO: handle exception
						}
						}
	                });
						
				 	delete=(ImageView) t.findViewWithTag("del");
				 	delete.setTag(hvac_retrieve.getString(0));
	                delete.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(final View v) {
							// TODO Auto-generated method stub
							
							AlertDialog.Builder builder = new AlertDialog.Builder(DrywallHVAC.this);
							builder.setMessage("Do you want to delete the selected HVAC Unit?")
									.setCancelable(false)
									.setPositiveButton("Yes",
											new DialogInterface.OnClickListener() {

												public void onClick(DialogInterface dialog,
														int id) {
													
													try
													{
													int i=Integer.parseInt(v.getTag().toString());
													if(i==hvacCurrent_select_id)
													{
														hvacCurrent_select_id=0;
													}
													cf.arr_db.execSQL("Delete from "+cf.DRY_hvac+" Where hvacId='"+i+"'");
													cf.ShowToast("Internal HVAC System deleted successfully.", 0);
													defaulthvac();hvacnospin.setSelection(0);//hvacnospin.setEnabled(true);
											        ((Button)findViewById(R.id.addhvac)).setText("Save/Add More HVAC Unit");
													Showhvacvalue();ihint=0;
													}
													catch (Exception e) {
														// TODO: handle exception
													}
												}
											})
									.setNegativeButton("No",
											new DialogInterface.OnClickListener() {
												public void onClick(DialogInterface dialog,
														int id) {

													dialog.cancel();
												}
											});
							AlertDialog al=builder.create();
							al.setTitle("Confirmation");
							al.setIcon(R.drawable.alertmsg);
							al.setCancelable(false);
							al.show();
						
							
						}
					});
	                
	                duplicate= (ImageView) t.findViewWithTag("dup");
	                duplicate.setId(789456+i);
	                duplicate.setTag(hvac_retrieve.getString(0));
	                duplicate.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
						try
						{
						final int i=Integer.parseInt(v.getTag().toString());
						String hvunt="";
						try{
							Cursor hvacret=cf.SelectTablefunction(cf.DRY_hvac, " Where hvacId='"+i+"'");
							if(hvacret.getCount()>0)
							{
								hvacret.moveToFirst();
								hvunt=hvacret.getString(2);
								
							}
						}catch(Exception e){}
						AlertDialog.Builder builder = new AlertDialog.Builder(DrywallHVAC.this);
						builder.setMessage("Are you sure, Do you want to duplicate the HVAC Unit "+hvunt+" ?")
								.setCancelable(false)
								.setPositiveButton("Yes",
										new DialogInterface.OnClickListener() {

											public void onClick(DialogInterface dialog,
													int id) {
												LayoutInflater factory = LayoutInflater.from(DrywallHVAC.this);
                                                final View textEntryView = factory.inflate(R.layout.duplicatealert, null);
                                                final EditText input1 = (EditText) textEntryView.findViewById(R.id.dupid);
                                                
                                                InputFilter[] FilterArray = new InputFilter[1];  
                        						FilterArray[0] = new InputFilter.LengthFilter(1);  
                        						input1.setFilters(FilterArray);
                                                
                                                
												input1.setText("", TextView.BufferType.EDITABLE);
												
												final AlertDialog.Builder alert = new AlertDialog.Builder(DrywallHVAC.this);
												alert.setCancelable(false);
												alert.setIcon(R.drawable.yell_alert_icon).setTitle(
												"Duplication:").setView(
												textEntryView).setPositiveButton("Duplicate",
												new DialogInterface.OnClickListener() {
												public void onClick(DialogInterface dialog,
												int whichButton) {
													 cf.hidekeyboard(input1);
													try{
														
														Cursor hvacret=cf.SelectTablefunction(cf.DRY_hvac, " Where hvacId='"+i+"'");
														if(hvacret.getCount()>0)
														{
															hvacret.moveToFirst();
															String manuf="",untloc="",hvunit="",untaces="",recrep="",copcor="",drkstn="";
															hvunit=hvacret.getString(2);
															untloc=cf.decode(hvacret.getString(3));
															manuf=cf.decode(hvacret.getString(4));
															untaces=hvacret.getString(5);
															recrep=hvacret.getString(6);
															copcor=hvacret.getString(7);
															drkstn=hvacret.getString(8);
															System.out.println("length"+input1.getText().toString());
															int j;
															for(j=0;j<Integer.parseInt(input1.getText().toString());)
															{
																System.out.println("j"+j);
																cf.arr_db.execSQL("INSERT INTO "
																		+ cf.DRY_hvac
																		+ " (fld_srid,hvacnum,unitloc,manufacturer,unitacess,recentrepairs,coppercorrs,darkstains)"
																		+ "VALUES('"+cf.selectedhomeid+"','"+hvunit+"',"+
																		"'"+cf.encode(untloc)+"',"+
																		"'"+cf.encode(manuf)+"',"+
																		"'"+untaces+"','"+recrep+"','"+copcor+"','"+drkstn+"')");
																j=j+1;System.out.println("iafter"+j);
																
															}
															 
															 defaulthvac();hvacnospin.setSelection(0);//hvacnospin.setEnabled(true);
															 cf.ShowToast("Data Duplicated Successfully.", 0);
															 Showhvacvalue();
															
															 dialog.dismiss();
															
														}
													}catch(Exception e){}
													dialog.dismiss();
												
												}
												}).setNegativeButton("Cancel",
												new DialogInterface.OnClickListener() {
												public void onClick(DialogInterface dialog,
												int whichButton) {
													dialog.dismiss();
												}
												});
												alert.show();
											
											}
										})
								.setNegativeButton("No",
										new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog,
													int id) {

												dialog.cancel();
											}
										});
						AlertDialog al=builder.create();
						al.setTitle("Confirmation");
						al.setIcon(R.drawable.alertmsg);
						al.setCancelable(false);
						al.show();
						 
						}
						catch (Exception e) {
							// TODO: handle exception
						}
						}
	                });
	                t1.removeAllViews();
					t.setPadding(10, 0, 0, 0);
					hvactblshow.addView(t,lp);
					hvac_retrieve.moveToNext();
				}
			}else{hvactblshow.setVisibility(cf.v1.GONE);((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.GONE); }
	}
	protected void updatehvac(int i) {
		// TODO Auto-generated method stub
		hvacCurrent_select_id = i;//hvacnospin.setEnabled(false);
		try{Cursor hvacret=cf.SelectTablefunction(cf.DRY_hvac, " Where hvacId='"+i+"'");
		if(hvacret.getCount()>0)
		{
			hvacret.moveToFirst();
			String manuf="",untloc="",hvunit="",untaces="",recrep="",copcor="",drkstn="";
			hvunit=hvacret.getString(2);
			untloc=cf.decode(hvacret.getString(3));
			manuf=cf.decode(hvacret.getString(4));
			untaces=hvacret.getString(5);
			recrep=hvacret.getString(6);
			copcor=hvacret.getString(7);
			drkstn=hvacret.getString(8);
			int pos=hvuntadap.getPosition(hvunit);hvacnospin.setSelection(pos);
			int recpos=hvrecadap.getPosition(recrep);hvacrecrepspin.setSelection(recpos);
			int coppos=hvcopadap.getPosition(copcor);hvaccopspin.setSelection(coppos);
			int darkpos=hvdrkkadap.getPosition(drkstn);hvacdarkspin.setSelection(darkpos);
			int unitpos=hvacesadap.getPosition(untaces);hvacuntacessspin.setSelection(unitpos);
			
			((EditText)findViewById(R.id.unitloc_etxt)).setText(untloc);
			((EditText)findViewById(R.id.manuf_etxt)).setText(manuf);
			((Button)findViewById(R.id.addhvac)).setText("Update");
		}
		}catch(Exception e){}
	}
	private void chk_values() {
		// TODO Auto-generated method stub
		try
		{
			Drysum_save=cf.SelectTablefunction(cf.DRY_sumcond, " where fld_srid='"+cf.selectedhomeid+"'");
		   
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		
		try{
			Dryhvac_save=cf.SelectTablefunction(cf.DRY_hvac, " where fld_srid='"+cf.selectedhomeid+"'");
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		
		
	
	}
	public void defaulthvac() {
		// TODO Auto-generated method stub
		((EditText)findViewById(R.id.unitloc_etxt)).setText("");
		((EditText)findViewById(R.id.manuf_etxt)).setText("");
		hvacuntacessspin.setSelection(0);
		hvacrecrepspin.setSelection(4);
		hvaccopspin.setSelection(2);
		hvacdarkspin.setSelection(2);
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			cf.goclass(62);
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		  if(requestCode==cf.loadcomment_code)
		  {
			load_comment=true;
			if(resultCode==RESULT_OK)
			{
				 cf.ShowToast("Comments added successfully.", 0);
				 String atccomts = ((EditText)findViewById(R.id.hvaccomment)).getText().toString();
				 ((EditText)findViewById(R.id.hvaccomment)).setText((atccomts+" "+data.getExtras().getString("Comments")).trim());
				
			}
			
		}
    }
	
}
