/*
   ************************************************************
   * Module : SummaryHazards.java
   * Creation History:
      Created By : Gowri on 
   * Modification History:
      Last Modified By : Gowri on 04/04/2013
   ************************************************************  
*/
package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;


public class SummaryHazards extends Activity {
	CommonFunctions cf;
	int shchkval=0;
	public RadioButton[] thrd_opt =  new RadioButton[8];
	RadioGroup vpprdgval,hoprdgval,obltrrdgval,tprdgval,srprdgval,brprdgval,usnrdgval,pdrdgval,nscnrdgval,
			   oanrdgval,ofprdgval,wsrrdgval,edrdgval,boprdgval,bgdrdgval,vpdnrdgval,spuwrdgval,ivnrdgval,rdrrdgval,
			   pcdprdgval,occdprdgval,nwssrdgval,nwsdrdgval;
	String vpprdgtxt="No",hoprdgtxt="No",triphazard_val="",boprdgtxt="No",obltrrdgtxt="No",tprdgtxt="No",srprdgtxt="No",
		   brprdgtxt="No",usnrdgtxt="No",pdrdgtxt="No",nscnrdgtxt="No",oanrdgtxt="No",ofprdgtxt="No",wsrrdgtxt="No",
		   edrdgtxt="No",bgdrdgtxt="No",vpdnrdgtxt="No",spuwrdgtxt="No",ivnrdgtxt="No",rdrrdgtxt="Beyond Scope of Inspection",
		   pcdprdgtxt="Beyond Scope of Inspection",occdprdgtxt="Beyond Scope of Inspection",
		   nwssrdgtxt="Beyond Scope of Inspection",nwsdrdgtxt="Beyond Scope of Inspection",retvprdtxt="";

	public boolean thnck=false,load_comment=true;
	
	@Override
	   public void onCreate(Bundle savedInstanceState) {


	        super.onCreate(savedInstanceState);
	        cf=new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);
				
		 	}
	        setContentView(R.layout.summaryhazards);
	        cf.getDeviceDimensions();
	        
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
			hdr_layout.addView(new HdrOnclickListener(this,1,"General Conditions & Hazards","Hazards & Concerns",4,0,cf));
	        LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
	        mainmenu_layout.setMinimumWidth(cf.wd);
	        mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 4, 0,0,cf));
	        LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 44, 1,0,cf));
			TableRow tblrw = (TableRow)findViewById(R.id.row2);
			tblrw.setMinimumHeight(cf.ht);
			declarations();
			cf.CreateARRTable(44);
			SummaryHazards_SetValue();
	}
	private void SummaryHazards_SetValue() {


		// TODO Auto-generated method stub
		try
		{
			chk_values();
		   Cursor SHZ_retrive=cf.SelectTablefunction(cf.GCH_SummaryHazardstbl, " where fld_srid='"+cf.selectedhomeid+"' AND (fld_shzchk='1' OR (fld_shzchk='0' AND fld_triphz<>''))");
		   if(SHZ_retrive.getCount()>0)
		   {  
			   SHZ_retrive.moveToFirst();
			   ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
			   if(SHZ_retrive.getInt(SHZ_retrive.getColumnIndex("fld_shzchk"))==1)
			   {
				   ((CheckBox)findViewById(R.id.sh_na)).setChecked(true);
				   ((LinearLayout)findViewById(R.id.sh_table)).setVisibility(cf.v1.GONE);
			   }
			   else
			   {
				   ((CheckBox)findViewById(R.id.sh_na)).setChecked(false);
				   ((LinearLayout)findViewById(R.id.sh_table)).setVisibility(cf.v1.VISIBLE);
				   
				   vpprdgtxt=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_vicious"));
				    ((RadioButton) vpprdgval.findViewWithTag(vpprdgtxt)).setChecked(true);
				    if(vpprdgtxt.equals("Yes"))
				    {	((LinearLayout)findViewById(R.id.viclin)).setVisibility(cf.v1.VISIBLE);
				        ((EditText)findViewById(R.id.viccomment)).setText(cf.decode(SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_viciousytxt"))));
				    }else{
				    	((LinearLayout)findViewById(R.id.viclin)).setVisibility(cf.v1.GONE);
			            ((EditText)findViewById(R.id.viccomment)).setText("");
				    }
				    
				    hoprdgtxt=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_horses"));
				    ((RadioButton) hoprdgval.findViewWithTag(hoprdgtxt)).setChecked(true);
				    if(hoprdgtxt.equals("Yes"))
				    {	((LinearLayout)findViewById(R.id.hllin)).setVisibility(cf.v1.VISIBLE);
				        ((EditText)findViewById(R.id.hlcomment)).setText(cf.decode(SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_horseytxt"))));
				    }else{
				    	((LinearLayout)findViewById(R.id.hllin)).setVisibility(cf.v1.GONE);
			            ((EditText)findViewById(R.id.hlcomment)).setText("");
				    }
				    
				    obltrrdgtxt=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_overhanging"));
				    ((RadioButton) obltrrdgval.findViewWithTag(obltrrdgtxt)).setChecked(true);
				    
				    tprdgtxt=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_trampoline"));
				    ((RadioButton) tprdgval.findViewWithTag(tprdgtxt)).setChecked(true);
				    
				    srprdgtxt=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_skateboard"));
				    ((RadioButton) srprdgval.findViewWithTag(srprdgtxt)).setChecked(true);
				    
				    brprdgtxt=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_bicycle"));
				    ((RadioButton) brprdgval.findViewWithTag(brprdgtxt)).setChecked(true);
				    
				    triphazard_val=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_triphz"));
				    System.out.println("rtriphazard_val"+triphazard_val);
				    cf.setRadioBtnValue(triphazard_val,thrd_opt);
				    if(triphazard_val.equals("None Observed")){
				    	((EditText)findViewById(R.id.th_otr)).setVisibility(cf.v1.GONE);
				    }else{
				    	((EditText)findViewById(R.id.th_otr)).setVisibility(cf.v1.VISIBLE);
				    	((EditText)findViewById(R.id.th_otr)).setText(cf.decode(SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_triphztxt"))));
				    }
				    
				    
				    usnrdgtxt=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_unsafe"));
				    ((RadioButton) usnrdgval.findViewWithTag(usnrdgtxt)).setChecked(true);
				    
				    pdrdgtxt=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_porchdeck"));
				    ((RadioButton) pdrdgval.findViewWithTag(pdrdgtxt)).setChecked(true);
				    
				    nscnrdgtxt=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_nonconst"));
				    ((RadioButton) nscnrdgval.findViewWithTag(nscnrdgtxt)).setChecked(true);
				    
				    oanrdgtxt=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_odoor"));
				    ((RadioButton) oanrdgval.findViewWithTag(oanrdgtxt)).setChecked(true);
				    
				    ofprdgtxt=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_openfound"));
				    ((RadioButton) ofprdgval.findViewWithTag(ofprdgtxt)).setChecked(true);
				    
				    wsrrdgtxt=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_wood"));
				    ((RadioButton) wsrrdgval.findViewWithTag(wsrrdgtxt)).setChecked(true);
				    
				    edrdgtxt=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_excessdb"));
				    ((RadioButton) edrdgval.findViewWithTag(edrdgtxt)).setChecked(true);
				    
				    boprdgtxt=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_bop"));
				    ((RadioButton) boprdgval.findViewWithTag(boprdgtxt)).setChecked(true);
				    if(boprdgtxt.equals("Yes"))
				    {	((LinearLayout)findViewById(R.id.bplin)).setVisibility(cf.v1.VISIBLE);
				        ((EditText)findViewById(R.id.bpcomment)).setText(cf.decode(SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_boptxt"))));
				    }else{
				    	((LinearLayout)findViewById(R.id.bplin)).setVisibility(cf.v1.GONE);
			            ((EditText)findViewById(R.id.bpcomment)).setText("");
				    }
				    
				    bgdrdgtxt=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_bgdisrepair"));
				    ((RadioButton) bgdrdgval.findViewWithTag(bgdrdgtxt)).setChecked(true);
				    
				    vpdnrdgtxt=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_visualpd"));
				    ((RadioButton) vpdnrdgval.findViewWithTag(vpdnrdgtxt)).setChecked(true);
				    
				    spuwrdgtxt=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_structp"));
				    ((RadioButton) spuwrdgval.findViewWithTag(spuwrdgtxt)).setChecked(true);
				    
				    ivnrdgtxt=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_inoper"));
				    ((RadioButton) ivnrdgval.findViewWithTag(ivnrdgtxt)).setChecked(true);
				    
				    rdrrdgtxt=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_recentdw"));
				    ((RadioButton) rdrrdgval.findViewWithTag(rdrrdgtxt)).setChecked(true);
				    
				    pcdprdgtxt=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_possiblecdw"));
				    ((RadioButton) pcdprdgval.findViewWithTag(pcdprdgtxt)).setChecked(true);
				    
				    occdprdgtxt=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_ownerccdw"));
				    ((RadioButton) occdprdgval.findViewWithTag(occdprdgtxt)).setChecked(true);
				    
				    nwssrdgtxt=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_nsssystem"));
				    ((RadioButton) nwssrdgval.findViewWithTag(nwssrdgtxt)).setChecked(true);
				    
				    nwsdrdgtxt=SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_nwsdedic"));
				    ((RadioButton) nwsdrdgval.findViewWithTag(nwsdrdgtxt)).setChecked(true);
				    
				    ((EditText)findViewById(R.id.sumcomment)).setText(cf.decode(SHZ_retrive.getString(SHZ_retrive.getColumnIndex("fld_summaryhzcomments"))));
			   }
		   }
		   else
		   {
			    cf.getinspectioname(cf.selectedhomeid);
			    if(cf.selinspname.equals("4"))
				{
					((CheckBox)findViewById(R.id.sh_na)).setChecked(true);
					((LinearLayout)findViewById(R.id.sh_table)).setVisibility(cf.v1.GONE);
				}
				else
				{
					((CheckBox)findViewById(R.id.sh_na)).setChecked(false);
					((LinearLayout)findViewById(R.id.sh_table)).setVisibility(cf.v1.VISIBLE);
				}
			   
		   }
		}
	    catch (Exception E)
		{
			String strerrorlog="Retrieving Pool Present data - GCH";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
	private void declarations() {

		// TODO Auto-generated method stub
		cf.setupUI((ScrollView) findViewById(R.id.scr));
		
    	vpprdgval = (RadioGroup)findViewById(R.id.vpps_rdg);
    	hoprdgval = (RadioGroup)findViewById(R.id.hop_rdg);
    	obltrrdgval = (RadioGroup)findViewById(R.id.ohltr_rdg);
    	tprdgval = (RadioGroup)findViewById(R.id.tp_rdg);
    	srprdgval = (RadioGroup)findViewById(R.id.srp_rdg);
    	brprdgval = (RadioGroup)findViewById(R.id.brp_rdg);
    	usnrdgval = (RadioGroup)findViewById(R.id.usn_rdg);
    	pdrdgval = (RadioGroup)findViewById(R.id.pd_rdg);
    	nscnrdgval = (RadioGroup)findViewById(R.id.nscn_rdg);
    	oanrdgval = (RadioGroup)findViewById(R.id.oan_rdg);
    	ofprdgval = (RadioGroup)findViewById(R.id.ofp_rdg);
    	wsrrdgval = (RadioGroup)findViewById(R.id.wsr_rdg);
    	edrdgval = (RadioGroup)findViewById(R.id.ed_rdg);
    	boprdgval = (RadioGroup)findViewById(R.id.bop_rdg);
    	bgdrdgval = (RadioGroup)findViewById(R.id.bgd_rdg);
    	vpdnrdgval = (RadioGroup)findViewById(R.id.vpdn_rdg);
    	spuwrdgval = (RadioGroup)findViewById(R.id.spuw_rdg);
    	ivnrdgval = (RadioGroup)findViewById(R.id.ivn_rdg);
    	rdrrdgval = (RadioGroup)findViewById(R.id.rdr_rdg);
    	pcdprdgval = (RadioGroup)findViewById(R.id.pcdp_rdg);
    	occdprdgval = (RadioGroup)findViewById(R.id.occdp_rdg);
    	nwssrdgval = (RadioGroup)findViewById(R.id.nwss_rdg);
    	nwsdrdgval = (RadioGroup)findViewById(R.id.nwsd_rdg);
    	
    	((EditText)findViewById(R.id.viccomment)).addTextChangedListener(new textwatcher(1));
    	((EditText)findViewById(R.id.hlcomment)).addTextChangedListener(new textwatcher(2));
     	
     	thrd_opt[0] = (RadioButton)findViewById(R.id.th_rd1);
     	thrd_opt[1] = (RadioButton)findViewById(R.id.th_rd2);
     	thrd_opt[2] = (RadioButton)findViewById(R.id.th_rd3);
     	thrd_opt[3] = (RadioButton)findViewById(R.id.th_rd4);
     	thrd_opt[4] = (RadioButton)findViewById(R.id.th_rd5);
     	thrd_opt[5] = (RadioButton)findViewById(R.id.th_rd6);
     	thrd_opt[6] = (RadioButton)findViewById(R.id.th_rd7);
     	thrd_opt[7] = (RadioButton)findViewById(R.id.th_rd8);
     	ClkEvnt_RadBtn_Trip(thrd_opt);   
    	((EditText)findViewById(R.id.bpcomment)).addTextChangedListener(new textwatcher(4));
    	((EditText)findViewById(R.id.sumcomment)).addTextChangedListener(new textwatcher(5));
    	
		set_defaultchk();
		
		 vpprdgval.setOnCheckedChangeListener(new checklistenetr(1));
		 hoprdgval.setOnCheckedChangeListener(new checklistenetr(2));
		 obltrrdgval.setOnCheckedChangeListener(new checklistenetr(3));
    	 tprdgval.setOnCheckedChangeListener(new checklistenetr(4));
    	 srprdgval.setOnCheckedChangeListener(new checklistenetr(5));
    	 brprdgval.setOnCheckedChangeListener(new checklistenetr(6));
    	 usnrdgval.setOnCheckedChangeListener(new checklistenetr(7));
    	 pdrdgval.setOnCheckedChangeListener(new checklistenetr(8));
    	 nscnrdgval.setOnCheckedChangeListener(new checklistenetr(9));
    	 oanrdgval.setOnCheckedChangeListener(new checklistenetr(10));
    	 ofprdgval.setOnCheckedChangeListener(new checklistenetr(11));
    	 wsrrdgval.setOnCheckedChangeListener(new checklistenetr(12));
    	 edrdgval.setOnCheckedChangeListener(new checklistenetr(13));
    	 boprdgval.setOnCheckedChangeListener(new checklistenetr(14));
    	 bgdrdgval.setOnCheckedChangeListener(new checklistenetr(15));
    	 vpdnrdgval.setOnCheckedChangeListener(new checklistenetr(16));
    	 spuwrdgval.setOnCheckedChangeListener(new checklistenetr(17));
    	 ivnrdgval.setOnCheckedChangeListener(new checklistenetr(18));
    	 rdrrdgval.setOnCheckedChangeListener(new checklistenetr(19));
    	 pcdprdgval.setOnCheckedChangeListener(new checklistenetr(20));
    	 occdprdgval.setOnCheckedChangeListener(new checklistenetr(21));
    	 nwssrdgval.setOnCheckedChangeListener(new checklistenetr(22));
    	 nwsdrdgval.setOnCheckedChangeListener(new checklistenetr(23));
	}
	class checklistenetr implements OnCheckedChangeListener
	{
    	int i;
		public checklistenetr(int i) {
			// TODO Auto-generated constructor stub
		   this.i=i;
		}

		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			   RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
		        boolean isChecked = checkedRadioButton.isChecked();
		        if (isChecked)
		        {
		          switch (i) {
					case 1:
						vpprdgtxt= checkedRadioButton.getText().toString().trim();System.out.println("vpprdgtxt"+vpprdgtxt);
						if(vpprdgtxt.equals("Yes"))	
						{
							((LinearLayout)findViewById(R.id.viclin)).setVisibility(cf.v1.VISIBLE);
						}
						else
						{
							((EditText)findViewById(R.id.viccomment)).setText("");
							((LinearLayout)findViewById(R.id.viclin)).setVisibility(cf.v1.GONE);
						}
						break;
					case 2:
						hoprdgtxt= checkedRadioButton.getText().toString().trim();System.out.println("hoprdgtxt"+hoprdgtxt);
						if(hoprdgtxt.equals("Yes"))
						{
							((LinearLayout)findViewById(R.id.hllin)).setVisibility(cf.v1.VISIBLE);
						}
						else
						{
							((EditText)findViewById(R.id.hlcomment)).setText("");
							((LinearLayout)findViewById(R.id.hllin)).setVisibility(cf.v1.GONE);
						}
						break;
					case 3:
						obltrrdgtxt= checkedRadioButton.getText().toString().trim();
					  break;
					case 4:
						tprdgtxt= checkedRadioButton.getText().toString().trim();
						break;
					case 5:
						srprdgtxt= checkedRadioButton.getText().toString().trim();
						break;
					case 6:
						brprdgtxt= checkedRadioButton.getText().toString().trim();
					  break;
					case 7:
						usnrdgtxt= checkedRadioButton.getText().toString().trim();
						break;
					case 8:
						pdrdgtxt= checkedRadioButton.getText().toString().trim();
						break;
					case 9:
						nscnrdgtxt= checkedRadioButton.getText().toString().trim();
					  break;
					case 10:
						oanrdgtxt= checkedRadioButton.getText().toString().trim();
						break;
					case 11:
						ofprdgtxt= checkedRadioButton.getText().toString().trim();
						break;
					case 12:
						wsrrdgtxt= checkedRadioButton.getText().toString().trim();
					  break;
					case 13:
						edrdgtxt= checkedRadioButton.getText().toString().trim();
						break;
					case 14:
						boprdgtxt= checkedRadioButton.getText().toString().trim();
						if(boprdgtxt.equals("Yes"))
						{
							((LinearLayout)findViewById(R.id.bplin)).setVisibility(cf.v1.VISIBLE);
						}
						else
						{
							((EditText)findViewById(R.id.bpcomment)).setText("");
							((LinearLayout)findViewById(R.id.bplin)).setVisibility(cf.v1.GONE);
						}
						break;
					case 15:
						bgdrdgtxt= checkedRadioButton.getText().toString().trim();
					  break;
					case 16:
						vpdnrdgtxt= checkedRadioButton.getText().toString().trim();
						break;
					case 17:
						spuwrdgtxt= checkedRadioButton.getText().toString().trim();
						break;
					case 18:
						ivnrdgtxt= checkedRadioButton.getText().toString().trim();
					  break;
					case 19:
						rdrrdgtxt= checkedRadioButton.getText().toString().trim();
						break;
					case 20:
						pcdprdgtxt= checkedRadioButton.getText().toString().trim();
						break;
					case 21:
						occdprdgtxt= checkedRadioButton.getText().toString().trim();
					  break;
					case 22:
						nwssrdgtxt= checkedRadioButton.getText().toString().trim();
					  break;
					case 23:
						nwsdrdgtxt= checkedRadioButton.getText().toString().trim();
					  break;
		          }
		       }
		}
	}
	private void ClkEvnt_RadBtn_Trip(RadioButton[] thrd_opt2) {
		// TODO Auto-generated method stub
		for(int i=0;i<thrd_opt2.length;i++)
		{
			
			thrd_opt2[i].setOnClickListener(new RadioArrClkListnrTrip());
		}
	}

	    class textwatcher implements TextWatcher
        {
	         public int type;
	         textwatcher(int type)
	    	{
	    		this.type=type;
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {


	    		// TODO Auto-generated method stub
	    		if(this.type==1)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.vic_tv_type1),250); 
	    		}
	    		else if(this.type==2)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.hl_tv_type1),250); 
	    		}
	    		else if(this.type==4)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.bp_tv_type1),250); 
	    		}
	    		else if(this.type==5)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.sumcom_tv_type1),500); 
	    		}
	    	}

	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,

	    			int after) {
	    		// TODO Auto-generated method stub
	    		
	    	}

	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,

	    			int count) {
	    		// TODO Auto-generated method stub
	    		
	    	}
	    	
	    }	
	
	class  RadioArrClkListnrTrip implements OnClickListener
	{
     

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
		for(int i=0;i<thrd_opt.length;i++)
			{
		        if(v.getTag().toString().equals("None Observed")){
		        	((EditText)findViewById(R.id.th_otr)).setVisibility(v.GONE);
		        	((EditText)findViewById(R.id.th_otr)).setText("");
		        }else{
		        	((EditText)findViewById(R.id.th_otr)).setText("");
		        	((EditText)findViewById(R.id.th_otr)).setVisibility(v.VISIBLE);
		        }
				if(v.getId()==thrd_opt[i].getId())
				{	
					
					thrd_opt[i].setChecked(true);
				}
				else
				{
					thrd_opt[i].setChecked(false);
				}
			}
		}
		
	}

	public void clicker(View v)

	{
		switch (v.getId()) {
		case R.id.sh_na:/** SUMMARY HAZARDS NOT APPLICABLE CHECKBOX **/
			if(((CheckBox)findViewById(R.id.sh_na)).isChecked())
			{
				//System.out.println("triphazard_val"+triphazard_val);
    			if(!retvprdtxt.equals("")){
 				    showunchkalert(1);
 			    }
    			else
    			{
    				clearsumhaz();set_defaultchk();System.out.println("uncheck1");
    				((LinearLayout)findViewById(R.id.sh_table)).setVisibility(v.GONE);System.out.println("unchec2k");
 			    	((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.INVISIBLE);System.out.println("uncheck3");
 			    }
				
			}
			else
			{System.out.println("elseuncheck");set_defaultchk();
				((LinearLayout)findViewById(R.id.sh_table)).setVisibility(v.VISIBLE);
				((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.INVISIBLE);
				((CheckBox)findViewById(R.id.sh_na)).setChecked(false);
			}
			
			break;
		case R.id.helpid:
			cf.ShowToast("Use for Residential GCH reports only.",0);
			break;
		case R.id.loadcomments:
			/***Call for the comments***/
			 int len=((EditText)findViewById(R.id.sumcomment)).getText().toString().length();
			 if(load_comment )
				{
					load_comment=false;
					int loc[] = new int[2];
					v.getLocationOnScreen(loc);
					Intent in1 = cf.loadComments("Summary of Hazards and Concerns Comments",loc);
					in1.putExtra("length", len);
					in1.putExtra("max_length", 500);
					startActivityForResult(in1, cf.loadcomment_code);
				}
			 break;
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.save:
			if(((CheckBox)findViewById(R.id.sh_na)).isChecked())
			{
				shchkval=1;HazardsInsert();
				((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
				cf.ShowToast("Summary of Hazards & Concerns saved successfully.", 0);
				cf.goclass(46);
			}
			else
			{
				shchkval=0;
				if(vpprdgtxt.equals("Yes"))
				{
					if(((EditText)findViewById(R.id.viccomment)).getText().toString().trim().equals(""))
					{
						cf.ShowToast("Please enter the comments for Vicious/exotic pets present.", 0);
				        cf.setFocus(((EditText)findViewById(R.id.viccomment)));
					}
					else
					{
						horsevalidation();
					}
				}
				else
				{
					horsevalidation();
				}
			}
			
			
			break;

		
		}
	}
	private void horsevalidation() {
		// TODO Auto-generated method stub
		if(hoprdgtxt.equals("Yes"))
			if(((EditText)findViewById(R.id.hlcomment)).getText().toString().trim().equals(""))
			{
				cf.ShowToast("Please enter the comments for Horses/livestock on premises.", 0);
				cf.setFocus(((EditText)findViewById(R.id.hlcomment)));
			}
			else
			{
				TripHazardValidation();
			}
		else
			TripHazardValidation();
	}
	private void TripHazardValidation() {
		// TODO Auto-generated method stub
		triphazard_val=cf.getslected_radio(thrd_opt);
		if(triphazard_val.equals(""))
			cf.ShowToast("Please select the option for Trip hazards noted.", 0);
		else
			if(triphazard_val.equals("None Observed")){
				BusinessPreValidation();
			}
			else{
				if(((EditText)findViewById(R.id.th_otr)).getText().toString().trim().equals(""))
				{
					cf.ShowToast("Please enter the text for Trip hazards noted.", 0);
					cf.setFocus(((EditText)findViewById(R.id.th_otr)));
				}
				else
				{
					BusinessPreValidation();
				}
			}
			
	}
	private void BusinessPreValidation() {
		// TODO Auto-generated method stub
		if(boprdgtxt.equals("Yes"))
			if(((EditText)findViewById(R.id.bpcomment)).getText().toString().trim().equals(""))
			{
				cf.ShowToast("Please enter the comments for Business on premises.", 0);
				cf.setFocus(((EditText)findViewById(R.id.bpcomment)));
			}
			else
			{
				SummaryCommentsValidation();
			}
		else
			SummaryCommentsValidation();
			
	}
	private void SummaryCommentsValidation() {
		// TODO Auto-generated method stub
		/*if(((EditText)findViewById(R.id.sumcomment)).getText().toString().trim().equals(""))
		{
			cf.ShowToast("Please enter Summary of Hazards & Concerns comments.", 0);
			cf.setFocus(((EditText)findViewById(R.id.sumcomment)));
		}
		else
		{*/
			HazardsInsert();
			((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
			cf.ShowToast("Summary of Hazards & Concerns saved successfully.", 0);
			cf.goclass(46);
		//}
	}
	private void HazardsInsert() {

		// TODO Auto-generated method stub
		
		Cursor SHZ_save=null;
		try
		{
			SHZ_save=cf.SelectTablefunction(cf.GCH_SummaryHazardstbl, " where fld_srid='"+cf.selectedhomeid+"'");
			if(SHZ_save.getCount()>0)
			{
				try
				{
					cf.arr_db.execSQL("UPDATE "+cf.GCH_SummaryHazardstbl+ " set fld_shzchk='"+shchkval+"',fld_vicious='"+vpprdgtxt+"',"+
				                               "fld_viciousytxt='"+cf.encode(((EditText)findViewById(R.id.viccomment)).getText().toString())+"',"+
							                   "fld_horses='"+hoprdgtxt+"',fld_horseytxt='"+cf.encode(((EditText)findViewById(R.id.hlcomment)).getText().toString())+"',"+
				                               "fld_overhanging='"+obltrrdgtxt+"',fld_trampoline='"+tprdgtxt+"',fld_skateboard='"+srprdgtxt+"',fld_bicycle='"+brprdgtxt+"',"+
							                   "fld_triphz='"+triphazard_val+"',fld_triphztxt='"+cf.encode(((EditText)findViewById(R.id.th_otr)).getText().toString())+"',"+
				                               "fld_unsafe='"+usnrdgtxt+"',fld_porchdeck='"+pdrdgtxt+"',fld_nonconst='"+nscnrdgtxt+"',fld_odoor='"+oanrdgtxt+"',"+
							                   "fld_openfound='"+ofprdgtxt+"',fld_wood='"+wsrrdgtxt+"',fld_excessdb='"+edrdgtxt+"',fld_bop='"+boprdgtxt+"',"+
				                               "fld_boptxt='"+cf.encode(((EditText)findViewById(R.id.bpcomment)).getText().toString())+"',fld_bgdisrepair='"+bgdrdgtxt+"',"+
							                   "fld_visualpd='"+vpdnrdgtxt+"',fld_structp='"+spuwrdgtxt+"',fld_inoper='"+ivnrdgtxt+"',fld_recentdw='"+rdrrdgtxt+"',"+
				                               "fld_possiblecdw='"+pcdprdgtxt+"',fld_ownerccdw='"+occdprdgtxt+"',fld_nsssystem='"+nwssrdgtxt+"',fld_nwsdedic='"+nwsdrdgtxt+"',"+ 
							                   "fld_summaryhzcomments='"+cf.encode(((EditText)findViewById(R.id.sumcomment)).getText().toString())+"' where fld_srid='"+cf.selectedhomeid+"'");
					
					System.out.println("UPDATE "+cf.GCH_SummaryHazardstbl+ " set fld_shzchk='"+shchkval+"',fld_vicious='"+vpprdgtxt+"',"+
				                               "fld_viciousytxt='"+cf.encode(((EditText)findViewById(R.id.viccomment)).getText().toString())+"',"+
							                   "fld_horses='"+hoprdgtxt+"',fld_horseytxt='"+cf.encode(((EditText)findViewById(R.id.hlcomment)).getText().toString())+"',"+
				                               "fld_overhanging='"+obltrrdgtxt+"',fld_trampoline='"+tprdgtxt+"',fld_skateboard='"+srprdgtxt+"',fld_bicycle='"+brprdgtxt+"',"+
							                   "fld_triphz='"+triphazard_val+"',fld_triphztxt='"+cf.encode(((EditText)findViewById(R.id.th_otr)).getText().toString())+"',"+
				                               "fld_unsafe='"+usnrdgtxt+"',fld_porchdeck='"+pdrdgtxt+"',fld_nonconst='"+nscnrdgtxt+"',fld_odoor='"+oanrdgtxt+"',"+
							                   "fld_openfound='"+ofprdgtxt+"',fld_wood='"+wsrrdgtxt+"',fld_excessdb='"+edrdgtxt+"',fld_bop='"+boprdgtxt+"',"+
				                               "fld_boptxt='"+cf.encode(((EditText)findViewById(R.id.bpcomment)).getText().toString())+"',fld_bgdisrepair='"+bgdrdgtxt+"',"+
							                   "fld_visualpd='"+vpdnrdgtxt+"',fld_structp='"+spuwrdgtxt+"',fld_inoper='"+ivnrdgtxt+"',fld_recentdw='"+rdrrdgtxt+"',"+
				                               "fld_possiblecdw='"+pcdprdgtxt+"',fld_ownerccdw='"+occdprdgtxt+"',fld_nsssystem='"+nwssrdgtxt+"',fld_nwsdedic='"+nwsdrdgtxt+"',"+ 
							                   "fld_summaryhzcomments='"+cf.encode(((EditText)findViewById(R.id.sumcomment)).getText().toString())+"' where fld_srid='"+cf.selectedhomeid+"'");
					
					/*((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
					cf.ShowToast("Summary of Hazads & Concerns saved successfully.", 0);
					cf.goclass(45);*/
				}
				catch (Exception E)
				{
					String strerrorlog="Updating the Summary of Hazards & Concerns - GCH";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
				
			}
			else
			{
				try
				{
					cf.arr_db.execSQL("INSERT INTO "
							+ cf.GCH_SummaryHazardstbl
							+ " (fld_srid,fld_shzchk,fld_vicious,fld_viciousytxt,fld_horses,fld_horseytxt,fld_overhanging,fld_trampoline,fld_skateboard,fld_bicycle,"+
							                   "fld_triphz,fld_triphztxt,fld_unsafe,fld_porchdeck,fld_nonconst,fld_odoor,fld_openfound,fld_wood,fld_excessdb,"+
							                   "fld_bop,fld_boptxt,fld_bgdisrepair,fld_visualpd,fld_structp,fld_inoper,fld_recentdw,fld_possiblecdw,"+
							                   "fld_ownerccdw,fld_nsssystem,fld_nwsdedic,fld_summaryhzcomments)"
							+ "VALUES ('"+cf.selectedhomeid+"','"+shchkval+"','"+vpprdgtxt+"','"+cf.encode(((EditText)findViewById(R.id.viccomment)).getText().toString())+"',"+
			                   "'"+hoprdgtxt+"','"+cf.encode(((EditText)findViewById(R.id.hlcomment)).getText().toString())+"',"+
                               "'"+obltrrdgtxt+"','"+tprdgtxt+"','"+srprdgtxt+"','"+brprdgtxt+"','"+triphazard_val+"','"+cf.encode(((EditText)findViewById(R.id.th_otr)).getText().toString())+"',"+
                               "'"+usnrdgtxt+"','"+pdrdgtxt+"','"+nscnrdgtxt+"','"+oanrdgtxt+"','"+ofprdgtxt+"','"+wsrrdgtxt+"','"+edrdgtxt+"','"+boprdgtxt+"',"+
                               "'"+cf.encode(((EditText)findViewById(R.id.bpcomment)).getText().toString())+"','"+bgdrdgtxt+"','"+vpdnrdgtxt+"',"+
                               "'"+spuwrdgtxt+"','"+ivnrdgtxt+"','"+rdrrdgtxt+"','"+pcdprdgtxt+"','"+occdprdgtxt+"','"+nwssrdgtxt+"','"+nwsdrdgtxt+"',"+ 
			                   "'"+cf.encode(((EditText)findViewById(R.id.sumcomment)).getText().toString())+"')");

					/*((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
					cf.ShowToast("Summary of Hazads & Concerns saved successfully.", 0);
					 cf.goclass(45);*/
			 	}
				catch (Exception E)
				{
					String strerrorlog="Inserting the Summary of Hazards & Concerns  - GCH ";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
				
			}
		}
		catch (Exception E)
		{
			String strerrorlog="Checking the rows inserted in the GCH Pool Fence table.";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of checking rows of GCH Pool Present table at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			cf.goclass(42);
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}
	private void chk_values() {
		// TODO Auto-generated method stub
		try
		{
		   Cursor sh_retrive=cf.SelectTablefunction(cf.GCH_SummaryHazardstbl, " where fld_srid='"+cf.selectedhomeid+"'");
		   int rws = sh_retrive.getCount();
			if(rws>0){
				sh_retrive.moveToFirst();
				retvprdtxt = sh_retrive.getString(sh_retrive.getColumnIndex("fld_triphz"));
			}
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}
	public void showunchkalert(final int i) {
		// TODO Auto-generated method stub
		AlertDialog.Builder bl = new Builder(SummaryHazards.this);
		bl.setTitle("Confirmation");
		bl.setMessage(Html.fromHtml("Do you want to clear the Summary of Hazards and Concerns data?"));
		bl.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog,
											int id) {
										 switch(i){
										 case 1:
											 ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.GONE);
							    			 clearsumhaz();//HazardsInsert();
							    			 //cf.arr_db.execSQL("Delete from "+cf.GCH_SummaryHazardstbl+" Where fld_srid='"+cf.selectedhomeid+"'");
											 set_defaultchk();
							    			 ((LinearLayout)findViewById(R.id.sh_table)).setVisibility(cf.v1.GONE);
							    			 break;
		                                 
										 }
										 	 
										 
									}
		});
		bl.setNegativeButton("No",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog,
							int id) {
						switch(i){
						 case 1:
							 ((CheckBox)findViewById(R.id.sh_na)).setChecked(false);
							 break;
					
						 }
					}
        });
		AlertDialog al=bl.create();
		al.setIcon(R.drawable.alertmsg);
		al.setCancelable(false);
		al.show();
		
		
	}
	protected void clearsumhaz() {
		// TODO Auto-generated method stub
		 shchkval=0;thnck=true;
		 try{
			 if(thnck)clearhnoted();
			}
		 catch(Exception e)
		 {
			 
		 }
		 triphazard_val="";
		 vpprdgtxt="No";hoprdgtxt="No";obltrrdgtxt="";tprdgtxt="";
		 srprdgtxt="";brprdgtxt="";usnrdgtxt="";pdrdgtxt="";
		 nscnrdgtxt="";oanrdgtxt="";ofprdgtxt="";wsrrdgtxt="";
		 edrdgtxt="";boprdgtxt="";bgdrdgtxt="";vpdnrdgtxt="";
		 spuwrdgtxt="";ivnrdgtxt="";rdrrdgtxt="";pcdprdgtxt="";
		 occdprdgtxt="";nwssrdgtxt="";nwsdrdgtxt="";
		 retvprdtxt="";
		((EditText)findViewById(R.id.viccomment)).setText("");
		((LinearLayout)findViewById(R.id.viclin)).setVisibility(cf.v1.GONE);
		((EditText)findViewById(R.id.hlcomment)).setText("");
		((LinearLayout)findViewById(R.id.hllin)).setVisibility(cf.v1.GONE);
		((EditText)findViewById(R.id.th_otr)).setText("");
		((EditText)findViewById(R.id.th_otr)).setVisibility(cf.v1.GONE);
		((EditText)findViewById(R.id.bpcomment)).setText("");
		((LinearLayout)findViewById(R.id.bplin)).setVisibility(cf.v1.GONE);
		((EditText)findViewById(R.id.sumcomment)).setText("");
	}
	protected void clearhnoted() {
		// TODO Auto-generated method stub
		((RadioButton)findViewById(R.id.th_rd1)).setChecked(false);
		((RadioButton)findViewById(R.id.th_rd2)).setChecked(false);
		((RadioButton)findViewById(R.id.th_rd3)).setChecked(false);
		((RadioButton)findViewById(R.id.th_rd4)).setChecked(false);
		((RadioButton)findViewById(R.id.th_rd5)).setChecked(false);
		
	}
	protected void set_defaultchk() {
		// TODO Auto-generated method stub
		((RadioButton) vpprdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) hoprdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) obltrrdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) tprdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) srprdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) brprdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) usnrdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) pdrdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) nscnrdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) oanrdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) ofprdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) wsrrdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) edrdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) boprdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) bgdrdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) vpdnrdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) spuwrdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) ivnrdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) rdrrdgval.findViewWithTag("Beyond Scope of Inspection")).setChecked(true);
		((RadioButton) pcdprdgval.findViewWithTag("Beyond Scope of Inspection")).setChecked(true);
		((RadioButton) occdprdgval.findViewWithTag("Beyond Scope of Inspection")).setChecked(true);
		((RadioButton) nwssrdgval.findViewWithTag("Beyond Scope of Inspection")).setChecked(true);
		((RadioButton) nwsdrdgval.findViewWithTag("Beyond Scope of Inspection")).setChecked(true);
		vpprdgtxt="No";hoprdgtxt="No";triphazard_val="";boprdgtxt="No";obltrrdgtxt="No";tprdgtxt="No";srprdgtxt="No";
				   brprdgtxt="No";usnrdgtxt="No";pdrdgtxt="No";nscnrdgtxt="No";oanrdgtxt="No";ofprdgtxt="No";wsrrdgtxt="No";
				   edrdgtxt="No";bgdrdgtxt="No";vpdnrdgtxt="No";spuwrdgtxt="No";ivnrdgtxt="No";rdrrdgtxt="Beyond Scope of Inspection";
				   pcdprdgtxt="Beyond Scope of Inspection";occdprdgtxt="Beyond Scope of Inspection";
				   nwssrdgtxt="Beyond Scope of Inspection";nwsdrdgtxt="Beyond Scope of Inspection";
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	
		  if(requestCode==cf.loadcomment_code)
		  {
			  load_comment=true;
			if(resultCode==RESULT_OK)
			{
				 cf.ShowToast("Comments added successfully.", 0);
				 String sumcomts = ((EditText)findViewById(R.id.sumcomment)).getText().toString();
				 ((EditText)findViewById(R.id.sumcomment)).setText((sumcomts+" "+data.getExtras().getString("Comments")).trim());
				
			}
			
		}

	}
}
