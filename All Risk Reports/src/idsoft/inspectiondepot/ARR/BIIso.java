package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;

public class BIIso extends Activity {
	CommonFunctions cf;
	public EditText ediso[] = new EditText[11];
	public int[] edisoid = {R.id.edframeper,R.id.edmasonryper,R.id.edmixedper,R.id.edjointedmasper,R.id.edmodfireresper,R.id.edsuperiormasonry,R.id.ednoncomb,R.id.edfireresitive,R.id.edheavylumber,R.id.edsuperiornoncomb,R.id.edsuperiormasonrynoncomb};
	TextWatcher watcheriso;
	int value=0;
	Cursor c1;
	int isovalue[] = new int[11];
	String txtiso[] = {"Frame","Masonry Non-Combustible","Mixed","Joisted Masonry","Modified Fire Resistive","Superior Masonry","Non-Combustible",
			"Fire Resistive","Heavy Lumber","Superior Non-Combustible","Superior Masonry Non-Combustible"};
	String BI_ISO="",ISO="",ISO_NA="",isonotappl="";
	public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	       
	         cf = new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);				
		 	}
	        setContentView(R.layout.iso);
	        cf.getDeviceDimensions();
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
         	hdr_layout.addView(new HdrOnclickListener(this,1,"General","Building Info","ISO Classification",1,1,cf));
         	LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
         	main_layout.setMinimumWidth(cf.wd);
         	main_layout.addView(new MyOnclickListener(getApplicationContext(), 1, 0,0,cf));
            LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
            submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 16, 1,0,cf));
	        LinearLayout submenu2_layout = (LinearLayout) findViewById(R.id.submenu1); 
	        submenu2_layout.addView(new MyOnclickListener(getApplicationContext(), 453, 1,0,cf));
	        TableRow tblrw = (TableRow)findViewById(R.id.row2);
	        tblrw.setMinimumHeight(cf.ht); 			
	        cf.CreateARRTable(61);
	        Declaration();
	        setValue();
	    }
	private void Declaration() {
		// TODO Auto-generated method stub
		cf.setupUI((ScrollView) findViewById(R.id.scr));
		 watcheriso = new TextWatcher() {
				public void onTextChanged(CharSequence s, int start, int before,int count) {
					// TODO Auto-generated method stub
					if(!s.toString().equals(""))
					{
						try
						{
							isovalue[0] = (((EditText)findViewById(R.id.edframeper)).getText().toString().trim().equals(""))? 0:Integer.parseInt(ediso[0].getText().toString().trim());
							isovalue[1] = (((EditText)findViewById(R.id.edmasonryper)).getText().toString().trim().equals(""))? 0:Integer.parseInt(ediso[1].getText().toString().trim());
							isovalue[2] = (((EditText)findViewById(R.id.edmixedper)).getText().toString().trim().equals(""))? 0:Integer.parseInt(ediso[2].getText().toString().trim());
							isovalue[3] = (((EditText)findViewById(R.id.edjointedmasper)).getText().toString().trim().equals(""))? 0:Integer.parseInt(ediso[3].getText().toString().trim());
							isovalue[4] = (((EditText)findViewById(R.id.edmodfireresper)).getText().toString().trim().equals(""))? 0:Integer.parseInt(ediso[4].getText().toString().trim());
							isovalue[5] = (((EditText)findViewById(R.id.edsuperiormasonry)).getText().toString().trim().equals(""))? 0:Integer.parseInt(ediso[5].getText().toString().trim());
							isovalue[6] = (((EditText)findViewById(R.id.ednoncomb)).getText().toString().trim().equals(""))? 0:Integer.parseInt(ediso[6].getText().toString().trim());
							isovalue[7] = (((EditText)findViewById(R.id.edfireresitive)).getText().toString().trim().equals(""))? 0:Integer.parseInt(ediso[7].getText().toString().trim());
							isovalue[8] = (((EditText)findViewById(R.id.edheavylumber)).getText().toString().trim().equals(""))? 0:Integer.parseInt(ediso[8].getText().toString().trim());
							isovalue[9] = (((EditText)findViewById(R.id.edsuperiornoncomb)).getText().toString().trim().equals(""))? 0:Integer.parseInt(ediso[9].getText().toString().trim());
							isovalue[10] = (((EditText)findViewById(R.id.edsuperiormasonrynoncomb)).getText().toString().trim().equals(""))? 0:Integer.parseInt(ediso[10].getText().toString().trim());
							
							if(isovalue[0]+isovalue[1]+isovalue[2]+isovalue[3]+isovalue[4]+isovalue[5]+isovalue[6]+isovalue[7]+isovalue[8]+isovalue[9]+isovalue[10]>100)
							{
								View v = getCurrentFocus();
								int id = v.getId();
								for(int i=0;i<ediso.length;i++)
								{
									if(v.getId()==ediso[i].getId())
									{
										cf.ShowToast("Percentage field exceeds 100% and should sum to 100.", 1);
										ediso[i].setText("");isovalue[i]=0;								
									}
								}						
							}
						}catch(Exception e){
							
						}
					}
					else
					{
						try
						{
							View v = getCurrentFocus();
							int id = v.getId();
							for(int i=0;i<ediso.length;i++)
							{
								//ediso[i].setFocusable(false);
								if(v.getId()==ediso[i].getId())
								{
									isovalue[i]=0;								
								}
							}
						}
						catch(Exception e)
						{
							System.out.println("EEEEEEE"+e.getMessage());
						}
					}
					
						value = isovalue[0]+isovalue[1]+isovalue[2]+isovalue[3]+isovalue[4]+isovalue[5]+isovalue[6]+isovalue[7]+isovalue[8]+isovalue[9]+isovalue[10];
						int rem = 100 -value;
						((TextView)findViewById(R.id.total)).setText(Html.fromHtml("Total : " + "<b><font color=#0B610B>" + value+ "</font></b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Remaining : "+"<b><font color=#0B610B>"+rem+"</font></b>"));
						
				}
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
					// TODO Auto-generated method stub
				}
				public void afterTextChanged(Editable e) {
					// TODO Auto-generated method stub
					View v1 = getCurrentFocus();
					if(!e.toString().equals("")){
	    			     if(Character.toString(e.toString().charAt(0)).equals("0"))
		    			 {
	    			    	for(int i=0;i<ediso.length;i++)
	 						{
	    			    		if(v1.getId()==ediso[i].getId())
								{
	    			    			ediso[i].setText("");	
									cf.ShowToast("Invalid Entry! Please enter the valid "+ txtiso[i] +" percentage.", 0);cf.hidekeyboard();
								}
	 						}
		    			 }
	    			 } 
				}
			};			
		for(int i=0;i<ediso.length;i++)
	   	{
			ediso[i] = (EditText)findViewById(edisoid[i]);
		   	ediso[i].addTextChangedListener(watcheriso);
		}
	}
	private void selecttbl() {
		c1 = cf.SelectTablefunction(cf.BI_General, " where fld_srid='"+cf.selectedhomeid+"'");
		if(c1.getCount()>0)
		{
			c1.moveToFirst();
			ISO_NA = c1.getString(c1.getColumnIndex("BI_ISO_NA"));
			ISO = cf.decode(c1.getString(c1.getColumnIndex("BI_ISO")));
		}
	}
	private void setValue() 
	{
		selecttbl();
		if(c1.getCount()==0)
		{
			((CheckBox)findViewById(R.id.isonotapplicable)).setChecked(true);
			((LinearLayout)findViewById(R.id.isolin)).setVisibility(cf.v1.GONE);
		}
		else
		{
			
			((TextView)findViewById(R.id.total)).setText(Html.fromHtml("Total : " + "<b><font color=#0B610B>0</font></b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Remaining : "+"<b><font color=#0B610B>100</font></b>"));
			if(ISO_NA.equals("0") || ISO_NA.equals(""))
			{
				
				if(!ISO.equals(""))
				{
					((LinearLayout)findViewById(R.id.isolin)).setVisibility(cf.v1.VISIBLE);
						String ISO_split[] = ISO.split("~");	
						if(ISO_split[0].length()>1){ediso[0].setText(ISO_split[0].trim());}
						if(ISO_split[1].length()>1){ediso[1].setText(ISO_split[1].trim());}
						if(ISO_split[2].length()>1){ediso[2].setText(ISO_split[2].trim());}
						if(ISO_split[3].length()>1){ediso[3].setText(ISO_split[3].trim());}
						if(ISO_split[4].length()>1){ediso[4].setText(ISO_split[4].trim());}
						if(ISO_split[5].length()>1){ediso[5].setText(ISO_split[5].trim());}
						if(ISO_split[6].length()>1){ediso[6].setText(ISO_split[6].trim());}
						if(ISO_split[7].length()>1){ediso[7].setText(ISO_split[7].trim());}
						if(ISO_split[8].length()>1){ediso[8].setText(ISO_split[8].trim());}
						if(ISO_split[9].length()>1){ediso[9].setText(ISO_split[9].trim());}
						if(ISO_split[10].length()>1){ediso[10].setText(ISO_split[10].trim());}
						((TextView)findViewById(R.id.savetxtiso)).setVisibility(cf.v1.VISIBLE);				
				}
				else
				{
					((LinearLayout)findViewById(R.id.isolin)).setVisibility(cf.v1.GONE);
					((TextView)findViewById(R.id.savetxtiso)).setVisibility(cf.v1.GONE);
					((CheckBox)findViewById(R.id.isonotapplicable)).setChecked(true);
				}
			}
			else
			{
				((CheckBox)findViewById(R.id.isonotapplicable)).setChecked(true);
				((LinearLayout)findViewById(R.id.isolin)).setVisibility(cf.v1.GONE);
				((TextView)findViewById(R.id.savetxtiso)).setVisibility(cf.v1.VISIBLE);
			}
		}
	}	
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.mouseoverid:
			cf.ShowToast("Complete for Replacement Cost Calculations.",0);
			break;
		case R.id.isosave:
		selecttbl();
		if(((CheckBox)findViewById(R.id.isonotapplicable)).isChecked()==false)
		{
			if(ediso[0].getText().toString().trim().equals("") && ediso[1].getText().toString().trim().equals("") && ediso[2].getText().toString().trim().equals("") && ediso[3].getText().toString().trim().equals("")
					&& ediso[4].getText().toString().trim().equals("") && ediso[5].getText().toString().trim().equals("") && ediso[6].getText().toString().trim().equals("")
					&& ediso[7].getText().toString().trim().equals("") && ediso[8].getText().toString().trim().equals("") && ediso[9].getText().toString().trim().equals("")
					&& ediso[10].getText().toString().trim().equals("")) 
				{
					cf.ShowToast("Please enter the percentage value that sum to 100.", 0);
					cf.setFocus(((EditText)findViewById(R.id.edframeper)));
				}
				else
				{
					if(value==100)
					{
						InsertISO();
					}
					else if(value>100)
					{
						cf.ShowToast("Percentage field exceeds 100% and  should sum to 100",0);
					}
					else if(value<100)
					{
						cf.ShowToast("Percentage field should sum to 100",0);
					}
				}
		}
		else
		{
			InsertISO();
		}
		break;
		case R.id.isonotapplicable:
			if(((CheckBox)findViewById(R.id.isonotapplicable)).isChecked())
			{
				isonotappl="1";
				if(!ISO.equals(""))
				{
					AlertDialog.Builder builder = new AlertDialog.Builder(BIIso.this);
					builder.setTitle("Confirmation");
					builder.setIcon(R.drawable.alertmsg);
					builder.setMessage("Do you want to clear the ISO Classification data?").setCancelable(false)
							.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,int id1) 
										{
												isoclear();
												((CheckBox)findViewById(R.id.isonotapplicable)).setChecked(true);
												((LinearLayout)findViewById(R.id.isolin)).setVisibility(cf.v1.GONE);
												((TextView)findViewById(R.id.savetxtiso)).setVisibility(cf.v1.GONE);
										}
									})
							.setNegativeButton("No",new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,	int id) {
											
											((CheckBox)findViewById(R.id.isonotapplicable)).setChecked(false);
											dialog.cancel();											
									}
							});
					builder.show();
				}
				else
				{
					((TextView)findViewById(R.id.savetxtiso)).setVisibility(cf.v1.GONE);
					((LinearLayout)findViewById(R.id.isolin)).setVisibility(cf.v1.GONE);
				}
			}
			else
			{
				isonotappl="0";isoclear();
				((LinearLayout)findViewById(R.id.isolin)).setVisibility(cf.v1.VISIBLE);
				((TextView)findViewById(R.id.savetxtiso)).setVisibility(cf.v1.GONE);
			}
			break;
		}		
	}
	
	protected void isoclear() {
		// TODO Auto-generated method stub
		ISO="";
		((EditText)findViewById(R.id.edframeper)).setText("");
		((EditText)findViewById(R.id.edmasonryper)).setText("");
		((EditText)findViewById(R.id.edmixedper)).setText("");
		((EditText)findViewById(R.id.edjointedmasper)).setText("");
		((EditText)findViewById(R.id.edmodfireresper)).setText("");
		((EditText)findViewById(R.id.edsuperiormasonry)).setText("");
		((EditText)findViewById(R.id.ednoncomb)).setText("");
		((EditText)findViewById(R.id.edfireresitive)).setText("");
		((EditText)findViewById(R.id.edheavylumber)).setText("");
		((EditText)findViewById(R.id.edsuperiornoncomb)).setText("");
		((EditText)findViewById(R.id.edsuperiormasonrynoncomb)).setText("");
		((TextView)findViewById(R.id.total)).setText(Html.fromHtml("Total : " + "<b><font color=#0B610B>0</font></b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Remaining : "+"<b><font color=#0B610B>100</font></b>"));
		
	}
	private void InsertISO() {
		// TODO Auto-generated method stub
		BI_ISO= " "+((EditText)findViewById(R.id.edframeper)).getText().toString()+"~ "+((EditText)findViewById(R.id.edmasonryper)).getText().toString()
				+"~ "+((EditText)findViewById(R.id.edmixedper)).getText().toString()+"~ "+((EditText)findViewById(R.id.edjointedmasper)).getText().toString()
				+"~ "+((EditText)findViewById(R.id.edmodfireresper)).getText().toString()+"~ "+((EditText)findViewById(R.id.edsuperiormasonry)).getText().toString()
				+"~ "+((EditText)findViewById(R.id.ednoncomb)).getText().toString()+"~ "+((EditText)findViewById(R.id.edfireresitive)).getText().toString()
				+"~ "+((EditText)findViewById(R.id.edheavylumber)).getText().toString()+"~ "+((EditText)findViewById(R.id.edsuperiornoncomb)).getText().toString()
				+"~ "+((EditText)findViewById(R.id.edsuperiormasonrynoncomb)).getText().toString();
		if(((CheckBox)findViewById(R.id.isonotapplicable)).isChecked()){isonotappl="1";BI_ISO="";}else{isonotappl="0";}
		
		updatenotappl();
		cf.ShowToast("ISO Classification saved successfully",1);
		//((TextView)findViewById(R.id.savetxtiso)).setVisibility(cf.v1.VISIBLE);
		cf.goclass(454);		
	}
	private void updatenotappl() {
		// TODO Auto-generated method stub
		try
		{
			selecttbl();
			if(c1.getCount()==0)
			{
				cf.arr_db.execSQL("INSERT INTO " + cf.BI_General + " (fld_srid,BI_ISO,BI_ISO_NA)"
						+ "VALUES ('"+cf.selectedhomeid+"','"+cf.encode(BI_ISO)+"','"+isonotappl+"')");
			}
			else
			{
				cf.arr_db.execSQL("UPDATE "+ cf.BI_General+ " SET BI_ISO='"+cf.encode(BI_ISO)+"',BI_ISO_NA='"+isonotappl+"'"
						+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
			}	
		}
		catch(Exception e){}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			cf.goclass(452);
			return true;
		}		
		return super.onKeyDown(keyCode, event);
	}
}
