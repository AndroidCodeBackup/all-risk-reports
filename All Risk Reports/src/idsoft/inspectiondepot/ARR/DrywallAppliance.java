package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;

import java.io.File;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class DrywallAppliance extends Activity{
	CommonFunctions cf;
	boolean load_comment=true;
	public int pick_img_code=24;
	Spinner appnospin,apptypspin,appobsspin,appuntacessspin,apprecrepspin,appcopspin,appdarkspin;
	ArrayAdapter appnoadap,apptypadap,appobsadap,appacesadap,apprecadap,appcopadap,appdrkkadap;
	String appnum[] = {"--Select--","1","2","3","4","5","6","7","8"};
	String apptyp[] = {"--Select--","Stove","Oven","Microwave","Refrigerator","Freezer","Dishwasher",
			"Exhaust Fan","Disposal","Bread Warmer","Jazuzzi Pump","Computers","Intercom/Radio",
			"Security System","Smoke Detectors","Other"};
	String appcop[] = {"--Select--","Yes","None Noted","Not Confirmed"};
	String appuntacess[] = {"--Select--","Full","Partial","No"};
	String apprecrep[] = {"--Select--","Yes","No","Not Determined","None Confirmed/Evident"};
	String appnostr="",apptypstr="",appobsstr="",appuntstr="",apprecrepstr="",appcopcorstr="",appdrkstr="";
	int apint=0,appCurrent_select_id;
	Cursor Drysum_save,Dryapp_save;
	String[] app_in_DB;
	TableLayout apptblshow;
	
	@Override
	   public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        cf=new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);
				
		 	}
	        setContentView(R.layout.drywallappliance);
	        cf.getDeviceDimensions();
	        
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
			hdr_layout.addView(new HdrOnclickListener(this,1,"Chinese Drywall","Internal Appliance System(s)",6,0,cf));
	        
	        LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
	        mainmenu_layout.setMinimumWidth(cf.wd);
	        mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 6, 0,0,cf));
	        LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 64, 1,0,cf));
	        TableRow tblrw = (TableRow)findViewById(R.id.row2);
			tblrw.setMinimumHeight(cf.ht);
			cf.CreateARRTable(7);
			cf.CreateARRTable(73);
			declarations();
			setComments();
			Showappvalue();
	}
	private void setComments() {
		// TODO Auto-generated method stub
		try
		{
		   Cursor SCf_retrive=cf.SelectTablefunction(cf.DRY_sumcond, " where fld_srid='"+cf.selectedhomeid+"'");
		  if(SCf_retrive.getCount()>0)
		   {  
			   SCf_retrive.moveToFirst();
			   ((EditText)findViewById(R.id.appcomment)).setText(cf.decode(SCf_retrive.getString(SCf_retrive.getColumnIndex("applianccomments"))));
		   }
		   
		}catch (Exception e) {
			// TODO: handle exception
		}
	}
	private void declarations() {
		// TODO Auto-generated method stub
		cf.setupUI((ScrollView) findViewById(R.id.scr));
		((EditText)findViewById(R.id.appcomment)).addTextChangedListener(new textwatcher(1));
		
		appnospin=(Spinner) findViewById(R.id.appno_spin);
		appnoadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, appnum);
		appnoadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		appnospin.setAdapter(appnoadap);
		appnospin.setOnItemSelectedListener(new  Spin_Selectedlistener(1));
		
		apptypspin=(Spinner) findViewById(R.id.apptyp_spin);
		apptypadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, apptyp);
		apptypadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		apptypspin.setAdapter(apptypadap);
		apptypspin.setOnItemSelectedListener(new  Spin_Selectedlistener(2));
		
		appobsspin=(Spinner) findViewById(R.id.obscorosn_spin);
		appobsadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, appcop);
		appobsadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		appobsspin.setAdapter(appobsadap);
		appobsspin.setOnItemSelectedListener(new  Spin_Selectedlistener(3));
		appobsspin.setSelection(2);
		
		appuntacessspin=(Spinner) findViewById(R.id.appunitacess_spin);
		appacesadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, appuntacess);
		appacesadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		appuntacessspin.setAdapter(appacesadap);
		appuntacessspin.setOnItemSelectedListener(new  Spin_Selectedlistener(4));
		appuntacessspin.setSelection(2);
		
		apprecrepspin=(Spinner) findViewById(R.id.apprecrepair_spin);
		apprecadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, apprecrep);
		apprecadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		apprecrepspin.setAdapter(apprecadap);
		apprecrepspin.setOnItemSelectedListener(new  Spin_Selectedlistener(5));
		apprecrepspin.setSelection(4);
		
		appcopspin=(Spinner) findViewById(R.id.appcopcorosn_spin);
		appcopadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, appcop);
		appcopadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		appcopspin.setAdapter(appcopadap);
		appcopspin.setOnItemSelectedListener(new  Spin_Selectedlistener(6));
		appcopspin.setSelection(2);
		
		appdarkspin=(Spinner) findViewById(R.id.appdark_spin);
		appdrkkadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, appcop);
		appdrkkadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		appdarkspin.setAdapter(appdrkkadap);
		appdarkspin.setOnItemSelectedListener(new  Spin_Selectedlistener(7));
		appdarkspin.setSelection(2);
		
		apptblshow = (TableLayout)findViewById(R.id.appvalue);
		
		((EditText)findViewById(R.id.apptyp_etxt)).addTextChangedListener(new CustomTextWatcherSpace(((EditText)findViewById(R.id.apptyp_etxt))));
			 
	}
	class Spin_Selectedlistener implements OnItemSelectedListener
	{
		int i;
		public Spin_Selectedlistener(int i) {
			// TODO Auto-generated constructor stub
			this.i=i;
		}

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			if(i==1){
				appnostr = appnospin.getSelectedItem().toString();
	            if(appnostr.equals("--Select--")){
					
				}else{
					if(apint==0){defaultappliance();}
				}
	        }
			else if(i==2)
			{
				apptypstr = apptypspin.getSelectedItem().toString();
				if(apptypstr.equals("Other"))
				{
					((EditText)findViewById(R.id.apptyp_etxt)).setVisibility(arg1.VISIBLE);
				}
				else
				{
					((EditText)findViewById(R.id.apptyp_etxt)).setText("");
					((EditText)findViewById(R.id.apptyp_etxt)).setVisibility(arg1.INVISIBLE);
				}
			}
			else if(i==3)
			{
				appobsstr = appobsspin.getSelectedItem().toString();
			}
			else if(i==4)
			{
				appuntstr = appuntacessspin.getSelectedItem().toString();
			}
			else if(i==5)
			{
				apprecrepstr = apprecrepspin.getSelectedItem().toString();
			}
			else if(i==6)
			{
				appcopcorstr = appcopspin.getSelectedItem().toString();
			}
			else if(i==7)
			{
				appdrkstr = appdarkspin.getSelectedItem().toString();
			}
			
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	class textwatcher implements TextWatcher
    {
	         public int type;
	         textwatcher(int type)
	    	{
	    		this.type=type;
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
	    		if(this.type==1)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.app_tv_type1),500); 
	    		}
	    	
	         }

	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,int after) {
	    		// TODO Auto-generated method stub
	     	}

	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,int count) {
	    		// TODO Auto-generated method stub
	    	}
	}	
	public void clicker(View v)
	{
		switch (v.getId()) {
			case R.id.hme:
				cf.gohome();
				break;
			case R.id.loadcomments:/***Call for the comments***/
				int len=((EditText)findViewById(R.id.appcomment)).getText().toString().length();
				if(load_comment )
				{
						load_comment=false;
						int loc[] = new int[2];
						v.getLocationOnScreen(loc);
						Intent in1 = cf.loadComments("Internal Appliance(s) System(s) Comments",loc);
						in1.putExtra("length", len);
						in1.putExtra("max_length", 500);
						startActivityForResult(in1, cf.loadcomment_code);
				}
				break;
			case R.id.save:
				chk_values();
				if(Dryapp_save.getCount()==0)
				{
					cf.ShowToast("Atleast one Internal Appliance System(s) should be added and saved.", 0);
				}
				else
				{
					/*if(((EditText)findViewById(R.id.appcomment)).getText().toString().trim().equals(""))
					{
						cf.ShowToast("Please enter Internal Appliance System(s) Comments.", 0);
						cf.setFocus(((EditText)findViewById(R.id.appcomment)));
					}
					else
					{*/
						if(Drysum_save.getCount()>0)
						{
							try
							{
								cf.arr_db.execSQL("UPDATE "+cf.DRY_sumcond+ " set applianccomments='"+cf.encode(((EditText)findViewById(R.id.appcomment)).getText().toString())+"' where fld_srid='"+cf.selectedhomeid+"'");
								cf.ShowToast("Internal Appliance System(s) saved successfully.", 1);
							}
							catch (Exception E)
							{
								String strerrorlog="Updating the Internal Appliance System(s) - Drywall";
								cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
							}
						}
						else
						{
							try
							{
								cf.arr_db.execSQL("INSERT INTO "
										+ cf.DRY_sumcond
										+ " (fld_srid,presofsulfur,obscopper,presofdry,addcomments,drywallcomments,atticcomments,"+
										   "applianccomments,hvaccomments,assesmentcomments)"
										+ "VALUES('"+cf.selectedhomeid+"','','',"+
										"'','','',"+
										"'','"+cf.encode(((EditText)findViewById(R.id.appcomment)).getText().toString())+"','','')");
								 cf.ShowToast("Internal Appliance System(s) saved successfully.", 1);
							}
							catch (Exception E)
							{
								String strerrorlog="Inserting the Internal Appliance System(s) - Drywall";
								cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
							}
						}
						((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
					    cf.goclass(65);
					//}
				}
				break;
			case R.id.dryappclear:
				apint=0;defaultappliance();
				appnospin.setSelection(0);appnospin.setEnabled(true);
				((Button)findViewById(R.id.addapp)).setText("Save/Add More Appliance System");
				((Button)findViewById(R.id.img_clr)).setVisibility(cf.v1.INVISIBLE);
				break;
			case R.id.img_pck:
				try
				{
					String s =((EditText)findViewById(R.id.updimg_ed).findViewWithTag("ModPath")).getText().toString();
					if(s==null)
					{
						s="";
					}
					Intent in=new Intent(DrywallAppliance.this,pickSingleimage.class);
					in.putExtra("path", s);
					in.putExtra("id", 2);
					startActivityForResult(in, pick_img_code);
				}
				catch (Exception e) {
					// TODO: handle exception
				}
				break;
			case R.id.img_clr:
				cf.ShowToast("Image cleared successfully.",0);
				((EditText)findViewById(R.id.updimg_ed)).setText("");
				((Button)findViewById(R.id.img_clr)).setVisibility(cf.v1.GONE);
				break;
		    case R.id.addapp:
				apint=2;
				if(appnostr.equals("--Select--")){
					cf.ShowToast("Please select Appliance Unit.", 0);
				}
				else
				{
					if(apptypstr.equals("--Select--")){
						cf.ShowToast("Please select Appliance Type.", 0);
					}
					else
					{
						if(apptypstr.equals("Other"))
						{
							if(((EditText)findViewById(R.id.apptyp_etxt)).getText().toString().trim().equals(""))
							{
								cf.ShowToast("Please enter the Other text for Appliance Type.", 0);
								cf.setFocus(((EditText)findViewById(R.id.apptyp_etxt)));
							}
							else
							{
								observed();
							}
						}
						else
						{
							observed();
						}
					}
				}
				break;
				
	   }
	}
	private void observed() {
		// TODO Auto-generated method stub
		if(appobsstr.equals("--Select--")){
			cf.ShowToast("Please select Observed Metal Corrosion.", 0);
		}
		else
		{
			if(appuntstr.equals("--Select--")){
				cf.ShowToast("Please select Unit Accessible.", 0);
			}
			else
			{
				if(apprecrepstr.equals("--Select--")){
					cf.ShowToast("Please select Recent Repairs to Units.", 0);
				}
				else
				{
					if(appcopcorstr.equals("--Select--")){
						cf.ShowToast("Please select Copper Corrosion Observed.", 0);
					}
					else
					{
						if(appdrkstr.equals("--Select--")){
							cf.ShowToast("Please select Darkening/Stains noted on Fixtures/Fittings.", 0);
						}
						else
						{
							next();
						}
					}
				}
			}
		}
	}
	private void next() {
		// TODO Auto-generated method stub
	/*	if(((EditText)findViewById(R.id.updimg_ed)).getText().toString().trim().equals(""))
		{
			cf.ShowToast("Please upload Appliance Unit Image.",0);
			cf.setFocus(((EditText)findViewById(R.id.updimg_ed)));
		}
		else
		{*/
				try
				{
					if(((Button)findViewById(R.id.addapp)).getText().toString().equals("Update")){
		                 cf.arr_db.execSQL("UPDATE  "+cf.DRY_appliance+" SET appnum='"+appnostr+"',"+
					           "apptyp='"+apptypstr+"',"+
					           "apptypotr='"+cf.encode(((EditText)findViewById(R.id.apptyp_etxt)).getText().toString())+"',"+
							   "unitacess='"+appuntstr+"',obscorrosion='"+appobsstr+"',recentrepairs='"+apprecrepstr+"',"+
					           "coppercorrs='"+appcopcorstr+"',"+
							   "darkstains='"+appdrkstr+"',"+
					           "appimg='"+cf.encode(((EditText)findViewById(R.id.updimg_ed)).getText().toString())+"' Where appId='"+appCurrent_select_id+"' ");
						defaultappliance();
				        appnospin.setSelection(0);appnospin.setEnabled(true);
						cf.ShowToast("Appliance unit updated successfully.", 0);((Button)findViewById(R.id.addapp)).setText("Save/Add More Appliance System");
						apint=0;Showappvalue();
					}else{
						chk_values();
						boolean[] b2 = {false};
						if(Dryapp_save.getCount()==0){
							
						}else{
							if(app_in_DB!=null)
							{
								for(int i=0;i<app_in_DB.length;i++)
								{
									if((app_in_DB[i].equals(appnostr)))
									{
										b2[0]=true;
									}
								}
							}
						}
						if(Dryapp_save.getCount()==8){
							defaultappliance();
							appnospin.setSelection(0);appnospin.setEnabled(true);
							cf.ShowToast("Limit Exceeds! You have already added 8 Appliance System.", 0);
						}else{
							if(!b2[0]){cf.arr_db.execSQL("INSERT INTO "
									+ cf.DRY_appliance
									+ " (fld_srid,appnum,apptyp,apptypotr,unitacess,obscorrosion,recentrepairs,coppercorrs,darkstains,appimg)"
									+ "VALUES('"+cf.selectedhomeid+"','"+appnostr+"','"+apptypstr+"',"+
									"'"+cf.encode(((EditText)findViewById(R.id.apptyp_etxt)).getText().toString())+"',"+
									"'"+appuntstr+"','"+appobsstr+"','"+apprecrepstr+"','"+appcopcorstr+"','"+appdrkstr+"',"+
									"'"+cf.encode(((EditText)findViewById(R.id.updimg_ed)).getText().toString())+"')");
							 cf.ShowToast("Internal Appliance System added Successfully.", 1);
							 defaultappliance();
							 appnospin.setSelection(0);appnospin.setEnabled(true);
								
							}else{cf.ShowToast("Already exists. Please select another Appliance System.", 0);
							defaultappliance();appnospin.setSelection(0);appnospin.setEnabled(true);
							}
							Showappvalue();
						}
							
					}
				}
				catch(Exception e){}
		//}
	}
	private void Showappvalue() {
		// TODO Auto-generated method stub
		   Cursor app_retrieve= cf.SelectTablefunction(cf.DRY_appliance, " Where fld_srid='"+cf.selectedhomeid+"' ");
	       if(app_retrieve.getCount()>0)
			{
	    	   if(!((EditText)findViewById(R.id.appcomment)).getText().toString().trim().equals("")){((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);}
	    	   app_retrieve.moveToFirst();
				try
				{
					apptblshow.removeAllViews();
				}
				catch (Exception e) {
					// TODO: handle exception
				}
				LinearLayout h1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvac_listview_header, null); 
				TableRow thv = (TableRow)h1.findViewById(R.id.attic);thv.setVisibility(cf.v1.GONE);
				TableRow thvac = (TableRow)h1.findViewById(R.id.hvac);thvac.setVisibility(cf.v1.GONE);
				LinearLayout th = (LinearLayout)h1.findViewById(R.id.app);th.setVisibility(cf.v1.VISIBLE);
				TableLayout.LayoutParams lp = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
			 	lp.setMargins(2, 0, 2, 2);h1.removeAllViews(); 
			 	
			 	apptblshow.addView(th,lp);
				app_in_DB=new String[app_retrieve.getCount()];
				apptblshow.setVisibility(View.VISIBLE); 
				
				for(int i=0;i<app_retrieve.getCount();i++)
				{
					TextView no,appunitno,appunitloc,appunitaccess,appobscor,apprecrepairs,appcopper,appdark;
					ImageView edit,delete;
					String untloc="",appimg="",untlocotr="",apsp="",untaces="",recrep="",copcor="",drkstn="",obscor="";
					apsp=app_retrieve.getString(2);
					untloc=app_retrieve.getString(3);
					untlocotr=cf.decode(app_retrieve.getString(4));
					untaces=app_retrieve.getString(5);
					obscor=app_retrieve.getString(6);
					recrep=app_retrieve.getString(7);
					copcor=app_retrieve.getString(8);
					drkstn=app_retrieve.getString(9);
					appimg=cf.decode(app_retrieve.getString(10));
					
					app_in_DB[i]=apsp;
					
					LinearLayout t1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvac_listview, null);
					LinearLayout t = (LinearLayout)t1.findViewById(R.id.app);t.setVisibility(cf.v1.VISIBLE);
					TableRow at = (TableRow)t1.findViewById(R.id.attic);at.setVisibility(cf.v1.GONE);
					TableRow hv = (TableRow)t1.findViewById(R.id.hvac);hv.setVisibility(cf.v1.GONE);
					t.setId(44444+i);/// Set some id for further use
				 	
				 	no= (TextView) t.findViewWithTag("No");
				 	no.setText(String.valueOf(i+1));
				 	appunitno= (TextView) t.findViewWithTag("APPUnit");
				 	appunitno.setText(apsp);
				 	appunitloc= (TextView) t.findViewWithTag("UnitLocation");
				 	if(untloc.equals("Other"))
				 	{
				 		appunitloc.setText(untloc+"("+untlocotr+")");
				 	}
				 	else
				 	{
				 		appunitloc.setText(untloc);
				 	}
				 	
				 	appobscor= (TextView) t.findViewWithTag("Observed");
				 	appobscor.setText(obscor);
				 	appunitaccess= (TextView) t.findViewWithTag("UnitAccessible");
				 	appunitaccess.setText(untaces);
				 	apprecrepairs= (TextView) t.findViewWithTag("RecentRepairs");
				 	apprecrepairs.setText(recrep);
				 	appcopper= (TextView) t.findViewWithTag("CopperCorrosion");
				 	appcopper.setText(copcor);
				 	appdark= (TextView) t.findViewWithTag("Darkening");
				 	appdark.setText(drkstn);
				 	ImageView  iv=(ImageView)t.findViewWithTag("HIMG");
					File f=new File(appimg);
					if(f.exists())
					{
						
						Bitmap b1= cf.ShrinkBitmap(appimg, 125, 125);
						if(b1!=null)
						{
							iv.setImageBitmap(b1);
							
						}
						else
						{
							iv.setImageDrawable(getResources().getDrawable(R.drawable.photonotavail));
						}
						
					}
					else
					{
						iv.setImageDrawable(getResources().getDrawable(R.drawable.photonotavail));
					}
				 	edit= (ImageView) t.findViewWithTag("edit");
				 	edit.setId(789456+i);
				 	edit.setTag(app_retrieve.getString(0));
	                edit.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
						try
						{
						int i=Integer.parseInt(v.getTag().toString());
						
						  apint=1;updateapp(i);
						}
						catch (Exception e) {
							// TODO: handle exception
						}
						}
	                });
						
				 	delete=(ImageView) t.findViewWithTag("del");
				 	delete.setTag(app_retrieve.getString(0));
	                delete.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(final View v) {
							// TODO Auto-generated method stub
							
							AlertDialog.Builder builder = new AlertDialog.Builder(DrywallAppliance.this);
							builder.setMessage("Do you want to delete the selected Appliance System?")
									.setCancelable(false)
									.setPositiveButton("Yes",
											new DialogInterface.OnClickListener() {

												public void onClick(DialogInterface dialog,
														int id) {
													
													try
													{
													int i=Integer.parseInt(v.getTag().toString());
													if(i==appCurrent_select_id)
													{
														appCurrent_select_id=0;
													}
													cf.arr_db.execSQL("Delete from "+cf.DRY_appliance+" Where appId='"+i+"'");
													cf.ShowToast("Internal Appliance System deleted successfully.", 0);
													defaultappliance();appnospin.setSelection(0);appnospin.setEnabled(true);
											        ((Button)findViewById(R.id.addapp)).setText("Save/Add More Appliance System");
													Showappvalue();apint=0;
													}
													catch (Exception e) {
														// TODO: handle exception
													}
												}
											})
									.setNegativeButton("No",
											new DialogInterface.OnClickListener() {
												public void onClick(DialogInterface dialog,
														int id) {

													dialog.cancel();
												}
											});
							AlertDialog al=builder.create();
							al.setTitle("Confirmation");
							al.setIcon(R.drawable.alertmsg);
							al.setCancelable(false);
							al.show();
						
							
						}
					});
	                t1.removeAllViews();
					//t.setPadding(10, 0, 0, 0);
					apptblshow.addView(t,lp);
					app_retrieve.moveToNext();
				}
			}else{apptblshow.setVisibility(View.GONE);((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.GONE); }
	}
	protected void updateapp(int i) {
		// TODO Auto-generated method stub
		appCurrent_select_id = i;appnospin.setEnabled(false);
		try{Cursor appret=cf.SelectTablefunction(cf.DRY_appliance, " Where appId='"+i+"'");
		if(appret.getCount()>0)
		{
			appret.moveToFirst();
			String untloc="",appimg="",appunit="",untlocotr="",untaces="",recrep="",copcor="",drkstn="",obscor="";
			appunit=appret.getString(2);
			untloc=appret.getString(3);
			untlocotr=cf.decode(appret.getString(4));
			untaces=appret.getString(5);
			obscor=appret.getString(6);
			recrep=appret.getString(7);
			copcor=appret.getString(8);
			drkstn=appret.getString(9);
			appimg=cf.decode(appret.getString(10));
			
			appnospin.setSelection(appnoadap.getPosition(appunit));
			apptypspin.setSelection(apptypadap.getPosition(untloc));
			if(untloc.equals("Other"))
			{
				((EditText)findViewById(R.id.apptyp_etxt)).setText(untlocotr);
				((EditText)findViewById(R.id.apptyp_etxt)).setVisibility(cf.v1.VISIBLE);
			}
			else
			{
				((EditText)findViewById(R.id.apptyp_etxt)).setVisibility(cf.v1.INVISIBLE);
			}
			appobsspin.setSelection(appobsadap.getPosition(obscor));
			apprecrepspin.setSelection(apprecadap.getPosition(recrep));
			appcopspin.setSelection(appcopadap.getPosition(copcor));
			appdarkspin.setSelection(appdrkkadap.getPosition(drkstn));
			appuntacessspin.setSelection(appacesadap.getPosition(untaces));
			((EditText)findViewById(R.id.updimg_ed)).setText(appimg);
			((Button)findViewById(R.id.img_clr)).setVisibility(cf.v1.VISIBLE);
			((Button)findViewById(R.id.addapp)).setText("Update");
		}
		}catch(Exception e){}
	}
	private void chk_values() {
		// TODO Auto-generated method stub
		try
		{
			Drysum_save=cf.SelectTablefunction(cf.DRY_sumcond, " where fld_srid='"+cf.selectedhomeid+"'");
		   
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		
		try{
			Dryapp_save=cf.SelectTablefunction(cf.DRY_appliance, " where fld_srid='"+cf.selectedhomeid+"'");
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}
	public void defaultappliance() {
		// TODO Auto-generated method stub
		apptypspin.setSelection(0);
		((EditText)findViewById(R.id.apptyp_etxt)).setText("");
		((EditText)findViewById(R.id.apptyp_etxt)).setVisibility(cf.v1.INVISIBLE);
		appobsspin.setSelection(2);
		appuntacessspin.setSelection(2);
		apprecrepspin.setSelection(4);
		appcopspin.setSelection(2);
		appdarkspin.setSelection(2);
		((EditText)findViewById(R.id.updimg_ed)).setText("");
		((Button)findViewById(R.id.img_clr)).setVisibility(cf.v1.INVISIBLE);
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			cf.goclass(63);
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		  if(requestCode==cf.loadcomment_code)
		  {
			load_comment=true;
			if(resultCode==RESULT_OK)
			{
				 cf.ShowToast("Comments added successfully.", 0);
				 String atccomts = ((EditText)findViewById(R.id.appcomment)).getText().toString();
				 ((EditText)findViewById(R.id.appcomment)).setText((atccomts+" "+data.getExtras().getString("Comments")).trim());
				
			}
			
		}
		  else if(requestCode==pick_img_code)
			{
				switch (resultCode) {
					case RESULT_CANCELED:
					//((EditText)findViewById(R.id.whi_ed).findViewWithTag("Path")).setText("");
					break;
					case RESULT_OK:
						cf.ShowToast("Image uploaded successfully.",0);
						((Button)findViewById(R.id.img_clr)).setVisibility(cf.v1.VISIBLE);
						/*if(data.getExtras().getInt("id")==1){
							((EditText)findViewById(R.id.updimg_ed).findViewWithTag("Path")).setText(data.getExtras().getString("path").trim());
							
						}else*/ if(data.getExtras().getInt("id")==2){
							((EditText)findViewById(R.id.updimg_ed).findViewWithTag("ModPath")).setText(data.getExtras().getString("path").trim());
						}
						
					break;
				}
			}
    }
}
