package idsoft.inspectiondepot.ARR;
import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.R.color;
import android.R.string;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract.CommonDataKinds.Relation;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ExportAlert extends Activity {
CommonFunctions cf;
LinearLayout insp_lay[]=new LinearLayout[11]; 
RelativeLayout lay_sub[]=new RelativeLayout[11];
CheckBox ins_chk[]=new CheckBox[11];
CheckBox four_arr[]=new CheckBox[6];
CheckBox survey_arr[]=new CheckBox[3];
CheckBox general_arr[]=new CheckBox[3];
CheckBox B11802_arr[]=new CheckBox[6];
CheckBox COM1_arr[]=new CheckBox[8];
CheckBox COM2_arr[]=new CheckBox[7];
CheckBox COM3_arr[]=new CheckBox[7];
CheckBox GCH_arr[]=new CheckBox[15];
CheckBox SINK_arr[]=new CheckBox[7];
CheckBox DRYWALL_arr[]=new CheckBox[7];
CheckBox Custom_arr[]=new CheckBox[2];

ImageView ins_sta_im[]=new ImageView[11];
ImageView general_sta_im[]=new ImageView[3];
ImageView four_sta_im[]=new ImageView[6];
ImageView survey_sta_im[]=new ImageView[3];
ImageView B11802_sta_im[]=new ImageView[6];
ImageView COM1_sta_im[]=new ImageView[8];
ImageView COM2_sta_im[]=new ImageView[7];
ImageView COM3_sta_im[]=new ImageView[7];
ImageView GCH_sta_im[]=new ImageView[15];
ImageView SINK_sta_im[]=new ImageView[7];
ImageView DRYWALL_sta_im[]=new ImageView[7];
ImageView Custom_sta_im[]=new ImageView[2];
ImageView expend[]=new ImageView[11];
String tmp="";
/****Error log starts****/
LinearLayout par_log,chaild_log;
ImageView log_expend;

Cursor c1;
String selected;
String result="",s1="";
int s=0;
String[]   separated=null;
public void onCreate(Bundle savedInstanceState)
{
	super.onCreate(savedInstanceState);
	setContentView(R.layout.export_alert);
	Bundle b =getIntent().getExtras();
	cf=new CommonFunctions(this);
	cf.selectedhomeid=b.getString("Srid");
	result=b.getString("result");
	
	cf.getInspectorId();
	set_all_check();
	if(result!=null)
	{
		if(result.equals("true"))
		{
			set_results(b);
		}
	}
	for(int j=1;j<insp_lay.length;j++)
	{
		
			insp_lay[j].setVisibility(View.GONE);
		
	}
	
	Cursor c =cf.SelectTablefunction(cf.Select_insp, " WHERE SI_InspectorId='"+cf.Insp_id+"' and SI_srid='"+cf.selectedhomeid+"'");
	if(c.getCount()>0)
	{
		c.moveToFirst();
		selected=cf.decode(c.getString(c.getColumnIndex("SI_InspectionNames")));System.out.println("test="+selected);		
		/*if(selected.contains(","))
		{
			tmp = selected;
			String arr[] = tmp.split(",");
			for(int i=0;i<arr.length;i++)
			{
				System.out.println("arr="+arr[i]);
				if(arr[i].contentEquals("9") || arr[i].contentEquals("11") || arr[i].contentEquals("13") || arr[i].contentEquals("18")
						|| arr[i].contentEquals("28") || arr[i].contentEquals("750") || arr[i].contentEquals("751"))
				{					
					System.out.println("ttttttt="+arr[i]);
				}
				else
				{
						s1 += "false"+"~";
				}
			}
		}
		else
		{
			if(selected.contentEquals("9") || selected.contentEquals("11") || selected.contentEquals("13") || selected.contentEquals("18")
					|| selected.contentEquals("28") || selected.contentEquals("750") || selected.contentEquals("751"))
			{					
				
			}
			else
			{
					s1 += "false"+"~";
			}
		}
		System.out.println("ssss="+s1);
		if(s1.contains("false"))
		{
			selected="10";
		}*/
		/*if(!selected.matches("9") || !selected.matches("11") || !selected.matches("13") || 
				!selected.matches("18") || !selected.matches("28") || !selected.matches("750") ||
				!selected.matches("751"))
		{
			System.out.println("mothins matckes"+selected);
			selected="10";
		}*/
		System.out.println("test sssssss="+selected);
			//Cursor c1 = cf.arr_db.rawQuery("select DISTINCT ARR_Custom_ID,ARR_Insp_Name from " + cf.allinspnamelist + " Where  ARR_Custom_ID in ("+selected+")  order by ARR_Custom_ID", null);
		/*if(selected.equals("10"))
		{*/
			//c1 = cf.arr_db.rawQuery("select Min(ARR_Custom_ID),ARR_Insp_ID,ARR_Insp_Name from " + cf.allinspnamelist + " Where  ARR_Custom_ID in ("+selected+")  order by ARR_Custom_ID", null);	
		//}
		/*else
		{*/
			//c1 =cf.SelectTablefunction(cf.inspnamelist, " Where  ARR_Insp_ID in ("+selected+")  order by ARR_Custom_ID");
		//}
c1 = cf.arr_db.rawQuery("select ARR_Custom_ID,ARR_Insp_ID,ARR_Insp_Name from tbl_allinspnamelist Where  ARR_Insp_ID in ("+selected+")  GROUP BY ARR_Custom_ID",null);
		
			System.out.println("some ieeuse"+c1.getCount()+"select ARR_Custom_ID from tbl_allinspnamelist Where  ARR_Insp_ID in ("+selected+")  GROUP BY ARR_Custom_ID");
			if(c1.getCount()>0)
			{
				c1.moveToFirst();
				separated=new String[c1.getCount()];System.out.println("test"+separated.length);				
			
				for(int i=0;i<c1.getCount();i++,c1.moveToNext())
				{
					System.out.println("id="+c1.getString(c1.getColumnIndex("ARR_Insp_ID")));
					String insid = c1.getString(c1.getColumnIndex("ARR_Insp_ID"));
					if(insid.contentEquals("9") || insid.contentEquals("11") || insid.contentEquals("13") || insid.contentEquals("18")
							|| insid.contentEquals("28") || insid.contentEquals("750") || insid.contentEquals("751"))
					{
						separated[i]=cf.decode(c1.getString(c1.getColumnIndex("ARR_Insp_Name"))).trim();
					}
					else
					{

						System.out.println("testtest");
						separated[i]="Custom Inspection";
						System.out.println("te=="+separated[i]);
					}
					System.out.println("separated[i]="+separated[i]);
				}
			}
		
		
		/*selected=selected.replace("^", "~");
		separated = selected.split("~");*/
		System.out.println("what is length="+cf.All_insp_list.length+"  "+separated.length);
		
		for(int i=0;i<separated.length;i++)
		{
			for(int j=1;j<cf.All_insp_list.length;j++)
			{
				if(separated[i].trim().equals(cf.All_insp_list[j]))
				{
					System.out.println("separated[i]"+separated[i]+"cf.All_insp_list[j]="+cf.All_insp_list[j]);
					if(j==4)
					{
						cf.getPolicyholderInformation(cf.selectedhomeid);
						System.out.println("cf.Stories"+cf.Stories);
						if(!cf.Stories.equals(""))
						{
							if(cf.Stories.equals("1") || cf.Stories.equals("2")  || cf.Stories.equals("3"))
							{
								insp_lay[j].setVisibility(View.VISIBLE);
								ins_chk[j].setText(separated[i]);		
							}
							else if(cf.Stories.equals("4") || cf.Stories.equals("5")  || cf.Stories.equals("6"))
							{
								insp_lay[j+1].setVisibility(View.VISIBLE);
								ins_chk[j+1].setText(separated[i]);		
							}
							else if(Integer.parseInt(cf.Stories)>=7)
							{	
								System.out.println("caminside 777");
								insp_lay[j+2].setVisibility(View.VISIBLE);
								ins_chk[j+2].setText(separated[i]);
							}
						}
					}
					
					else
					{
						insp_lay[j].setVisibility(View.VISIBLE);
						//System.out.println("DFF");
						System.out.println("CCCCC"+separated[i]+"wat is j="+j);
						ins_chk[j].setText(separated[i]);
					}
					
				}
			}
		}
	}
	else
	{
		cf.ShowToast("Sorry no inspection were selected ", 0);
		finish();
	}
	
}
private void set_results(Bundle data) {
	// TODO Auto-generated method stub
	String[] general_boo=data.getStringArray("general");
	String[] insp_boo=data.getStringArray("insp");
	String[] four_boo=data.getStringArray("four");
	String[] survey_boo=data.getStringArray("survey");
	String[] B11802_boo=data.getStringArray("B11802");
	String[] COM1_boo=data.getStringArray("COM1");
	String[] COM2_boo=data.getStringArray("COM2");
	String[] COM3_boo=data.getStringArray("COM3");
	String[] GCH_boo=data.getStringArray("GCH");
	String[] SINK_boo=data.getStringArray("SINK");
	String[] DRYWALL_boo=data.getStringArray("DRYWALL");
	String[] Custom_boo=data.getStringArray("CUSTOM");
	String erro_log=data.getString("error");
	/*for(int i=0;i<GCH_boo.length;i++)
	{
		System.out.println("the insp val"+i+"="+GCH_boo[i]);
	}*/
	if(!erro_log.equals(""))
	{
		par_log.setVisibility(View.VISIBLE);
		String temp[]=erro_log.split("@");
		chaild_log.removeAllViews();
		for(int i=0;i<temp.length;i++)
		{
			TextView tv =new TextView(this);
			tv.setBackgroundColor(getResources().getColor(R.color.Erro_background));
			tv.setTextColor(getResources().getColor(R.color.Erro_text));
			tv.setText(temp[i]);
			tv.setWidth(0);
			View v=new View(this);
			
			chaild_log.addView(tv,LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
			chaild_log.addView(v,LayoutParams.FILL_PARENT,1);
		}
	}
	else
	{
		par_log.setVisibility(View.GONE);
	}
	
	System.out.println("the general"+general_boo.length);
	for(int i=0;i<general_boo.length;i++)
	{
		System.out.println("the i value ="+general_boo[i]);
	}
	String erro=data.getString("error");
	for(int m=0;m<ins_chk.length;m++)
	{
		boolean head_sta=true;
		if(m==0)
		{
			for(int i=0;i<general_boo.length;i++)
			{
				if(general_boo[i].equals("true"))
				{
					general_arr[i].setChecked(true);
					ins_sta_im[m].setVisibility(View.VISIBLE);
					ins_chk[m].setChecked(true);  
					//ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.tick_icon));
					general_sta_im[i].setVisibility(View.VISIBLE);
					general_sta_im[i].setImageDrawable(getResources().getDrawable(R.drawable.tick_icon));
				}
				else if(general_boo[i].equals("false"))
				{
					general_arr[i].setChecked(true);
					ins_chk[m].setChecked(true);
					head_sta=false;
					//ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.iconcross));
					ins_sta_im[m].setVisibility(View.VISIBLE);
					general_sta_im[i].setVisibility(View.VISIBLE);
					general_sta_im[i].setImageDrawable(getResources().getDrawable(R.drawable.iconcross));
				}
			}
			if(head_sta)
			{
				ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.tick_icon));
			}
			else
			{
				ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.iconcross));
			}
		}
		if(m==1)
		{
			for(int i=0;i<four_boo.length;i++)
			{
				if(four_boo[i].equals("true"))
				{
					four_arr[i].setChecked(true);
					ins_sta_im[m].setVisibility(View.VISIBLE);
					ins_chk[m].setChecked(true);  
					//ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.tick_icon));
					four_sta_im[i].setVisibility(View.VISIBLE);
					four_sta_im[i].setImageDrawable(getResources().getDrawable(R.drawable.tick_icon));
				}
				else if(four_boo[i].equals("false"))
				{
					four_arr[i].setChecked(true);
					ins_chk[m].setChecked(true); 
					//ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.iconcross));
					ins_sta_im[m].setVisibility(View.VISIBLE);
					four_sta_im[i].setVisibility(View.VISIBLE);
					head_sta=false;
					four_sta_im[i].setImageDrawable(getResources().getDrawable(R.drawable.iconcross));
				}
			}
			if(head_sta)
			{
				ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.tick_icon));
			}
			else
			{
				ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.iconcross));
			}
		}
		if(m==2)
		{
			for(int i=0;i<survey_boo.length;i++)
			{
				if(survey_boo[i].equals("true"))
				{
					survey_arr[i].setChecked(true);
					ins_sta_im[m].setVisibility(View.VISIBLE);
					ins_chk[m].setChecked(true); 
					//ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.tick_icon));
					survey_sta_im[i].setVisibility(View.VISIBLE);
					survey_sta_im[i].setImageDrawable(getResources().getDrawable(R.drawable.tick_icon));
				}
				else if(survey_boo[i].equals("false"))
				{
					survey_arr[i].setChecked(true);
					ins_sta_im[m].setVisibility(View.VISIBLE);
					ins_chk[m].setChecked(true); 
					//ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.iconcross));
					survey_sta_im[i].setVisibility(View.VISIBLE);
					head_sta=false;
					survey_sta_im[i].setImageDrawable(getResources().getDrawable(R.drawable.iconcross));
				}
				
			}
			if(head_sta)
			{
				ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.tick_icon));
			}
			else
			{
				ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.iconcross));
			}
		}
		if(m==3)
		{
			for(int i=0;i<B11802_boo.length;i++)
			{
				if(B11802_boo[i].equals("true"))
				{
					B11802_arr[i].setChecked(true);
					ins_sta_im[m].setVisibility(View.VISIBLE);
					ins_chk[m].setChecked(true); 
					//ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.tick_icon));
					B11802_sta_im[i].setVisibility(View.VISIBLE);
					B11802_sta_im[i].setImageDrawable(getResources().getDrawable(R.drawable.tick_icon));
				}else if(B11802_boo[i].equals("false"))
				{
					B11802_arr[i].setChecked(true);    
					ins_sta_im[m].setVisibility(View.VISIBLE);
					ins_chk[m].setChecked(true);  
					//ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.iconcross));
					B11802_sta_im[i].setVisibility(View.VISIBLE);
					head_sta=false;
					B11802_sta_im[i].setImageDrawable(getResources().getDrawable(R.drawable.iconcross));
				}
				
				
			}
			if(head_sta)
			{
				ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.tick_icon));
			}
			else
			{
				ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.iconcross));
			}
		}
		if(m==4)
		{
			for(int i=0;i<COM1_boo.length;i++)
			{
				if(COM1_boo[i].equals("true"))
				{
					COM1_arr[i].setChecked(true);
					ins_sta_im[m].setVisibility(View.VISIBLE);
					ins_chk[m].setChecked(true); 
					//ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.tick_icon));
					COM1_sta_im[i].setVisibility(View.VISIBLE);
					COM1_sta_im[i].setImageDrawable(getResources().getDrawable(R.drawable.tick_icon));
				}
				else if(COM1_boo[i].equals("false"))
				{
					COM1_arr[i].setChecked(true);    
					ins_sta_im[m].setVisibility(View.VISIBLE);
					ins_chk[m].setChecked(true); 
					//ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.iconcross));
					COM1_sta_im[i].setVisibility(View.VISIBLE);
					head_sta=false;
					System.out.println("comes in false commeri"+i);
					COM1_sta_im[i].setImageDrawable(getResources().getDrawable(R.drawable.iconcross));
				}
				
				
			}
			if(head_sta)
			{
				ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.tick_icon));
			}
			else
			{
				ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.iconcross));
			}
		}
		if(m==5)
		{
			for(int i=0;i<COM2_boo.length;i++)
			{
				if(COM2_boo[i].equals("true"))
				{
					COM2_arr[i].setChecked(true);
					ins_sta_im[m].setVisibility(View.VISIBLE); 
					ins_chk[m].setChecked(true);  
					//ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.tick_icon));
					COM2_sta_im[i].setVisibility(View.VISIBLE);
					COM2_sta_im[i].setImageDrawable(getResources().getDrawable(R.drawable.tick_icon));
				}
				else if(COM2_boo[i].equals("false"))
				{
					COM2_arr[i].setChecked(true);    
					ins_sta_im[m].setVisibility(View.VISIBLE);
					ins_chk[m].setChecked(true);  
					//ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.iconcross));
					COM2_sta_im[i].setVisibility(View.VISIBLE);
					head_sta=false;
					COM2_sta_im[i].setImageDrawable(getResources().getDrawable(R.drawable.iconcross));
				}
			}
			if(head_sta)
			{
				ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.tick_icon));
			}
			else
			{
				ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.iconcross));
			}
		}
		if(m==6)
		{
			for(int i=0;i<COM3_boo.length;i++)
			{
				if(COM3_boo[i].equals("true"))
				{
					COM3_arr[i].setChecked(true);
					ins_sta_im[m].setVisibility(View.VISIBLE); 
					ins_chk[m].setChecked(true); 
					//ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.tick_icon));
					COM3_sta_im[i].setVisibility(View.VISIBLE);
					COM3_sta_im[i].setImageDrawable(getResources().getDrawable(R.drawable.tick_icon));
				}
				else if(COM3_boo[i].equals("false"))
				{
					COM3_arr[i].setChecked(true);    
					ins_sta_im[m].setVisibility(View.VISIBLE);
					ins_chk[m].setChecked(true);  
					//ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.iconcross));
					COM3_sta_im[i].setVisibility(View.VISIBLE);
					head_sta=false;
					COM3_sta_im[i].setImageDrawable(getResources().getDrawable(R.drawable.iconcross));
				}
			}
			if(head_sta)
			{
				ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.tick_icon));
			}
			else
			{
				ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.iconcross));
			}
		}
		if(m==7)
		{
			for(int i=0;i<GCH_boo.length;i++)
			{
				if(GCH_boo[i].equals("true"))
				{
					GCH_arr[i].setChecked(true);
					ins_sta_im[m].setVisibility(View.VISIBLE);
					ins_chk[m].setChecked(true);  
					//ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.tick_icon));
					GCH_sta_im[i].setVisibility(View.VISIBLE);
					GCH_sta_im[i].setImageDrawable(getResources().getDrawable(R.drawable.tick_icon));
				}
				else if(GCH_boo[i].equals("false"))
				{
					GCH_arr[i].setChecked(true);    
					ins_sta_im[m].setVisibility(View.VISIBLE); 
					ins_chk[m].setChecked(true); 
					//ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.iconcross));
					GCH_sta_im[i].setVisibility(View.VISIBLE);
					head_sta=false;
					GCH_sta_im[i].setImageDrawable(getResources().getDrawable(R.drawable.iconcross));
				}
			}
			if(head_sta)
			{
				ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.tick_icon));
			}
			else
			{
				ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.iconcross));
			}
		}
		if(m==8)
		{
			for(int i=0;i<SINK_boo.length;i++)
			{
				if(SINK_boo[i].equals("true"))
				{
					SINK_arr[i].setChecked(true);
					ins_sta_im[m].setVisibility(View.VISIBLE); 
					ins_chk[m].setChecked(true);  
					//ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.tick_icon));
					SINK_sta_im[i].setVisibility(View.VISIBLE);
					SINK_sta_im[i].setImageDrawable(getResources().getDrawable(R.drawable.tick_icon));
				}
				else if(SINK_boo[i].equals("false"))
				{
					SINK_arr[i].setChecked(true);    
					ins_sta_im[m].setVisibility(View.VISIBLE);
					ins_chk[m].setChecked(true);  
					//ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.iconcross));
					SINK_sta_im[i].setVisibility(View.VISIBLE);
					head_sta=false;
					SINK_sta_im[i].setImageDrawable(getResources().getDrawable(R.drawable.iconcross));
				}
			}
			if(head_sta)
			{
				ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.tick_icon));
			}
			else
			{
				ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.iconcross));
			}
		}
		if(m==9)
		{
			for(int i=0;i<DRYWALL_boo.length;i++)
			{
				if(DRYWALL_boo[i].equals("true"))
				{
					DRYWALL_arr[i].setChecked(true);
					ins_sta_im[m].setVisibility(View.VISIBLE); 
					ins_chk[m].setChecked(true);
				//	ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.tick_icon));
					DRYWALL_sta_im[i].setVisibility(View.VISIBLE);
					DRYWALL_sta_im[i].setImageDrawable(getResources().getDrawable(R.drawable.tick_icon));
				}
				else if(DRYWALL_boo[i].equals("false"))
				{
					DRYWALL_arr[i].setChecked(true);   
					ins_sta_im[m].setVisibility(View.VISIBLE);
					ins_chk[m].setChecked(true);
					//ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.iconcross));
					DRYWALL_sta_im[i].setVisibility(View.VISIBLE);
					head_sta=false;
					DRYWALL_sta_im[i].setImageDrawable(getResources().getDrawable(R.drawable.iconcross));
					
				}
			}
			if(head_sta)
			{
				ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.tick_icon));
			}
			else
			{
				ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.iconcross));
			}
		}
		if(m==10)
		{
			for(int i=0;i<Custom_boo.length;i++)
			{
				if(Custom_boo[i].equals("true"))
				{
					Custom_arr[i].setChecked(true);
					ins_sta_im[m].setVisibility(View.VISIBLE); 
					ins_chk[m].setChecked(true);
					Custom_sta_im[i].setVisibility(View.VISIBLE);
					Custom_sta_im[i].setImageDrawable(getResources().getDrawable(R.drawable.tick_icon));
				}
				else if(Custom_boo[i].equals("false"))
				{
					Custom_arr[i].setChecked(true);   
					ins_sta_im[m].setVisibility(View.VISIBLE);
					ins_chk[m].setChecked(true);
					//ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.iconcross));
					Custom_sta_im[i].setVisibility(View.VISIBLE);
					head_sta=false;
					Custom_sta_im[i].setImageDrawable(getResources().getDrawable(R.drawable.iconcross));
					
				}
			}
			if(head_sta)
			{
				ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.tick_icon));
			}
			else
			{
				ins_sta_im[m].setImageDrawable(getResources().getDrawable(R.drawable.iconcross));
			}
		}
	}
}
public void select_all(int j)
{
	for(int m=0;m<separated.length;m++)
	{
	Boolean boo=true;
	if(j==0 || j==-1 )
	{
		for(int i=0;i<general_arr.length;i++)
		{
			general_arr[i].setChecked(boo);
		}
	}
	if(j==1 ||( j==-1 && separated[m].contains(cf.All_insp_list[1])))
	{
		for(int i=0;i<four_arr.length;i++)
		{
			four_arr[i].setChecked(boo);
		}
	}
	
	if(j==2 ||( j==-1 && separated[m].contains(cf.All_insp_list[2])))
	{
		for(int i=0;i<survey_arr.length;i++)
		{
			survey_arr[i].setChecked(boo);
		}
	}
	if(j==3 ||( j==-1 && separated[m].contains(cf.All_insp_list[3])))
	{
		for(int i=0;i<B11802_arr.length;i++)
		{
			B11802_arr[i].setChecked(boo);
		}
	}
	if(j==4 ||( j==-1 && separated[m].contains(cf.All_insp_list[4])))
	{
		
			cf.getPolicyholderInformation(cf.selectedhomeid);
			if(!cf.Stories.equals(""))
			{
				if(cf.Stories.equals("1") || cf.Stories.equals("2")  || cf.Stories.equals("3"))
				{
					for(int i=0;i<COM1_arr.length;i++)
					{
						COM1_arr[i].setChecked(boo);
					}
							
				}
				else if(cf.Stories.equals("4") || cf.Stories.equals("5")  || cf.Stories.equals("6"))
				{
					for(int i=0;i<COM2_arr.length;i++)
					{
						COM2_arr[i].setChecked(boo);
					}	
				}
				else if(Integer.parseInt(cf.Stories)>=7)
				{	
					for(int i=0;i<COM3_arr.length;i++)
				
				{
					COM3_arr[i].setChecked(boo);
				}
				}
			}
		
	
	}
	/*if(j==5 ||( j==-1 && separated[m].contains(cf.All_insp_list[5])))
	{
		
	}
	if(j==6 ||( j==-1 && separated[m].contains(cf.All_insp_list[6])))
	{
		
	}*/
	if(j==7 ||( j==-1 && separated[m].contains(cf.All_insp_list[7])))
	{
		for(int i=0;i<GCH_arr.length;i++)
		{
			GCH_arr[i].setChecked(boo);
		}
	}
	if(j==8 ||( j==-1 && separated[m].contains(cf.All_insp_list[8])))
	{
		for(int i=0;i<SINK_arr.length;i++)
		{
			SINK_arr[i].setChecked(boo);
		}
	}
	if(j==9 ||( j==-1 && separated[m].contains(cf.All_insp_list[9])))
	{
		for(int i=0;i<DRYWALL_arr.length;i++)
		{
			DRYWALL_arr[i].setChecked(boo);
		}
	}
	if(j==10 ||( j==-1 && separated[m].contains("Custom Inspection")))
	{
		System.out.println("insisde j is 10");
		for(int i=0;i<Custom_arr.length;i++)
		{
			Custom_arr[i].setChecked(boo);
		}
	}
	if(j==-1)
	{
		for(int i=0;i<ins_chk.length;i++)
		{
			if(ins_chk[i].getVisibility()==View.VISIBLE)
				ins_chk[i].setChecked(boo);
		}
	}
}
}
private void set_all_check() {
	// TODO Auto-generated method stub
	
	insp_lay[0]=(LinearLayout) findViewById(R.id.ex_lay_general);
	lay_sub[0]=(RelativeLayout) findViewById(R.id.ex_general_sub_lay);
	expend[0]=(ImageView) findViewById(R.id.ex_expend_general);
	ins_chk[0]=(CheckBox) findViewById(R.id.ex_chk_general);
	ins_sta_im[0]=(ImageView) findViewById(R.id.ex_status_general);
	expend[0].setOnClickListener(new expend(0));
	ins_chk[0].setOnClickListener(new checked(0));
	general_arr[0]=(CheckBox) findViewById(R.id.ex_chk_sub_general1);
	general_arr[1]=(CheckBox) findViewById(R.id.ex_chk_sub_general2);
	general_arr[2]=(CheckBox) findViewById(R.id.ex_chk_sub_general3);
	
	general_sta_im[0]=(ImageView) findViewById(R.id.ex_sta_sub_general1);
	general_sta_im[1]=(ImageView) findViewById(R.id.ex_sta_sub_general2);
	general_sta_im[2]=(ImageView) findViewById(R.id.ex_sta_sub_general3);
	
	insp_lay[1]=(LinearLayout) findViewById(R.id.ex_lay_four);
	lay_sub[1]=(RelativeLayout) findViewById(R.id.ex_four_sub_lay);
	expend[1]=(ImageView) findViewById(R.id.ex_expend_four);
	ins_chk[1]=(CheckBox) findViewById(R.id.ex_chk_four);
	ins_sta_im[1]=(ImageView) findViewById(R.id.ex_status_four);
	expend[1].setOnClickListener(new expend(1));
	ins_chk[1].setOnClickListener(new checked(1));
	four_arr[0]=(CheckBox) findViewById(R.id.ex_chk_sub_four1);
	//four_arr[1]=(CheckBox) findViewById(R.id.ex_chk_sub_four2);
	four_arr[1]=(CheckBox) findViewById(R.id.ex_chk_sub_four3);
	four_arr[2]=(CheckBox) findViewById(R.id.ex_chk_sub_four4);
	four_arr[3]=(CheckBox) findViewById(R.id.ex_chk_sub_four5);
	four_arr[4]=(CheckBox) findViewById(R.id.ex_chk_sub_four_p);
	four_arr[5]=(CheckBox) findViewById(R.id.ex_chk_sub_four_f);

	four_sta_im[0]=(ImageView) findViewById(R.id.ex_sta_sub_four1);
	//four_sta_im[1]=(ImageView) findViewById(R.id.ex_sta_sub_four2);
	four_sta_im[1]=(ImageView) findViewById(R.id.ex_sta_sub_four3);
	four_sta_im[2]=(ImageView) findViewById(R.id.ex_sta_sub_four4);
	four_sta_im[3]=(ImageView) findViewById(R.id.ex_sta_sub_four5);
	four_sta_im[4]=(ImageView) findViewById(R.id.ex_sta_sub_four6);
	four_sta_im[5]=(ImageView) findViewById(R.id.ex_sta_sub_four7);

	
	insp_lay[2]=(LinearLayout) findViewById(R.id.ex_lay_survey);
	lay_sub[2]=(RelativeLayout) findViewById(R.id.ex_survey_sub_lay);
	expend[2]=(ImageView) findViewById(R.id.ex_expend_survey);
	ins_chk[2]=(CheckBox) findViewById(R.id.ex_chk_survey);
	ins_sta_im[2]=(ImageView) findViewById(R.id.ex_status_survey);
	expend[2].setOnClickListener(new expend(2));
	ins_chk[2].setOnClickListener(new checked(2));
	survey_arr[0]=(CheckBox) findViewById(R.id.ex_chk_sub_survey1);
	survey_arr[1]=(CheckBox) findViewById(R.id.ex_chk_sub_survey_p);
	survey_arr[2]=(CheckBox) findViewById(R.id.ex_chk_sub_survey_f);
	
	survey_sta_im[0]=(ImageView) findViewById(R.id.ex_sta_sub_survey1);
	survey_sta_im[1]=(ImageView) findViewById(R.id.ex_sta_sub_survey2);
	survey_sta_im[2]=(ImageView) findViewById(R.id.ex_sta_sub_survey3);
	
	
	insp_lay[3]=(LinearLayout) findViewById(R.id.ex_lay_B11802);
	lay_sub[3]=(RelativeLayout) findViewById(R.id.ex_B11802_sub_lay);
	expend[3]=(ImageView) findViewById(R.id.ex_expend_B11802);
	ins_chk[3]=(CheckBox) findViewById(R.id.ex_chk_B11802);
	ins_sta_im[3]=(ImageView) findViewById(R.id.ex_status_B11802);
	expend[3].setOnClickListener(new expend(3));
	ins_chk[3].setOnClickListener(new checked(3));
	B11802_arr[0]=(CheckBox) findViewById(R.id.ex_chk_sub_B118021);
	B11802_arr[1]=(CheckBox) findViewById(R.id.ex_chk_sub_B118022);
	B11802_arr[2]=(CheckBox) findViewById(R.id.ex_chk_sub_B118023);
	B11802_arr[3]=(CheckBox) findViewById(R.id.ex_chk_sub_B118024);
	B11802_arr[4]=(CheckBox) findViewById(R.id.ex_chk_sub_B11802_p);
	B11802_arr[5]=(CheckBox) findViewById(R.id.ex_chk_sub_B11802_f);
	
	B11802_sta_im[0]=(ImageView) findViewById(R.id.ex_sta_sub_B118021);
	B11802_sta_im[1]=(ImageView) findViewById(R.id.ex_sta_sub_B118022);
	B11802_sta_im[2]=(ImageView) findViewById(R.id.ex_sta_sub_B118023);
	B11802_sta_im[3]=(ImageView) findViewById(R.id.ex_sta_sub_B118024);
	B11802_sta_im[4]=(ImageView) findViewById(R.id.ex_sta_sub_B118025);
	B11802_sta_im[5]=(ImageView) findViewById(R.id.ex_sta_sub_B118026);
	
	insp_lay[4]=(LinearLayout) findViewById(R.id.ex_lay_COM1);
	lay_sub[4]=(RelativeLayout) findViewById(R.id.ex_COM1_sub_lay);
	expend[4]=(ImageView) findViewById(R.id.ex_expend_COM1);
	ins_chk[4]=(CheckBox) findViewById(R.id.ex_chk_COM1);
	ins_sta_im[4]=(ImageView) findViewById(R.id.ex_status_COM1);
	expend[4].setOnClickListener(new expend(4));
	ins_chk[4].setOnClickListener(new checked(4));
	COM1_arr[0]=(CheckBox) findViewById(R.id.ex_chk_sub_COM11);
	COM1_arr[1]=(CheckBox) findViewById(R.id.ex_chk_sub_COM12);
	COM1_arr[2]=(CheckBox) findViewById(R.id.ex_chk_sub_COM13);
	COM1_arr[3]=(CheckBox) findViewById(R.id.ex_chk_sub_COM14);
	COM1_arr[4]=(CheckBox) findViewById(R.id.ex_chk_sub_COM15);
	COM1_arr[5]=(CheckBox) findViewById(R.id.ex_chk_sub_COM16);
	COM1_arr[6]=(CheckBox) findViewById(R.id.ex_chk_sub_COM1_p);
	COM1_arr[7]=(CheckBox) findViewById(R.id.ex_chk_sub_COM1_f);
	
	COM1_sta_im[0]=(ImageView) findViewById(R.id.ex_sta_sub_COM11);
	COM1_sta_im[1]=(ImageView) findViewById(R.id.ex_sta_sub_COM12);
	COM1_sta_im[2]=(ImageView) findViewById(R.id.ex_sta_sub_COM13);
	COM1_sta_im[3]=(ImageView) findViewById(R.id.ex_sta_sub_COM14);
	COM1_sta_im[4]=(ImageView) findViewById(R.id.ex_sta_sub_COM15);
	COM1_sta_im[5]=(ImageView) findViewById(R.id.ex_sta_sub_COM16);
	COM1_sta_im[6]=(ImageView) findViewById(R.id.ex_sta_sub_COM17);
	COM1_sta_im[7]=(ImageView) findViewById(R.id.ex_sta_sub_COM18);
	
	insp_lay[5]=(LinearLayout) findViewById(R.id.ex_lay_COM2);
	lay_sub[5]=(RelativeLayout) findViewById(R.id.ex_COM2_sub_lay);
	expend[5]=(ImageView) findViewById(R.id.ex_expend_COM2);
	ins_chk[5]=(CheckBox) findViewById(R.id.ex_chk_COM2);
	ins_sta_im[5]=(ImageView) findViewById(R.id.ex_status_COM2);
	expend[5].setOnClickListener(new expend(5));
	ins_chk[5].setOnClickListener(new checked(5));
	COM2_arr[0]=(CheckBox) findViewById(R.id.ex_chk_sub_COM21);
	COM2_arr[1]=(CheckBox) findViewById(R.id.ex_chk_sub_COM22);
	COM2_arr[2]=(CheckBox) findViewById(R.id.ex_chk_sub_COM23);
	COM2_arr[3]=(CheckBox) findViewById(R.id.ex_chk_sub_COM24);
	COM2_arr[4]=(CheckBox) findViewById(R.id.ex_chk_sub_COM25);
	COM2_arr[5]=(CheckBox) findViewById(R.id.ex_chk_sub_COM2_p);
	COM2_arr[6]=(CheckBox) findViewById(R.id.ex_chk_sub_COM2_f);
	
	COM2_sta_im[0]=(ImageView) findViewById(R.id.ex_sta_sub_COM21);
	COM2_sta_im[1]=(ImageView) findViewById(R.id.ex_sta_sub_COM22);
	COM2_sta_im[2]=(ImageView) findViewById(R.id.ex_sta_sub_COM23);
	COM2_sta_im[3]=(ImageView) findViewById(R.id.ex_sta_sub_COM24);
	COM2_sta_im[4]=(ImageView) findViewById(R.id.ex_sta_sub_COM25);
	COM2_sta_im[5]=(ImageView) findViewById(R.id.ex_sta_sub_COM26);
	COM2_sta_im[6]=(ImageView) findViewById(R.id.ex_sta_sub_COM27);
	
	insp_lay[6]=(LinearLayout) findViewById(R.id.ex_lay_COM3);
	lay_sub[6]=(RelativeLayout) findViewById(R.id.ex_COM3_sub_lay);
	expend[6]=(ImageView) findViewById(R.id.ex_expend_COM3);
	ins_chk[6]=(CheckBox) findViewById(R.id.ex_chk_COM3);
	ins_sta_im[6]=(ImageView) findViewById(R.id.ex_status_COM3);
	expend[6].setOnClickListener(new expend(6));
	ins_chk[6].setOnClickListener(new checked(6));
	COM3_arr[0]=(CheckBox) findViewById(R.id.ex_chk_sub_COM31);
	COM3_arr[1]=(CheckBox) findViewById(R.id.ex_chk_sub_COM32);
	COM3_arr[2]=(CheckBox) findViewById(R.id.ex_chk_sub_COM33);
	COM3_arr[3]=(CheckBox) findViewById(R.id.ex_chk_sub_COM34);
	COM3_arr[4]=(CheckBox) findViewById(R.id.ex_chk_sub_COM35);
	COM3_arr[5]=(CheckBox) findViewById(R.id.ex_chk_sub_COM3_p);
	COM3_arr[6]=(CheckBox) findViewById(R.id.ex_chk_sub_COM3_f);
	
	COM3_sta_im[0]=(ImageView) findViewById(R.id.ex_sta_sub_COM31);
	COM3_sta_im[1]=(ImageView) findViewById(R.id.ex_sta_sub_COM32);
	COM3_sta_im[2]=(ImageView) findViewById(R.id.ex_sta_sub_COM33);
	COM3_sta_im[3]=(ImageView) findViewById(R.id.ex_sta_sub_COM34);
	COM3_sta_im[4]=(ImageView) findViewById(R.id.ex_sta_sub_COM35);
	COM3_sta_im[5]=(ImageView) findViewById(R.id.ex_sta_sub_COM36);
	COM3_sta_im[6]=(ImageView) findViewById(R.id.ex_sta_sub_COM37);
	
	insp_lay[7]=(LinearLayout) findViewById(R.id.ex_lay_GCH);
	lay_sub[7]=(RelativeLayout) findViewById(R.id.ex_GCH_sub_lay);
	expend[7]=(ImageView) findViewById(R.id.ex_expend_GCH);
	ins_chk[7]=(CheckBox) findViewById(R.id.ex_chk_GCH);
	ins_sta_im[7]=(ImageView) findViewById(R.id.ex_status_GCH);
	expend[7].setOnClickListener(new expend(7));
	ins_chk[7].setOnClickListener(new checked(7));
	GCH_arr[0]=(CheckBox) findViewById(R.id.ex_chk_sub_GCH1);
	GCH_arr[1]=(CheckBox) findViewById(R.id.ex_chk_sub_GCH2);
	GCH_arr[2]=(CheckBox) findViewById(R.id.ex_chk_sub_GCH3);
	GCH_arr[3]=(CheckBox) findViewById(R.id.ex_chk_sub_GCH4);
	GCH_arr[4]=(CheckBox) findViewById(R.id.ex_chk_sub_GCH5);
	GCH_arr[5]=(CheckBox) findViewById(R.id.ex_chk_sub_GCH6);
	GCH_arr[6]=(CheckBox) findViewById(R.id.ex_chk_sub_GCH7);
	GCH_arr[7]=(CheckBox) findViewById(R.id.ex_chk_sub_GCH8);
	GCH_arr[8]=(CheckBox) findViewById(R.id.ex_chk_sub_GCH9);
	GCH_arr[9]=(CheckBox) findViewById(R.id.ex_chk_sub_GCH10);
	GCH_arr[10]=(CheckBox) findViewById(R.id.ex_chk_sub_GCH11);
	GCH_arr[11]=(CheckBox) findViewById(R.id.ex_chk_sub_GCH12);
	GCH_arr[12]=(CheckBox) findViewById(R.id.ex_chk_sub_GCH_p);
	GCH_arr[13]=(CheckBox) findViewById(R.id.ex_chk_sub_GCH_f);
	GCH_arr[14]=(CheckBox) findViewById(R.id.ex_chk_sub_GCH15);
	
	GCH_sta_im[0]=(ImageView) findViewById(R.id.ex_sta_sub_GCH1);
	GCH_sta_im[1]=(ImageView) findViewById(R.id.ex_sta_sub_GCH2);
	GCH_sta_im[2]=(ImageView) findViewById(R.id.ex_sta_sub_GCH3);
	GCH_sta_im[3]=(ImageView) findViewById(R.id.ex_sta_sub_GCH4);
	GCH_sta_im[4]=(ImageView) findViewById(R.id.ex_sta_sub_GCH5);
	GCH_sta_im[5]=(ImageView) findViewById(R.id.ex_sta_sub_GCH6);
	GCH_sta_im[6]=(ImageView) findViewById(R.id.ex_sta_sub_GCH7);
	GCH_sta_im[7]=(ImageView) findViewById(R.id.ex_sta_sub_GCH8);
	GCH_sta_im[8]=(ImageView) findViewById(R.id.ex_sta_sub_GCH9);
	GCH_sta_im[9]=(ImageView) findViewById(R.id.ex_sta_sub_GCH10);
	GCH_sta_im[10]=(ImageView) findViewById(R.id.ex_sta_sub_GCH11);
	GCH_sta_im[11]=(ImageView) findViewById(R.id.ex_sta_sub_GCH12);
	GCH_sta_im[12]=(ImageView) findViewById(R.id.ex_sta_sub_GCH13);
	GCH_sta_im[13]=(ImageView) findViewById(R.id.ex_sta_sub_GCH14);
	GCH_sta_im[14]=(ImageView) findViewById(R.id.ex_sta_sub_GCH15);
	
	insp_lay[8]=(LinearLayout) findViewById(R.id.ex_lay_SINK);
	lay_sub[8]=(RelativeLayout) findViewById(R.id.ex_SINK_sub_lay);
	expend[8]=(ImageView) findViewById(R.id.ex_expend_SINK);
	ins_chk[8]=(CheckBox) findViewById(R.id.ex_chk_SINK);
	ins_sta_im[8]=(ImageView) findViewById(R.id.ex_status_SINK);
	expend[8].setOnClickListener(new expend(8));
	ins_chk[8].setOnClickListener(new checked(8));
	SINK_arr[0]=(CheckBox) findViewById(R.id.ex_chk_sub_SINK1);
	SINK_arr[1]=(CheckBox) findViewById(R.id.ex_chk_sub_SINK2);
	SINK_arr[2]=(CheckBox) findViewById(R.id.ex_chk_sub_SINK3);
	SINK_arr[3]=(CheckBox) findViewById(R.id.ex_chk_sub_SINK4);
	SINK_arr[4]=(CheckBox) findViewById(R.id.ex_chk_sub_SINK5);
	SINK_arr[5]=(CheckBox) findViewById(R.id.ex_chk_sub_SINK_p);
	SINK_arr[6]=(CheckBox) findViewById(R.id.ex_chk_sub_SINK_f);
	
	SINK_sta_im[0]=(ImageView) findViewById(R.id.ex_sta_sub_SINK1);
	SINK_sta_im[1]=(ImageView) findViewById(R.id.ex_sta_sub_SINK2);
	SINK_sta_im[2]=(ImageView) findViewById(R.id.ex_sta_sub_SINK3);
	SINK_sta_im[3]=(ImageView) findViewById(R.id.ex_sta_sub_SINK4);
	SINK_sta_im[4]=(ImageView) findViewById(R.id.ex_sta_sub_SINK5);
	SINK_sta_im[5]=(ImageView) findViewById(R.id.ex_sta_sub_SINK6);
	SINK_sta_im[6]=(ImageView) findViewById(R.id.ex_sta_sub_SINK7);
	
	insp_lay[9]=(LinearLayout) findViewById(R.id.ex_lay_DRYWALL);
	
	lay_sub[9]=(RelativeLayout) findViewById(R.id.ex_DRYWALL_sub_lay);
	expend[9]=(ImageView) findViewById(R.id.ex_expend_DRYWALL);
	ins_chk[9]=(CheckBox) findViewById(R.id.ex_chk_DRYWALL);
	ins_sta_im[9]=(ImageView) findViewById(R.id.ex_status_DRYWALL);
	expend[9].setOnClickListener(new expend(9));
	ins_chk[9].setOnClickListener(new checked(9));
	DRYWALL_arr[0]=(CheckBox) findViewById(R.id.ex_chk_sub_DRYWALL1);
	DRYWALL_arr[1]=(CheckBox) findViewById(R.id.ex_chk_sub_DRYWALL2);
	DRYWALL_arr[2]=(CheckBox) findViewById(R.id.ex_chk_sub_DRYWALL3);
	DRYWALL_arr[3]=(CheckBox) findViewById(R.id.ex_chk_sub_DRYWALL4);
	DRYWALL_arr[4]=(CheckBox) findViewById(R.id.ex_chk_sub_DRYWALL5);
	//DRYWALL_arr[5]=(CheckBox) findViewById(R.id.ex_chk_sub_DRYWALL6);
	DRYWALL_arr[5]=(CheckBox) findViewById(R.id.ex_chk_sub_DRYWALL_p);
	DRYWALL_arr[6]=(CheckBox) findViewById(R.id.ex_chk_sub_DRYWALL_f);
	
	DRYWALL_sta_im[0]=(ImageView) findViewById(R.id.ex_sta_sub_DRYWALL1);
	DRYWALL_sta_im[1]=(ImageView) findViewById(R.id.ex_sta_sub_DRYWALL2);
	DRYWALL_sta_im[2]=(ImageView) findViewById(R.id.ex_sta_sub_DRYWALL3);
	DRYWALL_sta_im[3]=(ImageView) findViewById(R.id.ex_sta_sub_DRYWALL4);
	DRYWALL_sta_im[4]=(ImageView) findViewById(R.id.ex_sta_sub_DRYWALL5);
	DRYWALL_sta_im[5]=(ImageView) findViewById(R.id.ex_sta_sub_DRYWALL6);
	DRYWALL_sta_im[6]=(ImageView) findViewById(R.id.ex_sta_sub_DRYWALL_f);
	//DRYWALL_sta_im[7]=(ImageView) findViewById(R.id.ex_sta_sub_DRYWALL8);
	ins_chk[10]=(CheckBox) findViewById(R.id.ex_chk_Custom);
	ins_chk[10].setOnClickListener(new checked(10));
	insp_lay[10]=(LinearLayout) findViewById(R.id.ex_lay_Custom);
	ins_sta_im[10]=(ImageView) findViewById(R.id.ex_status_Custom);
	lay_sub[10]=(RelativeLayout) findViewById(R.id.ex_Custom_sub_lay);
	expend[10]=(ImageView) findViewById(R.id.ex_expend_Custom);
	expend[10].setOnClickListener(new expend(10));
	Custom_arr[0]=(CheckBox) findViewById(R.id.ex_chk_sub_Custom1);
	Custom_arr[1]=(CheckBox) findViewById(R.id.ex_chk_sub_Custom2);
	
	Custom_sta_im[0]=(ImageView) findViewById(R.id.ex_sta_sub_Custom1);
	Custom_sta_im[1]=(ImageView) findViewById(R.id.ex_sta_sub_Custom2);
	
	
	par_log =(LinearLayout) findViewById(R.id.ex_lay_error_log);
	chaild_log =(LinearLayout) findViewById(R.id.list_error);
	log_expend =(ImageView) findViewById(R.id.ex_expend_show_error);
	log_expend.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if(log_expend.getTag().toString().equals("plus"))
			{
				chaild_log.setVisibility(View.VISIBLE);
				log_expend.setImageDrawable(getResources().getDrawable(R.drawable.minus));
				log_expend.setTag("minus");
			}
			else
			{
				chaild_log.setVisibility(View.GONE);
				log_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
				log_expend.setTag("plus");
			}
		}
	});
}

class checked implements OnClickListener
{
	int i;
	checked(int i)
	{
		this.i=i;
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		System.out.println("the i value="+i);
		
		if(((CheckBox)v).isChecked())
		{

			expend[i].setTag("minus");
			expend[i].setImageDrawable(getResources().getDrawable(R.drawable.minus));
			lay_sub[i].setVisibility(View.VISIBLE);
			if(i==5|| i==6)
			{
				select_all(4);
			}
			else
			{
			select_all(i);
			}
			
			// TODO Auto-generated method stub
			
		}
		else
		{
			expend[i].setTag("plus");
			expend[i].setImageDrawable(getResources().getDrawable(R.drawable.plus));
			lay_sub[i].setVisibility(View.GONE);
			clear_all_selection(cf.All_insp_list[i]);
			
		}
		
	}
	
}
class expend implements OnClickListener
{
	int i;
	expend(int i)
	{
		this.i=i;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		System.out.println("the i value="+i);
		if(v.getTag().equals("plus"))
		{

			expend[i].setTag("minus");
			expend[i].setImageDrawable(getResources().getDrawable(R.drawable.minus));
			lay_sub[i].setVisibility(View.VISIBLE);
			
			// TODO Auto-generated method stub
			
		}
		else
		{
			expend[i].setTag("plus");
			expend[i].setImageDrawable(getResources().getDrawable(R.drawable.plus));
			lay_sub[i].setVisibility(View.GONE);
			/*if(i==1)
			{
				for(int i=0;i<four_arr.length;i++)
				{
					four_arr[i].setChecked(false);
				}
			}
			if(i==2)
			{
				for(int i=0;i<survey_arr.length;i++)
				{
					survey_arr[i].setChecked(false);
				}
			}
			if(i==3)
			{
				for(int i=0;i<B11802_arr.length;i++)
				{
					B11802_arr[i].setChecked(false);
				}
			}
			if(i==4)
			{
				for(int i=0;i<COM1_arr.length;i++)
				{
					COM1_arr[i].setChecked(false);
				}
			}
			if(i==5)
			{
				for(int i=0;i<COM2_arr.length;i++)
				{
					COM2_arr[i].setChecked(false);
				}
			}
			if(i==6)
			{
				for(int i=0;i<COM3_arr.length;i++)
				{
					COM3_arr[i].setChecked(false);
				}
			}
			if(i==7)
			{
				for(int i=0;i<GCH_arr.length;i++)
				{
					GCH_arr[i].setChecked(false);
				}
			}
			if(i==8)
			{
				for(int i=0;i<SINK_arr.length;i++)
				{
					SINK_arr[i].setChecked(false);
				}
			}
			if(i==9)
			{
				for(int i=0;i<DRYWALL_arr.length;i++)
				{
					DRYWALL_arr[i].setChecked(false);
				}
			}*/
			
			
			
			

			
		}
	}
	
}
public void clicker(View v)
{
	switch(v.getId())
	{
		case R.id.clear:
			clear_all_selection("ALL");
		break;
		case R.id.cancel:
			Intent in =new Intent();
			setResult(RESULT_CANCELED,in);
			finish();
			
		break;
		case R.id.close:
			Intent in1 =new Intent();
			setResult(RESULT_CANCELED,in1);
			finish();
			
		break;
		case R.id.export:
			System.out.println("length="+ins_chk.length);
			for(int i =0;i<ins_chk.length;i++)
			{
				System.out.println("i="+i);
				if(ins_chk[i].isChecked())
				{
					System.out.println("came herer");
					send_export_insp();
					return;
				}
			}
			cf.ShowToast("Please select altleast one module to export", 0);
		break;
		case R.id.ex_chk_selectall:
			if(((CheckBox)v).isChecked())
			{
				System.out.println("come correctly");
				select_all(-1);
			}
			else
			{
				clear_all_selection("ALL");
			}
		break;
	}
}

private void send_export_insp() {
	// TODO Auto-generated method stub
	String insp_boo[]={"","","","","","","","","","",""};
	String general_boo[]={"","",""};
	String four_boo[]={"","","","","","",""};
	String survey_boo[]={"","",""};
	String B11802_boo[]={"","","","","",""};
	String COM1_boo[]={"","","","","","","",""};
	String COM2_boo[]={"","","","","","",""};
	String COM3_boo[]={"","","","","","",""};
	String GCH_boo[]={"","","","","","","","","","","","","","",""};
	String SINK_boo[]={"","","","","","",""};
	String DRYWALL_boo[]={"","","","","","","",""};
	String CUSTOM_boo[]={"",""};
	for(int m=0;m<ins_chk.length;m++)
	{
		if(m==0)
		{
			for(int i=0;i<general_arr.length;i++)
			{
				if(general_arr[i].isChecked())
				{
					general_boo[i]="true";
					insp_boo[m]="true";
				}
				else
				{
					general_boo[i]="";
				}
			}
		}
		
		if(m==1)
		{
			for(int i=0;i<four_arr.length;i++)
			{
				if(four_arr[i].isChecked())
				{
					four_boo[i]="true";
					insp_boo[m]="true";
				}
				else
				{
					four_boo[i]="";
				}
			}
		}
		if(m==2)
		{
			for(int i=0;i<survey_arr.length;i++)
			{
				if(survey_arr[i].isChecked())
				{
					survey_boo[i]="true";
					insp_boo[m]="true";
				}
				else
				{
					survey_boo[i]="";
				}
				
			}
		}
		if(m==3)
		{
			for(int i=0;i<B11802_arr.length;i++)
			{
				if(B11802_arr[i].isChecked())
				{
					B11802_boo[i]="true";
					insp_boo[m]="true";
				}else
				{
					B11802_boo[i]="";
				}
				
				
			}
		}
		if(m==4)
		{
			for(int i=0;i<COM1_arr.length;i++)
			{
				if(COM1_arr[i].isChecked())
				{
					COM1_boo[i]="true";
					insp_boo[m]="true";
				}
				else
				{
					COM1_boo[i]="";
				}
				
				
			}
		}
		if(m==5)
		{
			for(int i=0;i<COM2_arr.length;i++)
			{
				if(COM2_arr[i].isChecked())
				{
					COM2_boo[i]="true";
					insp_boo[m]="true";
				}
				else
				{
					COM2_boo[i]="";
				}
			}
		}
		if(m==6)
		{
			for(int i=0;i<COM3_arr.length;i++)
			{
				if(COM3_arr[i].isChecked())
				{
					COM3_boo[i]="true";
					insp_boo[m]="true";
				}
				else
				{
					COM3_boo[i]="";
				}
			}
		}
		if(m==7)
		{
			for(int i=0;i<GCH_arr.length;i++)
			{
				if(GCH_arr[i].isChecked())
				{
					GCH_boo[i]="true";
					insp_boo[m]="true";
				}
				else
				{
					GCH_boo[i]="";
				}
			}
		}
		if(m==8)
		{
			for(int i=0;i<SINK_arr.length;i++)
			{
				if(SINK_arr[i].isChecked())
				{
					SINK_boo[i]="true";
					insp_boo[m]="true";
				}
				else
				{
					SINK_boo[i]="";
				}
			}
		}
		if(m==9)
		{
			for(int i=0;i<DRYWALL_arr.length;i++)
			{
				if(DRYWALL_arr[i].isChecked())
				{
					DRYWALL_boo[i]="true";
					insp_boo[m]="true";
				}
				else
				{
					DRYWALL_boo[i]="";
				}
			}
		}
		if(m==10)
		{
			for(int i=0;i<Custom_arr.length;i++)
			{
				System.out.println("FFFF"+Custom_arr.length);
				if(Custom_arr[i].isChecked())
				{
					System.out.println("FAAA");
					CUSTOM_boo[i]="true";
					insp_boo[m]="true";
				}
				else
				{
					CUSTOM_boo[i]="";
				}
			}
		}
	}
	Intent n= new Intent();
	n.putExtra("insp", insp_boo);
	n.putExtra("general", general_boo);
	n.putExtra("four", four_boo);
	n.putExtra("survey", survey_boo);
	n.putExtra("B11802", B11802_boo);
	n.putExtra("COM1", COM1_boo);
	n.putExtra("COM2", COM2_boo);
	n.putExtra("COM3", COM3_boo);
	n.putExtra("GCH", GCH_boo);
	n.putExtra("SINK", SINK_boo);
	n.putExtra("DRYWALL", DRYWALL_boo);
	n.putExtra("CUSTOM", CUSTOM_boo);
	
	setResult(RESULT_OK,n);
	finish();
	
}

private void clear_all_selection(String insp) {
	// TODO Auto-generated method stub
	if(insp.equals(cf.All_insp_list[0]) || insp.equals("ALL"))
	{
		for(int i=0;i<general_arr.length;i++)
		{
			general_arr[i].setChecked(false);
		}
		expend[0].setTag("plus");
		expend[0].setImageDrawable(getResources().getDrawable(R.drawable.plus));
		lay_sub[0].setVisibility(View.GONE);
		ins_chk[0].setChecked(false);
		
	}
	if(insp.equals(cf.All_insp_list[1]) || insp.equals("ALL"))
	{
		for(int i=0;i<four_arr.length;i++)
		{
			four_arr[i].setChecked(false);
		}
		expend[1].setTag("plus");
		expend[1].setImageDrawable(getResources().getDrawable(R.drawable.plus));
		lay_sub[1].setVisibility(View.GONE);
		ins_chk[1].setChecked(false);
		
	}
	if(insp.equals(cf.All_insp_list[2]) || insp.equals("ALL"))
	{
		for(int i=0;i<survey_arr.length;i++)
		{
			survey_arr[i].setChecked(false);
		}
		expend[2].setTag("plus");
		expend[2].setImageDrawable(getResources().getDrawable(R.drawable.plus));
		lay_sub[2].setVisibility(View.GONE);
		ins_chk[2].setChecked(false);
		
	}
	if(insp.equals(cf.All_insp_list[3]) || insp.equals("ALL"))
	{
		for(int i=0;i<B11802_arr.length;i++)
		{
			B11802_arr[i].setChecked(false);
		}
		expend[3].setTag("plus");
		expend[3].setImageDrawable(getResources().getDrawable(R.drawable.plus));
		lay_sub[3].setVisibility(View.GONE);
		ins_chk[3].setChecked(false);
	
	}
	if(insp.equals(cf.All_insp_list[4]) || insp.equals("ALL"))
	{
		
		cf.getPolicyholderInformation(cf.selectedhomeid);
		if(!cf.Stories.equals(""))
		{
			if(cf.Stories.equals("1") || cf.Stories.equals("2")  || cf.Stories.equals("3"))
			{
				for(int i=0;i<COM1_arr.length;i++)
				{
					COM1_arr[i].setChecked(false);
					
				}
						
				expend[4].setTag("plus");
				expend[4].setImageDrawable(getResources().getDrawable(R.drawable.plus));
				lay_sub[4].setVisibility(View.GONE);
				ins_chk[4].setChecked(false);
			}
			else if(cf.Stories.equals("4") || cf.Stories.equals("5")  || cf.Stories.equals("6"))
			{
				
				for(int i=0;i<COM2_arr.length;i++)
				{COM2_arr[i].setChecked(false);
				
				}
				expend[5].setTag("plus");
				expend[5].setImageDrawable(getResources().getDrawable(R.drawable.plus));
				lay_sub[5].setVisibility(View.GONE);
				ins_chk[5].setChecked(false);
			}
			else if(Integer.parseInt(cf.Stories)>7)
			{	
				for(int i=0;i<COM3_arr.length;i++)
				{COM3_arr[i].setChecked(false);
				
				}
				expend[6].setTag("plus");
				expend[6].setImageDrawable(getResources().getDrawable(R.drawable.plus));
				lay_sub[6].setVisibility(View.GONE);
				ins_chk[6].setChecked(false);
			}
		}
		/*for(int i=0;i<COM1_arr.length;i++)
		{
			COM1_arr[i].setChecked(false);
		}
		expend[4].setTag("plus");
		expend[4].setImageDrawable(getResources().getDrawable(R.drawable.plus));
		lay_sub[4].setVisibility(View.GONE);
		ins_chk[4].setChecked(false);*/
		
	}
	/*if(insp.equals(cf.All_insp_list[5]) || insp.equals("ALL"))
	{
		for(int i=0;i<COM2_arr.length;i++)
		{
			COM2_arr[i].setChecked(false);
		}
		expend[5].setTag("plus");
		expend[5].setImageDrawable(getResources().getDrawable(R.drawable.plus));
		lay_sub[5].setVisibility(View.GONE);
		ins_chk[5].setChecked(false);
		
	}
	if(insp.equals(cf.All_insp_list[6]) || insp.equals("ALL"))
	{
		for(int i=0;i<COM3_arr.length;i++)
		{
			COM3_arr[i].setChecked(false);
		}
		expend[6].setTag("plus");
		expend[6].setImageDrawable(getResources().getDrawable(R.drawable.plus));
		lay_sub[6].setVisibility(View.GONE);
		ins_chk[6].setChecked(false);
	
	}*/
	if(insp.equals(cf.All_insp_list[7]) || insp.equals("ALL"))
	{
		for(int i=0;i<GCH_arr.length;i++)
		{
			GCH_arr[i].setChecked(false);
		}
		expend[7].setTag("plus");
		expend[7].setImageDrawable(getResources().getDrawable(R.drawable.plus));
		lay_sub[7].setVisibility(View.GONE);
		ins_chk[7].setChecked(false);
		
	}
	if(insp.equals(cf.All_insp_list[8]) || insp.equals("ALL"))
	{
		for(int i=0;i<SINK_arr.length;i++)
		{
			SINK_arr[i].setChecked(false);
		}
		expend[8].setTag("plus");
		expend[8].setImageDrawable(getResources().getDrawable(R.drawable.plus));
		lay_sub[8].setVisibility(View.GONE);
		ins_chk[8].setChecked(false);
		
	}
	if(insp.equals(cf.All_insp_list[9]) || insp.equals("ALL"))
	{
		for(int i=0;i<DRYWALL_arr.length;i++)
		{
			DRYWALL_arr[i].setChecked(false);
		}
		expend[9].setTag("plus");
		expend[9].setImageDrawable(getResources().getDrawable(R.drawable.plus));
		lay_sub[9].setVisibility(View.GONE);
		ins_chk[9].setChecked(false);
		((CheckBox)findViewById(R.id.ex_chk_selectall)).setChecked(false);
		
	}
	if(insp.equals(cf.All_insp_list[10]) || insp.equals("ALL"))
	{
		System.out.println("test=");
		for(int i=0;i<Custom_arr.length;i++)
		{
			Custom_arr[i].setChecked(false);
		}
		expend[10].setTag("plus");
		expend[10].setImageDrawable(getResources().getDrawable(R.drawable.plus));
		lay_sub[10].setVisibility(View.GONE);
		ins_chk[10].setChecked(false);
		((CheckBox)findViewById(R.id.ex_chk_selectall)).setChecked(false);
		
	}
}

public void four_sub_click(View v)
{
	if(!((CheckBox) v).isChecked())
	{
		//ins_chk[0].setChecked(false);
		((CheckBox)findViewById(R.id.ex_chk_selectall)).setChecked(false);
		for(int i =0;i<four_arr.length;i++)
		{
			if(four_arr[i].isChecked())
			{
				//send_export_insp();
				return;
			}
		}
		ins_chk[1].setChecked(false);
	}
	else
	{
		ins_chk[1].setChecked(true);
	}
	
}
public void survey_sub_click(View v)
{
	if(!((CheckBox) v).isChecked())
	{
		//ins_chk[1].setChecked(false);
		((CheckBox)findViewById(R.id.ex_chk_selectall)).setChecked(false);
		for(int i =0;i<survey_arr.length;i++)
		{
			if(survey_arr[i].isChecked())
			{

				return;
			}
		}
		ins_chk[2].setChecked(false);
		
	}else
	{
		ins_chk[2].setChecked(true);
	}
	
}
public void B11802_sub_click(View v)
{
	if(!((CheckBox) v).isChecked())
	{
		//ins_chk[2].setChecked(false);
		((CheckBox)findViewById(R.id.ex_chk_selectall)).setChecked(false);
		for(int i =0;i<B11802_arr.length;i++)
		{
			if(B11802_arr[i].isChecked())
			{

				return;
			}
		}
		ins_chk[3].setChecked(false);
		
		
	}else
	{
		ins_chk[3].setChecked(true);
	}
	
}
public void COM1_sub_click(View v)
{
	if(!((CheckBox) v).isChecked())
	{
		//ins_chk[3].setChecked(false);
		((CheckBox)findViewById(R.id.ex_chk_selectall)).setChecked(false);
		for(int i =0;i<COM1_arr.length;i++)
		{
			if(COM1_arr[i].isChecked())
			{

				return;
			}
		}
		ins_chk[4].setChecked(false);
		
		
	}else
	{
		ins_chk[4].setChecked(true);
	}
	
}
public void COM2_sub_click(View v)
{
	if(!((CheckBox) v).isChecked())
	{
		//ins_chk[4].setChecked(false);
		((CheckBox)findViewById(R.id.ex_chk_selectall)).setChecked(false);
		for(int i =0;i<COM2_arr.length;i++)
		{
			if(COM2_arr[i].isChecked())
			{

				return;
			}
		}
		ins_chk[5].setChecked(false);
		
		
	}else
	{
		ins_chk[5].setChecked(true);
	}
	
}
public void COM3_sub_click(View v)
{
	if(!((CheckBox) v).isChecked())
	{
		//ins_chk[5].setChecked(false);
		((CheckBox)findViewById(R.id.ex_chk_selectall)).setChecked(false);
		for(int i =0;i<COM3_arr.length;i++)
		{
			if(COM3_arr[i].isChecked())
			{

				return;
			}
		}
		ins_chk[6].setChecked(false);
		
		
	}
	else
	{
		ins_chk[6].setChecked(true);
	}
	
}
public void GCH_sub_click(View v)
{
	if(!((CheckBox) v).isChecked())
	{
		//ins_chk[6].setChecked(false);
		((CheckBox)findViewById(R.id.ex_chk_selectall)).setChecked(false);
		for(int i =0;i<GCH_arr.length;i++)
		{
			if(GCH_arr[i].isChecked())
			{

				return;
			}
		}
		ins_chk[7].setChecked(false);
		
		
	}
	else
	{
		ins_chk[7].setChecked(true);
	}
	
}
public void SINK_sub_click(View v)
{
	if(!((CheckBox) v).isChecked())
	{
		//ins_chk[7].setChecked(false);
		((CheckBox)findViewById(R.id.ex_chk_selectall)).setChecked(false);
		for(int i =0;i<SINK_arr.length;i++)
		{
			if(SINK_arr[i].isChecked())
			{

				return;
			}
		}
		ins_chk[8].setChecked(false);
		
		
	}
	else
	{
		ins_chk[8].setChecked(true);
	}
	
}
public void DRYWALL_sub_click(View v)
{
	if(!((CheckBox) v).isChecked())
	{
		//ins_chk[8].setChecked(false);
		((CheckBox)findViewById(R.id.ex_chk_selectall)).setChecked(false);
		for(int i =0;i<DRYWALL_arr.length;i++)
		{
			if(DRYWALL_arr[i].isChecked())
			{

				return;
			}
		}
		ins_chk[9].setChecked(false);
		
		
	}
	else
	{
		ins_chk[9].setChecked(true);
	}
	
}
public void Csutom_sub_click(View v)
{
	if(!((CheckBox) v).isChecked())
	{
		//ins_chk[8].setChecked(false);
		((CheckBox)findViewById(R.id.ex_chk_selectall)).setChecked(false);
		for(int i =0;i<Custom_arr.length;i++)
		{
			if(Custom_arr[i].isChecked())
			{

				return;
			}
		}
		ins_chk[10].setChecked(false);
		
		
	}
	else
	{
		ins_chk[10].setChecked(true);
	}
	
}
public void general_sub_click(View v)
{
	if(!((CheckBox) v).isChecked())
	{
		//ins_chk[8].setChecked(false);
		((CheckBox)findViewById(R.id.ex_chk_selectall)).setChecked(false);
		for(int i =0;i<general_arr.length;i++)
		{
			if(general_arr[i].isChecked())
			{

				return;
			}
		}
		ins_chk[0].setChecked(false);
		
		
	}
	else
	{
		ins_chk[0].setChecked(true);
	}
	
}
public boolean onKeyDown(int keyCode, KeyEvent event) {
	if (keyCode == KeyEvent.KEYCODE_BACK) {
		//cf.goback(0);
		return true;
	}
	return super.onKeyDown(keyCode, event);
}
}
