package idsoft.inspectiondepot.ARR;


import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;


import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Html;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class CommonFunctions implements android.content.DialogInterface.OnClickListener {
	 
	/*public String URL_IDMA="http://72.15.221.151:89/AndroidWebService.asmx";//Uat IDMA
	public String URL_ARR="http://72.15.221.151:81/ARRAndroidWebService.asmx";// UAT ARR
	 */
	 
	public String URL_IDMA="http://72.15.221.153:91/AndroidWebService.asmx";//LIVE IDMA
	public String URL_ARR="http://arrws.paperlessinspector.com/ARRAndroidWebService.asmx";  // Live ARR
	
	
	 public String NAMESPACE = "http://tempuri.org/";
	 public String MY_DATABASE_NAME = "ARRDatabases.db";
	 public static final String ARRVersion="ID_ARRVersion";
	 public  static final String cloud_tbl = "tbl_Cloudinfo";
	 public static final String quesorigmiti_tbl = "tbl_ques_origmiti";
	 public static final String master_tbl="tbl_master_info";
	 public static final String spinnertbl = "tbl_spinner";
	 public static final String GCH_PoolPrsenttbl = "tbl_gch_poolpresent";
	 public static final String GCH_PoolFencetbl = "tbl_gch_poolfence";
	 public static final String GCH_SummaryHazardstbl = "tbl_gch_summaryhazards";
	 public static final String GCH_FireProtecttbl = "tbl_gch_fireprotection";
	 public static final String GCH_SpecHazardstbl = "tbl_gch_specialhazards";
	 public static final String GCH_Hvactbl = "tbl_gch_hvac";
	 public static final String GCH_Electrictbl = "tbl_gch_electric";
	 public static final String GCH_HoodDuct="tbl_gch_hoodduct";	 
	 public static final String GCH_FireProtection="tbl_gch_fireprot";
	 public static final String GCH_Misc="tbl_gch_misc";
	 public String Roofcoverimages="Roofcoverimages";
	 public static String count_tbl = "Count_tbl";
	 public static final String GCH_commenttbl = "tbl_gch_comment";
	 public static final String Addendum = "tbl_addendumcomments";
	 public static final String SINK_Summarytbl = "tbl_sink_summary";
	 public static final String SINK_Obs1tbl = "tbl_sink_obs1";
	 public static final String SINK_Obs2tbl = "tbl_sink_obs2";
	 public static final String SINK_Obs3tbl = "tbl_sink_obs3";
	 public static final String SINK_Obs4tbl = "tbl_sink_obs4";
	 public static final String SINK_Obstreetbl = "tbl_sink_obstree";
	 public static final String DRY_sumcond = "tbl_dry_summcondt";
	 public static final String DRY_home = "tbl_dry_home";
	 
	 public static final String DRY_attic = "tbl_dry_attic";
	 public static final String DRY_hvac = "tbl_dry_hvac";
	 public static final String DRY_appliance = "tbl_dry_appliance";
	 public static final String DRY_room = "tbl_dry_room";
	 public static final String Four_HVAC_Unit = "tbl_four_hvac_unit";
	 public static final String Four_HVAC = "tbl_four_havc";
	 public static final String Four_Electrical_General = "tbl_four_generalelectrical";
	 public static final String Four_Electrical_Panel = "tbl_four_visibleelectrical";
	 public static final String Four_Electrical_Receptacles = "tbl_four_receptacles";
	 public static final String Four_Electrical_Details = "tbl_four_details";
	 public static final String Four_Electrical_GO = "tbl_four_go";
	 public static final String Four_Electrical_Wiring="Four_Electrical_Wiring";
	 public static final String Four_Electrical_MainPanel = "tbl_Four_Elctrical_MainPanel";
	 public static final String Four_Electrical_SubPanel = "tbl_Four_Elctrical_SubPanel";
	 public static final String Four_Electrical_SubPanelPresent = "tbl_Four_Elctrical_SubPanelPresent";
	 public static final String Four_Electrical_Attic = "tbl_Four_Electrical_Attic";
	 public static final String Four_Electrical_Plumbing = "tbl_Four_Electrical_Plumbing";
	 public static final String Plumbing_waterheater = "tbl_Four_Electrical_Plumbing_WH";
	 public static final String Roof_cover_type="tbl_RoofCoverType";
 	 public static final String Rct_table="tbl_RoofCoverType_new";
	 public static final String Roof_additional="tbl_Roofadditioanl_new";
	 public static final String RCN_table="tbl_Roofconditonnoted_new";
	 public static final String RCN_table_dy="tbl_Roofconditonnoted_dynamic";
	 public String Elevatorimages="Elevatorimages";
	 public static final String Custom_report = "tbl_customreport";

	 public static final String General_Roof_information="tbl_General_Roof_Information";
	 public static final String option_list="tbl_roofoption_list";
	 public static final String ImageTable = "tbl_Photos";
	public static final String Photo_caption = "tbl_Photos_caption";
	public static final String FeedBackInfoTable = "tbl_FeedbackInfo";
	public static final String FeedBackDocumentTable = "tbl_Feedbackdocument";
	public static final String MailingPolicyHolder = "MailingPolicyHolder";
	public static final String Agent_tabble = "Agent_information";
	public static final String Relator_table = "Relator_information";	
	public static final String Additional_table = "Additional_information";
	 public static final String Wind_Questiontbl = "tbl_wind_buildcode";
	 public static final String Wind_QuesCommtbl = "tbl_wind_quescomm";	 
	 public static final String quesroofcover = "tbl_quesroofcover";
	 public static final String WindMit_WallCons="tbl_wallconstruction";
	 public static final String Comm_AuxBuilding="tbl_comm_auxbuilding";
	 public static final String Wind_Commercial_Comm="tbl_commercial_comments";
	 public static final String Comm_RestSuppAppl="tbl_commercial_restsupp_appl";
	 public static final String Comm_RestSupplement="tbl_commercial_restsupp";
	 public static final String Communal_Areas="tbl_communal_areas";
	 public static final String Communal_AreasElevation="tbl_communal_areaselevation";
	 public static final String Commercial_Building="tbl_Comm_Buidingno";
	 public static final String Comm_Building="tbl_Comm_Building";
	 public static final String WeatherCondition="tbl_WeatherCondition";
     public static final String inspectorlogin = "tbl_inspectorlogin";
	 public static final String Select_insp = "Select_inspection";	 
	 public static final String allinspnamelist="tbl_allinspnamelist";
	 public static final String WindDoorSky_NA="tbl_na_winddoorsky";	 
	 public static final String IDMAVersion="ID_Version";
	 public String arrbuilsrid1[], arrbuilsrid2[],arrbuilsrid3[],arrspnsrid1[], arrspnsrid2[],arrspnsrid3[];
	 public TextView duptv1,duptv2,duptv3;
	 Spinner spn1,spn2,spn3;
	 boolean chkcommercial[] = {false,false,false};
	 boolean b1=false,b2=false,b3=false,b4=false;
	 public String Roof_master="tbl_GeneralRoofMaster",selinspname="",selinsptext="",CommercialFlag="",PH_FirstName="",PH_EmailChkbx="",PH_Email="",PH_Address1="",MainInspID="",bno1flag="",bno2flag="",bno3flag="",IS_Elec_On="",
			 IS_Water_On="",IS_Access_Conf="",IS_Pres_Occu="",IS_Sewer_Ser="",IS_Buil_type="";
	 public String Roof_covertype_view="Roof_covertype_view";
	 Map<String, String[]> map = new LinkedHashMap<String, String[]>();
	 public String Elev_master="tbl_GeneralElevMaster";
	 public String	 inspectionname="";
	 public String General_roof_view="General_roof_view";
	 public String Comments_lib="tbl_commentsLibrary";
	 public static final String Commercial_Wind="tbl_commercialwind";
	 public static final String BI_Skylights="tbl_buildskylights";
	 public static final String BI_Dooropenings="tbl_builddooropen";
	 public static final String BI_Windopenings="tbl_buildwindopen";
	 public static final String BI_General="tbl_buildgeneral";
	 public static final String policyholder="Policy_holder";
	 public static final String dyninspname="tbl_dynm_inspname";
	 public static final String inspnamelist="tbl_inspnamelist";
	 public static final String Comm_Aux_Master="tbl_comm_aux";
	 public static final String OnlineTable = "OnlineDataTable";
	 private static final int TEXT_ID = 0;
	 private static final int DLG_EXAMPLE1 = 0;
	public static final int loadcomment_code = 34;
	public static String loadinsp_n = "";
	public static String loadinsp_t = "";
	public static String loadinsp_q = "",s="";
	ProgressDialog pd;
	public String[] temp;
	public static String loadinsp_o = "All";
	public  EditText e1,e2,e3;
	public int ressupchk1=0;
	public Cursor Roof_Add,Roof_RCT,Roof_RCN,curbuilding,windopen,dooropen,skylightsopen,curna,qc,curcomm,curcomm2,curaux,curapppl,curaddendum,curcomments,wallcons,C_GEN,C_ADDEN,C_auxNA,C_RestNA
;
	public String str="",AddendumComm="",RCT_tit="Roof Covering Type",trval="",dcom="",Aux_NA="",Rest_NA="",
			 bccomments="",rccomments="",rdcomments="",rwcomments="",rgcomments="",swrcomments="",opcomments="",wccomments="",WindOpen_NA="",
			 DoorOpen_NA="",Skylights_NA="",terrainval="",rcvalue="",rdvalue="",swrvalue="",opvalue="",certificationval="",auxcomments="",
			 restsuppcomments="",communalcomments="",Gendata_val="",Loc_NA="",GEN_NA="",Loc_val="",OBS_NA="",Observ1="",Observ2="",Observ3="",Observ4="",Observ5="",
			 ISO_NA="",Iso_val="",Foun_NA="",Foundation_val="",WS_NA="",Wallstru_val="",WC_NA="",Wallclad_val="",BISEP_NA="",Buildsep_val="",comments="",
					 YOC="",inspectioname="",Permit_Confirmed="",Balcony_present="",Inci_Occu="",Addi_Stru="",BOCCU_SURV="",SCOPE="",WS_Other="",WC_Other="";
    public LinearLayout innersubmenu_layout;
	public boolean kstr = false;
	 public boolean application_sta=false;
	 String Roof_insp[],All_insp_list[];
	 String yearbuilt[] = { "--Select--","Unknown","Other","2015","2014","2013","2012","2011","2010","2009","2008","2007","2006","2005","2004","2003","2002","2001","2000","1999","1998","1997","1996",
				"1995","1994","1993","1992","1991","1990","1989","1988","1987","1986","1985","1984","1983","1982","1981","1980","1979","1978","1977","1976",
				"1975","1974","1973","1972","1971","1970","1969","1968","1967","1966","1965","1964","1963","1962","1961","1960","1959","1958","1957","1956",
				"1955","1954","1953","1952","1951","1950","1949","1948","1947","1946","1945","1944","1943","1942","1941","1940","1939","1938","1937","1936",
				"1935","1934","1933","1932","1931","1930","1929","1928","1927","1926","1925","1924","1923","1922","1921","1920","1919","1918","1917","1916",
				"1915","1914","1913","1912","1911","1910","1909","1908","1907","1906","1905","1904","1903","1902","1901","1900"};
	 SQLiteDatabase arr_db;
	 Button dupbtn1,dupbtn2,dupbtn3;
	 public int show_handler,Takeimage_rquest_code=54,info_requestcode=44,buildingno1=0,buildingno2=0,buildingno3=0;
	 String comm1_buildno="",comm2_buildno="",comm3_buildno="",ScheduledDate="",Roof="",
              newphone="",colorname,strerrorlog,apkrc="RC U 27-04-2016",selectedhomeid="",
			 onlinspectionid="11",onlstatus="40",strret="All Risk Report",
			 strcar="All Risk Report Carr.Order",Insp_id="2445", Insp_firstname="", Insp_middlename="", 
					 chk1="",chk2="",chk3="",srid="",appendbno1="",appendbno2="",appendbno3="",
							 bno1="",bno2="",bno3="",B1802AddeComm="",CommIAdd="",CommIIAdd="",CommIIIAdd="",FourpointAdd="",
									 comm1txt="",comm2txt="",comm3txt="",eno="",id="",model="",manufacturer="",image="",dateoflastinsp="",servingallfloors="",
			 Insp_lastname="", Insp_address="", Insp_companyname="", Insp_companyId="", Insp_username, Insp_password,
			 Insp_flag1,buildingno="",status,Insp_Photo,Insp_PhotoExtn,Insp_Status,Insp_email,County="",inspname="",WS_Stories="",dupinsid="";
      
	 ProgressDialog pd2;
  	 public Context con;
     public int noofbuildings,wd,ht,identityval=0,mYear,mMonth,mDay,typeidentity=1,windmitinstypeval,k1=0,mDayofWeek,selmDayofWeek,hours,minutes,amorpm,seconds;
     public Toast toast;
     public CheckBox chkinc4;
	 public	LinearLayout submenu_layout,mainlayout,mainmenu_layout,type_submenulayout;
	 public TableRow tblrw;
	 public Button btnsave,save;
	 public String[] separated;
	 public String year[]= new String[]{"--Select--","Other","Unknown","2015","2014","2013","2012","2011","2010","2009","2008","2007","2006","2005","2004","2003","2002","2001","2000","1999","1998","1997","1996","1995","1994","1993","1992","1991","1990","1989","1988","1987","1986","1985","1984","1983","1982","1981","1980","1979","1978","1977","1976","1975","1974","1973","1972","1971","1970","1969","1968","1967","1966","1965","1964","1963","1962","1961","1960","1959","1958","1957","1956","1955","1954","1953","1952","1951","1950","1949","1948","1947","1946","1945","1944","1943","1942","1941","1940","1939","1938","1937","1936","1935","1934","1933","1932","1931","1930","1929","1928","1927","1926","1925","1924","1923","1922","1921","1920","1919","1918","1917","1916","1915","1914","1913","1912","1911","1910","1909","1908","1907","1906","1905","1904","1903","1902","1901","1900"};
	 public String Y_N[]=new String[]{"--Select","Yes","No"};
	 public String Y_N_A[]=new String[]{"--Select","Yes","No","N/A"};
	 public String string="",insp_online="",singleinspname="",redcolor ="<font color=red> * </font>",alertcontent,alerttitle,selinsptype,unchkstr="",Stories="",YearPH="",BuildingSize="",s1="",s2="",s3="";
	 public Class moveclass;
	 public Class mainclass[] ={PolicyHolder.class,Roof_section_common.class,Roof_section_common.class,WindMit.class,GCH.class,SinkSummary.class,
			 DrywallSummary.class,photos.class,Feedback.class,Maps.class,Submit.class,Agent.class, CallAttempt.class,General.class,Additional.class,BIGeneralData.class,Weather.class};
	 public Class genclassnames[]={General.class,PolicyHolder.class, CallAttempt.class,Agent.class,Additional.class,BIGeneralData.class,Weather.class,Addendum.class};
	 public Class gchclassnames[]={GCH.class,PoolFence.class,PoolFence.class,Roof_section_common.class,SummaryHazards.class,FireProtection.class,SpecialHazards.class,GCHHVAC.class,GCHElectric.class,GCHComments.class,GCHHoodduct.class,GCHFireprot.class,GCHMisc.class};
	 public Class sinkclassnames[]={SinkSummary.class,Observation1.class,Observation2.class,Observation3.class,Observation4.class};
	 public Class drywallclassnames[]={DrywallSummary.class,DrywallRoom.class,DrywallHVAC.class,DrywallAppliance.class,DrywallAttic.class,DrywallComments.class};
	 public Class fourclassnames[]={Roof_section_common.class,Attic.class,Plumbing.class,Electrical.class,HVAC.class};
	 public Class windclassnames[]={WindMit.class,WindMitBuildCode.class,WindMitRoofCover.class,WindMitRoofDeck.class,WindMitRoofWall.class,WindMitRoofGeometry.class,WindMitSWR.class,WindMitOpenProt.class,WindMitWallCons.class,WindMitCommareas.class,WeblinkOpen.class,WindMitCommAuxbuildings.class,WindMitCommRestsupp.class};	 
	 public Class buildingclassnames[] = {BIGeneralData.class,BILocation.class,BIObservations.class,BIIso.class,BIFoundation.class,BIWallstructure.class,BIWallcladding.class,BISeperation.class,WindMitWindowopenings.class,WindMitDooropenings.class,WindMitSkylights.class,BIComments.class};
	 public Intent myintent;
	 public String spnstories[] = {"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65","66","67","68","69","70","71","72","73","74","75","76","77","78","79","80","81","82","83","84","85","86","87","88","89","90","91","92","93","94","95","96","97","98","99","100"};
	 public String spnpercentage[] = { "--Select--", "10","20","30","40","50","60","70","80","90","100",};
	 public String spnwindprottype[]={"--Select--","N/A","A","B","C","D","N","N-SUB","X"};
	 public String spnofqunatity[] = {"--Select--","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65","66","67","68","69","70","71","72","73","74","75","76","77","78","79","80","81","82","83","84","85","86","87","88","89","90","91","92","93","94","95","96","97","98","99","100"};
	 String spndoorelevation[] = { "--Select--", "Front Elevation","Back Elevation","Right Elevation","Left Elevation","Attic Elevation","Additional Elevation","Other Elevation"};	
	 String spnelevation[] = { "--Select--", "Front Elevation","Back Elevation","Right Elevation","Left Elevation","Other Elevation"};
	 /*String floorarr [] = {"First","Second","Third","Fourth","Fifth","Sixth","Seventh","Eighth","Nineth","Tenth","Eleventh","Twelfth","Thirteenth","Fourteenth","Fifteen",
				"Sixteen","Seventeen","Eighteen","Nineteen","Twentieth","Twenty One","Twenty Two","Twenty Third","Twenty Fourth","Twenty fifth","Twenty Sixth","Twenty Seventh","Twenty Eighth","Twenty Nineth","Thirtieth",
				"Thirty First","Thirty Second","Thirty Third","Thirty Fourth","Thirty Fifth","Thirty Sixth","Thirty Seventh","Thirty Eighth","Thirty Nineth","Fortieth",
				"Forty First","Forty Second","Forty Third","Forty Fourth","Forty Fifth","Forty Sixth","Forty Seventh","Forty Eighth","Forty Nineth","Fiftieth",
				"Fifty First","Fifty Second","Fifty Third","Fifty Fourth","Fifty Fifth","Fifty Sixth","Fifty Seventh","Fifty Eighth","Fifty Nineth","Sixtieth",
				"Sixty First","Sixty Second","Sixty Third","Sixty Fourth","Sixty Fifth","Sixty Sixth","Sixty Seventh","Sixty Eighth","Sixty Nineth","Seventieth",
				"Seventy First","Seventy Second","Seventy Third","Seventy Fourth","Seventy Fifth","Seventy Sixth","Seventy Seventh","Seventy Eighth","Seventy Nineth","Eightieth",
				"Eighty First","Eighty Second","Eighty Third","Eighty Fourth","Eighty Fifth","Eighty Sixth","Eighty Seventh","Eighty Eighth","Eighty Nineth","Ninetieth",
				"Ninety First","Ninety Second","Ninety Third","Ninety Fourth","Ninety Fifth","Ninety Sixth","Ninety Seventh","Ninety Eighth","Ninety Nineth","Hundredth"};
*/
	 String floorarr [] = {"First","Second","Third","Fourth","Fifth","Sixth","Seventh","Eight","Nineth","Tenth","Eleventh","Twelveth","Thirteenth","Fourteenth","Fifteenth",
				"Sixteenth","Seventeenth","Eighteenth","Nineteenth","Twenty","Twenty First","Twenty Second","Twenty Third","Twenty Fourth","Twenty Fifth","Twenty Sixth","Twenty Seventh","Twenty Eight","Twenty Nineth","Thirty",
				"Thirty First","Thirty Second","Thirty Third","Thirty Fourth","Thirty Fifth","Thirty Sixth","Thirty Seventh","Thirty Eight","Thirty Nineth","Forty",
				"Forty First","Forty Second","Forty Third","Forty Fourth","Forty Fifth","Forty Sixth","Forty Seventh","Forty Eight","Forty Nineth","Fifty",
				"Fifty First","Fifty Second","Fifty Third","Fifty Fourth","Fifty Fifth","Fifty Sixth","Fifty Seventh","Fifty Eight","Fifty Nineth","Sixty",
				"Sixty First","Sixty Second","Sixty Third","Sixty Fourth","Sixty Fifth","Sixty Sixth","Sixty Seventh","Sixty Eighth","Sixty Nineth","Seventy",
				"Seventy First","Seventy Second","Seventy Third","Seventy Fourth","Seventy Fifth","Seventy Sixth","Seventy Seventh","Seventy Eighth","Seventy Nineth","Eighty",
				"Eighty First","Eighty Second","Eighty Third","Eighty Fourth","Eighty Fifth","Eighty Sixth","Eighty Seventh","Eighty Eighth","Eighty Nineth","Ninety",
				"Ninety First","Ninety Second","Ninety Third","Ninety Fourth","Ninety Fifth","Ninety Sixth","Ninety Seventh","Ninety Eighth","Ninety Nineth","Hundred"};

	 public View v1;
	 public CharSequence datewithtime;
	 public android.text.format.DateFormat df;
	 public List<String> items= new ArrayList<String>();
	public boolean isaccess=false;
	public SoapObject request;
	public SoapSerializationEnvelope envelope;
	String Insp_date="";
	public static final String BI_GeneralDup="tbl_buildgeneraldup";


	public Cursor curbuilgen;
	public String ScopeofInspection="",InsuredIS="",Nofunits="",Neighbourhoodis="",PermitConfirmed="",PermconfComm="", OccupancyType="",BuiloccuPerc="",Builvacaperc="",BuildingOccu="",LightPoles="",FencesPrepLine="",AdditionalStructures="";
	public CommonFunctions(Context con) {
		// TODO Auto-generated constructor stub
		arr_db = con.openOrCreateDatabase(MY_DATABASE_NAME, 1, null);
		this.con = con;
		df = new android.text.format.DateFormat();
		datewithtime = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		Roof_insp=new String[6];
		try
		{
			CreateARRTable(1);	
		CreateARRTable(14);	
		
		Cursor c = arr_db.rawQuery("select  * from " + inspnamelist + " order by ARR_Custom_ID", null);
		System.out.println("count="+c.getCount());
			if(c.getCount()>0)
		    {
				All_insp_list=new String[c.getCount()+4];
				 c.moveToFirst();
		   		All_insp_list[0]="--Select--";
		   		int m=0;
		   		 for(int i=1;i<c.getCount()+4;i++, c.moveToNext())
		   		 {
		   			 
		   			if(i==10)
		   			{
		   				//System.out.println("what isss iii"+i);
		   				All_insp_list[i]="Custom Inspection";
		   			}
		   			 
		   			 
		   			// System.out.println("iiii="+i);
		   			All_insp_list[i]= decode(c.getString(c.getColumnIndex("ARR_Insp_Name"))).trim();
		   		//System.out.println("came her");
		   			if(i==5 || i==6)
		   			{
		   				c.moveToPrevious();
		   				if(i==5)
		   				{
		   					All_insp_list[i]="Commercial Type II Inspection";
		   				}
		   				else if(i==6)
		   				{
		   					All_insp_list[i]="Commercial Type III Inspection";
		   				}
		   				//All_insp_list[i]="comer2"; /**This is added for dummy we have already developed all over the page with the 9 data now it sort to  7 so we added**/
		   				
		   			}
		   			
		   			if(i!=3 && i<8)
		   			{
		   				Roof_insp[m]=All_insp_list[i];
		   				m++;
		   			}
		   			
		   		 }
		   		 
		    }
			else 
			{
				All_insp_list=con.getResources().getStringArray(R.array.Inspection_types);// get the default text
			}
		}   
		catch (Exception e) {
			// TODO: handle exception
			System.out.println("error comes "+e.getMessage());
		}

		//All_insp_list=//con.getResources().getStringArray(R.array.Inspection_types);

		/***checking for the All risk report application **/
		try
		{
		final PackageManager pm = con.getPackageManager();
        //get a list of installed apps.
		        List<ApplicationInfo> packages = pm
		                .getInstalledApplications(PackageManager.GET_META_DATA);
		
		        for (ApplicationInfo packageInfo : packages) {

			          
					  if(packageInfo.packageName.toString().trim().equals("idsoft.inspectiondepot.IDMA")) // for checking the main app has installed  or not 
			            {
						  application_sta=true; // check if the main appliccation IDinspection has installed  or not 
			            }
		        }
		        
		        
		}catch(Exception e)
		{
			System.out.println("Problem in the application sta"+e.getMessage());
		}
        /***checking for the All risk report application Ends here  **/
        try
        {
        	getInspectorId();
        }catch (Exception e) {
			// TODO: handle exception
		}
	}
	public void showhelp(String alerttitle,String alertcontent) {
		// TODO Auto-generated method stub
		final Dialog dialog = new Dialog(this.con,android.R.style.Theme_Translucent_NoTitleBar);
		dialog.getWindow().setContentView(R.layout.alert);
		TextView txt = (TextView) dialog.findViewById(R.id.txtid);
		txt.setText(Html.fromHtml(alertcontent));
		ImageView btn_helpclose = (ImageView) dialog.findViewById(R.id.helpclose);
		Button btn_ok = (Button) dialog.findViewById(R.id.ok);
		btn_ok.setVisibility(v1.GONE);

		btn_helpclose.setOnClickListener(new OnClickListener()
		{
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
			
		});
		dialog.setCancelable(false);
		dialog.show();
	}

	public boolean checkresponce(Object responce) {
		if (responce != null) {
			if (!"null".equals(responce.toString())
					&& !responce.toString().equals("")) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}


	public final boolean isInternetOn() {
		boolean chk = false;
		ConnectivityManager conMgr = (ConnectivityManager) this.con.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = conMgr.getActiveNetworkInfo();
		if (info != null && info.isConnected()) {
			chk = true;
		} else {
			chk = false;
		}

		return chk;
	}	

	public void getDeviceDimensions() {
		// TODO Auto-generated method stub
		    DisplayMetrics metrics = new DisplayMetrics();
			((Activity) this.con).getWindowManager().getDefaultDisplay().getMetrics(metrics);

			wd = metrics.widthPixels;
			ht = metrics.heightPixels;
	}
	public void CreateARRTable(int select) {

		switch (select) {
		
		case 432:
			try {
				
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "+ Roof_master+" ( RM_masterid INTEGER PRIMARY KEY AUTOINCREMENT, RM_inspectorid VARCHAR( 8 ) DEFAULT ( '' ),RM_srid VARCHAR( 8 ) DEFAULT ( '' ),RM_page VARCHAR( 2 ) DEFAULT ( '1' ),RM_insp_id VARCHAR( 2 ) DEFAULT ( '' ),RM_Covering VARCHAR( 150 ) DEFAULT ( '' ),RM_Enable   BOOLEAN  DEFAULT ( 1 ),RM_Created_date Varchar(50) Default(CURRENT_TIMESTAMP),RM_module_id VARCHAR( 2 ) DEFAULT ( '0' ));");
				CreateARRTable(435);
			} catch (Exception e) {
				System.out.println("issues occure create Roof master "+e.getMessage());
				
			}
		break;
		case 433:
		/*	arr_db.execSQL("CREATE VIEW IF NOT EXISTS	"+ Roof_covertype_view+
			" AS SELECT id,RM_srid as RCT_srid,RM_page as RCT_page,RM_insp_id as RCT_insp_id,RM_Covering as RCT_FielsName,RCT_TypeValue,RCT_FieldVal,RCT_Other,RM_Enable as RCT_Enable" +
			" FROM "+Roof_cover_type+" Left Join "+Roof_master+" where RCT_masterid=RM_masterid and RM_module_id='0'");
		*/
		break;
		case 434:
			/*arr_db.execSQL("CREATE VIEW IF NOT EXISTS	"+General_roof_view+" AS "+
		"SELECT r.RC_Id,m.RM_srid as RC_SRID,m.RM_page as RC_page,RM_insp_id as RC_insp_id,RC_Condition_type,RC_Condition_type_DESC,RC_Condition_type_val,m.RM_Created_date as Created_date,RC_Condition_type_order,RC_Enable"+
		" FROM "+General_Roof_information+" r Left Join "+Roof_master+" m where RC_masterid=RM_masterid and RM_module_id='1'");*/
		break;
		case 450:
			try {
			
			arr_db.execSQL("CREATE TABLE IF NOT EXISTS "+ Elev_master+" ( ELev_masterid INTEGER PRIMARY KEY AUTOINCREMENT, Elev_srid VARCHAR( 8 ) DEFAULT ( '' ),Elev_insp_id VARCHAR( 2 ) DEFAULT ( '' ),Elev_No VARCHAR( 150 ) DEFAULT ( '' ),Elev_Enable  BOOLEAN  DEFAULT ( 1 ));");
			
		} catch (Exception e) {
			System.out.println("issues occure create Roof master "+e.getMessage());
			
		}

		
			break;

		case 435:
			try {
				
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "+ Roofcoverimages+" ( id INTEGER PRIMARY KEY AUTOINCREMENT,RIM_masterid VARCHAR( 8 ) DEFAULT ( '' ),RIM_module_id VARCHAR( 2 ) DEFAULT ( '0' ),RIM_Path VARCHAR(50 ) DEFAULT ( '' ),RIM_Order VARCHAR( 2 ) DEFAULT ( '0' ),RIM_Caption VARCHAR(100 ) DEFAULT ( '' ));");
				
			} catch (Exception e) {
				System.out.println("issues occure create Roof master "+e.getMessage());
				
			}
		break;

		case 0:
			/** CREATING ARR VERSION TABLE **/
			try {
				
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ ARRVersion
						+ "(arrVId INTEGER PRIMARY KEY Not null,ARR_VersionCode varchar(50),ARR_VersionName varchar(50),ModifiedDate varchar(50));");

			} catch (Exception e) {
				
				
			}
		break;
		case 436:
			try {
				
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "+ Elevatorimages+" ( id INTEGER PRIMARY KEY AUTOINCREMENT,Elev_masterid VARCHAR( 8 ) DEFAULT ( '' ),Elev_module_id VARCHAR( 2 ) DEFAULT ( '0' ),Elev_Path VARCHAR(50 ) DEFAULT ( '' ),Elev_Order VARCHAR( 2 ) DEFAULT ( '0' ),Elev_Caption VARCHAR(100 ) DEFAULT ( '' ));");
				
			} catch (Exception e) {
				System.out.println("issues occure create Elevatorimages "+e.getMessage());
				
			}
		break;

		case 150:
			/** Builing info data for auto population **/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ BI_GeneralDup
						+ "(aid INTEGER PRIMARY KEY AUTOINCREMENT,SRID varchar(100),ScopeOfInspection varchar(500),fld_Scopeofinspother varchar(250)," +
						"fld_GenNA int default '0',fld_Insuredis varchar(500),fld_Insuredisother varchar(250),fld_Neighbourhoodis varchar(250),fld_NeighbourhoodisSub varchar(250)," +
						"fld_NeighbourhoodisSubOther varchar(250),fld_OccupancyType varchar(250),fld_OccupancyTypeother varchar(250),fld_noofunits varchar(50)," +
						"fld_BuildOccuper varchar(25),fld_BuildVacantper varchar(25),fld_BuildOccu varchar(250),fld_BuildOccuother varchar(250),fld_Permitconfirmed varchar(300),fld_addistru varchar(50)," +
						"fld_elec_turnon varchar(50),fld_water_turnon varchar(50),fld_acesss_confirmed varchar(50),fld_pres_occu varchar(50)," +
						"fld_sewer_serv varchar(50),fld_build_type varchar(50),fld_build_typeother varchar(250),fld_waterservice varchar(50)," +
						"fld_LightPoles varchar(250),fld_FencesPrepLine varchar(250));");

			} catch (Exception e) {
				System.out.println("BI_GeneralDup="+e.getMessage());
			}
			break;
		case 1:
		/*INSPECTOR LOGIN*/
		try {
			arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
					+ inspectorlogin
					+ " (Id INTEGER PRIMARY KEY AUTOINCREMENT,Fld_InspectorId varchar(50),Fld_InspectorFirstName varchar(50),Fld_InspectorMiddleName varchar(50),Fld_InspectorLastName varchar(50),Fld_InspectorAddress varchar(150),Fld_InspectorCompanyName varchar(150),Fld_InspectorCompanyId varchar(100),Fld_InspectorUserName varchar(50),Fld_InspectorPassword varchar(50),Fld_InspectorPhotoExtn Varchar(10),Android_status bit,Fld_InspectorFlag bit,Fld_Remember_pass  bit DEFAULT 0,Fld_InspectorEmail varchar(100),Fld_InspectorPEmail varchar(100),Fld_signature varchar(100) DEFAULT(''),Fld_licenceno varchar(100) DEFAULT(''),Fld_licencetype varchar(100) DEFAULT(''),Fld_Phone varchar(25) DEFAULT(''));");	} catch (Exception e) {
			strerrorlog="Inspector Login table not created";
			Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of Inspector Login Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			
		}
		break;
		case 2:
			/*Select inspection */
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ Select_insp
						+ " (Id INTEGER PRIMARY KEY AUTOINCREMENT,SI_InspectorId varchar(50),SI_srid varchar(50),SI_InspectionNames varchar(200),SI_CreatedDate varchar(50) DEFAULT(''))");
			} catch (Exception e) {
				strerrorlog="Inspectoion slection table Not Created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of Inspector Login Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		case 3:
			/* POLICYHOLDER */
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ policyholder
						+ " (Ph_Id INTEGER PRIMARY KEY AUTOINCREMENT,ARR_PH_InspectorId varchar(50) NOT NULL,ARR_PH_SRID varchar(50) NOT NULL," +
						"ARR_PH_FirstName varchar(100) NOT NULL,ARR_PH_LastName varchar(100) NOT NULL,ARR_PH_Address1 varchar(100)," +
						"ARR_PH_Address2 varchar(100),ARR_PH_City varchar(100),ARR_PH_Zip varchar(100),ARR_PH_State varchar(100)," +
						"ARR_PH_County varchar(100),ARR_PH_Policyno varchar(100),ARR_PH_Inspectionfees varchar(100),ARR_PH_InsuranceCompany varchar(100)," +
						"ARR_PH_HomePhone varchar(100),ARR_PH_WorkPhone varchar(100),ARR_PH_CellPhone varchar(100),ARR_PH_NOOFSTORIES varchar(5)," +
						"ARR_PH_WEBSITE varchar(100),ARR_PH_Email varchar(100),ARR_PH_EmailChkbx Integer,ARR_PH_InspectionTypeId Integer," +
						"ARR_PH_IsInspected Integer,ARR_PH_IsUploaded Integer,ARR_PH_Status Integer,ARR_PH_SubStatus Integer," +
						"ARR_Schedule_ScheduledDate varchar(100) DEFAULT (''),ARR_Schedule_InspectionStartTime varchar(100) DEFAULT ('')," +
						"ARR_Schedule_InspectionEndTime varchar(100) DEFAULT (''),ARR_Schedule_Comments varchar(1010) DEFAULT ('')," +
						"ARR_Schedule_ScheduleCreatedDate varchar(100) DEFAULT (''),ARR_Schedule_AssignedDate varchar(100) DEFAULT ('')," +
						"ARR_ScheduleFlag Integer DEFAULT (''),ARR_YearBuilt varchar(100) DEFAULT (''),ARR_ContactPerson Varchar(50) DEFAULT ('')," +
						"BuidingSize Varchar(20) DEFAULT (''),fld_latitude varchar(50),fld_longitude varchar(50),fld_homeownersign varchar(250)," +
						"fld_homewonercaption varchar(150),fld_paperworksign varchar(250),fld_paperworkcaption varchar(150),fld_noofbuildings varchar(50) DEFAULT ('')," +
						"fld_inspectionids varchar(50),Company_ID varchar(50),Commercial_Flag varchar(50),Workorderno varchar(25),iscustominspection Integer default '0');");
				
				
				
			} catch (Exception e) {
				strerrorlog="PolicyHolder table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of PolicyHolder Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
				
			}
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ count_tbl
						+ " (Id INTEGER PRIMARY KEY AUTOINCREMENT,insp_id varchar(50),C_28_pre varchar(5) Default('0'),C_28_uts varchar(5) Default('0'),C_28_can varchar(5) Default('0'),C_28_RR varchar(5) Default('0')," +
						"C_29_pre varchar(5) Default('0'),C_29_uts varchar(5) Default('0'),C_29_can varchar(5) Default('0'),C_29_RR varchar(5) Default('0'));");

			} catch (Exception e) {
				
			}
			
			break;
		case 90:
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "+ Custom_report 
						+ " (Ques_Id INTEGER PRIMARY KEY AUTOINCREMENT,SRID Varchar(50),ReportImage varchar(250) NOT NULL,ReportTitle varchar(250) NOT NULL,NotRequired Varchar(50),FBNotRequired Varchar(50),IsMainInspection Varchar(50),InspectionID Varchar(50));");
			} catch (Exception e) {
				
				System.out.println("tt"+e.getMessage());
			}
				
		break;


		case 13:
			/* DYNAMIC INSPECTION NAME */
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ dyninspname
						+ " (Ph_Id INTEGER PRIMARY KEY AUTOINCREMENT,ARR_PH_InspectorId varchar(25) NOT NULL,ARR_Insp_Type_ID varchar(20) NOT NULL,ARR_Insp_Name varchar(150) NOT NULL);");
			} catch (Exception e) {
				strerrorlog="DYNAMIC INSPECTION NAME table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of PolicyHolder Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
				
			}
			break;
			
		case 14:
			/* DYNAMIC INSPECTION NAME */
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ inspnamelist
						+ " (Ins_Id INTEGER PRIMARY KEY AUTOINCREMENT,ARR_Insp_ID varchar(20) NOT NULL,ARR_Insp_Name varchar(150) NOT NULL,ARR_Custom_ID varchar(25));");
			} catch (Exception e) {
				strerrorlog="DYNAMIC INSPECTION NAME LIST table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of PolicyHolder Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
				
			}
			break;


		case 41:
			/* GCH - POOLPRESENT TABLE*/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ GCH_PoolPrsenttbl
						+ " (ppId INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50),fld_ppchk Integer,fld_ppground varchar(50),fld_htpresent varchar(50),fld_htcovered varchar(50),fld_ppstructure varchar(50)," +
						"  fld_egppresent varchar(50),fld_pspresent varchar(50),fld_dbpresent varchar(50),fld_ppenclosure varchar(50),fld_pedisrepair varchar(50));");
			} catch (Exception e) {
				strerrorlog="Pool Present - GCH Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of GCH - PoolPresent Login Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		case 42:
			/* GCH - POOLFENCE TABLE*/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ GCH_PoolFencetbl
						+ " (ppfId INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50),fld_ppfchk Integer,fld_ppfence varchar(50),fld_ppfenceother varchar(50),fld_selflatches varchar(50),fld_pinstalled varchar(50),fld_pfdisrepair varchar(50));");
			} catch (Exception e) {
				strerrorlog="Pool Fence - GCH Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of GCH - PoolFence Login Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		case 6: 
			/** creating Addiitonal information table **/
			try
			{
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
			    + Additional_table + " (ARR_AD_id INTEGER PRIMARY KEY Not null,ARR_AD_SRID  Varchar(50) ,ARR_AD_INSPID Varchar(50),ARR_AD_IsRecord boolean,ARR_AD_UserTypeName Varchar(50),ARR_AD_ContactEmail varchar(50),ARR_AD_PhoneNumber varchar(15),ARR_AD_MobileNumber varchar(15),ARR_AD_BestTimetoCallYou varchar(50),ARR_AD_Bestdaycall varchar(50),ARR_AD_FirstChoice varchar(100),ARR_AD_SecondChoice varchar(50),ARR_AD_ThirdChoice varchar(50),SF_AD_FeedbackComments varchar(500));");
				
			} catch (Exception e) {
				strerrorlog="Additional info  table creation not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of Agent info Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}

			
	break;
		case 5: 
			/** creating Agent information table **/
			try
			{
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
			    + Agent_tabble + " (ARR_AI_id INTEGER PRIMARY KEY Not null,ARR_AI_SRID  Varchar(50) ,ARR_AI_AgencyName  Varchar(50) ,ARR_AI_AgentName  Varchar(50) ,ARR_AI_AgentAddress  Varchar(50) ,ARR_AI_AgentAddress2  Varchar(50) ,ARR_AI_AgentCity  Varchar(50) ,ARR_AI_AgentCounty  Varchar(50) ,ARR_AI_AgentRole  Varchar(50) ,ARR_AI_AgentState  Varchar(50) ,ARR_AI_AgentZip  Varchar(50) ,ARR_AI_AgentOffPhone  Varchar(50) ,ARR_AI_AgentContactPhone  Varchar(50) ,ARR_AI_AgentFax  Varchar(50) ,ARR_AI_AgentEmail  Varchar(50) ,ARR_AI_AgentWebSite Varchar(50));");
			} catch (Exception e) {
				strerrorlog="Agent info  table creation table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of Agent info Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}

			
	break;
		case 15: 
			/** creating Realtor information table **/
			try
			{
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
			    + Relator_table + " (ARR_RL_id INTEGER PRIMARY KEY Not null,ARR_RL_SRID  Varchar(50) ,ARR_RL_ComapanyName  Varchar(50) ,ARR_RL_RealtorName  Varchar(50) ,ARR_RL_RealAddress  Varchar(50) ,ARR_RL_RealAddress2  Varchar(50) ,ARR_RL_RealCity  Varchar(50) ,ARR_RL_RealCounty  Varchar(50) ,ARR_RL_RealRole  Varchar(50) ,ARR_RL_RealState  Varchar(50) ,ARR_RL_RealZip  Varchar(50) ,ARR_RL_RealContactPhone  Varchar(50) ,ARR_RL_RealFax  Varchar(50) ,ARR_RL_RealEmail  Varchar(50) ,ARR_RL_RealWebSite Varchar(50));");
			} catch (Exception e) {
				strerrorlog="Realtor info  table creation table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of Realtor info Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}

			
	break;
		case 4:
			/* MAILING DETAILS OF POLICYHOLDER */
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ MailingPolicyHolder
						+ " (MA_Ph_Id INTEGER PRIMARY KEY AUTOINCREMENT,ARR_ML_PH_InspectorId varchar(50) NOT NULL,ARR_ML_PH_SRID varchar(50) NOT NULL,ARR_ML Integer,ARR_ML_PH_Address1 varchar(100),ARR_ML_PH_Address2 varchar(100),ARR_ML_PH_City varchar(100),ARR_ML_PH_Zip varchar(100),ARR_ML_PH_State varchar(100),ARR_ML_PH_County varchar(100));");
			} catch (Exception e) {
				strerrorlog="Mailing PolicyHolder table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of PolicyHolder Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		 case 81:
			 try { arr_db.execSQL("CREATE TABLE IF NOT EXISTS " + ImageTable +
					 " (ARR_IM_ID INTEGER PRIMARY KEY Not null,ARR_IM_SRID Varchar(50) ,ARR_IM_Insepctiontype  Varchar(200) Default(''),ARR_IM_Elevation INTEGER Not null Default(0),ARR_IM_ImageName Varchar(150) ,ARR_IM_Nameext Varchar(150),ARR_IM_Description Varchar(150),ARR_IM_CreatedOn Datetime Not null default(CURRENT_TIMESTAMP),ARR_IM_ModifiedOn Datetime Not null default(CURRENT_TIMESTAMP),ARR_IM_ImageOrder INTEGER Not null default(0));"
					  );
					  
					  } catch (Exception e) {
					  strerrorlog="images table creation table not created";
					  Error_LogFile_Creation(strerrorlog+" "+" at "+
					  this.con.getClass().getName
					  ().toString()+" "+" in the stage of images Table Creation at "
					  +" "+datewithtime+" "+"in apk"+" "+apkrc); } 
			 break;
			case 21:
				/* DYNAMIC INSPECTION NAME INCLUDING CUSTOM INSPECTIONS */
				try {
					arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ allinspnamelist
							+ " (Ins_Id INTEGER PRIMARY KEY AUTOINCREMENT,ARR_Insp_ID integer ,ARR_Insp_Name varchar(150) NOT NULL,ARR_Custom_ID integer);");
				} catch (Exception e) {
					strerrorlog="DYNAMIC INSPECTION NAME LIST table not created";
					Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of PolicyHolder Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
				}
				break;

			 
			 case 82:
			 /** creating Photocaption table **/
			
			  try { arr_db.execSQL("CREATE TABLE IF NOT EXISTS " + Photo_caption +
			  " (ARR_IMP_ID INTEGER PRIMARY KEY Not null,ARR_IMP_INSP_ID Varchar(50) ,ARR_IMP_Caption Varchar(150) ,ARR_IMP_Insepctiontype  Varchar(200) Default(''),ARR_IMP_Elevation INTEGER Not null Default(0),ARR_IMP_ImageOrder Varchar(150));"
			  ); } catch (Exception e) {
			  strerrorlog="images table creation table not created";
			  Error_LogFile_Creation(strerrorlog+" "+" at "+
			  this.con.getClass().getName
			  ().toString()+" "+" in the stage of images Table Creation at "
			  +" "+datewithtime+" "+"in apk"+" "+apkrc); } 
			  break;
			  
			 case 83:
					/** Feedback info table **/
				 try {
						// db("DROP TABLE IF  EXISTS "
						// + FeedBackInfoTable + " ");
						arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
								+ FeedBackInfoTable
								+ " (ARR_FI_Wsid INTEGER PRIMARY KEY Not null,ARR_FI_Srid Varchar(50) ,ARR_FI_Inspection Varchar(50) Default(''), ARR_FI_IsCusServiceCompltd Bit Not null Default(0),ARR_FI_PresentatInspection Varchar(50) ,ARR_FI_IsInspectionPaperAvbl Bit Not null Default(0),ARR_FI_IsManufacturerInfo Bit Not null Default(0),ARR_FI_FeedbackComments Varchar(2000),ARR_FI_FeedbackComments_addendum Varchar(2000),ARR_FI_CreatedOn Datetime Not null default(CURRENT_TIMESTAMP),ARR_FI_othertxt Varchar(50),ARR_ISCUSTOM int default '0');");

					} catch (Exception e) {
						
					}


					break;
				case 84:
					/** Creating feedbackdocuments table **/
					try {
						// db("DROP TABLE IF  EXISTS "
						// + FeedBackDocumentTable + " ");
						arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
								+ FeedBackDocumentTable
								+ " (ARR_FI_DocumentId INTEGER PRIMARY KEY Not null,ARR_FI_D_SRID Varchar(50) ,ARR_FI_D_Inspection Varchar(50) Default(''),ARR_FI_D_DocumentTitle Varchar(50),ARR_FI_D_FileName Varchar(50) ,ARR_FI_D_Nameext Varchar(150),ARR_FI_D_ImageOrder INTEGER Not null default(0),ARR_FI_D_IsOfficeUse Bit Not null Default(0),ARR_FI_D_CreatedOn Datetime Not null default(CURRENT_TIMESTAMP),ARR_FI_D_ModifiedDate Datetime Not null default(CURRENT_TIMESTAMP),ARR_ISCUSTOM int default '0');");

					} catch (Exception e) {
						
					}
					break;
				case 431:/** CREATING Roof Cover type TABLE **/
					try {
						/*arr_db.execSQL("CREATE TABLE IF NOT EXISTS "+ Roof_cover_type+" (" +
								"id INTEGER PRIMARY KEY AUTOINCREMENT, RCT_masterid VARCHAR( 8 ) DEFAULT ( '' ),RCT_TypeValue VARCHAR( 100 )  DEFAULT ( '' )," +
								"RCT_FieldVal  VARCHAR( 200 )  DEFAULT ( '' ),RCT_Other     VARCHAR( 150 )  DEFAULT ( '' ));");*/
						arr_db.execSQL(" CREATE TABLE IF NOT EXISTS " +Rct_table+" (id INTEGER PRIMARY KEY AUTOINCREMENT, RCT_Ssrid VARCHAR( 5 ) DEFAULT ( '' ), RCT_insp_id VARCHAR( 5 ) DEFAULT ( '' ), RCT_rct VARCHAR( 150 ) DEFAULT ( '' ), RCT_PAD VARCHAR( 15 ) DEFAULT ( '' ), RCT_yoi VARCHAR( 5 ) DEFAULT ( '' ), RCT_npv VARCHAR( 5 ) DEFAULT ( 'false' ), RCT_rsl VARCHAR( 150 ) DEFAULT ( '' )" +
								", RCT_rsh VARCHAR( 50 ) DEFAULT ( '' ), RCT_rst VARCHAR( 50 ) DEFAULT ( '' ), RCT_bc VARCHAR( 50 ) DEFAULT ( '' ), RCT_rc VARCHAR( 50 ) DEFAULT ( '' ), RCT_arfl VARCHAR( 50 ) DEFAULT ( '' ), RCT_pre VARCHAR( 5 ) DEFAULT ( 'false' ), RCT_Rsh_per VARCHAR( 5 ) DEFAULT ( '0' )" +
								", RCT_other_rct VARCHAR( 150 ) DEFAULT ( '' ), RCT_other_yoi VARCHAR( 5 ) DEFAULT ( '' ), RCT_other_rsh VARCHAR( 150 ) DEFAULT ( '' ), RCT_other_rst VARCHAR( 150 ) DEFAULT ( '' ), RCT_other_aRFL VARCHAR( 150 ) DEFAULT ( '' ), RCT_saved VARCHAR( 1 ) DEFAULT ( '1' ))");
						arr_db.execSQL(" CREATE TABLE IF NOT EXISTS " +Roof_additional+" (id INTEGER PRIMARY KEY AUTOINCREMENT, R_ADD_Ssrid VARCHAR( 5 ) DEFAULT ( '' ), R_ADD_insp_id VARCHAR( 5 ) DEFAULT ( '' ), R_ADD_full VARCHAR( 5 ) DEFAULT ( '' ), R_ADD_scope VARCHAR( 150 ) DEFAULT ( '' ), R_ADD_na VARCHAR( 5 ) DEFAULT ( 'false' ), R_ADD_comment VARCHAR( 500 ) DEFAULT ( '' ),R_ADD_citizen VARCHAR( 500 ) DEFAULT ( '' )," +
								"R_ADD_WUWC VARCHAR( 25 ) DEFAULT ( '' ),R_ADD_PDRUC VARCHAR( 15 ) DEFAULT ( '' ),R_ADD_PN VARCHAR( 50 ) DEFAULT ( '' ),R_ADD_PD VARCHAR( 15 ) DEFAULT ( '' ), R_ADD_RME VARCHAR( 5 ) DEFAULT ( '' ),R_ADD_HVAC_E VARCHAR( 60 ) DEFAULT ( 'false' ),R_ADD_VEN_E VARCHAR( 60 ) DEFAULT ( 'false' ),R_ADD_STOR_E VARCHAR( 60 ) DEFAULT ( 'false' ),R_ADD_PIP VARCHAR( 5 ) DEFAULT ( 'false' ),R_ADD_clean_E VARCHAR( 5 ) DEFAULT ( 'false' )," +
								"R_ADD_ANT_P VARCHAR( 5 ) DEFAULT ( 'false' ),R_ADD_LSE VARCHAR( 5 ) DEFAULT ( 'false' ),R_ADD_O_E VARCHAR( 60 ) DEFAULT ( 'false' ),R_ADD_RMEL VARCHAR( 15 ) DEFAULT ( '' ),R_ADD_DRMS VARCHAR( 15 ) DEFAULT ( '' ),R_ADD_other_I VARCHAR( 250 ) DEFAULT ( '' ),R_RCT_na VARCHAR( 5 ) DEFAULT ( 'false' ),R_ADD_saved VARCHAR( 1 ) DEFAULT ( '1' ))");
						
						
						arr_db.execSQL(" CREATE TABLE IF NOT EXISTS " +RCN_table_dy+" (id INTEGER PRIMARY KEY AUTOINCREMENT, R_RCN_D_Ssrid VARCHAR( 5 ) DEFAULT ( '' ), R_RCN_D_insp_id VARCHAR( 5 ) DEFAULT ( '' ), R_RCN_D_title VARCHAR( 50 ) DEFAULT ( '' ), R_RCN_D_option VARCHAR( 15 ) DEFAULT ( '' ), R_RCN_D_Other VARCHAR( 250 ) DEFAULT ( '' ), R_RCN_D_saved VARCHAR( 1 ) DEFAULT ( '0' ))");
						arr_db.execSQL(" CREATE TABLE IF NOT EXISTS " +RCN_table+" (RCN_id INTEGER PRIMARY KEY AUTOINCREMENT, R_RCN_Ssrid VARCHAR( 5 ) DEFAULT ( '' ), R_RCN_insp_id VARCHAR( 5 ) DEFAULT ( '' ), R_RCN_OCR VARCHAR( 300 ) DEFAULT ( '' ),R_RCN_RRN VARCHAR( 300 ) DEFAULT ( '' ),R_RCN_VSL VARCHAR( 300 ) DEFAULT ( '' ),R_RCN_VSTDS VARCHAR( 300 ) DEFAULT ( '' ),R_RCN_VSCBS VARCHAR( 300 ) DEFAULT ( '' ),R_RCN_VSDTS VARCHAR( 300 ) DEFAULT ( '' )," +
								"R_RCN_VDRS VARCHAR( 300 ) DEFAULT ( '' ),R_RCN_ODN VARCHAR( 300 ) DEFAULT ( '' )" +
								",R_RCN_HRMS VARCHAR( 300 ) DEFAULT ( '' ),R_RCN_FC VARCHAR( 100 ) DEFAULT ( '' ),R_RCN_GRAC VARCHAR( 300 ) DEFAULT ( '' ),R_RCN_RLN VARCHAR( 300 ) DEFAULT ( '' ),R_RCN_RPN VARCHAR( 300 ) DEFAULT ( '' ),R_RCN_HREBR VARCHAR( 100 ) DEFAULT ( '' ),R_RCN_WORR VARCHAR( 300 ) DEFAULT ( '' ),R_RCN_RMRA VARCHAR( 300 ) DEFAULT ( '' ),R_RCN_SFPE VARCHAR( 300 ) DEFAULT ( '' ),R_RCN_Amount VARCHAR( 10 ) DEFAULT ( '' ),R_RCN_EVPWD VARCHAR( 300 ) DEFAULT ( '' ),R_RCN_RME VARCHAR( 300 ) DEFAULT ( '' )" +
								",R_RCN_BFP VARCHAR( 300 ) DEFAULT ( '' ),R_RCN_ESM VARCHAR( 300 ) DEFAULT ( '' ),R_RCN_SOSAS VARCHAR( 300 ) DEFAULT ( '' ),R_RCN_DRS VARCHAR( 300 ) DEFAULT ( '' )," +
								"R_RCN_na VARCHAR( 5 ) DEFAULT ( 'false' ),R_RCN_saved VARCHAR( 1 ) DEFAULT ( '1' ))");
						
					} catch (Exception e) {
						System.out.println("issues occure "+e.getMessage());
						
					}

					break;
				case 31:
					/* WIND MIT QUESTIONS DATA TABLE */
					try {
						arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
								+ Wind_Questiontbl
								+ " (Ques_Id INTEGER PRIMARY KEY AUTOINCREMENT,insp_typeid varchar(50) NOT NULL,fld_srid varchar(50) NOT NULL,fld_buildcode Integer,fld_bcyearbuilt varchar(100),fld_bcpermitappdate varchar(100),fld_roofdeck INTEGER,fld_rdothertext varchar(100),fld_roofwall INTEGER,fld_roofwallsub INTEGER,fld_roofwallclipsminval INTEGER,fld_roofwallclipssingminval INTEGER,fld_roofwallclipsdoubminval INTEGER,fld_rwothertext varchar(100),fld_roofgeometry INTEGER,fld_geomlength INTEGER,fld_roofgeomtotalarea INTEGER,fld_roofgeomothertext varchar(100),fld_swr INTEGER,fld_openprotect INTEGER,fld_optype INTEGER,fld_opsubvalue INTEGER,fld_oplevelchart varchar(100),fld_opGOwindentdoorval varchar(100),fld_opGOgardoorval varchar(100),fld_opGOskylightsval varchar(100),fld_opGOglassblockval  varchar(100),fld_opNGOentrydoorval varchar(100),fld_opNGOgaragedoorval varchar(100),fld_wcvalue varchar(100),fld_wcreinforcement varchar(500),fld_quesmodifydate Varchar(20));");
					} catch (Exception e) {
						
						strerrorlog="WindMit Questions table not created";
						Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of WindMit Questions Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
						
					}
					break;
				case 32:
					/* WIND MIT QUESTIONS COMMENTS TABLE */
					try {
						arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
								+ Wind_QuesCommtbl
								+ " (Ques_Id INTEGER PRIMARY KEY AUTOINCREMENT,insp_typeid varchar(50) NOT NULL,fld_srid varchar(50) NOT NULL,fld_buildcodecomments varchar(600),fld_roofcovercomments varchar(600),fld_roofdeckcomments varchar(600),fld_roofwallcomments varchar(600),fld_roofgeometrycomments varchar(600),fld_swrcomments varchar(600),fld_openprotectioncomments varchar(600),fld_wallconscomments varchar(600),fld_insoverallcomments Varchar(600));");
					} catch (Exception e) {
						System.out.println("table creating error"+e.getMessage());
						strerrorlog="Question Comments table not created";
						Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of WindMit Question Comments Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
						
					}
					break;
				case 33:
					/** creating roofcover table **/
					try {

						arr_db.execSQL("CREATE TABLE IF NOT EXISTS " + quesroofcover
								+ " (roofcoverid INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
								+ "INSP_TYPEID VARCHAR (50) NOT NULL DEFAULT '',"
								+ "SRID VARCHAR (50) NOT NULL DEFAULT '',"
								+ "RoofCoverType VARCHAR (50) NOT NULL DEFAULT '0',"
								+ "RoofCoverValue int NOT NULL DEFAULT '0',"
								+ "PermitApplnDate1 VARCHAR (50) NOT NULL DEFAULT '',"
								+ "PermitApplnDate2 VARCHAR (50) NOT NULL DEFAULT '',"
								+ "PermitApplnDate3 VARCHAR (50) NOT NULL DEFAULT '',"
								+ "PermitApplnDate4 VARCHAR (50) NOT NULL DEFAULT '',"
								+ "PermitApplnDate5 VARCHAR (50) NOT NULL DEFAULT '',"
								+ "PermitApplnDate6 VARCHAR (50) NOT NULL DEFAULT '',"
								+ "RoofCoverTypeOther VARCHAR (100) DEFAULT '',"
								+ "ProdApproval1 VARCHAR (50) NOT NULL DEFAULT '',"
								+ "ProdApproval2 VARCHAR (50) NOT NULL DEFAULT ' ',"
								+ "ProdApproval3 VARCHAR (50) NOT NULL DEFAULT '',"
								+ "ProdApproval4 VARCHAR (50) NOT NULL DEFAULT ' ',"
								+ "ProdApproval5 VARCHAR (50) NOT NULL DEFAULT '',"
								+ "ProdApproval6 VARCHAR (50) NOT NULL DEFAULT ' ',"
								+ "InstallYear1 VARCHAR (50) NOT NULL DEFAULT ' ',"
								+ "InstallYear2 VARCHAR (50) NOT NULL DEFAULT ' ',"
								+ "InstallYear3 VARCHAR (50) NOT NULL DEFAULT ' ',"
								+ "InstallYear4 VARCHAR (50) NOT NULL DEFAULT ' ',"
								+ "InstallYear5 VARCHAR (50) NOT NULL DEFAULT ' ',"
								+ "InstallYear6 VARCHAR (50) NOT NULL DEFAULT ' ',"
								+ "NoInfo1  VARCHAR (50) NOT NULL DEFAULT ' ',"
								+ "NoInfo2 VARCHAR (50) NOT NULL DEFAULT ' ',"
								+ "NoInfo3  VARCHAR (50) NOT NULL DEFAULT ' ',"
								+ "NoInfo4  VARCHAR (50) NOT NULL DEFAULT ' ',"
								+ "NoInfo5  VARCHAR (50) NOT NULL DEFAULT ' ',"
								+ "NoInfo6  VARCHAR (50) NOT NULL DEFAULT ' ',"
								+ "RoofPreDominant int NOT NULL DEFAULT '0');");

					}

					catch (Exception e) {
						
					}
					 break;
				case 34:
					/** WALL CONSTRUCTION - table **/

					try {

						arr_db.execSQL("CREATE TABLE IF NOT EXISTS " + WindMit_WallCons
								+ " (WCID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
								+ "insp_typeid VARCHAR (50) NOT NULL,"
								+ "fld_srid VARCHAR (50) NOT NULL DEFAULT '',"
								+ "fld_Elevation VARCHAR (100) NOT NULL DEFAULT '0',"
								+ "fld_Stories VARCHAR (100) NOT NULL DEFAULT '0',"								
								+ "fld_Percentage int NOT NULL DEFAULT '0',"
								+ "fld_elevation_othertxt varchar(150),"
								+ "fld_WallconsOther VARCHAR (100));");
					}
					catch (Exception e) {
						
					}
					 break;

				case 35:
					/** creating Auxillary Building - Commercial table **/

					try {

						arr_db.execSQL("CREATE TABLE IF NOT EXISTS " + Comm_AuxBuilding
								+ " (AUXID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
								+ "insp_typeid VARCHAR (50) NOT NULL,"
								+ "fld_srid VARCHAR (50) NOT NULL DEFAULT '',"
								+ "fld_aux_Building_type VARCHAR (250) NOT NULL DEFAULT '0',"
								+ "fld_aux_Building_other VARCHAR (500),"
								+ "fld_aux_Ins_Size VARCHAR (250) NOT NULL DEFAULT '0',"
								+ "fld_aux_Cons__RC_3Years VARCHAR (250) NOT NULL DEFAULT '0',"
								+ "fld_aux_WS_RC_OGN_DD VARCHAR (250) NOT NULL DEFAULT '0');");
					}
					catch (Exception e) {
						System.out.println("aux building "+e.getMessage());
					}
					 break;
				case 36:
					/** creating Comments Table - Commercial table **/

					try {

						arr_db.execSQL("CREATE TABLE IF NOT EXISTS " + Wind_Commercial_Comm
								+ " (CommID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
								+ "insp_typeid VARCHAR (50) NOT NULL,"
								+ "fld_srid VARCHAR (50) NOT NULL DEFAULT '',"
								+ "fld_commercialareascomments varchar(600),"
								+ "fld_auxillarycomments varchar(600),"
								+ "fld_restsuppcomments VARCHAR (600));");
					}
					catch (Exception e) {
						System.out.println("aux building "+e.getMessage());
					}
					 break;
				case 37:
					/** creating Appliance Table - Restaurant supplement Commercial table **/

					try {

						arr_db.execSQL("CREATE TABLE IF NOT EXISTS " + Comm_RestSuppAppl
								+ " (ApplID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
								+ "insp_typeid VARCHAR (50) NOT NULL,"
								+ "fld_srid VARCHAR (50) NOT NULL DEFAULT '',"
								+ "fld_appliancetype varchar(600),"
								+ "fld_applianceother varchar(100),"
								+ "fld_appldata varchar(600));");
					}
					catch (Exception e) {
						System.out.println("aux building "+e.getMessage());
					}
                      break;
				case 38:
					/** creating Restaurant Supplement Table - Commercial II & III **/

					try {

						arr_db.execSQL("CREATE TABLE IF NOT EXISTS " + Comm_RestSupplement
								+ " (Restid INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
								+ "insp_typeid VARCHAR (50) NOT NULL,"
								+ "fld_srid VARCHAR (50) NOT NULL DEFAULT '',"
								+ "fld_appliance_na varchar(600) DEFAULT '');");
					}
					catch (Exception e) {
						System.out.println("aux building "+e.getMessage());
					}
					 break;


				case 311:
					/** creating Communal Areas Elevation Table - B1802 & Commercial I **/
					try {

						arr_db.execSQL("CREATE TABLE IF NOT EXISTS " + Communal_AreasElevation
								+ " (Commid INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
								+ "insp_typeid VARCHAR (50) NOT NULL,"
								+ "fld_srid VARCHAR (50) NOT NULL DEFAULT '',"
								+ "fld_elevationno varchar(25),"
								+ "fld_model varchar(100),"
								+ "fld_servingallfloors varchar(10),"
								+ "fld_datelastinsp varchar(50),"
								+ "fld_uploadimg varchar(500),"
								+ "fld_manufacturer varchar(200));");
					}
					catch (Exception e) {
						System.out.println("aux building "+e.getMessage());
					}
						break;

				case 43:/** CREATING Roof section type TABLE **/
					try {
						/*arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
								+ General_Roof_information
								+ " (RC_Id INTEGER PRIMARY KEY Not null,RC_masterid VARCHAR( 8 ) DEFAULT ( '' ),RC_Condition_type Varchar(100) Default(''), RC_Condition_type_DESC Varchar(1000) Default('') ,RC_Condition_type_val Varchar(50) Default('No'),RC_Condition_type_order Varchar(5) Default(''),RC_Enable Varchar(2) Default('1'));");
*/
					} catch (Exception e) {
						System.out.println("issues occure "+e.getMessage());
						
					}



					break;

		case 8:
			 try { arr_db.execSQL("CREATE TABLE IF NOT EXISTS "+WeatherCondition+" (id INTEGER PRIMARY KEY AUTOINCREMENT,Wea_srid VARCHAR( 5 ) DEFAULT ( '' )," +
			 		"Wea_OutDoor VARCHAR( 10 )  DEFAULT ( '0.0' ),Wea_InDoor VARCHAR( 10 )  DEFAULT ( '0.0' ),Wea_WeatherCondi VARCHAR( 20 )  DEFAULT ( '' )," +
			 		"Wea_RecentCondi  VARCHAR( 20 )  DEFAULT ( '' ),Wea_R_outTemp    VARCHAR( 10 )  DEFAULT ( '' ),Wea_R_inTemp     VARCHAR( 10 )  DEFAULT ( '' )," +
			 		"Wea_date VARCHAR( 15 )  DEFAULT ( '' ),Wea_NA VARCHAR( 15 )  DEFAULT ( '' ));");
				  
				  } catch (Exception e) {
				  strerrorlog="weather table creation table not created";
				  Error_LogFile_Creation(strerrorlog+" "+" at "+
				  this.con.getClass().getName
				  ().toString()+" "+" in the stage of images Table Creation at "
				  +" "+datewithtime+" "+"in apk"+" "+apkrc); }
		break;
		case 9:
			 try { arr_db.execSQL("CREATE TABLE IF NOT EXISTS "+Comments_lib+" (_id INTEGER PRIMARY KEY AUTOINCREMENT,CL_inspid VARCHAR( 5 ) DEFAULT ( '' )," +
				 		"CL_inspectionName VARCHAR( 40 )  DEFAULT ( '' ),CL_type VARCHAR( 20 )  DEFAULT ( '' ),CL_question VARCHAR( 20 )  DEFAULT ( '' )," +
				 		"CL_option  VARCHAR( 30 )  DEFAULT ( '' ),CL_Comments    VARCHAR( 250 )  DEFAULT ( '' ),CL_Active    BOOLEAN  DEFAULT false,CL_date     datetime default current_timestamp," +
				 		"CL_Mdate datetime default current_timestamp);");
					  
					  } catch (Exception e) {
						  System.out.println("CREATE TABLE IF NOT EXISTS "+Comments_lib+" (_id INTEGER PRIMARY KEY AUTOINCREMENT,CL_inspid VARCHAR( 5 ) DEFAULT ( '' )," +
							 		"CL_inspectionName VARCHAR( 40 )  DEFAULT ( '' ),CL_type VARCHAR( 20 )  DEFAULT ( '' ),CL_question VARCHAR( 20 )  DEFAULT ( '' )," +
							 		"CL_option  VARCHAR( 30 )  DEFAULT ( '' ),CL_Comments    VARCHAR( 250 )  DEFAULT ( '' ),CL_Active    BOOLEAN  DEFAULT false,CL_date     datetime default current_timestamp," +
							 		"CL_Mdate datetime default current_timestamp);");
					  strerrorlog="weather table creation table not created";
					  Error_LogFile_Creation(strerrorlog+" "+" at "+
					  this.con.getClass().getName
					  ().toString()+" "+" in the stage of images Table Creation at "
					  +" "+datewithtime+" "+"in apk"+" "+apkrc); }
			break;
		case 312:					
			/** NA table for Window Openings, Door Openings, Skylights - B1802 & Commercial I **/
			try {

				arr_db.execSQL("CREATE TABLE IF NOT EXISTS " + WindDoorSky_NA
						+ " (sid INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
						+ "insp_typeid VARCHAR (50) NOT NULL,"
						+ "fld_srid VARCHAR (50) NOT NULL DEFAULT '',"
						+ "fld_windowopenNA varchar(25) DEFAULT '',"
						+ "fld_dooropenNA varchar(25) DEFAULT '',"
						+ "fld_skylightsNA varchar(25) DEFAULT '');");
			}
			catch (Exception e) {
				System.out.println("aux building "+e.getMessage());
			}	
			break;

		case 61:
			/* BUILDING INFORMATION TABLE */
            try{
				
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "+BI_General+" (BI_Id INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50)," +
						"BI_INS_NEI_OCC_NOB_NOS_NOU_BS varchar(600) DEFAULT(''),BI_YOC varchar(150) DEFAULT('')," +
						"BI_BOCCU_STYPE varchar(100) DEFAULT(''),BI_PERM_CONFIRMED varchar(200) DEFAULT(''),BI_BALCONYPRES varchar(150) DEFAULT(''),BI_INCI_OCCU varchar(150) DEFAULT('')," +
						"BI_ADDI_STRU varchar(250) DEFAULT(''),BI_SCOPE varchar(250) DEFAULT(''),BI_LOC_DESC varchar(200) DEFAULT(''),BI_LOC_NA varchar(25) DEFAULT(''),BI_OBSERV_NA varchar(25) DEFAULT(''),BI_OSERV1 varchar(200) DEFAULT(''),BI_OSERV2 varchar(200) DEFAULT('')," +
						"BI_OSERV3 varchar(200) DEFAULT(''),BI_OSERV4 varchar(200) DEFAULT(''),BI_OSERV5 varchar(200) DEFAULT(''),BI_OSERV6 varchar(200) DEFAULT('')," +
						"BI_ISO_NA varchar(25) DEFAULT(''),BI_ISO varchar(250) DEFAULT(''),BI_FOUN_NA varchar(25) DEFAULT(''),BI_FOUN varchar(250) DEFAULT(''),BI_WS_NA varchar(25) DEFAULT(''),BI_WALLSTRU varchar(200) DEFAULT(''),BI_WALLSTRUPER varchar(200) DEFAULT(''),BI_WALLSTRUC_OTHER varchar(100) DEFAULT('')," +
						"BI_WC_NA varchar(25) DEFAULT(''),BI_WALLCLAD varchar(200) DEFAULT(''),BI_WALLCLAD_OTHER varchar(100) DEFAULT(''),BI_BUILDSEP_NA varchar(25) DEFAULT(''),BI_BUILD_SEP varchar(200) DEFAULT(''),BI_BUILD_COMM varchar(550) DEFAULT(''),BI_NO_COMM1 varchar(25) DEFAULT(''),BI_NO_COMM2 varchar(25) DEFAULT(''),BI_NO_COMM3 varchar(25) DEFAULT(''),BI_GEN_NA varchar(25) DEFAULT(''));");
			}
			catch (Exception e) {
				strerrorlog="Building question table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
				
			}
			break;
			
		case 62:
			/* BUILDING INFORMATION WINDOW OPENINGS TABLE */
			try{
				
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "+BI_Windopenings+" (BI_WindId INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50),insptypeid varchar(50)," +
						"fld_elevation varchar(50),BI_WindowOpenings varchar(200),fld_elevation_othertxt varchar(150),fld_wind_othrtxt varchar(150));");
			}
			catch (Exception e) {
				System.out.println("table creatin eror"+e.getMessage());
				strerrorlog="Building Window Openings table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
				
			}
			
			break;
		case 63:
			/* BUILDING INFORMATION DOOR OPENINGS TABLE */
			try{
				
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "+BI_Dooropenings+" (BI_DoorId INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50),insptypeid varchar(50)," +
						"fld_elevation varchar(50),BI_DoorOpenings varchar(200),fld_elevation_othertxt varchar(150));");
			}
			catch (Exception e) {
				strerrorlog="Building Door Openings table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
				
			}
			
			break;
		case 64:
			/* BUILDING INFORMATION SKYLIGHTS TABLE */
			try{
				
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "+BI_Skylights+" (BI_SkyId INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50),insptypeid varchar(50)," +
						"fld_elevation varchar(50),BI_Skylights varchar(200),fld_elevation_othertxt varchar(150));");
			}
			catch (Exception e) {
				strerrorlog="Building Skylights table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
				
			}		
			break;

		case 65:
			/* COMMERCIAL BUILDNG NO DATA TABLE */
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ Commercial_Building
						+ " (C_Id INTEGER PRIMARY KEY AUTOINCREMENT,insp_typeid varchar(50) NOT NULL,fld_srid varchar(50) NOT NULL,BI_NO_COMM1 varchar(25) DEFAULT(''),BI_NO_COMM2 varchar(25) DEFAULT(''),BI_NO_COMM3 varchar(25) DEFAULT(''),Commercial_Flag varchar(25));");
			} catch (Exception e) {
				
				strerrorlog="WindMit Questions table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of WindMit Questions Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
				
			}

			break;
		case 70:
			/* COMMERCIAL BUILDNG NO DATA TABLE */
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ Comm_Building
						+ " (C_Id INTEGER PRIMARY KEY AUTOINCREMENT,insp_typeid varchar(50) NOT NULL,fld_srid varchar(50) NOT NULL,BUILDING_NO varchar(25) DEFAULT(''),Commercial_Flag varchar(25));");
			} catch (Exception e) {
				
				strerrorlog="WindMit Questions table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of WindMit Questions Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
				
			}

			break;
		case 39:
			/** creating Communal Areas Table - B1802 & Commercial I **/
			try {

			arr_db.execSQL("CREATE TABLE IF NOT EXISTS " + Communal_Areas
					+ " (Commid INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
					+ "insp_typeid VARCHAR (50) NOT NULL,"
					+ "fld_srid VARCHAR (50) NOT NULL DEFAULT '',"
					+ "fld_lobby_NA varchar(10) DEFAULT(''),"
					+ "fld_lobby varchar(600) DEFAULT(''),"
					+ "fld_stairwell_NA varchar(25) DEFAULT(''),"
					+ "fld_stairwells varchar(600) DEFAULT(''),"
					+ "fld_elevators_NA varchar(25) DEFAULT(''),"
					+ "fld_elevatorsoption varchar(600) DEFAULT(''),"
					+ "fld_poolareas_NA varchar(25) DEFAULT(''),"
					+ "fld_poolareasother varchar(75) DEFAULT(''),"
					+ "fld_poolareas varchar(600) DEFAULT(''));");
		}
		catch (Exception e) {
			System.out.println("aux building "+e.getMessage());
		}
		case 40:
			try {

				arr_db.execSQL("CREATE TABLE IF NOT EXISTS " + Commercial_Wind
						+ " (commwindid INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
						+ "insp_typeid VARCHAR (50) NOT NULL,"
						+ "fld_srid VARCHAR (50) NOT NULL DEFAULT '',"
						+ "fld_terrain varchar(250) DEFAULT(''),"
						+ "fld_roofcoverings varchar(600) DEFAULT(''),"
						+ "fld_roofdeck varchar(250) DEFAULT(''),"
						+ "fld_swr varchar(600) DEFAULT(''),"
						+ "fld_openingprotection varchar(250) DEFAULT(''),"
						+ "fld_certfication varchar(100) DEFAULT(''),"
						+ "fld_rccomments varchar(600) DEFAULT(''),"
						+ "fld_rdcomments varchar(600) DEFAULT(''),"
						+ "fld_swrcomments varchar(600) DEFAULT(''),"
						+ "fld_opcomments varchar(600) DEFAULT(''));");
			}
			catch (Exception e) {
				System.out.println("aux building "+e.getMessage());
			}
			
			break;
		case 66:
			/** OnlineTable **/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ OnlineTable
						+ " (Id INTEGER PRIMARY KEY AUTOINCREMENT,s_SRID varchar(50),InspectorId varchar(50),Status varchar(50),SubStatus varchar(50),InspectionTypeId varchar(50),s_OwnersNameFirst varchar(100),s_OwnersNameLast varchar(100),s_propertyAddress varchar(100),s_City varchar(100),s_State varchar(100),s_County varchar(100),s_ZipCode	varchar(100),s_OwnerPolicyNumber varchar(100),ScheduleDate varchar(100),InspectionStartTime varchar(100),InspectionEndTime varchar(100),InspectionName varchar(250));");

			} catch (Exception e) {
				
			}
			break;
		case 20:/** CREATING IDMA VERSION TABLE **/
			try {
				
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ IDMAVersion
						+ "(VId INTEGER PRIMARY KEY Not null,ID_VersionCode varchar(50),ID_VersionName varchar(50),ID_VersionType varchar(50));");

			} catch (Exception e) {
				
				
			}
			break;

		 case 99:
	        	/**Module id for differenciate the list,rct table , General information table, General roof =1, RCT=2,RCT_list=3 **/
			CreateARRTable(10);
			 try
				{

				 arr_db.execSQL("CREATE TABLE IF NOT EXISTS "+option_list+" (id INTEGER PRIMARY KEY AUTOINCREMENT,module_id VARCHAR( 4 )   DEFAULT ( '' ),option_tit VARCHAR( 100 ) DEFAULT ( '' ),options VARCHAR( 200 ) DEFAULT ( '' ),option_max_l INT( 10 ) DEFAULT ( 0 ),"+
					        "option_max_W INT( 10 ) DEFAULT ( 0 ),option_other VARCHAR( 200 ) DEFAULT ( '' ),other_l INT( 5 ) DEFAULT ( 0 ),other_w INT( 5 ) DEFAULT ( 0 ),option_type VARCHAR( 25 ) DEFAULT ( '' ),Default_option VARCHAR( 25 ) DEFAULT ( '' )," +
					        "tit_max_w INT( 5 )DEFAULT ( 250 ),Requirred BOOLEAN DEFAULT ( 0 ),option_potion INT DEFAULT ( 0 ),option_filter VARCHAR( 10 ) DEFAULT ( '' ),Other_filter VARCHAR( 10 ) DEFAULT ( '' ),insp_id VARCHAR( 4 )   DEFAULT ( '' ));");
						Cursor c =SelectTablefunction(option_list, "");
						CreateARRTable(10);
						if(c.getCount()==0)
						{
							int i=0;
							int Rdopt_maxwidth=110;
							int Comm_maxwidth=110;
							int max_option_p_r=5;
							int tit_max_w=250;
							
							arr_db.execSQL(" Insert into "+option_list+" SELECT  '"+(i++)+"' as id ,'2' as module_id,'"+encode("1")+"' as option_tit,'-Select-,Asphalt/Fiberglass Shingle,Concrete/Clay Tile,Metal,Built Up,Membrane,Other' as options,'0' as option_max_l,'200' as option_max_W,'Other' as option_other," +
											"'50' as other_l,'150' as other_w,'Spinner' as option_type,''  as Default_option,'180' as tit_max_w,'1' as Requirred,'0' as option_potion,'' as option_filter,''  as Other_filter,'1' as insp_id" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("2")+"','','0','160','','0','0','DatePicker','','180','1','1','','','1'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("3")+"','-Select-,Unknown,Other,2015,2014,2013,2012,2011,2010,2009,2008,2007,2006,2005,2004,2003,2002,2001,2000,1999,1998,1997,1996," +
									"1995,1994,1993,1992,1991,1990,1989,1988,1987,1986,1985,1984,1983,1982,1981,1980,1979,1978,1977,1976," +
									"1975,1974,1973,1972,1971,1970,1969,1968,1967,1966,1965,1964,1963,1962,1961,1960,1959,1958,1957,1956," +
									"1955,1954,1953,1952,1951,1950,1949,1948,1947,1946,1945,1944,1943,1942,1941,1940,1939,1938,1937,1936," +
									"1935,1934,1933,1932,1931,1930,1929,1928,1927,1926,1925,1924,1923,1922,1921,1920,1919,1918,1917,1916," +
									"1915,1914,1913,1912,1911,1910,1909,1908,1907,1906,1905,1904,1903,1902,1901,1900'," +
									"'0','200','Other','4','100','Spinner','','180','1','2','','Number','1'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("4")+"','','0','100','','0','0','CheckBox','','180','0','3','','','1'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("5")+"','-Select-,<1/12,1/12,2/12,3/12,4/12,5/12,6/12,7/12,8/12,9/12,10/12,11/12,12/12,13/12,14/12,Flat,Unknown','0','200','','0','0','Spinner','','180','1','4','','','1'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("6")+"','-Select-,Hip Roof,Gable Roof,Mansard Roof,Flat Roof,Shed Roof,Butterfly Roof,Other','0','150','Other','50','200','Spinner','','180','1','5','','','1'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("7")+"','-Select-,Conventional Frame,Wood Truss,Steel Truss,Concrete Frame,Structural,Steel Frame,Other','0','200','Other','50','150','Spinner','','180','1','6','','','1'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("8")+"','-Select-,>2001 FBC,1994 SFBC,Pre 2001 FBC,Pre 1994 SFBC,Not Applicable,Beyond Scope of Inspection','0','200','','0','0','Spinner','','180','1','7','','','1'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("9")+"','-Select-,Simple Roof Shape,Moderate Roof Shape,Complex Roof Shape','0','200','','0','0','Spinner','','180','1','8','','','1'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("10")+"','-Select-,Not Determined,Roof Aged,1 Year,2 Years,3 Years,4 Years,5 Years,6 Years,7 Years,8 Years,9 Years,>10 Years,Other','0','200','Other','3','100','Spinner','','180','1','9','','Number','1'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("11")+"','','0','100','','0','0','CheckBox','','180','0','10','','','1'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("12")+"','','0','160','','50','150','ImagePicker','','180','0','11','','','1'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("44")+"','','0','100','','0','0','ImagePicker','','110','0','12','','','1'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("14")+"','','0','100','','0','0','TextView','','30','0','0','','','1'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("1")+"','','0','100','','0','0','TextView','','120','0','1','','','1'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("2")+"','','0','100','','0','0','TextView','','140','0','2','','','1'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("3")+"','','0','100','','0','0','TextView','','125','0','3','','','1'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("4")+"','','0','100','','0','0','TextView','','160','0','4','','','1'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("5")+"','','0','100','','0','0','TextView','','100','0','5','','','1'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("6")+"','','0','100','','0','0','TextView','','120','0','6','','','1'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("7")+"','','0','100','','0','0','TextView','','130','0','7','','','1'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("8")+"','','0','100','','0','0','TextView','','110','0','8','','','1'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("9")+"','','0','100','','0','0','TextView','','125','0','9','','','1'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("10")+"','','0','100','','0','0','TextView','','110','0','10','','','1'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("11")+"','','0','100','','0','0','TextView','','100','0','11','','','1'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("13")+"','','0','100','','0','0','TextView','','120','0','13','','','1'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("15")+"','Yes,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Rdopt_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','1','','','1'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("16")+"','Yes,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Rdopt_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','2','','','1'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("17")+"','Yes,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Rdopt_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','3','','','1'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("18")+"','Yes,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Rdopt_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','4','','','1'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("19")+"','Yes,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Rdopt_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','5','','','1'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("20")+"','Yes,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Rdopt_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','6','','','1'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("21")+"','Yes,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Rdopt_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','7','','','1'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("22")+"','Excellent,Good,Fair,Poor','"+max_option_p_r+"','"+Rdopt_maxwidth+"','Poor','250','0','Radio','','"+tit_max_w+"','1','0','','','1'" +
									
									" UNION SELECT  '"+(i++)+"','2','"+encode("1")+"','-Select-,Asphalt/Fiberglass Shingle,Concrete/Clay Tile,Metal,Built Up,Membrane,Other','0','200','Other','50','150','Spinner','','180','1','0','','','2'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("2")+"','','0','160','','0','0','DatePicker','','180','1','1','','','2'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("3")+"','"+
									"-Select-,Unknown,Other,2015,2014,2013,2012,2011,2010,2009,2008,2007,2006,2005,2004,2003,2002,2001,2000,1999,1998,1997,1996," +
									"1995,1994,1993,1992,1991,1990,1989,1988,1987,1986,1985,1984,1983,1982,1981,1980,1979,1978,1977,1976," +
									"1975,1974,1973,1972,1971,1970,1969,1968,1967,1966,1965,1964,1963,1962,1961,1960,1959,1958,1957,1956," +
									"1955,1954,1953,1952,1951,1950,1949,1948,1947,1946,1945,1944,1943,1942,1941,1940,1939,1938,1937,1936," +
									"1935,1934,1933,1932,1931,1930,1929,1928,1927,1926,1925,1924,1923,1922,1921,1920,1919,1918,1917,1916," +
									"1915,1914,1913,1912,1911,1910,1909,1908,1907,1906,1905,1904,1903,1902,1901,1900'," +
									"'0','200','Other','4','100','Spinner','','180','1','2','','Number','2'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("4")+"','','0','100','','0','0','CheckBox','','180','0','3','','','2'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("5")+"','-Select-,<1/12,1/12,2/12,3/12,4/12,5/12,6/12,7/12,8/12,9/12,10/12,11/12,12/12,13/12,14/12,Flat,Unknown','0','200','','0','0','Spinner','','180','1','4','','','2'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("6")+"','-Select-,Hip Roof,Gable Roof,Mansard Roof,Flat Roof,Shed Roof,Butterfly Roof,Other','0','150','Other','50','200','Spinner','','180','1','5','','','2'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("7")+"','-Select-,Conventional Frame,Wood Truss,Steel Truss,Concrete Frame,Structural,Steel Frame,Other','0','200','Other','50','150','Spinner','','180','1','6','','','2'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("8")+"','-Select-,>2001 FBC,1994 SFBC,Pre 2001 FBC,Pre 1994 SFBC,Not Applicable,Beyond Scope of Inspection','0','200','','0','0','Spinner','','180','1','7','','','2'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("9")+"','-Select-,Simple Roof Shape,Moderate Roof Shape,Complex Roof Shape','0','200','','0','0','Spinner','','180','1','8','','','2'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("10")+"','-Select-,Not Determined,Roof Aged,1 Year,2 Years,3 Years,4 Years,5 Years,6 Years,7 Years,8 Years,9 Years,>10 Years,Other','0','200','Other','3','100','Spinner','','180','1','9','','Number','2'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("11")+"','','0','100','','0','0','CheckBox','','180','0','10','','','2'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("14")+"','','0','100','','0','0','TextView','','30','0','0','','','2'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("1")+"','','0','100','','0','0','TextView','','120','0','1','','','2'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("2")+"','','0','100','','0','0','TextView','','140','0','2','','','2'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("3")+"','','0','100','','0','0','TextView','','125','0','3','','','2'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("4")+"','','0','100','','0','0','TextView','','160','0','4','','','2'" + 
									" UNION SELECT  '"+(i++)+"','3','"+encode("5")+"','','0','100','','0','0','TextView','','100','0','5','','','2'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("6")+"','','0','100','','0','0','TextView','','120','0','6','','','2'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("7")+"','','0','100','','0','0','TextView','','130','0','7','','','2'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("8")+"','','0','100','','0','0','TextView','','110','0','8','','','2'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("9")+"','','0','100','','0','0','TextView','','125','0','9','','','2'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("10")+"','','0','100','','0','0','TextView','','110','0','10','','','2'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("11")+"','','0','100','','0','0','TextView','','100','0','11','','','2'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("13")+"','','0','100','','0','0','TextView','','120','0','13','','','2'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("12")+"','','0','160','','50','150','ImagePicker','','180','0','11','','','2'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("44")+"','','0','100','','0','0','ImagePicker','','110','0','12','','','2'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("22")+"','Excellent,Good,Fair,Poor','"+max_option_p_r+"','"+Rdopt_maxwidth+"','Poor','250','0','Radio','','"+tit_max_w+"','1','0','','','2'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("15")+"','Yes,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Rdopt_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','1','','','2'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("16")+"','Yes,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Rdopt_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','2','','','2'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("17")+"','Yes,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Rdopt_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','3','','','2'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("18")+"','Yes,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Rdopt_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','4','','','2'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("19")+"','Yes,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Rdopt_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','5','','','2'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("20")+"','Yes,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Rdopt_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','6','','','2'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("21")+"','Yes,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Rdopt_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','7','','','2'" +
															
									" UNION SELECT  '"+(i++)+"','2','"+encode("1")+"','-Select-,Asphalt/Fiberglass Shingle,Concrete/Clay Tile,Metal,Built Up,Membrane,Other','0','200','Other','50','150','Spinner','','180','1','0','','','5'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("2")+"','','0','160','','0','0','DatePicker','','180','1','1','','','5'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("3")+"','" +
									"-Select-,Unknown,Other,2015,2014,2013,2012,2011,2010,2009,2008,2007,2006,2005,2004,2003,2002,2001,2000,1999,1998,1997,1996," +
									"1995,1994,1993,1992,1991,1990,1989,1988,1987,1986,1985,1984,1983,1982,1981,1980,1979,1978,1977,1976," +
									"1975,1974,1973,1972,1971,1970,1969,1968,1967,1966,1965,1964,1963,1962,1961,1960,1959,1958,1957,1956," +
									"1955,1954,1953,1952,1951,1950,1949,1948,1947,1946,1945,1944,1943,1942,1941,1940,1939,1938,1937,1936," +
									"1935,1934,1933,1932,1931,1930,1929,1928,1927,1926,1925,1924,1923,1922,1921,1920,1919,1918,1917,1916," +
									"1915,1914,1913,1912,1911,1910,1909,1908,1907,1906,1905,1904,1903,1902,1901,1900'," +
									"'0','200','Other','4','100','Spinner','','180','1','2','','Number','5'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("4")+"','','0','100','','0','0','CheckBox','','180','0','3','','','5'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("5")+"','-Select-,<1/12,1/12,2/12,3/12,4/12,5/12,6/12,7/12,8/12,9/12,10/12,11/12,12/12,13/12,14/12,Flat,Unknown','0','200','','0','0','Spinner','','180','1','4','','','5'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("6")+"','-Select-,Hip Roof,Gable Roof,Mansard Roof,Flat Roof,Shed Roof,Butterfly Roof,Other','0','150','Other','50','200','Spinner','','180','1','5','','','5'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("7")+"','-Select-,Conventional Frame,Wood Truss,Steel Truss,Concrete Frame,Structural,Steel Frame,Other','0','200','Other','50','150','Spinner','','180','1','6','','','5'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("8")+"','-Select-,>2001 FBC,1994 SFBC,Pre 2001 FBC,Pre 1994 SFBC,Not Applicable,Beyond Scope of Inspection','0','200','','0','0','Spinner','','180','1','7','','','5'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("9")+"','-Select-,Simple Roof Shape,Moderate Roof Shape,Complex Roof Shape','0','200','','0','0','Spinner','','180','1','8','','','5'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("10")+"','-Select-,Not Determined,Roof Aged,1 Year,2 Years,3 Years,4 Years,5 Years,6 Years,7 Years,8 Years,9 Years,>10 Years,Other','0','200','Other','3','100','Spinner','','180','1','9','','Number','5'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("11")+"','','0','100','','0','0','CheckBox','','180','0','10','','','5'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("14")+"','','0','100','','0','0','TextView','','30','0','0','','','5'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("1")+"','','0','100','','0','0','TextView','','120','0','1','','','5'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("2")+"','','0','100','','0','0','TextView','','140','0','2','','','5'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("3")+"','','0','100','','0','0','TextView','','125','0','3','','','5'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("4")+"','','0','100','','0','0','TextView','','160','0','4','','','5'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("5")+"','','0','100','','0','0','TextView','','100','0','5','','','5'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("6")+"','','0','100','','0','0','TextView','','120','0','6','','','5'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("7")+"','','0','100','','0','0','TextView','','130','0','7','','','5'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("8")+"','','0','100','','0','0','TextView','','110','0','8','','','5'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("9")+"','','0','100','','0','0','TextView','','125','0','9','','','5'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("10")+"','','0','100','','0','0','TextView','','110','0','10','','','5'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("11")+"','','0','100','','0','0','TextView','','100','0','11','','','5'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("13")+"','','0','100','','0','0','TextView','','120','0','13','','','5'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("12")+"','','0','160','','50','150','ImagePicker','','180','0','11','','','5'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("44")+"','','0','100','','0','0','ImagePicker','','110','0','12','','','5'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("23")+"','Yes-BVCN,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Comm_maxwidth+"','Yes-BVCN','250','0','Radio','BSI','"+tit_max_w+"','1','0','','','5'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("24")+"','Roof Flashing Property Anchored,Loose Section Noted,Roof Coping Mechanically Fastened,Maintenance Noted','2','190','','','0','CheckBox','','"+tit_max_w+"','1','1','','','5'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("25")+"','Yes,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Comm_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','2','','','5'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("26")+"','Yes,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Comm_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','3','','','5'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("27")+"','Yes-BVCN,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Comm_maxwidth+"','Yes-BVCN','250','0','Radio','BSI','"+tit_max_w+"','1','4','','','5'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("28")+"','Moderate,Severe,N/D,BSI,N/A','"+max_option_p_r+"','"+Comm_maxwidth+"','','','0','Radio','','"+tit_max_w+"','1','5','','','5'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("29")+"','Yes-BVCN,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Comm_maxwidth+"','Yes-BVCN','250','0','Radio','BSI','"+tit_max_w+"','1','6','','','5'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("30")+"','Yes,No,Unknown,N/D,BSI,N/A','"+max_option_p_r+"','"+Comm_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','7','','','5'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("31")+"','Yes,No,Unknown,N/D,BSI,N/A','"+max_option_p_r+"','"+Comm_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','8','','','5'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("32")+"','','5','150','','','0','EditText','','"+tit_max_w+"','1','9','Number','','5'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("33")+"','Yes,No,Unknown,N/D,BSI,N/A','"+max_option_p_r+"','"+Comm_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','10','','','5'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("34")+"','Yes,No,Unknown,N/D,BSI,N/A','"+max_option_p_r+"','"+Comm_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','11','','','5'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("36")+"','Yes,No,Unknown,N/D,BSI,N/A','"+max_option_p_r+"','"+Comm_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','12','','','5'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("35")+"','Yes,No,Unknown,N/D,BSI,N/A','"+max_option_p_r+"','"+Comm_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','13','','','5'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("37")+"','Yes,No,Unknown,N/D,BSI,N/A','"+max_option_p_r+"','"+Comm_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','14','','','5'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("38")+"','Yes,No,Unknown,N/D,BSI,N/A','"+max_option_p_r+"','"+Comm_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','15','','','5'" +
									
									" UNION SELECT  '"+(i++)+"','2','"+encode("1")+"','-Select-,Asphalt/Fiberglass Shingle,Concrete/Clay Tile,Metal,Built Up,Membrane,Other','0','200','Other','50','150','Spinner','','180','1','0','','','6'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("2")+"','','0','160','','0','0','DatePicker','','180','1','1','','','6'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("3")+"','" +
									"-Select-,Unknown,Other,2015,2014,2013,2012,2011,2010,2009,2008,2007,2006,2005,2004,2003,2002,2001,2000,1999,1998,1997,1996," +
									"1995,1994,1993,1992,1991,1990,1989,1988,1987,1986,1985,1984,1983,1982,1981,1980,1979,1978,1977,1976," +
									"1975,1974,1973,1972,1971,1970,1969,1968,1967,1966,1965,1964,1963,1962,1961,1960,1959,1958,1957,1956," +
									"1955,1954,1953,1952,1951,1950,1949,1948,1947,1946,1945,1944,1943,1942,1941,1940,1939,1938,1937,1936," +
									"1935,1934,1933,1932,1931,1930,1929,1928,1927,1926,1925,1924,1923,1922,1921,1920,1919,1918,1917,1916," +
									"1915,1914,1913,1912,1911,1910,1909,1908,1907,1906,1905,1904,1903,1902,1901,1900'," +
									"'0','200','Other','4','100','Spinner','','180','1','2','','Number','6'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("4")+"','','0','100','','0','0','CheckBox','','180','0','3','','','6'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("5")+"','-Select-,<1/12,1/12,2/12,3/12,4/12,5/12,6/12,7/12,8/12,9/12,10/12,11/12,12/12,13/12,14/12,Flat,Unknown','0','200','','0','0','Spinner','','180','1','4','','','6'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("6")+"','-Select-,Hip Roof,Gable Roof,Mansard Roof,Flat Roof,Shed Roof,Butterfly Roof,Other','0','150','Other','50','200','Spinner','','180','1','5','','','6'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("7")+"','-Select-,Conventional Frame,Wood Truss,Steel Truss,Concrete Frame,Structural,Steel Frame,Other','0','200','Other','50','150','Spinner','','180','1','6','','','6'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("8")+"','-Select-,>2001 FBC,1994 SFBC,Pre 2001 FBC,Pre 1994 SFBC,Not Applicable,Beyond Scope of Inspection','0','200','','0','0','Spinner','','180','1','7','','','6'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("9")+"','-Select-,Simple Roof Shape,Moderate Roof Shape,Complex Roof Shape','0','200','','0','0','Spinner','','180','1','8','','','6'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("10")+"','-Select-,Not Determined,Roof Aged,1 Year,2 Years,3 Years,4 Years,5 Years,6 Years,7 Years,8 Years,9 Years,>10 Years,Other','0','200','Other','3','100','Spinner','','180','1','9','','Number','6'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("11")+"','','0','100','','0','0','CheckBox','','180','0','10','','','6'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("14")+"','','0','100','','0','0','TextView','','30','0','0','','','6'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("1")+"','','0','100','','0','0','TextView','','120','0','1','','','6'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("2")+"','','0','100','','0','0','TextView','','140','0','2','','','6'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("3")+"','','0','100','','0','0','TextView','','125','0','3','','','6'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("4")+"','','0','100','','0','0','TextView','','160','0','4','','','6'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("5")+"','','0','100','','0','0','TextView','','100','0','5','','','6'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("6")+"','','0','100','','0','0','TextView','','120','0','6','','','6'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("7")+"','','0','100','','0','0','TextView','','130','0','7','','','6'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("8")+"','','0','100','','0','0','TextView','','110','0','8','','','6'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("9")+"','','0','100','','0','0','TextView','','125','0','9','','','6'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("10")+"','','0','100','','0','0','TextView','','110','0','10','','','6'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("11")+"','','0','100','','0','0','TextView','','100','0','11','','','6'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("13")+"','','0','100','','0','0','TextView','','120','0','13','','','6'" +
									" UNION SELECT  '"+(i++)+"','2','"+encode("12")+"','','0','160','','50','150','ImagePicker','','180','0','11','','','6'" +
									" UNION SELECT  '"+(i++)+"','3','"+encode("44")+"','','0','100','','0','0','ImagePicker','','110','0','12','','','6'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("23")+"','Yes-BVCN,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Comm_maxwidth+"','Yes-BVCN','250','0','Radio','BSI','"+tit_max_w+"','1','0','','','6'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("24")+"','Roof Flashing Property Anchored,Loose Section Noted,Roof Coping Mechanically Fastened,Maintenance Noted','2','190','','','0','CheckBox','','"+tit_max_w+"','1','1','','','6'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("25")+"','Yes,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Comm_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','2','','','6'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("26")+"','Yes,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Comm_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','3','','','6'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("27")+"','Yes-BVCN,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Comm_maxwidth+"','Yes-BVCN','250','0','Radio','BSI','"+tit_max_w+"','1','4','','','6'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("28")+"','Moderate,Severe,N/D,BSI,N/A','"+max_option_p_r+"','"+Comm_maxwidth+"','','','0','Radio','','"+tit_max_w+"','1','5','','','6'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("29")+"','Yes-BVCN,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Comm_maxwidth+"','Yes-BVCN','250','0','Radio','BSI','"+tit_max_w+"','1','6','','','6'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("30")+"','Yes,No,Unknown,N/D,BSI,N/A','"+max_option_p_r+"','"+Comm_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','7','','','6'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("31")+"','Yes,No,Unknown,N/D,BSI,N/A','"+max_option_p_r+"','"+Comm_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','8','','','6'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("32")+"','','5','150','','','0','EditText','','"+tit_max_w+"','1','9','Number','','6'" +

									" UNION SELECT  '"+(i++)+"','1','"+encode("33")+"','Yes,No,Unknown,N/D,BSI,N/A','"+max_option_p_r+"','"+Comm_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','10','','','6'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("34")+"','Yes,No,Unknown,N/D,BSI,N/A','"+max_option_p_r+"','"+Comm_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','11','','','6'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("35")+"','Yes,No,Unknown,N/D,BSI,N/A','"+max_option_p_r+"','"+Comm_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','13','','','6'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("36")+"','Yes,No,Unknown,N/D,BSI,N/A','"+max_option_p_r+"','"+Comm_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','12','','','6'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("37")+"','Yes,No,Unknown,N/D,BSI,N/A','"+max_option_p_r+"','"+Comm_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','14','','','6'" +
									" UNION SELECT  '"+(i++)+"','1','"+encode("38")+"','Yes,No,Unknown,N/D,BSI,N/A','"+max_option_p_r+"','"+Comm_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','15','','','6'"+
								
					" UNION SELECT  '"+(i++)+"','2','"+encode("1")+"','-Select-,Asphalt/Fiberglass Shingle,Concrete/Clay Tile,Metal,Built Up,Membrane,Other','0','200','Other','50','150','Spinner','','180','1','0','','','4'" +
					" UNION SELECT  '"+(i++)+"','2','"+encode("2")+"','','0','160','','0','0','DatePicker','','180','1','1','','','4'" +
					" UNION SELECT  '"+(i++)+"','2','"+encode("3")+"','"+
					"-Select-,Unknown,Other,2015,2014,2013,2012,2011,2010,2009,2008,2007,2006,2005,2004,2003,2002,2001,2000,1999,1998,1997,1996," +
					"1995,1994,1993,1992,1991,1990,1989,1988,1987,1986,1985,1984,1983,1982,1981,1980,1979,1978,1977,1976," +
					"1975,1974,1973,1972,1971,1970,1969,1968,1967,1966,1965,1964,1963,1962,1961,1960,1959,1958,1957,1956," +
					"1955,1954,1953,1952,1951,1950,1949,1948,1947,1946,1945,1944,1943,1942,1941,1940,1939,1938,1937,1936," +
					"1935,1934,1933,1932,1931,1930,1929,1928,1927,1926,1925,1924,1923,1922,1921,1920,1919,1918,1917,1916," +
					"1915,1914,1913,1912,1911,1910,1909,1908,1907,1906,1905,1904,1903,1902,1901,1900'," +
					"'0','200','Other','4','100','Spinner','','180','1','2','','Number','4'" +
					" UNION SELECT  '"+(i++)+"','2','"+encode("4")+"','','0','100','','0','0','CheckBox','','180','0','3','','','4'" +
					" UNION SELECT  '"+(i++)+"','2','"+encode("5")+"','-Select-,<1/12,1/12,2/12,3/12,4/12,5/12,6/12,7/12,8/12,9/12,10/12,11/12,12/12,13/12,14/12,Flat,Unknown','0','200','','0','0','Spinner','','180','1','4','','','4'" +
					" UNION SELECT  '"+(i++)+"','2','"+encode("6")+"','-Select-,Hip Roof,Gable Roof,Mansard Roof,Flat Roof,Shed Roof,Butterfly Roof,Other','0','150','Other','50','200','Spinner','','180','1','5','','','4'" +
					" UNION SELECT  '"+(i++)+"','2','"+encode("7")+"','-Select-,Conventional Frame,Wood Truss,Steel Truss,Concrete Frame,Structural,Steel Frame,Other','0','200','Other','50','150','Spinner','','180','1','6','','','4'" +
					" UNION SELECT  '"+(i++)+"','2','"+encode("8")+"','-Select-,>2001 FBC,1994 SFBC,Pre 2001 FBC,Pre 1994 SFBC,Not Applicable,Beyond Scope of Inspection','0','200','','0','0','Spinner','','180','1','7','','','4'" +
					" UNION SELECT  '"+(i++)+"','2','"+encode("9")+"','-Select-,Simple Roof Shape,Moderate Roof Shape,Complex Roof Shape','0','200','','0','0','Spinner','','180','1','8','','','4'" +
					" UNION SELECT  '"+(i++)+"','2','"+encode("10")+"','-Select-,Not Determined,Roof Aged,1 Year,2 Years,3 Years,4 Years,5 Years,6 Years,7 Years,8 Years,9 Years,>10 Years,Other','0','200','Other','4','100','Spinner','','180','1','9','','Number','4'" +
					" UNION SELECT  '"+(i++)+"','2','"+encode("11")+"','','0','100','','0','0','CheckBox','','180','0','10','','','4'" +
					" UNION SELECT  '"+(i++)+"','3','"+encode("14")+"','','0','100','','0','0','TextView','','30','0','0','','','4'" +
					" UNION SELECT  '"+(i++)+"','3','"+encode("1")+"','','0','100','','0','0','TextView','','120','0','1','','','4'" +
					" UNION SELECT  '"+(i++)+"','3','"+encode("2")+"','','0','100','','0','0','TextView','','140','0','2','','','4'" +
					" UNION SELECT  '"+(i++)+"','3','"+encode("3")+"','','0','100','','0','0','TextView','','125','0','3','','','4'" +
					" UNION SELECT  '"+(i++)+"','3','"+encode("4")+"','','0','100','','0','0','TextView','','160','0','4','','','4'" + 
					" UNION SELECT  '"+(i++)+"','3','"+encode("5")+"','','0','100','','0','0','TextView','','100','0','5','','','4'" +
					" UNION SELECT  '"+(i++)+"','3','"+encode("6")+"','','0','100','','0','0','TextView','','120','0','6','','','4'" +
					" UNION SELECT  '"+(i++)+"','3','"+encode("7")+"','','0','100','','0','0','TextView','','130','0','7','','','4'" +
					" UNION SELECT  '"+(i++)+"','3','"+encode("8")+"','','0','100','','0','0','TextView','','110','0','8','','','4'" +
					" UNION SELECT  '"+(i++)+"','3','"+encode("9")+"','','0','100','','0','0','TextView','','125','0','9','','','4'" +
					" UNION SELECT  '"+(i++)+"','3','"+encode("10")+"','','0','100','','0','0','TextView','','110','0','10','','','4'" +
					" UNION SELECT  '"+(i++)+"','3','"+encode("11")+"','','0','100','','0','0','TextView','','100','0','11','','','4'" +
					" UNION SELECT  '"+(i++)+"','3','"+encode("13")+"','','0','100','','0','0','TextView','','120','0','13','','','4'" +
					" UNION SELECT  '"+(i++)+"','2','"+encode("12")+"','','0','160','','50','150','ImagePicker','','180','0','11','','','4'" +
					" UNION SELECT  '"+(i++)+"','3','"+encode("44")+"','','0','100','','0','0','ImagePicker','','110','0','12','','','4'" +
					" UNION SELECT  '"+(i++)+"','1','"+encode("22")+"','Excellent,Good,Fair,Poor','"+max_option_p_r+"','"+Rdopt_maxwidth+"','Poor','250','0','Radio','','"+tit_max_w+"','1','0','','','4'" +
					" UNION SELECT  '"+(i++)+"','1','"+encode("15")+"','Yes,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Rdopt_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','1','','','4'" +
					" UNION SELECT  '"+(i++)+"','1','"+encode("16")+"','Yes,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Rdopt_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','2','','','4'" +
					" UNION SELECT  '"+(i++)+"','1','"+encode("17")+"','Yes,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Rdopt_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','3','','','4'" +
					" UNION SELECT  '"+(i++)+"','1','"+encode("18")+"','Yes,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Rdopt_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','4','','','4'" +
					" UNION SELECT  '"+(i++)+"','1','"+encode("19")+"','Yes,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Rdopt_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','5','','','4'" +
					" UNION SELECT  '"+(i++)+"','1','"+encode("20")+"','Yes,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Rdopt_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','6','','','4'" +
					" UNION SELECT  '"+(i++)+"','1','"+encode("21")+"','Yes,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Rdopt_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','7','','','4'"+
						
							" UNION SELECT  '"+(i++)+"','2','"+encode("1")+"','-Select-,Asphalt/Fiberglass Shingle,Concrete/Clay Tile,Metal,Built Up,Membrane,Other','0','200','Other','50','150','Spinner','','180','1','0','','','7'" +
							" UNION SELECT  '"+(i++)+"','2','"+encode("2")+"','','0','160','','0','0','DatePicker','','180','1','1','','','7'" +
							" UNION SELECT  '"+(i++)+"','2','"+encode("3")+"','"+
							"-Select-,Unknown,Other,2015,2014,2013,2012,2011,2010,2009,2008,2007,2006,2005,2004,2003,2002,2001,2000,1999,1998,1997,1996," +
							"1995,1994,1993,1992,1991,1990,1989,1988,1987,1986,1985,1984,1983,1982,1981,1980,1979,1978,1977,1976," +
							"1975,1974,1973,1972,1971,1970,1969,1968,1967,1966,1965,1964,1963,1962,1961,1960,1959,1958,1957,1956," +
							"1955,1954,1953,1952,1951,1950,1949,1948,1947,1946,1945,1944,1943,1942,1941,1940,1939,1938,1937,1936," +
							"1935,1934,1933,1932,1931,1930,1929,1928,1927,1926,1925,1924,1923,1922,1921,1920,1919,1918,1917,1916," +
							"1915,1914,1913,1912,1911,1910,1909,1908,1907,1906,1905,1904,1903,1902,1901,1900'," +
							"'0','200','Other','4','100','Spinner','','180','1','2','','Number','7'" +
							" UNION SELECT  '"+(i++)+"','2','"+encode("4")+"','','0','100','','0','0','CheckBox','','180','0','3','','','7'" +
							" UNION SELECT  '"+(i++)+"','2','"+encode("5")+"','-Select-,<1/12,1/12,2/12,3/12,4/12,5/12,6/12,7/12,8/12,9/12,10/12,11/12,12/12,13/12,14/12,Flat,Unknown','0','200','','0','0','Spinner','','180','1','4','','','7'" +
							" UNION SELECT  '"+(i++)+"','2','"+encode("6")+"','-Select-,Hip Roof,Gable Roof,Mansard Roof,Flat Roof,Shed Roof,Butterfly Roof,Other','0','150','Other','50','200','Spinner','','180','1','5','','','7'" +
							" UNION SELECT  '"+(i++)+"','2','"+encode("7")+"','-Select-,Conventional Frame,Wood Truss,Steel Truss,Concrete Frame,Structural,Steel Frame,Other','0','200','Other','50','150','Spinner','','180','1','6','','','7'" +
							" UNION SELECT  '"+(i++)+"','2','"+encode("8")+"','-Select-,>2001 FBC,1994 SFBC,Pre 2001 FBC,Pre 1994 SFBC,Not Applicable,Beyond Scope of Inspection','0','200','','0','0','Spinner','','180','1','7','','','7'" +
							" UNION SELECT  '"+(i++)+"','2','"+encode("9")+"','-Select-,Simple Roof Shape,Moderate Roof Shape,Complex Roof Shape','0','200','','0','0','Spinner','','180','1','8','','','7'" +
							" UNION SELECT  '"+(i++)+"','2','"+encode("10")+"','-Select-,Not Determined,Roof Aged,1 Year,2 Years,3 Years,4 Years,5 Years,6 Years,7 Years,8 Years,9 Years,>10 Years,Other','0','200','Other','3','100','Spinner','','180','1','9','','Number','7'" +
							" UNION SELECT  '"+(i++)+"','2','"+encode("11")+"','','0','100','','0','0','CheckBox','','180','0','10','','','7'" +
							" UNION SELECT  '"+(i++)+"','3','"+encode("14")+"','','0','100','','0','0','TextView','','30','0','0','','','7'" +
							" UNION SELECT  '"+(i++)+"','3','"+encode("1")+"','','0','100','','0','0','TextView','','120','0','1','','','7'" +
							" UNION SELECT  '"+(i++)+"','3','"+encode("2")+"','','0','100','','0','0','TextView','','140','0','2','','','7'" +
							" UNION SELECT  '"+(i++)+"','3','"+encode("3")+"','','0','100','','0','0','TextView','','125','0','3','','','7'" +
							" UNION SELECT  '"+(i++)+"','3','"+encode("4")+"','','0','100','','0','0','TextView','','160','0','4','','','7'" + 
							" UNION SELECT  '"+(i++)+"','3','"+encode("5")+"','','0','100','','0','0','TextView','','100','0','5','','','7'" +
							" UNION SELECT  '"+(i++)+"','3','"+encode("6")+"','','0','100','','0','0','TextView','','120','0','6','','','7'" +
							" UNION SELECT  '"+(i++)+"','3','"+encode("7")+"','','0','100','','0','0','TextView','','130','0','7','','','7'" +
							" UNION SELECT  '"+(i++)+"','3','"+encode("8")+"','','0','100','','0','0','TextView','','110','0','8','','','7'" +
							" UNION SELECT  '"+(i++)+"','3','"+encode("9")+"','','0','100','','0','0','TextView','','125','0','9','','','7'" +
							" UNION SELECT  '"+(i++)+"','3','"+encode("10")+"','','0','100','','0','0','TextView','','110','0','10','','','7'" +
							" UNION SELECT  '"+(i++)+"','3','"+encode("11")+"','','0','100','','0','0','TextView','','100','0','11','','','7'" +
							" UNION SELECT  '"+(i++)+"','3','"+encode("13")+"','','0','100','','0','0','TextView','','120','0','13','','','7'" +
							" UNION SELECT  '"+(i++)+"','2','"+encode("12")+"','','0','160','','50','150','ImagePicker','','180','0','11','','','7'" +
							" UNION SELECT  '"+(i++)+"','3','"+encode("44")+"','','0','100','','0','0','ImagePicker','','110','0','12','','','7'" +
							" UNION SELECT  '"+(i++)+"','1','"+encode("22")+"','Excellent,Good,Fair,Poor','"+max_option_p_r+"','"+Rdopt_maxwidth+"','Poor','250','0','Radio','','"+tit_max_w+"','1','0','','','7'" +
							" UNION SELECT  '"+(i++)+"','1','"+encode("15")+"','Yes,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Rdopt_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','1','','','7'" +
							" UNION SELECT  '"+(i++)+"','1','"+encode("16")+"','Yes,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Rdopt_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','2','','','7'" +
							" UNION SELECT  '"+(i++)+"','1','"+encode("17")+"','Yes,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Rdopt_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','3','','','7'" +
							" UNION SELECT  '"+(i++)+"','1','"+encode("18")+"','Yes,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Rdopt_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','4','','','7'" +
							" UNION SELECT  '"+(i++)+"','1','"+encode("19")+"','Yes,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Rdopt_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','5','','','7'" +
							" UNION SELECT  '"+(i++)+"','1','"+encode("20")+"','Yes,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Rdopt_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','6','','','7'" +
							" UNION SELECT  '"+(i++)+"','1','"+encode("21")+"','Yes,No,N/D,BSI,N/A','"+max_option_p_r+"','"+Rdopt_maxwidth+"','Yes','250','0','Radio','BSI','"+tit_max_w+"','1','7','','','7'");
									
								
								/*" UNION SELECT  '"+(i++)+"','2','"+encode("1")+"','-Select-,Asphalt/Fiberglass Shingle,Concrete/Clay Tile,Metal,Built Up,Membrane,Other','0','200','Other','50','150','Spinner','','180','1','0','','','4'" +
								" UNION SELECT  '"+(i++)+"','2','"+encode("2")+"','','0','160','','0','0','DatePicker','','180','1','1','','','4'" +
								" UNION SELECT  '"+(i++)+"','2','"+encode("3")+"','" +
								"-Select-,Unknown,Other,2013,2012,2011,2010,2009,2008,2007,2006,2005,2004,2003,2002,2001,2000,1999,1998,1997,1996," +
								"1995,1994,1993,1992,1991,1990,1989,1988,1987,1986,1985,1984,1983,1982,1981,1980,1979,1978,1977,1976," +
								"1975,1974,1973,1972,1971,1970,1969,1968,1967,1966,1965,1964,1963,1962,1961,1960,1959,1958,1957,1956," +
								"1955,1954,1953,1952,1951,1950,1949,1948,1947,1946,1945,1944,1943,1942,1941,1940,1939,1938,1937,1936," +
								"1935,1934,1933,1932,1931,1930,1929,1928,1927,1926,1925,1924,1923,1922,1921,1920,1919,1918,1917,1916," +
								"1915,1914,1913,1912,1911,1910,1909,1908,1907,1906,1905,1904,1903,1902,1901,1900'," +
								"'0','200','Other','4','100','Spinner','','180','1','2','','Number','4'" +
								" UNION SELECT  '"+(i++)+"','2','"+encode("4")+"','','0','100','','0','0','CheckBox','','180','0','3','','','4'" +
								" UNION SELECT  '"+(i++)+"','2','"+encode("5")+"','-Select-,<1/12,1/12,2/12,3/12,4/12,5/12,6/12,7/12,8/12,9/12,10/12,11/12,12/12,13/12,14/12,Flat,Unknown','0','200','','0','0','Spinner','','180','1','4','','','4'" +
								" UNION SELECT  '"+(i++)+"','2','"+encode("6")+"','-Select-,Hip Roof,Gable Roof,Mansard Roof,Flat Roof,Shed Roof,Butterfly Roof,Other','0','150','Other','50','200','Spinner','','180','1','5','','','4'" +
								" UNION SELECT  '"+(i++)+"','2','"+encode("7")+"','-Select-,Conventional Frame,Wood Truss,Steel Truss,Concrete Frame,Structural,Steel Frame,Other','0','200','Other','50','150','Spinner','','180','1','6','','','4'" +
								" UNION SELECT  '"+(i++)+"','2','"+encode("8")+"','-Select-,>2001 FBC,1994 SFBC,Pre 2001 FBC,Pre 1994 SFBC,N/A,BSI','0','200','','0','0','Spinner','','180','1','7','','','4'" +
								" UNION SELECT  '"+(i++)+"','2','"+encode("9")+"','-Select-,Simple Roof Shape,Moderate Roof Shape,Complex Roof Shape','0','200','','0','0','Spinner','','180','1','8','','','4'" +
								" UNION SELECT  '"+(i++)+"','2','"+encode("10")+"','-Select-,1 Year,2 Years,3 Years,4 Years,5 Years,6 Years,7 Years,8 Years,9 Years,>10 Years,Other','0','200','Other','0','0','Spinner','','180','1','9','','','4'" +
								" UNION SELECT  '"+(i++)+"','2','"+encode("11")+"','','0','100','','0','0','CheckBox','','180','0','10','','','4'" +
								" UNION SELECT  '"+(i++)+"','3','"+encode("14")+"','','0','100','','0','0','TextView','','30','0','0','','','4'" +
								" UNION SELECT  '"+(i++)+"','3','"+encode("1")+"','','0','100','','0','0','TextView','','120','0','1','','','4'" +
								" UNION SELECT  '"+(i++)+"','3','"+encode("2")+"','','0','100','','0','0','TextView','','140','0','2','','','4'" +
								" UNION SELECT  '"+(i++)+"','3','"+encode("3")+"','','0','100','','0','0','TextView','','125','0','3','','','4'" +
								" UNION SELECT  '"+(i++)+"','3','"+encode("4")+"','','0','100','','0','0','TextView','','160','0','4','','','4'" +
								" UNION SELECT  '"+(i++)+"','3','"+encode("5")+"','','0','100','','0','0','TextView','','100','0','5','','','4'" +
								" UNION SELECT  '"+(i++)+"','3','"+encode("6")+"','','0','100','','0','0','TextView','','120','0','6','','','4'" +
								" UNION SELECT  '"+(i++)+"','3','"+encode("7")+"','','0','100','','0','0','TextView','','130','0','7','','','4'" +
								" UNION SELECT  '"+(i++)+"','3','"+encode("8")+"','','0','100','','0','0','TextView','','110','0','8','','','4'" +
								" UNION SELECT  '"+(i++)+"','3','"+encode("9")+"','','0','100','','0','0','TextView','','125','0','9','','','4'" +
								" UNION SELECT  '"+(i++)+"','3','"+encode("10")+"','','0','100','','0','0','TextView','','110','0','10','','','4'" +
								" UNION SELECT  '"+(i++)+"','3','"+encode("11")+"','','0','100','','0','0','TextView','','100','0','11','','','4'" +
								" UNION SELECT  '"+(i++)+"','3','"+encode("13")+"','','0','100','','0','0','TextView','','120','0','13','','','4'" +
								" UNION SELECT  '"+(i++)+"','2','"+encode("12")+"','','0','160','','50','150','ImagePicker','','180','1','11','','','4'" +
								" UNION SELECT  '"+(i++)+"','3','"+encode("12")+"','','0','100','','0','0','ImagePicker','','110','0','12','','','4'" +
								" UNION SELECT  '"+(i++)+"','1','"+encode("23")+"','Yes-BVCN,No,N/D,BSI,N/A','3','"+Comm_maxwidth+"','Yes-BVCN','250','0','Radio','BSI','250','1','0','','','4'" +
								" UNION SELECT  '"+(i++)+"','1','"+encode("24")+"','Roof Flashing Property Anchored,Loose Section Noted,Roof Coping mechanically Fastened,Maintenance Noted','2','190','','','0','CheckBox','','250','1','1','','','4'" +
								" UNION SELECT  '"+(i++)+"','1','"+encode("25")+"','Yes,No,N/D,BSI,N/A','3','"+Comm_maxwidth+"','Yes','250','0','Radio','BSI','250','1','2','','','4'" +
								" UNION SELECT  '"+(i++)+"','1','"+encode("26")+"','Yes,No,N/D,BSI,N/A','3','"+Comm_maxwidth+"','Yes','250','0','Radio','BSI','250','1','3','','','4'" +
								" UNION SELECT  '"+(i++)+"','1','"+encode("27")+"','Yes-BVCN,No,N/D,BSI,N/A','3','"+Comm_maxwidth+"','Yes-BVCN','250','0','Radio','BSI','250','1','4','','','4'" +
								" UNION SELECT  '"+(i++)+"','1','"+encode("28")+"','Moderate,Severe,N/D,BSI,N/A','3','"+Comm_maxwidth+"','','','0','Radio','','250','1','5','','','4'" +
								" UNION SELECT  '"+(i++)+"','1','"+encode("29")+"','Yes-BVCN,No,N/A,N/D,BSI,N/A','3','"+Comm_maxwidth+"','Yes-BVCN','250','0','Radio','BSI','250','1','6','','','4'" +
								" UNION SELECT  '"+(i++)+"','1','"+encode("30")+"','Yes,No,Unknown,N/D,BSI,N/A','3','"+Comm_maxwidth+"','Yes','250','0','Radio','BSI','250','1','7','','','4'" +
								" UNION SELECT  '"+(i++)+"','1','"+encode("31")+"','Yes,No,Unknown,N/A,N/D,BSI,N/A','3','"+Comm_maxwidth+"','Yes','250','0','Radio','BSI','250','1','8','','','4'" +
								" UNION SELECT  '"+(i++)+"','1','"+encode("32")+"','','5','150','','','0','EditText','','250','1','9','Number','','4'" +
								" UNION SELECT  '"+(i++)+"','1','"+encode("33")+"','Yes,No,Unknown,N/A,N/D,BSI,N/A','3','"+Comm_maxwidth+"','Yes','250','0','Radio','BSI','250','1','10','','','4'" +
								" UNION SELECT  '"+(i++)+"','1','"+encode("34")+"','Yes,No,Unknown,N/A,N/D,BSI,N/A','3','"+Comm_maxwidth+"','Yes','250','0','Radio','BSI','250','1','11','','','4'" +
								" UNION SELECT  '"+(i++)+"','1','"+encode("35")+"','Yes,No,Unknown,N/A,N/D,BSI,N/A','3','"+Comm_maxwidth+"','Yes','250','0','Radio','BSI','250','1','12','','','4'" +
								" UNION SELECT  '"+(i++)+"','1','"+encode("36")+"','Yes,No,Unknown,N/A,N/D,BSI,N/A','3','"+Comm_maxwidth+"','Yes','250','0','Radio','BSI','250','1','13','','','4'" +
								" UNION SELECT  '"+(i++)+"','1','"+encode("37")+"','Yes,No,Unknown,N/A,N/D,BSI,N/A','3','"+Comm_maxwidth+"','Yes','250','0','Radio','BSI','250','1','14','','','4'" +
								" UNION SELECT  '"+(i++)+"','1','"+encode("38")+"','Yes,No,Unknown,N/A,N/D,BSI,N/A','3','"+Comm_maxwidth+"','Yes','250','0','Radio','BSI','250','1','15','','','4'");*/
						}
						if(c!=null)
							c.close();
						CreateARRTable(432);
						CreateARRTable(43);
						CreateARRTable(431);
						CreateARRTable(433);
						CreateARRTable(434);
						CreateARRTable(435);
				}
				catch (Exception e) {
					// TODO: handle exception
					System.out.println("you have prion value"+e.getMessage());
					
				}  

	            break;

    	case 44:
			/* GCH - SUMMARY OF HAZARDS & CONCERNS TABLE*/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ GCH_SummaryHazardstbl
						+ " (shzfId INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50),fld_shzchk Integer,fld_vicious varchar(50),fld_viciousytxt varchar(150),fld_horses varchar(50),fld_horseytxt varchar(150),fld_overhanging varchar(50),fld_trampoline varchar(50)," +
						"fld_skateboard vatchar(50),fld_bicycle varchar(50),fld_triphz varchar(50),fld_triphztxt varchar(50),fld_unsafe varchar(50),fld_porchdeck varchar(50),fld_nonconst varchar(50),fld_odoor varchar(50)," +
						"fld_openfound varchar(50),fld_wood varchar(50),fld_excessdb varchar(50),fld_bop varchar(50),fld_boptxt varchar(150),fld_bgdisrepair varchar(50),fld_visualpd varchar(50),fld_structp varchar(50)," +
						"fld_inoper varchar(50),fld_recentdw varchar(50),fld_possiblecdw varchar(50),fld_ownerccdw varchar(50),fld_nsssystem varchar(50),fld_nwsdedic varchar(50),fld_summaryhzcomments varchar(1000));");
			} catch (Exception e) {
				strerrorlog="Summary of Hazards & Concerns - GCH Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+ datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		case 45:
			/* GCH - FIRE PROTECTION/SURVEY TABLE*/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ GCH_FireProtecttbl
						+ " (fpsId INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50),fld_fpschk Integer,fld_fireext varchar(50),fld_firedet varchar(50),fld_size varchar(50),fld_type varchar(50),fld_wservice varchar(50),fld_taged varchar(50),fld_tagdate varchar(50),fld_smokedet varchar(50),"+
						"fld_smokoptions varchar(50),fld_ssys varchar(50),fld_scover varchar(50),fld_salarm varchar(50),fld_sign varchar(50));");
			} catch (Exception e) {
				strerrorlog="Fire Protection - GCH Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		case 46:
			/* GCH - SPECIAL HAZARDS TABLE*/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ GCH_SpecHazardstbl
						+ " (shzId INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50),fld_shzchk Integer,fld_fgases varchar(50),fld_fliquids varchar(50),fld_cliquids varchar(50),fld_cmetals varchar(50),fld_cchim varchar(50),fld_welding varchar(50),fld_spaint varchar(50),"+
						"fld_dip varchar(50),fld_exp varchar(50),fld_astorage varchar(50),fld_incin varchar(50),fld_ifovens varchar(50),fld_cgp varchar(50),fld_agfstorage varchar(50),fld_ufstorage varchar(50),fld_soilyrages varchar(50),fld_expdust varchar(50),fld_wworking varchar(50),"+
						"fld_hpiled varchar(50),fld_pbaler varchar(50),fld_ccook varchar(50));");
			} catch (Exception e) {
				strerrorlog="Special Hazards- GCH Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		case 47:
			/* GCH - HVAC TABLE*/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ GCH_Hvactbl
						+ " (hvId INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50),fld_hvchk Integer,fld_hvacsystem varchar(50),fld_type varchar(50),fld_fuel varchar(50),fld_air varchar(50),fld_condsatis varchar(50),fld_updated varchar(50),fld_sdlupd varchar(100),fld_sconf varchar(100),fld_age varchar(50),fld_appearsop varchar(50),"+
						"fld_other varchar(50),fld_comments varchar(600));");
			} catch (Exception e) {
				strerrorlog="HVAC- GCH Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		case 48:
			/* GCH - ELECTRIC TABLE*/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ GCH_Electrictbl
						+ " (eleId INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50),fld_elecchk Integer,fld_mdisconnect varchar(50),fld_mpamp varchar(50),fld_mpampotr varchar(50),fld_electype varchar(50),fld_voprotect varchar(50),fld_oehz varchar(50),fld_csatis varchar(50),fld_updated varchar(50),fld_age varchar(50),fld_comments varchar(600));");
			} catch (Exception e) {
				strerrorlog="HVAC- GCH Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		case 49:
			/* GCH - COMMENTS TABLE*/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ GCH_commenttbl
						+ " (cmtId INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50),gchcomment varchar(1000));");
			} catch (Exception e) {
				strerrorlog="Comment - GCH Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		case 50:
			/* ADDENDUM - COMMENTS TABLE*/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ Addendum
						+ " (adenId INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50),insp_typeid varchar(25),addendumcomment varchar(1000) default '');");
			} catch (Exception e) {
				strerrorlog="Comment - Addendum Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
	/*	case 50:
			 ADDENDUM - COMMENTS TABLE
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ Addendum
						+ " (adenId INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50),fourpointaddcomment varchar(1000) default ''," +
						"b1802addcomment varchar(1000) default '',commer1addcomment varchar(1000) default '',commer2addcomment varchar(1000) default ''," +
						"commer3addcomment varchar(1000) default '');");
			} catch (Exception e) {
				strerrorlog="Comment - Addendum Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;*/
		case 416:
			/* GCH - HOODDUCT TABLE*/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ GCH_HoodDuct
						+ " (hId INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50),fld_hoodduct_na varchar(25) DEFAULT '',fld_hoodandductwork varchar(600) DEFAULT '');");
			} catch (Exception e) {
				strerrorlog="Hood Duct - GCH Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of GCH - Hood Dust Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		case 417:
			/* GCH - FIRE PROTECTION TABLE*/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ GCH_FireProtection
						+ " (fId INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50),fld_fireprot_na varchar(25) DEFAULT '',fld_fireprotection varchar(600) DEFAULT '');");
			} catch (Exception e) {
				strerrorlog="Fire Protectoin - GCH Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of GCH - Fire Protectoin Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		case 418:
			/* GCH - MISCELLLANEOUS TABLE*/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ GCH_Misc
						+ " (mId INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50),fld_misc_na varchar(25) DEFAULT '',fld_trash varchar(100) DEFAULT '',fld_descroption varchar(100) DEFAULT '',fld_desccomments varchar(100) DEFAULT '',fld_soiled varchar(100) DEFAULT '');");
			} catch (Exception e) {
				strerrorlog="Miscellaneous - GCH Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of GCH - Miscellaneous Table Creation at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;

		case 51:
			/* SINK - SUMMARY TABLE*/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ SINK_Summarytbl
						+ " (sumId INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50),fld_plocated varchar(25),fld_aproperty varchar(25),fld_sumcomments varchar(1000),fld_hopresent Integer,fld_hoq1 varchar(25),fld_hoq2 varchar(25),fld_hoq3 varchar(25),fld_hoq4 varchar(25),fld_hoq5 varchar(25),fld_hoq6 varchar(25),fld_hoq7 varchar(25),fld_hocomments varchar(1000));");
			} catch (Exception e) {
				strerrorlog="SINK- Summary Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		case 52:
			/* SINK - OBSERVATION1 TABLE*/
			try {	
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
				         + SINK_Obs1tbl
				         + "(OB1Id INTEGER PRIMARY KEY Not null,fld_srid varchar(50),IrregularLandSurface varchar(50),IrregularLandSurfaceLocation varchar(250),IrregularLandSurfaceComments varchar(500),VisibleBuriedDebris varchar(50),VisibleBuriedDebrisLocation varchar(250),VisibleBuriedDebrisComments varchar(500),IrregularLandSurfaceAdjacent varchar(50),"+
				                  "IrregularLandSurfaceAdjacentLocation varchar(250),IrregularLandSurfaceAdjacentComments varchar(500),SoilCollapse varchar(50),SoilCollapseLocation varchar(250),SoilCollapseComments varchar(500),SoilErosionAroundFoundation varchar(50),SoilErosionAroundFoundationOption varchar(300),SoilErosionAroundFoundationComments varchar(500),"+
				                  "DrivewaysCracksNoted varchar(50),DrivewaysCracksOption varchar(200),DrivewaysCracksComments varchar(500),UpliftDrivewaysSurface varchar(50),UpliftDrivewaysSurfaceOption varchar(200),UpliftDrivewaysSurfaceComments varchar(500),LargeTree varchar(50),LargeTreeComments varchar(500),CypressTree varchar(50),CypressTreeLocation varchar(250),"+
				                  "CypressTreeComments varchar(500),LargeTreesRemoved varchar(50),LargeTreesRemovedLocation varchar(250),LargeTreesRemovedComments varchar(500),FoundationCrackNoted varchar(50),FoundationTypeofCrack varchar(200),FoundationLocation varchar(250),FoundationWidthofCrack  varchar(250),FoundationLengthofCrack varchar(100),"+
				                  "FoundationCleanliness varchar(250),FoundationProbableCause varchar(250),FoundationComments varchar(500),RetainingWallsServiceable varchar(100),RetainingWallsServiceOption varchar(250),RetainingWallsServiceComments varchar(500));");

			}catch (Exception e) {
				strerrorlog="SINK- Observation1 Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		case 53:
			/* SINK - OBSERVATION2 TABLE*/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
					      + SINK_Obs2tbl
					      + "(OB2Id INTEGER PRIMARY KEY Not null,fld_srid varchar(50),PoolDeckSlabNoted varchar(25),PoolDeckSlabComments varchar(500),PoolShellPlumbLevel varchar(25),PoolShellPlumbLevelComments varchar(500),ExcessiveSettlement varchar(25),ExcessiveSettlementTypeofCrack varchar(200),ExcessiveSettlementLocation varchar(250),ExcessiveSettlementWidthofCrack varchar(250),"+
					           "ExcessiveSettlementLengthofCrack varchar(100),ExcessiveSettlementCleanliness varchar(250),ExcessiveSettlementProbableCause varchar(250),ExcessiveSettlementComments varchar(500),WallLeaningNoted varchar(25),WallLeaningComments varchar(500),WallsVisiblyNotLevel varchar(25),WallsVisiblyNotLevelComments varchar(500),WallsVisiblyBulgingNoted varchar(25),"+
					           "WallsVisiblyBulgingComments varchar(500),OpeningsOutofSquare varchar(25),OpeningsOutofSquareComments varchar(500),DamagedFinishesNoted varchar(25),DamagedFinishesComments varchar(500),SeperationCrackNoted varchar(25),SeperationCrackTypeofCrack varchar(200),SeperationCrackComments varchar(500),ExteriorOpeningCracksNoted varchar(25),"+
					           "ExteriorOpeningCracksTypeofCrack varchar(200),ExteriorOpeningCracksLocation varchar(250),ExteriorOpeningCracksWidthofCrack varchar(250),ExteriorOpeningCracksLengthofCrack varchar(100),ExteriorOpeningCracksCleanliness varchar(250),ExteriorOpeningCracksProbableCause varchar(250),ExteriorOpeningCracksComments varchar(500),"+
					           "ObservationSettlementNoted varchar(100),ObservationSettlementTypeofCrack varchar (200),ObservationSettlementLocation varchar (250),ObservationSettlementWidthofCrack varchar (250),ObservationSettlementLengthofCrack varchar (100),ObservationSettlementCleanliness varchar (250),ObservationSettlementProbableCause varchar (250),"+
					           "ObservationSettlementComments varchar (500));");

			}catch (Exception e) {
				strerrorlog="SINK- Observation2 Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		case 54:
			/* SINK - OBSERVATION3 TABLE*/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ SINK_Obs3tbl
						+ "(OB3Id INTEGER PRIMARY KEY Not null,fld_srid varchar(50),InteriorWallCrackNoted varchar(25),InteriorWallCrackOption varchar(500),InteriorWallCrackComments varchar(500),WallSlabCrackNoted varchar(25),WallSlabCrackOption varchar(500),WallSlabCrackComments varchar(500),InteriorCeilingCrackNoted varchar(25),"+
						   "InteriorCeilingCrackOption varchar(500),InteriorCeilingCrackComments varchar(500),InteriorFloorCrackNoted  varchar(25),InteriorFloorCrackOption  varchar(500),InteriorFloorCrackComments varchar(500),WindowFrameOutofSquare varchar(25),WindowFrameOutofSquareOption varchar(500),"+
						   "WindowFrameOutofSquareComments varchar(500),FloorSlopingNoted varchar(25),FloorSlopingOption varchar(500),FloorSlopingComments varchar(500),CrackedGlazingNoted varchar(25),CrackedGlazingOption varchar(500),CrackedGlazingComments varchar(500),PreviousCorrectiveMeasureNoted varchar(25),"+
						   "PreviousCorrectiveMeasureOption varchar(500),PreviousCorrectiveMeasureComments varchar(500),BindingDoorOpeningNoted	varchar(25),BindingDoorOpeningOption varchar(500),BindingDoorOpeningComments varchar(500));");

			}catch (Exception e) {
				strerrorlog="SINK- Observation3 Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		case 55:
			/* SINK - OBSERVATION4 TABLE*/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ SINK_Obs4tbl
							+ "(OB4Id INTEGER PRIMARY KEY Not null,fld_srid varchar(50),CrawlSpacePresent varchar(20),CrawlSpaceComments varchar(500),StandingWaterNoted varchar(20),StandingWaterComments varchar(500),LeakingPlumbNoted varchar(20),LeakingPlumbComments  varchar(500),VentilationNoted varchar(20),VentilationComments  varchar(500),CrawlSpaceAccessible  varchar(20)," +
							"CrawlSpaceAccessibleComments  varchar(500),RoofSagNoted  varchar(20),RoofSagComments  varchar(500),RoofLeakageEvidence  varchar(20),RoofLeakageComments  varchar(500),DeferredMaintanenceIssuesNoted  varchar(20),DeferredMaintanenceComments  varchar(500),DeterioratedNoted  varchar(20),DeterioratedComments  varchar (500),"+
							"DeterioratedExteriorFinishesNoted  varchar(20),DeterioratedExteriorFinishesComments  varchar(500),DeterioratedExteriorSidingNoted  varchar(20),DeterioratedExteriorSidingComments  varchar(500),SafetyNoted  varchar(20),SafetyComments  varchar(500),OverallComments varchar(1000));");

			}catch (Exception e) {
				strerrorlog="SINK- Observation4 Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		case 56:
			/* SINK - OBSERVATION TREE TABLE*/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
					      + SINK_Obstreetbl
					      + "(OB1TId INTEGER PRIMARY KEY Not null,fld_srid varchar(50),Width varchar(100),Height varchar(100),Location varchar(300));");

			}catch (Exception e) {
				strerrorlog="SINK- Observation Tree Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		case 7:
			/* DRYWALL - SUMMARY CONDITIONS TABLE*/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ DRY_sumcond
						+ " (sumcondId INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50),presofsulfur varchar(25),obscopper varchar(25),presofdry varchar(25),addcomments varchar(500),"+
						  "drywallcomments varchar(1000),atticcomments varchar(1000),applianccomments varchar(1000),hvaccomments varchar(1000),"+
						  "assesmentcomments varchar(1000));");
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ DRY_home
						+ " (homeId INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50),home_pre varchar(5),home_assessment varchar(5),home_priorof_pre varchar(5),home_attorny varchar(5)," +
						"home_odor varchar(5),home_furnishing varchar(5),home_breakdown varchar(5),home_cleanevidence varchar(5),home_test_requ varchar(5),home_testing_iden varchar(5),home_signed varchar(5)," +
						"home_comments varchar(500));");
			} catch (Exception e) {
				strerrorlog="DRYWALL - SUMMARY CONDITIONS  Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		case 71:
			/* DRYWALL - ATTIC TABLE*/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ DRY_attic
						+ " (atticId INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50),atticnum INTEGER,Accesspoint varchar(50),Accesspointotr varchar(50),Locationsobserved varchar(500));");
			} catch (Exception e) {
				strerrorlog="DRYWALL - Attic  Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		case 72:
			/* DRYWALL - HVAC TABLE*/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ DRY_hvac
						+ " (hvacId INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50),hvacnum varchar(50),unitloc varchar(50),manufacturer varchar(50),unitacess varchar(50),"+
						   "recentrepairs varchar(50),coppercorrs varchar(50),darkstains varchar(50));");
			} catch (Exception e) {
				strerrorlog="DRYWALL - HVAC  Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		case 73:
			/* DRYWALL - APPLIANCE TABLE*/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ DRY_appliance
						+ " (appId INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50),appnum varchar(50),apptyp varchar(50),apptypotr varchar(50),unitacess varchar(50),obscorrosion varchar(50),"+
						   "recentrepairs varchar(50),coppercorrs varchar(50),darkstains varchar(50),appimg varchar(500));");
			} catch (Exception e) {
				strerrorlog="DRYWALL - APPLIANCE  Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		case 74:
			/* DRYWALL - ROOM TABLE*/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ DRY_room
						+ " (roomId INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50),roomnum Integer(1000),roomname varchar(50),rmnameotr varchar(50),outletsaccess varchar(50),outletseval varchar(50),"+
						   "switchaccess varchar(50),switcheval varchar(50),invasive varchar(50),sulfur varchar(50),confpresence varchar(50),"+
						   "obscopcorrosion varchar(50),obsmetcorrosion varchar(50),darkstains varchar(50));");
			} catch (Exception e) {
				strerrorlog="DRYWALL - ROOM  Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		case 22:
			/* ATTIC - TABLE*/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ Four_Electrical_Attic
						+ " (atcId INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50),atcinc varchar,atcoptions varchar(250),atccomments varchar(500)," +
						"rfdcinc varchar,rfdcmat varchar(200),rfstmat varchar(200),rfdcthi varchar(200),rfdcatt varchar(200),rftruss varchar(200),"+
						"nailspac varchar(200),missednails varchar(200),rfwallinc varchar,rtowopt varchar(200),nail3 varchar(100),"+
						"nail4 varchar(100),nail5 varchar(100),truss varchar(100),truss1 varchar(100),gablewallinc varchar,gablewallopt varchar(200),"+
						"atccondinc varchar,moisture varchar(500),dsheath varchar(500),droof varchar(500),nsalt varchar(500),"+
						"othertxt varchar(100),other varchar(500),Attic_Na varchar(5) default('false'));");
			} catch (Exception e) {
				strerrorlog="ATTIC -  Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		case 23:
			/** PLUMBING - TABLE **/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ Four_Electrical_Plumbing
						+ " (pluId INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50),wsinc varchar(10),wstyp varchar(100),wptyp varchar(200),"+
						   "wage varchar varchar(100),wyiconf varchar(100),mainshut varchar(100),wloc varchar(100),wconf varchar(200),"+
						   "wrupd varchar(100),wrupdy varchar(200),wdlupd varchar(100),wpconf varchar(100),"+
						   "ssinc varchar(10),sstype varchar(100),ptype varchar(200),sapproxage varchar(100),"+
						   "yiconf varchar(100),scpfound varchar(100),srupd varchar(100),sdlupd varchar(100),spconf varchar(100),slevi varchar(100),"+
						   "sonoted varchar(100),sflrv varchar(100),scomments varchar(250),nobinc varchar,tnob varachar(25),nobfc varchar(25),sencpre varchar(25),ptrepned varcahr(25),"+
						   "ocinc varchar(10),lrmpres varchar(100),dpuwasher varchar(100),lsnpre varchar(100),levi varchar(100),wafceil varchar(100),aleak varchar(100),inpleak varchar(100),clkcrack varchar(100),whleak varchar(100),other varchar(200),"+
						   "ovrplucondinc varchar(10),ovrplucondopt varchar(300),plucomments varchar(500),odinc varchar(10),whinc varchar(10));");
			} catch (Exception e) {
				strerrorlog="PLUMBING -  Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		case 231:
			/* PLUMBING - WATER HEATER TABLE*/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ Plumbing_waterheater
						+ " (pwhidId INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50),fld_flag varchar(10),whtrinc Integer,whtrtype varchar(100),"+
						  "whtrage varchar(100),whtrcap varchar(100),whtrloct varchar(100),whtrloc varchar(100),whtrdp varchar(100),whtrdl varchar(100),whtrle varchar(100),whtrimg varchar(500),whtrmodimg varchar(500));");
			} catch (Exception e) {
				strerrorlog="PLUMBING - WATER HEATER   Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		case 24:
			/* FOURPOINT - ELECTRICAL - GENERAL ELECTRICAL DETAILS TABLE*/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ Four_Electrical_General
						+ " (geId INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50),fld_inc Integer,electricalservice varchar(50),servicetype varchar(50),electricalmeter varchar(50),location varchar(50),locationotr varchar(50),servicemain varchar(50),servicemainotr varchar(50));");
			} catch (Exception e) {
				strerrorlog="FOURPOINT - ELECTRICAL GENERAL ELECTRICAL DETAILS  Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		case 242:
			/* FOURPOINT - ELECTRICAL - VISIBLE ELECTRICAL PANEL TABLE*/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ Four_Electrical_Panel
						+ " (vepId INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50),fld_inc Integer,visiblepanel varchar(50),type varchar(50));");
			} catch (Exception e) {
				strerrorlog="FOURPOINT - ELECTRICAL - ELECTRICAL PANEL  Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		case 243:
			/* FOURPOINT - ELECTRICAL - RECEPTACLES/OUTLETS TABLE*/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ Four_Electrical_Receptacles
						+ " (recpId INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50),fld_inc Integer,receptacle varchar(50),gfcireceptacle varchar(50));");
			} catch (Exception e) {
				strerrorlog="FOURPOINT - ELECTRICAL -RECEPTACLES/OUTLETS Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		case 244:
			/* FOURPOINT - ELECTRICAL - DETAILS TABLE*/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ Four_Electrical_Details
						+ " (detId INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50),fld_vbcinc Integer,vbc varchar(50),fld_lfrin Integer,"+
						   "lfr varchar(50),lfrotr varchar(50),fld_oesinc Integer,oes varchar(50),oescomment varchar(250),addchk varchar(25),addcomment varchar(1000));");
			} catch (Exception e) {
				strerrorlog="FOURPOINT - ELECTRICAL - DETAILS Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		case 245:
			/* FOURPOINT - ELECTRICAL - GENERAL OBSERVATION TABLE*/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ Four_Electrical_GO
						+ " (goId INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50),fld_goinc Integer,"+
						"haz varchar(50),hazyes varchar(200),haznocomt varchar(500),recp varchar(50),recpyescomt varchar(500),lconn varchar(50),"+
						"lconnyescomt varchar(500));");
			} catch (Exception e) {
				strerrorlog="FOURPOINT - ELECTRICAL -GENERAL OBSERVATION Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		case 246:
			/* FOURPOINT - ELECTRICAL - WIRING TYPE TABLE*/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ Four_Electrical_Wiring
						+ " (wirId INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50),fld_wirinc Integer,wirtyp varchar(50),aluminopt varchar(50),"+
						"aluminsubopt1 varchar(100),aluminsubopt2 varchar(100),aluminsubopt3 varchar(100));");
			} catch (Exception e) {
				strerrorlog="FOURPOINT - ELECTRICAL - WIRING TYPE Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		case 247:
			/* FOURPOINT - ELECTRICAL - MAIN PANEL TYPE TABLE*/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ Four_Electrical_MainPanel
						+ " (mplId INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50),fld_electinc Integer,mmpcond varchar(50),mptype varchar(50),"+
						"mptypopt varchar(50),agempl varchar(50),agemplotr varchar(50),yiconf varchar(50),mplocation varchar(50),mplocationotr varchar(50),"+
						"dslocation varchar(50),dslocationotr varchar(50),recupd varchar(50),recupdyes varchar(50),rectyp varchar(200),recdate varcahr(50),recpc varchar(50));");
			} catch (Exception e) {
				strerrorlog="FOURPOINT - MAIN PANEL TYPE Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		case 248:
			/* FOURPOINT - ELECTRICAL - SUB PANEL TYPE TABLE*/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ Four_Electrical_SubPanel
						+ " (splId INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50),fld_electinc Integer,spp varchar(50),nosub varchar(25));");
			} catch (Exception e) {
				strerrorlog="FOURPOINT - SUB PANEL TYPE Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		case 249:
			/* FOURPOINT - ELECTRICAL - SUB PANEL PRESENT TABLE*/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ Four_Electrical_SubPanelPresent
						+ " (sppId INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50),fld_flag varchar(10),splocation varchar(50),spamps varchar(25),sptype vrachar(200));");
				
			} catch (Exception e) {
				strerrorlog="FOURPOINT - SUB PANEL PRESENT Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		case 25:
			/* FOURPOINT - UNIT TABLE*/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ Four_HVAC_Unit
						+ " (huntId INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50),fld_flag varchar(10),unttype varchar(50),untnum varchar(50),approxage varchar(50),approxageother varchar(50),"+
						  "upgraded varchar(50),upgradedper varchar(50),hsystem varchar(100),hsystemotr varcahr(50),zone varchar(50),location varchar(50),overallcond varchar(50),"+
						  "ceiling varchar(50),ceilingother varchar(50),dripplan varchar(50),appimg varchar(500),cupgraded varchar(50),cupgradedper varchar(50));");
			} catch (Exception e) {
				strerrorlog="FOURPOINT - UNIT  Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		case 26:
			/* FOURPOINT - UNIT TABLE*/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ Four_HVAC
						+ " (hvacId INTEGER PRIMARY KEY AUTOINCREMENT,fld_srid varchar(50),fld_inchvac varchar,heatsource varchar(250),noofzones varchar(50),tankloc varchar(50),"+
						"distance varchar(50),openflame varchar(50),factor varchar(50),type varchar(50),typeotr varchar(50),porheater varchar(50),poroption varchar(200),"+
						"working varchar(50),wrkcomments varchar(250),hvaccomments varchar(1000),fld_ageofmainpanel varchar(50),fld_ageofmainpanelother varchar(50),fld_yearofinstconfdience varchar(50));");
			} catch (Exception e) {
				strerrorlog="FOURPOINT - HVAC  Table not created";
				Error_LogFile_Creation(strerrorlog+" "+" at "+ this.con.getClass().getName().toString()+" "+" at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
			break;
		case 355:
			try {

				arr_db.execSQL("CREATE TABLE IF NOT EXISTS " + Comm_Aux_Master
						+ " (AUXBUIILDID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
						+ "insp_typeid VARCHAR (50) NOT NULL,"
						+ "fld_srid VARCHAR (50) NOT NULL DEFAULT '',"
						+ "fld_aux_NA VARCHAR (50) NOT NULL DEFAULT '');");
			}
			catch (Exception e) {
				System.out.println("aux building "+e.getMessage());
			}
			
			
			break;
		case 11:
			/** cloud information **/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ cloud_tbl
						+ "(cid INTEGER PRIMARY KEY AUTOINCREMENT,SRID varchar(100),assignmentId 	REAL,assignmentMasterId REAL,inspectionPolicy_Id REAL,priorAssignmentId REAL"
						+ ",inspectionType varchar(100),assignmentType varchar(100),policyId varchar(100),policyVersion varchar(100),policyEndorsement varchar(100),policyEffectiveDate varchar(100),policyForm varchar(100),"
						+ "policySystem varchar(100),lob varchar(100),structureCount varchar(100),structureNumber varchar(100),structureDescription varchar(100),propertyAddress varchar(100),propertyAddress2 varchar(100),"
						+ "propertyCity varchar(100),propertyState varchar(100),propertyCounty varchar(100),propertyZip varchar(100),insuredFirstName varchar(100),insuredLastName varchar(100),insuredHomePhone varchar(100),insuredWorkPhone varchar(100),"
						+ "insuredAlternatePhone varchar(100),insuredEmail varchar(100),insuredMailingAddress varchar(100),insuredMailingAddress2 varchar(100),insuredMailingCity varchar(100),insuredMailingState varchar(100),insuredMailingZip varchar(100),"
						+ "agencyID varchar(100),agencyName varchar(100),agencyPhone varchar(100),agencyFax varchar(100),agencyEmail varchar(100),agencyPrincipalFirstName varchar(100),agencyPrincipalLastName varchar(100),agencyPrincipalEmail VARCHAR"
						+ ",agencyFEIN varchar(100),agencyMailingAddress varchar(100),agencyMailingAddress2 varchar(100),agencyMailingCity varchar(100),agencyMailingState varchar(100),agencyMailingZip varchar(100),agentID varchar(100),"
						+ "agentFirstName varchar(100),agentLastName varchar(100),agentEmail varchar(100),previousInspectionDate varchar(100),previousInspectionCompany varchar(100),previousInspectorFirstName varchar(100),"
						+ "previousInspectorLastName  varchar(100),previousInspectorLicense varchar(100),yearBuilt varchar(100),squareFootage varchar(100),numberOfStories varchar(100),construction varchar(100),roofCovering varchar(100),"
						+ "roofDeckAttachment varchar(100),roofWallAttachment varchar(100),roofShape varchar(100),secondaryWaterResistance varchar(100),openingCoverage varchar(100),buildingType VARCHAR(100));");

			} catch (Exception e) {
			
			}
			break;
		case 12:
			/** Questions original Mitigation information **/
			try {
				arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ quesorigmiti_tbl
						+ "(qid INTEGER PRIMARY KEY AUTOINCREMENT,SRID varchar(100),bcoriginal varchar(100),rcoriginal varchar(100),rdoriginal varchar(100),rworiginal varchar(100),rgoriginal varchar(100),swroriginal varchar(100),oporiginal varchar(100),wcoriginal varchar(100));");

			} catch (Exception e) {
			
			}
		break;
		case 10:
			try{arr_db.execSQL("CREATE TABLE IF NOT EXISTS "
					+ master_tbl +" (mst_id INTEGER PRIMARY KEY AUTOINCREMENT,mst_module_id VARCHAR( 4 ) DEFAULT ( '' ),mst_custom_id VARCHAR( 4 ) DEFAULT ( '' ),mst_text VARCHAR( 50 ) DEFAULT ( '' ))");
			Cursor c =SelectTablefunction(master_tbl, "");
				if(c.getCount()==0)
				{
					int i=1,j=1;
					
					arr_db.execSQL("Insert into "+master_tbl+" SELECT  '"+(i++)+"' as mst_id, '1' as mst_module_id,'"+(i++)+"' as mst_custom_id,'"+encode("Four Point Inspection")+"' as mst_text " +
					" UNION SELECT '"+(i++)+"','1','"+(i)+"','"+encode("Roof Survey")+"'" +
					" UNION SELECT '"+(i++)+"','1','"+(i)+"','"+encode("B1 1802 Inspection")+"'"+
					" UNION SELECT '"+(i++)+"','1','"+(i)+"','"+encode("Commercial Type I")+"'"+
					" UNION SELECT '"+(i++)+"','1','"+(i)+"','"+encode("Commercial Type II")+"'"+
					" UNION SELECT '"+(i++)+"','1','"+(i)+"','"+encode("Commercial Type III")+"'"+
					" UNION SELECT '"+(i++)+"','1','"+(i)+"','"+encode("General Conditions & Hazards")+"'"+
					" UNION SELECT '"+(i++)+"','1','"+(i)+"','"+encode("Sinkhole Inspection")+"'"+
					" UNION SELECT '"+(i++)+"','1','"+(i)+"','"+encode("Chinese Drywall")+"'" +
					
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Roof Covering Type")+"'" +
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Permit Application Date")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Year of Insallation")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("No Permit/Verification Documents")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Roof Slope")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Roof Shape (%)")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Roof Structure")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Building Code")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Roof Complexity")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Approximate Remaining Functional Life")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Predominant")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Upload Photo")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Edit/Delete")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("No")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Recent Repairs Noted")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Visible Signs of Leakage")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Visible Signs of Torn/Damaged Shingles")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Visible Signs of Curling/Brittle Shingles")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Visible Signs of Damaged Tiling/Slates")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Visible Damage to Roof Structure/Sheathing")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Other Damage Noted")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Overall Condition of Roof")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Has the roof mounted structure been adequately installed to prevent water seepage into the structure?")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Flashing/Coping")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("General roof appears condition satisfactory")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Roof leakage noted")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Roof ponding noted")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Has roof ever been replaced?")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Was old roof removed?")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Roof maintenance records available")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Is there a specific fund in place that is earmarked for roof replacement?")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Amount fund")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Is there any evidence of previous water damage?")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Is the roofing material extended up the parapet?")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Is there any evidence of streching of the membrane?")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Is there base flashing at parapet?")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Are all seams overlapped sufficiently to avoid seepage?")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Is there any damage to the roof structure (Cuts,Holes,etc)?")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Excessive Roof Aging Evident")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Approxiate Remaining Functional Life")+"'"+//3 Year Replacement Probability
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Roof Damage Noted")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Roof Repairs Noted")+"'"+
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Other Condition Present")+"'" +
					" UNION SELECT '"+(i++)+"','2','"+(j++)+"','"+encode("Photo")+"'");
					
					
				}
				if(c!=null)
					c.close();
			}catch(Exception e)
			{
				
			}


		break;


		}
	}
	public void showing_limit(String string, LinearLayout findViewById,
			LinearLayout findViewById2, TextView findViewById3, String string2) {
		// TODO Auto-generated method stub
		int length=string.toString().length();
		double par_width= findViewById.getWidth();
		double par_unit;
		if(par_width==0)
		{
			par_width=Double.parseDouble(string2); // SET THE DEFAULT VALUE FOR THE FIRST TIME LOADING 
		}
		
		par_unit=par_width/Double.parseDouble(string2);
		
		
		
		LinearLayout.LayoutParams mParam = new LinearLayout.LayoutParams((int)(length*par_unit),(int)(8));
		findViewById2.setLayoutParams(mParam);
		double totalle=(length*par_unit);
	    int percent=(int) ((length)*(100.00/Double.parseDouble(string2)));
	    if(length==Integer.parseInt(string2))
	    {
	    	LinearLayout.LayoutParams mParam1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT,(int)(8));
	    	findViewById2.setLayoutParams(mParam1);
	    	
	    	findViewById3.setText(Html.fromHtml("   "+percent+" % Exceed limit"));
	    	findViewById3.setTextColor(Color.RED);
	    }
	    else
	    {
	    	findViewById3.setText("   "+percent+" %");
	    }
	}
	 public String encode(String oldstring) {
			if(oldstring==null)
			{
				oldstring="";
			}
			try {
				oldstring = URLEncoder.encode(oldstring, "UTF-8");
				
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}
			return oldstring;

		}
	    public String decode(String newstring) {
			try {
				newstring = URLDecoder.decode(newstring, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return newstring;

		}
	 public void Error_LogFile_Creation(String strerrorlog2)
	    {
	        try {
	 			
	 			 DataOutputStream out = new DataOutputStream(this.con.openFileOutput("errorlogfile.txt", this.con.MODE_APPEND));
	 			 out.writeUTF(strerrorlog2);
	 			 
	    	 }
	    	 catch(Exception e)
	    	 {
	    		 
	    	 }
	    }
	public void gohome() {
		// TODO Auto-generated method stub
		this.con.startActivity(new Intent(con,HomeScreen.class));
	}
	 public Cursor SelectTablefunction(String tablename, String wherec) {
			Cursor cur = arr_db.rawQuery("select * from " + tablename + " " + wherec, null);
			return cur;

		}
	 public void ShowToast(String s, int i) {
		
		switch(i)
		{
		case 0:
			colorname="#000000";
			break;
		case 1:
			colorname="#890200";
			break;
		case 2:
			colorname="#F3C3C3";
			break;
		}
		toast = new Toast(this.con);
		 LayoutInflater inflater = (LayoutInflater)this.con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				 View layout = inflater.inflate(R.layout.toast, null);
		
		TextView tv = (TextView) layout.findViewById(
				R.id.text);
		tv.setTextColor(Color.parseColor(colorname));
		toast.setGravity(Gravity.CENTER, 0, 0);
		tv.setText(s);
		toast.setView(layout); 
		fireLongToast();
		
	}


public void ShowToast1(Spanned spanned, int i) {
	
	switch(i)
	{
	case 0:
		colorname="#000000";
		break;
	case 1:
		colorname="#890200";
		break;
	case 2:
		colorname="#F3C3C3";
		break;
	}
	 toast = new Toast(this.con);
	 LayoutInflater inflater = (LayoutInflater)this.con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			 View layout = inflater.inflate(R.layout.toast, null);
	
	TextView tv = (TextView) layout.findViewById(
			R.id.text);
	tv.setTextColor(Color.parseColor(colorname));
	toast.setGravity(Gravity.CENTER, 0, 0);
	tv.setText(spanned);
	toast.setView(layout);
	fireLongToast();
	

}
	private void fireLongToast() {

	        Thread t = new Thread() {
	            public void run() {
	                int count = 0;
	                try {
	                    while (true && count < 10) {
	                        toast.show();
	                        sleep(3);
	                        count++;

	                        // do some logic that breaks out of the while loop
	                    }
	                } catch (Exception e) {
	                   
	                }
	            }
	        };
	        t.start();
	    }
		public void getCalender() {
			// TODO Auto-generated method stub
			final Calendar c = Calendar.getInstance();
				mYear = c.get(Calendar.YEAR);
				mMonth = c.get(Calendar.MONTH);
				mDay = c.get(Calendar.DAY_OF_MONTH);
				
				mDayofWeek = c.get(Calendar.DAY_OF_WEEK);
				hours = c.get(Calendar.HOUR);
				minutes = c.get(Calendar.MINUTE);
				seconds = c.get(Calendar.SECOND);
				amorpm = c.get(Calendar.AM_PM);
		
		}
   public void showing_limit(String string,TextView tv,int max) {
			// TODO Auto-generated method stub
			int length=string.toString().length();
			
		    int percent=(int)((length)*(100.00/Double.parseDouble(String.valueOf(max))));
		    
		    if(length==max)
		    {
		    	tv.setText(Html.fromHtml("   "+percent+" % Exceed limit"));
		    	tv.setTextColor(Color.RED);
		    }
		    else
		    {
		    	tv.setText("   "+percent+" %");tv.setTextColor(Color.parseColor("#9900FF"));
		    }
		}
	public CharSequence Set_phoneno(String strphn) {
		// TODO Auto-generated method stub
		StringBuilder sVowelBuilder1 = new StringBuilder(strphn);
		sVowelBuilder1.deleteCharAt(0);
		sVowelBuilder1.deleteCharAt(3);
		sVowelBuilder1.deleteCharAt(6);
		strphn = sVowelBuilder1.toString();
	return strphn;
	}

	public String PhoneNo_validation(String decode) {
		// TODO Auto-generated method stub
		if (decode.length() == 13) {
			StringBuilder sVowelBuilder = new StringBuilder(decode);
			sVowelBuilder.insert(0, "(");
			sVowelBuilder.insert(4, ")");
			sVowelBuilder.insert(8, "-");
			newphone = sVowelBuilder.toString();
			return "Yes";
            
		} else {
			return "No";
		}
	}


	public void LimitShowing(String string, LinearLayout linearLayout,
			LinearLayout linearLayout2, TextView textView, String string2) {
		// TODO Auto-generated method stub
			int length=string.toString().length();
			double par_width= linearLayout.getWidth();
			double par_unit;
			if(par_width==0)
			{
				par_width=Double.parseDouble(string2); // SET THE DEFAULT VALUE FOR THE FIRST TIME LOADING 
			}
			
			par_unit=par_width/Double.parseDouble(string2);
			LinearLayout.LayoutParams mParam = new LinearLayout.LayoutParams((int)(length*par_unit),(int)(8));
			linearLayout2.setLayoutParams(mParam);
			double totalle=(length*par_unit);
		    int percent=(int) ((length)*(100.00/Double.parseDouble(string2)));
		    if(length==Integer.parseInt(string2))
		    {
		    	textView.setText("   "+percent+" % Exceed limit");
		    }
		    else
		    {
		    	textView.setText("   "+percent+" %");
		    }
	
	}
	public void setFocus(EditText etcomments_obs1_opt12) {
		// TODO Auto-generated method stub
		etcomments_obs1_opt12.setFocusableInTouchMode(true);
		etcomments_obs1_opt12.requestFocus();
		etcomments_obs1_opt12.setSelection(etcomments_obs1_opt12.getText().length()); //For set the cursor at the end of the selection
	}

	public Bitmap ShrinkBitmap(String file, int width, int height) {
		 Bitmap bitmap =null;
			try {
				BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
				bmpFactoryOptions.inJustDecodeBounds = true;
			    bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);

				int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight
						/ (float) height);
				int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth
						/ (float) width);
             
				if (heightRatio > 1 || widthRatio > 1) {
					if (heightRatio > widthRatio) {
						bmpFactoryOptions.inSampleSize = heightRatio;
					} else {
						bmpFactoryOptions.inSampleSize = widthRatio;
					}
				}

				bmpFactoryOptions.inJustDecodeBounds = false;
				bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
				return bitmap;
			} catch (Exception e) {
			
				return bitmap;
			}

		}
	public boolean common(String path) {
		File f = new File(path);
		if (f.length() < 2097152) {
			return true;
		} else {
			return false;
		}

	}


	public void goclass(int i) {


		// TODO Auto-generated method stub
		switch (i) {
		case 11:/** SCHEDULE **/
		      moveclass = genclassnames[0];	
			break;
		case 12:/** POLICY HOLDER INFORMATION **/
			  moveclass = genclassnames[1];	
			break;
		case 13:/** CALL ATTEMPT **/
			 moveclass = genclassnames[2];
			break;
		case 14:/** AGENT INFORMATION **/
			  moveclass = genclassnames[3];	
			break;
		case 15:/** ADDITIONAL INFORMATION **/
			  moveclass = genclassnames[4];	
			break;
		case 16:/** BUILDING INFORMATION **/
			  moveclass = genclassnames[5];	
			break;
		case 17:/** WEATHER **/
			  moveclass = genclassnames[6];	
			break;
		case 18:/** CLOUD **/
			moveclass = genclassnames[7];
			break;
		case 19:/** ADDENDUM **/
			moveclass = Addendum.class;
			break;
		case 20:/** ADDENDUM **/
			moveclass = WindAddendumcomm.class;
			break;
		case 21: /** ROOF - FOUR POINT **/			
			moveclass = fourclassnames[0];
			Roof=Roof_insp[0];
			break;
		case 22: /** ATTIC - FOUR POINT **/
			moveclass = fourclassnames[1];
			break;
		case 23: /** PLUMBING - FOUR POINT **/
			moveclass = fourclassnames[2];
			break;
		case 24: /** ELECTRICAL - FOUR POINT **/
			moveclass = fourclassnames[3];
			break;
		case 25: /** HVAC - FOUR POINT **/
			moveclass = fourclassnames[4];
			break;
		case 29: /** Roof - FOUR POINT **/			
			moveclass = Roof_section_common.class;
			Roof=Roof_insp[1];
			break;
		case 3:/** WIND MITIGATION **/
			moveclass = windclassnames[0];
			break;	
		case 301:/** WIND MITIGATION BUILDING CODE**/
			moveclass = windclassnames[0];
			break;	
		case 302:/** WIND MITIGATION ROOF COVER**/
			moveclass = windclassnames[2];
			break;	
		case 303:/** WIND MITIGATION ROOF DECK**/
			moveclass = windclassnames[3];
			break;	
		case 304:/** WIND MITIGATION ROOF WALL**/
			moveclass = windclassnames[4];
			break;		
		case 305:/** WIND MITIGATION ROOF GEOMETRY**/
			moveclass = windclassnames[5];
			break;	
		case 306:/** WIND MITIGATION SWR**/
			moveclass = windclassnames[6];
			break;	
		case 307:/** WIND MITIGATION OPENING PROTECTION **/
			moveclass = windclassnames[7];
			break;	
		case 308:/** WIND MITIGATION WALL CONSTRUCTION**/
			moveclass = windclassnames[8];
			break;
		case 56: /** Custom Inspection**/
			moveclass = CustomInspection.class;	
			break;
		case 41: /** GCH - POOL PRESENT **/
			 moveclass = gchclassnames[0];	
			break;
		case 42: /** GCH - POOL FENCE **/
			moveclass = gchclassnames[1];	
			break;
		case 43: /** GCH - Roof Section **/			
			moveclass = gchclassnames[3];
			Roof=Roof_insp[4];
			break;
		case 44: /** GCH - SUMMARY OF HAZARDS AND CONCERNS **/
			moveclass = gchclassnames[4];	
			break;
		case 45: /** GCH - FIRE PROTECTION AND SURVEY **/
			moveclass = gchclassnames[5];	
			break;
		case 46: /** GCH - SPECIAL HAZARDS**/
			moveclass = gchclassnames[6];	
			break;
		case 47: /** GCH - HVAC**/
			moveclass = gchclassnames[7];	
			break;
		case 48: /** GCH - ELECTRIC**/
			moveclass = gchclassnames[8];	
			break;
		case 49: /** GCH - COMMENTS**/
			moveclass = gchclassnames[9];	
			break;			
		case 416: /** GCH - HOOD DUCT**/
			moveclass = gchclassnames[10];	
			break;
		case 417: /** GCH - FIRE PROTECTION**/
			moveclass = gchclassnames[11];	
			break;
		case 418: /** GCH - MISCELLANEOUS**/
			moveclass = gchclassnames[12];	
			break;
		case 419: /** GCH - RESTAURANT SUPPLEMENT**/
			moveclass = gchclassnames[13];	
			break;

		case 51: /** SINKHOLE**/
			moveclass = sinkclassnames[0];	
			break;
		case 52: /** SINKHOLE - OBSERVATION1**/
			moveclass = sinkclassnames[1];	
			break;
		case 53: /** SINKHOLE - OBSERVATION2**/
			moveclass = sinkclassnames[2];	
			break;
		case 54: /** SINKHOLE - OBSERVATION3**/
			moveclass = sinkclassnames[3];	
			break;
		case 55: /** SINKHOLE - OBSERVATION4**/
			moveclass = sinkclassnames[4];	
			break;
		
		case 61: /** CHINESE DRYWALL **/
			moveclass= drywallclassnames[0];
			break;
		case 62: /** CHINESE DRYWALL - ROOM **/
			moveclass=drywallclassnames[1];
			break;
		case 63: /** CHINESE DRYWALL - HVAC **/
			moveclass=drywallclassnames[2];
			break;
		case 64: /** CHINESE DRYWALL - APPLIANCE **/
			moveclass=drywallclassnames[3];
			break;
		case 65: /** CHINESE DRYWALL - ATTIC **/
			moveclass=drywallclassnames[4];
			break;
		case 66: /** CHINESE DRYWALL - COMMENTS **/
			moveclass=drywallclassnames[5];
			break;
			
		case 6: /** SIGNATURE **/
			moveclass=Signature.class;
			break;
		case 7: /** PHOTOS **/
			moveclass=photos.class;
			break;
		case 8: /** FEEDBACK **/
			moveclass=Feedback.class;
			break;
		case 9: /** MAPS **/
			moveclass=Maps.class;
			break;
		case 10: /** SUBMIT **/
			moveclass=Submit.class;
			break;
		case 450:/** BI - GENERAL DATA **/			 
			moveclass = buildingclassnames[0];	
			break;
		case 451:/** BI - LOCATION **/	
			moveclass = buildingclassnames[1];	
			break;
		case 452:/** BI - OBSERVATIONS **/	
			moveclass = buildingclassnames[2];	
			break; 
		case 453:/** BI - ISO **/	
			moveclass = buildingclassnames[3];	
			break;
		case 454:/** BI - FOUNDATION**/	
			moveclass = buildingclassnames[4];	
			break; 
		case 455:/** BI - WALL STRUCTURE **/	
			moveclass = buildingclassnames[5];	
			break;
		case 456:/** BI - WALL CLADDING **/	
			moveclass = buildingclassnames[6];	
			break; 
		case 457:/** BI - BUILDING SEPERATION **/	
			moveclass = buildingclassnames[7];	
			break;
		case 458:/** BI - OVERALL COMMENTS **/	
			moveclass = buildingclassnames[8];	
			break; 
		case 459:/** WIND OPENINGS **/	
			moveclass = buildingclassnames[9];	
			break;
		case 460:/** DOOR OPENINGS **/	
			moveclass = buildingclassnames[10];	
			break; 
		case 461:/** SKYLIGHTS **/	
			moveclass = buildingclassnames[11];	
			break;
		default:
			break;
			
		
		}
		myintent = new Intent(this.con,moveclass); 
		myintent.putExtra("Roof", Roof);
		putExtraIntent(myintent);
		if(moveclass==RoofSection.class)
		{
			//showloadingalert(myintent);
		}
		else
		{
			(this.con).startActivity(myintent);
		}
	
	}
	boolean chk_InspTypeQuery(String selectedhomeid2, String string) {
		// TODO Auto-generated method stub
		try
		{  
		   Cursor IPT_retrive=SelectTablefunction(Select_insp, " where SI_srid='"+selectedhomeid2+"'");
		   if(IPT_retrive.getCount()>0)
		   {  
			   IPT_retrive.moveToFirst();
               selinsptype = decode(IPT_retrive.getString(IPT_retrive.getColumnIndex("SI_InspectionNames")));
               System.out.println("sleinsptype="+selinsptype);
               separated = selinsptype.split(",");
               for(int i=0;i<separated.length;i++)
			   {
				   if(separated[i].trim().equals(string))
				  {
					  isaccess=true;
					  return true;
				  }
			  }
              
			  return false;
		   }
		}
	    catch (Exception E)
		{
			String strerrorlog="Retrieving Inspectiontype data - Select Inspection table";
			Error_LogFile_Creation(strerrorlog+" "+" at "+ con+" "+" at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
		}
		 System.out.println("isacdes="+isaccess);
		return isaccess;
	}
	
	public String retrieve_radioname(RadioButton radioButton) {
		// TODO Auto-generated method stub
		return radioButton.getText().toString();
	}
	public void setRadioBtnValue(String rdval_ppf, RadioButton[] ppf_rd) {
		// TODO Auto-generated method stub
		if(!rdval_ppf.equals(""))
		{
			for(int i=0;i<ppf_rd.length;i++)
			{
				if(rdval_ppf.equals(ppf_rd[i].getText().toString()))
				{
					ppf_rd[i].setChecked(true);
					return;
				}
			}
			ppf_rd[ppf_rd.length-1].setChecked(true); // no matches found so we set the other text to true and pass the value to that edit text view 
			
			return;
		}
	}
	public String getslected_radio(RadioButton[] R) {
		// TODO Auto-generated method stub
		for(int i=0;i<R.length;i++)
		{
			//if(R[i].isChecked() && !R[i].getText().toString().equals("Other"))
				if(R[i].isChecked())
			{
				return R[i].getText().toString();
			}
		}
		return "";
	}
	public String getselected_chk(CheckBox[] CB) {
		// TODO Auto-generated method stub
		
		String s = "";
		for(int i=0;i<CB.length;i++)
		{
			if(CB[i].isChecked() && !CB[i].getText().toString().equals("Other"))
			{
				 s+=CB[i].getText().toString()+"&#94;";
			}
		} 
		if(s.length()>=2)
		{
			s=s.substring(0, s.length()-5);
		}
		return s;
	}
	

	public void setvaluechk1(String typechk_val, CheckBox[] typechk,
			EditText editText) {
		// TODO Auto-generated method stub
		if(!typechk_val.equals(""))
		{
			if(typechk_val.contains("&#40;"))
			{
				typechk_val=typechk_val.replace("&#40;", "&#94;");
			}
			else
			{
				typechk_val = typechk_val;
			}
			
			String k=typechk_val.replace("&#94;", "&#126;");
			String temp[]=k.split("&#126;");
			
			int j=0;
			do{
				for(int i=0;i<typechk.length;i++)
				{
					if(temp[j].trim().equals(typechk[i].getText().toString().trim()))
					{
						typechk[i].setChecked(true);
						j++;
						if(j==temp.length)
						{
							return;
						}
					}
					
				}
				if(j!=temp.length)
				{
					
					typechk[typechk.length-1].setChecked(true); // no matches found for the last value in the split text so we set the other to tru  
					editText.setVisibility(View.VISIBLE);
					editText.setText(temp[j]);
					j++;	
				}
			}while(j<temp.length);
		}
	}
	public void Set_UncheckBox(CheckBox[] obs6achkloc, EditText editText) {
		// TODO Auto-generated method stub
		for(int i=0;i<obs6achkloc.length;i++)
		{
			obs6achkloc[i].setChecked(false);
			 if(obs6achkloc[i].getText().toString().equals("Other"))
			 {
				 editText.setText("");
				 editText.setVisibility(v1.GONE);
			 }
		}
	}
	public void getInspectorId() {

		// TODO Auto-generated method stub
		try {
			CreateARRTable(1);
			Cursor cur = this.SelectTablefunction(this.inspectorlogin," where Fld_InspectorFlag=1");
			cur.moveToFirst();
			if (cur != null) {
				do {
					Insp_id = decode(cur.getString(cur.getColumnIndex("Fld_InspectorId")));
					
					Insp_firstname = decode(cur.getString(cur.getColumnIndex("Fld_InspectorFirstName")));
					Insp_lastname = decode(cur.getString(cur.getColumnIndex("Fld_InspectorLastName")));
					Insp_address = decode(cur.getString(cur.getColumnIndex("Fld_InspectorAddress")));
					Insp_companyname = decode(cur.getString(cur.getColumnIndex("Fld_InspectorCompanyName")));
					Insp_email = decode(cur.getString(cur.getColumnIndex("Fld_InspectorEmail")));
					
				} while (cur.moveToNext());
			}
			cur.close();
		} catch (Exception e) {System.out.println("erro"+e.getMessage());
			Error_LogFile_Creation(e.getMessage()+" "+" at "+ this.con.getClass().getName().toString()+" "+" in the stage of getting inspector details from Inspector Login table at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
		}
		/*Insp_id="2820";
		Insp_firstname="ANDREW";Insp_lastname="DULCIE";*/
	}
	public void MoveTo_HomeOwner(String string, int carrcit2, String string2, int i) {
		// TODO Auto-generated method stub
		
		if(carrcit2==0)
		{
			ShowToast("No records found in "+string2+" status for "+strret+".",0);
		}
		else
		{
			if(i==1)
			{
				Intent intent = new Intent(this.con,HomeOwnerList.class);
				intent.putExtra("InspectionType", string);
				intent.putExtra("status", string2);
				this.con.startActivity(intent);				
			}
			else
			{
				Intent intent = new Intent(this.con,DashboardList.class);
				intent.putExtra("InspectionType", string);
				intent.putExtra("status", string2);
				this.con.startActivity(intent);
			}
		}
	}
	public void MoveTo_OnlineList(String string, int onlcarrassign2,
			String string2, String string3, String string4) {
		// TODO Auto-generated method stub
		  	if(onlcarrassign2==0)
			{
				ShowToast("Sorry, No records found in "+string4+" status for "+strret+".",0);
			}
			else
			{
				Intent intent = new Intent(this.con,OnlineList.class);
				intent.putExtra("InspectionType", string);
				intent.putExtra("status", string2);
				intent.putExtra("substatus", string3);
				intent.putExtra("statusname", string4);
				this.con.startActivity(intent);
			}
	}
	public SoapObject Calling_WS1(String id, String string) throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException,SocketTimeoutException, XmlPullParserException {
		// TODO Auto-generated method stub
		
		SoapObject request = new SoapObject(NAMESPACE,string);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("inspectorid",id);
		envelope.setOutputSoapObject(request);
		System.out.println("the property ="+request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL_ARR);
		androidHttpTransport.call(NAMESPACE+string,envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		return result;
	}
	public void delete_all(String temp) {
		// TODO Auto-generated method stub
		
	}
	public void hidekeyboard()	
	{
		try
		{
			InputMethodManager imm = (InputMethodManager)con.getSystemService(Activity.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(((Activity) con).getCurrentFocus().getWindowToken(), 0);
		}
		catch (Exception e) {
			// TODO: handle exception
		}

	}
	public void hidekeyboard(EditText ed)	
	{
		try
		{
			
			((InputMethodManager) con.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(ed.getWindowToken(), 0);	
		}
		catch (Exception e) {
			// TODO: handle exception
		}

	}
	public void putExtraIntent(Intent intimg) {
		// TODO Auto-generated method stub
		intimg.putExtra("homeid", selectedhomeid);
		intimg.putExtra("InspectionType", onlinspectionid);
		intimg.putExtra("status", onlstatus);
		intimg.putExtra("identity", identityval);
		//intimg.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		
	}
	public void getExtras(Bundle extras) {
		// TODO Auto-generated method stub
		selectedhomeid =  extras.getString("homeid");
		onlinspectionid = extras.getString("InspectionType");
	    onlstatus = extras.getString("status");
	    identityval = extras.getInt("identity");
	}
	public void showpopup(final String string) {
		// TODO Auto-generated method stub
		AlertDialog.Builder alt_bld = new AlertDialog.Builder(con);
		getsingleinspectiotextname(string);
		alt_bld.setMessage("Do you want to include "+singleinspname+" ?")
		.setCancelable(false)
		.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int id) {
		// Action for 'Yes' Button
			if (string.equals("1"))
				insp_online = "13";
			else if (string.equals("2"))
				insp_online = "750";
			else if (string.equals("3"))
				insp_online = "28";
			else if (string.equals("4"))
				insp_online = "9";
			else if (string.equals("5"))
				//insp_online = "9";
				insp_online="62";
			else if (string.equals("6"))
				//insp_online = "9";
				insp_online="90";
			else if (string.equals("7"))
				insp_online = "11";
			else if (string.equals("8"))
				insp_online = "18";
			else if (string.equals("9"))
				insp_online = "751";
			try
			{
				getPolicyholderInformation(selectedhomeid);
				System.out.println("Maininso"+MainInspID+"string="+string);
				if(MainInspID.equals("9"))
				{
					if(string.equals("28"))
					{
						ShowToast("You cannot add B1802 Inspection as Additional Service for Commercial Insurance Category.", 0);
					}
					else
					{
						AddAdditionalservice(string);
					}
				}
				else if(MainInspID.equals("28"))
				{
					if(string.equals("4") || string.equals("5") || string.equals("6"))
					{
						ShowToast("You cannot add Commercial Type Inspection as Additional Service for Residential Insurance Category.", 0);
					}
					else
					{
						AddAdditionalservice(string);
					}
				}
				else
				{
					AddAdditionalservice(string);
				}
				
				/***Need to add gowri sys ends***/
			}catch (Exception e) {
				// TODO: handle exception
				System.out.println("The problem in the inserting the selected inspection"+e.getMessage());
			}
		}
		})
		.setNegativeButton("No", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int id) {
		//  Action for 'NO' Button
			if(string.equals("3") || string.equals("4") || string.equals("5") || string.equals("6"))
			{
				goclass(3);
			}

		dialog.cancel();
		}
		});
		 AlertDialog alert = alt_bld.create();
		// Title for AlertDialog
		alert.setTitle("This inspection type was not ordered.");
		// Icon for AlertDialog
		alert.setIcon(R.drawable.alertmsg);
		alert.show();

	}
	private void AddAdditionalservice(final String string)
	{
		if(isInternetOn())
		{
			String source = "<font color=#FFFFFF>Loading... Please wait..."+ "</font>";
			pd2 = ProgressDialog.show(con,"", Html.fromHtml(source), true);
	        new Thread(new Runnable() {
	                public void run() {
	                	Looper.prepare();
	                	try {
	                		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
	            			envelope.dotNet = true;
	            			SoapObject ad_property1 = new SoapObject(NAMESPACE,"AddAdditionalService");
	            			
	            			ad_property1.addProperty("InspectorID", Insp_id);
	            			ad_property1.addProperty("AddiInspId", string);
	            			ad_property1.addProperty("SRID", selectedhomeid);
	            			System.out.println("ad_property1="+ad_property1);
	            			envelope.setOutputSoapObject(ad_property1);
	            			HttpTransportSE androidHttpTransport1 = new HttpTransportSE(URL_ARR);
	            			androidHttpTransport1.call(NAMESPACE+"AddAdditionalService", envelope);
	            			String result  = envelope.getResponse().toString();
	            			System.out.println("resultaddservice="+result);
	                		
	            			if(result.equals("true"))
	            			{
	            				Cursor SI_c=SelectTablefunction(Select_insp, " WHERE SI_srid ='"+selectedhomeid+"'");
		        				if(SI_c.getCount()>0)
		        				{
		        					selinsptype += ","+string;
		        					System.out.println("sele"+selinsptype);
		        					
		        					arr_db.execSQL(" UPDATE  "+Select_insp+" SET SI_InspectionNames='"+encode(selinsptype)+"' WHERE SI_srid='"+selectedhomeid+"'");
		        					
		        					show_handler=0;
			            			inspectionname = string;
		        				}
		        				System.out.println("came still here"+inspectionname);
		        				
	            			}
	            			handler.sendEmptyMessage(0);
	            		} catch (Exception e) {
	            			// TODO Auto-generated catch block
	            			System.out.println("EE"+e.getMessage());
	            			show_handler=2;
	            			handler.sendEmptyMessage(0);
	            		}                
	                }
	            }).start();	
		}
		else
		{
			ShowToast("Internet Connection not available.", 0);
		}
	}
	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			pd2.dismiss();
			if (show_handler == 0) 
			{
				ShowToast("Additional service added successfully.",0);	
				System.out.println("show_handler="+inspectionname);
				if(inspectionname.equals("18"))
				{
					goclass(51);
				}
				else if(inspectionname.equals("751"))
				{
					goclass(61);
				}
				else if(inspectionname.equals("750"))
				{
					goclass(29);
				}
				else if(inspectionname.equals("11"))
				{
					goclass(41);
				}
				else if(inspectionname.equals("13"))
				{
					goclass(21);
				}
				else if(inspectionname.equals("28"))
				{
					gotoclass(31,WindMitBuildCode.class);
				}
				else if(inspectionname.equals("9"))
				{
					getPolicyholderInformation(selectedhomeid);
					System.out.println("what is stories="+Stories);
					if(Stories.equals("1") || Stories.equals("2")  || Stories.equals("3"))
					{
						getcommercialbno(1,32,"Commercial Type I");	
					}
					else if(Stories.equals("4") || Stories.equals("5")  || Stories.equals("6"))
					{
						getcommercialbno(2,33,"Commercial Type II");					
					}
					else 
					{
						getcommercialbno(3,34,"Commercial Type III");	
					}
				}
			}
			else if (show_handler == 2)
			{
				ShowToast("You have problem in the server. Please try again later",0); 
			}
		}
	};
	private int ressupchk;
	
	public void setupUI(View findViewById) {
		// TODO Auto-generated method stub
		//Set up touch listener for non-text box views to hide keyboard.
	    if(!(findViewById instanceof EditText)) {

	    	findViewById.setOnTouchListener(new OnTouchListener() {
	    		@Override
	            public boolean onTouch(View v, MotionEvent event) {
	               
	               View cur_f=((Activity) con).getWindow().getCurrentFocus();
	               if(cur_f!=null)
	               {
	            	   if(cur_f instanceof EditText)
	            	   {
	            		   hidekeyboard();
	            		   cur_f.clearFocus();
	            		   cur_f.setFocusable(false);
	            		   cur_f.setFocusableInTouchMode(false);
	            	   }
	               }
	                return false;
	            }

	        });
	    }
	    else
	    {
	    	
	    	
	    	((EditText) findViewById).setOnTouchListener(new Touch_Listener(0));
	    }

	    //If a layout container, iterate over children and seed recursion.
	    if (findViewById instanceof ViewGroup) {

	        for (int i = 0; i < ((ViewGroup) findViewById).getChildCount(); i++) {

	            View innerView = ((ViewGroup) findViewById).getChildAt(i);
	            
	            setupUI(innerView);
	        }
	    }

	}

	public void getcommercialbno(final int i, int typeid, String inspname) {
		// TODO Auto-generated method stub
		CreateARRTable(70);
		getcommercialbuilno(CommercialFlag);
		selecttbl(typeid);
		Cursor c1= arr_db.rawQuery("select * from "+Comm_Building, null);
		if(c1.getCount()==0)
		{
			System.out.println("buildin");
			showenterbuildnopopup(i,typeid,CommercialFlag,c1.getCount());
		}
/*		else if((inspname.equals("Commercial Type I") && (buildingno1!=0 && chkcomm1(buildingno1,CommercialFlag)==true) && (comm1_buildno.equals("") && chkrecordssavedcomm1(selectedhomeid,32,2)==false))
				|| (inspname.equals("Commercial Type II") && (buildingno1!=0 && chkcomm2(buildingno1,CommercialFlag)==true) && (comm1_buildno.equals("") && chkrecordssavedcomm2(selectedhomeid,33,2)==false))
				|| (inspname.equals("Commercial Type III") && (buildingno1!=0 && chkcomm3(buildingno1,CommercialFlag)==true) && (comm1_buildno.equals("") && chkrecordssavedcomm3(selectedhomeid,34,2)==false)))*/
		else if((inspname.equals("Commercial Type I") && (buildingno1!=0 && chkcomm1(buildingno1,CommercialFlag)==true) && (chkrecordssavedcomm1(selectedhomeid,32,2)==false))
					|| (inspname.equals("Commercial Type II") && (buildingno1!=0 && chkcomm2(buildingno1,CommercialFlag)==true) && (chkrecordssavedcomm2(selectedhomeid,33,2)==false))
					|| (inspname.equals("Commercial Type III") && (buildingno1!=0 && chkcomm3(buildingno1,CommercialFlag)==true) && (chkrecordssavedcomm3(selectedhomeid,34,2)==false)))

		{
			System.out.println("0000");
			showduplicatepopup(inspname+",",buildingno1,typeid,CommercialFlag);
		}
		else
		{
			System.out.println("comm"+comm1_buildno);
			/*if((inspname.equals("Commercial Type I") && (buildingno1!=0 && chkcomm1(buildingno1,CommercialFlag)==false) && (comm1_buildno.equals("") || chkrecordssavedcomm1(selectedhomeid,32,2)==false))
					|| (inspname.equals("Commercial Type II") && (buildingno2!=0 && chkcomm2(buildingno1,CommercialFlag)==false) && (comm1_buildno.equals("") || chkrecordssavedcomm2(selectedhomeid,33,2)==false))
					|| (inspname.equals("Commercial Type III") && (buildingno3!=0 && chkcomm3(buildingno1,CommercialFlag)==false) && (comm1_buildno.equals("") || chkrecordssavedcomm3(selectedhomeid,34,2)==false)))*/
		/*	if((inspname.equals("Commercial Type I") && (buildingno1!=0 && chkcomm1(buildingno1,CommercialFlag)==false))
						|| (inspname.equals("Commercial Type II") && (buildingno2!=0 && chkcomm2(buildingno1,CommercialFlag)==false))
						|| (inspname.equals("Commercial Type III") && (buildingno3!=0 && chkcomm3(buildingno1,CommercialFlag)==false)))
			{				
				System.out.println("ddf");
				showenterbuildnopopup(i,typeid,CommercialFlag,c1.getCount());
			}

			else*/ if(!comm1_buildno.equals("") && inspname.equals("Commercial Type I"))
			{
				System.out.println("1111");
				if(i==1){gotoclass(32,WindMitBuildCode.class);}	
			}
			else if(!comm1_buildno.equals("") && inspname.equals("Commercial Type II"))
			{
				System.out.println("222");
				if(i==2){gotoclass(33,CommercialWind.class);}
			}
			else if(!comm1_buildno.equals("") && inspname.equals("Commercial Type III"))
			{
				System.out.println("333");
				if(i==3){gotoclass(34,CommercialWind.class);}
			}
			else
			{
				System.out.println("444");
				showenterbuildnopopup(i,typeid,CommercialFlag,c1.getCount());
			}

		}
		
	}

	public void showduplicatepopup(String inspname, int buildingno1, final int i,final String flag) 
	{
			CreateARRTable(70);
	 		getbuildingno(flag);
	 		String arr1[] = bno1.split("~");
 			arrspnsrid1=new String[arr1.length];	
 			for(int k=0;k<arr1.length;k++)
 			{
 					Cursor c1 =arr_db.rawQuery("select * from  "+Comm_Building+" where BUILDING_NO='"+arr1[k]+"' and Commercial_Flag='"+flag+"'", null);
 					if(c1.getCount()>0)
 					{
 						c1.moveToFirst();
 						arrspnsrid1[k] = c1.getString(c1.getColumnIndex("fld_srid"));System.out.println("ts="+arrspnsrid1[k]);
 						if(chkrecordscommonsave(arrspnsrid1[k], i,1)==true)
 						{
 							appendbno1 += c1.getString(c1.getColumnIndex("BUILDING_NO"))+"~";
 							
 						}
 					 }
 			}System.out.println("append="+appendbno1);
 			appendbno1 = "--Select--"+"~"+appendbno1;
 			String arrbnospn1[] = appendbno1.split("~"); System.out.println("arrbnospn1="+arrbnospn1.length);
 			if(arrbnospn1.length>=1)
	 		{
	 				final Dialog dialog1 = new Dialog(this.con,android.R.style.Theme_Translucent_NoTitleBar);
	 				dialog1.getWindow().setContentView(R.layout.alertcomm);
	 				
	 				Button btn_yes = (Button) dialog1.findViewById(R.id.yes);
	 		 		Button btn_cancel = (Button) dialog1.findViewById(R.id.cancel);
	 		 		ImageView btn_help = (ImageView) dialog1.findViewById(R.id.helpclose);
	 		 		e1 = (EditText) dialog1.findViewById(R.id.buildingno);
	 		 		
	 		 		Cursor c1 = arr_db.rawQuery("select * from " + Comm_Building + " where fld_srid='"+selectedhomeid+"'", null);
 					if(c1.getCount()>0)
 					{
 						c1.moveToFirst();
 						String bno = c1.getString(c1.getColumnIndex("BUILDING_NO"));
 						e1.setText(bno);
 						e1.setEnabled(false);
 					}
 					else
 					{
 						e1.setEnabled(true);
 					}
 					
	 		 		
	 		 		
	 		 		spn1=(Spinner) dialog1.findViewById(R.id.sp1);	
	  	 			ArrayAdapter ad1 =new ArrayAdapter(this.con,android.R.layout.simple_spinner_item, arrbnospn1);
	 	 			ad1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	 	 			spn1.setAdapter(ad1);	
	 	 			spn1.setOnItemSelectedListener(new  Spin_Selectedlistener(1));
	 	 			
	 	 			btn_cancel.setOnClickListener(new OnClickListener()
	 				{
	 					@Override
	 					public void onClick(View arg0) 
	 	         		{
	 						bno1="";
	 						appendbno1="";
	 						
	 						dialog1.cancel();
	 						if(submenu_layout!=null){submenu_layout.removeAllViews();}
	 	         		}
	 				});
	 	 			
	 	 			btn_help.setOnClickListener(new OnClickListener()
	 				{
	 					@Override
	 					public void onClick(View arg0) 
	 	         		{
	 						bno1="";
	 						appendbno1="";
	 						
	 						dialog1.cancel();
	 						if(submenu_layout!=null){submenu_layout.removeAllViews();}
	 	         		}
	 				});
	 	 			
	 	 			
	 	 			btn_yes.setOnClickListener(new OnClickListener()
	 		 		{
	 		 			@Override
	 		 			public void onClick(View arg0) 

	 		           	{
	 		 				if(!e1.getText().toString().trim().equals("") 
	 		 						//&& noofbuildingvalidation(e1.getText().toString().trim())== true 
	 		 						&& buildingnovalidation(e1.getText().toString().trim())!="zero")
	 		 						//&& CheckDupBNo(e1.getText().toString().trim(),flag)==true)
	 		 				{
	 		 					
	 		 					if(comm1txt.equals("--Select--"))
	 		 					{
	 		 						insertbuildingno(e1.getText().toString(),flag,i);
	 		 					}
	 		 					else
	 		 					{

	 		 		 				
		 		 					System.out.println("dfdsfsfdsfdsfsdfsdfsdfs");
		 		 					
		 		 						Cursor c1 = arr_db.rawQuery("select * from " + Comm_Building + " where BUILDING_NO='"+comm1txt+"'", null);
		 			 					if(c1.getCount()>0)
		 			 					{
		 			 						c1.moveToFirst();
		 			 						srid = c1.getString(c1.getColumnIndex("fld_srid"));						
		 			 					}
		 			 						AlertDialog.Builder builder = new AlertDialog.Builder(con);
		 			 						builder.setMessage("Are you sure, Do you want to duplicate the Building Number Details.?").setTitle("Confirmation")
		 			 								.setIcon(R.drawable.alertmsg)
		 			 								.setCancelable(false)
		 			 								.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
		 			 									public void onClick(DialogInterface dialog,int id) 
		 			 									{
		 			 										if(i==32)
		 			 										{
		 			 											try
		 			 											{
				 			 											Cursor c2 = arr_db.rawQuery("select * from " + Comm_Building +" Where BUILDING_NO='"+comm1txt+"' and Commercial_Flag='"+flag+"'", null);	
				 			 											if(c2.getCount()>0)
				 			 											{
				 			 												c2.moveToFirst();
				 			 												String homeid = c2.getString(c2.getColumnIndex("fld_srid"));
				 			 												Cursor c1=SelectTablefunction(Wind_Questiontbl, " where fld_srid='"+homeid+"'");
				 			 												if(c1.getCount()>0)
				 			 												{
				 			 													c1.moveToFirst();
				 			 													String buildcode=c1.getString(3);
				 			 													String yearbuilt=c1.getString(4);
				 			 													String permitdate=c1.getString(5);
				 			 													String roofdeck=c1.getString(6);
				 			 													String rdother=c1.getString(7);
				 			 													String rwall=c1.getString(8);
				 			 													String rsubwall=c1.getString(9);
				 			 													String rclips=c1.getString(10);
				 			 													String rcsingle=c1.getString(11);
				 			 													String rcdouble=c1.getString(12);
				 			 													String rwother=c1.getString(13);
				 			 													String rgeovalue=c1.getString(14);
				 			 													String rgeolength=c1.getString(15);
				 			 													String rgeoarea=c1.getString(16);
				 			 													String rgeoother=c1.getString(17);
				 			 													String swr=c1.getString(18);
				 			 													String openprot=c1.getString(20);
				 			 													String openprotsub=c1.getString(21);
				 			 													String opwindentry = c1.getString(23);
				 			 													String opgaradoors = c1.getString(24);
				 			 													String opskylights = c1.getString(25);
				 			 													String opglassblocks = c1.getString(26);
				 			 													String opentrydoors = c1.getString(27);
				 			 													String opNGOgaragedoors = c1.getString(28);
				 			 													String wcvalue = c1.getString(29);
				 			 													String wcreinforcement = c1.getString(30);								
				 			 														
				 			 													arr_db.execSQL("INSERT INTO "
				 			 																	+ Wind_Questiontbl
				 			 																	+ " (fld_srid,insp_typeid,fld_bcyearbuilt,fld_bcpermitappdate,fld_buildcode,fld_roofdeck,fld_rdothertext," +
				 			 																	"	fld_roofwall,fld_roofwallsub,fld_roofwallclipsminval,fld_roofwallclipssingminval,fld_roofwallclipsdoubminval,fld_rwothertext," +
				 			 																	"	fld_roofgeometry ,fld_geomlength ,fld_roofgeomtotalarea ,fld_roofgeomothertext ,fld_swr,fld_optype,fld_openprotect,fld_opsubvalue,fld_oplevelchart," +
				 			 																	"   fld_opGOwindentdoorval,fld_opGOgardoorval,fld_opGOskylightsval,fld_opGOglassblockval,fld_opNGOentrydoorval,fld_opNGOgaragedoorval, " +
				 			 																	"	fld_wcvalue,fld_wcreinforcement,fld_quesmodifydate)"								
				 			 																	+ "VALUES ('" + selectedhomeid + "','32','" + yearbuilt + "','"+ permitdate + "','" + buildcode + "','" + roofdeck + "','"+rdother+"'," +
				 			 																			"'" + rwall + "','" + rsubwall + "','" + rclips + "','" + rcsingle + "','" + rcdouble	+ "','" + rwother	+ "','"+ rgeovalue + "'," +
				 			 																			"'" + rgeolength + "','" + rgeoarea + "','"+rgeoother+"','" + swr + "','"+ 0 +"','"+ openprot + "','" + openprotsub + "','" + 0 + "'," +
				 			 																			"'" + opwindentry + "','" + opgaradoors + "','" + opskylights + "','" + opglassblocks + "','" + opentrydoors + "','"+ opNGOgaragedoors +"'," +
				 			 																			"'" + wcvalue + "','" + wcreinforcement + "','"+ datewithtime + "')");
				 			 												}
				 			 													
				 			 												Cursor c3 = arr_db.rawQuery("SELECT * FROM " + quesroofcover + " WHERE SRID='" + homeid + "'", null);
				 			 												if(c3.getCount()>0)
				 			 												{
				 			 													c3.moveToFirst();
				 			 													int predominant = c3.getInt(c3.getColumnIndex("RoofPreDominant"));
				 			 													int roofcovervalue = c3.getInt(c3.getColumnIndex("RoofCoverValue"));
				 			 													String roofcovertype = c3.getString(c3.getColumnIndex("RoofCoverType"));
				 			 													
				 			 													String permitdate1 = c3.getString(c3.getColumnIndex("PermitApplnDate1"));
				 			 												    String permitdate2 = c3.getString(c3.getColumnIndex("PermitApplnDate2"));
				 			 												    String permitdate3 = c3.getString(c3.getColumnIndex("PermitApplnDate3"));
				 			 												    String permitdate4 = c3.getString(c3.getColumnIndex("PermitApplnDate4"));
				 			 												    String permitdate5 = c3.getString(c3.getColumnIndex("PermitApplnDate5"));
				 			 												    String permitdate6 = c3.getString(c3.getColumnIndex("PermitApplnDate6"));
				 			 												    
				 			 												    String prodappr1 = c3.getString(c3.getColumnIndex("ProdApproval1"));
				 			 												    String prodappr2 = c3.getString(c3.getColumnIndex("ProdApproval2"));
				 			 												    String prodappr3 = c3.getString(c3.getColumnIndex("ProdApproval3"));
				 			 												    String prodappr4 = c3.getString(c3.getColumnIndex("ProdApproval4"));
				 			 												    String prodappr5 = c3.getString(c3.getColumnIndex("ProdApproval5"));
				 			 												    String prodappr6 = c3.getString(c3.getColumnIndex("ProdApproval6"));
				 			 												    
				 			 												    String installyr1 = c3.getString(c3.getColumnIndex("InstallYear1"));
				 			 												    String installyr2 = c3.getString(c3.getColumnIndex("InstallYear2"));
				 			 												    String installyr3 = c3.getString(c3.getColumnIndex("InstallYear3"));
				 			 												    String installyr4 = c3.getString(c3.getColumnIndex("InstallYear4"));
				 			 												    String installyr5 = c3.getString(c3.getColumnIndex("InstallYear5"));
				 			 												    String installyr6 = c3.getString(c3.getColumnIndex("InstallYear6"));
				 			 												    
				 			 												    String noinfo1 = c3.getString(c3.getColumnIndex("NoInfo1"));
				 			 												    String noinfo2 = c3.getString(c3.getColumnIndex("NoInfo2"));
				 			 												    String noinfo3 = c3.getString(c3.getColumnIndex("NoInfo3"));
				 			 												    String noinfo4 = c3.getString(c3.getColumnIndex("NoInfo4"));
				 			 												    String noinfo5 = c3.getString(c3.getColumnIndex("NoInfo5"));
				 			 												    String noinfo6 = c3.getString(c3.getColumnIndex("NoInfo6"));
				 			 												    String roofcoverother = c3.getString(c3.getColumnIndex("RoofCoverTypeOther"));
				 			 													
				 			 												  Cursor c13 = arr_db.rawQuery("SELECT * FROM " + quesroofcover + " WHERE SRID='" + selectedhomeid + "'", null); 
				 															  if(c13.getCount()==0)
				 															  {
				 			 													 arr_db.execSQL("INSERT INTO "
				 			 																+ quesroofcover
				 			 																+ " (SRID,INSP_TYPEID,RoofCoverType,RoofCoverValue,RoofPreDominant,PermitApplnDate1,PermitApplnDate2,PermitApplnDate3,PermitApplnDate4,PermitApplnDate5,PermitApplnDate6,RoofCoverTypeOther,ProdApproval1,ProdApproval2,ProdApproval3,ProdApproval4,ProdApproval5,ProdApproval6,InstallYear1,InstallYear2,InstallYear3,InstallYear4,InstallYear5,InstallYear6,NoInfo1,NoInfo2,NoInfo3,NoInfo4,NoInfo5,NoInfo6)"
				 			 																+ "VALUES ('" + selectedhomeid  + "','32','"+ roofcovertype + "','"+ roofcovervalue + "','"+ predominant + "','"+ permitdate1 + "','"
				 			 																+ permitdate2 + "','" + permitdate3 + "','" + permitdate4 + "','"+ permitdate5+ "','" + permitdate6+ "','" + roofcoverother
				 			 																+ "','" +prodappr1 + "','" + prodappr2 + "','" + prodappr3+ "','" + prodappr4 + "','" + prodappr5+ "','" + prodappr6
				 			 																+ "','" + installyr1+ "','" + installyr2+ "','" + installyr3+ "','" +installyr4+ "','" + installyr5+ "','" + installyr6
				 			 																+ "','" + noinfo1+ "','" + noinfo2 + "','" + noinfo3 + "','"+ noinfo4+ "','" + noinfo5 + "','" + noinfo6+ "')");
				 			 												  }
				 			 												}
				 			 												try
				 			 												{
				 			 													Cursor c4 = arr_db.rawQuery("SELECT * FROM " + Wind_QuesCommtbl + " WHERE fld_srid='" + homeid + "'", null);
				 			 													if(c4.getCount()>0)
				 			 													{
				 			 														c4.moveToFirst();
				 			 														Cursor c14 = arr_db.rawQuery("SELECT * FROM " + Wind_QuesCommtbl + " WHERE fld_srid='" + selectedhomeid + "'", null);
				 			 														if(c14.getCount()==0)
				 			 														{
				 			 															arr_db.execSQL("INSERT INTO "
				 			 																+ Wind_QuesCommtbl
				 			 																+ " (fld_srid,insp_typeid,fld_buildcodecomments,fld_roofcovercomments,fld_roofdeckcomments,fld_roofwallcomments," +
				 			 																"fld_roofgeometrycomments,fld_swrcomments,fld_openprotectioncomments,fld_wallconscomments,fld_insoverallcomments )"
				 			 																+ "VALUES('"+selectedhomeid+"','32','"+ c4.getString(3)+ "','"+c4.getString(4)+"','"+c4.getString(5)+"','"+c4.getString(6)+"','"+c4.getString(7)+"','"+c4.getString(8)+"','"+c4.getString(9)+"','"+c4.getString(10)+"','')");
				 			 														}
				 			 													}
				 			 												}
				 			 												catch(Exception e)
				 			 												{
				 			 													System.out.println("Questions"+e.getMessage());
				 			 												}
				 			 												
				 			 												Cursor c5=SelectTablefunction(WindMit_WallCons, " where fld_srid='"+homeid+"'");
				 			 												if(c5.getCount()>0)
				 			 												{
				 			 													c5.moveToFirst();
				 			 													Cursor c15=SelectTablefunction(WindMit_WallCons, " where fld_srid='"+selectedhomeid+"'");
				 			 													if(c15.getCount()==0)
				 			 													{
				 			 	 													for(int i=0;i<c5.getCount();i++)
				 			 	 													{
				 			 	 														arr_db.execSQL("INSERT INTO "+ WindMit_WallCons
				 			 	 															+ " (fld_srid,insp_typeid,fld_Elevation,fld_Percentage,fld_Stories,fld_elevation_othertxt,fld_WallconsOther)"
				 			 	 															+ "VALUES ('" + selectedhomeid + "','32','"+ c5.getString(3)+ "','"+c5.getString(5)+"','"+c5.getString(4)+"','"+c5.getString(6)+"','"+c5.getString(7)+"')");
				 			 	 														c5.moveToNext();
				 			 	 													}
				 			 													}
				 			 												}
				 			 												
				 			 												Cursor c6=SelectTablefunction(BI_Windopenings, " where fld_srid='"+homeid+"'");
				 			 												if(c6.getCount()>0)
				 			 												{
				 			 													c6.moveToFirst();
				 			 													Cursor c16=SelectTablefunction(BI_Windopenings, " where fld_srid='"+selectedhomeid+"'");
				 			 													if(c16.getCount()==0)
				 			 													{
				 			 	 													for(int i=0;i<c6.getCount();i++)
				 			 	 													{
				 			 	 														arr_db.execSQL("INSERT INTO "+ BI_Windopenings
				 			 	 															+ " (fld_srid,insptypeid,fld_elevation,BI_WindowOpenings,fld_elevation_othertxt,fld_wind_othrtxt)"
				 			 	 															+ "VALUES ('"+selectedhomeid+"','32','"+c6.getString(3)+"','"+c6.getString(4)+"','"+c6.getString(5)+"','"+c6.getString(6)+"')");
				 			 	 														c6.moveToNext();
				 			 	 													}
				 			 													}
				 			 												}
				 			 													
				 			 												Cursor c7=SelectTablefunction(BI_Dooropenings, " where fld_srid='"+homeid+"'");
				 			 												if(c7.getCount()>0)
				 			 												{
				 			 													c7.moveToFirst();
				 			 													Cursor c17=SelectTablefunction(BI_Dooropenings, " where fld_srid='"+selectedhomeid+"'");
				 			 													if(c17.getCount()==0)
				 			 													{
				 			 	 													for(int i=0;i<c7.getCount();i++)
				 			 	 													{
				 			 	 														 arr_db.execSQL("INSERT INTO "
				 			 	 																	+ BI_Dooropenings
				 			 	 																	+ " (fld_srid,insptypeid,fld_elevation,BI_DoorOpenings,fld_elevation_othertxt)"
				 			 	 																	+ "VALUES ('"+selectedhomeid+"','32','"+c7.getString(3)+"','"+c7.getString(4)+"','"+c7.getString(5)+"')");
				 			 	 														c7.moveToNext();
				 			 	 													}
				 			 													}
				 			 												}
				 			 													
				 			 												Cursor c8=SelectTablefunction(BI_Skylights, " where fld_srid='"+homeid+"'");
				 			 												if(c8.getCount()>0)
				 			 												{
				 			 													c8.moveToFirst();
				 			 													Cursor c18=SelectTablefunction(BI_Skylights, " where fld_srid='"+selectedhomeid+"'");
				 			 													if(c18.getCount()==0)
				 			 													{
				 			 	 													for(int i=0;i<c8.getCount();i++)
				 			 	 													{
				 			 	 														arr_db.execSQL("INSERT INTO "
				 			 	 																+ BI_Skylights
				 			 	 																+ " (fld_srid,insptypeid,fld_elevation,BI_Skylights,fld_elevation_othertxt)"
				 			 	 																+ "VALUES ('"+selectedhomeid+"','32','"+c8.getString(3)+"','"+c8.getString(4)+"','"+c8.getString(5)+"')");
				 			 	 														c8.moveToNext();
				 			 	 													}
				 			 													}
				 			 												}
				 			 												
				 			 												Cursor c9=SelectTablefunction(WindDoorSky_NA, " where fld_srid='"+homeid+"'");
				 			 												if(c9.getCount()>0)
				 			 												{
				 			 													c9.moveToFirst();
				 			 													Cursor c19=SelectTablefunction(WindDoorSky_NA, " where fld_srid='"+selectedhomeid+"'");
				 			 													if(c19.getCount()==0)
				 			 													{
				 			 	 													for(int i=0;i<c9.getCount();i++)
				 			 	 													{
				 			 	 														arr_db.execSQL("INSERT INTO "
				 			 	 																+ WindDoorSky_NA
				 			 	 																+ " (fld_srid,insp_typeid,fld_windowopenNA,fld_dooropenNA,fld_skylightsNA)"
				 			 	 																+ "VALUES ('"+selectedhomeid+"','32','"+c9.getString(3)+"','"+c9.getString(4)+"','"+c9.getString(5)+"')");
				 			 	 														c8.moveToNext();
				 			 	 													}
				 			 													}
				 			 												}
				 			 									
				 			 												Communalareasduplication(32,homeid);
						 													Roofsectionduplication(32,homeid); 
						 													AddendumComments(32,homeid);
						 												
						 													ShowToast("Commercial Type I Inspection has beed duplicated successfully.",1);
						 													

				 			 												
				 			 										}
		 			 											}
		 			 											catch (Exception e) {
																	// TODO: handle exception
																}
		 		
		 			 										}
		 			 										else if(i==33)
		 			 										{
		 			 											try
		 			 											{
			 			 											Cursor c2 = arr_db.rawQuery("select * from " + Comm_Building +" Where BUILDING_NO='"+comm1txt+"' and Commercial_Flag='"+flag+"'", null);
				 													if(c2.getCount()>0)
				 													{
				 														c2.moveToFirst();
				 														String homeid = c2.getString(c2.getColumnIndex("fld_srid"));System.out
																				.println("homeid22="+homeid);
				 														
				 														AddendumComments(33,homeid);
				 														Communalareasduplication(33,homeid);
				 														CommercialWind(33,homeid);
				 														Roofsectionduplication(33,homeid);
				 													}
				 													ShowToast("Commercial Type II Inspection has beed duplicated successfully.",1);
				 													
		 			 											}
		 			 											catch (Exception e) {
																	// TODO: handle exception
																}

		 			 										}
		 			 										else if(i==34)
		 			 										{
		 			 											try
		 			 											{
			 			 											Cursor c2 = arr_db.rawQuery("select * from " + Comm_Building +" Where BUILDING_NO='"+comm1txt+"' and Commercial_Flag='"+flag+"'", null);
				 													if(c2.getCount()>0)
				 													{
				 														c2.moveToFirst();
				 														String homeid = c2.getString(c2.getColumnIndex("fld_srid"));
				 														
				 														AddendumComments(34,homeid);
				 														Communalareasduplication(34,homeid);
				 														CommercialWind(34,homeid);
				 														Roofsectionduplication(34,homeid);
				 													}
				 													ShowToast("Commercial Type III Inspection has been duplicated successfully.",1);
		 			 											}
		 			 											catch (Exception e) {
																	// TODO: handle exception
																}
		 			 										}
		 			 										insertbuildingno(e1.getText().toString(),flag,i);
		 			 									
		 			 									}	 	
		 			 									
		 			 									
		 			 									
		 			 							})
		 			 							.setNegativeButton("No",new DialogInterface.OnClickListener() {
		 			 									public void onClick(DialogInterface dialog,int id) {
		 			 											dialog.cancel();
		 			 									}
		 			 							});
		 			 							builder.show();	
		 			 							
		 			 							dialog1.setCancelable(false);
		 			 		 		 			dialog1.show();

		 			 					
		 		 				  
	 		 					}
	 		 					
	 		 					
	 		 				}
	 		 				else
	 		 				{
	 		 					if(e1.getText().toString().trim().equals(""))
	 		 					{
	 		 						ShowToast("Please enter the Building Number.", 1);
	 		 						setFocus(e1);
	 		 					}
	 		 					else if(noofbuildingvalidation(e1.getText().toString().trim()) == false)
	 							{
	 								ShowToast("Please enter the building number less than or equal to "+noofbuildings, 0);
	 								e1.setText("");							
	 							}
	 							else if(buildingnovalidation(e1.getText().toString().trim()).equals("zero"))
	 							{
	 								ShowToast("Invalid Entry! Please enter the valid building number.", 0);
	 								e1.setText("");							
	 							}
	 							else if(CheckDupBNo(e1.getText().toString().trim(),flag)==false)
	 							{
	 								ShowToast("Already Exists! Please enter the different building number.", 0);
	 								e1.setText("");							
	 							}	 		 					
	 							/*else if(comm1txt.equals("--Select--"))
	 		 					{
	 		 						ShowToast("Please Choose the Completed Inspections Building Number to Duplicate.", 1);
	 		 					}*/
	 		 					
	 		 				}
	 		           		}
	 		 			});
	 	 			dialog1.setCancelable(false);
	 	 			dialog1.show();

	 		}

	}
		
	
	private boolean chkrecordscommonsave(String srid, int i,int iden) {
		// TODO Auto-generated method stub
		boolean chkcomm1 = chkrecordssavedcomm1(srid,i,iden); 
		  boolean chkcomm = Check_Comm1(srid,i,iden);
	  	  Check_Comm2(srid,i,iden);
	  	Check_Addencomments(srid,iden,i);
	  	  Cursor croof = arr_db.rawQuery("select * from "+Roof_master+" where (RM_insp_id='4' || RM_insp_id='5' || RM_insp_id='6') and RM_srid='"+srid+"'",null);
	  	
	  	  if(chkcomm1==true || chkcomm==true || !communalcomments.equals("") || curaux.getCount()!=0 || curapppl.getCount()!=0 || Aux_NA.equals("1") ||
	  				 Rest_NA.equals("1") || curcomm2.getCount()>0 || croof.getCount()>0 || curaddendum.getCount()!=0)
		  {		
	  		 b4=true;	
		  }
	  	  else
	  	  {
	  		 b4=false;
	  	  }
	 
	   return b4;
	}
	public void showenterbuildnopopup(final int i, final int typeid,final String flag, int count) {
		// TODO Auto-generated method stub
		final Dialog dialog1 = new Dialog(con,android.R.style.Theme_Translucent_NoTitleBar);
		dialog1.getWindow().setContentView(R.layout.alert);
		LinearLayout maintable= (LinearLayout) dialog1.findViewById(R.id.maintable);
		LinearLayout currenttable= (LinearLayout) dialog1.findViewById(R.id.Edit_number);
		maintable.setVisibility(View.GONE);
		currenttable.setVisibility(View.VISIBLE);
		Button btnsave=(Button) dialog1.findViewById(R.id.EN_save);
	    Button btncancel=(Button) dialog1.findViewById(R.id.EN_cancel);
		btncancel.setVisibility(v1.GONE);
		TextView txttitle=(TextView) dialog1.findViewById(R.id.RP_txthelp);txttitle.setText("Building Number");
		TextView txtcontent=(TextView) dialog1.findViewById(R.id.EN_txtid);txtcontent.setText("Please enter the Building Number");
		
		InputFilter[] FilterArray = new InputFilter[1];
		FilterArray[0] = new InputFilter.LengthFilter(3);
		((EditText) dialog1.findViewById(R.id.EN_number)).setFilters(FilterArray);
		
		
		 ImageView bt_close=(ImageView) dialog1.findViewById(R.id.EN_close);
		   bt_close.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(submenu_layout!=null){submenu_layout.removeAllViews();}
				dialog1.setCancelable(true);
				
				dialog1.dismiss();
				hidekeyboard(((EditText) dialog1.findViewById(R.id.EN_number)));
				//goclass(3);
			}
		});
		btnsave.setOnClickListener(new OnClickListener() {						
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub	
					String s =((EditText) dialog1.findViewById(R.id.EN_number)).getText().toString().trim();		
					if(s.equals(""))
					{
						ShowToast("Please enter the Building Number.", 1);												
					}	
					else if(noofbuildingvalidation(s) == false)
					{
						ShowToast("Please enter the building number less than or equal to "+noofbuildings, 0);
						((EditText) dialog1.findViewById(R.id.EN_number)).setText("");							
					}
					else if(buildingnovalidation(s).equals("zero"))
					{
						ShowToast("Invalid Entry! Please enter the valid Building Number.", 0);
						((EditText) dialog1.findViewById(R.id.EN_number)).setText("");							
					}
					else if(CheckDupBNo(s,flag)==false)
					{
						ShowToast("Already Exists. Please enter a different Building Number.", 0);
						((EditText) dialog1.findViewById(R.id.EN_number)).setText("");							
					}
					else
					{
						System.out.println("inside first eles");
						try
						{
							Cursor c1 = SelectTablefunction(Comm_Building, " where Commercial_Flag='"+CommercialFlag+"' and fld_srid='"+selectedhomeid+"'");
							if(c1.getCount()==0)
							{
								
								
								arr_db.execSQL("INSERT INTO " +Comm_Building + " (fld_srid,insp_typeid,BUILDING_NO,Commercial_Flag)"+ "VALUES ('"+selectedhomeid+"','"+typeid+"','"+s+"','"+CommercialFlag+"')");						
							}
							else
							{				
									arr_db.execSQL("UPDATE "+ Comm_Building+ " SET BUILDING_NO='"+s+"' WHERE fld_srid ='"+ selectedhomeid + "'");							
							}
						}
						
						catch(Exception e){System.out.println("dsdsfdsdsddfds="+e.getMessage());}
						dialog1.setCancelable(true);
						dialog1.dismiss();	
						ShowToast("Building Number saved successfully.",1);
						if(i==1){gotoclass(32,WindMitBuildCode.class);}
						else if(i==2){gotoclass(33,CommercialWind.class);}
						else if(i==3){gotoclass(34,CommercialWind.class);}
						
					}
				}
			});				
		dialog1.show();
	}

	class Touch_Listener implements OnTouchListener
	{
		   public int type;
		   Touch_Listener(int type)
			{
				this.type=type;
				
			}
		    @Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
		    	((EditText) v).setFocusable(true);
		    	((EditText) v).setFocusableInTouchMode(true);
		    	((EditText) v).requestFocus();
		    	((EditText) v).setSelection(((EditText) v).length());   
		    
		    	return false;
		    }
	}
	
	public void gotoclass(int i, Class<?> applicationContext) {
		// TODO Auto-generated method stub
		identityval=i;
		myintent = new Intent(this.con, applicationContext); 
		putExtraIntent(myintent);
		myintent.putExtra("Roof", Roof);
		myintent.putExtra("identity", i);
		if(applicationContext==RoofSection.class)
		{
			//showloadingalert(myintent);
			
		}else
		{
			(this.con).startActivity(myintent);
		}

	}	
	public void windmittypechk(int identity, RadioButton chx1,RadioButton chk2, RadioButton chk3, RadioButton chk4,TextView tv,ImageView editimg) {
		// TODO Auto-generated method stub		
		  if(identity==31){  chx1.setChecked(true);}
		  else if(identity==32){chk2.setChecked(true);}
		  else if(identity==33){chk3.setChecked(true);}
		  else if(identity==34){chk4.setChecked(true);}
		  
		  if(chk2.isChecked() || chk3.isChecked() || chk4.isChecked())
		  {
			  selecttbl(identity);
				if(curbuilding.getCount()>0)
				{
					tv.setVisibility(v1.VISIBLE);
					if(chk2.isChecked())
					{
						if(!comm1_buildno.equals(""))
						{
							tv.setText(Html.fromHtml("<b><font color=#0B610B>Building No : " + "</font><font color=#ff0000>" + comm1_buildno+ "</font></b>"));							
							//editimg.setVisibility(v1.VISIBLE);
						}
					}
					else if(chk3.isChecked())
					{
						if(!comm1_buildno.equals(""))
						{
							tv.setText(Html.fromHtml("<b><font color=#0B610B>Building No : " + "</font><font color=#ff0000>" + comm1_buildno+ "</font></b>"));			
							//.setVisibility(v1.VISIBLE);
						}
						
					}
					else if(chk4.isChecked())
					{
						if(!comm1_buildno.equals(""))
						{
							tv.setText(Html.fromHtml("<b><font color=#0B610B>Building No : " + "</font><font color=#ff0000>" + comm1_buildno+ "</font></b>"));		
							//editimg.setVisibility(v1.VISIBLE);
						}
						
					}
				}
			  							 
		  }
		  
	}


	public void setvalueradio(String s,RadioButton[] R) {
		// TODO Auto-generated method stub
		
		if(!s.equals(""))
		{
			for(int i=0;i<R.length;i++)
			{
				if(s.equals(R[i].getTag().toString()))
				{
					R[i].setChecked(true);
					return;
				}
			}
			return;
		}
	}
	public void goback(int i) {
		// TODO Auto-generated method stub
		 Class backclass = mainclass[i];
		 Intent myintent =new Intent(this.con,backclass);
		  myintent.putExtra("Roof", Roof);
		  
		  putExtraIntent(myintent);
		  if(backclass==RoofSection.class)
		  {
			//  showloadingalert(myintent);
		  }
		  else
		  {
			  (this.con).startActivity(myintent);
		  }


	}
	
	public boolean checkAttic() {
		// TODO Auto-generated method stub
		 CreateARRTable(43);
		 CreateARRTable(431);
		 CreateARRTable(432);
		 CreateARRTable(434);
		 Cursor c2=arr_db.rawQuery(" SELECT R_ADD_scope  from "+Roof_additional+" Where R_ADD_Ssrid='"+selectedhomeid+"' and  R_ADD_insp_id='1' ",null);
		 boolean atcchk=false;
		 if(c2.getCount()>0)
		 {
			 c2.moveToFirst();
			// String typechk_val=decode(c2.getString(c2.getColumnIndex("RC_Condition_type_val")));
			 String tmp_desc=decode(c2.getString(c2.getColumnIndex("R_ADD_scope")));
			 /*if(typechk_val.equals("Yes") || typechk_val.equals("No"))
			 {*/
								
				 if(tmp_desc.contains("Exterior Only"))
				 {
					 atcchk=true;
				 }else{
					 atcchk=false;
				
				}
			/* }
			 else
			 {
				 atcchk=false;
			 }*/
		 }
		return atcchk;
	}
	public String getResourcesvalue(int id) {
		// TODO Auto-generated method stub
		return con.getResources().getString(id);
	}
	/*public void showloadingalert(final Intent myintent)
	{
		CreateARRTable(99);
		CreateARRTable(431);
		CreateARRTable(43);
		CreateARRTable(432);
		CreateARRTable(433);
		CreateARRTable(434);
		
	
	String page ="2",page1 ="1",inspe_id="7";
	
	 String savedform=myintent.getExtras().getString("Roof_save_from");
	if(savedform==null)
	{
		savedform="";
	}
	
	
	if(savedform.equals("") || (savedform.equals(Roof)))
	{
		
		if(Roof.equals(Roof_insp[0]))
		 {
			inspe_id="1";
			*//*** For 4poi2point Roof Cover";
			page1="4point RCNoted";
			 /****For 4poin ends**//*
	     }
		 else if(Roof.equals(Roof_insp[1]))
		 {
			*//*** Roof Survey***//*
			 inspe_id="2";
			*//****Roof Survay ends**//*
			 
		 }
		 else  if(Roof.equals(Roof_insp[4]))
		 {
			 inspe_id="7";	   
			 *//*** For GCH***2CH Roof Cover";
			 page1="GCH RCNoted";
			 /***	 For GCH Ends***//*
		 }
		 else if(Roof.equals(Roof_insp[2]))
		 {
			
			*//*** Roof Survey***//*
			 inspe_id="5";
			 *//****Roof Survay ends**//*
		 } 
		 else if(Roof.equals(Roof_insp[3]))
		 {
			
			*//*** Roof Survey***//*
			 inspe_id="6";
			*//****Roof Survay ends**//*
		 }
		 else if (Roof.equals(getResourcesvalue(R.string.insp_4)))
		 {
			
			 inspe_id="4";
			*//*** Roof S2ommercial1 Roof Cover";
			 page1="Commercial1 RCNoted";
			 /****Roof Survay ends**//*
		 }else if (Roof.equals(getResourcesvalue(R.string.insp_5)))
		 {
			
			 inspe_id="5";
			*//*** Roof S2ommercial1 Roof Cover";
			 page1="Commercial1 RCNoted";
			 /****Roof Survay ends**//*
		 }
		 else if (Roof.equals(getResourcesvalue(R.string.insp_6)))
		 {
			
			 inspe_id="6";
			*//*** Roof S2ommercial1 Roof Cover";
			 page1="Commercial1 RCNoted";
			 /****Roof Survay ends**//*
	

	/****TO GET THE POLICY HOLDE INFORMATION**/
	public void getPolicyholderInformation(String Srid)
	{
		try
		{
			CreateARRTable(3);
			Cursor c =SelectTablefunction(policyholder, " WHERE ARR_PH_SRID='"+Srid+"'");
			if(c.getCount()>0)
			{
				c.moveToFirst();
				PH_FirstName=c.getString(c.getColumnIndex("ARR_PH_FirstName"));
				County=c.getString(c.getColumnIndex("ARR_PH_County"));
				Stories=c.getString(c.getColumnIndex("ARR_PH_NOOFSTORIES"));
				YearPH=c.getString(c.getColumnIndex("ARR_YearBuilt"));
				BuildingSize=c.getString(c.getColumnIndex("BuidingSize"));
				CommercialFlag=c.getString(c.getColumnIndex("Commercial_Flag"));
				MainInspID=c.getString(c.getColumnIndex("ARR_PH_InspectionTypeId"));
				
				PH_Email=c.getString(c.getColumnIndex("ARR_PH_Email"));
				PH_EmailChkbx=c.getString(c.getColumnIndex("ARR_PH_EmailChkbx"));
				PH_Address1=c.getString(c.getColumnIndex("ARR_PH_Address1"));
				Insp_date = decode(c.getString(c.getColumnIndex("ARR_Schedule_ScheduledDate")));System.out.println("inspdate="+Insp_date);
				IS_Elec_On =c.getString(c.getColumnIndex("fld_elec_turnon"));
				IS_Water_On=c.getString(c.getColumnIndex("fld_water_turnon"));
				IS_Access_Conf=c.getString(c.getColumnIndex("fld_acesss_confirmed"));
				IS_Pres_Occu=c.getString(c.getColumnIndex("fld_pres_occu"));
				IS_Sewer_Ser=c.getString(c.getColumnIndex("fld_sewer_serv"));
				IS_Buil_type=c.getString(c.getColumnIndex("fld_build_type"));
				
				
			}
		}
		catch(Exception e)
		{
			
		}
	}
	/****TO GET THE POLICY HOLDE INFORMATION* ENDS
	 * @param string 
	 * @param plumbing 
	 * @param hvac 
	 * @return */
	public Intent loadComments(String string,int loc[]) {
		// TODO Auto-generated method stub
		
		Intent in =new Intent(this.con,LoadComments.class);
	 	in.putExtra("insp_name", loadinsp_n);
		in.putExtra("insp_type", onlinspectionid);
		in.putExtra("insp_ques", loadinsp_q);
		in.putExtra("insp_opt", string);
		in.putExtra("xfrom", loc[0]+10);
		in.putExtra("yfrom", loc[1]+10);
	   return in;
	}
	public void clearother(EditText editText) {
		// TODO Auto-generated method stub
		editText.setVisibility(v1.GONE);
		editText.setText("");
	}
	public void setRowFocus(LinearLayout findViewById) {
		// TODO Auto-generated method stub
		findViewById.setFocusableInTouchMode(true);
		findViewById.requestFocus();
	
	}
	public String yearValidation(String string) {
		// TODO Auto-generated method stub
		
		String g="false";getCalender();
		int fv = Character.getNumericValue(string.charAt(0));
		int sv = Character.getNumericValue(string.charAt(1));
		int tv = Character.getNumericValue(string.charAt(2));
		int fov = Character.getNumericValue(string.charAt(3));
		
		if(Integer.parseInt(string)>mYear){
			g="true";
		}else if((Integer.parseInt(string)==0000) || fv==0){
			g="zero";
		}else{
			g="false";
		}
		return g;
	}
	
	public String checkfortodaysdate(String string) {
		// TODO Auto-generated method stub
		
			int i1 = string.indexOf("/");
			String result = string.substring(0, i1);
			int i2 = string.lastIndexOf("/");
			String result1 = string.substring(i1 + 1, i2);
			String result2 = string.substring(i2 + 1);
			result2 = result2.trim();
			int smonth= Integer.parseInt(result);
			int sdate = Integer.parseInt(result1);
			int syear = Integer.parseInt(result2);
		    getCalender();
		    mMonth = mMonth + 1;
		    if ( syear  < mYear || (smonth < mMonth && syear  <= mYear) || (smonth <= mMonth && syear <=mYear && sdate <= mDay )) {
		    	return "true";
			} else {
				return "false";
			}
		
	}
	public String checkfortodaysdategch(String string) {
		// TODO Auto-generated method stub
		System.out.println("struong ="+string);
		if(!string.equals("") && !string.equals("Not Determined"))
		{
			System.out.println("SDDDD");
			int i1 = string.indexOf("/");
			String result = string.substring(0, i1);
			int i2 = string.lastIndexOf("/");
			String result1 = string.substring(i1 + 1, i2);
			String result2 = string.substring(i2 + 1);
			result2 = result2.trim();
			int smonth= Integer.parseInt(result);
			int sdate = Integer.parseInt(result1);
			int syear = Integer.parseInt(result2);
		    getCalender();
		    mMonth = mMonth + 1;
		    if ( syear  < mYear || (smonth < mMonth && syear  <= mYear) || (smonth <= mMonth && syear <=mYear && sdate <= mDay )) {
		    	return "true";
			} else {
				return "false";
			}
		}
		else
		{
			return "true";
		}
	}
	public void findinspectionname(int value) {
		// TODO Auto-generated method stub
		 if(value==31){ inspname=All_insp_list[3];}
         else if(value==32){inspname=All_insp_list[4];}
         else if(value==33){inspname=All_insp_list[5];}
         else if(value==34){inspname=All_insp_list[6];}		
	}
	public void cbfocus(CheckBox checkBox) {
		// TODO Auto-generated method stub
		/*checkBox.setFocusableInTouchMode(true);
		checkBox.requestFocus();
		checkBox.setFocusableInTouchMode(false);
		checkBox.setFocusable(false);
		checkBox.setFocusable(true);*/
		/*checkBox.setFocusable(false);
		checkBox.setFocusableInTouchMode(false);
		checkBox.setFocusable(true);
		checkBox.setFocusableInTouchMode(true);
		checkBox.requestFocus();
		checkBox.setFocusableInTouchMode(false);*/
		
		checkBox.setChecked(true);
		checkBox.setFocusable(true);
		checkBox.setFocusableInTouchMode(true);
		checkBox.requestFocus();
		checkBox.setFocusableInTouchMode(false);
	}
	@Override
	public void onClick(DialogInterface dialog, int which) {
		// TODO Auto-generated method stub
		
		switch(which){
		case DialogInterface.BUTTON_NEUTRAL: // neutral
		 kstr=true;
		break;
		default:
		// nothing
		break;
		}
	}
	public void ShowError(EditText editText, String string) {
		// TODO Auto-generated method stub
		/*final float scale = getResources().getDisplayMetrics().density;
        mPopup = new ErrorPopup(err, (int) (200 * scale + 0.5f), (int) (50 * scale + 0.5f));
        mPopup.setFocusable(false);*/
		
		editText.setError(string);
		
		
		//int ecolor = #000000; // whatever color you want
		/*ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.CYAN);
		SpannableStringBuilder ssbuilder = new SpannableStringBuilder(string);
		ssbuilder.setSpan(fgcspan, 0, string.length(), 0);
		editText.setError(ssbuilder);*/
	}
	public void layoutfocus(LinearLayout linearLayout) {
		// TODO Auto-generated method stub
		linearLayout.setFocusable(true);
		linearLayout.setFocusableInTouchMode(true);
		linearLayout.requestFocus();
		linearLayout.setFocusableInTouchMode(false);
	}
	public String getselecctedtag_radio(RadioButton[] R) {
		// TODO Auto-generated method stub
		for(int i=0;i<R.length;i++)
		{
			if(R[i].isChecked())
			{
					s= R[i].getTag().toString();
			}
		}		
		return s;
	}
	public void setObsvalue(String obs6achkloc_val, CheckBox[] obs6achkloc, EditText editText) {
		// TODO Auto-generated method stub
			if(!obs6achkloc_val.equals(""))
			{
				String temp[]=obs6achkloc_val.split(",");
				
				int j=0;
				do{
					for(int i=0;i<obs6achkloc.length;i++)
					{
						
						if(temp[j].equals(obs6achkloc[i].getText().toString()))
						{
							obs6achkloc[i].setChecked(true);
							j++;
							if(j==temp.length)
							{
								return;
							}
						}
						
					}
					if(j!=temp.length)
					{
						
						obs6achkloc[obs6achkloc.length-1].setChecked(true); // no matches found for the last value in the split text so we set the other to tru  
						editText.setVisibility(View.VISIBLE);
						editText.setText(temp[j]);
						j++;	
					}
				}while(j<temp.length);
			}	
	}
	
	public void selecttbl(int i)
	{
		try
		{
			CreateARRTable(65);	
			getPolicyholderInformation(selectedhomeid);
			curbuilding= SelectTablefunction(Comm_Building, " where Commercial_Flag='"+CommercialFlag+"' and fld_srid='"+selectedhomeid+"' and insp_typeid='"+i+"'");
			System.out.println(" where Commercial_Flag='"+CommercialFlag+"' and fld_srid='"+selectedhomeid+"' and insp_typeid='"+i+"'"+curbuilding.getCount());
			if(curbuilding.getCount()>0)
			{
				curbuilding.moveToFirst();
				comm1_buildno = curbuilding.getString(curbuilding.getColumnIndex("BUILDING_NO"));System.out.println("test="+comm1_buildno);
			}
		}
		catch(Exception e)
		{
			System.out.println("selecttbl="+e.getMessage());
		}
	}




	public String[] setvalueToArray(Cursor tbl) {
		// TODO Auto-generated method stub
		String[] arr=null;
		if(tbl!=null && tbl.getCount()==1)
		{
			tbl.moveToFirst();
			arr=new String[tbl.getColumnCount()];
			for(int i=0;i<tbl.getColumnCount();i++)
			{
				if(tbl.getString(i)==null)
				{
					arr[i]="";	
				}
				else
				{
					arr[i]=decode(tbl.getString(i));
				}
			}
		}
			return arr;
	}
	
	public boolean doesTblExist(String tblName){ 
        Cursor rs = null; 
        try{ 
            rs = arr_db.rawQuery("SELECT * FROM " + tblName + " WHERE 1=0", null ); 
            return true; 
        }catch(Exception ex){ 
            return false; 
        }finally{ 
            if (rs != null) rs.close(); 
        } 
}
	public SoapObject SoapRequest(String string) {
		// TODO Auto-generated method stub
		
		if(con instanceof Export)
		{
			
			((Export) con).Web_mehtod=string;
		}
		SoapObject crequest = new SoapObject(NAMESPACE,string);
		envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		return crequest;
		
	}
	public boolean SoapResponse(String string, SoapObject request2)throws SocketException, NetworkErrorException, TimeoutException, IOException,XmlPullParserException,NullPointerException{
		// TODO Auto-generated method stub
		
		envelope.setOutputSoapObject(request2);
		/*if(!string.equals("ANDROIDERRORMAIL"))
			string="dfgfsg";*/
			HttpTransportSE androidHttpTransport = new HttpTransportSE(URL_ARR);
		
		
		//ANDROIDERRORMAIL
		androidHttpTransport.debug = true; 
			if(con instanceof Export)
			{
				((Export) con).curr_Request=request2.toString();
			}
			
			androidHttpTransport.call(NAMESPACE+string,envelope);
		
			try
			{
			String result =  envelope.getResponse().toString();System.out.println("COMMERCIALWINDresul="+result);
			return Boolean.parseBoolean(result);
			}catch (SoapFault e) {
				// TODO: handle exceptionth
				System.out.println("the ieesue "+e);
				return false;
				//throw e;
			}
	
	}
	public boolean SoapResponse(String string,
			SoapSerializationEnvelope envelope2) throws SocketException, NetworkErrorException, TimeoutException, IOException,XmlPullParserException,NullPointerException{

			HttpTransportSE androidHttpTransport = new HttpTransportSE(URL_ARR);
		androidHttpTransport.debug = true; 
			androidHttpTransport.call(NAMESPACE+string,envelope2);
		
			try
			{
			String result =  envelope.getResponse().toString();
			return Boolean.parseBoolean(result);
			}catch (SoapFault e) {
				// TODO: handle exceptionth
				
				return false;
				//throw e;
			}
			}
	public String buildingnovalidation(String string) {
		// TODO Auto-generated method stub
		String g="false";
		try
		{
			int fv = Character.getNumericValue(string.charAt(0));
			if(fv==0){
				g="zero";
			}else{
				g="false";
			}			
		}
		catch(Exception e)
		{
			g="false";			
		}
		System.out.println("what is g ="+g);
		return g;
	}
	public boolean FP_AtticValidation() {
		// TODO Auto-generated method stub
		boolean bolatc = false;
		try
			{
			Cursor C_FPATT= SelectTablefunction(Four_Electrical_Attic, " where fld_srid='" + selectedhomeid + "' ");
			if(C_FPATT.getCount()>0)
			{
				C_FPATT.moveToFirst();
				String selatcopt=decode(C_FPATT.getString(C_FPATT.getColumnIndex("atcoptions")));
				String rfd = decode(C_FPATT.getString(C_FPATT.getColumnIndex("rfdcinc")));
				String rfw = decode(C_FPATT.getString(C_FPATT.getColumnIndex("rfwallinc")));
				String gw = decode(C_FPATT.getString(C_FPATT.getColumnIndex("gablewallinc")));
				String ac = decode(C_FPATT.getString(C_FPATT.getColumnIndex("atccondinc")));
				String acomm = decode(C_FPATT.getString(C_FPATT.getColumnIndex("atccomments")));
				String attic_na = decode(C_FPATT.getString(C_FPATT.getColumnIndex("Attic_Na")));
				if((!selatcopt.equals("") && !rfd.equals("")&& !rfw.equals("") && !gw.equals("") && !ac.equals("")) || attic_na.equals("true"))
				{
					bolatc=true;
				}
				
			}
			
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		return bolatc;
	}
	public boolean FP_PlumbingValidation() {
		// TODO Auto-generated method stub
		boolean bolplu=false;
		CreateARRTable(23);CreateARRTable(231);
		try
		{
			Cursor C_FPP= SelectTablefunction(Four_Electrical_Plumbing, " where fld_srid='" + selectedhomeid + "' ");
			if(C_FPP.getCount()>0)
			{
				C_FPP.moveToFirst();
				String pwss=C_FPP.getString(C_FPP.getColumnIndex("wsinc"));
				String psss=C_FPP.getString(C_FPP.getColumnIndex("ssinc"));
				String pwh=C_FPP.getString(C_FPP.getColumnIndex("whinc"));
				String pnob=C_FPP.getString(C_FPP.getColumnIndex("nobinc"));
				String poc=C_FPP.getString(C_FPP.getColumnIndex("ocinc"));
				String pod=C_FPP.getString(C_FPP.getColumnIndex("odinc"));
				String pcond=C_FPP.getString(C_FPP.getColumnIndex("ovrplucondinc"));
				if(!pwss.equals("") && !psss.equals("") && !pwh.equals("") && !pnob.equals("")
					&& !poc.equals("") && !pcond.equals(""))
				{
					bolplu=true;
				}
				
			}
		
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		return bolplu;
	}
	public boolean FP_GeneralValidation() {
		// TODO Auto-generated method stub
		CreateARRTable(24);
		boolean bolgen=false;
		try
		{
			Cursor C_FPEG= SelectTablefunction(Four_Electrical_General, " where fld_srid='" + selectedhomeid + "' ");
			if(C_FPEG.getCount()>0)
			{
				bolgen=true;
			}
			
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		return bolgen;
	}
	public boolean FP_MPanelValidation() {
		// TODO Auto-generated method stub
		boolean bolmp = false;
		CreateARRTable(247);
		try
		{
			Cursor C_FPMP= SelectTablefunction(Four_Electrical_MainPanel, " where fld_srid='" + selectedhomeid + "' ");
			if(C_FPMP.getCount()>0)
			{
				bolmp = true;
			}
			
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		return bolmp;
	}
	public boolean FP_SPanelValidation() {
		// TODO Auto-generated method stub
		boolean bolsp = false;
		CreateARRTable(248);
		try
		{
			Cursor C_FPSP= SelectTablefunction(Four_Electrical_SubPanel, " where fld_srid='" + selectedhomeid + "' ");
			if(C_FPSP.getCount()>0)
			{
				bolsp = true;
			}
		
		}catch (Exception e) {
			// TODO: handle exception
		}
		return bolsp;
	}
	public boolean FP_PanelValidation() {
		// TODO Auto-generated method stub
		boolean bolp = false;
		CreateARRTable(242);
		try
		{
			Cursor C_FPEP= SelectTablefunction(Four_Electrical_Panel, " where fld_srid='" + selectedhomeid + "' ");
			if(C_FPEP.getCount()>0)
			{
				bolp = true;
			}
			
		}catch (Exception e) {
			// TODO: handle exception
		}
		return bolp;
	}
	public boolean FP_WirValidation() {
		// TODO Auto-generated method stub
		boolean bolwir = false;
		CreateARRTable(246);
		try
		{
			Cursor C_FPWR= SelectTablefunction(Four_Electrical_Wiring, " where fld_srid='" + selectedhomeid + "' ");
			if(C_FPWR.getCount()>0)
			{
				bolwir = true;
			}
			
		}catch (Exception e) {
			// TODO: handle exception
		}
		return bolwir;
	}
	public boolean FP_RecpValidation() {
		// TODO Auto-generated method stub
		boolean bolrec = false;
		CreateARRTable(243);
		try
		{
			Cursor C_FPR= SelectTablefunction(Four_Electrical_Receptacles, " where fld_srid='" + selectedhomeid + "' ");
			if(C_FPR.getCount()>0)
			{
				bolrec = true;
			}
			
		}catch (Exception e) {
			// TODO: handle exception
		}
		return bolrec;
	}
	public boolean FP_GoValidation() {
		// TODO Auto-generated method stub
		boolean bolgo = false;
		CreateARRTable(245);
		try
		{
			Cursor C_FGO= SelectTablefunction(Four_Electrical_GO, " where fld_srid='" + selectedhomeid + "' ");
			if(C_FGO.getCount()>0)
			{
				bolgo = true;
			}
			
		}catch (Exception e) {
			// TODO: handle exception
		}
		return bolgo;
	}
	public boolean FP_HVACValidation() {
		// TODO Auto-generated method stub
		boolean bolhv = false;
		CreateARRTable(25);
		try
		{
			Cursor C_FPH= SelectTablefunction(Four_HVAC, " where fld_srid='" + selectedhomeid + "'");
			if(C_FPH.getCount()>0)
			{
				bolhv=true;
				
			}
		
		}catch (Exception e) {
			// TODO: handle exception
		}
		return bolhv;
	}
	public boolean FP_HVACUnValidation() {
		// TODO Auto-generated method stub
		boolean bolhvu = false;
		CreateARRTable(26);
			try
					{
						Cursor C_FPHU= SelectTablefunction(Four_HVAC_Unit, " where fld_srid='" + selectedhomeid + "' ");
						if(C_FPHU.getCount()>0)
						{
							bolhvu=true;
						}
					
					}
					catch (Exception e) {
						// TODO: handle exception
			
				
			}
	
		return bolhvu;
	}
	public boolean FP_HVACAnValidation() {
		// TODO Auto-generated method stub
		boolean bolhva = false;
		CreateARRTable(25);
		try
		{
			Cursor C_FPH= SelectTablefunction(Four_HVAC, " where fld_srid='" + selectedhomeid + "' ");
			if(C_FPH.getCount()>0)
			{
				C_FPH.moveToFirst();
				String hvc=C_FPH.getString(C_FPH.getColumnIndex("fld_inchvac"));
				if(hvc.equals("0"))
				{
					bolhva=true;
					
				}
			}
	   }catch (Exception e) {
			// TODO: handle exception
		}
		return bolhva;
	}
	public boolean GCH_PPValidation() {
		// TODO Auto-generated method stub
		boolean bolpp = false;
		CreateARRTable(41);
		try {
			Cursor C_SPP = arr_db.rawQuery("select * from tbl_gch_poolpresent where fld_srid='"+selectedhomeid+"' AND (fld_ppchk='1' OR (fld_ppchk='0' AND fld_ppground<>''))", null);
			if(C_SPP.getCount()>0)
			{
				bolpp=true;
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return bolpp;
	}
	public boolean GCH_PfValidation() {
		// TODO Auto-generated method stub
		boolean bolpf = false;
		CreateARRTable(42);
		try {
			Cursor C_PPF= SelectTablefunction(GCH_PoolFencetbl, " where fld_srid='" + selectedhomeid + "' AND (fld_ppfchk='1' OR (fld_ppfchk='0' AND fld_ppfence<>''))");
			if(C_PPF.getCount()>0)
			{
				bolpf=true;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return bolpf;
	}
	public boolean GCH_SHValidation() {
		// TODO Auto-generated method stub
		boolean bolsh = false;
		CreateARRTable(44);
		try {
			 Cursor C_SHC= SelectTablefunction(GCH_SummaryHazardstbl, " where fld_srid='" + selectedhomeid + "' AND (fld_shzchk='1' OR (fld_shzchk='0' AND fld_triphz<>''))");
			 if(C_SHC.getCount()>0)
			 {
				 bolsh=true;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return bolsh;
	}
	public boolean GCH_FPValidation() {
		// TODO Auto-generated method stub
		boolean bolfp = false;
		CreateARRTable(45);
		try
		{
			Cursor C_FP= SelectTablefunction(GCH_FireProtecttbl, " where fld_srid='" + selectedhomeid + "' AND (fld_fpschk='1' OR (fld_fpschk='0' AND fld_size<>''))");
			if(C_FP.getCount()>0)
			{
				bolfp = true;
			}
		}catch (Exception e) {
			// TODO: handle exception
		}
		return bolfp;
	}
	public boolean GCH_SPHValidation() {
		// TODO Auto-generated method stub
		boolean bolsph = false;
		CreateARRTable(46);
		try
		{
			Cursor C_SH= SelectTablefunction(GCH_SpecHazardstbl, " where fld_srid='" + selectedhomeid + "' AND (fld_shzchk='1' OR (fld_shzchk='0' AND fld_fgases<>''))");
			if(C_SH.getCount()>0)
			{
				bolsph = true;
			}
			
		}catch (Exception e) {
			// TODO: handle exception
		}
		return bolsph;
	}
	public boolean GCH_HVValidation() {
		// TODO Auto-generated method stub
		boolean bolhv = false;
		CreateARRTable(47);
		try
		{
			Cursor C_HV= SelectTablefunction(GCH_Hvactbl, " where fld_srid='" + selectedhomeid + "' AND (fld_hvchk='1' OR (fld_hvchk='0' AND fld_type<>''))");
			if(C_HV.getCount()>0)
			{
				bolhv = true;
			}
		}catch (Exception e) {
			// TODO: handle exception
		}
		return bolhv;
	}
	public boolean GCH_ELEValidation() {
		// TODO Auto-generated method stub
		boolean bolele = false;
		CreateARRTable(48);
		try
		{
			Cursor C_EL= SelectTablefunction(GCH_Electrictbl, " where fld_srid='" + selectedhomeid + "' AND (fld_elecchk='1' OR (fld_elecchk='0' AND fld_mdisconnect<>''))");
			if(C_EL.getCount()>0)
			{
				bolele = true;
			}
		}catch (Exception e) {
			// TODO: handle exception
		}
		return bolele;
	}
	public boolean GCH_HDValidation() {
		// TODO Auto-generated method stub
		boolean bolhd = false;
		CreateARRTable(416);
		try
		{
			Cursor c1 =arr_db.rawQuery("SELECT * FROM " + GCH_HoodDuct +"  where fld_srid='"+selectedhomeid+"' AND (fld_hoodduct_na='1' OR (fld_hoodduct_na='0' AND fld_hoodandductwork<>''))",null);
			if(c1.getCount()>0)
			{
				bolhd = true;	
			}
			else
			{
				bolhd = false;	
			}
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		
		return bolhd;
	}
	public boolean GCH_FPTValidation() {
		// TODO Auto-generated method stub
		boolean bolfpt = false;
		CreateARRTable(417);
		try
		{
			Cursor C_NFP =arr_db.rawQuery("SELECT * FROM " + GCH_FireProtection +"  where fld_srid='"+selectedhomeid+"' AND (fld_fireprot_na='1' OR (fld_fireprot_na='0' AND fld_fireprotection<>''))",null);
			if(C_NFP.getCount()>0)
			{
				bolfpt = true;	
			}
			else
			{
				bolfpt = false;	
			}			
		}catch (Exception e) {
			// TODO: handle exception
		}
		return bolfpt;
	}
	public boolean GCH_MSValidation() {
		// TODO Auto-generated method stub
		boolean bolms = false;
		CreateARRTable(418);
		try
		{ 
			Cursor C_MS =arr_db.rawQuery("SELECT * FROM " + GCH_Misc +"  where fld_srid='"+selectedhomeid+"' AND (fld_misc_na='1' OR (fld_misc_na='0' AND fld_trash<>''))",null);
			if(C_MS.getCount()>0)
			{
				bolms = true;	
			}
			else
			{
				bolms = false;	
			}
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		return bolms;
	}
	public boolean GCH_RestValidation() {
		// TODO Auto-generated method stub
		boolean bolrest = false;
		CreateARRTable(418);
		try
		{ 
			Cursor C_RS =arr_db.rawQuery("SELECT * FROM " + Comm_RestSupplement +"  where fld_srid='"+selectedhomeid+"' AND insp_typeid='35'",null);
			Cursor C_App =arr_db.rawQuery("SELECT * FROM " + Comm_RestSuppAppl +"  where fld_srid='"+selectedhomeid+"' AND insp_typeid='35'",null);
			if(C_RS.getCount()>0 || C_App.getCount()>0)
			{
				bolrest = true;	
			}
			else
			{
				bolrest = false;	
			}
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		return bolrest;
	}
	public boolean GCH_COMValidation() {
		// TODO Auto-generated method stub
		boolean bolcom = false;
		CreateARRTable(49);
		try {
			Cursor C_Com= SelectTablefunction(GCH_commenttbl, " where fld_srid='" + selectedhomeid + "' ");
			if(C_Com.getCount()>0)
			{
				bolcom = true;
			}
			if(C_Com!=null)
				C_Com.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return bolcom;
	}
	public int RoofValidation(int i) {
		// TODO Auto-generated method stub
		CreateARRTable(99);
		int enable=0;
		try
		{
			
		//	Cursor C_RS= SelectTablefunction(General_roof_view, " WHERE RC_SRID='"+selectedhomeid+"'  and RC_insp_id='"+i+"'  and RC_Condition_type_order='11' and RC_Condition_type='"+encode("Comments")+"' ");
			Cursor C_RS= arr_db.rawQuery("SELECT R_ADD_saved FROM "+Roof_additional+" WHERE R_ADD_Ssrid='"+selectedhomeid+"'  and R_ADD_insp_id='"+i+"'  ",null);
			System.out.println("SELECT R_ADD_saved FROM "+Roof_additional+" WHERE R_ADD_Ssrid='"+selectedhomeid+"'  and R_ADD_insp_id='"+i+"'  "+"Count="+C_RS.getCount());
			if(C_RS.getCount()>0)
			{
				C_RS.moveToFirst();
				enable=C_RS.getInt(C_RS.getColumnIndex("R_ADD_saved"));
				//if()
			}
			if(C_RS!=null)
				C_RS.close();
		}catch (Exception e) {
			// TODO: handle exception
		}
		return enable;
	}
	public boolean SH_SummaryValidation() {
		// TODO Auto-generated method stub
		boolean bolsh = false;
		CreateARRTable(51);
		try
		{
			Cursor C_SS= SelectTablefunction(SINK_Summarytbl, " where fld_srid='" + selectedhomeid + "' ");
			if(C_SS.getCount()>0)
			{
				bolsh = true;
			}
		}catch (Exception e) {
			// TODO: handle exception
		}
		return bolsh;
	}
	public boolean SH_OBS1Validation() {
		// TODO Auto-generated method stub
		boolean bolobs1 = false;
		CreateARRTable(52);
		try
		{
			Cursor C_SOB1= SelectTablefunction(SINK_Obs1tbl, " where fld_srid='" + selectedhomeid + "' ");
			if(C_SOB1.getCount()>0)
			{
				bolobs1 = true;
				C_SOB1.moveToFirst();
				trval = C_SOB1.getString(C_SOB1.getColumnIndex("LargeTree"));
			}
		}catch (Exception e) {
			// TODO: handle exception
		}
		return bolobs1;
	}
	public boolean SH_OBS1TValidation() {
		// TODO Auto-generated method stub
		boolean	bolobs1t = false;
		CreateARRTable(56);
		try
		{
			Cursor C_SOB1T= SelectTablefunction(SINK_Obstreetbl, " where fld_srid='" + selectedhomeid + "' ");
			if(C_SOB1T.getCount()>0)
			{
				bolobs1t = true;
			}
			
		}catch (Exception e) {
			// TODO: handle exception
		}
		return bolobs1t;
	}
	public boolean SH_OBS2Validation() {
		// TODO Auto-generated method stub
		boolean	bolobs2 = false;
		CreateARRTable(53);
		try
		{
			Cursor C_SOB2= SelectTablefunction(SINK_Obs2tbl, " where fld_srid='" + selectedhomeid + "' ");
			if(C_SOB2.getCount()>0)
			{
				bolobs2 = true;
			}
		}catch (Exception e) {
			// TODO: handle exception
		}
		return bolobs2;
	}
	public boolean SH_OBS3Validation() {
		// TODO Auto-generated method stub
		boolean	bolobs3 = false;
		CreateARRTable(54);
		try
		{
			Cursor C_SOB3= SelectTablefunction(SINK_Obs3tbl, " where fld_srid='" + selectedhomeid + "' ");
			if(C_SOB3.getCount()>0)
			{
				bolobs3 = true;
			}
		}catch (Exception e) {
			// TODO: handle exception
		}
		return bolobs3;
	}
	public boolean SH_OBS4Validation() {
		// TODO Auto-generated method stub
		boolean	bolobs4 = false;
		CreateARRTable(55);
		try
		{
			Cursor C_SOB4= SelectTablefunction(SINK_Obs4tbl, " where fld_srid='" + selectedhomeid + "' ");
			if(C_SOB4.getCount()>0)
			{
				bolobs4 = true;
			}
		}catch (Exception e) {
			// TODO: handle exception
		}
		return bolobs4;
	}
	public boolean CD_sumValidation() {
		// TODO Auto-generated method stub
		boolean	bolsum = false;
		CreateARRTable(7);
		try
		{
			Cursor C_CDS= SelectTablefunction(DRY_sumcond, " where fld_srid='" + selectedhomeid + "' ");
			if(C_CDS.getCount()>0)
			{
				bolsum = true;
				C_CDS.moveToFirst();
				dcom = decode(C_CDS.getString(C_CDS.getColumnIndex("drywallcomments")));
			}
		}catch (Exception e) {
			// TODO: handle exception
		}
		return bolsum;
	}
	public boolean CD_roomValidation() {
		// TODO Auto-generated method stub
		boolean	bolrm = false;
		CreateARRTable(7);
		CreateARRTable(74);
		try
		{
			Cursor C_CDR= SelectTablefunction(DRY_room, " where fld_srid='" + selectedhomeid + "' ");
			if(C_CDR.getCount()>0)
			{
				Cursor C_CDM= arr_db.rawQuery("select assesmentcomments from " + DRY_sumcond + " where fld_srid='" + selectedhomeid + "'" , null);
				if(C_CDM.getCount()>0)
				{
					bolrm = true;
				}
				
			}
		}catch (Exception e) {
			// TODO: handle exception
		}
		return bolrm;
	}
	public boolean CD_hvacValidation() {
		// TODO Auto-generated method stub
		boolean	bolhvac = false;
		CreateARRTable(7);
		CreateARRTable(72);
		try
		{
			Cursor C_CDH= SelectTablefunction(DRY_hvac, " where fld_srid='" + selectedhomeid + "' ");
			if(C_CDH.getCount()>0)
			{
				Cursor C_CDHC= arr_db.rawQuery("select hvaccomments from " + DRY_sumcond + " where fld_srid='" + selectedhomeid + "'" , null);
				if(C_CDHC.getCount()>0)
				{
					bolhvac = true;
				}
				
			}
		}catch (Exception e) {
			// TODO: handle exception
		}
		return bolhvac;
	}
	public boolean CD_appValidation() {
		// TODO Auto-generated method stub
		boolean	bolapp = false;
		CreateARRTable(7);
		CreateARRTable(73);
		try
		{
			Cursor C_CDA= SelectTablefunction(DRY_appliance, " where fld_srid='" + selectedhomeid + "' ");
			if(C_CDA.getCount()>0)
			{
				Cursor C_CDAC= arr_db.rawQuery("select applianccomments from " + DRY_sumcond + " where fld_srid='" + selectedhomeid + "'" , null);
				if(C_CDAC.getCount()>0)
				{
					bolapp = true;
				}
				
			}
		}catch (Exception e) {
			// TODO: handle exception
		}
		return bolapp;
	}
	public boolean CD_attcValidation() {
		// TODO Auto-generated method stub
		boolean	bolattc = false;
		CreateARRTable(7);
		CreateARRTable(71);
		try
		{
			Cursor C_CDAT= SelectTablefunction(DRY_attic, " where fld_srid='" + selectedhomeid + "' ");
			if(C_CDAT.getCount()>0)
			{
				Cursor C_CDATC= arr_db.rawQuery("select atticcomments from " + DRY_sumcond + " where fld_srid='" + selectedhomeid + "'" , null);
				if(C_CDATC.getCount()>0)
				{
					bolattc = true;
				}
				
			}
		}catch (Exception e) {
			// TODO: handle exception
		}
		return bolattc;
	}
	public void CommercialComents(String selectedhomeid,int i,int iden)
	{
		try
		{
			communalcomments="";auxcomments="";restsuppcomments="";communalcomments="";
			if(iden==1)
			{
				curcomments = SelectTablefunction(Wind_Commercial_Comm, " where fld_srid='" + selectedhomeid + "'");	
			}
			else
			{
				curcomments = SelectTablefunction(Wind_Commercial_Comm, " where fld_srid='" + selectedhomeid + "' and insp_typeid='"+i+"'");
			}
			
			if(curcomments.getCount()>0)
			{
				 curcomments.moveToFirst();
				 auxcomments = decode(curcomments.getString(curcomments.getColumnIndex("fld_auxillarycomments")));
				 restsuppcomments = decode(curcomments.getString(curcomments.getColumnIndex("fld_restsuppcomments")));	
				 communalcomments = decode(curcomments.getString(curcomments.getColumnIndex("fld_commercialareascomments")));
			}
		}
		catch(Exception e)
		{
			System.out.println("CommercialComents"+e.getMessage());
		}
	}
	public void Check_BI()
	{
		try
		{
		CreateARRTable(61);
		 C_GEN= SelectTablefunction(BI_General, " where fld_srid='" + selectedhomeid + "' ");
		 if(C_GEN.getCount()>0)
		 {		
			 
			 
			 C_GEN.moveToFirst();
			 Gendata_val = decode(C_GEN.getString(C_GEN.getColumnIndex("BI_INS_NEI_OCC_NOB_NOS_NOU_BS")));
			 YOC = decode(C_GEN.getString(C_GEN.getColumnIndex("BI_YOC")));
			 Permit_Confirmed = decode(C_GEN.getString(C_GEN.getColumnIndex("BI_PERM_CONFIRMED")));
			 Balcony_present = decode(C_GEN.getString(C_GEN.getColumnIndex("BI_BALCONYPRES")));
			 Inci_Occu = decode(C_GEN.getString(C_GEN.getColumnIndex("BI_INCI_OCCU")));
			 Addi_Stru = decode(C_GEN.getString(C_GEN.getColumnIndex("BI_ADDI_STRU")));		 
			 BOCCU_SURV= decode(C_GEN.getString(C_GEN.getColumnIndex("BI_BOCCU_STYPE")));
			 SCOPE= decode(C_GEN.getString(C_GEN.getColumnIndex("BI_SCOPE")));	
			 Loc_NA = C_GEN.getString(C_GEN.getColumnIndex("BI_LOC_NA"));
			
			 Loc_val = decode(C_GEN.getString(C_GEN.getColumnIndex("BI_LOC_DESC")));
			 OBS_NA = C_GEN.getString(C_GEN.getColumnIndex("BI_OBSERV_NA"));System.out.println("OBS_NA="+OBS_NA);
			 Observ1 = decode(C_GEN.getString(C_GEN.getColumnIndex("BI_OSERV1")));
			 Observ2 = decode(C_GEN.getString(C_GEN.getColumnIndex("BI_OSERV2")));
			 Observ3 = decode(C_GEN.getString(C_GEN.getColumnIndex("BI_OSERV3")));
			 Observ4 = decode(C_GEN.getString(C_GEN.getColumnIndex("BI_OSERV4")));
			 Observ5 = decode(C_GEN.getString(C_GEN.getColumnIndex("BI_OSERV5")));
			 ISO_NA = C_GEN.getString(C_GEN.getColumnIndex("BI_ISO_NA"));
			 Iso_val = decode(C_GEN.getString(C_GEN.getColumnIndex("BI_ISO")));
			 Foun_NA = C_GEN.getString(C_GEN.getColumnIndex("BI_FOUN_NA"));
			 Foundation_val = decode(C_GEN.getString(C_GEN.getColumnIndex("BI_FOUN")));			 
			 WS_NA = C_GEN.getString(C_GEN.getColumnIndex("BI_WS_NA"));
			 Wallstru_val = decode(C_GEN.getString(C_GEN.getColumnIndex("BI_WALLSTRUPER")));			 
			 WS_Stories = decode(C_GEN.getString(C_GEN.getColumnIndex("BI_WALLSTRU")));
			 WS_Other = decode(C_GEN.getString(C_GEN.getColumnIndex("BI_WALLSTRUC_OTHER")));
			 WC_NA = C_GEN.getString(C_GEN.getColumnIndex("BI_WC_NA"));
			 Wallclad_val = decode(C_GEN.getString(C_GEN.getColumnIndex("BI_WALLCLAD")));
			 WC_Other = decode(C_GEN.getString(C_GEN.getColumnIndex("BI_WALLCLAD_OTHER")));
			 BISEP_NA = C_GEN.getString(C_GEN.getColumnIndex("BI_BUILDSEP_NA"));
			 Buildsep_val = decode(C_GEN.getString(C_GEN.getColumnIndex("BI_BUILD_SEP")));			 
			 comments = decode(C_GEN.getString(C_GEN.getColumnIndex("BI_BUILD_COMM")));
			 GEN_NA = C_GEN.getString(C_GEN.getColumnIndex("BI_GEN_NA"));
		 }
		}
		catch(Exception e)
		{
			System.out.println("Check_BI"+e.getMessage());
		}
	}

	
	public boolean photovalidation()
	{
		/***Start checking the photos **/
		
		CreateARRTable(81);
		Cursor cs=SelectTablefunction(Select_insp, " Where SI_srid='"+selectedhomeid+"'");
		cs.moveToFirst();
		String insp1="",elev="";
		getPolicyholderInformation(selectedhomeid);
		if(cs.getCount()>0)
		{
			String sel_insp=decode(cs.getString(cs.getColumnIndex("SI_InspectionNames")));
			if(sel_insp.contains("4"))
			{
				if(Stories.equals("4") || Stories.equals("5")  || Stories.equals("6"))
				{
					sel_insp=sel_insp.replace("4", "5");		
				}
				else if(Integer.parseInt(Stories)>=7)
				{	
					sel_insp=sel_insp.replace("4", "6");		
				}
			}
			
		String sql="Select distinct ARR_IM_Insepctiontype,ARR_IM_Elevation from "+ImageTable+" Where  ARR_IM_SRID='"+selectedhomeid+"' and ARR_IM_Elevation<>'0'";
		Cursor C_photos= arr_db.rawQuery(sql, null);		
		C_photos.moveToFirst();
		sel_insp=sel_insp.replace("^", ",");
		String[] inspe_list=sel_insp.split(",");
		System.out.println("add="+C_photos.getCount());
		if(C_photos.getCount()>0)
		{
			for(int j=0;j<inspe_list.length;j++)
			{
				
				C_photos.moveToFirst();
				Boolean b=false;
				
				for(int i=0;i<C_photos.getCount();i++,C_photos.moveToNext())
				{
					if(inspe_list[j].trim().equals(C_photos.getString(C_photos.getColumnIndex("ARR_IM_Insepctiontype"))))
					{
						if(!C_photos.getString(C_photos.getColumnIndex("ARR_IM_Elevation")).equals("23"))
						{
							b=true;
								
						}						
					}
					
				}
				System.out.println("b="+b);
				if(!b)
				{
					inspectioname=All_insp_list[Integer.parseInt(inspe_list[j])];
					return false;
				}	
			}
		}
		else
		{
			System.out.println("addelse");
			inspectioname=All_insp_list[Integer.parseInt(inspe_list[0])];
			System.out.println("inspection="+inspectioname);
			
			return false;
		}
			 
		}
		else
		{
			return false;
		}
		/***End checking the photos **/
		return true;
		
	}
	public boolean frontpdfvalidation()
	{
		String elev="";
		Cursor cs=SelectTablefunction(Select_insp, " Where SI_srid='"+selectedhomeid+"'");
		cs.moveToFirst();
		String insp1="";
		Boolean b=false;
		if(cs.getCount()>0)
		{
			String sel_insp=decode(cs.getString(cs.getColumnIndex("SI_InspectionNames")));
			if(sel_insp.contains("4"))
			{
				if(Stories.equals("4") || Stories.equals("5")  || Stories.equals("6"))
				{
					sel_insp=sel_insp.replace("4", "5");		
				}
				else if(Integer.parseInt(Stories)>=7)
				{	
					sel_insp=sel_insp.replace("4", "6");		
				}
			}
			
			sel_insp=sel_insp.replace("^", ",");
			String[] inspe_list=sel_insp.split(",");
			
			
			String sql="Select  ARR_IM_Insepctiontype,ARR_IM_Elevation from "+ImageTable+" Where ARR_IM_SRID='"+selectedhomeid+"' and ARR_IM_Elevation='23'";
			Cursor C_photos= arr_db.rawQuery(sql, null);			
			C_photos.moveToFirst();
			if(C_photos.getCount()>0)
			{
				for(int j=0;j<inspe_list.length;j++)
				{
				
					Boolean c=false;
					C_photos.moveToFirst();
					
					for(int i=0;i<C_photos.getCount();i++,C_photos.moveToNext())
					{
						if(inspe_list[j].trim().equals(C_photos.getString(C_photos.getColumnIndex("ARR_IM_Insepctiontype"))))
						{
							if(C_photos.getString(C_photos.getColumnIndex("ARR_IM_Elevation")).equals("23"))
							{
								c=true;	
							}	
						}
						
					}
					
					if(!c)
					{
						inspectioname=All_insp_list[Integer.parseInt(inspe_list[j])];
						
						return false;
					}	
				}
				}
			else
			{
				inspectioname=All_insp_list[Integer.parseInt(inspe_list[0])];
				
				return false;
			}
		}
		else
		{
			return false;
		}
		return true;
		
	}
	public boolean feedvalidation()
	{
		/***Start checking the photos **/
		CreateARRTable(83);
		Cursor cs=SelectTablefunction(Select_insp, " Where SI_srid='"+selectedhomeid+"'");
		cs.moveToFirst();
		String insp1="";
		if(cs.getCount()>0)
		{
			getPolicyholderInformation(selectedhomeid);
			String sel_insp=decode(cs.getString(cs.getColumnIndex("SI_InspectionNames")));
			if(sel_insp.contains("4"))
			{
				if(Stories.equals("4") || Stories.equals("5")  || Stories.equals("6"))
				{
					sel_insp=sel_insp.replace("4", "5");		
				}
				else if(Integer.parseInt(Stories)>=7)
				{	
					sel_insp=sel_insp.replace("4", "6");		
				}
			}
			
	
		String sql="Select distinct ARR_FI_Inspection from "+FeedBackInfoTable+" Where ARR_FI_Srid='"+selectedhomeid+"'";
		Cursor C_photos= arr_db.rawQuery(sql, null);
		C_photos.moveToFirst();
		sel_insp=sel_insp.replace("^", ",");
		String[] inspe_list=sel_insp.split(",");
		if(C_photos.getCount()>0)
		{
			for(int j=0;j<inspe_list.length;j++)
			{
				C_photos.moveToFirst();
				Boolean b=false;
				for(int i=0;i<C_photos.getCount();i++,C_photos.moveToNext())
				{
					if(inspe_list[j].trim().equals(C_photos.getString(C_photos.getColumnIndex("ARR_FI_Inspection"))))
					{
						b=true;
					}
					
				}
				System.out.println("b="+b);
				if(!b)
				{
					inspectioname=All_insp_list[Integer.parseInt(inspe_list[j])];
					
					return false;
				}	
			}
			}
		else
		{
			inspectioname=All_insp_list[Integer.parseInt(inspe_list[0])];
			
			return false;
		}
			 
		}
		else
		{
			return false;
		}/***End checking the photos **/
		return true;
		
	}


	public void Check_B1802(String selectedhomeid,int i,int iden) {
		// TODO Auto-generated method stub
		boolean chkcomm=false;
		try
		{
			
			String elevation[];
			CreateARRTable(32);CreateARRTable(312);CreateARRTable(62);CreateARRTable(63);CreateARRTable(64);CreateARRTable(34);
			
			if(iden==1)
			{
				qc = SelectTablefunction(Wind_QuesCommtbl, " where fld_srid='" + selectedhomeid + "'");	
			}
			else
			{
				qc = SelectTablefunction(Wind_QuesCommtbl, " where fld_srid='" + selectedhomeid + "'  and insp_typeid='"+i+"'");
			}
			
			if(qc.getCount()>0)
			{
				qc.moveToFirst();
					 bccomments = qc.getString(qc.getColumnIndex("fld_buildcodecomments"));
					 rccomments = qc.getString(qc.getColumnIndex("fld_roofcovercomments"));
					 rdcomments = qc.getString(qc.getColumnIndex("fld_roofdeckcomments"));
					 rwcomments = qc.getString(qc.getColumnIndex("fld_roofwallcomments"));
					 rgcomments = qc.getString(qc.getColumnIndex("fld_roofgeometrycomments"));
					 swrcomments = qc.getString(qc.getColumnIndex("fld_swrcomments"));
					 opcomments = qc.getString(qc.getColumnIndex("fld_openprotectioncomments"));
					 wccomments = qc.getString(qc.getColumnIndex("fld_wallconscomments"));
				
			}
			if(iden==1)
			{			
				wallcons =SelectTablefunction(WindMit_WallCons, " where fld_srid='"+selectedhomeid+"'");
			}
			else
			{
				wallcons =SelectTablefunction(WindMit_WallCons, " where fld_srid='"+selectedhomeid+"' and insp_typeid='"+i+"'");
			}
			
			if(iden==1)
			{			
				windopen = SelectTablefunction(BI_Windopenings, " where fld_srid='" + selectedhomeid + "'");
			}
			else
			{
				windopen = SelectTablefunction(BI_Windopenings, " where fld_srid='" + selectedhomeid + "' and insptypeid='"+i+"'");
			}
			
			if(iden==1)
			{			
				 dooropen =  SelectTablefunction(BI_Dooropenings, " where fld_srid='"+selectedhomeid+"'  Group by fld_elevation");
			}
			else
			{
				 dooropen =  SelectTablefunction(BI_Dooropenings, " where fld_srid='"+selectedhomeid+"'  and insptypeid='"+i+"' Group by fld_elevation");
			}
			
			
			
			 if(dooropen.getCount()>0)
			 {
				dooropen.moveToFirst();
			    elevation = new String[dooropen.getCount()];
				int l=0;
				 do {
					 elevation[l] = dooropen.getString(dooropen.getColumnIndex("fld_elevation"));
					 for(int j=0;j<spnelevation.length;j++)
					 {
						 if(spnelevation[j].equals(elevation[l]))
						 {
							 ressupchk1++;
						 }
					 }
					  l++;
				} while (dooropen.moveToNext());
			}
			 
			 if(iden==1)
			 {
				 skylightsopen = SelectTablefunction(BI_Skylights, " where fld_srid='" + selectedhomeid + "'");	 
			 }
			 else
			 {
				 skylightsopen = SelectTablefunction(BI_Skylights, " where fld_srid='" + selectedhomeid + "' and insptypeid='"+i+"'");
			 }
			 
			 if(iden==1)
			 {
				 curna = SelectTablefunction(WindDoorSky_NA, " where fld_srid='" + selectedhomeid + "'");	 
			 }
			 else
			 {
				 curna = SelectTablefunction(WindDoorSky_NA, " where fld_srid='" + selectedhomeid + "' and insp_typeid='"+i+"'"); 
			 }
			if(curna.getCount()>0)
			{
				curna.moveToFirst();
				WindOpen_NA =curna.getString(curna.getColumnIndex("fld_windowopenNA"));
				DoorOpen_NA = curna.getString(curna.getColumnIndex("fld_dooropenNA"));
				Skylights_NA = curna.getString(curna.getColumnIndex("fld_skylightsNA"));
			}
			
			/* if(!bccomments.equals("") || !rccomments.equals("") || !rdcomments.equals("") || !rwcomments.equals("") || !rgcomments.equals("")
					 || !swrcomments.equals("") || !opcomments.equals("") || !wccomments.equals("") || qc.getCount()>0 || wallcons.getCount()>0
				 || windopen.getCount()>0 || dooropen.getCount()>0 ||  skylightsopen.getCount()>0 || curna.getCount()>0)
			 {
					chkcomm=true;
			 }
			 else
			 {
					chkcomm=false;
			 }*/
			
		}
		catch(Exception e)
		{
			System.out.println("Check_B1802 error"+e.getMessage());
		}
		System.out.println("Check_B1802"+chkcomm);
		//return chkcomm;
	}


	public boolean Check_Comm1(String selectedhomeid,int i,int iden)
	{
		boolean chkcomm=false;
		try
		{
			CreateARRTable(39);
			if(iden==2)
			{
				curcomm = SelectTablefunction(Communal_Areas, " where fld_srid='" + selectedhomeid + "' and insp_typeid='"+i+"'");
			}
			else
			{
				curcomm = SelectTablefunction(Communal_Areas, " where fld_srid='" + selectedhomeid + "'");
			}
			System.out.println("curcom3="+curcomm.getCount());
			if(curcomm.getCount()>0)
			{
				curcomm.moveToFirst();
				if((!decode(curcomm.getString(curcomm.getColumnIndex("fld_lobby"))).equals("") || curcomm.getString(curcomm.getColumnIndex("fld_lobby_NA")).equals("1"))  
					&& (!decode(curcomm.getString(curcomm.getColumnIndex("fld_stairwells"))).equals("") || curcomm.getString(curcomm.getColumnIndex("fld_stairwell_NA")).equals("1"))
						&& (!decode(curcomm.getString(curcomm.getColumnIndex("fld_elevatorsoption"))).equals("") || curcomm.getString(curcomm.getColumnIndex("fld_elevators_NA")).equals("1"))
						&& (!decode(curcomm.getString(curcomm.getColumnIndex("fld_poolareas"))).equals("") || curcomm.getString(curcomm.getColumnIndex("fld_poolareas_NA")).equals("1")))
				{
					chkcomm=true;
				}
				else
				{
					chkcomm=false;
				}
			}	
			else
			{
				chkcomm=false;
			}			
		}
		catch(Exception e)
		{
			System.out.println("Check_Comm2"+e.getMessage());
		}
		return chkcomm;	
	}
	public void Check_Comm2(String selectedhomeid, int i,int iden)
	{
		try
		{
			Aux_NA="";Rest_NA="";
			CreateARRTable(40);CreateARRTable(35);CreateARRTable(36);CreateARRTable(37);CreateARRTable(38);CreateARRTable(355);
			if(iden==1)
			{
				curcomm2 = SelectTablefunction(Commercial_Wind, " where fld_srid='" + selectedhomeid + "'");	
			}
			else
			{
				curcomm2 = SelectTablefunction(Commercial_Wind, " where fld_srid='" + selectedhomeid + "'  and insp_typeid='"+i+"'");
			}
			
			if(curcomm2.getCount()>0)
			{
				curcomm2.moveToFirst();
				 terrainval = curcomm2.getString(curcomm2.getColumnIndex("fld_terrain"));
				 rcvalue = curcomm2.getString(curcomm2.getColumnIndex("fld_roofcoverings"));
				 rdvalue = curcomm2.getString(curcomm2.getColumnIndex("fld_roofdeck"));
				 swrvalue = curcomm2.getString(curcomm2.getColumnIndex("fld_swr"));
				 opvalue = curcomm2.getString(curcomm2.getColumnIndex("fld_openingprotection"));
				 certificationval = curcomm2.getString(curcomm2.getColumnIndex("fld_certfication"));
			}
			if(iden==1)
			{
				curaux = SelectTablefunction(Comm_AuxBuilding, " where fld_srid='" + selectedhomeid + "'");
			}
			else
			{
				curaux = SelectTablefunction(Comm_AuxBuilding, " where fld_srid='" + selectedhomeid + "'  and insp_typeid='"+i+"'");
			}
			
			if(iden==1)
			{
				curapppl = SelectTablefunction(Comm_RestSuppAppl, " where fld_srid='" + selectedhomeid + "' ");
			}
			else
			{
				curapppl = SelectTablefunction(Comm_RestSuppAppl, " where fld_srid='" + selectedhomeid + "'  and insp_typeid='"+i+"' ");
			}
			
			if(iden==1)
			{
				 C_auxNA =SelectTablefunction(Comm_Aux_Master, " where fld_srid='" + selectedhomeid + "'"); 
			}
			else
			{
				 C_auxNA =SelectTablefunction(Comm_Aux_Master, " where fld_srid='" + selectedhomeid + "'  and insp_typeid='"+i+"'"); 
			}
			
			
			
			if(C_auxNA.getCount()>0)
			{
				C_auxNA.moveToFirst();
				Aux_NA = C_auxNA.getString(C_auxNA.getColumnIndex("fld_aux_NA"));			
			}
			
			if(iden==1)
			{
				C_RestNA =SelectTablefunction(Comm_RestSupplement, " where fld_srid='" + selectedhomeid + "'"); 
			}
			else
			{
				C_RestNA =SelectTablefunction(Comm_RestSupplement, " where fld_srid='" + selectedhomeid + "' and insp_typeid='"+i+"'"); 
			}
			
			
			if(C_RestNA.getCount()>0)
			{
				C_RestNA.moveToFirst();
				Rest_NA = C_RestNA.getString(C_RestNA.getColumnIndex("fld_appliance_na"));		
			}
		}
		catch(Exception e)
		{
			System.out.println("Check_Comm2"+e.getMessage());
		}
	}
	public void show_ProgressDialog(String string) {
	// TODO Auto-generated method stub
	String source = "<b><font color=#00FF33>"+string+" . Please wait...</font></b>";
	pd = ProgressDialog.show(this.con, "", Html.fromHtml(source), true);
    }
    
	
	

	public boolean CheckDupBNo(String buildingno1,String flag) {
 		// TODO Auto-generated method stub
  		boolean bchk1 = false;
  		if(buildingno1.equals(""))
		{
			bchk1=true;
		}
		else
		{
			Cursor c1= SelectTablefunction(Comm_Building, " where Commercial_Flag='"+flag+"' and BUILDING_NO='"+buildingno1+"'");
			if(c1.getCount()==0)
			{
				bchk1=true;
			}
			else
			{
				bchk1=false;
			}
  		}
  		System.out.println("bchk1="+bchk1);
  		return bchk1;
 	}
	
  

  	public void getcommercialbuilno(String flag)
	{
		CreateARRTable(70);
		Cursor c3 =arr_db.rawQuery("select count(*) as a from  "+Comm_Building+" where BUILDING_NO<>'' and Commercial_Flag='"+flag+"'", null);
		c3.moveToFirst();
		buildingno1 = c3.getInt(c3.getColumnIndex("a"));
		
	}
  	public boolean chkcomm1(int buildingno1,String flag) {
		// TODO Auto-generated method stub
		  if(buildingno1!=0)
 		  {
 			Cursor c1 =arr_db.rawQuery("select * from  "+Comm_Building+" where BUILDING_NO<>'' and Commercial_Flag='"+flag+"'", null);
 			 if(c1.getCount()>0)
				 {
					c1.moveToFirst();
					int i=0;
					arrbuilsrid1=new String[buildingno1];
					do
					{						
						arrbuilsrid1[i] = c1.getString(c1.getColumnIndex("fld_srid"));System.out.println("AAA"+arrbuilsrid1[i]);
						if(chkrecordssavedcomm1(arrbuilsrid1[i],32,1)==true)
						{
							chk1 += "true"+"~";							
						}
						/*else
						{
							chkcommercial[0]=false;
						}	*/
						i++;
					}while(c1.moveToNext());
					System.out.println("chk1="+chk1);
					if(chk1.contains("true"))
			    	  {
						chkcommercial[0]=true; 
			    	  }
			    	  else
			    	  {
			    		  chkcommercial[0]=false;
			    	  }
				 }
				 else
				 {
					 chkcommercial[0]=true;
				 }	
 		  }
    	  else
    	  {
    		  chkcommercial[0]=true;
    	  }
    	  System.out.println("To check value saved for atleast one srid in commercial I "+chkcommercial[0]);
    	return chkcommercial[0];
    	  
    	
	}
	public boolean chkrecordssavedcomm1(String srid, int i,int iden) 
    {
		// TODO Auto-generated method stub
		System.out.println("first time");
		  Check_B1802(srid,i,iden);
		  //System.out.println("second time"+chkcomm);  
  	  //Check_B1802(srid, i,iden);
  	  CommercialComents(srid,i,iden);
  	  Check_Addencomments(srid,iden,i);
  	  Roofchecksaved(srid,i);	
 			System.out.println("qc.getCount()"+qc.getCount()+"wll="+wallcons.getCount());
  	  
 			if((qc.getCount()>0 || wallcons.getCount()>0) || windopen.getCount()>0 || dooropen.getCount()>0 || 
 				 skylightsopen.getCount()>0 || curna.getCount()>0 || (Roof_Add.getCount()>0 || Roof_RCT.getCount()>0 || Roof_RCN.getCount()>0) ||
 				 !communalcomments.equals("") || curaddendum.getCount()!=0)
 			{
	   			b1=true;
 			}
 			else
 			{
 				b1=false;
 			}
 			System.out.println("chkrecordsave1="+b1);
 			return b1;
    }
	public void Roofchecksaved(String srid,int i)
	{
		if(i==31)
		{
			i=4;
		}
		else if(i==32)
		{
			i=5;
		}
		else if(i==34)
		{
			i=6;
		}
		CreateARRTable(431);
		Roof_Add= SelectTablefunction(Roof_additional, " WHERE  R_ADD_Ssrid='"+srid+"'");
		
		
		Roof_RCT= SelectTablefunction(Rct_table, " WHERE  RCT_Ssrid='"+srid+"'");
		
		
		Roof_RCN= SelectTablefunction(RCN_table, " WHERE  R_RCN_Ssrid='"+srid+"'");	
		
	}
	
	public void Check_Addencomments(String srid,int iden,int i) {
		// TODO Auto-generated method stub
		CreateARRTable(50);
		if(iden==1)
		{
			curaddendum = SelectTablefunction(Addendum, " where fld_srid='" + srid + "'");
		}
		else
		{
			
			curaddendum = SelectTablefunction(Addendum, " where fld_srid='" + srid + "'  and insp_typeid='"+i+"'");
		}
	}
	public boolean chkcomm2(int buildingno2,String flag) {

		// TODO Auto-generated method stub
		if(buildingno2!=0)
		 {
			Cursor c1 =arr_db.rawQuery("select * from  "+Comm_Building+" where BUILDING_NO<>'' and Commercial_Flag='"+flag+"'", null);
			
				 if(c1.getCount()>0)
				 {
					c1.moveToFirst();
					int i=0;
					arrbuilsrid2=new String[buildingno2];
					do
					{
						arrbuilsrid2[i] = c1.getString(c1.getColumnIndex("fld_srid"));
						if(chkrecordssavedcomm2(arrbuilsrid2[i],33,1)==true)
						{
							chk2 += "true"+"~";							
						}
						i++;
					}while(c1.moveToNext());
					  
					 if(chk2.contains("true"))
				   	  {
				   		  chkcommercial[1]=true; 
				   	  }
				   	  else
				   	  {
				   		  chkcommercial[1]=false;
				   	  }
				 }
				 else
				 {
					 chkcommercial[1]=true;
				 }	
		  }
	  	  else
	      {
	  		  chkcommercial[1]=true;
	      }
		return chkcommercial[1];
	}
	
	  public boolean chkrecordssavedcomm2(String srid, int i,int iden) 
      {
		// TODO Auto-generated method stub
		  
	  	  boolean chkcomm = Check_Comm1(srid,33,iden);
	  	  Check_Comm2(srid,33,iden);
	  	  Roofchecksaved(srid,i);
	  	  Check_Addencomments(srid,iden,i);	
	  	  
	  	  if(chkcomm==true || !communalcomments.equals("") || curaux.getCount()!=0 || curapppl.getCount()!=0 || Aux_NA.equals("1") ||
	  				 Rest_NA.equals("1") || curcomm2.getCount()>0 || curaddendum.getCount()!=0 || (Roof_Add.getCount()>0 || Roof_RCT.getCount()>0 || Roof_RCN.getCount()>0) )
		  {
	  		 b2=true;
		  }
	  	  else
	  	  {
	  		 b2=false;
	  	  }
	  	  
	  	  return b2;
    }
	
	  public boolean chkcomm3(int buildingno3,String flag) {

		// TODO Auto-generated method stub
  	  	if(buildingno3!=0)
		 {
    		Cursor c1 =arr_db.rawQuery("select * from  "+Comm_Building+" where BUILDING_NO<>'' and Commercial_Flag='"+flag+"'", null);
				 if(c1.getCount()>0)
				 {
					c1.moveToFirst();
					int i=0;
					arrbuilsrid3=new String[buildingno3];
					do
					{
						arrbuilsrid3[i] = c1.getString(c1.getColumnIndex("fld_srid"));
						if(chkrecordssavedcomm3(arrbuilsrid3[i],34,1)==true)
						{
							chk3 += "true"+"~";							
						}
						else
						{
							chkcommercial[2]=false;
						}	
						i++;
					}while(c1.moveToNext());
					if(chk3.contains("true"))
				   	  {
				   		  chkcommercial[2]=true; 
				   	  }
				   	  else
				   	  {
				   		  chkcommercial[2]=false;
				   	  }
				 }
				 else
				 {
					 chkcommercial[2]=true;
				 }	
		 }

   	  else
   	  {
   		  chkcommercial[2]=true;
   	  } System.out.println("bchkcommercial[2]2="+chkcommercial[2]);
  	 	return chkcommercial[2];
	}
    public boolean chkrecordssavedcomm3(String srid, int i,int iden) 
    {
    	
		// TODO Auto-generated method stub
  	  boolean chkcomm = Check_Comm1(srid,34,iden);
  	  Check_Comm2(srid,34,iden);
  	 Check_Addencomments(srid,iden,i);
  	Roofchecksaved(srid,i);
  	
  		
	  	  if(chkcomm==true || !communalcomments.equals("") || curaux.getCount()!=0 || curapppl.getCount()!=0 || Aux_NA.equals("1") ||
	  				 Rest_NA.equals("1") || curcomm2.getCount()>0 || curaddendum.getCount()!=0 || (Roof_Add.getCount()>0 || Roof_RCT.getCount()>0 || Roof_RCN.getCount()>0))
		  {
	  		 b3=true;
		  }
	  	  else
	  	  {
	  		 b3=false;
	  	  }
	  	  
  	  return b3;
    } 
  
    public void AddendumComments(int i, String homeid) {
		// TODO Auto-generated method stub
    	Cursor c1 = arr_db.rawQuery("SELECT * FROM " + Addendum + " WHERE fld_srid='"+ homeid + "'", null);		
  		if(c1.getCount()>0)
 		{
 			c1.moveToFirst();
 			Cursor c11 = arr_db.rawQuery("SELECT * FROM " + Addendum + " WHERE fld_srid='"+ selectedhomeid + "'", null); 				
 			if(c11.getCount()==0)
			{
 					arr_db.execSQL("INSERT INTO " + Addendum + " (fld_srid,insp_typeid,addendumcomment)"
 		 					+ "VALUES ('"+selectedhomeid+"','"+i+"','"+c1.getString(3)+"')"); 				
 			}
 		}
	}

    public void insertbuildingno(String s,String flag,int typeid)
    {
    	try
    	{
    		getPolicyholderInformation(selectedhomeid);
    		Cursor curbuilding= SelectTablefunction(Comm_Building, " where fld_srid='"+selectedhomeid+"' and insp_typeid='"+typeid+"'");    		
			if(curbuilding.getCount()==0)
			{
				arr_db.execSQL("INSERT INTO " +Comm_Building + " (fld_srid,insp_typeid,BUILDING_NO,Commercial_Flag)"+ "VALUES ('"+selectedhomeid+"','"+typeid+"','"+s+"','"+flag+"')");	//changes done
			}
			else
			{
					arr_db.execSQL("UPDATE " +Comm_Building + " SET BUILDING_NO='"+s+"' WHERE fld_srid ='"+ selectedhomeid + "' and insp_typeid='"+typeid+"'");
			}
		}
    	catch(Exception e)
    	{
    		System.out.println("insertbuildingno"+e.getMessage());
    	}
			if(typeid==32){gotoclass(32,WindMitBuildCode.class);}
			else if(typeid==33){gotoclass(33,CommercialWind.class);}
			else if(typeid==34){gotoclass(34,CommercialWind.class);}
			else {nextscreen();}
    }
    public void createtbl() {
  		// TODO Auto-generated method stub
  		CreateARRTable(31);CreateARRTable(32);CreateARRTable(33);CreateARRTable(34);CreateARRTable(35);
  		CreateARRTable(36);CreateARRTable(37);CreateARRTable(38);CreateARRTable(39);CreateARRTable(311);
  		CreateARRTable(62);CreateARRTable(63);CreateARRTable(64);CreateARRTable(355);
  		
  	}
    public void getbuildingno(String flag) 
    {
		// TODO Auto-generated method stub
  	   try
		  {
				createtbl();
				bno1="";bno2="";bno3="";appendbno1="";appendbno2="";appendbno3="";
				Cursor curbuild = arr_db.rawQuery("select * from " + Comm_Building+" where Commercial_Flag='"+flag+"'", null);
				if(curbuild.getCount()>0)
				{
					curbuild.moveToFirst();
					do
					{
						if(!curbuild.getString(curbuild.getColumnIndex("BUILDING_NO")).equals(""))
						{
							bno1 += curbuild.getString(curbuild.getColumnIndex("BUILDING_NO"))+"~";	
						}
					}while(curbuild.moveToNext());
				}
			}
			catch(Exception e)
			{
				System.out.println("get buildingno "+e.getMessage());
			}
	  }
 	class Spin_Selectedlistener implements OnItemSelectedListener
  	{
  		int i;
  		public Spin_Selectedlistener(int i) {
  			// TODO Auto-generated constructor stub
  			this.i=i;
  		}

  		@Override
  		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,long arg3) {
  			// TODO Auto-generated method stub
  			
  				comm1txt = spn1.getSelectedItem().toString();
  		  		}

  		@Override
  		public void onNothingSelected(AdapterView<?> arg0) {
  			// TODO Auto-generated method stub
  			
  		}
  		
  	}
 	private void Communalareasduplication(int i, String homeid) {
 		// TODO Auto-generated method stub
 		try{
  		Cursor c1 = arr_db.rawQuery("SELECT * FROM " + Communal_Areas + " WHERE fld_srid='"+ homeid + "'", null);
  		System.out.println("SELECT * FROM " + Communal_Areas + " WHERE fld_srid='"+ homeid + "'");
  		if(c1.getCount()>0)
 		{
 			c1.moveToFirst();
 			Cursor c11 = arr_db.rawQuery("SELECT * FROM " + Communal_Areas + " WHERE fld_srid='"+ selectedhomeid + "'", null);	
 			System.out.println("222"+"SELECT * FROM " + Communal_Areas + " WHERE fld_srid='"+ selectedhomeid + "'"+"CC="+c11.getCount()
 					);
 			
 			if(c11.getCount()==0)
			{
 				System.out.println("INSERT INTO " + Communal_Areas + " (fld_srid,insp_typeid,fld_lobby,fld_lobby_NA,fld_stairwell_NA,fld_stairwells,fld_elevators_NA,fld_elevatorsoption,fld_poolareas_NA,fld_poolareasother,fld_poolareas)"
 					+ "VALUES ('"+selectedhomeid+"','"+i+"','"+c1.getString(4)+"','"+c1.getString(3)+"','"+c1.getString(5)+"','"+c1.getString(6)+"','"+c1.getString(7)+"','"+c1.getString(8)+"','"+c1.getString(9)+"','"+c1.getString(10)+"','"+c1.getString(11)+"')");
				arr_db.execSQL("INSERT INTO " + Communal_Areas + " (fld_srid,insp_typeid,fld_lobby,fld_lobby_NA,fld_stairwell_NA,fld_stairwells,fld_elevators_NA,fld_elevatorsoption,fld_poolareas_NA,fld_poolareasother,fld_poolareas)"
 					+ "VALUES ('"+selectedhomeid+"','"+i+"','"+c1.getString(4)+"','"+c1.getString(3)+"','"+c1.getString(5)+"','"+c1.getString(6)+"','"+c1.getString(7)+"','"+c1.getString(8)+"','"+c1.getString(9)+"','"+c1.getString(10)+"','"+c1.getString(11)+"')");
 			
			}
 		}	
 		
  		
 		Cursor c2 = arr_db.rawQuery("SELECT * FROM " + Wind_Commercial_Comm + " WHERE fld_srid='"+ homeid + "'", null);	
 		if(c2.getCount()>0)
 		{
 			c2.moveToFirst();
 			Cursor c12 = arr_db.rawQuery("SELECT * FROM " + Wind_Commercial_Comm + " WHERE fld_srid='"+ selectedhomeid + "' ", null);
 			if(c12.getCount()==0)
 	 		{
 	 			arr_db.execSQL("INSERT INTO "
 					+ Wind_Commercial_Comm
 					+ " (fld_srid,insp_typeid,fld_commercialareascomments,fld_auxillarycomments,fld_restsuppcomments)"
 					+ "VALUES ('" + selectedhomeid + "','"+i+"','"+c2.getString(3)+"','"+c2.getString(4)+"','"+c2.getString(5)+"')");
 	 		}
 		}
 		
 		Cursor c3 = arr_db.rawQuery("SELECT * FROM " + Communal_AreasElevation + " WHERE fld_srid='"+ homeid + "' ", null); // Old SRID
 		if(c3.getCount()>0)
 		{
 			c3.moveToFirst();
 			Cursor c13 = arr_db.rawQuery("SELECT * FROM " + Communal_AreasElevation + " WHERE fld_srid='"+ selectedhomeid + "'", null);//new SRID
 			System.out.println("test="+c13.getCount());
 			if(c13.getCount()==0)
 	 		{
 				for(int j=0;j<c3.getCount();j++,c3.moveToNext())
	 			{
	 				 id = c3.getString(c3.getColumnIndex("Commid"));
	 				 eno = c3.getString(c3.getColumnIndex("fld_elevationno"));
	 				 model = c3.getString(c3.getColumnIndex("fld_model"));
	 				 manufacturer= c3.getString(c3.getColumnIndex("fld_manufacturer"));
	 				 image = c3.getString(c3.getColumnIndex("fld_uploadimg"));
	 				 dateoflastinsp = c3.getString(c3.getColumnIndex("fld_datelastinsp"));
	 				 servingallfloors = c3.getString(c3.getColumnIndex("fld_servingallfloors"));
	 				 
	 				 arr_db.execSQL("INSERT INTO "
	 						+ Communal_AreasElevation
	 						+ " (fld_srid,insp_typeid,fld_elevationno,fld_model,fld_manufacturer,fld_servingallfloors,fld_datelastinsp,fld_uploadimg)"
	 						+ "VALUES ('" + selectedhomeid + "','"+i+"','"+eno+"','"+model+"','"+manufacturer+"','"+servingallfloors+"','"+dateoflastinsp+"','"+image+"')");					 
	 				
	 			}
 	 		} 			
 		}
 			Cursor c4 = arr_db.rawQuery("SELECT * FROM " + Elev_master + " WHERE Elev_srid='"+ homeid + "'", null);//old srid
 			if(c4.getCount()>0)
 	 		{
 				
	 				Cursor c5 = arr_db.rawQuery("SELECT * FROM " + Elev_master + " WHERE Elev_srid='"+ selectedhomeid + "'", null);//new SRIDs	 				
	 				if(c5.getCount()==0)
	 	 	 		{
	 					
	 				/*	for(int j=0;j<c4.getCount();j++,c4.moveToNext())
			 			{
	 						c4.moveToFirst();
	 						String masterid = c4.getString(c4.getColumnIndex("ELev_masterid"));System.out.println(",astt"+masterid);*/
	 						 //String elevno = c4.getString(c4.getColumnIndex("Elev_No"));
			 				 Cursor c6 = arr_db.rawQuery("SELECT * FROM " + Communal_AreasElevation + " WHERE  fld_srid='"+selectedhomeid+"'", null);
			 				 if(c6.getCount()>0)
			 				 {
			 					c6.moveToFirst();
			 				 	for(int m=0;m<c6.getCount();m++,c6.moveToNext())
					 			{	
			 				 		arr_db.execSQL("INSERT INTO "
					 	 						+ Elev_master
					 	 						+ " (ELev_masterid,Elev_srid,Elev_insp_id,Elev_No,Elev_Enable)"
					 	 						+ "VALUES ('"+ c6.getString(c6.getColumnIndex("Commid"))+"','" + selectedhomeid + "','"+i+"','"+ c6.getString(c6.getColumnIndex("fld_elevationno"))+"','1')");	
					 			
			 				 		System.out.println("INSERT INTO "
				 	 						+ Elev_master
				 	 						+ " (ELev_masterid,Elev_srid,Elev_insp_id,Elev_No,Elev_Enable)"
				 	 						+ "VALUES ('"+ c6.getString(c6.getColumnIndex("Commid"))+"','" + selectedhomeid + "','"+i+"','"+ c6.getString(c6.getColumnIndex("fld_elevationno"))+"','1')");
		 				 		
					 			}
			 				 }  System.out.println("ssss");
			 			//}
	 					System.out.println("all comep");
	 	 	 		}
	 				System.out.println("completed");
	 				
 	 		 }	
 	//	}
 		
 			Cursor master5 = arr_db.rawQuery("SELECT * FROM " + Elev_master + " WHERE Elev_srid='"+ homeid + "'", null);//new srid
 			System.out.println("dsfdf"+master5.getCount());
 			String temp1[];
 			if(master5.getCount()>0)
 			{
 				int s=0;
 				temp1= new String[master5.getCount()];
 				master5.moveToFirst();
 				do{
 					
 					temp1[s] = master5.getString(master5.getColumnIndex("ELev_masterid"));System.out.println("sffdffd"+temp1[s]);
 					
 					
 					s++;
 				}while(master5.moveToNext());
 				map.put("master_id",temp1);
 				 
 			}
 			
 			
 			
 		Cursor master = arr_db.rawQuery("SELECT * FROM " + Elev_master + " WHERE Elev_srid='"+ selectedhomeid + "'", null);//new srid
		if(master.getCount()>0)
		{ 	
				master.moveToFirst();
				String[] photocaption = map.get("master_id");

				for(int l=0;l<master.getCount();l++,master.moveToNext())
				{
					System.out.println("test="+l+"test"+photocaption[l]);
					
				System.out
						.println(master.getString(master.getColumnIndex("ELev_masterid"))+"test"+map.get("master_id"+l));
				//Cursor master1=cf.SelectTablefunction(cf.Elevatorimages, " Where Elev_masterid='"+master.getString(master.getColumnIndex("ELev_masterid"))+"'");
				Cursor master1 = arr_db.rawQuery("SELECT * FROM " + Elevatorimages + " WHERE Elev_masterid='"+ master.getString(master.getColumnIndex("ELev_masterid")) + "'", null);//new srid
				System.out
						.println("imagecount="+master1.getCount());
				if(master1.getCount()<=0)
				{
					System.out
							.println("INSERT INTO "+Elevatorimages+" (Elev_masterid,Elev_module_id,Elev_Path,Elev_Order,Elev_Caption) select '"+master.getString(master.getColumnIndex("ELev_masterid"))+"',0,Elev_Path,Elev_Order,Elev_Caption  From "+Elevatorimages+" " +
									"WHERE Elev_masterid='"+photocaption[l]+"'");
					
					arr_db.execSQL("INSERT INTO "+Elevatorimages+" (Elev_masterid,Elev_module_id,Elev_Path,Elev_Order,Elev_Caption) select '"+master.getString(master.getColumnIndex("ELev_masterid"))+"',0,Elev_Path,Elev_Order,Elev_Caption  From "+Elevatorimages+" " +
							"WHERE Elev_masterid='"+photocaption[l]+"'");
				}
			}
		}
			
			
 		}
		catch (Exception e) {
			// TODO: handle exception
			System.out.println("Communal Areas Exception"+e.getMessage());
		}	
		//}
 		
 		
 		
 		
 		
			
		/*Cursor c15 = arr_db.rawQuery("SELECT * FROM " + Elev_master + " WHERE Elev_srid='"+ selectedhomeid + "'", null);//new SRIDs
		System.out.println("came count="+c15.getCount());
		if(c15.getCount()>0)
	 		{
			c15.moveToFirst();
			for(int l=0;l<c15.getCount();l++,c15.moveToNext())
			{
				String masterid = c15.getString(c15.getColumnIndex("ELev_masterid"));System.out.println("masterid"+masterid);
				String elevno = c15.getString(c15.getColumnIndex("Elev_No"));System.out.println("elevno"+elevno);
				Cursor c9 = arr_db.rawQuery("SELECT * FROM " + Elevatorimages + " WHERE Elev_masterid ='"+ masterid + "'", null);
				System.out.println("c9=="+c9.getCount());
				if(c9.getCount()==0)
				{
					c9.moveToFirst();
					 Cursor c7 = arr_db.rawQuery("SELECT * FROM " + Communal_AreasElevation + " WHERE fld_elevationno='"+ elevno + "' and fld_srid='"+selectedhomeid+"'", null);
					System.out.println("c7=="+c7.getCount());
					 if(c7.getCount()>0)
 				 {
 					 	c7.moveToFirst();
 					 	for(int n=0;n<c7.getCount();n++,c7.moveToNext())
			 			{
 					 		Cursor c11 = arr_db.rawQuery("SELECT * FROM " + Elevatorimages + " WHERE Elev_masterid ='"+ c7.getString(c7.getColumnIndex("Commid")) + "'", null);
 					 		if(c11.getCount()==0)
				 			{
			 					for(int k=0;k<c9.getCount();k++,c9.moveToNext())
					 			{
			 						System.out.println("sdd");
					 			
			 						String imagepath = c9.getString(c9.getColumnIndex("Elev_Path"));System.out.println("img="+imagepath);
			 						String ElevOrder = c9.getString(c9.getColumnIndex("Elev_Order"));System.out.println("ElevOrder="+ElevOrder);
			 						String caption = c9.getString(c9.getColumnIndex("Elev_Caption"));System.out.println("caption="+caption);
			 						
			 						System.out.println("tets="+"INSERT INTO "
				 	 						+ Elevatorimages
				 	 						+ " (Elev_masterid,Elev_module_id ,Elev_Path,Elev_Order,Elev_Caption)"
				 	 						+ "VALUES ('"+ c7.getString(c7.getColumnIndex("Commid"))+"','0','" + imagepath + "','"+ElevOrder+"','"+caption+"')");
			 						
			 						arr_db.execSQL("INSERT INTO "
				 	 						+ Elevatorimages
				 	 						+ " (Elev_masterid,Elev_module_id ,Elev_Path,Elev_Order,Elev_Caption)"
				 	 						+ "VALUES ('"+ c7.getString(c7.getColumnIndex("Commid"))+"','0','" + imagepath + "','"+ElevOrder+"','"+caption+"')");	
				 				 
					 			}
				 			}
			 			}
 				 }

 			}
			
	 		}
	 		}*/
 	}
 	private void CommercialWind(int i, String homeid) {
 		// TODO Auto-generated method stub
  		Cursor c4 = arr_db.rawQuery("SELECT * FROM " + Commercial_Wind + " WHERE fld_srid='"+ homeid + "'", null);	
  		if(c4.getCount()>0)
 		{
 			c4.moveToFirst();
 			Cursor c14 = arr_db.rawQuery("SELECT * FROM " + Commercial_Wind + " WHERE fld_srid='"+ selectedhomeid + "'", null);	
 	  		if(c14.getCount()==0)
 	 		{
 	  			arr_db.execSQL("INSERT INTO "+ Commercial_Wind
 					+ " (fld_srid,insp_typeid,fld_terrain,fld_roofcoverings,fld_roofdeck,fld_swr,fld_openingprotection,fld_certfication,fld_rccomments,fld_rdcomments,fld_swrcomments,fld_opcomments)"
 					+ "VALUES ('"+selectedhomeid+"','"+i+"','"+c4.getString(3)+"','"+c4.getString(4)+"','"+c4.getString(5)+"','"+c4.getString(6)+"','"+c4.getString(7)+"','"+c4.getString(8)+"','"+c4.getString(9)+"','"+c4.getString(10)+"','"+c4.getString(11)+"','"+c4.getString(12)+"')");
 			}
 		}
 		Cursor c5 = arr_db.rawQuery("SELECT * FROM " + Comm_AuxBuilding + " WHERE fld_srid='" + homeid + "'", null);				
 		if(c5.getCount()>0)
 		{
 			c5.moveToFirst();
 			Cursor c15 = arr_db.rawQuery("SELECT * FROM " + Comm_AuxBuilding + " WHERE fld_srid='" + selectedhomeid + "'", null);				
 	 		if(c15.getCount()==0)
 	 		{
 	 			for(int k=0;k<c5.getCount();k++)
	 			{
	 				arr_db.execSQL("INSERT INTO "
	 						+ Comm_AuxBuilding
	 						+ " (fld_srid,insp_typeid,fld_aux_Building_type,fld_aux_Building_other,fld_aux_Ins_Size,fld_aux_Cons__RC_3Years,fld_aux_WS_RC_OGN_DD)"
	 						+ "VALUES ('"+selectedhomeid+"','"+i+"','"+ c5.getString(3) + "','"+c5.getString(4)+"','"+c5.getString(5)+"','"+c5.getString(6)+"','"+c5.getString(7)+"')");
	 				c5.moveToNext();
	 			}
 	 		}
 		}
 		Cursor c6 = arr_db.rawQuery("SELECT * FROM " + Comm_RestSuppAppl + " WHERE fld_srid='" + homeid + "'", null);				
 		if(c6.getCount()>0)
 		{
 			c6.moveToFirst();
 			Cursor c16 = arr_db.rawQuery("SELECT * FROM " + Comm_RestSuppAppl + " WHERE fld_srid='" + selectedhomeid + "'", null);				
 	 		if(c16.getCount()==0)
 	 		{
	 			for(int l=0;l<c6.getCount();l++)
	 			{
	 				arr_db.execSQL("INSERT INTO "
	 						+ Comm_RestSuppAppl
	 						+ " (fld_srid,insp_typeid,fld_appliancetype,fld_applianceother,fld_appldata)"
	 						+ "VALUES ('" + selectedhomeid + "','"+i+"','"+ c6.getString(3) + "','"+c6.getString(4)+"','"+c6.getString(5)+"')");
	 				c6.moveToNext();
	 			}
 	 		}
 		}
 		Cursor c7 = arr_db.rawQuery("SELECT * FROM "+ Comm_RestSupplement + " WHERE fld_srid='"+ homeid + "'", null);				
 		if(c7.getCount()>0)
 		{
 			c7.moveToFirst();
 			Cursor c17 = arr_db.rawQuery("SELECT * FROM "+ Comm_RestSupplement + " WHERE fld_srid='"+ selectedhomeid + "'", null);				
 	 		if(c17.getCount()==0)
 	 		{
 	 			arr_db.execSQL("INSERT INTO " + Comm_RestSupplement + " (fld_srid,insp_typeid,fld_appliance_na)"
 					+ "VALUES ('"+selectedhomeid+"','"+i+"','"+c7.getString(3)+"')");
 	 		}
 		}
 		
 		Cursor c8 = arr_db.rawQuery("SELECT * FROM "+ Comm_Aux_Master + " WHERE fld_srid='"+ homeid + "'", null);
 		if(c8.getCount()>0)
 		{
 			c8.moveToFirst();
 			Cursor c18 = arr_db.rawQuery("SELECT * FROM "+ Comm_Aux_Master + " WHERE fld_srid='"+ selectedhomeid + "'", null);
 	 		if(c18.getCount()==0)
 	 		{
 	 			arr_db.execSQL("INSERT INTO " + Comm_Aux_Master + " (fld_srid,insp_typeid,fld_aux_NA)"
 					+ "VALUES ('"+selectedhomeid+"','"+i+"','"+c8.getString(3)+"')");
 	 		}
 		}
 	}
 	private void Roofsectionduplication(int iden,String homeid) 
    {
			// TODO Auto-generated method stub
 		System.out.println("inside Roof Roofsectionduplication");
   	 if(iden==32)
   	 {
   		 iden=4;
   	 }
   	 else if(iden==33)
   	 {
   		 iden=5;
   	 }
   	 else if(iden==34)
   	 {
   		 iden=6;
   	 }
		try
		{
				Cursor val1= SelectTablefunction(Roof_additional, " WHERE  R_ADD_Ssrid='"+homeid+"' and R_ADD_insp_id IN (4,5,6)");
				System.out.println("inside Roof val1"+val1.getCount());
				if(val1.getCount()>0)
				{
					arr_db.execSQL(" INSERT INTO "+Roof_additional+" (R_ADD_Ssrid,R_ADD_insp_id,R_ADD_full,R_ADD_scope,R_ADD_na,R_ADD_comment,R_ADD_citizen,R_ADD_WUWC,R_ADD_PDRUC,R_ADD_PN,R_ADD_PD,R_ADD_RME,R_ADD_HVAC_E,R_ADD_VEN_E,R_ADD_STOR_E,R_ADD_PIP,R_ADD_clean_E,R_ADD_ANT_P,R_ADD_LSE,R_ADD_O_E,R_ADD_RMEL,R_ADD_DRMS,R_ADD_other_I,R_RCT_na,R_ADD_saved) " +
							" Select '"+selectedhomeid+"','"+iden+"',R_ADD_full,R_ADD_scope,R_ADD_na,R_ADD_comment,R_ADD_citizen,R_ADD_WUWC,R_ADD_PDRUC,R_ADD_PN,R_ADD_PD,R_ADD_RME,R_ADD_HVAC_E,R_ADD_VEN_E,R_ADD_STOR_E,R_ADD_PIP,R_ADD_clean_E,R_ADD_ANT_P,R_ADD_LSE,R_ADD_O_E,R_ADD_RMEL,R_ADD_DRMS,R_ADD_other_I,R_RCT_na,'0' FROM "+Roof_additional+"  WHERE  R_ADD_Ssrid='"+homeid+"' and R_ADD_insp_id IN (4,5,6)" );
				}
				Cursor val= SelectTablefunction(Rct_table, " WHERE  RCT_Ssrid='"+homeid+"' and RCT_insp_id in (4,5,6) ");
				if(val.getCount()>0)
				{
						arr_db.execSQL(" INSERT INTO "+Rct_table+" (RCT_Ssrid,RCT_insp_id,RCT_rct,RCT_PAD,RCT_yoi,RCT_npv,RCT_rsl,RCT_rsh,RCT_rst,RCT_bc,RCT_rc,RCT_arfl,RCT_pre,RCT_Rsh_per,RCT_other_rct,RCT_other_yoi,RCT_other_rsh,RCT_other_rst,RCT_other_aRFL,RCT_saved) " +
							" Select '"+selectedhomeid+"','"+iden+"',RCT_rct,RCT_PAD,RCT_yoi,RCT_npv,RCT_rsl,RCT_rsh,RCT_rst,RCT_bc,RCT_rc,RCT_arfl,RCT_pre,RCT_Rsh_per,RCT_other_rct,RCT_other_yoi,RCT_other_rsh,RCT_other_rst,RCT_other_aRFL,'0' FROM "+Rct_table+"  WHERE  RCT_Ssrid='"+homeid+"' and RCT_insp_id in (4,5,6)" );
						val.moveToFirst();
						Cursor master=SelectTablefunction(Roof_master, " Where RM_srid='"+homeid+"' and RM_insp_id IN (4,5,6)");
						if(master.getCount()>0)
						{
							
							String sql = "insert into "+Roof_master+" (RM_inspectorid,RM_srid,RM_page,RM_insp_id,RM_Covering,RM_Enable,RM_module_id) SELECT" +
									" RM_inspectorid,'"+selectedhomeid+"',RM_page,'"+iden+"',RM_Covering,RM_Enable,RM_module_id from  "+Roof_master+" where  RM_insp_id IN (4,5,6) and RM_srid='"+homeid+"'";
							
							arr_db.execSQL(sql);
						}
					for(int i=0;i<val.getCount();i++,val.moveToNext())
					{						
						master.moveToFirst();
						arr_db.execSQL("INSERT INTO "+Roofcoverimages+" (RIM_masterid,RIM_module_id,RIM_Path,RIM_Order,RIM_Caption) " +
								"SELECT (Select RM_masterid  as RIM_masterid from "+Roof_master+" Where RM_inspectorid='"+Insp_id+"' and " +
										"RM_srid='"+selectedhomeid+"' and RM_insp_id IN (4,5,6)  and" +
												" RM_Covering='"+val.getString(val.getColumnIndex("RCT_rct"))+"')," +
														"RIM_module_id,RIM_Path,RIM_Order,RIM_Caption  From "+Roofcoverimages+" " +
							"WHERE RIM_masterid=(Select RM_masterid from "+Roof_master+" Where  RM_srid='"+homeid+"' and RM_insp_id in (4,5,6) and RM_Covering='"+val.getString(val.getColumnIndex("RCT_rct"))+"') ");
					}
				}
				
				Cursor val3= SelectTablefunction(RCN_table, " WHERE  R_RCN_Ssrid='"+homeid+"' and R_RCN_insp_id IN (4,5,6)");
				if(val3.getCount()>0)
				{
					arr_db.execSQL(" INSERT INTO "+RCN_table+" (R_RCN_Ssrid,R_RCN_insp_id,R_RCN_OCR,R_RCN_RRN,R_RCN_VSL,R_RCN_VSTDS,R_RCN_VSCBS,R_RCN_VSDTS,R_RCN_VDRS,R_RCN_ODN,R_RCN_HRMS,R_RCN_FC,R_RCN_GRAC,R_RCN_RLN,R_RCN_RPN,R_RCN_HREBR,R_RCN_WORR,R_RCN_RMRA,R_RCN_SFPE,R_RCN_Amount,R_RCN_EVPWD,R_RCN_RME,R_RCN_BFP,R_RCN_ESM,R_RCN_SOSAS,R_RCN_DRS,R_RCN_na,R_RCN_saved) " +
							" Select '"+selectedhomeid+"','"+iden+"',R_RCN_OCR,R_RCN_RRN,R_RCN_VSL,R_RCN_VSTDS,R_RCN_VSCBS,R_RCN_VSDTS,R_RCN_VDRS,R_RCN_ODN,R_RCN_HRMS,R_RCN_FC,R_RCN_GRAC,R_RCN_RLN,R_RCN_RPN,R_RCN_HREBR,R_RCN_WORR,R_RCN_RMRA,R_RCN_SFPE,R_RCN_Amount,R_RCN_EVPWD,R_RCN_RME,R_RCN_BFP,R_RCN_ESM,R_RCN_SOSAS,R_RCN_DRS,R_RCN_na,'0' FROM "+RCN_table+"  WHERE  R_RCN_Ssrid='"+homeid+"' and R_RCN_insp_id IN (4,5,6)" );
				}			
				/*Cursor val4= SelectTablefunction(RCN_table_dy, " WHERE  R_RCN_D_Ssrid='"+homeid+"' and R_RCN_D_insp_id IN (4,5,6)");				
				if(val4.getCount()>0)
				{
					Cursor val5= SelectTablefunction(RCN_table_dy, " WHERE  R_RCN_D_Ssrid='"+selectedhomeid+"'");
					val5.moveToFirst();
					if(val5.getCount()<=0)
					{						
							
							arr_db.execSQL(" INSERT INTO "+RCN_table_dy+" (R_RCN_D_Ssrid,R_RCN_D_insp_id,R_RCN_D_title,R_RCN_D_option,R_RCN_D_Other,R_RCN_D_saved) " +
								" Select '"+selectedhomeid+"','"+iden+"',R_RCN_D_title,R_RCN_D_option,R_RCN_D_Other,R_RCN_D_saved FROM "+RCN_table_dy+"  WHERE  R_RCN_D_Ssrid='"+homeid+"' and R_RCN_D_insp_id IN (4,5,6) " );
											
					}
				}			*/
		}
		catch (Exception e) {
			// TODO: handle exception
			System.out.println("Roof Exception"+e.getMessage());
		}
    }
 	public void nextscreen() {
		// TODO Auto-generated method stub
 		 CreateARRTable(2);
		 try
			{
			     Intent intimg = new Intent(this.con,PolicyHolder.class);
				 putExtraIntent(intimg); 
				 (this.con).startActivity(intimg);
			}
		    catch (Exception E)
			{
		    	String strerrorlog="Retrieving Inspectiontype data - Select Inspection table";
				//Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+datewithtime+" "+"in apk"+" "+apkrc);
			}
		    
	}
    public void showDialogDate(EditText edt) {
		// TODO Auto-generated method stub
	    getCalender();
		Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialog= new DatePickerDialog(con, new mDateSetListener(edt),
                   mYear, mMonth, mDay);
        dialog.show();
	}
    
    public void getinspectioname(String SRID) 
    {
		// TODO Auto-generated method stub
   	 Cursor c =SelectTablefunction(Select_insp, " Where SI_srid='"+SRID+"' and SI_InspectorId='"+Insp_id+"' and SI_InspectionNames!=''");
   	 if(c.getCount()>0)
   	 {
   		 c.moveToFirst();
   		 selinspname = decode(c.getString(c.getColumnIndex("SI_InspectionNames"))); 
   	 }
    }
    public void getinspectiotextname(String inspids) 
    {
    	String insp_local="";
		// TODO Auto-generated method stub
    	String inspid[] = inspids.split(","); 
    	for(int i=0;i<inspid.length;i++)
    	{
    		
    		Cursor c =SelectTablefunction(inspnamelist, " Where ARR_Custom_ID='"+inspid[i]+"'");
    		System.out.println("inspid[i]="+inspid[i]);
    		if(c.getCount()>0)
    	   	 {
    	   		 c.moveToFirst();System.out.println(decode(c.getString(c.getColumnIndex("ARR_Insp_Name"))));
    	   		 selinsptext += decode(c.getString(c.getColumnIndex("ARR_Insp_Name")))+","; System.out.println("selinsptext[i]="+selinsptext);
    	   	 }		
    	}
    }
    public void getsingleinspectiotextname(String id) 
    {
    System.out.println("signli="+id);	
    		Cursor c =SelectTablefunction(inspnamelist, " Where ARR_Insp_ID='"+id+"'");
    		if(c.getCount()>0)
    	   	{
    	   		 c.moveToFirst();
    	   		 singleinspname = decode(c.getString(c.getColumnIndex("ARR_Insp_Name")));
    	   		 System.out.println("signle="+singleinspname);
    	   		
    	   	}	
    }
	class mDateSetListener implements DatePickerDialog.OnDateSetListener
	{
		EditText v;
		mDateSetListener(EditText v)
		{
			this.v=v;
		}
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			// TODO Auto-generated method stub
			getCalender();
			mYear = year;
			mMonth = monthOfYear+1;
			mDay = dayOfMonth;
			selmDayofWeek = mDayofWeek ;
			
			v.setText(new StringBuilder()
					// Month is 0 based so add 1
					.append(pad(mMonth)).append("/").append(pad(mDay)).append("/")
					.append(mYear).append(" "));
		}
		
	}
	public void showalert(String alerttitle2, String alertcontent2) {
		// TODO Auto-generated method stub
		final Dialog dialog1 = new Dialog(this.con,android.R.style.Theme_Translucent_NoTitleBar);
		dialog1.getWindow().setContentView(R.layout.alert);
		TextView txttitle = (TextView) dialog1.findViewById(R.id.txthelp);
		txttitle.setText(alerttitle2);
		TextView txt = (TextView) dialog1.findViewById(R.id.txtid);
		txt.setText(Html.fromHtml(alertcontent2));
		ImageView btn_helpclose = (ImageView) dialog1.findViewById(R.id.helpclose);
		btn_helpclose.setOnClickListener(new OnClickListener()
		{
		public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dialog1.dismiss();
			}
			
		});
		Button btn_ok = (Button) dialog1.findViewById(R.id.ok);
		btn_ok.setVisibility(v1.VISIBLE);
		btn_ok.setOnClickListener(new OnClickListener()
		{
		public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dialog1.dismiss();
				con.startActivity(new Intent(con,Dashboard.class));
			}
			
		});
		
		dialog1.setCancelable(false);
		dialog1.show();
	}
	public String pad(int c) {
		// TODO Auto-generated method stub		
		return c>=10 ? ""+c : "0"+c;   
	}
	public String IsCurrentInspector(String inspid,String srid,String methodname) throws IOException, XmlPullParserException {
		// TODO Auto-generated method stub
		String currrentinsp,resultres;
		SoapSerializationEnvelope envelope1 = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope1.dotNet = true;
		SoapObject ad_property1 = new SoapObject(NAMESPACE,methodname);
		ad_property1.addProperty("InspectorID", inspid);
		ad_property1.addProperty("Srid", srid);
		envelope1.setOutputSoapObject(ad_property1);
		System.out.println(" ad_property1 "+ad_property1);
		HttpTransportSE androidHttpTransport2 = new HttpTransportSE(URL_ARR);
		androidHttpTransport2.call(NAMESPACE+methodname,envelope1);System.out.println("httptrans");
		resultres = String.valueOf(envelope1.getResponse());System.out.println("resultres="+resultres);
		System.out.println(" resultIsCurrentInspector "+resultres);
		
		return resultres;
	}
	public void createtable()
	{
		CreateARRTable(3);CreateARRTable(3);CreateARRTable(81);CreateARRTable(41);CreateARRTable(42);CreateARRTable(44);CreateARRTable(83);
		CreateARRTable(84);CreateARRTable(45);CreateARRTable(46);CreateARRTable(47);CreateARRTable(48);CreateARRTable(49);CreateARRTable(416);
		CreateARRTable(417);CreateARRTable(418);CreateARRTable(51);CreateARRTable(52);CreateARRTable(53);CreateARRTable(54);CreateARRTable(55);
		CreateARRTable(56);CreateARRTable(7);CreateARRTable(71);CreateARRTable(72);CreateARRTable(73);CreateARRTable(74);CreateARRTable(22);
		CreateARRTable(23);CreateARRTable(231);CreateARRTable(24);CreateARRTable(242);CreateARRTable(243);CreateARRTable(244);CreateARRTable(245);
		CreateARRTable(246);CreateARRTable(247);CreateARRTable(248);CreateARRTable(249);CreateARRTable(25);CreateARRTable(26);CreateARRTable(355);
		CreateARRTable(12);CreateARRTable(40);CreateARRTable(39);CreateARRTable(65);CreateARRTable(62);CreateARRTable(63);CreateARRTable(64);
		CreateARRTable(312);CreateARRTable(311);CreateARRTable(38);CreateARRTable(37);CreateARRTable(36);CreateARRTable(35);CreateARRTable(34);
		CreateARRTable(33);CreateARRTable(32);CreateARRTable(31);CreateARRTable(34);		
	}
	public boolean FP_VBCValidation() {
		// TODO Auto-generated method stub
		boolean bolvbc = false;
		CreateARRTable(244);
		try
		{
			Cursor C_FPVBC= SelectTablefunction(Four_Electrical_Details, " where fld_srid='" + selectedhomeid + "' AND fld_vbcinc<>'' ");
			if(C_FPVBC.getCount()>0)
			{
				bolvbc = true;
			}
			
		}catch (Exception e) {
			// TODO: handle exception
		}
		return bolvbc;
	}
	public boolean FP_LFRValidation() {
		// TODO Auto-generated method stub
		boolean bollfr = false;
		CreateARRTable(244);
		try
		{
			Cursor C_FPLFR= SelectTablefunction(Four_Electrical_Details, " where fld_srid='" + selectedhomeid + "' AND fld_lfrin<>''");
			if(C_FPLFR.getCount()>0)
			{
				bollfr = true;
			}
			
		}catch (Exception e) {
			// TODO: handle exception
		}
		return bollfr;
	}
	public boolean FP_OESValidation() {
		// TODO Auto-generated method stub
		boolean boloes = false;
		CreateARRTable(244);
		try
		{
			Cursor C_FPOES= SelectTablefunction(Four_Electrical_Details, " where fld_srid='" + selectedhomeid + "' AND fld_oesinc<>''");
			if(C_FPOES.getCount()>0)
			{
				boloes = true;
			}
			
		}catch (Exception e) {
			// TODO: handle exception
		}
		return boloes;
	}
	public boolean FP_ComValidation() {
		// TODO Auto-generated method stub
		boolean bolcom = false;
		CreateARRTable(244);
		try
		{
			Cursor C_FPCOM= SelectTablefunction(Four_Electrical_Details, " where fld_srid='" + selectedhomeid + "' and addcomment<>''");
			if(C_FPCOM.getCount()>0)
			{
				bolcom = true;
			}
			
		}catch (Exception e) {
			// TODO: handle exception
		}
		return bolcom;
	}
	public void duplicaterestapp(final int iden) {

		CreateARRTable(38);
		String inspection="--Select--";String insparr[] = null;
		
		Cursor cres = SelectTablefunction(Comm_RestSupplement, " where fld_srid='"+selectedhomeid+"' and insp_typeid='"+iden+"'");
		//Cursor capp = SelectTablefunction( Comm_RestSuppAppl , " where fld_srid='"+selectedhomeid+"' and insp_typeid='"+iden+"'");
		if(cres.getCount()>0)
		{
			gotoclass(iden,WindMitCommRestsupp.class);
		}
		else
		{
			Cursor c1= SelectTablefunction(Comm_RestSupplement, " where fld_srid='"+selectedhomeid+"'");
			if(c1.getCount()>0)
			{
				c1.moveToFirst();			
				Cursor c11= SelectTablefunction(Comm_RestSupplement, " where insp_typeid='33' and  fld_srid='"+selectedhomeid+"'");
				Cursor c12= SelectTablefunction(Comm_RestSupplement, " where insp_typeid='34' and fld_srid='"+selectedhomeid+"'");
				Cursor c13= SelectTablefunction(Comm_RestSupplement, " where insp_typeid='35' and fld_srid='"+selectedhomeid+"'");
				System.out.print("CCc"+c13.getCount());
				
				{
					getPolicyholderInformation(selectedhomeid);
					if(Stories.equals("4") || Stories.equals("5") || Stories.equals("6"))
					{
						inspection += "~"+"Commercial Type II";		
					}
					else if(Integer.parseInt(Stories)>=7)
					{
						inspection += "~"+"Commercial Type III";	
					}						
				}				
				if(c13.getCount()>0)
				{
					inspection += "~"+"General Conditions & Hazards";	
				}
				insparr=inspection.split("~");
				
				final Dialog dialog = new Dialog(this.con,android.R.style.Theme_Translucent_NoTitleBar);
				dialog.getWindow().setContentView(R.layout.alert);
				ImageView btn_helpclose = (ImageView) dialog.findViewById(R.id.choose_close);
				LinearLayout hide=(LinearLayout) dialog.findViewById(R.id.maintable);
				final LinearLayout show=(LinearLayout) dialog.findViewById(R.id.choose_insp);
				hide.setVisibility(View.GONE);
				show.setVisibility(View.VISIBLE);
				Button btn_save = (Button) dialog.findViewById(R.id.choose_save);
				Button btn_cancel = (Button) dialog.findViewById(R.id.choose_cancel);
				final Spinner sp=(Spinner) dialog.findViewById(R.id.choose_sp);				
				ArrayAdapter ad =new ArrayAdapter(this.con,android.R.layout.simple_spinner_item, insparr);
				ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				sp.setAdapter(ad);
				btn_helpclose.setOnClickListener(new OnClickListener()
				{
					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						dialog.cancel();
					}					
				});
				btn_cancel.setOnClickListener(new OnClickListener()
				{
					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						dialog.cancel();
						gotoclass(iden, WindMitCommRestsupp.class);
					}					
				});
				btn_save.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						if(sp.getSelectedItem().toString().equals("-Select-"))
						{
							ShowToast("Please select the Inspection Type", 0);
						}
						else
						{
							if(sp.getSelectedItem().toString().equals("Commercial Type II"))
							{
								dupinsid="33";
							}
							else if(sp.getSelectedItem().toString().equals("Commercial Type III"))
							{
								dupinsid="34";
							}
							else if(sp.getSelectedItem().toString().equals("General Conditions & Hazards"))
							{
								dupinsid="35";
							}
							try
							{
								Cursor c1 = arr_db.rawQuery("select * from " + Comm_RestSupplement +" Where insp_typeid='"+dupinsid+"' and fld_srid='"+selectedhomeid+"'", null);
								if(c1.getCount()>0)
								{
									c1.moveToFirst();
									arr_db.execSQL("INSERT INTO "+ Comm_RestSupplement
												+ " (fld_srid,insp_typeid,fld_appliance_na)"
												+ "VALUES ('" + selectedhomeid + "','"+iden+"','"+ c1.getString(3)+ "')");
								}
			
								Cursor c2 = arr_db.rawQuery("SELECT * FROM " + Comm_RestSuppAppl + " WHERE insp_typeid='"+dupinsid+"' and fld_srid='"+selectedhomeid+"'", null);
								if(c2.getCount()>0)
					 	 		{
					 	 			c2.moveToFirst();
						 			for(int l=0;l<c2.getCount();l++)
						 			{
						 				arr_db.execSQL("INSERT INTO "
						 						+ Comm_RestSuppAppl
						 						+ " (fld_srid,insp_typeid,fld_appliancetype,fld_applianceother,fld_appldata)"
						 						+ "VALUES ('" + selectedhomeid + "','"+iden+"','"+ c2.getString(3) + "','"+c2.getString(4)+"','"+c2.getString(5)+"')");
						 				c2.moveToNext();
						 			}
					 	 		}
					 	 		
					 	 		Cursor c3 = arr_db.rawQuery("select * from " + Wind_Commercial_Comm +" Where insp_typeid='"+dupinsid+"' and fld_srid='"+selectedhomeid+"' and fld_restsuppcomments!=''", null);
					 	 		if(c3.getCount()>0)
								{
									c3.moveToFirst();
									Cursor c4 = arr_db.rawQuery("select * from " + Wind_Commercial_Comm +" Where insp_typeid='"+dupinsid+"' and fld_srid='"+selectedhomeid+"'", null);
									if(c4.getCount()>0)
									{
										arr_db.execSQL("INSERT INTO "+ Wind_Commercial_Comm
												+ " (fld_srid,insp_typeid,fld_commercialareascomments,fld_auxillarycomments,fld_restsuppcomments)"
												+ "VALUES ('" + selectedhomeid + "','"+iden+"','','','"+ c3.getString(5)+ "')");
									}
									else
									{
										arr_db.execSQL("UPDATE "
												+ Wind_Commercial_Comm
												+ " SET fld_restsuppcomments='"+c3.getString(5)+"'"
												+ " WHERE fld_srid ='"
												+ selectedhomeid + "' and insp_typeid='"+iden+"'");
								
									}
								}
					 	 		
					 	 		show.setVisibility(View.GONE);
							}
							catch(Exception e)
							{
								System.out.println("GCH dupl"+e.getMessage());
							}
							show.setVisibility(View.GONE);
							dialog.dismiss();
							ShowToast("Duplicated Successfully", 0);
							gotoclass(iden,WindMitCommRestsupp.class);
						}
					}			
				});		
				dialog.setCancelable(false);
				dialog.show();
			}
			else
			{
				gotoclass(iden,WindMitCommRestsupp.class);
			}
		}
	}
	public void uncheckcommercial(RadioButton chk1,RadioButton chk2,RadioButton chk3,RadioButton chk4)
	{
			getinspectioname(selectedhomeid);
			boolean b1802access = chk_InspTypeQuery(selectedhomeid,"3");
			/*if(b1802access==true)
			{
				chk4.setEnabled(true);
				chk4.setTextColor(Color.BLACK);
			}
			else
			{
				chk4.setEnabled(false);
				chk4.setTextColor(Color.GRAY);
			}*/
			System.out.println("selinspnameinside="+selinspname);
			/*if(selinspname.contains("4"))
			{*/
				getPolicyholderInformation(selectedhomeid);
				
				boolean c1isaccess= chk_InspTypeQuery(selectedhomeid,"4");

				Windremovechk(chk1,chk2,chk3);System.out.println("Stories="+Stories+"MainInspID="+MainInspID);
				if(MainInspID.equals("9") || MainInspID.equals("62") || MainInspID.equals("90"))
				{
						
						if(Stories.equals("1") || Stories.equals("2")  || Stories.equals("3"))
						{
							chk1.setEnabled(true);
							chk1.setTextColor(Color.BLACK);
							/*System.out.println("C1s="+c1isaccess);
							if(c1isaccess==true)
							{
								chk1.setEnabled(true);
								chk1.setTextColor(Color.BLACK);
							}
							else
							{
								chk1.setEnabled(false);
								chk1.setTextColor(Color.GRAY);
							}*/
							
						}
						else if(Stories.equals("4") || Stories.equals("5")  || Stories.equals("6"))
						{
							chk2.setEnabled(true);
							chk2.setTextColor(Color.BLACK);
							/*if(c1isaccess==true)
							{
								chk2.setEnabled(true);
								chk2.setTextColor(Color.BLACK);
							}
							else
							{
								chk2.setEnabled(false);
								chk2.setTextColor(Color.GRAY);
							}*/
						
						}
						else 
						{	
							chk3.setEnabled(true);
							chk3.setTextColor(Color.BLACK);
							/*if(c1isaccess==true)
							{
								chk3.setEnabled(true);
								chk3.setTextColor(Color.BLACK);
							}	
							else
							{
								chk3.setEnabled(false);
								chk3.setTextColor(Color.GRAY);
							}*/
							
						}
					/*}
					else
					{
						Windremovechk(chk1, chk2, chk3);
					}*/
				}
	}
	public void Windremovechk(RadioButton chk1,RadioButton chk2,RadioButton chk3) 
	{
		// TODO Auto-generated method stub
		
		chk1.setEnabled(false);chk1.setTextColor(Color.GRAY);
		chk2.setEnabled(false);chk2.setTextColor(Color.GRAY);
		chk3.setEnabled(false);chk3.setTextColor(Color.GRAY);
	}
	public boolean noofbuildingvalidation(String bno) {
		// TODO Auto-generated method stub
		boolean chk=true;
		try
		{
			System.out.println("bn0="+bno);
			getPolicyholderInformation(selectedhomeid);
			Cursor c =SelectTablefunction(policyholder, " WHERE Commercial_Flag='"+CommercialFlag+"'");
			noofbuildings  = c.getCount();System.out.println("noofbuildings"+noofbuildings);
			if(Integer.parseInt(bno)>noofbuildings)
			{
				chk = false;	
			}
			else
			{
				chk=  true;
			}
		}
		catch(Exception e)
		{
			
		}
			return chk;		
	}
	public void dropTable(int select) {
		switch (select) {
		case 66:
			/** drop OnlineTable **/
			try {
				arr_db.execSQL("DROP TABLE IF EXISTS "+ OnlineTable );
				
			} catch (Exception e) {
				strerrorlog="Problem in deleting online table";
			
			}
			break;
		}
    }
	public void getBIGeneraldata(String selectedhomeid) {
		// TODO Auto-generated method stub
		CreateARRTable(150);
		curbuilgen=SelectTablefunction(BI_GeneralDup, " where SRID='"+selectedhomeid+"'");System.out.println("cf.sel="+selectedhomeid);
		System.out.println("count="+curbuilgen.getCount());
		if(curbuilgen.getCount()>0)
		{
			curbuilgen.moveToFirst();
			ScopeofInspection = decode(curbuilgen.getString(curbuilgen.getColumnIndex("ScopeOfInspection")));
			InsuredIS = decode(curbuilgen.getString(curbuilgen.getColumnIndex("fld_Insuredis")));
			Neighbourhoodis = decode(curbuilgen.getString(curbuilgen.getColumnIndex("fld_Neighbourhoodis")));
			OccupancyType = decode(curbuilgen.getString(curbuilgen.getColumnIndex("fld_OccupancyType")));
			Nofunits = curbuilgen.getString(curbuilgen.getColumnIndex("fld_noofunits"));
			BuiloccuPerc = decode(curbuilgen.getString(curbuilgen.getColumnIndex("fld_BuildOccuper")));
			Builvacaperc = decode(curbuilgen.getString(curbuilgen.getColumnIndex("fld_BuildVacantper")));
			BuildingOccu = decode(curbuilgen.getString(curbuilgen.getColumnIndex("fld_BuildOccu")));
			AdditionalStructures = decode(curbuilgen.getString(curbuilgen.getColumnIndex("fld_addistru")));
			
			PermitConfirmed = decode(curbuilgen.getString(curbuilgen.getColumnIndex("fld_Permitconfirmed")));System.out.println("PP="+PermitConfirmed);
			
			LightPoles = decode(curbuilgen.getString(curbuilgen.getColumnIndex("fld_LightPoles")));
			FencesPrepLine = decode(curbuilgen.getString(curbuilgen.getColumnIndex("fld_FencesPrepLine")));
			
		}
	}
	public void Addendumcomments(String homeid, int i) {
		// TODO Auto-generated method stub
		try
		{
		CreateARRTable(50);
		 C_ADDEN= SelectTablefunction(Addendum, " where fld_srid='" + homeid + "' and insp_typeid='"+i+"'");
		 if(C_ADDEN.getCount()>0)
		 {		
			 C_ADDEN.moveToFirst();
			 AddendumComm = decode(C_ADDEN.getString(C_ADDEN.getColumnIndex("addendumcomment")));
			
		 }
		}
		catch(Exception e)
		{
			System.out.println("AddendumValidation"+e.getMessage());
		}
	}
	public String getelvationtext(int elev,String insp)
	{
		String selected="";
		
		
		if(insp.equals("1"))
		{
			 if(elev==1) selected="Exterior Photographs";
			 if(elev==2) selected="Roof Photographs";
			 if(elev==3) selected="Plumbing Photographs";
			 if(elev==4) selected="Electrical Photographs";
			 if(elev==5) selected="HVAC Photographs";
			 if(elev==7) selected="Other Photographs";
			 if(elev==23) selected="Front Photo";
		}
		else if(insp.equals("2"))
		{
			
			if(elev==2)selected="Roof Photographs";
			if(elev==7)selected="Other Photographs";
			/**need to change ***/
			if(elev==1)selected="Exterior Photographs";
			if(elev==6)selected="Attic Photographs";	
			if(elev==23) selected="Front Photo";
		}
		else if(insp.equals("3"))
		{
			
			if(elev==17)selected="Front Elevation";
			if(elev==18)selected="Right Elevation";
			if(elev==19)selected="Left Elevation";
			if(elev==20)selected="Back Elevation";
			if(elev==6)selected="Attic Photographs";
			if(elev==12)selected="Additional Photographs";
			if(elev==7)selected="Other Photographs";
			if(elev==23) selected="Front Photo";
		}
		else if(insp.equals("4"))
		{
			if(elev==17)selected="Front Elevation";
			if(elev==18)selected="Right Elevation";
			if(elev==19)selected="Left Elevation";
			if(elev==20)selected="Back Elevation";
			if(elev==6)selected="Attic Photographs";
			if(elev==12)selected="Additional Photographs";
			if(elev==7)selected="Other Photographs";
			if(elev==23) selected="Front Photo";
		}
		else if(insp.equals("5"))
		{
			if(elev==17)selected="Front Elevation";
			if(elev==18)selected="Right Elevation";
			if(elev==19)selected="Left Elevation";
			if(elev==20)selected="Back Elevation";
			if(elev==6)selected="Attic Photographs";
			if(elev==12)selected="Additional Photographs";
			if(elev==7)selected="Other Photographs";
			if(elev==23) selected="Front Photo";
		}
		else if(insp.equals("6"))
		{
			 if(elev==17) selected="Front Elevation";
			 if(elev==18) selected="Right Elevation";
			 if(elev==19) selected="Left Elevation";
			 if(elev==20) selected="Back Elevation";
			 if(elev==6) selected="Attic Photographs";
			 if(elev==12) selected="Additional Photographs";
			 if(elev==7) selected="Other Photographs";
			 if(elev==23) selected="Front Photo";
		}
		else if(insp.equals("7"))
		{
			 if(elev==17) selected="Front Elevation";
			 if(elev==18) selected="Right Elevation";
			 if(elev==19) selected="Left Elevation";
			 if(elev==20) selected="Back Elevation";
			 if(elev==6) selected="Attic Photographs";
			 if(elev==12) selected="Additional Photographs";
			 if(elev==2) selected="Roof Photographs";
			 if(elev==7) selected="Other Photographs";		
			 if(elev==23) selected="Front Photo";
			
		}
		else if(insp.equals("8"))
		{
			 if(elev==8) selected="External Photographs";
			 if(elev==9) selected="Roof and attic Photographs";
			 if(elev==10) selected="Ground and adjoining images";
			 if(elev==11) selected="Internal Photographs";
			 if(elev==12) selected="Additional Photographs";
			 if(elev==7) selected="Other Photographs";
			 if(elev==23) selected="Front Photo";
		}
		else if(insp.equals("9"))
		{
			if(elev==25) selected="Internal Photographs";
			 if(elev==5) selected="HVAC Photographs";
			 if(elev==4) selected="Electrical Photographs";
			 if(elev==3) selected="Plumbing Photographs";
			 if(elev==7) selected="Other Photographs";
			 if(elev==1) selected="Exterior Photographs";
			 if(elev==26) selected="Interior Fixture Photographs";
			 if(elev==27) selected="Drywall Photographs";
			 if(elev==23) selected="Front Photo";
		
		}
		
		return selected;
	}
}