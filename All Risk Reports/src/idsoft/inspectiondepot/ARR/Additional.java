package idsoft.inspectiondepot.ARR;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

/*** 	Page Information
    		Version :1.0
    		VCode:1
    		Created Date:20/10/2012
    		Purpose:For Additonal information 
    		Created By:Rakki S
    		Last Updated:1/19/2013
    		Last Updatedby:Rakki S **/


public class Additional extends Activity{
	CommonFunctions cf;
	String strhomeid;
	TextView[] additional = new TextView[13];
	public void onCreate(Bundle SavedInstanceState) {
		super.onCreate(SavedInstanceState);
		cf = new CommonFunctions(this);
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			cf.selectedhomeid = strhomeid= extras.getString("homeid");
			cf.onlinspectionid = extras.getString("InspectionType");
		    cf.onlstatus = extras.getString("status");
	 	}
		setContentView(R.layout.additionalinfo);
		
		cf.getDeviceDimensions();
		LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
     	hdr_layout.addView(new HdrOnclickListener(this,1,"General","Additional Contact Info",1,0,cf));
     	LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
     	main_layout.setMinimumWidth(cf.wd);
     	main_layout.addView(new MyOnclickListener(getApplicationContext(), 1, 0,0,cf));
	    LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
	    submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 12, 1,0,cf));
	    LinearLayout submenu2_layout = (LinearLayout) findViewById(R.id.submenu1); System.out.println("test submenu1");
	    submenu2_layout.addView(new MyOnclickListener(getApplicationContext(), 60, 1,0,cf));System.out.println("test 56");
	
	    
	    TableRow scr = (TableRow)findViewById(R.id.row2);
	    scr.setMinimumHeight(cf.ht);
        
		cf.getInspectorId();
		additional[4] = (TextView) findViewById(R.id.efirstname);
		additional[5] = (TextView) findViewById(R.id.econtactperson);
		additional[6] = (TextView) findViewById(R.id.txtaddr1);
		additional[7] = (TextView) findViewById(R.id.txtaddr2);
		additional[8] = (TextView) findViewById(R.id.txtdaycal);
		additional[9] = (TextView) findViewById(R.id.txtcity);
		additional[10] = (TextView) findViewById(R.id.txtstate);
		additional[11] = (TextView) findViewById(R.id.txtcountry);
		additional[12] = (TextView) findViewById(R.id.txtzip);
		
		cf.CreateARRTable(6);
		 try {
				Cursor cur = cf.SelectTablefunction(cf.Additional_table, " where ARR_AD_SRID='"+ cf.selectedhomeid + "'");
				if(cur.getCount()>0)
				{
					cur.moveToFirst();
					if (cur != null) {
						for (int i = 4; i <= 12; i++) {
							additional[i].setText(cf.decode(cur.getString(i+1)));
							if (cur.getString(i+1).equals("anyType{}") || cur.getString(i+1).trim().equals("") || cur.getString(i+1).equals("N/A")) 
							{
								additional[i].setText("N/A");
							}
						}  
	
					}while (cur.moveToNext());
				}
				else
				{
					for (int i = 4; i <= 12; i++) 
					{
						additional[i].setText("N/A");
					}
				}
		    }
		    catch(Exception e)
		    {
		    	cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ Additional.this +" "+" in the stage of(catch) retreving  at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				System.out.println("thats fine "+e.getMessage());
		    }
	}
	 public void clicker(View v) {
			switch (v.getId()) {

			case R.id.home:
						cf.gohome();
			break;

			}
		}
	 public boolean onKeyDown(int keyCode, KeyEvent event) {
			if (keyCode == KeyEvent.KEYCODE_BACK) {
				
				cf.goback(13);

				return true;
			}
			return super.onKeyDown(keyCode, event);
		}

}
