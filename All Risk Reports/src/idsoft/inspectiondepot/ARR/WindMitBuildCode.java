/*
************************************************************
* Project: ALL RISK REPORT
* Module : WindMitBuildCode.java
* Creation History:
Created By : Nalini on 10/24/2012
* Modification History:
Last Modified By : Gowri on 11/28/2012
************************************************************ 
*/
package idsoft.inspectiondepot.ARR;
import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.HVAC.Spin_Selectedlistener;
import idsoft.inspectiondepot.ARR.Observation1.Touch_Listener;
import idsoft.inspectiondepot.ARR.SinkSummary.textwatcher;

import java.util.Calendar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.opengl.Visibility;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

public class WindMitBuildCode extends Activity {
	CommonFunctions cf;
	String b1_q1[] = { "Meets 2001 FBC", "Meets SFBC -94","Unknown - Does Not Meet" };
	Calendar cal = Calendar.getInstance();
	String yroptionA[] = { "--Select--", "Unknown","2001", "2002", "2003", "2004", "2005",
			"2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013","2014" };
	String yroptionB[] = { "--Select--",  "Unknown","1994", "1995", "1996", "1997", "1998", "1999", "2000",
			"2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008",
			"2009", "2010", "2011", "2012","2013" ,"2014"};
	int current_year = cal.get(Calendar.YEAR),sel=0,yhme=0;
	EditText edoptionA,edoptionB,bccomment;
	ArrayAdapter spnAadap,spnBadap;
	RadioButton rdiooptionA,rdiooptionB,rdiooptionC;
	Spinner spnA,spnB;boolean load_comment=true;
	String tmp="",quest_option="",updatecnt="",spnAvalue="",spnAOtherval="",spnBvalue="",spnBOtherval="",yearbuilt="",yearother="",edtoptvalue="",compchk="",optvalue="";
	TableRow tblrw;
	protected static final int DATE_DIALOG_ID = 0;
	@Override
	   public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        cf=new CommonFunctions(this);	
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);				
		 	}
			setContentView(R.layout.windmitbuildcode);
	        cf.getDeviceDimensions();
	       
	        
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
	        if(cf.identityval==31)
	        {
	          hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","B1 1802(Rev.01/12)","Building Code",3,1,cf));
	        }
	        else if(cf.identityval==32)
	        { 
	        	hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","Commercial Type I","Building Code",3,1,cf));
	        }
			LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
         	main_layout.setMinimumWidth(cf.wd);
         	main_layout.addView(new MyOnclickListener(getApplicationContext(), 3, 0,0,cf));
	       
	        cf.type_submenulayout =(LinearLayout) findViewById(R.id.typesubmenu); //** WINDMIT TYPE SUBMENU **//
	        cf.type_submenulayout.addView(new MyOnclickListener(this, 300, 1,0, cf));
	        
	        cf.windmittypechk(cf.identityval,((RadioButton)findViewById(R.id.b1802_chk)),((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((TextView)findViewById(R.id.commtxt)),((ImageView)findViewById(R.id.editbno))); // To check the checkbox of choosen inspection type
	        cf.uncheckcommercial(((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((RadioButton)findViewById(R.id.b1802_chk))); 
	    	         
	        cf.submenu_layout = (LinearLayout) findViewById(R.id.submenu);//** WINDMIT TYPE SUBMENU **//	
	        System.out.println("tetxxx");
	        cf.submenu_layout .addView(new MyOnclickListener(this,cf.identityval,1,301, cf));
	       
	        cf.mainlayout = (LinearLayout)findViewById(R.id.bc_table);
	        cf.save = (Button)findViewById(R.id.save);
	        cf.tblrw = (TableRow)findViewById(R.id.row2);
	        cf.tblrw.setMinimumHeight(cf.ht);			
			cf.CreateARRTable(31);
			cf.CreateARRTable(32);
			
			/*cf.getPolicyholderInformation(cf.selectedhomeid);
			if(!cf.CommercialFlag.equals("0"))
			{
				if(cf.Stories.equals("1") || cf.Stories.equals("2")  || cf.Stories.equals("3"))
				{
					cf.getbuildinginfo(1,32,"Commercial Type I");
				}    	
			}*/
			Declarations();
			cf.getCalender();
			BC_setValue();
	    }
	private void BC_setValue() {
		// TODO Auto-generated method stub
		
		Cursor c1 = cf.SelectTablefunction(cf.policyholder,	"where ARR_PH_SRID='" + cf.selectedhomeid + "'");
		c1.moveToFirst();
		String yearofhome = c1.getString(c1.getColumnIndex("ARR_YearBuilt"));
		String country = cf.decode(c1.getString(c1.getColumnIndex("ARR_PH_County")));
		System.out.println("yearofhome"+yearofhome);
		if (!yearofhome.equals("")) {
			try {
				yhme = Integer.parseInt(yearofhome);System.out.println("fsdf");
			} catch (Exception e) {
				System.out.println("catch="+e.getMessage());
				yhme = 0;
			}
		}
		System.out.println("yohome="+yhme+"count="+country);

		if (country.toLowerCase().equals("miami-dade") 	|| country.toLowerCase().equals("broward")) {
			Cursor c2 = cf.SelectTablefunction(cf.Wind_Questiontbl,	"where fld_srid='" + cf.selectedhomeid + "'");	
			System.out.println("QuesRows="+c2.getCount());
			if (c2.getCount() == 0) 
			{
				optvalue = "2";
				rdiooptionB.setChecked(true);((Button)findViewById(R.id.Calcicon2)).setEnabled(true);					
				spnB.setEnabled(true);
				int spinnerPosition = spnBadap.getPosition(yearofhome);
				spnB.setSelection(spinnerPosition);
				bccomment.setText("This home was verified as meeting the requirements of the OIR B1- 1802, Selection B, Question 1, South Florida Building Code, 1994.");
			}
			else
			{
				c2.moveToFirst();
				if (c2 != null) {
					if (c2.getString(c2.getColumnIndex("fld_buildcode")).equals("0")) 
					{
						optvalue = "2";
						rdiooptionB.setChecked(true);((Button)findViewById(R.id.Calcicon2)).setEnabled(true);					
						spnB.setEnabled(true);
						int spinnerPosition = spnBadap.getPosition(yearofhome);
						spnB.setSelection(spinnerPosition);
						bccomment.setText("This home was verified as meeting the requirements of the OIR B1- 1802, Selection B, Question 1, South Florida Building Code, 1994.");
					}
				}
			}
		}
		else
		{
			Cursor c3 = cf.SelectTablefunction(cf.Wind_Questiontbl,	"where fld_srid='" + cf.selectedhomeid + "'");
			System.out.println("Cunt"+c3.getCount());
			if (c3.getCount() == 0) 
			{
				System.out.println("yhme than 1994="+yhme);
				if(yhme>=1994)
				{
					System.out.println("greater than 1994");
					optvalue = "1";
					rdiooptionA.setChecked(true);System.out.println("dsfsfsdfffffdfdf");
					((Button)findViewById(R.id.Calcicon1)).setEnabled(true);	System.out.println("dsfsdf");
					spnA.setEnabled(true);System.out.println("DSf");
					int spinnerPosition = spnAadap.getPosition(yearofhome);System.out.println("greater than yearofhome");
					spnA.setSelection(spinnerPosition);
					bccomment.setText("This home was verified as meeting the requirements of the OIR B1- 1802, Selection A, Question 1 Building Code.");
				}
			} 
			else 
			{				
				System.out.println("greater than");
				c3.moveToFirst();
				System.out.println("greater values"+c3.getString(c3.getColumnIndex("fld_buildcode")));
					if (c3.getString(c3.getColumnIndex("fld_buildcode")).equals("0") && yhme>=1994) 
					{
						System.out.println("grecame");
						optvalue = "1";
						rdiooptionA.setChecked(true);System.out.println("grecameoptA");
						((Button)findViewById(R.id.Calcicon1)).setEnabled(true);	System.out.println("Calcicon1");
						spnA.setEnabled(true);System.out.println("yearofhome"+yearofhome);
						int spinnerPosition = spnAadap.getPosition(yearofhome);System.out.println("yearofhome");
						spnA.setSelection(spinnerPosition);System.out.println("spnA");
						bccomment.setText("This home was verified as meeting the requirements of the OIR B1- 1802, Selection A, Question 1 Building Code.");
					}
			}		
		}
		
			if (yhme < 1994 && yhme != 0) {
				optvalue = "3";
				rdiooptionB.setChecked(false);
				rdiooptionC.setChecked(true);
				bccomment.setText("This home was verified as NOT meeting the requirements of the OIR B1-1802, Question 1, selection A or B.");
			}
		
		
		try
		{			
			Cursor c3 = cf.arr_db.rawQuery("SELECT * FROM "	+ cf.Wind_Questiontbl + " WHERE fld_srid='"
							+ cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'", null);		
			System.out.println("c3="+c3.getCount());
			if(c3.getCount()>0)
			{
				c3.moveToFirst();
				if (c3 != null) 
				{
					String yrbuiltval = c3.getString(c3.getColumnIndex("fld_bcyearbuilt"));
					String permitappdateval = c3.getString(c3.getColumnIndex("fld_bcpermitappdate"));
					String bcvalue = c3.getString(c3.getColumnIndex("fld_buildcode"));
					
					if(bcvalue.equals("1") || bcvalue.equals("2") || bcvalue.equals("3"))
					{
						if (bcvalue.equals("1")) {
							optvalue = "1";
							showhideoption();
							rdiooptionA.setEnabled(true);rdiooptionA.setChecked(true);spnA.setEnabled(true);
							((Button)findViewById(R.id.Calcicon1)).setEnabled(true);
							int selA = spnAadap.getPosition(yrbuiltval);
							spnA.setSelection(selA);	
							edoptionA.setText("" + permitappdateval);
						} else if (bcvalue.equals("2")) {
							optvalue = "2";
							showhideoption();
							rdiooptionB.setEnabled(true);rdiooptionB.setChecked(true);spnB.setEnabled(true);
							((Button)findViewById(R.id.Calcicon2)).setEnabled(true);						
							int selB = spnBadap.getPosition(yrbuiltval);
							spnB.setSelection(selB);	
							edoptionB.setText("" + permitappdateval);
						}else if (bcvalue.equals("3")) {
							optvalue="3";
							showhideoption();
							rdiooptionC.setChecked(true);
						}
						((TextView)findViewById(R.id.savetxtbc)).setVisibility(cf.v1.VISIBLE);
					}
					else
					{
						((TextView)findViewById(R.id.savetxtbc)).setVisibility(cf.v1.GONE);
					}
				}
			}
			else
			{
				((TextView)findViewById(R.id.savetxtbc)).setVisibility(cf.v1.GONE);
			}

		}
			 catch (Exception E)
				{
		    	System.out.println("EE questios"+E.getMessage());
				String strerrorlog="Retrieving BUILD CODE - WINDMIT";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			}
			
		try{
		  Cursor c12=cf.SelectTablefunction(cf.Wind_QuesCommtbl, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
		  if(c12.getCount()>0)
		   {	
			  c12.moveToFirst();
			  bccomment.setText(cf.decode(c12.getString(c12.getColumnIndex("fld_buildcodecomments"))));} 
		}
	    catch (Exception E)
		{
	    	System.out.println("EE b comments "+E.getMessage());
			String strerrorlog="Retrieving BUILD CODE - WINDMIT";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
	private void Declarations() {
		// TODO Auto-generated method stub
		cf.setupUI((ScrollView) findViewById(R.id.scr));
		((TextView)findViewById(R.id.bc_chk)).setText(Html
				.fromHtml("Was the structure built in compliance with the Florida Building Code (FBC 2001 or later) OR for homes located in the HVHZ (Miami-Dade or Broward counties), South Florida Building Code (SFBC-94)? "));
		edoptionA = ((EditText)findViewById(R.id.edtoptionA));
		edoptionB = ((EditText)findViewById(R.id.edtoptionB));
		rdiooptionA = (RadioButton) this.findViewById(R.id.BCoptionA);rdiooptionA.setOnClickListener(OnClickListener);
		rdiooptionB = (RadioButton) this.findViewById(R.id.BCoptionB);rdiooptionB.setOnClickListener(OnClickListener);
		rdiooptionC = (RadioButton) this.findViewById(R.id.BCoptionC);rdiooptionC.setOnClickListener(OnClickListener);
		spnA = ((Spinner)findViewById(R.id.spnroptioA));
		spnB = ((Spinner)findViewById(R.id.spnroptioB));
		
		spnAadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, yroptionA);
		spnAadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spnA.setAdapter(spnAadap);
		spnA.setOnItemSelectedListener(new  Spin_Selectedlistener(1));
		 
		spnBadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, yroptionB);
		spnBadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spnB.setAdapter(spnBadap);
		spnB.setOnItemSelectedListener(new  Spin_Selectedlistener(2));
		spnA.setEnabled(false);spnB.setEnabled(false);
		
		bccomment = ((EditText)findViewById(R.id.bccomment));
		bccomment.addTextChangedListener(new textwatcher());
		TextView helptxt = (TextView) findViewById(R.id.help);
		helptxt.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if (optvalue=="")
				{	cf.alertcontent = "To see help comments, please select Building Code options.";}
				else if (optvalue.equals("1") || rdiooptionA.isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection A, Question 1 Building Code.";
				} else if (optvalue.equals("2") || rdiooptionB.isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection B, Question 1, South Florida Building Code, 1994.";
				} else if (optvalue.equals("3") || rdiooptionC.isChecked()) {
					cf.alertcontent = "This home was verified as NOT meeting the requirements of the OIR B1-1802, Question 1, selection A or B.";
				} else {
					cf.alertcontent = "To see help comments, please select Building Code options.";
				}
				cf.showhelp("HELP",cf.alertcontent);
			}
		});
	}
	class textwatcher implements TextWatcher
    {
	         public int type;
	         textwatcher()
	    	{
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {
	    		// TODO Auto-generated method stub
	    		cf.showing_limit(s.toString(),(TextView)findViewById(R.id.bc_tv_type1),500); 
	    		
	}

	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,

	    			int after) {
	    		// TODO Auto-generated method stub
	     	}

	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,

	    			int count) {
	    		// TODO Auto-generated method stub
	    	}
	   }	
	class Spin_Selectedlistener implements OnItemSelectedListener
	{
		int i;
		public Spin_Selectedlistener(int i) {
			// TODO Auto-generated constructor stub
			this.i=i;
		}
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			if(i==1)
			{
				spnAvalue = spnA.getSelectedItem().toString();
			}
			else if(i==2)
			{
				spnBvalue = spnB.getSelectedItem().toString();		
			}			
		}
		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub			
		}		
	}
	
	RadioButton.OnClickListener OnClickListener = new RadioButton.OnClickListener() {
		public void onClick(View v) {
			try {
				switch (v.getId()) {
				case R.id.BCoptionA:
					optvalue="1";
					bccomment.setText("This home was verified as meeting the requirements of the OIR B1- 1802, Selection A, Question 1 Building Code.");
					showhideoption();
					rdiooptionA.setChecked(true);
					edoptionA.setEnabled(true);edoptionA.setCursorVisible(false);
					spnA.setEnabled(true);spnA.requestFocus();
					((Button)findViewById(R.id.Calcicon1)).setEnabled(true);
					break;

				case R.id.BCoptionB:
					optvalue="2";
					bccomment.setText("This home was verified as meeting the requirements of the OIR B1- 1802, Selection B, Question 1, South Florida Building Code, 1994.");
					showhideoption();
					rdiooptionB.setChecked(true);
					edoptionB.setEnabled(true);edoptionB.setCursorVisible(false);
					spnB.setEnabled(true);spnB.requestFocus();
					((Button)findViewById(R.id.Calcicon2)).setEnabled(true);
					break;

				case R.id.BCoptionC:
					optvalue="3";
					bccomment.setText("This home was verified as NOT meeting the requirements of the OIR B1-1802, Question 1, selection A or B.");
					showhideoption();
					rdiooptionC.setChecked(true);
					break;
				}
			} catch (Exception e) {
				System.out.println("erro in on click " + e);
			}
		}
	};
	public void showhideoption() /** TO HIDE CONTROLS WHEN CLICKED ON RADIO OPTION**/
	{
		rdiooptionA.setChecked(false);
		rdiooptionB.setChecked(false);
		rdiooptionC.setChecked(false);
		edoptionA.setEnabled(false);edoptionA.setText("");
		edoptionB.setEnabled(false);edoptionB.setText("");
		spnA.setEnabled(false);spnA.setSelection(0);
		spnB.setEnabled(false);spnB.setSelection(0);
		((Button)findViewById(R.id.Calcicon1)).setEnabled(false);
		((Button)findViewById(R.id.Calcicon2)).setEnabled(false);
	}
	public void clicker(View v) {
		switch(v.getId())
		{
		case R.id.loadcomments:
			/***Call for the comments***/
			cf.findinspectionname(cf.identityval);BC_suboption();
			int len=((EditText)findViewById(R.id.bccomment)).getText().toString().length();			
			if(!tmp.equals(""))
			{
				/*if(len<500)
				{
				*/	if(load_comment)
					{
						load_comment=false;
						int loc1[] = new int[2];
						v.getLocationOnScreen(loc1);
						Intent in = cf.loadComments(tmp,loc1);
						in.putExtra("insp_name", cf.inspname);
						in.putExtra("insp_ques", "Building Code");
						in.putExtra("length", len);
						in.putExtra("max_length", 500);
						startActivityForResult(in, cf.loadcomment_code);
					}
				/*}
				else
				{
					cf.ShowToast("You have exceed the maximum length.", 0);
				}*/
			}
			else
			{
				cf.ShowToast("Please select the option for Building Code.",0);	
			}			
			break;
		case R.id.Calcicon1:/** DATE ICON 1 TO BE CLICKED **/
			cf.getCalender();
			showDialog(DATE_DIALOG_ID);
			break;
		case R.id.Calcicon2:/** DATE ICON 2 TO BE CLICKED **/
			cf.getCalender();
			showDialog(DATE_DIALOG_ID);
			break;
		
		case R.id.helpoptA: /** HELP FOR OPTION1 **/
			cf.alerttitle="A - Meets 2001 FBC";
		    cf.alertcontent="Built in compliance with the FBC: year built. For homes built in 2002 or 2003, provide a permit application with a date after 3/1/2002: building permit application date (mm/dd/yyyy).";
		    cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		case R.id.helpoptB: /** HELP FOR OPTION2 **/
			cf.alerttitle="B - Meets SFBC-94";
		    cf.alertcontent="For the HVHZ Only: Built in compliance with the SFBC-94: year built. For homes built in 1994, 1995 or 1996, provide a permit application with a date after 9/1/1994: buildings permit application date (mm/dd/yyyy).";
		    cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;			  
		case R.id.helpoptC: /** HELP FOR OPTION3 **/
			cf.alerttitle="C - Unknown - Does Not Meet";
		    cf.alertcontent="Unknown or does not meet the requirements of answer A or B.";
		    cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		case R.id.hme:
    		cf.gohome();
    		break;
		
		case R.id.save:
			if (rdiooptionA.isChecked()) { 		yearbuilt = spnAvalue; edtoptvalue = edoptionA.getText().toString(); yearother = spnAvalue; edtoptvalue = edoptionA.getText().toString();  }
			else if (rdiooptionB.isChecked()) {	yearbuilt = spnBvalue; edtoptvalue = edoptionB.getText().toString(); yearother = spnBvalue; edtoptvalue = edoptionB.getText().toString();}
			else if (rdiooptionC.isChecked()) {	yearbuilt ="";	edtoptvalue ="";}
			
			if (rdiooptionA.isChecked() || rdiooptionB.isChecked() || rdiooptionC.isChecked()) 
			{
					if (rdiooptionA.isChecked() || rdiooptionB.isChecked()) {
						
						if (yearbuilt.equals("") || yearbuilt.equals("--Select--")) 
						{
							cf.ShowToast("Please select Year Built.",0);							
						} 
						else if(yearbuilt.equals("Unknown"))
						{
							
							if(edtoptvalue.equals(""))
							{
								insertdata();	
							}
							else
							{
								if (cf.checkfortodaysdate(edtoptvalue) == "true") 
								{
									insertdata();	
								}
								else
								{
									cf.ShowToast("Building Permit Application Date should not be greater than Today's Date.",0);

									if (rdiooptionA.isChecked()) {
										edoptionA.setText("");
									} else if (rdiooptionB.isChecked()) {
										edoptionB.setText("");
									}
								}
							}
							
						}
						else 
						{
							if (yearvalidation(yearbuilt) == "true") 
							{
								if ((yearbuilt.equals("2002") || yearbuilt.equals("2003"))
										|| ((yearbuilt.equals("1994") || yearbuilt.equals("1995") || yearbuilt.equals("1996")))) 
								{
									
									System.out.println("edtoptvalue="+edtoptvalue);
									
									if (edtoptvalue.equals("")) 
									{
										cf.ShowToast("Please select the Building Permit Application Date.",0);										
									} else {
										if (cf.checkfortodaysdate(edtoptvalue) == "true") {
											if (compareyearbuiltandpermidate(yearbuilt,	edtoptvalue) == "true") {
												insertdata();
											}

											else if (cf.checkfortodaysdate(edtoptvalue) == "false") {
												if (rdiooptionA.isChecked()) 
												{
													if (yearbuilt.equals("2002") || yearbuilt.equals("2003")) 
													{
														cf.ShowToast("Please enter Building Permit Application Date after(3/1/2002).",0);
													} 
													else 
													{
														cf.ShowToast("Building Permit Application Date should not be greater than Year Built.",0);
													}
													edoptionA.setText("");
												} 
												else if (rdiooptionB.isChecked()) {
													if (yearbuilt.equals("1994") || yearbuilt.equals("1995") || yearbuilt.equals("1996")) 
													{
														cf.ShowToast("Please enter Building Permit Application Date after(9/1/1994).",0);
													} 
													else 
													{
														cf.ShowToast("Building Permit Application Date should not be greater than Year Built.",0);
													}
													edoptionB.setText("");
													edoptionB.requestFocus();
												}
											}

										} else {
											cf.ShowToast("Building Permit Application Date should not be greater than Today's Date.",0);

											if (rdiooptionA.isChecked()) {
												edoptionA.setText("");
											} else if (rdiooptionB.isChecked()) {
												edoptionB.setText("");
											}
										}
									}
								} else {
									if (edtoptvalue.equals("")) {
										insertdata();
									} else {
										if (cf.checkfortodaysdate(edtoptvalue) == "true") {
											if (checkpermitdategraterthanbuild(yearbuilt, edtoptvalue) == true) {
												if (compareyearbuiltandpermidate(yearbuilt, edtoptvalue) == "true") {
												insertdata();
												}
											} else {
												if (rdiooptionA.isChecked()) {
													edoptionA.setText("");												
												} else if (rdiooptionB.isChecked()) {
													edoptionB.setText("");	
												}
											}
										} else {
											cf.ShowToast("Building Permit Application Date should not be greater than Today's Date.",0);
											if (rdiooptionA.isChecked()) {
												edoptionA.setText("");
											} else if (rdiooptionB.isChecked()) {
												edoptionB.setText("");	
											}
										}
									}
								}

							}
						  else
						  {
								if (rdiooptionA.isChecked()) 
								{
									spnA.setSelection(0);
									cf.ShowToast("Please select the Year Built greater than 2000.",0);

								} else if (rdiooptionB.isChecked()) {
									spnB.setSelection(0);									
									cf.ShowToast("Please select the Year Built greater than 1994.",0);
								}
							}
							
						}
					}
					else if (rdiooptionC.isChecked()) 
					{
						
							insertdata();
					}
			}
			else
			{
				cf.ShowToast("Please select the option for Building Code.",0);
			}
			
			break;
		}
		
			
	}
	
	private void BC_suboption() {
		// TODO Auto-generated method stub
		System.out.println("BCSub");
		if(rdiooptionA.isChecked())	{		System.out.println("BCSub1isc");tmp=b1_q1[0];}
		else if(rdiooptionB.isChecked()) {	System.out.println("BCSub2isc");tmp=b1_q1[1];}
		else if(rdiooptionC.isChecked()) {	System.out.println("BCSub3isc");tmp=b1_q1[2];}
		else{tmp="";System.out.println("BCSubelse"+tmp);}System.out.println("whole"+tmp);
	}
	private void insertdata() {
		// TODO Auto-generated method stub
		 if(bccomment.getText().toString().trim().equals(""))
		 {
			 cf.ShowToast("Please enter the Building Code comments.", 0);
			 cf.setFocus(bccomment);
		 }
		 else
		 {
			 try
			 {
				Cursor WindBC_retrive=cf.SelectTablefunction(cf.Wind_Questiontbl, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
				if(WindBC_retrive.getCount()==0)
				 {
					 cf.arr_db.execSQL("INSERT INTO "
								+ cf.Wind_Questiontbl
								+ " (fld_srid,insp_typeid,fld_bcyearbuilt,fld_bcpermitappdate,fld_buildcode,fld_roofdeck,fld_rdothertext," +
								"	fld_roofwall,fld_roofwallsub,fld_roofwallclipsminval,fld_roofwallclipssingminval,fld_roofwallclipsdoubminval,fld_rwothertext," +
								"	fld_roofgeometry ,fld_geomlength ,fld_roofgeomtotalarea ,fld_roofgeomothertext ,fld_swr,fld_optype,fld_openprotect,fld_opsubvalue,fld_oplevelchart," +
								"   fld_opGOwindentdoorval,fld_opGOgardoorval,fld_opGOskylightsval,fld_opGOglassblockval,fld_opNGOentrydoorval,fld_opNGOgaragedoorval, " +
								"	fld_wcvalue,fld_wcreinforcement,fld_quesmodifydate)"								
								+ "VALUES ('" + cf.selectedhomeid + "','"+cf.identityval+"','" + yearbuilt + "','"+ edtoptvalue + "','" + optvalue + "','" + 0 + "','','" +
								+ 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0	+ "','','"+
								+ 0 + "','" + 0 + "','" + 0 + "','','" + 0 + "','"+ 0 +"','"+ 0 + "','" + 0 + "','" + 0 + "','" +
								+ 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','"+ 0 +"','" +
								+ 0 + "','" + 0 + "','"+ cf.datewithtime + "')");
				 }
				 else
				 {
					cf.arr_db.execSQL("UPDATE " + cf.Wind_Questiontbl
								+ " SET fld_bcyearbuilt='" + yearbuilt
								+ "',fld_bcpermitappdate ='" + edtoptvalue
								+ "',fld_buildcode='" + optvalue + "',fld_quesmodifydate='"
								+ cf.datewithtime + "' WHERE fld_srid ='" + cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'");
				 }
				 WindBC_retrive.close();
				 	Cursor c2 = cf.SelectTablefunction(cf.Wind_QuesCommtbl, "where fld_srid='"+ cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'");
					if (c2.getCount() == 0) 
					{
						cf.arr_db.execSQL("INSERT INTO "
								+ cf.Wind_QuesCommtbl
								+ " (fld_srid,insp_typeid,fld_buildcodecomments,fld_roofcovercomments,fld_roofdeckcomments,fld_roofwallcomments," +
								"fld_roofgeometrycomments,fld_swrcomments,fld_openprotectioncomments,fld_wallconscomments,fld_insoverallcomments )"
								+ "VALUES('"+cf.selectedhomeid+"','"+cf.identityval+"','"+cf.encode(bccomment.getText().toString())+"','','','','','','','','')");
					}
					else
					{
						cf.arr_db.execSQL("UPDATE " + cf.Wind_QuesCommtbl
								+ " SET fld_buildcodecomments='"+ cf.encode(bccomment.getText().toString())	+ "' WHERE fld_srid ='" + cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'");
					}
					updatecnt = "1";
			 }
			 catch (Exception E)
			 {
				 updatecnt = "0";
				 String strerrorlog="Updating Questions Buildcode - WindMit";
				 cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			 }
				if (updatecnt == "1") {
					cf.ShowToast("Building Code has been saved successfully.",0);
					cf.gotoclass(cf.identityval, WindMitRoofCover.class);
					
				}
		 }
	}
	
	
	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			cf.mYear = year;
			cf.mMonth = monthOfYear;
			cf.mDay = dayOfMonth;
			if (rdiooptionA.isChecked()) {
				edoptionA.setText(new StringBuilder()
						// Month is 0 based so add 1
						.append(cf.mMonth + 1).append("/").append(cf.mDay)
						.append("/").append(cf.mYear).append(" "));
			} else if (rdiooptionB.isChecked()) {
				edoptionB.setText(new StringBuilder()
						// Month is 0 based so add 1
						.append(cf.mMonth + 1).append("/").append(cf.mDay)
						.append("/").append(cf.mYear).append(" "));
			} else {

			}

		}
	};

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			return new DatePickerDialog(this, mDateSetListener, cf.mYear, cf.mMonth,cf.mDay);
		}
		return null;
	}
	private String yearvalidation(String yearbuilt3) {
		// TODO Auto-generated method stub
		int year = Integer.parseInt(yearbuilt3);
		String chk = null;
		if (rdiooptionA.isChecked() == true) {
			if (year > 2000 && year <= current_year) {
				chk = "true";
			} else {
				chk = "false";
			}
		} else if (rdiooptionB.isChecked() == true) {
			if (year > 1993 && year <= current_year) {
				chk = "true";
			} else {
				chk = "false";
			}
		}
		return chk;
	}
	
	
	private boolean checkpermitdategraterthanbuild(String yearbuilt3,
			String permitDate3) {
		// TODO Auto-generated method stub
		int i1 = permitDate3.indexOf("/");
		String result = permitDate3.substring(0, i1);
		int i2 = permitDate3.lastIndexOf("/");
		String result1 = permitDate3.substring(i1 + 1, i2);
		String result2 = permitDate3.substring(i2 + 1);
		result2 = result2.trim();
		int j = Integer.parseInt(result2);
		final int j1 = Integer.parseInt(result1);
		final int j2 = Integer.parseInt(result);
		yearbuilt3 = yearbuilt3.trim();
		int yr = Integer.parseInt(yearbuilt3);
		if (yr < j) {
			cf.ShowToast("Building Permit Application Date should not be greater than the Year Built.",0);
			return false;
		} else {
			return true;
		}
	}
	private String compareyearbuiltandpermidate(String yearbuilt4,String permitDate4) {

		// TODO Auto-generated method stub
		int i1 = permitDate4.indexOf("/");
		String result = permitDate4.substring(0, i1);
		int i2 = permitDate4.lastIndexOf("/");
		String result1 = permitDate4.substring(i1 + 1, i2);
		String result2 = permitDate4.substring(i2 + 1);
		result2 = result2.trim();
		int j = Integer.parseInt(result2);
		final int j1 = Integer.parseInt(result1);
		final int j2 = Integer.parseInt(result);
		yearbuilt4 = yearbuilt4.trim();
		int yr = Integer.parseInt(yearbuilt4);
		if (rdiooptionA.isChecked() == true) {

			if (yearbuilt4.equals("2002") || yearbuilt4.equals("2003")) {
				if ((j >= 2002) && (j2 >= 3 || j > 2002)
						&& (j1 >= 2 || j > 2002 || j2 > 3)) {
					if (j < 2002) {
						compchk = "true";
					} else if (j == yr || j + 1 == yr) {
						compchk = "false";
						AlertDialog.Builder builder = new AlertDialog.Builder(WindMitBuildCode.this);
						builder.setTitle("Confirmation")
								.setMessage("The Year Built and Permit Application Date are outside of typical values,Please confirm this is correct?")
								.setCancelable(false)
								.setPositiveButton("Yes",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int id) {
												if (j2 >= 3) {
													if (j1 > 1) {

														chkupdate();

													} else {

														chkupdate1();

													}
												} else {

													chkupdate1();

												}

											}

											private void chkupdate1() {
												// TODO Auto-generated method
												// stub
												compchk = "false";
												edoptionA.setText("");
											}

											private void chkupdate() {
												// TODO Auto-generated method
												// stub
												try

												{
													insertdata();
												} catch (Exception e) {
													//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ BuildCode.this +" inserting or updating buildocde datas on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
												}
											}
										})
								.setNegativeButton("No",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int id) {
												chkupdate1();
											}

											private void chkupdate1() {
												// TODO Auto-generated method
												// stub
												compchk = "false";
												edoptionA.setText("");
											}
										});

						builder.show();

					} else if (j > yr) {
						compchk = "false";
						cf.ShowToast("Building Permit Application Date Should not be greater than the Year Built.",0);
						edoptionA.setText("");
					}

				} else {
					compchk = "false";
					cf.ShowToast("Building Permit Application Date should be greater than(03/01/2002) for the Year Built(2002 or 2003).",0);
					edoptionA.setText("");

				}
			} else {
				if (j + 1 == yr || j == yr) {
					AlertDialog.Builder builder = new AlertDialog.Builder(WindMitBuildCode.this);
					builder.setTitle("Confirmation")
							.setMessage(
									"The Year Built and Permit Application Date are outside of Typical values, Please confirm this is correct?.")
							.setCancelable(false)
							.setPositiveButton("Yes",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
											chkupdate();
										}

										private void chkupdate() {
											// TODO Auto-generated method stub
											compchk = "true";
											try

											{
												insertdata();
											} catch (Exception e) {
												//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ BuildCode.this +" inserting or updating buildocode data on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
												
											}
										}
									})
							.setNegativeButton("No",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
											chkupdate1();
										}

										private void chkupdate1() {
											// TODO Auto-generated method stub
											compchk = "false";
											edoptionA.setText("");
										}
									});

					builder.show();

				} else if (j < yr) {
					compchk = "true";
				} else if (j > yr) {

					compchk = "false";
					cf.ShowToast("Building Permit Application Date should not be greater than the Year Built.",0);
					edoptionA.setText("");
				}

			}

		} else if (rdiooptionB.isChecked() == true) {
			if (yearbuilt4.equals("1994") || yearbuilt4.equals("1995")
					|| yearbuilt4.equals("1996")) {
				if (j <= yr) {
					if ((j >= 1994) && (j2 >= 9 || j > 1994)
							&& (j1 > 1 || j > 1994 || j2 > 9)) {
						if (j == yr || j + 1 == yr) {
							compchk = "false";
							AlertDialog.Builder builder = new AlertDialog.Builder(WindMitBuildCode.this);
							builder.setTitle("Confimation")
									.setMessage("Year Built and Building Permit Application Date are outside of Typical values, Please confirm this is correct?.")
									.setCancelable(false)
									.setPositiveButton(
											"Yes",
											new DialogInterface.OnClickListener() {
												public void onClick(
														DialogInterface dialog,
														int id) {
													chkupdate();
												}

												private void chkupdate() {
													// TODO Auto-generated
													// method stub
													compchk = "true";
													try

													{
														insertdata();
													} catch (Exception e) {
														//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ BuildCode.this +" inserting or updating buildocde datas on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
														
													}
												}
											})
									.setNegativeButton(
											"No",
											new DialogInterface.OnClickListener() {
												public void onClick(
														DialogInterface dialog,
														int id) {
													chkupdate1();
												}

												private void chkupdate1() {
													// TODO Auto-generated
													// method stub
													compchk = "false";
													edoptionB.setText("");
													edoptionB.requestFocus();
												}
											});

							builder.show();
						} else {
							compchk = "true";
						}
					} else {
						cf.ShowToast("Building Permit Application Date should be greater than(09/01/1994) for Year Built(1994 to 1996)",0);
						edoptionB.setText("");
						edoptionB.requestFocus();
						compchk = "false";
					}
				} else if (j > yr) {
					cf.ShowToast("Building Permit Application Date should not be greater than Year Built.",0);
					edoptionB.setText("");
					edoptionB.requestFocus();
					compchk = "false";
				} else if (j == yr || j + 1 == yr) {
					compchk = "false";
					AlertDialog.Builder builder = new AlertDialog.Builder(WindMitBuildCode.this);
					builder.setTitle("Confimation")
							.setMessage("Year Built and Building Permit Application Date are outside of Typical values, Please confirm this is correct?.")
							.setCancelable(false)
							.setPositiveButton("Yes",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
											chkupdate();
										}

										private void chkupdate() {
											// TODO Auto-generated method stub
											compchk = "true";
											try

											{
												insertdata();
											} catch (Exception e) {
												//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ BuildCode.this +" in inserting or updating buildocde datas on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
												
											}
										}
									})
							.setNegativeButton("No",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
											chkupdate1();

										}

										private void chkupdate1() {
											// TODO Auto-generated method stub
											compchk = "false";
											edoptionB.setText("");
											edoptionB.requestFocus();
										}
									});

					builder.show();

				} else {
					cf.ShowToast("Building Permit Application Date should be greater than(09/01/1994) for Year Built(1994 to 1996).",0);
					edoptionB.setText("");
					edoptionB.requestFocus();
					compchk = "false";
				}
			} else {
				if (j <= yr) {
					if (j == yr || j + 1 == yr) {
						compchk = "false";
						AlertDialog.Builder builder = new AlertDialog.Builder(WindMitBuildCode.this);
						builder.setTitle("Confimation")
								.setMessage(
										"The Year Built and Permit Application Date are outside of Typical values, Please confirm this is correct?.")
								.setCancelable(false)
								.setPositiveButton("Yes",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int id) {
												chkupdate();
											}

											private void chkupdate() {
												// TODO Auto-generated method
												// stub
												compchk = "true";
												try

												{
													insertdata();
												} catch (Exception e) {
													//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ BuildCode.this +" in inserting or updating buildocde data on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
													
												}
											}
										})
								.setNegativeButton("No",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int id) {
												chkupdate1();
											}

											private void chkupdate1() {
												// TODO Auto-generated method
												// stub
												compchk = "false";
												edoptionB.setText("");
												edoptionB.requestFocus();
											}
										});

						builder.show();

					} else {
						compchk = "true";
					}
				} else {
					cf.ShowToast("Building Permit Application Date should not be greater than Year Built.",0);
					compchk = "false";
				}
			}
		}
		return compchk;
	}
	 public boolean onKeyDown(int keyCode, KeyEvent event) {
	 		// replaces the default 'Back' button action
	 		if (keyCode == KeyEvent.KEYCODE_BACK) {
	 			cf.isaccess= cf.chk_InspTypeQuery(cf.selectedhomeid,"Roof Survey");
				if(cf.isaccess==true){
				cf.Roof=cf.Roof_insp[1];
	 			cf.goback(2);}
				else{
					cf.goback(0);}
	 			return true;
	 		}
	 		
	 		return super.onKeyDown(keyCode, event);
	 	}
	 /**on activity result for the load comments**/
	 @Override
	 		protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	 		try
	 		{
	 			if(requestCode==cf.loadcomment_code)
	 			{
	 				load_comment=true;
	 				if(resultCode==RESULT_OK)
	 				{
	 					String bccomments = ((EditText)findViewById(R.id.bccomment)).getText().toString();
	 					bccomment.setText(bccomments +" "+data.getExtras().getString("Comments"));	 
	 				}
	 			}
	 		}
	 		catch (Exception e) {
	 			// TODO: handle exception
	 			System.out.println("the erro was "+e.getMessage());
	 		}
	 	}/**on activity result for the load comments ends**/
}