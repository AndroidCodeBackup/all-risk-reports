package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;


public class CommonDeleteInsp {
	CommonFunctions cf;
	Context con;String where="";
	public CommonDeleteInsp(Context con) 
	{
		this.con =con;
		cf=new CommonFunctions(this.con);
	}
	public void delinsp_bystatus(final String temp) {
		// TODO Auto-generated method stub
		cf.createtable();
		showalert(1,temp);		
	}
	public void delinsp_all() {
		// TODO Auto-generated method stub
		cf.createtable();  
		showalert(2, "");
		
	}
	public  void delinsp_byrecord(final String temp) {
		// TODO Auto-generated method stub
		cf.createtable();  
		delinsp_byrecord1(temp);	
	}
	
	
	private void showalert(final int i,final String temp)
	{
		  final Dialog dialog1 = new Dialog(con,android.R.style.Theme_Translucent_NoTitleBar);
			dialog1.getWindow().setContentView(R.layout.alertsync);
			TextView txttitle = (TextView) dialog1.findViewById(R.id.txthelp);
			txttitle.setText("Confirmation");
			TextView txt = (TextView) dialog1.findViewById(R.id.txtid);
			txt.setText(Html.fromHtml("Do you want to delete all the inspections?"));
			Button btn_yes = (Button) dialog1.findViewById(R.id.yes);
			Button btn_cancel = (Button) dialog1.findViewById(R.id.cancel);
			btn_yes.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog1.dismiss();
					if(i==1)
					{
						delinsp_bystatus1(temp);
						cf.ShowToast("Deleted successfully.",1);
						con.startActivity(new Intent(con,HomeScreen.class));
					}
					else if(i==2)
					{
						delinsp_all1();
						cf.ShowToast("Deleted successfully.",1);
						con.startActivity(new Intent(con,HomeScreen.class));
					}
				}
				
			});
			btn_cancel.setOnClickListener(new OnClickListener()
			{
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialog1.setCancelable(true);
					dialog1.dismiss();
				}			
			});
			dialog1.setCancelable(false);		
			dialog1.show();
	}
	
	private  void delinsp_byrecord1(String temp) {
		// TODO Auto-generated method stub
		try
		{
		cf.arr_db.execSQL("Delete from "+cf.policyholder+" Where ARR_PH_SRID ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.ImageTable +" Where ARR_IM_SRID ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.FeedBackInfoTable +" Where ARR_FI_Srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.FeedBackDocumentTable +" Where ARR_FI_D_SRID ='"+temp+"'");				
		
		cf.arr_db.execSQL("Delete from "+cf.GCH_PoolPrsenttbl+" Where fld_srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.GCH_PoolFencetbl+" Where fld_srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.GCH_SummaryHazardstbl+" Where fld_srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.GCH_FireProtecttbl+" Where fld_srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.GCH_SpecHazardstbl+" Where fld_srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.GCH_Hvactbl+" Where fld_srid ='"+temp+"'");				
		cf.arr_db.execSQL("Delete from "+cf.GCH_Electrictbl+" Where fld_srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.GCH_commenttbl+" Where fld_srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.GCH_HoodDuct+" Where fld_srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.GCH_FireProtection+" Where fld_srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.GCH_Misc+" Where fld_srid ='"+temp+"'");
		
		cf.arr_db.execSQL("Delete from "+cf.SINK_Summarytbl+" Where fld_srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.SINK_Obs1tbl+" Where fld_srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.SINK_Obs2tbl+" Where fld_srid ='"+temp+"'");				
		cf.arr_db.execSQL("Delete from "+cf.SINK_Obs3tbl+" Where fld_srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.SINK_Obs4tbl+" Where fld_srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.SINK_Obstreetbl+" Where fld_srid ='"+temp+"'");
		
		cf.arr_db.execSQL("Delete from "+cf.DRY_sumcond+" Where fld_srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.DRY_attic+" Where fld_srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.DRY_hvac+" Where fld_srid ='"+temp+"'");			
		cf.arr_db.execSQL("Delete from "+cf.DRY_appliance+" Where fld_srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.DRY_room+" Where fld_srid ='"+temp+"'");
		
		cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_Attic+" Where fld_srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_Plumbing+" Where fld_srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.Plumbing_waterheater+" Where fld_srid ='"+temp+"'");		
		cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_General+" Where fld_srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_Panel+" Where fld_srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_Receptacles+" Where fld_srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_Details+" Where fld_srid ='"+temp+"'");				
		cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_GO+" Where fld_srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_Wiring+" Where fld_srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_MainPanel+" Where fld_srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_SubPanel+" Where fld_srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_SubPanelPresent+" Where fld_srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.Four_HVAC_Unit+" Where fld_srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.Four_HVAC+" Where fld_srid ='"+temp+"'");
		
		cf.arr_db.execSQL("Delete from "+cf.Wind_QuesCommtbl+" Where fld_srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.Wind_Questiontbl+" Where fld_srid ='"+temp+"'");		
		cf.arr_db.execSQL("Delete from "+cf.quesroofcover+" Where SRID ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.WindMit_WallCons+" Where fld_srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.quesorigmiti_tbl+" Where SRID ='"+temp+"'");				
		cf.arr_db.execSQL("Delete from "+cf.BI_Skylights+" Where fld_srid ='"+temp+"'");			
		cf.arr_db.execSQL("Delete from "+cf.BI_Dooropenings+" Where fld_srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.BI_Windopenings+" Where fld_srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.Comm_Aux_Master+" Where fld_srid ='"+temp+"'");			
		cf.arr_db.execSQL("Delete from "+cf.Commercial_Wind+" Where fld_srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.Communal_Areas+" Where fld_srid ='"+temp+"'");			
		cf.arr_db.execSQL("Delete from "+cf.Commercial_Building+" Where fld_srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.WindDoorSky_NA+" Where fld_srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.Communal_AreasElevation+" Where fld_srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.Comm_RestSupplement+" Where fld_srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.Comm_RestSuppAppl+" Where fld_srid ='"+temp+"'");		
		cf.arr_db.execSQL("Delete from "+cf.Wind_Commercial_Comm+" Where fld_srid ='"+temp+"'");
		cf.arr_db.execSQL("Delete from "+cf.Comm_AuxBuilding+" Where fld_srid ='"+temp+"'");		
		}
		catch(Exception e)
		{
			System.out.println("ece"+e.getMessage());
		}	
	}
	private void delinsp_bystatus1(String temp)
	{
		try
		{
		cf.arr_db.execSQL("Delete from "+cf.policyholder+" Where ARR_PH_SRID IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.ImageTable +" Where ARR_IM_SRID IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.FeedBackInfoTable +" Where ARR_FI_Srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.FeedBackDocumentTable +" Where ARR_FI_D_SRID IN "+temp);				
		
		cf.arr_db.execSQL("Delete from "+cf.GCH_PoolPrsenttbl+" Where fld_srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.GCH_PoolFencetbl+" Where fld_srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.GCH_SummaryHazardstbl+" Where fld_srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.GCH_FireProtecttbl+" Where fld_srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.GCH_SpecHazardstbl+" Where fld_srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.GCH_Hvactbl+" Where fld_srid IN "+temp);				
		cf.arr_db.execSQL("Delete from "+cf.GCH_Electrictbl+" Where fld_srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.GCH_commenttbl+" Where fld_srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.GCH_HoodDuct+" Where fld_srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.GCH_FireProtection+" Where fld_srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.GCH_Misc+" Where fld_srid IN "+temp);
		
		cf.arr_db.execSQL("Delete from "+cf.SINK_Summarytbl+" Where fld_srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.SINK_Obs1tbl+" Where fld_srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.SINK_Obs2tbl+" Where fld_srid IN "+temp);				
		cf.arr_db.execSQL("Delete from "+cf.SINK_Obs3tbl+" Where fld_srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.SINK_Obs4tbl+" Where fld_srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.SINK_Obstreetbl+" Where fld_srid IN "+temp);
		
		cf.arr_db.execSQL("Delete from "+cf.DRY_sumcond+" Where fld_srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.DRY_attic+" Where fld_srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.DRY_hvac+" Where fld_srid IN "+temp);			
		cf.arr_db.execSQL("Delete from "+cf.DRY_appliance+" Where fld_srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.DRY_room+" Where fld_srid IN "+temp);
		
		cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_Attic+" Where fld_srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_Plumbing+" Where fld_srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.Plumbing_waterheater+" Where fld_srid IN "+temp);		
		cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_General+" Where fld_srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_Panel+" Where fld_srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_Receptacles+" Where fld_srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_Details+" Where fld_srid IN "+temp);				
		cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_GO+" Where fld_srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_Wiring+" Where fld_srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_MainPanel+" Where fld_srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_SubPanel+" Where fld_srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_SubPanelPresent+" Where fld_srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.Four_HVAC_Unit+" Where fld_srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.Four_HVAC+" Where fld_srid IN "+temp);
		
		cf.arr_db.execSQL("Delete from "+cf.Wind_QuesCommtbl+" Where fld_srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.Wind_Questiontbl+" Where fld_srid IN "+temp);		
		cf.arr_db.execSQL("Delete from "+cf.quesroofcover+" Where SRID IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.WindMit_WallCons+" Where fld_srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.quesorigmiti_tbl+" Where SRID IN "+temp);				
		cf.arr_db.execSQL("Delete from "+cf.BI_Skylights+" Where fld_srid IN "+temp);			
		cf.arr_db.execSQL("Delete from "+cf.BI_Dooropenings+" Where fld_srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.BI_Windopenings+" Where fld_srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.Comm_Aux_Master+" Where fld_srid IN "+temp);			
		cf.arr_db.execSQL("Delete from "+cf.Commercial_Wind+" Where fld_srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.Communal_Areas+" Where fld_srid IN "+temp);			
		cf.arr_db.execSQL("Delete from "+cf.Commercial_Building+" Where fld_srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.WindDoorSky_NA+" Where fld_srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.Communal_AreasElevation+" Where fld_srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.Comm_RestSupplement+" Where fld_srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.Comm_RestSuppAppl+" Where fld_srid IN "+temp);		
		cf.arr_db.execSQL("Delete from "+cf.Wind_Commercial_Comm+" Where fld_srid IN "+temp);
		cf.arr_db.execSQL("Delete from "+cf.Comm_AuxBuilding+" Where fld_srid IN "+temp);	
		}catch(Exception e)
		{}
	}
	private void delinsp_all1()
	{
		try
		{
		cf.arr_db.execSQL("Delete from "+cf.policyholder);
		cf.arr_db.execSQL("Delete from "+cf.ImageTable);
		cf.arr_db.execSQL("Delete from "+cf.FeedBackInfoTable);
		cf.arr_db.execSQL("Delete from "+cf.FeedBackDocumentTable);				
		
		cf.arr_db.execSQL("Delete from "+cf.GCH_PoolPrsenttbl);
		cf.arr_db.execSQL("Delete from "+cf.GCH_PoolFencetbl);
		cf.arr_db.execSQL("Delete from "+cf.GCH_SummaryHazardstbl);
		cf.arr_db.execSQL("Delete from "+cf.GCH_FireProtecttbl);
		cf.arr_db.execSQL("Delete from "+cf.GCH_SpecHazardstbl);
		cf.arr_db.execSQL("Delete from "+cf.GCH_Hvactbl);				
		cf.arr_db.execSQL("Delete from "+cf.GCH_Electrictbl);
		cf.arr_db.execSQL("Delete from "+cf.GCH_commenttbl);
		cf.arr_db.execSQL("Delete from "+cf.GCH_HoodDuct);
		cf.arr_db.execSQL("Delete from "+cf.GCH_FireProtection);
		cf.arr_db.execSQL("Delete from "+cf.GCH_Misc);
		
		cf.arr_db.execSQL("Delete from "+cf.SINK_Summarytbl);
		cf.arr_db.execSQL("Delete from "+cf.SINK_Obs1tbl);
		cf.arr_db.execSQL("Delete from "+cf.SINK_Obs2tbl);				
		cf.arr_db.execSQL("Delete from "+cf.SINK_Obs3tbl);
		cf.arr_db.execSQL("Delete from "+cf.SINK_Obs4tbl);
		cf.arr_db.execSQL("Delete from "+cf.SINK_Obstreetbl);
		
		cf.arr_db.execSQL("Delete from "+cf.DRY_sumcond);
		cf.arr_db.execSQL("Delete from "+cf.DRY_attic);
		cf.arr_db.execSQL("Delete from "+cf.DRY_hvac);			
		cf.arr_db.execSQL("Delete from "+cf.DRY_appliance);
		cf.arr_db.execSQL("Delete from "+cf.DRY_room);
		
		cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_Attic);
		cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_Plumbing);
		cf.arr_db.execSQL("Delete from "+cf.Plumbing_waterheater);		
		cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_General);
		cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_Panel);
		cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_Receptacles);
		cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_Details);				
		cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_GO);
		cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_Wiring);
		cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_MainPanel);
		cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_SubPanel);
		cf.arr_db.execSQL("Delete from "+cf.Four_Electrical_SubPanelPresent);
		cf.arr_db.execSQL("Delete from "+cf.Four_HVAC_Unit);
		cf.arr_db.execSQL("Delete from "+cf.Four_HVAC);
		
		cf.arr_db.execSQL("Delete from "+cf.Wind_QuesCommtbl);
		cf.arr_db.execSQL("Delete from "+cf.Wind_Questiontbl);		
		cf.arr_db.execSQL("Delete from "+cf.quesroofcover);
		cf.arr_db.execSQL("Delete from "+cf.WindMit_WallCons);
		cf.arr_db.execSQL("Delete from "+cf.quesorigmiti_tbl);				
		cf.arr_db.execSQL("Delete from "+cf.BI_Skylights);			
		cf.arr_db.execSQL("Delete from "+cf.BI_Dooropenings);
		cf.arr_db.execSQL("Delete from "+cf.BI_Windopenings);
		cf.arr_db.execSQL("Delete from "+cf.Comm_Aux_Master);			
		cf.arr_db.execSQL("Delete from "+cf.Commercial_Wind);
		cf.arr_db.execSQL("Delete from "+cf.Communal_Areas);			
		cf.arr_db.execSQL("Delete from "+cf.Commercial_Building);
		cf.arr_db.execSQL("Delete from "+cf.WindDoorSky_NA);
		cf.arr_db.execSQL("Delete from "+cf.Communal_AreasElevation);
		cf.arr_db.execSQL("Delete from "+cf.Comm_RestSupplement);
		cf.arr_db.execSQL("Delete from "+cf.Comm_RestSuppAppl);		
		cf.arr_db.execSQL("Delete from "+cf.Wind_Commercial_Comm);
		cf.arr_db.execSQL("Delete from "+cf.Comm_AuxBuilding);
		}catch(Exception e)
		{}
	}
}
