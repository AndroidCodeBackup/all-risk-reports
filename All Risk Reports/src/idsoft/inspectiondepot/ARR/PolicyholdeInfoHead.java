package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TextView;

public class PolicyholdeInfoHead extends Activity implements AnimationListener {
String homeid,insp_id,table_name;
CommonFunctions cf;
boolean b=false;
public Animation myFadeInAnimation;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Bundle b = getIntent().getExtras();
		setContentView(R.layout.policyholder_info_head);
		String type="";
		cf=new CommonFunctions(this);
		if(b!=null)
		{
			homeid=b.getString("homeid");
			type=b.getString("Type");
			insp_id=b.getString("insp_id");
			if(type==null)
			{
				finish();
			}
		}
		if(type.equals("Abbreviation"))
		{
			findViewById(R.id.abbreviation_info_tbl).setVisibility(View.VISIBLE);
			
		}
		else
		{
		
        
		if(homeid==null || type==null || insp_id==null)
		{
			finish();
		}	
		else if(type.equals("Policyholder"))
		{
			
			try
			{
			Show_ph_info();
			}
			catch (Exception e) {
				// TODO: handle exception
				finish();
			}
		}
		else if(type.equals("Inspector"))
		{     
			try
			{
				Show_insp_info();
			}
			catch (Exception e) {
				// TODO: handle exception
				finish();
			}
		}
		}
		cf.getInspectorId();
	    TranslateAnimation trans=new TranslateAnimation(310,0,0,0); 
	       trans.setDuration(500);
	       trans.setFillAfter(true);
	       ((ScrollView) findViewById(R.id.anim_con)).setAnimation(trans);
	       trans.start();
	    
	}
	private void Show_ph_info() {
		// TODO Auto-generated method stub
		((TableLayout) findViewById(R.id.ph_info_tbl)).setVisibility(View.VISIBLE);
		
		Cursor c=cf.arr_db.rawQuery(" Select ARR_PH_FirstName||' '||ARR_PH_LastName as name,ARR_PH_Address1||' '|| ARR_PH_Address2 as address,ARR_PH_City,ARR_PH_County,ARR_PH_State,ARR_PH_HomePhone,ARR_PH_Policyno FROM "+cf.policyholder+" Where ARR_PH_SRID='"+homeid+"' and ARR_PH_InspectorId='"+insp_id+"' limit 1 ", null);
		if(c.getCount()>0)
		{ c.moveToFirst();
			if(c.getString(c.getColumnIndex("name"))!=null)
				((TextView) findViewById(R.id.ph_name)).setText(cf.decode(c.getString(c.getColumnIndex("name"))).trim());
			if(c.getString(c.getColumnIndex("address"))!=null)
				((TextView) findViewById(R.id.ph_address)).setText(cf.decode(c.getString(c.getColumnIndex("address"))).trim());
			if(c.getString(c.getColumnIndex("ARR_PH_City"))!=null)
				((TextView) findViewById(R.id.ph_city)).setText(cf.decode(c.getString(c.getColumnIndex("ARR_PH_City"))).trim());
			if(c.getString(c.getColumnIndex("ARR_PH_County"))!=null)
				((TextView) findViewById(R.id.ph_county)).setText(cf.decode(c.getString(c.getColumnIndex("ARR_PH_County"))).trim());
			if(c.getString(c.getColumnIndex("ARR_PH_State"))!=null)
				((TextView) findViewById(R.id.ph_state)).setText(cf.decode(c.getString(c.getColumnIndex("ARR_PH_State"))).trim());
			if(c.getString(c.getColumnIndex("ARR_PH_HomePhone"))!=null)
				((TextView) findViewById(R.id.ph_ph_no)).setText(cf.decode(c.getString(c.getColumnIndex("ARR_PH_HomePhone"))).trim());
			if(c.getString(c.getColumnIndex("ARR_PH_Policyno"))!=null)
				((TextView) findViewById(R.id.ph_p_no)).setText(cf.decode(c.getString(c.getColumnIndex("ARR_PH_Policyno"))).trim());
			
		}
	}
	private void Show_insp_info() {     
		// TODO Auto-generated method stub
		((TableLayout) findViewById(R.id.insp_info_tbl)).setVisibility(View.VISIBLE);
		
		
		Cursor c=cf.arr_db.rawQuery(" Select Fld_InspectorFirstName||'  '||Fld_InspectorLastName	 as name,Fld_InspectorAddress as address,Fld_InspectorCompanyName,Fld_InspectorCompanyId,Fld_InspectorPhotoExtn,Fld_InspectorEmail FROM "+cf.inspectorlogin+" Where Fld_InspectorId='"+insp_id+"' limit 1 ", null);
		if(c.getCount()>0)
		{ c.moveToFirst();
			if(c.getString(c.getColumnIndex("name"))!=null)
				((TextView) findViewById(R.id.insp_name)).setText(cf.decode(c.getString(c.getColumnIndex("name"))).trim());
			if(c.getString(c.getColumnIndex("address"))!=null)
				((TextView) findViewById(R.id.insp_address)).setText(cf.decode(c.getString(c.getColumnIndex("address"))).trim());
			
			if(c.getString(c.getColumnIndex("Fld_InspectorCompanyName"))!=null)
				((TextView) findViewById(R.id.insp_company)).setText(cf.decode(c.getString(c.getColumnIndex("Fld_InspectorCompanyName"))).trim());
			if(c.getString(c.getColumnIndex("Fld_InspectorEmail"))!=null)
				((TextView) findViewById(R.id.insp_email)).setText(cf.decode(c.getString(c.getColumnIndex("Fld_InspectorEmail"))).trim());
			/*if(c.getString(c.getColumnIndex("Fld_InspectorCompanyId"))!=null)
				((TextView) findViewById(R.id.insp_company_id)).setText(cf.decode(c.getString(c.getColumnIndex("Fld_InspectorCompanyId"))).trim());*/
			String Ext=c.getString(c.getColumnIndex("Fld_InspectorPhotoExtn"));
			try {
				File outputFile = new File(this.getFilesDir()+"/"+cf.Insp_id.toString()+Ext);
				
				BitmapFactory.Options o = new BitmapFactory.Options();
				o.inJustDecodeBounds = true;
				BitmapFactory.decodeStream(new FileInputStream(outputFile),
						null, o);
				final int REQUIRED_SIZE = 200;
				int width_tmp = o.outWidth, height_tmp = o.outHeight;
				int scale = 1;
				while (true) {
					if (width_tmp / 2 < REQUIRED_SIZE
							|| height_tmp / 2 < REQUIRED_SIZE)
						break;
					width_tmp /= 2;
					height_tmp /= 2;
					scale *= 2;
				}
				BitmapFactory.Options o2 = new BitmapFactory.Options();
				o2.inSampleSize = scale;
				Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(
						outputFile), null, o2);
				BitmapDrawable bmd = new BitmapDrawable(bitmap);
				((ImageView) findViewById(R.id.insp_photo)).setImageDrawable(bmd);
				
				
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("the value in the s=  ffds  ff"+e.getMessage());
			}
			
			//File f =new File(+insp_id+Ext);
			
		}
	}
	
	public void clicker(View v)

	{
		/*TranslateAnimation trans1=new TranslateAnimation(0,310,0,0); 
	       trans1.setDuration(1500);
	       trans1.setFillAfter(true);
	       ((ScrollView) findViewById(R.id.anim_con)).setAnimation(trans1);
	       trans1.start();
	       trans1.setAnimationListener(new AnimationListener() {
			
			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
			System.out.println("Animation starts");	
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				
			}
		});*/
		
	       finish();
	}
	
	@Override
	public void onAnimationEnd(Animation animation) {
		// TODO Auto-generated method stub
		
			((RelativeLayout) findViewById(R.id.ph_head_scr)).setBackgroundColor(getResources().getColor(R.color.transpahrrant));
		
	}
	@Override
	public void onAnimationRepeat(Animation animation) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onAnimationStart(Animation animation) {
		// TODO Auto-generated method stub
		
			((RelativeLayout) findViewById(R.id.ph_head_scr)).setBackgroundColor(getResources().getColor(R.color.transpahrrant));
		
	}
	
	
		  
}
