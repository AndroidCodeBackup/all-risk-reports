/*
************************************************************
* Project: ALL RISK REPORT
* Module : WindMitCommRestSupp.java
* Creation History:
Created By : Nalini on 10/24/2012
* Modification History:
Last Modified By : Gowri on 11/28/2012
************************************************************ 
*/
package idsoft.inspectiondepot.ARR;


import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.Electrical.Spin_Selectedlistener;
import idsoft.inspectiondepot.ARR.WindMitCommAuxbuildings.Aux_EditClick;
import idsoft.inspectiondepot.ARR.WindMitCommAuxbuildings.checklistenetr;
import idsoft.inspectiondepot.ARR.WindMitCommareas.textwatcher;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class WindMitCommRestsupp extends Activity{
	int k,rwsrestsupp,identity=0,rwscommesupp,rwsrestcomm,applid,yoc=0;
	CommonFunctions cf;Cursor c1;
	String applnotappl="0";
	public RadioButton yrbuiltoption[] = new RadioButton[3];
	boolean load_comment=true;
	protected static final int DATE_DIALOG_ID = 0;
	Spinner volts_spin,doors_spin,burners_spin,agespin,applspin,typepffuelspin;
	ArrayAdapter voltsadap,bruneradap,voltadap,dooradap,ageadap,appladap,numbadap,fueladap;
	RadioGroup underhoodrdg,surfaceprotrdgp,yearrdgp,domesticrdgp,commercialrdgrp;
	String burnerstxt="",voltstxt="",doorstxt="",headername="",agestr,apppltxt="",underhoodrdgtxt="",surfaceprotrdgtxt="",yearagetxt="",appl_content_val="",applotherxt="",ageothertxt="",
			domestictxt="",commercialtxt="",typefueltxt="",typefuelotherxt="",voltstxtotherxt="",updatecnt="",Type_FUEL_SPLIT="",AGE_SPLIT,Type_VOLT_SPLIT="";
	String spnappl[] ={"--Select--", "Ranges","Ovens","Deep Fryers","Grills","Steam","Cleanliness","Compressors","Refrigerator","Bread warmer","Gas Stove","Double oven","Disposal","Stove top","Microwave","Other"};
	String spntypeoffuel[] ={"--Select--", "Gas","Electric","Wood/Coal","Oil","Other"};
	String spnburner[] ={"--Select--","1","2","3","4","5","6","7","8","9","10"};
	String spndoors[] ={"--Select--","0","1","2","3","4","5","6","7","8","9","10","N/A"};
	String spnvolts[] ={"--Select--","<60 VOLTS","60 VOLTS","100 VOLTS","150 VOLTS","200 VOLTS","N/A","Other"};
	String[] rest_supp_data,rest_supp_type,rest_supp_other,rest_supp_id;
	LinearLayout subpnlshw;
	TableLayout dynviewerst;
	boolean domesticchk=false,commchk=false,underhoodchk=false,surfprot=false,agechk=false;			
	Cursor currest,curappl,curappliances;
	boolean f[]=new boolean[7];
	String appliance_na="";
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cf=new CommonFunctions(this);	 
        Bundle extras = getIntent().getExtras();
  		if (extras != null) {
  			cf.getExtras(extras);				
  	 	}
  		  setContentView(R.layout.windmitcommrestsupp);
  		
          cf.getDeviceDimensions();
          
          LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
          if(cf.identityval==33)
          {
            hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","Commercial Type II","Restaurant Supplement",3,1,cf));
          }
          else if(cf.identityval==34)
          {
              hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","Commercial Type III","Restaurant Supplement",3,1,cf));
          }
          else if(cf.identityval==35)
          {
              hdr_layout.addView(new HdrOnclickListener(this,1,"General Conditions & Hazards","Restaurant Supplement",4,0,cf));
          }
          cf.mainmenu_layout = (LinearLayout) findViewById(R.id.header); //** HEADER MENU **//
          cf.mainmenu_layout.setMinimumWidth(cf.wd);
          cf.submenu_layout = (LinearLayout) findViewById(R.id.submenu);//** WINDMIT TYPE SUBMENU **//	
          
          if(cf.identityval==33 || cf.identityval==34)
          {
        	  cf.mainmenu_layout.addView(new MyOnclickListener(this, 3, 0,0, cf));
        	  cf.type_submenulayout =(LinearLayout) findViewById(R.id.typesubmenu); //** WINDMIT TYPE SUBMENU **//
	          cf.type_submenulayout.addView(new MyOnclickListener(this, 300, 1,0, cf));
	          cf.windmittypechk(cf.identityval,((RadioButton)findViewById(R.id.b1802_chk)),((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((TextView)findViewById(R.id.commtxt)),((ImageView)findViewById(R.id.editbno))); // To check the checkbox of choosen inspection type
	          cf.uncheckcommercial(((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((RadioButton)findViewById(R.id.b1802_chk))); 
	      	    
	            
	          cf.submenu_layout .addView(new MyOnclickListener(this,cf.identityval,1,312, cf));
	          
	          cf.innersubmenu_layout = (LinearLayout) findViewById(R.id.innersubmenu);//** WINDMIT TYPE SUBMENU **//
	          cf.innersubmenu_layout.addView(new MyOnclickListener(this,cf.identityval,1,146, cf));
	          
	          cf.mainlayout = (LinearLayout)findViewById(R.id.bc_table);
          }
          else
          {
        	  cf.mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 4, 0,0,cf));
        	  cf.submenu_layout.addView(new MyOnclickListener(this, 35, 1,0,cf));
        	//  cf.submenu_layout .addView(new MyOnclickListener(this,cf.identityval,1,312, cf));
          }
          cf.save = (Button)findViewById(R.id.save);
          cf.tblrw = (TableRow)findViewById(R.id.row2);
          cf.tblrw.setMinimumHeight(cf.ht);	
          Declarations();
          cf.CreateARRTable(36); 
          cf.CreateARRTable(37); cf.CreateARRTable(38);
          Rest_Supp_HFM_SetValue();
          Rest_Supp_Appl_SetValue();
    }
	private void Rest_Supp_HFM_SetValue() {
		// TODO Auto-generated method stub
		selectresttbl();
		if(c1.getCount()>0)
		{
			
			if(appliance_na.equals("0") || appliance_na.equals("") || curappliances.getCount()>0)
			{
				((TextView)findViewById(R.id.savetxtappliance)).setVisibility(cf.v1.VISIBLE);
				((LinearLayout)findViewById(R.id.applin)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.shwappl)).setEnabled(true);
				((ImageView)findViewById(R.id.shwappl)).setVisibility(cf.v1.GONE);
				((ImageView)findViewById(R.id.hdwappl)).setVisibility(cf.v1.VISIBLE);
				((CheckBox)findViewById(R.id.appliancenotapplicable)).setChecked(false);
			}
			else
			{
				((CheckBox)findViewById(R.id.appliancenotapplicable)).setChecked(true);
				((LinearLayout)findViewById(R.id.applin)).setVisibility(cf.v1.GONE);
				((TextView)findViewById(R.id.savetxtappliance)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.shwappl)).setEnabled(false);
				((ImageView)findViewById(R.id.shwappl)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdwappl)).setVisibility(cf.v1.GONE);
			}
			
		}
		else
		{
			hideotherlayouts();
			Cursor c = cf.arr_db.rawQuery("SELECT * FROM "	+ cf.Comm_RestSuppAppl + " WHERE fld_srid='"
					+ cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'", null);		
			if(c.getCount()==0)
			{
				((CheckBox)findViewById(R.id.appliancenotapplicable)).setChecked(true);
			}
			

		}
		Cursor c3 = cf.arr_db.rawQuery("SELECT * FROM " + cf.Wind_Commercial_Comm + " WHERE fld_srid='" + cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'", null);				
		if(c3.getCount()>0)
		{
			 c3.moveToFirst();
			 if(!cf.decode(c3.getString(c3.getColumnIndex("fld_restsuppcomments"))).equals(""))
			 {
				 ((EditText)findViewById(R.id.restsuppcomment)).setText(cf.decode(c3.getString(c3.getColumnIndex("fld_restsuppcomments"))));
			 }
		}
			
	}
	
	private void Rest_Supp_Appl_SetValue() {
		// TODO Auto-generated method stub
			try
			{
				Cursor c1 = cf.arr_db.rawQuery("SELECT * FROM "	+ cf.Comm_RestSuppAppl + " WHERE fld_srid='"
						+ cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'", null);		
				
				rwsrestsupp = c1.getCount();
				rest_supp_type = new String[rwsrestsupp];
				rest_supp_data = new String[rwsrestsupp];
				rest_supp_other = new String[rwsrestsupp];
				rest_supp_id= new String[rwsrestsupp];
				 if(rwsrestsupp>0)
				 {
					 ((TableLayout)findViewById(R.id.applin)).setVisibility(cf.v1.VISIBLE);
						((TextView)findViewById(R.id.savetxtappliance)).setVisibility(cf.v1.VISIBLE);
					c1.moveToFirst();
					int i=0;
					do { 
						rest_supp_id[i]= c1.getString(c1.getColumnIndex("ApplID"));
						 rest_supp_type[i]= c1.getString(c1.getColumnIndex("fld_appliancetype"));
						 rest_supp_data[i]= cf.decode(c1.getString(c1.getColumnIndex("fld_appldata")));
						 rest_supp_other[i]= cf.decode(c1.getString(c1.getColumnIndex("fld_applianceother")));
						 i++;
							} while (c1.moveToNext());		
						 subpnlshw.setVisibility(cf.v1.VISIBLE);
						 ((TableLayout)findViewById(R.id.tableLayoutNote)).setVisibility(cf.v1.VISIBLE);
						 ((TextView)findViewById(R.id.txtbriefexplanation)).setText(Html.fromHtml("Dome."+cf.redcolor+"  - Domestic Grade" + "<br/>"
											+ "Comm. "+cf.redcolor+"  - Commercial Grade" + "<br/>"
											+ "UH "+cf.redcolor+"  - Under Hood with Filters " + "<br/>"
											+ "SP"+cf.redcolor+" - Surface Protection " + "<br/>"));
						 
							show_rest_supp_Value();
				 }
				 else
				 {
					 subpnlshw.setVisibility(cf.v1.GONE);((TableLayout)findViewById(R.id.tableLayoutNote)).setVisibility(cf.v1.GONE);
				 }
			 }
			 catch(Exception e)
			 {
				 System.out.println("wc erro "+e.getMessage());
			 }			
	}
	private void show_rest_supp_Value() {
		// TODO Auto-generated method stub
		dynviewerst.removeAllViews();
		LinearLayout h1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvac_listview_header, null);
		LinearLayout th= (LinearLayout)h1.findViewById(R.id.restsupplement_hdr);th.setVisibility(cf.v1.VISIBLE);
		TableLayout.LayoutParams lp = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
	 	lp.setMargins(2, 0, 2, 2); 
	 	h1.removeAllViews(); 
	 	
	 	dynviewerst.addView(th,lp);	
	 	dynviewerst.setVisibility(View.VISIBLE);	
	 	
	 	for (int i = 0; i < rwsrestsupp; i++) 
		{	
			TextView No,txtrestsupptype,txtfueltype,txtnoofappl,txtnoofburners,txtdomestic,txtcommercial,txtunderhood,txtsurfpro,txtage,txtnoofvolts,txtnoofdoors;
			ImageView edit,delete,duplicate;
			
			LinearLayout t1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvac_listview, null);	
			LinearLayout t = (LinearLayout)t1.findViewById(R.id.restsupplement);t.setVisibility(cf.v1.VISIBLE);
			t.setId(44444+i);/// Set some id for further use
			
		 	No= (TextView) t.findViewWithTag("Number");			
		 	txtrestsupptype= (TextView) t.findViewWithTag("Rest_Supp_Type"); 	
		 	txtfueltype= (TextView) t.findViewWithTag("Fuel_Type");	
		 	txtnoofappl= (TextView) t.findViewWithTag("No_Appl");	
		 	txtnoofburners= (TextView) t.findViewWithTag("Burners");
		 	txtnoofvolts= (TextView) t.findViewWithTag("Volts");
		 	txtnoofdoors= (TextView) t.findViewWithTag("Doors");
		 	txtdomestic= (TextView) t.findViewWithTag("Domestic");	
		 	txtcommercial= (TextView) t.findViewWithTag("Comm");	
		 	txtunderhood= (TextView) t.findViewWithTag("Under");	
		 	txtsurfpro= (TextView) t.findViewWithTag("Surface_prot");	
		 	txtage= (TextView) t.findViewWithTag("Age");	
		 	
		 	No.setText(String.valueOf(i+1));	
		 	if(rest_supp_type[i].equals("Other"))
		 	{
		 		txtrestsupptype.setText(rest_supp_type[i]+" ("+rest_supp_other[i]+")");
		 	}
		 	else
		 	{
		 		txtrestsupptype.setText(rest_supp_type[i]);	
		 	} 	
			String Rest_Supp_Spilt[] = rest_supp_data[i].split("&#94;");			
			String Type_Fuel_Split[] = Rest_Supp_Spilt[0].split("&#39;");			
			if(Type_Fuel_Split.length>1)
			{
				Type_FUEL_SPLIT=Type_Fuel_Split[0]+" ("+Type_Fuel_Split[1]+")";
			}
			else{Type_FUEL_SPLIT=Type_Fuel_Split[0];}			

			txtfueltype.setText(Type_FUEL_SPLIT);
		 	txtnoofappl.setText(Rest_Supp_Spilt[1]);
		 	if(Rest_Supp_Spilt[2].equals("--Select--"))
		 	{
		 		txtnoofburners.setText("-");
		 	}
		 	else
		 	{
		 		txtnoofburners.setText(Rest_Supp_Spilt[2]);	
		 	}		 	
		 	
		 	String Volts_Split[] = Rest_Supp_Spilt[3].split("&#39;");
		 	if(Volts_Split.length>1)
		 	{
		 		Type_VOLT_SPLIT=Volts_Split[0]+" ("+Volts_Split[1]+")";
		 	}
		 	else{Type_VOLT_SPLIT=Volts_Split[0];}	
			
		 	txtnoofvolts.setText(Type_VOLT_SPLIT);
		 	
		  	txtnoofdoors.setText(Rest_Supp_Spilt[4]);
		 	txtdomestic.setText(Rest_Supp_Spilt[5]);		
		 	txtcommercial.setText(Rest_Supp_Spilt[6]);
		 	txtunderhood.setText(Rest_Supp_Spilt[7]);
		 	txtsurfpro.setText(Rest_Supp_Spilt[8]);
		 	
		 	String age_split[] = Rest_Supp_Spilt[9].split("&#39;");
		 	if(age_split[1].equals(""))
		 	{
		 		AGE_SPLIT = age_split[0]+" - "+age_split[2];
		 	}
		 	else
		 	{
		 		AGE_SPLIT = age_split[0]+"("+age_split[1]+") - "+age_split[2];
		 	}
		 	txtage.setText(AGE_SPLIT);
		 	
		 	edit= (ImageView) t.findViewWithTag("Rest_edit");			 
		 	edit.setTag(rest_supp_id[i]);
		 	edit.setOnClickListener(new RestSupp_EditClick(1,rest_supp_id[i]));
		 	delete= (ImageView) t.findViewWithTag("Rest_delete");
		 	delete.setTag(rest_supp_id[i]);
		 	delete.setOnClickListener(new RestSupp_EditClick(2,rest_supp_id[i]));
		 	
		 	duplicate= (ImageView) t.findViewWithTag("Rest_duplicate");
		 	duplicate.setTag(rest_supp_id[i]);
		 	duplicate.setOnClickListener(new OnClickListener() {					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
					try
					{
					final int i=Integer.parseInt(v.getTag().toString());
					String rmno="";
					try{
						Cursor rmret=cf.SelectTablefunction(cf.Comm_RestSuppAppl, " Where ApplID='"+i+"'");
						if(rmret.getCount()>0)
						{
							rmret.moveToFirst();
							rmno=rmret.getString(rmret.getColumnIndex("fld_appliancetype"));
							
						}
					}catch(Exception e){}
					AlertDialog.Builder builder = new AlertDialog.Builder(WindMitCommRestsupp.this);
					builder.setMessage("Do you want to duplicate the Appliance?")
							.setCancelable(false)
							.setPositiveButton("Yes",
									new DialogInterface.OnClickListener() {

										public void onClick(DialogInterface dialog,
												int id) {
											LayoutInflater factory = LayoutInflater.from(WindMitCommRestsupp.this);
                                          final View textEntryView = factory.inflate(R.layout.duplicatealert, null);
                                          final EditText input1 = (EditText) textEntryView.findViewById(R.id.dupid);
                                          
                                          InputFilter[] FilterArray = new InputFilter[1];  
                  						FilterArray[0] = new InputFilter.LengthFilter(1);  
                  						input1.setFilters(FilterArray);
                                          
											input1.setText("", TextView.BufferType.EDITABLE);
											
											final AlertDialog.Builder alert = new AlertDialog.Builder(WindMitCommRestsupp.this);
											alert.setCancelable(false);
											alert.setIcon(R.drawable.yell_alert_icon).setTitle(
											"Duplication:").setView(
											textEntryView).setPositiveButton("Duplicate",
											new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog,
											int whichButton) {
												try{
													Cursor c1=cf.SelectTablefunction(cf.Comm_RestSuppAppl, " Where ApplID='"+i+"'");
													if(c1.getCount()>0)
													{
														c1.moveToFirst();
														String appl_type="",appl_other="",appl_content="";
														appl_type=c1.getString(3);
														appl_other=c1.getString(4);
														appl_content=c1.getString(5);
														int j;
														for(j=0;j<Integer.parseInt(input1.getText().toString());)
														{
															cf.arr_db.execSQL("INSERT INTO "
																	+ cf.Comm_RestSuppAppl
																	+ " (fld_srid,insp_typeid,fld_appliancetype,fld_applianceother,fld_appldata)"
																	+ "VALUES ('" + cf.selectedhomeid + "','"+cf.identityval+"','"+ appl_type + "','"+cf.encode(appl_other)+"','"+appl_content+"')");
																j=j+1;	
														}
														 cf.ShowToast("Duplicated Successfully.", 1);
														 UncheckAppliances();      
														 Rest_Supp_Appl_SetValue();
														 dialog.dismiss();														
													}
												}catch(Exception e){}
												dialog.dismiss();
											
											}
											}).setNegativeButton("Cancel",
											new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog,
											int whichButton) {
												dialog.dismiss();
											}
											});
											alert.show();
										
										}
									})
							.setNegativeButton("No",
									new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,
												int id) {

											dialog.cancel();
										}
									});
					AlertDialog al=builder.create();
					al.setTitle("Confirmation");
					al.setIcon(R.drawable.alertmsg);
					al.setCancelable(false);
					al.show();
					 
					}
					catch (Exception e) {
						// TODO: handle exception
					}
					}
              });
		 	
		 	t1.removeAllViews();
			dynviewerst.addView(t,lp);
		}
	 	
	}
	class RestSupp_EditClick implements OnClickListener
	{
		int type,edit;  
		RestSupp_EditClick(int edit,String id1)
		{
			this.edit=edit;
			this.type=Integer.parseInt(id1);
		}
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if(edit==1)
			{
				identity=1;
				((Button)findViewById(R.id.saveapp)).setText("Update");
				updateAppl(type);
				
			}
			else if(edit==2)
			{
				AlertDialog.Builder builder = new AlertDialog.Builder(WindMitCommRestsupp.this);				
						builder.setMessage("Do you want to delete the selected Appliance?")
								.setCancelable(false)
								.setPositiveButton("Yes",
										new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog, int id) {
												try
												{
													cf.arr_db.execSQL("DELETE FROM "+cf.Comm_RestSuppAppl+" WHERE ApplID='"+type+"' and insp_typeid='"+cf.identityval+"'");
													cf.ShowToast("Deleted Successfully.", 0);
													Rest_Supp_Appl_SetValue();
													clear();
												}
												catch (Exception e) {
													// TODO: handle exception
												}
											}
										})
								.setNegativeButton("No",
										new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog,
													int id) {

												dialog.cancel();
											}
										});
						builder.setTitle("Confirmation");
						builder.setIcon(R.drawable.alertmsg);
						builder.show();					
						
			}
		}
	}
	private void updateAppl(int restvalue) {
		// TODO Auto-generated method stub
		//layoutfocus((RelativeLayout)findViewById(R.id.applianceheader));
		applid = restvalue; 
		Cursor c2=cf.SelectTablefunction(cf.Comm_RestSuppAppl, " WHERE fld_srid='"+cf.selectedhomeid+"' and ApplID='"+applid+"' and insp_typeid='"+cf.identityval+"'");
		if(c2.getCount()>0)
		{
			c2.moveToFirst();
			String restsuppappl_value = c2.getString(c2.getColumnIndex("fld_appliancetype"));
			String restsuppappl_other = cf.decode(c2.getString(c2.getColumnIndex("fld_applianceother")));
			String restsuppdataapp_value = cf.decode(c2.getString(c2.getColumnIndex("fld_appldata")));
			int spinnerPosition1 = appladap.getPosition(restsuppappl_value);
			applspin.setSelection(spinnerPosition1);
			//applspin.setEnabled(false);
			
			if(restsuppappl_value.equals("Other"))
			{
				((EditText)findViewById(R.id.edappl_otr)).setVisibility(cf.v1.VISIBLE);
				((EditText)findViewById(R.id.edappl_otr)).setText(restsuppappl_other);
				((EditText)findViewById(R.id.edappl_otr)).setEnabled(false);
			}else
			{
				((EditText)findViewById(R.id.edappl_otr)).setVisibility(cf.v1.GONE);
				((EditText)findViewById(R.id.edappl_otr)).setText("");
				((EditText)findViewById(R.id.edappl_otr)).setEnabled(true);
			}
			
			String restsuppdataarr[] = restsuppdataapp_value.split("&#94;");			
			String typefuelspliarr[] = restsuppdataarr[0].split("&#39;");
			
			int spinnerPosition2 = fueladap.getPosition(typefuelspliarr[0]);
			typepffuelspin.setSelection(spinnerPosition2);
			
			if(typefuelspliarr[0].equals("Other")){
				((EditText)findViewById(R.id.edtypeoffuel_otr)).setVisibility(cf.v1.VISIBLE);
				((EditText)findViewById(R.id.edtypeoffuel_otr)).setText(typefuelspliarr[1]);
			}else{
				((EditText)findViewById(R.id.edtypeoffuel_otr)).setVisibility(cf.v1.GONE);}
			
			((EditText)findViewById(R.id.ednumbers)).setText(restsuppdataarr[1]);	
			
			int spinnerPosition3 = bruneradap.getPosition(restsuppdataarr[2]);
			 burners_spin.setSelection(spinnerPosition3);
			
			String voltsspliarr[] = restsuppdataarr[3].split("&#39;");
			int spinnerPosition4 = voltsadap.getPosition(voltsspliarr[0]);
			volts_spin.setSelection(spinnerPosition4);
			if(voltsspliarr[0].equals("Other")){
				((EditText)findViewById(R.id.edvolt_otr)).setVisibility(cf.v1.VISIBLE);
				((EditText)findViewById(R.id.edvolt_otr)).setText(voltsspliarr[1]);
			}else{
				((EditText)findViewById(R.id.edvolt_otr)).setVisibility(cf.v1.GONE);}
			
			
			int spinnerPosition5 = dooradap.getPosition(restsuppdataarr[4]);
			doors_spin.setSelection(spinnerPosition5);
			
			((RadioButton) domesticrdgp.findViewWithTag(restsuppdataarr[5])).setChecked(true);
		 	((RadioButton) commercialrdgrp.findViewWithTag(restsuppdataarr[6])).setChecked(true);
		 	((RadioButton) underhoodrdg.findViewWithTag(restsuppdataarr[7])).setChecked(true);;
		 	((RadioButton) surfaceprotrdgp.findViewWithTag(restsuppdataarr[8])).setChecked(true);;
		 	
		 	yrbuiltoption[0].setChecked(false);
			yrbuiltoption[1].setChecked(false);
			yrbuiltoption[2].setChecked(false);
		 	
		 	if(!restsuppdataarr[9].equals(""))
		 	{
		 		yoc=1;
			 	String age_split[] = restsuppdataarr[9].split("&#39;");
				int spinnerPosition9 = ageadap.getPosition(age_split[0]);
			 	agespin.setSelection(spinnerPosition9);;
			 	
			 	if(age_split[0].equals("Other"))
			 	{
			 		cf.setFocus(((EditText)findViewById(R.id.age_otr)));
			 		((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.VISIBLE);
					((EditText)findViewById(R.id.age_otr)).setText(age_split[1]);
			 	}
			 	else
			 	{
			 		((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.GONE);
			 	}
			 	
			 	cf.setRadioBtnValue(age_split[2],yrbuiltoption); 
			 	
		 	}
		}
	}
	private void Declarations() {
		// TODO Auto-generated method stub
		cf.setupUI((ScrollView) findViewById(R.id.scr));
		
		applspin=(Spinner) findViewById(R.id.appl_spin);
		 appladap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, spnappl);
		 appladap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 applspin.setAdapter(appladap);
		 applspin.setOnItemSelectedListener(new  Spin_Selectedlistener(1));		 
	 
		 typepffuelspin=(Spinner) findViewById(R.id.fuel_spin);
		 fueladap = new ArrayAdapter(this,android.R.layout.simple_spinner_item,spntypeoffuel);
		 fueladap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 typepffuelspin.setAdapter(fueladap);
		 typepffuelspin.setOnItemSelectedListener(new  Spin_Selectedlistener(2));		 
		 
		 agespin=(Spinner) findViewById(R.id.age_spin);
		 ageadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, cf.yearbuilt);
		 ageadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 agespin.setAdapter(ageadap);
		 agespin.setOnItemSelectedListener(new  Spin_Selectedlistener(3));	 
		 
		 burners_spin=(Spinner) findViewById(R.id.noofburners_spin);
		 bruneradap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, spnburner);
		 bruneradap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 burners_spin.setAdapter(bruneradap);
		 burners_spin.setOnItemSelectedListener(new  Spin_Selectedlistener(4));	 
		 
		 volts_spin=(Spinner) findViewById(R.id.noofvolts_spin);
		 voltsadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, spnvolts);
		 voltsadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 volts_spin.setAdapter(voltsadap);
		 volts_spin.setOnItemSelectedListener(new  Spin_Selectedlistener(5));
		 final EditText etvolt = (EditText)findViewById(R.id.edvolt_otr);
		 etvolt.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if(etvolt.getText().toString().trim().equals("0"))
				{
					etvolt.setText("");
				}
				if(etvolt.getText().toString().trim().equals(" "))
				{
					etvolt.setText("");
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		 
		 doors_spin=(Spinner) findViewById(R.id.noofdoors_spin);
		 dooradap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, spndoors);
		 dooradap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 doors_spin.setAdapter(dooradap);
		 doors_spin.setOnItemSelectedListener(new  Spin_Selectedlistener(6));	 
		 
		underhoodrdg = (RadioGroup)findViewById(R.id.underhood_rdgp);
		underhoodrdg.setOnCheckedChangeListener(new checklistenetr(1));		
		surfaceprotrdgp = (RadioGroup)findViewById(R.id.surf_prot_rdg);
		surfaceprotrdgp.setOnCheckedChangeListener(new checklistenetr(2));		
		/*yearrdgp = (RadioGroup)findViewById(R.id.year_rdgp);
		yearrdgp.setOnCheckedChangeListener(new checklistenetr(3));*/		
		domesticrdgp = (RadioGroup)findViewById(R.id.domestic_rdgp);
		domesticrdgp.setOnCheckedChangeListener(new checklistenetr(4));		
		commercialrdgrp = (RadioGroup)findViewById(R.id.commercial_rdgp);
		commercialrdgrp.setOnCheckedChangeListener(new checklistenetr(5));
		
		dynviewerst = (TableLayout)findViewById(R.id.dynamicrest);
	    subpnlshw = (LinearLayout)findViewById(R.id.subpnlshow);		
	     ((EditText)findViewById(R.id.restsuppcomment)).addTextChangedListener(new textwatcher(1));
	    ((EditText)findViewById(R.id.ednumbers)).addTextChangedListener(new textwatcher(2));
	    
	    yrbuiltoption[0] = (RadioButton)findViewById(R.id.yrbuiltrdio1);
		 yrbuiltoption[1] = (RadioButton)findViewById(R.id.yrbuiltrdio2);
		 yrbuiltoption[2] = (RadioButton)findViewById(R.id.yrbuiltrdio3);
	}
	class textwatcher implements TextWatcher
	{
	       public int type;
	         textwatcher(int type)
	    	{
	    		this.type=type;
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {
	    		// TODO Auto-generated method stub
	    		if(this.type==1)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.restsupp_tv_type1),500); 
	    		}
	    		else if(this.type==2)
	    		{
	    			 if(!s.toString().equals("")){
	    			     if(Character.toString(s.toString().charAt(0)).equals("0"))
		    			 {
		    				 cf.ShowToast("Invalid Entry! Please enter the valid Number of Units.", 0);
		    				 ((EditText)findViewById(R.id.ednumbers)).setText("");cf.hidekeyboard();
		    			 }
	    			 } 
	    		}	
	    	}
	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,

	    			int after) {
	    		// TODO Auto-generated method stub
	    	}
	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,int count) {
	    		// TODO Auto-generated method stub
	    	}	    	
	    }
	class checklistenetr implements OnCheckedChangeListener
	{
    	int i;
		public checklistenetr(int i) {
			// TODO Auto-generated constructor stub
		   this.i=i;
		}

		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			   RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
		        boolean isChecked = checkedRadioButton.isChecked();
		        if (isChecked)
		        {
		          switch (i) 
		          {
					case 1:
						underhoodrdgtxt= checkedRadioButton.getText().toString().trim();							
						break;
					case 2:
						surfaceprotrdgtxt= checkedRadioButton.getText().toString().trim();					
						break;
					case 3:
						yearagetxt= checkedRadioButton.getText().toString().trim();					
						break;
					case 4:
						domestictxt= checkedRadioButton.getText().toString().trim();					
						break;
					case 5:
						commercialtxt= checkedRadioButton.getText().toString().trim();					
						break;					
		          }
		        }
		}
	}
	class Spin_Selectedlistener implements OnItemSelectedListener
	{
		int i;
		public Spin_Selectedlistener(int i) {
			// TODO Auto-generated constructor stub
			this.i=i;
		}

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			if(i==1){
				apppltxt = applspin.getSelectedItem().toString();
				if(apppltxt.equals("Other")){
					((EditText)findViewById(R.id.edappl_otr)).setVisibility(cf.v1.VISIBLE);
					((EditText)findViewById(R.id.edappl_otr)).setEnabled(true);
					cf.setFocus(((EditText)findViewById(R.id.edappl_otr)));
					}
				else{((EditText)findViewById(R.id.edappl_otr)).setVisibility(cf.v1.GONE);((EditText)findViewById(R.id.edappl_otr)).setText("");}		
			}
			else if(i==2)
			{
				typefueltxt = typepffuelspin.getSelectedItem().toString();
				if(typefueltxt.equals("Other")){ ((EditText)findViewById(R.id.edtypeoffuel_otr)).setVisibility(cf.v1.VISIBLE);
				cf.setFocus(((EditText)findViewById(R.id.edtypeoffuel_otr)));}
				else{((EditText)findViewById(R.id.edtypeoffuel_otr)).setVisibility(cf.v1.GONE);((EditText)findViewById(R.id.edtypeoffuel_otr)).setText("");}
			}
			else if(i==3)
			{
				 agestr = agespin.getSelectedItem().toString();
				if(!agestr.equals("--Select--"))
				 {
						if(agestr.equals("Other"))
						{
							((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.VISIBLE);
							 yrbuiltoption[0].setChecked(false);
							 yrbuiltoption[1].setChecked(false);
			 				 yrbuiltoption[2].setChecked(true);
			 				
		                }
						else if(agestr.equals("Unknown"))
						{
							((EditText)findViewById(R.id.age_otr)).setText("");
							((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.GONE);
						
							
		 					yrbuiltoption[0].setChecked(false);
		 					yrbuiltoption[1].setChecked(true);
		 					yrbuiltoption[2].setChecked(false);
						}
						else
						{
							((EditText)findViewById(R.id.age_otr)).setText("");
							((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.GONE);
							 yrbuiltoption[1].setChecked(false);
		 					yrbuiltoption[0].setChecked(true);
		 					yrbuiltoption[2].setChecked(false);
						}
				 }
				 else
				 {
					 ((EditText)findViewById(R.id.age_otr)).setText("");
						((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.GONE);
				
					 if(yoc==1)
					 {
						 yrbuiltoption[2].setChecked(true);
					 }
					 else
					 {
						 yrbuiltoption[2].setChecked(false);
					 }
				 }
				
				
			}
			else if(i==4){
				burnerstxt = burners_spin.getSelectedItem().toString();
			}
			else if(i==5){
				voltstxt = volts_spin.getSelectedItem().toString();
				if(voltstxt.equals("Other")){
					((EditText)findViewById(R.id.edvolt_otr)).setVisibility(cf.v1.VISIBLE);
					 cf.setFocus(((EditText)findViewById(R.id.edvolt_otr)));
					}
				else
				{
					((EditText)findViewById(R.id.edvolt_otr)).setVisibility(cf.v1.GONE);
					((EditText)findViewById(R.id.edvolt_otr)).setText("");
				}		
	
			}
			else if(i==6){
				doorstxt = doors_spin.getSelectedItem().toString();
			}
		}
		
		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	private void selectresttbl()
	{
		try
		{
			c1 = cf.arr_db.rawQuery("SELECT * FROM "+ cf.Comm_RestSupplement + " WHERE fld_srid='"+ cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'", null);				
			rwscommesupp = c1.getCount();
			if(rwscommesupp>0)
			{
				c1.moveToFirst();
				appliance_na = c1.getString(c1.getColumnIndex("fld_appliance_na"));
		    }
		}
		catch(Exception e){
			System.out.println("EEe"+e.getMessage());
			
		}
		try
		{
			curappliances = cf.arr_db.rawQuery("SELECT * FROM " + cf.Comm_RestSuppAppl + " WHERE fld_srid='" + cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'", null);
		}
		catch(Exception e)
		{
			
		}
	}
	
	public void layoutfocus(RelativeLayout linearlayout)
	{
		linearlayout.setFocusable(false);
		linearlayout.setFocusableInTouchMode(true);
		linearlayout.requestFocus();
		linearlayout.setFocusableInTouchMode(false);
		
	}
	public void clicker(View v) {
		switch(v.getId())
		{
		case R.id.shwappl:
			if(!((CheckBox)findViewById(R.id.appliancenotapplicable)).isChecked()){
				hideotherlayouts();
				((CheckBox)findViewById(R.id.appliancenotapplicable)).setChecked(false);
				((LinearLayout)findViewById(R.id.applin)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdwappl)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.shwappl)).setVisibility(cf.v1.GONE);
			}
			break;
			case R.id.hdwappl:
				((ImageView)findViewById(R.id.shwappl)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdwappl)).setVisibility(cf.v1.GONE);
				((LinearLayout)findViewById(R.id.applin)).setVisibility(cf.v1.GONE);
				break;
		case R.id.hme:
			cf.gohome();
			break;
	
		case R.id.mouseoverid:
			cf.ShowToast("Complete for Replacement Cost Valuation only.",0);
			break;
	
		case R.id.loadcomments:
			/***Call for the comments***/
			cf.findinspectionname(cf.identityval);
			 int len=((EditText)findViewById(R.id.restsuppcomment)).getText().toString().length();
			 if(load_comment)
				{
					load_comment=false;
					int loc[] = new int[2];
					v.getLocationOnScreen(loc);
					Intent in1 = cf.loadComments("Restaurant Supplement Comments",loc);
					in1.putExtra("insp_name", cf.inspname);
					in1.putExtra("insp_ques", "Restaurant Supplement");
					in1.putExtra("length", len);
					in1.putExtra("max_length", 500);
					startActivityForResult(in1, cf.loadcomment_code);
				}		
			/***Call for the comments ends***/			
			break;
	
		
		case R.id.save:
			selectresttbl();
			try{
			if(curappliances.getCount()==0 && ((CheckBox)findViewById(R.id.appliancenotapplicable)).isChecked()==false)
			{
				if(curappliances.getCount()==0)
				{
					cf.ShowToast("Atleast one Appliance need to be added and saved.", 0);
				}
				else
				{
					cf.ShowToast("Please save Appliance.", 0);
				}						
			}			
			/*else if(((EditText)findViewById(R.id.restsuppcomment)).getText().toString().trim().equals(""))
			{
				cf.ShowToast("Please enter the Restaurant Supplement comments.", 0);	
				cf.setFocus(((EditText)findViewById(R.id.restsuppcomment)));
			}*/
			else
			{
				insertrest();
			}
			}catch(Exception e){}
		break;
		
		case R.id.saveapp:
			appl_content_val="";
			boolean b[] =  new boolean[8];
			b[1]=((typefueltxt.equals("Other")) &&  ((EditText)findViewById(R.id.edtypeoffuel_otr)).getText().toString().trim().equals("")) ? false:true;
			b[3]=((apppltxt.equals("Other")) &&  ((EditText)findViewById(R.id.edappl_otr)).getText().toString().trim().equals("")) ? false:true;
			b[4]=((voltstxt.equals("Other")) &&  ((EditText)findViewById(R.id.edvolt_otr)).getText().toString().trim().equals("")) ? false:true;
			
			String applianceothrtxt = ((EditText)findViewById(R.id.edappl_otr)).getText().toString().toLowerCase();
			if(!apppltxt.equals("--Select--") && b[3])
			{
					if(!typefueltxt.equals("--Select--") && b[1])
					{
						if(!((EditText)findViewById(R.id.ednumbers)).getText().toString().trim().equals(""))
						{
						 if((!burnerstxt.equals("--Select--") && b[3]) || (burnerstxt.equals("Refrigerator")))
						 {
							 if(!voltstxt.equals("--Select--")&& b[4])
							 {
							   if(!doorstxt.equals("--Select--"))
							   {
								if(!domestictxt.equals(""))
								{
									if(!commercialtxt.equals(""))
									{
										if(!underhoodrdgtxt.equals(""))
										{
											if(!surfaceprotrdgtxt.equals(""))
											{
												if(!agestr.equals("--Select--"))
												{
													if(agestr.equals("Other"))
													{
														if(!((EditText)findViewById(R.id.age_otr)).getText().toString().trim().equals(""))
														{
															if(((EditText)findViewById(R.id.age_otr)).getText().length()<4)
															{
																cf.ShowToast("Please enter the year in four digits.", 0);
																cf.setFocus(((EditText)findViewById(R.id.age_otr)));
															}
															else if(cf.yearValidation(((EditText)findViewById(R.id.age_otr)).getText().toString()).equals("true"))
															{
																cf.ShowToast("The entered year should not exceed current year.", 0);
																cf.setFocus(((EditText)findViewById(R.id.age_otr)));
															}
															else if(cf.yearValidation(((EditText)findViewById(R.id.age_otr)).getText().toString()).equals("zero"))
															{
																cf.ShowToast("Invalid Entry! Please enter the valid year.", 0);
																((EditText)findViewById(R.id.age_otr)).setText("");
																cf.setFocus(((EditText)findViewById(R.id.age_otr)));
															}
															else
															{
																yeartypevalidation();
															}
														}
														else
														{
															cf.ShowToast("Please enter the other text for Age.", 0);	
															cf.setFocus((EditText)findViewById(R.id.age_otr));
														}
													}
													else
													{
														yeartypevalidation();
													}													
												}
												else
												{
													cf.ShowToast("Please select the Age.",0);	
												}
											}
											else
											{
												cf.ShowToast("Please select the option for Surface Protection.",0);
											}
										}
										else
										{
											cf.ShowToast("Please select the option for Under Hood with Filters.",0);
										}

									}
									else
									{
										cf.ShowToast("Please select the option for Commercial Grade.",0);
									}
								}
								else
								{
									cf.ShowToast("Please select the option for Domestic Grade.",0);
								}
							 }
							 else
							 {
								cf.ShowToast("Please select the No. of Doors",0);
							  }
							}
							else
							{
								if(voltstxt.equals("--Select--"))
								{
									cf.ShowToast("Please select the No. of Volts",0);
								}
								else if(b[4]==false)
								{
									cf.ShowToast("Please enter the other text for No. of Volts",0);
								    cf.setFocus(((EditText)findViewById(R.id.edvolt_otr)));
								}
								
							}
						  }
						  else
						  {
							cf.ShowToast("Please select the No. of Burners",0);
						  }
						}
						else
						{
							 cf.ShowToast("Please enter the Number of Units.",0);
							 cf.setFocus(((EditText)findViewById(R.id.ednumbers)));
						}
				}
				else
				{
					if(typefueltxt.equals("--Select--")){cf.ShowToast("Please select the Type of Fuel.",0);}
					else if(b[1]==false){cf.ShowToast("Please enter the other text for Type of Fuel.",0);
					cf.setFocus(((EditText)findViewById(R.id.edtypeoffuel_otr)));
					}
				}
			}
			else
			{
				cf.ShowToast("Please select the Appliance.",0);
				if(apppltxt.equals("--Select--")){cf.ShowToast("Please select the Appliance.",0);}
				else if(b[3]==false){cf.ShowToast("Please enter the other text for Appliance.",0);
				cf.setFocus(((EditText)findViewById(R.id.edappl_otr)));}
			}
			break;
		case R.id.applcancel:
			clear();
			break;
		case R.id.appliancenotapplicable:
			if(((CheckBox)findViewById(R.id.appliancenotapplicable)).isChecked())
			{
				applnotappl="1";
				if(rwsrestsupp>0)
				{
					showalert();					
				}
				else
				{
					((TextView)findViewById(R.id.savetxtappliance)).setVisibility(cf.v1.INVISIBLE);
					((LinearLayout)findViewById(R.id.applin)).setVisibility(cf.v1.GONE);
					((ImageView)findViewById(R.id.shwappl)).setVisibility(cf.v1.VISIBLE);
 			    	((ImageView)findViewById(R.id.hdwappl)).setVisibility(cf.v1.GONE);
 			    	((ImageView)findViewById(R.id.shwappl)).setEnabled(false);					
				}
			}
			else
			{
				applnotappl="0";
				UncheckAppliances();				
				((LinearLayout)findViewById(R.id.applin)).setVisibility(cf.v1.VISIBLE);
				((TextView)findViewById(R.id.savetxtappliance)).setVisibility(cf.v1.INVISIBLE);
				 ((CheckBox)findViewById(R.id.appliancenotapplicable)).setChecked(false);
				((ImageView)findViewById(R.id.shwappl)).setVisibility(cf.v1.GONE);
			     ((ImageView)findViewById(R.id.hdwappl)).setVisibility(cf.v1.VISIBLE);
			     ((ImageView)findViewById(R.id.shwappl)).setEnabled(true);
			}
			break;	
		case R.id.yrbuiltrdio1:		
			if(agespin.getSelectedItem().toString().equals("UnKnown") || agespin.getSelectedItem().toString().equals("Other"))
			{
				((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.GONE);				
				agespin.setSelection(0);
			}
			yoc=0;			
			yrbuiltoption[2].setChecked(false);
			yrbuiltoption[0].setChecked(true);
			yrbuiltoption[1].setChecked(false);
			
			break;
		case R.id.yrbuiltrdio2:			
			int spnrpos1 =ageadap.getPosition("Unknown");
			agespin.setSelection(spnrpos1);	
			yrbuiltoption[1].setChecked(true);
			yrbuiltoption[0].setChecked(false);
			yrbuiltoption[2].setChecked(false);			
			break;
		case R.id.yrbuiltrdio3:
			agespin.setSelection(0);
			yoc=1;
			yrbuiltoption[0].setChecked(false);
			yrbuiltoption[1].setChecked(false);			
			yrbuiltoption[2].setChecked(true);	
			break;
		}
	}
	
	protected void updatenotappl() {
		// TODO Auto-generated method stub		
		try
		{
		selectresttbl();
			if(c1.getCount()==0)
			{
				cf.arr_db.execSQL("INSERT INTO " + cf.Comm_RestSupplement + " (fld_srid,insp_typeid,fld_appliance_na)"
						+ "VALUES ('"+cf.selectedhomeid+"','"+cf.identityval+"','"+applnotappl+"')");
			}
			else
			{
				cf.arr_db.execSQL("UPDATE "	+ cf.Comm_RestSupplement	+ " SET fld_appliance_na='"+applnotappl+"' WHERE fld_srid ='"+ cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'");	
			}			
		}
		catch(Exception e)
		{
			System.out.println("sdfdfds"+e.getMessage());
		}
	}
	
	protected void UncheckAppliances() {
		// TODO Auto-generated method stub
		clear();
		subpnlshw.setVisibility(cf.v1.GONE);rwsrestsupp=0;
		((TableLayout)findViewById(R.id.tableLayoutNote)).setVisibility(cf.v1.GONE);		
	}
	private void yeartypevalidation() {
		// TODO Auto-generated method stub
		yearagetxt=cf.getslected_radio(yrbuiltoption);
		if(!"".equals(yearagetxt))
		{
			insertappl();
			Rest_Supp_Appl_SetValue();
		}
		else
		{
			cf.ShowToast("Please select the option for Age.",0);
		}
	}
	
	private void insertrest() {
		// TODO Auto-generated method stub
		
		if(((CheckBox)findViewById(R.id.appliancenotapplicable)).isChecked())
		 {
			applnotappl  ="1";
			 try
			 {
				 cf.arr_db.execSQL("Delete from "+cf.Comm_RestSuppAppl+" Where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
			 }
			 catch(Exception e)
			 {
					System.out.println("applnotappl="+e.getMessage());		
			 }
		 }
        else
        {
        	applnotappl ="0";
        }
		updatenotappl();
		Cursor c3 = cf.arr_db.rawQuery("SELECT * FROM " + cf.Wind_Commercial_Comm + " WHERE fld_srid='" + cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'", null);				
		if(c3.getCount()==0)
		{
			 cf.arr_db.execSQL("INSERT INTO "
						+ cf.Wind_Commercial_Comm
						+ " (fld_srid,insp_typeid,fld_commercialareascomments,fld_auxillarycomments,fld_restsuppcomments)"
						+ "VALUES ('" + cf.selectedhomeid + "','"+cf.identityval+"','','','"+cf.encode(((EditText)findViewById(R.id.restsuppcomment)).getText().toString().trim())+"')");
		}
		else
		{
			cf.arr_db.execSQL("UPDATE "
					+ cf.Wind_Commercial_Comm
					+ " SET fld_restsuppcomments='"+cf.encode(((EditText)findViewById(R.id.restsuppcomment)).getText().toString().trim())+"'"
					+ " WHERE fld_srid ='"
					+ cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'");
		}
		if(cf.identityval==33 || cf.identityval==34)
		{
			 cf.ShowToast("Restaurant Supplement saved successfully", 0);Rest_Supp_HFM_SetValue();
			 cf.gotoclass(cf.identityval, WindAddendumcomm.class);
		}
		else
		{
			 cf.ShowToast("Restaurant Supplement saved successfully", 0);
			 cf.goclass(416);		 
		}		
	}
	private void clear() {
		// TODO Auto-generated method stub
		applspin.setSelection(0);
	//	applspin.setEnabled(true);
		typepffuelspin.setSelection(0);
		((EditText)findViewById(R.id.edtypeoffuel_otr)).setVisibility(cf.v1.GONE);
		((EditText)findViewById(R.id.edtypeoffuel_otr)).setText("");
		((EditText)findViewById(R.id.edvolt_otr)).setVisibility(cf.v1.GONE);
		((EditText)findViewById(R.id.edvolt_otr)).setText("");
		((EditText)findViewById(R.id.ednumbers)).setText("");	
		domesticchk=true;commchk=true;underhoodchk=true;surfprot=true;agechk=true;
		try{if(domesticchk){domesticrdgp.clearCheck();}}catch(Exception e){}
		try{if(commchk){commercialrdgrp.clearCheck();}}catch(Exception e){}
		try{if(underhoodchk){underhoodrdg.clearCheck();}}catch(Exception e){}
		try{if(surfprot){surfaceprotrdgp.clearCheck();}}catch(Exception e){}
		
		agespin.setSelection(0);
		yrbuiltoption[0].setChecked(false);
		yrbuiltoption[1].setChecked(false);
		yrbuiltoption[2].setChecked(false);
		burners_spin.setSelection(0);
		volts_spin.setSelection(0);
		doors_spin.setSelection(0);
		((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.GONE);
		((EditText)findViewById(R.id.age_otr)).setText("");
		((Button)findViewById(R.id.saveapp)).setText("Save/Add More Appliance");
		yoc=0;
	}
	private void insertappl() {
		// TODO Auto-generated method stub	
		if(typefueltxt.trim().equals("")){typefuelotherxt="";}else{
			typefuelotherxt=((EditText)findViewById(R.id.edtypeoffuel_otr)).getText().toString().trim();
		}
		
		if(agestr.equals("Other") || agestr.equals("Unknown")){ ageothertxt = ((EditText)findViewById(R.id.age_otr)).getText().toString().trim();}
		else{ageothertxt = ""; }
		
		if(voltstxt.trim().equals("")){voltstxtotherxt="";}else{
			voltstxtotherxt=((EditText)findViewById(R.id.edvolt_otr)).getText().toString().trim();
		}
		
		appl_content_val +=typepffuelspin.getSelectedItem().toString()+"&#39;"+cf.encode(typefuelotherxt)+"&#94;"+
				((EditText)findViewById(R.id.ednumbers)).getText().toString().trim()+"&#94;"+burnerstxt+"&#94;"+voltstxt+"&#39;"+cf.encode(voltstxtotherxt)+"&#94;"+doorstxt+"&#94;"+
				domestictxt+"&#94;"+commercialtxt+"&#94;"+underhoodrdgtxt+"&#94;"+surfaceprotrdgtxt+"&#94;"+
				agestr+"&#39;"+ageothertxt+"&#39;"+yearagetxt;
		
		try
		 {
			if(((Button)findViewById(R.id.saveapp)).getText().toString().equals("Update")){			
				 cf.arr_db.execSQL("UPDATE "
							+ cf.Comm_RestSuppAppl
							+ " SET fld_appliancetype='"+applspin.getSelectedItem().toString()+"',fld_appldata='"+appl_content_val+"',fld_applianceother='"+cf.encode(((EditText)findViewById(R.id.edappl_otr)).getText().toString().trim())+"' " +
									"WHERE fld_srid ='" + cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"' and ApplID='"+applid+"'");
				((Button)findViewById(R.id.saveapp)).setText("Save/Add More Appliance");
			}
			else
			{
				Cursor c1=cf.SelectTablefunction(cf.Comm_RestSuppAppl, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
				cf.arr_db.execSQL("INSERT INTO "
							+ cf.Comm_RestSuppAppl
							+ " (fld_srid,insp_typeid,fld_appliancetype,fld_applianceother,fld_appldata)"
							+ "VALUES ('" + cf.selectedhomeid + "','"+cf.identityval+"','"+ applspin.getSelectedItem().toString() + "','"+cf.encode(((EditText)findViewById(R.id.edappl_otr)).getText().toString().trim())+"','"+appl_content_val+"')");
			}
			cf.ShowToast("Appliance saved successfully.",0);
			show_rest_supp_Value();
				clear(); 
		}
		 catch (Exception E)
		 {
			 System.out.println("erro excel "+E.getMessage());
			String strerrorlog="Updating Questions Restaurant";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		 }
	}
	private void hideotherlayouts() {
		// TODO Auto-generated method stub
		((TableLayout)findViewById(R.id.applin)).setVisibility(cf.v1.GONE);		
	}
	private void showalert() {
		// TODO Auto-generated method stub	
		AlertDialog.Builder bl = new Builder(WindMitCommRestsupp.this);
		bl.setTitle("Confirmation");
		bl.setMessage(Html.fromHtml("Do you want to clear the Appliance data?"));
		bl.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) 
				{
					UncheckAppliances();
					((CheckBox)findViewById(R.id.appliancenotapplicable)).setChecked(true);
					((LinearLayout)findViewById(R.id.applin)).setVisibility(cf.v1.GONE);
					((TextView)findViewById(R.id.savetxtappliance)).setVisibility(cf.v1.INVISIBLE);
					((ImageView)findViewById(R.id.shwappl)).setVisibility(cf.v1.VISIBLE);
	    			((ImageView)findViewById(R.id.hdwappl)).setVisibility(cf.v1.GONE);
	    			((ImageView)findViewById(R.id.shwappl)).setEnabled(false);
					

				}
		});
		bl.setNegativeButton("No",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) 
					{
						 ((CheckBox)findViewById(R.id.appliancenotapplicable)).setChecked(false);	
						 ((ImageView)findViewById(R.id.shwappl)).setVisibility(cf.v1.GONE);
			    		 ((ImageView)findViewById(R.id.hdwappl)).setVisibility(cf.v1.VISIBLE);
					}
        });
		AlertDialog al=bl.create();
		al.setIcon(R.drawable.alertmsg);
		al.setCancelable(false);
		al.show();		
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
 		// replaces the default 'Back' button action
 		if (keyCode == KeyEvent.KEYCODE_BACK) {
 			if(cf.identityval==33 || cf.identityval==34)
 			{
 				cf.gotoclass(cf.identityval, WindMitCommAuxbuildings.class);
 			}
 			else
 			{
 				cf.goclass(45);
 			} 			
 			return true;
 		}
 		
 		return super.onKeyDown(keyCode, event);
 	}
 /**on activity result for the load comments**/
	 @Override
	 		protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	 		try
	 		{
	 			if(requestCode==cf.loadcomment_code)
	 			{
	 				load_comment=true;
	 				if(resultCode==RESULT_OK)
	 				{
	 					String restsuppcomments = ((EditText)findViewById(R.id.restsuppcomment)).getText().toString();
	 					((EditText)findViewById(R.id.restsuppcomment)).setText(restsuppcomments +" "+data.getExtras().getString("Comments"));	 				
	 				}
	 			}
	 		}
	 		catch (Exception e) {
	 			// TODO: handle exception
	 			System.out.println("the erro was "+e.getMessage());
	 		}
	 	}/**on activity result for the load comments ends**/
}
