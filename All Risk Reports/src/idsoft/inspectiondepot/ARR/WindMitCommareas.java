/*
************************************************************
* Project: ALL RISK REPORT
* Module : WindMitCommareas.java
* Creation History:
Created By : Nalini on 10/24/2012
* Modification History:
Last Modified By : Gowri on 04/03/2012
************************************************************ 
*/

package idsoft.inspectiondepot.ARR;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.Roof_commercial_information.RCT_edit_delete;
import idsoft.inspectiondepot.ARR.WindMitCommAuxbuildings.Touch_Listener;
import idsoft.inspectiondepot.ARR.WindMitCommAuxbuildings.textwatcher;
import idsoft.inspectiondepot.ARR.WindMitCommRestsupp.RestSupp_EditClick;
import idsoft.inspectiondepot.ARR.WindMitCommRestsupp.Spin_Selectedlistener;
import idsoft.inspectiondepot.ARR.WindMitCommRestsupp.checklistenetr;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class WindMitCommareas extends Activity{
	CommonFunctions cf;int k=0,elevid;
	  public static int s=0;
	private List<String> items1= new ArrayList<String>();
	String headername="",metalrailingstext="",poolareasother="",wallschk_value="",floorchk_val="",ceilingschk_val="",doorschk_val="",windowschk_val="",elevoptvalue="",
			stairconschk_value="",floortypechk_val="",railingschk_val="",floorinspectedrdgtxt="",starirwellschk_val="",lobbynotappl="0",stairsnotappl="0",
			servingfloordgtxt="",areastxt1="",areastxt2="",areastxt3="",areastxt4="",poolareasvalue="",lobbytxt="",stairwellstxt="",lobby_na="",elevatorsnotappl="0",areasnotappl="0",
			lobby_val="",stairwells_na="",stairwells_val="",elevators_na="",elevators_val="",poolareas_val="",communalareascomm="",poolareas_na="";
	RadioGroup metalrailingrdgval,floorinspectedrdgval,servingoffloorsedrdgval;
	int elevrows,rwselev,lobbychkval=0,stairschkval=0,elevatorschk=0,areaschkval=0,rwselevator;
	TableLayout dynviewelev;LinearLayout subpnlshw;
	/* String floorarr [] = {"First","Second","Third","Fourth","Fifth","Sixth","Seventh","Eighth","Nineth","Tenth",
			 	"Eleventh","Twelveth","Thirteenth","Fourteenth","Fifteenth",
				"Sixteenth","Seventeenth","Eighteenth","Nineteenth","Twentieth","Twenty First","Twenty Second","Twenty Third","Twenty Fourth","Twenty fifth",
				"Twenty Sixth","Twenty Seventh","Twenty Eighth","Twenty Nineth","Thirtieth",
				"Thirty First","Thirty Second","Thirty Third","Thirty Fourth","Thirty Fifth","Thirty Sixth","Thirty Seventh","Thirty Eighth",
				"Thirty Nineth","Fortieth",
				"Forty First","Forty Second","Forty Third","Forty Fourth","Forty Fifth","Forty Sixth","Forty Seventh","Forty eight","Forty Nine","Fiftieth",
				"Fifty First","Fifty Second","Fifty Third","Fifty Fourth","Fifty Fifth","Fifty Sixth","Fifty Seventh","Fifty eight","Fifty Nine","Sixtieth",
				"Sixty First","Sixty Second","Sixty Third","Sixty Fourth","Sixty Fifth","Sixty Sixth","Sixty Seventh","Sixty eight","Sixty Nine","Seventieth",
				"Seventy First","Seventy Second","Seventy Third","Seventy Fourth","Seventy Fifth"};*/
	Spinner servingfloor;
	String elevno="";
	Cursor c1;
	int type;
	ImageView edit,delete,duplicate;
	public int pick_img_code=24;
	protected static final int DATE_DIALOG_ID = 0;
	ArrayAdapter servingflooradap,spnareas1adap,spnareas2adap,spnareas3adap,spnareas4adap;
	String[] arrelevno,arrelevmodel,arrelevmanufacturer,arrelevid,arrelevservingfloors,arrelevimg,arrelevdate;
	int areaval1=0,areaval2=0,areaval3=0,areaval4=0;
	String spnnumbers[] =  {"--Select--","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65","66","67","68","69","70","71","72","73","74","75","76","77","78","79","80","81","82","83","84","85","86","87","88","89","90","91","92","93","94","95","96","97","98","99","100"};
	Cursor c2,curcommareaelev,curcomments;
	TextView tv;
	
	boolean load_comment=true;
	public CheckBox areaschk[] = new CheckBox[4];
	
	public Spinner areasspinner[] = new Spinner[4];
	public int[] areasspinnerid = {R.id.areas_chk1,R.id.areas_chk2,R.id.areas_chk3,R.id.areas_chk4};
	
	public CheckBox wallschk[] = new CheckBox[8];
	public int[] wallschkid = {R.id.walls_chk1,R.id.walls_chk2,R.id.walls_chk3,R.id.walls_chk4,R.id.walls_chk5,R.id.walls_chk6,R.id.walls_chk7,R.id.walls_chk8};
	
	public CheckBox floorchk[] = new CheckBox[19];
	public int[] floorchkid = {R.id.floor_chk1,R.id.floor_chk2,R.id.floor_chk3,R.id.floor_chk4,R.id.floor_chk5,R.id.floor_chk6,R.id.floor_chk7,R.id.floor_chk8,R.id.floor_chk9,R.id.floor_chk10,
			R.id.floor_chk11,R.id.floor_chk12,R.id.floor_chk13,R.id.floor_chk14,R.id.floor_chk15,R.id.floor_chk16,R.id.floor_chk17,R.id.floor_chk18,R.id.floor_chk19};
	
	public CheckBox ceilingschk[] = new CheckBox[9];
	public int[] ceilingschkid = {R.id.ceil_chk1,R.id.ceil_chk2,R.id.ceil_chk3,R.id.ceil_chk4,R.id.ceil_chk5,R.id.ceil_chk6,R.id.ceil_chk7,R.id.ceil_chk8,R.id.ceil_chk9};
	
	public CheckBox doorschk[] = new CheckBox[20];
	public int[] doorschkid = {R.id.doors_chk1,R.id.doors_chk2,R.id.doors_chk3,R.id.doors_chk4,R.id.doors_chk5,R.id.doors_chk6,R.id.doors_chk7,R.id.doors_chk8,R.id.doors_chk9,R.id.doors_chk10,
			R.id.doors_chk11,R.id.doors_chk12,R.id.doors_chk13,R.id.doors_chk14,R.id.doors_chk15,R.id.doors_chk16,R.id.doors_chk17,R.id.doors_chk18,R.id.doors_chk19,R.id.doors_chk20};

	public CheckBox windowschk[] = new CheckBox[17];
	public int[] windowschkid = {R.id.windows_chk1,R.id.windows_chk2,R.id.windows_chk3,R.id.windows_chk4,R.id.windows_chk5,R.id.windows_chk6,R.id.windows_chk7,R.id.windows_chk8,R.id.windows_chk9,R.id.windows_chk10,
			R.id.windows_chk11,R.id.windows_chk12,R.id.windows_chk13,R.id.windows_chk14,R.id.windows_chk15,R.id.windows_chk16,R.id.windows_chk17};
	
	public CheckBox stairschk[] = new CheckBox[8];
	public int[] stairschkid = {R.id.stair_chk1,R.id.stair_chk2,R.id.stair_chk3,R.id.stair_chk4,R.id.stair_chk5,R.id.stair_chk6,R.id.stair_chk7,R.id.stair_chk8};
	
	public CheckBox floortypechk[] = new CheckBox[5];
	public int[] floortypechkid = {R.id.floortyp_chk1,R.id.floortyp_chk2,R.id.floortyp_chk3,R.id.floortyp_chk4,R.id.floortyp_chk5};
	
	public CheckBox railingchk[] = new CheckBox[5];
	public int[] railingchkid = {R.id.railing_chk1,R.id.railing_chk2,R.id.railing_chk3,R.id.railing_chk4,R.id.railing_chk5};
	
	public CheckBox stairwellschk[] = new CheckBox[2];
	public int[] stairwellschkid = {R.id.stairwells_chk1,R.id.stairwells_chk2};
	boolean metalchk=false,floorvalchk=false,servingfloorchk=false;
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cf=new CommonFunctions(this);	 
        Bundle extras = getIntent().getExtras();
  		if (extras != null) {
  			cf.getExtras(extras);				
  	 	}
  		  setContentView(R.layout.windcommunalareas);System.out.println("inside comm");
          cf.getDeviceDimensions();
          if(cf.identityval==32){ cf.typeidentity=309;}
          else if(cf.identityval==33){cf.typeidentity=310;}
          else if(cf.identityval==34){cf.typeidentity=310;}
          
          LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
          
          if(cf.identityval==32)
          {
             hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","Commercial Type I","Communal Areas",3,1,cf));
          }
          else if(cf.identityval==33)
          {
        	  hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","Commercial Type II","Communal Areas",3,1,cf));
          }
          else if(cf.identityval==34)
          {
        	  hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","Commercial Type III","Communal Areas",3,1,cf));
          }
          LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
       	  main_layout.setMinimumWidth(cf.wd);System.out.println("heamain widtr");
       	  main_layout.addView(new MyOnclickListener(getApplicationContext(), 3, 0,0,cf));
        System.out.println("header");
          cf.type_submenulayout =(LinearLayout) findViewById(R.id.typesubmenu); //** WINDMIT TYPE SUBMENU **//
          cf.type_submenulayout.addView(new MyOnclickListener(this, 300, 1,0, cf));System.out.println("heafter type  sub");
          cf.windmittypechk(cf.identityval,((RadioButton)findViewById(R.id.b1802_chk)),((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((TextView)findViewById(R.id.commtxt)),((ImageView)findViewById(R.id.editbno))); // To check the checkbox of choosen inspection type
          System.out.println("hewidr");
          cf.uncheckcommercial(((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((RadioButton)findViewById(R.id.b1802_chk))); 
          System.out.println("huncheckcommr");
          cf.submenu_layout = (LinearLayout) findViewById(R.id.submenu);//** WINDMIT TYPE SUBMENU **//
          System.out.println("cf.typeidentity"+cf.typeidentity);
          cf.submenu_layout .addView(new MyOnclickListener(this,cf.identityval,1,cf.typeidentity, cf));
          
          
          cf.innersubmenu_layout = (LinearLayout) findViewById(R.id.innersubmenu);//** WINDMIT TYPE SUBMENU **//
          cf.innersubmenu_layout.addView(new MyOnclickListener(this,cf.identityval,1,143, cf));
          
          System.out.println("hesubmneu");
          
         // cf.innersubmenu_layout = (LinearLayout) findViewById(R.id.innersubmenu);//** WINDMIT TYPE SUBMENU **//
         // cf.innersubmenu_layout.addView(new MyOnclickListener(this,cf.identityval,1,123, cf));
          
          
          cf.mainlayout = (LinearLayout)findViewById(R.id.bc_table);System.out.println("hebctable");
          cf.save = (Button)findViewById(R.id.save);System.out.println("hesaveer");
          cf.tblrw = (TableRow)findViewById(R.id.row2);System.out.println("headerrow2");
          cf.tblrw.setMinimumHeight(cf.ht);			
          Declarations();
          cf.CreateARRTable(39); 
          cf.CreateARRTable(36);
          cf.CreateARRTable(311);
          cf.CreateARRTable(436);
          cf.CreateARRTable(450);

          cf.getCalender();
          Comm_Areas_SetValue();
          Show_elev_value(); //**  TO SHOW ELEVATORS IN DYNAMIC VIEW **//
    }
	private void chkvalues() {
		try
		{
			c1 = cf.arr_db.rawQuery("SELECT * FROM " + cf.Communal_Areas + " WHERE fld_srid='"+ cf.selectedhomeid + "'  and insp_typeid='"+cf.identityval+"'", null);				
			if(c1.getCount()>0)
			{
				c1.moveToFirst();
				lobby_na = c1.getString(c1.getColumnIndex("fld_lobby_NA"));
				lobby_val = cf.decode(c1.getString(c1.getColumnIndex("fld_lobby")));
				stairwells_na = c1.getString(c1.getColumnIndex("fld_stairwell_NA"));
				stairwells_val = cf.decode(c1.getString(c1.getColumnIndex("fld_stairwells")));
				elevators_na = c1.getString(c1.getColumnIndex("fld_elevators_NA"));
				elevators_val = cf.decode(c1.getString(c1.getColumnIndex("fld_elevatorsoption")));
				poolareas_na = c1.getString(c1.getColumnIndex("fld_poolareas_NA"));		
				poolareasother= cf.decode(c1.getString(c1.getColumnIndex("fld_poolareasother")));
				poolareas_val = cf.decode(c1.getString(c1.getColumnIndex("fld_poolareas")));
			}
		}catch(Exception e)
		{
			System.out.println("catch 1"+e.getMessage());
		}
		try
		{
			c2 = cf.arr_db.rawQuery("SELECT * FROM " + cf.Wind_Commercial_Comm + " WHERE fld_srid='" + cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'", null);				
			
			if(c2.getCount()>0)
			{
				c2.moveToFirst();				
				communalareascomm  = cf.decode(c2.getString(c2.getColumnIndex("fld_commercialareascomments")));		
			
			}
		}
		catch(Exception e)
		{
			System.out.println("catch 2"+e.getMessage());
		}
			
		try
		{
			curcommareaelev = cf.arr_db.rawQuery("SELECT * FROM " + cf.Communal_AreasElevation + " WHERE fld_srid='" + cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'", null);
			//rwselevator = curcommareaelev.getCount();
		}
		catch (Exception e) {
			// TODO: handle exception
			System.out.println("selecte"+e.getMessage());
		}
	}
	private void Comm_Areas_SetValue() {
		// TODO Auto-generated method stub
			chkvalues();
			if(c1.getCount()>0)
			{
				if(lobby_na.equals("0") || lobby_na.equals(""))
				{
					if(!lobby_val.equals(""))
					{
						String lobbytxt_splt[] = lobby_val.split("&#39;");
						cf.setvaluechk1(lobbytxt_splt[0],wallschk,((EditText)findViewById(R.id.lobbywall_other)));
						cf.setvaluechk1(lobbytxt_splt[1],floorchk,((EditText)findViewById(R.id.edfloor_other)));
						cf.setvaluechk1(lobbytxt_splt[2],ceilingschk,((EditText)findViewById(R.id.edceilings_other)));
						cf.setvaluechk1(lobbytxt_splt[3],doorschk,((EditText)findViewById(R.id.eddoors_other)));
						cf.setvaluechk1(lobbytxt_splt[4],windowschk,((EditText)findViewById(R.id.edwindows_other)));
						((TextView)findViewById(R.id.savetxtlobby)).setVisibility(cf.v1.VISIBLE);
						((LinearLayout)findViewById(R.id.lobbylin)).setVisibility(cf.v1.VISIBLE);	

						((ImageView)findViewById(R.id.shwlobby)).setEnabled(true);
						((ImageView)findViewById(R.id.shwlobby)).setVisibility(cf.v1.GONE);
						((ImageView)findViewById(R.id.hdwlobby)).setVisibility(cf.v1.VISIBLE);
					}
					else
					{
						((TextView)findViewById(R.id.savetxtlobby)).setVisibility(cf.v1.INVISIBLE);
						((LinearLayout)findViewById(R.id.lobbylin)).setVisibility(cf.v1.GONE);	
					}
				}
				else
				{
					((CheckBox)findViewById(R.id.lobbynotapplicable)).setChecked(true);
					((LinearLayout)findViewById(R.id.lobbylin)).setVisibility(cf.v1.GONE);
					((TextView)findViewById(R.id.savetxtlobby)).setVisibility(cf.v1.VISIBLE);
					((ImageView)findViewById(R.id.shwlobby)).setEnabled(false);
					((ImageView)findViewById(R.id.shwlobby)).setVisibility(cf.v1.VISIBLE);
					((ImageView)findViewById(R.id.hdwlobby)).setVisibility(cf.v1.GONE);
					
				}
				
				if(stairwells_na.equals("0") || stairwells_na.equals(""))
				{
					 if(!stairwells_val.equals(""))
					{
						String stairwellstxt_splt[] = stairwells_val.split("&#39;");
					     cf.setvaluechk1(stairwellstxt_splt[0],stairschk,((EditText)findViewById(R.id.edstaircons_other)));
					     cf.setvaluechk1(stairwellstxt_splt[1],floortypechk,((EditText)findViewById(R.id.edfloortype_other)));
					     
					     cf.setvaluechk1(stairwellstxt_splt[4],stairwellschk,((EditText)findViewById(R.id.edrailing_other)));
					     if(stairwellstxt_splt[2].equals("Yes")){
								((LinearLayout)findViewById(R.id.ralilinglayout)).setVisibility(cf.v1.VISIBLE);
								cf.setvaluechk1(stairwellstxt_splt[3],railingchk,((EditText)findViewById(R.id.edrailing_other)));
						}else{((LinearLayout)findViewById(R.id.ralilinglayout)).setVisibility(cf.v1.GONE);}
					     
					     ((RadioButton) metalrailingrdgval.findViewWithTag(stairwellstxt_splt[2])).setChecked(true);
					     ((RadioButton) floorinspectedrdgval.findViewWithTag(stairwellstxt_splt[5])).setChecked(true);
					     ((TextView)findViewById(R.id.savetxtstair)).setVisibility(cf.v1.VISIBLE);
					     ((LinearLayout)findViewById(R.id.stairlin)).setVisibility(cf.v1.VISIBLE);
					     
					     ((ImageView)findViewById(R.id.shwstairs)).setEnabled(true);
						 ((ImageView)findViewById(R.id.shwstairs)).setVisibility(cf.v1.GONE);
						 ((ImageView)findViewById(R.id.hdwstairs)).setVisibility(cf.v1.VISIBLE);
					}
					else
					{
						((TextView)findViewById(R.id.savetxtstair)).setVisibility(cf.v1.INVISIBLE);
						((LinearLayout)findViewById(R.id.stairlin)).setVisibility(cf.v1.GONE);
					}
				}
				else
				{
					((CheckBox)findViewById(R.id.stairsnotapplicable)).setChecked(true);
					((LinearLayout)findViewById(R.id.stairlin)).setVisibility(cf.v1.GONE);
					((TextView)findViewById(R.id.savetxtstair)).setVisibility(cf.v1.VISIBLE);					
					((ImageView)findViewById(R.id.shwlobby)).setEnabled(false);
					((ImageView)findViewById(R.id.shwlobby)).setVisibility(cf.v1.VISIBLE);
					((ImageView)findViewById(R.id.hdwlobby)).setVisibility(cf.v1.GONE);
				}
				System.out.println("elevators_na"+elevators_na);
				if(elevators_na.equals("0") || elevators_na.equals(""))
				{
					if(!elevators_val.equals(""))
					{
						((EditText)findViewById(R.id.elevcomments)).setText(elevators_val);	
						((TextView)findViewById(R.id.savetxtelevators)).setVisibility(cf.v1.VISIBLE);
						((LinearLayout)findViewById(R.id.elevatorslin)).setVisibility(cf.v1.VISIBLE);
						((ImageView)findViewById(R.id.shwelev)).setEnabled(true);
						((ImageView)findViewById(R.id.shwelev)).setVisibility(cf.v1.GONE);
						((ImageView)findViewById(R.id.hdwelev)).setVisibility(cf.v1.VISIBLE);
					}
					else
					{
						((TextView)findViewById(R.id.savetxtelevators)).setVisibility(cf.v1.INVISIBLE);
						((LinearLayout)findViewById(R.id.elevatorslin)).setVisibility(cf.v1.GONE);
					}
				}
				else
				{
					((CheckBox)findViewById(R.id.elevnotapplicable)).setChecked(true);
					((LinearLayout)findViewById(R.id.elevatorslin)).setVisibility(cf.v1.GONE);
					((TextView)findViewById(R.id.savetxtelevators)).setVisibility(cf.v1.VISIBLE);
					((ImageView)findViewById(R.id.shwelev)).setEnabled(false);
					((ImageView)findViewById(R.id.shwelev)).setVisibility(cf.v1.VISIBLE);
					((ImageView)findViewById(R.id.hdwelev)).setVisibility(cf.v1.GONE);
					
				}
				
				if(poolareas_na.equals("0") || poolareas_na.equals(""))
				{
					if(!poolareas_val.equals(""))
					{
						 String poolareas_split[] =  poolareas_val.split("\\^"); 
						 String Pool_area_include[] = poolareas_split[0].split("~");
						 String Firness_area_include[] = poolareas_split[1].split("~");
						 String Common_area_include[] = poolareas_split[2].split("~"); 
						 String Other_area_include[] = poolareas_split[3].split("~");			
						 	 if(Pool_area_include[0].equals("1")){
								 
								 areaschk[0].setChecked(true);	areasspinner[0].setEnabled(true);			
								 int spinnerPosition1 = spnareas1adap.getPosition(Pool_area_include[1]);
								 areasspinner[0].setSelection(spinnerPosition1);			 	
							 }else{
								 areaschk[0].setChecked(false);	areasspinner[0].setEnabled(false);
								 areasspinner[0].setSelection(0);
							 }
							  if(Firness_area_include[0].equals("1")){
								 areaschk[1].setChecked(true);			areasspinner[1].setEnabled(true);		
								 int spinnerPosition2 = spnareas2adap.getPosition(Firness_area_include[1]);
								 areasspinner[1].setSelection(spinnerPosition2);			 	
							 }else{
								 areaschk[1].setChecked(false);areasspinner[1].setEnabled(false);		
								 areasspinner[1].setSelection(0);
							}
							if(Common_area_include[0].equals("1")){
								 areaschk[2].setChecked(true);			areasspinner[2].setEnabled(true);			
								 int spinnerPosition3 = spnareas3adap.getPosition(Common_area_include[1]);
								 areasspinner[2].setSelection(spinnerPosition3);			 	
							 }else{
								 areaschk[2].setChecked(false);areasspinner[2].setEnabled(false);		
								 areasspinner[2].setSelection(0);
							}			 
							if(Other_area_include[0].equals("1")){
								 areaschk[3].setChecked(true);		areasspinner[3].setEnabled(true);				
								 int spinnerPosition4 = spnareas4adap.getPosition(Other_area_include[1]);
								 areasspinner[3].setSelection(spinnerPosition4);
	
							 }else{
								 areaschk[3].setChecked(false);areasspinner[3].setEnabled(false);		
								 areasspinner[3].setSelection(0);
							}
							 if(!poolareasother.equals(""))
							 {
								 ((EditText)findViewById(R.id.edotherareas)).setEnabled(true);
								 ((EditText)findViewById(R.id.edotherareas)).setText(poolareasother);						 
							 }
							 else
							 {
								 ((EditText)findViewById(R.id.edotherareas)).setEnabled(false);
								 ((EditText)findViewById(R.id.edotherareas)).setText("");
							 }
							 ((LinearLayout)findViewById(R.id.areaslin)).setVisibility(cf.v1.VISIBLE);
							 ((TextView)findViewById(R.id.savetxtareas)).setVisibility(cf.v1.VISIBLE);
							((ImageView)findViewById(R.id.shwareas)).setEnabled(true);
							((ImageView)findViewById(R.id.shwareas)).setVisibility(cf.v1.GONE);
							((ImageView)findViewById(R.id.hdwareas)).setVisibility(cf.v1.VISIBLE);
					}
					else
					{
						((TextView)findViewById(R.id.savetxtareas)).setVisibility(cf.v1.INVISIBLE);
						((LinearLayout)findViewById(R.id.areaslin)).setVisibility(cf.v1.GONE);
					}
				}
				else
				{
					((CheckBox)findViewById(R.id.areasnotapplicable)).setChecked(true);
					((LinearLayout)findViewById(R.id.areaslin)).setVisibility(cf.v1.GONE);
					((TextView)findViewById(R.id.savetxtareas)).setVisibility(cf.v1.VISIBLE);
					((ImageView)findViewById(R.id.shwareas)).setEnabled(false);
					((ImageView)findViewById(R.id.shwareas)).setVisibility(cf.v1.VISIBLE);
					((ImageView)findViewById(R.id.hdwareas)).setVisibility(cf.v1.GONE);					
				}
				
				if(!communalareascomm.equals("")){
					//((TextView)findViewById(R.id.savetxt10)).setVisibility(cf.v1.VISIBLE);
					((EditText)findViewById(R.id.communalareascomm)).setText(communalareascomm);
				}
				else
				{
					//((TextView)findViewById(R.id.savetxt10)).setVisibility(cf.v1.GONE);
				}
		}
		else
		{
				hideotherlayouts();
		}
	}
	private void Declarations() {
		// TODO Auto-generated method stub
		cf.setupUI((ScrollView) findViewById(R.id.scr));
		Button save = (Button)findViewById(R.id.save);   
		
		for(int i=0;i<wallschk.length;i++)
		{
			wallschk[i] = (CheckBox)findViewById(wallschkid[i]);			
   	 	}
		for(int i=0;i<floorchk.length;i++)
		{
			 floorchk[i] = (CheckBox)findViewById(floorchkid[i]);
		 }
		 for(int i=0;i<ceilingschk.length;i++)
	   	 {
	   		ceilingschk[i] = (CheckBox)findViewById(ceilingschkid[i]);
	   	 }
		 for(int i=0;i<doorschk.length;i++)
	   	 {
	   		doorschk[i] = (CheckBox)findViewById(doorschkid[i]);
	   	 }
		 for(int i=0;i<windowschk.length;i++)
		 {
			windowschk[i] = (CheckBox)findViewById(windowschkid[i]);
		 }			 
		 for(int i=0;i<stairschk.length;i++)
		 {
		  	stairschk[i] = (CheckBox)findViewById(stairschkid[i]);
		 }
		 for(int i=0;i<floortypechk.length;i++)
		 {
		 	floortypechk[i] = (CheckBox)findViewById(floortypechkid[i]);
		 }
		 for(int i=0;i<railingchk.length;i++)
		 {
		 	railingchk[i] = (CheckBox)findViewById(railingchkid[i]);
		 }
		 for(int i=0;i<stairwellschk.length;i++)
		 {
		 	stairwellschk[i] = (CheckBox)findViewById(stairwellschkid[i]);
		 }
			 metalrailingrdgval = (RadioGroup)findViewById(R.id.metalrailing_rdgp);metalrailingrdgval.setOnCheckedChangeListener(new checklistenetr(1));
			 floorinspectedrdgval = (RadioGroup)findViewById(R.id.floorinspected_rdgp);
			 
			 areaschk[0] = (CheckBox)findViewById(R.id.areas_chk1);
			 areaschk[1] = (CheckBox)findViewById(R.id.areas_chk2);
			 areaschk[2] = (CheckBox)findViewById(R.id.areas_chk3);
			 areaschk[3] = (CheckBox)findViewById(R.id.areas_chk4);
			 
			 areasspinner[0] = (Spinner)findViewById(R.id.areas_spn1); areasspinner[0].setEnabled(false);
			 areasspinner[1] = (Spinner)findViewById(R.id.areas_spn2); areasspinner[1].setEnabled(false);
			 areasspinner[2] = (Spinner)findViewById(R.id.areas_spn3); areasspinner[2].setEnabled(false);
			 areasspinner[3] = (Spinner)findViewById(R.id.areas_spn4); areasspinner[3].setEnabled(false);			 
			
			 spnareas1adap = new ArrayAdapter(this,android.R.layout.simple_spinner_item,spnnumbers);
			 spnareas1adap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			 areasspinner[0].setAdapter(spnareas1adap);
			 areasspinner[0].setOnItemSelectedListener(new  Spin_Selectedlistener(1));	
			 
			 spnareas2adap = new ArrayAdapter(this,android.R.layout.simple_spinner_item,spnnumbers);
			 spnareas2adap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			 areasspinner[1].setAdapter(spnareas2adap);
			 areasspinner[1].setOnItemSelectedListener(new  Spin_Selectedlistener(2));
			 
			 spnareas3adap = new ArrayAdapter(this,android.R.layout.simple_spinner_item,spnnumbers);
			 spnareas3adap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			 areasspinner[2].setAdapter(spnareas3adap);
			 areasspinner[2].setOnItemSelectedListener(new  Spin_Selectedlistener(3));
			 
			 spnareas4adap = new ArrayAdapter(this,android.R.layout.simple_spinner_item,spnnumbers);
			 spnareas4adap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			 areasspinner[3].setAdapter(spnareas4adap);
			 areasspinner[3].setOnItemSelectedListener(new  Spin_Selectedlistener(4));
			 items1.add("--Select--");
			 for(int i=0;i<cf.floorarr.length;i++)
			 {
				 items1.add(cf.floorarr[i]+" Floor");
			 }
			 
			 servingfloor = (Spinner)findViewById(R.id.serving_spin); 
			 servingflooradap = new ArrayAdapter(this,android.R.layout.simple_spinner_item,items1);
			 servingflooradap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			 servingfloor.setAdapter(servingflooradap);
			 ((EditText)findViewById(R.id.edotherareas)).setEnabled(false);
			 
			 dynviewelev = (TableLayout)findViewById(R.id.dynamicelevation);
			 subpnlshw = (LinearLayout)findViewById(R.id.subpnlshow); 
			   
			 ((EditText)findViewById(R.id.elevcomments)).setOnTouchListener(new Touch_Listener(1));
		     ((EditText)findViewById(R.id.elevcomments)).addTextChangedListener(new textwatcher(1));
		    
		     ((EditText)findViewById(R.id.communalareascomm)).setOnTouchListener(new Touch_Listener(2));
		     ((EditText)findViewById(R.id.communalareascomm)).addTextChangedListener(new textwatcher(2));		   
		     ((EditText)findViewById(R.id.edelevatorno)).addTextChangedListener(new textwatcher(3));
		     
		     tv = (TextView)findViewById(R.id.Elev_TV_upload);
		     
		     tv.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						System.out.println("tv click");
						Intent in=new Intent(WindMitCommareas.this,ElevImagePicking.class);
						in.putExtra("elev_img", true);					    

					    String cv="";
					    try
					    {
					    	elevno=((EditText)findViewById(R.id.edelevatorno)).getText().toString();
					    	System.out.println("elevno="+elevno);
					    	
					    if(!elevno.equals(""))

					    {
					    	in.putExtra("elevno", elevno);
					    	if(!((Button)findViewById(R.id.elevsave)).getText().toString().equals("Update"))
					    	{
					    		Cursor c=cf.arr_db.rawQuery("Select * from "+cf.Elevatorimages+" Where Elev_masterid=(Select ELev_masterid from "+cf.Elev_master+
					    				" Where Elev_insp_id='"+cf.identityval+"' and Elev_srid='"+cf.selectedhomeid+"' and Elev_No='"+cf.encode(elevno)+"') order by Elev_Order", null);
					    		if(c.getCount()>0)
					    		{
					    			c.moveToFirst();
					    			System.out.println("mast="+c.getString(c.getColumnIndex("Elev_masterid")));
					    			in.putExtra("id",c.getString(c.getColumnIndex("Elev_masterid")));	
					    		}
					    	}
					    	else
					    	{
					    		in.putExtra("id", tv.getTag().toString());	
					    	}
					    	
					    	
						    
						    in.putExtra("Insp_id",cf.identityval);
						    in.putExtra("SRID",cf.selectedhomeid);
						    in.putExtra("oper", ((Button)findViewById(R.id.elevsave)).getText().toString());
						  //  in.putExtra("readonly",true);
						    startActivityForResult(in, pick_img_code);
					    }
					    else
					    {
					    	cf.ShowToast("Please select Elevator No.", 0);

					    }
					    }
					    catch (Exception e) {
							// TODO: handle exception
					    	System.out.println("Sdfdsf"+e.getMessage());
						}
					}
				});

	}
	class checklistenetr implements OnCheckedChangeListener
	{
    	int i;
		public checklistenetr(int i) {
			// TODO Auto-generated constructor stub
		   this.i=i;
		}

		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			   RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
		        boolean isChecked = checkedRadioButton.isChecked();
		        if (isChecked)
		        {
		          switch (i) 
		          {
					case 1:
						metalrailingstext= checkedRadioButton.getText().toString().trim();		
						if(metalrailingstext.equals("Yes")){
							((LinearLayout)findViewById(R.id.ralilinglayout)).setVisibility(cf.v1.VISIBLE);
						}else{((LinearLayout)findViewById(R.id.ralilinglayout)).setVisibility(cf.v1.GONE);}
						break;
		          }
		        }
		}
	}
	 class Touch_Listener implements OnTouchListener
	 {
	   	   public int type;
	   	   Touch_Listener(int type)
	   		{
	   			this.type=type;
	   		}
	    	    @Override
	    		public boolean onTouch(View v, MotionEvent event) {
	    			// TODO Auto-generated method stub
	    	    	((EditText) findViewById(v.getId())).setFocusableInTouchMode(true);
			    	((EditText) findViewById(v.getId())).requestFocus();
	    			return false;
	    	    }
	 }
	 class textwatcher implements TextWatcher
	 {
	       public int type;
	         textwatcher(int type)
	    	{
	    		this.type=type;
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {
	    		// TODO Auto-generated method stub
	    		if(this.type==1)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.elevcomm_tv_type1),500); 
	    		}
	    		else if(this.type==2)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.communalareas_tv_type1),500); 
	    		}	 
	    		else if(this.type==3)
	    		{
	    			 if(!s.toString().equals("")){
	    				 
    					Cursor c1 = cf.arr_db.rawQuery("SELECT * FROM " + cf.Communal_AreasElevation + " WHERE fld_srid='"+ cf.selectedhomeid + "'  and " +
    							"insp_typeid='"+cf.identityval+"' and fld_elevationno='"+((EditText)findViewById(R.id.edelevatorno)).getText().toString().trim()+"'", null);				
    					elevrows= c1.getCount();
    					if(elevrows!=0)
    					{
    						if(!((Button)findViewById(R.id.elevsave)).getText().toString().equals("Update"))
    						{
    							cf.ShowToast("Already Exist! Please enter a different Elevator No.", 0);
        						((EditText)findViewById(R.id.edelevatorno)).setText("");	
    						}
    						
    					}
	    				 
	    			     if(Character.toString(s.toString().charAt(0)).equals("0"))
		    			 {
		    				 cf.ShowToast("Invalid Entry! Please enter the valid Elevator No.", 0);
		    				 ((EditText)findViewById(R.id.edelevatorno)).setText("");
		    			 }
	    			 } 
	    		}	 
	    	}
	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,

	    			int after) {
	    		// TODO Auto-generated method stub
	    	}
	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,int count) {
	    		// TODO Auto-generated method stub
	    	}	    	
	    }
	class Spin_Selectedlistener implements OnItemSelectedListener
	{
		int i;
		public Spin_Selectedlistener(int i) {
			// TODO Auto-generated constructor stub
			this.i=i;
		}

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			if(i==1){ 		areastxt1 = areasspinner[0].getSelectedItem().toString();
			}else if(i==2){	areastxt2 = areasspinner[1].getSelectedItem().toString();
			}else if(i==3){	areastxt3 =areasspinner[2].getSelectedItem().toString();
			}else if(i==4){	areastxt4 = areasspinner[3].getSelectedItem().toString();
			}
			
		}
		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub			
		}		
	}
	public void layoutfocus(RelativeLayout linearlayout)
	{
		linearlayout.setFocusable(false);
		linearlayout.setFocusableInTouchMode(true);
		linearlayout.requestFocus();
		linearlayout.setFocusableInTouchMode(false);
		
	}
	public void clicker(View v) {
		switch(v.getId())
		{
		/*case R.id.img_clr:
			cf.ShowToast("Image cleared successfully.", 0);
			((EditText)findViewById(R.id.whi_ed)).setText("");
			((Button)findViewById(R.id.img_clr)).setVisibility(v.INVISIBLE);
			break;*/
		case R.id.mouseoverid1:
			cf.ShowToast("Complete for Replacement Cost Valuation only.",0);
			break;
		case R.id.mouseoverid2:
			cf.ShowToast("Complete for Replacement Cost Valuation only.",0);
			break;
		case R.id.mouseoverid3:
			cf.ShowToast("Complete for Replacement Cost Valuation only.",0);
			break;
		case R.id.whi_pck:
			
			Intent in=new Intent(WindMitCommareas.this,ElevImagePicking.class);
		    in.putExtra("elev_img", true);
		    
		    try
		    {
		    	elevno=((EditText)findViewById(R.id.edelevatorno)).getText().toString();
		    	System.out.println("test="+elevno+"value="+cf.identityval+"ElevImagePicking="+WindMitCommareas.s);
		   
		    if(!elevno.equals(""))
		    {
		    	if(((Button)findViewById(R.id.elevsave)).getText().toString().equals("Update"))
		    	{
		    		
		    		System.out.println("www"+WindMitCommareas.s);
		    		in.putExtra("id", String.valueOf(WindMitCommareas.s));
		    	}
		    	else
		    	{
		    		in.putExtra("id", "0");	
		    	}
		    	
	    		in.putExtra("elevno", elevno);
			    in.putExtra("Insp_id",cf.identityval);
			    in.putExtra("SRID",cf.selectedhomeid);
			    in.putExtra("oper", ((Button)findViewById(R.id.elevsave)).getText().toString());
			 //   in.putExtra("readonly",true);
			    
			    startActivityForResult(in, pick_img_code);
		    }
		    else
		    {
		    	cf.ShowToast("Please select Elevator No.", 0);
		    }
		    }
		    catch (Exception e) {
				// TODO: handle exception
			}		
			/*try
			{
				String s =((EditText)findViewById(R.id.whi_ed).findViewWithTag("Path")).getText().toString();
				if(s==null)
				{
					s="";
				}
				Intent in=new Intent(WindMitCommareas.this,pickSingleimage.class);
				in.putExtra("path", s);
				in.putExtra("id", 1);
				startActivityForResult(in, pick_img_code);
			}
			catch (Exception e) {
				// TODO: handle exception
			}*/
			break;
		case R.id.stair_chk8:
			if(((CheckBox)findViewById(R.id.stair_chk8)).isChecked())
			{
				((EditText)findViewById(R.id.edstaircons_other)).setVisibility(cf.v1.VISIBLE);
				cf.setFocus((EditText)findViewById(R.id.edstaircons_other));
			}
			else
			{
				((EditText)findViewById(R.id.edstaircons_other)).setVisibility(cf.v1.GONE);
				((EditText)findViewById(R.id.edstaircons_other)).setText("");
			}				
			break;
		case R.id.elvloadcomments:
				cf.findinspectionname(cf.identityval);
				int len=((EditText)findViewById(R.id.elevcomments)).getText().toString().length();
				if(load_comment)
				{
						load_comment=false;
						int loc1[] = new int[2];
						v.getLocationOnScreen(loc1);
						Intent in1 = cf.loadComments("Elevator Comments",loc1);
						in1.putExtra("insp_name", cf.inspname);
						in1.putExtra("insp_ques", "Communal Areas");
						in1.putExtra("length", len);
						in1.putExtra("max_length",500);
						startActivityForResult(in1, cf.loadcomment_code);
				}
			//***Call for the comments ends***//*			
			break;		
		case R.id.loadcomments:
				 cf.findinspectionname(cf.identityval);
				 int len1=((EditText)findViewById(R.id.communalareascomm)).getText().toString().length();
				 if(load_comment)
				{
						load_comment=false;
						int loc1[] = new int[2];
						v.getLocationOnScreen(loc1);
						Intent in1 = cf.loadComments("Overall Communal Areas Comments",loc1);
						in1.putExtra("insp_name", cf.inspname);
						in1.putExtra("insp_ques", "Communal Areas");
						in1.putExtra("length", len1);
						in1.putExtra("max_length", 500);
						startActivityForResult(in1, cf.loadcomment_code);
				}
				 			
			break;
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.areas_chk1:
			if(((CheckBox)findViewById(R.id.areas_chk1)).isChecked())
			{
				areasspinner[0].setEnabled(true);
			}
			else
			{	
				areasspinner[0].setEnabled(false);
			 	areasspinner[0].setSelection(0);
			}
			break;
		case R.id.areas_chk2:
			if(((CheckBox)findViewById(R.id.areas_chk2)).isChecked())
			{
				areasspinner[1].setEnabled(true);
			}
			else
			{	
				areasspinner[1].setEnabled(false);
				areasspinner[1].setSelection(0);
			}
			break;
		case R.id.areas_chk3:
			if(((CheckBox)findViewById(R.id.areas_chk3)).isChecked())
			{
				areasspinner[2].setEnabled(true);
			}
			else
			{	
				areasspinner[2].setEnabled(false);
				areasspinner[2].setSelection(0);
			}
			break;
		case R.id.areas_chk4:
			if(((CheckBox)findViewById(R.id.areas_chk4)).isChecked())
			{
				areasspinner[3].setEnabled(true);
				((EditText)findViewById(R.id.edotherareas)).setEnabled(true);
			}
			else
			{
				areasspinner[3].setEnabled(false);
				areasspinner[3].setSelection(0);
				((EditText)findViewById(R.id.edotherareas)).setText("");
				((EditText)findViewById(R.id.edotherareas)).setEnabled(false);
			}
			break;
		/*case R.id.areassave:
			poolareas_val="";			
				boolean b[] = {true,true,true,true};				
				if(((CheckBox)findViewById(R.id.areas_chk1)).isChecked()){areaval1=1;}else{areaval1=0;}
				if(((CheckBox)findViewById(R.id.areas_chk2)).isChecked()){areaval2=1;}else{areaval2=0;}
				if(((CheckBox)findViewById(R.id.areas_chk3)).isChecked()){areaval3=1;}else{areaval3=0;}
				if(((CheckBox)findViewById(R.id.areas_chk4)).isChecked()){areaval4=1;}else{areaval4=0;}
				b[0]=((areaval1==1) && (!areastxt1.equals("--Select--"))) ? true:false;
				b[1]=((areaval2==1) && (!areastxt2.equals("--Select--"))) ? true:false;
				b[2]=((areaval3==1) && (!areastxt3.equals("--Select--"))) ? true:false;
				b[3]=((areaval4==1) && (!areastxt4.equals("--Select--")) && (!((EditText)findViewById(R.id.edotherareas)).getText().toString().trim().equals(""))) ? true:false;
				
				if(((CheckBox)findViewById(R.id.areas_chk1)).isChecked()==false && ((CheckBox)findViewById(R.id.areas_chk2)).isChecked()==false
						&& ((CheckBox)findViewById(R.id.areas_chk3)).isChecked()==false && ((CheckBox)findViewById(R.id.areas_chk4)).isChecked()==false)
				{
					cf.ShowToast("Please select atleast one option under Other Areas.", 0);
				}
				else 
				{
					if(b[0]==false && ((CheckBox)findViewById(R.id.areas_chk1)).isChecked())
					{
						cf.ShowToast("Please select the Pool Areas.", 0);
					}
					else if(b[1]==false && ((CheckBox)findViewById(R.id.areas_chk2)).isChecked())
					{
						cf.ShowToast("Please select the Fitness Areas.", 0);
					}
					else if(b[2]==false && ((CheckBox)findViewById(R.id.areas_chk3)).isChecked())
					{
						cf.ShowToast("Please select the Communal Bathrooms.", 0);
					}
					else if((b[3]==false && ((CheckBox)findViewById(R.id.areas_chk4)).isChecked()))
					{
						if(((EditText)findViewById(R.id.edotherareas)).getText().toString().trim().equals(""))
						{
							cf.ShowToast("Please enter the other text under Other Areas.", 0);
							cf.setFocus(((EditText)findViewById(R.id.edotherareas)));
						}
						else
						{
							cf.ShowToast("Please select the Other Areas.", 0);	
						}
					}
					else
					{
						inserotherareas();
					}
				}
			break;*/
		case R.id.chknotdetermined:
			if(((CheckBox)findViewById(R.id.chknotdetermined)).isChecked())
			{	
				k=1;
				((EditText)findViewById(R.id.edlastinspection)).setText("Not Determined");
				((EditText)findViewById(R.id.edlastinspection)).setEnabled(false);
				((Button)findViewById(R.id.dateicon1)).setEnabled(false);
			}
			else
			{
				k=0;
				((Button)findViewById(R.id.dateicon1)).setEnabled(true);	
				((EditText)findViewById(R.id.edlastinspection)).setText("");
			}			
			break;
		case R.id.dateicon1:
			System.out.println("dateicon"+k);
			if(k==0)
			{
				((Button)findViewById(R.id.dateicon1)).setEnabled(true);	System.out.println("dateicon1"+k);
				cf.showDialogDate(((EditText)findViewById(R.id.edlastinspection)));
			}
			else
			{
				
			}
			break;
		/*case R.id.elevationsave:
				chkvalues();						
				if(rwselev==0)
				{
					cf.ShowToast("Atleast one Elevator entry need to be added and saved.", 0);
				}
				else
				{
					validationcomm();
				}
			break;	*/		
		case R.id.elevclear:
			clear();
			break;
		case R.id.elevsave:
			if(!((EditText)findViewById(R.id.edelevatorno)).getText().toString().trim().equals(""))
			{
				if(!((EditText)findViewById(R.id.edmodel)).getText().toString().trim().equals(""))
				{
					if(!((EditText)findViewById(R.id.edmanufacture)).getText().toString().trim().equals(""))
					{
						if(!"--Select--".equals(servingfloor.getSelectedItem().toString()))
						{
							/*if(((EditText)findViewById(R.id.whi_ed)).getText().toString().trim().equals(""))
							{
								cf.ShowToast("Please upload Image.",0);
								cf.setFocus(((EditText)findViewById(R.id.whi_ed)));
							}
							else
							{				*/				
								 if(!((EditText)findViewById(R.id.edlastinspection)).getText().toString().trim().equals("") || ((CheckBox)findViewById(R.id.chknotdetermined)).isChecked()==true)
								 {
										if(k==0)
									 	{
									 		if(cf.checkfortodaysdate(((EditText)findViewById(R.id.edlastinspection)).getText().toString())=="false")
							 		       	{
												 cf.ShowToast("Date of Last Inspection should not exceed current Year/Month/Date.",0);
							 		       		
							 		       	}
											else
											{
												elevationvalidation();
											}
										}
										else
										{
											elevationvalidation();
										}								 
								 }
								 else
								 {
									 cf.ShowToast("Please enter the Date of Last Inspection.", 0);
								 }
							//}							
						}
						else
						{
							 cf.ShowToast("Please select Number of Floors Served.", 0);
						}
					}
					else
					{
						 cf.ShowToast("Please enter the Manufacturer.", 0);
						 cf.setFocus(((EditText)findViewById(R.id.edmanufacture)));
					}
				}
				else
				{
					 cf.ShowToast("Please enter the Elevator Model.", 0);
					 cf.setFocus(((EditText)findViewById(R.id.edmodel)));
				}
			}
			else
			{
				 cf.ShowToast("Please enter the Elevator No.", 0);
				 cf.setFocus(((EditText)findViewById(R.id.edelevatorno)));
			}
			break;			
			case R.id.shwlobby:
			if(!((CheckBox)findViewById(R.id.lobbynotapplicable)).isChecked()){
				hideotherlayouts();
				((CheckBox)findViewById(R.id.lobbynotapplicable)).setChecked(false);
				((LinearLayout)findViewById(R.id.lobbylin)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdwlobby)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.shwlobby)).setVisibility(cf.v1.GONE);
			}
			break;
			case R.id.hdwlobby:
				((ImageView)findViewById(R.id.shwlobby)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdwlobby)).setVisibility(cf.v1.GONE);
				((LinearLayout)findViewById(R.id.lobbylin)).setVisibility(cf.v1.GONE);
				break;
			case R.id.shwstairs:
				if(!((CheckBox)findViewById(R.id.stairsnotapplicable)).isChecked()){
					hideotherlayouts();
					((CheckBox)findViewById(R.id.stairsnotapplicable)).setChecked(false);
					((LinearLayout)findViewById(R.id.stairlin)).setVisibility(cf.v1.VISIBLE);
					((ImageView)findViewById(R.id.hdwstairs)).setVisibility(cf.v1.VISIBLE);
					((ImageView)findViewById(R.id.shwstairs)).setVisibility(cf.v1.GONE);
				}
				break;
			case R.id.hdwstairs:
				((ImageView)findViewById(R.id.shwstairs)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdwstairs)).setVisibility(cf.v1.GONE);
				((LinearLayout)findViewById(R.id.stairlin)).setVisibility(cf.v1.GONE);
				break;
			case R.id.shwelev:
				if(!((CheckBox)findViewById(R.id.elevnotapplicable)).isChecked()){
					hideotherlayouts();
					((CheckBox)findViewById(R.id.elevnotapplicable)).setChecked(false);
					((LinearLayout)findViewById(R.id.elevatorslin)).setVisibility(cf.v1.VISIBLE);
					((ImageView)findViewById(R.id.hdwelev)).setVisibility(cf.v1.VISIBLE);
					((ImageView)findViewById(R.id.shwelev)).setVisibility(cf.v1.GONE);
				}
				break;
			case R.id.hdwelev:
				((ImageView)findViewById(R.id.shwelev)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdwelev)).setVisibility(cf.v1.GONE);
				((LinearLayout)findViewById(R.id.elevatorslin)).setVisibility(cf.v1.GONE);
				break;
			case R.id.shwareas:
				if(!((CheckBox)findViewById(R.id.areasnotapplicable)).isChecked()){
					hideotherlayouts();
					((CheckBox)findViewById(R.id.areasnotapplicable)).setChecked(false);
					((LinearLayout)findViewById(R.id.areaslin)).setVisibility(cf.v1.VISIBLE);
					((ImageView)findViewById(R.id.hdwareas)).setVisibility(cf.v1.VISIBLE);
					((ImageView)findViewById(R.id.shwareas)).setVisibility(cf.v1.GONE);
				}
				break;
			case R.id.hdwareas:
				((ImageView)findViewById(R.id.shwareas)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdwareas)).setVisibility(cf.v1.GONE);
				((LinearLayout)findViewById(R.id.areaslin)).setVisibility(cf.v1.GONE);
				break;
			case R.id.lobbynotapplicable:			
				if(((CheckBox)findViewById(R.id.lobbynotapplicable)).isChecked())
				{
					lobbynotappl="1";
					if(!lobby_val.equals(""))
					{
						showalert(1);
					}
					else
					{
						((TextView)findViewById(R.id.savetxtlobby)).setVisibility(cf.v1.INVISIBLE);
						((LinearLayout)findViewById(R.id.lobbylin)).setVisibility(cf.v1.GONE);						
						((ImageView)findViewById(R.id.shwlobby)).setVisibility(cf.v1.VISIBLE);
	 			    	((ImageView)findViewById(R.id.hdwlobby)).setVisibility(cf.v1.GONE);
	 			    	((ImageView)findViewById(R.id.shwlobby)).setEnabled(false);
					}					
				}
				else
				{
				 	  lobbynotappl="0";
					  UncheckLobby();
					 ((ImageView)findViewById(R.id.shwlobby)).setVisibility(cf.v1.GONE);
				     ((ImageView)findViewById(R.id.hdwlobby)).setVisibility(cf.v1.VISIBLE);
				     ((ImageView)findViewById(R.id.shwlobby)).setEnabled(true);
					 ((CheckBox)findViewById(R.id.lobbynotapplicable)).setChecked(false);
					 ((LinearLayout)findViewById(R.id.lobbylin)).setVisibility(cf.v1.VISIBLE);
					 ((TextView)findViewById(R.id.savetxtlobby)).setVisibility(cf.v1.INVISIBLE);
					
				}
				break;
			case R.id.stairsnotapplicable:			
				if(((CheckBox)findViewById(R.id.stairsnotapplicable)).isChecked())
				{
					stairsnotappl="1";;					
					if(!stairwells_val.equals(""))
					{
						showalert(2);
					}
					else
					{
						((TextView)findViewById(R.id.savetxtstair)).setVisibility(cf.v1.INVISIBLE);
						((LinearLayout)findViewById(R.id.stairlin)).setVisibility(cf.v1.GONE);						
						((ImageView)findViewById(R.id.shwstairs)).setVisibility(cf.v1.VISIBLE);
	 			    	((ImageView)findViewById(R.id.hdwstairs)).setVisibility(cf.v1.GONE);
	 			    	((ImageView)findViewById(R.id.shwstairs)).setEnabled(false);
					}					
				}
				else
				{
					 stairsnotappl="0";
					 UncheckStairs();					
					 ((ImageView)findViewById(R.id.shwstairs)).setVisibility(cf.v1.GONE);
				     ((ImageView)findViewById(R.id.hdwstairs)).setVisibility(cf.v1.VISIBLE);
				     ((ImageView)findViewById(R.id.shwstairs)).setEnabled(true);
					 ((CheckBox)findViewById(R.id.stairsnotapplicable)).setChecked(false);
					 ((LinearLayout)findViewById(R.id.stairlin)).setVisibility(cf.v1.VISIBLE);
					 ((TextView)findViewById(R.id.savetxtstair)).setVisibility(cf.v1.INVISIBLE);
				}
				break;
			case R.id.elevnotapplicable:			
				if(((CheckBox)findViewById(R.id.elevnotapplicable)).isChecked())
				{
					elevatorsnotappl="1";//((EditText)findViewById(R.id.elevcomments)).setText("");
					System.out.println("rwsel"+rwselev);
				    if(!elevators_val.equals("") || rwselev>0)
					{
						showalert(3);
					}
					else
					{						
						((TextView)findViewById(R.id.savetxtelevators)).setVisibility(cf.v1.INVISIBLE);
						((LinearLayout)findViewById(R.id.elevatorslin)).setVisibility(cf.v1.GONE);						
						((ImageView)findViewById(R.id.shwelev)).setVisibility(cf.v1.VISIBLE);
	 			    	((ImageView)findViewById(R.id.hdwelev)).setVisibility(cf.v1.GONE);
	 			    	((ImageView)findViewById(R.id.shwelev)).setEnabled(false);
					}					
				}
				else
				{
					elevatorsnotappl="0";//((EditText)findViewById(R.id.elevcomments)).setText("");
					UncheckElevators();
					 ((ImageView)findViewById(R.id.shwelev)).setVisibility(cf.v1.GONE);
				     ((ImageView)findViewById(R.id.hdwelev)).setVisibility(cf.v1.VISIBLE);
				     ((ImageView)findViewById(R.id.shwelev)).setEnabled(true);
					 ((CheckBox)findViewById(R.id.elevnotapplicable)).setChecked(false);
					 ((LinearLayout)findViewById(R.id.elevatorslin)).setVisibility(cf.v1.VISIBLE);
					 ((TextView)findViewById(R.id.savetxtelevators)).setVisibility(cf.v1.INVISIBLE);
				}
				break;
			case R.id.areasnotapplicable:			
				if(((CheckBox)findViewById(R.id.areasnotapplicable)).isChecked())
				{
					areasnotappl="1";
					((EditText)findViewById(R.id.edotherareas)).setText("");
					if(!poolareas_val .equals(""))
					{
						showalert(4);
					}
					else
					{
						((TextView)findViewById(R.id.savetxtareas)).setVisibility(cf.v1.INVISIBLE);
						((LinearLayout)findViewById(R.id.areaslin)).setVisibility(cf.v1.GONE);						
						((ImageView)findViewById(R.id.shwareas)).setVisibility(cf.v1.VISIBLE);
	 			    	((ImageView)findViewById(R.id.hdwareas)).setVisibility(cf.v1.GONE);
	 			    	((ImageView)findViewById(R.id.shwareas)).setEnabled(false);
					}					
				}
				else
				{
					areasnotappl="1";((EditText)findViewById(R.id.edotherareas)).setText("");UncheckAreas();
					((ImageView)findViewById(R.id.shwareas)).setVisibility(cf.v1.GONE);
				     ((ImageView)findViewById(R.id.hdwareas)).setVisibility(cf.v1.VISIBLE);
				     ((ImageView)findViewById(R.id.shwareas)).setEnabled(true);
					 ((CheckBox)findViewById(R.id.areasnotapplicable)).setChecked(false);
					 ((LinearLayout)findViewById(R.id.areaslin)).setVisibility(cf.v1.VISIBLE);
					 ((TextView)findViewById(R.id.savetxtareas)).setVisibility(cf.v1.INVISIBLE);
				}
				break;
			case R.id.save:
				if(((CheckBox)findViewById(R.id.lobbynotapplicable)).isChecked())
				{
					stairvalidation();
				}
				else
				{					
					wallschk_value= cf.getselected_chk(wallschk);
					String wallsotrval =(wallschk[wallschk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.lobbywall_other)).getText().toString():""; 
					wallschk_value+=wallsotrval;
					if(wallschk_value.equals(""))
					{
						cf.ShowToast("Please select the option for Walls under Main Lobby/Areas." , 0);
				    }
					else
					{
						 if(wallschk[wallschk.length-1].isChecked())
					     {	 
								 if(wallsotrval.trim().equals("&#40;"))
								 {
									 cf.ShowToast("Please enter the other text for Walls under Main Lobby/Areas.", 0);
									 cf.setFocus(((EditText)findViewById(R.id.lobbywall_other)));
								 }
								 else
								 {
									floorvalidation();	
								 }							 
						 }
						 else
						 {
							 floorvalidation();	
						 }	
					}
				}
				break;
			/*case R.id.save: 
				chkvalues();
				if(lobby_val.equals("") && ((CheckBox)findViewById(R.id.lobbynotapplicable)).isChecked()==false)
				{
					cf.ShowToast("Please save the Main Lobby/Areas.", 0);
				}
				else if(stairwells_val.equals("") && ((CheckBox)findViewById(R.id.stairsnotapplicable)).isChecked()==false)
				{
					cf.ShowToast("Please save the Stairwells.", 0);
				}
				else if(elevators_val.equals("") && ((CheckBox)findViewById(R.id.elevnotapplicable)).isChecked()==false)
				{
					if(rwselev==0)
					{
						cf.ShowToast("Atleast one Elevator entry need to be added and saved.", 0);
					}
					else
					{
						cf.ShowToast("Please save Elevators.", 0);
					}						
				}
				else if(poolareas_val.equals("") && ((CheckBox)findViewById(R.id.areasnotapplicable)).isChecked()==false)
				{
					cf.ShowToast("Please save the Other Areas.", 0);
				}
				else
				{
					insertcommunalareas();
				}
				
				break;*/
			case R.id.walls_chk8:
				  if(wallschk[wallschk.length-1].isChecked())
				  { 
					  ((EditText)findViewById(R.id.lobbywall_other)).setVisibility(v.VISIBLE);
				  }
				  else
				  {
					  ((EditText)findViewById(R.id.lobbywall_other)).setVisibility(v.GONE);
					  ((EditText)findViewById(R.id.lobbywall_other)).setText("");
				  }							
			break;
			case R.id.floor_chk19:
				 if(floorchk[floorchk.length-1].isChecked())
				  { 
					  ((EditText)findViewById(R.id.edfloor_other)).setVisibility(v.VISIBLE);
				  }
				  else
				  {
					  ((EditText)findViewById(R.id.edfloor_other)).setVisibility(v.GONE);
					  ((EditText)findViewById(R.id.edfloor_other)).setText("");
				  }				
				break;
			case R.id.ceil_chk9:
				if(ceilingschk[ceilingschk.length-1].isChecked())
				  { 
					  ((EditText)findViewById(R.id.edceilings_other)).setVisibility(v.VISIBLE);
				  }
				  else
				  {
					  ((EditText)findViewById(R.id.edceilings_other)).setVisibility(v.GONE);
					  ((EditText)findViewById(R.id.edceilings_other)).setText("");
				  }							
				break;
			case R.id.doors_chk20:
				if(doorschk[doorschk.length-1].isChecked())
				  { 
					  ((EditText)findViewById(R.id.eddoors_other)).setVisibility(v.VISIBLE);
				  }
				  else
				  {
					  ((EditText)findViewById(R.id.eddoors_other)).setVisibility(v.GONE);
					  ((EditText)findViewById(R.id.eddoors_other)).setText("");
				  }							
				break;
			case R.id.windows_chk17:
				if(windowschk[windowschk.length-1].isChecked())
				  { 
					  ((EditText)findViewById(R.id.edwindows_other)).setVisibility(v.VISIBLE);
				  }
				  else
				  {
					  ((EditText)findViewById(R.id.edwindows_other)).setVisibility(v.GONE);
					  ((EditText)findViewById(R.id.edwindows_other)).setText("");
				  }							
				break;
			case R.id.stair_chk7:
				if(stairschk[stairschk.length-1].isChecked())
				  { 
					  ((EditText)findViewById(R.id.edstaircons_other)).setVisibility(v.VISIBLE);
				  }
				  else
				  {
					  ((EditText)findViewById(R.id.edstaircons_other)).setVisibility(v.GONE);
					  ((EditText)findViewById(R.id.edstaircons_other)).setText("");
				  }							
				break;
			case R.id.floortyp_chk5:
				if(floortypechk[floortypechk.length-1].isChecked())
				  { 
					  ((EditText)findViewById(R.id.edfloortype_other)).setVisibility(v.VISIBLE);
				  }
				  else
				  {
					  ((EditText)findViewById(R.id.edfloortype_other)).setVisibility(v.GONE);
					  ((EditText)findViewById(R.id.edfloortype_other)).setText("");
				  }			
				break;
			case R.id.railing_chk5:
				if(railingchk[railingchk.length-1].isChecked())
				  { 
					  ((EditText)findViewById(R.id.edrailing_other)).setVisibility(v.VISIBLE);
				  }
				  else
				  {
					  ((EditText)findViewById(R.id.edrailing_other)).setVisibility(v.GONE);
					  ((EditText)findViewById(R.id.edrailing_other)).setText("");
				  }			
				break;
			/*case R.id.lobbysave:
					wallschk_value= cf.getselected_chk(wallschk);
					String wallsotrval =(wallschk[wallschk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.lobbywall_other)).getText().toString():""; 
					wallschk_value+=wallsotrval;
					if(wallschk_value.equals(""))
					{
						cf.ShowToast("Please select the option for Walls." , 0);
				    }
					else
					{
						 if(wallschk[wallschk.length-1].isChecked())
					     {	 
								 if(wallsotrval.trim().equals("&#40;"))
								 {
									 cf.ShowToast("Please enter the other text for Walls.", 0);
									 cf.setFocus(((EditText)findViewById(R.id.lobbywall_other)));
								 }
								 else
								 {
									floorvalidation();	
								 }							 
						 }
						 else
						 {
							 floorvalidation();	
						 }	
					}
				break;*/
		/*	case R.id.stairwellsave:
				  int floorinspectedrdgpid= floorinspectedrdgval.getCheckedRadioButtonId();	
				 floorinspectedrdgtxt = (floorinspectedrdgpid==-1)? "":((RadioButton) findViewById(floorinspectedrdgpid)).getText().toString();
				 if(!floorinspectedrdgtxt.trim().equals(""))
				 {
						 starirwellschk_val= cf.getselected_chk(stairwellschk);
						 if(starirwellschk_val.equals(""))
						 {
							 cf.ShowToast("Please select the option for Stair Wells." , 0);
						 }
						 else
						 {
							 stairconsvalidation();
						 }
				 }
				 else
				 {
					 cf.ShowToast("Please select Provided to All Floor Levels." , 0);
				 }		 
				break;*/
		}
	}
	private void stairvalidation()
	{
		if(((CheckBox)findViewById(R.id.stairsnotapplicable)).isChecked())
		{
			elevvalidation();			
		}
		else
		{
		  int floorinspectedrdgpid= floorinspectedrdgval.getCheckedRadioButtonId();	
			 floorinspectedrdgtxt = (floorinspectedrdgpid==-1)? "":((RadioButton) findViewById(floorinspectedrdgpid)).getText().toString();
			 if(!floorinspectedrdgtxt.trim().equals(""))
			 {
					 starirwellschk_val= cf.getselected_chk(stairwellschk);
					 if(starirwellschk_val.equals(""))
					 {
						 cf.ShowToast("Please select the option for Stair Wells." , 0);
					 }
					 else
					 {
						 stairconsvalidation();
					 }
			 }
			 else
			 {
				 cf.ShowToast("Please select Provided to All Floor Levels under Stairwells." , 0);
			 }		
		}
	}
	private void stairschk() {
		// TODO Auto-generated method stub
		
		if(((CheckBox)findViewById(R.id.stairsnotapplicable)).isChecked())
		{
			chkvalues();
			if(stairsnotappl.equals(""))
			{
				cf.arr_db.execSQL("UPDATE "+cf.Communal_Areas+ " set fld_stairwell_NA='"+1+"' where fld_srid='"+cf.selectedhomeid+"' insp_typeid='"+cf.identityval+"'");
			    	
			}
			else
			{
				
			}
			elevchk();
		}
		else
		{  
			 chkvalues();
			 if(stairwells_val.equals(""))
			 {
				 cf.ShowToast("Please save the Stairwells.", 0);
			 }
			 else
			 {
				 elevchk();
			 }
		}
	}
	private void elevchk() {
		// TODO Auto-generated method stub
		
		if(((CheckBox)findViewById(R.id.elevnotapplicable)).isChecked())
		{
			chkvalues();System.out.println("elevators_na="+elevators_na);
			if(elevators_na.equals(""))
			{
				System.out.println("elevators_na="+elevators_na);
				cf.arr_db.execSQL("UPDATE "+cf.Communal_Areas+ " set fld_elevators_NA='"+1+"',fld_elevatorsoption='' where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
				cf.arr_db.execSQL("Delete from "+cf.Communal_AreasElevation+" Where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
				
			}
			else
			{
				
			}
			areaschk();
		}
		else
		{  
			 chkvalues();
			 if(elevators_val.equals("") || rwselev==0)
			 {
				 cf.ShowToast("Please save the Elevators.", 0);
			 }
			 else
			 {
				 areaschk();
			 }
		}
	}
	private void areaschk() {
		// TODO Auto-generated method stub
		if(((CheckBox)findViewById(R.id.areasnotapplicable)).isChecked())
		{
			chkvalues();
			if(poolareas_na .equals(""))
			{
				cf.arr_db.execSQL("UPDATE "+cf.Communal_Areas+ " set fld_poolareas_NA='"+1+"' where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
			    	
			}
			else
			{
				
			}
			insertcommunalareas();
		}
		else
		{  
			 chkvalues();
			 if(poolareas_val.equals(""))
			 {
				 cf.ShowToast("Please save the Other Areas.", 0);
			 }
			 else
			 {
				 insertcommunalareas();
			 }
		}
		
	}
	private void elevationvalidation() {
		// TODO Auto-generated method stub
		if(!((Button)findViewById(R.id.elevsave)).getText().toString().equals("Update"))
		{
		
			Cursor c1 = cf.arr_db.rawQuery("SELECT * FROM " + cf.Communal_AreasElevation + " WHERE fld_srid='"+ cf.selectedhomeid + "'  and " +
					"insp_typeid='"+cf.identityval+"' and fld_elevationno='"+((EditText)findViewById(R.id.edelevatorno)).getText().toString().trim()+"'", null);				
			elevrows= c1.getCount();
			if(elevrows==0)
			{
			  insertelevation();
			}
			else
			{
				cf.ShowToast("Already Exist! Please enter a different Elevator No.", 0);
				((EditText)findViewById(R.id.edelevatorno)).setText("");
			}
		}
		else
		{
			insertelevation();
		}
	}
	private void updatenotapplpoolareas() {
		// TODO Auto-generated method stub
		try
		{
			Cursor c1 = cf.arr_db.rawQuery("SELECT * FROM " + cf.Communal_Areas + " WHERE fld_srid='"+ cf.selectedhomeid + "'  and insp_typeid='"+cf.identityval+"'", null);				
			if(c1.getCount()==0)
			{
				cf.arr_db.execSQL("INSERT INTO " + cf.Communal_Areas + " (fld_srid,insp_typeid,fld_poolareas,fld_poolareasother,fld_poolareas_NA)"
						+ "VALUES ('"+cf.selectedhomeid+"','"+cf.identityval+"','"+cf.encode(poolareas_val)+"','"+cf.encode(((EditText)findViewById(R.id.edotherareas)).getText().toString().trim())+"','"+areasnotappl+"')");
			}
			else
			{
				cf.arr_db.execSQL("UPDATE "	+ cf.Communal_Areas	+ " SET fld_poolareas='"+cf.encode(poolareas_val)+"',fld_poolareasother='"+cf.encode(((EditText)findViewById(R.id.edotherareas)).getText().toString().trim())+"',fld_poolareas_NA='"+areasnotappl+"' WHERE fld_srid ='"+ cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'");	
			}			
		}
		catch(Exception e)
		{
			System.out.println("sdfdfds"+e.getMessage());
		}
	}
	protected void updatenotapplelevators() {
		// TODO Auto-generated method stub
		try
		{
			Cursor c1 = cf.arr_db.rawQuery("SELECT * FROM " + cf.Communal_Areas + " WHERE fld_srid='"+ cf.selectedhomeid + "'  and insp_typeid='"+cf.identityval+"'", null);				
			if(c1.getCount()==0)
			{
				cf.arr_db.execSQL("INSERT INTO " + cf.Communal_Areas + " (fld_srid,insp_typeid,fld_elevatorsoption,fld_elevators_NA)"
						+ "VALUES ('"+cf.selectedhomeid+"','"+cf.identityval+"','"+cf.encode(((EditText)findViewById(R.id.elevcomments)).getText().toString().trim())+"','"+elevatorsnotappl+"')");
			
			}
			else
			{
				cf.arr_db.execSQL("UPDATE "	+ cf.Communal_Areas	+ " SET fld_elevatorsoption='"+cf.encode(((EditText)findViewById(R.id.elevcomments)).getText().toString().trim())+"',fld_elevators_NA='"+elevatorsnotappl+"' WHERE fld_srid ='"+ cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'");	
			}					
		}
		catch(Exception e)
		{
			System.out.println("updatenotapplelevators"+e.getMessage());
		}
	}
	private void stairconsvalidation() {
		// TODO Auto-generated method stub
		stairconschk_value= cf.getselected_chk(stairschk);
		String stairsotrval =(stairschk[stairschk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.edstaircons_other)).getText().toString():""; 
		stairconschk_value+=stairsotrval;
		
		 if(stairconschk_value.equals(""))
		 {
			 cf.ShowToast("Please select the option for Stair Construction under Stairwells." , 0);
		 }
		 else
		 { 
			 if(stairschk[stairschk.length-1].isChecked())
			 {	 
				 if(stairsotrval.trim().equals("&#40;"))
				 {
					 cf.ShowToast("Please enter the other text for Stair Construction under Stairwells.", 0);
					 cf.setFocus(((EditText)findViewById(R.id.edstaircons_other)));
				 }
				 else
				 {
					 floortypevalidation();	
				 }							 
			 }
			 else
			 {
				 floortypevalidation();
			 }
		 }
	}
	private void Uncheckhdrs(CheckBox[] headerschk1) {
		// TODO Auto-generated method stub
		for(int i=0;i<headerschk1.length;i++){
			headerschk1[i].setChecked(false);
		}
		chkvalues();
		if(!lobby_val.equals("")){
			headerschk1[0].setChecked(true);((LinearLayout)findViewById(R.id.lobbylin)).setVisibility(cf.v1.VISIBLE);
		}
		else
		{
			UncheckLobby();
		}
		
		if(!stairwells_val.equals("")){
			headerschk1[1].setChecked(true);((LinearLayout)findViewById(R.id.stairlin)).setVisibility(cf.v1.VISIBLE);
		}
		else
		{
			UncheckStairs();
		}
		if(rwselev>0)
		{
			headerschk1[2].setChecked(true);((LinearLayout)findViewById(R.id.elevatorslin)).setVisibility(cf.v1.VISIBLE);
		}
		else			
		{
			UncheckElevators();
		}
		if(!poolareas_val.equals(""))
		{
			headerschk1[3].setChecked(true);((LinearLayout)findViewById(R.id.areaslin)).setVisibility(cf.v1.VISIBLE);
		}
		else
		{
			UncheckAreas();
		}
	}
	private void validationcomm() {
		// TODO Auto-generated method stub
		if(!((EditText)findViewById(R.id.elevcomments)).getText().toString().trim().equals(""))
		{
			otherareasvalidation();
		}
		else
		{
			cf.ShowToast("Please enter the Elevator Comments.", 0);
			cf.setFocus(((EditText)findViewById(R.id.elevcomments)));
		}
	}
	private void otherareasvalidation()
	{
		if(((CheckBox)findViewById(R.id.areasnotapplicable)).isChecked())
		{
			insertcommunalareas();			
		}
		else
		{
			poolareas_val="";			
			boolean b[] = {true,true,true,true};				
			if(((CheckBox)findViewById(R.id.areas_chk1)).isChecked()){areaval1=1;}else{areaval1=0;}
			if(((CheckBox)findViewById(R.id.areas_chk2)).isChecked()){areaval2=1;}else{areaval2=0;}
			if(((CheckBox)findViewById(R.id.areas_chk3)).isChecked()){areaval3=1;}else{areaval3=0;}
			if(((CheckBox)findViewById(R.id.areas_chk4)).isChecked()){areaval4=1;}else{areaval4=0;}
			b[0]=((areaval1==1) && (!areastxt1.equals("--Select--"))) ? true:false;
			b[1]=((areaval2==1) && (!areastxt2.equals("--Select--"))) ? true:false;
			b[2]=((areaval3==1) && (!areastxt3.equals("--Select--"))) ? true:false;
			b[3]=((areaval4==1) && (!areastxt4.equals("--Select--")) && (!((EditText)findViewById(R.id.edotherareas)).getText().toString().trim().equals(""))) ? true:false;
			
			if(((CheckBox)findViewById(R.id.areas_chk1)).isChecked()==false && ((CheckBox)findViewById(R.id.areas_chk2)).isChecked()==false
					&& ((CheckBox)findViewById(R.id.areas_chk3)).isChecked()==false && ((CheckBox)findViewById(R.id.areas_chk4)).isChecked()==false)
			{
				cf.ShowToast("Please select atleast one option under Other Areas.", 0);
			}
			else 
			{
				if(b[0]==false && ((CheckBox)findViewById(R.id.areas_chk1)).isChecked())
				{
					cf.ShowToast("Please select the Floor Levels for Pool Areas.", 0);
				}
				else if(b[1]==false && ((CheckBox)findViewById(R.id.areas_chk2)).isChecked())
				{
					cf.ShowToast("Please select the Floor Levels for Fitness Areas.", 0);
				}
				else if(b[2]==false && ((CheckBox)findViewById(R.id.areas_chk3)).isChecked())
				{
					cf.ShowToast("Please select the Floor Levels for Communal Bathrooms.", 0);
				}
				else if((b[3]==false && ((CheckBox)findViewById(R.id.areas_chk4)).isChecked()))
				{
					if(((EditText)findViewById(R.id.edotherareas)).getText().toString().trim().equals(""))
					{
						cf.ShowToast("Please enter the other text for Other Areas.", 0);
						cf.setFocus(((EditText)findViewById(R.id.edotherareas)));
					}
					else
					{
						cf.ShowToast("Please select the Floor Levels for Other Areas.", 0);	
					}
				}
				else
				{
					insertcommunalareas();
				}
			}
		}
	}
	private void UncheckLobby() {
		// TODO Auto-generated method stub
		for(int i=0;i<wallschk.length;i++)
		{
			wallschk[i].setChecked(false);			
		}
		for(int i=0;i<floorchk.length;i++)
		{
			floorchk[i].setChecked(false);			
		}
		for(int i=0;i<ceilingschk.length;i++)
		{
			ceilingschk[i].setChecked(false);			
		}
		for(int i=0;i<doorschk.length;i++)
		{
			doorschk[i].setChecked(false);			
		}
		for(int i=0;i<windowschk.length;i++)
		{
			windowschk[i].setChecked(false);			
		}
		((EditText)findViewById(R.id.lobbywall_other)).setVisibility(cf.v1.GONE);((EditText)findViewById(R.id.lobbywall_other)).setText("");
		((EditText)findViewById(R.id.edfloor_other)).setVisibility(cf.v1.GONE);((EditText)findViewById(R.id.edfloor_other)).setText("");
		((EditText)findViewById(R.id.edceilings_other)).setVisibility(cf.v1.GONE);((EditText)findViewById(R.id.edceilings_other)).setText("");
		((EditText)findViewById(R.id.eddoors_other)).setVisibility(cf.v1.GONE);((EditText)findViewById(R.id.eddoors_other)).setText("");
		((EditText)findViewById(R.id.edwindows_other)).setVisibility(cf.v1.GONE);((EditText)findViewById(R.id.edwindows_other)).setText("");
		((TextView)findViewById(R.id.savetxtlobby)).setVisibility(cf.v1.INVISIBLE);
		wallschk_value="";floorchk_val="";ceilingschk_val="";doorschk_val="";windowschk_val="";	lobby_val="";
		
	}
	private void UncheckStairs() {
		// TODO Auto-generated method stub
		for(int i=0;i<stairschk.length;i++)
		{
			stairschk[i].setChecked(false);			
		}
		for(int i=0;i<floortypechk.length;i++)
		{
			floortypechk[i].setChecked(false);			
		}
		for(int i=0;i<railingchk.length;i++)
		{
			railingchk[i].setChecked(false);			
		}
		for(int i=0;i<stairwellschk.length;i++)
		{
			stairwellschk[i].setChecked(false);			
		}
		((EditText)findViewById(R.id.edstaircons_other)).setVisibility(cf.v1.GONE);((EditText)findViewById(R.id.edstaircons_other)).setText("");
		((EditText)findViewById(R.id.edfloortype_other)).setVisibility(cf.v1.GONE);((EditText)findViewById(R.id.edfloortype_other)).setText("");
		((EditText)findViewById(R.id.edrailing_other)).setVisibility(cf.v1.GONE);((EditText)findViewById(R.id.edrailing_other)).setText("");
		metalchk=true;floorvalchk=true;
		try{if(metalchk){metalrailingrdgval.clearCheck();}}catch(Exception e){}
		try{if(floorvalchk){floorinspectedrdgval.clearCheck();}}catch(Exception e){}
		stairconschk_value="";floortypechk_val="";metalrailingstext="";railingschk_val="";starirwellschk_val="";floorinspectedrdgtxt="";		
		stairschkval=0;stairwells_val="";
		((LinearLayout)findViewById(R.id.ralilinglayout)).setVisibility(cf.v1.GONE);
		//insertStairwells();
	}
	private void UncheckAreas() {
		// TODO Auto-generated method stub
		areaschk[0].setChecked(false);
		 areaschk[1].setChecked(false);
		 areaschk[2].setChecked(false);
		 areaschk[3].setChecked(false);		 
		 areasspinner[0].setSelection(0); areasspinner[0].setEnabled(false);
		 areasspinner[1].setSelection(0); areasspinner[1].setEnabled(false);
		 areasspinner[2].setSelection(0); areasspinner[2].setEnabled(false);
		 areasspinner[3].setSelection(0); areasspinner[3].setEnabled(false);
		 ((EditText)findViewById(R.id.edotherareas)).setText("");		 
		 areaval1=0;areaval2=0;areaval3=0;areaval4=0;
		 areaschkval=0;poolareas_val="";
	}
	private void UncheckElevators() {
		// TODO Auto-generated method stub
		try
		{
			cf.arr_db.execSQL("DELETE FROM "+cf.Communal_AreasElevation+" WHERE fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
			
			Cursor c1 = cf.arr_db.rawQuery("SELECT * FROM " + cf.Elev_master + " WHERE Elev_srid='"+ cf.selectedhomeid + "'  and " +
					"Elev_insp_id='"+cf.identityval+"'", null);	
			if(c1.getCount()>0)
			{
				c1.moveToFirst();
				for(int i=0;i<c1.getCount();i++,c1.moveToNext())
				{
					String masterid =  c1.getString(c1.getColumnIndex("ELev_masterid"));
					cf.arr_db.execSQL("DELETE FROM "+cf.Elevatorimages+" WHERE Elev_masterid='"+masterid+"'");					
				}
			}
			cf.arr_db.execSQL("DELETE FROM "+cf.Elev_master+" WHERE Elev_srid='"+cf.selectedhomeid+"' and Elev_insp_id='"+cf.identityval+"'");
					
		}
		catch(Exception e)
		{
			System.out.println("inside  deeeeee  "+e.getMessage());
		}		
		servingfloorchk=true;elevators_val="";
		try{if(servingfloorchk){servingoffloorsedrdgval.clearCheck();}}catch(Exception e){}
		((CheckBox) findViewById(R.id.chknotdetermined)).setChecked(false);
		((EditText) findViewById(R.id.edlastinspection)).setText("");
		((EditText)findViewById(R.id.elevcomments)).setText("");
		subpnlshw.setVisibility(cf.v1.GONE);
		//((Button)findViewById(R.id.img_clr)).setVisibility(cf.v1.INVISIBLE);
		servingfloordgtxt="";
		rwselev=0;
		clear();
	}
	private void hideotherlayouts() {
		// TODO Auto-generated method stub
		((LinearLayout)findViewById(R.id.areaslin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.elevatorslin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.stairlin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.lobbylin)).setVisibility(cf.v1.GONE);		
		((ImageView)findViewById(R.id.shwlobby)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwstairs)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwelev)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwareas)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.hdwlobby)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdwstairs)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdwelev)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdwareas)).setVisibility(cf.v1.GONE);	
	}
	private void Show_elev_value() {
		// TODO Auto-generated method stub
		Cursor c1 = cf.arr_db.rawQuery("SELECT * FROM " + cf.Communal_AreasElevation + " WHERE fld_srid='" + cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"' order by Commid", null);				
		rwselev= c1.getCount();System.out.println("rwselevdf"+rwselev);
		arrelevno=new String[rwselev];
		arrelevmodel=new String[rwselev];
		arrelevmanufacturer=new String[rwselev];
		arrelevid=new String[rwselev];		
		arrelevservingfloors=new String[rwselev];
		arrelevimg=new String[rwselev];
		arrelevdate=new String[rwselev];System.out.println("arrelevdate"+rwselev);		
		if(rwselev>0)
		{System.out.println("grett"+rwselev);	
			c1.moveToFirst();
			 int i=0;
			 do {
				 arrelevid[i] = c1.getString(c1.getColumnIndex("Commid"));
				 arrelevno[i] = c1.getString(c1.getColumnIndex("fld_elevationno"));
				 arrelevmodel[i]=cf.decode(c1.getString(c1.getColumnIndex("fld_model")));
				 arrelevmanufacturer[i]=cf.decode(c1.getString(c1.getColumnIndex("fld_manufacturer")));
				 arrelevservingfloors[i]=cf.decode(c1.getString(c1.getColumnIndex("fld_servingallfloors")));
				 arrelevimg[i]=cf.decode(c1.getString(c1.getColumnIndex("fld_uploadimg")));
				 arrelevdate[i]=cf.decode(c1.getString(c1.getColumnIndex("fld_datelastinsp")));
					i++;
				} while (c1.moveToNext());	
			 subpnlshw.setVisibility(cf.v1.VISIBLE);
			 Disp_elev_value();
		}	
		else
		{
			subpnlshw.setVisibility(cf.v1.GONE);
			((Button)findViewById(R.id.elevsave)).setText("Save/Add More Elevators");
			((EditText)findViewById(R.id.edelevatorno)).setEnabled(true);
		}
	}

	private void Disp_elev_value() {
		// TODO Auto-generated method stub
		dynviewelev.removeAllViews();
		LinearLayout h1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvac_listview_header, null);
		LinearLayout th= (LinearLayout)h1.findViewById(R.id.elevationcommareas_hdr);th.setVisibility(cf.v1.VISIBLE);
		TableLayout.LayoutParams lp = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
	 	lp.setMargins(2, 0, 2, 2); 
	 	h1.removeAllViews(); 
	 	
	 	dynviewelev.addView(th,lp);	
	 	dynviewelev.setVisibility(View.VISIBLE);	

	 	for (int i = 0; i < rwselev; i++) 
		{	
			TextView No,txtmodel,txtmanufacturer,txtservingfloors,txtinspectiondate,txtphotos;
		
			
			LinearLayout t1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvac_listview, null);
			LinearLayout t = (LinearLayout)t1.findViewById(R.id.elevation_commareas);t.setVisibility(cf.v1.VISIBLE);
			t.setId(44444+i);/// Set some id for further use
			
		 	No= (TextView) t.findViewWithTag("No");			
		 	txtmodel= (TextView) t.findViewWithTag("Model"); 	
		 	txtmanufacturer= (TextView) t.findViewWithTag("Manufacturer");	
		 	
		 	No.setText(arrelevno[i]);
		 	txtmodel.setText(arrelevmodel[i]);
		 	txtmanufacturer.setText(arrelevmanufacturer[i]);
		 	
		 	txtservingfloors= (TextView) t.findViewWithTag("Servingfloors");
		 	txtservingfloors.setText(arrelevservingfloors[i]);
		 	
		 	txtinspectiondate= (TextView) t.findViewWithTag("dateinspection");
		 	txtinspectiondate.setText(arrelevdate[i]);
		 	
			txtphotos= (TextView) t.findViewWithTag("ViewPhotos");
			
			/*Cursor c1=cf.arr_db.rawQuery("Select * from "+cf.Elevatorimages+" Where Elev_masterid=(Select ELev_masterid from "+cf.Elev_master+
    				" Where Elev_srid='"+cf.selectedhomeid+"' and Elev_insp_id='"+cf.identityval+"' and Elev_No='"+cf.encode(arrelevno[i])+"') order by Elev_Order", null);
    		System.out.println("Select * from "+cf.Elevatorimages+" Where Elev_masterid=(Select ELev_masterid from "+cf.Elev_master+
    				" Where Elev_srid='"+cf.selectedhomeid+"' and Elev_insp_id='"+cf.identityval+"' and Elev_No='"+cf.encode(arrelevno[i])+"') order by Elev_Order");
			*/
			
			Cursor c1=cf.arr_db.rawQuery("Select * from "+cf.Elevatorimages+" Where Elev_masterid='"+arrelevid[i]+"' order by Elev_Order", null);
    		System.out.println("Select * from "+cf.Elevatorimages+" Where Elev_masterid='"+arrelevid[i]+"' order by Elev_Order");
			
			
    		System.out.println("Imagescount="+c1.getCount());
    		if(c1.getCount()>0)
    		{
    			txtphotos.setText(Html.fromHtml("<u>View Photos ("+c1.getCount()+")</u>"));
    			txtphotos.setOnClickListener(new Elev_EditClick(3,arrelevid[i]));
    			txtphotos.setTag(arrelevid[i]);
    			txtphotos.setTextColor(Color.BLUE);
			
    		}
    		else
    		{
    			txtphotos.setText("N/A");
    			//Ed.setOnClickListener(new RCT_edit_delete(3));
    			//txtphotos.setTag(cf.decode(c.getString(c.getColumnIndex("id"))));
    			//li_spu.addView(Ed);
    		}
					 	
		 /*	ImageView  iv=(ImageView)t.findViewWithTag("imgupload");
			File f=new File(arrelevimg[i]);
			if(f.exists())
			{
				
				Bitmap b1= cf.ShrinkBitmap(arrelevimg[i], 125, 125);
				if(b1!=null)
				{
					iv.setImageBitmap(b1);
					
				}
				else
				{
					iv.setImageDrawable(getResources().getDrawable(R.drawable.photonotavail));
				}
				
			}
			else
			{
				iv.setImageDrawable(getResources().getDrawable(R.drawable.photonotavail));
			}*/
		 	
		 	
		 	edit= (ImageView) t.findViewWithTag("Elev_edit");			 
		 	edit.setTag(arrelevid[i]);
		 	
		 	edit.setOnClickListener(new Elev_EditClick(1,arrelevid[i]));
		 	delete= (ImageView) t.findViewWithTag("Elev_delete");
		 	delete.setTag(arrelevid[i]);
		 	delete.setOnClickListener(new Elev_EditClick(2,arrelevid[i]));
		 	
		 	
		 	duplicate= (ImageView) t.findViewWithTag("dup");			 
		 	duplicate.setTag(arrelevid[i]);
		 	
		 	
		 	duplicate.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
				try
				{
				final int i=Integer.parseInt(v.getTag().toString());
				String elevno="";
				try{
					Cursor c1=cf.SelectTablefunction(cf.Communal_AreasElevation, " Where Commid='"+i+"'");
					if(c1.getCount()>0)
					{
						c1.moveToFirst();
						elevno=c1.getString(3);
						
					}
				}catch(Exception e){}
				AlertDialog.Builder builder = new AlertDialog.Builder(WindMitCommareas.this);
				builder.setMessage("Are you sure, Do you want to duplicate the Elevators details?")
						.setCancelable(false)
						.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog,
											int id) {
										LayoutInflater factory = LayoutInflater.from(WindMitCommareas.this);
                                        final View textEntryView = factory.inflate(R.layout.duplicatealert, null);
                                        final EditText input1 = (EditText) textEntryView.findViewById(R.id.dupid);
                                        
                                        InputFilter[] FilterArray = new InputFilter[1];  
                						FilterArray[0] = new InputFilter.LengthFilter(1);  
                						input1.setFilters(FilterArray);
                                        
                                        
										input1.setText("", TextView.BufferType.EDITABLE);
										
										final AlertDialog.Builder alert = new AlertDialog.Builder(WindMitCommareas.this);
										alert.setCancelable(false);
										alert.setIcon(R.drawable.yell_alert_icon).setTitle("Duplication:").setView(
										textEntryView).setPositiveButton("Duplicate",
										new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,
										int whichButton) {
											 cf.hidekeyboard(input1);
											try{
												
												Cursor elevdet=cf.SelectTablefunction(cf.Communal_AreasElevation, " Where Commid='"+i+"'");
												if(elevdet.getCount()>0)
												{
													elevdet.moveToFirst();
													String elevationno="",model="",manufacture="",servingfloors="",datelastinsp="";
													elevationno=elevdet.getString(3);
													model=elevdet.getString(4);
													servingfloors=elevdet.getString(5);
													datelastinsp=elevdet.getString(6);
													manufacture=elevdet.getString(8);
													System.out.println("length"+input1.getText().toString());
													int j;
													for(j=0;j<Integer.parseInt(input1.getText().toString());)
													{
														System.out.println("j"+j);
														cf.arr_db.execSQL("INSERT INTO "
																+ cf.Communal_AreasElevation
																+ " (insp_typeid,fld_srid,fld_elevationno,fld_model,fld_servingallfloors,fld_datelastinsp,fld_uploadimg,fld_manufacturer)"
																+ "VALUES('"+cf.identityval+"','"+cf.selectedhomeid+"','"+elevationno+"','"+model+"','"+servingfloors+"','"+datelastinsp+"','','"+manufacture+"')");
														
														
														Cursor mas=cf.SelectTablefunction(cf.Elevatorimages, " Where ELev_masterid='"+i+"'");
														System.out
																.println("massss="+mas.getCount()+"i="+i);
														if(mas.getCount()>0)
														{
															
															cf.arr_db.execSQL("INSERT INTO "+cf.Elev_master+" (Elev_srid,Elev_insp_id,Elev_No) Values ('"+cf.selectedhomeid+"','"+cf.identityval+"','"+elevationno+"')");
															System.out.println("whatisINSERT INTO "+cf.Elev_master+" (Elev_srid,Elev_insp_id,Elev_No) Values ('"+cf.selectedhomeid+"','"+cf.identityval+"','"+elevationno+"')");
														}
														j=j+1;System.out.println("iafter"+j);
														
													}
													
													/*Cursor master=cf.SelectTablefunction(cf.Elev_master, " Where Elev_srid='"+cf.selectedhomeid+"' and Elev_insp_id='"+cf.identityval+"' and Elev_No='"+elevationno+"'");
													System.out.println(" master count"+master.getCount());
													int k;
												
													if(master.getCount()>0)
													{
														for(k=0;k<Integer.parseInt(input1.getText().toString());)
														{
															cf.arr_db.execSQL("INSERT INTO "+cf.Elev_master+" (Elev_srid,Elev_insp_id,Elev_No) Values ('"+cf.selectedhomeid+"','"+cf.identityval+"','"+elevationno+"')");
															System.out.println("INSERT INTO "+cf.Elev_master+" (Elev_srid,Elev_insp_id,Elev_No) Values ('"+cf.selectedhomeid+"','"+cf.identityval+"','"+elevationno+"')");
															master.moveToFirst();
															for(int i=0;i<master.getCount();i++,master.moveToNext())
															{
															
															System.out
																	.println("INSERT INTO "+cf.Elevatorimages+" (Elev_masterid,Elev_module_id,Elev_Path,Elev_Order,Elev_Caption) select '"+i+"',0,Elev_Path,Elev_Order,Elev_Caption  From "+cf.Elevatorimages+" " +
																	"WHERE (Elev_masterid=(Select ELev_masterid from "+cf.Elev_master+" Where Elev_srid='"+cf.selectedhomeid+"' and Elev_insp_id='"+cf.identityval+"' and Elev_No='"+elevationno+"' )) ");
															
															cf.arr_db.execSQL("INSERT INTO "+cf.Elevatorimages+" (Elev_masterid,Elev_module_id,Elev_Path,Elev_Order,Elev_Caption) select '"+i+"',0,Elev_Path,Elev_Order,Elev_Caption  From "+cf.Elevatorimages+" " +
																	"WHERE (Elev_masterid=(Select ELev_masterid from "+cf.Elev_master+" Where Elev_srid='"+cf.selectedhomeid+"' and Elev_insp_id='"+cf.identityval+"' and Elev_No='"+elevationno+"' )) ");
															}

															
														k=k+1;
														}
														
														
														
													}	*/
													
													
												/*	master.moveToFirst();
													System.out
															.println("dfdsf"+master.);
													for(int i=0;i<master.getCount();i++,master.moveToNext())
													{
														System.out
																.println("ca,a");
														System.out
														.println("sdfds"+master.getString(master.getColumnIndex("ELev_masterid")));
														
														Cursor master2=cf.SelectTablefunction(cf.Elevatorimages, " Where Elev_masterid='"+master.getString(master.getColumnIndex("ELev_masterid"))+"'");
														System.out.println(" master2 count"+master2.getCount());
														if(master2.getCount()<=0)
														{
															System.out
															.println("INSERT INTO "+cf.Elevatorimages+" (Elev_masterid,Elev_module_id,Elev_Path,Elev_Order,Elev_Caption) '"+master.getString(master.getColumnIndex("ELev_masterid"))+"',0,Elev_Path,Elev_Order,Elev_Caption  From "+cf.Elevatorimages+" " +
														"WHERE (Elev_masterid=(Select ELev_masterid from "+cf.Elev_master+" Where Elev_srid='"+cf.selectedhomeid+"' and Elev_insp_id='"+cf.identityval+"' and  Elev_No='"+elevationno+"' )) ");
											
															cf.arr_db.execSQL("INSERT INTO "+cf.Elevatorimages+" (Elev_masterid,Elev_module_id,Elev_Path,Elev_Order,Elev_Caption) '"+master.getString(master.getColumnIndex("ELev_masterid"))+"',0,Elev_Path,Elev_Order,Elev_Caption  From "+cf.Elevatorimages+" " +
																"WHERE (Elev_masterid=(Select ELev_masterid from "+cf.Elev_master+" Where Elev_srid='"+cf.selectedhomeid+"' and Elev_insp_id='"+cf.identityval+"' and Elev_No='"+elevationno+"' )) ");
													
														}
														
													}*/
													
													/*int l;
													
													master.moveToFirst();
													for(l=0;l<Integer.parseInt(input1.getText().toString());)
													{
														
														for(int i=0;i<master.getCount();i++,master.moveToNext())
														{
															
															System.out
															.println("INSERT INTO "+cf.Elevatorimages+" (Elev_masterid,Elev_module_id,Elev_Path,Elev_Order,Elev_Caption) SELECT (Select ELev_masterid  as RIM_masterid from "+cf.Elev_master+" Where Elev_srid='"+cf.selectedhomeid+"' and Elev_insp_id='"+cf.identityval+"' and Elev_No='"+elevationno+"'),0,Elev_Path,Elev_Order,Elev_Caption  From "+cf.Elevatorimages+" " +
														"WHERE (Elev_masterid=(Select ELev_masterid from "+cf.Elev_master+" Where Elev_srid='"+cf.selectedhomeid+"' and Elev_insp_id='"+cf.identityval+"' and  Elev_No='"+elevationno+"' )) ");
											
															cf.arr_db.execSQL("INSERT INTO "+cf.Elevatorimages+" (Elev_masterid,Elev_module_id,Elev_Path,Elev_Order,Elev_Caption) SELECT (Select ELev_masterid  as RIM_masterid from "+cf.Elev_master+" Where Elev_srid='"+cf.selectedhomeid+"' and Elev_insp_id='"+cf.identityval+"' and Elev_No='"+elevationno+"'),0,Elev_Path,Elev_Order,Elev_Caption  From "+cf.Elevatorimages+" " +
																"WHERE (Elev_masterid=(Select ELev_masterid from "+cf.Elev_master+" Where Elev_srid='"+cf.selectedhomeid+"' and Elev_insp_id='"+cf.identityval+"' and Elev_No='"+elevationno+"' )) ");
														}
													
														
														
														
														l=l+1;
													}	*/	
													
													
													Cursor master=cf.SelectTablefunction(cf.Elev_master, " Where Elev_srid='"+cf.selectedhomeid+"' and Elev_insp_id='"+cf.identityval+"' and Elev_No='"+elevationno+"'");
													System.out.println(" master count"+master.getCount());
													int k;
												
													if(master.getCount()>0)
													{
														/*for(k=0;k<Integer.parseInt(input1.getText().toString());)
														{*/
															master.moveToFirst();
															for(int l=0;l<master.getCount();l++,master.moveToNext())
															{
															System.out
																	.println(master.getString(master.getColumnIndex("ELev_masterid"))+"test");
															//Cursor master1=cf.SelectTablefunction(cf.Elevatorimages, " Where Elev_masterid='"+master.getString(master.getColumnIndex("ELev_masterid"))+"'");
															Cursor master1=cf.SelectTablefunction(cf.Elevatorimages, " Where Elev_masterid='"+master.getString(master.getColumnIndex("ELev_masterid"))+"'");
															System.out
																	.println("imagecount="+master1.getCount());
															if(master1.getCount()<=0)
															{
																System.out
																		.println("INSERT INTO "+cf.Elevatorimages+" (Elev_masterid,Elev_module_id,Elev_Path,Elev_Order,Elev_Caption) select '"+master.getString(master.getColumnIndex("ELev_masterid"))+"',0,Elev_Path,Elev_Order,Elev_Caption  From "+cf.Elevatorimages+" " +
																		"WHERE Elev_masterid='"+i+"'");
																
																cf.arr_db.execSQL("INSERT INTO "+cf.Elevatorimages+" (Elev_masterid,Elev_module_id,Elev_Path,Elev_Order,Elev_Caption) select '"+master.getString(master.getColumnIndex("ELev_masterid"))+"',0,Elev_Path,Elev_Order,Elev_Caption  From "+cf.Elevatorimages+" " +
																		"WHERE Elev_masterid='"+i+"' ");
															}
														/*}

															
														k=k+1;*/
														}
														
														
														
													}
													
													
													
													
													 clear();
													 cf.ShowToast("Data Duplicated Successfully.", 0);
													 Show_elev_value();
													
													 dialog.dismiss();
													
												}
											}catch(Exception e){}
											dialog.dismiss();
										
										}
										}).setNegativeButton("Cancel",
										new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,
										int whichButton) {
											dialog.dismiss();
										}
										});
										alert.show();
									
									}
								})
						.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {

										dialog.cancel();
									}
								});
				AlertDialog al=builder.create();
				al.setTitle("Confirmation");
				al.setIcon(R.drawable.alertmsg);
				al.setCancelable(false);
				al.show();
				 
				}
				catch (Exception e) {
					// TODO: handle exception
				}
				}
            });
		 	
		 	t1.removeAllViews();
		 	dynviewelev.addView(t,lp);
		}
	 	
	}
	class Elev_EditClick implements OnClickListener
	{
		int edit,type;  
		Elev_EditClick(int edit,String id1)
		{
			this.edit=edit;
			this.type=Integer.parseInt(id1);
			
			System.out.println("type="+type+"edit="+edit);
		}
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			final String id=v.getTag().toString();
			WindMitCommareas.this.type = this.type;
			s =  this.type;
			System.out.println("what is edit="+edit);
			if(edit==1)
			{				
				((Button)findViewById(R.id.elevsave)).setText("Update");
				
				updateElev(type);
			}
			else if(edit==2)
			{
				AlertDialog.Builder builder = new AlertDialog.Builder(WindMitCommareas.this);
						builder.setMessage("Do you want to delete the selected Elevator?")
								.setCancelable(false)
								.setPositiveButton("Yes",
										new DialogInterface.OnClickListener() {

											public void onClick(DialogInterface dialog,
													int id) {												
												try
												{
													cf.arr_db.execSQL("DELETE FROM "+cf.Communal_AreasElevation+" WHERE Commid='"+type+"' and insp_typeid='"+cf.identityval+"'");
													
													
													
													cf.arr_db.execSQL("DELETE FROM "+cf.Elev_master+" WHERE ELev_masterid='"+type+"'");
													
													cf.arr_db.execSQL("DELETE FROM "+cf.Elevatorimages+" WHERE Elev_masterid='"+type+"'");
													
													cf.ShowToast("Deleted Successfully.", 0);
													
													
													Show_elev_value();
													clear();
												}
												catch (Exception e) {
													// TODO: handle exception
												}
											}
										})
								.setNegativeButton("No",
										new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog,
													int id) {

												dialog.cancel();
											}
										});
						builder.setTitle("Confirmation");
						builder.setIcon(R.drawable.alertmsg);
						builder.show();				
						
			}
			else if(edit==3)
			{
				Intent in=new Intent(WindMitCommareas.this,ElevImagePicking.class);
				in.putExtra("elev_img", true);					    

				Cursor c =cf.SelectTablefunction(cf.Communal_AreasElevation, " WHERE Commid='"+id+"'");
				System.out.println("ccc"+c.getCount()+"id="+id);				
				c.moveToFirst();
				String elevno="";
				if(c.getCount()>0)
				{
					elevno= cf.decode(c.getString(c.getColumnIndex("fld_elevationno")));	
				}
				
			    try
			    {
			    					    
			   
			    if(!elevno.equals(""))
			    {
			    	
			    	System.out.println("what is id="+id);
				    in.putExtra("elevno", elevno);
				    in.putExtra("id", id);
				    in.putExtra("Insp_id",cf.identityval);
				    in.putExtra("oper", ((Button)findViewById(R.id.elevsave)).getText().toString());
				    in.putExtra("SRID",cf.selectedhomeid);
				    in.putExtra("readonly", true);
				    startActivityForResult(in, pick_img_code);
			    }
			    else
			    {
			    	cf.ShowToast("Please select Elevator No.", 0);

			    }
			    }
			    catch (Exception e) {
					// TODO: handle exception
				}
				
			}
		}		
	}
	private void updateElev(int restvalue) {
		// TODO Auto-generated method stub
		elevid = restvalue; 
		Cursor c2=cf.SelectTablefunction(cf.Communal_AreasElevation, " WHERE fld_srid='"+cf.selectedhomeid+"' and Commid='"+elevid+"' and insp_typeid='"+cf.identityval+"'");
		
		if(c2.getCount()>0)
		{
			c2.moveToFirst();
			((EditText)findViewById(R.id.edelevatorno)).setText(c2.getString(c2.getColumnIndex("fld_elevationno")));		
			((EditText)findViewById(R.id.edelevatorno)).setEnabled(false);
			((EditText)findViewById(R.id.edmodel)).setText(cf.decode(c2.getString(c2.getColumnIndex("fld_model"))));
			((EditText)findViewById(R.id.edmanufacture)).setText(cf.decode(c2.getString(c2.getColumnIndex("fld_manufacturer"))));		 	
			String servfloors = c2.getString(c2.getColumnIndex("fld_servingallfloors"));
			int spinnerPosition1 = servingflooradap.getPosition(servfloors);
			servingfloor.setSelection(spinnerPosition1);
			//((EditText)findViewById(R.id.whi_ed)).setText(cf.decode(c2.getString(c2.getColumnIndex("fld_uploadimg"))));	
			
			if(cf.decode(c2.getString(c2.getColumnIndex("fld_datelastinsp"))).equals("Not Determined"))
			{
				k=1;
				((EditText)findViewById(R.id.edlastinspection)).setText("Not Determined");
				((EditText)findViewById(R.id.edlastinspection)).setEnabled(false);
				((CheckBox)findViewById(R.id.chknotdetermined)).setChecked(true);
			}
			else
			{
				k=0;
				((EditText)findViewById(R.id.edlastinspection)).setText(cf.decode(c2.getString(c2.getColumnIndex("fld_datelastinsp"))));
				((CheckBox)findViewById(R.id.chknotdetermined)).setChecked(false);
			}
			
		/*	Cursor c=cf.arr_db.rawQuery("Select * from "+cf.Elevatorimages+" Where Elev_masterid=(Select ELev_masterid from "+cf.Elev_master+
    				" Where  Elev_srid='"+cf.selectedhomeid+"' and Elev_insp_id='"+cf.identityval+"' and Elev_No='"+c2.getString(c2.getColumnIndex("fld_elevationno"))+"') order by Elev_Order", null);*/
			
			System.out.println("what is elevid=="+elevid);
			Cursor c=cf.arr_db.rawQuery("Select * from "+cf.Elevatorimages+" Where Elev_masterid='"+elevid+"' order by Elev_Order", null);
    		
    		if(c.getCount()>0)
    		{
    			tv.setVisibility(View.VISIBLE);
    			tv.setText(Html.fromHtml("<u>View/Edit Photos ("+c.getCount()+")</u>"));
    			tv.setTag(elevid);
		    	
    		}
    		else
    		{
    				tv.setVisibility(View.INVISIBLE);
    		}
			
			//((Button)findViewById(R.id.img_clr)).setVisibility(cf.v1.VISIBLE);
		}
	}
	private void floortypevalidation() {
		// TODO Auto-generated method stub
		floortypechk_val= cf.getselected_chk(floortypechk);
		String floortypeotrval =(floortypechk[floortypechk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.edfloortype_other)).getText().toString():""; 
		floortypechk_val+=floortypeotrval;	
			 if(floortypechk_val.equals(""))
			 {
				 cf.ShowToast("Please select the option for Floor Type under Stairwells." , 0);
			 }
			 else
			 {
				 if(floortypechk[floortypechk.length-1].isChecked())
				 {	 
					 if(floortypeotrval.trim().equals("&#40;"))
					 {
						 cf.ShowToast("Please enter the other text for Floor Type under Stairwells.", 0);
						 cf.setFocus(((EditText)findViewById(R.id.edfloortype_other)));
					 }
					 else
					 {
						 otherstairvalidation();	
					 }
				 }
				 else
				 {
					 otherstairvalidation();
				 }
			 }	 
	}

	private void otherstairvalidation() {
		// TODO Auto-generated method stub
		if(!metalrailingstext.trim().equals(""))
		{
			if(metalrailingstext.equals("Yes"))
			{
				railingschk_val= cf.getselected_chk(railingchk);
				String railingotrval =(railingchk[railingchk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.edrailing_other)).getText().toString():""; 
				railingschk_val+=railingotrval;
				 if(railingschk_val.equals(""))
					 {
						 cf.ShowToast("Please select the option for Railings under Stairwells." , 0);
					 }
					 else
					 {
						 if(railingchk[railingchk.length-1].isChecked())
						 {	 
							 if(railingotrval.trim().equals("&#40;"))
							 {
								 cf.ShowToast("Please enter the other text for Railings under Stairwells.", 0);
								 cf.setFocus(((EditText)findViewById(R.id.edrailing_other)));
							 }
							 else
							 {
								 elevvalidation();	
							 }					 
						 }
						 else
						 {
							 elevvalidation();
						 }
					 }
			}
			else
			{
				elevvalidation();
			}			
		 }
		else
		{
			 cf.ShowToast("Please select the option for Stair Rail Type.", 0);
		}		
	}	
	private void elevvalidation()
	{
		if(((CheckBox)findViewById(R.id.elevnotapplicable)).isChecked())
		{
			otherareasvalidation();	
		}
		else
		{
			chkvalues();						
			if(rwselev==0)
			{
				cf.ShowToast("Please save atleast one Elevator.", 0);
			}
			else
			{
				validationcomm();
			}
		}
	}
	private void floorvalidation() {
		// TODO Auto-generated method stub		
		floorchk_val= cf.getselected_chk(floorchk);
		String floorotrval =(floorchk[floorchk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.edfloor_other)).getText().toString():""; 
		floorchk_val+=floorotrval;
		 if(floorchk_val.equals(""))
		 {
			 cf.ShowToast("Please select the option for Floors under Main Lobby/Areas." , 0);
		 }
		 else
		 {
			 if(floorchk[floorchk.length-1].isChecked())
			 {	 
				 if(floorotrval.trim().equals("&#40;"))
				 {
					 cf.ShowToast("Please enter the other text for Floors under Main Lobby/Areas.", 0);
					 cf.setFocus(((EditText)findViewById(R.id.edfloor_other)));
				 }
				 else
				 {
					 ceilingsvalidation();	
				 }			 
			 }
			 else
			 {
				 ceilingsvalidation();
			 }
		 }
	}

	private void ceilingsvalidation() {
		// TODO Auto-generated method stub		
		ceilingschk_val= cf.getselected_chk(ceilingschk);
		String ceilingsotrval =(ceilingschk[ceilingschk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.edceilings_other)).getText().toString():""; 
		ceilingschk_val+=ceilingsotrval;
		 if(ceilingschk_val.equals(""))
		 {
			 cf.ShowToast("Please select the option for Ceilings under Main Lobby/Areas." , 0);
		 }
		 else
		 {
			 if(ceilingschk[ceilingschk.length-1].isChecked())
			 {	 
				 if(ceilingsotrval.trim().equals("&#40;"))
				 {
					 cf.ShowToast("Please enter the other text for Ceilings under Main Lobby/Areas.", 0);
					 cf.setFocus(((EditText)findViewById(R.id.edceilings_other)));
				 }
				 else
				 {
					 doorsvalidation();	
				 }			 
			 }
			 else
			 {
				 doorsvalidation();
			 }
		 }
	}

	private void doorsvalidation() {
		// TODO Auto-generated method stub
		doorschk_val= cf.getselected_chk(doorschk);
		String doorsotrval =(doorschk[doorschk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.eddoors_other)).getText().toString():""; 
		doorschk_val+=doorsotrval;
		 if(doorschk_val.equals(""))
		 {
			 cf.ShowToast("Please select the option for Doors under Main Lobby/Areas." , 0);
		 }
		 else
		 {
			 if(doorschk[doorschk.length-1].isChecked())
			 {	 
				 if(doorsotrval.trim().equals("&#40;"))
				 {
					 cf.ShowToast("Please enter the other text for Doors under Main Lobby/Areas.", 0);
					 cf.setFocus(((EditText)findViewById(R.id.eddoors_other)));
				 }
				 else
				 {
					 windowsvalidation();	
				 }			 
			 }
			 else
			 {
				 windowsvalidation();
			 }
		 }		
	}
	private void windowsvalidation()
	{
		windowschk_val= cf.getselected_chk(windowschk);
		String windowsotrval =(windowschk[windowschk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.edwindows_other)).getText().toString():""; 
		windowschk_val+=windowsotrval;
		 if(windowschk_val.equals(""))
		 {
			 cf.ShowToast("Please select the option for Windows under Main Lobby/Areas." , 0);
		 }
		 else
		 {
			 if(windowschk[windowschk.length-1].isChecked())
			 {	 
				 if(windowsotrval.trim().equals("&#40;"))
				 {
					 cf.ShowToast("Please enter the other text for Windows under Main Lobby/Areas.", 0);
					 cf.setFocus(((EditText)findViewById(R.id.edwindows_other)));
				 }
				 else
				 {
					 stairvalidation();	 		
				 }			 
			 }
			 else
			 {
				 stairvalidation();	
			 }
		 }	
	}
	
	private void clear()
	{
		tv.setVisibility(cf.v1.INVISIBLE);
		((EditText)findViewById(R.id.edelevatorno)).setText("");	
		((EditText)findViewById(R.id.edmodel)).setText("");
		((EditText)findViewById(R.id.edmanufacture)).setText("");
		((Button)findViewById(R.id.elevsave)).setText("Save/Add More Elevators");
		((EditText)findViewById(R.id.edelevatorno)).setEnabled(true);
		((EditText)findViewById(R.id.edmanufacture)).setFocusable(false);
		//((EditText)findViewById(R.id.whi_ed)).setText("");
		((EditText)findViewById(R.id.edlastinspection)).setText("");	
		((CheckBox) findViewById(R.id.chknotdetermined)).setChecked(false);
		((Spinner)findViewById(R.id.serving_spin)).setSelection(0);
	}

	private void Lobby_Insert() {
		// TODO Auto-generated method stub		
		if(!wallschk_value.equals("") && !floorchk_val.equals("") && !ceilingschk_val.equals("") && !doorschk_val.equals("") && !windowschk_val.equals(""))
		{
			lobby_val =wallschk_value+"&#39;"+floorchk_val+"&#39;"+ceilingschk_val+"&#39;"+doorschk_val+"&#39;"+windowschk_val;
		}
		else
		{
			lobby_val="";
		}
		if(((CheckBox)findViewById(R.id.lobbynotapplicable)).isChecked()){lobbynotappl="1";}else{lobbynotappl="0";}
		updatenotappllobby();
		cf.ShowToast("Main Lobby/Areas saved successfully." , 0);	
		((TextView)findViewById(R.id.savetxtlobby)).setVisibility(cf.v1.VISIBLE);
	}
	private void insertStairwells() {
		// TODO Auto-generated method stub
		
		 if(!stairconschk_value.equals("") && !floortypechk_val.equals("") && !metalrailingstext.equals("")  && !starirwellschk_val.equals("") && !floorinspectedrdgtxt.equals(""))
		 {
			 stairwells_val =stairconschk_value+"&#39;"+floortypechk_val+"&#39;"+metalrailingstext+"&#39;"+railingschk_val+"&#39;"+starirwellschk_val+"&#39;"+floorinspectedrdgtxt;
		 }
		 else
		 {
			 stairwells_val="";
		 }
		 if(((CheckBox)findViewById(R.id.stairsnotapplicable)).isChecked()){stairsnotappl="1";}else{stairsnotappl="0";}
		 updatenotapplstairwells();	
		 cf.ShowToast("Stairwells saved successfully." , 0);		
		((TextView)findViewById(R.id.savetxtstair)).setVisibility(cf.v1.VISIBLE);		
	}
	private void insertelev() {
		// TODO Auto-generated method stub
		updatenotapplelevators();
		 cf.ShowToast("Elevators saved successfully." , 0);	
		((TextView)findViewById(R.id.savetxtelevators)).setVisibility(cf.v1.VISIBLE);
	}

	private void insertelevation() {
		// TODO Auto-generated method stub
			String elevnoval = ((EditText)findViewById(R.id.edelevatorno)).getText().toString().trim();
			String elevmodelval = ((EditText)findViewById(R.id.edmodel)).getText().toString().trim();
			String elevmanufactureval = ((EditText)findViewById(R.id.edmanufacture)).getText().toString().trim();
			
			Cursor c1 = cf.arr_db.rawQuery("SELECT * FROM " + cf.Communal_AreasElevation + " WHERE fld_srid='"+ cf.selectedhomeid + "'  and " +
					"insp_typeid='"+cf.identityval+"' and fld_elevationno='"+elevnoval+"'", null);	
			if(c1.getCount()==0)
			{
					 cf.arr_db.execSQL("INSERT INTO "
							+ cf.Communal_AreasElevation
							+ " (fld_srid,insp_typeid,fld_elevationno,fld_model,fld_manufacturer,fld_servingallfloors,fld_datelastinsp,fld_uploadimg)"
							+ "VALUES ('" + cf.selectedhomeid + "','"+cf.identityval+"','"+elevnoval+"','"+cf.encode(elevmodelval)+"','"+cf.encode(elevmanufactureval)+"','"+servingfloor.getSelectedItem().toString()+"','"+((EditText)findViewById(R.id.edlastinspection)).getText().toString().trim()+"','')");					 
			}
			else
			{
				/* cf.arr_db.execSQL("UPDATE "
							+ cf.Communal_AreasElevation
							+ " SET fld_model='"+cf.encode(elevmodelval)+"',fld_manufacturer='"+cf.encode(elevmanufactureval)+"',fld_servingallfloors='"+servingfloor.getSelectedItem().toString()+"',fld_datelastinsp='"+((EditText)findViewById(R.id.edlastinspection)).getText().toString().trim()+"'"
							+ " WHERE fld_srid ='"
							+ cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"' and fld_elevationno='"+elevnoval+"'");*/
				 System.out.println("UPDATE "
							+ cf.Communal_AreasElevation
							+ " SET fld_model='"+cf.encode(elevmodelval)+"',fld_manufacturer='"+cf.encode(elevmanufactureval)+"',fld_servingallfloors='"+servingfloor.getSelectedItem().toString()+"',fld_datelastinsp='"+((EditText)findViewById(R.id.edlastinspection)).getText().toString().trim()+"'"
							+ " WHERE Commid ='"+type+"' and  fld_srid ='"
							+ cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"' and fld_elevationno='"+elevnoval+"'");
				 
				 cf.arr_db.execSQL("UPDATE "
							+ cf.Communal_AreasElevation
							+ " SET fld_model='"+cf.encode(elevmodelval)+"',fld_manufacturer='"+cf.encode(elevmanufactureval)+"',fld_servingallfloors='"+servingfloor.getSelectedItem().toString()+"',fld_datelastinsp='"+((EditText)findViewById(R.id.edlastinspection)).getText().toString().trim()+"'"
							+ " WHERE Commid ='"+type+"' and  fld_srid ='"
							+ cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"' and fld_elevationno='"+elevnoval+"'");
			}
			
			cf.ShowToast("Elevator Information saved successfully." , 0);
		//((Button)findViewById(R.id.img_clr)).setVisibility(cf.v1.INVISIBLE);
			// ((Button)findViewById(R.id.whi_pck)).setVisibility(cf.v1.INVISIBLE);
			clear();
			Show_elev_value();
	}
	private void inserotherareas() {
		// TODO Auto-generated method stub
		if(areaval1==1 || areaval2==1 || areaval3==1 || areaval4==1)
		{
			poolareas_val += areaval1+"~"+areastxt1+"^"+areaval2+"~"+areastxt2+"^"+areaval3+"~"+areastxt3+"^"+areaval4+"~"+areastxt4;			
		}
		else
		{
			poolareas_val="";
		}
		 if(((CheckBox)findViewById(R.id.areasnotapplicable)).isChecked()){areasnotappl="1";}else{areasnotappl="0";}
		updatenotapplpoolareas();
		cf.ShowToast("Other Areas saved successfully." , 0);	
		((TextView)findViewById(R.id.savetxtareas)).setVisibility(cf.v1.VISIBLE);
	}
	private void insertcommunalareas() {
		// TODO Auto-generated method stub

		/* if(((EditText)findViewById(R.id.communalareascomm)).getText().toString().trim().equals(""))
		 {
			cf.ShowToast("Please enter the Overall Communal Areas Comments.",0);
			cf.setFocus(((EditText)findViewById(R.id.communalareascomm)));
		 }
		 else
		 {*/
			 try
			 {
				 if(((CheckBox)findViewById(R.id.lobbynotapplicable)).isChecked())
				 {
					lobbynotappl ="1";lobby_val="";
				 }
	             else
	             {
	            	 lobbynotappl ="0";
	            	 if(!wallschk_value.equals("") && !floorchk_val.equals("") && !ceilingschk_val.equals("") && !doorschk_val.equals("") && !windowschk_val.equals(""))
						{
							lobby_val =wallschk_value+"&#39;"+floorchk_val+"&#39;"+ceilingschk_val+"&#39;"+doorschk_val+"&#39;"+windowschk_val;
						}
						else
						{
							lobby_val="";
						}
	             }
				 
				 if(((CheckBox)findViewById(R.id.stairsnotapplicable)).isChecked())
				 {
					 stairsnotappl ="1";stairwells_val ="";
				 }
	             else
	             {
	            	 stairsnotappl ="0";
	            	 if(!stairconschk_value.equals("") && !floortypechk_val.equals("") && !metalrailingstext.equals("")  && !starirwellschk_val.equals("") && !floorinspectedrdgtxt.equals(""))
	        		 {
	        			 stairwells_val =stairconschk_value+"&#39;"+floortypechk_val+"&#39;"+metalrailingstext+"&#39;"+railingschk_val+"&#39;"+starirwellschk_val+"&#39;"+floorinspectedrdgtxt;
	        		 }
	        		 else
	        		 {
	        			 stairwells_val="";
	        		 }
	             }
				 
				 if(((CheckBox)findViewById(R.id.elevnotapplicable)).isChecked())
				 {
					 elevatorsnotappl  ="1";((EditText)findViewById(R.id.elevcomments)).setText("");
					 try
					 {
						cf.arr_db.execSQL("Delete from "+cf.Communal_AreasElevation+" Where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
					 }
					 catch(Exception e)
					 {
							System.out.println("elevator="+e.getMessage());		
					 }
				 }
	             else
	             {
	            	 elevatorsnotappl ="0";
	             }
				 
				 
				 if(((CheckBox)findViewById(R.id.areasnotapplicable)).isChecked())
				 {
					 areasnotappl ="1";poolareas_val ="";((EditText)findViewById(R.id.edotherareas)).setText("");
				 }
	             else
	             {
	            	 areasnotappl ="0";
	            	 if(areaval1==1 || areaval2==1 || areaval3==1 || areaval4==1)
	         		{
	         			poolareas_val += areaval1+"~"+areastxt1+"^"+areaval2+"~"+areastxt2+"^"+areaval3+"~"+areastxt3+"^"+areaval4+"~"+areastxt4;			
	         		}
	         		else
	         		{
	         			poolareas_val="";
	         		}
	             }
				   
				 
				updatenotappllobby();updatenotapplstairwells();updatenotapplelevators();updatenotapplpoolareas();
				chkvalues();
			if(c2.getCount()==0)
				{
						 cf.arr_db.execSQL("INSERT INTO "
									+ cf.Wind_Commercial_Comm
									+ " (fld_srid,insp_typeid,fld_commercialareascomments,fld_auxillarycomments,fld_restsuppcomments)"
									+ "VALUES ('" + cf.selectedhomeid + "','"+cf.identityval+"','"+cf.encode(((EditText)findViewById(R.id.communalareascomm)).getText().toString().trim())+"','','')");
				}
				else
				{
						cf.arr_db.execSQL("UPDATE "
								+ cf.Wind_Commercial_Comm
								+ " SET fld_commercialareascomments='"+cf.encode(((EditText)findViewById(R.id.communalareascomm)).getText().toString().trim())+"'"
								+ " WHERE fld_srid ='"
								+ cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'");
				}
				if(cf.identityval==32)
				{
					cf.ShowToast("Communal Areas saved successfully", 0);	
					cf.Roof=cf.getResourcesvalue(R.string.insp_4);
					cf.gotoclass(cf.identityval, Roof_section_common.class);
				}
				else if(cf.identityval==33)	
				{
					cf.ShowToast("Communal Areas saved successfully", 0);	
					cf.Roof =cf.getResourcesvalue(R.string.insp_5); 
					cf.gotoclass(cf.identityval, Roof_commercial_information.class);
				}
				else if(cf.identityval==34)	
				{
					cf.ShowToast("Communal Areas saved successfully", 0);	
					cf.Roof=cf.getResourcesvalue(R.string.insp_6);
					cf.gotoclass(cf.identityval, Roof_commercial_information.class);
				}
				
						
				
				//((TextView)findViewById(R.id.savetxt10)).setVisibility(cf.v1.VISIBLE);
			}
			catch(Exception e)
			{
				System.out.println("e suxccess"+e.getMessage());
			}
		 //}

		
			//Lobby_Insert();insertStairwells();insertelevation();insertelev();inserotherareas();
			/*if(elevatorsnotappl.equals("0") || ((CheckBox)findViewById(R.id.elevnotapplicable)).isChecked()==true)
			{
				try
				{
				cf.arr_db.execSQL("Delete from "+cf.Communal_AreasElevation+" Where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
				}
				catch(Exception e)
				{
					System.out.println("elevator="+e.getMessage());		
				}
			}		*/
			
		
		
		
	}
	private void updatenotappllobby() {
		// TODO Auto-generated method stub
			try
			{
				Cursor c1 = cf.arr_db.rawQuery("SELECT * FROM " + cf.Communal_Areas + " WHERE fld_srid='"+ cf.selectedhomeid + "'  and insp_typeid='"+cf.identityval+"'", null);				
				if(c1.getCount()==0)
				{
					cf.arr_db.execSQL("INSERT INTO " + cf.Communal_Areas + " (fld_srid,insp_typeid,fld_lobby,fld_lobby_NA)"
							+ "VALUES ('"+cf.selectedhomeid+"','"+cf.identityval+"','"+cf.encode(lobby_val)+"','"+lobbynotappl+"')");
				}
				else
				{
					cf.arr_db.execSQL("UPDATE "	+ cf.Communal_Areas	+ " SET fld_lobby='"+cf.encode(lobby_val)+"',fld_lobby_NA='"+lobbynotappl+"' WHERE fld_srid ='"+ cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'");	
				}			
			}
			catch(Exception e)
			{
				System.out.println("sdfdfds"+e.getMessage());
			}
	}
	private void updatenotapplstairwells() {
		// TODO Auto-generated method stub
		try
		{
			Cursor c1 = cf.arr_db.rawQuery("SELECT * FROM " + cf.Communal_Areas + " WHERE fld_srid='"+ cf.selectedhomeid + "'  and insp_typeid='"+cf.identityval+"'", null);				
			if(c1.getCount()==0)
			{
				cf.arr_db.execSQL("INSERT INTO " + cf.Communal_Areas + " (fld_srid,insp_typeid,fld_stairwells,fld_stairwell_NA)"
						+ "VALUES ('"+cf.selectedhomeid+"','"+cf.identityval+"','"+cf.encode(stairwells_val)+"','"+stairsnotappl+"')");
			}
			else
			{
				cf.arr_db.execSQL("UPDATE "	+ cf.Communal_Areas	+ " SET fld_stairwells='"+cf.encode(stairwells_val)+"',fld_stairwell_NA='"+stairsnotappl+"' WHERE fld_srid ='"+ cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'");	
			}			
		}
		catch(Exception e)
		{
			System.out.println("sdfdfds"+e.getMessage());
		}
	}

	private void showalert(final int i) {
		// TODO Auto-generated method stub	
		String titname="";
		switch(i)
		{
		case 1:
			titname="Main Lobby/Areas";
			break;
		case 2:
			titname="Stairwells";
			break;
		case 3:
			titname="Elevators";
			break;
		case 4:
			titname="Other Areas";
			break;
		default:
			break;
		}
		
			AlertDialog.Builder bl = new Builder(WindMitCommareas.this);
			bl.setTitle("Confirmation");
			bl.setIcon(R.drawable.alertmsg);
			bl.setMessage(Html.fromHtml("Do you want to clear the "+titname+" saved data?"));
			bl.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							switch(i)
							{
								 case 1:	
									 UncheckLobby();
									((CheckBox)findViewById(R.id.lobbynotapplicable)).setChecked(true);
									((LinearLayout)findViewById(R.id.lobbylin)).setVisibility(cf.v1.GONE);
									((TextView)findViewById(R.id.savetxtlobby)).setVisibility(cf.v1.INVISIBLE);
									((ImageView)findViewById(R.id.shwlobby)).setVisibility(cf.v1.VISIBLE);
					    			((ImageView)findViewById(R.id.hdwlobby)).setVisibility(cf.v1.GONE);
					    			((ImageView)findViewById(R.id.shwlobby)).setEnabled(false);
								 break;
								 case 2:								 	
									 UncheckStairs();
									((CheckBox)findViewById(R.id.stairsnotapplicable)).setChecked(true);
									((LinearLayout)findViewById(R.id.stairlin)).setVisibility(cf.v1.GONE);
									((TextView)findViewById(R.id.savetxtstair)).setVisibility(cf.v1.INVISIBLE);
									((ImageView)findViewById(R.id.shwstairs)).setVisibility(cf.v1.VISIBLE);
					    			((ImageView)findViewById(R.id.hdwstairs)).setVisibility(cf.v1.GONE);
					    			((ImageView)findViewById(R.id.shwstairs)).setEnabled(false);
								break;
								case 3:
									UncheckElevators();
									//cf.arr_db.execSQL("Delete from "+cf.Communal_AreasElevation+" Where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
									subpnlshw.setVisibility(cf.v1.GONE);
									((CheckBox)findViewById(R.id.elevnotapplicable)).setChecked(true);
									((LinearLayout)findViewById(R.id.elevatorslin)).setVisibility(cf.v1.GONE);
									((TextView)findViewById(R.id.savetxtelevators)).setVisibility(cf.v1.INVISIBLE);
									((ImageView)findViewById(R.id.shwelev)).setVisibility(cf.v1.VISIBLE);
					    			((ImageView)findViewById(R.id.hdwelev)).setVisibility(cf.v1.GONE);
					    			((ImageView)findViewById(R.id.shwelev)).setEnabled(false);
								break;
								case 4:
									UncheckAreas();
									((CheckBox)findViewById(R.id.areasnotapplicable)).setChecked(true);
									((LinearLayout)findViewById(R.id.areaslin)).setVisibility(cf.v1.GONE);
									((TextView)findViewById(R.id.savetxtareas)).setVisibility(cf.v1.INVISIBLE);
									((ImageView)findViewById(R.id.shwareas)).setVisibility(cf.v1.VISIBLE);
					    			((ImageView)findViewById(R.id.hdwareas)).setVisibility(cf.v1.GONE);
					    			((ImageView)findViewById(R.id.shwareas)).setEnabled(false);
								break;											
						 }	
					}
			});
			bl.setNegativeButton("No",
					new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog,
								int id) {
							switch(i){
							 case 1:
								 ((CheckBox)findViewById(R.id.lobbynotapplicable)).setChecked(false);
								 ((ImageView)findViewById(R.id.shwlobby)).setVisibility(cf.v1.GONE);
					    		 ((ImageView)findViewById(R.id.hdwlobby)).setVisibility(cf.v1.VISIBLE);
					    		 //((LinearLayout)findViewById(R.id.lobbynotapplicable)).setVisibility(cf.v1.VISIBLE);
								 break;
							 case 2:
								 ((CheckBox)findViewById(R.id.stairsnotapplicable)).setChecked(false);
								 ((ImageView)findViewById(R.id.shwstairs)).setVisibility(cf.v1.GONE);
					    		 ((ImageView)findViewById(R.id.hdwstairs)).setVisibility(cf.v1.VISIBLE);
					    		 //((LinearLayout)findViewById(R.id.lobbynotapplicable)).setVisibility(cf.v1.VISIBLE);
								 break;
							 case 3:
								 ((CheckBox)findViewById(R.id.elevnotapplicable)).setChecked(false);
								 ((ImageView)findViewById(R.id.shwelev)).setVisibility(cf.v1.GONE);
					    		 ((ImageView)findViewById(R.id.hdwelev)).setVisibility(cf.v1.VISIBLE);
								 break;
							 case 4:
								 ((CheckBox)findViewById(R.id.areasnotapplicable)).setChecked(false);
								 ((ImageView)findViewById(R.id.shwareas)).setVisibility(cf.v1.GONE);
					    		 ((ImageView)findViewById(R.id.hdwareas)).setVisibility(cf.v1.VISIBLE);
								 break;							
							 }
						}
	        });
			AlertDialog al=bl.create();
			al.setIcon(R.drawable.alertmsg);
			al.setCancelable(false);
			al.show();
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
 		// replaces the default 'Back' button action
 		if (keyCode == KeyEvent.KEYCODE_BACK) {
 			System.out.println("testtttt"+cf.identityval);
 			if(cf.identityval==32 ){
 				System.out.println("onlinspectionid"+cf.onlinspectionid);
 				cf.gotoclass(cf.identityval, WindMitOpenProt.class);
 			/*	if(cf.onlinspectionid.equals("12")){
 					
 			cf.gotoclass(cf.identityval, WindMitWallCons.class);}
 				else if(cf.onlinspectionid.equals("11")){
 					cf.gotoclass(cf.identityval, WindMitOpenProt.class);
 				}*/
 			}
 			else{

 				cf.gotoclass(cf.identityval,CommercialWind.class);
 			}
 			return true;
 		}
 		
 		return super.onKeyDown(keyCode, event);
 	}
	/**on activity result for the load comments**/
	 @Override
	 		protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	 		try
	 		{
	 			if(requestCode==pick_img_code)
	 			{
	 				switch (resultCode) {
	 					case RESULT_CANCELED:
	 					//((EditText)findViewById(R.id.whi_ed).findViewWithTag("Path")).setText("");
	 					break;
	 					case RESULT_OK:
	 					/*	cf.ShowToast("Image uploaded successfully.",0);
	 						if(data.getExtras().getInt("id")==1){
	 							((Button)findViewById(R.id.img_clr)).setVisibility(cf.v1.VISIBLE);
	 							((EditText)findViewById(R.id.whi_ed).findViewWithTag("Path")).setText(data.getExtras().getString("path").trim());
	 							
	 						}*/
	 						elevno=data.getExtras().getString("elevno");System.out.println("test="+elevno);
	 						

	 					break;
		 			}
	 				try
 					{
	 					Cursor c;
	 					if(!((Button)findViewById(R.id.elevsave)).getText().toString().equals("Update"))
	 					{
	 						 c=cf.arr_db.rawQuery("Select * from "+cf.Elevatorimages+" Where Elev_masterid=(Select ELev_masterid from "+cf.Elev_master+
 				    				" Where Elev_srid='"+cf.selectedhomeid+"' and Elev_insp_id='"+cf.identityval+"' and Elev_No='"+elevno+"') order by Elev_Order", null);
 				    		
	 					}
	 					else
	 					{
	 						 c=cf.arr_db.rawQuery("Select * from "+cf.Elevatorimages+" Where Elev_masterid=(Select ELev_masterid from "+cf.Elev_master+
 				    				" Where Elev_srid='"+cf.selectedhomeid+"' and Elev_insp_id='"+cf.identityval+"' and Elev_No='"+elevno+"' and ELev_masterid='"+WindMitCommareas.s+"') order by Elev_Order", null);
 				    	
	 					}
	 					
 							if(c.getCount()>0)
 				    		{
 				    			if(elevno.equals(((EditText)findViewById(R.id.edelevatorno)).getText().toString().trim()))
 				    			{
 				    				tv.setVisibility(View.VISIBLE);
 	 				    			tv.setText(Html.fromHtml("<u>View/Edit Photos ("+c.getCount()+")</u>"));
 	 				    	
 				    			}
 				    			
 				    		}
 				    		else
 				    		{
 				    				System.out.println("null  only cames");
 				    				tv.setVisibility(View.INVISIBLE);
 				    		}
 				    }
 				    catch (Exception e) {
 						// TODO: handle exception
 					}

	 			}
	 			else if(requestCode==cf.loadcomment_code)
	 			{
	 				load_comment=true;
	 				if(resultCode==RESULT_OK)
	 				{
	 					if(data.getExtras().getString("insp_opt").equals("Elevator Comments"))
	 					{
	 						cf.ShowToast("Comments added successfully.", 0);
	 						String elevcomments = ((EditText)findViewById(R.id.elevcomments)).getText().toString();
	 						((EditText)findViewById(R.id.elevcomments)).setText(elevcomments +" "+data.getExtras().getString("Comments"));
	 					}
	 					else
	 					{
	 						cf.ShowToast("Comments added successfully.", 0);
	 						String overallcommareascomm = ((EditText)findViewById(R.id.communalareascomm)).getText().toString();
	 						((EditText)findViewById(R.id.communalareascomm)).setText(overallcommareascomm +" "+data.getExtras().getString("Comments"));
	 					}
	 				}
	 			}
	 	  }catch (Exception e) 
	 	  {
	 			// TODO: handle exception
	 		System.out.println("the erro was "+e.getMessage());
	 	  }
	 	}/**on activity result for the load comments ends**/
}