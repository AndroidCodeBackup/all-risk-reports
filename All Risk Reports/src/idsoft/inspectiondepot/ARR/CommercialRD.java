/*
************************************************************
* Project: ALL RISK REPORT
* Module : WindMitCommareas.java
* Creation History:
Created By : Nalini on 10/24/2012
* Modification History:
Last Modified By : Gowri on 04/04/2013
************************************************************ 
*/

package idsoft.inspectiondepot.ARR;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.CommercialRC.RCrdioclicker;
import idsoft.inspectiondepot.ARR.WindMitCommAuxbuildings.Touch_Listener;
import idsoft.inspectiondepot.ARR.WindMitCommAuxbuildings.textwatcher;
import idsoft.inspectiondepot.ARR.WindMitCommRestsupp.RestSupp_EditClick;
import idsoft.inspectiondepot.ARR.WindMitCommRestsupp.Spin_Selectedlistener;
import idsoft.inspectiondepot.ARR.WindMitCommRestsupp.checklistenetr;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class CommercialRD extends Activity{
	CommonFunctions cf;
	String rdvalue="",rdsubvalue="",roofdeck="",tmp="";
	RadioGroup rdgpA;
	String c1_q2[] = {"Level A -- Wood OR Other Deck Type II only","Level B -- Metal Deck Type II or III","Level C -- Reinforced Concrete Roof Deck Type I, II or III","Not Applicable","Not Inspected","Unknown"};
	RadioButton rdrdio[] = new RadioButton[6];
	int[] rdid = {R.id.rd_rdgp1,R.id.rd_rdgp2,R.id.rd_rdgp3,R.id.rd_rdgp4,R.id.rd_rdgp5,R.id.rd_rdgp6};
	boolean load_comment=true;
	boolean levelAchk=false;
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cf=new CommonFunctions(this);	 
        Bundle extras = getIntent().getExtras();
  		if (extras != null) {
  			cf.getExtras(extras);				
  	 	}
  		  setContentView(R.layout.commwindroofdeck);
          cf.getDeviceDimensions();
          
          if(cf.identityval==32){ cf.typeidentity=309;}
          else if(cf.identityval==33){cf.typeidentity=310;}
          else if(cf.identityval==34){cf.typeidentity=310;}
         
          LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
          if(cf.identityval==33)
          {
            hdr_layout.addView(new HdrOnclickListener(this,1,"Commercial Type II","Commercial Wind","Roof Deck",3,1,cf));
          }
          else if(cf.identityval==34)
          {
        	  hdr_layout.addView(new HdrOnclickListener(this,1,"Commercial Type III","Commercial Wind","Roof Deck",3,1,cf));
          }
          
          
          cf.mainmenu_layout = (LinearLayout) findViewById(R.id.header); //** HEADER MENU **//
          cf.mainmenu_layout.setMinimumWidth(cf.wd);
          cf.mainmenu_layout.addView(new MyOnclickListener(this, 3, 0,0, cf));
         
          cf.type_submenulayout =(LinearLayout) findViewById(R.id.typesubmenu); //** WINDMIT TYPE SUBMENU **//
          cf.type_submenulayout.addView(new MyOnclickListener(this, 300, 1,0, cf));
          cf.windmittypechk(cf.identityval,((RadioButton)findViewById(R.id.b1802_chk)),((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((TextView)findViewById(R.id.commtxt)),((ImageView)findViewById(R.id.editbno))); // To check the checkbox of choosen inspection type
          cf.uncheckcommercial(((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((RadioButton)findViewById(R.id.b1802_chk))); 
      	    
          System.out.println("cf.ide"+cf.identityval+"type"+cf.typeidentity);
          cf.submenu_layout = (LinearLayout) findViewById(R.id.submenu);//** WINDMIT TYPE SUBMENU **//
          cf.submenu_layout.addView(new MyOnclickListener(this,cf.identityval,1,314, cf));
          
          cf.innersubmenu_layout = (LinearLayout) findViewById(R.id.innersubmenu);//** WINDMIT TYPE SUBMENU **//
          cf.innersubmenu_layout.addView(new MyOnclickListener(this,cf.identityval,1,125, cf));
          
          
          
          cf.mainlayout = (LinearLayout)findViewById(R.id.bc_table);
          cf.save = (Button)findViewById(R.id.save);
          cf.tblrw = (TableRow)findViewById(R.id.row2);
          cf.tblrw.setMinimumHeight(cf.ht);			
          Declaration(); cf.CreateARRTable(40);setValue();
    }
	private void setValue() {
		// TODO Auto-generated method stub
		try
		{
			Cursor c1 = cf.arr_db.rawQuery("SELECT * FROM " + cf.Commercial_Wind + " WHERE fld_srid='"+ cf.selectedhomeid + "'  and insp_typeid='"+cf.identityval+"'", null);				
			if(c1.getCount()>0)
			{
				c1.moveToFirst();
				String roofdeckvalue = c1.getString(c1.getColumnIndex("fld_roofdeck"));
				
					if(!"".equals(roofdeckvalue))
					{
						String rdsplit[] = roofdeckvalue.split("~");
						if(!String.valueOf(rdsplit[0]).equals(""))
						 {
							for(int i=0;i<rdrdio.length;i++)
							{
								if(String.valueOf(rdsplit[0]).equals(rdrdio[i].getTag().toString()))
								{
									rdrdio[i].setChecked(true);						
								}
							}
						 }
						if(rdsplit[0].equals("A"))
						{
							((LinearLayout)findViewById(R.id.levelBlin)).setVisibility(cf.v1.VISIBLE);
							((RadioButton) rdgpA.findViewWithTag(rdsplit[1])).setChecked(true);			
						}
						((TextView)findViewById(R.id.savetxtrd)).setVisibility(cf.v1.VISIBLE);
					}
					else
					{
						((TextView)findViewById(R.id.savetxtrd)).setVisibility(cf.v1.GONE);
					}
				       String rdcomments = cf.decode(c1.getString(c1.getColumnIndex("fld_rdcomments")));
					   if(!rdcomments.equals(""))
					   {
						   ((EditText)findViewById(R.id.rdcomment)).setText(rdcomments);
					   }				   
			}
			else
			{
				((TextView)findViewById(R.id.savetxtrd)).setVisibility(cf.v1.GONE);
			}
		}catch(Exception e)
		{
			System.out.println("catch 1"+e.getMessage());
		}
	}
	private void Declaration() {
		// TODO Auto-generated method stub
		cf.setupUI((ScrollView) findViewById(R.id.scr));
		 for(int i=0;i<rdrdio.length;i++)
	   	 {
			 rdrdio[i] = (RadioButton)findViewById(rdid[i]); 	
			 rdrdio[i].setOnClickListener(new RDrdioclicker());	   
	   	 }	
		 rdgpA = (RadioGroup)findViewById(R.id.levelArdgp);
		// rdgpA.setOnCheckedChangeListener(new checklistenetr(1));
		 ((EditText)findViewById(R.id.rdcomment)).addTextChangedListener(new textwatcher());
	}
	class textwatcher implements TextWatcher
    {
	         public int type;
	         textwatcher()
	    	{
	        	
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {


	    		// TODO Auto-generated method stub
	    		cf.showing_limit(s.toString(),(TextView)findViewById(R.id.rd_tv_type1),500);
	    		if(s.toString().startsWith(" "))
	    		{
	    			((EditText)findViewById(R.id.rdcomment)).setText("");
	    		}
	    		}

	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,

	    			int after) { 
	    		// TODO Auto-generated method stub
	     	}

	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,

	    			int count) {
	    		// TODO Auto-generated method stub
	    	}
	   }	
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.loadcomments:
			/***Call for the comments***/
			cf.findinspectionname(cf.identityval);RD_suboption();
			int len=((EditText)findViewById(R.id.rdcomment)).getText().toString().length();			
			if(!tmp.equals(""))
			{
					if(load_comment)
					{
						load_comment=false;
						int loc1[] = new int[2];
						v.getLocationOnScreen(loc1);
						Intent in = cf.loadComments(tmp,loc1);
						in.putExtra("insp_name", cf.inspname);
						in.putExtra("insp_ques", "Roof Deck");
						in.putExtra("length", len);
						in.putExtra("max_length", 500);
						startActivityForResult(in, cf.loadcomment_code);
					}
			}
			else
			{
				cf.ShowToast("Please select the option for Roof Deck.",1);
			}	
			break;
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.save:
			rdvalue=cf.getselecctedtag_radio(rdrdio);System.out.println("rdval"+rdvalue);	
			if(rdvalue.equals(""))
			{
				cf.ShowToast("Please select the option for Roof Deck." , 0);
			}
			else
			{
				if(rdrdio[0].isChecked())
				{
					int additionalrdgpid= rdgpA.getCheckedRadioButtonId();	
					rdsubvalue = (additionalrdgpid==-1)? "":((RadioButton) findViewById(additionalrdgpid)).getTag().toString();
					if(rdsubvalue.equals(""))
					{
						cf.ShowToast("Please select atleast one option under Level A - Wood OR Other Deck Type II only." , 0);
					}
					else
					{
						InsertRoofDeck();
					}
				}
				else
				{
					InsertRoofDeck();
				}
			}
			break;
		}
	}
	private void RD_suboption() {
		// TODO Auto-generated method stub
		 for(int i=0;i<rdrdio.length;i++)
    	 {
			 if(rdrdio[i].isChecked())
			 {
				tmp = c1_q2[i];
			 }
    	 }
	}
	class  RDrdioclicker implements OnClickListener
	{
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			for(int i=0;i<rdrdio.length;i++)
			{
				if(v.getId()==rdrdio[i].getId())
				{
					rdrdio[i].setChecked(true);
					
					if(v.getId()==rdrdio[0].getId())
					{
						levelAchk=true;
						try{if(levelAchk){rdgpA.clearCheck();}}catch(Exception e){}
						
						((LinearLayout)findViewById(R.id.levelBlin)).setVisibility(cf.v1.VISIBLE);						
					}
					else
					{
						((LinearLayout)findViewById(R.id.levelBlin)).setVisibility(cf.v1.GONE);
					}
				}
				else
				{
					rdrdio[i].setChecked(false);
				}
			}
		}
	}
	private void InsertRoofDeck() {
		// TODO Auto-generated method stub
		if(((EditText)findViewById(R.id.rdcomment)).getText().toString().trim().equals(""))
		{
			cf.ShowToast("Please enter the Roof Deck Comments.",1);
			cf.setFocus(((EditText)findViewById(R.id.rdcomment)));
		}
		else
		{
		roofdeck = rdvalue+"~"+rdsubvalue;System.out.println("roofdeck"+roofdeck);
		try
		{
			Cursor c1 =cf.SelectTablefunction(cf.Commercial_Wind, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
			if(c1.getCount()==0)
			{
				cf.arr_db.execSQL("INSERT INTO "
						+ cf.Commercial_Wind
						+ " (fld_srid,insp_typeid,fld_roofdeck,fld_rdcomments)"
						+ "VALUES ('"+cf.selectedhomeid+"','"+cf.identityval+"','"+roofdeck+"','"+cf.encode(((EditText)findViewById(R.id.rdcomment)).getText().toString().trim())+"')");
				
			}
			else
			{
				cf.arr_db.execSQL("UPDATE "
					+ cf.Commercial_Wind
					+ " SET fld_roofdeck='" + roofdeck + "',fld_rdcomments='"+cf.encode(((EditText)findViewById(R.id.rdcomment)).getText().toString().trim())+"'"
					+ " WHERE fld_srid ='"+ cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'");	
			}
			cf.ShowToast("Roof Deck saved successfully",1);
			cf.gotoclass(cf.identityval, CommercialSWR.class);
		}
		catch(Exception e)
		{
			System.out.println("catch inside "+e.getMessage());
		}
		}
	}
/*	class checklistenetr implements OnCheckedChangeListener
	{
    	int i;
		public checklistenetr(int i) {
			// TODO Auto-generated constructor stub
		 this.i=i;
		}

		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			   RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
		        // This puts the value (true/false) into the variable
		        boolean isChecked = checkedRadioButton.isChecked();
		        // If the radiobutton that has changed in check state is now checked...
		        if (isChecked)
		        {
		          switch (i) {
					case 1:
						rdsubvalue= checkedRadioButton.getText().toString().trim();
						 break;
		          }
		        }
		}
	}*/
	public boolean onKeyDown(int keyCode, KeyEvent event) {
 		// replaces the default 'Back' button action
 		if (keyCode == KeyEvent.KEYCODE_BACK) {
 			cf.gotoclass(cf.identityval, CommercialRC.class);
 			return true;
 		}
 		
 		return super.onKeyDown(keyCode, event);
 	}
	 /**on activity result for the load comments**/
	 @Override
	 		protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	 		try
	 		{
	 			if(requestCode==cf.loadcomment_code)
	 			{
	 				load_comment=true;
	 				if(resultCode==RESULT_OK)
	 				{
	 					String rdcomm = ((EditText)findViewById(R.id.rdcomment)).getText().toString();	 
	 					((EditText)findViewById(R.id.rdcomment)).setText(rdcomm +" "+data.getExtras().getString("Comments"));
		 			}
	 			}
	 		}
	 		catch (Exception e) {
	 			// TODO: handle exception
	 			System.out.println("the erro was "+e.getMessage());
	 		}
	 	}/**on activity result for the load comments ends**/
}