/*
************************************************************
* Project: ALL RISK REPORT
* Module : WindMitCommareas.java
* Creation History:
Created By : Nalini on 10/24/2012
* Modification History:
Last Modified By : Gowri on 11/28/2012
************************************************************ 
*/

package idsoft.inspectiondepot.ARR;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import idsoft.inspectiondepot.ARR.WindMitCommAuxbuildings.Touch_Listener;
import idsoft.inspectiondepot.ARR.WindMitCommAuxbuildings.textwatcher;
import idsoft.inspectiondepot.ARR.WindMitCommRestsupp.RestSupp_EditClick;
import idsoft.inspectiondepot.ARR.WindMitCommRestsupp.Spin_Selectedlistener;
import idsoft.inspectiondepot.ARR.WindMitCommRestsupp.checklistenetr;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class CommercialWind extends Activity{
	CommonFunctions cf;
	Cursor c1;
	RadioGroup terrainexpordgpid,windspeeedrdgpid,winddesignrdgpid;
	String terrainvalue="",wnidspeedvalue="",wniddesignvalue="",terrain="",terrexpo="",windspeed="",winddesign="",tervalue="";
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cf=new CommonFunctions(this);	 
        Bundle extras = getIntent().getExtras();
  		if (extras != null) {
  			cf.getExtras(extras);				
  	 	}
  		  setContentView(R.layout.commercialwind);
          cf.getDeviceDimensions();
          if(cf.identityval==32){ cf.typeidentity=309;}
          else if(cf.identityval==33){cf.typeidentity=310;}
          else if(cf.identityval==34){cf.typeidentity=310;}
          
          LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
          if(cf.identityval==33)
          {
            hdr_layout.addView(new HdrOnclickListener(this,1,"Commercial Type II","Commercial Wind","Terrain Exposure",3,1,cf));
          }
          else if(cf.identityval==34)
          {
        	  hdr_layout.addView(new HdrOnclickListener(this,1,"Commercial Type III","Commercial Wind","Terrain Exposure",3,1,cf));
          }
          cf.mainmenu_layout = (LinearLayout) findViewById(R.id.header); //** HEADER MENU **//
          cf.mainmenu_layout.setMinimumWidth(cf.wd);
          cf.mainmenu_layout.addView(new MyOnclickListener(this, 3, 0,0, cf));
         
          cf.type_submenulayout =(LinearLayout) findViewById(R.id.typesubmenu); //** WINDMIT TYPE SUBMENU **//
          cf.type_submenulayout.addView(new MyOnclickListener(this, 300, 1,0, cf));
          cf.windmittypechk(cf.identityval,((RadioButton)findViewById(R.id.b1802_chk)),((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((TextView)findViewById(R.id.commtxt)),((ImageView)findViewById(R.id.editbno))); // To check the checkbox of choosen inspection type
          cf.uncheckcommercial(((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((RadioButton)findViewById(R.id.b1802_chk))); 
      	 
          cf.submenu_layout = (LinearLayout) findViewById(R.id.submenu);//** WINDMIT TYPE SUBMENU **//
          cf.submenu_layout.addView(new MyOnclickListener(this,cf.identityval,1,314, cf));
         
          cf.innersubmenu_layout = (LinearLayout) findViewById(R.id.innersubmenu);//** WINDMIT TYPE SUBMENU **//
          cf.innersubmenu_layout.addView(new MyOnclickListener(this,cf.identityval,1,123, cf));
          
          cf.mainlayout = (LinearLayout)findViewById(R.id.bc_table);  
          cf.save = (Button)findViewById(R.id.save);
          cf.tblrw = (TableRow)findViewById(R.id.row2);
          cf.tblrw.setMinimumHeight(cf.ht);
          
         /* try
          {
        	  cf.getPolicyholderInformation(cf.selectedhomeid);
				if(!cf.CommercialFlag.equals("0"))
				{
					if(cf.Stories.equals("4") || cf.Stories.equals("5")  || cf.Stories.equals("6"))
					{
						cf.getbuildinginfo(2,33,"Commercial Type II");
					} 
					else if(Integer.parseInt(cf.Stories)>=7)
					{
						cf.getbuildinginfo(3,34,"Commercial Type III");
					}
				}
          }
          catch(Exception e)
          {
        	  System.out.println("EE="+e.getMessage());
          }*/
          
          Declaration();
          cf.CreateARRTable(3);
          cf.getInspectorId();
          cf.CreateARRTable(40);setValue();
          
        
    }
	private void setValue() {
		// TODO Auto-generated method stub
		try
		{
			selectbl();
			Cursor c5 = cf.SelectTablefunction(cf.policyholder,	"where ARR_PH_SRID='" + cf.selectedhomeid + "'");
			if(c5.getCount()>0)
			{
			c5.moveToFirst();
			String country = cf.decode(c5.getString(c5.getColumnIndex("ARR_PH_County")));
			if (country.toLowerCase().equals("miami-dade") 	|| country.toLowerCase().equals("broward")) 
			{
				if(tervalue.equals(""))
				{
					((RadioButton) terrainexpordgpid.findViewWithTag("Unknown")).setChecked(true);	
					((RadioButton) windspeeedrdgpid.findViewWithTag("C")).setChecked(true);	
					((RadioButton) winddesignrdgpid.findViewWithTag("C")).setChecked(true);
					((TextView)findViewById(R.id.savecommwind1)).setVisibility(cf.v1.GONE);
					((TextView)findViewById(R.id.savecommwind2)).setVisibility(cf.v1.GONE);
				}
				else
				{
					((RadioButton) terrainexpordgpid.findViewWithTag(terrexpo)).setChecked(true);	
					((RadioButton) windspeeedrdgpid.findViewWithTag(windspeed)).setChecked(true);	
					((RadioButton) winddesignrdgpid.findViewWithTag(winddesign)).setChecked(true);
					((TextView)findViewById(R.id.savecommwind1)).setVisibility(cf.v1.VISIBLE);
					((TextView)findViewById(R.id.savecommwind2)).setVisibility(cf.v1.VISIBLE);
				}
			}
			else
			{
				if(tervalue.equals(""))
				{
					
					((RadioButton) terrainexpordgpid.findViewWithTag("Exposure B")).setChecked(true);
					((TextView)findViewById(R.id.savecommwind1)).setVisibility(cf.v1.GONE);
					((TextView)findViewById(R.id.savecommwind2)).setVisibility(cf.v1.GONE);
				}
				else
				{
					
					((RadioButton) terrainexpordgpid.findViewWithTag(terrexpo)).setChecked(true);	
					((RadioButton) windspeeedrdgpid.findViewWithTag(windspeed)).setChecked(true);	
					((RadioButton) winddesignrdgpid.findViewWithTag(winddesign)).setChecked(true);
					((TextView)findViewById(R.id.savecommwind1)).setVisibility(cf.v1.VISIBLE);
					((TextView)findViewById(R.id.savecommwind2)).setVisibility(cf.v1.VISIBLE);
				}
			}
			}
			else
			{
				
			}
		}
	    catch(Exception e)
		{
			System.out.println("catch 1"+e.getMessage());
		}
	}
	private void selectbl()
	{
		c1 = cf.arr_db.rawQuery("SELECT * FROM " + cf.Commercial_Wind + " WHERE fld_srid='"+ cf.selectedhomeid + "'  and insp_typeid='"+cf.identityval+"'", null);				
		if(c1.getCount()>0)
		{
			c1.moveToFirst();
			tervalue = c1.getString(c1.getColumnIndex("fld_terrain"));
			if(!tervalue.equals(""))
			{
					String terrainaplit[] = tervalue.split("~");
					terrexpo = terrainaplit[0];
					windspeed = terrainaplit[1];
					winddesign = terrainaplit[2];
			}
		}
	}
	private void Declaration() {
		// TODO Auto-generated method stub
		terrainexpordgpid = (RadioGroup)findViewById(R.id.terrainrdgrp); terrainexpordgpid.setOnCheckedChangeListener(new checklistenetr(1));
		windspeeedrdgpid = (RadioGroup)findViewById(R.id.windspeedrdgp); windspeeedrdgpid.setOnCheckedChangeListener(new checklistenetr(2));
		winddesignrdgpid = (RadioGroup)findViewById(R.id.winddesignrdgp); winddesignrdgpid.setOnCheckedChangeListener(new checklistenetr(3));
		
		((TextView)findViewById(R.id.title1)).setText(Html.fromHtml("<b>TERRAIN EXPOSURE CATEGORY</b>"+" must be provided for each insured location."));
		((TextView)findViewById(R.id.title2)).setText(Html.fromHtml("<b>CERTIFICATION OF WIND SPEED</b>"+" is required to establish the basic wind speed of the location (Complete for Terrain B only if Year Built On or After Jan. 1, 2002)."));
		((TextView)findViewById(R.id.title3)).setText(Html.fromHtml("<b>CERTIFICATION OF WIND DESIGN</b>"+"  is required when the building is constructed in a manner to exceed the basic wind speed design established for the structure location(complete or Terrain B only if Year Build On or After Jan. 1, 2002)."));
	
		((TextView)findViewById(R.id.terrainheader1)).setText(Html
				.fromHtml("<font color=black>I, "
						+ "</font>"
						+ "<b><font color=#bddb00>"
						+ cf.Insp_firstname.toUpperCase()+" "+cf.Insp_lastname.toUpperCase()
						+ "</font></b><font color=black> hereby certify that the building or unit at the address indicated above TERRAIN EXPOSURE CATEGORY as defined under the Florida Building Code is (Check One)."
						+ "</font>"));

		((TextView)findViewById(R.id.terrainheader2)).setText(Html
				.fromHtml("<font color=black>I, "
						+ "</font>"
						+ "<b><font color=#bddb00>"
						+ cf.Insp_firstname.toUpperCase()+" "+cf.Insp_lastname.toUpperCase()
						+ "</font></b><font color=black> hereby certify that the basic WIND SPEED of the building or unit at the address indicated above based upon county wind speed lines defined under the Florida Building Code (FBC) is (Check One)."
						+ "</font>"));
		

		((TextView)findViewById(R.id.terrainheader3)).setText(Html
				.fromHtml("<font color=black>I, "
						+ "</font>"
						+ "<b><font color=#bddb00>"
						+ cf.Insp_firstname.toUpperCase()+" "+cf.Insp_lastname.toUpperCase()
						+ "</font></b><font color=black> I hereby certify that the building or unit at the address indicated above is designed and mitigated to the Florida Building ode (FBC)WIND DESIGN of (Check One)."
						+ "</font>"));
		
		
	}
	class checklistenetr implements OnCheckedChangeListener
	{
    	int i;
		public checklistenetr(int i) {
			// TODO Auto-generated constructor stub
		 this.i=i;
		}

		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			   RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
		        // This puts the value (true/false) into the variable
		        boolean isChecked = checkedRadioButton.isChecked();
		        // If the radiobutton that has changed in check state is now checked...
		        if (isChecked)
		        {
		          switch (i) {
					case 1:
						terrainvalue= checkedRadioButton.getTag().toString().trim();						
					break;
					case 2:
						wnidspeedvalue= checkedRadioButton.getTag().toString().trim();						
					break;
					case 3:
						wniddesignvalue= checkedRadioButton.getTag().toString().trim();						
					break;
		          }
		        }
		}
	}
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.save:
			if(!terrainvalue.equals(""))
			{
				if(!wnidspeedvalue.equals(""))
				{
					if(!wniddesignvalue.equals(""))
					{
						
						InsertTerrain();
					}
					else
					{
						cf.ShowToast("Please select the option for Certification of Wind Design.", 1);
					}					
				}
				else
				{
					cf.ShowToast("Please select the option for Certification of Wind Speed.", 1);
				}
			}
			else
			{
				cf.ShowToast("Please select the option for Terrain Exposure Category.", 1);
			}
			break;
		}
	}
	private void InsertTerrain() {
		// TODO Auto-generated method stub
		terrain = terrainvalue+"~"+wnidspeedvalue+"~"+wniddesignvalue;
		try
		{
			Cursor c1 =cf.SelectTablefunction(cf.Commercial_Wind, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
			if(c1.getCount()==0)
			{
				cf.arr_db.execSQL("INSERT INTO "
						+ cf.Commercial_Wind
						+ " (fld_srid,insp_typeid,fld_terrain)"
						+ "VALUES ('"+cf.selectedhomeid+"','"+cf.identityval+"','"+terrain+"')");
			}
			else
			{
				cf.arr_db.execSQL("UPDATE "
					+ cf.Commercial_Wind
					+ " SET fld_terrain='" + terrain + "'"
					+ " WHERE fld_srid ='"+ cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'");			
			
			}
			cf.ShowToast("Terrain Exposure saved successfully",1);
			cf.gotoclass(cf.identityval, CommercialRC.class);
		}
		catch(Exception e)
		{
			System.out.println("catch inside "+e.getMessage());
		}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
 		// replaces the default 'Back' button action
 		if (keyCode == KeyEvent.KEYCODE_BACK) {
 			cf.isaccess= cf.chk_InspTypeQuery(cf.selectedhomeid,"Roof Survey");
				if(cf.isaccess==true)
				{
				    cf.Roof=cf.Roof_insp[1];
		 			cf.goback(2);
		 		}
				else
				{
					cf.goback(0);
				} 			
 			return true;
 		}
 		
 		return super.onKeyDown(keyCode, event);
 	}
}