/*
 ************************************************************
 * Project: ALL RISK REPORT
 * Module : Roof Section.java
 * Creation History:
    Created By : Rakki s on 10/01/2012
 * Modification History:
    Last Modified By : Rakki S on 02/12/2013
 ************************************************************  
*/
package idsoft.inspectiondepot.ARR;
import idsoft.inspectiondepot.ARR.R;

import java.io.IOException;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;
import java.util.concurrent.TimeoutException;

import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class RoofSection extends Activity implements Runnable {
	CommonFunctions cf;
	/***Roof page static content Declaration starts **/
	int DATE_DIALOG_ID=24;
	public Button GCH_RC_RCT_SA,GCH_RC_RCT_can;
	private int mYear,mMonth,mDay;
	TableLayout RCT_ShowValue,RCN_ShowValue,RCTD_Val;
	int Current_select_id=0;
	int Per_in_DB=0;
	int focus=0;
	int colomprerow=6;
	boolean load_comment=true;
    EditText RC_cmt,ADD_com2_RMS_HVAC_ed,ADD_com2_RMS_VE_ed,ADD_com2_RMS_SE_ed,ADD_com2_RMS_OE_ed,ADD_com2_Other,ADD_com2_PDU_no_ed,ADD_com2_PDU_date_ed;
    //ADD_yrs,ADD_AR_Other,ADD_DLU_ed,
	TextView RC_cmt_tv,RC_Add_cmt_tv;
	ImageView ADD_expend,RCN_expend;
	//String[] survay={"-Select-","Exterior From Ground Only","Parts Not Visible Due to Lot Configuration","Attic Not Inspected for Leakage/Damage","Roof Not Walked"};
	String[] Add_sp1={"-Select-","Full Replacement","Partial Replacement","Not Determined"};
	String[] Add_sp2={"-Select-","Yes","No","Not Determined"};
	Spinner sp1,sp2;//,ADD_AR_sp;
	RadioGroup ADD_RG1,ADD_com2_RG,ADD_com2_PDU_RG,ADD_com2_RME_RG;//ADD_AR_RG,ADD_AU_RG,
	Boolean b1=false;
	Button RC_AMC;//,ADD_date_pck;
	TableRow RCN_row[];//,RC_Add_Y_N;//RC_Add_limit;
	CheckBox[] ADD_com2_RMS,RC_RSurvay_chk;//Add_Limit,Add_Y_N,
	CheckBox ADD_chk_top,RCN_chk_top;//ADD_DLU_RD,
	LinearLayout ADD_layout_head,RCN_layout_head,RCT_content;
    
	
	String Roof="",str="";
	String[] def_tit;
	int tbl_id[],RCN_id[],RCT_id[];
	RelativeLayout RCT_head,ADD_head,RCn_head;
	String Save_from="",skipped_insp="",startfrom="";
	String Roof_page_value[]=new String[6];
	Button citi_cer;
	CheckBox citi_inclu;
	private String[] Roof_additional_txt;
	private ProgressDialog pd;
	private int show_handler;
	/***Cutomized values condrols id for comercial ***/
	int amt[]={-1,-1},everReplaced[]={-1,-1},hasroof[]={-1,-1};
	
	String[] amt_tit={"Is there a specific fund in place that is earmarked for roof replacement?","Amount fund"},
	everReplaced_tit={"Roof ponding noted","Has roof ever been replaced?"},has_roof={"Has the roof mounted structure been adequately installed to prevent water seepage into the structure?","Flashing/Coping"};
	
	String com_roof_yesspl="Yes-BVCN";
	String pre_domtit="Predominant",
	no_perdomin_tit="No Permit/Verification Documents",
	Roof_shap_tit="Roof Shape (%)",
	permit_dat_tit="Permit Application Date",
	Image_tit="Photo";
	String buildingcode_tit="Building Code";
	String year_ins="Year of Insallation";
	String over_all="Overall Condition of Roof",upload_tit="Upload Photo",approx="Approximate Remaining Functional Life";
	
	String[] over_all_option={"Excellent","Good"};
	String check_alert_rcn="",check_alert_add="";
	int over_all_id=-1;
	/***Cutomized values condrols id  for comercial ends ***/
	/***Roof page  Declaration Ends **/
	int update=0;
	String submenu=""; 
	public int pick_img_code=24;
	EditText RMS_loc[]=new EditText[3];
	RadioGroup RMS_RG[]=new RadioGroup[3];
	Cursor RCN_c;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		 Bundle extras = getIntent().getExtras();
		 cf=new CommonFunctions(this);
		 if (extras != null) {
		
				cf.selectedhomeid = extras.getString("homeid");
				cf.onlinspectionid = extras.getString("InspectionType");				
				   
			    cf.onlstatus = extras.getString("status");
			    cf.Roof=Roof=extras.getString("Roof");
			    
			    if(Roof.equals(cf.getResourcesvalue(R.string.insp_4)) || Roof.equals(cf.getResourcesvalue(R.string.insp_5)) || Roof.equals(cf.getResourcesvalue(R.string.insp_6)))
			    {
			    	cf.identityval = extras.getInt("identity");
			    }
			    Save_from = extras.getString("Roof_save_from");
			    
			    startfrom = extras.getString("Roof_start_from");
			    
			    if(startfrom==null)
			    {
			    	startfrom="";
			    }
			    skipped_insp=extras.getString("Roof_skiiped_insp");
			    submenu=extras.getString("for_loading");
			    if(submenu==null)
				{	
					submenu="";
				}
				if(Save_from==null)
				{
					Save_from=Roof;
					skipped_insp="";
				}
				
		       
	    	}
		
		 cf.getPolicyholderInformation(cf.selectedhomeid);
		 setContentView(R.layout.roof_section);
		 cf.getInspectorId();
		 cf.getDeviceDimensions();
		 cf.CreateARRTable(10);
		 cf.CreateARRTable(99);
		 cf.CreateARRTable(431);
		 cf.CreateARRTable(43);
		 cf.CreateARRTable(432);
		 cf.CreateARRTable(433);
		 cf.CreateARRTable(434);
		 custom_creat();
		 //if()
		
		
	}

	public void custom_creat()
	{
		 cf.loadinsp_n=Roof;
		 Roof_page_value=getRoofValueForVariables(Roof);
		 if(Roof.equals(cf.Roof_insp[0]))
		 {
			 if(Save_from.equals(Roof) || submenu.equals("true")||startfrom.equals(""))
			 {
				    LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
					hdr_layout.addView(new HdrOnclickListener(this,1,cf.All_insp_list[1],"Roof System",2,0,cf));
		         	LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
		         	main_layout.setMinimumWidth(cf.wd-20);
		         	main_layout.addView(new MyOnclickListener(getApplicationContext(), 2, 0,0,cf));
				 
			        LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
				    submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 21, 1,0,cf));
				
			 }
			 else
			 {
				 ((LinearLayout) findViewById(R.id.Heading_li)).setVisibility(View.VISIBLE);
				 ((RelativeLayout) findViewById(R.id.savenext_li)).setVisibility(View.GONE);
				 ((RelativeLayout) findViewById(R.id.saveskip_li)).setVisibility(View.VISIBLE);
				 ((TextView) findViewById(R.id.Heading)).setText(Roof); 
				 ((RelativeLayout) findViewById(R.id.LinearLayout01)).setPadding(0, 10, 0, 0); 
				 
			 }
		 }
		 else if(Roof.equals(cf.Roof_insp[1]))
		 {
			
			 if(Save_from.equals(Roof)|| submenu.equals("true")||startfrom.equals(""))
			 {
				    LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
					hdr_layout.addView(new HdrOnclickListener(this,1,cf.All_insp_list[2],"",29,2,cf));
		         	LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
		         	main_layout.setMinimumWidth(cf.wd);
		         	main_layout.addView(new MyOnclickListener(getApplicationContext(), 29, 0,0,cf));
				
		       ((Button) findViewById(R.id.GCH_RC_savebtn)).setText("Save");
		       ((LinearLayout) findViewById(R.id.contentrel)).setBackgroundColor(0);
		       LinearLayout.LayoutParams lp =new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
		       lp.setMargins(0, 0, 0, 0);
		       ((LinearLayout) findViewById(R.id.insidecontentrel)).setLayoutParams(lp);
		       ((LinearLayout) findViewById(R.id.Submenu_layout)).setLayoutParams(lp);
		       ((LinearLayout) findViewById(R.id.Submenu_layout)).setPadding(0, 0, 0,0);
		       ((LinearLayout) findViewById(R.id.typesubmenu)).setPadding(0, 0, 0,0);
			 }
			 else
			 {
				 ((LinearLayout) findViewById(R.id.Heading_li)).setVisibility(View.VISIBLE);
				 ((RelativeLayout) findViewById(R.id.savenext_li)).setVisibility(View.GONE);
				 ((RelativeLayout) findViewById(R.id.saveskip_li)).setVisibility(View.VISIBLE);
				 ((TextView) findViewById(R.id.Heading)).setText(Roof);
				 ((RelativeLayout) findViewById(R.id.LinearLayout01)).setPadding(0, 10, 0, 0);
			 }
		 }
		 else  if(Roof.equals(cf.Roof_insp[4]))
		 {
				   
		 /*** For GCH***/
			
			 
			 if(Save_from.equals(Roof)|| submenu.equals("true")||startfrom.equals(""))
			 {
				    LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
					hdr_layout.addView(new HdrOnclickListener(this,1,cf.All_insp_list[7],"Roof System",4,0,cf));
		         	LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
		         	main_layout.setMinimumWidth(cf.wd-10);
		         	main_layout.addView(new MyOnclickListener(getApplicationContext(), 4, 0,0,cf));
		         	
			 	    LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
				    submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 43, 1,0,cf));
			 }
			 else
			 {
				 ((LinearLayout) findViewById(R.id.Heading_li)).setVisibility(View.VISIBLE);
				 ((RelativeLayout) findViewById(R.id.savenext_li)).setVisibility(View.GONE);
				 ((RelativeLayout) findViewById(R.id.saveskip_li)).setVisibility(View.VISIBLE);
				 ((TextView) findViewById(R.id.Heading)).setText(Roof);
				 ((RelativeLayout) findViewById(R.id.LinearLayout01)).setPadding(0, 10, 0, 0);
			 }
		/***	 For GCH Ends***/
		 }
		 else if(Roof.equals(cf.getResourcesvalue(R.string.insp_5)))
		 {
			
			/*** commercial type 2***/
			
			 if(Save_from.equals(Roof)|| submenu.equals("true")||startfrom.equals(""))
			 {
				 if(cf.identityval==33 || cf.identityval==34){ cf.typeidentity=313;}
		          
				    LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
				   hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","Commercial Type II","Roof System",3,1,cf));
			       
			       
		         	LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
		         	main_layout.setMinimumWidth(cf.wd-10);
		         	main_layout.addView(new MyOnclickListener(getApplicationContext(), 3, 0,0,cf));
		         	
		           cf.mainlayout= (LinearLayout) findViewById(R.id.insidecontentrel);
      	        
			        
					
		          cf.type_submenulayout =(LinearLayout) findViewById(R.id.typesubmenu); //** WINDMIT TYPE SUBMENU **//
		          cf.type_submenulayout.addView(new MyOnclickListener(this, 300, 1,0, cf));
		          cf.windmittypechk(cf.identityval,((RadioButton)findViewById(R.id.b1802_chk)),((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((TextView)findViewById(R.id.commtxt)),((ImageView)findViewById(R.id.editbno))); // To check the checkbox of choosen inspection type
		          cf.uncheckcommercial(((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((RadioButton)findViewById(R.id.b1802_chk))); 
		      	   
		          cf.submenu_layout = (LinearLayout) findViewById(R.id.submenu);//** WINDMIT TYPE SUBMENU **//
		          cf.submenu_layout .addView(new MyOnclickListener(this,cf.identityval,1,cf.typeidentity, cf));
		          
		         
		          cf.tblrw = (TableRow)findViewById(R.id.row2);
		          cf.tblrw.setMinimumHeight(cf.ht);	
		          cf.save = (Button)findViewById(R.id.GCH_RC_savebtn);
			 }
			 else
			 {
				 ((LinearLayout) findViewById(R.id.Heading_li)).setVisibility(View.VISIBLE);
				 ((RelativeLayout) findViewById(R.id.savenext_li)).setVisibility(View.GONE);
				 ((RelativeLayout) findViewById(R.id.saveskip_li)).setVisibility(View.VISIBLE);
				 ((TextView) findViewById(R.id.Heading)).setText(Roof);
				 ((RelativeLayout) findViewById(R.id.LinearLayout01)).setPadding(0, 10, 0, 0);
			 }
			 findViewById(R.id.Add_help).setVisibility(View.VISIBLE);
			 findViewById(R.id.RCN_help).setVisibility(View.VISIBLE);
			 /****commercial type 2 ends**/
		 } 
		 else if(Roof.equals(cf.getResourcesvalue(R.string.insp_6)))
		 {
			 /****commercial type 3 satrts**/
			
			 if(Save_from.equals(Roof) || submenu.equals("true")||startfrom.equals(""))
			 {
				 if(cf.identityval==33 || cf.identityval==34){ cf.typeidentity=313;}
	          
				    LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
				     hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","Commercial Type III","Roof System",3,1,cf));
			        
					LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
		         	main_layout.setMinimumWidth(cf.wd-10);
		         	main_layout.addView(new MyOnclickListener(getApplicationContext(), 3, 0,0,cf));
		         
		          cf.type_submenulayout =(LinearLayout) findViewById(R.id.typesubmenu); //** WINDMIT TYPE SUBMENU **//
		          cf.type_submenulayout.addView(new MyOnclickListener(this, 300, 1,0, cf));
		          cf.windmittypechk(cf.identityval,((RadioButton)findViewById(R.id.b1802_chk)),((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((TextView)findViewById(R.id.commtxt)),((ImageView)findViewById(R.id.editbno))); // To check the checkbox of choosen inspection type
		          cf.uncheckcommercial(((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((RadioButton)findViewById(R.id.b1802_chk))); 
		      	   
		  		 cf.mainlayout= (LinearLayout) findViewById(R.id.insidecontentrel);
		          
		          cf.submenu_layout = (LinearLayout) findViewById(R.id.submenu);//** WINDMIT TYPE SUBMENU **//
		          cf.submenu_layout .addView(new MyOnclickListener(this,cf.identityval,1,cf.typeidentity, cf));
		          
		          
		          cf.tblrw = (TableRow)findViewById(R.id.row2);
		          cf.tblrw.setMinimumHeight(cf.ht);
		          cf.save = (Button)findViewById(R.id.GCH_RC_savebtn);
			 }
			 else
			 {
				 ((LinearLayout) findViewById(R.id.Heading_li)).setVisibility(View.VISIBLE);
				 ((RelativeLayout) findViewById(R.id.savenext_li)).setVisibility(View.GONE);
				 ((RelativeLayout) findViewById(R.id.saveskip_li)).setVisibility(View.VISIBLE);
				 ((TextView) findViewById(R.id.Heading)).setText(Roof);
				 ((RelativeLayout) findViewById(R.id.LinearLayout01)).setPadding(0, 10, 0, 0);
			 }
			 /****Roof Survay ends**/
			 findViewById(R.id.Add_help).setVisibility(View.VISIBLE);
			 findViewById(R.id.RCN_help).setVisibility(View.VISIBLE);
		 } 
		 else if(Roof.equals(cf.getResourcesvalue(R.string.insp_4)))
		 {
			System.out.println("Inside");
			 /****commercial type 1 stats**/
			 if(Save_from.equals(Roof)|| submenu.equals("true")||startfrom.equals(""))
			 {
				 System.out.println("cf.identityval"+cf.identityval);
				 if(cf.identityval==32 || cf.identityval==33 || cf.identityval==34){ cf.typeidentity=313;}
	          
				    LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
				    hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","Commercial Type I","Roof System",3,1,cf));
		
					LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
		         	main_layout.setMinimumWidth(cf.wd-10);
		         	main_layout.addView(new MyOnclickListener(getApplicationContext(), 3, 0,0,cf));
		          cf.mainlayout= (LinearLayout) findViewById(R.id.insidecontentrel);
		          
		          cf.type_submenulayout =(LinearLayout) findViewById(R.id.typesubmenu); //** WINDMIT TYPE SUBMENU **//
		          cf.type_submenulayout.addView(new MyOnclickListener(this, 300, 1,0, cf));
		          cf.windmittypechk(cf.identityval,((RadioButton)findViewById(R.id.b1802_chk)),((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((TextView)findViewById(R.id.commtxt)),((ImageView)findViewById(R.id.editbno))); // To check the checkbox of choosen inspection type
		          cf.uncheckcommercial(((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((RadioButton)findViewById(R.id.b1802_chk))); 
		      	  
		          cf.submenu_layout = (LinearLayout) findViewById(R.id.submenu);//** WINDMIT TYPE SUBMENU **//
		          cf.submenu_layout .addView(new MyOnclickListener(this,cf.identityval,1,cf.typeidentity, cf));
		          
		       
		          cf.tblrw = (TableRow)findViewById(R.id.row2);
		          cf.tblrw.setMinimumHeight(cf.ht);
		          cf.save = (Button)findViewById(R.id.GCH_RC_savebtn);
			 }
			 else
			 {
				 ((LinearLayout) findViewById(R.id.Heading_li)).setVisibility(View.VISIBLE);
				 ((RelativeLayout) findViewById(R.id.savenext_li)).setVisibility(View.GONE);
				 ((RelativeLayout) findViewById(R.id.saveskip_li)).setVisibility(View.VISIBLE);
				 ((TextView) findViewById(R.id.Heading)).setText(Roof);
				 ((RelativeLayout) findViewById(R.id.LinearLayout01)).setPadding(0, 10, 0, 0);
			 }
			 /****commercial type 1 ends**/
		 } 
       
			
			TableRow tblrw = (TableRow)findViewById(R.id.row2);
			tblrw.setMinimumHeight(cf.ht);
			final Calendar cal = Calendar.getInstance();
			mYear = cal.get(Calendar.YEAR);
			mMonth = cal.get(Calendar.MONTH);
			mDay = cal.get(Calendar.DAY_OF_MONTH);
			this.setTheme(R.style.AppTheme);
			focus=1;
			
			Set_RC_value();	
			cf.setupUI(((View)findViewById(R.id.contentrel)));
	}
	
	
	private void Set_RC_value() {
		// TODO Auto-generated method stub
		
	 	
	 	GCH_RC_RCT_SA =(Button) findViewById(R.id.GCH_RC_RCT_SA);
	 	GCH_RC_RCT_SA.setOnClickListener(new RC_clicker());
	 	
	 	
	 	GCH_RC_RCT_can =(Button) findViewById(R.id.GCH_RC_RCT_can);
	 	GCH_RC_RCT_can.setOnClickListener(new RC_clicker());
		
	 	
	 	RCT_ShowValue= (TableLayout) findViewById(R.id.RC_RCT_ShowValue);
	 	
	 	/***declaring the General Roof information ***/
	  	RC_cmt_tv = (TextView) findViewById(R.id.GCH_RC_ED_txt_Comments);
	  	RC_cmt=(EditText) findViewById(R.id.GCH_RC_ED_Li_Comments);
	 	RC_cmt.setOnTouchListener(new RC_editclick());
	 	RC_cmt.addTextChangedListener(new GCH_RC_watcher(RC_cmt_tv,"500"));
	 	RCN_ShowValue= (TableLayout) findViewById(R.id.RCN_Condition_N_tbl);
	 	RC_AMC=(Button) findViewById(R.id.RC_AMC);
	 	RC_AMC.setOnClickListener(new RC_clicker());
	 	RCTD_Val = (TableLayout) findViewById(R.id.RCT_Table);
	   /***declaring the General Roof information ends ***/
	   	ADD_RG1 =(RadioGroup) findViewById(R.id.ADD_RG);
		 RC_RSurvay_chk=new CheckBox[6];
		 RC_RSurvay_chk[0]=(CheckBox) findViewById(R.id.RC_RSurvay_chk1);
		 RC_RSurvay_chk[1]=(CheckBox) findViewById(R.id.RC_RSurvay_chk2);
		 RC_RSurvay_chk[2]=(CheckBox) findViewById(R.id.RC_RSurvay_chk3);
		 RC_RSurvay_chk[3]=(CheckBox) findViewById(R.id.RC_RSurvay_chk4);
		 RC_RSurvay_chk[4]=(CheckBox) findViewById(R.id.RC_RSurvay_chk5);
		 RC_RSurvay_chk[5]=(CheckBox) findViewById(R.id.RC_RSurvay_chk6);
		 citi_cer=(Button) findViewById(R.id.sampleCitizenCertificate);
		 citi_inclu=(CheckBox) findViewById(R.id.includeCitizenForm);
		 	/** For  Commercial type  **/
	 	ADD_com2_RG =(RadioGroup) findViewById(R.id.ADD_com2_RG);
	 	ADD_com2_PDU_RG=(RadioGroup) findViewById(R.id.ADD_com2_PDU_RG);
	 	Button datepck= (Button) findViewById(R.id.ADD_com2_RG1_yes_bt1);
	 	ADD_com2_PDU_date_ed=(EditText) findViewById(R.id.ADD_com2_RG1_yes_ed2);
	 	ADD_com2_PDU_no_ed=(EditText) findViewById(R.id.ADD_com2_RG1_yes_ed1);
	 	ADD_com2_RMS=new CheckBox[8];
	 	ADD_com2_RMS[0]=(CheckBox)findViewById(R.id.ADD_RMS_chk1);
	 	ADD_com2_RMS[1]=(CheckBox)findViewById(R.id.ADD_RMS_chk2);
	 	ADD_com2_RMS[2]=(CheckBox)findViewById(R.id.ADD_RMS_chk3);
	 	ADD_com2_RMS[3]=(CheckBox)findViewById(R.id.ADD_RMS_chk4);
	 	ADD_com2_RMS[4]=(CheckBox)findViewById(R.id.ADD_RMS_chk5);
	 	ADD_com2_RMS[5]=(CheckBox)findViewById(R.id.ADD_RMS_chk6);
	 	ADD_com2_RMS[6]=(CheckBox)findViewById(R.id.ADD_RMS_chk7);
	 	ADD_com2_RMS[7]=(CheckBox)findViewById(R.id.ADD_RMS_chk8);
	 	
	 	
	 	ADD_com2_RMS_HVAC_ed=(EditText) findViewById(R.id.ADD_RMS_No1);
	 	
	 	ADD_com2_RMS_VE_ed=(EditText) findViewById(R.id.ADD_RMS_No2);
	 	
	 	ADD_com2_RMS_SE_ed=(EditText) findViewById(R.id.ADD_RMS_No3);
	 	ADD_com2_RMS_OE_ed=(EditText) findViewById(R.id.ADD_RMS_No4);
	 	
	 	RMS_loc[0]=(EditText) findViewById(R.id.ADD_RMS_loc1);
	 	RMS_loc[1]=(EditText) findViewById(R.id.ADD_RMS_loc2);
	 	RMS_loc[2]=(EditText) findViewById(R.id.ADD_RMS_loc3);
	 	
	 	RMS_RG[0]=(RadioGroup) findViewById(R.id.RMS_RG1);
	 	RMS_RG[1]=(RadioGroup) findViewById(R.id.RMS_RG2);
	 	String s="Invalid Entry! Please enter valid number of ";
	 	ADD_com2_RMS_HVAC_ed.addTextChangedListener(new text_watch_number(s+cf.getResourcesvalue(R.string.hvacE), ADD_com2_RMS_HVAC_ed));
	 	ADD_com2_RMS_VE_ed.addTextChangedListener(new text_watch_number(s+cf.getResourcesvalue(R.string.VentalationE), ADD_com2_RMS_VE_ed));
	 	ADD_com2_RMS_SE_ed.addTextChangedListener(new text_watch_number(s+cf.getResourcesvalue(R.string.StorageE), ADD_com2_RMS_SE_ed));
	 	
	 	ADD_com2_RME_RG=(RadioGroup) findViewById(R.id.ADD_com2_RME_RG);
	 	ADD_com2_RMS[0].setOnClickListener(new setVisibleOnChecked(findViewById(R.id.RMS_chk1_details)));
	 	ADD_com2_RMS[1].setOnClickListener(new setVisibleOnChecked(findViewById(R.id.RMS_chk2_details)));
	 	ADD_com2_RMS[2].setOnClickListener(new setVisibleOnChecked(findViewById(R.id.RMS_chk3_details)));
	 	ADD_com2_RMS[7].setOnClickListener(new setVisibleOnChecked(findViewById(R.id.RMS_chk8_details)));
	 	
	 	ADD_com2_RME_RG.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				try
				{
					if(group.findViewById(checkedId).getTag().equals(getResources().getString(R.string.yes)))
					{
						(findViewById(R.id.ADD_com3_equ_rw)).setVisibility(View.VISIBLE);
						(findViewById(R.id.ADD_com3_equ_rw2)).setVisibility(View.VISIBLE);
						(findViewById(R.id.ADD_com3_equ_rw3)).setVisibility(View.VISIBLE);
						
					}
					else
					{
						clearValueofgroub(((TableRow)findViewById(R.id.ADD_com3_equ_rw)));
						clearValueofgroub(((TableRow)findViewById(R.id.ADD_com3_equ_rw2)));
						clearValueofgroub(((TableRow)findViewById(R.id.ADD_com3_equ_rw3)));
						//Clear_eqp();
						(findViewById(R.id.ADD_com3_equ_rw)).setVisibility(View.GONE);
						(findViewById(R.id.ADD_com3_equ_rw2)).setVisibility(View.GONE);
						(findViewById(R.id.ADD_com3_equ_rw3)).setVisibility(View.GONE);
						(findViewById(R.id.RMS_chk1_details)).setVisibility(View.INVISIBLE);
						(findViewById(R.id.RMS_chk2_details)).setVisibility(View.INVISIBLE);
						(findViewById(R.id.RMS_chk3_details)).setVisibility(View.INVISIBLE);
						(findViewById(R.id.RMS_chk8_details)).setVisibility(View.INVISIBLE);
					
					}
				}catch (Exception e) {
					// TODO: handle exception
				}
			}

		
		});
	 	ADD_com2_Other=(EditText) findViewById(R.id.ADD_com2_Other);
	 	final TextView Add_com2_other_lim=(TextView) findViewById(R.id.ADD_other_limit);
	 	ADD_com2_Other.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				try
				{
					cf.showing_limit(s.toString(),Add_com2_other_lim , 250);
				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
	 	ADD_com2_PDU_date_ed.setEnabled(false);
	 	ADD_com2_PDU_date_ed.setTag("Date");
	 	datepck.setOnClickListener(new datepick(((View) findViewById(R.id.ADD_PDU_sub)).getId()));
	 	ADD_com2_PDU_RG.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				try
				{
					if(findViewById(checkedId).getTag().toString().equals("Yes"))
					{
						ADD_com2_PDU_date_ed.setText("");
						ADD_com2_PDU_no_ed.setText("");
						((View) findViewById(R.id.ADD_PDU_sub)).setVisibility(View.VISIBLE);
						
					}
					else
					{
						((View) findViewById(R.id.ADD_PDU_sub)).setVisibility(View.GONE);
					}
				}
				catch (Exception e) {
					// TODO: handle exception
				}
			}
		});
	 	if(Roof_page_value[5].equals("1"))
	 	{
	 		((RelativeLayout)findViewById(R.id.RC_additional_main)).setVisibility(View.VISIBLE);
	 		((RelativeLayout)findViewById(R.id.RC_survay_head)).setVisibility(View.VISIBLE);
	 		//((View)findViewById(R.id.RC_Survey_Additional)).setVisibility(View.GONE);
	 		((View)findViewById(R.id.RC_Commercial_Additional)).setVisibility(View.GONE);
	 		
	 		
	 	}
	 	else if(Roof_page_value[5].equals("2"))
	 	{
			((RelativeLayout)findViewById(R.id.RC_additional_main)).setVisibility(View.VISIBLE);
			((View)findViewById(R.id.RC_Commercial_Additional)).setVisibility(View.GONE);
			((RelativeLayout)findViewById(R.id.RC_survay_head)).setVisibility(View.VISIBLE);
			citi_cer.setVisibility(View.VISIBLE);
			citi_inclu.setVisibility(View.VISIBLE);
	 		
	 	}
	 	else if(Roof_page_value[5].equals("4"))
	 	{
	 		((RelativeLayout)findViewById(R.id.RC_additional_main)).setVisibility(View.VISIBLE);
			((View)findViewById(R.id.RC_Commercial_Additional)).setVisibility(View.GONE);
			((RelativeLayout)findViewById(R.id.RC_survay_head)).setVisibility(View.VISIBLE);
	 	}
	 	else if(Roof_page_value[5].equals("5")|| Roof_page_value[5].equals("6"))
	 	{
			((View)findViewById(R.id.RC_Additional)).setVisibility(View.GONE);
			//((View)findViewById(R.id.RC_Survey_Additional)).setVisibility(View.GONE);
			((View)findViewById(R.id.RC_Commercial_Additional)).setVisibility(View.VISIBLE);
	 		
	 	}
	 	else
	  	{
	 		((RelativeLayout)findViewById(R.id.RC_additional_main)).setVisibility(View.GONE);
	 		((RelativeLayout)findViewById(R.id.RC_survay_head)).setVisibility(View.GONE);
	 		
	 	}
	 	
	 	ADD_expend=(ImageView) findViewById(R.id.ADD_expend);
	 	ADD_expend.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String tag=v.getTag().toString();
				if(tag.equals("plus"))
				{
					if(!ADD_chk_top.isChecked())
					{
						ADD_layout_head.setVisibility(View.VISIBLE);
						ADD_expend.setTag("minus");
						ADD_expend.setImageDrawable(getResources().getDrawable(R.drawable.minus));
						if(findViewById(R.id.RCN_comp).getVisibility()!=View.VISIBLE)
						{
							RCN_expend.setTag("plus");
							RCN_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
							RCN_layout_head.setVisibility(View.GONE);	
						}
						
					}
				}
				else
				{
					ADD_layout_head.setVisibility(View.GONE);
					ADD_expend.setTag("plus");
					ADD_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
				}
			}
		});
	 	RCN_expend=(ImageView) findViewById(R.id.RCN_expend);
	 	RCN_expend.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String tag=v.getTag().toString();
				if(tag.equals("plus"))
				{
					if(!RCN_chk_top.isChecked())
					{
						RCN_layout_head.setVisibility(View.VISIBLE);
						RCN_expend.setTag("minus");
						RCN_expend.setImageDrawable(getResources().getDrawable(R.drawable.minus));
						if(findViewById(R.id.ADD_comp).getVisibility()!=View.VISIBLE)
						{
							ADD_expend.setTag("plus");
							ADD_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
							ADD_layout_head.setVisibility(View.GONE);	
						}
					}
				}
				else
				{
					RCN_layout_head.setVisibility(View.GONE);
					RCN_expend.setTag("plus");
					RCN_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
				}
			}
		});
	 	ADD_chk_top =(CheckBox) findViewById(R.id.Add_chk_top);
	 	ADD_layout_head =(LinearLayout) findViewById(R.id.ADD_layou_head);
	 	ADD_chk_top.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				if(ADD_chk_top.isChecked())
				{
				//	Cursor	c=cf.SelectTablefunction(cf.General_roof_view," WHERE RC_SRID='"+cf.selectedhomeid+"'  and RC_insp_id='"+cf.encode(Roof_page_value[5])+"'  and RC_Condition_type_order='11' and RC_Condition_type<>'"+cf.encode(cf.getResourcesvalue(R.string.Comments))+"' and RC_Condition_type<>'"+cf.encode(cf.getResourcesvalue(R.string.roof_add_4poin_txt2))+"' ");
					if(check_alert_add.equals("true"))
					{
						showAlert(0);
					}
					else
					{
						
						clear_additonal();
						ADD_expend.setTag("plus");
         				ADD_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
						ADD_layout_head.setVisibility(View.GONE);
					}
					/*if(findViewById(R.id.ADD_comp).getVisibility()!=View.VISIBLE)
					{
						ADD_layout_head.setVisibility(View.GONE);	
					}*/
					if(findViewById(R.id.RCN_comp).getVisibility()!=View.VISIBLE)
					{
						RCN_layout_head.setVisibility(View.GONE);
						RCN_expend.setTag("plus");
         				RCN_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
					}
					/*if(RCN_chk_top.isChecked())
					{
					RCN_layout_head.setVisibility(View.GONE);
					}*/
					//RCT_content.setVisibility(View.GONE);
				}
				else
				{
					check_alert_add="";
					ADD_expend.setTag("minus");
     				ADD_expend.setImageDrawable(getResources().getDrawable(R.drawable.minus));
					ADD_layout_head.setVisibility(View.VISIBLE);
					findViewById(R.id.ADD_comp).setVisibility(View.INVISIBLE);
					if(findViewById(R.id.RCN_comp).getVisibility()!=View.VISIBLE)
					{
						RCN_layout_head.setVisibility(View.GONE);
						RCN_expend.setTag("plus");
         				RCN_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
					}
					
				}
				ADD_chk_top.setFocusableInTouchMode(true);
				ADD_chk_top.requestFocus();
				ADD_chk_top.setFocusableInTouchMode(false);
				ADD_chk_top.setFocusable(false);
				ADD_chk_top.setFocusable(true);
				
			}
		});
	 	RCN_chk_top =(CheckBox) findViewById(R.id.RCN_chk_top);
	 	RCN_layout_head =(LinearLayout) findViewById(R.id.RCN_layou_head);
	 	RCN_chk_top.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(RCN_chk_top.isChecked())
				{
				
					//Cursor	c=cf.SelectTablefunction(cf.General_roof_view," WHERE RC_SRID='"+cf.selectedhomeid+"'  and RC_insp_id='"+cf.encode(Roof_page_value[5])+"'  and RC_Condition_type_order='' ");
					if(check_alert_rcn.equals("true"))
					{
						showAlert(1);
					}
					else
					{
						
						clear_RCN();
						RCN_expend.setTag("plus");
         				RCN_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
						RCN_layout_head.setVisibility(View.GONE);
					}
					if(findViewById(R.id.ADD_comp).getVisibility()!=View.VISIBLE)
					{
						ADD_expend.setTag("plus");
         				ADD_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
						ADD_layout_head.setVisibility(View.GONE);	
					}
					/*if(findViewById(R.id.RCN_comp).getVisibility()!=View.VISIBLE)
					{
						RCN_layout_head.setVisibility(View.GONE);
					}*/
				//	RCT_content.setVisibility(View.GONE);
				}
				else
				{
					check_alert_rcn="";
					RCN_expend.setTag("minus");
     				RCN_expend.setImageDrawable(getResources().getDrawable(R.drawable.minus));
					RCN_layout_head.setVisibility(View.VISIBLE);
					if(findViewById(R.id.ADD_comp).getVisibility()!=View.VISIBLE)
					{
						ADD_expend.setTag("plus");
         				ADD_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
						ADD_layout_head.setVisibility(View.GONE);	
					}
					findViewById(R.id.RCN_comp).setVisibility(View.INVISIBLE);
					
				}
				//RCN_chk_top.setFocusable(true);
				RCN_chk_top.setFocusableInTouchMode(true);
				RCN_chk_top.requestFocus();
			    RCN_chk_top.setFocusableInTouchMode(false);
			    RCN_chk_top.setFocusable(false);
				RCN_chk_top.setFocusable(true);
			}
		});
	 	/***For including option ends layout***/
	 	/***For 4 poitn roof  Additional information ***/
	 	/***show the clicked headrs content **/
	 	RCT_head=(RelativeLayout) findViewById(R.id.RCT_head);
	 	ADD_head=(RelativeLayout) findViewById(R.id.additional_head);
	 	RCn_head=(RelativeLayout) findViewById(R.id.RCN_head);
	 	RCT_content=(LinearLayout) findViewById(R.id.RCT_content);
	/***Default layout ***/
	 	ADD_layout_head.setVisibility(View.GONE);
		RCN_layout_head.setVisibility(View.GONE);
		RCT_content.setVisibility(View.VISIBLE);
			/***Default layout ends ***/
	 	try
	 	{
		 	Show_RCT_ValueD();
		 	show_GRI_Value();
	 	}
	 	catch (Exception e) {
			// TODO: handle exception
	 		System.out.println("the iseeus "+e.getMessage());
		}
 	
	 	
	 	
	
	}
	private void clear_RCN() {
		// TODO Auto-generated method stub
		int count=tbl_id.length;
		String sql="Select opt.*,mst.mst_text as title from "+cf.option_list+" opt left join "+cf.master_tbl+" mst  on mst_custom_id=option_tit and mst_module_id='2'  WHERE module_id='"+Roof_page_value[1]+"' and  insp_id='"+Roof_page_value[5]+"' ORDER BY option_potion  ";
		Cursor c12 =cf.arr_db.rawQuery(sql, null);
		c12.moveToFirst();
		String dynamic="false"; 
		int[] temp_tblid=tbl_id; 
		String type="",opt1[],Default_option_tyep = "",Default_option[]=null,RCN = "",default_t="",default_opt="",other_opt="",default_other="";
		for (int i=0;i<count;i++)
			{	
				
				if(i<c12.getCount())
				{
				
				RCN=cf.decode(c12.getString(c12.getColumnIndex("title")));
				type=c12.getString(c12.getColumnIndex("option_type"));
				opt1=c12.getString(c12.getColumnIndex("options")).split(",");
				default_t=c12.getString(c12.getColumnIndex("Default_option"));
				other_opt=c12.getString(c12.getColumnIndex("option_other"));
				try
				{
					
					c12.moveToNext();
				}catch (Exception e) {
					// TODO: handle exception
					RCN=c12.getString(c12.getColumnIndex("title"));
					type=Default_option_tyep;
					opt1=Default_option;
					default_t=default_opt;
					other_opt=default_other;
				}
				
	    		
				/** Create a Table row Dynamically **/
				
	    		
			try
			{
				
				if(type.equals("Radio"))
	     		{
					
					for(int j=0;j<opt1.length;j++)
					{
					
						if(!default_t.equals(opt1[j]))
						{
							((RadioButton)findViewById(tbl_id[i]).findViewWithTag(opt1[j])).setChecked(false);
						}
						else
						{
							((RadioButton)findViewById(tbl_id[i]).findViewWithTag(opt1[j])).setChecked(true);
						}
					}
					try
					{
						((EditText) findViewById(tbl_id[i]).findViewWithTag("Other_DESC")).setText("");
						if(!other_opt.equals(default_t))
						{
							((View) findViewById(tbl_id[i]).findViewWithTag("Other_DESC_li")).setVisibility(View.GONE);
						}
						else
						{
							((View) findViewById(tbl_id[i]).findViewWithTag("Other_DESC_li")).setVisibility(View.VISIBLE);
						}
					}
					catch (Exception e) {
						// TODO: handle exception
					}
	     				
	     		}
	     		else if(type.equals("CheckBox"))
		     	{

					for(int j=0;j<opt1.length;j++)
					{
						if(!default_t.equals(opt1[j]))
						{
							((CheckBox)findViewById(tbl_id[i]).findViewWithTag(opt1[j])).setChecked(false);
						}
						else
						{
							((RadioButton)findViewById(tbl_id[i]).findViewWithTag(opt1[j])).setChecked(true);
						}
					}
					try
					{
						((EditText) findViewById(tbl_id[i]).findViewWithTag("Other_DESC")).setText("");
						if(!other_opt.equals(default_t))
						{
							((View) findViewById(tbl_id[i]).findViewWithTag("Other_DESC_li")).setVisibility(View.GONE);
						}
						else
						{
							((View) findViewById(tbl_id[i]).findViewWithTag("Other_DESC_li")).setVisibility(View.VISIBLE);
						}
					}
	     			catch (Exception e) {
						// TODO: handle exception
					}
	     		}
	     		else if(type.equals("Spinner"))
	     		{
	     			
	     			
					
						((Spinner) findViewById(tbl_id[i]).findViewWithTag("Spinner_val")).setSelection(0);
					if(!default_t.equals(""))
					{							{
						for(int j=0;j<opt1.length;j++)
						{
							if(opt1[j].equals(default_t))
							{
								((Spinner) findViewById(tbl_id[i]).findViewWithTag("Spinner_val")).setSelection(j);
							}
							
						}
					}
	     			try
					{
						((EditText) findViewById(tbl_id[i]).findViewWithTag("Other_DESC")).setText("");
						if(!other_opt.equals(default_t))
						{
							((View) findViewById(tbl_id[i]).findViewWithTag("Other_DESC_li")).setVisibility(View.GONE);
						}
						else
						{
							((View) findViewById(tbl_id[i]).findViewWithTag("Other_DESC_li")).setVisibility(View.VISIBLE);
						}
					}
					catch (Exception e) {
						// TODO: handle exception
					}
				}	
	     		
	     		}
	     		else if(type.equals("DatePicker"))
	     		{ 
	     			((EditText) findViewById(tbl_id[i]).findViewWithTag("Date")).setText("");
     			
	     		}
	     		else if(type.equals("EditText"))
	     		{ 
	     			((EditText) findViewById(tbl_id[i]).findViewWithTag("EditText")).setText("");
	     			
	     		}
				/*** Customized option clear for commercial**/
				if(((EditText)findViewById(tbl_id[i]).findViewWithTag("Title_ed")).getText().toString().trim().equals(everReplaced_tit[1]))
    			{
					findViewById(tbl_id[i]).setVisibility(View.GONE);
    			}
				if(((EditText)findViewById(tbl_id[i]).findViewWithTag("Title_ed")).getText().toString().trim().equals(amt_tit[1]))
    			{
					findViewById(tbl_id[i]).setVisibility(View.GONE);
    			}
				if(((EditText)findViewById(tbl_id[i]).findViewWithTag("Title_ed")).getText().toString().trim().equals(has_roof[1]))
    			{
					findViewById(tbl_id[i]).setVisibility(View.GONE);
    			}

			
			if(RCN.equals(Roof_page_value[4]))
			{
				Default_option=opt1;
				Default_option_tyep=type;
				default_opt=default_t;
				default_other=other_opt;
			}
				
			
			
			}catch (Exception e) {
				// TODO: handle exception
				System.out.println("error"+e.getMessage());
			}
			
		}
		else
		{
			
			int[] temp=Delete_from_array(temp_tblid, tbl_id[i]);
			temp_tblid=null;
			temp_tblid=temp;
			
		}					
	
			}
		tbl_id=null;
			tbl_id=temp_tblid;
	}

	private void clear_additonal() {
		// TODO Auto-generated method stub
		
		clearValueofgroub((ViewGroup)findViewById(R.id.ADD_layou_head));
		((RadioButton)ADD_RG1.findViewWithTag("No")).setChecked(true);
		
		if(((Roof_page_value[5].equals("5")) || Roof_page_value[5].equals("6") ))
	 	{
			findViewById(R.id.ADD_com3_equ_rw).setVisibility(View.GONE);
			findViewById(R.id.ADD_com3_equ_rw2).setVisibility(View.GONE);
			findViewById(R.id.ADD_com3_equ_rw3).setVisibility(View.GONE);
			findViewById(R.id.RMS_chk1_details).setVisibility(View.GONE);
			findViewById(R.id.RMS_chk2_details).setVisibility(View.GONE);
			findViewById(R.id.RMS_chk3_details).setVisibility(View.GONE);
			findViewById(R.id.RMS_chk8_details).setVisibility(View.GONE);
			findViewById(R.id.ADD_PDU_sub).setVisibility(View.GONE);
	 	}
	
	
	}
	public void showAlert(final int type)
	{
		String tit="Confirmation",txt="Do you want to clear the saved data?";
		if(type==0)
		{
			txt="Do you want to clear the Additional Roof Details saved data?";
		}else if(type==1)
		{
			txt="Do you want to clear the Roof Conditions Noted saved data?";
		}
		
		final AlertDialog.Builder ab =new AlertDialog.Builder(RoofSection.this);
		ab.setTitle(tit);
		ab.setMessage(Html.fromHtml(txt));
		ab.setIcon(R.drawable.alertmsg);
		ab.setCancelable(false);
		ab.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				if(type==0)
				{
					clear_additonal();
					try
					{
						
						((TextView) findViewById(R.id.ADD_comp)).setVisibility(View.INVISIBLE);
						
						
					}catch (Exception e) {
						// TODO: handle exception
						System.out.println("error comes"+e.getMessage());
					}
					ADD_layout_head.setVisibility(View.GONE);
					ADD_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
					ADD_expend.setTag("plus");
				}
				else
				{
					clear_RCN();
					try
					{
						((TextView) findViewById(R.id.RCN_comp)).setVisibility(View.INVISIBLE);
					
						
					}catch (Exception e) {
						// TODO: handle exception  
						System.out.println("error comes"+e.getMessage());
					}
					
					RCN_layout_head.setVisibility(View.GONE);
					RCN_expend.setTag("plus");
					RCN_expend.setImageDrawable(getResources().getDrawable(R.drawable.plus));
				}
				dialog.dismiss();
			}

			
		})
		.setNegativeButton("No", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				
				if(type==0)
				{
					ADD_chk_top.setChecked(false);
				}
				else
				{
					RCN_chk_top.setChecked(false);
				}
			}
		});
		AlertDialog al=ab.create();
		al.show();
	}
	class setVisibleOnChecked implements OnClickListener
	{
		View other=null;
	 setVisibleOnChecked(View v)
	{
			other=v;
	}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if(((CheckBox) v).isChecked())
			{
				other.setVisibility(View.VISIBLE);
			}
			else
			{
				clearValueofgroub((ViewGroup) other);
				other.setVisibility(View.INVISIBLE);
			
			}
		}
		
	}
	public void clearValueofgroub(ViewGroup other)
	{
		for(int i=0;i< other.getChildCount();i++)
		{
			if(other.isShown())
			{
				if(other.getChildAt(i) instanceof EditText) {
					((EditText) other.getChildAt(i)).setText("");
				}
				if(other.getChildAt(i) instanceof RadioGroup) {
					((RadioGroup) other.getChildAt(i)).clearCheck();
				}
				if(other.getChildAt(i) instanceof CheckBox) {
					((CheckBox) other.getChildAt(i)).setChecked(false);
				}
				if(other.getChildAt(i) instanceof RadioButton) {
					((RadioButton) other.getChildAt(i)).setChecked(false);
				}
				if(other.getChildAt(i) instanceof ViewGroup) {
					clearValueofgroub(((ViewGroup) other.getChildAt(i)));
				}
			}	
		}
	}
	
	private void Show_RCT_ValueD() {
		// TODO Auto-generated method stub
		
		/*****/
		String sql="SELECT opt.*,mst.mst_text as title FROM "+cf.option_list+" as opt left join "+cf.master_tbl+" as  mst  on mst_custom_id=option_tit and mst_module_id='2' Where module_id='"+Roof_page_value[0]+"' and  insp_id='"+Roof_page_value[5]+"' ORDER BY option_potion";
		Cursor c=cf.arr_db.rawQuery(sql, null);
				//cf.SelectTablefunction(cf.option_list, " Where module_id='"+Roof_page_value[0]+"' and  insp_id='"+Roof_page_value[5]+"' ORDER BY option_potion");
		RCTD_Val.removeAllViews();
		if(c.getCount()>0)
		{ 
			RCT_id= new int[c.getCount()];
			int j=0;
			c.moveToFirst();
			TableRow tbl=new TableRow(this);
			for(int i=1;i<=c.getCount();i++)
			{
				LinearLayout.LayoutParams lin_params_ops = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
				TableRow.LayoutParams lin_params = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
				lin_params.setMargins(0, 0, 20, 0);
				/** Create a Table row Dynamically **/
				LinearLayout lin = new LinearLayout(this);
				lin.setOrientation(LinearLayout.HORIZONTAL);
				lin.setId(c.getInt(c.getColumnIndex("option_tit")));
				
				RCT_id[i-1]=lin.getId();
			
				lin.setLayoutParams(lin_params);
				/** Create a Title of the event **/
				TextView tit_star=new TextView(this,null,R.attr.star);
				tit_star.setTag("Star");
				tit_star.setPadding(10, 0, 5, 0);
				//tbl.addView(tit_star);
				lin.addView(tit_star);
				TextView tit_tv=new TextView(this,null,R.attr.Tit_txt);
				tit_tv.setTag("Title");
				
				if(c.getString(c.getColumnIndex("Requirred")).equals("1"))
				{
					tit_star.setVisibility(View.VISIBLE);
					//tit_tv.setText(Html.fromHtml(cf.redcolor+" "+cf.decode(c.getString(c.getColumnIndex("title")))));
				}
				else
				{
					tit_star.setVisibility(View.INVISIBLE);
				}
				tit_tv.setText(cf.decode(c.getString(c.getColumnIndex("title"))));
				tit_tv.setVisibility(View.VISIBLE);
				
				tit_tv.setWidth(c.getInt(c.getColumnIndex("tit_max_w")));
							
				lin.addView(tit_tv);
				/** Create a Title of the event Ends**/
				/**Create colon **/
				TextView colon_tv=new TextView(this,null,R.attr.colon);
				lin.addView(colon_tv);

				/**Create colon **/
				
				/** Create a option of the event **/
				LinearLayout op_li_t= new LinearLayout(this);
				op_li_t.setOrientation(LinearLayout.VERTICAL);
				
				/** Get the configerd value form the db **/
				String[] opt=c.getString(c.getColumnIndex("options")).split(",");
				int NoOptionPerRow=c.getInt(c.getColumnIndex("option_max_l"));
				int opt_maxW=c.getInt(c.getColumnIndex("option_max_W"));
				String Other_option=c.getString(c.getColumnIndex("option_other"));
				String type=c.getString(c.getColumnIndex("option_type"));
				String default_opt=c.getString(c.getColumnIndex("Default_option"));
				if(cf.decode(c.getString(c.getColumnIndex("title"))).equals(buildingcode_tit))
				{
					if(cf.County.trim().toLowerCase().equals("Miami-Dade".toLowerCase())|| cf.County.trim().toLowerCase().equals("Broward".toLowerCase()))
					{
						default_opt="1994 SFBC";
					}
				}
				int otherl=c.getInt(c.getColumnIndex("other_l"));
				int otherw=c.getInt(c.getColumnIndex("other_w"));
				String other_fi=c.getString(c.getColumnIndex("Other_filter"));
				
				LinearLayout op_li = null;
				/** Get the configerd value form the db ends **/
				/**Set the option Starts***/
				
				if(type.equals("Radio"))
				{
					
				op_li=Add_ROption_dynamic(lin.getId(), opt,NoOptionPerRow,opt_maxW,Other_option);
				
				
				op_li_t.addView(op_li);
					if(!default_opt.equals(""))
					{
						((RadioButton) op_li_t.findViewWithTag(default_opt)).setChecked(true);
						
					}
				}
				else if(type.equals("CheckBox"))
				{
					 op_li=Add_CHKption_dynamic(lin.getId(), opt,NoOptionPerRow,opt_maxW,Other_option);
				
					op_li_t.addView(op_li);
				
					if(!default_opt.equals(""))
					{
						
						Setcheckvalue(op_li,default_opt,",");
						
					}
				
				}
				else if(type.equals("Spinner"))
				{
					 op_li=Add_Spinption_dynamic(lin.getId(), opt,opt_maxW,Other_option);
					
					op_li_t.addView(op_li);
				
					if(!default_opt.equals(""))
					{
						for(int m=0;m<opt.length;m++)
						{
							if(opt[m].trim().equals(default_opt.trim()))
								((Spinner)op_li.findViewWithTag("Spinner_val")).setSelection(m);
						}
						 
						//Setcheckvalue(op_li,default_opt,",");
						
					}
				}
				else if(type.equals("DatePicker"))
				{
					 op_li=Add_DPinption_dynamic(lin.getId(),opt_maxW);
					
					op_li_t.addView(op_li);
				
					if(!default_opt.equals(""))
					{
						
						//Setcheckvalue(op_li,default_opt,",");
						
					}
				}
				else if(type.equals("ImagePicker"))
				{
					 op_li=Add_IPicker_dynamic(lin.getId(),opt_maxW);
					
					op_li_t.addView(op_li);
					
					if(!default_opt.equals(""))
					{
						//((EditText)op_li_t.findViewWithTag("Path")).setText(default_opt);
						
						//Setcheckvalue(op_li,default_opt,",");
						
					}
				}
				
				/**Set the option Endss***/
				/**Start Checking for the other option Starts*/
				if(!Other_option.trim().equals(""))
				{
					LinearLayout other_li=new LinearLayout(this);
					other_li.setLayoutParams(lin_params_ops);
					other_li.setTag("Other_DESC_li");
					other_li.setOrientation(LinearLayout.VERTICAL);
					if(cf.decode(c.getString(c.getColumnIndex("title"))).equals(approx))
					{
						other_li.setOrientation(LinearLayout.HORIZONTAL);
						TextView tv=new TextView(this,null,R.attr.Tit_txt);
						
						tv.setTag("Ye");
						tv.setText("No of Years :");
						tv.setPadding(10, 0, 0, 0);
						tv.setVisibility(View.VISIBLE);
						other_li.addView(tv);
					}
					EditText yes_ed=new EditText(this,null,R.attr.Roof_RCN_yes_ed);
					InputFilter[] FilterArray;
					if(other_fi.trim().equals("Number"))
					{
						yes_ed.setInputType(InputType.TYPE_CLASS_NUMBER);
						String s="Invalid Entry! Please enter valid year for ";
						yes_ed.addTextChangedListener(new text_watch_number(s+cf.decode(c.getString(c.getColumnIndex("title"))),yes_ed));
						
					}
						FilterArray = new InputFilter[1];
						FilterArray[0] = new InputFilter.LengthFilter(otherl);
					
					yes_ed.setFilters(FilterArray);
					/*yes_ed.setMaxWidth(otherw);
					yes_ed.setMinWidth(otherw);*/
					
					other_li.addView(yes_ed,otherw,LayoutParams.WRAP_CONTENT);
					other_li.setVisibility(View.GONE);
					
					
					yes_ed.setOnClickListener(new OnClickListener() {
	
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							((TextView) v).setFocusable(true);
							((TextView) v).setFocusableInTouchMode(true);
							((TextView) v).requestFocus();
						}
					});
					
					yes_ed.setHint("");
					
					yes_ed.setTag("Other_DESC");
					op_li_t.addView(other_li);
					
					if(default_opt.contains(Other_option))
					{
						((LinearLayout) op_li_t.findViewWithTag("Other_DESC_li")).setVisibility(View.VISIBLE);
					}
					
				}
				
				/**Start Checking for the other option Ends*/
				/**customized for rooof shape**/
					if(cf.decode(c.getString(c.getColumnIndex("title"))).trim().equals(Roof_shap_tit))
					{
						LinearLayout  li_rs_sub =new LinearLayout(this);
						li_rs_sub.setLayoutParams(lin_params_ops);
						li_rs_sub.setTag("Sub_percent");
						li_rs_sub.setOrientation(LinearLayout.VERTICAL);
						EditText per_ed=new EditText(this,null,R.attr.ed_percent);
						String s="Invalid Entry! Please enter valid percent for ";
						per_ed.addTextChangedListener(new text_watch_number(s+cf.decode(c.getString(c.getColumnIndex("title"))),per_ed));
						//per_ed.setHint("");
						per_ed.setTag("RSH_percent");
						
						li_rs_sub.addView(per_ed);
						op_li.addView(li_rs_sub);
						
						TextView per_txt=new TextView(this,null,R.attr.text_view);
						per_txt.setText("%");
						per_txt.setTag("percent_txt");
						
						per_txt.setMaxWidth(20);
						per_txt.setMinWidth(10);
						op_li.addView(per_txt);
						final int sp_id=lin.getId();
						final String other_option=Other_option;
						Spinner sp=((Spinner)op_li.findViewWithTag("Spinner_val"));
						sp.setOnItemSelectedListener(new OnItemSelectedListener() {

							@Override
							public void onItemSelected(AdapterView<?> arg0,
									View arg1, int arg2, long arg3) {
								// TODO Auto-generated method stub
								
								try
								{
								// TODO Auto-generated method stub
								if(((Spinner) findViewById(sp_id).findViewWithTag("Spinner_val")).getSelectedItem().toString().trim().equals(other_option))
								{
									((LinearLayout)	findViewById(sp_id).findViewWithTag("Other_DESC_li")).setVisibility(View.VISIBLE);
									//((EditText)	findViewById(sp_id).findViewWithTag("Other_DESC")).setText("");
									
								}
								else
								{
									((LinearLayout)	findViewById(sp_id).findViewWithTag("Other_DESC_li")).setVisibility(View.GONE);
									((EditText)	findViewById(sp_id).findViewWithTag("Other_DESC")).setText("");
								}
							
								if(((Spinner) findViewById(sp_id).findViewWithTag("Spinner_val")).getSelectedItem().toString().trim().equals("-Select-"))
								{
									((EditText) findViewById(sp_id).findViewWithTag("RSH_percent")).setText("");
									((View) findViewById(sp_id).findViewWithTag("RSH_percent")).setEnabled(false);
									/*((View) findViewById(sp_id).findViewWithTag("percent_txt")).setVisibility(View.INVISIBLE);*/
									
								}
								else
								{
									((View) findViewById(sp_id).findViewWithTag("RSH_percent")).setEnabled(true);//(View.VISIBLE);
									if(((EditText) findViewById(sp_id).findViewWithTag("RSH_percent")).getText().toString().equals(""))
									{
										((EditText) findViewById(sp_id).findViewWithTag("RSH_percent")).setText("100");
									}
									//((View) findViewById(sp_id).findViewWithTag("percent_txt")).setVisibility(View.VISIBLE);
								}
								
								
								}
								catch (Exception e) {
									// TODO: handle exception
									System.out.println("no more text"+e.getMessage());
								}

							}

							@Override
							public void onNothingSelected(AdapterView<?> arg0) {
								// TODO Auto-generated method stub
								
							}
						});
					}
				
				/**customized for rooof shape ends**/  
				
				
				/***Customized validation for the No information provide****/
				if(cf.decode(c.getString(c.getColumnIndex("title"))).trim().equals(no_perdomin_tit))
				{
					sql="SELECT * from "+cf.option_list+" Where module_id='"+Roof_page_value[0]+"' and  insp_id='"+Roof_page_value[5]+"' and option_tit=(SELECT mst_custom_id From "+cf.master_tbl+" Where mst_text='"+cf.encode(permit_dat_tit)+"')";
					 Cursor s2=	 cf.arr_db.rawQuery(sql, null);
							 //cf.SelectTablefunction(cf.option_list, " Where module_id='"+Roof_page_value[0]+"' and  insp_id='"+Roof_page_value[5]+"' and option_tit='"+cf.encode(permit_dat_tit)+"'");
				
					if(s2.getCount()>0)
					{	
						
					s2.moveToFirst();
					final int no_id=s2.getInt(s2.getColumnIndex("option_tit"));
						 if(type.equals("CheckBox"))
						{ 
							
						((CheckBox)op_li_t.findViewWithTag("empty")).setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								if(((CheckBox) v).isChecked())
								{
									
									((EditText) findViewById(no_id).findViewWithTag("Date")).setText("Not Applicable");
									((Button) findViewById(no_id).findViewWithTag("Date_pck")).setVisibility(View.INVISIBLE);
									
								}
								else
								{
									
									((EditText) findViewById(no_id).findViewWithTag("Date")).setText("");
									((Button) findViewById(no_id).findViewWithTag("Date_pck")).setVisibility(View.VISIBLE);
								}
							}
						});
							
					}
				}
				}
				/***Customized validation for the No information provide****/
				/***Customized validation for the PreDominent provide****/
				if(cf.decode(c.getString(c.getColumnIndex("title"))).trim().equals(pre_domtit))
				{
					if(type.equals("CheckBox"))
						{ 
							final int tit_id= c.getInt(c.getColumnIndex("option_tit"));
							((CheckBox)op_li_t.findViewWithTag("empty")).setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(final View v) {
								// TODO Auto-generated method stub
								if(((CheckBox) v).isChecked())
								{
									
									Cursor cs=cf.SelectTablefunction(cf.Roof_covertype_view, " Where RCT_FieldVal='true' and RCT_insp_id='"+cf.encode(Roof_page_value[5])+"' and RCT_TypeValue='"+tit_id+"' and RCT_srid='"+cf.selectedhomeid+"'");
									if(cs.getCount()>0)
									{
										final AlertDialog.Builder ab =new AlertDialog.Builder(RoofSection.this);
										ab.setTitle("Add Predominant");
										ab.setMessage("Already selected in a Roof Covering Type. Do you want to change this?");
										ab.setIcon(R.drawable.alertmsg);
										ab.setCancelable(false);
										ab.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
											
											@Override
											public void onClick(DialogInterface dialog, int which) {
												// TODO Auto-generated method stub
												ab.setCancelable(true);
												dialog.dismiss();
											}
										})
										.setNegativeButton("No", new DialogInterface.OnClickListener() {
											
											@Override
											public void onClick(DialogInterface dialog, int which) {
												// TODO Auto-generated method stub
												((CheckBox) v).setChecked(false);
												ab.setCancelable(true);
												dialog.dismiss();
												
											}
										});
										AlertDialog al=ab.create();
										al.show();
									}
									
									
								}
								
							}
						});
							
					}
				}
				
				/***Customized validation for the PreDominent provide****/
				/***Customized validation for the Roof cover type ****/
				if(cf.decode(c.getString(c.getColumnIndex("title"))).trim().equals(Roof_page_value[3]))
				{
					final int id=lin.getId();
					final String otheropt=Other_option;
					((Spinner)op_li.findViewWithTag("Spinner_val")).setOnItemSelectedListener(new OnItemSelectedListener() {
						@Override
						public void onItemSelected(AdapterView<?> arg0, View arg1,
								int arg2, long arg3) {
							try
							{
							// TODO Auto-generated method stub
								if(update==0)
								{
									clear_RCT(0);
								}
								else
								{
									update=0;
								}
							if(((Spinner) findViewById(id).findViewWithTag("Spinner_val")).getSelectedItem().toString().trim().equals(otheropt))
							{
								((LinearLayout)	findViewById(id).findViewWithTag("Other_DESC_li")).setVisibility(View.VISIBLE);
								
							}
							else
							{
								((LinearLayout)	findViewById(id).findViewWithTag("Other_DESC_li")).setVisibility(View.GONE);
								((EditText)	findViewById(id).findViewWithTag("Other_DESC")).setText("");
							}
												
							
							
							}
							catch (Exception e) {
								// TODO: handle exception
								System.out.println("no more text"+e.getMessage());
							}
						}
	
						@Override
						public void onNothingSelected(AdapterView<?> arg0) {
							// TODO Auto-generated method stub
							
						}
					});
							
				}
				/***Customized validation for the Roof cover type ends ****/
				lin.addView(op_li_t);
				
				if(c.getString(c.getColumnIndex("options")).equals("") && (type.equals("Radio")||type.equals("CheckBox")))
				{
					
					if(type.equals("Radio"))
					{
						
					((RadioButton) op_li_t.findViewWithTag("empty")).setTag(cf.decode(c.getString(c.getColumnIndex("title"))));
					}
					else if(type.equals("CheckBox"))
					{ 
						
					((CheckBox)op_li_t.findViewWithTag("empty")).setTag(cf.decode(c.getString(c.getColumnIndex("title"))));
					}
						
				}
			
				tbl.addView(lin);
				if((i%2)==0 || i==c.getCount())
				{
					
					RCTD_Val.addView(tbl);
					tbl=new TableRow(this);
					j++;
				}
				c.moveToNext();
				// TODO Auto-generated method stub
				
				
				/** Create a Table row Dynamically Ends **/

			}
		}
	/***Show the saved Roof cover values ****/	
		if(Roof.equals(Save_from) )
		{
			show_RCT_List(1);
		}
		else
		{
			loadDataRCT();
		}
		
    /***Show the saved Roof cover values Ends ****/
	}
	class text_watch_number implements TextWatcher
	{
		String tit="";
		EditText ed;
		text_watch_number(String tit,EditText ed)
		{
		this.tit=tit;
		this.ed=ed;
		}
	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void afterTextChanged(Editable s) {
		// TODO Auto-generated method stub
		if(s.toString().equals("0"))
		{
			cf.ShowToast(tit, 0);
				ed.setText("");
			
		}
	}

	}

	public void show_RCT_List(int enb) {
		// TODO Auto-generated method stub
		
		getRoofValueForVariables(Roof);/// This code  for solving the problem from the load data from GCH to four point
		String sql="SELECT GRI.*,mst.mst_text as title,opt.tit_max_w FROM "+cf.Roof_covertype_view+" as GRI LEFT JOIN "+cf.option_list+" as opt on opt.option_tit=GRI.RCT_TypeValue and module_id='2' and insp_id='"+Roof_page_value[5]+"'  left join "+cf.master_tbl+" mst  on mst_custom_id=GRI.RCT_TypeValue and mst_module_id='2' Where RCT_srid='"+cf.selectedhomeid+"' and RCT_insp_id='"+cf.encode(Roof_page_value[5])+"' ORDER BY RCT_FielsName";
		System.out.println("the list quesr="+sql);
		Cursor c1= cf.arr_db.rawQuery(sql, null);
		//Cursor c1= cf.SelectTablefunction(cf.Roof_covertype_view, " Where RCT_srid='"+cf.selectedhomeid+"' and RCT_page='"+cf.encode(Roof_page_value[0])+"' ORDER BY RCT_FielsName");
		sql="SELECT opt.*,mst.mst_text as title from "+cf.option_list+" opt left join "+cf.master_tbl+" mst  on mst_custom_id=option_tit and mst_module_id='2' Where module_id='"+Roof_page_value[2]+"' and  insp_id='"+Roof_page_value[5]+"' ORDER BY option_potion ";
		Cursor c=cf.arr_db.rawQuery(sql, null); 
		//System.out.println("the sql was= "+sql);
				//cf.SelectTablefunction(cf.option_list, " Where module_id='"+Roof_page_value[2]+"' and  insp_id='"+Roof_page_value[5]+"' ORDER BY option_potion ");//Cursor c1= cf.SelectTablefunction(cf.Roof_covertype_view, " Where RCT_srid='"+cf.selectedhomeid+"' and RCT_page='"+cf.encode(Roof_page_value[0])+"' ORDER BY RCT_FielsName");
		
		String RoofType="";
		Boolean b=false;
		
		if(c1.getCount()>0)
			{
			 findViewById(R.id.RCT_comp).setVisibility(View.VISIBLE);
			c1.moveToFirst();
			c.moveToFirst();
			RCT_ShowValue.removeAllViews();
			RCT_ShowValue.setVisibility(View.VISIBLE);
			TableRow tbl =new TableRow(this);
			TableRow.LayoutParams lin_params = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
			tbl.setBackgroundResource(R.drawable.roofhead); 
			tbl.setLayoutParams(lin_params);
			tbl.setGravity(Gravity.CENTER_VERTICAL);
			
			/****Start set the header for the saved RCT ***/
			if(c1.getCount()>0)
			{			/**loading two line list **/
				TableLayout li_opt=new TableLayout(this);
				li_opt.setOrientation(LinearLayout.VERTICAL);
				/**Sub row linear **/
				TableRow li_spu =new TableRow(this);
				li_opt.addView(li_spu,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
				for(int i=0;i<c.getCount();i++,c.moveToNext())
				{
					
					if(i<2)
					{
						TextView Ed=new TextView(this,null,R.attr.text_view);
						Ed.setVisibility(View.VISIBLE);
						Ed.setTextColor(Color.WHITE);
						tbl.addView(Ed,c.getInt(c.getColumnIndex("tit_max_w")),LayoutParams.WRAP_CONTENT);
						Ed.setText(cf.decode(c.getString(c.getColumnIndex("title"))));
						View v=new View(this);
						v.setBackgroundResource(R.color.black);
						tbl.addView(v,1,LayoutParams.FILL_PARENT);
					}
					else
					{
						
						if((i-2)%colomprerow==0 && (i-2)!=0)
						{
							View v=new View(this);
							v.setBackgroundResource(R.color.row_head_line);
							li_opt.addView(v,LayoutParams.FILL_PARENT,1);
							
							li_spu=new TableRow(this);
							li_opt.addView(li_spu,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
						}
						else if(1+1!=c.getCount() && (i-2)!=0)
						{
							View v=new View(this);
							v.setBackgroundResource(R.color.black);
							li_spu.addView(v,1,LayoutParams.FILL_PARENT);
						}
							
						TextView Ed=new TextView(this,null,R.attr.text_view);
						Ed.setVisibility(View.VISIBLE);
						Ed.setTextColor(Color.WHITE);
						
						li_spu.addView(Ed,c.getInt(c.getColumnIndex("tit_max_w")),LayoutParams.WRAP_CONTENT);
						Ed.setText(cf.decode(c.getString(c.getColumnIndex("title"))));
						
					}
				}
				tbl.addView(li_opt,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
				RCT_ShowValue.addView(tbl);
				
				/****Start set the Rows of the saved RCT ***/
				TextView Ed=null;
				c1.moveToFirst();
				
				for(int m=0;m<c1.getCount()/(c.getCount()-2);m++)
				{
					tbl=new TableRow(this);
					//tbl.setBackgroundResource(R.drawable.roofhead); 
					tbl.setLayoutParams(lin_params);
					tbl.setGravity(Gravity.CENTER_VERTICAL);
					if(m%2==0)
					{
						
						tbl.setBackgroundResource(R.drawable.rooflist);
					}
					else
					{
						tbl.setBackgroundResource(R.drawable.rooflistw);
						
					}
					c.moveToFirst();
					
					li_opt=new TableLayout(this);
					li_opt.setOrientation(LinearLayout.VERTICAL);
					li_spu=new TableRow(this);
					
					for(int i=0;i<c.getCount();i++,c.moveToNext())
					{
						
						Ed=new TextView(this,null,R.attr.text_view);
						if(i<2)
						{
							
							
							Ed.setVisibility(View.VISIBLE);
							Ed.setTextColor(Color.BLACK);
							tbl.addView(Ed,c.getInt(c.getColumnIndex("tit_max_w")),LayoutParams.WRAP_CONTENT);
							View v=new View(this);
							v.setBackgroundResource(R.color.black);
							tbl.addView(v,1,LayoutParams.FILL_PARENT);
							
						}
						else
						{
							
							if((i-2)%colomprerow==0 && (i-2)!=0)
							{
								li_opt.addView(li_spu,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
								View v=new View(this);
								v.setBackgroundResource(R.color.row_head_line);
								li_opt.addView(v,LayoutParams.FILL_PARENT,1);
								li_spu=new TableRow(this);
								li_opt.addView(li_spu,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
								//tbl.addView();
							}
							else if(1+1!=c.getCount() && (i-2)!=0)
							{
								View v=new View(this);
								v.setBackgroundResource(R.color.black);
								li_spu.addView(v,1,LayoutParams.FILL_PARENT);
							}
							
						}
						String tit=cf.decode(c.getString(c.getColumnIndex("title")));
						if(tit.equals("No"))
						{
							Ed.setTextColor(Color.BLACK);
							Ed.setText((m+1)+"");
							
						}
						else if((tit.trim().equals(no_perdomin_tit) || tit.trim().equals("Predominant")) && cf.decode(c1.getString(c1.getColumnIndex("RCT_FieldVal"))).equals("true"))
						{
							ImageView  iv=new ImageView(this);
							iv.setBackgroundResource(R.drawable.tick_icon);
							iv.setVisibility(View.VISIBLE);
							RelativeLayout rl=new RelativeLayout(this);
							rl.setGravity(Gravity.CENTER);
							rl.addView(iv);
							li_spu.addView(rl,c.getInt(c.getColumnIndex("tit_max_w")),LayoutParams.WRAP_CONTENT);
						}
						else if(tit.trim().equals(Image_tit))
						{ 
						
							TextView tv=new TextView(this,null,R.attr.text_view);
							tv.setVisibility(View.VISIBLE);
							tv.setTextColor(Color.BLUE);
							//tv.setTag("only_view");
							tv.setTag(cf.decode(c1.getString(c1.getColumnIndex("RCT_FielsName"))));
						//	tv.setText(Html.fromHtml("<u>View Photos</u>"));
							Cursor c12=cf.arr_db.rawQuery("Select * from "+cf.Roofcoverimages+" Where RIM_masterid=(Select RM_masterid from "+cf.Roof_master+
		    				" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+Roof_page_value[5]+"' and RM_Covering='"+c1.getString(c1.getColumnIndex("RCT_FielsName"))+"') order by RIM_Order", null);
		    				if(c12.getCount()>0)
				    		{
				    			
				    			tv.setText(Html.fromHtml("<u>View Photos ("+c12.getCount()+")</u>"));
				    			tv.setOnClickListener(new onclick_viewimage(cf.decode(c1.getString(c1.getColumnIndex("RCT_FielsName"))),Roof_page_value[5]));
				    		
				    			li_spu.addView(tv);
								
				    		}
				    		else
				    		{
				    			tv.setTextColor(Color.BLACK);
				    			tv.setText("N/A");
				    			
				    			//rl.setGravity(Gravity.CENTER);
				    			
				    			li_spu.addView(tv);

				    		}
							
							
						
						}
						else if(tit.equals("Edit/Delete"))
						{
							c1.moveToPrevious();/***it in the last postion so we move to the previous one **/
							LinearLayout li=new LinearLayout(this);
							ImageView im_up =new ImageView(this,null,R.attr.Roof_RCN_Tit_img);
														
							im_up.setTag(cf.decode(c1.getString(c1.getColumnIndex("RCT_FielsName"))));
							li.addView(im_up);
							im_up.setVisibility(View.VISIBLE);
							im_up.setPadding(5, 5, 0, 5);
							im_up.setOnClickListener(new RCT_EditClick(1));
							ImageView im_dl =new ImageView(this,null,R.attr.Roof_RCN_opt_img);
							im_dl.setTag(cf.decode(c1.getString(c1.getColumnIndex("RCT_FielsName"))));
							im_dl.setPadding(5, 5, 0, 5);
							im_dl.setVisibility(View.VISIBLE);
							im_dl.setOnClickListener(new RCT_EditClick(2));
							li.addView(im_dl);
							li.setGravity(Gravity.CENTER);
							li_spu.addView(li);
							c1.moveToNext();/***Reset the perious state**/
						}
						else
						{
							
							Ed.setVisibility(View.VISIBLE);
							Ed.setTextColor(Color.BLACK);
							//tbl.addView(Ed,c.getInt(c.getColumnIndex("tit_max_w")),LayoutParams.WRAP_CONTENT);
							if(cf.decode(c1.getString(c1.getColumnIndex("RCT_FieldVal"))).equals("Other"))
							{
								
								if(tit.equals(approx))
								{
									if(cf.decode(c1.getString(c1.getColumnIndex("RCT_Other"))).trim().equals("1")|| cf.decode(c1.getString(c1.getColumnIndex("RCT_Other"))).trim().equals("0"))
									{
										Ed.setText(cf.decode(c1.getString(c1.getColumnIndex("RCT_Other"))+" Year"));
									}
									else
									{
										Ed.setText(cf.decode(c1.getString(c1.getColumnIndex("RCT_Other"))+" Year"));
									}
									
								}
								else
								{
									Ed.setText(cf.decode(c1.getString(c1.getColumnIndex("RCT_Other"))));
								}
							}
							else if(!cf.decode(c1.getString(c1.getColumnIndex("RCT_FieldVal"))).equals(""))
							{
								if(tit.equals(Roof_shap_tit))
								{
									String[] tmp=cf.decode(c1.getString(c1.getColumnIndex("RCT_FieldVal"))).split("~");
									String s;
									if(tmp[0].equals("Other"))
									{
										s=cf.decode(c1.getString(c1.getColumnIndex("RCT_Other")));
									}else
									{
										s=tmp[0];
									}
									
									
									if(tmp.length>1 && tmp[1].trim().length()>0)
									{
										s+=" ("+tmp[1]+"%)";
									}
									Ed.setText(s);
								}
								else
								{
									
									Ed.setText(cf.decode(c1.getString(c1.getColumnIndex("RCT_FieldVal"))));
								}
							}
							else 
							{
								Ed.setText("N/A");
							}
							
							if(tit.equals(cf.RCT_tit))
							{
								//tbl.addView(Ed,c.getInt(c.getColumnIndex("tit_max_w")),LayoutParams.WRAP_CONTENT);
							}
							else
							{
								li_spu.addView(Ed,c.getInt(c.getColumnIndex("tit_max_w")),LayoutParams.WRAP_CONTENT);
							}
						
						}
						
						if(!tit.equals("Edit/Delete")&& !tit.equals("No"))
						{
							c1.moveToNext();
						}
						
							
							
							
					}
						
						
					tbl.addView(li_opt,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
					RCT_ShowValue.addView(tbl);
					/****Start set the Rows of the saved RCT ends ***/		
			}
			}		
			
		}
		else {
			findViewById(R.id.RCT_comp).setVisibility(View.INVISIBLE);
			RCT_ShowValue.removeAllViews();
			RCT_ShowValue.setVisibility(View.GONE);
			
		}
		/**navigation bar visibility **/
		
			
		LinearLayout.LayoutParams lp=new LinearLayout.LayoutParams(cf.wd-60,android.widget.LinearLayout.LayoutParams.WRAP_CONTENT);
		//HZ.setLayoutParams(lp);
		//tmp.setMinimumWidth(cf.wd);
		
		/**navigation bar visibility ends **/
		}
	class onclick_viewimage implements OnClickListener
	{
		String cover,insp;
		onclick_viewimage(String cover,String insp)
		{
			this.cover=cover;
			this.insp=insp;
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			System.out.println("the cover values="+cover+"insp="+insp);
			Intent in=new Intent(RoofSection.this,RoofImagePicking.class);
		    in.putExtra("roof_cover_img", true);
			in.putExtra("Cover", cover);
			in.putExtra("Insp_id",Roof_page_value[5]);
			in.putExtra("SRID",cf.selectedhomeid);
			in.putExtra("readonly",true);
			startActivityForResult(in, pick_img_code);
		}
		
	}
	private void loadDataRCT() {
		// TODO Auto-generated method stub
		String[] Roof_page_value=getRoofValueForVariables(Save_from);
		Cursor c=cf.SelectTablefunction(cf.Roof_covertype_view, " Where RCT_srid='"+cf.selectedhomeid+"' and RCT_insp_id='"+cf.encode(this.Roof_page_value[5])+"'  ORDER BY RCT_FielsName");
		
		String[] cover=null;
		if(c.getCount()<=0)
		{
			//Cursor c1=cf.SelectTablefunction(cf.Roof_covertype_view, " Where RCT_srid='"+cf.selectedhomeid+"' and RCT_page='"+cf.encode(Roof_page_value[0])+"' and RCT_Enable='1' ORDER BY RCT_FielsName ");
			Cursor c1=cf.SelectTablefunction(cf.Roof_covertype_view, " Where RCT_srid='"+cf.selectedhomeid+"' and RCT_insp_id='"+cf.encode(Roof_page_value[5])+"' and RCT_Enable='1' ORDER BY RCT_FielsName ");
			
			if(c1.getCount()>0)
			{
				//cover=new String[c1.getCount()];
				c1.moveToFirst();
				String cover1="",tmp = "";
				for(int i=0;i<c1.getCount();i++,c1.moveToNext())
				{
					if(!cover1.equals(c1.getString(c1.getColumnIndex("RCT_FielsName"))))
					{
						
						tmp+=c1.getString(c1.getColumnIndex("RCT_FielsName"))+",";
						cover1=c1.getString(c1.getColumnIndex("RCT_FielsName"));
						
						
					}
					
				}
				tmp=tmp.substring(0,tmp.length()-1);
				cover=tmp.split(",");
		}
			
			/**insert int ot the master table **/
		cf.arr_db.execSQL("Insert into "+cf.Roof_master+" (RM_inspectorid,RM_srid,RM_page,RM_insp_id,RM_Covering,RM_Enable)  " +
				"Select RM_inspectorid,RM_srid,RM_page,'"+cf.encode(this.Roof_page_value[5])+"',RM_Covering,RM_Enable from "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(Roof_page_value[5])+"' and RM_Enable='1' and RM_module_id='0' ");
		/**insert int ot the master table ends  **/
			Cursor c2=cf.SelectTablefunction(cf.option_list, " Where module_id='"+this.Roof_page_value[0]+"' and  insp_id='"+this.Roof_page_value[5]+"'");
			if(cover!=null)
			{
			for(int j=0;j<cover.length;j++)
			{
				c2.moveToFirst();
			for(int i=0;i<c2.getCount();i++)
				{
					
					try
					{	
						
					
						cf.arr_db.execSQL("INSERT INTO "+cf.Roof_cover_type+" (RCT_masterid,RCT_TypeValue,RCT_FieldVal,RCT_Other) SELECT (Select RM_masterid  as RCT_masterid from "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_Enable='1' and RM_Covering='"+cover[j]+"' and RM_module_id='0'),'"+c2.getInt(c2.getColumnIndex("option_tit"))+"' as RCT_TypeValue,RCT_FieldVal,RCT_Other  From "+cf.Roof_cover_type+" " +
								"WHERE (RCT_masterid=(Select RM_masterid from "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(Roof_page_value[5])+"' and RM_Enable='1' and RM_Covering='"+cover[j]+"' and RM_module_id='0' ))   and RCT_TypeValue='"+c2.getString(c2.getColumnIndex("option_tit"))+"'");
						Cursor c11=cf.arr_db.rawQuery("Select changes() as Row",null);
							c11.moveToFirst();
			
							if(c11.getInt(c11.getColumnIndex("Row"))<=0 && cover[j]!=null)
							{
								
									cf.arr_db.execSQL("INSERT INTO "+cf.Roof_cover_type+" (RCT_masterid,RCT_TypeValue,RCT_FieldVal,RCT_Other) VALUES((Select RM_masterid  as RCT_masterid from "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_Enable='1' and RM_Covering='"+cover[j]+"'and RM_module_id='0'),'"+c2.getString(c2.getColumnIndex("option_tit"))+"','','');");
								
							}
							c2.moveToNext();
							
					}
					catch (Exception e) {
						// TODO: handle exception
						System.out.println("the comes the erro "+e.getMessage());
					}
				}
			try
			{
			cf.arr_db.execSQL("INSERT INTO "+cf.Roofcoverimages+" (RIM_masterid,RIM_module_id,RIM_Path,RIM_Order,RIM_Caption) SELECT (Select RM_masterid  as RIM_masterid from "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_Enable='1' and RM_Covering='"+cover[j]+"' and RM_module_id='0'),RIM_module_id,RIM_Path,RIM_Order,RIM_Caption  From "+cf.Roofcoverimages+" " +
					"WHERE (RIM_masterid=(Select RM_masterid from "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(Roof_page_value[5])+"' and RM_Enable='1' and RM_Covering='"+cover[j]+"' and RM_module_id='0' )) ");
			}
			catch (Exception e) {
				// TODO: handle exception
				System.out.println("exceptin in the roof image  "+e.getMessage());
			}
			
		}
	}
				show_RCT_List(1);
		}
		else
		{
			c.moveToFirst();
			int enb=c.getInt(c.getColumnIndex("RCT_Enable"));
			show_RCT_List(enb);
		}
			/*}
		}
		else
		{
			c.moveToFirst();
			int enb=c.getInt(c.getColumnIndex("RCT_Enable"));
			show_RCT_List(enb);
		}*/
	}
	private void loadDataGRI() {
		// TODO Auto-generated method stub
			
		String[] Roof_page_value=getRoofValueForVariables(Save_from);
		//String sql="Select count() from "+cf.General_Roof_information+" Where RC_SRID='"+cf.selectedhomeid+"' and RC_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and ";
		Cursor c=cf.SelectTablefunction(cf.General_roof_view, " Where RC_SRID='"+cf.selectedhomeid+"' and RC_insp_id='"+cf.encode(this.Roof_page_value[5])+"'");
		
		if(c.getCount()<=0)
		{
			String sql="Select * from "+cf.General_roof_view+" join " +cf.option_list+
					" where module_id='1' and insp_id='"+this.Roof_page_value[5]+"' and RC_SRID='"+cf.selectedhomeid+"' and RC_insp_id='"+Roof_page_value[5]+"' and RC_Condition_type_order<>'11' and RC_Condition_type=option_tit";
			System.out.println("the RCN quesry="+sql);
			Cursor c1=cf.arr_db.rawQuery(sql, null);
					//cf.SelectTablefunction(cf.General_roof_view, " Where RC_SRID='"+cf.selectedhomeid+"' and RC_insp_id='"+cf.encode(Roof_page_value[5])+"' and RC_Condition_type_order<>'11' ");
			
		
			Cursor c2=cf.SelectTablefunction(cf.option_list, " Where module_id='"+this.Roof_page_value[1]+"' and insp_id='"+this.Roof_page_value[5]+"'  order by option_potion ");
		
			if(c1.getCount()>0)
			{
		
				Cursor c22=cf.SelectTablefunction(cf.Roof_master, " Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' ");
		
				if(c22.getCount()<=0)
				{
		
					cf.arr_db.execSQL("INSERT INTO "+cf.Roof_master+" (RM_inspectorid,RM_srid,RM_insp_id,RM_module_id) Values ('"+cf.Insp_id+"','"+cf.selectedhomeid+"','"+cf.encode(this.Roof_page_value[5])+"','1')");
				}
					
		
				c1.moveToFirst();
				c2.moveToFirst();
					for(int i=0;i<c2.getCount();i++)
					{
						try
						{
							
							cf.arr_db.execSQL("INSERT INTO "+cf.General_Roof_information+" (RC_masterid,RC_Condition_type,RC_Condition_type_val,RC_Condition_type_DESC,RC_Condition_type_order)" +
									" SELECT (SELECT RM_masterid as RC_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' ), " +
									"RC_Condition_type,RC_Condition_type_val,RC_Condition_type_DESC,RC_Condition_type_order From "+cf.General_Roof_information+" WHERE  RC_masterid=(SELECT RM_masterid  FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(Roof_page_value[5])+"' and RM_module_id='1' )  and RC_Condition_type='"+c2.getString(c2.getColumnIndex("option_tit"))+"'");
							Cursor c11=cf.arr_db.rawQuery("Select changes() as Row",null);
							c11.moveToFirst();
							
							if(c11.getInt(c11.getColumnIndex("Row"))<=0)
							{
								System.out.println("the quesr for empty=INSERT INTO "+cf.General_Roof_information+" (RC_masterid,RC_Condition_type,RC_Condition_type_val,RC_Condition_type_DESC,RC_Condition_type_order) VALUES((SELECT RM_masterid as RC_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' ),'"+cf.encode(c2.getString(c2.getColumnIndex("option_tit")))+"','"+cf.encode(c2.getString(c2.getColumnIndex("Default_option")))+"','',''");
								cf.arr_db.execSQL("INSERT INTO "+cf.General_Roof_information+" (RC_masterid,RC_Condition_type,RC_Condition_type_val,RC_Condition_type_DESC,RC_Condition_type_order) VALUES((SELECT RM_masterid as RC_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' ),'"+cf.encode(c2.getString(c2.getColumnIndex("option_tit")))+"','"+cf.encode(c2.getString(c2.getColumnIndex("Default_option")))+"','','')");
							}
						}
						catch (Exception e) {
							// TODO: handle exception
							System.out.println("the comes the erro "+e.getMessage());
						}
						c2.moveToNext();
						
					}
						show_GRID_Value(1);
				
			}
			else
			{
				show_GRID_Value(1);
			}
		}
		else
		{
			c.moveToFirst();
			
			show_GRID_Value(1);
		}
		
	}
	private void loadDataAdd() {
		// TODO Auto-generated method stub
		
		getRoofValueForVariables(Roof);
		String[] add_tit=Roof_additional_txt;
		String[] Roof_page_value=getRoofValueForVariables(Save_from);
		Cursor c22=cf.SelectTablefunction(cf.Roof_master, " Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' ");
	
		if(c22.getCount()<=0)
		{
			cf.arr_db.execSQL("INSERT INTO "+cf.Roof_master+" (RM_inspectorid,RM_srid,RM_insp_id,RM_module_id) Values ('"+cf.Insp_id+"','"+cf.selectedhomeid+"','"+cf.encode(this.Roof_page_value[5])+"','1')");
		}
			for(int i=0;i<add_tit.length;i++)
			{
				
				cf.arr_db.execSQL("INSERT INTO "+cf.General_Roof_information+" (RC_masterid,RC_Condition_type,RC_Condition_type_val,RC_Condition_type_DESC,RC_Condition_type_order) SELECT (SELECT RM_masterid as RC_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' ),RC_Condition_type,RC_Condition_type_val,RC_Condition_type_DESC,RC_Condition_type_order From "+cf.General_Roof_information+" WHERE  RC_masterid= (SELECT RM_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(Roof_page_value[5])+"' and RM_module_id='1' )  and RC_Condition_type='"+cf.encode(add_tit[i])+"' ");
				
			}
			
			cf.arr_db.execSQL("INSERT INTO "+cf.General_Roof_information+" (RC_masterid,RC_Condition_type,RC_Condition_type_val,RC_Condition_type_DESC,RC_Condition_type_order,RC_Enable) SELECT (SELECT RM_masterid as RC_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' ) ,RC_Condition_type,RC_Condition_type_val,RC_Condition_type_DESC,RC_Condition_type_order,'1' as RC_Enable From "+cf.General_Roof_information+" WHERE  RC_masterid= (SELECT RM_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(Roof_page_value[5])+"' and RM_module_id='1' ) and  RC_Condition_type_order='12' and RC_Condition_type<>'"+cf.encode("updatedDate")+"' ");
			
	}

      
	private String[] getRoofValueForVariables(String Roof) {
		// TODO Auto-generated method stub
		String Roof_page_value[]=new String[7];
		cf.loadinsp_o="Overall Roof Commnets";
		Roof_page_value[0]="2";
		 Roof_page_value[1]="1";
		 Roof_page_value[2]="3";
		 colomprerow=6;
		if(Roof.equals(cf.Roof_insp[0]))
		 {
			/*** For 4poin***/
			 
			 Roof_page_value[3]=cf.RCT_tit;
			 Roof_page_value[4]="Other Damage Noted";
			 Roof_page_value[5]="1";
			 cf.loadinsp_q=cf.getResourcesvalue(R.string.four_1);
			 cf.loadinsp_n=cf.All_insp_list[1];
			 Roof_additional_txt = getResources().getStringArray(R.array.Roof_4point_additional);
			 /****For 4poin ends**/
	      }
		 else if(Roof.equals(cf.Roof_insp[1]))
		 {
			/*** Roof Survey***/
			
			 Roof_page_value[3]=cf.RCT_tit;
			 Roof_page_value[4]="Other Damage Noted";
			 Roof_page_value[5]="2";
			 cf.loadinsp_n=cf.All_insp_list[2];
			 cf.loadinsp_q=cf.getResourcesvalue(R.string.roof_1);
			 Roof_additional_txt = getResources().getStringArray(R.array.Roof_roofsurvey_additional);
			 /****Roof Survay ends**/
			 
		 }
		 else  if(Roof.equals(cf.Roof_insp[4]))
		 {
				   
			 /*** For GCH***/
			 Roof_page_value[3]=cf.RCT_tit;
			 Roof_page_value[4]="Other Damage Noted";
			 cf.loadinsp_q=cf.getResourcesvalue(R.string.gch_3);
			 Roof_page_value[5]="7";
			 Roof_additional_txt = getResources().getStringArray(R.array.Roof_GCH_additional);
			// colomprerow=5;
			 cf.loadinsp_n=cf.All_insp_list[7];
			 /***	 For GCH Ends***/
		 }
		 else if(Roof.equals(cf.getResourcesvalue(R.string.insp_5)))
		 {
			
			/*** Roof Survey***/
			 Roof_page_value[3]=cf.RCT_tit;
			 Roof_page_value[4]="Is there any damage to the roof structure (Cuts,Holes,etc)?";
			 cf.loadinsp_q=cf.getResourcesvalue(R.string.com2_2);
			 Roof_page_value[5]="5";
			 Roof_additional_txt = getResources().getStringArray(R.array.Roof_commercial_additional);
			 cf.loadinsp_n=cf.All_insp_list[5];
			 		 /****Roof Survay ends**/
		 } 
		 else if(Roof.equals(cf.getResourcesvalue(R.string.insp_6)))
		 {
			
			/*** Roof Survey***/
			 Roof_page_value[3]=cf.RCT_tit;
			 Roof_page_value[4]="Is there any damage to the roof structure (Cuts,Holes,etc)?";
			 cf.loadinsp_q=cf.getResourcesvalue(R.string.com2_2);
			 Roof_page_value[5]="6";
			 Roof_additional_txt = getResources().getStringArray(R.array.Roof_commercial_additional);
			 cf.loadinsp_n=cf.All_insp_list[6];
			  /****Roof Survay ends**/
		 }
		 else if (Roof.equals(cf.getResourcesvalue(R.string.insp_4)))
		 {
			
			/*** Roof Survey***/
			 Roof_page_value[3]=cf.RCT_tit;
			 Roof_page_value[4]="Other Damage Noted";//"Other Condition Present";
			 cf.loadinsp_q=cf.getResourcesvalue(R.string.com2_2);
			 Roof_page_value[5]="4";
			 cf.loadinsp_n=cf.All_insp_list[4];
			 Roof_additional_txt = getResources().getStringArray(R.array.Roof_4point_additional);
			  /****Roof Survay ends**/
		 }
        else 
		 {
			 /*** For GCH***/
			
			 Roof_page_value[3]=cf.RCT_tit;
			 Roof_page_value[4]="Other Condition Present";
			 cf.loadinsp_q=cf.getResourcesvalue(R.string.gch_3);
			 Roof_page_value[5]="7";
			 Roof_additional_txt = getResources().getStringArray(R.array.Roof_GCH_additional);
		 }
		return Roof_page_value;
	}


	private void show_GRI_Value() {
	
	
			// TODO Auto-generated method stub
	    	this.setTheme(R.style.AppTheme);
	    
	    	boolean de=false;
	    	/***Show all roof condition noted**/
	    	
	    	if(Roof.equals(Save_from) )
			{
	    		
	    		
	    		show_GRID_Value(1);
	    		
			}
			else
			{
				
				loadDataGRI();
				loadDataAdd();
			}
	    
	    	   	/***Show all roof condition noted ends**/
	    	/**Loading Addional starts**/
	    	System.out.println("comes correctly1");	
	     	Cursor	RC_GRI_2=cf.SelectTablefunction(cf.General_roof_view, " WHERE RC_SRID='"+cf.selectedhomeid+"'  and RC_insp_id='"+cf.encode(Roof_page_value[5])+"'  and RC_Condition_type_order='11' and RC_Condition_type<>'"+cf.encode("Comments")+"' ");
	     	RC_GRI_2.moveToFirst();
	     	
	     	
	     	
	     	if(RC_GRI_2.getCount()>0)
	     	{
	     		check_alert_add="true";
	     		//ADD_layout_head.setVisibility(View.GONE);
	     		if(!Roof.equals(cf.Roof_insp[0]) && !Roof.equals(cf.Roof_insp[1]) && !Roof.equals(cf.Roof_insp[5]))
	     		{
	     			//ADD_chk_top.setChecked(false);
	     			//ADD_layout_head.setVisibility(View.VISIBLE);	
	     			
	     			if(RC_GRI_2.getInt(RC_GRI_2.getColumnIndex("RC_Enable"))==2)
	     			{
	     				((TextView) findViewById(R.id.ADD_comp)).setVisibility(View.VISIBLE);
	     			}
	     			
	     		}
	     			/*else if(RC_GRI_2.getCount()>2)
	     		{
	     			ADD_chk_top.setChecked(true);
	     		}*/
	     	}
	     	else 
	     	{
	     		//ADD_chk_top.setChecked(true);
	     		ADD_layout_head.setVisibility(View.GONE);	
	     	}
	     	
	     	
	     	System.out.println("comes correctly1.0"+RC_GRI_2.getCount());
	     	if(RC_GRI_2.getCount()>0 )
	     	{
	     		
	     		
	     		
	     		for(int i=0;i<RC_GRI_2.getCount();i++)
	     		{
	     			
	     			
	     			String Val_tit=cf.decode(RC_GRI_2.getString(RC_GRI_2.getColumnIndex("RC_Condition_type"))).trim();
	     			String Value=cf.decode(RC_GRI_2.getString(RC_GRI_2.getColumnIndex("RC_Condition_type_val"))).trim();
	     			String Value_desc=cf.decode(RC_GRI_2.getString(RC_GRI_2.getColumnIndex("RC_Condition_type_DESC"))).trim();
	     			
	     			
		     	 if(Val_tit.equals("Comments"))
					{
		     		System.out.println("comments page1="+Value_desc);
						RC_cmt.setText(Value_desc);
					}
		     	 else if(Val_tit.equals(cf.getResourcesvalue(R.string.roof_add_4poin_txt2)))
		       	 {
		     		 cf.setvaluechk1(Value,RC_RSurvay_chk, null);
		     		/*ArrayAdapter adapter;
					adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, survay);
		     		 RC_Survay.setSelection(adapter.getPosition(Value));
		     		*/
		     	
		     	 }
		     	else if(Val_tit.equals(cf.getResourcesvalue(R.string.roof_add_4poin_txt)))
		      	 {
		     		
		     	
		    		String val=Value;
		    		
		     		if(!val.equals(""))
		     		{
		     			
		     		if(val.equals("Yes") || val.equals("No"))
		     		{
		     			((RadioButton)ADD_RG1.findViewWithTag(val)).setChecked(true);
		     			//cf.setvaluechk1(Value_desc, Add_Y_N, null);
		     			
		     		} 
		     		//ADD_chk_top.setChecked(false);
		     		ADD_layout_head.setVisibility(View.VISIBLE);
		     		if(RC_GRI_2.getInt(RC_GRI_2.getColumnIndex("RC_Enable"))==2)
	     			{
		     			((TextView) findViewById(R.id.ADD_comp)).setVisibility(View.VISIBLE);
	     			}
		     		
		    		 
		     		} 
		    		 
		    	 }
		     
		     	else if(Val_tit.equals(cf.getResourcesvalue(R.string.ROOF_Survey5)))
		     	{
		     		
		     		
		     		((RadioButton)ADD_com2_RG.findViewWithTag(Value)).setChecked(true);
		     	}
		     	else if(Val_tit.equals(cf.getResourcesvalue(R.string.COMER2)) )
		     	{
		     		
		     		
		     		((RadioButton)ADD_com2_PDU_RG.findViewWithTag(Value)).setChecked(true);
		     		if(Value.equals("Yes"))
		     		{
		     			if(!Value_desc.equals(""))
		     			{
		     				String[] tmp=Value_desc.split("#94");
		     				if(tmp[0].contains(":"))
		     				{
		     					ADD_com2_PDU_no_ed.setText(tmp[0].substring(tmp[0].indexOf(":")+1));
		     						
		     				}
		     				if(tmp[1].contains(":"))
		     				{
		     					ADD_com2_PDU_date_ed.setText(tmp[1].substring(tmp[1].indexOf(":")+1));
		     						
		     				}
		     				
		     			}
		     		}
		     	}
		     	else if(Val_tit.equals(cf.getResourcesvalue(R.string.COMER4)))
		     	{
		     		
		     		((RadioButton)ADD_com2_RME_RG.findViewWithTag(Value)).setChecked(true);
		     	}
		     	else if(Val_tit.equals(cf.getResourcesvalue(R.string.COMER3)))
		     	{
		     	
		     		
		     		cf.setvaluechk1(Value, ADD_com2_RMS,null);
		     		if(!Value_desc.equals(""))
		     		{
		     			
		     			Value_desc=Value_desc.replace("#94", " #94 ");
		     			String tmp[]=Value_desc.split("#95");
		     			
		     			String percent[]=tmp[0].split("#94");
		     			
		     			String loc[]=tmp[1].split("#94");
		     				
		     			
		     		
		     		
		     			if(!percent[0].trim().equals(""))
		     			{
		     				ADD_com2_RMS_HVAC_ed.setText(percent[0].trim());
		     				RMS_loc[0].setText(loc[0].trim());
		     				findViewById(R.id.RMS_chk1_details).setVisibility(View.VISIBLE);
		     			}
		     			else
		     			{
		     				findViewById(R.id.RMS_chk1_details).setVisibility(View.INVISIBLE);
		     			}
		     		
		     			if(!percent[1].trim().equals(""))
		     			{
		     				ADD_com2_RMS_VE_ed.setText(percent[1].trim());
		     				RMS_loc[1].setText(loc[1].trim());
		     				findViewById(R.id.RMS_chk2_details).setVisibility(View.VISIBLE);
		     			}
		     			else
		     			{
		     				findViewById(R.id.RMS_chk2_details).setVisibility(View.INVISIBLE);
		     			}
		     		
		     			if(!percent[2].trim().equals(""))
		     			{
		     				ADD_com2_RMS_SE_ed.setText(percent[2].trim());
		     				RMS_loc[2].setText(loc[2].trim());
		     				findViewById(R.id.RMS_chk3_details).setVisibility(View.VISIBLE);
		     			}
		     			else
		     			{
		     				findViewById(R.id.RMS_chk3_details).setVisibility(View.INVISIBLE);
		     			}
		     			
		     			
		     			if(!percent[3].trim().equals(""))
		     			{
		     				ADD_com2_RMS_OE_ed.setText(percent[3].trim());
		     				findViewById(R.id.RMS_chk8_details).setVisibility(View.VISIBLE);
		     			}
		     			else
		     			{
		     				findViewById(R.id.RMS_chk8_details).setVisibility(View.INVISIBLE);
		     			}
		     			
		     			
		     		}
		     		
		     	}
		     	else if(Val_tit.equals(cf.getResourcesvalue(R.string.EquipmentL)))
		     	{
		     		if(!Value.equals(""))
		     		{
		     			((RadioButton)RMS_RG[0].findViewWithTag(Value)).setChecked(true);
		     		}
		     		
		     	}
	     		else if(Val_tit.equals(cf.getResourcesvalue(R.string.DamageR)))
		     	{
		     		
	     			if(!Value.equals(""))
		     		{
	     				((RadioButton)RMS_RG[1].findViewWithTag(Value)).setChecked(true);
		     		}
		     		
		     		
		     	}
		     	else if(Val_tit.equals(cf.getResourcesvalue(R.string.COMER5)))
		     	{
		     		
		     		ADD_com2_Other.setText(Value);
		     	} 

		     	RC_GRI_2.moveToNext();
	    	 }
	     		/**Loading Additional ends**/
	     		
	     	}
	     	else
	     	{
	     	
	     		
	     		try
	     		{
	     		/***Auto populate the building information **/
	     		cf.CreateARRTable(61);
	     		Cursor c =cf.arr_db.rawQuery("Select BI_BOCCU_STYPE from "+cf.BI_General+" Where fld_srid='"+cf.selectedhomeid+"'", null);
	     		if(c.getCount()>0)
	     		{
	     			c.moveToFirst();
	     			String soi=cf.decode(c.getString(c.getColumnIndex("BI_BOCCU_STYPE")));
	     			if(!soi.trim().equals(""))
	     			{
		     			String[] Soi_val=soi.split("&#126;");
		     			String[] scopeofinsp=Soi_val[1].split("&#94;");
		     			String[] SOI_arr=getResources().getStringArray(R.array.SOI_array);
		     			String value="";
		     			for(int i=0;i<scopeofinsp.length;i++)
		     			{
		     				for(int j=0;j<SOI_arr.length;j++)
			     			{
		     					if(SOI_arr[j].trim().equals(scopeofinsp[i].trim()))
		     					{
		     						value+=scopeofinsp[i].trim()+"&#94;";
		     					}
			     			}
		     				
		     			}
		     			if(!value.equals(""))
		     			{
		     				cf.setvaluechk1(value,RC_RSurvay_chk, null);
		     			}
	     			}
	     		}
	     		}catch (Exception e) {
					// TODO: handle exception
				}
	     		 
	     		/***Auto populate the building information ends **/
	     	}
	     	System.out.println("comes correctly2");
	     	/**Loading commentds Starts**/
     		RC_GRI_2=cf.SelectTablefunction(cf.General_roof_view, " WHERE RC_SRID='"+cf.selectedhomeid+"'  and RC_insp_id='"+cf.encode(Roof_page_value[5])+"'  and RC_Condition_type_order='11' and RC_Condition_type='"+cf.encode("Comments")+"' ");	
     		if(RC_GRI_2.getCount()>0)
     		{   RC_GRI_2.moveToFirst();
     			String Val_tit=cf.decode(RC_GRI_2.getString(RC_GRI_2.getColumnIndex("RC_Condition_type"))).trim();
     			String Value_desc=cf.decode(RC_GRI_2.getString(RC_GRI_2.getColumnIndex("RC_Condition_type_DESC"))).trim();
     			System.out.println("comments page2="+Value_desc);
     	 	 if(Val_tit.equals("Comments"))
				{
	     		 
					RC_cmt.setText(Value_desc);
				}
     			
     			
     		}
     	/**Loading commentds ends**/
     		
     		RC_GRI_2=cf.SelectTablefunction(cf.General_roof_view, " WHERE RC_SRID='"+cf.selectedhomeid+"'  and RC_insp_id='"+cf.encode(Roof_page_value[5])+"'  and RC_Condition_type_order='12' ");	
     		if(RC_GRI_2.getCount()>0)
     		{   RC_GRI_2.moveToFirst();
     		//and (RC_Condition_type='"+cf.encode("ADD_notapplicable")+"' ||
     			for(int i=0;i<RC_GRI_2.getCount();i++)
     			{
     				String Val_tit=cf.decode(RC_GRI_2.getString(RC_GRI_2.getColumnIndex("RC_Condition_type"))).trim();
     				String value=cf.decode(RC_GRI_2.getString(RC_GRI_2.getColumnIndex("RC_Condition_type_val"))).trim();
     				if(Val_tit.equals("ADD_notapplicable"))
     				{
	         			if(value.equals("true"))
	         			{
	         				ADD_chk_top.setChecked(true);
	         				ADD_layout_head.setVisibility(View.GONE);
	         				if(RC_GRI_2.getInt(RC_GRI_2.getColumnIndex("RC_Enable"))==2)
	    	     			{
	    	     				((TextView) findViewById(R.id.ADD_comp)).setVisibility(View.VISIBLE);
	    	     			}
	         				
	         			}
	         			else
	         			{
	         				ADD_chk_top.setChecked(false);
	         				ADD_layout_head.setVisibility(View.VISIBLE);
	         				ADD_expend.setTag("minus");
	         				ADD_expend.setImageDrawable(getResources().getDrawable(R.drawable.minus));
	         			}
     				}
     				else if(Val_tit.equals("RCN_notapplicable"))
         				{
    	         			if(value.equals("true"))
    	         			{
    	         				RCN_chk_top.setChecked(true);
    	         				RCN_layout_head.setVisibility(View.GONE);
    	         				if(RC_GRI_2.getInt(RC_GRI_2.getColumnIndex("RC_Enable"))==2)
    	    	     			{
    	    	     				((TextView) findViewById(R.id.RCN_comp)).setVisibility(View.VISIBLE);
    	    	     			}
    	         			}
    	         			else
    	         			{
    	         				RCN_chk_top.setChecked(false);
    	         				RCN_layout_head.setVisibility(View.VISIBLE);
    	         				RCN_expend.setTag("minus");
    	         				RCN_expend.setImageDrawable(getResources().getDrawable(R.drawable.minus));
    	         			}
         				}
     				else if(Val_tit.equals("ICCF"))
     				{
     					try
     					{
	     					if(value.equals("true"))
		         			{
	     						citi_inclu.setChecked(true);
		         				
		         			}
		         			else
		         			{
		         				citi_inclu.setChecked(false);
		         				
		         			}

     					}catch (Exception e) {
							// TODO: handle exception
     						System.out.println("the erro "+e.getMessage());
						}
     				}
         			RC_GRI_2.moveToNext();
     			}
     			
     		}
     		
     		
	   }


	private void show_GRID_Value(int enbl) {
	

		/**Set the default values **/
		String sql="Select GRI.*,mst.mst_text as title  From "+cf.General_roof_view+" as GRI LEFT JOIN "+cf.option_list+" as optl on optl.option_tit=GRI.RC_Condition_type and module_id='1' and insp_id='"+this.Roof_page_value[5]+"'  LEFT JOIN "+cf.master_tbl+" mst  on mst_custom_id=option_tit and mst_module_id='2'  WHERE RC_SRID='"+cf.selectedhomeid+"' and RC_insp_id='"+cf.encode(Roof_page_value[5])+"' and RC_Condition_type_order<>'10' and RC_Condition_type_order<>'11' and RC_Condition_type_order<>'12'";
		System.out.println("The selected query "+sql);
		Cursor RC_GRI=cf.arr_db.rawQuery(sql, null);
		System.out.println("the count "+RC_GRI.getCount());
		sql="SELECT opt.*,mst.mst_text as title FROM "+cf.option_list+" opt LEFT JOIN "+cf.master_tbl+" mst  on mst_custom_id=option_tit and mst_module_id='2' WHERE module_id='"+Roof_page_value[1]+"' and insp_id='"+this.Roof_page_value[5]+"' ORDER BY option_potion";
		
		RCN_c =cf.arr_db.rawQuery(sql, null);
			
		sql="SELECT opt.*,mst.mst_text as title FROM "+cf.option_list+" opt LEFT JOIN "+cf.master_tbl+" mst  on mst_custom_id=option_tit and mst_module_id='2' WHERE module_id='"+Roof_page_value[1]+"' and insp_id='"+this.Roof_page_value[5]+"' and option_tit=(Select mst_custom_id from "+cf.master_tbl+" WHERE mst_text='"+cf.encode(Roof_page_value[4])+"')";
		
		Cursor default_c =cf.arr_db.rawQuery(sql, null);
			
		
		default_c.moveToFirst();
		int option_max_l,option_max_W,other_l,other_w,tit_max_w,Requirred,option_potion,id;
		String option_tit,options,option_other,option_type,Default_option,option_filter,Other_filter,Other_desc;
		option_tit=options=option_other=option_type=Default_option=option_filter=Other_filter=Other_desc="";
		id=option_max_l=option_max_W=other_l=other_w=tit_max_w=Requirred=option_potion=0;
		TableLayout.LayoutParams row_params = new TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
		row_params.setMargins(0, 5, 0, 5);
		TableRow.LayoutParams lin_params_tit = new TableRow.LayoutParams(250,LayoutParams.WRAP_CONTENT);
		
		TableRow.LayoutParams lin_params_ops = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
		TableRow.LayoutParams lin_params_ops_Rg = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
		TableRow.LayoutParams lin_params_yes = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
		int Count;
		RCN_c.moveToFirst();
		
		if(RC_GRI.getCount()>0)
		{
			check_alert_rcn="true";
			Count=RC_GRI.getCount();
			tbl_id=new int[RC_GRI.getCount()];
			RC_GRI.moveToFirst();
			if(RC_GRI.getInt(RC_GRI.getColumnIndex("RC_Enable"))==2)
			{
				((TextView) findViewById(R.id.RCN_comp)).setVisibility(View.VISIBLE);
			}
			/*RCN_chk_top.setChecked(false);
			RCN_layout_head.setVisibility(View.VISIBLE);*/	
			
		}
		else
		{
			Count=RCN_c.getCount();
			tbl_id=new int[RCN_c.getCount()];
			RCN_layout_head.setVisibility(View.GONE);	
			//RCN_chk_top.setChecked(true);
		}
		
		String[] opt=null;
		/**Set the default values **/
		/***Start to set the value to the respective colomns **/
		
		for(int i=0;i<Count;i++)
		{
			
		
			if(RC_GRI.getCount()>0)
			{/**Check for whether the value already stored in the DB **/
				if(RCN_c.getCount()>i)
				{
				/***Get the configration from the option list table ***/
					
					options=RCN_c.getString(RCN_c.getColumnIndex("options"));
					option_other=RCN_c.getString(RCN_c.getColumnIndex("option_other"));
					option_type=RCN_c.getString(RCN_c.getColumnIndex("option_type"));
					option_max_l=RCN_c.getInt(RCN_c.getColumnIndex("option_max_l"));
					option_max_W=RCN_c.getInt(RCN_c.getColumnIndex("option_max_W"));
					other_l=RCN_c.getInt(RCN_c.getColumnIndex("other_l"));
					other_w=RCN_c.getInt(RCN_c.getColumnIndex("other_w"));
					tit_max_w=RCN_c.getInt(RCN_c.getColumnIndex("tit_max_w"));
					Requirred=RCN_c.getInt(RCN_c.getColumnIndex("Requirred"));
					option_filter=RCN_c.getString(RCN_c.getColumnIndex("option_filter"));
					opt=options.split(",");
					
					/***Get the configration from the option list table ends ***/
				
				}else
				{
					if(default_c.getCount()>0)
					{
						/***The Row was added using Add more option mean we set the other condtion config to that all  ***/	
					options=default_c.getString(default_c.getColumnIndex("options"));
					option_type=default_c.getString(default_c.getColumnIndex("option_type"));
					option_other=default_c.getString(default_c.getColumnIndex("option_other"));
					option_max_l=default_c.getInt(default_c.getColumnIndex("option_max_l"));
					option_max_W=default_c.getInt(default_c.getColumnIndex("option_max_W"));
					other_l=default_c.getInt(default_c.getColumnIndex("other_l"));
					other_w=default_c.getInt(default_c.getColumnIndex("other_w"));
					tit_max_w=default_c.getInt(default_c.getColumnIndex("tit_max_w"));
					Requirred=default_c.getInt(default_c.getColumnIndex("Requirred"));
					option_filter=default_c.getString(default_c.getColumnIndex("option_filter"));
					opt=options.split(",");
					/***The Row was added using Add more option mean we set the other condtion config to that all  ends ***/
					}
				}
				/***Get the value from the table   ***/
				
				Default_option=cf.decode(RC_GRI.getString(RC_GRI.getColumnIndex("RC_Condition_type_val")));
				try
				{
					option_tit=cf.decode(RC_GRI.getString(RC_GRI.getColumnIndex("title")));
				}
				catch (Exception e) {
					// TODO: handle exception
					
					option_tit="";
				}
				
				if(option_tit==null)
				{
					option_tit=cf.decode(RC_GRI.getString(RC_GRI.getColumnIndex("RC_Condition_type")));
					
				}
				else if(option_tit.equals(""))
				{
					option_tit=cf.decode(RC_GRI.getString(RC_GRI.getColumnIndex("RC_Condition_type")));
					
				}
				
				Other_desc=cf.decode(RC_GRI.getString(RC_GRI.getColumnIndex("RC_Condition_type_DESC")));
				id=RC_GRI.getInt(RC_GRI.getColumnIndex("RC_Id"));
				/***Get the value from the table ends   ***/
			}
			else
			{
				/***Get the Configration from the option list table   ***/
				option_tit=cf.decode(RCN_c.getString(RCN_c.getColumnIndex("title")));
				options=RCN_c.getString(RCN_c.getColumnIndex("options"));
				option_other=RCN_c.getString(RCN_c.getColumnIndex("option_other"));
				option_type=RCN_c.getString(RCN_c.getColumnIndex("option_type"));
				Default_option=RCN_c.getString(RCN_c.getColumnIndex("Default_option"));
				option_max_l=RCN_c.getInt(RCN_c.getColumnIndex("option_max_l"));
				option_max_W=RCN_c.getInt(RCN_c.getColumnIndex("option_max_W"));
				other_l=RCN_c.getInt(RCN_c.getColumnIndex("other_l"));
				other_w=RCN_c.getInt(RCN_c.getColumnIndex("other_w"));
				tit_max_w=RCN_c.getInt(RCN_c.getColumnIndex("tit_max_w"));
				Requirred=RCN_c.getInt(RCN_c.getColumnIndex("Requirred"));
				opt=options.split(",") ;
	    		id=RCN_c.getInt(RCN_c.getColumnIndex("option_tit"));
	    		option_filter=RCN_c.getString(RCN_c.getColumnIndex("option_filter"));
	    		Other_desc="";
	    		/***Get the Configration from the option list table ends  ***/
	    		
			}
		
			TableRow tbl=new TableRow(this);
    		tbl.setId(1000+id);/***Set the dynamic id for the Each Row for retrive the value ***/
    		//System.out.println(option_tit+"the id ="+1000+id);
    		tbl_id[i]=tbl.getId();
    	
    		/** Create a Title of the event **/
    		LinearLayout title_li=new LinearLayout(this);
    		title_li.setOrientation(LinearLayout.VERTICAL);
    		TextView tit_tv=new TextView(this,null,R.attr.Roof_RCN_Tit_txt);
    		tit_tv.setWidth(0);
    		TextView tit_star=new TextView(this,null,R.attr.star);
			tit_star.setTag("Star");
			
			tit_star.setPadding(0, 0, 5, 0);
			tbl.addView(tit_star);
    		if(Requirred==1)
    		{/***If the question was requirred mean we set the star for the fields titles **/
    			
    			tit_star.setVisibility(View.VISIBLE);
    		}
    		else
    		{
    			tit_star.setVisibility(View.INVISIBLE);
    			//tit_tv.setText(option_tit);
    		}
			tit_tv.setText(option_tit);
			
    		title_li.addView(tit_tv,tit_max_w,LayoutParams.WRAP_CONTENT);
    		tit_tv.setTag("Title_txt");
    		
    		EditText tit_ed=new EditText(this,null,R.attr.Roof_RCN_Tit_ed);
    		tit_ed.setTag("Title_ed");
    	
    		if(i<RCN_c.getCount())
    		{
    			InputFilter[] FilterArray = new InputFilter[1];
    			FilterArray[0] = new InputFilter.LengthFilter(150);
    			tit_ed.setFilters(FilterArray);
    			
    		}
    		tit_ed.setText(option_tit);
    		title_li.addView(tit_ed,tit_max_w,LayoutParams.WRAP_CONTENT);
    	
    		
    		
    	
    		/** Create a Title of the event Ends**/
    		tbl.addView(title_li,lin_params_tit);
    		TextView colon_tv=new TextView(this,null,R.attr.colon_withoutmargin);
    		tbl.addView(colon_tv);
    		
    		/** Create a option of the event **/
    		LinearLayout op_li = null;
    		
    		if(option_type.equals("Radio"))
    		{
    			
    		 op_li=Add_ROption_dynamic(tbl.getId(), opt, option_max_l,option_max_W,option_other);
    		 
    		 if(!Default_option.equals(""))
				{
					((RadioButton) op_li.findViewWithTag(Default_option)).setChecked(true);
			
					
				}
    		 
    		 }
    		else if(option_type.equals("CheckBox"))
    		{
    			op_li=Add_CHKption_dynamic(tbl.getId(), opt, option_max_l,option_max_W,option_other);
    			if(!Default_option.equals(""))
				{
					
					Setcheckvalue(op_li,Default_option,",");
					
				}
    		}
    		else if(option_type.equals("Spinner"))
			{
				 op_li=Add_Spinption_dynamic(tbl.getId(), opt,option_max_W,option_other);
				if(!Default_option.equals(""))
				{
					for(int m=0;m<opt.length;m++)
					{
					 if(opt[m].trim().equals(Default_option))
					 {
						 ((Spinner)op_li.findViewWithTag("Spinner_val")).setSelection(m);
					 }
					}
				}
			}
			else if(option_type.equals("DatePicker"))
			{
				 op_li=Add_DPinption_dynamic(tbl.getId(),option_max_W);
				 if(!Default_option.equals(""))
					{
					 ((EditText)op_li.findViewWithTag("Date")).setText(Default_option);
					}
			}
			else if(option_type.equals("EditText"))
			{
				 op_li=Add_ETinption_dynamic(tbl.getId(),option_max_W,option_max_l,option_filter);
				 if(!Default_option.equals(""))
					{
					 ((EditText)op_li.findViewWithTag("EditText")).setText(Default_option);
					}
			}
    		/**Start to set the Radio buttons for the radio group endss**/
    		
    		EditText yes_ed = null;
    		/***Set the Other text or description to thre fields ***/
    		if(!option_other.trim().equals(""))
    		{
	    		LinearLayout yes_li=new LinearLayout(this);
	    		yes_li.setLayoutParams(lin_params_yes);
	    		yes_li.setOrientation(LinearLayout.VERTICAL);
	    		yes_ed=new EditText(this,null,R.attr.Roof_RCN_yes_ed);
	    		yes_li.addView(yes_ed);
	    		yes_ed.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
						// TODO Auto-generated method stub
						((TextView) v).setFocusable(true);
						((TextView) v).setFocusableInTouchMode(true);
						((TextView) v).requestFocus();
					}
				});
	    		yes_ed.setTag("Other_DESC");
	    		TextView yes_tv=new TextView(this,null,R.attr.Roof_RCN_yes_txt);
	    		//yes_tv.setTextColor(R.color.pink_tv);
	    		yes_li.addView(yes_tv);
	    		yes_li.setTag("Other_DESC_li");
	    		yes_tv.setTag("Other_DESC_txt");
	    		yes_li.setVisibility(View.GONE);
	    		op_li.addView(yes_li);
	    		InputFilter[]	FilterArray = new InputFilter[1];
				FilterArray[0] = new InputFilter.LengthFilter(other_l);
				yes_ed.setFilters(FilterArray);
				yes_ed.addTextChangedListener(new RCT_watcher(tbl_id[i],other_l));
	    		if(Default_option.equals(option_other))
	    		{
	    			yes_li.setVisibility(View.VISIBLE);
	    		}
    		}
    		/***Set the Other text or description to thre fields ends ***/
    		
    		tbl.addView(op_li);
    		
    		/** Create  option of the event Ends**/
    		ImageView tit_img=new ImageView(this,null,R.attr.Roof_RCN_Tit_img);
    		
    		tit_img.setPadding(0, 0, 5, 0);
    		tbl.addView(tit_img);
    		ImageView opt_img=new ImageView(this,null,R.attr.Roof_RCN_opt_img);
    		tbl.addView(opt_img);
    		/**Check for the question was from the default or not if not mean we enable the edit and delete option for that question**/
    		if(i>=RCN_c.getCount())
    		{
	    		tit_img.setVisibility(View.VISIBLE);
	    		opt_img.setVisibility(View.VISIBLE);
	    		opt_img.setOnClickListener(new RCN_clicker(2,tbl.getId()));
	    		tit_img.setOnClickListener(new RCN_clicker(1,tbl.getId()));
	    	}
    		/***Set the id of the question for the update process **/
    		TextView id_tv1=new TextView(this,null,R.attr.Roof_RCN_yes_txt);
    		id_tv1.setVisibility(View.GONE);
    		id_tv1.setTag("GCH_RCT_id");
    		tbl.addView(id_tv1);
    		
    		RCN_ShowValue.addView(tbl,row_params);
    		
    		/***Set the id of the question for the update process ends **/
    		if(yes_ed!=null)
    			yes_ed.setText(Other_desc);
    		if(RC_GRI.getCount()>0)
			   		id_tv1.setText(id+"");
    		else
    			id_tv1.setText("");
    		/**Customized validation for the Commerciala 2 ***/
    		
    		if(option_tit.equals(amt_tit[0]))
    		{
    			amt[0]=tbl.getId();
    		}
    		else if(option_tit.equals(amt_tit[1]))
    		{
    			tbl.setVisibility(View.GONE);
    			amt[1]=tbl.getId();
    			
    		}
    		if(option_tit.equals(everReplaced_tit[0]))
    		{
    			everReplaced[0]=tbl.getId();
    		}
    		else if(option_tit.equals(everReplaced_tit[1]))
    		{
    			tbl.setVisibility(View.GONE);
    			everReplaced[1]=tbl.getId();
    		}	
    		if(option_tit.equals(has_roof[0]))
    		{
    			hasroof[0]=tbl.getId();
   					
    		}
    		if(option_tit.equals(has_roof[1]))
    		{
    			hasroof[1]=tbl.getId();
    			tbl.setVisibility(View.GONE);
    		}
    		if(option_tit.equals(over_all))
    		{
    			over_all_id=tbl.getId();
    			//tbl.setVisibility(View.GONE);
    		}
    		
    		/**Customized validation for Has the roof mounted structure been adequately installed to prevent water seepage into the structure Flashing/Coping***/
    		
    		/*** Customized validation for Has the roof mounted structure been adequately installed to prevent water seepage into the structure ends***/
    		
    		if(i<RCN_c.getCount())
	    	{
	    		RCN_c.moveToNext();
	    	}
    		if(RC_GRI.getCount()>0)
    		{
    			RC_GRI.moveToNext();
    		}
    	
		}
		try
		{
		if(amt[0]!=-1 && amt[1]!=-1 )
		{
			if(((RadioButton) findViewById(amt[0]).findViewWithTag(getResources().getString(R.string.yes))).isChecked())
			{
				((TableRow) findViewById(amt[1])).setVisibility(View.VISIBLE);
			}
		}
		if(everReplaced[0]!=-1 && everReplaced[1]!=-1 )
		{
			if(((RadioButton) findViewById(everReplaced[0]).findViewWithTag(com_roof_yesspl)).isChecked())
			{
				((TableRow) findViewById(everReplaced[1])).setVisibility(View.VISIBLE);
			}
		}
		if(hasroof[0]!=-1 && hasroof[1]!=-1 )
		{
			if(((RadioButton) findViewById(hasroof[0]).findViewWithTag(com_roof_yesspl)).isChecked())
			{
				((TableRow) findViewById(hasroof[1])).setVisibility(View.VISIBLE);
			}
		}
		
		}catch (Exception e) {
			// TODO: handle exception
			System.out.println("erro comes"+e.getMessage());
		}
		
		/***Start to set the value to the respective colomns ends **/	
		System.out.println("comepleted correctly");
		}
	private void Save_RCTD() {
		Boolean confi=false;
		Cursor c=cf.arr_db.rawQuery("Select com.*,sub.mst_custom_id as noinf_id,mst.mst_text as title from "+cf.option_list+" as com Left join "+cf.master_tbl +" as sub on sub.mst_text='"+cf.encode(no_perdomin_tit)+"' LEFT JOIN "+cf.master_tbl+" mst  on mst.mst_custom_id=option_tit and mst.mst_module_id='2'  Where com.module_id='"+Roof_page_value[0]+"' and com.insp_id='"+this.Roof_page_value[5]+"'", null);
	//	System.out.println(" the sql=Select com.*,sub.mst_custom_id as noinf_id,mst.mst_text as title from "+cf.option_list+" as com Left join "+cf.master_tbl +" as sub on sub.mst_text='"+cf.encode(no_perdomin_tit)+"' LEFT JOIN "+cf.master_tbl+" mst  on mst.mst_custom_id=option_tit and mst.mst_module_id='2'  Where com.module_id='"+Roof_page_value[0]+"' and com.insp_id='"+this.Roof_page_value[5]+"'");  
		boolean validation=true;
		if(c.getCount()>0)
		{
			
			c.moveToFirst();
			String RoofType="";
		 for(int i=0;i<c.getCount();i++)
		 {
			  //confi=false;
			 View focusable=null; 
			 for(int j=0;j<RCT_id.length;j++)
			 {
				 
				 boolean re=false,noin=true;
				 String tit= ((TextView) findViewById(RCT_id[j]).findViewWithTag("Title")).getText().toString();
				 if(c.getInt(c.getColumnIndex("Requirred"))==1)
				 {
					 re=true;
					 //tit=tit.substring(1);
				 }
				 
				 /**Customized validation for the No infor provide**/
				 int id=c.getInt(c.getColumnIndex("noinf_id"));
			
				 if(id!=0)
				 {
				 if(((View)findViewById(id).findViewWithTag(no_perdomin_tit)) instanceof CheckBox)
				 {
				 	 if(((CheckBox)findViewById(id).findViewWithTag(no_perdomin_tit)).isChecked())
					  {
						 noin=false;
					  }
				 }
				 else if(((View)findViewById(id).findViewWithTag(no_perdomin_tit)) instanceof RadioButton)
				 {
				 	 if(((RadioButton)findViewById(id).findViewWithTag(no_perdomin_tit)).isChecked())
					  {
						 noin=false;
					  }
				 }
				 }
				 
				 /**Customized validation for the No infor provide ends**/
				 if(tit.trim().equals(cf.decode(c.getString(c.getColumnIndex("title")))))
				 {
					 
					
					if(re)
					{
						String type=c.getString(c.getColumnIndex("option_type"));
						String Other=c.getString(c.getColumnIndex("option_other"));
						String[] opt=c.getString(c.getColumnIndex("options")).split(",");
						String type_val="";
						Boolean importan_val=true;
						String Other_txt="";
						String Err="";
						if(type.equals("Radio"))
			     		{
							Err="Please select option for";
							if(c.getString(c.getColumnIndex("options")).trim().equals(""))
							{
								focusable=((RadioButton) findViewById(RCT_id[j]).findViewWithTag(tit));
								if(((RadioButton) findViewById(RCT_id[j]).findViewWithTag(tit)).isChecked())
								{
									
									type_val="true";
								}
								
									
								
							}
							else
							{
								focusable=findViewById(RCT_id[j]);
								type_val=getvalfromoption(RCT_id[j], opt);
							}
							/**checking for the other validation**/
			     			if(type_val.trim().equals(Other.trim()) && !type_val.trim().equals(""))
			     			{
			     				
			     				String otherval=((EditText) findViewById(RCT_id[j]).findViewWithTag("Other_DESC")).getText().toString();
			     				focusable=findViewById(RCT_id[j]).findViewWithTag("Other_DESC");
			     				if(otherval.trim().equals(""))
			     				{
			     					type_val="";
			     					Err="Please enter the Other text for";
			     					importan_val=false;
			     				}
			     			}
			     			/**checking for the other validation ends**/
			     		}
			     		else if(type.equals("CheckBox"))
				     	{
			     			Err="Please select option for";
			     			if(c.getString(c.getColumnIndex("options")).trim().equals(""))
							{
								if(((CheckBox) findViewById(RCT_id[j]).findViewWithTag(tit)).isChecked())
								{
									focusable=findViewById(RCT_id[j]).findViewWithTag(tit);
									type_val="true";
								}
							}
							else
							{
								focusable=findViewById(RCT_id[j]);
								type_val=getvalfromchkoption(RCT_id[j], opt);
							}
			     			/**checking for the other validation**/
			     			if(type_val.trim().equals(Other.trim()) && !type_val.trim().equals(""))
			     			{
			     				
			     				String otherval=((EditText) findViewById(RCT_id[j]).findViewWithTag("Other_DESC")).getText().toString();
			     				if(otherval.trim().equals(""))
			     				{
			     					focusable=findViewById(RCT_id[j]).findViewWithTag("Other_DESC");
			     					type_val="";
			     					Err="Please enter the Other text for";
			     					importan_val=false;
			     				}
			     			}
			     			/**checking for the other validation ends**/
				     	}
			     		else if(type.equals("Spinner"))
			     		{
			     			Err="Please select ";
			     			type_val=((Spinner) findViewById(RCT_id[j]).findViewWithTag("Spinner_val")).getSelectedItem().toString();
			     			System.out.println("the type val"+type_val+" tit="+tit+" Roofcove="+Roof_page_value[3]);
			     			if(tit.trim().equals(Roof_page_value[3].trim()))
							 {
								 RoofType=type_val;
							 }
			     			if(type_val.equals("-Select-"))
			     			{
			     				focusable=findViewById(RCT_id[j]).findViewWithTag("Spinner_val");
			     				type_val="";
			     			}
			     			/**checking for the other validation**/
			     			if(type_val.trim().equals(Other.trim()) && !type_val.trim().equals(""))
			     			{
			     				
			     				String otherval=((EditText) findViewById(RCT_id[j]).findViewWithTag("Other_DESC")).getText().toString();
			     				if(otherval.trim().equals(""))
			     				{
			     					focusable=findViewById(RCT_id[j]).findViewWithTag("Other_DESC");
			     					type_val="";
			     					Err="Please enter the Other text for";
			     					importan_val=false;
			     				}
			     			}
			     			/**checking for the other validation ends**/
			     			
			     		}
			     		else if(type.equals("DatePicker"))
			     		{ 
			     			
			     			Err="Please select ";
			     			focusable=findViewById(RCT_id[j]).findViewWithTag("Date");
			     			type_val=((EditText) findViewById(RCT_id[j]).findViewWithTag("Date")).getText().toString();
			     			if(!type_val.trim().equals(cf.getResourcesvalue(R.string.na))&& !type_val.trim().equals(cf.getResourcesvalue(R.string.nd)) && !type_val.trim().equals(""))
				     		{
			     				try
				     			{
				     				if(!cf.checkfortodaysdate(type_val).equals("true"))
				     				{
				     					importan_val=false;
				     					type_val="";
				     					cf.ShowToast(tit.trim()+" should not be greater than today's date.", 0);
				     					return;
				     					//Err="Date should not be greater than today's date for";
				     					
				     				}
				     			}catch (Exception e) {
									// TODO: handle exception
								}
			     				
			     			}
			     				
			     		}
			     		else if(type.equals("ImagePicker"))
						{
			     			Err="Please ";
			     			focusable=findViewById(RCT_id[j]);//.findViewWithTag("Path");
			     			type_val="";//((EditText) findViewById(RCT_id[j]).findViewWithTag("Path")).getText().toString(); 
						}
						/**customized for rooof shape validation**/
						if(tit.trim().equals(Roof_shap_tit))
						{
						String s=((EditText) findViewById(RCT_id[j]).findViewWithTag("RSH_percent")).getText().toString();
						
						if(s.trim().equals("") && !type_val.trim().equals(""))
						{
							focusable=findViewById(RCT_id[j]).findViewWithTag("RSH_percent");
							Err="Please enter percentage for";
							importan_val=false;
							type_val="";
							
							
						}
						else if(!s.trim().equals(""))
						{
						 if(Integer.parseInt(s)>100 || Integer.parseInt(s)<1)
						 {
							 focusable=findViewById(RCT_id[j]).findViewWithTag("RSH_percent");
							 Err="Please enter percentage between 1 to 100";
							 importan_val=false;
								type_val="";
								
						 }
						 else 
							{
							 	
							 Cursor per= cf.arr_db.rawQuery("Select RCT_FieldVal from "+cf.Roof_covertype_view+" as RCT " +
										" WHERE RCT_insp_id='"+cf.encode(Roof_page_value[5])+"' and	RCT_srid='"+cf.selectedhomeid+"' and RCT_TypeValue=(Select mst_custom_id from "+cf.master_tbl+" where mst_module_id='2' and mst_text='"+cf.encode(Roof_shap_tit)+"' and RCT_FielsName<>'"+cf.encode(RoofType)+"' )" ,null);
							 
								if(per.getCount()>0)
								{
									per.moveToFirst();
									int per_tot=0;
									for(int k=0;k<per.getCount();k++,per.moveToNext())
									{
										String per_str=cf.decode(per.getString(per.getColumnIndex("RCT_FieldVal")));
								
										if(!per_str.equals(""))
										{
											String[] tmp=per_str.split("~");
											if(!tmp[1].equals(""))
											{
												per_tot+=Integer.parseInt(tmp[1]);
											}
										}
										
									}
									
									if((per_tot+Integer.parseInt(s))>100)
									{
										((EditText) findViewById(RCT_id[j]).findViewWithTag("RSH_percent")).requestFocus();
										cf.ShowToast("Percentage exceeds! Sum of "+Roof_shap_tit+" percentage should be 100.", 0);
										return;
									}
								}
							}

						}
													
						}
						/**customized for rooof shape validation ends**/

						/**customized for year validation**/
						if(tit.trim().equals(year_ins) && type_val.equals("Other"))
						{
							
							
							String otherval=((EditText) findViewById(RCT_id[j]).findViewWithTag("Other_DESC")).getText().toString();
							if(!otherval.trim().equals(""))
							{
								if(otherval.length()<4)
								{
									focusable=findViewById(RCT_id[j]).findViewWithTag("Other_DESC");
									Err="Please enter year format like YYYY ";
									importan_val=false;
									type_val="";
								}
								else if(cf.yearValidation(otherval).equals("true"))
								{
									focusable=findViewById(RCT_id[j]).findViewWithTag("Other_DESC");
									Err="The entered year should not be greater than the current year in";
									importan_val=false;
									type_val="";
								}
								else if(cf.yearValidation(otherval).equals("zero"))
								{
									focusable=findViewById(RCT_id[j]).findViewWithTag("Other_DESC");
									Err="Invalid year in";
									importan_val=false;
									type_val="";
								}
						}
							
						}
						/**customized for year validation ends**/
						
						if(type_val.trim().equals("") && (noin || !importan_val || tit.trim().equals(cf.RCT_tit)))
						{
							Setfocuserro(focusable, focusable, focusable);
							validation=false;
							cf.ShowToast(Err.trim()+" "+tit.trim()+".", 0);
							return;
						}
						/**Start get confirmation from the user for updates**/
						 if(tit.trim().equals(cf.RCT_tit))
						 {
							 String Sql=" SELECT rct.RCT_TypeValue FROM  "+cf.Roof_covertype_view+" as  rct LEFT JOIN " +cf.master_tbl+" as opt on  opt.mst_text='"+cf.encode(cf.RCT_tit)+"' "+
							 		" Where rct.RCT_insp_id='"+cf.encode(Roof_page_value[5])+"' and	RCT_srid='"+cf.selectedhomeid+"' and rct.RCT_TypeValue=opt.mst_custom_id and rct.RCT_FieldVal='"+cf.encode(type_val)+"'";
							 System.out.println("the update sql"+Sql);
							 Cursor c2=cf.arr_db.rawQuery(Sql, null);
							 if(c2.getCount()>0)
							 {
								 confi=true;
							 }
							 else
							 {
								 confi=false;
							 }
								 
						 }
						 /**Start get confirmation from the user for updates ends**/
							
					}
					 
					 j=RCT_id.length;
				 }
				 
			 }
			 
			 c.moveToNext();
		 }
		 if(validation)
		 {
			if(confi && GCH_RC_RCT_SA.getText().toString().trim().equals(cf.getResourcesvalue(R.string.S_Addmore)))
			{
				 	final AlertDialog.Builder ab =new AlertDialog.Builder(RoofSection.this);
					ab.setTitle("Update Roof Cover");
					ab.setMessage("This Roof Covering Type already exists. Do you want to replace?");
					ab.setIcon(R.drawable.alertmsg);
					ab.setCancelable(false);
					ab.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							Insertvalues_RCTD();
							ab.setCancelable(true);
							dialog.dismiss();
							
						}
					})
					.setNegativeButton("No", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							
							ab.setCancelable(true);
							dialog.dismiss();
							//return;
							
						}
					});
					AlertDialog al=ab.create();
					al.show();
			}
			else
			{
			 Insertvalues_RCTD();
			}
			 
		 }
		}
		
		
		
	}
	private void Insertvalues_RCTD() {
		// TODO Auto-generated method stub
		
		
		String RoofType="";
		boolean boo=true;
		 for(int j=0;j<RCT_id.length;j++)
		 {
			 boolean re=false;
			 String tit= ((TextView) findViewById(RCT_id[j]).findViewWithTag("Title")).getText().toString();
			 if(tit.contains("*"))
			 {
				 tit=tit.substring(1).trim();
			 }
			 
			 Cursor c=cf.SelectTablefunction(cf.option_list, " Where module_id='"+Roof_page_value[0]+"' and insp_id='"+Roof_page_value[5]+"' and option_tit=( SELECT mst_custom_id From "+cf.master_tbl+" Where mst_text='"+cf.encode(tit)+"')");
			 if(c.getCount()>=1)
			 {
				 c.moveToFirst();
				 String type=c.getString(c.getColumnIndex("option_type"));
				 String Other=c.getString(c.getColumnIndex("option_other"));
				 String[] opt=c.getString(c.getColumnIndex("options")).split(",");
				 int id=c.getInt(c.getColumnIndex("option_tit"));
				  String type_val="";
				  String Other_txt="";
				 String Err="";
				 if(type.equals("Radio"))
		     	 {
					if(c.getString(c.getColumnIndex("options")).trim().equals(""))
						{
							if(((RadioButton) findViewById(RCT_id[j]).findViewWithTag(tit)).isChecked())
							{
								type_val="true";
							}
						}
						else
						{
							type_val=getvalfromoption(RCT_id[j], opt);
						}
						/**checking for the other validation**/
		     			if(type_val.trim().equals(Other.trim()) && !type_val.trim().equals(""))
		     			{
		     			  Other_txt=((EditText) findViewById(RCT_id[j]).findViewWithTag("Other_DESC")).getText().toString();
		     			}
		     			/**checking for the other validation ends**/
		     		}
		     		else if(type.equals("CheckBox"))
			     	{
		     			if(c.getString(c.getColumnIndex("options")).trim().equals(""))
						{
							if(((CheckBox) findViewById(RCT_id[j]).findViewWithTag(tit)).isChecked())
							{
								type_val="true";
							}
						}
						else
						{
							type_val=getvalfromchkoption(RCT_id[j], opt);
						}
		     			/**checking for the other validation**/
		     			if(type_val.trim().equals(Other.trim()) && !type_val.trim().equals(""))
		     			{
		     				Other_txt=((EditText) findViewById(RCT_id[j]).findViewWithTag("Other_DESC")).getText().toString();
		     			}
		     			/**checking for the other validation ends**/
			     	}
		     		else if(type.equals("Spinner"))
		     		{
		     			type_val=((Spinner) findViewById(RCT_id[j]).findViewWithTag("Spinner_val")).getSelectedItem().toString();
		     			if(type_val.equals("-Select-"))
		     			{
		     				type_val="";
		     			}
		     			/**checking for the other validation**/
		     			if(type_val.trim().equals(Other.trim()) && !type_val.trim().equals(""))
		     			{
		     				Other_txt=((EditText) findViewById(RCT_id[j]).findViewWithTag("Other_DESC")).getText().toString();
		     			}
		     			/**checking for the other validation ends**/
		     		}
		     		else if(type.equals("DatePicker"))
		     		{ 
		     			type_val=((EditText) findViewById(RCT_id[j]).findViewWithTag("Date")).getText().toString();
		     			
		     		}
		     		else if(type.equals("ImagePicker"))
					{
		     			type_val="";//((EditText) findViewById(RCT_id[j]).findViewWithTag("Path")).getText().toString(); 
					}
					if(tit.equals(Roof_page_value[3]))
					 {
						 RoofType=type_val;
						 Cursor c12=cf.SelectTablefunction(cf.Roof_master, " Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(Roof_page_value[5])+"' and RM_Enable='1' and RM_Covering='"+cf.encode(RoofType)+"' and RM_module_id='0'  ");
						 if(c12.getCount()<=0)
						 {
							 cf.arr_db.execSQL("Insert into "+cf.Roof_master+" (RM_inspectorid,RM_srid,RM_page,RM_insp_id,RM_Covering,RM_Enable) Values ('"+cf.Insp_id+"','"+cf.selectedhomeid+"','1','"+cf.encode(Roof_page_value[5])+"','"+cf.encode(RoofType)+"','1')");
						 }
					 }
					/**customized for rooof shape insert**/
					if(tit.trim().equals(Roof_shap_tit))
					{
						try
						{
						String s=((EditText) findViewById(RCT_id[j]).findViewWithTag("RSH_percent")).getText().toString();
						if(!s.trim().equals(""))
							type_val+="~"+Integer.parseInt(s);
						
						}
						catch (Exception e) {
							// TODO: handle exception
						}
					}
					/**customized for rooof shape validation insert**/
					try
					{
					
						if(tit.equals("Predominant") && type_val.equals("true"))
						{
								
								cf.arr_db.execSQL("UPDATE  "+cf.Roof_cover_type+" SET RCT_FieldVal=''  WHERE (RCT_masterid in (Select RM_masterid from "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"'  and RM_module_id='0')) and RCT_TypeValue='"+id+"' ");
										//" RCT_insp_id='"+cf.encode(Roof_page_value[5])+"' and RCT_TypeValue='"+id+"' ");
							
							
						}
						
						Cursor c2 = cf.SelectTablefunction(cf.Roof_covertype_view, " WHERE RCT_srid='"+cf.selectedhomeid+"' and RCT_insp_id='"+cf.encode(Roof_page_value[5])+"' and RCT_TypeValue='"+id+"' and RCT_FielsName='"+cf.encode(RoofType)+"' ");
						System.out.println(" WHERE RCT_insp_id='"+cf.encode(Roof_page_value[5])+"' and RCT_TypeValue='"+id+"' and RCT_FielsName='"+cf.encode(RoofType)+"' ");
						if(c2.getCount()>0)
						{
						
							//,RCT_Enable='1'
								cf.arr_db.execSQL("UPDATE  "+cf.Roof_cover_type+" SET RCT_FieldVal='"+cf.encode(type_val)+"',RCT_Other='"+cf.encode(Other_txt)+"'  WHERE (RCT_masterid=(Select RM_masterid from "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_Enable='1' and RM_Covering='"+cf.encode(RoofType)+"' and RM_module_id='0')) and RCT_TypeValue='"+id+"' ");
										//"RCT_insp_id='"+cf.encode(Roof_page_value[5])+"' and RCT_TypeValue='"+id+"' and RCT_FielsName='"+cf.encode(RoofType)+"'");
								
						}
						else
						{
						
							cf.arr_db.execSQL("INSERT INTO "+cf.Roof_cover_type+" (RCT_masterid,RCT_TypeValue,RCT_FieldVal,RCT_Other) Values ((Select RM_masterid as RCT_masterid from "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_Enable='1' and RM_Covering='"+cf.encode(RoofType)+"' and RM_module_id='0' ),'"+id+"','"+cf.encode(type_val)+"','"+cf.encode(Other_txt)+"')");
						
						}
						/***Update the all fields to permenant**/
						//	cf.arr_db.execSQL("UPDATE  "+cf.Roof_cover_type+" SET RCT_Enable='1'  WHERE RCT_insp_id='"+cf.encode(Roof_page_value[5])+"'");
						
					}catch (Exception e) {
						boo=false;
						System.out.println(" error while save the value  "+e.getMessage());
						// TODO: handle exception
					}
				}
			}
		 if(boo)
			{
				 if(GCH_RC_RCT_SA.getText().toString().equals("Update"))
				 {
					 cf.ShowToast("Roof Covering Type updated successfully", 1);
				 }else
				 {
					cf.ShowToast("Roof Covering Type saved successfully", 1);
				 }
				clear_RCT(1);
				show_RCT_List(1);
			}
			else
			{
				cf.ShowToast("You have problem in storing data", 0);
			}
	}
	public void Add_more_condtion() {
		// TODO Auto-generated method stub
	   	 Random r = new Random();
	   	 int tbli=r.nextInt(2000-1500) + 1500;
		 tbl_id=dynamicarraysetting(tbl_id);
		 Cursor c =cf.SelectTablefunction(cf.option_list, " WHERE module_id='"+Roof_page_value[1]+"' and insp_id='"+Roof_page_value[5]+"' and option_tit=( SELECT mst_custom_id From "+cf.master_tbl+" Where mst_text='"+cf.encode(Roof_page_value[4])+"') ORDER BY option_potion ");
		
		int option_max_l,option_max_W,other_l,other_w,tit_max_w,Requirred,option_potion,id;
		String option_tit,options,option_other,option_type,Default_option,option_filter,Other_filter;
		/**Set the default values **/
		option_tit=options=option_other=option_type=Default_option=option_filter=Other_filter="";
		id=option_max_l=option_max_W=other_l=other_w=tit_max_w=Requirred=option_potion=0;
		
		/**Set the default values **/
		
		TableLayout.LayoutParams row_params = new TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
		row_params.setMargins(0, 5, 0, 5);
		TableRow.LayoutParams lin_params_tit = new TableRow.LayoutParams(250,LayoutParams.WRAP_CONTENT);
		TableRow.LayoutParams lin_params_ops = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
		TableRow.LayoutParams lin_params_ops_Rg = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
		TableRow.LayoutParams lin_params_yes = new TableRow.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
		
		if(c.getCount()>0)
		{
				c.moveToFirst();
				option_tit=cf.decode(c.getString(c.getColumnIndex("option_tit")));
				options=c.getString(c.getColumnIndex("options"));
				option_other=c.getString(c.getColumnIndex("option_other"));
				option_type=c.getString(c.getColumnIndex("option_type"));
				Default_option=c.getString(c.getColumnIndex("Default_option"));
				option_max_l=c.getInt(c.getColumnIndex("option_max_l"));
				option_max_W=c.getInt(c.getColumnIndex("option_max_W"));
				other_l=c.getInt(c.getColumnIndex("other_l"));
				other_w=c.getInt(c.getColumnIndex("other_w"));
				tit_max_w=c.getInt(c.getColumnIndex("tit_max_w"));
				Requirred=c.getInt(c.getColumnIndex("Requirred"));
				String[] opt=options.split(",") ;
	    		id=c.getInt(c.getColumnIndex("option_tit"));
				/** Create a Table row Dynamically **/
	    		TableRow tbl=new TableRow(this);
	    		tbl.setId(tbli);
	    		tbl_id[tbl_id.length-1]=tbl.getId();
	    		/** Create a Table row Dynamically Ends **/
	    		
	    		/** Create a Title of the event **/
	    		LinearLayout title_li=new LinearLayout(this);
	    		title_li.setOrientation(LinearLayout.VERTICAL);
	    		TextView tit_tv=new TextView(this,null,R.attr.Roof_RCN_Tit_txt);
	    		title_li.addView(tit_tv,tit_max_w,LayoutParams.FILL_PARENT);
	    		tit_tv.setTag("Title_txt");
	    		EditText tit_ed=new EditText(this,null,R.attr.Roof_RCN_Tit_ed);
	    		tit_ed.setTag("Title_ed");
	    		tit_ed.setVisibility(View.VISIBLE);
	    		tit_tv.setText("");
	    		title_li.addView(tit_ed,tit_max_w,LayoutParams.FILL_PARENT);
	    		tit_ed.setOnTouchListener(new OnTouchListener() {
					
					@Override
					public boolean onTouch(View v, MotionEvent event) {
						// TODO Auto-generated method stub
						((EditText) v).setFocusable(true);
						((EditText) v).setFocusableInTouchMode(true);
						((EditText) v).requestFocus();
						return false;
					}
				});
	    		tit_tv.setText("");
	    		
	    		tit_tv.setVisibility(View.GONE);
	    		TextView tit_star=new TextView(this,null,R.attr.star);
				tit_star.setTag("Star");
				tit_star.setPadding(0, 0, 5, 0);
				tbl.addView(tit_star);
	    		/** Create a Title of the event Ends**/
	    		tbl.addView(title_li,lin_params_tit);
	    		TextView colon_tv=new TextView(this,null,R.attr.colon_withoutmargin);
	    		tbl.addView(colon_tv);
	    		/** Create a option of the event **/
	    		LinearLayout op_li = null;
	    		if(option_type.equals("Radio"))
	    		{
	    		 op_li=Add_ROption_dynamic(tbl.getId(), opt, option_max_l,option_max_W,option_other);
	    		 if(!Default_option.equals(""))
					{
						((RadioButton) op_li.findViewWithTag(Default_option)).setChecked(true);
					}
	    		}
	    		else if(option_type.equals("CheckBox"))
	    		{
	    			op_li=Add_CHKption_dynamic(tbl.getId(), opt, option_max_l,option_max_W,option_other);
	    			if(!Default_option.equals(""))
					{
						Setcheckvalue(op_li,Default_option,",");
					}
	    		}
	    		else if(option_type.equals("Spinner"))
				{
					 op_li=Add_Spinption_dynamic(tbl.getId(), opt,option_max_W,option_other);
					if(!Default_option.equals(""))
					{
						for(int m=0;m<opt.length;m++)
						{
						 if(opt[m].trim().equals(Default_option))
						 {
							 ((Spinner)op_li.findViewWithTag("Spinner_val")).setSelection(m);
						 }
						}
					}
				}
				else if(option_type.equals("DatePicker"))
				{
					 op_li=Add_DPinption_dynamic(tbl.getId(),option_max_W);
					 if(!Default_option.equals(""))
						{
						 ((EditText)op_li.findViewWithTag("Date")).setText(Default_option);
						}
				}
	    		/**Start to set the Radio buttons for the radio group endss**/
	    		if(!option_other.trim().equals(""))
	    		{
		    		LinearLayout yes_li=new LinearLayout(this);
		    		yes_li.setLayoutParams(lin_params_yes);
		    		yes_li.setOrientation(LinearLayout.VERTICAL);
		    		EditText yes_ed=new EditText(this,null,R.attr.Roof_RCN_yes_ed);
		    		yes_li.addView(yes_ed);
		    		yes_ed.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							((TextView) v).setFocusable(true);
							((TextView) v).setFocusableInTouchMode(true);
							((TextView) v).requestFocus();
						}
					});
		    		yes_ed.setTag("Other_DESC");
		    		TextView yes_tv=new TextView(this,null,R.attr.Roof_RCN_yes_txt);
		    		yes_li.addView(yes_tv);
		    		yes_li.setTag("Other_DESC_li");
		    		yes_tv.setTag("Other_DESC_txt");
		    		//yes_tv.setTextColor(R.color.pink_tv);
		    		
		    		yes_li.setVisibility(View.GONE);
		    		InputFilter[]	FilterArray = new InputFilter[1];
					FilterArray[0] = new InputFilter.LengthFilter(other_l);
					yes_ed.setFilters(FilterArray);
		    		op_li.addView(yes_li);      
		    		yes_ed.addTextChangedListener(new RCT_watcher(tbl.getId(),other_l));
	    		}
	    		tbl.addView(op_li);
	    		/** Create a option of the event Ends**/
	    		ImageView tit_img=new ImageView(this,null,R.attr.Roof_RCN_Tit_img);
	    		tbl.addView(tit_img);
	    		ImageView opt_img=new ImageView(this,null,R.attr.Roof_RCN_opt_img);
	    		tbl.addView(opt_img);
	    		tit_img.setVisibility(View.INVISIBLE);
	    		opt_img.setVisibility(View.VISIBLE);
	    		opt_img.setOnClickListener(new RCN_clicker(2,tbl.getId()));
	    		tit_img.setOnClickListener(new RCN_clicker(1,tbl.getId()));
	    		TextView id_tv1=new TextView(this,null,R.attr.Roof_RCN_yes_txt);
	    		id_tv1.setVisibility(View.GONE);
	    		id_tv1.setTag("GCH_RCT_id");
	    		id_tv1.setText("");
	    		
	    		tbl.addView(id_tv1);
	    		RCN_ShowValue.addView(tbl,row_params);
    	}

			
			
	}
	private void clear_RCT(int type_v) {
		// TODO Auto-generated method stub
		String sql="SELECT opt.*,mst.mst_text as title FROM "+cf.option_list+" opt LEFT JOIN "+cf.master_tbl+" mst  on mst_custom_id=option_tit and mst_module_id='2' Where module_id='"+Roof_page_value[0]+"' and insp_id='"+Roof_page_value[5]+"'";
		Cursor c=cf.arr_db.rawQuery(sql, null);
				//cf.SelectTablefunction(cf.option_list, " Where module_id='"+Roof_page_value[0]+"' and insp_id='"+Roof_page_value[5]+"'");
		if(c.getCount()>0)
		{	c.moveToFirst();
			for(int i=0;i<c.getCount();i++)
			{
			    String type=c.getString(c.getColumnIndex("option_type"));
				String Other=c.getString(c.getColumnIndex("option_other"));
				String[] opt=c.getString(c.getColumnIndex("options")).split(",");
				String tit=cf.decode(c.getString(c.getColumnIndex("title")));
				String Default=c.getString(c.getColumnIndex("Default_option"));
				if(!(type_v==0 && tit.equals(Roof_page_value[3])))
				{
					if(tit.equals(buildingcode_tit))
					{
						if(cf.County.trim().toLowerCase().equals("Miami-Dade".toLowerCase())|| cf.County.trim().toLowerCase().equals("Broward".toLowerCase()))
						{
							Default="1994 SFBC";
						}
					}
					if(type.equals("Radio"))
			 		{
						if(c.getString(c.getColumnIndex("options")).trim().equals(""))
						{
							((RadioButton) findViewById(RCT_id[i]).findViewWithTag(tit)).setChecked(false);
						}
						else
						{
							for(int j=0;j<opt.length;j++)
							{
								((RadioButton) findViewById(RCT_id[i]).findViewWithTag(opt[j])).setChecked(false);
								if(Default.trim().equals(opt[j]))
								{
									((RadioButton) findViewById(RCT_id[i]).findViewWithTag(opt[j])).setChecked(true);
								}
							}
						}
						/**checking for the other validation**/
			 			if(!Other.trim().equals(""))
			 			{
			 				((LinearLayout) findViewById(RCT_id[i]).findViewWithTag("Other_DESC_li")).setVisibility(View.GONE);
			 			}
			 			/**checking for the other validation ends**/
			 		}
					if(type.equals("CheckBox"))
			 		{
						if(c.getString(c.getColumnIndex("options")).trim().equals(""))
						{
							((CheckBox) findViewById(RCT_id[i]).findViewWithTag(tit)).setChecked(false);
						}
						else
						{
							for(int j=0;j<opt.length;j++)
							{
								((CheckBox) findViewById(RCT_id[i]).findViewWithTag(opt[j])).setChecked(false);
								if(Default.trim().equals(opt[j]))
								{
									((CheckBox) findViewById(RCT_id[i]).findViewWithTag(opt[j])).setChecked(true);
								}
							}
						}
						/**checking for the other validation**/
			 			if(!Other.trim().equals(""))
			 			{
			 				((LinearLayout) findViewById(RCT_id[i]).findViewWithTag("Other_DESC_li")).setVisibility(View.GONE);
			 			}
			 			/**checking for the other validation ends**/
			 		}
			 		else if(type.equals("Spinner"))
			 		{
			 			if(!Default.trim().equals(""))
			 			{
			 				for(int k=0;k<opt.length;k++)
			 				{
			 					if(Default.trim().equals(opt[k]))
			 					{
			 						((Spinner) findViewById(RCT_id[i]).findViewWithTag("Spinner_val")).setSelection(k);
			 					}
			 				}
			 			}
			 			else
			 			{
			 				((Spinner) findViewById(RCT_id[i]).findViewWithTag("Spinner_val")).setSelection(0);
			 			}
			 			/**checking for the other validation**/
			 			if(!Other.trim().equals(""))
			 			{
			 				((LinearLayout) findViewById(RCT_id[i]).findViewWithTag("Other_DESC_li")).setVisibility(View.GONE);
			 			}
			 			/**checking for the other validation ends**/
			 		}
			 		else if(type.equals("DatePicker"))
			 		{ 
			 			((EditText) findViewById(RCT_id[i]).findViewWithTag("Date")).setText(Default);
			 			((Button) findViewById(RCT_id[i]).findViewWithTag("Date_pck")).setVisibility(View.VISIBLE);
			 		}
			 		else if(type.equals("ImagePicker"))
			 		{ 
			 			//((EditText) findViewById(RCT_id[i]).findViewWithTag("Path")).setText(Default);
			 			if(findViewById(RCT_id[0]).findViewWithTag("Spinner_val")!=null)
				    	{
				    		String cv=((Spinner)findViewById(RCT_id[0]).findViewWithTag("Spinner_val")).getSelectedItem().toString().trim();
				    		
				    		Cursor c24=cf.arr_db.rawQuery("Select * from "+cf.Roofcoverimages+" Where RIM_masterid=(Select RM_masterid from "+cf.Roof_master+
				    				" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+Roof_page_value[5]+"' and RM_Covering='"+cf.encode(cv)+"') order by RIM_Order", null);
				    			if(c24.getCount()>0)
		    		    		{
				    				if(update==1)
						    		{
							    		((TextView)findViewById(RCT_id[i]).findViewWithTag("View_photos")).setVisibility(View.VISIBLE);
			    		    			((TextView)findViewById(RCT_id[i]).findViewWithTag("View_photos")).setText(Html.fromHtml("<u>View/Edit Photos ("+c24.getCount()+")</u>"));
						    		}
		    		    		}
		    		    		else
		    		    		{
		    		    			((TextView)findViewById(RCT_id[i]).findViewWithTag("View_photos")).setVisibility(View.GONE);
		    		    		}
				    		
				    	}
			 		}
					/**customized for rooof shape insert**/
					if(tit.trim().equals(Roof_shap_tit))
					{
						((EditText) findViewById(RCT_id[i]).findViewWithTag("RSH_percent")).setText("");
					}
					/**customized for rooof shape validation insert**/
					/**customized for Roof cover type  disable**/
					if(tit.trim().equals(cf.RCT_tit))
					{
						((Spinner) findViewById(RCT_id[i]).findViewWithTag("Spinner_val")).setEnabled(true);
						((EditText) findViewById(RCT_id[i]).findViewWithTag("Other_DESC")).setEnabled(true);
					}
					
					/**customized for Roof cover type  disable ends**/
				}
					
				
				c.moveToNext();
		}
			GCH_RC_RCT_SA.setText(cf.getResourcesvalue(R.string.S_Addmore));
	}
	}
	public void updateRCT(String type2) {
		// TODO Auto-generated method stub
		
		Cursor c2=cf.SelectTablefunction(cf.Roof_covertype_view, " WHERE RCT_srid='"+cf.selectedhomeid+"' and RCT_insp_id='"+cf.encode(Roof_page_value[5])+"' and RCT_FielsName='"+cf.encode(type2.trim())+"'");
		
		update=1;
		if(c2.getCount()>0)
		{
			String sql="SELECT opt.*,mst.mst_text as title FROM "+cf.option_list+" opt LEFT JOIN "+cf.master_tbl+" mst  on mst_custom_id=option_tit and mst_module_id='2' Where module_id='"+Roof_page_value[0]+"' and insp_id='"+Roof_page_value[5]+"'";
			Cursor c=cf.arr_db.rawQuery(sql, null);
					//cf.SelectTablefunction(cf.option_list, " Where module_id='"+Roof_page_value[0]+"' and insp_id='"+Roof_page_value[5]+"'");
			
			if(c.getCount()>0)
			{
				c.moveToFirst();
				c2.moveToFirst();
			 for(int i=0;i<c.getCount();i++)
			 {
				 
				 for(int j=0;j<RCT_id.length;j++)
				 { 
				 
					 boolean re=false;
					
					 String tit= ((TextView) findViewById(RCT_id[j]).findViewWithTag("Title")).getText().toString();
				 
					 if(c.getInt(c.getColumnIndex("Requirred"))==1)
					 {
						
						  
						// tit=tit.substring(1).trim();
						 
					 }
					
					 if(tit.trim().equals(cf.decode(c.getString(c.getColumnIndex("title")))))
					 {
						
						
						 String type=c.getString(c.getColumnIndex("option_type"));
							String Other=c.getString(c.getColumnIndex("option_other"));
							String[] opt=c.getString(c.getColumnIndex("options")).split(",");
						
							
							String type_val=cf.decode(c2.getString(c2.getColumnIndex("RCT_FieldVal")));
							String Other_txt=cf.decode(c2.getString(c2.getColumnIndex("RCT_Other")));
							
							
							/**customized for rooof shape insert**/
							if(tit.trim().equals(Roof_shap_tit))
							{
								String[] tmp=type_val.split("~");
								type_val=tmp[0];
								if(tmp.length>1)
								{
									((EditText) findViewById(RCT_id[j]).findViewWithTag("RSH_percent")).setText(tmp[1]);
								}
								
								
							
							}
							/**customized for rooof shape validation insert**/
							/**customized for Roof cover type  disable**/
							if(tit.trim().equals(cf.RCT_tit))
							{
								((Spinner) findViewById(RCT_id[j]).findViewWithTag("Spinner_val")).setEnabled(false);
								//((EditText) findViewById(RCT_id[j]).findViewWithTag("Other_DESC")).setEnabled(false);
							}
							
							/**customized for Roof cover type  disable ends**/
							if(type.equals("Radio"))
				     		{
								if(c.getString(c.getColumnIndex("options")).trim().equals(""))
								{
									if(type_val.equals("true"))
									{
											((RadioButton) findViewById(RCT_id[j]).findViewWithTag(tit)).setChecked(true);
									}
									
									
								}
								else
								{
									try
									{
										((RadioButton) findViewById(RCT_id[j]).findViewWithTag(type_val)).setChecked(true);
									}
									catch (Exception e) {
										// TODO: handle exception
									}
									
								}
									
								
								/**checking for the other validation**/
				     			if(type_val.trim().equals(Other.trim()) && !type_val.trim().equals(""))
				     			{
				     			 ((EditText) findViewById(RCT_id[j]).findViewWithTag("Other_DESC")).setText(Other_txt);
				     			 ((LinearLayout) findViewById(RCT_id[j]).findViewWithTag("Other_DESC_li")).setVisibility(View.VISIBLE);
				     			}
				     			/**checking for the other validation ends**/
				     		}
				     		else if(type.equals("CheckBox"))
					     	{
				     			if(c.getString(c.getColumnIndex("options")).trim().equals(""))
								{
									if(type_val.equals("true"))
									{
											((CheckBox) findViewById(RCT_id[j]).findViewWithTag(tit)).setChecked(true);
									}
									
									
								}
								else
								{
									String[]tmp=type_val.split(",");
									
									for(int k=0;k<tmp.length;k++)
									{
										try
										{
											((CheckBox) findViewById(RCT_id[j]).findViewWithTag(tmp[k])).setChecked(true);
										}
										catch (Exception e) {
											// TODO: handle exception
										}
										
									}
								}
									
								
				     			/**checking for the other validation**/
				     			if(type_val.trim().equals(Other.trim()) && ! type_val.trim().equals(""))
				     			{
				     			 ((EditText) findViewById(RCT_id[j]).findViewWithTag("Other_DESC")).setText(Other_txt);
				     			 ((LinearLayout) findViewById(RCT_id[j]).findViewWithTag("Other_DESC_li")).setVisibility(View.VISIBLE);
				     			}
				     			/**checking for the other validation ends**/
					     	}
				     		else if(type.equals("Spinner"))
				     		{
				     		
				     			
				     			for(int k=0;k<opt.length;k++)
				     			{
				     				if(opt[k].trim().equals(type_val.trim()))
				     				{
				     					((Spinner) findViewById(RCT_id[j]).findViewWithTag("Spinner_val")).setSelection(k);
				     					
				     				}
				     				
				     					
				     			}
				     			
				     			if(Roof_page_value[3].trim().equals(tit.trim()))
				     			{
				     				
				     				((Spinner) findViewById(RCT_id[j]).findViewWithTag("Spinner_val")).setFocusable(true);
				     				((Spinner) findViewById(RCT_id[j]).findViewWithTag("Spinner_val")).setFocusableInTouchMode(true);
				     				((Spinner) findViewById(RCT_id[j]).findViewWithTag("Spinner_val")).requestFocus();
				     				((Spinner) findViewById(RCT_id[j]).findViewWithTag("Spinner_val")).setFocusableInTouchMode(false);
				     				((Spinner) findViewById(RCT_id[j]).findViewWithTag("Spinner_val")).setFocusable(false);
				     				((Spinner) findViewById(RCT_id[j]).findViewWithTag("Spinner_val")).setFocusable(true);
				     			}
				     			/**checking for the other validation**/
				     			if(type_val.trim().equals(Other.trim()) && !type_val.trim().equals(""))
				     			{
				     			
				     			 
				     			 ((EditText) findViewById(RCT_id[j]).findViewWithTag("Other_DESC")).setText(Other_txt);
				     			
				     			 ((LinearLayout) findViewById(RCT_id[j]).findViewWithTag("Other_DESC_li")).setVisibility(View.VISIBLE);
				     			
				     			
				     			}
				     			/**checking for the other validation ends**/
				     			
				     		}
				     		else if(type.equals("DatePicker"))
				     		{ 
				     			
				     			
				     			((EditText) findViewById(RCT_id[j]).findViewWithTag("Date")).setText(type_val);
				     		
				     		}
				     		else if(type.equals("ImagePicker"))
							{
				     			try
				    			{
				    		    	if(((Spinner) findViewById(RCT_id[0]).findViewWithTag("Spinner_val"))!=null)
				    		    	{
				    		    		String cv=((Spinner) findViewById(RCT_id[0]).findViewWithTag("Spinner_val")).getSelectedItem().toString();
				    		    		Cursor c23=cf.arr_db.rawQuery("Select * from "+cf.Roofcoverimages+" Where RIM_masterid=(Select RM_masterid from "+cf.Roof_master+
				    		    				" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+Roof_page_value[5]+"' and RM_Covering='"+cf.encode(cv)+"') order by RIM_Order", null);
				    		    		
				    		    		if(c23.getCount()>0)
				    		    		{
				    		    			((TextView)findViewById(RCT_id[j]).findViewWithTag("View_photos")).setVisibility(View.VISIBLE);
				    		    			((TextView)findViewById(RCT_id[i]).findViewWithTag("View_photos")).setText(Html.fromHtml("<u>View/Edit Photos ("+c23.getCount()+")</u>"));
				    		    		}
				    		    		else
				    		    		{
				    		    			((TextView)findViewById(RCT_id[j]).findViewWithTag("View_photos")).setVisibility(View.GONE);
				    		    		}
				    		    		
				    		    	}
				    		    }
				    		    catch (Exception e) {
				    				// TODO: handle exception
				    			}
				     			//((EditText) findViewById(RCT_id[j]).findViewWithTag("Path")).setText(type_val); 
							}
							
							final Cursor s2=cf.SelectTablefunction(cf.option_list, " Where module_id='"+Roof_page_value[0]+"' and insp_id='"+Roof_page_value[5]+"' and option_tit=( SELECT mst_custom_id From "+cf.master_tbl+" Where mst_text='"+cf.encode(permit_dat_tit)+"')");
							s2.moveToFirst();
							if(tit.trim().equals(no_perdomin_tit))
							{
								if(s2.getCount()>0 && type_val.equals("true"))
								{
									
									((EditText) findViewById(s2.getInt(s2.getColumnIndex("option_tit"))).findViewWithTag("Date")).setText(cf.getResourcesvalue(R.string.na));
									((View) findViewById(s2.getInt(s2.getColumnIndex("option_tit"))).findViewWithTag("Date_pck")).setVisibility(View.INVISIBLE);
									
								}
							}
							
							 	
						 j=RCT_id.length;
					 }
					 
				 }
				 
				 c.moveToNext();
				 c2.moveToNext();
			 }
		}
			GCH_RC_RCT_SA.setText("Update");	
	}
		  
}
	private void RC_Save_validation() {
		// TODO Auto-generated method stub

		try
		{
		String RCN="",ERAE="",RCN_o="",RCN_val="",YR3="",cmt="";
		/**Validate the same question already exist **/
	
		int compl=0;
		int count=tbl_id.length;
		View focusable=null;
		int Requirred;
		String []Check_tit=new String [count];
		/**Validate the same question already exist **/
		String sql="SELECT (Select count(RC_insp_id) FROM "+cf.General_roof_view+" where RC_SRID='"+cf.selectedhomeid+"' and RC_insp_id='"+cf.encode(Roof_page_value[5])+"' and RC_Enable='2' and RC_Condition_type_order='11') as Add_roof, "+
				"(Select count(RC_insp_id) FROM "+cf.General_roof_view+" where RC_SRID='"+cf.selectedhomeid+"' and RC_insp_id='"+cf.encode(Roof_page_value[5])+"' and RC_Enable='2' and RC_Condition_type_order='') as RCN";
		//System.out.println(" the sql ="+sql);
		Cursor chk=cf.arr_db.rawQuery(sql, null);
		chk.moveToFirst();
		if(compl==0  )
		{	
			if(!ADD_chk_top.isChecked() && !Roof_page_value[5].equals("7"))
			{
				if(ADD_savevalidation(false) && chk.getInt(chk.getColumnIndex("Add_roof"))>0)
				{
					
					compl=0;
				}
				else
				{
					compl=1;
					((TextView) findViewById(R.id.ADD_comp)).setVisibility(View.INVISIBLE);
					cf.ShowToast("Please save the Additional Roof Details", 0);
				}
				
			}
		}
		if(!RCN_chk_top.isChecked() && compl==0)
			{
				if(RCN_savevalidation(false)&& chk.getInt(chk.getColumnIndex("RCN"))>0)
				{ 
					compl=0;
				}
				else
				{
					compl=1;
					((TextView) findViewById(R.id.RCN_comp)).setVisibility(View.INVISIBLE);
					cf.ShowToast("Please save the Roof Conditions Noted", 0);
				}
				
			}
		
		
	/**Static values validation Starts**/
		if(compl==0  )
		{	
			if(RC_cmt.getText().toString().trim().equals(""))
			{
				Setfocuserro(RC_cmt, RC_cmt, RC_cmt);
				cf.ShowToast("Please enter the Overall Roof Comments.",0);
				compl=1;
				return;
			}
			else
			{
				insertRCValues();
			}
			/**Static values validation Ends**/
		
		}
		
		
		
		
		
		}catch (Exception e) {
			// TODO: handle exception
			System.out.println("ther is problem in saving "+e.getMessage());
		}
		/**Start insert or update the data ***/
		
	}
	private Boolean RCN_savevalidation(boolean toast_show) {
		// TODO Auto-generated method stub
		String RCN="",RCN_o="",RCN_val="";
		/**Validate the same question already exist **/
		String RCN_db[] = null;
		int db_count=0,compl=0;
		int count=tbl_id.length;
		System.out.println("the tbl length"+tbl_id.length);
		Cursor c12 =cf.SelectTablefunction(cf.option_list, " WHERE module_id='"+Roof_page_value[1]+"' and insp_id='"+Roof_page_value[5]+"' ORDER BY option_potion ");
		c12.moveToFirst();
		RCN_db=new String[count];
		View focusable=null;
		int Requirred;
		String option_other,type,Default_otheroption,Default_option_tyep;
		/**Set the default values **/
		String[] opt1=null,Default_option=null;
		Default_otheroption=Default_option_tyep=option_other=type="";
		 
		String []Check_tit=new String [count];
		
		/**Validate the same question already exist **/
		
		
			/**Set the default values **/
			for (int i=0;i<count;i++)
			{	focusable=null;
				if(i<c12.getCount())
				{
				
				option_other=c12.getString(c12.getColumnIndex("option_other"));
				type=c12.getString(c12.getColumnIndex("option_type"));
				Requirred=c12.getInt(c12.getColumnIndex("Requirred"));
				opt1=c12.getString(c12.getColumnIndex("options")).split(",");
				try
				{
					
					c12.moveToNext();
				}catch (Exception e) {
					// TODO: handle exception
					option_other=Default_otheroption;
					type=Default_option_tyep;
					opt1=Default_option;
				}
				
	    		
				/** Create a Table row Dynamically **/
				}
				else
				{
					Requirred=1;
					option_other=Default_otheroption;
					type=Default_option_tyep;
					opt1=Default_option;
				}
	    					
				/***Leve the validation for the non showed items ***/
				
				if(((View)findViewById(tbl_id[i])).getVisibility()!=View.VISIBLE)
				{
					Requirred=0;
				}
			
			try
			{
				String Err="";
				if(type.equals("Radio"))
	     		{
					Err="Please select option for ";
				
						RCN_val=getvalfromoption(tbl_id[i], opt1);
					
					/**checking for the other validation**/
						
	     			if(RCN_val.trim().equals(option_other.trim()) && !RCN_val.trim().equals(""))
	     			{
	     				
	     				String otherval=((EditText) findViewById(tbl_id[i]).findViewWithTag("Other_DESC")).getText().toString();
	     				
	     				if(otherval.trim().equals(""))
	     				{
	     					focusable=((EditText) findViewById(tbl_id[i]).findViewWithTag("Other_DESC"));
	     					RCN_val="";
	     					Err="Please enter comments for ";
	     				}
	     			}
	     			/**checking for the other validation ends**/
	     		}
	     		else if(type.equals("CheckBox"))
		     	{
	     			Err="Please select option for ";
					
						RCN_val=getvalfromchkoption(tbl_id[i], opt1);
					
					/**checking for the other validation**/
	     			if(RCN_val.trim().equals(option_other.trim()) && !RCN_val.trim().equals(""))
	     			{
	     				
	     				String otherval=((EditText) findViewById(tbl_id[i]).findViewWithTag("Other_DESC")).getText().toString();
	     				if(otherval.trim().equals(""))
	     				{
	     					focusable=((EditText) findViewById(tbl_id[i]).findViewWithTag("Other_DESC"));
	     					RCN_val="";
	     					Err="Please enter comments for ";
	     				}
	     			}
	     			/**checking for the other validation ends**/
	     		}
	     		else if(type.equals("Spinner"))
	     		{
	     			Err="Please select ";
	     			RCN_val=((Spinner) findViewById(tbl_id[i]).findViewWithTag("Spinner_val")).getSelectedItem().toString();
	     			if(RCN_val.equals("-Select-"))
	     			{
	     				focusable=((Spinner) findViewById(tbl_id[i]).findViewWithTag("Spinner_val"));
	     				RCN_val="";
	     			}
	     			/**checking for the other validation**/
	     			if(RCN_val.trim().equals(option_other.trim()) && !RCN_val.trim().equals(""))
	     			{
	     				
	     				String otherval=((EditText) findViewById(RCT_id[i]).findViewWithTag("Other_DESC")).getText().toString();
	     				if(otherval.trim().equals(""))
	     				{
	     					focusable=((EditText) findViewById(tbl_id[i]).findViewWithTag("Other_DESC"));
	     					RCN_val="";
	     					Err="Please enter comments for ";
	     				}
	     			}
	     			/**checking for the other validation ends**/
	     		}
	     		else if(type.equals("DatePicker"))
	     		{ 
	     			focusable=((EditText) findViewById(tbl_id[i]).findViewWithTag("Date"));
	     			
	     			Err="Please select ";
	     			RCN_val=((EditText) findViewById(tbl_id[i]).findViewWithTag("Date")).getText().toString();
	     			try
	     			{
	     				if(!cf.checkfortodaysdate(RCN_val).equals("true"))
	     				{
	     					RCN_val="";
	     					RCN=((EditText) findViewById(tbl_id[i]).findViewWithTag("Title_ed")).getText().toString().trim();
	     					cf.ShowToast(RCN.trim()+" should not be greater than today's date.",0);
	     					return false;
	     					
	     				}
	     			}catch (Exception e) {
						// TODO: handle exception
					}
	     			
	     		}
	     		else if(type.equals("EditText"))
	     		{ 
	     			focusable=((EditText) findViewById(tbl_id[i]).findViewWithTag("EditText"));
	     			Err="Please enter value for ";
	     			RCN_val=((EditText) findViewById(tbl_id[i]).findViewWithTag("EditText")).getText().toString();
	     			
	     		}
				
			
			RCN=((EditText) findViewById(tbl_id[i]).findViewWithTag("Title_ed")).getText().toString().trim();
			
			Check_tit[i]=RCN;
			
			
			
			if(RCN.equals(Roof_page_value[4]))
			{
				Default_option=opt1;
				Default_otheroption=option_other;
				Default_option_tyep=type;
			}
			if(!RCN.equals("") )
			{
				if(RCN_val.equals("") && Requirred==1)
				{
					if(toast_show)
					{
						cf.ShowToast(Err.trim()+" "+RCN.trim(),0);
					}
					compl=1;
					if(focusable!=null && RCN_layout_head.getVisibility()==View.VISIBLE)
					{
						focusable.setFocusable(true);
						focusable.setFocusableInTouchMode(true);
						focusable.requestFocus();
						
					}
					else
					{
						RCN_chk_top.setFocusableInTouchMode(true);
						RCN_chk_top.requestFocus();
						RCN_chk_top.setFocusableInTouchMode(false);
						RCN_chk_top.setFocusable(false);
						RCN_chk_top.setFocusable(true);
					}
					return false;
				}
			}
			else
			{
				cf.ShowToast("Please enter the title for add new conditions.",0);
				compl=1;
				return false;
			}	
			
			
			}catch (Exception e) {
				// TODO: handle exception
				System.out.println("error"+e.getMessage());
			}
			
								
			
			}
			if(!RCN_chk_top.isChecked())
			{
			for(int i=0;i<count;i++)
			{
				for(int j=0;j<count;j++)
				{
					if(Check_tit[i].trim().toLowerCase().equals(Check_tit[j].trim().toLowerCase())&& (i!=j))
					{
						cf.ShowToast("Already Exists! Please enter a different title",0);
						//cf.ShowToast("Condition title of \""+Check_tit[j]+"\" was repeated please change it.",0);
						return false;
					}
				}
			}
		}
			
		return true;
	}


	private Boolean ADD_savevalidation(boolean toast_show) {
		// TODO Auto-generated method stub
		if((Roof_page_value[5].equals("5")) || Roof_page_value[5].equals("6"))
	 	{
			int RG_id=ADD_com2_RG.getCheckedRadioButtonId();
			String s=(RG_id!=-1)?ADD_com2_RG.findViewById(RG_id).getTag().toString():"";
			if(s.equals(""))
			{
				Setfocuserro(ADD_com2_RG, ADD_layout_head, ADD_chk_top);
				if(toast_show)
				{
					cf.ShowToast("Please select the option for "+cf.getResourcesvalue(R.string.ROOF_Survey5)+".",0);
				}
				
				return false;
			}
			RG_id=ADD_com2_PDU_RG.getCheckedRadioButtonId();
			s=(RG_id!=-1)?ADD_com2_PDU_RG.findViewById(RG_id).getTag().toString():"";
			if(s.equals(""))
			{
				Setfocuserro(ADD_com2_PDU_RG, ADD_layout_head, ADD_chk_top);
				if(toast_show)
				{
					cf.ShowToast("Please select the option for "+cf.getResourcesvalue(R.string.COMER2)+".",0);
				}
				return false;
			}
			else if(s.equals("Yes"))
			{
				String other=ADD_com2_PDU_no_ed.getText().toString().trim();	
				if(other.trim().equals(""))
				{
					Setfocuserro(ADD_com2_PDU_no_ed, ADD_layout_head, ADD_chk_top);
					if(toast_show)
					{
						cf.ShowToast("Please enter Permit No for "+cf.getResourcesvalue(R.string.COMER2)+".",0);
					}
					return false;
				}
				other=ADD_com2_PDU_date_ed.getText().toString().trim();	
				if(other.trim().equals(""))
				{
					Setfocuserro(ADD_com2_PDU_date_ed, ADD_layout_head, ADD_chk_top);
					if(toast_show)
					{
						cf.ShowToast("Please enter Date for "+cf.getResourcesvalue(R.string.COMER2)+".",0);
					}
					return false;
				}
				else if(!ADD_com2_PDU_date_ed.getText().toString().trim().equals(cf.getResourcesvalue(R.string.na))&& !ADD_com2_PDU_date_ed.getText().toString().trim().equals(cf.getResourcesvalue(R.string.nd)))
	     		{
     				try
	     			{
	     				if(!cf.checkfortodaysdate(ADD_com2_PDU_date_ed.getText().toString().trim()).equals("true"))
	     				{
	     					Setfocuserro(ADD_com2_PDU_date_ed, ADD_layout_head, ADD_chk_top);
	     					if(toast_show)
	     					{
	     						cf.ShowToast("Date should not be greater than Today's date for "+cf.getResourcesvalue(R.string.COMER2)+".",0);
	     					}
	    					return false;
	     					
	     				}
	     			}catch (Exception e) {
						// TODO: handle exception
					}
	     		}
				
			}
			RG_id=ADD_com2_RME_RG.getCheckedRadioButtonId();
			s=(RG_id!=-1)?ADD_com2_RME_RG.findViewById(RG_id).getTag().toString():"";
			if(s.equals(""))
			{
				Setfocuserro(ADD_com2_RME_RG, ADD_layout_head, ADD_chk_top);
				if(toast_show)
				{
					cf.ShowToast("Please select the option for "+cf.getResourcesvalue(R.string.COMER4)+".",0);
				}
				return false;
			}
			if(s.equals(getResources().getString(R.string.yes)))
			{
				s=cf.getselected_chk(ADD_com2_RMS);
				if(s.equals(""))
				{
					Setfocuserro(ADD_com2_RMS[0], ADD_layout_head, ADD_chk_top);
					if(toast_show)
					{
						cf.ShowToast("Please select the option for "+cf.getResourcesvalue(R.string.COMER3)+".",0);
					}
					return false;
				}
				if(ADD_com2_RMS[0].isChecked())
				{
					
					String other=ADD_com2_RMS_HVAC_ed.getText().toString().trim();
					String other1=RMS_loc[0].getText().toString().trim();
					
						
					if(other.trim().equals(""))
					{
						
						Setfocuserro(ADD_com2_RMS_HVAC_ed, ADD_layout_head, ADD_chk_top);
						
						if(toast_show)
						{
							
							cf.ShowToast("Please enter Percentage for "+cf.getResourcesvalue(R.string.hvacE)+".",0);
						}
						return false;
					}
					else 
					{
					
						try
						{
							int	percent=Integer.parseInt(other);
							if(percent<1 || percent>100)
							{
								if(toast_show)
								{
									cf.ShowToast("Please enter valid Percentage for "+cf.getResourcesvalue(R.string.hvacE)+".",0);
								}
								return false;
							}
							
						}
						catch (Exception e) {
							// TODO: handle exception
						}
					}
					if(other1.equals(""))
					{
						
						if(toast_show)
						{
							cf.ShowToast("Please enter Location for "+cf.getResourcesvalue(R.string.hvacE)+".",0);
						}
						return false;
					}
				}
				if(ADD_com2_RMS[1].isChecked())
				{
					String other=ADD_com2_RMS_VE_ed.getText().toString().trim();	
					String other1=RMS_loc[1].getText().toString().trim();
					if(other.trim().equals(""))
					{
						Setfocuserro(ADD_com2_RMS_VE_ed, ADD_layout_head, ADD_chk_top);
						if(toast_show)
						{
							cf.ShowToast("Please enter Percentage for "+cf.getResourcesvalue(R.string.VentalationE)+".",0);
						}
						return false;
					}
					else 
					{
					
						try
						{
							int	percent=Integer.parseInt(other);
							if(percent<1 || percent>100)
							{
								if(toast_show)
								{
									cf.ShowToast("Please enter valid Percentage for "+cf.getResourcesvalue(R.string.VentalationE)+".",0);
								}
								return false;
							}
							
						}
						catch (Exception e) {
							// TODO: handle exception
						}
					}
					if(other1.equals(""))
					{
						
						if(toast_show)
						{
							cf.ShowToast("Please enter Location for "+cf.getResourcesvalue(R.string.VentalationE)+".",0);
						}
						return false;
					}
					
					
					
				}
				if(ADD_com2_RMS[2].isChecked())
				{
					String other=ADD_com2_RMS_SE_ed.getText().toString().trim();	
					String other1=RMS_loc[1].getText().toString().trim();
					if(other.trim().equals(""))
					{
						Setfocuserro(ADD_com2_RMS_SE_ed, ADD_layout_head, ADD_chk_top);
						if(toast_show)
						{
							cf.ShowToast("Please enter Percentage for "+cf.getResourcesvalue(R.string.StorageE)+".",0);
						}
						return false;
					}
					else 
					{
					
						try
						{
							int	percent=Integer.parseInt(other);
							if(percent<1 || percent>100)
							{
								if(toast_show)
								{
									cf.ShowToast("Please enter valid Percentage for "+cf.getResourcesvalue(R.string.StorageE)+".",0);
								}
								return false;
							}
							
						}
						catch (Exception e) {
							// TODO: handle exception
						}
					}
					if(other1.equals(""))
					{
						
						if(toast_show)
						{
							cf.ShowToast("Please enter Location for "+cf.getResourcesvalue(R.string.StorageE)+".",0);
						}
						return false;
					}
				}
				if(ADD_com2_RMS[7].isChecked())
				{
					String other=ADD_com2_RMS_OE_ed.getText().toString().trim();	
					if(other.trim().equals(""))
					{
						Setfocuserro(ADD_com2_RMS_OE_ed, ADD_layout_head, ADD_chk_top);
						if(toast_show)
						{
							cf.ShowToast("Please enter the Other text for "+cf.getResourcesvalue(R.string.OtherE)+".",0);
						}
						return false;
					}
				}
				RG_id=RMS_RG[0].getCheckedRadioButtonId();
				s=(RG_id!=-1)?RMS_RG[0].findViewById(RG_id).getTag().toString():"";
				if(s.equals(""))
				{
					Setfocuserro(RMS_RG[0], ADD_layout_head, ADD_chk_top);
					if(toast_show)
					{
						cf.ShowToast("Please select option for "+cf.getResourcesvalue(R.string.EquipmentL)+".",0);
					}
					return false;
				}
				RG_id=RMS_RG[1].getCheckedRadioButtonId();
				s=(RG_id!=-1)?RMS_RG[1].findViewById(RG_id).getTag().toString():"";
				if(s.equals(""))
				{
					Setfocuserro(RMS_RG[1], ADD_layout_head, ADD_chk_top);
					if(toast_show)
					{
						cf.ShowToast("Please select option for "+cf.getResourcesvalue(R.string.DamageR)+".",0);
					}
					return false;
				}
			}	
			
	 	}
		if((Roof_page_value[5].equals("1") || Roof_page_value[5].equals("2"))||(Roof_page_value[5].equals("4")))
	 	{
			int RG_id=ADD_RG1.getCheckedRadioButtonId();
			String s=(RG_id!=-1)?ADD_RG1.findViewById(RG_id).getTag().toString():"";
			if(s.equals(""))
			{
				Setfocuserro(ADD_RG1, ADD_layout_head, ADD_chk_top);
				if(toast_show)
				{
					cf.ShowToast("Please select option for "+cf.getResourcesvalue(R.string.roof_add_4poin_txt)+".",0);
				}
				return false;
			}
		
			
	}
		

		return true;
	}
	
	private Boolean RCN_Insert_Update()
	{
		String RCN="",ERAE="",RCN_o="",RCN_val="",YR3="",cmt="";
		/**Validate the same question already exist **/
		String RCN_db[] = null;
		int db_count=0;
		int count=tbl_id.length;
		
	
		RCN_db=new String[count];
		int Requirred;
		String option_other,type,Default_otheroption,Default_option_tyep;
		String[] opt1=null,Default_option=null;
		Default_otheroption=Default_option_tyep=option_other=type="";
		 
		String []Check_tit=new String [count];
		/**Start insert or update the data ***/

		
		try
		{
			Cursor c22=cf.SelectTablefunction(cf.Roof_master, " Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(Roof_page_value[5])+"' and RM_module_id='1' ");
			if(c22.getCount()<=0)
			{
				cf.arr_db.execSQL("INSERT INTO "+cf.Roof_master+" (RM_inspectorid,RM_srid,RM_insp_id,RM_module_id) Values ('"+cf.Insp_id+"','"+cf.selectedhomeid+"','"+cf.encode(Roof_page_value[5])+"','1')");
			}
			String Sql_insert="",Sql_update="",case_conT=" CASE RC_Id",case_conTD=" CASE RC_Id",case_conTV=" CASE RC_Id",case_conE=" CASE RC_Id",case_con_id="";
			int max_c=1;
			cf.arr_db.execSQL("Delete From "+cf.General_Roof_information+" WHERE RC_masterid=(SELECT RM_masterid FROM "+cf.Roof_master+" Where RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(Roof_page_value[5])+"' and RM_module_id='1' )  and (RC_Condition_type_order='' or (RC_Condition_type_order='12' and RC_Condition_type='"+cf.encode("RCN_notapplicable")+"')) ");
			Cursor c12 =cf.SelectTablefunction(cf.option_list, " WHERE module_id='"+Roof_page_value[1]+"' and insp_id='"+Roof_page_value[5]+"' ORDER BY option_potion ");
			System.out.println(" the where statement "+" WHERE module_id='"+Roof_page_value[1]+"' and insp_id='"+Roof_page_value[5]+"' ORDER BY option_potion ");
			c12.moveToFirst();
			Cursor c5=cf.arr_db.rawQuery("Select * from "+cf.General_roof_view+" Where RC_SRID='"+cf.selectedhomeid+"' and RC_page='"+this.Roof_page_value[1]+"'  and RC_insp_id='"+cf.encode(Roof_page_value[5])+"'",null);
		
			
		for (int i=0;i<count;i++)
		{
		RCN=RCN_o=RCN_val=ERAE=YR3=cmt="";	
		int id=-1;
			
				System.out.println("tyhe count was"+c12.getCount());
				if(i<c12.getCount())
				{
				
					option_other=c12.getString(c12.getColumnIndex("option_other"));
					type=c12.getString(c12.getColumnIndex("option_type"));
					Requirred=c12.getInt(c12.getColumnIndex("Requirred"));
					opt1=c12.getString(c12.getColumnIndex("options")).split(",");
					id=c12.getInt(c12.getColumnIndex("option_tit"));
					try
					{
						
						c12.moveToNext();
					}catch (Exception e) {
						// TODO: handle exception
					
						option_other=Default_otheroption;
						type=Default_option_tyep;
						opt1=Default_option;
					}
				}
				else
				{
					option_other=Default_otheroption;
					type=Default_option_tyep;
					opt1=Default_option;
				}
				
				if(type.equals("Radio"))
	     		{
					
					
						RCN_val=getvalfromoption(tbl_id[i], opt1);
					
					/**checking for the other validation**/
	     			if(RCN_val.trim().equals(option_other.trim()) && !RCN_val.trim().equals(""))
	     			{
	     				
	     				RCN_o=((EditText) findViewById(tbl_id[i]).findViewWithTag("Other_DESC")).getText().toString();
	     				
	     			}
	     			/**checking for the other validation ends**/
	     		}
	     		else if(type.equals("CheckBox"))
		     	{
	     			
					
						RCN_val=getvalfromchkoption(tbl_id[i], opt1);
					
					/**checking for the other validation**/
	     			if(RCN_val.trim().equals(option_other.trim()) && !RCN_val.trim().equals(""))
	     			{
	     				
	     				RCN_o=((EditText) findViewById(tbl_id[i]).findViewWithTag("Other_DESC")).getText().toString();
	     				
	     			}
	     			/**checking for the other validation ends**/
	     		}
	     		else if(type.equals("Spinner"))
	     		{
	     			
	     			RCN_val=((Spinner) findViewById(tbl_id[i]).findViewWithTag("Spinner_val")).getSelectedItem().toString();
	     			if(RCN_val.equals("-Select-"))
	     			{
	     				RCN_val="";
	     			}
	     			/**checking for the other validation**/
	     			if(RCN_val.trim().equals(option_other.trim()) && !RCN_val.trim().equals(""))
	     			{
	     				
	     				RCN_o=((EditText) findViewById(RCT_id[i]).findViewWithTag("Other_DESC")).getText().toString();
	     						     			}
	     			/**checking for the other validation ends**/
	     		}
	     		else if(type.equals("DatePicker"))
	     		{ 
	     			
	     			
	     			RCN_val=((EditText) findViewById(tbl_id[i]).findViewWithTag("Date")).getText().toString();
	     			
	     		}
	     		else if(type.equals("EditText"))
	     		{ 
	     			
	     			
	     			RCN_val=((EditText) findViewById(tbl_id[i]).findViewWithTag("EditText")).getText().toString();
	     			
	     		}
				RCN=((TextView) findViewById(tbl_id[i]).findViewWithTag("Title_ed")).getText().toString().trim();
				//System.out.println("The rcn val="+RCN);
				if(RCN.equals(Roof_page_value[4]))
				{
					Default_option=opt1;
					Default_otheroption=option_other;
					Default_option_tyep=type;
				}
				
				/***Add empty value to the non visible contents ***/
				if(((View)findViewById(tbl_id[i])).getVisibility()!=View.VISIBLE)
				{
					RCN_o="";
					RCN_val="";
				}
				
				/***Add empty value to the non visible contents ***/
				
			
				
				
				
			
				String temp="";
				try
				{
					
				//temp=((TextView) findViewById(tbl_id[i]).findViewWithTag("GCH_RCT_id")).getText().toString().trim();
				
				}
				catch (Exception e) {
					// TODO: handle exception
					System.out.println("eception comes "+e.getMessage());
					temp="";
				}
			System.out.println("the tem"+temp+ " id ="+id);
				if(temp!=null)
				{
					if(!temp.equals("") && id!=-1 )
					{
						c5.moveToFirst();
						boolean row_pre=false;
						int pos=0;
						System.out.println("come in the first correct"+c5.getCount());
						 for(int ct=0;ct<c5.getCount();ct++)
						 {
							
							 if(c5.getString(c5.getColumnIndex("RC_Id")).equals(temp.trim()))
							 {
								 row_pre=true;
								 pos=ct;
								 break;
							 }
							 // and RC_Id='"+temp+"'
							 c5.moveToNext();
						 }
						 c5.moveToPosition(pos);
							if(row_pre)
							{
								if(id==-1)
								{
									
									//Sql_update+="UPDATE  "+cf.General_Roof_information +" SET RC_Condition_type='"+RCN+"', RC_Condition_type_DESC='"+cf.encode(RCN_o)+"',RC_Condition_type_val='"+cf.encode(RCN_val)+"',RC_Enable='2' WHERE RC_SRID='"+cf.selectedhomeid+"'  and RC_insp_id='"+cf.encode(Roof_page_value[5])+"'  and RC_Id='"+temp+"';";
									case_conT+=" WHEN "+temp+" THEN '"+cf.encode(RCN)+"'";
									case_conTD+=" WHEN "+temp+" THEN '"+cf.encode(RCN_o)+"'";
									case_conTV+=" WHEN "+temp+" THEN '"+cf.encode(RCN_val)+"'";
									case_con_id+=temp+",";
									
								//	cf.arr_db.execSQL("UPDATE  "+cf.General_Roof_information +" SET RC_Condition_type='"+RCN+"', RC_Condition_type_DESC='"+cf.encode(RCN_o)+"',RC_Condition_type_val='"+cf.encode(RCN_val)+"',RC_Enable='2' WHERE RC_SRID='"+cf.selectedhomeid+"'  and RC_insp_id='"+cf.encode(Roof_page_value[5])+"'  and RC_Id='"+temp+"'");
								}
								else
								{
									
									//Sql_update+="UPDATE  "+cf.General_Roof_information +" SET RC_Condition_type='"+id+"', RC_Condition_type_DESC='"+cf.encode(RCN_o)+"',RC_Condition_type_val='"+cf.encode(RCN_val)+"',RC_Enable='2' WHERE RC_SRID='"+cf.selectedhomeid+"'  and RC_insp_id='"+cf.encode(Roof_page_value[5])+"'  and RC_Id='"+temp+"';";
									case_conT+=" WHEN "+temp+" THEN '"+id+"'";
									case_conTD+=" WHEN "+temp+" THEN '"+cf.encode(RCN_o)+"'";
									case_conTV+=" WHEN "+temp+" THEN '"+cf.encode(RCN_val)+"'";
									case_con_id+=temp+",";
								//	cf.arr_db.execSQL("UPDATE  "+cf.General_Roof_information +" SET RC_Condition_type='"+id+"', RC_Condition_type_DESC='"+cf.encode(RCN_o)+"',RC_Condition_type_val='"+cf.encode(RCN_val)+"',RC_Enable='2' WHERE RC_SRID='"+cf.selectedhomeid+"'  and RC_insp_id='"+cf.encode(Roof_page_value[5])+"'  and RC_Id='"+temp+"'");
								}
				
							}
							else
							{
								if(Sql_insert.equals(""))
								{
									Sql_insert="INSERT INTO "+cf.General_Roof_information +" SELECT (Select (case when ((SELECT MAX(RC_Id) FROM "+cf.General_Roof_information+") is NULL ) then '0' else (SELECT MAX(RC_Id) FROM "+cf.General_Roof_information+") end))+"+max_c+"  as RC_Id,(SELECT RM_masterid as RC_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' ) ,'"+id+"' as RC_Condition_type,'"+cf.encode(RCN_o)+"' as RC_Condition_type_DESC,'"+cf.encode(RCN_val)+"' as RC_Condition_type_val,'' as RC_Condition_type_order,'2' as RC_Enable";
									max_c++;
								}
								else
								{
									Sql_insert+=" UNION SELECT  (Select (case when ((SELECT MAX(RC_Id) FROM "+cf.General_Roof_information+") is NULL ) then '0' else (SELECT MAX(RC_Id) FROM "+cf.General_Roof_information+") end))+"+max_c+",(SELECT RM_masterid as RC_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' ),'"+id+"','"+cf.encode(RCN_o)+"','"+cf.encode(RCN_val)+"','' as RC_Condition_type_order,'2' as RC_Enable";
									max_c++;
								}
								/*cf.arr_db.execSQL("INSERT INTO "+cf.General_Roof_information +" (RC_SRID,RC_page,RC_Condition_type,RC_Condition_type_DESC,RC_Condition_type_val,RC_Enable,RC_insp_id) VALUES " +
										"('"+cf.selectedhomeid+"','"+cf.encode(Roof_page_value[1])+"','"+id+"','"+cf.encode(RCN_o)+"','"+cf.encode(RCN_val)+"','2','"+Roof_page_value[5]+"')");
				*/
							}
						}
					else
					{
						
						if(!temp.equals(""))
						{
							RCN=temp;
						}
						else if(id!=-1)
						{
							RCN=id+"";
						}
						if(Sql_insert.equals(""))
						{
							
							Sql_insert="INSERT INTO "+cf.General_Roof_information +" SELECT (Select (case when ((SELECT MAX(RC_Id) FROM "+cf.General_Roof_information+") is NULL ) then '0' else (SELECT MAX(RC_Id) FROM "+cf.General_Roof_information+") end))+"+max_c+" as RC_Id,(SELECT RM_masterid as RC_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' ),'"+cf.encode(RCN)+"' as RC_Condition_type,'"+cf.encode(RCN_o)+"' as RC_Condition_type_DESC,'"+cf.encode(RCN_val)+"' as RC_Condition_type_val,'' as RC_Condition_type_order,'2' as RC_Enable";
							max_c++;
						}
						else
						{
							Sql_insert+=" UNION SELECT (Select (case when ((SELECT MAX(RC_Id) FROM "+cf.General_Roof_information+") is NULL ) then '0' else (SELECT MAX(RC_Id) FROM "+cf.General_Roof_information+") end))+"+max_c+",(SELECT RM_masterid as RC_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' ) ,'"+cf.encode(RCN)+"','"+cf.encode(RCN_o)+"','"+cf.encode(RCN_val)+"','' as RC_Condition_type_order,'2' as RC_Enable";
							max_c++;
						}
						//cf.arr_db.execSQL("INSERT INTO "+cf.General_Roof_information +" (RC_SRID,RC_page,RC_Condition_type,RC_Condition_type_DESC,RC_Condition_type_val,RC_Enable,RC_insp_id) VALUES ('"+cf.selectedhomeid+"','"+cf.encode(Roof_page_value[1])+"','"+cf.encode(RCN)+"','"+cf.encode(RCN_o)+"','"+cf.encode(RCN_val)+"','2','"+Roof_page_value[5]+"')");
			
					}
					
				}
				else
				{
					System.out.println("the RCN  value"+RCN);
					if(id!=-1)
					{
						RCN=id+"";
					}
					//cf.arr_db.execSQL("INSERT INTO "+cf.General_Roof_information +" (RC_SRID,RC_page,RC_Condition_type,RC_Condition_type_DESC,RC_Condition_type_val,RC_Enable,RC_insp_id) VALUES ('"+cf.selectedhomeid+"','"+cf.encode(Roof_page_value[1])+"','"+cf.encode(RCN)+"','"+cf.encode(RCN_o)+"','"+cf.encode(RCN_val)+"','2','"+Roof_page_value[5]+"')");
					if(Sql_insert.equals(""))
					{
						Sql_insert="INSERT INTO "+cf.General_Roof_information +" SELECT (Select (case when ((SELECT MAX(RC_Id) FROM "+cf.General_Roof_information+") is NULL ) then '0' else (SELECT MAX(RC_Id) FROM "+cf.General_Roof_information+") end))+"+max_c+" as RC_Id,(SELECT RM_masterid as RC_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' ) ,'"+cf.encode(RCN)+"' as RC_Condition_type,'"+cf.encode(RCN_o)+"' as RC_Condition_type_DESC,'"+cf.encode(RCN_val)+"' as RC_Condition_type_val,'' as RC_Condition_type_order,'2' as RC_Enable";
						max_c++;
					}
					else
					{
						Sql_insert+=" UNION SELECT (Select (case when ((SELECT MAX(RC_Id) FROM "+cf.General_Roof_information+") is NULL ) then '0' else (SELECT MAX(RC_Id) FROM "+cf.General_Roof_information+") end))+"+max_c+",(SELECT RM_masterid as RC_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' ) ,'"+cf.encode(RCN)+"','"+cf.encode(RCN_o)+"','"+cf.encode(RCN_val)+"','' as RC_Condition_type_order,'2' as RC_Enable";
					}
			
				}
				
				
		
		}
		c5.moveToFirst();
		boolean row_pre=false;
		int pos=0;
		
		 for(int ct=0;ct<c5.getCount();ct++)
		 {//RC_Condition_type='"+cf.encode("RCN_notapplicable")+"'
			 if(c5.getString(c5.getColumnIndex("RC_Condition_type")).equals("RCN_notapplicable"))
			 {
				 row_pre=true;
				 pos=ct;
				 break;
			 }
			 // and RC_Id='"+temp+"'
			 c5.moveToNext();
		 }
		 c5.moveToPosition(pos);
	//Cursor c1=cf.SelectTablefunction(cf.General_roof_view, " Where RC_SRID='"+cf.selectedhomeid+"' and RC_page='"+this.Roof_page_value[1]+"'  and RC_insp_id='"+cf.encode(Roof_page_value[5])+"' and  RC_Condition_type='"+cf.encode("RCN_notapplicable")+"'");
		
		if(row_pre)
		{
			case_conT+=" WHEN "+c5.getString(c5.getColumnIndex("RC_Id"))+" THEN '"+cf.encode("RCN_notapplicable")+"'";
			case_conTD+=" WHEN "+c5.getString(c5.getColumnIndex("RC_Id"))+" THEN ''";
			case_conTV+=" WHEN "+c5.getString(c5.getColumnIndex("RC_Id"))+" THEN 'false'";
			case_con_id+=c5.getString(c5.getColumnIndex("RC_Id"))+",";
			//cf.arr_db.execSQL("UPDATE  "+cf.General_Roof_information +" SET RC_Condition_type_val='false',RC_Enable='2' WHERE RC_SRID='"+cf.selectedhomeid+"' and  RC_insp_id='"+cf.encode(Roof_page_value[5])+"' and RC_Condition_type_order='12' and  RC_Condition_type='"+cf.encode("RCN_notapplicable")+"'");
		}
		else
		{
			if(Sql_insert.equals(""))
			{
				Sql_insert="INSERT INTO "+cf.General_Roof_information +" SELECT (Select (case when ((SELECT MAX(RC_Id) FROM "+cf.General_Roof_information+") is NULL ) then '0' else (SELECT MAX(RC_Id) FROM "+cf.General_Roof_information+") end))+"+max_c+" as RC_Id,(SELECT RM_masterid as RC_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' ) ,'"+cf.encode("RCN_notapplicable")+"' as RC_Condition_type,'' as RC_Condition_type_DESC,'false' as RC_Condition_type_val,'12' as RC_Condition_type_order,'2' as RC_Enable";
				max_c++;
			}
			else
			{
				Sql_insert+=" UNION SELECT (Select (case when ((SELECT MAX(RC_Id) FROM "+cf.General_Roof_information+") is NULL ) then '0' else (SELECT MAX(RC_Id) FROM "+cf.General_Roof_information+") end))+"+max_c+",(SELECT RM_masterid as RC_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' ) ,'"+cf.encode("RCN_notapplicable")+"','','false','12' as RC_Condition_type_order,'2' as RC_Enable";
			}
			//cf.arr_db.execSQL("INSERT INTO "+cf.General_Roof_information +" (RC_SRID,RC_page,RC_Condition_type,RC_Condition_type_DESC,RC_Condition_type_val,RC_Condition_type_order,RC_Enable,RC_insp_id) VALUES ('"+cf.selectedhomeid+"','"+cf.encode(Roof_page_value[1])+"','"+cf.encode("RCN_notapplicable")+"','','false','12','2','"+Roof_page_value[5]+"')");
		}
	
		if(!Sql_insert.equals(""))
		{
			
			cf.arr_db.execSQL(Sql_insert);
		}
		if(!case_con_id.equals(""))
		{			
			case_con_id=case_con_id.substring(0,case_con_id.length()-1);
		
			
			Sql_update=" UPDATE "+ cf.General_Roof_information+" SET RC_Condition_type= "+case_conT+" End ,RC_Condition_type_DESC= "+case_conTD+" END ,RC_Condition_type_val= "+case_conTV+" End,RC_Enable='2'  Where RC_Id IN ("+case_con_id+")";
			System.out.println("the wrong sql"+Sql_update);
			cf.arr_db.execSQL(Sql_update);
			
		}
		RCN_ShowValue.removeAllViews();
		show_GRID_Value(1);
		}catch(Exception e)
		{
			System.out.println("the erro was "+e.getMessage());
			return false;
		}
		return true;
	}
	private Boolean Add_Insert_Update()
	{
		Cursor c=null;
		try
		{
			Cursor c22=cf.SelectTablefunction(cf.Roof_master, " Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(Roof_page_value[5])+"' and RM_module_id='1' ");
			if(c22.getCount()<=0)
			{
				cf.arr_db.execSQL("INSERT INTO "+cf.Roof_master+" (RM_inspectorid,RM_srid,RM_insp_id,RM_module_id) Values ('"+cf.Insp_id+"','"+cf.selectedhomeid+"','"+cf.encode(Roof_page_value[5])+"','1')");
			}
			if((Roof_page_value[5].equals("5")) || Roof_page_value[5].equals("6"))
		 	{
				String ADD_com2_RG_v,ADD_com2_PDU_RG_v,ADD_com2_PDU_RG_o,ADD_com2_RMS_v,ADD_com2_RMS_o,ADD_com2_RME_RG_v,ADD_com2_Other_v,ADD_RMS_RG_v1,ADD_RMS_RG_v2;
				ADD_com2_RG_v=ADD_com2_PDU_RG_v=ADD_com2_PDU_RG_o=ADD_com2_RMS_v=ADD_com2_RMS_o=ADD_com2_RME_RG_v=ADD_com2_Other_v=ADD_RMS_RG_v1=ADD_RMS_RG_v2="";
				
				int RG_id=ADD_com2_RG.getCheckedRadioButtonId();
				ADD_com2_RG_v=(RG_id!=-1)?ADD_com2_RG.findViewById(RG_id).getTag().toString():"";
				
				RG_id=ADD_com2_PDU_RG.getCheckedRadioButtonId();
				ADD_com2_PDU_RG_v=(RG_id!=-1)?ADD_com2_PDU_RG.findViewById(RG_id).getTag().toString():"";
				if(ADD_com2_PDU_RG_v.equals("Yes"))
				{
					ADD_com2_PDU_RG_o="NO:";
					ADD_com2_PDU_RG_o+=ADD_com2_PDU_no_ed.getText().toString().trim()+"#94";	
					ADD_com2_PDU_RG_o+="DATE:";
					ADD_com2_PDU_RG_o+=ADD_com2_PDU_date_ed.getText().toString().trim();	
					
				}
				
				/****
				#94 used to split the diffrent values
				#95 used to split the values of percentage and location 
				***/
				ADD_com2_RMS_v=cf.getselected_chk(ADD_com2_RMS);
				ADD_com2_RMS_o=ADD_com2_RMS_HVAC_ed.getText().toString().trim()+"#94"+ADD_com2_RMS_VE_ed.getText().toString().trim()
						+"#94"+ADD_com2_RMS_SE_ed.getText().toString().trim()+"#94"+ADD_com2_RMS_OE_ed.getText().toString().trim();
				ADD_com2_RMS_o+="#95"+RMS_loc[0].getText().toString().trim()+"#94"+RMS_loc[1].getText().toString().trim()+"#94"+RMS_loc[2].getText().toString().trim();
				RG_id=ADD_com2_RME_RG.getCheckedRadioButtonId();
				ADD_com2_RME_RG_v=(RG_id!=-1)?ADD_com2_RME_RG.findViewById(RG_id).getTag().toString():"";
				RG_id=RMS_RG[0].getCheckedRadioButtonId();
				ADD_RMS_RG_v1=(RG_id!=-1)?RMS_RG[0].findViewById(RG_id).getTag().toString():"";
				RG_id=RMS_RG[1].getCheckedRadioButtonId();
				ADD_RMS_RG_v2=(RG_id!=-1)?RMS_RG[1].findViewById(RG_id).getTag().toString():"";
				ADD_com2_Other_v=ADD_com2_Other.getText().toString().trim();
				

				
				String Sql_insert="",Sql_update="",case_conTD=" CASE RC_Condition_type",case_conTV=" CASE RC_Condition_type",case_con_id="";
				int max_c=1;
				 c=cf.SelectTablefunction(cf.General_roof_view, " Where RC_SRID='"+cf.selectedhomeid+"' and  RC_insp_id='"+cf.encode(Roof_page_value[5])+"'  and RC_Condition_type_order='11' and RC_Condition_type='"+cf.encode("What if any, updates were completed?")+"' ");
				if(c.getCount()>0)
				{
					
					
					case_conTD+=" WHEN '"+cf.encode(cf.getResourcesvalue(R.string.ROOF_Survey5))+"' THEN ''";
					case_conTV+=" WHEN '"+cf.encode(cf.getResourcesvalue(R.string.ROOF_Survey5))+"' THEN '"+cf.encode(ADD_com2_RG_v)+"'";
					case_con_id+="'"+cf.encode(cf.getResourcesvalue(R.string.ROOF_Survey5))+"',";
					
					case_conTD+=" WHEN '"+cf.encode(cf.getResourcesvalue(R.string.COMER2))+"' THEN '"+cf.encode(ADD_com2_PDU_RG_o)+"'";
					case_conTV+=" WHEN '"+cf.encode(cf.getResourcesvalue(R.string.COMER2))+"' THEN '"+cf.encode(ADD_com2_PDU_RG_v)+"'";
					case_con_id+="'"+cf.encode(cf.getResourcesvalue(R.string.COMER2))+"',";
					
					case_conTD+=" WHEN '"+cf.encode(cf.getResourcesvalue(R.string.COMER3))+"' THEN '"+cf.encode(ADD_com2_RMS_o)+"'";
					case_conTV+=" WHEN '"+cf.encode(cf.getResourcesvalue(R.string.COMER3))+"' THEN '"+cf.encode(ADD_com2_RMS_v)+"'";
					case_con_id+="'"+cf.encode(cf.getResourcesvalue(R.string.COMER3))+"',";
					
					case_conTD+=" WHEN '"+cf.encode(cf.getResourcesvalue(R.string.COMER4))+"' THEN ''";
					case_conTV+=" WHEN '"+cf.encode(cf.getResourcesvalue(R.string.COMER4))+"' THEN '"+cf.encode(ADD_com2_RME_RG_v)+"'";
					case_con_id+="'"+cf.encode(cf.getResourcesvalue(R.string.COMER4))+"',";
					
					case_conTD+=" WHEN '"+cf.encode(cf.getResourcesvalue(R.string.EquipmentL))+"' THEN ''";
					case_conTV+=" WHEN '"+cf.encode(cf.getResourcesvalue(R.string.EquipmentL))+"' THEN '"+cf.encode(ADD_RMS_RG_v1)+"'";
					case_con_id+="'"+cf.encode(cf.getResourcesvalue(R.string.EquipmentL))+"',";
					
					case_conTD+=" WHEN '"+cf.encode(cf.getResourcesvalue(R.string.DamageR))+"' THEN ''";
					case_conTV+=" WHEN '"+cf.encode(cf.getResourcesvalue(R.string.DamageR))+"' THEN '"+cf.encode(ADD_RMS_RG_v2)+"'";
					case_con_id+="'"+cf.encode(cf.getResourcesvalue(R.string.DamageR))+"',";
					
					case_conTD+=" WHEN '"+cf.encode(cf.getResourcesvalue(R.string.COMER5))+"' THEN ''";
					case_conTV+=" WHEN '"+cf.encode(cf.getResourcesvalue(R.string.COMER5))+"' THEN '"+cf.encode(ADD_com2_Other_v)+"'";
					case_con_id+="'"+cf.encode(cf.getResourcesvalue(R.string.COMER5))+"'";
					
					//case_con_id=case_con_id.substring(0,case_con_id.length()-1);
					
					Sql_update=" UPDATE "+ cf.General_Roof_information+" SET RC_Condition_type_DESC= "+case_conTD+" END ,RC_Condition_type_val= "+case_conTV+" End,RC_Enable='2'  " +
							 " Where RC_Condition_type IN ("+case_con_id+") and RC_masterid=(SELECT RM_masterid  FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' )  and RC_Condition_type_order='11' ";
					System.out.println("the update query="+Sql_update);
					cf.arr_db.execSQL(Sql_update);
					
				
				
					   
				}
				else
				{
					
					
						Sql_insert="INSERT INTO "+cf.General_Roof_information +" SELECT (Select (case when ((SELECT MAX(RC_Id) FROM "+cf.General_Roof_information+") is NULL ) then '0' else (SELECT MAX(RC_Id) FROM "+cf.General_Roof_information+") end))+"+max_c+" as RC_Id,(SELECT RM_masterid as RC_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' ),'"+cf.encode(cf.getResourcesvalue(R.string.ROOF_Survey5))+"' as RC_Condition_type,'' as RC_Condition_type_DESC,'"+cf.encode(ADD_com2_RG_v)+"' as RC_Condition_type_val,'11' as RC_Condition_type_order,'2' as RC_Enable";max_c++;
						Sql_insert+=" UNION SELECT (Select (case when ((SELECT MAX(RC_Id) FROM "+cf.General_Roof_information+") is NULL ) then '0' else (SELECT MAX(RC_Id) FROM "+cf.General_Roof_information+") end))+"+max_c+",(SELECT RM_masterid as RC_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' ) ,'"+cf.encode(cf.getResourcesvalue(R.string.COMER2))+"','"+cf.encode(ADD_com2_PDU_RG_o)+"','"+cf.encode(ADD_com2_PDU_RG_v)+"','11' as RC_Condition_type_order,'2'";max_c++;
						Sql_insert+=" UNION SELECT (Select (case when ((SELECT MAX(RC_Id) FROM "+cf.General_Roof_information+") is NULL ) then '0' else (SELECT MAX(RC_Id) FROM "+cf.General_Roof_information+") end))+"+max_c+",(SELECT RM_masterid as RC_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' ) ,'"+cf.encode(cf.getResourcesvalue(R.string.COMER3))+"','"+cf.encode(ADD_com2_RMS_o)+"','"+cf.encode(ADD_com2_RMS_v)+"','11' as RC_Condition_type_order,'2'";max_c++;
						Sql_insert+=" UNION SELECT (Select (case when ((SELECT MAX(RC_Id) FROM "+cf.General_Roof_information+") is NULL ) then '0' else (SELECT MAX(RC_Id) FROM "+cf.General_Roof_information+") end))+"+max_c+",(SELECT RM_masterid as RC_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' ) ,'"+cf.encode(cf.getResourcesvalue(R.string.COMER4))+"','','"+cf.encode(ADD_com2_RME_RG_v)+"','11' as RC_Condition_type_order,'2'";max_c++;
						Sql_insert+=" UNION SELECT (Select (case when ((SELECT MAX(RC_Id) FROM "+cf.General_Roof_information+") is NULL ) then '0' else (SELECT MAX(RC_Id) FROM "+cf.General_Roof_information+") end))+"+max_c+",(SELECT RM_masterid as RC_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' ) ,'"+cf.encode(cf.getResourcesvalue(R.string.EquipmentL))+"','','"+cf.encode(ADD_RMS_RG_v1)+"','11' as RC_Condition_type_order,'2'";max_c++;
						Sql_insert+=" UNION SELECT (Select (case when ((SELECT MAX(RC_Id) FROM "+cf.General_Roof_information+") is NULL ) then '0' else (SELECT MAX(RC_Id) FROM "+cf.General_Roof_information+") end))+"+max_c+",(SELECT RM_masterid as RC_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' ) ,'"+cf.encode(cf.getResourcesvalue(R.string.DamageR))+"','','"+cf.encode(ADD_RMS_RG_v2)+"','11' as RC_Condition_type_order,'2'";max_c++;
						Sql_insert+=" UNION SELECT (Select (case when ((SELECT MAX(RC_Id) FROM "+cf.General_Roof_information+") is NULL ) then '0' else (SELECT MAX(RC_Id) FROM "+cf.General_Roof_information+") end))+"+max_c+",(SELECT RM_masterid as RC_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' ) ,'"+cf.encode(cf.getResourcesvalue(R.string.COMER5))+"','','"+cf.encode(ADD_com2_Other_v)+"','11' as RC_Condition_type_order,'2'";max_c++;
						//System.out.println("the query"+Sql_insert); 
					cf.arr_db.execSQL(Sql_insert);
					
				
				}
		 	}
			if(Roof_page_value[5].equals("1") || Roof_page_value[5].equals("2") || Roof_page_value[5].equals("4"))
		 	{
				int RG_id=ADD_RG1.getCheckedRadioButtonId();
				String s=(RG_id!=-1)?ADD_RG1.findViewById(RG_id).getTag().toString():"";
				String other="";
				
				c=cf.SelectTablefunction(cf.General_roof_view, " Where RC_SRID='"+cf.selectedhomeid+"' and  RC_insp_id='"+cf.encode(Roof_page_value[5])+"'  and RC_Condition_type_order='11' and RC_Condition_type='"+cf.encode(cf.getResourcesvalue(R.string.roof_add_4poin_txt))+"' ");
				if(c.getCount()>0)
				{
					cf.arr_db.execSQL("UPDATE  "+cf.General_Roof_information +" SET RC_Condition_type_DESC='"+cf.encode(other)+"',RC_Condition_type_val='"+cf.encode(s)+"',RC_Enable='2' WHERE RC_masterid=(SELECT RM_masterid as RC_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' )  and RC_Condition_type_order='11' and  RC_Condition_type='"+cf.encode(cf.getResourcesvalue(R.string.roof_add_4poin_txt))+"'");
					cf.arr_db.execSQL("UPDATE  "+cf.General_Roof_information +" SET RC_Condition_type_val='false',RC_Enable='2' WHERE RC_masterid=(SELECT RM_masterid as RC_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' ) and RC_Condition_type_order='12' and  RC_Condition_type='"+cf.encode("ADD_notapplicable")+"'");
				}
				else
				{
					cf.arr_db.execSQL("INSERT INTO "+cf.General_Roof_information +" (RC_masterid,RC_Condition_type,RC_Condition_type_DESC,RC_Condition_type_val,RC_Condition_type_order,RC_Enable) VALUES ((SELECT RM_masterid as RC_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' ),'"+cf.encode(cf.getResourcesvalue(R.string.roof_add_4poin_txt))+"','"+cf.encode(other)+"','"+cf.encode(s)+"','11','2')");
					
				}
				
			}
			c=cf.SelectTablefunction(cf.General_roof_view, " Where RC_SRID='"+cf.selectedhomeid+"' and  RC_insp_id='"+cf.encode(Roof_page_value[5])+"' and RC_Condition_type_order='12' and  RC_Condition_type='"+cf.encode("ADD_notapplicable")+"' ");
			System.out.println("the quesry= Where RC_SRID='"+cf.selectedhomeid+"' and  RC_insp_id='"+cf.encode(Roof_page_value[5])+"' and RC_Condition_type_order='12' and  RC_Condition_type='"+cf.encode("ADD_notapplicable")+"'");
			if(c.getCount()<=0)
			{
				cf.arr_db.execSQL("INSERT INTO "+cf.General_Roof_information +" (RC_masterid,RC_Condition_type,RC_Condition_type_DESC,RC_Condition_type_val,RC_Condition_type_order,RC_Enable) VALUES ((SELECT RM_masterid as RC_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' ),'"+cf.encode("ADD_notapplicable")+"','','false','12','2')");
			}
			else
			{
				cf.arr_db.execSQL("UPDATE  "+cf.General_Roof_information +" SET RC_Condition_type_val='false' WHERE RC_masterid=(SELECT RM_masterid  FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1'  ) and RC_Condition_type_order='12' and  RC_Condition_type='"+cf.encode("ADD_notapplicable")+"'");
			}
			check_alert_add="true";
			
			
	 	}catch (Exception e) {
			// TODO: handle exception
			System.out.println(" other issues "+e.getMessage());
			return false;
		}
		return true;
	}
	private void insertRCValues() {
		// TODO Auto-generated method stub
		
	 	//cf.arr_db.execSQL("Delete from "+cf.General_Roof_information+" where RC_Enable='0'");
	
		
		Cursor c22=cf.SelectTablefunction(cf.Roof_master, " Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(Roof_page_value[5])+"' and RM_module_id='1' ");
		if(c22.getCount()<=0)
		{
			cf.arr_db.execSQL("INSERT INTO "+cf.Roof_master+" (RM_inspectorid,RM_srid,RM_insp_id,RM_module_id) Values ('"+cf.Insp_id+"','"+cf.selectedhomeid+"','"+cf.encode(Roof_page_value[5])+"','1')");
		}
			if(RCN_chk_top.isChecked())
			{
				cf.arr_db.execSQL("Delete From "+cf.General_Roof_information+" WHERE RC_masterid=(SELECT RM_masterid FROM "+cf.Roof_master+" Where RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(Roof_page_value[5])+"' and RM_module_id='1' )  and (RC_Condition_type_order='' or (RC_Condition_type_order='12' and RC_Condition_type='"+cf.encode("RCN_notapplicable")+"')) ");
				Cursor c=cf.SelectTablefunction(cf.General_roof_view, " Where RC_SRID='"+cf.selectedhomeid+"' and  RC_insp_id='"+cf.encode(Roof_page_value[5])+"' and RC_Condition_type_order='12' and  RC_Condition_type='"+cf.encode("RCN_notapplicable")+"' ");
				
				if(c.getCount()==0)
				{
					cf.arr_db.execSQL("INSERT INTO "+cf.General_Roof_information +" (RC_masterid,RC_Condition_type,RC_Condition_type_DESC,RC_Condition_type_val,RC_Condition_type_order,RC_Enable) VALUES ((SELECT RM_masterid as RC_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' ),'"+cf.encode("RCN_notapplicable")+"','','true','12','2')");
				}
				else
				{
								
					cf.arr_db.execSQL("UPDATE  "+cf.General_Roof_information +" SET RC_Condition_type_val='true',RC_Enable='2' WHERE RC_masterid=(SELECT  RC_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' ) and RC_Condition_type_order='12' and  RC_Condition_type='"+cf.encode("RCN_notapplicable")+"'");
					
				}
				((TextView) findViewById(R.id.RCN_comp)).setVisibility(View.VISIBLE);	
					
			}
			
			
			
			/**Inserting OR updating the static loaded contents***/
			if(ADD_chk_top.isChecked() && !Roof_page_value[5].equals("7"))
			{
				cf.arr_db.execSQL("Delete From "+cf.General_Roof_information+" WHERE RC_masterid=(SELECT RM_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(Roof_page_value[5])+"' and RM_module_id='1' )" +
						" and (RC_Condition_type_order='11' or (RC_Condition_type_order='12' and RC_Condition_type='"+cf.encode("ADD_notapplicable")+"'))  and RC_Condition_type<>'"+cf.encode(cf.getResourcesvalue(R.string.Comments))+"' and RC_Condition_type<>'"+cf.encode(cf.getResourcesvalue(R.string.roof_add_4poin_txt2))+"' ");
				Cursor c=cf.SelectTablefunction(cf.General_roof_view, " Where RC_SRID='"+cf.selectedhomeid+"' and  RC_insp_id='"+cf.encode(Roof_page_value[5])+"' and RC_Condition_type_order='12' and  RC_Condition_type='"+cf.encode("ADD_notapplicable")+"' ");
				if(c.getCount()==0)
				{
					cf.arr_db.execSQL("INSERT INTO "+cf.General_Roof_information +" (RC_masterid,RC_Condition_type,RC_Condition_type_DESC,RC_Condition_type_val,RC_Condition_type_order,RC_Enable) VALUES ((SELECT RM_masterid as RC_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' ),'"+cf.encode("ADD_notapplicable")+"','','true','12','2')");
				}
				else
				{
					cf.arr_db.execSQL("UPDATE  "+cf.General_Roof_information +" SET RC_Condition_type_val='true',RC_Enable='2' WHERE RC_masterid=(SELECT RM_masterid  FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' ) and RC_Condition_type_order='12' and  RC_Condition_type='"+cf.encode("ADD_notapplicable")+"'");
				}
				((TextView) findViewById(R.id.ADD_comp)).setVisibility(View.VISIBLE);
			}	
			
				//cf.arr_db.execSQL("DELETE FROM "+cf.General_Roof_information+" WHERE RC_SRID='"+cf.selectedhomeid+"' and  RC_page='"+cf.encode(Roof_page_value[1])+"' and RC_Condition_type_order='11' and  RC_Condition_type<>'"+cf.encode("Comments")+"'");
				if(Roof_page_value[5].equals("1") || Roof_page_value[5].equals("2")|| Roof_page_value[5].equals("4"))
			 	{
									
					String sp3_val=cf.getselected_chk(RC_RSurvay_chk).trim();
					Cursor c=cf.SelectTablefunction(cf.General_roof_view, " Where RC_SRID='"+cf.selectedhomeid+"' and  RC_insp_id='"+cf.encode(Roof_page_value[5])+"'  and RC_Condition_type_order='11' and RC_Condition_type='"+cf.encode(cf.getResourcesvalue(R.string.roof_add_4poin_txt2))+"' ");
					if(c.getCount()>0)
					{	cf.arr_db.execSQL("UPDATE  "+cf.General_Roof_information +" SET RC_Condition_type_val='"+cf.encode(sp3_val)+"',RC_Enable='2' WHERE RC_masterid=(SELECT RM_masterid as RC_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' ) and RC_Condition_type_order='11' and  RC_Condition_type='"+cf.encode(cf.getResourcesvalue(R.string.roof_add_4poin_txt2))+"' ");
						
					}
					else
					{
						cf.arr_db.execSQL("INSERT INTO "+cf.General_Roof_information +" (RC_masterid,RC_Condition_type,RC_Condition_type_DESC,RC_Condition_type_val,RC_Condition_type_order) VALUES ((SELECT RM_masterid as RC_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' ),'"+cf.encode(cf.getResourcesvalue(R.string.roof_add_4poin_txt2))+"','','"+cf.encode(sp3_val)+"','11')");
					}
			 	}
			/***inserting include citizen certificate form value***/
				if(Roof_page_value[5].equals("2"))
			 	{
									
					
					Cursor c=cf.SelectTablefunction(cf.General_roof_view, " Where RC_SRID='"+cf.selectedhomeid+"' and  RC_insp_id='"+cf.encode(Roof_page_value[5])+"'  and RC_Condition_type_order='12' and RC_Condition_type='"+cf.encode("ICCF")+"' and RC_Condition_type_order='12' ");
							
					if(c.getCount()>0)
					{	cf.arr_db.execSQL("UPDATE  "+cf.General_Roof_information +" SET RC_Condition_type_val='"+citi_inclu.isChecked()+"',RC_Enable='2' WHERE RC_masterid=(SELECT RM_masterid as RC_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' ) and RC_Condition_type_order='11' and  RC_Condition_type='"+cf.encode("ICCF")+"' ");
					
						
					}
					else
					{
						cf.arr_db.execSQL("INSERT INTO "+cf.General_Roof_information +" (RC_masterid,RC_Condition_type,RC_Condition_type_DESC,RC_Condition_type_val,RC_Condition_type_order) VALUES ((SELECT RM_masterid as RC_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' ),'"+cf.encode("ICCF")+"','','"+citi_inclu.isChecked()+"','12')");
					}
			 	}
		   /***inserting include citizen certificate form value ends***/
				
		String cmt=cf.encode(RC_cmt.getText().toString().trim());
			try
			{
				Cursor c=cf.SelectTablefunction(cf.General_roof_view, " Where RC_SRID='"+cf.selectedhomeid+"' and  RC_insp_id='"+cf.encode(Roof_page_value[5])+"'  and RC_Condition_type_order='11' and RC_Condition_type='Comments' ");
				if(c.getCount()>0)
				{
					
					cf.arr_db.execSQL("UPDATE  "+cf.General_Roof_information +" SET RC_Condition_type_DESC='"+cmt+"',RC_Condition_type_val='',RC_Enable='2' WHERE RC_masterid=(SELECT RM_masterid  FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' ) and RC_Condition_type_order='11' and  RC_Condition_type='Comments' ");
					//cf.arr_db.execSQL("UPDATE  "+cf.General_Roof_information +" SET RC_Condition_type_DESC='"+cf.datewithtime+"',RC_Condition_type_val='',RC_Enable='1' WHERE RC_SRID='"+cf.selectedhomeid+"' and  RC_insp_id='"+cf.encode(Roof_page_value[5])+"' and RC_Condition_type_order='12' and  RC_Condition_type='updatedDate' ");
				}
				else
				{
					
					cf.arr_db.execSQL("INSERT INTO "+cf.General_Roof_information +" (RC_masterid,RC_Condition_type,RC_Condition_type_DESC,RC_Condition_type_order,RC_Enable) VALUES ((SELECT RM_masterid as RC_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' ),'Comments','"+cmt+"','11','2')");
					//cf.arr_db.execSQL("INSERT INTO "+cf.General_Roof_information +" (RC_masterid,RC_Condition_type,RC_Condition_type_DESC,RC_Condition_type_order) VALUES ('"+cf.selectedhomeid+"','"+cf.encode(Roof_page_value[1])+"','updatedDate','"+cf.datewithtime+"','12','1','"+Roof_page_value[5]+"')");
				}
				c=cf.SelectTablefunction(cf.General_roof_view, " Where RC_SRID='"+cf.selectedhomeid+"' and  RC_page='"+cf.encode(Roof_page_value[1])+"'  and RC_Condition_type_order='12' and RC_Condition_type='updatedDate' ");
				if(c.getCount()>0)
				{
					cf.arr_db.execSQL("UPDATE  "+cf.General_Roof_information +" SET RC_Condition_type_DESC='"+cf.datewithtime+"',RC_Condition_type_val='' WHERE RC_masterid=(SELECT RM_masterid  FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' ) and RC_Condition_type_order='12' and  RC_Condition_type='updatedDate' ");
				}
				else
				{
					cf.arr_db.execSQL("INSERT INTO "+cf.General_Roof_information +" (RC_masterid,RC_Condition_type,RC_Condition_type_DESC,RC_Condition_type_order) VALUES ((SELECT RM_masterid as RC_masterid FROM "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(this.Roof_page_value[5])+"' and RM_module_id='1' ),'updatedDate','"+cf.datewithtime+"','12')");
				}
				
				
				/*cf.arr_db.execSQL("UPDATE  "+cf.General_Roof_information +" SET RC_Enable='1' WHERE RC_SRID='"+cf.selectedhomeid+"' and  RC_insp_id='"+cf.encode(Roof_page_value[5])+"' and RC_Enable='0'");
				cf.arr_db.execSQL("UPDATE  "+cf.Roof_cover_type +" SET RCT_Enable='1' WHERE RCT_srid='"+cf.selectedhomeid+"' and  RCT_insp_id='"+cf.encode(Roof_page_value[5])+"' and RCT_Enable='0'");*/
			}
			catch (Exception e) {
				// TODO: handle exception
				System.out.println("isseus in the save "+e.getMessage());
			}
			/**Inserting OR updating the static loaded contents ends***/
			//nextlayout();
			
			checkforsurveyaleret();
			
		}
	


	private void checkforsurveyaleret() {
		// TODO Auto-generated method stub
		if(Roof.equals(cf.Roof_insp[1]) && citi_inclu.isChecked())
		//if(Roof.equals(cf.Roof_insp[1]))
		{
			/****Alert to get the confirmation from the user for generate the citizen form**/
			AlertDialog.Builder alt_bld = new AlertDialog.Builder(this);
			AlertDialog alert = null;
			alt_bld.setMessage("Roof Information saved successfully .\n  want to generate Citizen Certification Form ?")
			.setCancelable(false)
			.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			

			public void onClick(DialogInterface dialog, int id) {
			// Action for 'Yes' Button
				try
				{
				
					ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
					NetworkInfo info = conMgr.getActiveNetworkInfo();
					if(cf.isInternetOn() && info!=null)
					{
						
							dialog.cancel();
							String source = "<b><font color=#00FF33>Generating Citizen Certification Form. Please wait..."
									+ "</font></b>";
								pd = ProgressDialog.show(RoofSection.this, "", Html.fromHtml(source), true);
								Thread td =new Thread(RoofSection.this);
								td.start();
									//res=generate_form();
								
					
					}
					else
					{
						cf.ShowToast("There is a problem on your network. Please try again!", 0);
					}
				//	movetonext();
					//dialog.cancel();
				}catch (Exception e) {
					// TODO: handle exception
					System.out.println("The problem in the inserting the selected inspection"+e.getMessage());
				}
			}
			})
			.setNegativeButton("No", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
			//  Action for 'NO' Button
				//((Dialog) dialog).setCancelable(true);
				movetonext();
			dialog.cancel();
			}
			});
		
			// Title for AlertDialog
			alert = alt_bld.create();
			alert.setTitle("Citizen Certificate");
			// Icon for AlertDialog
			alert.setIcon(R.drawable.alertmsg);
			alert.show();
			alert.setCancelable(false);
			/****Alert to get the confirmation from the user for generate the citizen form Ends**/
		}
		else
		{
			cf.ShowToast("Roof Information saved successfully ", 1);
			movetonext();
		}	
	}
  

	protected boolean generate_form()throws SocketException, NetworkErrorException, TimeoutException, IOException, XmlPullParserException {
		// TODO Auto-generated method stub
		
		cf.request=cf.SoapRequest("GENERATECITIZENFORMPDF");
		cf.request.addProperty("SRID", cf.selectedhomeid);
		cf.request.addProperty("Inspectionid", Roof_page_value[5]);
		cf.request.addProperty("InspectorID", cf.Insp_id);
		
		String sql=" Select ph.ARR_PH_FirstName||' '||ph.ARR_PH_LastName as ph_Name, ph.ARR_PH_Address1||' '||ph.ARR_PH_Address2 as ph_Address,ph.ARR_PH_Policyno,ph.ARR_Schedule_ScheduledDate From  "+cf.policyholder+" as ph WHERE ARR_PH_SRID='"+cf.selectedhomeid+"'";
		Cursor cs=cf.arr_db.rawQuery(sql, null);
		System.out.println(sql);
		cs.moveToFirst();
		if(cs.getCount()>0)
		{
			cf.request.addProperty("Insuredname", cf.decode(cs.getString(cs.getColumnIndex("ph_Name"))));
			cf.request.addProperty("Policyno", cf.decode(cs.getString(cs.getColumnIndex("ARR_PH_Policyno"))));
			cf.request.addProperty("Inspectedaddress", cf.decode(cs.getString(cs.getColumnIndex("ph_Address"))));
			cf.request.addProperty("Dateofinspection", cf.decode(cs.getString(cs.getColumnIndex("ARR_Schedule_ScheduledDate"))));
			
		
		}
		/***Roof covering type value **/
		sql=" Select GRI.*,M.mst_text from "+cf.Roof_cover_type+" GRI LEFT JOIN "+cf.master_tbl+" M on GRI.RCT_TypeValue=m.mst_custom_id and M.mst_module_id='2' WHERE RCT_masterid=(SELECT RCT_masterid from "+cf.Roof_cover_type+" Where RCT_TypeValue='11' and RCT_masterid in " +
				"(SELECT RM_masterid from "+cf.Roof_master+" where RM_insp_id='2' and RM_srid='"+cf.selectedhomeid+"' and RM_module_id='0') and RCT_FieldVal='true' ) "; 
		System.out.println("the sql1"+sql);
		cs=cf.arr_db.rawQuery(sql, null);
		System.out.println(sql);
		cs.moveToFirst();
		String roof_cover="",appr="",roof_age="",date="";
		if(cs.getCount()>0)
		{
			do
			{
				
				
				if(cs.getString(cs.getColumnIndex("RCT_TypeValue")).equals("1"))
				{
					roof_cover=cf.decode(cs.getString(cs.getColumnIndex("RCT_FieldVal")));
					
				}
				if(cs.getString(cs.getColumnIndex("RCT_TypeValue")).equals("2"))
				{
					date=cf.decode(cs.getString(cs.getColumnIndex("RCT_FieldVal")));
					
					
				}
				if(cs.getString(cs.getColumnIndex("RCT_TypeValue")).equals("3"))
				{
					roof_age=cf.decode(cs.getString(cs.getColumnIndex("RCT_FieldVal")));
					
				}
				if(cs.getString(cs.getColumnIndex("RCT_TypeValue")).equals("10"))
				{
					appr=cf.decode(cs.getString(cs.getColumnIndex("RCT_FieldVal")));
					
				}
			}
			while(cs.moveToNext());
			cf.request.addProperty("Roofcovering", roof_cover);
			cf.request.addProperty("datelastupdate", date);
			cf.request.addProperty("Ageofroof", roof_age);
			cf.request.addProperty("remainingroof", appr);
		}
		
		/***Roof covering type value  **/
		/**Roof condition noted value **/
		sql=" Select GRI.*,M.mst_text from "+cf.General_Roof_information+" GRI LEFT JOIN "+cf.master_tbl+" M on GRI.RC_Condition_type=m.mst_custom_id and M.mst_module_id='2' WHERE RC_masterid=" +
				"(SELECT RM_masterid from "+cf.Roof_master+" where RM_insp_id='2' and RM_srid='"+cf.selectedhomeid+"' and RM_module_id='1') and (RC_Condition_type='16' or RC_Condition_type='18')  "; 
		System.out.println("the sql1"+sql);
		cs=cf.arr_db.rawQuery(sql, null);
		cs.moveToFirst();
		if(cs.getCount()>0)
		{
			
			do
			{
				
				String val=cf.decode(cs.getString(cs.getColumnIndex("RC_Condition_type_val"))),
						desc=cf.decode(cs.getString(cs.getColumnIndex("RC_Condition_type_DESC"))),
						tit=cf.decode(cs.getString(cs.getColumnIndex("RC_Condition_type")));
				if(tit.trim().equals("18"))
				{
					
					
					cf.request.addProperty("Singofdamaged", val);
					cf.request.addProperty("Singdamagedvalue", desc);
					
					
					
					
				}
				if(tit.trim().equals("16"))
				{
					cf.request.addProperty("Singofleaks", val);
					cf.request.addProperty("Singofleaksvalue", desc);
					
				}
			}while(cs.moveToNext());
		}
		String timeStamp = new SimpleDateFormat("MM/dd/yyyy").format(Calendar.getInstance().getTime());
		cf.request.addProperty("Date", timeStamp);
		System.out.println("the request "+cf.request);
		Boolean result=cf.SoapResponse("GENERATECITIZENFORMPDF", cf.request);
		
		/**Roof condition noted value ends **/
		return result;
	}
	class Customized_clicker implements OnClickListener
	{
		int tbl,id;
		Customized_clicker(int tbl,int id)
		{
			this.tbl=tbl;
			this.id=id;
		}
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			
			if(v.getTag().toString().equals("Yes"))
			{
				((RadioButton)findViewById(id).findViewWithTag("No")).setChecked(false);
				findViewById(tbl).setVisibility(View.VISIBLE);
			}
			else
			{
				((RadioButton)findViewById(id).findViewWithTag("Yes")).setChecked(false);
				findViewById(tbl).setVisibility(View.GONE);
			}
		}
			
	}
   
	class RCN_clicker implements OnClickListener
    {
    	
    	int type;
    	int id;
    	RCN_clicker(int type,int id)
    	{
    		
    		this.type=type;
    		this.id=id;
    		
    	}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			
			
			if(type==2)
			{
								
				AlertDialog.Builder builder = new AlertDialog.Builder(RoofSection.this);
				builder.setMessage("Do you want to delete this Roof Conditions Noted?")
						.setCancelable(false)
						.setTitle("Confirmation")
						.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog,
											int id1) {
										
										int[] temp=Delete_from_array(tbl_id, id);
										tbl_id=null;
										tbl_id=temp;
										try
										{
										cf.arr_db.execSQL(" DELETE FROM "+cf.General_Roof_information+" WHERE RC_Id='"+(id-1000)+"'");
										}catch (Exception e) {
											// TODO: handle exception
											System.out.println("no more isuses ");
										}
									}
								})
						.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {

										dialog.cancel();
									}
								});
				builder.setIcon(R.drawable.alertmsg);
				
				builder.show();
				
				
				
				//show_GRI_Value();
				
				
			}
			else if(type==1)
			{
				TableRow tb=(TableRow) findViewById(this.id);
				((TextView) tb.findViewWithTag("Title_txt")).setVisibility(View.GONE);
				((EditText) tb.findViewWithTag("Title_ed")).setVisibility(View.VISIBLE);
				((ImageView) v).setVisibility(View.INVISIBLE);
				
			}
		}
    	
    }
	class GCH_check_listener implements OnCheckedChangeListener
	{
    	EditText ed;
    	LinearLayout li;
    	GCH_check_listener(EditText ed,LinearLayout li)
    	{
    		this.ed=ed;
    		this.li=li;
    	}
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			if(((RadioButton) findViewById(group.getCheckedRadioButtonId())).getText().toString().trim().equals("Yes"))
			{
				li.setVisibility(View.VISIBLE);
			}
			else
			{
				
				ed.setText("");
				
				li.setVisibility(View.GONE);
			}
		}
	}

	class GCH_RC_watcher implements TextWatcher
    {
    	String size;
    	LinearLayout par,chi;
    	TextView tv;
    	
    	GCH_RC_watcher(TextView tv,String s)
    	{
    		this.size=s;
    		/*this.par=par;
    		this.chi=chi*/;
    		this.tv=tv;
    	}

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			int k=150;
			try
			{
				k=Integer.parseInt(size);
			}
			catch (Exception e) {
				// TODO: handle exception
			}
			
			cf.showing_limit(s.toString(),tv,k);
			
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			
		}
    	
    }
   
	class RC_editclick implements OnTouchListener
	{
		
	@Override
		public boolean onTouch(View v, MotionEvent event) {
			// TODO Auto-generated method stub
			((EditText) v).setFocusableInTouchMode(true);
			((EditText) v).requestFocus();
			 ((EditText) v).setCursorVisible(true);
			return false;
		}
		
	}
	class RC_clicker implements OnClickListener
	{
		
		

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
		
			switch (v.getId()) {
		
			case R.id.GCH_RC_RCT_SA:
				Save_RCTD();
			break;
			case R.id.GCH_RC_RCT_can:
				clear_RCT(1);
			break;
			case R.id.RC_AMC:
				Add_more_condtion();
			break;
			
			default:
				break;
			}
			
		
		}

		
	
	}
	
	private TableRow[] dynamicarraysetting(TableRow[] pieces3) {
		// TODO Auto-generated method stub
		 try
		 {
		if(pieces3==null)
		{
			pieces3=new TableRow[1];
			
			//pieces3[0]=ErrorMsg;
		}
		else
		{
			
			TableRow tmp[]=new TableRow[pieces3.length+1];
			int i;
			for(i =0;i<pieces3.length;i++)
			{
				
				tmp[i]=pieces3[i];
				
			}
		
			//tmp[tmp.length-1]=ErrorMsg;
			pieces3=null;
			pieces3=tmp.clone();
		}
		 }
		 catch(Exception e)
		 {
			 System.out.println("Exception "+e.getMessage());
			
		 }
		return pieces3;
	}
	private String[] dynamicarraysetting(String[] pieces3) {
		// TODO Auto-generated method stub
		 try
		 {
		if(pieces3==null)
		{
			pieces3=new String[1];
			
			//pieces3[0]=ErrorMsg;
		}
		else
		{
			
			String tmp[]=new String[pieces3.length+1];
			int i;
			for(i =0;i<pieces3.length;i++)
			{
				
				tmp[i]=pieces3[i];
				
			}
		
			//tmp[tmp.length-1]=ErrorMsg;
			pieces3=null;
			pieces3=tmp.clone();
		}
		 }
		 catch(Exception e)
		 {
			 System.out.println("Exception "+e.getMessage());
			
		 }
		return pieces3;
	}
	private int[] dynamicarraysetting(int[] pieces3) {
		// TODO Auto-generated method stub
		 try
		 {
		if(pieces3==null)
		{
			pieces3=new int[1];
			
			//pieces3[0]=ErrorMsg;
		}
		else
		{
			
			int tmp[]=new int[pieces3.length+1];
			int i;
			for(i =0;i<pieces3.length;i++)
			{
				
				tmp[i]=pieces3[i];
				
			}
		
			//tmp[tmp.length-1]=ErrorMsg;
			pieces3=null;
			pieces3=tmp.clone();
		}
		 }
		 catch(Exception e)
		 {
			 System.out.println("Exception "+e.getMessage());
			
		 }
		return pieces3;
	}
	protected int[] Delete_from_array(int[] id,int d_id) {
	
		if(id.length>1)
		{
			
			int[] temp=new int[id.length-1];
			String[] temp1=new String[id.length-1];
			
			TableRow tbl=(TableRow) findViewById(d_id);
			
			for(int i=0,j=0;i<id.length;i++)
			{
					
				if(id[i]!=d_id)
				{
					temp[j]=id[i];
					
					j++;
				}
				else
				{
					RCN_ShowValue.removeView(tbl);
					
				}
			}
			
			def_tit=null;
			def_tit=temp1;
			return temp;
			
		}
		else
		{
			
			return null;
		}
		
		
		
	}
		
	class radiocheck implements OnClickListener
{
		int Rg_id;
		String[] option;
		String OtherOption="";
		radiocheck(int Rg_id,String[] option,String OtherOption)
			{
				this.Rg_id=Rg_id;
				this.option=option;
				this.OtherOption=OtherOption;
				
						
			}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		try
		{
			if(!OtherOption.trim().equals(""))
			{
				if(v.getTag().equals(OtherOption))
				{
					((LinearLayout)	findViewById(Rg_id).findViewWithTag("Other_DESC_li")).setVisibility(View.VISIBLE);
				}
				else
				{
					((LinearLayout)	findViewById(Rg_id).findViewWithTag("Other_DESC_li")).setVisibility(View.GONE);
					((EditText) findViewById(Rg_id).findViewWithTag("Other_DESC")).setText("");
				}
			}
			/**Customized validation for the commercial **/
			
		
			if(amt[0]==Rg_id)
			{
				  try{ 
				  if(v.getTag().toString().trim().equals(getResources().getString(R.string.yes)))
					((TableRow) findViewById(amt[1])).setVisibility(View.VISIBLE);
				   else
				   {
					 ((TableRow) findViewById(amt[1])).setVisibility(View.GONE);
					 ((EditText) findViewById(Rg_id).findViewWithTag("EditText")).setText("");
				   }
				  	
				  }
				  catch (Exception e) {
					// TODO: handle exception
					  System.out.println("issues in there"+e.getMessage());
				}
			}
			if(everReplaced[0]==Rg_id)
			{
				  try{ 
				  if(v.getTag().toString().trim().equals(com_roof_yesspl))
					((TableRow) findViewById(everReplaced[1])).setVisibility(View.VISIBLE);
				   else
				   {
					 ((TableRow) findViewById(everReplaced[1])).setVisibility(View.GONE);
					 
				   }
				  }
				  catch (Exception e) {
					// TODO: handle exception
					  System.out.println("issues in there"+e.getMessage());
				}
			}
			if(hasroof[0]==Rg_id)
			{
				  try{ 
				  if(v.getTag().toString().trim().equals(com_roof_yesspl))
					((TableRow) findViewById(hasroof[1])).setVisibility(View.VISIBLE);
				   else
				   {
					 ((TableRow) findViewById(hasroof[1])).setVisibility(View.GONE);
					 
				   }
				  }
				  catch (Exception e) {
					// TODO: handle exception
					  System.out.println("issues in there"+e.getMessage());
				}
			}
			/**Customized validation for the commercial ends **/
			/**Customised roof autopopulation for over all condtiotn roof **/
			if(over_all_id==Rg_id)
			{
				
				String Default="No";
				
				  try{ 
					  if(v.getTag().toString().trim().equals(over_all_option[0])||v.getTag().toString().trim().equals(over_all_option[1]))
					  {
						  RCN_c.moveToFirst();
						for(int i=0;i<RCN_c.getCount();i++,RCN_c.moveToNext())
						{
							
							String[] opt=RCN_c.getString(RCN_c.getColumnIndex("options")).split(",");
							String option_other=RCN_c.getString(RCN_c.getColumnIndex("option_other"));
							String title=((EditText) findViewById(tbl_id[i]).findViewWithTag("Title_ed")).getText().toString().trim();
							if(tbl_id[i]!=Rg_id)
							{
								if(cf.decode(RCN_c.getString(RCN_c.getColumnIndex("title"))).equals(title))
								{
									for(int j=0;j<opt.length;j++)
									{
										if(opt[j].equals(Default))
										{
											
											((RadioButton)findViewById(tbl_id[i]).findViewWithTag(opt[j])).setChecked(true);
										}
										else
										{
											if(option_other.equals(opt[j]) && ((RadioButton)findViewById(tbl_id[i]).findViewWithTag(opt[j])).isChecked())
											{
												((EditText)findViewById(tbl_id[i]).findViewWithTag("Other_DESC")).setText("");
												((LinearLayout)findViewById(tbl_id[i]).findViewWithTag("Other_DESC_li")).setVisibility(View.GONE);
											}
											((RadioButton)findViewById(tbl_id[i]).findViewWithTag(opt[j])).setChecked(false);
										}
										
									}
									
								}
								
							}
						}
					  }
					   
					  }
					  catch (Exception e) {
						// TODO: handle exception
						  System.out.println("issues in there"+e.getMessage());
					}
			}
			/**Customised roof autopopulation for over all condtiotn roof ends**/
			
			
		}
		catch (Exception e) {
			// TODO: handle exception
		}
			for(int i=0;i<option.length;i++)
			{
				if(!((RadioButton) v).getText().equals(option[i]))
				{
					((RadioButton)	findViewById(Rg_id).findViewWithTag(option[i])).setChecked(false);
				
				}
			}
		
		
	}

}
	class check_lis implements OnClickListener
	{
		
	int Rg_id;
	String[] option;
	String OtherOption="";
	check_lis(int Rg_id,String[] option,String OtherOption)
		{
			this.Rg_id=Rg_id;
			this.option=option;
			this.OtherOption=OtherOption;
			
					
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			try
			{
				if(!OtherOption.trim().equals(""))
				{
					if(v.getTag().equals(OtherOption))
					{
						if(((CheckBox)v).isChecked())
						{
							((LinearLayout)	findViewById(Rg_id).findViewWithTag("Other_DESC_li")).setVisibility(View.VISIBLE);
						
						}
						else
						{
							((LinearLayout)	findViewById(Rg_id).findViewWithTag("Other_DESC_li")).setVisibility(View.GONE);
							((EditText)	findViewById(Rg_id).findViewWithTag("Other_DESC")).setText("");
						}
						
					}
				
				}
				
				
			}
			catch (Exception e) {
				// TODO: handle exception
			}
				
			
			
		}

	}
	
	class selectchange implements OnCheckedChangeListener
	{
		LinearLayout li;
		selectchange(LinearLayout li)
		{
			this.li=li;
		}

		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			if(group.findViewById(checkedId).getTag().equals("Yes"))
			{
				li.setVisibility(View.VISIBLE);
			}
			else
			{
				li.setVisibility(View.GONE);
			}
		}
	
		
	
	}
	class datepick implements OnClickListener
	{
		int id;
		datepick(int id)
		{
			this.id=id;
		}
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			((EditText)findViewById(id).findViewWithTag("Date")).setText("");
			DATE_DIALOG_ID=id;
			cf.showDialogDate(((EditText)findViewById(DATE_DIALOG_ID).findViewWithTag("Date")));
	
		}
	}
	class imagepick implements OnClickListener
	{
		int id;
		int type;
		imagepick(int id,int type)
		{
			this.id=id;
			this.type=type;
		}
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			try
			{
				
				if(type==0)
				{
					
					Intent in=new Intent(RoofSection.this,RoofImagePicking.class);
				    in.putExtra("roof_cover_img", true);
				    String cv="";
				    try
				    {
				    	if(findViewById(RCT_id[0]).findViewWithTag("Spinner_val")!=null)
				    	{
				    		cv=((Spinner)findViewById(RCT_id[0]).findViewWithTag("Spinner_val")).getSelectedItem().toString().trim();
				    	}
				    }
				    catch (Exception e) {
						// TODO: handle exception
					}
				    if(!cv.equals("-Select-"))
				    {
					    in.putExtra("Cover", cv);
					    in.putExtra("Insp_id",Roof_page_value[5]);
					    in.putExtra("SRID",cf.selectedhomeid);
					    in.putExtra("element_id",id);
						startActivityForResult(in, pick_img_code);
				    }
				    else
				    {
				    	cf.ShowToast("Please select Roof Covering Type ", 0);
				    }
				}
				else if(type==1)
				{
					//((EditText)findViewById(id).findViewWithTag("Path")).setText("");
					 cf.ShowToast("Image cleared successfully.", 1);
				}
			}
			catch (Exception e) {
				// TODO: handle exception
			}
			
		}
	}
	
	class Spselect implements OnItemSelectedListener
	{
		int id;
		String other_option;
		Spselect(int id,String other_option)
		{
			this.id=id;
			this.other_option=other_option;
			
			
		}
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1,
				int arg2, long arg3) {
			try
			{
			// TODO Auto-generated method stub
			if(((Spinner) findViewById(id).findViewWithTag("Spinner_val")).getSelectedItem().toString().trim().equals(other_option))
			{
				((LinearLayout)	findViewById(id).findViewWithTag("Other_DESC_li")).setVisibility(View.VISIBLE);
				
			}
			else
			{
				((LinearLayout)	findViewById(id).findViewWithTag("Other_DESC_li")).setVisibility(View.GONE);
				((EditText)	findViewById(id).findViewWithTag("Other_DESC")).setText("");
			}
			
			
			}
			catch (Exception e) {
				// TODO: handle exception
				System.out.println("no more text"+e.getMessage());
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
	}
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.sampleCitizenCertificate:
			Intent in=new Intent(this,CitizenCertification.class);
			cf.putExtraIntent(in);
			startActivityForResult(in, 10);
			break;
		case R.id.GCH_RC_skip:
			
			skipped_insp+=Roof;
		movetonext();
		break;
		case R.id.GCH_RC_cancel:
		
			Roof="Canceled";
			movetonext();
		break;	
		case R.id.GCH_RC_savebtn:
			/**Checking weather the all requirred fields are saved **/
			boolean b=false;
			
			/**validation for the Roof survay*/
			if(Roof_page_value[5].equals("1") ||Roof_page_value[5].equals("2") || Roof_page_value[5].equals("4") )
		 	{
			String s3=cf.getselected_chk(RC_RSurvay_chk).trim();//RC_Survay.getSelectedItem().toString().trim();
			if(s3.equals(""))
			{
				Setfocuserro(RC_RSurvay_chk[0],RC_RSurvay_chk[0], RC_RSurvay_chk[0]);
				cf.ShowToast("Please select the option for "+cf.getResourcesvalue(R.string.roof_add_4poin_txt2)+".",0);
				return;
			}
		 	}
			/**validation for the Roof survay ends*/
			String sql="Select main.*,(SELECT mst_text FROM "+cf.master_tbl+" WHERE mst_custom_id=main.RCT_TypeValue and mst_module_id='2') as title," +
						"noin.RCT_FieldVal as noinfoval from "+cf.Roof_covertype_view+" as main" +
						" LEFT join "+cf.Roof_covertype_view+" as noin on noin.RCT_srid=main.RCT_srid and noin.RCT_insp_id=main.RCT_insp_id and noin.RCT_TypeValue=opt.option_tit and noin.RCT_FielsName=main.RCT_FielsName " +
						"LEFT JOIN  " +cf.option_list+" as opt ON option_tit=( SELECT mst_custom_id From "+cf.master_tbl+" Where mst_text='"+cf.encode(no_perdomin_tit)+"') AND module_id='"+Roof_page_value[0]+"' and insp_id='"+Roof_page_value[5]+"' "+
						" Where main.RCT_srid='"+cf.selectedhomeid+"' and main.RCT_insp_id='"+cf.encode(Roof_page_value[5])+"' and main.RCT_TypeValue<>opt.option_tit";
						//System.out.println("the sql was ="+sql);
				Cursor c3=cf.arr_db.rawQuery(sql, null);

				
				
				if(c3.getCount()>0)
				{ int i=0;
					for(c3.moveToFirst();i<c3.getCount();i++,c3.moveToNext())
					{
						
						if((!cf.decode(c3.getString(c3.getColumnIndex("noinfoval"))).trim().equals("true") 
							&& cf.decode(c3.getString(c3.getColumnIndex("RCT_FieldVal"))).trim().equals("")) && !cf.decode(c3.getString(c3.getColumnIndex("title"))).equals("Predominant") && !cf.decode(c3.getString(c3.getColumnIndex("title"))).equals(upload_tit) )
						{
							cf.ShowToast("Please select the " +cf.decode(c3.getString(c3.getColumnIndex("title")))+ " under Roof Covering Type - "+cf.decode(c3.getString(c3.getColumnIndex("RCT_FielsName"))),0);
							return;
						}
						else if(cf.decode(c3.getString(c3.getColumnIndex("title"))).equals("Predominant") && cf.decode(c3.getString(c3.getColumnIndex("RCT_FieldVal"))).trim().equals("true"))
						{
							b=true;
						}
							
					}
				}else
				{
					
					cf.ShowToast("Atleast one Roof Covering Type should be added and saved.",0);
					return;
				}
				try
				{
					/***Checking for the all roof percentage must be hundered**/
				 Cursor per= cf.arr_db.rawQuery("Select RCT_FieldVal from "+cf.Roof_covertype_view+" as RCT " +
							" WHERE RCT_insp_id='"+cf.encode(Roof_page_value[5])+"' and	RCT_srid='"+cf.selectedhomeid+"' and RCT_TypeValue=(Select mst_custom_id from "+cf.master_tbl+" where mst_module_id='2' and mst_text='"+cf.encode(Roof_shap_tit)+"')" ,null);
					
					if(per.getCount()>0)
					{
						per.moveToFirst();
						int per_tot=0;
						for(int k=0;k<per.getCount();k++,per.moveToNext())
						{
							String per_str=cf.decode(per.getString(per.getColumnIndex("RCT_FieldVal")));
							if(!per_str.equals(""))
							{
								String[] tmp=per_str.split("~");
								if(!tmp[1].equals(""))
								{
									per_tot+=Integer.parseInt(tmp[1]);
								}
							}
							
						}
						if(per_tot!=100)
						{
							cf.ShowToast(Roof_shap_tit+" percentage should sum to 100 under Roof Covering Type.", 0);
							return;
						}
					}
					/***Checking for the all roof percentage must be hundered ends**/
				}
				catch (Exception e) {
					// TODO: handle exception
				}
			
			/**Checking weather the all requirred fields are saved ends**/
		
				if(b)
				{
					RC_Save_validation();
				}
				else
				{
					cf.ShowToast("Atleast one Roof Covering Type need to be Predominant.", 0);
				}
			
			break;
		case R.id.GCH_RC_homebtn:
			 cf.gohome();
			break;
		case R.id.loadcomments:
			/***Call for the comments***/
			getRoofValueForVariables(cf.Roof);
			int len=RC_cmt.getText().toString().length();
			
				if(load_comment )
				{
					load_comment=false;
					int loc[] = new int[2];
					v.getLocationOnScreen(loc);
					Intent in1 = cf.loadComments("Overall Roof Comments",loc);
					in1.putExtra("length", len);
					in1.putExtra("max_length", 500);
					startActivityForResult(in1, cf.loadcomment_code);
				}
			
			
		break;
		case R.id.ADD_save:
			if(!ADD_chk_top.isChecked())
			{
				Boolean returned=ADD_savevalidation(true);
				if(returned)
				{
					if(Add_Insert_Update())
					{
						cf.ShowToast("Additional Roof Details saved successfully", 1);
						((TextView) findViewById(R.id.ADD_comp)).setVisibility(View.VISIBLE);
						
					}
					else
					{
						cf.ShowToast("You have problem in saving data. Please try again.", 1);
						((TextView) findViewById(R.id.ADD_comp)).setVisibility(View.INVISIBLE);
					}
				}
				
					
			}
		break;
		case R.id.RCN_save:
			if(!RCN_chk_top.isChecked())
			{
				Boolean RCN_returned=RCN_savevalidation(true);
				if(RCN_returned)
				{
					if(RCN_Insert_Update())
					{
						cf.ShowToast("Roof Conditions Noted saved successfully", 1);
						((TextView) findViewById(R.id.RCN_comp)).setVisibility(View.VISIBLE);
					}
					else
					{
						cf.ShowToast("You have problem in saving data. Please try again.", 1);
						((TextView) findViewById(R.id.RCN_comp)).setVisibility(View.INVISIBLE);
					}
				}
			}
		break;
		/***Call for the comments ends***/
		case R.id.Add_help:
			cf.ShowToast("Complete for all Wind Mitigation Inspection. ", 0);
		break;
		case R.id.RCN_help:
			cf.ShowToast("Complete for all Wind Mitigation Inspection. ", 0);
		break;
		case R.id.abbrev:
			Intent s= new Intent(this,PolicyholdeInfoHead.class);
			s.putExtra("homeid", cf.selectedhomeid);
			s.putExtra("Type", "Abbreviation");
			s.putExtra("insp_id", cf.Insp_id);
			startActivityForResult(s, cf.info_requestcode);
		break;
		default:
			break;
		}
	}

	
	class RCT_EditClick implements OnClickListener
	{
		String type="";
		int edit; 
		RCT_EditClick(int edit)
		{
			this.edit=edit;
		}
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			type=v.getTag().toString();
			if(edit==1)
			{
				clear_RCT(0);
				updateRCT(type);
				
			}
			else
			{
				AlertDialog.Builder b =new AlertDialog.Builder(RoofSection.this);
				b.setTitle("Confirmation");
				b.setMessage("Do you want to delete the selected Roof Covering Type?");
				b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						try
						{
						cf.arr_db.execSQL("DELETE FROM "+cf.Roof_cover_type+" WHERE (RCT_masterid= (Select RM_masterid from "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(Roof_page_value[5])+"' and RM_Covering='"+cf.encode(type)+"' and RM_module_id='0'))");
						cf.arr_db.execSQL("DELETE from "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+cf.encode(Roof_page_value[5])+"' and RM_Enable='1' and RM_Covering='"+cf.encode(type)+"' and RM_module_id='0'");
								//"RCT_srid='"+cf.selectedhomeid+"' and RCT_insp_id='"+cf.encode(Roof_page_value[5])+"' and RCT_FielsName='"+cf.encode(type)+"'");
						}catch (Exception e) {
							// TODO: handle exception
							System.out.println("the exeption in delete"+e.getMessage());
						}
						System.gc();
						clear_RCT(1);
						show_RCT_List(1);
						cf.ShowToast("The selected Roof Covering Type has been deleted successfully.", 1);
						
					}
				});
				b.setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						
					}
				});   
				AlertDialog al=b.create();
				al.setIcon(R.drawable.alertmsg);
				al.setCancelable(false);
				al.show();  
				
			}
			
		}
		
	}
	class RCT_watcher implements TextWatcher
	{
		int id,max;
		RCT_watcher(int id,int max)
		{
			this.id=id;
			this.max=max;
		}
		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			TextView tv=((TextView) findViewById(id).findViewWithTag("Other_DESC_txt"));
			cf.showing_limit(s.toString(), tv, max);
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			
		}
		
	}
	 public String getvalfromoption(int id,String[] s)
	 {
		 for(int i=0;i<s.length;i++)
		 {
		 RadioButton tmp=(RadioButton) findViewById(id).findViewWithTag(s[i]);
		 if(tmp.isChecked())
		 {
			 return s[i];
		 }
		 
		 }
		 return "";
	 }
	 public String getvalfromchkoption(int id,String[] s)
	 {
		 String st="";
		 for(int i=0;i<s.length;i++)
		 {
			 CheckBox tmp=(CheckBox) findViewById(id).findViewWithTag(s[i]);
		 if(tmp.isChecked())
		 {
			 st+=s[i]+",";
		 }
		 
		 }
		 if(!st.trim().equals(""))
			 st=st.substring(0,st.length()-1);
		 return st;
	 }
	
	
	public LinearLayout Add_CHKption_dynamic(int id,String[] opt,int max,int maxwidth,String otherOption) {
		// TODO Auto-generated method stub
		
		TableRow.LayoutParams lin_params_ops = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
		LinearLayout op_li=new LinearLayout(this);
		op_li.setLayoutParams(lin_params_ops);
		op_li.setOrientation(LinearLayout.VERTICAL);
		
		/**Start to set the Radio buttons for the radio group Strats**/
		
		LinearLayout op_RG_li=new LinearLayout(this);
		op_RG_li.setLayoutParams(lin_params_ops);
		op_RG_li.setGravity(Gravity.CENTER_VERTICAL);
		
		for(int j=0;j<opt.length;j++)
		{
			
			
			CheckBox rb=new CheckBox(this,null,R.attr.checkbox);
			rb.setOnClickListener(new check_lis(id,opt,otherOption));
			
			rb.setText(opt[j]);
			rb.setMaxWidth(maxwidth);
			
			if(!opt[j].trim().equals(""))
			{
				rb.setTag(opt[j]);
			}
			else
			{
				
				rb.setTag("empty");
			}
			op_RG_li.addView(rb);
			
			if(opt.length>1)
			{
				
				
				if(((j+1)%(max)==0 && j!=0 )|| j==opt.length-1)
				{
					
					op_li.addView(op_RG_li);
					op_RG_li=new LinearLayout(this);
					op_RG_li.setGravity(Gravity.CENTER_VERTICAL);
		    		op_RG_li.setLayoutParams(lin_params_ops);
				}
			}
			else
			{
				op_li.addView(op_RG_li);
			}
		}
		
		return op_li;
	}
	public LinearLayout Add_ROption_dynamic(int id,String[] opt,int max,int maxwidth,String otherOption)
	{
		
		TableRow.LayoutParams lin_params_ops = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
		LinearLayout op_li=new LinearLayout(this);
		op_li.setLayoutParams(lin_params_ops);
		op_li.setOrientation(LinearLayout.VERTICAL);
		
		/**Start to set the Radio buttons for the radio group Strats**/
		
		LinearLayout op_RG_li=new LinearLayout(this);
		op_RG_li.setLayoutParams(lin_params_ops);
		op_RG_li.setGravity(Gravity.CENTER_VERTICAL);
		
		for(int j=0;j<opt.length;j++)
		{
		
			
			
			RadioButton rb=new RadioButton(this,null,R.attr.Roof_RCN_opt_rb);
			rb.setOnClickListener(new radiocheck(id,opt,otherOption));
			
			rb.setText(opt[j]);
			//rb.setMaxWidth(maxwidth);
			//rb.setMinWidth(100);
			if(!opt[j].trim().equals(""))
			{
				
				rb.setTag(opt[j]);
			}
			else
			{
			
				
				rb.setTag("empty");
				
			}
			op_RG_li.addView(rb,maxwidth,LayoutParams.FILL_PARENT);
			
			if(opt.length>1)
			{
				
				if(((j+1)%(max)==0 && j!=0 )|| j==opt.length-1)
				{
					op_li.addView(op_RG_li);
					op_RG_li=new LinearLayout(this);
					op_RG_li.setGravity(Gravity.CENTER_VERTICAL);
		    		op_RG_li.setLayoutParams(lin_params_ops);
				}
			}
			else
			{
				op_li.addView(op_RG_li);
			}
			
		}
		
		return op_li;
	}
	private LinearLayout Add_Spinption_dynamic(int id, String[] opt,int opt_maxW, final String other_option) {
		// TODO Auto-generated method stub
		TableRow.LayoutParams lin_params_ops = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
		LinearLayout op_li=new LinearLayout(this);
		op_li.setLayoutParams(lin_params_ops);
		op_li.setOrientation(LinearLayout.HORIZONTAL);
		
		/**Start to set the Radio buttons for the radio group Strats**/
		LinearLayout op_li_sub=new LinearLayout(this);
		op_li_sub.setLayoutParams(lin_params_ops);
		op_li_sub.setOrientation(LinearLayout.VERTICAL);	
			Spinner sp=new Spinner(this,null,R.attr.Spinner);
			ArrayAdapter adapter;
			adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, opt);
		 	adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 	sp.setAdapter(adapter);
		 	//sp.setMinimumWidth(opt_maxW);
			sp.setTag("Spinner_val");
			op_li_sub.addView(sp,opt_maxW,LayoutParams.WRAP_CONTENT);
			op_li.addView(op_li_sub,opt_maxW,LayoutParams.WRAP_CONTENT);
			
		 	sp.setOnItemSelectedListener(new Spselect(id,other_option));
		 	
			//rb.setOnClickListener(new radiocheck(id,opt,otherOption));
			
		return op_li;
	}
	private LinearLayout Add_DPinption_dynamic(int id, int opt_maxW) {
		// TODO Auto-generated method stub
		TableRow.LayoutParams lin_params_ops = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
		LinearLayout op_li=new LinearLayout(this);
		op_li.setLayoutParams(lin_params_ops);
		op_li.setOrientation(LinearLayout.HORIZONTAL);
		op_li.setGravity(Gravity.CENTER_VERTICAL);
		/**Start to set the Radio buttons for the radio group Strats**/
			
			LinearLayout rl= new LinearLayout(this);
			rl.setOrientation(LinearLayout.VERTICAL);
			EditText Ed=new EditText(this,null,R.attr.EditText);
			Ed.setVisibility(View.VISIBLE);
			Ed.setEnabled(false);
			Ed.setTag("Date");
			Ed.setFocusable(false);
			/*Ed.setMinWidth(opt_maxW);
			Ed.setMinWidth(opt_maxW);*/
			rl.addView(Ed,opt_maxW,LayoutParams.WRAP_CONTENT);
			op_li.addView(rl,opt_maxW,LayoutParams.WRAP_CONTENT);
			
			Button Bt=new Button(this,null,R.attr.DatePicker);
			Bt.setTag("Date_pck");
			op_li.addView(Bt);
			Bt.setOnClickListener(new datepick(id));
			
		return op_li;
	}
	private LinearLayout Add_ETinption_dynamic(int id, int opt_maxW,int option_max_l,String option_filter) {
		// TODO Auto-generated method stub
		TableRow.LayoutParams lin_params_ops = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
		LinearLayout op_li=new LinearLayout(this);
		op_li.setLayoutParams(lin_params_ops);
		op_li.setOrientation(LinearLayout.HORIZONTAL);
		
		/**Start to set the Radio buttons for the radio group Strats**/
				
			EditText Ed=new EditText(this,null,R.attr.EditText);
			Ed.setVisibility(View.VISIBLE);
			
			Ed.setTag("EditText");
			Ed.setFocusable(false);
			Ed.setOnTouchListener(new RC_editclick());
			Ed.setMinWidth(opt_maxW);
			Ed.setMinWidth(opt_maxW);
			InputFilter[] FilterArray = new InputFilter[1];
			FilterArray[0] = new InputFilter.LengthFilter(option_max_l);
			Ed.setFilters(FilterArray);
			if(option_filter.equals("Number"))
			{
				Ed.setInputType(InputType.TYPE_CLASS_NUMBER);
			}
			op_li.addView(Ed,opt_maxW,LayoutParams.WRAP_CONTENT);
			
		return op_li;
	}
	private LinearLayout Add_IPicker_dynamic(int id, int opt_maxW) {
		// TODO Auto-generated method stub
	
		TableRow.LayoutParams lin_params_ops = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
		LinearLayout op_li=new LinearLayout(this);
		op_li.setLayoutParams(lin_params_ops);
		op_li.setOrientation(LinearLayout.HORIZONTAL);
		op_li.setGravity(Gravity.CENTER_VERTICAL);
		/**Start to set the Radio buttons for the radio group Strats**/
				
			EditText Ed=new EditText(this,null,R.attr.EditText);
			Ed.setVisibility(View.VISIBLE);
			Ed.setEnabled(false);
			Ed.setTag("Path");
			//Ed.setFocusable(false);
			
			//op_li.addView(Ed,opt_maxW,LayoutParams.WRAP_CONTENT);
			
			Button Bt=new Button(this,null,R.attr.DatePicker);
			Bt.setTag("Image_pck");
			Bt.setBackgroundDrawable(getResources().getDrawable(R.drawable.imagepicker));
			Button Bt_clear=new Button(this,null,R.attr.Roof_RCN_opt_img);
			Bt_clear.setTag("Image_clear");
			Bt_clear.setBackgroundDrawable(getResources().getDrawable(R.drawable.cancelicon));
			Bt_clear.setPadding(5, 0, 0, 0);
			op_li.addView(Bt);
			TextView tv=new TextView(this,null,R.attr.text_view);
			
			tv.setVisibility(View.GONE);
			tv.setTextColor(Color.BLUE);
			tv.setTag("View_photos");
			tv.setText(Html.fromHtml("<u>View/Edit Photos</u>"));
			
			//op_li.addView(Bt_clear,30,30);
			Bt.setOnClickListener(new imagepick(id,0));
			Bt_clear.setOnClickListener(new imagepick(id,1));
			Ed.addTextChangedListener(new textwatcher(id));
			tv.setOnClickListener(new imagepick(id,0));
			tv.setVisibility(View.GONE);
			try
		    {
		    	if(findViewById(RCT_id[0]).findViewWithTag("Spinner_val")!=null)
		    	{
		    		String cv=((Spinner)findViewById(RCT_id[0]).findViewWithTag("Spinner_val")).getSelectedItem().toString().trim();
		    		Cursor c=cf.arr_db.rawQuery("Select * from "+cf.Roofcoverimages+" Where RIM_masterid=(Select RM_masterid from "+cf.Roof_master+
		    				" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+Roof_page_value[5]+"' and RM_Covering='"+cf.encode(cv)+"') order by RIM_Order", null);
		    		
		    		if(c.getCount()>0)
		    		{
		    			tv.setVisibility(View.VISIBLE);
		    		}
		    		
		    	}
		    }
		    catch (Exception e) {
				// TODO: handle exception
			}
			op_li.addView(tv);	
		return op_li;
	}
	class textwatcher implements TextWatcher
	{
		int id;
		textwatcher(int id)
		{
			this.id=id;
		}
		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			if(s.toString().trim().equals(""))
			{
				((Button) findViewById(id).findViewWithTag("Image_clear")).setVisibility(View.INVISIBLE);
			}
			else if(((Button) findViewById(id).findViewWithTag("Image_clear")).getVisibility()==View.INVISIBLE || ((Button) findViewById(id).findViewWithTag("Image_clear")).getVisibility()==View.GONE)
			{
				((Button) findViewById(id).findViewWithTag("Image_clear")).setVisibility(View.VISIBLE);
			}
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			
		}
		
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		System.out.println("requestCode"+requestCode);
		if(requestCode==14)
		{
		switch (resultCode) {
				case 10:
					System.out.println("case 10");
				
				cf.ShowToast("You have cancelled the generate Citizen Certificate ", 0);
				movetonext();
				break;
				case RESULT_OK:
				/**Before that need to generate the form throw the web service**/
					System.out.println("RESULT_OK 10");
				cf.ShowToast("You have generated Citizen Certificate", 0);
			
				movetonext();
				break;
				
		}
		}
		else if(requestCode==pick_img_code)
		{int id = 0;System.out.println("pick_img_code 10");
		String cv="";
			switch (resultCode) {
				case RESULT_CANCELED:
				//	cf.ShowToast("You have canceld the Roof cover image selection ", 0);
					id=data.getExtras().getInt("element_id");
					cv=data.getExtras().getString("Cover");
					//((EditText)findViewById(id1).findViewWithTag("Path")).setText("");
				break;
				case RESULT_OK:
					/*cf.ShowToast("The image uploaded successfully.",1);*/
				
					id=data.getExtras().getInt("element_id");
					cv=data.getExtras().getString("Cover");
					//((EditText)findViewById(id).findViewWithTag("Path")).setText(data.getExtras().getString("path").trim());
				break;
				
			}
			try
			{
				System.out.println("the values from intent"+id+" cover"+cv);
		    	if(id!=0)
		    	{
		    		Cursor c=cf.arr_db.rawQuery("Select * from "+cf.Roofcoverimages+" Where RIM_masterid=(Select RM_masterid from "+cf.Roof_master+
		    				" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+Roof_page_value[5]+"' and RM_Covering='"+cf.encode(cv)+"') order by RIM_Order", null);
		    		
		    		if(c.getCount()>0)
		    		{
		    			((TextView)findViewById(id).findViewWithTag("View_photos")).setVisibility(View.VISIBLE);
		    			((TextView)findViewById(id).findViewWithTag("View_photos")).setText(Html.fromHtml("<u>View/Edit Photos ("+c.getCount()+")</u>"));
		    			if(RCT_ShowValue.findViewWithTag(cv)!=null)
				    	{
				    		((TextView)RCT_ShowValue.findViewWithTag(cv)).setText(Html.fromHtml("<u>View Photos ("+c.getCount()+")</u>"));
				    		((TextView)RCT_ShowValue.findViewWithTag(cv)).setOnClickListener(new onclick_viewimage(cv,Roof_page_value[5]));
				    		((TextView)RCT_ShowValue.findViewWithTag(cv)).setTextColor(Color.BLUE);
				    	}
		    			else
		    			{
		    				System.out.println("null  only cames");
		    			}
		    			
		    		}
		    		else
		    		{
		    			((TextView)findViewById(id).findViewWithTag("View_photos")).setVisibility(View.GONE);
		    			if(RCT_ShowValue.findViewWithTag(cv)!=null)
				    	{
				    		((TextView)RCT_ShowValue.findViewWithTag(cv)).setText(Html.fromHtml("N/A"));
				    		((TextView)RCT_ShowValue.findViewWithTag(cv)).setTextColor(Color.BLACK);
				    		((TextView)RCT_ShowValue.findViewWithTag(cv)).setOnClickListener(null);
				    	}
		    			else
		    			{
		    				System.out.println("null  only cames");
		    			}
		    		}
		    		
		    	}
		    	
		    }
		    catch (Exception e) {
				// TODO: handle exception
			}
		}
		else if(requestCode==cf.loadcomment_code)
		{
			load_comment=true;
			if(resultCode==RESULT_OK)
			{
				RC_cmt.setText((RC_cmt.getText().toString()+" "+data.getExtras().getString("Comments")).trim());
				
			}
			
			
				
		}

	}

	public void Setcheckvalue(LinearLayout li,String s,String limter)
	{
		
		if(!s.trim().equals(""))
		{
			String[] tmp;
			
			if(s.contains(limter))
			{
				
				tmp=s.split(limter);
				for(int i=0;i<tmp.length;i++)
				{
					
					try
					{
						
						((CheckBox) li.findViewWithTag(tmp[i])).setChecked(true);
					}catch (Exception e) {
						// TODO: handle exception
						System.out.println("the error occuers"+e.getMessage());
					}
				}
			}
			else
			{
				((CheckBox) li.findViewWithTag(s)).setChecked(true);
			}
		}
		
	}
	
	   
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			
			Intent  myintent=null;
			
			
			if(!submenu.equals("true") && !startfrom.equals(""))
			{
				Roof=startfrom;
			}
			
			//Roof=Save_from;
			
			if(Roof.equals(cf.Roof_insp[0]))
			 {
			    /*** For 4poin***/
				 cf.goback(0);	 
			 }
			 else if(Roof.equals(cf.Roof_insp[1]))
			 {
				/*** Roof Survey***/
				 cf.chk_InspTypeQuery(cf.selectedhomeid,cf.Roof_insp[0]);
			
				 if(cf.isaccess)
				 {
					 cf.Roof=cf.Roof_insp[0];
					 cf.goback(1);
				 
				 }
				 else
				 {
					 cf.goback(0);
				 }
			        
			 }
			 else  if(Roof.equals(cf.Roof_insp[4]))
			 {
					
			 /*** For GCH***/
				
				 cf.goclass(42);
			/***	 For GCH Ends***/
			 }
			 else if(Roof.equals(cf.getResourcesvalue(R.string.insp_5)))
			 {
				
				/*** Roof Survey***/
				/*if(cf.identityval==33 || cf.identityval==34)
				{*/
			 
				 cf.identityval=33; 
			 
					cf.gotoclass(cf.identityval, WindMitCommareas.class);
				//}
				
				 /****Roof Survay ends**/
			 }
			 else if(Roof.equals(cf.getResourcesvalue(R.string.insp_6)))
			 {
				
				/*** Roof Survey***/
				/*if(cf.identityval==33 || cf.identityval==34)
				{*/
			 
				 cf.identityval=34; 
			 
					cf.gotoclass(cf.identityval, WindMitCommareas.class);
				//}
				
				 /****Roof Survay ends**/
			 }
			 else if(Roof.equals(cf.getResourcesvalue(R.string.insp_4)))
			 {
				
				/*** Roof Survey***/
				/*if(cf.identityval==33 || cf.identityval==34)
				{*/
			 
				 cf.identityval=32; 
			 
					cf.gotoclass(cf.identityval, WindMitCommareas.class);
				//}
				
				 /****Roof Survay ends**/
			 }
			 else 
			 {
				 /*** For GCH***/
				 cf.goback(0);
				 
				 
			/***	 For GCH Ends***/ 
			 }
			
		return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}	
	
	private String checkfordateistoday(String getinspecdate2) {
		// TODO Auto-generated method stub
		String chkdate = null;
		int i1 = getinspecdate2.indexOf("/");
		String result = getinspecdate2.substring(0, i1);
		int i2 = getinspecdate2.lastIndexOf("/");
		String result1 = getinspecdate2.substring(i1 + 1, i2);
		String result2 = getinspecdate2.substring(i2 + 1);
		result2 = result2.trim();
		int j1 = Integer.parseInt(result);
		int j2 = Integer.parseInt(result1);
		int j = Integer.parseInt(result2);
		final Calendar c = Calendar.getInstance();
		int thsyr = c.get(Calendar.YEAR);
		int curmnth = c.get(Calendar.MONTH);
		int curdate = c.get(Calendar.DAY_OF_MONTH);
		int day = c.get(Calendar.DAY_OF_WEEK);
		curmnth = curmnth + 1;

		if (j < thsyr || (j1 < curmnth && j <= thsyr)

		|| (j2 <= curdate && j1 <= curmnth && j <= thsyr)) {
			chkdate = "true";
		} else {
			chkdate = "false";
		}

		return chkdate;

	}

	public void onWindowFocusChanged(boolean hasFocus) {

        super.onWindowFocusChanged(hasFocus);
         
}
	public void movetonext()
	{
	//	Roof_page_value=getRoofValueForVariables(Roof);
		
		 String rf="";
		String nxtinsp="";
		System.out.println("Roof="+Roof);
		if(!Roof.equals("Canceled"))
		{
			// nxtinsp=getnextinsp();
		
			getnextinspnew();
			return;
		}
		else
		{
			Roof=cf.Roof;
		}
		
		if(submenu.equals("true"))
		{
			Save_from=Roof;
		}
			//Save_from;)
		if(nxtinsp.equals(""))
		{
			if(!Roof.equals(Save_from))
				rf=Roof;
			
			if(!startfrom.equals(""))
			{
				cf.Roof=startfrom;
				Roof=startfrom;
			}
			/*else
			{
				cf.Roof=Save_from;
				Roof=Save_from;
			}*/
			
		}
	
		if(!nxtinsp.equals(""))
		{
			cf.Roof=nxtinsp;
			
			Intent myintent = new Intent(this,RoofSection.class); 
			myintent.putExtra("Roof", cf.Roof);
			myintent.putExtra("Roof_save_from",Save_from);
			myintent.putExtra("Roof_skiiped_insp",skipped_insp);
			
			cf.putExtraIntent(myintent);
			//cf.showloadingalert(myintent);
			//startActivity(myintent);
		}
		else if(Roof.equals(cf.Roof_insp[0]))
		 {
			boolean  s=false;
		 /*** For 4poin***/
			
			 /***For hide the attic if the External only selected menas ends**/
		
				cf.goclass(23);
		
		 }
		 else if(Roof.equals(cf.Roof_insp[1]))
		 {
			if(!rf.equals(Roof) && !rf.equals("") )
			{
				cf.goclass(29);
			}
			/*** Roof Survey***/
		 }
		 else  if(Roof.equals(cf.Roof_insp[4]))
		 {
				
		 /*** For GCH***/
			
			 cf.goclass(44);
		/***	 For GCH Ends***/
		 }
		 else if(Roof.equals(cf.getResourcesvalue(R.string.insp_5)))
		 {
			 
			/*** Roof Survey***/
				/*if(cf.identityval==33 || cf.identityval==34)
				{*/
					cf.gotoclass(33, WindMitCommAuxbuildings.class);
				//}
				
			 /****Roof Survay ends**/
		 } 
		 else if (Roof.equals(cf.getResourcesvalue(R.string.insp_6)))
		 {
			 cf.gotoclass(34, WindMitCommAuxbuildings.class);
		 }
		 else if (Roof.equals(cf.getResourcesvalue(R.string.insp_4)))
		 {
			 cf.gotoclass(32, RoofSection.class);
		 }
		 else 
		 {
			 /*** For GCH***/
			// cf.goback(PolicyHolder.class);
		/***	 For GCH Ends***/ 
		 }
	}
	protected void atticinsert() {
		// TODO Auto-generated method stub
			try
			{
				cf.arr_db.execSQL("INSERT INTO "
						+ cf.Four_Electrical_Attic
						+ " (fld_srid,atcinc,atcoptions,atccomments,rfdcinc,rfdcmat,rfstmat,rfdcthi,rfdcatt,rftruss,"+
						"nailspac,missednails,rfwallinc,rtowopt,nail3,nail4,truss,truss1,gablewallinc,gablewallopt,"+
						"atccondinc,moisture,dsheath,droof,nsalt,othertxt,other)"
						+ "VALUES('"+cf.selectedhomeid+"','"+str+"','','','','','','','','','','',"+
						  "'','','','','','','','','','','','','','','')");

				cf.goclass(23);
		 	}
			catch (Exception E)
			{
				String strerrorlog="Inserting the Attic Details  - FourPoint";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			}
	}

	private void getnextinspnew() {
		 Roof="Canceled";
	     movetonext();}
	public void Setfocuserro(View chi,View par,View secondry)
	{
		
		if(par.getVisibility()==View.VISIBLE)
		{ 		chi.setFocusable(true);
				chi.setFocusableInTouchMode(true);
				chi.requestFocus();
				if(!(chi instanceof EditText))
				{
					
					chi.setFocusableInTouchMode(false);
					chi.setFocusable(false);
				}
		}
		else
		{
			secondry.setFocusableInTouchMode(true);
			secondry.requestFocus();
			secondry.setFocusableInTouchMode(false);
			secondry.setFocusable(false);
			secondry.setFocusable(true);
		}
}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		boolean res=false;
		try
		{
					res=generate_form();
				
		}
		catch (SocketException e) {
			// TODO: handle exception
			
		}
		catch (NetworkErrorException e) {
			// TODO: handle exception
			
		}
		catch (TimeoutException e) {
			// TODO: handle exception
			
		}
		catch (IOException e) {
			// TODO: handle exception
			
		}
		catch (XmlPullParserException e) {
			// TODO: handle exception
			
		}
		catch (Exception e) {
			// TODO: handle exception
			
		}
		if(res)
		{
			show_handler=1;
		}
		else
		{
			show_handler=2;
			//movetonext();
		}
		handler.sendEmptyMessage(0);
	}
	private Handler handler = new Handler() {
		

		public void handleMessage(Message msg) {
			pd.dismiss();
			if(show_handler==1)
			{

				
					cf.ShowToast("Citizen Certification Form generated successfully. ", 0);
				
			}
			else if(show_handler==2)
			{
				cf.ShowToast(" Network unavailable! Unable to generate Citizen Certification Form. Please try again later", 0);
			}
				
		}
	};
}