package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class BIObservations extends Activity {
	String Obser_NA="",Observ1="",Observ2="",Observ3="",Observ4="",Observ5="",Observ6="",
			obs1txt="",obs2txt="",obs3txt="",obs4txt="",obs5txt="",obs6txt="",
			BI_OBSERV1="",BI_OBSERV2="",BI_OBSERV3="",BI_OBSERV4="",BI_OBSERV5="",BI_OBSERV6="",observnotappl="";
	CommonFunctions cf;
	Cursor c1;
	RadioGroup obs_rdgp1val,obs_rdgp2val,obs_rdgp3val,obs_rdgp4val,obs_rdgp5val,obs_rdgp6val,obs_rdgp7val;
	public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	       
	         cf = new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);				
		 	}
	        setContentView(R.layout.observation);
	        cf.getDeviceDimensions();
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
         	hdr_layout.addView(new HdrOnclickListener(this,1,"General","Building Info","General Building Observations",1,1,cf));
         	LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
         	main_layout.setMinimumWidth(cf.wd);
         	main_layout.addView(new MyOnclickListener(getApplicationContext(), 1, 0,0,cf));
         	LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
            submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 16, 1,0,cf));
	        LinearLayout submenu2_layout = (LinearLayout) findViewById(R.id.submenu1); 
	        submenu2_layout.addView(new MyOnclickListener(getApplicationContext(), 452, 1,0,cf));
	        TableRow tblrw = (TableRow)findViewById(R.id.row2);
	        tblrw.setMinimumHeight(cf.ht); 	
	        cf.CreateARRTable(61);
	        Declaration();
	        setValue();
	    }
	private void setValue() {
		// TODO Auto-generated method stub
		selecttbl();
		if(c1.getCount()==0)
		{
			 cf.getinspectioname(cf.selectedhomeid);
			 if(cf.selinspname.equals("7"))// 7 is for GCH in our db
			 {
				 ((CheckBox)findViewById(R.id.observnotapplicable)).setChecked(false);
				 ((LinearLayout)findViewById(R.id.observation_table)).setVisibility(cf.v1.VISIBLE);
			 }
			 else
			 {
				 ((CheckBox)findViewById(R.id.observnotapplicable)).setChecked(true);
				 ((LinearLayout)findViewById(R.id.observation_table)).setVisibility(cf.v1.GONE);
			 }
			
			
		}
		else
		{
				if(Obser_NA.equals("0") || Obser_NA.equals(""))
				{
					((LinearLayout)findViewById(R.id.observation_table)).setVisibility(cf.v1.VISIBLE);
					 if(!Observ1.equals("")){
							String Observ1_split[] = Observ1.split("&#94;");
							((RadioButton) obs_rdgp1val.findViewWithTag(Observ1_split[0])).setChecked(true);
							if(Observ1_split[0].equals("No")){
								((LinearLayout)findViewById(R.id.obs_rw1_lin)).setVisibility(cf.v1.VISIBLE);
								((EditText)findViewById(R.id.etobserv1desc)).setText(Observ1_split[1]);						
							}
							else
							{
								((LinearLayout)findViewById(R.id.obs_rw1_lin)).setVisibility(cf.v1.GONE);
								((EditText)findViewById(R.id.etobserv1desc)).setText("");}
								((TextView)findViewById(R.id.savetxtbuildobs)).setVisibility(cf.v1.VISIBLE);
							}
						else
						{
							((TextView)findViewById(R.id.savetxtbuildobs)).setVisibility(cf.v1.GONE);
							((LinearLayout)findViewById(R.id.observation_table)).setVisibility(cf.v1.GONE);
							((CheckBox)findViewById(R.id.observnotapplicable)).setChecked(true);
						}
						if(!Observ2.equals("")){
							String Observ2_split[] = Observ2.split("&#94;");
							((RadioButton) obs_rdgp2val.findViewWithTag(Observ2_split[0])).setChecked(true);
							if(Observ2_split[0].equals("Yes")){((LinearLayout)findViewById(R.id.obs_rw2_lin)).setVisibility(cf.v1.VISIBLE);
							((EditText)findViewById(R.id.etobserv2desc)).setText(Observ2_split[1]);}
							else{((LinearLayout)findViewById(R.id.obs_rw2_lin)).setVisibility(cf.v1.GONE);
							((EditText)findViewById(R.id.etobserv2desc)).setText("");}
						}		
						if(!Observ3.equals("")){
							String Observ3_split[] = Observ3.split("&#94;");
							((RadioButton) obs_rdgp3val.findViewWithTag(Observ3_split[0])).setChecked(true);
							if(Observ3_split[0].equals("No")){((LinearLayout)findViewById(R.id.obs_rw3_lin)).setVisibility(cf.v1.VISIBLE);
							((EditText)findViewById(R.id.etobserv3desc)).setText(Observ3_split[1]);}
							else{((LinearLayout)findViewById(R.id.obs_rw3_lin)).setVisibility(cf.v1.GONE);
							((EditText)findViewById(R.id.etobserv3desc)).setText("");}
						}
						if(!Observ4.equals("")){
							String Observ4_split[] = Observ4.split("&#94;");
							((RadioButton) obs_rdgp4val.findViewWithTag(Observ4_split[0])).setChecked(true);
							if(Observ4_split[0].equals("Yes")){((LinearLayout)findViewById(R.id.obs_rw4_lin)).setVisibility(cf.v1.VISIBLE);
							((EditText)findViewById(R.id.etobserv4desc)).setText(Observ4_split[1]);}
							else{((LinearLayout)findViewById(R.id.obs_rw4_lin)).setVisibility(cf.v1.GONE);
							((EditText)findViewById(R.id.etobserv4desc)).setText("");}
						}
						if(!Observ5.equals("")){
							String Observ5_split[] = Observ5.split("&#94;");
							((RadioButton) obs_rdgp5val.findViewWithTag(Observ5_split[0])).setChecked(true);
							if(Observ5_split[0].equals("Yes")){((LinearLayout)findViewById(R.id.obs_rw5_lin)).setVisibility(cf.v1.VISIBLE);
							((EditText)findViewById(R.id.etobserv5desc)).setText(Observ5_split[1]);}
							else{((LinearLayout)findViewById(R.id.obs_rw5_lin)).setVisibility(cf.v1.GONE);
							((EditText)findViewById(R.id.etobserv5desc)).setText("");}
						}
				}
				else
				{
					((CheckBox)findViewById(R.id.observnotapplicable)).setChecked(true);
					((LinearLayout)findViewById(R.id.observation_table)).setVisibility(cf.v1.GONE);
					((TextView)findViewById(R.id.savetxtbuildobs)).setVisibility(cf.v1.VISIBLE);
				}
		}
	}
	private void Declaration() {
		// TODO Auto-generated method stub
		cf.setupUI((ScrollView) findViewById(R.id.scr));
		 obs_rdgp1val= (RadioGroup)findViewById(R.id.obs_rdgp1);
		 obs_rdgp1val.setOnCheckedChangeListener(new checklistenetr(1));
		 obs_rdgp2val= (RadioGroup)findViewById(R.id.obs_rdgp2);
		 obs_rdgp2val.setOnCheckedChangeListener(new checklistenetr(2));
		 obs_rdgp3val= (RadioGroup)findViewById(R.id.obs_rdgp3);
		 obs_rdgp3val.setOnCheckedChangeListener(new checklistenetr(3));
		 obs_rdgp4val= (RadioGroup)findViewById(R.id.obs_rdgp4);
		 obs_rdgp4val.setOnCheckedChangeListener(new checklistenetr(4));
		 obs_rdgp5val= (RadioGroup)findViewById(R.id.obs_rdgp5);
		 obs_rdgp5val.setOnCheckedChangeListener(new checklistenetr(5));
		 
		 ((EditText)findViewById(R.id.etobserv1desc)).addTextChangedListener(new textwatcher(1));
		  ((EditText)findViewById(R.id.etobserv2desc)).addTextChangedListener(new textwatcher(2));
		  ((EditText)findViewById(R.id.etobserv3desc)).addTextChangedListener(new textwatcher(3));
		  ((EditText)findViewById(R.id.etobserv4desc)).addTextChangedListener(new textwatcher(4));
		  ((EditText)findViewById(R.id.etobserv5desc)).addTextChangedListener(new textwatcher(5));
		  
		  
	}
	 class textwatcher implements TextWatcher
	 {
	       public int type;
	         textwatcher(int type)
	    	{
	    		this.type=type;
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {
	    		// TODO Auto-generated method stub
	    		if(this.type==1)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs_tv_type1),250); 
	    			 if (s.toString().startsWith(" "))
	    		     {
	    				 ((EditText)findViewById(R.id.etobserv1desc)).setText("");
	    		     }
	    		}
	    		else if(this.type==2)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs_tv_type2),250); 
	    			 if (s.toString().startsWith(" "))
	    		     {
	    				 ((EditText)findViewById(R.id.etobserv2desc)).setText("");
	    		     }
	    		}
	    		else if(this.type==3)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs_tv_type3),250);
	    			 if (s.toString().startsWith(" "))
	    		     {
	    				 ((EditText)findViewById(R.id.etobserv3desc)).setText("");
	    		     }
	    		}
	    		else if(this.type==4)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs_tv_type4),250);
	    			 if (s.toString().startsWith(" "))
	    		     {
	    				 ((EditText)findViewById(R.id.etobserv4desc)).setText("");
	    		     }
	    		}
	    		else if(this.type==5)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs_tv_type5),250);
	    			 if (s.toString().startsWith(" "))
	    		     {
	    				 ((EditText)findViewById(R.id.etobserv5desc)).setText("");
	    		     }
	    		}
	    	}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				
			}
	 }
	class checklistenetr implements OnCheckedChangeListener
	{
    	int i;
		public checklistenetr(int i) {
			// TODO Auto-generated constructor stub
		 this.i=i;
		}

		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			   RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
		        // This puts the value (true/false) into the variable
		        boolean isChecked = checkedRadioButton.isChecked();
		        // If the radiobutton that has changed in check state is now checked...
		        if (isChecked)
		        {
		          switch (i) {	
					case 1:
						obs1txt= checkedRadioButton.getText().toString().trim();
						if(obs1txt.equals("No")){
							((LinearLayout)findViewById(R.id.obs_rw1_lin)).setVisibility(cf.v1.VISIBLE);
							}
						else if(obs1txt.equals("Yes")){
							((LinearLayout)findViewById(R.id.obs_rw1_lin)).setVisibility(cf.v1.GONE);
							((EditText)findViewById(R.id.etobserv1desc)).setText("");}
						else if(obs1txt.equals("Not Determined")){
							((LinearLayout)findViewById(R.id.obs_rw1_lin)).setVisibility(cf.v1.GONE);
							((EditText)findViewById(R.id.etobserv1desc)).setText("");}
						else if(obs1txt.equals("Not Applicable"))
						{
							((LinearLayout)findViewById(R.id.obs_rw1_lin)).setVisibility(cf.v1.GONE);
							((EditText)findViewById(R.id.etobserv1desc)).setText("");
						}
						 break;
					case 2:
						obs2txt= checkedRadioButton.getText().toString().trim();
						if(obs2txt.equals("Yes")){
							((LinearLayout)findViewById(R.id.obs_rw2_lin)).setVisibility(cf.v1.VISIBLE);
							}
						else if(obs2txt.equals("No")){
							((LinearLayout)findViewById(R.id.obs_rw2_lin)).setVisibility(cf.v1.GONE);
							((EditText)findViewById(R.id.etobserv2desc)).setText("");}
						else if(obs2txt.equals("Not Determined")){
							((LinearLayout)findViewById(R.id.obs_rw2_lin)).setVisibility(cf.v1.GONE);
							((EditText)findViewById(R.id.etobserv2desc)).setText("");}
						else if(obs2txt.equals("Not Applicable"))
						{
							((LinearLayout)findViewById(R.id.obs_rw2_lin)).setVisibility(cf.v1.GONE);
							((EditText)findViewById(R.id.etobserv2desc)).setText("");
							}
						 break;
					case 3:
						obs3txt= checkedRadioButton.getText().toString().trim();
						if(obs3txt.equals("Yes")){
							((LinearLayout)findViewById(R.id.obs_rw3_lin)).setVisibility(cf.v1.GONE);
							((EditText)findViewById(R.id.etobserv3desc)).setText("");
						}
						else if(obs3txt.equals("No")){
							((LinearLayout)findViewById(R.id.obs_rw3_lin)).setVisibility(cf.v1.VISIBLE);
							}
						
						else if(obs3txt.equals("Not Determined")){
							((LinearLayout)findViewById(R.id.obs_rw3_lin)).setVisibility(cf.v1.GONE);
							((EditText)findViewById(R.id.etobserv3desc)).setText("");
						}
						else if(obs3txt.equals("Not Applicable"))
						{
							((LinearLayout)findViewById(R.id.obs_rw3_lin)).setVisibility(cf.v1.GONE);
							((EditText)findViewById(R.id.etobserv3desc)).setText("");
							}
						 break;
					case 4:
						obs4txt= checkedRadioButton.getText().toString().trim();
						if(obs4txt.equals("Yes")){
							((LinearLayout)findViewById(R.id.obs_rw4_lin)).setVisibility(cf.v1.VISIBLE);
							}
						else if(obs4txt.equals("No")){
							((LinearLayout)findViewById(R.id.obs_rw4_lin)).setVisibility(cf.v1.GONE);
							((EditText)findViewById(R.id.etobserv4desc)).setText("");}
						else if(obs4txt.equals("Not Determined")){
							((LinearLayout)findViewById(R.id.obs_rw4_lin)).setVisibility(cf.v1.GONE);
							((EditText)findViewById(R.id.etobserv4desc)).setText("");}
						else if(obs4txt.equals("Not Applicable"))
						{
							((LinearLayout)findViewById(R.id.obs_rw4_lin)).setVisibility(cf.v1.GONE);
							((EditText)findViewById(R.id.etobserv4desc)).setText("");
							}
						 break;
					case 5:
						obs5txt= checkedRadioButton.getText().toString().trim();
						if(obs5txt.equals("Yes")){
							((LinearLayout)findViewById(R.id.obs_rw5_lin)).setVisibility(cf.v1.VISIBLE);
							}
						else if(obs5txt.equals("No")){
							((LinearLayout)findViewById(R.id.obs_rw5_lin)).setVisibility(cf.v1.GONE);
							((EditText)findViewById(R.id.etobserv5desc)).setText("");}
						else if(obs5txt.equals("Not Determined")){
							((LinearLayout)findViewById(R.id.obs_rw5_lin)).setVisibility(cf.v1.GONE);
							((EditText)findViewById(R.id.etobserv5desc)).setText("");}
						else if(obs5txt.equals("Not Applicable"))
						{
							((LinearLayout)findViewById(R.id.obs_rw5_lin)).setVisibility(cf.v1.GONE);
							((EditText)findViewById(R.id.etobserv5desc)).setText("");
							}
						 break;
					
		          }
					}
		        }
		     }
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.mouseoverid:
			cf.ShowToast("Use for General Conditions Report only.",0);
			break;
		case R.id.obssave:
			boolean b[] = new boolean[6];
			selecttbl();
			if(((CheckBox)findViewById(R.id.observnotapplicable)).isChecked()==false)
			{
			b[0]=(obs1txt.equals("No"))? ((((EditText)findViewById(R.id.etobserv1desc)).getText().toString().trim().equals(""))? false:true):true;
			b[1]=(obs2txt.equals("Yes"))? ((((EditText)findViewById(R.id.etobserv2desc)).getText().toString().trim().equals(""))? false:true):true;
			b[2]=(obs3txt.equals("No"))? ((((EditText)findViewById(R.id.etobserv3desc)).getText().toString().trim().equals(""))? false:true):true;
			b[3]=(obs4txt.equals("Yes"))? ((((EditText)findViewById(R.id.etobserv4desc)).getText().toString().trim().equals(""))? false:true):true;
			b[4]=(obs5txt.equals("Yes"))? ((((EditText)findViewById(R.id.etobserv5desc)).getText().toString().trim().equals(""))? false:true):true;
			
			if(!"".equals(obs1txt) && b[0]==true)
			{
				if(!"".equals(obs2txt) && b[1]==true)
				{
					if(!"".equals(obs3txt) && b[2]==true)
					{
						if(!"".equals(obs4txt) && b[3]==true)
						{
							if(!"".equals(obs5txt) && b[4]==true)
							{
								InsertObservation();																						
							}
							else
							{
								if(obs5txt.equals("")){	cf.ShowToast("Please select Permit Information for Alterations Confirmed.", 0); }
								else{	cf.ShowToast("Please enter the Comments under Permit Information for Alterations Confirmed.", 0);
								cf.setFocus(((EditText)findViewById(R.id.etobserv5desc)));
								}
							}
						}
						else
						{
							if(obs4txt.equals("")){	cf.ShowToast("Please select Visible Non-Standard/Home Made/Do it Yourself Alterations.", 0); }
							else{	cf.ShowToast("Please enter the Comments under Visible Non-Standard/Home Made/Do it Yourself Alterations.", 0);
							cf.setFocus(((EditText)findViewById(R.id.etobserv4desc)));}	
						}
					}
					else
					{
						if(obs3txt.equals("")){	cf.ShowToast("Please select Professional Construction Practices Appears Present.", 0); }
						else{	cf.ShowToast("Please enter the Comments under Professional Construction Practices Appears Present.", 0);
						cf.setFocus(((EditText)findViewById(R.id.etobserv3desc)));}	
					}
				}
				else
				{
					if(obs2txt.equals("")){	cf.ShowToast("Please select Alterations / Remodeling Noted Since Original Construction.", 0); }
					else{	cf.ShowToast("Please enter the Comments under Alterations / Remodeling Noted Since Original Construction.", 0);
					cf.setFocus(((EditText)findViewById(R.id.etobserv2desc)));}	
				}
			}
			else
			{
				if(obs1txt.equals("")){	cf.ShowToast("Please select Construction Originally Intended for Current Use.", 0); }
				else{	cf.ShowToast("Please enter the Comments under Construction Originally Intended for Current Use.", 0);
				cf.setFocus(((EditText)findViewById(R.id.etobserv1desc)));}	
			}
			}
			else
			{
				InsertObservation();	
			}
			break;
		case R.id.observnotapplicable:
			if(((CheckBox)findViewById(R.id.observnotapplicable)).isChecked())
			{
				observnotappl="1";
				if(!Observ1.equals(""))
				{
					AlertDialog.Builder builder = new AlertDialog.Builder(BIObservations.this);
					builder.setTitle("Confirmation");
					builder.setIcon(R.drawable.alertmsg);
					builder.setMessage("Do you want to clear the General Building Observation data?").setCancelable(false)
							.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,int id1) 
										{
												clearvalues();
												observationclear();
												((CheckBox)findViewById(R.id.observnotapplicable)).setChecked(true);
												((LinearLayout)findViewById(R.id.observation_table)).setVisibility(cf.v1.GONE);
												((TextView)findViewById(R.id.savetxtbuildobs)).setVisibility(cf.v1.GONE);
										}
									})
							.setNegativeButton("No",new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,	int id) {
											
											((CheckBox)findViewById(R.id.observnotapplicable)).setChecked(false);
											dialog.cancel();											
									}
							});
					builder.show();
				}
				else
				{
					clearvalues();
					((TextView)findViewById(R.id.savetxtbuildobs)).setVisibility(cf.v1.GONE);
					((LinearLayout)findViewById(R.id.observation_table)).setVisibility(cf.v1.GONE);
				}
			}
			else
			{
				observnotappl="0";clearvalues();observationclear();
				((LinearLayout)findViewById(R.id.observation_table)).setVisibility(cf.v1.VISIBLE);
				((TextView)findViewById(R.id.savetxtbuildobs)).setVisibility(cf.v1.GONE);
			}
			break;
		}		
	}	
	private void selecttbl() {
		c1 = cf.SelectTablefunction(cf.BI_General, " where fld_srid='"+cf.selectedhomeid+"'");
		if(c1.getCount()>0)
		{
			c1.moveToFirst();
			 Obser_NA = c1.getString(c1.getColumnIndex("BI_OBSERV_NA"));
			 Observ1 = cf.decode(c1.getString(c1.getColumnIndex("BI_OSERV1")));
			 Observ2 = cf.decode(c1.getString(c1.getColumnIndex("BI_OSERV2")));
			 Observ3 = cf.decode(c1.getString(c1.getColumnIndex("BI_OSERV3")));
			 Observ4 = cf.decode(c1.getString(c1.getColumnIndex("BI_OSERV4")));
			 Observ5 = cf.decode(c1.getString(c1.getColumnIndex("BI_OSERV5")));
		}
	}
	private void InsertObservation() {
		// TODO Auto-generated method stub		
		if(!obs1txt.equals("")){BI_OBSERV1 =obs1txt+"&#94;"+((EditText)findViewById(R.id.etobserv1desc)).getText().toString().trim();}
		if(!obs2txt.equals("")){BI_OBSERV2 =obs2txt+"&#94;"+((EditText)findViewById(R.id.etobserv2desc)).getText().toString().trim();}
		if(!obs3txt.equals("")){BI_OBSERV3 =obs3txt+"&#94;"+((EditText)findViewById(R.id.etobserv3desc)).getText().toString().trim();}
		if(!obs4txt.equals("")){BI_OBSERV4 =obs4txt+"&#94;"+((EditText)findViewById(R.id.etobserv4desc)).getText().toString().trim();}
		if(!obs5txt.equals("")){BI_OBSERV5 =obs5txt+"&#94;"+((EditText)findViewById(R.id.etobserv5desc)).getText().toString().trim();}
		if(((CheckBox)findViewById(R.id.observnotapplicable)).isChecked()){observnotappl="1";clearvalues();}else{observnotappl="0";}
		updatenotappl();
		cf.ShowToast("General Building Observations saved successfully",1);
	//	((TextView)findViewById(R.id.savetxtbuildobs)).setVisibility(cf.v1.VISIBLE);
		cf.goclass(453);
	}

	private void updatenotappl()
	{
	  try
	  {
		selecttbl();System.out.println("updddd"+c1.getCount());
		if(c1.getCount()==0)
		{
			cf.arr_db.execSQL("INSERT INTO " + cf.BI_General + " (fld_srid,BI_OBSERV_NA,BI_OSERV1,BI_OSERV2,BI_OSERV3,BI_OSERV4,BI_OSERV5,BI_OSERV6)"
					+ "VALUES ('"+cf.selectedhomeid+"','"+observnotappl+"','"+cf.encode(BI_OBSERV1)+"','"+cf.encode(BI_OBSERV2)+"','"+cf.encode(BI_OBSERV3)+"','"+cf.encode(BI_OBSERV4)+"','"+cf.encode(BI_OBSERV5)+"','')");
		}
		else
		{
			cf.arr_db.execSQL("UPDATE "
					+ cf.BI_General
					+ " SET BI_OSERV1='"+cf.encode(BI_OBSERV1)+"',BI_OSERV2='"+cf.encode(BI_OBSERV2)+"',BI_OSERV3='"+cf.encode(BI_OBSERV3)+"',BI_OSERV4='"+cf.encode(BI_OBSERV4)+"',BI_OSERV5='"+cf.encode(BI_OBSERV5)+"',BI_OSERV6='',BI_OBSERV_NA='"+observnotappl+"'"
					+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
		}
	  }
	  catch(Exception e)
	  {
		  
	  }
	}
	private void clearvalues()
	{
		BI_OBSERV1="";BI_OBSERV2="";BI_OBSERV3="";BI_OBSERV4="";BI_OBSERV5="";BI_OBSERV6="";
	}
	protected void observationclear() {
		// TODO Auto-generated method stub
		try{obs_rdgp1val.clearCheck();}catch(Exception e){}
		try{obs_rdgp2val.clearCheck();}catch(Exception e){}
		try{obs_rdgp3val.clearCheck();}catch(Exception e){}
		try{obs_rdgp4val.clearCheck();}catch(Exception e){}
		try{obs_rdgp5val.clearCheck();}catch(Exception e){}
		
		((LinearLayout)findViewById(R.id.obs_rw1_lin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.obs_rw2_lin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.obs_rw3_lin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.obs_rw4_lin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.obs_rw5_lin)).setVisibility(cf.v1.GONE);
		
		((EditText)findViewById(R.id.etobserv1desc)).setText("");
		((EditText)findViewById(R.id.etobserv2desc)).setText("");
		((EditText)findViewById(R.id.etobserv3desc)).setText("");
		((EditText)findViewById(R.id.etobserv4desc)).setText("");obs1txt="";obs2txt="";obs3txt="";obs4txt="";obs5txt="";obs6txt="";
		((EditText)findViewById(R.id.etobserv5desc)).setText("");Observ1="";Observ2="";Observ3="";Observ4="";Observ5="";
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			cf.goclass(451);
			return true;
		}		
		return super.onKeyDown(keyCode, event);
	}
}
