/*
   ************************************************************
   * Project: ALL RISK REPORT
   * Module : Observation4.java
   * Creation History:
      Created By : Gowri on 
   * Modification History:
      Last Modified By : Gowri on 04/04/2013
   ************************************************************  
*/
package idsoft.inspectiondepot.ARR;


import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.SinkSummary.Touch_Listener;
import idsoft.inspectiondepot.ARR.SinkSummary.checklistenetr;
import idsoft.inspectiondepot.ARR.SinkSummary.textwatcher;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class Observation4 extends Activity{
	CommonFunctions cf;
	RadioGroup obs23rdgval,obs23ardgval,obs23brdgval,obs23crdgval,obs23drdgval,obs24rdgval,obs25rdgval,
	obs26rdgval,obs26ardgval,obs26brdgval,obs26crdgval,obs27rdgval;
	String obs23rdgtxt="",obs23ardgtxt="",obs23brdgtxt="",obs23crdgtxt="",obs23drdgtxt="",obs24rdgtxt="",
			obs25rdgtxt="",obs26rdgtxt="",obs26ardgtxt="",obs26brdgtxt="",obs26crdgtxt="",obs27rdgtxt="";
	
	@Override
	   public void onCreate(Bundle savedInstanceState) {

	        super.onCreate(savedInstanceState);
	        cf=new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);
				
		 	}
	        setContentView(R.layout.sinkobservation4);
	        cf.getDeviceDimensions();
	        
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
			hdr_layout.addView(new HdrOnclickListener(this,1,"Sinkhole Inspection","Observations(23 - 27)",5,0,cf));
	        LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
	        mainmenu_layout.setMinimumWidth(cf.wd);
	        mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 5, 0,0,cf));
	        LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 55, 1,0,cf));
			TableRow tblrw = (TableRow)findViewById(R.id.row2);
			tblrw.setMinimumHeight(cf.ht);
			cf.CreateARRTable(55);
		    declaration();
		    SinkObs4_setvalue();
	}
	private void SinkObs4_setvalue() {

		// TODO Auto-generated method stub
		try
		{
		   Cursor Obs4_retrive=cf.SelectTablefunction(cf.SINK_Obs4tbl, " where fld_srid='"+cf.selectedhomeid+"'");
		   if(Obs4_retrive.getCount()>0)
		   {  
			   Obs4_retrive.moveToFirst();
			   ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
			    obs23rdgtxt=Obs4_retrive.getString(Obs4_retrive.getColumnIndex("CrawlSpacePresent"));
			   ((RadioButton) obs23rdgval.findViewWithTag(obs23rdgtxt)).setChecked(true);
     		   ((EditText)findViewById(R.id.obs23comment)).setText(cf.decode(Obs4_retrive.getString(Obs4_retrive.getColumnIndex("CrawlSpaceComments"))));
     		   
     		  obs23ardgtxt=Obs4_retrive.getString(Obs4_retrive.getColumnIndex("StandingWaterNoted"));
			   ((RadioButton) obs23ardgval.findViewWithTag(obs23ardgtxt)).setChecked(true);
    		   ((EditText)findViewById(R.id.obs23acomment)).setText(cf.decode(Obs4_retrive.getString(Obs4_retrive.getColumnIndex("StandingWaterComments"))));
    		   
    		   obs23brdgtxt=Obs4_retrive.getString(Obs4_retrive.getColumnIndex("LeakingPlumbNoted"));
			   ((RadioButton) obs23brdgval.findViewWithTag(obs23brdgtxt)).setChecked(true);
     		   ((EditText)findViewById(R.id.obs23bcomment)).setText(cf.decode(Obs4_retrive.getString(Obs4_retrive.getColumnIndex("LeakingPlumbComments"))));
     		   
     		  obs23crdgtxt=Obs4_retrive.getString(Obs4_retrive.getColumnIndex("VentilationNoted"));
			   ((RadioButton) obs23crdgval.findViewWithTag(obs23crdgtxt)).setChecked(true);
    		   ((EditText)findViewById(R.id.obs23ccomment)).setText(cf.decode(Obs4_retrive.getString(Obs4_retrive.getColumnIndex("VentilationComments"))));
    		   
    		   obs23drdgtxt=Obs4_retrive.getString(Obs4_retrive.getColumnIndex("CrawlSpaceAccessible"));
			   ((RadioButton) obs23drdgval.findViewWithTag(obs23drdgtxt)).setChecked(true);
     		   ((EditText)findViewById(R.id.obs23dcomment)).setText(cf.decode(Obs4_retrive.getString(Obs4_retrive.getColumnIndex("CrawlSpaceAccessibleComments"))));
     		   
     		  obs24rdgtxt=Obs4_retrive.getString(Obs4_retrive.getColumnIndex("RoofSagNoted"));
			   ((RadioButton) obs24rdgval.findViewWithTag(obs24rdgtxt)).setChecked(true);
    		   ((EditText)findViewById(R.id.obs24comment)).setText(cf.decode(Obs4_retrive.getString(Obs4_retrive.getColumnIndex("RoofSagComments"))));
    		   
    		   obs25rdgtxt=Obs4_retrive.getString(Obs4_retrive.getColumnIndex("RoofLeakageEvidence"));
			   ((RadioButton) obs25rdgval.findViewWithTag(obs25rdgtxt)).setChecked(true);
     		   ((EditText)findViewById(R.id.obs25comment)).setText(cf.decode(Obs4_retrive.getString(Obs4_retrive.getColumnIndex("RoofLeakageComments"))));
     		   
     		  obs26rdgtxt=Obs4_retrive.getString(Obs4_retrive.getColumnIndex("DeferredMaintanenceIssuesNoted"));
			   ((RadioButton) obs26rdgval.findViewWithTag(obs26rdgtxt)).setChecked(true);
    		   ((EditText)findViewById(R.id.obs26comment)).setText(cf.decode(Obs4_retrive.getString(Obs4_retrive.getColumnIndex("DeferredMaintanenceComments"))));
    		   
    		   obs26ardgtxt=Obs4_retrive.getString(Obs4_retrive.getColumnIndex("DeterioratedNoted"));
			   ((RadioButton) obs26ardgval.findViewWithTag(obs26ardgtxt)).setChecked(true);
    		   ((EditText)findViewById(R.id.obs26acomment)).setText(cf.decode(Obs4_retrive.getString(Obs4_retrive.getColumnIndex("DeterioratedComments"))));
    		   
    		   obs26brdgtxt=Obs4_retrive.getString(Obs4_retrive.getColumnIndex("DeterioratedExteriorFinishesNoted"));
			   ((RadioButton) obs26brdgval.findViewWithTag(obs26brdgtxt)).setChecked(true);
     		   ((EditText)findViewById(R.id.obs26bcomment)).setText(cf.decode(Obs4_retrive.getString(Obs4_retrive.getColumnIndex("DeterioratedExteriorFinishesComments"))));
     		   
     		  obs26crdgtxt=Obs4_retrive.getString(Obs4_retrive.getColumnIndex("DeterioratedExteriorSidingNoted"));
			   ((RadioButton) obs26crdgval.findViewWithTag(obs26crdgtxt)).setChecked(true);
    		   ((EditText)findViewById(R.id.obs26ccomment)).setText(cf.decode(Obs4_retrive.getString(Obs4_retrive.getColumnIndex("DeterioratedExteriorSidingComments"))));
    		   
    		   obs27rdgtxt=Obs4_retrive.getString(Obs4_retrive.getColumnIndex("SafetyNoted"));
			   ((RadioButton) obs27rdgval.findViewWithTag(obs27rdgtxt)).setChecked(true);
     		   ((EditText)findViewById(R.id.obs27comment)).setText(cf.decode(Obs4_retrive.getString(Obs4_retrive.getColumnIndex("SafetyComments"))));
     		  ((EditText)findViewById(R.id.addcomments)).setText(cf.decode(Obs4_retrive.getString(Obs4_retrive.getColumnIndex("OverallComments"))));
			 
		   }
		   else
		   {
			   ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.GONE);
		   }
		}
	    catch (Exception E)
		{
			String strerrorlog="Retrieving Observation4 - Sink";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
	private void declaration() {

		// TODO Auto-generated method stub
		((TextView)findViewById(R.id.txtaddednum)).setText(Html.fromHtml(cf.redcolor+" Addendum page: any other information/comments"));
		 ((EditText)findViewById(R.id.addcomments)).setOnTouchListener(new Touch_Listener(1));
    	 ((EditText)findViewById(R.id.addcomments)).addTextChangedListener(new textwatcher(1));
    	 
    	 ((EditText)findViewById(R.id.obs23comment)).setOnTouchListener(new Touch_Listener(2));
    	 ((EditText)findViewById(R.id.obs23comment)).addTextChangedListener(new textwatcher(2));
    	 
    	 ((EditText)findViewById(R.id.obs23acomment)).setOnTouchListener(new Touch_Listener(3));
    	 ((EditText)findViewById(R.id.obs23acomment)).addTextChangedListener(new textwatcher(3));
    	 
    	 ((EditText)findViewById(R.id.obs23bcomment)).setOnTouchListener(new Touch_Listener(4));
    	 ((EditText)findViewById(R.id.obs23bcomment)).addTextChangedListener(new textwatcher(4));
    	 
    	 ((EditText)findViewById(R.id.obs23ccomment)).setOnTouchListener(new Touch_Listener(5));
    	 ((EditText)findViewById(R.id.obs23ccomment)).addTextChangedListener(new textwatcher(5));
    	 
    	 ((EditText)findViewById(R.id.obs23dcomment)).setOnTouchListener(new Touch_Listener(6));
    	 ((EditText)findViewById(R.id.obs23dcomment)).addTextChangedListener(new textwatcher(6));
    	 
    	 ((EditText)findViewById(R.id.obs24comment)).setOnTouchListener(new Touch_Listener(7));
    	 ((EditText)findViewById(R.id.obs24comment)).addTextChangedListener(new textwatcher(7));
    	 
    	 ((EditText)findViewById(R.id.obs25comment)).setOnTouchListener(new Touch_Listener(8));
    	 ((EditText)findViewById(R.id.obs25comment)).addTextChangedListener(new textwatcher(8));
    	 
    	 ((EditText)findViewById(R.id.obs26comment)).setOnTouchListener(new Touch_Listener(9));
    	 ((EditText)findViewById(R.id.obs26comment)).addTextChangedListener(new textwatcher(9));
    	 
    	 ((EditText)findViewById(R.id.obs26acomment)).setOnTouchListener(new Touch_Listener(10));
    	 ((EditText)findViewById(R.id.obs26acomment)).addTextChangedListener(new textwatcher(10));
    	 
    	 ((EditText)findViewById(R.id.obs26bcomment)).setOnTouchListener(new Touch_Listener(11));
    	 ((EditText)findViewById(R.id.obs26bcomment)).addTextChangedListener(new textwatcher(11));
    	 
    	 ((EditText)findViewById(R.id.obs26ccomment)).setOnTouchListener(new Touch_Listener(12));
    	 ((EditText)findViewById(R.id.obs26ccomment)).addTextChangedListener(new textwatcher(12));
    	 
    	 ((EditText)findViewById(R.id.obs27comment)).setOnTouchListener(new Touch_Listener(13));
    	 ((EditText)findViewById(R.id.obs27comment)).addTextChangedListener(new textwatcher(13));
    	 
    	 obs23rdgval = (RadioGroup)findViewById(R.id.obs23_rdg);
    	 obs23rdgval.setOnCheckedChangeListener(new checklistenetr(obs23rdgval,1));
    	 obs23ardgval = (RadioGroup)findViewById(R.id.obs23a_rdg);
    	 obs23ardgval.setOnCheckedChangeListener(new checklistenetr(obs23ardgval,2));
    	 obs23brdgval = (RadioGroup)findViewById(R.id.obs23b_rdg);
    	 obs23brdgval.setOnCheckedChangeListener(new checklistenetr(obs23brdgval,3));
    	 obs23crdgval = (RadioGroup)findViewById(R.id.obs23c_rdg);
    	 obs23crdgval.setOnCheckedChangeListener(new checklistenetr(obs23crdgval,4));
    	 obs23drdgval = (RadioGroup)findViewById(R.id.obs23d_rdg);
    	 obs23drdgval.setOnCheckedChangeListener(new checklistenetr(obs23drdgval,5));
    	 obs24rdgval = (RadioGroup)findViewById(R.id.obs24_rdg);
    	 obs24rdgval.setOnCheckedChangeListener(new checklistenetr(obs24rdgval,6));
    	 obs25rdgval = (RadioGroup)findViewById(R.id.obs25_rdg);
    	 obs25rdgval.setOnCheckedChangeListener(new checklistenetr(obs25rdgval,7));
    	 obs26rdgval = (RadioGroup)findViewById(R.id.obs26_rdg);
    	 obs26rdgval.setOnCheckedChangeListener(new checklistenetr(obs26rdgval,8));
    	 obs26ardgval = (RadioGroup)findViewById(R.id.obs26a_rdg);
    	 obs26ardgval.setOnCheckedChangeListener(new checklistenetr(obs26ardgval,9));
    	 obs26brdgval = (RadioGroup)findViewById(R.id.obs26b_rdg);
    	 obs26brdgval.setOnCheckedChangeListener(new checklistenetr(obs26brdgval,10));
    	 obs26crdgval = (RadioGroup)findViewById(R.id.obs26c_rdg);
    	 obs26crdgval.setOnCheckedChangeListener(new checklistenetr(obs26crdgval,11));
    	 obs27rdgval = (RadioGroup)findViewById(R.id.obs27_rdg);
    	 obs27rdgval.setOnCheckedChangeListener(new checklistenetr(obs27rdgval,12));
	}
	class checklistenetr implements OnCheckedChangeListener
	{
    	RadioGroup aprdgval;
    	int i;
		public checklistenetr(RadioGroup aprdgval, int i) {
			// TODO Auto-generated constructor stub
			this.aprdgval=aprdgval;this.i=i;
		}

		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			   RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
		        // This puts the value (true/false) into the variable
		        boolean isChecked = checkedRadioButton.isChecked();
		        // If the radiobutton that has changed in check state is now checked...
		        if (isChecked)
		        {
		          switch (i) {
					case 1:
						obs23rdgtxt= checkedRadioButton.getText().toString().trim();
						if(obs23rdgtxt.equals("No"))
						{cf.hidekeyboard();
							((EditText)findViewById(R.id.obs23comment)).setText(R.string.novalueobs);
							((RadioButton) obs23ardgval.findViewWithTag("Not Applicable")).setChecked(true);
							((RadioButton) obs23brdgval.findViewWithTag("Not Applicable")).setChecked(true);
							((RadioButton) obs23crdgval.findViewWithTag("Not Applicable")).setChecked(true);
							((RadioButton) obs23drdgval.findViewWithTag("Not Applicable")).setChecked(true);
							((EditText)findViewById(R.id.obs23acomment)).setText(R.string.navalueobs);
							((EditText)findViewById(R.id.obs23bcomment)).setText(R.string.navalueobs);
							((EditText)findViewById(R.id.obs23ccomment)).setText(R.string.navalueobs);
							((EditText)findViewById(R.id.obs23dcomment)).setText(R.string.navalueobs);
						}
						else
						{cf.hidekeyboard();
							((EditText)findViewById(R.id.obs23comment)).setText("");
						}
							
			        	break;
					case 2:
						obs23ardgtxt= checkedRadioButton.getText().toString().trim();
						if(obs23ardgtxt.equals("No"))
						{cf.hidekeyboard();
							((EditText)findViewById(R.id.obs23acomment)).setText(R.string.novalueobs);
						}
						else if(obs23ardgtxt.equals("Not Applicable"))
						{cf.hidekeyboard();
							((EditText)findViewById(R.id.obs23acomment)).setText(R.string.navalueobs);
						}
						else
						{cf.hidekeyboard();
							((EditText)findViewById(R.id.obs23acomment)).setText("");
						}
						break;
					case 3:
						obs23brdgtxt= checkedRadioButton.getText().toString().trim();
						if(obs23brdgtxt.equals("No"))
						{cf.hidekeyboard();
							((EditText)findViewById(R.id.obs23bcomment)).setText(R.string.novalueobs);
						}
						else if(obs23brdgtxt.equals("Not Applicable"))
						{cf.hidekeyboard();
							((EditText)findViewById(R.id.obs23bcomment)).setText(R.string.navalueobs);
						}
						else
						{cf.hidekeyboard();
							((EditText)findViewById(R.id.obs23bcomment)).setText("");
						}
						break;
					case 4:
						obs23crdgtxt= checkedRadioButton.getText().toString().trim();
						if(obs23crdgtxt.equals("No"))
						{cf.hidekeyboard();
							((EditText)findViewById(R.id.obs23ccomment)).setText(R.string.novalueobs);
						}
						else if(obs23crdgtxt.equals("Not Applicable"))
						{cf.hidekeyboard();
							((EditText)findViewById(R.id.obs23ccomment)).setText(R.string.navalueobs);
						}
						else
						{cf.hidekeyboard();
							((EditText)findViewById(R.id.obs23ccomment)).setText("");
						}
						break;
					case 5:
						obs23drdgtxt= checkedRadioButton.getText().toString().trim();
						if(obs23drdgtxt.equals("No"))
						{cf.hidekeyboard();
							((EditText)findViewById(R.id.obs23dcomment)).setText(R.string.novalueobs);
						}
						else if(obs23drdgtxt.equals("Not Applicable"))
						{cf.hidekeyboard();
							((EditText)findViewById(R.id.obs23dcomment)).setText(R.string.navalueobs);
						}
						else
						{cf.hidekeyboard();
							((EditText)findViewById(R.id.obs23dcomment)).setText("");
						}
						break;
					case 6:
						obs24rdgtxt= checkedRadioButton.getText().toString().trim();
						if(obs24rdgtxt.equals("No"))
						{cf.hidekeyboard();
						 ((EditText)findViewById(R.id.obs24comment)).setText(R.string.novalueobs);
						}
						else{cf.hidekeyboard();
						((EditText)findViewById(R.id.obs24comment)).setText("");
						}
					break;
					case 7:
						obs25rdgtxt= checkedRadioButton.getText().toString().trim();
						if(obs25rdgtxt.equals("No")){cf.hidekeyboard();
						 ((EditText)findViewById(R.id.obs25comment)).setText(R.string.novalueobs);
						}else{cf.hidekeyboard();
						((EditText)findViewById(R.id.obs25comment)).setText("");}
					break;
					case 8:
						obs26rdgtxt= checkedRadioButton.getText().toString().trim();
						if(obs26rdgtxt.equals("Not Applicable"))
						{cf.hidekeyboard();
							((EditText)findViewById(R.id.obs26comment)).setText(R.string.naobs26);
							((RadioButton) obs26ardgval.findViewWithTag("Not Applicable")).setChecked(true);
							((RadioButton) obs26brdgval.findViewWithTag("Not Applicable")).setChecked(true);
							((RadioButton) obs26crdgval.findViewWithTag("Not Applicable")).setChecked(true);
							((EditText)findViewById(R.id.obs26acomment)).setText(R.string.naobs26);
							((EditText)findViewById(R.id.obs26bcomment)).setText(R.string.naobs26);
							((EditText)findViewById(R.id.obs26ccomment)).setText(R.string.naobs26);
						}
						else if(obs26rdgtxt.equals("No"))
						{cf.hidekeyboard();
							((EditText)findViewById(R.id.obs26comment)).setText(R.string.novalueobs);
						}
						else
						{cf.hidekeyboard();
							((EditText)findViewById(R.id.obs26comment)).setText("");
						}
						break;
					case 9:
						obs26ardgtxt= checkedRadioButton.getText().toString().trim();
						if(obs26ardgtxt.equals("No")){cf.hidekeyboard();
						  ((EditText)findViewById(R.id.obs26acomment)).setText(R.string.novalueobs);
						}else{cf.hidekeyboard();
						  ((EditText)findViewById(R.id.obs26acomment)).setText("");}
						break;
					case 10:
						obs26brdgtxt= checkedRadioButton.getText().toString().trim();
						if(obs26brdgtxt.equals("No")){cf.hidekeyboard();
						  ((EditText)findViewById(R.id.obs26bcomment)).setText(R.string.novalueobs);
						}else{cf.hidekeyboard();
						  ((EditText)findViewById(R.id.obs26bcomment)).setText("");}
						break;
					case 11:
						obs26crdgtxt= checkedRadioButton.getText().toString().trim();
						if(obs26crdgtxt.equals("No")){cf.hidekeyboard();
						  ((EditText)findViewById(R.id.obs26ccomment)).setText(R.string.novalueobs);}
						else{cf.hidekeyboard();
						  ((EditText)findViewById(R.id.obs26ccomment)).setText("");}
						break;
					case 12:
						obs27rdgtxt= checkedRadioButton.getText().toString().trim();
						if(obs27rdgtxt.equals("No")){cf.hidekeyboard();
						  ((EditText)findViewById(R.id.obs27comment)).setText(R.string.novalueobs);
						}else if(obs27rdgtxt.equals("Not Applicable")){cf.hidekeyboard();
							  ((EditText)findViewById(R.id.obs27comment)).setText(R.string.naobs26);
						}else{cf.hidekeyboard();
						  ((EditText)findViewById(R.id.obs27comment)).setText("");}
						break;
					default:
						break;
					}
		        }
		}
	}
	class Touch_Listener implements OnTouchListener

    {
    	   public int type;
    	   Touch_Listener(int type)
    		{
    			this.type=type;
    			
    		}
    	    @Override
    		public boolean onTouch(View v, MotionEvent event) {

    			// TODO Auto-generated method stub
    	    	((EditText) findViewById(v.getId())).setFocusableInTouchMode(true);
		    	((EditText) findViewById(v.getId())).requestFocus();
    			return false;
    	    }
    }
    class textwatcher implements TextWatcher


    {
         public int type;
         textwatcher(int type)
    	{
    		this.type=type;
    	}
    	@Override
    	public void afterTextChanged(Editable s) {


    		// TODO Auto-generated method stub
    		if(this.type==1)
    		{
    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.add_tv_type),500); 
    			if(s.toString().startsWith(" "))
    			{
    		    	 ((EditText)findViewById(R.id.addcomments)).setText("");
    			}
    		}
    		else if(this.type==2)
    		{
    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs23_tv_type1),250);
    			if(s.toString().startsWith(" "))
    			{
    		    	 ((EditText)findViewById(R.id.obs23comment)).setText("");
    			}
    		}
    		else if(this.type==3)
    		{
    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs23a_tv_type1),250); 
    			if(s.toString().startsWith(" "))
    			{
    		    	 ((EditText)findViewById(R.id.obs23acomment)).setText("");
    			}
    		}
    		else if(this.type==4)
    		{
    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs23b_tv_type1),250); 
    			if(s.toString().startsWith(" "))
    			{
    		    	 ((EditText)findViewById(R.id.obs23bcomment)).setText("");
    			}
    		}
    		else if(this.type==5)
    		{
    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs23c_tv_type1),250); 
    			if(s.toString().startsWith(" "))
    			{
    		    	 ((EditText)findViewById(R.id.obs23ccomment)).setText("");
    			}
    		}
    		else if(this.type==6)
    		{
    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs23d_tv_type1),250); 
    			if(s.toString().startsWith(" "))
    			{
    		    	 ((EditText)findViewById(R.id.obs23dcomment)).setText("");
    			}
    		}
    		else if(this.type==7)
    		{
    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs24_tv_type1),250);
    			if(s.toString().startsWith(" "))
    			{
    		    	 ((EditText)findViewById(R.id.obs24comment)).setText("");
    			}
    		}
    		else if(this.type==8)
    		{
    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs25_tv_type1),250); 
    			if(s.toString().startsWith(" "))
    			{
    		    	 ((EditText)findViewById(R.id.obs25comment)).setText("");
    			}
    		}
    		else if(this.type==9)
    		{
    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs26_tv_type1),250); 
    			if(s.toString().startsWith(" "))
    			{
    		    	 ((EditText)findViewById(R.id.obs26comment)).setText("");
    			}
    		}
    		else if(this.type==10)
    		{
    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs26a_tv_type1),250); 
    			if(s.toString().startsWith(" "))
    			{
    		    	 ((EditText)findViewById(R.id.obs26acomment)).setText("");
    			}
    		}
    		else if(this.type==11)
    		{
    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs26b_tv_type1),250);
    			if(s.toString().startsWith(" "))
    			{
    		    	 ((EditText)findViewById(R.id.obs26bcomment)).setText("");
    			}
    		}
    		else if(this.type==12)
    		{
    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs26c_tv_type1),250);
    			if(s.toString().startsWith(" "))
    			{
    		    	 ((EditText)findViewById(R.id.obs26ccomment)).setText("");
    			}
    		}
    		else if(this.type==13)
    		{
    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs27_tv_type1),250);
    			if(s.toString().startsWith(" "))
    			{
    		    	 ((EditText)findViewById(R.id.obs27comment)).setText("");
    			}
    		}
    		
    	}

    	@Override
    	public void beforeTextChanged(CharSequence s, int start, int count,

    			int after) {
    		// TODO Auto-generated method stub
     	}

    	@Override
    	public void onTextChanged(CharSequence s, int start, int before,

    			int count) {
    		// TODO Auto-generated method stub
    	}
   }	
    public void clicker(View v)

    {
    	switch(v.getId())
    	{
    	case R.id.hme:
    		cf.gohome();
    		break;
    	case R.id.save:
    		if(obs23rdgtxt.equals(""))
			{
				cf.ShowToast("Please select the option for Question No.23.", 0);
			}
    		else
    		{
    			if(((EditText)findViewById(R.id.obs23comment)).getText().toString().trim().equals(""))
    			{
    				cf.ShowToast("Please enter the comments for Question No.23.", 0);
    				cf.setFocus(((EditText)findViewById(R.id.obs23comment)));
    			}
    			else
    			{
    				if(obs23ardgtxt.equals(""))/* CHECK QUESTION NO.23 a RADIO BUTON*/
    				{
    					cf.ShowToast("Please select the option for Question No.23(a).", 0);
    					
    				}
    				else
    				{
    					if(((EditText)findViewById(R.id.obs23acomment)).getText().toString().trim().equals(""))/* CHECK COMMENTS QUESTION NO.23 (a)*/
    					{
    						cf.ShowToast("Please enter the comments for Question No.23(a).", 0);
    						cf.setFocus(((EditText)findViewById(R.id.obs23acomment)));
    					}
    					else
    					{
    						if(obs23brdgtxt.equals(""))/* CHECK QUESTION NO.23 b RADIO BUTON*/
    	    				{
    	    					cf.ShowToast("Please select the option for Question No.23(b).", 0);
    	    					
    	    				}
    	    				else
    	    				{
    	    					if(((EditText)findViewById(R.id.obs23bcomment)).getText().toString().trim().equals(""))/* CHECK COMMENTS QUESTION NO.23 (b)*/
    	    					{
    	    						cf.ShowToast("Please enter the comments for Question No.23(b).", 0);
    	    						cf.setFocus(((EditText)findViewById(R.id.obs23bcomment)));
    	    					}
    	    					else
    	    					{
    	    						if(obs23crdgtxt.equals(""))/* CHECK QUESTION NO.23 c RADIO BUTON*/
    	    	    				{
    	    	    					cf.ShowToast("Please select the option for Question No.23(c).", 0);
    	    	    					
    	    	    				}
    	    	    				else
    	    	    				{
    	    	    					if(((EditText)findViewById(R.id.obs23ccomment)).getText().toString().trim().equals(""))/* CHECK COMMENTS QUESTION NO.23 (c)*/
    	    	    					{
    	    	    						cf.ShowToast("Please enter the comments for Question No.23(c).", 0);
    	    	    						cf.setFocus(((EditText)findViewById(R.id.obs23ccomment)));
    	    	    					}
    	    	    					else
    	    	    					{
    	    	    						if(obs23drdgtxt.equals(""))/* CHECK QUESTION NO.23 d RADIO BUTON*/
    	    	    	    				{
    	    	    	    					cf.ShowToast("Please select the option for Question No.23(d).", 0);
    	    	    	    					
    	    	    	    				}
    	    	    	    				else
    	    	    	    				{
    	    	    	    					if(((EditText)findViewById(R.id.obs23dcomment)).getText().toString().trim().equals(""))/* CHECK COMMENTS QUESTION NO.23 (d)*/
    	    	    	    					{
    	    	    	    						cf.ShowToast("Please enter the comments for Question No.23(d).", 0);
    	    	    	    						cf.setFocus(((EditText)findViewById(R.id.obs23dcomment)));
    	    	    	    					}
    	    	    	    					else
    	    	    	    					{
    	    	    	    						if(obs24rdgtxt.equals(""))/* CHECK QUESTION NO.24 RADIO BUTON*/
    	    	    	    	    				{
    	    	    	    	    					cf.ShowToast("Please select the option for Question No.24.", 0);
    	    	    	    	    					
    	    	    	    	    				}
    	    	    	    	    				else
    	    	    	    	    				{
    	    	    	    	    					if(((EditText)findViewById(R.id.obs24comment)).getText().toString().trim().equals(""))/* CHECK COMMENTS QUESTION NO.24*/
    	    	    	    	    					{
    	    	    	    	    						cf.ShowToast("Please enter the comments for Question No.24.", 0);
    	    	    	    	    						cf.setFocus(((EditText)findViewById(R.id.obs24comment)));
    	    	    	    	    					}
    	    	    	    	    					else
    	    	    	    	    					{
    	    	    	    	    						if(obs25rdgtxt.equals(""))/* CHECK QUESTION NO.25 RADIO BUTON*/
    	    	    	    	    	    				{
    	    	    	    	    	    					cf.ShowToast("Please select the option for Question No.25.", 0);
    	    	    	    	    	    					
    	    	    	    	    	    				}
    	    	    	    	    	    				else
    	    	    	    	    	    				{
    	    	    	    	    	    					if(((EditText)findViewById(R.id.obs25comment)).getText().toString().trim().equals(""))/* CHECK COMMENTS QUESTION NO.25*/
    	    	    	    	    	    					{
    	    	    	    	    	    						cf.ShowToast("Please enter the comments for Question No.25.", 0);
    	    	    	    	    	    						cf.setFocus(((EditText)findViewById(R.id.obs25comment)));
    	    	    	    	    	    					}
    	    	    	    	    	    					else
    	    	    	    	    	    					{
    	    	    	    	    	    						if(obs26rdgtxt.equals(""))/* CHECK QUESTION NO.26 RADIO BUTON*/
    	    	    	    	    	    	    				{
    	    	    	    	    	    	    					cf.ShowToast("Please select the option for Question No.26.", 0);
    	    	    	    	    	    	    					
    	    	    	    	    	    	    				}
    	    	    	    	    	    	    				else
    	    	    	    	    	    	    				{
    	    	    	    	    	    	    					if(((EditText)findViewById(R.id.obs26comment)).getText().toString().trim().equals(""))/* CHECK COMMENTS QUESTION NO.26*/
    	    	    	    	    	    	    					{
    	    	    	    	    	    	    						cf.ShowToast("Please enter the comments for Question No.26 Comments.", 0);
    	    	    	    	    	    	    						cf.setFocus(((EditText)findViewById(R.id.obs26comment)));
    	    	    	    	    	    	    					}
    	    	    	    	    	    	    					else
    	    	    	    	    	    	    					{
    	    	    	    	    	    	    						if(obs26ardgtxt.equals(""))/* CHECK QUESTION NO.26 a RADIO BUTON*/
    	    	    	    	    	    	    	    				{
    	    	    	    	    	    	    	    					cf.ShowToast("Please select the option for Question No.26(a).", 0);
    	    	    	    	    	    	    	    					
    	    	    	    	    	    	    	    				}
    	    	    	    	    	    	    	    				else
    	    	    	    	    	    	    	    				{
    	    	    	    	    	    	    	    					if(((EditText)findViewById(R.id.obs26acomment)).getText().toString().trim().equals(""))/* CHECK COMMENTS QUESTION NO.26 (a)*/
    	    	    	    	    	    	    	    					{
    	    	    	    	    	    	    	    						cf.ShowToast("Please enter the comments for Question No.26(a).", 0);
    	    	    	    	    	    	    	    						cf.setFocus(((EditText)findViewById(R.id.obs26acomment)));
    	    	    	    	    	    	    	    					}
    	    	    	    	    	    	    	    					else
    	    	    	    	    	    	    	    					{
    	    	    	    	    	    	    	    						if(obs26brdgtxt.equals(""))/* CHECK QUESTION NO.26 b RADIO BUTON*/
    	    	    	    	    	    	    	    	    				{
    	    	    	    	    	    	    	    	    					cf.ShowToast("Please select the option for Question No.26(b).", 0);
    	    	    	    	    	    	    	    	    					
    	    	    	    	    	    	    	    	    				}
    	    	    	    	    	    	    	    	    				else
    	    	    	    	    	    	    	    	    				{
    	    	    	    	    	    	    	    	    					if(((EditText)findViewById(R.id.obs26bcomment)).getText().toString().trim().equals(""))/* CHECK COMMENTS QUESTION NO.26 (b)*/
    	    	    	    	    	    	    	    	    					{
    	    	    	    	    	    	    	    	    						cf.ShowToast("Please enter the comments for Question No.26(b).", 0);
    	    	    	    	    	    	    	    	    						cf.setFocus(((EditText)findViewById(R.id.obs26bcomment)));
    	    	    	    	    	    	    	    	    					}
    	    	    	    	    	    	    	    	    					else
    	    	    	    	    	    	    	    	    					{
    	    	    	    	    	    	    	    	    						if(obs26crdgtxt.equals(""))/* CHECK QUESTION NO.26 c RADIO BUTON*/
    	    	    	    	    	    	    	    	    	    				{
    	    	    	    	    	    	    	    	    	    					cf.ShowToast("Please select the option for Question No.26(c).", 0);
    	    	    	    	    	    	    	    	    	    					
    	    	    	    	    	    	    	    	    	    				}
    	    	    	    	    	    	    	    	    	    				else
    	    	    	    	    	    	    	    	    	    				{
    	    	    	    	    	    	    	    	    	    					if(((EditText)findViewById(R.id.obs26ccomment)).getText().toString().trim().equals(""))/* CHECK COMMENTS QUESTION NO.26 (c)*/
    	    	    	    	    	    	    	    	    	    					{
    	    	    	    	    	    	    	    	    	    						cf.ShowToast("Please enter the comments for Question No.26(c).", 0);
    	    	    	    	    	    	    	    	    	    						cf.setFocus(((EditText)findViewById(R.id.obs26ccomment)));
    	    	    	    	    	    	    	    	    	    					}
    	    	    	    	    	    	    	    	    	    					else
    	    	    	    	    	    	    	    	    	    					{
    	    	    	    	    	    	    	    	    	    						if(obs27rdgtxt.equals(""))/* CHECK QUESTION NO.27 RADIO BUTON*/
    	    	    	    	    	    	    	    	    	    	    				{
    	    	    	    	    	    	    	    	    	    	    					cf.ShowToast("Please select the option for Question No.27.", 0);
    	    	    	    	    	    	    	    	    	    	    					
    	    	    	    	    	    	    	    	    	    	    				}
    	    	    	    	    	    	    	    	    	    	    				else
    	    	    	    	    	    	    	    	    	    	    				{
    	    	    	    	    	    	    	    	    	    	    					if(((EditText)findViewById(R.id.obs27comment)).getText().toString().trim().equals(""))/* CHECK COMMENTS QUESTION NO.27*/
    	    	    	    	    	    	    	    	    	    	    					{
    	    	    	    	    	    	    	    	    	    	    						cf.ShowToast("Please enter the comments for Question No.27.", 0);
    	    	    	    	    	    	    	    	    	    	    						cf.setFocus(((EditText)findViewById(R.id.obs27comment)));
    	    	    	    	    	    	    	    	    	    	    					}
    	    	    	    	    	    	    	    	    	    	    					else
    	    	    	    	    	    	    	    	    	    	    					{
    	    	    	    	    	    	    	    	    	    	    						   if(((EditText)findViewById(R.id.addcomments)).getText().toString().trim().equals(""))/* CHECK ADDEDNUM COMMENTs*/
    	    	    	    	    	    	    	    	    	    	    	    					{
    	    	    	    	    	    	    	    	    	    	    	    						cf.ShowToast("Please enter the Addendum Comments.", 0);
    	    	    	    	    	    	    	    	    	    	    	    						cf.setFocus(((EditText)findViewById(R.id.addcomments)));
    	    	    	    	    	    	    	    	    	    	    	    					}
    	    	    	    	    	    	    	    	    	    	    	    					else
    	    	    	    	    	    	    	    	    	    	    	    					{
    	    	    	    	    	    	    	    	    	    	    	    						obs4_Insert();
    	    	    	    	    	    	    	    	    	    	    	    					}
    	    	    	    	    	    	    	    	    	    	    					
    	    	    	    	    	    	    	    	    	    	    					}
    	    	    	    	    	    	    	    	    	    	    				}
    	    	    	    	    	    	    	    	    	    					}
    	    	    	    	    	    	    	    	    	    				}
    	    	    	    	    	    	    	    	    					}
    	    	    	    	    	    	    	    	    				}
    	    	    	    	    	    	    	    					}
    	    	    	    	    	    	    	    				}
    	    	    	    	    	    	    					}
    	    	    	    	    	    	    				}
    	    	    	    	    	    					}
    	    	    	    	    	    				}
    	    	    	    	    					}
    	    	    	    	    				}
    	    	    	    					}
    	    	    	    				}
    	    	    					}
    	    	    				}
    	    					}
    	    				}
    					}
    				}
    			}
    		}
    	break;
    	 case R.id.addendum_help: /*HELP FOR ADDEDNUM */
			  cf.alerttitle="HELP";
			  cf.alertcontent="<p>Inspectors should use the " +
			  		"Comment Box to clearly outline any other conditions or issues relating " +
			  		"to the inspection at hand.</p><p>If inspectors are unable to properly " +
			  		"document the findings of any of the questions listed below and the " +
			  		"associated comment boxes, then inspectors must use the comment box below " +
			  		"to conclude the finding noted above.</p><p>If the comment box below is " +
			  		"used to amplify conditions noted above, inspectors should clearly start " +
			  		"the additional comments with the question number and question and state " +
			  		"if this comment refers to the relevant question above and refer to " +
			  		"relevant pictures and comments/disclosure for the relevant " +
			  		"question.</p>";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
    	 case R.id.obs23_help: /*HELP FOR OBSERVATION23 */
			  cf.alerttitle="HELP";
			  cf.alertcontent="<p>Crawl space areas can be " +
			  		"host to many issues including leaking drainage systems, termite and " +
			  		"moisture damage to the sub-floor structure and settlement or washout to " +
			  		"the ground area underneath.  Many times homeowners conduct unauthorized " +
			  		"repairs or amateur repairs to the building structures and support, all " +
			  		"of which could cause localized settlement/movement.</p><p>Not properly " +
			  		"ventilated, the structure itself may have suffered as a result of " +
			  		"moisture - vapor build up/decay underneath.  Inspectors are " +
			  		"required to record the general condition of the underside of the surface " +
			  		"of the crawlspace if one is present as it relates to potential " +
			  		"settlement or sinkholes.</p><p>On older homes it can almost be assumed " +
			  		"that decay and rot of some nature from previous or ongoing leakage can " +
			  		"be found around the water fixtures from bathrooms, kitchens, water " +
			  		"heaters, etc.  within the crawlspace</p><p>The internal inspection " +
			  		"should determine what areas of uneven flooring -  localized settlement.  " +
			  		"If areas of abnormal settlement are noted initially, these need to be " +
			  		"accessed and inspected.</p>";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
		  case R.id.obs23a_help: /*HELP FOR OBSERVATION23-OPTA */
			  cf.alerttitle="HELP";
			  cf.alertcontent="<p>Standing water within " +
			  		"crawlspaces under homes can cause a multitude of problems including but " +
			  		"not limited to rot, decay, mold, health issues, to mention a " +
			  		"view.</p><p>Standing water can, in addition cause washout or settlement " +
			  		"- subsidence to the main structure, and this is the core purpose " +
			  		"of our inspection.</p><p>If standing water is noted, inspectors must " +
			  		"identify the area(s) concerned and recommend additional evaluation of " +
			  		"the structure.  Any limitations of the inspection of the crawlspace area " +
			  		"should be recorded.</p><p>Within the inspection report, inspectors must " +
			  		"recommend that the standing water be reviewed and evaluated and the area " +
			  		"remediated to prevent standing water and potential water issues.</p>";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
		  case R.id.obs23b_help: /*HELP FOR OBSERVATION23-OPTB */
			  cf.alerttitle="HELP";
			  cf.alertcontent="<p>During the course of the " +
			  		"inspection, Inspectors should attempt to identify any plumbing leakages " +
			  		"visible from the exterior perimeter for areas inspected, and that a full " +
			  		"detailed plumbing inspection is beyond the scope of this " +
			  		"inspection.</p><p>Any issues noted should be photographed and recorded " +
			  		"as part of the inspection service.</p>";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
		  case R.id.obs23c_help: /*HELP FOR OBSERVATION23-OPTC */
			  cf.alerttitle="HELP";
			  cf.alertcontent="<p>An inadequately " +
			  		"ventilated crawlspace can cause potential rot and decay to the main " +
			  		"floor structure and unevenness to the floor as a result of floor " +
			  		"structure failure.  This is particularly prevalent to areas where the " +
			  		"undercarriage is exposed with limited ventilation and the water table is " +
			  		"high or standing water was present within the crawlspace " +
			  		"area.</p><p>Proper ventilation is crucial for the life cycle of the " +
			  		"building in question.</p><p>Any issues associated with ventilation or " +
			  		"lack thereof should be recorded and photographed accordingly.</p>";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
		  case R.id.obs23d_help: /*HELP FOR OBSERVATION23-OPTD */
			  cf.alerttitle="HELP";
			  cf.alertcontent="<p>Inspectors are required " +
			  		"to examine any home where a crawlspace is present from the underside of " +
			  		"the home if accessible.  If inaccessible, this should be outlined within " +
			  		"the report and photographed in detail accordingly.</p><p>Deferred " +
			  		"Maintenance should be marked \"Not Applicable\" unless " +
			  		"otherwise instructed by client</p><p>If maintenance or hazardous issues " +
			  		"are required to be collected at the time of the inspection, then " +
			  		"inspector should clearly outline and comment with ample photography any " +
			  		"issues found associated with these sections of the inspection " +
			  		"report</p>";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
		  case R.id.obs24_help: /*HELP FOR OBSERVATION24 */
			  cf.alerttitle="HELP";
			  cf.alertcontent="<p>Every attic area must be " +
			  		"inspected as part of this inspection process.</p><p>Attic inspections " +
			  		"are even more important where settlement issues are noted and inspectors " +
			  		"are trying to determine the load bearing walls versus non-load-bearing " +
			  		"walls for the purposes of those settlement issues being " +
			  		"observed.</p><p>The exterior inspection will determine abnormal " +
			  		"settlement issues with regard to the roof lines insofar as settlement or " +
			  		"roof sags, however, issues with roof lines from a perspective of roof " +
			  		"settlement or sagging is more often associated with poor construction " +
			  		"techniques or alterations conducted by the home owner or an unlicensed " +
			  		"contractor.</p><p>Where roof sags or settlement is noted, these should " +
			  		"be properly identified in the comment area and photographs in addition " +
			  		"to the attic area.</p><p>Roof sagging or deflection as a result of poor " +
			  		"construction practices is not associated with sinkhole or structural " +
			  		"activity, but if noted, should be outlined on the report in addition to " +
			  		"the probable cause.</p><p>Deferred Maintenance should be marked \"Not Applicable\" unless otherwise instructed by " +
	  				"client.</p><p>If maintenance or hazardous issues are required to be " +
	  				"collected at the time of the inspection, then inspector should clearly " +
	  				"outline and comment with ample photography any issues found associated " +
	  				"with these sections of the inspection report.</p>";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
		  case R.id.obs25_help: /*HELP FOR OBSERVATION25 */
			  cf.alerttitle="HELP";
			  cf.alertcontent="<p>Roof leakage damage as it " +
			  		"relates to this inspection is associated with any roof sagging or " +
			  		"surface unevenness that may be associated with settlement or sinkhole " +
			  		"issues.  Any sagging or surface issues - unevenness must be " +
			  		"reported and analyzed during the attic and exterior inspections and " +
			  		"reported and recorded accordingly to eliminate any connection with " +
			  		"sinkhole or settlement issues.</p><p>Deferred Maintenance should be " +
			  		"marked \"Not Applicable\" unless otherwise instructed by " +
			  		"client.</p>";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
		  case R.id.obs26_help: /*HELP FOR OBSERVATION26 */
			  cf.alerttitle="HELP";
			  cf.alertcontent="<p>Deferred Maintenance " +
			  		"should be marked \"Not Applicable\" unless otherwise " +
			  		"instructed by client.</p><p>If maintenance or hazardous issues are " +
			  		"required to be collected at the time of the inspection, then inspector " +
			  		"should clearly outline and comment with ample photography any issues " +
			  		"found associated with these sections of the inspection report.</p>";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
		  case R.id.shwobs23:
			  hidelayouts();
			  ((ImageView)findViewById(R.id.shwobs23)).setVisibility(cf.v1.GONE);
			  ((ImageView)findViewById(R.id.hdobs23)).setVisibility(cf.v1.VISIBLE);
			  ((LinearLayout)findViewById(R.id.obs23lin)).setVisibility(cf.v1.VISIBLE);
			  ((TextView)findViewById(R.id.fsttxt)).requestFocus();
			  break;
		  case R.id.hdobs23:
				((ImageView)findViewById(R.id.shwobs23)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdobs23)).setVisibility(cf.v1.GONE);
				((LinearLayout)findViewById(R.id.obs23lin)).setVisibility(cf.v1.GONE);
				break;
		  case R.id.shwobs24:
			  hidelayouts();
			  ((ImageView)findViewById(R.id.shwobs24)).setVisibility(cf.v1.GONE);
			  ((ImageView)findViewById(R.id.hdobs24)).setVisibility(cf.v1.VISIBLE);
			  ((LinearLayout)findViewById(R.id.obs24lin)).setVisibility(cf.v1.VISIBLE);
			  ((TextView)findViewById(R.id.sectxt)).requestFocus();
			  break;
		  case R.id.hdobs24:
				((ImageView)findViewById(R.id.shwobs24)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdobs24)).setVisibility(cf.v1.GONE);
				((LinearLayout)findViewById(R.id.obs24lin)).setVisibility(cf.v1.GONE);
				break;
		  case R.id.shwobs25:
			  hidelayouts();
			  ((ImageView)findViewById(R.id.shwobs25)).setVisibility(cf.v1.GONE);
			  ((ImageView)findViewById(R.id.hdobs25)).setVisibility(cf.v1.VISIBLE);
			  ((LinearLayout)findViewById(R.id.obs25lin)).setVisibility(cf.v1.VISIBLE);
			  ((TextView)findViewById(R.id.thdtxt)).requestFocus();
			  break;
		  case R.id.hdobs25:
				((ImageView)findViewById(R.id.shwobs25)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdobs25)).setVisibility(cf.v1.GONE);
				((LinearLayout)findViewById(R.id.obs25lin)).setVisibility(cf.v1.GONE);
				break;
		  case R.id.shwobs26:
			  hidelayouts();
			  ((ImageView)findViewById(R.id.shwobs26)).setVisibility(cf.v1.GONE);
			  ((ImageView)findViewById(R.id.hdobs26)).setVisibility(cf.v1.VISIBLE);
			  ((LinearLayout)findViewById(R.id.obs26lin)).setVisibility(cf.v1.VISIBLE);
			  ((TextView)findViewById(R.id.fourtxt)).requestFocus();
			  break;
		  case R.id.hdobs26:
				((ImageView)findViewById(R.id.shwobs26)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdobs26)).setVisibility(cf.v1.GONE);
				((LinearLayout)findViewById(R.id.obs26lin)).setVisibility(cf.v1.GONE);
				break;
		  case R.id.shwobs27:
			  hidelayouts();
			  ((ImageView)findViewById(R.id.shwobs27)).setVisibility(cf.v1.GONE);
			  ((ImageView)findViewById(R.id.hdobs27)).setVisibility(cf.v1.VISIBLE);
			  ((LinearLayout)findViewById(R.id.obs27lin)).setVisibility(cf.v1.VISIBLE);
			  ((TextView)findViewById(R.id.fivtxt)).requestFocus();
			  break;
		  case R.id.hdobs27:
				((ImageView)findViewById(R.id.shwobs27)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdobs27)).setVisibility(cf.v1.GONE);
				((LinearLayout)findViewById(R.id.obs27lin)).setVisibility(cf.v1.GONE);
				break;
    	}
    	}
    private void obs4_Insert() {
		// TODO Auto-generated method stub
    	Cursor OBS4_save=null;
		try
		{
			OBS4_save=cf.SelectTablefunction(cf.SINK_Obs4tbl, " where fld_srid='"+cf.selectedhomeid+"'");
			if(OBS4_save.getCount()>0)
			{
				try
				{
					cf.arr_db.execSQL("UPDATE "+cf.SINK_Obs4tbl+ " set CrawlSpacePresent='"+obs23rdgtxt+"',CrawlSpaceComments='"+cf.encode(((EditText)findViewById(R.id.obs23comment)).getText().toString())+"',"+
				                                              "StandingWaterNoted='"+obs23ardgtxt+"',StandingWaterComments='"+cf.encode(((EditText)findViewById(R.id.obs23acomment)).getText().toString())+"',"+
							                                  "LeakingPlumbNoted='"+obs23brdgtxt+"',LeakingPlumbComments='"+cf.encode(((EditText)findViewById(R.id.obs23bcomment)).getText().toString())+"',"+
				                                              "VentilationNoted='"+obs23crdgtxt+"',VentilationComments='"+cf.encode(((EditText)findViewById(R.id.obs23ccomment)).getText().toString())+"',"+
							                                  "CrawlSpaceAccessible='"+obs23drdgtxt+"',CrawlSpaceAccessibleComments='"+cf.encode(((EditText)findViewById(R.id.obs23dcomment)).getText().toString())+"',"+
				                                              "RoofSagNoted='"+obs24rdgtxt+"',RoofSagComments='"+cf.encode(((EditText)findViewById(R.id.obs24comment)).getText().toString())+"',"+
							                                  "RoofLeakageEvidence='"+obs25rdgtxt+"',RoofLeakageComments='"+cf.encode(((EditText)findViewById(R.id.obs25comment)).getText().toString())+"',"+
				                                              "DeferredMaintanenceIssuesNoted='"+obs26ardgtxt+"',DeferredMaintanenceComments='"+cf.encode(((EditText)findViewById(R.id.obs26comment)).getText().toString())+"',"+
							                                  "DeterioratedNoted='"+obs26ardgtxt+"',DeterioratedComments='"+cf.encode(((EditText)findViewById(R.id.obs26acomment)).getText().toString())+"',"+
							                                  "DeterioratedExteriorFinishesNoted='"+obs26brdgtxt+"',DeterioratedExteriorFinishesComments='"+cf.encode(((EditText)findViewById(R.id.obs26bcomment)).getText().toString())+"',"+
							                                  "DeterioratedExteriorSidingNoted='"+obs26crdgtxt+"',DeterioratedExteriorSidingComments='"+cf.encode(((EditText)findViewById(R.id.obs26ccomment)).getText().toString())+"',"+
							                                  "SafetyNoted='"+obs27rdgtxt+"',SafetyComments='"+cf.encode(((EditText)findViewById(R.id.obs27comment)).getText().toString())+"',"+
							                                  "OverallComments='"+cf.encode(((EditText)findViewById(R.id.addcomments)).getText().toString())+"' where fld_srid='"+cf.selectedhomeid+"'");
					cf.ShowToast("Observations(23-27) updated successfully.", 0);
				    ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
				    
				}
				catch (Exception E)
				{
					String strerrorlog="Updating the Observation4 - Sinkhole";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
			}
			else
			{
				try
				{
					cf.arr_db.execSQL("INSERT INTO "
							+ cf.SINK_Obs4tbl
							+ " (fld_srid,CrawlSpacePresent,CrawlSpaceComments,StandingWaterNoted,StandingWaterComments,LeakingPlumbNoted,LeakingPlumbComments,"+
				              "VentilationNoted,VentilationComments,CrawlSpaceAccessible,CrawlSpaceAccessibleComments,RoofSagNoted,RoofSagComments,"+
							  "RoofLeakageEvidence,RoofLeakageComments,DeferredMaintanenceIssuesNoted,DeferredMaintanenceComments,"+
							  "DeterioratedNoted,DeterioratedComments,DeterioratedExteriorFinishesNoted,DeterioratedExteriorFinishesComments,"+
							  "DeterioratedExteriorSidingNoted,DeterioratedExteriorSidingComments,SafetyNoted,SafetyComments,OverallComments)"
							+ "VALUES ('"+cf.selectedhomeid+"','"+obs23rdgtxt+"','"+cf.encode(((EditText)findViewById(R.id.obs23comment)).getText().toString())+"',"+
				                       "'"+obs23ardgtxt+"','"+cf.encode(((EditText)findViewById(R.id.obs23acomment)).getText().toString())+"',"+
							           "'"+obs23brdgtxt+"','"+cf.encode(((EditText)findViewById(R.id.obs23bcomment)).getText().toString())+"',"+
				                       "'"+obs23crdgtxt+"','"+cf.encode(((EditText)findViewById(R.id.obs23ccomment)).getText().toString())+"',"+
							           "'"+obs23drdgtxt+"','"+cf.encode(((EditText)findViewById(R.id.obs23dcomment)).getText().toString())+"',"+
				                       "'"+obs24rdgtxt+"','"+cf.encode(((EditText)findViewById(R.id.obs24comment)).getText().toString())+"',"+
							           "'"+obs25rdgtxt+"','"+cf.encode(((EditText)findViewById(R.id.obs25comment)).getText().toString())+"',"+
				                       "'"+obs26ardgtxt+"','"+cf.encode(((EditText)findViewById(R.id.obs26comment)).getText().toString())+"',"+
							           "'"+obs26ardgtxt+"','"+cf.encode(((EditText)findViewById(R.id.obs26acomment)).getText().toString())+"',"+
							           "'"+obs26brdgtxt+"','"+cf.encode(((EditText)findViewById(R.id.obs26bcomment)).getText().toString())+"',"+
							           "'"+obs26crdgtxt+"','"+cf.encode(((EditText)findViewById(R.id.obs26ccomment)).getText().toString())+"',"+
							           "'"+obs27rdgtxt+"','"+cf.encode(((EditText)findViewById(R.id.obs27comment)).getText().toString())+"',"+
							           "'"+cf.encode(((EditText)findViewById(R.id.addcomments)).getText().toString())+"')");

					 cf.ShowToast("Observations(23-27) saved successfully.", 0);
					 ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE); 
					 
			 	}
				catch (Exception E)
				{
					String strerrorlog="Inserting the Observation4 - Sinkhole";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
			}
		}
		catch (Exception E)
		{
			String strerrorlog="Checking the rows inserted in the Observation4 - Sinkhole table.";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+"table at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
	
	private void hidelayouts() {
		// TODO Auto-generated method stub
		((LinearLayout)findViewById(R.id.obs23lin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.obs24lin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.obs25lin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.obs26lin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.obs27lin)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdobs23)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdobs24)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdobs25)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdobs26)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdobs27)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.shwobs23)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwobs24)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwobs25)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwobs26)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwobs27)).setVisibility(cf.v1.VISIBLE);
		
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
		     cf.goclass(54);
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}
}
