package idsoft.inspectiondepot.ARR;

/***   	Page Information
Version :1.0
VCode:1
Created Date:19/10/2012
Purpose:For Call Attempt 
Created By:Rakki S
Last Updated:04/02/2013   
Last Updatedby:Gowri
***/


import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.BIGeneralData.textwatcher;
import idsoft.inspectiondepot.ARR.WindMitBuildCode.Spin_Selectedlistener;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.Calendar;
import java.util.concurrent.TimeoutException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class CallAttempt extends Activity implements Runnable {
	CommonFunctions cf;
	static final int DATE_DIALOG_ID = 0;
	protected static final int visibility = 0;
	EditText etgen, Calldate,Callday, Calltime, Numbercalled, Personanswered,
			Initialcomments, OtherTitle,resul_other;
	RadioButton chkbx1, chkbx2;
	TextView no_rd[]=new TextView[7];
	TextView edit_link[]=new TextView[7];
	View gencomtxt;
	SoapObject res;	
	boolean load_comment=true;
	android.text.format.DateFormat df;
	CharSequence cd, md;
	final Calendar cal = Calendar.getInstance();
	Button Submitcallattempt, getdate, Currenttime, Submitandcancelinspection,
			seldate;
	TableLayout perstbl, tittbl, comttbl, lsttblhdr;
	View viewhomeph, txtPersonanswered, txtInitialcomments, txtTitle,
			txtOther;
	String titlespin, resultspin, phnnum, homeid, InspectionType, status,
			selitem, selresitem, seltitle, strcaldat, strcalday, strcaltime,
			strcalcom, strcalnum, strcalres, strcaltit, chkalert, assdate;
	static String anytype = "anyType{}";
	String[] array_unable, array_insp, array_res, array_title;
	int value, Count, c, selitmflg, selresitmflg, seltitflg, k, verify = 0,
			day, cnt, ichk;
	static String[] arrtit, arrnum, arrres, arrcom, arrdat, arrday, arrtime;
	private ArrayAdapter<CharSequence> loUnableschedule, loInspectionrefused,
			loResult, loTitle;
	Spinner Unableschedule, Inspectionrefused, Result, Title;
	TableRow tr1, tr2, tr3, tr4;
	Cursor cur1;
	ProgressDialog pd;
	TextView policyholderinfo,txtnolist,AIC_txt[]=new TextView[5];
	public LinearLayout SQ_ED_type1_parrant,SQ_ED_type1,SQ_ED_type2_parrant,SQ_ED_type2;
	public TextView SQ_TV_type1,SQ_TV_type2;
	String no_txt[]=new String[7]; 
	RadioButton ph_rd[] = new RadioButton[7];
	
	EditText ed_number;
	TableLayout RCT_ShowValue;
	  public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        cf = new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.selectedhomeid = extras.getString("homeid");
				cf.onlinspectionid = extras.getString("InspectionType");
			    cf.onlstatus = extras.getString("status");
       	}
       	   setContentView(R.layout.newcallattempt);
       	/**Used for hide he key board when it clik out side**/

       	cf.setupUI((ScrollView) findViewById(R.id.scr));

       /**Used for hide he key board when it clik out side**/
       	   cf.getDeviceDimensions();
       	   LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
     	   hdr_layout.setMinimumWidth(cf.wd);
     	   hdr_layout.addView(new HdrOnclickListener(this,1,"General","Call Attempt",1,0,cf));
     	   LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
           main_layout.setMinimumWidth(cf.wd);
           main_layout.addView(new MyOnclickListener(getApplicationContext(), 1, 0,0,cf));
		   LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
		   submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 12, 1,0,cf));
		   LinearLayout submenu2_layout = (LinearLayout) findViewById(R.id.submenu1); System.out.println("test submenu1");
		    submenu2_layout.addView(new MyOnclickListener(getApplicationContext(), 58, 1,0,cf));System.out.println("test 56");
			
		   ScrollView scr=(ScrollView)findViewById(R.id.scr);
		   scr.setMinimumHeight(cf.ht);
		   
		   Declaration();
		   cf.getInspectorId();
		   cf.getCalender();
		 
	       set_Callatttemptvalue();
	       UnablescheduleList();
	       InspectionrefusedList();		
	       ResultList();
	       TitleList();
		
			String source = "<font color=#FFFFFF>Loading Call attempt data. Please wait..."+ "</font>";
			if(cf.isInternetOn()==true)
			{
	            source = "<font color=#FFFFFF>Loading Call attempt data. Please wait..."	+ "</font>";
	            k=1;
				pd = ProgressDialog.show(this,"", Html.fromHtml(source), true);
				Thread thread = new Thread(this);
				thread.start();
			}
			else
			{
				k=2;
				cf.ShowToast("Internet connection not available.",0);
			}
	}
	  private void set_Callatttemptvalue() 
	  {
		// TODO Auto-generated method stub
		  try
			{
				Cursor cur1=cf.SelectTablefunction(cf.policyholder, " where ARR_PH_SRID='" + cf.selectedhomeid + "' and ARR_PH_InspectorId='"	+ cf.Insp_id + "'");
				
				if(cur1.getCount()>0)
				{
					cur1.moveToFirst();
	                  assdate=cf.decode(cur1.getString(cur1.getColumnIndex("ARR_Schedule_AssignedDate")));
	                  no_txt[0]=cf.decode(cur1.getString(cur1.getColumnIndex("ARR_PH_HomePhone"))).trim();
	                  no_txt[1]=cf.decode(cur1.getString(cur1.getColumnIndex("ARR_PH_WorkPhone"))).trim();
	                  no_txt[2]=cf.decode(cur1.getString(cur1.getColumnIndex("ARR_PH_CellPhone"))).trim();
	                  if(no_txt[0].equals(""))
	                  {
	                	  no_rd[0].setText("Not Available");
	                	  no_rd[0].setVisibility(View.INVISIBLE);
	                  }
	                  else
	                  {
	                	  no_rd[0].setText(no_txt[0]);
	                  }
	                  
	                  if(no_txt[1].equals(""))
	                  {
	                	  no_rd[1].setText("Not Available");
	                	  no_rd[1].setVisibility(View.INVISIBLE);
	                  }
	                  else
	                  {
	                	  no_rd[1].setText(no_txt[1]);
	                  }
	                  if(no_txt[2].equals(""))
	                  {
	                	  no_rd[2].setText("Not Available");
	                	  no_rd[2].setVisibility(View.INVISIBLE);
	                  }
	                  else
	                  {
	                	  no_rd[2].setText(no_txt[2]);
	                  }
	            }
				
				cf.CreateARRTable(5);
				Cursor c2=cf.SelectTablefunction(cf.Agent_tabble, " where ARR_AI_SRID='" + cf.selectedhomeid + "'");
				System.out.println("the count agent "+c2.getCount());
				if(c2.getCount()>0)
				{
					c2.moveToFirst();
					no_txt[3]=cf.decode(c2.getString(c2.getColumnIndex("ARR_AI_AgentOffPhone")).trim());
					
					no_txt[4]=cf.decode(c2.getString(c2.getColumnIndex("ARR_AI_AgentContactPhone")).trim());
					if(no_txt[3].equals(""))
	                {
	              	  no_rd[3].setText("Not Available");
	              	  no_rd[3].setVisibility(View.INVISIBLE);
	                }
	                else
	                {
	              	  no_rd[3].setText(no_txt[3]);
	                }
					if(no_txt[4].equals(""))
	                {
	              	  no_rd[4].setText("Not Available");
	              	  no_rd[4].setVisibility(View.INVISIBLE);	              	
	                }
	                else
	                {
	              	  no_rd[4].setText(no_txt[4]);
	                }
				}
				else
				{
					
					no_txt[3]="0";
					no_rd[3].setVisibility(View.INVISIBLE);
					 no_txt[4]="";
					 no_rd[4].setVisibility(View.INVISIBLE);
				}
				try
				{
					cf.CreateARRTable(6);
				 Cursor cur = cf.SelectTablefunction(cf.Additional_table, " where ARR_AD_SRID='"+ cf.selectedhomeid + "'");
					int  rws = cur.getCount();
				 cur.moveToFirst();
				 if (cur != null && rws>0) {
					 
					 do{
						 AIC_txt[0].setText((cur.getString(cur.getColumnIndex("ARR_AD_Bestdaycall")).trim().equals(""))?"N/A":cf.decode(cur.getString(cur.getColumnIndex("ARR_AD_Bestdaycall")).trim()));
						 AIC_txt[1].setText((cur.getString(cur.getColumnIndex("ARR_AD_BestTimetoCallYou")).trim().equals(""))?"N/A":cf.decode(cur.getString(cur.getColumnIndex("ARR_AD_BestTimetoCallYou")).trim()));
						 AIC_txt[2].setText((cur.getString(cur.getColumnIndex("ARR_AD_FirstChoice")).trim().equals(""))?"N/A":cf.decode(cur.getString(cur.getColumnIndex("ARR_AD_FirstChoice")).trim()));
						 AIC_txt[3].setText((cur.getString(cur.getColumnIndex("ARR_AD_SecondChoice")).trim().equals(""))?"N/A":cf.decode(cur.getString(cur.getColumnIndex("ARR_AD_SecondChoice")).trim()));
						 AIC_txt[4].setText((cur.getString(cur.getColumnIndex("ARR_AD_ThirdChoice")).trim().equals(""))?"N/A":cf.decode(cur.getString(cur.getColumnIndex("ARR_AD_ThirdChoice")).trim()));
						 no_txt[5]=(cur.getString(cur.getColumnIndex("ARR_AD_MobileNumber")).trim().equals(""))?"N/A":cf.decode(cur.getString(cur.getColumnIndex("ARR_AD_MobileNumber")).trim());
						 no_txt[6]=(cur.getString(cur.getColumnIndex("ARR_AD_PhoneNumber")).trim().equals(""))?"N/A":cf.decode(cur.getString(cur.getColumnIndex("ARR_AD_PhoneNumber")).trim());
					 } while(cur.moveToNext());
				 }
				 if(no_txt[5].equals(""))
	             {
	           	  no_rd[5].setText("");
	           	  no_rd[5].setVisibility(View.INVISIBLE);
	             }
	             else
	             {
	           	  no_rd[5].setText(no_txt[5]);
	             }
				 if(no_txt[6].equals(""))
	             {
	           	  no_rd[6].setText("");
	           	  no_rd[6].setVisibility(View.INVISIBLE);
	             }
	             else
	             {
	           	  no_rd[6].setText(no_txt[6]);
	             }
				}
				catch (Exception e) {
					// TODO: handle exception
				}
				
			}
			catch(Exception e)
			{
				System.out.println("the error was "+e.getMessage());
				cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ CallAttempt.this +" "+" in the processing stage of retrieving data from PH table  at call attempton create  "+" "+ cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				
				
			}
	}
	private void Declaration() 
	  {
		// TODO Auto-generated method stub
		
		  	chkbx1 = (RadioButton) findViewById(R.id.chk1);
			chkbx1.setOnClickListener(OnClickListener);
			chkbx2 = (RadioButton) findViewById(R.id.chk2);
			chkbx2.setOnClickListener(OnClickListener);
			Unableschedule = (Spinner) findViewById(R.id.spunableschedule);
			Inspectionrefused = (Spinner)findViewById(R.id.spinspectionrefused);
			gencomtxt =  findViewById(R.id.gencmt);

			Unableschedule.setEnabled(false);
			Inspectionrefused.setEnabled(false);
			etgen = (EditText) findViewById(R.id.ed1);
			Calldate = (EditText) findViewById(R.id.calldate);
			getdate = (Button) findViewById(R.id.currentdate);
			seldate = (Button) findViewById(R.id.seldate);
			Callday = (EditText) findViewById(R.id.callday);
			Calltime = (EditText) findViewById(R.id.calltime);
			Currenttime = (Button) findViewById(R.id.currenttime);
			Numbercalled = (EditText) findViewById(R.id.numbercalled);Numbercalled.addTextChangedListener(new CustomTextWatcher(Numbercalled));
			Result = (Spinner) findViewById(R.id.spresult);
			resul_other = (EditText) findViewById(R.id.resul_other);resul_other.addTextChangedListener(new CustomTextWatcherSpace(resul_other));
			txtPersonanswered =findViewById(R.id.txtpersonanswered);
			Personanswered = (EditText) findViewById(R.id.personanswered);Personanswered.addTextChangedListener(new CustomTextWatcherSpace(Personanswered));
			txtTitle = findViewById(R.id.txttitle);
			Title = (Spinner) findViewById(R.id.sptitle);
			AIC_txt[0]=(TextView) findViewById(R.id.ACI_BDC);
			AIC_txt[1]=(TextView) findViewById(R.id.ACI_BTC);
			AIC_txt[2]=(TextView) findViewById(R.id.ACI_first);
			AIC_txt[3]=(TextView) findViewById(R.id.ACI_second);
			AIC_txt[4]=(TextView) findViewById(R.id.ACI_third);
			
			OtherTitle = (EditText) findViewById(R.id.othertitle);OtherTitle.setOnTouchListener(new Touch_Listener(3));
			OtherTitle.addTextChangedListener(new CustomTextWatcherSpace(OtherTitle));
			SQ_TV_type1 = (TextView) findViewById(R.id.initialcomm_txt);
			Initialcomments = (EditText)findViewById(R.id.initialcomment);
			Initialcomments.setOnTouchListener(new Touch_Listener(1));
			Initialcomments.addTextChangedListener(new Callatt_textwatcher(1));
			
			resul_other.setOnTouchListener(new Touch_Listener(2));
			no_rd[0]=(TextView) findViewById(R.id.Call_H_ph);
			no_rd[1]=(TextView) findViewById(R.id.Call_W_ph);
			no_rd[2]=(TextView) findViewById(R.id.Call_C_ph);
			no_rd[3]=(TextView) findViewById(R.id.Call_A_ph);
			no_rd[4]=(TextView) findViewById(R.id.Call_A_C_ph);
			no_rd[5]=(TextView) findViewById(R.id.ACI_mbl_no);
			no_rd[6]=(TextView) findViewById(R.id.ACI_ofc_no);
			ph_rd[0] = (RadioButton)findViewById(R.id.H_ph);
			ph_rd[1] = (RadioButton)findViewById(R.id.W_ph);
			ph_rd[2] = (RadioButton)findViewById(R.id.C_ph);
			ph_rd[3] = (RadioButton)findViewById(R.id.O_ph);
			ph_rd[4] = (RadioButton)findViewById(R.id.Con_ph);
			ph_rd[5] = (RadioButton)findViewById(R.id.m_ph);
			ph_rd[6] = (RadioButton)findViewById(R.id.P_ph);
			RCT_ShowValue=(TableLayout) findViewById(R.id.call_ShowValue);
			Submitcallattempt = (Button) findViewById(R.id.submt);
			Submitandcancelinspection = (Button) findViewById(R.id.cancl);
			txtnolist = (TextView) findViewById(R.id.nolist);
			Result.setOnItemSelectedListener(new  Spin_Selectedlistener());
			Title.setOnItemSelectedListener(new  Spin_Selectedlistener());			
			etgen.addTextChangedListener(new textwatcher(1));			
	  }
	class textwatcher implements TextWatcher
	 {
	       public int type;
	         textwatcher(int type)
	    	{
	    		this.type=type;
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {
	    		// TODO Auto-generated method stub
	    		if(this.type==1)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.gencomm_tv_type1),500); 
	    			if(etgen.getText().toString().startsWith(" "))
	    			{
	    				etgen.setText("");
	    			}
	    		}	
	    	}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				
			}
	 }
	
	class Spin_Selectedlistener implements OnItemSelectedListener
	{

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			String spintxt;
			switch (arg0.getId())
			{
			
			 case R.id.spresult:
				 if (!"--Select--".equals(Result.getSelectedItem().toString())) 
				 {
						resultspin = Result.getSelectedItem().toString();
						if("Answered".equals(Result.getSelectedItem().toString()))
						{
							txtPersonanswered.setVisibility(View.VISIBLE);
							txtTitle.setVisibility(View.VISIBLE);
						} 
						else
						{
							Noanswered();
						}
						if("No Answer".equals(Result.getSelectedItem().toString()))
						{
							Initialcomments.setText("NILL");
						}
						else
						{
							if(Initialcomments.getText().toString().equals("NILL"))
							{
								Initialcomments.setText("");
							}
							else
							{
							Initialcomments.setText(Initialcomments.getText().toString());
							}
						}
					}
					if("Other".equals(Result.getSelectedItem().toString()))
					{
						resul_other.setVisibility(View.VISIBLE);
					}
					else
					{
						resul_other.setVisibility(View.GONE);
						resul_other.setText("");
					}
                break;
                
			 case R.id.sptitle:
				 	if (!"--Select--".equals(Title.getSelectedItem().toString())) 
				 	{
						titlespin = Title.getSelectedItem().toString();
						if ("Other".equals(Title.getSelectedItem().toString())) 
						{
							OtherTitle.setVisibility(View.VISIBLE);
						} 
						else
						{							
							OtherTitle.setVisibility(View.GONE);
						}
					}
	             break;
			}			 
		}
		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
	}
	class Touch_Listener implements OnTouchListener
		{
			   public int type;
			   Touch_Listener(int type)
				{
					this.type=type;
					
				}
			    @Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
			    	if(this.type==1)
					{
			    		cf.setFocus(Initialcomments);
					}
			    	else if(this.type==2)
			    	{
			    		cf.setFocus(resul_other);
			    	}
			    	else if(this.type==3)
			    	{
			    		cf.setFocus(OtherTitle);
			    	}
					return false;
				}
			   
		}
	RadioButton.OnClickListener OnClickListener = new RadioButton.OnClickListener() {
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.chk1:
				
				c = 1;
				chkbx1.setChecked(true);
				chkbx2.setChecked(false);
				chkbx2.setSelected(false);
				Unableschedule.setEnabled(true);
				Inspectionrefused.setEnabled(false);
				gencomtxt.setVisibility(cf.v1.VISIBLE);
				etgen.setVisibility(cf.v1.VISIBLE);
				Inspectionrefused.setSelection(0);

				break;

			case R.id.chk2:
				Initialcomments.setFocusable(false);
				c = 2;
				chkbx2.setChecked(true);
				chkbx1.setSelected(false);
				chkbx1.setChecked(false);
				Unableschedule.setEnabled(false);
				Inspectionrefused.setEnabled(true);
				gencomtxt.setVisibility(cf.v1.VISIBLE);
				etgen.setVisibility(cf.v1.VISIBLE);
				Unableschedule.setSelection(0);

				break;
			}
		}
	};
	class Callatt_textwatcher implements TextWatcher
	{
	     public int type;
	     Callatt_textwatcher(int type)
		{
			this.type=type;
		}
		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			if(this.type==1)
			{
				cf.showing_limit(s.toString(),SQ_TV_type1,500);
				if(Initialcomments.getText().toString().startsWith(" "))
				{
					Initialcomments.setText("");	
				}
			}
		}
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			
		}
	}
	private void validation()
	{
		if (!"".equals(Calldate.getText().toString())
				&& !"".equals(Calltime.getText().toString())
				&& !"--Select--".equals(Result.getSelectedItem().toString())
				&& !"".equals(Numbercalled.getText().toString())) 
		{
			if (cf.checkfortodaysdate(Calldate.getText().toString()) == "true")
			{
				if ("Answered".equals(Result.getSelectedItem().toString())) 
				{
					if (!"".equals(Calldate.getText().toString()) && !"--Select--".equals(Title.getSelectedItem().toString()) && !Personanswered.getText().equals("")) 
					{
						if (strhmephnvalid(Numbercalled.getText().toString()) == "yes") 
						{
							phnnum = Numbercalled.getText().toString();
							long phonenumber;
							try
							{
								phonenumber = Long.parseLong(phnnum);
							}
							catch(Exception e)
							{
								phonenumber=0;
									System.out.println("E "+e.getMessage());
							}
							
							if(phonenumber>=1)
							{
								if ("Answered".equals(Result.getSelectedItem().toString())) 
								{
									if (!"".equals(Calldate.getText().toString()) && !"--Select--".equals(Title.getSelectedItem().toString()) && !Personanswered.getText().toString().trim().equals("")) 
									{
										if (!"Other".equals(Title.getSelectedItem().toString()) || !OtherTitle.getText().toString().trim().equals("")) 
										{										
											submitprocess();
										}
										else
										{
											cf.ShowToast("Please enter other text for title.",0);
											cf.setFocus(OtherTitle);
										}								
									} 
									else 
									{
										System.out.println("itehewhrkwhjrwrj"+Personanswered.getText().toString().trim());
										if(Personanswered.getText().toString().trim().equals(""))
										{
											cf.ShowToast("Please enter Person answered.",0);
											cf.setFocus(Personanswered);
										}
										if ("--Select--".equals(Title.getSelectedItem().toString()))
										{
											cf.ShowToast("Please select Title.",0);
										}										
									}	
								} 
								else 
								{
									if(("Other".equals(Result.getSelectedItem().toString())) && resul_other.getText().toString().trim().equals(""))
									{
										cf.ShowToast("Please enter other text value for Result",0);
										cf.setFocus(resul_other);
									}
									else if ((!"Other".equals(Title.getSelectedItem().toString()) || !OtherTitle.getText().toString().trim().equals("")))
									{
										Noanswered();
										submitprocess();
									}
									else
									{
										cf.ShowToast("Please enter other text value for title",0);
										cf.setFocus(resul_other);
									}								
								}
							}
							else
							{
								Numbercalled.setText("");						
								cf.ShowToast("Please enter the Called Number in correct format.",0);
								cf.setFocus(Numbercalled);
							}						
						} 
						else 
						{
							Numbercalled.setText("");
							cf.ShowToast("Please enter the Called Number in 10 digits.",0);
							cf.setFocus(Numbercalled);
						}
					} 
					else
					{
						System.out.println("itehewhrasdasd"+Personanswered.getText().toString().trim());
						if (Personanswered.getText().toString().trim().equals(""))
						{
							cf.ShowToast("Please enter Person answered.",0);
							cf.setFocus(Personanswered);
						}	
						else if ("--Select--".equals(Title.getSelectedItem().toString()))
						{
							cf.ShowToast("Please select Title.",0);
						}						
					}
				} 
				else 
				{
					if(!("Other".equals(Result.getSelectedItem().toString())) || !resul_other.getText().toString().trim().equals(""))
					{
						if (strhmephnvalid(Numbercalled.getText().toString()) == "yes") 
						{
							phnnum = Numbercalled.getText().toString();
							submitprocess();
						} 
						else
						{
							Numbercalled.setText("");
							cf.ShowToast("Please enter the Called Number in 10 digits.",0);
							cf.setFocus(Numbercalled);
						}
					}
					else
					{
						cf.ShowToast("Please enter other text value for Result", 0);
						cf.setFocus(resul_other);
					}
				}
			} 
			else 
			{
				cf.ShowToast("Call Date should not be greater than Today's Date.",0);
			}
		} 
		else 
		{
			if ("".equals(Calldate.getText().toString()))
			{
				cf.ShowToast("Please enter Call Date.",0);
			}
			else if("".equals(Calltime.getText().toString()))
			{
				cf.ShowToast("Please enter Call Time.",0);
			}
			else if("".equals(Numbercalled.getText().toString().trim()))
			{
				cf.ShowToast("Please enter Number called.",0);
			}
			else if("--Select--".equals(Result.getSelectedItem().toString()))
			{
				cf.ShowToast("Please select Result",0);
			}			
		}
	}

	

	private String checkforassigndate(String getinsurancedate,
			String getinspectiondate) {
		try
		{
		if (!getinsurancedate.trim().equals("")
				|| getinsurancedate.equals("N/A")
				|| getinsurancedate.equals("Not Available")
				|| getinsurancedate.equals("anytype")
				|| getinsurancedate.equals("Null")) {
			String chkdate = null;
			int i1 = getinsurancedate.indexOf("/");
			String result = getinsurancedate.substring(0, i1);
			int i2 = getinsurancedate.lastIndexOf("/");
			String result1 = getinsurancedate.substring(i1 + 1, i2);
			String result2 = getinsurancedate.substring(i2 + 1);
			result2 = result2.trim();
			int j1 = Integer.parseInt(result);
			int j2 = Integer.parseInt(result1);
			int j = Integer.parseInt(result2);

			int i3 = getinspectiondate.indexOf("/");
			String result3 = getinspectiondate.substring(0, i3);
			int i4 = getinspectiondate.lastIndexOf("/");
			String result4 = getinspectiondate.substring(i3 + 1, i4);
			String result5 = getinspectiondate.substring(i4 + 1);
			result5 = result5.trim();
			int k1 = Integer.parseInt(result3);
			int k2 = Integer.parseInt(result4);
			int k = Integer.parseInt(result5);

			if (j > k) {
				chkdate = "false";
			} else if (j < k) {
				chkdate = "true";
			} else if (j == k) {

				if (j1 > k1) {

					chkdate = "false";
				} else if (j1 < k1) {
					chkdate = "true";
				} else if (j1 == k1) {
					if (j2 > k2) {
						chkdate = "false";
					} else if (j2 < k2) {
						chkdate = "true";
					} else if (j2 == k2) {

						chkdate = "true";
					}

				}

			}

			return chkdate;
		} else {
			return "true";
		}
		}
		catch (Exception e)
		{
			return "true";
		}
	}

	private String strhmephnvalid(String strhmephn2) {

		String chk;
		if (strhmephn2.length() == 13) {
			StringBuilder sVowelBuilder = new StringBuilder(strhmephn2);
			sVowelBuilder.insert(0, "(");
			sVowelBuilder.insert(4, ") ");
			sVowelBuilder.insert(9, " ");
			phnnum = sVowelBuilder.toString();

			chk = "yes";

		} else {
			chk = "no";
		}

		return chk;
	}

	private void newCallattemptDownload() {
		View v = null;
		try {
			SoapObject request = new SoapObject(cf.NAMESPACE, "LOADCALLATTEMPT");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.dotNet = true;
			request.addProperty("SRID", cf.selectedhomeid.toString());
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL_ARR);
			androidHttpTransport.call(cf.NAMESPACE+"LOADCALLATTEMPT", envelope);
			res = (SoapObject) envelope.getResponse();System.out.println("res="+res);
			
			if (res != null) {
				cnt = res.getPropertyCount();System.out.println("cnt="+cnt);
				if (cnt != 0) {
					
		     		show_callattempt_Value();
		     		txtnolist.setVisibility(cf.v1.GONE);
				} else {
					txtnolist.setVisibility(visibility);
					
					txtnolist.setText("No call attempt information yet");
				}
			} else {
				pd.dismiss();
				verify = 3;
				cf.ShowToast("There is a problem in saving your data due to invalid character.",0);
			}
		} catch (Exception e) {
			
		}
	}

	private void CallattemptDownload ()throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException, XmlPullParserException {
		View v = null;
		try {
			
			SoapObject request = new SoapObject(cf.NAMESPACE, "LOADCALLATTEMPT");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.dotNet = true;
			request.addProperty("SRID", cf.selectedhomeid.toString());
			System.out.println("call attempt download request"+request);
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL_ARR);
			try {
				androidHttpTransport.call(cf.NAMESPACE+"LOADCALLATTEMPT",envelope);
			} catch (Exception e) {
				System.out.println("call attempt download exception "+e.getMessage());
				throw e;
				
			}System.out.println("reee");
			res = (SoapObject) envelope.getResponse();
			System.out.println("reeesss"+res);
			cnt = res.getPropertyCount();
			
		} catch (SocketTimeoutException s) {
			cf.ShowToast("Please check your network connection and try again.",0);

		} catch (NetworkErrorException n) {
			cf.ShowToast("Please check your network connection and try again.",0);
		} catch (IOException io) {
			cf.ShowToast("Please check your network connection and try again.",0);
		} catch (XmlPullParserException x) {
			cf.ShowToast("Please check your network connection and try again.",0);
		} catch (Exception e) {
			cf.ShowToast("Please check your network connection and try again.",0);
		}
	}

	
	protected void submitprocess() {

		if (ichk == 0) 
		{
			 String source = "<font color=#FFFFFF>Submitting Call Attempt..."	+ "</font>";
			 pd = ProgressDialog.show(this,"", Html.fromHtml(source), true);

			new Thread() {
				private boolean toastst = true;

				public void run() {
					Looper.prepare();
					try {
						String Chk_Inspector=cf.IsCurrentInspector(cf.Insp_id,cf.selectedhomeid,"ISCURRENTINSPECTOR");
						if(Chk_Inspector.equals("true"))
						{  
							if (CancelStatusCheck() == "true") {
								verify = 2;
	
								handler.sendEmptyMessage(0);
								
							} else {
								handler.sendEmptyMessage(0);
								
							}
						}	else
							{
								verify = 3;
								handler.sendEmptyMessage(0);
								
							}
					} catch (Exception e) {

					}
				}

				private String CancelStatusCheck() {

					String chk;
					try 
					{
						cf.getPolicyholderInformation(cf.selectedhomeid);
						SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
						envelope.dotNet = true;
						SoapObject ad_property = new SoapObject(cf.NAMESPACE,"ExportUTSCallAttempt");
						ad_property.addProperty("InspectorID", cf.Insp_id);						
						ad_property.addProperty("Srid",cf.selectedhomeid);
						ad_property.addProperty("CallDate", Calldate.getText().toString());
						ad_property.addProperty("CallTime", Calltime.getText().toString());
						ad_property.addProperty("NumberCalled", Numbercalled.getText().toString());
						int start = Result.getSelectedItemPosition();
						ad_property.addProperty("CallAttemptResultId", start);
						if (start == 1 || start == 7) {
							ad_property.addProperty("PersonAnswered",
									Personanswered.getText().toString());
							int end = Title.getSelectedItemPosition();
							if (end == 7) {
								ad_property.addProperty("PersonAnswered", "");
								ad_property.addProperty("CallAttemptTitleId", end);
								ad_property.addProperty("OtherTitle",
										OtherTitle.getText().toString());
							} else if(start == 7) {
								try
								{
									ad_property.addProperty("PersonAnswered", "");
									ad_property.addProperty("CallAttemptTitleId", 7);
									ad_property.addProperty("OtherTitle", resul_other.getText().toString());
								}
								catch (Exception e)
								{
									ad_property.addProperty("OtherTitle", "");	
									cf.Error_LogFile_Creation("error tracking at the callattempt when i send the result as oters text value ");
								}
							}
							else
							{
								ad_property.addProperty("PersonAnswered", "");
								ad_property.addProperty("CallAttemptTitleId", 8);
								ad_property.addProperty("OtherTitle", "");
							}
						}
						
						else
						{
							ad_property.addProperty("PersonAnswered", "");
							ad_property.addProperty("CallAttemptTitleId", 8);
							ad_property.addProperty("OtherTitle", "");
						}

						ad_property.addProperty("Comments", Initialcomments.getText().toString());
						ad_property.addProperty("CreatedDate", cf.datewithtime);
						ad_property.addProperty("ModifiedDate", cf.datewithtime);
						if (c == 0) {
							ad_property.addProperty("UnableToSchedule", false);
							ad_property.addProperty("InspectionRefused", false);
							ad_property.addProperty("ddlReason", "");
							ad_property.addProperty("ddlRefusedReason", "");
							ad_property.addProperty("GeneralComment", "");

						} else if (c == 1) {
							ad_property.addProperty("UnableToSchedule", true);
							ad_property.addProperty("ddlReason", Unableschedule.getSelectedItem().toString());
							ad_property.addProperty("GeneralComment", etgen.getText().toString());
							ad_property.addProperty("InspectionRefused", false);
							ad_property.addProperty("ddlRefusedReason", "");

						} else if (c == 2) {
							ad_property.addProperty("UnableToSchedule", false);
							ad_property.addProperty("ddlReason", "");
							ad_property.addProperty("GeneralComment", etgen.getText().toString());
							ad_property.addProperty("InspectionRefused", true);
							ad_property.addProperty("ddlRefusedReason",Inspectionrefused.getSelectedItem().toString());
						}
						ad_property.addProperty("Callattemptflag", cf.CommercialFlag);
						System.out.println("there is no more issues the property "+ad_property);
						envelope.setOutputSoapObject(ad_property);
						HttpTransportSE androidHttpTransport1 = new HttpTransportSE(
								cf.URL_ARR);
						
						try {
							androidHttpTransport1.call(cf.NAMESPACE+"ExportUTSCallAttempt", envelope);
						} catch (Exception e) {
							throw e;
						}
						SoapObject result = (SoapObject) envelope.bodyIn;
						String result1 = String.valueOf(envelope.getResponse());
						System.out.println("result="+result1);
						if (result1.contains("Inspection Status updated successfully")) 
						{
							
							if(cf.CommercialFlag.equals("0"))
							{
								cf.arr_db.execSQL("UPDATE "
										+ cf.policyholder
										+ " SET  ARR_PH_Status='110' where ARR_PH_SRID='"
										+ cf.encode(cf.selectedhomeid) + "'");
							}
							else
							{
								cf.arr_db.execSQL("UPDATE "
										+ cf.policyholder
										+ " SET  ARR_PH_Status='110' where Commercial_Flag='"+ cf.CommercialFlag + "'");	
							}
				
							cf.ShowToast(result1.toString(),0);
							Intent Star = new Intent(getApplicationContext(),
									HomeScreen.class);
							Star.putExtra("keyName", 1);
							startActivity(Star);
						}

						else 
						{
							cf.ShowToast(result1.toString(),0);
						}
						chk = "true";
					} catch (SocketTimeoutException s) {
						verify = 1;
						chk = "false";

					} catch (NetworkErrorException n) {

						verify = 1;
						chk = "false";
					} catch (IOException io) {
						verify = 1;
						chk = "false";
					} catch (XmlPullParserException x) {
						verify = 1;
						chk = "false";
					} catch (Exception e) {
						chk = "false";

					}

					return chk;
				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {

						if (verify == 2 && toastst) {
							cf.ShowToast("Call Attempt has been submitted successfully.",0);
							Calldate.setText("");
							Callday.setText("");
							Calltime.setText("");
							Numbercalled.setText("");
							Personanswered.setText("");
							etgen.setText("");
							Inspectionrefused.setSelection(0);
							Unableschedule.setSelection(0);
							Unableschedule.setEnabled(false);
							Inspectionrefused.setEnabled(false);
							chkbx1.setChecked(false);
							chkbx2.setChecked(false);
							Result.setSelection(0);
							Title.setSelection(0);
							etgen.setVisibility(View.GONE);
							gencomtxt.setVisibility(View.GONE);
							ph_rd[0].setChecked(false);
							ph_rd[1].setChecked(false);
							ph_rd[2].setChecked(false);
							ph_rd[3].setChecked(false);
							Initialcomments.setText("");
							Noanswered();
							newCallattemptDownload();
						} else if (verify == 1) {
							cf.ShowToast("Please check your network connection and try again.",0);

						} else {
							if (toastst) {
								cf.ShowToast("Call Attempt has not been submitted. Please try again later.",0);
								verify = 0;
								newCallattemptDownload();
							}

						}
						pd.dismiss();
					}
				};
			}.start();
		} else {
			cf.ShowToast(
					"Internet connection is not available.",0);

		}
	}

	protected void submitprocess1() {
      
		if (ichk == 0) 
		{
			String source = "<font color=#FFFFFF>Submitting Call Attempt..."	+ "</font>";
	         pd = ProgressDialog.show(this,"", Html.fromHtml(source), true);
			
			new Thread() {
				public void run() {
					Looper.prepare();
					try {
						String Chk_Inspector=cf.IsCurrentInspector(cf.Insp_id,cf.selectedhomeid,"ISCURRENTINSPECTOR");
						if(Chk_Inspector.equals("true"))
						{  
							if (CancelStatusCheck1() == "true") {
								verify = 2;
								handler1.sendEmptyMessage(0);
								pd.dismiss();
							} else {
								pd.dismiss();
								handler1.sendEmptyMessage(0);
	
							}
						}
						else
						{
							verify = 3;
							handler1.sendEmptyMessage(0);
							pd.dismiss();
						}
					} catch (Exception e) {

					}
				}

				private String CancelStatusCheck1() {
					String chk;
					try {
						cf.getPolicyholderInformation(cf.selectedhomeid);
						SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
						envelope.dotNet = true;
						SoapObject ad_property = new SoapObject(cf.NAMESPACE,"EXPORTCALLATTEMPT");
						ad_property.addProperty("InspectorID", cf.Insp_id);
						ad_property.addProperty("Srid", cf.selectedhomeid);
						ad_property.addProperty("CallDate", Calldate.getText().toString());
						ad_property.addProperty("CallTime", Calltime.getText().toString());
						ad_property.addProperty("NumberCalled", Numbercalled.getText().toString());
						int start = Result.getSelectedItemPosition();
						ad_property.addProperty("CallAttemptResultId", start);
						if (start == 1) {
							ad_property.addProperty("PersonAnswered",
									Personanswered.getText().toString());
							int end = Title.getSelectedItemPosition();
							ad_property.addProperty("CallAttemptTitleId", end);
							if (end == 7) {
								ad_property.addProperty("OtherTitle",
										OtherTitle.getText().toString());
							} 
							else {
								ad_property.addProperty("OtherTitle", "");
							}

						} else if(start == 7) {
							try
							{
							ad_property.addProperty("PersonAnswered", "");
							ad_property.addProperty("CallAttemptTitleId", 7);
							ad_property.addProperty("OtherTitle", resul_other.getText().toString());
							}
							catch (Exception e)
							{
								ad_property.addProperty("OtherTitle", "");	
								cf.Error_LogFile_Creation("error tracking at the callattempt when i send the result as oters text value ");
							}
						} else {
							ad_property.addProperty("PersonAnswered", "");
							ad_property.addProperty("CallAttemptTitleId", 8);
							ad_property.addProperty("OtherTitle", "");
						}
						ad_property.addProperty("Comments", Initialcomments
								.getText().toString());
						ad_property.addProperty("CreatedDate",cf.datewithtime);
						ad_property.addProperty("ModifiedDate",cf.datewithtime);
						ad_property.addProperty("Callattemptflag", cf.CommercialFlag);
						
						envelope.setOutputSoapObject(ad_property);
						System.out.println("there is no more issues the property"+ad_property); 
						HttpTransportSE androidHttpTransport1 = new HttpTransportSE(
								cf.URL_ARR);
						
						try {
							androidHttpTransport1.call(cf.NAMESPACE+"EXPORTCALLATTEMPT", envelope);
							SoapObject result = (SoapObject) envelope.bodyIn;
							String result1 = String.valueOf(envelope.getResponse());System.out.println("result1"+result1);
							
							chk = "true";
						} catch (Exception e) {
							
							throw e;
						}

					}

					catch (SocketTimeoutException s) {
						verify = 1;
						chk = "false";

					} catch (NetworkErrorException n) {
						verify = 1;
						chk = "false";
					} catch (IOException io) {
						verify = 1;
						chk = "false";
					} catch (XmlPullParserException x) {
						verify = 1;
						chk = "false";
					} catch (Exception e) {
						verify = 1;
						chk = "false";
                 	}

					return chk;
				}

				private Handler handler1 = new Handler() {
					
					@Override
					public void handleMessage(Message msg) {

						
						if (verify == 2) {
							Calldate.setText("");
							Callday.setText("");
							Calltime.setText("");
							Numbercalled.setText("");
							Personanswered.setText("");
							etgen.setText("");
							Inspectionrefused.setSelection(0);
							Unableschedule.setSelection(0);
							Unableschedule.setEnabled(false);
							Inspectionrefused.setEnabled(false);
							chkbx1.setChecked(false);
							chkbx2.setChecked(false);
							Result.setSelection(0);
							Title.setSelection(0);
							OtherTitle.setText("");
							etgen.setVisibility(View.GONE);
							gencomtxt.setVisibility(View.GONE);
							ph_rd[0].setChecked(false);
							ph_rd[1].setChecked(false);
							ph_rd[2].setChecked(false);
							ph_rd[3].setChecked(false);
							Initialcomments.setText("");
							Noanswered();
							newCallattemptDownload();
							cf.ShowToast(
									"Call Attempt has been submitted successfully.",0);
							
						}
						else if(verify==3){
							cf.ShowToast("Sorry. This record has been reallocated to another inspector.", 0);
							
						}
						else {
							cf.ShowToast(
									"Call Attempt has not been submitted. Please try again later.",0);
							verify = 0;

						}

					}
				};
			}.start();
		} else {
			cf.ShowToast(
					"Internet connection is not available.",0);

		}
	}

	private void UnablescheduleList() {
		Unableschedule = (Spinner) findViewById(R.id.spunableschedule);
		loUnableschedule = new ArrayAdapter<CharSequence>(this,
				android.R.layout.simple_spinner_item);
		loUnableschedule
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		Unableschedule.setAdapter(loUnableschedule);
		loUnableschedule.add("--Select--");
		loUnableschedule.add("Bad Contact Information");
		loUnableschedule.add("No Contact with Policyholder");
		loUnableschedule.add("Scheduled then Cancelled");
		loUnableschedule.add("Seasonal Resident");
		loUnableschedule.add("Policyholder on Military Deployment");
		loUnableschedule.add("Not Insured by Carrier Anymore");
		loUnableschedule.add("Other");
	}

	private void InspectionrefusedList() {
		Inspectionrefused = (Spinner) findViewById(R.id.spinspectionrefused);
		loInspectionrefused = new ArrayAdapter<CharSequence>(this,
				android.R.layout.simple_spinner_item);
		loInspectionrefused
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		Inspectionrefused.setAdapter(loInspectionrefused);
		loInspectionrefused.add("--Select--");
		loInspectionrefused.add("Out of Coverage Area");
		loInspectionrefused.add("Unable to meet Cycle Times");
		loInspectionrefused.add("Conflict � Did Original Inspection");
		loInspectionrefused.add("Other");

	}

	private void ResultList() {
		Result = (Spinner) findViewById(R.id.spresult);
		loResult = new ArrayAdapter<CharSequence>(this,
				android.R.layout.simple_spinner_item);
		loResult.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		Result.setAdapter(loResult);
		loResult.add("--Select--");
		loResult.add("Answered");
		loResult.add("No Answer");
		loResult.add("Left Voicemail");
		loResult.add("Number Disconnected");
		loResult.add("Answered & Disconnected");
		loResult.add("Re Order Left voice mail");
		loResult.add("Other");

	}

	private void TitleList() {
		//Title = (Spinner) findViewById(R.id.sptitle);
		loTitle = new ArrayAdapter<CharSequence>(this,
				android.R.layout.simple_spinner_item);
		loTitle.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		Title.setAdapter(loTitle);
		loTitle.add("--Select--");
		loTitle.add("Policyholder");
		loTitle.add("Agent");
		loTitle.add("Agency CSR");
		loTitle.add("Property Manager");
		loTitle.add("Family Member");
		loTitle.add("Realtor");
		loTitle.add("Other");
		Title.setSelection(0);
	}

	private void Noanswered() {
		try
		{
		txtPersonanswered.setVisibility(View.GONE);
		//Personanswered.setVisibility(View.GONE);
		txtTitle.setVisibility(View.GONE);
		//Title.setVisibility(View.GONE);
		//txtOther.setVisibility(View.GONE);
		OtherTitle.setVisibility(View.GONE);
		OtherTitle.setText("");
		}
		catch(Exception e)
		{
			
		}

	}

	public void clicker(View v) {
		final Dialog dialog1;
		TextView txt_v;
		Button bt_save;
		
		switch (v.getId()) {

		case R.id.hme:
			cf.gohome();
			break;
		case R.id.submt: /** Submit Call attempt **/
			if (!"".equals(Calldate.getText().toString()) && !"".equals(Calltime.getText().toString())
					&& !"--Select--".equals(Result.getSelectedItem().toString()) && !"".equals(Numbercalled.getText().toString()))
			{
				if (cf.checkfortodaysdate(Calldate.getText().toString()) == "true") 
				{
					if (checkforassigndate(assdate, Calldate.getText().toString()) == "true") 
					{
						if (strhmephnvalid(Numbercalled.getText().toString()) == "yes") 
						{
							phnnum = Numbercalled.getText().toString();
							long phonenumber;
							try
							{
								phnnum=phnnum.replace("(", "");
								phnnum=phnnum.replace(")", "");
								phnnum=phnnum.replace("-", "");
								
								phonenumber = Long.parseLong(phnnum);
							}
							catch(Exception e)
							{
								phonenumber=0;
									System.out.println("E "+e.getMessage());
							}
							
							if(phonenumber>=1)
							{
							
								if ("Answered".equals(Result.getSelectedItem().toString())) {
								
								if (!"".equals(Calldate.getText().toString()) && !"--Select--".equals(Title.getSelectedItem().toString()) 
										&& !Personanswered.getText().toString().trim().equals("")) 
								{
									if (!"Other".equals(Title.getSelectedItem().toString()) || !OtherTitle.getText().toString().trim().equals("")) 
									{										
										submitprocess1();
									}
									else
									{
										cf.ShowToast("Please enter other text value for title",0);
										cf.setFocus(OtherTitle);
									}

								} 
								else 
								{System.out.println("itej"+Personanswered.getText().toString().trim());
									if ("--Select--".equals(Title.getSelectedItem().toString()))
									{
										cf.ShowToast("Please select Title.",0);
									}
									if(Personanswered.getText().toString().trim().equals(""))
									{
										cf.ShowToast("Please enter Person answered.",0);
										cf.setFocus(Personanswered);
									}									
								}

							} 
							else 
							{
								if(("Other".equals(Result.getSelectedItem().toString())) && resul_other.getText().toString().trim().equals(""))
								{
									cf.ShowToast("Please enter other text value for Result",0);
									cf.setFocus(resul_other);
									
								}
								else if ((!"Other".equals(Title.getSelectedItem().toString()) || !OtherTitle.getText().toString().trim().equals(""))) 
								{
									Noanswered();
									submitprocess1();
								}
								else
								{
									cf.ShowToast("Please enter other text value for title",0);
								}
							}							
						}
						else
						{
							Numbercalled.setText("");
							Numbercalled.requestFocus();							
							cf.ShowToast("Please enter the Called Number in Correct format.",0);
						}
							
						} else {
							Numbercalled.setText("");							
							cf.ShowToast("Please enter the Called Number in 10 digits.",0);cf.setFocus(Numbercalled);

						}
					} else {
						cf.ShowToast("Call Date should be greater than or equal to Assign Date.",0);

						Calldate.setText("");cf.setFocus(Calldate);
					}
				} else {
					cf.ShowToast("Call Date should not be greater than Today's Date.",0);
					Calldate.setText("");cf.setFocus(Calldate);
				}
			} 
			else 
			{
				if ("".equals(Calldate.getText().toString()))
				{
					cf.ShowToast("Please enter Call Date.",0);
				}
				else if("".equals(Calltime.getText().toString()))
				{
					cf.ShowToast("Please enter Call Time.",0);
				}
				else if("".equals(Numbercalled.getText().toString()))
				{
					cf.ShowToast("Please enter Number Called.",0);
				}
				else if("--Select--".equals(Result.getSelectedItem().toString()))
				{
					cf.ShowToast("Please select Result",0);
				}
			}
			break;
		case R.id.cancl:
			
			if (chkbx1.isChecked() && !"--Select--".equals(Unableschedule.getSelectedItem().toString()))
			{
				if(!etgen.getText().toString().trim().equals(""))
				{
					if ("No Contact with Policyholder".equals(Unableschedule.getSelectedItem().toString()) || 
							"Scheduled then Cancelled".equals(Unableschedule.getSelectedItem().toString())) 
					{
						if (cnt >= 3) 
						{
							validation();
						}
						else 
						{
							cf.ShowToast("Please submit minimum three Call Attempts.",0);
						}
					} 
					else 
					{
						validation();
					}
				}
				else
				{
					cf.ShowToast("Please enter the General Comments.",0);cf.setFocus(etgen);
				}
			} 
			else if (chkbx2.isChecked() && !"--Select--".equals(Inspectionrefused.getSelectedItem().toString()))
			{

				if(!etgen.getText().toString().trim().equals(""))
				{
					validation();
				}
				else
				{
					cf.ShowToast("Please enter General Comments.",0);cf.setFocus(etgen);
				}
			} 
			else 
			{
				if(chkbx1.isChecked())
				{
					if("--Select--".equals(Unableschedule.getSelectedItem().toString()))
					{
						cf.ShowToast("Please select Unable to Schedule option.",0);
					}
				}
				else if(chkbx2.isChecked())
				{
					if("--Select--".equals(Inspectionrefused.getSelectedItem().toString()))
					{
						cf.ShowToast("Please select Inspection Refused option.",0);
					}
				}
				else
				{
					cf.ShowToast("Please select either Unable to Schedule or Inspection Refused.",0);
				}
			}
			break;
		case R.id.clear:
			Calldate.setText("");
			Callday.setText("");
			Calltime.setText("");
			Numbercalled.setText("");
			Personanswered.setText("");
			etgen.setText("");
			Inspectionrefused.setSelection(0);
			Unableschedule.setSelection(0);
			Unableschedule.setEnabled(false);
			Inspectionrefused.setEnabled(false);
			chkbx1.setChecked(false);
			chkbx2.setChecked(false);
			Result.setSelection(0);
			Title.setSelection(0);
			OtherTitle.setText("");
			etgen.setVisibility(View.GONE);
			gencomtxt.setVisibility(View.GONE);
			OtherTitle.setVisibility(View.GONE);
			Initialcomments.setText("");
		break;
		case R.id.seldate:
			Calldate.setText("");
			Callday.setText("");
			cf.showDialogDate(Calldate);
			int no_day = cf.mDay + Month(cf.mMonth + 1, cf.mYear);
			int y1 = cf.mYear - 1900;
			int leap = (y1 - 1) / 4;
			y1 = (y1 * 365) + leap;
			no_day += y1;
			int day = (no_day) % 7;
			switch (day) {
			case 0:
				day = 1;
				break;
			case 1:
				day = 2;
				break;
			case 2:
				day = 3;
				break;
			case 3:
				day = 4;
				break;
			case 4:
				day = 5;
				break;
			case 5:
				day = 6;
				break;
			case 6:
				day = 7;
				break;
			}
			
			Set_Day(day);
			
			break;
		case R.id.currentdate:
			cf.getCalender();
			int mMonth1 = cf.mMonth+1;
			Calldate.setText(new StringBuilder()
			// Month is 0 based so add 1
			.append(cf.pad(mMonth1)).append("/").append(cf.pad(cf.mDay)).append("/")
			.append(cf.mYear).append(" "));
			Set_Day(cf.mDayofWeek);
			
			break;
		case R.id.currenttime:
			if (cf.amorpm == 0) {
				Calltime.setText(new StringBuilder()
						// Month is 0 based so add 1
						.append(cf.hours).append(":").append(cf.minutes)
						.append(" ").append("AM").append(" "));
			}
			if (cf.amorpm == 1) {
				Calltime.setText(new StringBuilder()
						// Month is 0 based so add 1
						.append(cf.hours).append(":").append(cf.minutes)
						.append(" ").append("PM").append(" "));
			}

			break;	
			
	
			
		case R.id.c_edit_H_ph:
			  dialog1=showalert();
			   txt_v=(TextView) dialog1.findViewById(R.id.EN_txtid);
			    bt_save=(Button) dialog1.findViewById(R.id.EN_save);
			//   ed_number=(EditText) dialog1.findViewById(R.id.EN_number);
			  txt_v.setText("Please enter the Home phone number ");
			/*  no_txt[0]=no_txt[0].replace("(", "");
				no_txt[0]=no_txt[0].replace(")", "");
				no_txt[0]=no_txt[0].replace("-", "");*/
			  ed_number.setText(no_txt[0]);
			  bt_save.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try
					{
						if(!ed_number.getText().toString().trim().equals("") )
						{
							if(cf.PhoneNo_validation(ed_number.getText().toString().trim()).equals("Yes"))
							{
								
								cf.arr_db.execSQL("update "+cf.policyholder+" set ARR_PH_HomePhone='"+cf.encode(ed_number.getText().toString())+"' where ARR_PH_SRID='"+cf.selectedhomeid+"'");
								no_rd[0].setText(ed_number.getText().toString());
								/*cf.newphone=cf.newphone.replace("(", "");
								cf.newphone=cf.newphone.replace(")", "");
								cf.newphone=cf.newphone.replace("-", "");*/
								no_txt[0]=ed_number.getText().toString();System.out.println("cf.no_txt[0]"+no_txt[0]);
								dialog1.setCancelable(true);
								dialog1.dismiss();
								no_rd[0].setVisibility(View.VISIBLE);
								cf.ShowToast("Phone number updated successfully",0);
								/* if(edit_link[0].getText().toString().equals("Add"))
								 {
									 edit_link[0].setText("Edit");
								 }*/
								 
								 
								 
								
							}
							else
							{
								cf.ShowToast("Please enter valid phone number",0);
							}
							
						}
						else
						{
							cf.ShowToast("Please enter valid phone number",0);
						}
					}
					catch(Exception e)
					{
						cf.ShowToast("Phone number not updated",0);
						dialog1.setCancelable(true);
						dialog1.dismiss();
						System.out.println("Error in the query"+e.getMessage());
						cf.Error_LogFile_Creation("Updating the phone nyumber of the policy holder in the page cal attempt error ="+e.getMessage());
					}
				}
			});
			  dialog1.setCancelable(false);
			  dialog1.show();
			break;
			case R.id.c_edit_W_ph:
				  dialog1=showalert();
				   txt_v=(TextView) dialog1.findViewById(R.id.EN_txtid);
				    bt_save=(Button) dialog1.findViewById(R.id.EN_save);
				   //ed_number=(EditText) dialog1.findViewById(R.id.EN_number);
				  txt_v.setText("Please enter the Work phone number ");
				  	/*no_txt[1]=no_txt[1].replace("(", "");
					no_txt[1]=no_txt[1].replace(")", "");
					no_txt[1]=no_txt[1].replace("-", "");*/
				  ed_number.setText(no_txt[1]);
				  bt_save.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						try
						{
							if(!ed_number.getText().toString().trim().equals("") )
							{
								if(cf.PhoneNo_validation(ed_number.getText().toString().trim()).equals("Yes"))
								{
									
									cf.arr_db.execSQL("update "+cf.policyholder+" set ARR_PH_WorkPhone='"+cf.encode(ed_number.getText().toString())+"' where ARR_PH_SRID='"+cf.selectedhomeid+"'");
									no_rd[1].setText(ed_number.getText().toString());
									/*cf.newphone=cf.newphone.replace("(", "");
									cf.newphone=cf.newphone.replace(")", "");
									cf.newphone=cf.newphone.replace("-", "");*/
									no_txt[1]=ed_number.getText().toString();
									dialog1.setCancelable(true);
									dialog1.dismiss();
									no_rd[1].setVisibility(View.VISIBLE);
									cf.ShowToast("Phone number updated successfully",0);
									/*if(edit_link[1].getText().toString().equals("Add"))
									 {
										 edit_link[1].setText("Edit");
									 }*/
								}
								else
								{
									cf.ShowToast("Please enter valid phone number",0);
								}
								
							}
							else
							{
								cf.ShowToast("Please enter valid phone number",0);
							}
						}
						catch(Exception e)
						{
							cf.ShowToast("Phone number not updated",0);
							dialog1.setCancelable(true);
							dialog1.dismiss();
							System.out.println("Error in the query"+e.getMessage());
							cf.Error_LogFile_Creation("Updating the phone nyumber of the policy holder in the page cal attempt error ="+e.getMessage());
						}
					}
				});
				  dialog1.setCancelable(false);
				  dialog1.show();
				break;
			case R.id.c_edit_C_ph:
				  dialog1=showalert();
				   txt_v=(TextView) dialog1.findViewById(R.id.EN_txtid);
				    bt_save=(Button) dialog1.findViewById(R.id.EN_save);
				   //ed_number=(EditText) dialog1.findViewById(R.id.EN_number);
				   txt_v.setText("Please enter the Cell phone number "); 
				  	/*no_txt[2]=no_txt[2].replace("(", "");
					no_txt[2]=no_txt[2].replace(")", "");
					no_txt[2]=no_txt[2].replace("-", "");*/
				  ed_number.setText(no_txt[2]);
				  bt_save.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						try
						{
							if(!ed_number.getText().toString().trim().equals("") )
							{
								if(cf.PhoneNo_validation(ed_number.getText().toString().trim()).equals("Yes"))
								{
									
									cf.arr_db.execSQL("update "+cf.policyholder+" set ARR_PH_CellPhone='"+cf.encode(ed_number.getText().toString())+"' where ARR_PH_SRID='"+cf.selectedhomeid+"'");
									no_rd[2].setText(ed_number.getText().toString());
									/*cf.newphone=cf.newphone.replace("(", "");
									cf.newphone=cf.newphone.replace(")", "");
									cf.newphone=cf.newphone.replace("-", "");*/
									no_txt[2]=ed_number.getText().toString();
									dialog1.setCancelable(true);
									dialog1.dismiss();
									no_rd[2].setVisibility(View.VISIBLE);
									cf.ShowToast("Phone number updated successfully",0);
									/*if(edit_link[2].getText().toString().equals("Add"))
									 {
										 edit_link[2].setText("Edit");
									 }*/
								}
								else
								{
									cf.ShowToast("Please enter valid phone number",0);
								}
								
							}
							else
							{
								cf.ShowToast("Please enter valid phone number",0);
							}
						}
						catch(Exception e)
						{
							cf.ShowToast("Phone number not updated  ",0);
							dialog1.setCancelable(true);
							dialog1.dismiss();
							System.out.println("Error in the query"+e.getMessage());
							cf.Error_LogFile_Creation("Updating the phone nyumber of the policy holder in the page cal attempt error ="+e.getMessage());
						}
					}
				});
				  dialog1.setCancelable(false);
				  dialog1.show();
				break;
			case R.id.c_edit_A_C_ph:
				try
				{
				  dialog1=showalert();
				   txt_v=(TextView) dialog1.findViewById(R.id.EN_txtid);
				    bt_save=(Button) dialog1.findViewById(R.id.EN_save);
				   //ed_number=(EditText) dialog1.findViewById(R.id.EN_number);
				   txt_v.setText("Please enter the Agent contact phone number");
				  
				/*  no_txt[4]=no_txt[4].replace("(", "");
					no_txt[4]=no_txt[4].replace(")", "");
					no_txt[4]=no_txt[4].replace("-", "");*/
				  ed_number.setText(no_txt[4]);
				  bt_save.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						try
						{
							if(!ed_number.getText().toString().trim().equals("") )
							{
								if(cf.PhoneNo_validation(ed_number.getText().toString().trim()).equals("Yes"))
								{
									
									cf.arr_db.execSQL("update "+cf.Agent_tabble+" set ARR_AI_AgentContactPhone='"+cf.encode(ed_number.getText().toString())+"' where ARR_AI_SRID ='"+cf.selectedhomeid+"'");
									System.out.println("update "+cf.Agent_tabble+" set ARR_AI_AgentContactPhone='"+cf.encode(ed_number.getText().toString())+"' where ARR_AI_SRID ='"+cf.selectedhomeid+"'");
									no_rd[4].setText(ed_number.getText().toString());
									/*cf.newphone=cf.newphone.replace("(", "");
									cf.newphone=cf.newphone.replace(")", "");
									cf.newphone=cf.newphone.replace("-", "");*/
									no_txt[4]=ed_number.getText().toString();
									dialog1.setCancelable(true);
									dialog1.dismiss();
									no_rd[4].setVisibility(View.VISIBLE);
									cf.ShowToast("Phone number updated successfully",0);
									/*if(edit_link[4].getText().toString().equals("Add"))
									 {
										 edit_link[4].setText("Edit");
									 }*/
								}
								else
								{
									cf.ShowToast("Please enter valid phone number",0);
								}
								
							}
							else
							{
								cf.ShowToast("Please enter valid phone number",0);
							}
						}
						catch(Exception e)
						{
							cf.ShowToast("Phone number not updated  ",0);
							dialog1.setCancelable(true);
							dialog1.dismiss();
							System.out.println("Error in the query"+e.getMessage());
							cf.Error_LogFile_Creation("Updating the phone nyumber of the policy holder in the page cal attempt error ="+e.getMessage());
						}
					}
				});
				  dialog1.setCancelable(false);
				  dialog1.show();
				}catch (Exception e) {
					// TODO: handle exception
					System.out.println("the issues "+e.getMessage());
					
				}
				break;
			case R.id.c_edit_A_ph:
				  dialog1=showalert();
				   txt_v=(TextView) dialog1.findViewById(R.id.EN_txtid);
				    bt_save=(Button) dialog1.findViewById(R.id.EN_save);
				   //ed_number=(EditText) dialog1.findViewById(R.id.EN_number);
				  /* 	no_txt[3]=no_txt[3].replace("(", "");
					no_txt[3]=no_txt[3].replace(")", "");
					no_txt[3]=no_txt[3].replace("-", "");*/
				   ed_number.setText(no_txt[3]);
				   txt_v.setText("Please enter the Agent office phone number");
				  bt_save.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						try
						{
							if(!ed_number.getText().toString().trim().equals("") )
							{
								if(cf.PhoneNo_validation(ed_number.getText().toString().trim()).equals("Yes"))
								{
									
									cf.arr_db.execSQL("update "+cf.Agent_tabble+" set ARR_AI_AgentOffPhone='"+cf.encode(ed_number.getText().toString())+"' where ARR_AI_SRID ='"+cf.selectedhomeid+"'");
									no_rd[3].setText(ed_number.getText().toString());
									/*cf.newphone=cf.newphone.replace("(", "");
									cf.newphone=cf.newphone.replace(")", "");
									cf.newphone=cf.newphone.replace("-", "");*/
									no_txt[3]=ed_number.getText().toString();
									dialog1.setCancelable(true);
									dialog1.dismiss();
									no_rd[3].setVisibility(View.VISIBLE);
									cf.ShowToast("Phone number updated successfully",0);
									/*if(edit_link[3].getText().toString().equals("Add"))
									 {
										 edit_link[3].setText("Edit");
									 }*/
								}
								else
								{
									cf.ShowToast("Please enter valid phone number",0);
								}
								
							}
							else
							{
								cf.ShowToast("Please enter valid phone number",0);
							}
						}
						catch(Exception e)
						{
							cf.ShowToast("Phone number not updated  ",0);
							dialog1.setCancelable(true);
							dialog1.dismiss();
							System.out.println("Error in the query"+e.getMessage());
							cf.Error_LogFile_Creation("Updating the phone nyumber of the policy holder in the page cal attempt error ="+e.getMessage());
						}
					}
				});
				  dialog1.setCancelable(false);
				  dialog1.show();
				break;
			case R.id.ACI_mbl_edit:
				  dialog1=showalert();
				   txt_v=(TextView) dialog1.findViewById(R.id.EN_txtid);
				    bt_save=(Button) dialog1.findViewById(R.id.EN_save);
				  // ed_number=(EditText) dialog1.findViewById(R.id.EN_number);
				 /*  no_txt[5]=no_txt[5].replace("(", "");
				   no_txt[5]=no_txt[5].replace(")", "");
				   no_txt[5]=no_txt[5].replace("-", "");*/
				   ed_number.setText(no_txt[5]);
				   txt_v.setText("Please enter the Additional contact Mobile number");
				  bt_save.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						try
						{
							if(!ed_number.getText().toString().trim().equals("") )
							{
								if(cf.PhoneNo_validation(ed_number.getText().toString().trim()).equals("Yes"))
								{
									
									cf.arr_db.execSQL("update "+cf.Additional_table+" set ARR_AD_MobileNumber='"+cf.encode(ed_number.getText().toString())+"' where ARR_AD_SRID ='"+cf.selectedhomeid+"'");
									no_rd[5].setText(ed_number.getText().toString());
									/*cf.newphone=cf.newphone.replace("(", "");
									cf.newphone=cf.newphone.replace(")", "");
									cf.newphone=cf.newphone.replace("-", "");*/
									no_txt[5]=ed_number.getText().toString();
									dialog1.setCancelable(true);
									dialog1.dismiss();
									no_rd[5].setVisibility(View.VISIBLE);
									cf.ShowToast("Phone number updated successfully",0);
									/*if(edit_link[5].getText().toString().equals("Add"))
									 {
										 edit_link[5].setText("Edit");
									 }*/
								}
								else
								{
									cf.ShowToast("Please enter valid phone number",0);
								}
								
							}
							else
							{
								cf.ShowToast("Please enter valid phone number",0);
							}
						}
						catch(Exception e)
						{
							cf.ShowToast("Phone number not updated",0);
							dialog1.setCancelable(true);
							dialog1.dismiss();
							System.out.println("Error in the query"+e.getMessage());
							cf.Error_LogFile_Creation("Updating the phone nyumber of the policy holder in the page cal attempt error ="+e.getMessage());
						}
					}
				});
				  dialog1.setCancelable(false);
				  dialog1.show();
				break;
			case R.id.ACI_ph_edit:
				  dialog1=showalert();
				   txt_v=(TextView) dialog1.findViewById(R.id.EN_txtid);
				    bt_save=(Button) dialog1.findViewById(R.id.EN_save);
				   //ed_number=(EditText) dialog1.findViewById(R.id.EN_number);
				/*   no_txt[6]=no_txt[6].replace("(", "");
				   no_txt[6]=no_txt[6].replace(")", "");
				   no_txt[6]=no_txt[6].replace("-", "");*/
				   ed_number.setText(no_txt[6]);
				   txt_v.setText("Please enter the Additional contact Phone number");
				  bt_save.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						try
						{
							if(!ed_number.getText().toString().trim().equals("") )
							{
								if(cf.PhoneNo_validation(ed_number.getText().toString().trim()).equals("Yes"))
								{
									
									cf.arr_db.execSQL("update "+cf.Additional_table+" set ARR_AD_PhoneNumber='"+cf.encode(ed_number.getText().toString())+"' where ARR_AD_SRID ='"+cf.selectedhomeid+"'");
									no_rd[6].setText(ed_number.getText().toString());
									/*cf.newphone=cf.newphone.replace("(", "");
									cf.newphone=cf.newphone.replace(")", "");
									cf.newphone=cf.newphone.replace("-", "");*/
									no_txt[6]=ed_number.getText().toString();
									dialog1.setCancelable(true);
									dialog1.dismiss();
									no_rd[6].setVisibility(View.VISIBLE);
									cf.ShowToast("Phone number updated successfully",0);
									/*if(edit_link[6].getText().toString().equals("Add"))
									 {
										 edit_link[6].setText("Edit");
									 }*/
								}
								else
								{
									cf.ShowToast("Please enter valid phone number",0);
								}
								
							}
							else
							{
								cf.ShowToast("Please enter valid phone number",0);
							}
						}
						catch(Exception e)
						{
							cf.ShowToast("Phone number not updated",0);
							dialog1.setCancelable(true);
							dialog1.dismiss();
							System.out.println("Error in the query"+e.getMessage());
							cf.Error_LogFile_Creation("Updating the phone number of the policy holder in the page call attempt error ="+e.getMessage());
						}
					}
				});
				  dialog1.setCancelable(false);
				  dialog1.show();
				break;
			case R.id.load_comments:
				int len=Initialcomments.getText().toString().length();
				
				if(load_comment )
				{
					load_comment=false;
					int loc[] = new int[2];
					v.getLocationOnScreen(loc);
					cf.loadinsp_n="General Information";
					cf.loadinsp_q=cf.getResourcesvalue(R.string.gen_3);
					Intent in1 = cf.loadComments("Initials/Comments",loc);
					in1.putExtra("length", len);
					in1.putExtra("max_length", 500);
					startActivityForResult(in1, cf.loadcomment_code);
				}
				break;
		}
	}
	private void Set_Day(int day) {
		// TODO Auto-generated method stub
		if (day == 1) {
			Callday.setText(new StringBuilder().append("Sunday"));
		}
		if (day == 2) {
			Callday.setText(new StringBuilder().append("Monday"));
		}
		if (day  == 3) {
			Callday.setText(new StringBuilder().append("Tuesday"));
		}
		if (day  == 4) {
			Callday.setText(new StringBuilder().append("Wednesday"));
		}
		if (day  == 5) {
			Callday.setText(new StringBuilder().append("Thursday"));
		}
		if (day  == 6) {
			Callday.setText(new StringBuilder().append("Friday"));
		}
		if (day == 7) {
			Callday.setText(new StringBuilder().append("Saturday"));
		}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intimg = new Intent(getApplicationContext(), Agent.class);
			intimg.putExtra("homeid", cf.selectedhomeid);
			intimg.putExtra("InspectionType", cf.onlinspectionid);
			intimg.putExtra("status", cf.onlstatus);
			
			startActivity(intimg);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			
			
			if (k == 1) {
				View v = null;
				if (cnt != 0) {
				
					
		     		
		     		System.out.println("No moer issues ");
		     		
		     		txtnolist.setVisibility(cf.v1.GONE);
		     		show_callattempt_Value();
				} else {
					txtnolist.setVisibility(visibility);
					
					txtnolist.setText("No call attempt information.");
				}

			} else if (k == 2) {
				cf.ShowToast(
						"Internet connection is not available.",0);
			}
			pd.dismiss();
		}
	};
	protected int Month(int month, int year) {
		// TODO Auto-generated method stub
		int days = 0, m = month, y = year;
		if (m > 2) {
			if ((y % 4 == 0 && y % 100 != 0) || y % 400 == 0) {
				days += 1;
			}
		}
		for (int i = 1; i < m; i++) {
			if (i == 1 || i == 3 || i == 5 || i == 7 || i == 8 || i == 10
					|| i == 12) {
				days += 31;
			} else {
				if (i == 4 || i == 6 || i == 9 || i == 11) {
					days += 30;
				} else {
					if (i == 2) {
						days += 28;
					}
				}
			}
		} // no of days in months

		return days;
	}
	private void show_callattempt_Value() {
		// TODO Auto-generated method stub
		if(cnt>0)
		{	try
			{
			RCT_ShowValue.removeAllViews();
			}
			catch (Exception e) {
				// TODO: handle exception
				System.out.println("Not issue"+e.getLocalizedMessage());
			}
	
			TableRow th= (TableRow) getLayoutInflater().inflate(R.layout.call_attempt_inflat, null); System.out.println("Not issues in here ater inflat");
			TableLayout.LayoutParams lp = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT); System.out.println("Not issues in here ater inflat1");
		 	lp.setMargins(2, 0, 2, 2);
		 	th.setPadding(10, 0, 0, 0);
		 	th.setBackgroundColor(0xff6B6D6B);
		 	
		 	
		 	RCT_ShowValue.addView(th,lp);
			RCT_ShowValue.setVisibility(View.VISIBLE);
			for (int i = 0; i < cnt; i++) {
				SoapObject obj = (SoapObject) res.getProperty(i);
				System.out.println("The downloaded call attempt "+obj.toString());
				
				TextView no,RCT,PAD,YIR,RS,RSh,RSt,PD;
				 ImageView edit,delete;
				String CADate_s="",CADay_s="",CAT_s="",CAN_s="",CAR_s="",CATi_s="",CAC_s="";
				
				CADate_s=String.valueOf(obj.getProperty("CallAttemptDate"));
				CADay_s=String.valueOf(obj.getProperty("CallAttemptDay"));
				CAT_s= String.valueOf(obj.getProperty("CallAttemptTime"));
				CAN_s=String.valueOf(obj.getProperty("CallAttemptNumberCalled"));
				CAR_s=String.valueOf(obj.getProperty("CallAttemptResult"));
				CATi_s=String.valueOf(obj.getProperty("CallAttemptTitle"));
				CAC_s=String.valueOf(obj.getProperty("CallAttemptIntialsComment"));
				

				if (CADate_s.contains(anytype)) {
					CADate_s = CADate_s.replace(anytype, "NIL");
				}
				if (CADay_s.contains(anytype)) {
					CADay_s = CADay_s.replace(anytype, "NIL");
				}
				if (CAT_s.contains(anytype)) {
					CAT_s = CAT_s.replace(anytype, "NIL");
				}
				if (CAN_s.contains(anytype)) {
					CAN_s = CAN_s.replace(anytype, "NIL");
				}
				if (CAR_s.contains(anytype)) {
					CAR_s = CAR_s.replace(anytype, "NIL");
				}
				if (CATi_s.contains(anytype)) {
					CATi_s = CATi_s.replace(anytype, "NIL");
				}
				if (CAC_s.contains(anytype)) {
					CAC_s = CAC_s.replace(anytype, "NIL");
				}
				
				
				if (CADate_s.contains("null")) {
					CADate_s = CADate_s.replace("null", "");
				}
				if (CADay_s.contains("null")) {
					CADay_s = CADay_s.replace("null", "");
				}
				if (CAT_s.contains("null")) {
					CAT_s = CAT_s.replace("null", "");
				}
				if (CAN_s.contains("null")) {
					CAN_s = CAN_s.replace("null", "");
				}
				if (CAR_s.contains("null")) {
					CAR_s = CAR_s.replace("null", "");
				}
				if (CATi_s.contains("null")) {
					CATi_s = CATi_s.replace("null", "");
				}
				if (CAC_s.contains("null")) {
					CAC_s = CAC_s.replace("null", "");
				}
				
		
				
				
				TableRow t= (TableRow) getLayoutInflater().inflate(R.layout.call_attempt_inflat, null);
			 	t.setId(44444+i);/// Set some id for further use
			 	
			 	
			 	no= (TextView) t.findViewWithTag("RC_RCT_No_1");
			 	no.setText(String.valueOf(i+1));
			 	no.setTextColor(0xff000000);
			 	RCT= (TextView) t.findViewWithTag("RC_RCT_RCT_1");
			 	RCT.setText(CADate_s);
			 	RCT.setTextColor(0xff000000);
			 	
			 	PAD= (TextView) t.findViewWithTag("RC_RCT_PAD_1");
			 	PAD.setTextColor(0xff000000);
			 	
			 	if (CADay_s.equals("1")) {
			 		CADay_s = "Monday";
				} else if (CADay_s.equals("2")) {
					CADay_s = "Tuesday";
				} else if (CADay_s.equals("3")) {
					CADay_s = "Wednesday";
				} else if (CADay_s.equals("4")) {
					CADay_s = "Thursday";
				} else if (CADay_s.equals("5")) {
					CADay_s = "Friday";
				} else if (CADay_s.equals("6")) {
					CADay_s = "Saturday";
				} else if (CADay_s.equals("7")) {
					CADay_s = "Sunday";
				}
			
			 	PAD.setText(CADay_s);
			 	
			 	YIR= (TextView) t.findViewWithTag("RC_RCT_YIR_1");
			 	YIR.setText(CAT_s);
			 	YIR.setTextColor(0xff000000);
			 	
			 	
			 	RS= (TextView) t.findViewWithTag("RC_RCT_RS_1");
			 	RS.setText(CAN_s);
			 	RS.setTextColor(0xff000000);
			 	
			 	RSh= (TextView) t.findViewWithTag("RC_RCT_RSh_1");
			 	RSh.setText(CAR_s);
			 	RSh.setTextColor(0xff000000);
			 	
			 	RSt= (TextView) t.findViewWithTag("RC_RCT_RSt_1");
			 	RSt.setText(CATi_s);
			 	RSt.setTextColor(0xff000000);
			 	
			 	PD= (TextView) t.findViewWithTag("RC_RCT_PD_1");
			 	PD.setText(CAC_s);
			 	PD.setTextColor(0xff000000);
			 	
			 	
			 	
			 	t.setPadding(10, 0, 0, 0);
			 	RCT_ShowValue.addView(t,lp);
				
			}
			
			
		}else
		{
			
			RCT_ShowValue.setVisibility(View.GONE);
			
			
		}
		
	 	
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode==cf.loadcomment_code)
		{
				load_comment=true;
				if(resultCode==RESULT_OK)
				{
					Initialcomments.setText((Initialcomments.getText().toString()+" "+data.getExtras().getString("Comments")).trim());
					
				}
				/*else if(resultCode==RESULT_CANCELED)
				{
					cf.ShowToast("You have canceled  the comments selction ",0);
				}*/
		}		 		
 		
 	}
	private Dialog showalert() {
		// TODO Auto-generated method stub
		
		
		final Dialog dialog1 = new Dialog(CallAttempt.this,android.R.style.Theme_Translucent_NoTitleBar);
		dialog1.getWindow().setContentView(R.layout.alert);
		LinearLayout maintable= (LinearLayout) dialog1.findViewById(R.id.maintable);
		LinearLayout currenttable= (LinearLayout) dialog1.findViewById(R.id.Edit_number);
		maintable.setVisibility(View.GONE);
		currenttable.setVisibility(View.VISIBLE);
		
		
		ed_number  = (EditText)dialog1. findViewById(R.id.EN_number);
		ed_number.addTextChangedListener(new CustomTextWatcher(ed_number));
		 Button bt_cancel=(Button) dialog1.findViewById(R.id.EN_cancel);
		 ImageView bt_close=(ImageView) dialog1.findViewById(R.id.EN_close);
		   bt_close.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog1.setCancelable(true);
				dialog1.dismiss();
				cf.ShowToast("Phone number not updated",0);
			}
		});
		  bt_cancel.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog1.setCancelable(true);
					dialog1.dismiss();
					cf.ShowToast("Phone number not updated",0);
				}
			});  
		cf.setupUI(dialog1.findViewById(R.id.main_head));
		  return dialog1;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try
		{
		CallattemptDownload();
		}catch (NetworkErrorException n) 
		{
			System.out.println("issues happent"+n.getMessage());			
		}
		catch (SocketTimeoutException s) 
		{
			System.out.println("issues happent s"+s.getMessage());
		}
		catch (IOException io) 
		{
			System.out.println("issues happent io"+io.getMessage());
					
		}
		catch (XmlPullParserException x) 
		{
			System.out.println("issues happent x"+x.getMessage());
		}
		catch (Exception ex) 
		{
			System.out.println("issues happent ex"+ex.getMessage());
		}
		//cf.pd.dismiss();
		handler.sendEmptyMessage(0);
	}
	

	public void clicker_rd(View v)
	{
		for(int i=0;i<ph_rd.length;i++)
		{
			if(v.getId()==ph_rd[i].getId())
			{
				if(!ph_rd[i].getText().toString().equals(""))
				{
					Numbercalled.setText(no_txt[i]);
				}
				else
				{
					ph_rd[i].setChecked(false);
				}
			}
			else
			{
				ph_rd[i].setChecked(false);
			}
				
		}
			

	}
}