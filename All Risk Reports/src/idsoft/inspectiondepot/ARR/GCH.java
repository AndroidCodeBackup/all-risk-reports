/*
   ************************************************************
   * Project: ALL RISK REPORT
   * Module : GCH.java
   * Creation History:
      Created By : Gowri on 
   * Modification History:
      Last Modified By : Gowri on 3/28/2013
   ************************************************************  
*/
package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;

import java.io.IOException;
import java.net.SocketTimeoutException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Html;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TableRow;
import android.widget.TextView;

public class GCH extends Activity {
	CommonFunctions cf;
	String  retgndrdgtxt="",gndrdgtxt="",htbprdgtxt="No",htbcrdgtxt="No",rethtrdtxt="",
			ppsrdgtxt="No",egpprdgtxt="No",psprdgtxt="No",dbprdgtxt="No",pperdgtxt="No",pedrdgtxt="No";
	int ppchkval=0;
	RadioGroup gndrdgval,htbprdgval,htbcrdgval,ppsrdgval,egpprdgval,psprdgval,dbprdgval,pperdgval,pedrdgval;
	boolean gndck=false,htpck=false,htbcck=false,ppsck=false,egppck=false,pspck=false,dbpck=false,
			ppeck=false,pedck=false;
    
	@Override
	   public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        cf=new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);
				
		 	}
	        setContentView(R.layout.poolpresent);
	        cf.getDeviceDimensions();
	        
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
			hdr_layout.addView(new HdrOnclickListener(this,1,"General Conditions & Hazards","Swimming Pool Present",4,0,cf));
	        LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
	        mainmenu_layout.setMinimumWidth(cf.wd);
	        mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 4, 0,0,cf));
	        LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 41, 1,0,cf));
			TableRow tblrw = (TableRow)findViewById(R.id.row2);
			tblrw.setMinimumHeight(cf.ht);
			declarations();
			cf.CreateARRTable(41);
			PoolPresent_SetValue();
	    }
	private void PoolPresent_SetValue() {
		// TODO Auto-generated method stub
		    try
			{
		    	chk_values();
			   Cursor PP_retrive=cf.SelectTablefunction(cf.GCH_PoolPrsenttbl, " where fld_srid='"+cf.selectedhomeid+"' AND (fld_ppchk='1' OR (fld_ppchk='0' AND fld_ppground<>''))");
			   if(PP_retrive.getCount()>0)
			   {  
				   PP_retrive.moveToFirst();
				   ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
				   if(PP_retrive.getInt(PP_retrive.getColumnIndex("fld_ppchk"))==1)
				   { 
					   ((CheckBox)findViewById(R.id.pp_na)).setChecked(true);
					   ((LinearLayout)findViewById(R.id.pp_table)).setVisibility(cf.v1.GONE);
				   }
				   else
				   {
					   ((CheckBox)findViewById(R.id.pp_na)).setChecked(false);
					   ((LinearLayout)findViewById(R.id.pp_table)).setVisibility(cf.v1.VISIBLE);
					   gndrdgtxt=PP_retrive.getString(PP_retrive.getColumnIndex("fld_ppground"));
					   ((RadioButton) gndrdgval.findViewWithTag(gndrdgtxt)).setChecked(true);
					   htbprdgtxt=PP_retrive.getString(PP_retrive.getColumnIndex("fld_htpresent"));
					   if(htbprdgtxt.equals("Yes")){
						   ((LinearLayout)findViewById(R.id.hottubcovlin)).setVisibility(cf.v1.VISIBLE);
						   ((LinearLayout)findViewById(R.id.ppslin)).setVisibility(cf.v1.VISIBLE);
						   ((RadioButton) htbprdgval.findViewWithTag(htbprdgtxt)).setChecked(true);
						   htbcrdgtxt=PP_retrive.getString(PP_retrive.getColumnIndex("fld_htcovered"));
						   ((RadioButton) htbcrdgval.findViewWithTag(htbcrdgtxt)).setChecked(true);
						   ppsrdgtxt=PP_retrive.getString(PP_retrive.getColumnIndex("fld_ppstructure"));
						   ((RadioButton) ppsrdgval.findViewWithTag(ppsrdgtxt)).setChecked(true);
					   }else{
						   ((LinearLayout)findViewById(R.id.hottubcovlin)).setVisibility(cf.v1.GONE);
						   ((LinearLayout)findViewById(R.id.ppslin)).setVisibility(cf.v1.GONE);
					   }
					   
					  
					   egpprdgtxt=PP_retrive.getString(PP_retrive.getColumnIndex("fld_egppresent"));
					   ((RadioButton) egpprdgval.findViewWithTag(egpprdgtxt)).setChecked(true);
					   psprdgtxt=PP_retrive.getString(PP_retrive.getColumnIndex("fld_pspresent"));
					   ((RadioButton) psprdgval.findViewWithTag(psprdgtxt)).setChecked(true);
					   dbprdgtxt=PP_retrive.getString(PP_retrive.getColumnIndex("fld_dbpresent"));
					   ((RadioButton) dbprdgval.findViewWithTag(dbprdgtxt)).setChecked(true);
					   pperdgtxt=PP_retrive.getString(PP_retrive.getColumnIndex("fld_ppenclosure"));
					   ((RadioButton) pperdgval.findViewWithTag(pperdgtxt)).setChecked(true);
					   pedrdgtxt=PP_retrive.getString(PP_retrive.getColumnIndex("fld_pedisrepair"));
					   ((RadioButton) pedrdgval.findViewWithTag(pedrdgtxt)).setChecked(true);
					   
				   }
				   
			   }
			   else
			   {
				   ((CheckBox)findViewById(R.id.pp_na)).setChecked(true);
				   ((LinearLayout)findViewById(R.id.pp_table)).setVisibility(cf.v1.GONE);
			   }
			   
			}
		    catch (Exception E)
			{
				String strerrorlog="Retrieving Pool Present data - GCH";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			}
	}
	private void declarations() {
		// TODO Auto-generated method stub
		
		gndrdgval = (RadioGroup)findViewById(R.id.gnd_rdg);
		gndrdgval.setOnCheckedChangeListener(new checklistenetr(1));
		htbprdgval = (RadioGroup)findViewById(R.id.htbp_rdg);
		htbprdgval.setOnCheckedChangeListener(new checklistenetr(2));
		htbcrdgval = (RadioGroup)findViewById(R.id.htbc_rdg);
		htbcrdgval.setOnCheckedChangeListener(new checklistenetr(3));
		ppsrdgval = (RadioGroup)findViewById(R.id.pps_rdg);
		ppsrdgval.setOnCheckedChangeListener(new checklistenetr(4));
		egpprdgval = (RadioGroup)findViewById(R.id.egpp_rdg);
		egpprdgval.setOnCheckedChangeListener(new checklistenetr(5));
		psprdgval = (RadioGroup)findViewById(R.id.psp_rdg);
		psprdgval.setOnCheckedChangeListener(new checklistenetr(6));
		dbprdgval = (RadioGroup)findViewById(R.id.dbp_rdg);
		dbprdgval.setOnCheckedChangeListener(new checklistenetr(7));
		pperdgval = (RadioGroup)findViewById(R.id.ppe_rdg);
		pperdgval.setOnCheckedChangeListener(new checklistenetr(8));
		pedrdgval = (RadioGroup)findViewById(R.id.ped_rdg);
		pedrdgval.setOnCheckedChangeListener(new checklistenetr(9));
		
		set_defaultchk();
	}
	class checklistenetr implements OnCheckedChangeListener
	{
    	int i;
		public checklistenetr(int i) {
			// TODO Auto-generated constructor stub
		   this.i=i;
		}

		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			   RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
		        boolean isChecked = checkedRadioButton.isChecked();
		        if (isChecked)
		        {
		          switch (i) {
					case 1:
						gndrdgtxt= checkedRadioButton.getText().toString().trim();gndck=false;
					  break;
					case 2:
						htbprdgtxt= checkedRadioButton.getText().toString().trim();htpck=false;
						if(htbprdgtxt.equals("Yes")){
							((LinearLayout)findViewById(R.id.hottubcovlin)).setVisibility(cf.v1.VISIBLE);
							((LinearLayout)findViewById(R.id.ppslin)).setVisibility(cf.v1.VISIBLE);
						}else{
							((LinearLayout)findViewById(R.id.hottubcovlin)).setVisibility(cf.v1.GONE);
							((LinearLayout)findViewById(R.id.ppslin)).setVisibility(cf.v1.GONE);
						}
					  break;
					case 3:
						htbcrdgtxt= checkedRadioButton.getText().toString().trim();htbcck=false;
					  break;
					case 4:
						ppsrdgtxt= checkedRadioButton.getText().toString().trim();ppsck=false;
					  break;
					case 5:
						egpprdgtxt= checkedRadioButton.getText().toString().trim();egppck=false;
					  break;
					case 6:
						psprdgtxt= checkedRadioButton.getText().toString().trim();pspck=false;
					  break;
					case 7:
						dbprdgtxt= checkedRadioButton.getText().toString().trim();dbpck=false;
					  break;
					case 8:
						pperdgtxt= checkedRadioButton.getText().toString().trim();ppeck=false;
					  break;
					case 9:
						pedrdgtxt= checkedRadioButton.getText().toString().trim();pedck=false;
					  break;
					
				  }
		        }
		}

		
	}
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.pp_na:/** SWIMMING POOL PRESENT NOT APPLICABLE CHECKBOX **/
			if(((CheckBox)findViewById(R.id.pp_na)).isChecked())
			{
				
				if(!retgndrdgtxt.equals("")){
 				    showunchkalert(1);
 			    }
    			else
    			{
    				 clearspp();
    				 set_defaultchk();
    				 //cf.arr_db.execSQL("Delete from "+cf.GCH_PoolPrsenttbl+" Where fld_srid='"+cf.selectedhomeid+"'");
    				((LinearLayout)findViewById(R.id.pp_table)).setVisibility(v.GONE);
 			    	((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.INVISIBLE);
 			    }
				
			}
			else
			{
				clearspp();
				//cf.arr_db.execSQL("Delete from "+cf.GCH_PoolPrsenttbl+" Where fld_srid='"+cf.selectedhomeid+"'");
				((LinearLayout)findViewById(R.id.pp_table)).setVisibility(v.VISIBLE);
				((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.INVISIBLE);
				((CheckBox)findViewById(R.id.pp_na)).setChecked(false);
			}
			
			break;
		case R.id.helpid:
			cf.ShowToast("Use for Commercial,Residential and GCH reports.",0);
			break;
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.save:
			if(((CheckBox)findViewById(R.id.pp_na)).isChecked())
			{
				ppchkval=1;poolpresent_insert();
				((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
				cf.ShowToast("Swimming Pool Present saved successfully.", 0);
			    cf.goclass(42);
			}
			else
			{
				ppchkval=0;
				if(gndrdgtxt.equals(""))
	    		{
	    			cf.ShowToast("Please select the option for Pool Present.", 0);
	    		}
				else
				{
					poolpresent_insert();
					((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
					cf.ShowToast("Swimming Pool Present saved successfully.", 0);
				    cf.goclass(42);
			    }
			}
			
			
			break;

		default:
			break;
		}
	}
	public void showunchkalert(final int i) {
		// TODO Auto-generated method stub
		AlertDialog.Builder bl = new Builder(GCH.this);
		bl.setTitle("Confirmation");
		bl.setMessage(Html.fromHtml("Do you want to clear the Swimming Pool Present data?"));
		bl.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog,
											int id) {
										 switch(i){
										 case 1:
											((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.GONE);
							    			 clearspp();//poolpresent_insert();
							    			 ((LinearLayout)findViewById(R.id.pp_table)).setVisibility(cf.v1.GONE);
							    			 break;
										 
										
										 }
										 	 
										 
									}
		});
		bl.setNegativeButton("No",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog,
							int id) {
						switch(i){
						 case 1:
							 ((CheckBox)findViewById(R.id.pp_na)).setChecked(false);
							 break;
					
						 }
					}
        });
		AlertDialog al=bl.create();
		al.setIcon(R.drawable.alertmsg);
		al.setCancelable(false);
		al.show();
		
		
	}
	protected void clearspp() {
		// TODO Auto-generated method stub
		 ppchkval=0;gndck=true;htpck=true;htbcck=true;ppsck=true;
		 egppck=true;pspck=true;dbpck=true;ppeck=true;pedck=true;
		 gndrdgtxt="";htbprdgtxt="";htbcrdgtxt="";ppsrdgtxt="";
		 egpprdgtxt="";psprdgtxt="";dbprdgtxt="";pperdgtxt="";pedrdgtxt="";
		 retgndrdgtxt="";
		 try{if(gndck){gndrdgval.clearCheck();}}catch(Exception e){}
		 try{if(htpck){htbprdgval.clearCheck();}}catch(Exception e){}
		 try{if(htbcck){htbcrdgval.clearCheck();}}catch(Exception e){}
		 try{if(ppsck){ppsrdgval.clearCheck();}}catch (Exception e) {}
		 try{if(egppck){egpprdgval.clearCheck();}}catch(Exception e){}
		 try{if(pspck){psprdgval.clearCheck();}}catch(Exception e){}
		 try{if(dbpck){dbprdgval.clearCheck();}}catch(Exception e){}
		 try{if(ppeck){pperdgval.clearCheck();}}catch (Exception e) {}
		 try{if(pedck){pedrdgval.clearCheck();}}catch(Exception e){}
		 ((LinearLayout)findViewById(R.id.hottubcovlin)).setVisibility(cf.v1.GONE);
		 ((LinearLayout)findViewById(R.id.ppslin)).setVisibility(cf.v1.GONE);
		 set_defaultchk();
	}
	protected void set_defaultchk() {
		// TODO Auto-generated method stub
		((RadioButton) htbprdgval.findViewWithTag("No")).setChecked(true);
		 ((RadioButton) htbcrdgval.findViewWithTag("No")).setChecked(true);
		 ((RadioButton) ppsrdgval.findViewWithTag("No")).setChecked(true);
		 ((RadioButton) egpprdgval.findViewWithTag("No")).setChecked(true);
		 ((RadioButton) psprdgval.findViewWithTag("No")).setChecked(true);
		 ((RadioButton) dbprdgval.findViewWithTag("No")).setChecked(true);
		 ((RadioButton) pperdgval.findViewWithTag("No")).setChecked(true);
		 ((RadioButton) pedrdgval.findViewWithTag("No")).setChecked(true);
		 gndrdgtxt="";htbprdgtxt="No";htbcrdgtxt="No";rethtrdtxt="";
					ppsrdgtxt="No";egpprdgtxt="No";psprdgtxt="No";dbprdgtxt="No";pperdgtxt="No";pedrdgtxt="No";
	}
	private void chk_values() {
		// TODO Auto-generated method stub
		try
		{
		   Cursor PP_retrive=cf.SelectTablefunction(cf.GCH_PoolPrsenttbl, " where fld_srid='"+cf.selectedhomeid+"'");
		   int rws = PP_retrive.getCount();
			if(rws>0){
				PP_retrive.moveToFirst();
				retgndrdgtxt = PP_retrive.getString(PP_retrive.getColumnIndex("fld_ppground"));
			}
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}
	private void poolpresent_insert() {
		// TODO Auto-generated method stub
		Cursor GCH_save=null;
		try
		{
			GCH_save=cf.SelectTablefunction(cf.GCH_PoolPrsenttbl, " where fld_srid='"+cf.selectedhomeid+"'");
			if(GCH_save.getCount()>0)
			{
				try
				{
					cf.arr_db.execSQL("UPDATE "+cf.GCH_PoolPrsenttbl+ " set fld_ppchk='"+ppchkval+"',fld_ppground='"+gndrdgtxt+"',"+
				                               "fld_ppstructure='"+ppsrdgtxt+"',fld_htpresent='"+htbprdgtxt+"',fld_egppresent='"+egpprdgtxt+"',"+
				                               "fld_htcovered='"+htbcrdgtxt+"',fld_pspresent='"+psprdgtxt+"',fld_dbpresent='"+dbprdgtxt+"',"+
				                               "fld_ppenclosure='"+pperdgtxt+"',fld_pedisrepair='"+pedrdgtxt+"' where fld_srid='"+cf.selectedhomeid+"'");
					/*((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
					     cf.ShowToast("Swimming Pool Present saved successfully.", 0);
					     cf.goclass(42);*/
					
					
				}
				catch (Exception E)
				{
					String strerrorlog="Updating the Pool Present data - GCH";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
				
			}
			else
			{
				try
				{
					cf.arr_db.execSQL("INSERT INTO "
							+ cf.GCH_PoolPrsenttbl
							+ " (fld_srid,fld_ppchk,fld_ppground,fld_htpresent,fld_htcovered,fld_ppstructure,fld_egppresent,fld_pspresent,fld_dbpresent,fld_ppenclosure,fld_pedisrepair)"
							+ "VALUES ('"+cf.selectedhomeid+"','"+ppchkval+"','"+gndrdgtxt+"','"+htbprdgtxt+"','"+htbcrdgtxt+"','"+ppsrdgtxt+"','"+egpprdgtxt+"','"+psprdgtxt+"','"+dbprdgtxt+"','"+pperdgtxt+"','"+pedrdgtxt+"')");
					/* ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
					    cf.ShowToast("Swimming Pool Present saved successfully.", 0);
					cf.goclass(42);*/
						
				}
				catch (Exception E)
				{
					String strerrorlog="Inserting the Pool Present data - GCH ";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
				
			}
		}
		catch (Exception E)
		{
			String strerrorlog="Checking the rows inserted in the GCH Pool Present table.";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of checking rows of GCH Pool Present table at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
	
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			cf.goback(3);
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}
	
}