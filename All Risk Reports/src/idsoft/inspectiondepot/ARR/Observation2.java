/*
   ************************************************************
   * Project: ALL RISK REPORT
   * Module : Observation2.java
   * Creation History:
      Created By : Gowri on 
   * Modification History:
      Last Modified By : Gowri on 04/04/2013
   ************************************************************  
*/
package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.Observation3.Touch_Listener;
import idsoft.inspectiondepot.ARR.Observation3.checklistenetr;
import idsoft.inspectiondepot.ARR.Observation3.textwatcher;
import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class Observation2 extends Activity{
	CommonFunctions cf;
	RadioGroup obs8rdgval,obs9rdgval,obs10rdgval,obs11rdgval,obs12rdgval,obs13rdgval,
	obs10ardgval,obs10brdgval,obs10crdgval,obs10drdgval,obs10erdgval;
	String obs8rdgtxt="",obs9rdgtxt="",obs10rdgtxt="",obs11rdgtxt="",obs12rdgtxt="",obs13rdgtxt="",
			obs10ardgtxt="",obs10brdgtxt="",obs10crdgtxt="",obs10drdgtxt="",obs10erdgtxt="";
	public int[] chkbx10crack = {R.id.obs10crk1,R.id.obs10crk2,R.id.obs10crk3,R.id.obs10crk4,R.id.obs10crk5};
	public int[] chkbx10loc = {R.id.obs10loc1,R.id.obs10loc2,R.id.obs10loc3,R.id.obs10loc4,R.id.obs10loc5};
	public int[] chkbx10wid = {R.id.obs10wid1,R.id.obs10wid2,R.id.obs10wid3,R.id.obs10wid4,R.id.obs10wid5};
	public int[] chkbx10cond = {R.id.obs10cond1,R.id.obs10cond2,R.id.obs10cond3,R.id.obs10cond4,R.id.obs10cond5,
			                    R.id.obs10cond6,R.id.obs10cond7,R.id.obs10cond8};
	public int[] chkbx10caus = {R.id.obs10caus1,R.id.obs10caus2,R.id.obs10caus3,R.id.obs10caus4,R.id.obs10caus5,
			R.id.obs10caus6,R.id.obs10caus7};
	public int[] chkbx11sep = {R.id.obs11chk1,R.id.obs11chk2,R.id.obs11chk3};
	public int[] chkbx12crack = {R.id.obs12crk1,R.id.obs12crk2,R.id.obs12crk3,R.id.obs12crk4,R.id.obs12crk5};
	public int[] chkbx12loc = {R.id.obs12loc1,R.id.obs12loc2,R.id.obs12loc3,R.id.obs12loc4,R.id.obs12loc5};
	public int[] chkbx12wid = {R.id.obs12wid1,R.id.obs12wid2,R.id.obs12wid3,R.id.obs12wid4,R.id.obs12wid5};
	public int[] chkbx12cond = {R.id.obs12cond1,R.id.obs12cond2,R.id.obs12cond3,R.id.obs12cond4,R.id.obs12cond5,
			                    R.id.obs12cond6,R.id.obs12cond7,R.id.obs12cond8};
	public int[] chkbx12caus = {R.id.obs12caus1,R.id.obs12caus2,R.id.obs12caus3,R.id.obs12caus4,R.id.obs12caus5,
			R.id.obs12caus6,R.id.obs12caus7};
	public int[] chkbx13crack = {R.id.obs13crk1,R.id.obs13crk2,R.id.obs13crk3,R.id.obs13crk4,R.id.obs13crk5};
	public int[] chkbx13loc = {R.id.obs13loc1,R.id.obs13loc2,R.id.obs13loc3,R.id.obs13loc4,R.id.obs13loc5};
	public int[] chkbx13wid = {R.id.obs13wid1,R.id.obs13wid2,R.id.obs13wid3,R.id.obs13wid4,R.id.obs13wid5};
	public int[] chkbx13cond = {R.id.obs13cond1,R.id.obs13cond2,R.id.obs13cond3,R.id.obs13cond4,R.id.obs13cond5,
			                    R.id.obs13cond6,R.id.obs13cond7,R.id.obs13cond8};
	public int[] chkbx13caus = {R.id.obs13caus1,R.id.obs13caus2,R.id.obs13caus3,R.id.obs13caus4,R.id.obs13caus5,
			R.id.obs13caus6,R.id.obs13caus7};
	public CheckBox obs10chkcrck[] = new CheckBox[5];
	public CheckBox obs10chkloc[] = new CheckBox[5];
	public CheckBox obs10chkwidt[] = new CheckBox[5];
	public CheckBox obs10chkcond[] = new CheckBox[8];
	public CheckBox obs10chkcaus[] = new CheckBox[7];
	public CheckBox obs11sep[] = new CheckBox[3];
	public CheckBox obs12chkcrck[] = new CheckBox[5];
	public CheckBox obs12chkloc[] = new CheckBox[5];
	public CheckBox obs12chkwidt[] = new CheckBox[5];
	public CheckBox obs12chkcond[] = new CheckBox[8];
	public CheckBox obs12chkcaus[] = new CheckBox[7];
	public CheckBox obs13chkcrck[] = new CheckBox[5];
	public CheckBox obs13chkloc[] = new CheckBox[5];
	public CheckBox obs13chkwidt[] = new CheckBox[5];
	public CheckBox obs13chkcond[] = new CheckBox[8];
	public CheckBox obs13chkcaus[] = new CheckBox[7];
	
	String obs10chkcrk_val="",obs10chkloc_val="",obs10chkwidt_val="",obs10chkcond_val="",obs10chkcaus_val="",obs11chksep_val="",
			obs12chkcrk_val="",obs12chkloc_val="",obs12chkwidt_val="",obs12chkcond_val="",obs12chkcaus_val="",
			obs13chkcrk_val="",obs13chkloc_val="",obs13chkwidt_val="",obs13chkcond_val="",obs13chkcaus_val="";

	@Override
	   public void onCreate(Bundle savedInstanceState) {

	        super.onCreate(savedInstanceState);
	        cf=new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);
				
		 	}
	        setContentView(R.layout.sinkobservation2);
	        cf.getDeviceDimensions();
	        
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
			hdr_layout.addView(new HdrOnclickListener(this,1,"Sinkhole Inspection","Observations(8 - 13)",5,0,cf));
	        LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
	        mainmenu_layout.setMinimumWidth(cf.wd);
	        mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 5, 0,0,cf));
	        LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 53, 1,0,cf));
			TableRow tblrw = (TableRow)findViewById(R.id.row2);
			tblrw.setMinimumHeight(cf.ht);
			declarations();
		    cf.CreateARRTable(53);
		    Obs2_Setvalue();
			
	}
	private void Obs2_Setvalue() {
		// TODO Auto-generated method stub
		 try
			{
			   Cursor Obs2_retrive=cf.SelectTablefunction(cf.SINK_Obs2tbl, " where fld_srid='"+cf.selectedhomeid+"'");
			   if(Obs2_retrive.getCount()>0)
			   {  
				   Obs2_retrive.moveToFirst();
				   ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
				    obs8rdgtxt=Obs2_retrive.getString(Obs2_retrive.getColumnIndex("PoolDeckSlabNoted"));
				   ((RadioButton) obs8rdgval.findViewWithTag(obs8rdgtxt)).setChecked(true);
				    ((EditText)findViewById(R.id.obs8comment)).setText(cf.decode(Obs2_retrive.getString(Obs2_retrive.getColumnIndex("PoolDeckSlabComments"))));
				    
				    obs9rdgtxt=Obs2_retrive.getString(Obs2_retrive.getColumnIndex("PoolShellPlumbLevel"));
				    ((RadioButton) obs9rdgval.findViewWithTag(obs9rdgtxt)).setChecked(true);
				    ((EditText)findViewById(R.id.obs9comment)).setText(cf.decode(Obs2_retrive.getString(Obs2_retrive.getColumnIndex("PoolShellPlumbLevelComments"))));
		        
				    obs10rdgtxt=Obs2_retrive.getString(Obs2_retrive.getColumnIndex("ExcessiveSettlement"));
				    ((RadioButton) obs10rdgval.findViewWithTag(obs10rdgtxt)).setChecked(true);
				    if(obs10rdgtxt.equals("Yes"))
				    {
				    	((LinearLayout)findViewById(R.id.obs10yes)).setVisibility(cf.v1.VISIBLE);
				    	 obs10chkcrk_val=cf.decode(Obs2_retrive.getString(Obs2_retrive.getColumnIndex("ExcessiveSettlementTypeofCrack")));
						 cf.setvaluechk1(obs10chkcrk_val,obs10chkcrck,((EditText)findViewById(R.id.obs10crk_etother)));
						 obs10chkloc_val=cf.decode(Obs2_retrive.getString(Obs2_retrive.getColumnIndex("ExcessiveSettlementLocation")));
						 cf.setvaluechk1(obs10chkloc_val,obs10chkloc,((EditText)findViewById(R.id.obs10loc_etother)));
						 obs10chkwidt_val=cf.decode(Obs2_retrive.getString(Obs2_retrive.getColumnIndex("ExcessiveSettlementWidthofCrack")));
						 cf.setvaluechk1(obs10chkwidt_val,obs10chkwidt,((EditText)findViewById(R.id.obs10wid_etother)));
						 ((EditText)findViewById(R.id.obs10_etlen)).setText(cf.decode(Obs2_retrive.getString(Obs2_retrive.getColumnIndex("ExcessiveSettlementLengthofCrack"))));
						 obs10chkcond_val=cf.decode(Obs2_retrive.getString(Obs2_retrive.getColumnIndex("ExcessiveSettlementCleanliness")));
						 cf.setvaluechk1(obs10chkcond_val,obs10chkcond,((EditText)findViewById(R.id.obs10loc_etother)));
						 obs10chkcaus_val=cf.decode(Obs2_retrive.getString(Obs2_retrive.getColumnIndex("ExcessiveSettlementProbableCause")));
						 cf.setvaluechk1(obs10chkcaus_val,obs10chkcaus,((EditText)findViewById(R.id.obs10caus7_etother)));
				    }
				    else
				    {
				    	((LinearLayout)findViewById(R.id.obs10yes)).setVisibility(cf.v1.GONE);
				    }
				    ((EditText)findViewById(R.id.obs10comment)).setText(cf.decode(Obs2_retrive.getString(Obs2_retrive.getColumnIndex("ExcessiveSettlementComments"))));
				    
				    obs10ardgtxt=Obs2_retrive.getString(Obs2_retrive.getColumnIndex("WallLeaningNoted"));
				    ((RadioButton) obs10ardgval.findViewWithTag(obs10ardgtxt)).setChecked(true);
				    ((EditText)findViewById(R.id.obs10acomment)).setText(cf.decode(Obs2_retrive.getString(Obs2_retrive.getColumnIndex("WallLeaningComments"))));
				    
				    obs10brdgtxt=Obs2_retrive.getString(Obs2_retrive.getColumnIndex("WallsVisiblyNotLevel"));
				    ((RadioButton) obs10brdgval.findViewWithTag(obs10brdgtxt)).setChecked(true);
				    ((EditText)findViewById(R.id.obs10bcomment)).setText(cf.decode(Obs2_retrive.getString(Obs2_retrive.getColumnIndex("WallsVisiblyNotLevelComments"))));
				    
				    obs10crdgtxt=Obs2_retrive.getString(Obs2_retrive.getColumnIndex("WallsVisiblyBulgingNoted"));
				    ((RadioButton) obs10crdgval.findViewWithTag(obs10crdgtxt)).setChecked(true);
				    ((EditText)findViewById(R.id.obs10ccomment)).setText(cf.decode(Obs2_retrive.getString(Obs2_retrive.getColumnIndex("WallsVisiblyBulgingComments"))));
				    
				    obs10drdgtxt=Obs2_retrive.getString(Obs2_retrive.getColumnIndex("OpeningsOutofSquare"));
				    ((RadioButton) obs10drdgval.findViewWithTag(obs10drdgtxt)).setChecked(true);
				    ((EditText)findViewById(R.id.obs10dcomment)).setText(cf.decode(Obs2_retrive.getString(Obs2_retrive.getColumnIndex("OpeningsOutofSquareComments"))));
				    
				    obs10erdgtxt=Obs2_retrive.getString(Obs2_retrive.getColumnIndex("DamagedFinishesNoted"));
				    ((RadioButton) obs10erdgval.findViewWithTag(obs10erdgtxt)).setChecked(true);
				    ((EditText)findViewById(R.id.obs10ecomment)).setText(cf.decode(Obs2_retrive.getString(Obs2_retrive.getColumnIndex("DamagedFinishesComments"))));
			      
				    obs11rdgtxt=Obs2_retrive.getString(Obs2_retrive.getColumnIndex("SeperationCrackNoted"));
				    ((RadioButton) obs11rdgval.findViewWithTag(obs11rdgtxt)).setChecked(true);
				    if(obs11rdgtxt.equals("Yes"))
				    {
				    	((RelativeLayout)findViewById(R.id.obs11yes)).setVisibility(cf.v1.VISIBLE);
				    	 obs11chksep_val=cf.decode(Obs2_retrive.getString(Obs2_retrive.getColumnIndex("SeperationCrackTypeofCrack")));
						 cf.setvaluechk1(obs11chksep_val,obs11sep,((EditText)findViewById(R.id.obs11chk3_etother)));
						
				    }
				    else
				    {
				    	((RelativeLayout )findViewById(R.id.obs11yes)).setVisibility(cf.v1.GONE);
				    }
				    ((EditText)findViewById(R.id.obs11comment)).setText(cf.decode(Obs2_retrive.getString(Obs2_retrive.getColumnIndex("SeperationCrackComments"))));
				    
				    obs12rdgtxt=Obs2_retrive.getString(Obs2_retrive.getColumnIndex("ExteriorOpeningCracksNoted"));
				    ((RadioButton) obs12rdgval.findViewWithTag(obs12rdgtxt)).setChecked(true);
				    if(obs12rdgtxt.equals("Yes"))
				    {
				    	((LinearLayout)findViewById(R.id.obs12yes)).setVisibility(cf.v1.VISIBLE);
				    	 obs12chkcrk_val=cf.decode(Obs2_retrive.getString(Obs2_retrive.getColumnIndex("ExteriorOpeningCracksTypeofCrack")));
						 cf.setvaluechk1(obs12chkcrk_val,obs12chkcrck,((EditText)findViewById(R.id.obs12crk_etother)));
						 obs12chkloc_val=cf.decode(Obs2_retrive.getString(Obs2_retrive.getColumnIndex("ExteriorOpeningCracksLocation")));
						 cf.setvaluechk1(obs12chkloc_val,obs12chkloc,((EditText)findViewById(R.id.obs12loc_etother)));
						 obs12chkwidt_val=cf.decode(Obs2_retrive.getString(Obs2_retrive.getColumnIndex("ExteriorOpeningCracksWidthofCrack")));
						 cf.setvaluechk1(obs12chkwidt_val,obs12chkwidt,((EditText)findViewById(R.id.obs12wid_etother)));
						 ((EditText)findViewById(R.id.obs12_etlen)).setText(cf.decode(Obs2_retrive.getString(Obs2_retrive.getColumnIndex("ExteriorOpeningCracksLengthofCrack"))));
						 obs12chkcond_val=cf.decode(Obs2_retrive.getString(Obs2_retrive.getColumnIndex("ExteriorOpeningCracksCleanliness")));
						 cf.setvaluechk1(obs12chkcond_val,obs12chkcond,((EditText)findViewById(R.id.obs12loc_etother)));
						 obs12chkcaus_val=cf.decode(Obs2_retrive.getString(Obs2_retrive.getColumnIndex("ExteriorOpeningCracksProbableCause")));
						 cf.setvaluechk1(obs12chkcaus_val,obs12chkcaus,((EditText)findViewById(R.id.obs12caus7_etother)));
				    }
				    else
				    {
				    	((LinearLayout)findViewById(R.id.obs12yes)).setVisibility(cf.v1.GONE);
				    }
				    ((EditText)findViewById(R.id.obs12comment)).setText(cf.decode(Obs2_retrive.getString(Obs2_retrive.getColumnIndex("ExteriorOpeningCracksComments"))));
				    
				    obs13rdgtxt=Obs2_retrive.getString(Obs2_retrive.getColumnIndex("ObservationSettlementNoted"));
				    ((RadioButton) obs13rdgval.findViewWithTag(obs13rdgtxt)).setChecked(true);
				    if(obs13rdgtxt.equals("Yes"))
				    {
				    	((LinearLayout)findViewById(R.id.obs13yes)).setVisibility(cf.v1.VISIBLE);
				    	 obs13chkcrk_val=cf.decode(Obs2_retrive.getString(Obs2_retrive.getColumnIndex("ObservationSettlementTypeofCrack")));
						 cf.setvaluechk1(obs13chkcrk_val,obs13chkcrck,((EditText)findViewById(R.id.obs13crk_etother)));
						 obs13chkloc_val=cf.decode(Obs2_retrive.getString(Obs2_retrive.getColumnIndex("ObservationSettlementLocation")));
						 cf.setvaluechk1(obs13chkloc_val,obs13chkloc,((EditText)findViewById(R.id.obs13loc_etother)));
						 obs13chkwidt_val=cf.decode(Obs2_retrive.getString(Obs2_retrive.getColumnIndex("ObservationSettlementWidthofCrack")));
						 cf.setvaluechk1(obs13chkwidt_val,obs13chkwidt,((EditText)findViewById(R.id.obs13wid_etother)));
						 ((EditText)findViewById(R.id.obs13_etlen)).setText(cf.decode(Obs2_retrive.getString(Obs2_retrive.getColumnIndex("ObservationSettlementLengthofCrack"))));
						 obs13chkcond_val=cf.decode(Obs2_retrive.getString(Obs2_retrive.getColumnIndex("ObservationSettlementCleanliness")));
						 cf.setvaluechk1(obs13chkcond_val,obs13chkcond,((EditText)findViewById(R.id.obs13loc_etother)));
						 obs13chkcaus_val=cf.decode(Obs2_retrive.getString(Obs2_retrive.getColumnIndex("ObservationSettlementProbableCause")));
						 cf.setvaluechk1(obs13chkcaus_val,obs13chkcaus,((EditText)findViewById(R.id.obs13caus7_etother)));
				    }
				    else
				    {
				    	((LinearLayout)findViewById(R.id.obs13yes)).setVisibility(cf.v1.GONE);
				    }
				    ((EditText)findViewById(R.id.obs13comment)).setText(cf.decode(Obs2_retrive.getString(Obs2_retrive.getColumnIndex("ObservationSettlementComments"))));
			   }
			   else
			   {
				   ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.GONE);
			   }
			}
		    catch (Exception E)
			{
				String strerrorlog="Retrieving Observation2 - Sink";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			}
	}
	private void declarations() {



		// TODO Auto-generated method stub
		 ((EditText)findViewById(R.id.obs8comment)).setOnTouchListener(new Touch_Listener(1));
	     ((EditText)findViewById(R.id.obs8comment)).addTextChangedListener(new textwatcher(1));
	     
	     ((EditText)findViewById(R.id.obs9comment)).setOnTouchListener(new Touch_Listener(2));
	     ((EditText)findViewById(R.id.obs9comment)).addTextChangedListener(new textwatcher(2));
	     
	     ((EditText)findViewById(R.id.obs10comment)).setOnTouchListener(new Touch_Listener(3));
	     ((EditText)findViewById(R.id.obs10comment)).addTextChangedListener(new textwatcher(3));
	     
	     ((EditText)findViewById(R.id.obs10crk_etother)).setOnTouchListener(new Touch_Listener(12));
	     ((EditText)findViewById(R.id.obs10loc_etother)).setOnTouchListener(new Touch_Listener(13));
	     ((EditText)findViewById(R.id.obs10wid_etother)).setOnTouchListener(new Touch_Listener(14));
	     ((EditText)findViewById(R.id.obs10_etlen)).setOnTouchListener(new Touch_Listener(15));
	     ((EditText)findViewById(R.id.obs10_etlen)).setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
	     ((EditText)findViewById(R.id.obs10_etlen)).addTextChangedListener(new textwatcher(12));
	     ((EditText)findViewById(R.id.obs10caus7_etother)).setOnTouchListener(new Touch_Listener(16));
	    
	     ((EditText)findViewById(R.id.obs10acomment)).setOnTouchListener(new Touch_Listener(17));
	     ((EditText)findViewById(R.id.obs10acomment)).addTextChangedListener(new textwatcher(17));
	     
	     ((EditText)findViewById(R.id.obs10bcomment)).setOnTouchListener(new Touch_Listener(18));
	     ((EditText)findViewById(R.id.obs10bcomment)).addTextChangedListener(new textwatcher(18));
	     
	     ((EditText)findViewById(R.id.obs10ccomment)).setOnTouchListener(new Touch_Listener(19));
	     ((EditText)findViewById(R.id.obs10ccomment)).addTextChangedListener(new textwatcher(19));
	     
	     ((EditText)findViewById(R.id.obs10dcomment)).setOnTouchListener(new Touch_Listener(20));
	     ((EditText)findViewById(R.id.obs10dcomment)).addTextChangedListener(new textwatcher(20));
	     
	     ((EditText)findViewById(R.id.obs10ecomment)).setOnTouchListener(new Touch_Listener(21));
	     ((EditText)findViewById(R.id.obs10ecomment)).addTextChangedListener(new textwatcher(21));
	     
	     ((EditText)findViewById(R.id.obs11comment)).setOnTouchListener(new Touch_Listener(9));
	     ((EditText)findViewById(R.id.obs11comment)).addTextChangedListener(new textwatcher(9));
	     ((EditText)findViewById(R.id.obs11chk3_etother)).setOnTouchListener(new Touch_Listener(22));
	     
	     ((EditText)findViewById(R.id.obs12comment)).setOnTouchListener(new Touch_Listener(10));
	     ((EditText)findViewById(R.id.obs12comment)).addTextChangedListener(new textwatcher(10));
	     ((EditText)findViewById(R.id.obs12crk_etother)).setOnTouchListener(new Touch_Listener(23));
	     ((EditText)findViewById(R.id.obs12loc_etother)).setOnTouchListener(new Touch_Listener(24));
	     ((EditText)findViewById(R.id.obs12wid_etother)).setOnTouchListener(new Touch_Listener(25));
	     ((EditText)findViewById(R.id.obs12_etlen)).setOnTouchListener(new Touch_Listener(26));
	     ((EditText)findViewById(R.id.obs12_etlen)).setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
	     ((EditText)findViewById(R.id.obs12_etlen)).addTextChangedListener(new textwatcher(13));
	     ((EditText)findViewById(R.id.obs12caus7_etother)).setOnTouchListener(new Touch_Listener(27));
	     
	     ((EditText)findViewById(R.id.obs13comment)).setOnTouchListener(new Touch_Listener(11));
	     ((EditText)findViewById(R.id.obs13comment)).addTextChangedListener(new textwatcher(11));
	     ((EditText)findViewById(R.id.obs13crk_etother)).setOnTouchListener(new Touch_Listener(28));
	     ((EditText)findViewById(R.id.obs13loc_etother)).setOnTouchListener(new Touch_Listener(29));
	     ((EditText)findViewById(R.id.obs13wid_etother)).setOnTouchListener(new Touch_Listener(30));
	     ((EditText)findViewById(R.id.obs13_etlen)).setOnTouchListener(new Touch_Listener(31));
	     ((EditText)findViewById(R.id.obs13_etlen)).setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
	     ((EditText)findViewById(R.id.obs13_etlen)).addTextChangedListener(new textwatcher(14));
	     ((EditText)findViewById(R.id.obs13caus7_etother)).setOnTouchListener(new Touch_Listener(32));
	     
	     obs8rdgval = (RadioGroup)findViewById(R.id.obs8_rdg);
	     obs8rdgval.setOnCheckedChangeListener(new checklistenetr(1));
	     obs9rdgval = (RadioGroup)findViewById(R.id.obs9_rdg);
	     obs9rdgval.setOnCheckedChangeListener(new checklistenetr(2));
	     obs10rdgval = (RadioGroup)findViewById(R.id.obs10_rdg);
	     obs10rdgval.setOnCheckedChangeListener(new checklistenetr(3));
	     obs10ardgval = (RadioGroup)findViewById(R.id.obs10a_rdg);
	     obs10ardgval.setOnCheckedChangeListener(new checklistenetr(7));
	     obs10brdgval = (RadioGroup)findViewById(R.id.obs10b_rdg);
	     obs10brdgval.setOnCheckedChangeListener(new checklistenetr(8));
	     obs10crdgval = (RadioGroup)findViewById(R.id.obs10c_rdg);
	     obs10crdgval.setOnCheckedChangeListener(new checklistenetr(9));
	     obs10drdgval = (RadioGroup)findViewById(R.id.obs10d_rdg);
	     obs10drdgval.setOnCheckedChangeListener(new checklistenetr(10));
	     obs10erdgval = (RadioGroup)findViewById(R.id.obs10e_rdg);
	     obs10erdgval.setOnCheckedChangeListener(new checklistenetr(11));
	     obs11rdgval = (RadioGroup)findViewById(R.id.obs11_rdg);
	     obs11rdgval.setOnCheckedChangeListener(new checklistenetr(4));
	     obs12rdgval = (RadioGroup)findViewById(R.id.obs12_rdg);
	     obs12rdgval.setOnCheckedChangeListener(new checklistenetr(5));
	     obs13rdgval = (RadioGroup)findViewById(R.id.obs13_rdg);
	     obs13rdgval.setOnCheckedChangeListener(new checklistenetr(6));
	     
	     for(int i=0;i<5;i++)
    	 {
    		try{
    			obs10chkcrck[i] = (CheckBox)findViewById(chkbx10crack[i]);
    			obs10chkloc[i] = (CheckBox)findViewById(chkbx10loc[i]);
    			obs10chkwidt[i] = (CheckBox)findViewById(chkbx10wid[i]);
    			obs12chkcrck[i] = (CheckBox)findViewById(chkbx12crack[i]);
    			obs12chkloc[i] = (CheckBox)findViewById(chkbx12loc[i]);
    			obs12chkwidt[i] = (CheckBox)findViewById(chkbx12wid[i]);
    			obs13chkcrck[i] = (CheckBox)findViewById(chkbx13crack[i]);
    			obs13chkloc[i] = (CheckBox)findViewById(chkbx13loc[i]);
    			obs13chkwidt[i] = (CheckBox)findViewById(chkbx13wid[i]);
    		}
			catch(Exception e)
			{
				System.out.println("error= "+e.getMessage());
			}
    	 }
	     for(int i=0;i<8;i++)
    	 {
    		try{
    			obs10chkcond[i] = (CheckBox)findViewById(chkbx10cond[i]);
    			obs12chkcond[i] = (CheckBox)findViewById(chkbx12cond[i]);
    			obs13chkcond[i] = (CheckBox)findViewById(chkbx13cond[i]);
    		}
			catch(Exception e)
			{
				System.out.println("error= "+e.getMessage());
			}
    	 }
	     for(int i=0;i<7;i++)
    	 {
    		try{
    			obs10chkcaus[i] = (CheckBox)findViewById(chkbx10caus[i]);
    			obs12chkcaus[i] = (CheckBox)findViewById(chkbx12caus[i]);
    			obs13chkcaus[i] = (CheckBox)findViewById(chkbx13caus[i]);
    		}
			catch(Exception e)
			{
				System.out.println("error= "+e.getMessage());
			}
    	 }
	     for(int i=0;i<3;i++)
    	 {
    		try{
    			obs11sep[i] = (CheckBox)findViewById(chkbx11sep[i]);
    			obs11sep[i] = (CheckBox)findViewById(chkbx11sep[i]);
    			obs11sep[i] = (CheckBox)findViewById(chkbx11sep[i]);
    		}
			catch(Exception e)
			{
				System.out.println("error= "+e.getMessage());
			}
    	 }
	     
  
	}
	class checklistenetr implements OnCheckedChangeListener
	{
    	int i;
		public checklistenetr(int i) {
			// TODO Auto-generated constructor stub
		   this.i=i;
		}

		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			   RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
		        // This puts the value (true/false) into the variable
		        boolean isChecked = checkedRadioButton.isChecked();
		        // If the radiobutton that has changed in check state is now checked...
		        if (isChecked)
		        {
		          switch (i) {
					case 1:
						obs8rdgtxt= checkedRadioButton.getText().toString().trim();
						if(obs8rdgtxt.equals("No"))
						{
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs8comment)).setText(R.string.novalueobs);
						}else if(obs8rdgtxt.equals("Not Applicable")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs8comment)).setText(R.string.nopool);
						}else if(obs8rdgtxt.equals("Yes")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs8comment)).setText("");
						}
					 break;
					case 2:
						obs9rdgtxt= checkedRadioButton.getText().toString().trim();
						if(obs9rdgtxt.equals("No")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs9comment)).setText(R.string.novalueobs);
						}else if(obs9rdgtxt.equals("Not Applicable")){
							cf.hidekeyboard();
							((EditText)findViewById(R.id.obs9comment)).setText(R.string.nopool);
		          		}else if(obs9rdgtxt.equals("Yes")){
		          			cf.hidekeyboard();
							((EditText)findViewById(R.id.obs9comment)).setText("");
		        		}
					 break;
					case 3:
						obs10rdgtxt= checkedRadioButton.getText().toString().trim();
						if(obs10rdgtxt.equals("No")){cf.hidekeyboard();
							((EditText)findViewById(R.id.obs10comment)).setText(R.string.novalueobs);
							cf.Set_UncheckBox(obs10chkcrck, ((EditText)findViewById(R.id.obs10crk_etother)));
							cf.Set_UncheckBox(obs10chkloc, ((EditText)findViewById(R.id.obs10loc_etother)));
							cf.Set_UncheckBox(obs10chkwidt, ((EditText)findViewById(R.id.obs10wid_etother)));
							((EditText)findViewById(R.id.obs10_etlen)).setText("");
							cf.Set_UncheckBox(obs10chkcond, ((EditText)findViewById(R.id.obs10crk_etother)));
							cf.Set_UncheckBox(obs10chkcaus, ((EditText)findViewById(R.id.obs10caus7_etother)));
							((LinearLayout)findViewById(R.id.obs10yes)).setVisibility(cf.v1.GONE);}
						else if(obs10rdgtxt.equals("Not Determined")){cf.hidekeyboard();
							((EditText)findViewById(R.id.obs10comment)).setText("");
							cf.Set_UncheckBox(obs10chkcrck, ((EditText)findViewById(R.id.obs10crk_etother)));
							cf.Set_UncheckBox(obs10chkloc, ((EditText)findViewById(R.id.obs10loc_etother)));
							cf.Set_UncheckBox(obs10chkwidt, ((EditText)findViewById(R.id.obs10wid_etother)));
							((EditText)findViewById(R.id.obs10_etlen)).setText("");
							cf.Set_UncheckBox(obs10chkcond, ((EditText)findViewById(R.id.obs10crk_etother)));
							cf.Set_UncheckBox(obs10chkcaus, ((EditText)findViewById(R.id.obs10caus7_etother)));
							((LinearLayout)findViewById(R.id.obs10yes)).setVisibility(cf.v1.GONE);}
						else if(obs10rdgtxt.equals("Yes")){cf.hidekeyboard();
							((EditText)findViewById(R.id.obs10comment)).setText("");
							((LinearLayout)findViewById(R.id.obs10yes)).setVisibility(cf.v1.VISIBLE);}
					 break;
					case 7:
						obs10ardgtxt= checkedRadioButton.getText().toString().trim();
						if(obs10ardgtxt.equals("No")){cf.hidekeyboard();
							((EditText)findViewById(R.id.obs10acomment)).setText(R.string.novalueobs);
						}else{cf.hidekeyboard();
							((EditText)findViewById(R.id.obs10acomment)).setText("");}
					 break;
					case 8:
						obs10brdgtxt= checkedRadioButton.getText().toString().trim();
						if(obs10brdgtxt.equals("No")){cf.hidekeyboard();
							((EditText)findViewById(R.id.obs10bcomment)).setText(R.string.novalueobs);
						}else{cf.hidekeyboard();
							((EditText)findViewById(R.id.obs10bcomment)).setText("");}
					 break;
					case 9:
						obs10crdgtxt= checkedRadioButton.getText().toString().trim();
						if(obs10crdgtxt.equals("No")){cf.hidekeyboard();
							((EditText)findViewById(R.id.obs10ccomment)).setText(R.string.novalueobs);
		          	}else{cf.hidekeyboard();
							((EditText)findViewById(R.id.obs10ccomment)).setText("");}
					 break;
					case 10:
						obs10drdgtxt= checkedRadioButton.getText().toString().trim();
						if(obs10drdgtxt.equals("No")){cf.hidekeyboard();
							((EditText)findViewById(R.id.obs10dcomment)).setText(R.string.novalueobs);
						}else{cf.hidekeyboard();
							((EditText)findViewById(R.id.obs10dcomment)).setText("");}
					 break;
					case 11:
						obs10erdgtxt= checkedRadioButton.getText().toString().trim();
						if(obs10erdgtxt.equals("No")){cf.hidekeyboard();
							((EditText)findViewById(R.id.obs10ecomment)).setText(R.string.novalueobs);
						}else{cf.hidekeyboard();
							((EditText)findViewById(R.id.obs10ecomment)).setText("");}
					 break;
					case 4:
						obs11rdgtxt= checkedRadioButton.getText().toString().trim();
						if(obs11rdgtxt.equals("No")){cf.hidekeyboard();
							((EditText)findViewById(R.id.obs11comment)).setText(R.string.novalueobs);
							cf.Set_UncheckBox(obs11sep, ((EditText)findViewById(R.id.obs11chk3_etother)));
							((RelativeLayout)findViewById(R.id.obs11yes)).setVisibility(cf.v1.GONE);
							}
						else if(obs11rdgtxt.equals("Not Determined")){cf.hidekeyboard();
							((EditText)findViewById(R.id.obs11comment)).setText("");
							cf.Set_UncheckBox(obs11sep, ((EditText)findViewById(R.id.obs11chk3_etother)));
							((RelativeLayout)findViewById(R.id.obs11yes)).setVisibility(cf.v1.GONE);}
						else if(obs11rdgtxt.equals("Yes")){cf.hidekeyboard();
							((EditText)findViewById(R.id.obs11comment)).setText("");
							((RelativeLayout)findViewById(R.id.obs11yes)).setVisibility(cf.v1.VISIBLE);}
					 break;
					case 5:
						obs12rdgtxt= checkedRadioButton.getText().toString().trim();
						if(obs12rdgtxt.equals("No")){cf.hidekeyboard();
							((EditText)findViewById(R.id.obs12comment)).setText(R.string.novalueobs);
							cf.Set_UncheckBox(obs12chkcrck, ((EditText)findViewById(R.id.obs12crk_etother)));
							cf.Set_UncheckBox(obs12chkloc, ((EditText)findViewById(R.id.obs12loc_etother)));
							cf.Set_UncheckBox(obs12chkwidt, ((EditText)findViewById(R.id.obs12wid_etother)));
							((EditText)findViewById(R.id.obs12_etlen)).setText("");
							cf.Set_UncheckBox(obs12chkcond, ((EditText)findViewById(R.id.obs12crk_etother)));
							cf.Set_UncheckBox(obs12chkcaus, ((EditText)findViewById(R.id.obs12caus7_etother)));
							((LinearLayout)findViewById(R.id.obs12yes)).setVisibility(cf.v1.GONE);}
						else if(obs12rdgtxt.equals("Not Determined")){cf.hidekeyboard();
							((EditText)findViewById(R.id.obs12comment)).setText("");
							cf.Set_UncheckBox(obs12chkcrck, ((EditText)findViewById(R.id.obs12crk_etother)));
							cf.Set_UncheckBox(obs12chkloc, ((EditText)findViewById(R.id.obs12loc_etother)));
							cf.Set_UncheckBox(obs12chkwidt, ((EditText)findViewById(R.id.obs12wid_etother)));
							((EditText)findViewById(R.id.obs12_etlen)).setText("");
							cf.Set_UncheckBox(obs12chkcond, ((EditText)findViewById(R.id.obs12crk_etother)));
							cf.Set_UncheckBox(obs12chkcaus, ((EditText)findViewById(R.id.obs12caus7_etother)));
							((LinearLayout)findViewById(R.id.obs12yes)).setVisibility(cf.v1.GONE);}
						else if(obs12rdgtxt.equals("Yes")){cf.hidekeyboard();
							((EditText)findViewById(R.id.obs12comment)).setText("");
							((LinearLayout)findViewById(R.id.obs12yes)).setVisibility(cf.v1.VISIBLE);}
					 break;
					case 6:
						obs13rdgtxt= checkedRadioButton.getText().toString().trim();
						if(obs13rdgtxt.equals("None Noted to Visible Areas")){cf.hidekeyboard();
							((EditText)findViewById(R.id.obs13comment)).setText(R.string.novalueobs);
							cf.Set_UncheckBox(obs13chkcrck, ((EditText)findViewById(R.id.obs13crk_etother)));
							cf.Set_UncheckBox(obs13chkloc, ((EditText)findViewById(R.id.obs13loc_etother)));
							cf.Set_UncheckBox(obs13chkwidt, ((EditText)findViewById(R.id.obs13wid_etother)));
							((EditText)findViewById(R.id.obs13_etlen)).setText("");
							cf.Set_UncheckBox(obs13chkcond, ((EditText)findViewById(R.id.obs13crk_etother)));
							cf.Set_UncheckBox(obs13chkcaus, ((EditText)findViewById(R.id.obs13caus7_etother)));
							((LinearLayout)findViewById(R.id.obs13yes)).setVisibility(cf.v1.GONE);}
						else if(obs13rdgtxt.equals("Not Determined")){cf.hidekeyboard();
							((EditText)findViewById(R.id.obs13comment)).setText("");
							cf.Set_UncheckBox(obs13chkcrck, ((EditText)findViewById(R.id.obs13crk_etother)));
							cf.Set_UncheckBox(obs13chkloc, ((EditText)findViewById(R.id.obs13loc_etother)));
							cf.Set_UncheckBox(obs13chkwidt, ((EditText)findViewById(R.id.obs13wid_etother)));
							((EditText)findViewById(R.id.obs13_etlen)).setText("");
							cf.Set_UncheckBox(obs13chkcond, ((EditText)findViewById(R.id.obs13crk_etother)));
							cf.Set_UncheckBox(obs13chkcaus, ((EditText)findViewById(R.id.obs13caus7_etother)));
							((LinearLayout)findViewById(R.id.obs13yes)).setVisibility(cf.v1.GONE);}
						else if(obs13rdgtxt.equals("Yes")){cf.hidekeyboard();
							((EditText)findViewById(R.id.obs13comment)).setText("");
							((LinearLayout)findViewById(R.id.obs13yes)).setVisibility(cf.v1.VISIBLE);}
					 break;
					default:
						break;
					}
		        }
		     }
    }	
 class Touch_Listener implements OnTouchListener

	    {
	    	   public int type;
	    	   Touch_Listener(int type)
	    		{
	    			this.type=type;
	    			
	    		}
	    	    @Override
	    		public boolean onTouch(View v, MotionEvent event) {

	    			// TODO Auto-generated method stub
	    	    	((EditText) findViewById(v.getId())).setFocusableInTouchMode(true);
			    	((EditText) findViewById(v.getId())).requestFocus();
	    			return false;
	    	    }
	    }
	 class textwatcher implements TextWatcher
     {
	         public int type;
	         textwatcher(int type)
	    	{
	    		this.type=type;
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {


	    		// TODO Auto-generated method stub
	    		if(this.type==1)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs8_tv_type1),250); 
	    			if(s.toString().startsWith(" "))
	    			{
	    				 ((EditText)findViewById(R.id.obs8comment)).setText("");
	    			}
	    		}
	    		else if(this.type==2)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs9_tv_type1),250);
	    			if(s.toString().startsWith(" "))
	    			{
	    				 ((EditText)findViewById(R.id.obs9comment)).setText("");
	    			}
	    		}
	    		else if(this.type==3)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs10_tv_type1),250); 
	    			if(s.toString().startsWith(" "))
	    			{
	    				 ((EditText)findViewById(R.id.obs10comment)).setText("");
	    			}
	    		}
	    		else if(this.type==17)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs10a_tv_type1),250); 
	    			if(s.toString().startsWith(" "))
	    			{
	    				 ((EditText)findViewById(R.id.obs10acomment)).setText("");
	    			}
	    		}
	    		else if(this.type==18)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs10b_tv_type1),250);
	    			if(s.toString().startsWith(" "))
	    			{
	    				 ((EditText)findViewById(R.id.obs10bcomment)).setText("");
	    			}
	    		}
	    		else if(this.type==19)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs10c_tv_type1),250); 
	    			if(s.toString().startsWith(" "))
	    			{
	    				 ((EditText)findViewById(R.id.obs10ccomment)).setText("");
	    			}
	    		}
	    		else if(this.type==20)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs10d_tv_type1),250); 
	    			if(s.toString().startsWith(" "))
	    			{
	    				 ((EditText)findViewById(R.id.obs10dcomment)).setText("");
	    			}
	    		}
	    		else if(this.type==21)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs10e_tv_type1),250); 
	    			if(s.toString().startsWith(" "))
	    			{
	    				 ((EditText)findViewById(R.id.obs10ecomment)).setText("");
	    			}
	    		}
	    		else if(this.type==9)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs11_tv_type1),250); 
	    			if(s.toString().startsWith(" "))
	    			{
	    				 ((EditText)findViewById(R.id.obs11comment)).setText("");
	    			}
	    		}
	    		else if(this.type==10)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs12_tv_type1),250); 
	    			if(s.toString().startsWith(" "))
	    			{
	    				 ((EditText)findViewById(R.id.obs12comment)).setText("");
	    			}
	    		}
	    		else if(this.type==11)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.obs13_tv_type1),250); 
	    			if(s.toString().startsWith(" "))
	    			{
	    				 ((EditText)findViewById(R.id.obs13comment)).setText("");
	    			}
	    		}
	    		else if(this.type==12)
	    		{
	    			 if(!s.toString().equals("")){
	    				 if(Character.toString(s.toString().charAt(0)).equals("0") || Character.toString(s.toString().charAt(0)).equals("."))
		    			 {
		    				 cf.ShowToast("Invalid Entry! Please enter valid Approximate Length of cracks.", 0);
		    				 ((EditText)findViewById(R.id.obs10_etlen)).setText("");
		    			 }
	    			 }
	    		}
	    		else if(this.type==13)
	    		{
	    			 if(!s.toString().equals("")){
	    				 if(Character.toString(s.toString().charAt(0)).equals("0") || Character.toString(s.toString().charAt(0)).equals("."))
		    			 {
		    				 cf.ShowToast("Invalid Entry! Please enter valid Approximate Length of cracks.", 0);
		    				 ((EditText)findViewById(R.id.obs12_etlen)).setText("");
		    			 }
	    			 }
	    		}
	    		else if(this.type==14)
	    		{
	    			 if(!s.toString().equals("")){
	    				 if(Character.toString(s.toString().charAt(0)).equals("0") || Character.toString(s.toString().charAt(0)).equals("."))
		    			 {
		    				 cf.ShowToast("Invalid Entry! Please enter valid Approximate Length of cracks.", 0);
		    				 ((EditText)findViewById(R.id.obs13_etlen)).setText("");
		    			 }
	    			 }
	    		}
	    	}

	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,

	    			int after) {
	    		// TODO Auto-generated method stub
	     	}

	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,

	    			int count) {
	    		// TODO Auto-generated method stub
	    	}
	   }	
	public void clicker(View v)


    {
	    	switch(v.getId())
	    	{
	    	case R.id.hme:
	    		cf.gohome();
	    		break;
	    	case R.id.obs10crk5: /** CHECK BOX - TYPE OF CRACK 10 **/
	    		 if(obs10chkcrck[4].isChecked())
				  {
	    			  ((EditText)findViewById(R.id.obs10crk_etother)).setVisibility(v.VISIBLE);
	    			  ((EditText)findViewById(R.id.obs10crk_etother)).requestFocus();
				  }
				  else
				  {
					  cf.clearother(((EditText)findViewById(R.id.obs10crk_etother)));
				  }
				break;
	    	case R.id.obs10loc5: /** CHECK BOX - LOCATION OF CRACK 10 **/
	    		 if(obs10chkloc[4].isChecked())
				  {
	    			  ((EditText)findViewById(R.id.obs10loc_etother)).setVisibility(v.VISIBLE);
	    			  ((EditText)findViewById(R.id.obs10loc_etother)).requestFocus();
				  }
				  else
				  {
					  cf.clearother(((EditText)findViewById(R.id.obs10loc_etother)));
				  }
				break;
	    	case R.id.obs10wid5: /** CHECK BOX - WIDTH OF CRACK 10 **/
	    		 if(obs10chkwidt[4].isChecked())
				  {
	    			  ((EditText)findViewById(R.id.obs10wid_etother)).setVisibility(v.VISIBLE);
	    			  ((EditText)findViewById(R.id.obs10wid_etother)).requestFocus();
				  }
				  else
				  {
					  cf.clearother(((EditText)findViewById(R.id.obs10wid_etother)));
				  }
				break;
	    	case R.id.obs10caus7: /** CHECK BOX - POSSIBLE CAUSE  10 **/
	    		 if(obs10chkcaus[6].isChecked())
				  {
	    			  ((EditText)findViewById(R.id.obs10caus7_etother)).setVisibility(v.VISIBLE);
	    			  ((EditText)findViewById(R.id.obs10caus7_etother)).requestFocus();
				  }
				  else
				  {
					  cf.clearother(((EditText)findViewById(R.id.obs10caus7_etother)));
				  }
				break;
	    	case R.id.obs11chk3: /** CHECK BOX - OTHER 11 **/
	    		if(obs11sep[2].isChecked())
				  {
	    			  ((EditText)findViewById(R.id.obs11chk3_etother)).setVisibility(v.VISIBLE);
	    			  ((EditText)findViewById(R.id.obs11chk3_etother)).requestFocus();
				  }
				  else
				  {
					  cf.clearother(((EditText)findViewById(R.id.obs11chk3_etother)));
				  }
	    	break;
	    	case R.id.obs12crk5: /** CHECK BOX - TYPE OF CRACK 12 **/
	    		 if(obs12chkcrck[4].isChecked())
				  {
	    			  ((EditText)findViewById(R.id.obs12crk_etother)).setVisibility(v.VISIBLE);
	    			  ((EditText)findViewById(R.id.obs12crk_etother)).requestFocus();
				  }
				  else
				  {
					  cf.clearother(((EditText)findViewById(R.id.obs12crk_etother)));
				  }
				break;
	    	case R.id.obs12loc5: /** CHECK BOX - LOCATION OF CRACK 12 **/
	    		 if(obs12chkloc[4].isChecked())
				  {
	    			  ((EditText)findViewById(R.id.obs12loc_etother)).setVisibility(v.VISIBLE);
	    			  ((EditText)findViewById(R.id.obs12loc_etother)).requestFocus();
				  }
				  else
				  {
					  cf.clearother(((EditText)findViewById(R.id.obs12loc_etother)));
				  }
				break;
	    	case R.id.obs12wid5: /** CHECK BOX - WIDTH OF CRACK 12 **/
	    		 if(obs12chkwidt[4].isChecked())
				  {
	    			  ((EditText)findViewById(R.id.obs12wid_etother)).setVisibility(v.VISIBLE);
	    			  ((EditText)findViewById(R.id.obs12wid_etother)).requestFocus();
				  }
				  else
				  {
					  cf.clearother(((EditText)findViewById(R.id.obs12wid_etother)));
				  }
				break;
	    	case R.id.obs12caus7: /** CHECK BOX - POSSIBLE CAUSE  12 **/
	    		 if(obs12chkcaus[6].isChecked())
				  {
	    			  ((EditText)findViewById(R.id.obs12caus7_etother)).setVisibility(v.VISIBLE);
	    			  ((EditText)findViewById(R.id.obs12caus7_etother)).requestFocus();
				  }
				  else
				  {
					  cf.clearother(((EditText)findViewById(R.id.obs12caus7_etother)));
				  }
				break;
	    	case R.id.obs13crk5: /** CHECK BOX - TYPE OF CRACK 13 **/
	    		 if(obs13chkcrck[4].isChecked())
				  {
	    			  ((EditText)findViewById(R.id.obs13crk_etother)).setVisibility(v.VISIBLE);
	    			  ((EditText)findViewById(R.id.obs13crk_etother)).requestFocus();
				  }
				  else
				  {
					  cf.clearother(((EditText)findViewById(R.id.obs13crk_etother)));
				  }
				break;
	    	case R.id.obs13loc5: /** CHECK BOX - LOCATION OF CRACK 13 **/
	    		 if(obs13chkloc[4].isChecked())
				  {
	    			  ((EditText)findViewById(R.id.obs13loc_etother)).setVisibility(v.VISIBLE);
	    			  ((EditText)findViewById(R.id.obs13loc_etother)).requestFocus();
				  }
				  else
				  {
					  cf.clearother(((EditText)findViewById(R.id.obs13loc_etother)));
				  }
				break;
	    	case R.id.obs13wid5: /** CHECK BOX - WIDTH OF CRACK 13 **/
	    		 if(obs13chkwidt[4].isChecked())
				  {
	    			  ((EditText)findViewById(R.id.obs13wid_etother)).setVisibility(v.VISIBLE);
	    			  ((EditText)findViewById(R.id.obs13wid_etother)).requestFocus();
				  }
				  else
				  {
					  cf.clearother(((EditText)findViewById(R.id.obs13wid_etother)));
				  }
				break;
	    	case R.id.obs13caus7: /** CHECK BOX - POSSIBLE CAUSE  13 **/
	    		 if(obs13chkcaus[6].isChecked())
				  {
	    			  ((EditText)findViewById(R.id.obs13caus7_etother)).setVisibility(v.VISIBLE);
	    			  ((EditText)findViewById(R.id.obs13caus7_etother)).requestFocus();
				  }
				  else
				  {
					  cf.clearother(((EditText)findViewById(R.id.obs13caus7_etother)));
				  }
				break;
	    	case R.id.save:
	    		if(obs8rdgtxt.equals(""))
	    		{
	    			cf.ShowToast("Please select the option for Question No.8.", 0);
	    		}
	    		else
	    		{
	    			if(((EditText)findViewById(R.id.obs8comment)).getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.8 */
					{
						cf.ShowToast("Please enter Question No.8 Comments.", 0);
						cf.setFocus(((EditText)findViewById(R.id.obs8comment)));
					}
					else
					{
						if(obs9rdgtxt.equals(""))
			    		{
			    			cf.ShowToast("Please select the option for Question No.9.", 0);
			    		}
			    		else
			    		{
			    			if(((EditText)findViewById(R.id.obs9comment)).getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.9 */
							{
								cf.ShowToast("Please enter Question No.9 Comments.", 0);
								cf.setFocus(((EditText)findViewById(R.id.obs9comment)));
							}
							else
							{
								Chk_obs10Validation();
							}
			    		}
					}
	    		}
	    		break;
	    	case R.id.shwobs8:
				  hidelayouts();
				  ((ImageView)findViewById(R.id.shwobs8)).setVisibility(cf.v1.GONE);
				  ((ImageView)findViewById(R.id.hdobs8)).setVisibility(cf.v1.VISIBLE);
				  ((LinearLayout)findViewById(R.id.obs8lin)).setVisibility(cf.v1.VISIBLE);
				  ((TextView)findViewById(R.id.fsttxt)).requestFocus();
				  break;
	    	case R.id.hdobs8:
				((ImageView)findViewById(R.id.shwobs8)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdobs8)).setVisibility(cf.v1.GONE);
				((LinearLayout)findViewById(R.id.obs8lin)).setVisibility(cf.v1.GONE);
				break;
	    	case R.id.shwobs9:
				  hidelayouts();
				  ((ImageView)findViewById(R.id.shwobs9)).setVisibility(cf.v1.GONE);
				  ((ImageView)findViewById(R.id.hdobs9)).setVisibility(cf.v1.VISIBLE);
				  ((LinearLayout)findViewById(R.id.obs9lin)).setVisibility(cf.v1.VISIBLE);
				  ((TextView)findViewById(R.id.sectxt)).requestFocus();
				  break;
	    	case R.id.hdobs9:
				((ImageView)findViewById(R.id.shwobs9)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdobs9)).setVisibility(cf.v1.GONE);
				((LinearLayout)findViewById(R.id.obs9lin)).setVisibility(cf.v1.GONE);
				break;
	    	case R.id.shwobs10:
				  hidelayouts();
				  ((ImageView)findViewById(R.id.shwobs10)).setVisibility(cf.v1.GONE);
				  ((ImageView)findViewById(R.id.hdobs10)).setVisibility(cf.v1.VISIBLE);
				  ((LinearLayout)findViewById(R.id.obs10lin)).setVisibility(cf.v1.VISIBLE);
				  ((TextView)findViewById(R.id.thdtxt)).requestFocus();
				  break;
	    	case R.id.hdobs10:
				((ImageView)findViewById(R.id.shwobs10)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdobs10)).setVisibility(cf.v1.GONE);
				((LinearLayout)findViewById(R.id.obs10lin)).setVisibility(cf.v1.GONE);
				break;
	    	case R.id.shwobs11:
				  hidelayouts();
				  ((ImageView)findViewById(R.id.shwobs11)).setVisibility(cf.v1.GONE);
				  ((ImageView)findViewById(R.id.hdobs11)).setVisibility(cf.v1.VISIBLE);
				  ((LinearLayout)findViewById(R.id.obs11lin)).setVisibility(cf.v1.VISIBLE);
				  ((TextView)findViewById(R.id.fourtxt)).requestFocus();
				  break;
	    	case R.id.hdobs11:
				((ImageView)findViewById(R.id.shwobs11)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdobs11)).setVisibility(cf.v1.GONE);
				((LinearLayout)findViewById(R.id.obs11lin)).setVisibility(cf.v1.GONE);
				break;
	    	case R.id.shwobs12:
				  hidelayouts();
				  ((ImageView)findViewById(R.id.shwobs12)).setVisibility(cf.v1.GONE);
				  ((ImageView)findViewById(R.id.hdobs12)).setVisibility(cf.v1.VISIBLE);
				  ((LinearLayout)findViewById(R.id.obs12lin)).setVisibility(cf.v1.VISIBLE);
				  ((TextView)findViewById(R.id.fivtxt)).requestFocus();
				  break;
	    	case R.id.hdobs12:
				((ImageView)findViewById(R.id.shwobs12)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdobs12)).setVisibility(cf.v1.GONE);
				((LinearLayout)findViewById(R.id.obs12lin)).setVisibility(cf.v1.GONE);
				break;
	    	case R.id.shwobs13:
				  hidelayouts();
				  ((ImageView)findViewById(R.id.shwobs13)).setVisibility(cf.v1.GONE);
				  ((ImageView)findViewById(R.id.hdobs13)).setVisibility(cf.v1.VISIBLE);
				  ((LinearLayout)findViewById(R.id.obs13lin)).setVisibility(cf.v1.VISIBLE);
				  ((TextView)findViewById(R.id.sixtxt)).requestFocus();
				  break;
	    	case R.id.hdobs13:
				((ImageView)findViewById(R.id.shwobs13)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdobs13)).setVisibility(cf.v1.GONE);
				((LinearLayout)findViewById(R.id.obs13lin)).setVisibility(cf.v1.GONE);
				break;
	    	 case R.id.obs8_help: /** HELP FOR OBSERVATION8**/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>Pool deck - cracking /settlement is a common finding on almost all homes with a pool " +
				  		"incorporating a concrete surface - finish.  Most times the cracking is " +
				  		"related to initial settlement and shrinkage whereby the ground " +
				  		"underneath the slab was not properly compacted or the thickness of " +
				  		"concrete/construction workmanship was not correct.</p><p>Pool deck " +
				  		"cracking of all nature must be recorded, photographed and outlined " +
				  		"within this report.  If hairline fractures are noted at the corners, " +
				  		"these are typical.  Areas of more serious concern are those where " +
				  		"displacement is evident.</p><p>Displacement is where one side of the " +
				  		"patio/slab cracking is higher or uneven compared to the opposite side of " +
				  		"the crack.  Particular attention should be drawn to the pool shell where " +
				  		"it meets the pool deck to ensure the pool is properly installed, level " +
				  		"and plumb and any holes noted are not corresponding on the pool " +
				  		"structure.</p><p>Inspectors should compare the water level with the top " +
				  		"tile or perimeter of the pool.  If level and plumb, the water level will " +
				  		"be the same distance around the perimeter, measured to the closest " +
				  		"tile.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.obs9_help: /** HELP FOR OBSERVATION9**/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>If pools are present, " +
				  		"Inspectors should take a 4\" spirit level on the pool perimeter " +
				  		"and photograph.  Inspectors can do this by reviewing the water line, to " +
				  		"check for out of plumb compared to the perimeter tile.  The distance " +
				  		"between the top of the water and the coping or tile should be the same " +
				  		"throughout the perimeter of the pool.  Any evidence of displacement or " +
				  		"unevenness should be recorded as a potentially poorly constructed or off " +
				  		"level pool or other settlement issue.</p><p>Settling pools will also " +
				  		"change electrical and plumbing lines causing additional damage.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.obs10_help: /** HELP FOR OBSERVATION10**/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>An inadequately " +
				  		"ventilated crawlspace can cause potential rot and decay to the main " +
				  		"floor structure and unevenness to the floor as a result of floor " +
				  		"structure failure.  This is particularly prevalent to areas where the " +
				  		"undercarriage is exposed with limited ventilation and the water table is " +
				  		"high or standing water was present within the crawlspace " +
				  		"area.</p><p>Proper ventilation is crucial for the life cycle of the " +
				  		"building in question.</p><p>Any issues associated with ventilation or " +
				  		"lack thereof should be recorded and photographed accordingly.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.obs10a_help: /** HELP FOR OBSERVATION10 - OPTIONA **/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>Inspectors need to pay " +
				  		"close attention to all exterior wall surfaces on every floor level, " +
				  		"around every opening, building corners, at roof level, etc. to ensure " +
				  		"walls are not leaning or off plumb in any area.</p><p>A 4 foot level " +
				  		"should be used and photographed to verify the inspector\'s " +
				  		"findings in relation to the exterior wall survey.</p><p>If the Inspector " +
				  		"is unable to determine or evaluate the entire exterior area because of " +
				  		"the type of structure, zero lot line or other, this needs to be " +
				  		"identified in the comment area accordingly.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.obs10b_help: /** HELP FOR OBSERVATION10 - OPTIONB **/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>All horizontal surfaces, " +
				  		"including windowsills, door openings, window openings, decorative " +
				  		"features or any other feature where horizontal surfaces are present on " +
				  		"the exterior of a structure should be checked for any unevenness as a " +
				  		"result of settlement.  Inspectors need to photograph horizontal sills " +
				  		"with a small level (torpedo level) to verify to the quality assurance " +
				  		"department that sills were or were not affected and comment " +
				  		"accordingly.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.obs10c_help: /** HELP FOR OBSERVATION10 - OPTIONC **/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>Inspectors need to pay " +
				  		"close attention to the exterior wall surfaces/finishes of every floor " +
				  		"level, for any evidence of bulging or unevenness.</p><p>A 4 foot level " +
				  		"should be used and photographed to verify the inspector\'s " +
				  		"findings in relation to the exterior wall survey.</p><p>If the Inspector " +
				  		"is unable to determine or evaluate the entire exterior area because of " +
				  		"the type of structure, zero lot line or other, this should be clearly " +
				  		"identified in the comment area.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.obs10d_help: /** HELP FOR OBSERVATION10 - OPTIOND **/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>Inspectors are required " +
				  		"to observe all openings on the exterior of the structure, example tests " +
				  		"using a 4\"; spirit level and verify using photographs and level " +
				  		"in place and record that openings were not out of square or uneven.  " +
				  		"Photograph annotations should clearly say \"No openings or sills " +
				  		"were found out of square or not level\".</p><p>Inspectors are " +
				  		"required to sample or examine using a 4\" spirit level and a " +
				  		"torpedo level as needed, a representative sampling of openings and sills " +
				  		"- horizontal surface  including a minimum of 1 per elevation and " +
				  		"1 per floor.</p><p>If any openings are not visible or accessible based " +
				  		"on the perimeter or surrounding features such as large trees, " +
				  		"structures, etc., this should be outlined within the Comment " +
				  		"area/photograph section.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.obs10e_help: /** HELP FOR OBSERVATION10 - OPTIONE **/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>Inspectors should pay " +
				  		"close attention to all finishes throughout the exterior of the home to " +
				  		"determine if any damage exists that may be associated with settlement " +
				  		"issues.</p><p>For example, cracks in stucco associated with structural " +
				  		"settlement should be outlined.  Scrapes and scratches relating to " +
				  		"lawnmower damage are not relevant and should merely be " +
				  		"photographed.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
		     case R.id.obs11_help: /** HELP FOR OBSERVATION11**/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>Inspectors are required " +
				  		"to determine what type of separation cracks are present by either " +
				  		"selecting, New Addition, Differing Materials or outlining using the " +
				  		"Other checkbox selection and describing what separation cracks were " +
				  		"noted.</p><p>Separation cracks are normally associated with additions " +
				  		"where new and old constructions meet, differing materials adjoining, " +
				  		"where siding possibly meets stucco and plaster applications or around " +
				  		"windows where windows may be separating or caulk splitting.</p><p>All " +
				  		"separation cracks should be properly identified, photographed and " +
				  		"commented if present.  If any of these cracks are possibly related to " +
				  		"sinkhole or major structural issues, these should be outlined clearly on " +
				  		"the photograph annotations and comment areas.  Use a 4\" spirit " +
				  		"level or torpedo level in photographs as needed to augment your " +
				  		"findings.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.obs12_help: /** HELP FOR OBSERVATION12**/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>All Inspectors need to " +
				  		"review all openings present on the exterior of the home, including " +
				  		"windows and doors.  Cracking of any nature, including diagonal cracking " +
				  		"from top corners, cracking along the bottom or at the junction of the " +
				  		"window frames and openings needs to be recorded, photographed and " +
				  		"commented.</p><p>Typical hairline fractures are to be expected and this " +
				  		"must be outlined via the various selections and the probable cause if " +
				  		"noted.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
			  case R.id.obs13_help: /** HELP FOR OBSERVATION13**/
				  cf.alerttitle="HELP";
				  cf.alertcontent="<p>Inspectors are required " +
				  		"to observe the conditions associated with all adjoining property which " +
				  		"includes properties on both sides, back and front, where visible and " +
				  		"accessible.</p><p>Inspectors should walk along the footpath on the front " +
				  		"of adjoining homes to verify if any issues exist that may be associated " +
				  		"with potential sinkhole issues.</p><p>Based on the limitations at the " +
				  		"time of inspection, Inspectors should clearly outline the extent of " +
				  		"inspections conducted and any limitations related to these " +
				  		"observations.</p><p>At no time should Inspectors be entering adjoining " +
				  		"homes, properties as part of this inspection service.</p>";
				  cf.showhelp(cf.alerttitle,cf.alertcontent);
				  break;
	    	}
    }
	private void Chk_obs10Validation() {
		// TODO Auto-generated method stub
		if(obs10rdgtxt.equals(""))
		{
			cf.ShowToast("Please select the option for Question No.10.", 0);
		}
		else
		{
			if(obs10rdgtxt.equals("Yes"))
			{
				 obs10chkcrk_val= cf.getselected_chk(obs10chkcrck);
		    	 String chk_Value10crkother=(obs10chkcrck[obs10chkcrck.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.obs10crk_etother)).getText().toString():""; // append the other text value in to the selected option
		    	 obs10chkcrk_val+=chk_Value10crkother;
		    	 if(obs10chkcrk_val.equals(""))
				 {
					 cf.ShowToast("Please select Type of cracks for Question No.10." , 0);
				 }
		    	 else
		    	 {
		    		 if(obs10chkcrck[obs10chkcrck.length-1].isChecked())
					 {
						 if(chk_Value10crkother.trim().equals("&#40;"))
						 {
							 cf.ShowToast("Please enter the Other text for Type of cracks for Question No.10.", 0);
							 cf.setFocus(((EditText)findViewById(R.id.obs10crk_etother)));
						 }
						 else
						 {
							 chk_location10();	
						 }
					 }
					 else
					 {
						 chk_location10();
					 }
		    	 }
			}
			else
			{
				chk_obs10comments();
			}
		}
	}
	private void chk_location10() {


		// TODO Auto-generated method stub
	 obs10chkloc_val= cf.getselected_chk(obs10chkloc);
   	 String chk_Value10locother=(obs10chkloc[obs10chkloc.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.obs10loc_etother)).getText().toString():""; // append the other text value in to the selected option
   	 obs10chkloc_val+=chk_Value10locother;
   	 if(obs10chkloc_val.equals(""))
		 {
			 cf.ShowToast("Please select Location of cracks for Question No.10." , 0);
		 }
   	 else
   	 {
   		 if(obs10chkloc[obs10chkloc.length-1].isChecked())
			 {
				 if(chk_Value10locother.trim().equals("&#40;"))
				 {
					 cf.ShowToast("Please enter the Other text for Location of cracks for Question No.10.", 0);
					 cf.setFocus(((EditText)findViewById(R.id.obs10loc_etother)));
				 }
				 else
				 {
					 chk_width10();	
				 }
			 }
			 else
			 {
				 chk_width10();
			 }
   	 }
	}
	private void chk_width10() {

		// TODO Auto-generated method stub
		 obs10chkwidt_val= cf.getselected_chk(obs10chkwidt);
	   	 String chk_Value10widother=(obs10chkwidt[obs10chkwidt.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.obs10wid_etother)).getText().toString():""; // append the other text value in to the selected option
	   	obs10chkwidt_val+=chk_Value10widother;
	   	 if(obs10chkwidt_val.equals(""))
			 {
				 cf.ShowToast("Please select the Approximate width of cracks for Question No.10." , 0);
			 }
	   	 else
	   	 {
	   		 if(obs10chkwidt[obs10chkwidt.length-1].isChecked())
				 {
					 if(chk_Value10widother.trim().equals("&#40;"))
					 {
						 cf.ShowToast("Please enter the Other text for Approximate width of cracks for Question No.10.", 0);
						 cf.setFocus(((EditText)findViewById(R.id.obs10wid_etother)));
					 }
					 else
					 {
						chk_len10();	
					 }
				 }
				 else
				 {
					 chk_len10();
				 }
	   	 }
	}
	private void chk_len10() {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
				if(((EditText)findViewById(R.id.obs10_etlen)).getText().toString().trim().equals("")) /*CHECK LENGTH QUESTION NO.10 */
				{
					cf.ShowToast("Please enter the Approximate Length of cracks for Question No.10.", 0);
					cf.setFocus(((EditText)findViewById(R.id.obs10_etlen)));
				}
				else
				{
					obs10chkcond_val= cf.getselected_chk(obs10chkcond);
					 if(obs10chkcond_val.equals(""))
					 {
						 cf.ShowToast("Please select Condition of cracks noted for Question No.10." , 0);
					 }
			   	    else
			   	     {
			   	    	chk_possiblecause10();
			   	     }
				}
	}
	private void chk_possiblecause10() {

		// TODO Auto-generated method stub
		 obs10chkcaus_val= cf.getselected_chk(obs10chkcaus);
	   	 String chk_Value10causother=(obs10chkcaus[obs10chkcaus.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.obs10caus7_etother)).getText().toString():""; // append the other text value in to the selected option
	   	obs10chkcaus_val+=chk_Value10causother;
	   	 if(obs10chkcaus_val.equals(""))
			 {
				 cf.ShowToast("Please select Possible cause of settlement based on limited survey for Question No.10." , 0);
			 }
	   	 else
	   	 {
	   		 if(obs10chkcaus[obs10chkcaus.length-1].isChecked())
				 {
					 if(chk_Value10causother.trim().equals("&#40;"))
					 {
						 cf.ShowToast("Please enter the Other text for Possible cause of settlement based on limited survey for Question No.10.", 0);
						 cf.setFocus(((EditText)findViewById(R.id.obs10caus7_etother)));
					 }
					 else
					 {
						chk_obs10comments();	
					 }
				 }
				 else
				 {
					 chk_obs10comments();
				 }
	   	 }
	}
	private void chk_obs10comments() {


		// TODO Auto-generated method stub
		if(((EditText)findViewById(R.id.obs10comment)).getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.10 */
		{
			cf.ShowToast("Please enter Question No.10 Comments.", 0);
			cf.setFocus(((EditText)findViewById(R.id.obs10comment)));
		}
		else
		{
			if(obs10ardgtxt.equals(""))
    		{
    			cf.ShowToast("Please select the option for Question No.10(a).", 0);
    		}
    		else
    		{
    			if(((EditText)findViewById(R.id.obs10acomment)).getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.10(a) */
				{
					cf.ShowToast("Please enter Question No.10(a) Comments.", 0);
					cf.setFocus(((EditText)findViewById(R.id.obs10acomment)));
				}
    			else
    			{
    				if(obs10brdgtxt.equals(""))
    	    		{
    	    			cf.ShowToast("Please select the option for Question No.10(b).", 0);
    	    		}
    	    		else
    	    		{
    	    			if(((EditText)findViewById(R.id.obs10bcomment)).getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.10(b) */
    					{
    						cf.ShowToast("Please enter Question No.10(b) Comments.", 0);
    						cf.setFocus(((EditText)findViewById(R.id.obs10bcomment)));
    					}
    	    			else
    	    			{
    	    				if(obs10crdgtxt.equals(""))
    	    	    		{
    	    	    			cf.ShowToast("Please select the option for Question No.10(c).", 0);
    	    	    		}
    	    	    		else
    	    	    		{
    	    	    			if(((EditText)findViewById(R.id.obs10ccomment)).getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.10(c) */
    	    					{
    	    						cf.ShowToast("Please enter Question No.10(c) Comments.", 0);
    	    						cf.setFocus(((EditText)findViewById(R.id.obs10ccomment)));
    	    					}
    	    	    			else
    	    	    			{
    	    	    				if(obs10drdgtxt.equals(""))
    	    	    	    		{
    	    	    	    			cf.ShowToast("Please select the option for Question No.10(d).", 0);
    	    	    	    		}
    	    	    	    		else
    	    	    	    		{
    	    	    	    			if(((EditText)findViewById(R.id.obs10dcomment)).getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.10(d) */
    	    	    					{
    	    	    						cf.ShowToast("Please enter the comments for Question No.10(d).", 0);
    	    	    						cf.setFocus(((EditText)findViewById(R.id.obs10dcomment)));
    	    	    					}
    	    	    	    			else
    	    	    	    			{
    	    	    	    				if(obs10erdgtxt.equals(""))
    	    	    	    	    		{
    	    	    	    	    			cf.ShowToast("Please select the option for Question No.10(e).", 0);
    	    	    	    	    		}
    	    	    	    	    		else
    	    	    	    	    		{
    	    	    	    	    			if(((EditText)findViewById(R.id.obs10ecomment)).getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.10(e) */
    	    	    	    					{
    	    	    	    						cf.ShowToast("Please enter the comments for Question No.10(e).", 0);
    	    	    	    						cf.setFocus(((EditText)findViewById(R.id.obs10ecomment)));
    	    	    	    					}
    	    	    	    	    			else
    	    	    	    	    			{
    	    	    	    	    				chk_obs11Validation();
    	    	    	    	    			}
    	    	    	    	    		}
    	    	    	    			}
    	    	    	    		}
    	    	    			}
    	    	    		}
    	    			}
    	    		}
    			}
    		}
		}
	}
	private void chk_obs11Validation() {


		// TODO Auto-generated method stub
		if(obs11rdgtxt.equals(""))
		{
			cf.ShowToast("Please select the option for Question No.11.", 0);
		}
		else
		{
			if(obs11rdgtxt.equals("Yes"))
			{
				 obs11chksep_val= cf.getselected_chk(obs11sep);
		    	 String chk_Value11sepother=(obs11sep[obs11sep.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.obs11chk3_etother)).getText().toString():""; // append the other text value in to the selected option
		    	 obs11chksep_val+=chk_Value11sepother;
		    	 if(obs11chksep_val.equals(""))
				 {
					 cf.ShowToast("Please select the option for Question No.11." , 0);
				 }
		    	 else
		    	 {
		    		 if(obs11sep[obs11sep.length-1].isChecked())
					 {
						 if(chk_Value11sepother.trim().equals("&#40;"))
						 {
							 cf.ShowToast("Please enter the Other text for Question No.11.", 0);
							 cf.setFocus(((EditText)findViewById(R.id.obs11chk3_etother)));
						 }
						 else
						 {
							obs11_comments();	
						 }
					 }
					 else
					 {
						 obs11_comments();
					 }
		    	 }
			}
			else
			{
				obs11_comments();
			}
		}
	}
	private void obs11_comments() {


		// TODO Auto-generated method stub
		if(((EditText)findViewById(R.id.obs11comment)).getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.11 */
		{
			cf.ShowToast("Please enter the comments for Question No.11.", 0);
			cf.setFocus(((EditText)findViewById(R.id.obs11comment)));
		}
		else
		{
			if(obs12rdgtxt.equals(""))
			{
				cf.ShowToast("Please select the option for Question No.12.", 0);
			}
			else
			{
				if(obs12rdgtxt.equals("Yes"))
				{
					 obs12chkcrk_val= cf.getselected_chk(obs12chkcrck);
			    	 String chk_Value12crkother=(obs12chkcrck[obs12chkcrck.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.obs12crk_etother)).getText().toString():""; // append the other text value in to the selected option
			    	 obs12chkcrk_val+=chk_Value12crkother;
			    	 if(obs12chkcrk_val.equals(""))
					 {
						 cf.ShowToast("Please select Type of cracks for Question No.12." , 0);
					 }
			    	 else
			    	 {
			    		 if(obs12chkcrck[obs12chkcrck.length-1].isChecked())
						 {
							 if(chk_Value12crkother.trim().equals("&#40;"))
							 {
								 cf.ShowToast("Please enter the Other text for Type of cracks for Question No.12.", 0);
								 cf.setFocus(((EditText)findViewById(R.id.obs12crk_etother)));
							 }
							 else
							 {
								chk_location12();	
							 }
						 }
						 else
						 {
							 chk_location12();
						 }
			    	 }
				}
				else
				{
					chk_obs12comments();
				}
			}
		}
	}
	private void chk_obs12comments() {
		// TODO Auto-generated method stub
		if(((EditText)findViewById(R.id.obs12comment)).getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.12 */
		{
			cf.ShowToast("Please enter the comments for Question No.12.", 0);
			((EditText)findViewById(R.id.obs12comment)).requestFocus();
		}
		else
		{
			if(obs13rdgtxt.equals(""))
			{
				cf.ShowToast("Please select the option for Question No.13.", 0);
			}
			else
			{
				if(obs13rdgtxt.equals("Yes"))
				{
					 obs13chkcrk_val= cf.getselected_chk(obs13chkcrck);
			    	 String chk_Value13crkother=(obs13chkcrck[obs13chkcrck.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.obs13crk_etother)).getText().toString():""; // append the other text value in to the selected option
			    	 obs13chkcrk_val+=chk_Value13crkother;
			    	 if(obs13chkcrk_val.equals(""))
					 {
						 cf.ShowToast("Please select Type of cracks for Question No.13." , 0);
					 }
			    	 else
			    	 {
			    		 if(obs13chkcrck[obs13chkcrck.length-1].isChecked())
						 {
							 if(chk_Value13crkother.trim().equals("&#40;"))
							 {
								 cf.ShowToast("Please enter the Other text for Type of cracks for Question No.13.", 0);
								 cf.setFocus(((EditText)findViewById(R.id.obs13crk_etother)));
							 }
							 else
							 {
								chk_location13();	
							 }
						 }
						 else
						 {
							 chk_location13();
						 }
			    	 }
				}
				else
				{
					chk_obs13comments();
				}
			}
		}
	}
	private void chk_obs13comments() {

		// TODO Auto-generated method stub
		if(((EditText)findViewById(R.id.obs13comment)).getText().toString().trim().equals("")) /*CHECK COMMENTS QUESTION NO.13 */
		{
			cf.ShowToast("Please enter the comments for Question No.13.", 0);
			cf.setFocus(((EditText)findViewById(R.id.obs13comment)));
		}
		else
		{
			obs2_Insert();
		}
	}
	private void obs2_Insert() {

		// TODO Auto-generated method stub
		Cursor OBS2_save=null;
		try
		{
			OBS2_save=cf.SelectTablefunction(cf.SINK_Obs2tbl, " where fld_srid='"+cf.selectedhomeid+"'");
			if(OBS2_save.getCount()>0)
			{
				try
				{
					cf.arr_db.execSQL("UPDATE "+cf.SINK_Obs2tbl+ " set PoolDeckSlabNoted='"+obs8rdgtxt+"',PoolDeckSlabComments='"+cf.encode(((EditText)findViewById(R.id.obs8comment)).getText().toString())+"',"+
				               "PoolShellPlumbLevel='"+obs9rdgtxt+"',PoolShellPlumbLevelComments='"+cf.encode(((EditText)findViewById(R.id.obs9comment)).getText().toString())+"',"+
							   "ExcessiveSettlement='"+obs10rdgtxt+"',ExcessiveSettlementTypeofCrack='"+cf.encode(obs10chkcrk_val)+"',ExcessiveSettlementLocation='"+cf.encode(obs10chkloc_val)+"',"+
				               "ExcessiveSettlementWidthofCrack='"+cf.encode(obs10chkwidt_val)+"',ExcessiveSettlementLengthofCrack='"+cf.encode(((EditText)findViewById(R.id.obs10_etlen)).getText().toString())+"',"+
							   "ExcessiveSettlementCleanliness='"+cf.encode(obs10chkcond_val)+"',ExcessiveSettlementProbableCause='"+cf.encode(obs10chkcaus_val)+"',"+
				               "ExcessiveSettlementComments='"+cf.encode(((EditText)findViewById(R.id.obs10comment)).getText().toString())+"',WallLeaningNoted='"+obs10ardgtxt+"',"+
							   "WallLeaningComments='"+cf.encode(((EditText)findViewById(R.id.obs10acomment)).getText().toString())+"',WallsVisiblyNotLevel='"+obs10brdgtxt+"',"+
				               "WallsVisiblyNotLevelComments='"+cf.encode(((EditText)findViewById(R.id.obs10bcomment)).getText().toString())+"',WallsVisiblyBulgingNoted='"+obs10crdgtxt+"',"+
					           "WallsVisiblyBulgingComments='"+cf.encode(((EditText)findViewById(R.id.obs10ccomment)).getText().toString())+"',OpeningsOutofSquare='"+obs10drdgtxt+"',"+
				               "OpeningsOutofSquareComments='"+cf.encode(((EditText)findViewById(R.id.obs10dcomment)).getText().toString())+"',DamagedFinishesNoted='"+obs10erdgtxt+"',"+
					           "DamagedFinishesComments='"+cf.encode(((EditText)findViewById(R.id.obs10ecomment)).getText().toString())+"',SeperationCrackNoted='"+obs11rdgtxt+"',"+
				               "SeperationCrackTypeofCrack='"+cf.encode(obs11chksep_val)+"',SeperationCrackComments='"+cf.encode(((EditText)findViewById(R.id.obs11comment)).getText().toString())+"',"+
					           "ExteriorOpeningCracksNoted='"+obs12rdgtxt+"',ExteriorOpeningCracksTypeofCrack='"+cf.encode(obs12chkcrk_val)+"',ExteriorOpeningCracksLocation='"+cf.encode(obs12chkloc_val)+"',"+
				               "ExteriorOpeningCracksWidthofCrack='"+cf.encode(obs12chkwidt_val)+"',ExteriorOpeningCracksLengthofCrack='"+cf.encode(((EditText)findViewById(R.id.obs12_etlen)).getText().toString())+"',"+
					           "ExteriorOpeningCracksCleanliness='"+cf.encode(obs12chkcond_val)+"',ExteriorOpeningCracksProbableCause='"+cf.encode(obs12chkcaus_val)+"',"+
				               "ExteriorOpeningCracksComments='"+cf.encode(((EditText)findViewById(R.id.obs12comment)).getText().toString())+"',ObservationSettlementNoted='"+obs13rdgtxt+"',"+
					           "ObservationSettlementTypeofCrack='"+cf.encode(obs13chkcrk_val)+"',ObservationSettlementLocation='"+cf.encode(obs13chkloc_val)+"',"+
				               "ObservationSettlementWidthofCrack='"+cf.encode(obs13chkwidt_val)+"',ObservationSettlementLengthofCrack='"+cf.encode(((EditText)findViewById(R.id.obs13_etlen)).getText().toString())+"',"+
					           "ObservationSettlementCleanliness='"+cf.encode(obs13chkcond_val)+"',ObservationSettlementProbableCause='"+cf.encode(obs13chkcaus_val)+"',"+		               		
					           "ObservationSettlementComments='"+cf.encode(((EditText)findViewById(R.id.obs13comment)).getText().toString())+"' where fld_srid='"+cf.selectedhomeid+"'");
					cf.ShowToast("Observations(8-13) updated successfully.", 1);
				    ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
				     cf.goclass(54);
				}
				catch (Exception E)
				{
					String strerrorlog="Updating the Observation2 - Sinkhole";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
			}
			else
			{
				try
				{
					cf.arr_db.execSQL("INSERT INTO "
							+ cf.SINK_Obs2tbl
							+ " (fld_srid,PoolDeckSlabNoted,PoolDeckSlabComments,PoolShellPlumbLevel,PoolShellPlumbLevelComments,ExcessiveSettlement,"+
							   "ExcessiveSettlementTypeofCrack,ExcessiveSettlementLocation,ExcessiveSettlementWidthofCrack,"+
							   "ExcessiveSettlementLengthofCrack,ExcessiveSettlementCleanliness,ExcessiveSettlementProbableCause,"+
				               "ExcessiveSettlementComments,WallLeaningNoted,WallLeaningComments,WallsVisiblyNotLevel,"+
				               "WallsVisiblyNotLevelComments,WallsVisiblyBulgingNoted,WallsVisiblyBulgingComments,OpeningsOutofSquare,"+
				               "OpeningsOutofSquareComments,DamagedFinishesNoted,DamagedFinishesComments,SeperationCrackNoted,SeperationCrackTypeofCrack,"+
				               "SeperationCrackComments,ExteriorOpeningCracksNoted,ExteriorOpeningCracksTypeofCrack,ExteriorOpeningCracksLocation,"+
				               "ExteriorOpeningCracksWidthofCrack,ExteriorOpeningCracksLengthofCrack,ExteriorOpeningCracksCleanliness,"+
				               "ExteriorOpeningCracksProbableCause,ExteriorOpeningCracksComments,ObservationSettlementNoted,ObservationSettlementTypeofCrack,"+
				               "ObservationSettlementLocation,ObservationSettlementWidthofCrack,ObservationSettlementLengthofCrack,"+
					           "ObservationSettlementCleanliness,ObservationSettlementProbableCause,ObservationSettlementComments)"
							+ "VALUES ('"+cf.selectedhomeid+"','"+obs8rdgtxt+"','"+cf.encode(((EditText)findViewById(R.id.obs8comment)).getText().toString())+"',"+
				               "'"+obs9rdgtxt+"','"+cf.encode(((EditText)findViewById(R.id.obs9comment)).getText().toString())+"',"+
							   "'"+obs10rdgtxt+"','"+cf.encode(obs10chkcrk_val)+"','"+cf.encode(obs10chkloc_val)+"',"+
				               "'"+cf.encode(obs10chkwidt_val)+"','"+cf.encode(((EditText)findViewById(R.id.obs10_etlen)).getText().toString())+"',"+
							   "'"+cf.encode(obs10chkcond_val)+"','"+cf.encode(obs10chkcaus_val)+"',"+
				               "'"+cf.encode(((EditText)findViewById(R.id.obs10comment)).getText().toString())+"','"+obs10ardgtxt+"',"+
							   "'"+cf.encode(((EditText)findViewById(R.id.obs10acomment)).getText().toString())+"','"+obs10brdgtxt+"',"+
				               "'"+cf.encode(((EditText)findViewById(R.id.obs10bcomment)).getText().toString())+"','"+obs10crdgtxt+"',"+
					           "'"+cf.encode(((EditText)findViewById(R.id.obs10ccomment)).getText().toString())+"','"+obs10drdgtxt+"',"+
				               "'"+cf.encode(((EditText)findViewById(R.id.obs10dcomment)).getText().toString())+"','"+obs10erdgtxt+"',"+
					           "'"+cf.encode(((EditText)findViewById(R.id.obs10ecomment)).getText().toString())+"','"+obs11rdgtxt+"',"+
				               "'"+cf.encode(obs11chksep_val)+"','"+cf.encode(((EditText)findViewById(R.id.obs11comment)).getText().toString())+"',"+
					           "'"+obs12rdgtxt+"','"+cf.encode(obs12chkcrk_val)+"','"+cf.encode(obs12chkloc_val)+"',"+
				               "'"+cf.encode(obs12chkwidt_val)+"','"+cf.encode(((EditText)findViewById(R.id.obs12_etlen)).getText().toString())+"',"+
					           "'"+cf.encode(obs12chkcond_val)+"','"+cf.encode(obs12chkcaus_val)+"',"+
				               "'"+cf.encode(((EditText)findViewById(R.id.obs12comment)).getText().toString())+"','"+obs13rdgtxt+"',"+
					           "'"+cf.encode(obs13chkcrk_val)+"','"+cf.encode(obs13chkloc_val)+"',"+
				               "'"+cf.encode(obs13chkwidt_val)+"','"+cf.encode(((EditText)findViewById(R.id.obs13_etlen)).getText().toString())+"',"+
					           "'"+cf.encode(obs13chkcond_val)+"','"+cf.encode(obs13chkcaus_val)+"',"+		               		
					           "'"+cf.encode(((EditText)findViewById(R.id.obs13comment)).getText().toString())+"')");

					 cf.ShowToast("Observations(8-13) saved successfully.", 1);
					 ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE); 
					 cf.goclass(54);
			 	}
				catch (Exception E)
				{
					String strerrorlog="Inserting the Observation2 - Sinkhole";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
			}
		}
		catch (Exception E)
		{
			String strerrorlog="Checking the rows inserted in the Observation2 - Sinkhole table.";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+"table at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
	private void chk_location13() {
		// TODO Auto-generated method stub
		 obs13chkloc_val= cf.getselected_chk(obs13chkloc);
	   	 String chk_Value13locother=(obs13chkloc[obs13chkloc.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.obs13loc_etother)).getText().toString():""; // append the other text value in to the selected option
	   	 obs13chkloc_val+=chk_Value13locother;
	   	 if(obs13chkloc_val.equals(""))
			 {
				 cf.ShowToast("Please select Location of cracks for Question No.13." , 0);
			 }
	   	 else
	   	 {
	   		 if(obs13chkloc[obs13chkloc.length-1].isChecked())
				 {
					 if(chk_Value13locother.trim().equals("&#40;"))
					 {
						 cf.ShowToast("Please enter the Other text for Location of cracks for Question No.13.", 0);
						 cf.setFocus(((EditText)findViewById(R.id.obs13loc_etother)));
					 }
					 else
					 {
						chk_width13();	
					 }
				 }
				 else
				 {
					 chk_width13();
				 }
	   	 }
	}
	private void chk_width13() {
		// TODO Auto-generated method stub
		 obs13chkwidt_val= cf.getselected_chk(obs13chkwidt);
	   	 String chk_Value13widother=(obs13chkwidt[obs13chkwidt.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.obs13wid_etother)).getText().toString():""; // append the other text value in to the selected option
	   	obs13chkwidt_val+=chk_Value13widother;
	   	 if(obs13chkwidt_val.equals(""))
			 {
				 cf.ShowToast("Please select the Approximate width of cracks for Question No.13." , 0);
			 }
	   	 else
	   	 {
	   		 if(obs13chkwidt[obs13chkwidt.length-1].isChecked())
				 {
					 if(chk_Value13widother.trim().equals("&#40;"))
					 {
						 cf.ShowToast("Please enter the Other text for Approximate width of cracks for Question No.13.", 0);
						 cf.setFocus(((EditText)findViewById(R.id.obs13wid_etother)));
					 }
					 else
					 {
						 chk_len13();	
					 }
				 }
				 else
				 {
					 chk_len13();
				 }
	   	 }
	}
	private void chk_len13() {
		// TODO Auto-generated method stub
		if(((EditText)findViewById(R.id.obs13_etlen)).getText().toString().trim().equals("")) /*CHECK LENGTH QUESTION NO.13 */
		{
			cf.ShowToast("Please enter the Approximate Length for Question No.13.", 0);
			cf.setFocus(((EditText)findViewById(R.id.obs13_etlen)));
		}
		else
		{
			obs13chkcond_val= cf.getselected_chk(obs13chkcond);
			 if(obs13chkcond_val.equals(""))
			 {
				 cf.ShowToast("Please select Condition of cracks noted for Question No.13." , 0);
			 }
	   	    else
	   	     {
	   	    	chk_possiblecause13();
	   	     }
		}
	}
	private void chk_possiblecause13() {
		// TODO Auto-generated method stub
		 obs13chkcaus_val= cf.getselected_chk(obs13chkcaus);
	   	 String chk_Value13causother=(obs13chkcaus[obs13chkcaus.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.obs13caus7_etother)).getText().toString():""; // append the other text value in to the selected option
	   	obs13chkcaus_val+=chk_Value13causother;
	   	 if(obs13chkcaus_val.equals(""))
			 {
				 cf.ShowToast("Please select Possible cause of settlement based on limited survey for Question No.13." , 0);
			 }
	   	 else
	   	 {
	   		 if(obs13chkcaus[obs13chkcaus.length-1].isChecked())
				 {
					 if(chk_Value13causother.trim().equals("&#40;"))
					 {
						 cf.ShowToast("Please enter the Other text for Possible cause of settlement based on limited survey for Question No.13.", 0);
						 cf.setFocus(((EditText)findViewById(R.id.obs13caus7_etother)));
					 }
					 else
					 {
						 chk_obs13comments();	
					 }
				 }
				 else
				 {
					 chk_obs13comments();
				 }
	   	 }
	}
	private void chk_location12() {
		// TODO Auto-generated method stub
		 obs12chkloc_val= cf.getselected_chk(obs12chkloc);
	   	 String chk_Value12locother=(obs12chkloc[obs12chkloc.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.obs12loc_etother)).getText().toString():""; // append the other text value in to the selected option
	   	 obs12chkloc_val+=chk_Value12locother;
	   	 if(obs12chkloc_val.equals(""))
			 {
				 cf.ShowToast("Please select Location of cracks for Question No.12." , 0);
			 }
	   	 else
	   	 {
	   		 if(obs12chkloc[obs12chkloc.length-1].isChecked())
				 {
					 if(chk_Value12locother.trim().equals("&#40;"))
					 {
						 cf.ShowToast("Please enter the Other text for Location of cracks for Question No.12.", 0);
						 cf.setFocus(((EditText)findViewById(R.id.obs12loc_etother)));
					 }
					 else
					 {
						 chk_width12();	
					 }
				 }
				 else
				 {
					 chk_width12();
				 }
	   	 }
	}
	private void chk_width12() {

		// TODO Auto-generated method stub
		 obs12chkwidt_val= cf.getselected_chk(obs12chkwidt);
	   	 String chk_Value12widother=(obs12chkwidt[obs12chkwidt.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.obs12wid_etother)).getText().toString():""; // append the other text value in to the selected option
	   	obs12chkwidt_val+=chk_Value12widother;
	   	 if(obs12chkwidt_val.equals(""))
			 {
				 cf.ShowToast("Please select the Approximate width of cracks for Question No.12." , 0);
			 }
	   	 else
	   	 {
	   		 if(obs12chkwidt[obs12chkwidt.length-1].isChecked())
				 {
					 if(chk_Value12widother.trim().equals("&#40;"))
					 {
						 cf.ShowToast("Please enter the Other text for Approximate width of cracks for Question No.12.", 0);
						 cf.setFocus(((EditText)findViewById(R.id.obs12wid_etother)));
					 }
					 else
					 {
						chk_len12();	
					 }
				 }
				 else
				 {
					 chk_len12();
				 }
	   	 }
	}
	private void chk_len12() {
		// TODO Auto-generated method stub
		if(((EditText)findViewById(R.id.obs12_etlen)).getText().toString().trim().equals("")) /*CHECK LENGTH QUESTION NO.12 */
		{
			cf.ShowToast("Please enter the Approximate Length for Question No.12.", 0);
			cf.setFocus(((EditText)findViewById(R.id.obs12_etlen)));
		}
		else
		{
			obs12chkcond_val= cf.getselected_chk(obs12chkcond);
			 if(obs12chkcond_val.equals(""))
			 {
				 cf.ShowToast("Please select Condition of cracks noted for Question No.12." , 0);
			 }
	   	    else
	   	     {
	   	    	chk_possiblecause12();
	   	     }
		}
	}
	private void chk_possiblecause12() {

		// TODO Auto-generated method stub
		 obs12chkcaus_val= cf.getselected_chk(obs12chkcaus);
	   	 String chk_Value12causother=(obs12chkcaus[obs12chkcaus.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.obs12caus7_etother)).getText().toString():""; // append the other text value in to the selected option
	   	obs12chkcaus_val+=chk_Value12causother;
	   	 if(obs12chkcaus_val.equals(""))
			 {
				 cf.ShowToast("Please select Possible cause of settlement based on limited survey for Question No.12." , 0);
			 }
	   	 else
	   	 {
	   		 if(obs12chkcaus[obs12chkcaus.length-1].isChecked())
				 {
					 if(chk_Value12causother.trim().equals("&#40;"))
					 {
						 cf.ShowToast("Please enter the Other text for Possible cause of settlement based on limited survey for Question No.12.", 0);
						 cf.setFocus(((EditText)findViewById(R.id.obs12caus7_etother)));
					 }
					 else
					 {
						 chk_obs12comments();	
					 }
				 }
				 else
				 {
					 chk_obs12comments();
				 }
	   	 }
	}
	private void hidelayouts() {
		// TODO Auto-generated method stub
		((LinearLayout)findViewById(R.id.obs8lin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.obs9lin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.obs10lin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.obs11lin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.obs12lin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.obs13lin)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdobs8)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdobs9)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdobs10)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdobs11)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdobs12)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdobs13)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.shwobs8)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwobs9)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwobs10)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwobs11)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwobs12)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwobs13)).setVisibility(cf.v1.VISIBLE);
		
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
		     cf.goclass(52);
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}
}
