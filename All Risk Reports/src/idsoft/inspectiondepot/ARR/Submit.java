/*
   ************************************************************
   * Project: ALL RISK REPORT
   * Module : Submit.java
   * Creation History:
      Created By : Nalini on 10/20/2012
   * Modification History:
      Last Modified By : Gowri on 04/04/2013
   ************************************************************  
*/

package idsoft.inspectiondepot.ARR;
import idsoft.inspectiondepot.ARR.R;

import java.util.Random;


import android.R.bool;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Html;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TableRow;
import android.widget.TextView;

public class Submit extends Activity{
	CommonFunctions cf;String[] myString;String word;Resources res;
	EditText etsetword,etgetword;
	String FourpointAddendum="",B1802Addendum="",Comm1Addendum="",Comm2Addendum="",Comm3Addendum="";
	private static final Random rgenerator = new Random();
	boolean sbmtchk[] = {false,false,false,false,false,false,false,false,false,false};
	String sbmtval[] = {"Four Point Inspection","Roof Survey","B1 1802 Inspection",
			             "Commercial Type I","Commercial Type II","Commercial Type III",
			             "General Conditions & Hazards","Sinkhole Inspection","Chinese Drywall"};
	boolean chkcommercial=false,chkb1802=false;
	
	public void onCreate(Bundle SavedInstanceState) {
		super.onCreate(SavedInstanceState);
		cf = new CommonFunctions(this);
		 Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);
			}
		setContentView(R.layout.submit);
		cf.getInspectorId();;
		cf.getDeviceDimensions();
		
		LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
		hdr_layout.addView(new HdrOnclickListener(this,1,"Submit","",1,2,cf));
		
        cf.mainmenu_layout = (LinearLayout) findViewById(R.id.header);
        cf.mainmenu_layout.setMinimumWidth(cf.wd);
        cf.mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 10, 0,0, cf));
        
        /*TableRow tblrw = (TableRow)findViewById(R.id.row2);
		tblrw.setMinimumHeight(cf.ht);*/
		Declarations();
	}
	private void Declarations() {
		// TODO Auto-generated method stub
		res = getResources();
		myString = res.getStringArray(R.array.myArray);
		etsetword = (EditText) findViewById(R.id.wrdedt);
		etgetword = (EditText) findViewById(R.id.wrdedt1);
		word = myString[rgenerator.nextInt(myString.length)];
		
		etsetword.setText(word);
		if (word.contains(" ")) {
			word = word.replace(" ", "");
		}
	   ((TextView)findViewById(R.id.tv1)).setText(Html
				.fromHtml("<font color=black>I, "
						+ "</font>"
						+ "<b><font color=#bddb00>"
						+ cf.Insp_firstname.toUpperCase()+" "+cf.Insp_lastname.toUpperCase()
						+ "</font></b><font color=black> have conducted this inspection in accordance with the required standards and processes and confirm that I am properly licensed to conduct and sign off on the same inspection. I have made every attempt to collect the data required, have spoken to the Policyholder and/or Agent of record and have conducted the necessary research requirements to ensure permitting, replacement cost valuations or other information are properly completed as part of this inspection process."
						+ "</font>"));
		((TextView)findViewById(R.id.tv2)).setText(Html
				.fromHtml("<font color=black>I, "
						+ "</font>"
						+ "<b><font color=#bddb00>"
						+ cf.Insp_firstname.toUpperCase()+" "+cf.Insp_lastname.toUpperCase()
						+ "</font></b><font color=black> also understand that failure to properly conduct this inspection and submit the verifiable inspection data is grounds for termination and considered possible insurance fraud."
						+ "</font>"));
	
	}	
	public void clicker(View v)
	 {
		  switch(v.getId())
		  {
		  case R.id.submit:
			if (((RadioButton)findViewById(R.id.accept)).isChecked()) {
					if (((TextView)findViewById(R.id.wrdedt1)).getText().toString().equals("")) 
					{
						cf.ShowToast("Please enter Word Verification.",0);
						((TextView)findViewById(R.id.wrdedt1)).requestFocus();
					} else {

						if (((TextView)findViewById(R.id.wrdedt1)).getText().toString().equals(word.trim().toString())) {
						
							//checkandchangestatus();
							 try
							   {
							  	  cf.arr_db.execSQL("UPDATE "
										+ cf.policyholder
										+ " SET ARR_PH_IsInspected=1,ARR_PH_Status=1 WHERE ARR_PH_SRID ='"
										+ cf.selectedhomeid	+ "'  and ARR_PH_InspectorId = '"+cf.Insp_id + "'");
									cf.ShowToast("Saved successfully.", 1);
									Intent submitint = new Intent(getApplicationContext(),Export.class);
									submitint.putExtra("type", "export");
									startActivity(submitint);
							   }
							   catch(Exception e)
							   {
								   System.out.println("updating status"+e.getMessage());
							   }
							
							
						} else {
							cf.ShowToast("Please enter valid Word Verification(case sensitive).",0);
							((TextView)findViewById(R.id.wrdedt1)).setText("");
							((TextView)findViewById(R.id.wrdedt1)).requestFocus();
						}

					}

				} else {
					cf.ShowToast("Please select I Accept/Decline Radio Button.",0);

				}
			  break;
		  case R.id.hme:
			  cf.gohome();
			  break;
		  
		  case R.id.S_refresh:			
			  word = myString[rgenerator.nextInt(myString.length)];
				
				etsetword.setText(word);
				if (word.contains(" ")) {
					word = word.replace(" ", "");
				}	
			break;
		  }
	 }
	private void checkandchangestatus() {
		// TODO Auto-generated method stub
		/** CHECKING WHETHER INSPECTION HAS BEEN SELECTED **/
		
		cf.getPolicyholderInformation(cf.selectedhomeid);
		boolean FourPtisaccess = cf.chk_InspTypeQuery(cf.selectedhomeid,"1");
		boolean RoofSyisaccess = cf.chk_InspTypeQuery(cf.selectedhomeid,"2");
		boolean B1802isaccess = cf.chk_InspTypeQuery(cf.selectedhomeid,"3");
		boolean Commisaccess = cf.chk_InspTypeQuery(cf.selectedhomeid,"4");		
		boolean GCHisaccess = cf.chk_InspTypeQuery(cf.selectedhomeid,"7");
		boolean Sinkholeisaccess = cf.chk_InspTypeQuery(cf.selectedhomeid,"8");
		boolean Chineseisaccess = cf.chk_InspTypeQuery(cf.selectedhomeid,"9");
		boolean BIisaccess=true; 
		
		if(BIisaccess)
		{
			Export_BIcheck();
			if(sbmtchk[9]==false)
			{
				return;
			}
		}
		else
		{
			sbmtchk[9]=true;
		}
	
		if(FourPtisaccess)
		{
			Fourpt_Check();
			if(sbmtchk[0]==false)
			{
				return;
			}
		}
		else
		{
			sbmtchk[0]=true;
		}
		if(RoofSyisaccess)
		{
			Roofsurvey_Check();
			if(sbmtchk[1]==false)
			{
				return;
			}
		}
		else
		{
			sbmtchk[1]=true;
		}
		if(B1802isaccess)
		{
			sbmtchk[2] = B11802_Check();
			if(sbmtchk[2]==false)
			{
				return;
			}
		}
		else
		{
			sbmtchk[2]=true;
		}
		if(Commisaccess)
		{
			cf.getPolicyholderInformation(cf.selectedhomeid);
			if(!cf.Stories.equals(""))
			{
				if(cf.Stories.equals("1") || cf.Stories.equals("2")  || cf.Stories.equals("3"))
				{
					sbmtchk[3] = Commtype1_Check();
					if(sbmtchk[3]==false)
					{
						return;
					}
				}
				else if(cf.Stories.equals("4") || cf.Stories.equals("5")  || cf.Stories.equals("6"))
				{
					sbmtchk[3] =Commtype2_Check();
				    if(sbmtchk[3]==false)
					{
						return;
					}		
				}
				else if(Integer.parseInt(cf.Stories)>=7)
				{	
					sbmtchk[3] = Commtype3_Check();
					if(sbmtchk[3]==false)
					{
						return;
					}
				}	
			}
			else
			{
				sbmtchk[3]=true;
			}
		}
		else
		{
			sbmtchk[3]=true;
		}		
		if(GCHisaccess)
	    {
			sbmtchk[4] = GCH_Check();
			if(sbmtchk[4]==false)
			{
				return;
			}			
	    }
		else
		{
			sbmtchk[4]=true;
		}
	    if(Sinkholeisaccess)
		{
	    	sbmtchk[5] = Sinkhole_Check();
		   if(sbmtchk[5]==false)
			{
				return;
			}
		}
	    else
	    {
	    	sbmtchk[5]=true;
	    }
		if(Chineseisaccess)
		{
			sbmtchk[6]= Chinesedrywall_Check();
			if(sbmtchk[6]==false)
			{
				return;
			}
	    }
		else
		{
			sbmtchk[6]=true;
		}
		/**check for the image table**/
		if(!cf.photovalidation())
		{
		
			cf.ShowToast("Please add atleast one image in "+cf.inspectioname+".", 1);
			sbmtchk[7]=true;
			return;
		}
		else
		{	
			
			if(!cf.frontpdfvalidation())
			{
				
				cf.ShowToast("Please add atleast one Front PDF image in "+cf.inspectioname+".", 1);
				sbmtchk[7]=true;
				return;
			}
			else
			{
				sbmtchk[7]=true;
			}
		}
		
		/**check for the feedback table**/
		/*if(!cf.feedvalidation())
		{
			
			cf.ShowToast("Please save feedback information in "+cf.inspectioname+".", 1);
			sbmtchk[8]=true;
			return;
		}
		else
		{
			sbmtchk[8]=true;
		}*/

		
		
	   if(sbmtchk[0]==true && sbmtchk[1]==true && sbmtchk[2]==true && sbmtchk[3]==true && sbmtchk[4]==true
			 && sbmtchk[5]==true && sbmtchk[6]==true && sbmtchk[7]==true  && sbmtchk[9]==true)
		{
				 /** CHANGING INSPECTION STATUS IN POLICY HOLDER TABLE **/
		   try
		   {
		  	  cf.arr_db.execSQL("UPDATE "
					+ cf.policyholder
					+ " SET ARR_PH_IsInspected=1,ARR_PH_Status=1 WHERE ARR_PH_SRID ='"
					+ cf.selectedhomeid	+ "'  and ARR_PH_InspectorId = '"+cf.Insp_id + "'");
				cf.ShowToast("Saved successfully.", 1);
				Intent submitint = new Intent(getApplicationContext(),Export.class);
				submitint.putExtra("type", "export");
				startActivity(submitint);
		   }
		   catch(Exception e)
		   {
			   System.out.println("updating status"+e.getMessage());
		   }
			
		}
		
	}
	private void Export_BIcheck() 
	{
		cf.Check_BI();
		 if(cf.C_GEN.getCount()>0)
		 {			
			 if(!cf.Gendata_val.equals(""))
			 {
				 if(!cf.Loc_val.equals("") || cf.Loc_NA.equals("1"))
				 {
					 if((!cf.Observ1.equals("") && !cf.Observ2.equals("") && !cf.Observ3.equals("") && !cf.Observ4.equals("") && !cf.Observ5.equals("")) || cf.OBS_NA.equals("1"))
					 {
						 if(!cf.Iso_val.equals("") || cf.ISO_NA.equals("1"))
						 {
							 if(!cf.Foundation_val.equals("") || cf.Foun_NA.equals("1"))
							 {
								 if(!cf.Wallstru_val.equals("") || cf.WS_NA.equals("1"))
								 {
									 if(!cf.Wallclad_val.equals("") || cf.WC_NA.equals("1"))
									 {
										 if(!cf.Buildsep_val.equals("") || cf.BISEP_NA.equals("1"))
										 {
											/* if(!cf.comments.equals(""))
											 {*/
												 sbmtchk[9]=true;
											 /*}
											 else
											 {
												 cf.ShowToast("Please save the Overall Building Comments under Building Information.", 0);
											 }*/ 
										 }
										 else
										 {
											 cf.ShowToast("Please save the Building Seperation under Building Information.", 0);
										 }  
									 }
									 else
									 {
										 cf.ShowToast("Please save the Wall Cladding under Building Information.", 0);
									 }  
								 }
								 else
								 {
									 cf.ShowToast("Please save the Wall Structure under Building Information.", 0);
								 }  
							 }
							 else
							 {
								 cf.ShowToast("Please save the Foundation under Building Information.", 0);
							 } 
						 }
						 else
						 {
							 cf.ShowToast("Please save the ISO Classification under Building Information.", 0);
						 }
					 }
					 else
					 {
						 cf.ShowToast("Please save the General Building Observations under Building Information.", 0);
					 }
				 }
				 else
				 {
					 cf.ShowToast("Please save the Location - Proximity Information under Building Information.", 0);
				 }
			 }
			 else
			 {
				 cf.ShowToast("Please save the General Data under Building Information.", 0);
			 }
		 }
		 else
		 {
			sbmtchk[9]=false;
			 cf.ShowToast("Please save the General Data under Building Information.", 0);
		 }
	}


	private boolean Chinesedrywall_Check() {
		// TODO Auto-generated method stub
		if(cf.doesTblExist(cf.DRY_sumcond))
		{
			boolean chksum = cf.CD_sumValidation();
			if(chksum)
			{
				boolean chkroom = cf.CD_roomValidation();
				if(chkroom)
				{
					boolean chkhvac = cf.CD_hvacValidation();
					if(chkhvac)
					{
						boolean chkapp = cf.CD_appValidation();
						if(chkapp)
						{
							boolean chkattc = cf.CD_attcValidation();
							if(chkattc)
							{
								boolean chksum1 = cf.CD_sumValidation();
								if(chksum1)
								{
									/*if(cf.dcom.equals(""))
									{
										cf.ShowToast("Please save the Overall Chinese Drywall comments.", 0);
									}
									else
									{*/
										sbmtchk[6]=true;
									//}
									
								}
								else
								{
									cf.ShowToast("Please save the Overall Chinese Drywall comments.", 0);
								}
							}
							else
							{
								cf.ShowToast("Please save Attic under Chinese Drywall.", 0);
							}
						}
						else
						{
							cf.ShowToast("Please save Internal Appliance System(s) under Chinese Drywall.", 0);
						}
					}
					else
					{
						cf.ShowToast("Please save Internal HVAC System(s) under Chinese Drywall.", 0);
					}
				}
				else
				{
					cf.ShowToast("Please save Visual Assessment Room under Chinese Drywall.", 0);
				}
				
			}
			else
			{
				cf.ShowToast("Please save Summary of Conditions/Inspectors Findings under Chinese Drywall.", 0);
				
			}
		    
		}
		else
		{
			sbmtchk[6]=false;
			cf.ShowToast("Please save data under "+sbmtval[8], 0);
		}
		return sbmtchk[6];
	}
	private boolean Sinkhole_Check() {
		// TODO Auto-generated method stub
		if(cf.doesTblExist(cf.SINK_Summarytbl))
		{
			boolean chkss = cf.SH_SummaryValidation();
		    if(chkss)
			{
		    	boolean chkobs1 = cf.SH_OBS1Validation();
				if(chkobs1)
				{
					if(cf.trval.equals("Yes"))
					{
						boolean chkobs1T = cf.SH_OBS1TValidation();
						if(chkobs1T)
						{
							chk_OBS2();
						}
						else
						{
							cf.ShowToast("Please save atleast one Large Tree Observations under Sinkhole.", 0);
						}
					}
					else
					{
						chk_OBS2();
					}
				}
				else
				{
					cf.ShowToast("Please save Observations(1 - 7) under Sinkhole.", 0);
				}
			}
			else
			{
				cf.ShowToast("Please save Summary Questions under Sinkhole.", 0);
			}
		}
		else
		{
			sbmtchk[5]=false;
			cf.ShowToast("Please save data under "+sbmtval[7], 0);
		}
		return sbmtchk[5];
	}
	private void chk_OBS2() {
		// TODO Auto-generated method stub
		boolean chkobs2 = cf.SH_OBS2Validation();
		if(chkobs2)
		{
			boolean chkobs3 = cf.SH_OBS3Validation();
			if(chkobs3)
			{
				boolean chkobs4 = cf.SH_OBS4Validation();
				if(chkobs4)
				{
					sbmtchk[5]=true;
				} 
				else
				{
					cf.ShowToast("Please save Observations(23 - 27) under Sinkhole.", 0);
				}
			} 
			else
			{
				cf.ShowToast("Please save Observations(14 - 22) under Sinkhole.", 0);
			}
		} 
		else
		{
			cf.ShowToast("Please save Observations(8 - 13) under Sinkhole.", 0);
		}
	}
	private boolean Commtype3_Check() 
	{
		// TODO Auto-generated method stub
		if(commercialvalidation("Commercial Type III Inspection",34)==true)
		{
			return true;	
		}
		else
		{
			return false;	
		}
	}
	private boolean Commtype2_Check() 
	{
		// TODO Auto-generated method stub
		if(commercialvalidation("Commercial Type II Inspection",33)==true)
		{
			return true;	
		}
		else
		{
			return false;	
		}
	}	
	private boolean Commtype1_Check() 
	{
		// TODO Auto-generated method stub
		if(B1802validation("Commercial Type I Inspection", 32)==true)
		{
			return true;	
		}
		else
		{
			return false;	
		}
	}
	private boolean B11802_Check() 
	{
		// TODO Auto-generated method stub				
		if(B1802validation("B1802 Inspection", 31)==true)
		{
			return true;	
		}
		else
		{
			return false;	
		}
	}
	private boolean B1802validation(String inspname,int typeid) 
	{
		// TODO Auto-generated method stub'
		try
		{
			cf.WindOpen_NA="";cf.DoorOpen_NA="";cf.Skylights_NA=""; chkb1802=false;
			cf.Check_B1802(cf.selectedhomeid,typeid,2);
			
			cf.Addendumcomments(cf.selectedhomeid,typeid);
			

			
			if(cf.qc.getCount()>0)
			{
				if(!cf.bccomments.equals(""))
				{
					if(!cf.rccomments.equals(""))
					{
						if(!cf.rdcomments.equals(""))
						{
							if(!cf.rwcomments.equals(""))
							{
								if(!cf.rgcomments.equals(""))
								{
									if(!cf.swrcomments.equals(""))
									{
										if(cf.WindOpen_NA.equals("1") || cf.windopen.getCount()!=0)
										{
											if(cf.DoorOpen_NA.equals("1") || cf.dooropen.getCount()!=0)
											{
												if(cf.Skylights_NA.equals("1") ||cf.skylightsopen.getCount()!=0)
												{
													if(!cf.opcomments.equals(""))
													{
														/*if(!cf.AddendumComm.equals(""))
														{*/
															if(cf.onlinspectionid.equals("12"))
															{
																if(!cf.wccomments.equals("") && cf.wallcons.getCount()>0)
																{
																	if(typeid==31)
																	{
																		sbmtchk[2]=true;
																		chkb1802=sbmtchk[2];
																	}
																	else
																	{
																		boolean chkcomm = cf.Check_Comm1(cf.selectedhomeid,32,2);
																		cf.CommercialComents(cf.selectedhomeid,typeid,2);
																		if(chkcomm==true && !cf.communalcomments.equals(""))
																		{
																			int chkrf = cf.RoofValidation(4);
																			if(chkrf==2)
																			{
																				sbmtchk[3]=true;
																				chkb1802=sbmtchk[3];
																			}
																			else
																			{
																				cf.ShowToast("Please save the Roof System data under "+inspname, 0);
																			}																		
																		}
																		else
																		{
																			cf.ShowToast("Please save the Communal Areas under "+inspname, 0);
																		}
																	}	
																}
																else
																{
																	cf.ShowToast("Please save the Wall Construction under "+inspname, 0);
																}															
															}
															else
															{
																sbmtchk[2]=true;
																chkb1802=sbmtchk[2];
															}
													 /*  }
													   else
													   {
															cf.ShowToast("Please save the Addendum Comments under "+inspname, 0);
													   }*/
													}
													else
													{
														cf.ShowToast("Please save the Opening Protection under "+inspname, 0);
													}
												 }
												else
												{
													cf.ShowToast("Please save the Skylights under "+inspname, 0);
												}
											}
											else
											{
													cf.ShowToast("Please save the Door Openings under "+inspname, 0);
											}
										}
										else
										{
											cf.ShowToast("Please save the Window Openings under "+inspname, 0);
										}									
									}
									else
									{
										cf.ShowToast("Please save the SWR under "+inspname, 0);
									}
								}
								else
								{
									cf.ShowToast("Please save the Roof Geometry under "+inspname, 0);
								}
							}
							else
							{
								cf.ShowToast("Please save the Roof Wall under "+inspname, 0);
							}
						}
						else
						{
							cf.ShowToast("Please save the Roof Deck under "+inspname, 0);
						}
					}
					else
					{
						cf.ShowToast("Please save the Roof Cover under "+inspname, 0);
					}
				}
				else
				{
					cf.ShowToast("Please save the Building Code under "+inspname, 0);
				}
			}
			else
			{
				if(typeid==31)
				{
					
					sbmtchk[2]=false;	
					chkb1802=sbmtchk[2];
				} 
				else if(typeid==32) 
				{
					sbmtchk[3]=false;
					chkb1802=sbmtchk[3];
				}
				cf.ShowToast("Please save "+inspname, 0);		
			}
		}
		catch(Exception e){
			System.out.println("Catch"+e.getMessage());

		}
			return chkb1802;
	}

	public boolean commercialvalidation(String inspname ,int insptypeid)
	{
		try
		{
			cf.Aux_NA="";cf.Rest_NA="";chkcommercial=false;int roofiden=0;cf.communalcomments="";
			cf.Check_Comm2(cf.selectedhomeid,insptypeid,2);
			cf.Addendumcomments(cf.selectedhomeid,insptypeid);

			if(insptypeid==33){roofiden=5;}
			else if(insptypeid==34){roofiden=6;}
			if(cf.curcomm2.getCount()>0)
			{
				if(!cf.terrainval.equals(""))
				{
					if(!cf.rcvalue.equals(""))
					{
						if(!cf.rdvalue.equals(""))
						{
							if(!cf.swrvalue.equals(""))
							{
								if(!cf.opvalue.equals(""))
								{
									if(!cf.certificationval.equals(""))
									{
										boolean chkcomm = cf.Check_Comm1(cf.selectedhomeid,insptypeid,2);
										cf.CommercialComents(cf.selectedhomeid,insptypeid,2);
										if(chkcomm==true && !cf.communalcomments.equals(""))
										{
											int chkrf = cf.RoofValidation(roofiden);
											if(chkrf==2)
											{
												if(cf.curcomments.getCount()>0)
												{
													if(cf.curaux.getCount()!=0 || cf.Aux_NA.equals("1"))
													{
														if(cf.curapppl.getCount()!=0 || cf.Rest_NA.equals("1"))
														{
															if(insptypeid==33)
															{
																/*if(cf.AddendumComm.equals(""))
																{
																	cf.ShowToast("Please save the Addendum comments under "+inspname, 0);
																}
																else
																{*/
																	sbmtchk[4]=true;
																	chkcommercial=sbmtchk[4];	
																//}
																
															}
															else if(insptypeid==34)
															{
																/*if(cf.AddendumComm.equals(""))
																{
																	cf.ShowToast("Please save the Addendum comments under "+inspname, 0);
																}
																else
																{*/
																	sbmtchk[5]=true;
																	chkcommercial=sbmtchk[5];	
																//}
															}		
														}
														else
														{
															cf.ShowToast("Please save the Restaurant Supplement under "+inspname, 0);
														}
													}
													else
													{
														cf.ShowToast("Please save the Auxiliary Building under "+inspname, 0);
													}
												}
												else
												{
													sbmtchk[5]=false;
													chkcommercial=sbmtchk[5];
													cf.ShowToast("Please save the Auxiliary Building under "+inspname, 0);
												}
											}
											else
											{
												cf.ShowToast("Please save the Roof System data under "+inspname, 0);
											}
										}
										else
										{
											cf.ShowToast("Please save the Communal Areas under "+inspname, 0);
										}
									}
									else
									{
										cf.ShowToast("Please save the Certification for Commercial Wind under "+inspname, 0);
									}
								}
								else
								{
									cf.ShowToast("Please save the Opening Protection for Commercial Wind under "+inspname, 0);
								}
							}
							else
							{
								cf.ShowToast("Please save the SWR for Commercial Wind under "+inspname, 0);
							}
						}
						else
						{
							cf.ShowToast("Please save the Roof Deck for Commercial Wind under "+inspname, 0);
						}
					}
					else
					{
						cf.ShowToast("Please save the Roof Coverings for Commercial Wind under "+inspname, 0);
					}
				}
				else
				{
					cf.ShowToast("Please save the Terrain Exposure for Commercial Wind under "+inspname, 0);
				}
			}
			else
			{
				cf.ShowToast("Please save "+inspname, 0);
				if(insptypeid==33)
				{
					sbmtchk[4]=false;
					chkcommercial = sbmtchk[4];
				}
				else if(insptypeid==34)
				{
					sbmtchk[5]=false;
					chkcommercial = sbmtchk[5];
				}	
				
			}
		}
		catch(Exception e)
		{
			System.out.println("commercial validation"+e.getMessage());
		}
		return chkcommercial;
	}
	
	private boolean Roofsurvey_Check() {
		// TODO Auto-generated method stub
		int chkrf = cf.RoofValidation(2);
		if(chkrf==2)
		{
			sbmtchk[1]=true;
		}
		else
		{
			cf.ShowToast("Please save the "+sbmtval[1], 0);
			sbmtchk[1]=false;			
	    }
		return sbmtchk[1];
	}
	
    private boolean Fourpt_Check() {
    //	sbmtchk[0]=true;
    	cf.Addendumcomments(cf.selectedhomeid,1);
    	if(cf.doesTblExist(cf.Roof_additional))
		{
    		int chkrf = cf.RoofValidation(1);
    		if(chkrf==2)
			{
					boolean chkattic = cf.FP_AtticValidation();
					if(chkattic)
					{
						boolean chkplumbing = cf.FP_PlumbingValidation();
						if(chkplumbing)
						{
							boolean chkgen = cf.FP_GeneralValidation();
							if(chkgen)
							{
								boolean chkmpanel = cf.FP_MPanelValidation();
								if(chkmpanel)
								{
									boolean chkspanel = cf.FP_SPanelValidation();
									if(chkspanel)
									{
										boolean chkpanel = cf.FP_PanelValidation();
										if(chkpanel)
										{
											boolean chkwir = cf.FP_WirValidation();
											if(chkwir)
											{
												boolean chkrecp = cf.FP_RecpValidation();
												if(chkrecp)
												{
													boolean chkvbc = cf.FP_VBCValidation();
													if(chkvbc)
													{
														boolean chklfr = cf.FP_LFRValidation();
														if(chklfr)
														{
															boolean chkgo = cf.FP_GoValidation();
															if(chkgo)
															{
																boolean chkoes = cf.FP_OESValidation();
																if(chkoes)
																{
																	boolean chkcom = cf.FP_ComValidation();
																	if(chkcom)
																	{
																		boolean chkhvac = cf.FP_HVACValidation();
																		if(chkhvac)
																		{
																			boolean chkhvaca = cf.FP_HVACAnValidation();
																			if(chkhvaca)
																			{
																				boolean chkhvacunt = cf.FP_HVACUnValidation();
																				if(chkhvacunt)
																				{
																					
																					/*if(cf.AddendumComm.equals(""))
																					{
																						cf.ShowToast("Please save Addendum comments under Four Point Inspection.", 0);
																					}
																					else
																					{*/
																						sbmtchk[0]=true;	
																					//}
																					
																				}
																				else
																				{
																					cf.ShowToast("Please save atleast 1 HVAC Unit under Four Point Inspection.", 0);
																				}
																			}
																			else
																			{
																				sbmtchk[0]=true;	
																				/*if(cf.AddendumComm.equals(""))
																				{
																					cf.ShowToast("Please save Addendum comments under Four Point Inspection.", 0);
																				}
																				else
																				{
																					sbmtchk[0]=true;	
																				}*/
																			}
																				
																		}
																		else
																		{
																			cf.ShowToast("Please save HVAC data under Four Point Inspection.", 0);
																		}
																	}
																	else
																	{
																		cf.ShowToast("Please save Overall Electrical Services Comments under Four Point Inspection.", 0);
																	}
																}
																else
																{
																	cf.ShowToast("Please save Conditonal of Overall Electrical Services data under Four Point Inspection.", 0);
																}
															}
															else
															{
																cf.ShowToast("Please save General Observations data under Four Point Inspection.", 0);
															}
													}
													else
													{
														cf.ShowToast("Please save Light fixtures/Receptacles under Four Point Inspection.", 0);
													}
												}
												else
												{
													cf.ShowToast("Please save Visible Branch Circuity under Four Point Inspection.", 0);
												}
												}
												else
												{
													cf.ShowToast("Please save Receptacles/Outlets data under Four Point Inspection.", 0);
												}
											}
											else
											{
												cf.ShowToast("Please save Wiring Type data under Four Point Inspection.", 0);
											}
										}
										else
										{
											cf.ShowToast("Please save Visible Electrical Panel/Disconnect Issues Noted data under Four Point Inspection.", 0);
										}
									}
									else
									{
										cf.ShowToast("Please save Electrical Sub Panel data under Four Point Inspection.", 0);
									}
								}
								else
								{
									cf.ShowToast("Please save Electrical Main Panel data under Four Point Inspection.", 0);
								}
							}
							else
							{
								cf.ShowToast("Please save General Electrical Details data under Four Point Inspection.", 0);
							}
							
						}
						else
						{
							cf.ShowToast("Please save Plumbing System data under Four Point Inspection.", 0);
						}
						
					}
					else
					{
						cf.ShowToast("Please save Attic System data under Four Point Inspection.", 0);
					}
				}
				else
				{
					cf.ShowToast("Please save Roof System data under Four Point Inspection.", 0);
				}
    		
			
		}
    	else
    	{
    		sbmtchk[0]=false;
			cf.ShowToast("Please save data under "+sbmtval[0], 0);
    	}
		return sbmtchk[0];
	}
	private boolean GCH_Check() {
		// TODO Auto-generated method stub
		 if(cf.doesTblExist(cf.GCH_PoolPrsenttbl))
		 {
			 boolean chkpp = cf.GCH_PPValidation();
		     if(chkpp)
			 {
		    	 boolean chkpf = cf.GCH_PfValidation();
				 if(chkpf)
				 {
					int chkrf = cf.RoofValidation(7);
					if(chkrf==2)
					{
						boolean chksh = cf.GCH_SHValidation();
                        if(chksh)
						{
                        	boolean chkfp = cf.GCH_FPValidation();
							if(chkfp)
							{
								boolean chksph = cf.GCH_SPHValidation();
								if(chksph)
								{
									boolean chkhv = cf.GCH_HVValidation();
									if(chkhv)
									{
										boolean chkele = cf.GCH_ELEValidation();
									    if(chkele)
										{
									    	boolean chkhd = cf.GCH_HDValidation();
											if(chkhd)
											{
												boolean chkfpt = cf.GCH_FPTValidation();
												if(chkfpt)
												{
													boolean chkms = cf.GCH_MSValidation();
													if(chkms)
													{
														sbmtchk[4]=true;
														/*boolean chkcom = cf.GCH_COMValidation();
														if(chkcom)
														{
															sbmtchk[4]=true;
														}
														else
														{
															cf.ShowToast("Please save the Overall GCH Comments under General Conditions & Hazards.", 0);
														}*/
													}
													else
													{
														cf.ShowToast("Please save the Miscellaneous under General Conditions & Hazards.", 0);
													}
												}
												else
												{
													cf.ShowToast("Please save the Fire Protection under General Conditions & Hazards.", 0);
												}
											}
											else
											{
												cf.ShowToast("Please save the Hood and Duct Work under General Conditions & Hazards.", 0);
											}
										}
										else
										{
											cf.ShowToast("Please save the Electrical System data under General Conditions & Hazards.", 0);
										}
									}
									else
									{
										cf.ShowToast("Please save the HVAC System data under General Conditions & Hazards.", 0);
									}
								}
								else
								{
									cf.ShowToast("Please save the Special Hazards data under General Conditions & Hazards.", 0);
								}
							}
							else
							{
								cf.ShowToast("Please save the Fire Protection data under General Conditions & Hazards.", 0);
							}
						}
						else
						{
							cf.ShowToast("Please save the Summary of Hazards & Concerns data under General Conditions & Hazards.", 0);
						}
					}
					else
					{
						cf.ShowToast("Please save the Roof System data under General Conditions & Hazards.", 0);
					}

				}
				else
				{
					cf.ShowToast("Please save the Perimeter Pool Fence data under General Conditions & Hazards.", 0);
				}
			}
			else
			{
				cf.ShowToast("Please save the Swimming Pool Present data under General Conditions & Hazards.", 0);
			}
	 }
	 else
	 {
		 sbmtchk[4]=false;
		 cf.ShowToast("Please save data under "+sbmtval[6], 0);
	 }
		return sbmtchk[4];
	}
	
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			cf.goback(9);
			return true;
		}
		return true;
	}
		  
}