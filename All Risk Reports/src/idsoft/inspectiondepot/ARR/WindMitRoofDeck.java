/*
************************************************************
* Project: ALL RISK REPORT
* Module : WindMitRoofDeck.java
* Creation History:
Created By : Nalini on 10/24/2012
* Modification History:
Last Modified By : Gowri on 11/28/2012
************************************************************ 
*/
package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.WindMitBuildCode.textwatcher;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.opengl.Visibility;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;

public class WindMitRoofDeck extends Activity {
	CommonFunctions cf;
	public RadioButton roofdeckrdio[] = new RadioButton[7];
	public int[] rdoption = {R.id.RDoption1,R.id.RDoption2,R.id.RDoption3,R.id.RDoption4,R.id.RDoption5,R.id.RDoption6,R.id.RDoption7};
	int rdvalue;
	String comments[]={"This home was verified as meeting the requirements of the OIR B1- 1802, Selection A, Question 3 Roof Deck Attachment, where mean lift is less than that of B and C Selections.",
			"This home was verified as meeting the requirements of the OIR B1- 1802, Selection B, Question 3 Roof Deck Attachment.",
			"This home was verified as meeting the requirements of the OIR B1- 1802, Selection C, Question 3 Roof Deck Attachment.",
			"This home was verified as meeting the requirements of the OIR B1- 1802, Selection D, Question 3 Roof Deck Attachment.",
			"This home was verified as meeting the requirements of the OIR B1- 1802, Selection E, Question 3 Roof Deck Attachment.",
			"This home was verified as meeting the requirements of the OIR B1- 1802, Selection F, Question 3 Roof Deck Attachment.",
			"We were unable to verify the weakest form or type of roof wall connection to this home as a result of no attic access."};
	String b1_q3[] = {"Mean Uplift less than B and C","103 PSF", "182 PSF", "Concrete", "Other", "Unknown", "No Attic" };
	EditText edroofdeckother,edrdcomments;
	String tmp="",roofdeck_vlaue="",s="";
	boolean load_comment=true;
	private int[] RDoption;
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cf=new CommonFunctions(this);
        
        Bundle extras = getIntent().getExtras();
		if (extras != null) {
			cf.getExtras(extras);				
	 	}
		setContentView(R.layout.windmitroofdeck);
        cf.getDeviceDimensions();
        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
        if(cf.identityval==31)
        {
          hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","B1 1802(Rev.01/12)","Roof Deck",3,1,cf));
        }
        else if(cf.identityval==32)
        { 
        	hdr_layout.addView(new HdrOnclickListener(this,1,"Wind.Mit.1802","Commercial Type I","Roof Deck",3,1,cf));
        }
		LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
     	main_layout.setMinimumWidth(cf.wd);
     	main_layout.addView(new MyOnclickListener(getApplicationContext(), 3, 0,0,cf));
       
        cf.type_submenulayout =(LinearLayout) findViewById(R.id.typesubmenu); //** WINDMIT TYPE SUBMENU **//
        cf.type_submenulayout.addView(new MyOnclickListener(this, 300, 1,0, cf));
        cf.windmittypechk(cf.identityval,((RadioButton)findViewById(R.id.b1802_chk)),((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((TextView)findViewById(R.id.commtxt)),((ImageView)findViewById(R.id.editbno))); // To check the checkbox of choosen inspection type
        cf.uncheckcommercial(((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((RadioButton)findViewById(R.id.b1802_chk))); 
    	       
        cf.submenu_layout = (LinearLayout) findViewById(R.id.submenu);//** WINDMIT TYPE SUBMENU **//	        
        cf.submenu_layout .addView(new MyOnclickListener(this,cf.identityval,1,303, cf));
        
        cf.mainlayout = (LinearLayout)findViewById(R.id.bc_table);
        cf.save = (Button)findViewById(R.id.save);
        cf.tblrw = (TableRow)findViewById(R.id.row2);
        cf.tblrw.setMinimumHeight(cf.ht);			
		cf.CreateARRTable(31);
		cf.CreateARRTable(32);
		Declarations();
		RoofDeck_setValue();
    }
	private void RoofDeck_setValue() {
		// TODO Auto-generated method stub
		try
		{
		   Cursor c2=cf.SelectTablefunction(cf.Wind_Questiontbl, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
		   if(c2.getCount()>0)
		   {  
			   c2.moveToFirst();			   
			   rdvalue=c2.getInt(c2.getColumnIndex("fld_roofdeck"));
			   System.out.println("rdvalue"+rdvalue);
			   if(!String.valueOf(rdvalue).equals("") || String.valueOf(rdvalue).equals("0"))
				{
					for(int i=0;i<roofdeckrdio.length;i++)
					{
						if(String.valueOf(rdvalue).equals(roofdeckrdio[i].getTag().toString()))
						{
							roofdeckrdio[i].setChecked(true);						
						}
					}
					   if(rdvalue==5)
					   {
						   edroofdeckother.setText(cf.decode(c2.getString(c2.getColumnIndex("fld_rdothertext"))));
						   edroofdeckother.setVisibility(cf.v1.VISIBLE);
					   }
					   else
					   {
						   edroofdeckother.setVisibility(cf.v1.INVISIBLE);
					   }
					   	 if(String.valueOf(rdvalue).equals("0"))
						 {
							 ((TextView)findViewById(R.id.savetxtrd)).setVisibility(cf.v1.GONE);
						 }
						 else
						 {
							 ((TextView)findViewById(R.id.savetxtrd)).setVisibility(cf.v1.VISIBLE);	 
						 }
					   
				}
			   else
			   {
				   ((TextView)findViewById(R.id.savetxtrd)).setVisibility(cf.v1.GONE);
			   }
			   
		   }
		   else
		   {
			   ((TextView)findViewById(R.id.savetxtrd)).setVisibility(cf.v1.GONE);
		   }
		   c2.close();
		  Cursor c1=cf.SelectTablefunction(cf.Wind_QuesCommtbl, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+cf.identityval+"'");
		  if(c1.getCount()>0)
		   {
			   c1.moveToFirst();
			   edrdcomments.setText(cf.decode(c1.getString(c1.getColumnIndex("fld_roofdeckcomments"))));
		 }
		   
		}
	    catch (Exception E)
		{
	    	System.out.println("EE "+E.getMessage());
			String strerrorlog="Retrieving RoofDeck - WINDMIT";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
	private void Declarations() {
		// TODO Auto-generated method stub
		cf.setupUI((ScrollView) findViewById(R.id.scr));
		((TextView)findViewById(R.id.rdheader)).setText(Html.fromHtml("What is the weakest form of roof deck attachment?"));
		
		roofdeckrdio[0] = (RadioButton)findViewById(R.id.RDoption1);
		roofdeckrdio[1] = (RadioButton)findViewById(R.id.RDoption2);
		roofdeckrdio[2] = (RadioButton)findViewById(R.id.RDoption3);
		roofdeckrdio[3] = (RadioButton)findViewById(R.id.RDoption4);
		roofdeckrdio[4] = (RadioButton)findViewById(R.id.RDoption5);
		roofdeckrdio[5] = (RadioButton)findViewById(R.id.RDoption6);
		roofdeckrdio[6] = (RadioButton)findViewById(R.id.RDoption7);
		ArrayListenerInsured(roofdeckrdio);
	
	    edroofdeckother=(EditText) findViewById(R.id.txtroofdeckother);
	    edrdcomments=(EditText) findViewById(R.id.rdcomment);
	    edrdcomments.addTextChangedListener(new textwatcher());
	  TextView helptxt = (TextView) findViewById(R.id.help);
		helptxt.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if (!roofdeckrdio[0].isChecked() && !roofdeckrdio[1].isChecked() && !roofdeckrdio[2].isChecked() && !roofdeckrdio[3].isChecked()
						&&!roofdeckrdio[4].isChecked() && !roofdeckrdio[5].isChecked() && !roofdeckrdio[6].isChecked()){
					cf.alertcontent = "To see help comments, please select Roof Deck options.";
				}
				else if (roofdeckrdio[0].isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection A, Question 3 Roof Deck Attachment, where mean lift is less than that of B and C Selections.";
				} else if (roofdeckrdio[1].isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection B, Question 3 Roof Deck Attachment.";
				} else if (roofdeckrdio[2].isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection C, Question 3 Roof Deck Attachment.";
				} else if (roofdeckrdio[3].isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection D, Question 3 Roof Deck Attachment.";
				} else if (roofdeckrdio[4].isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection E, Question 3 Roof Deck Attachment.";
				} else if (roofdeckrdio[5].isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection F, Question 3 Roof Deck Attachment.";
				} else if (roofdeckrdio[6].isChecked()) {
					cf.alertcontent = "We were unable to verify the weakest form or type of roof wall connection to this home as a result of no attic access.";
				} else {
					cf.alertcontent = "To see help comments, please select Roof Deck options.";
				}
				cf.showhelp("HELP",cf.alertcontent);
			}
		});
	}
	private void ArrayListenerInsured(RadioButton[] BI_INS_txt1_opt) {
		// TODO Auto-generated method stub
		for(int i=0;i<BI_INS_txt1_opt.length;i++)
		{
			
			roofdeckrdio[i].setOnClickListener(new RadioArrayclickerRoofDeck());
		}
	}
	class textwatcher implements TextWatcher
    {
	         public int type;
	         textwatcher()
	    	{
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {
	    		// TODO Auto-generated method stub
	    		cf.showing_limit(s.toString(),(TextView)findViewById(R.id.rd_tv_type1),500); 
	    		
	}

	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,

	    			int after) {
	    		// TODO Auto-generated method stub
	     	}

	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,

	    			int count) {
	    		// TODO Auto-generated method stub
	    	}
	   }	

	class  RadioArrayclickerRoofDeck implements OnClickListener
	{

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			
			for(int i=0;i<roofdeckrdio.length;i++)
			{
				if(v.getId()==roofdeckrdio[i].getId())
				{
					edrdcomments.setText(comments[i]);
					roofdeckrdio[i].setChecked(true);
					if(v.getId()==roofdeckrdio[4].getId())
					{
						edroofdeckother.setVisibility(cf.v1.VISIBLE);
						cf.setFocus(edroofdeckother);
						
					}
					else
					{
						edroofdeckother.setVisibility(cf.v1.INVISIBLE);
						edroofdeckother.setText("");
						edroofdeckother.setFocusable(false);
					}
				}
				else
				{
					roofdeckrdio[i].setChecked(false);
				}
			}
		}
		
	}
	public void clicker(View v) {
		switch(v.getId())
		{
		case R.id.loadcomments:
			/***Call for the comments***/
			cf.findinspectionname(cf.identityval);RoofDeck_option();
			int len=((EditText)findViewById(R.id.rdcomment)).getText().toString().length();			
			if(!tmp.equals(""))
			{
					if(load_comment)
					{
						load_comment=false;
						int loc1[] = new int[2];
						v.getLocationOnScreen(loc1);
						Intent in = cf.loadComments(tmp,loc1);
						in.putExtra("insp_name", cf.inspname);
						in.putExtra("insp_ques", "Roof Deck");
						in.putExtra("length", len);
						in.putExtra("max_length", 500);
						startActivityForResult(in, cf.loadcomment_code);
					}
			}
			else
			{
				cf.ShowToast("Please select the option for Roof Deck.",0);
			}	
			break;			
		case R.id.helpoptA: /** HELP FOR OPTION1 **/
			cf.alerttitle="A - Mean Uplift less than B and C";
		    cf.alertcontent="Plywood/oriented-strand board (OSB) roof sheathing attached to the roof truss/rafter (spaced a maximum of 24 inches o.c.) by staples or 6d nails spaced at 6 inches along the edge and 12 inches in the field OR batten decking supporting wood shakes or wood shingles OR any system of screws, nails, adhesives, other deck fastening system or truss/rafter spacing that has an equivalent mean uplift less than that required for options B or C below.";
		    cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		case R.id.helpoptB: /** HELP FOR OPTION2 **/
			cf.alerttitle="B - 103 PSF";
			cf.alertcontent="Plywood/OSB roof sheathing, with a minimum thickness of 7/16 inch, attached to the roof truss/rafter (spaced a maximum of 24 inches o.c.) by 8d common nails spaced a maximum of 12 inches in the field OR any system of screws, nails, adhesives, other deck fastening system or truss/rafter spacing that is shown to have an equivalent or greater resistance than 8d nails spaced a maximum of 12 inches in the field or has a mean uplift resistance of at least 103 psf.";
			cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;			  
		case R.id.helpoptC: /** HELP FOR OPTION3 **/
			cf.alerttitle="C - 182 PSF";
			cf.alertcontent="Plywood/OSB roof sheathing, with a minimum thickness of 7/16 inch, attached to the roof truss/rafter (spaced a maximum of 24 inches o.c.) by 8d common nails spaced a maximum of 6 inches in the field OR dimensional lumber/tongue-and-groove decking with a minimum of two nails per board (or one nail per board if each board is equal to or less than 6 inches in width) OR any system of screws, nails, adhesives, other deck fastening system or truss/rafter spacing that is shown to have an equivalent or greater resistance than 8d common nails spaced a maximum of 6 inches in the field or has a mean uplift resistance of at least 182 psf";
			cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		case R.id.helpoptD:
			cf.alerttitle="D - Concrete";
			cf.alertcontent="Reinforced concrete Roof Deck";
			cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
			
		case R.id.helpoptE:
			cf.alerttitle="E - other";
		    cf.alertcontent="Other.";
		    cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
			
		case R.id.helpoptF:
			cf.alerttitle="F - Unknown";
		    cf.alertcontent="Unknown or unidentified.";
		    cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
			
		case R.id.helpoptG:
			cf.alerttitle="G - No Attic";
		    cf.alertcontent="No attic access.";
		    cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		case R.id.hme:
    		cf.gohome();
    		break;
		
		case R.id.save:			
			roofdeck_vlaue=cf.getselecctedtag_radio(roofdeckrdio);	
			if(!roofdeck_vlaue.equals(""))
			{
				if(roofdeck_vlaue.equals("5"))
				{
					if (edroofdeckother.getText().toString().trim().equals("")) {
						cf.ShowToast("Please enter the other text for Roof Deck.",0);
						cf.setFocus(edroofdeckother);						
					} 
					else 
					{
						insertdata();
					}
				}
				else
				{
					insertdata();
				}					
			}
			else
			{
				cf.ShowToast("Please select the option for Roof Deck.",0);
			}
			break;
		}
	}
	private void RoofDeck_option() {
		// TODO Auto-generated method stub
		 for(int i=0;i<roofdeckrdio.length;i++)
    	 {
			 if(roofdeckrdio[i].isChecked())
			 {
				tmp = b1_q3[i];
			 }
    	 }
	}

	private void insertdata() {
		// TODO Auto-generated method stub
		if(edrdcomments.getText().toString().trim().equals(""))
		{
			cf.ShowToast("Please enter the Comments for Roof Deck.",0);
			cf.setFocus(edrdcomments);
		}
		else
		{
			try
			{
			 Cursor c1=cf.SelectTablefunction(cf.Wind_Questiontbl, " where fld_srid='"+cf.selectedhomeid+"'");
			 if(c1.getCount()==0)
			 {
				 cf.arr_db.execSQL("INSERT INTO "
							+ cf.Wind_Questiontbl
							+ " (fld_srid,insp_typeid,fld_bcyearbuilt,fld_bcpermitappdate,fld_buildcode,fld_roofdeck,fld_rdothertext," +
							"	fld_roofwall,fld_roofwallsub,fld_roofwallclipsminval,fld_roofwallclipssingminval,fld_roofwallclipsdoubminval,fld_rwothertext," +
							"	fld_roofgeometry ,fld_geomlength ,fld_roofgeomtotalarea ,fld_roofgeomothertext ,fld_swr,fld_optype,fld_openprotect,fld_opsubvalue,fld_oplevelchart," +
							"   fld_opGOwindentdoorval,fld_opGOgardoorval,fld_opGOskylightsval,fld_opGOglassblockval,fld_opNGOentrydoorval,fld_opNGOgaragedoorval, " +
							"	fld_wcvalue,fld_wcreinforcement,fld_quesmodifydate)"
							+ "VALUES ('" + cf.selectedhomeid + "','"+cf.identityval+"','','','','" + roofdeck_vlaue + "','"+cf.encode(edroofdeckother.getText().toString())+"','" +
							+ 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0	+ "','','"+
							+ 0 + "','" + 0 + "','" + 0 + "','','" + 0 + "','"+ 0 +"','"+ 0 + "','" + 0 + "','" + 0 + "','" +
							+ 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','"+ 0 +"','" +
							+ 0 + "','" + 0 + "','"+ cf.datewithtime + "')");				 
			 }
			 else
			 {
				 cf.arr_db.execSQL("UPDATE " + cf.Wind_Questiontbl
							+ " SET fld_roofdeck='"+roofdeck_vlaue+"',fld_rdothertext='"
							+ cf.encode(edroofdeckother.getText().toString())
							+ "'" + " WHERE fld_srid ='"
							+ cf.selectedhomeid.toString() + "' and insp_typeid='"+cf.identityval+"'");				
			 }
			 c1.close();
			 	Cursor c2 = cf.SelectTablefunction(cf.Wind_QuesCommtbl, "where fld_srid='"+ cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'");
				if (c2.getCount() == 0) 
				{
					cf.arr_db.execSQL("INSERT INTO "
							+ cf.Wind_QuesCommtbl
							+ " (fld_srid,insp_typeid,fld_buildcodecomments,fld_roofcovercomments,fld_roofdeckcomments,fld_roofwallcomments," +
							"fld_roofgeometrycomments,fld_swrcomments,fld_openprotectioncomments,fld_wallconscomments,fld_insoverallcomments )"
							+ "VALUES('"+cf.selectedhomeid+"','"+cf.identityval+"','','','"+cf.encode(edrdcomments.getText().toString())+"','','','','','','')");
				}
				else
				{
					cf.arr_db.execSQL("UPDATE " + cf.Wind_QuesCommtbl
							+ " SET fld_roofdeckcomments='"+cf.encode(edrdcomments.getText().toString())+ "' WHERE fld_srid ='" + cf.selectedhomeid + "' and insp_typeid='"+cf.identityval+"'");
				}	
				cf.ShowToast("Roof Deck Details saved successfully.",0);
				cf.gotoclass(cf.identityval, WindMitRoofWall.class);
		 }
		 catch (Exception E)
		 {
			String strerrorlog="Updating Questions Buildcode - FourPoint";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		 }
		}
		
	}
	
	 public boolean onKeyDown(int keyCode, KeyEvent event) {
	 		// replaces the default 'Back' button action
	 		if (keyCode == KeyEvent.KEYCODE_BACK) {
	 			cf.gotoclass(cf.identityval, WindMitRoofCover.class);
	 			return true;
	 		}
	 		
	 		return super.onKeyDown(keyCode, event);
	 	}
	 /**on activity result for the load comments**/
	 @Override
	 		protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	 		try
	 		{
	 			if(requestCode==cf.loadcomment_code)
	 			{
	 				load_comment=true;
	 				if(resultCode==RESULT_OK)
	 				{
	 					String rdcomm = edrdcomments.getText().toString();	 
	 					edrdcomments.setText(rdcomm +" "+data.getExtras().getString("Comments"));
		 			}
	 			}
	 		}
	 		catch (Exception e) {
	 			// TODO: handle exception
	 			System.out.println("the erro was "+e.getMessage());
	 		}
	 	}/**on activity result for the load comments ends**/
}
