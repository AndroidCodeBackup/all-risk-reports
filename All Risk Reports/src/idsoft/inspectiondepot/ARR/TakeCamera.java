package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;

public class TakeCamera extends  Activity{
CommonFunctions cf;
String separated[];
private Uri CapturedImageURI;
private String path;
public Spinner sp1,sp2,sp3;
public int max_allowed=8;
protected int elev;
protected String inspections;
int imageorder=1;
int pos=0;
int currnet_rotated=0;
protected Bitmap rotated_b;
ImageView im;
private Button rotateright;
private Button rotateleft;
private String inspection;
private String customid;
SummaryFunctions sf;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		cf=new CommonFunctions(this);
		cf.CreateARRTable(82);
		cf.CreateARRTable(81);
		sf=new SummaryFunctions(this);
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
		
				cf.selectedhomeid = extras.getString("homeid");
		 }
		try
		{
		PackageManager pm = getPackageManager();
		if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
			
			String fileName = "temp.jpg";
			ContentValues values = new ContentValues();
			values.put(MediaStore.Images.Media.TITLE, fileName);
			CapturedImageURI = getContentResolver().insert(
					MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			intent.putExtra(MediaStore.EXTRA_OUTPUT, CapturedImageURI);
			startActivityForResult(intent, 0);
		}
		else
		{
			cf.ShowToast("You dont have Camera Feature in your device", 0);
		}
		}catch (Exception e) {
			// TODO: handle exception
			cf.ShowToast("Unexpected error! Please contact paperless admin and try again.", 1);
			finish();
		}
		setContentView(R.layout.take_camera);
		im=(ImageView) findViewById(R.id.Roof_img);
		sp1=(Spinner) findViewById(R.id.spin_inspection);
		sp2=(Spinner) findViewById(R.id.spin_type);
		sp3=(Spinner) findViewById(R.id.spin_caption);
		rotateleft=(Button) findViewById(R.id.rotateleft);
		rotateright=(Button) findViewById(R.id.rotateright);
		rotateright.setOnClickListener(new OnClickListener() {  
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				System.gc();
				currnet_rotated+=90;
				if(currnet_rotated>=360)
				{
					currnet_rotated=0;
				}
				
				Bitmap myImg;
				try {
					myImg = BitmapFactory.decodeStream(new FileInputStream(path));
					Matrix matrix =new Matrix();
					matrix.reset();
					//matrix.setRotate(currnet_rotated);
					System.out.println("Ther is no more issues top ");
					matrix.postRotate(currnet_rotated);
					
					 rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
					        matrix, true);
					 System.gc();
					 im.setImageBitmap(rotated_b);

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (Exception e) {
					
				}
				catch (OutOfMemoryError e) {
					System.out.println("comes in to out ot mem exception");
					System.gc();
					try {
						myImg=null;
						System.gc();
						Matrix matrix =new Matrix();
						matrix.reset();
						//matrix.setRotate(currnet_rotated);
						matrix.postRotate(currnet_rotated);
						myImg= cf.ShrinkBitmap(path, 800, 800);
						rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
						
						System.gc();
						im.setImageBitmap(rotated_b); 

					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					 catch (OutOfMemoryError e1) {
							// TODO Auto-generated catch block
						 cf.ShowToast("You cannot rotate this image. Image size exceeds 2MB.",0);
					}
				}

			}
		});
		rotateleft.setOnClickListener(new OnClickListener() {  			
			public void onClick(View v) {

				// TODO Auto-generated method stub
			
				System.gc();
				currnet_rotated-=90;
				if(currnet_rotated<0)
				{
					currnet_rotated=270;
				}

				
				Bitmap myImg;
				try {
					myImg = BitmapFactory.decodeStream(new FileInputStream(path));
					Matrix matrix =new Matrix();
					matrix.reset();
					//matrix.setRotate(currnet_rotated);
					System.out.println("Ther is no more issues top ");
					matrix.postRotate(currnet_rotated);
					
					 rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
					        matrix, true);
					 
					 im.setImageBitmap(rotated_b);

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (Exception e) {
					
				}
				catch (OutOfMemoryError e) {
					System.out.println("comes in to out ot mem exception");
					System.gc();
					try {
						myImg=null;
						System.gc();
						Matrix matrix =new Matrix();
						matrix.reset();
						//matrix.setRotate(currnet_rotated);
						matrix.postRotate(currnet_rotated);
						myImg= cf.ShrinkBitmap(path, 800, 800);
						rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
						System.gc();
						im.setImageBitmap(rotated_b); 

					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					 catch (OutOfMemoryError e1) {
							// TODO Auto-generated catch block
						 cf.ShowToast("You can not rotate this image. Image size exceeds 2MB.",0);
					}
				}

			
			}
		});
	
		
		cf.getPolicyholderInformation(cf.selectedhomeid);
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		String selectedImagePath = "", picname = "";
		try
		{
			if (resultCode == RESULT_OK) {
				if (requestCode == 0) {
					String[] projection = { MediaStore.Images.Media.DATA };
					Cursor cursor = managedQuery(CapturedImageURI, projection,
							null, null, null);
					int column_index_data = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					cursor.moveToFirst();
					path  = cursor.getString(column_index_data);
					
				}
				
			} else {
				path = "";
				cf.ShowToast("Captured image not saved", 0);
				finish();
				// edbrowse.setText("");
			}
		
		}
		catch(Exception e)
		{
			path = "";
			cf.ShowToast("Captured image not saved", 0);
			finish();
			System.out.println("no more issues "+e.getMessage());
			path="";
		}
		File f=new File(path);
		if(f.exists())
		{
			Bitmap b=cf.ShrinkBitmap(path, 400, 400);
			((ImageView) findViewById(R.id.Roof_img)).setImageBitmap(b);
			
			String arr[]=null;
			 try
			    {
				 
			    	/*Cursor s=cf.SelectTablefunction(cf.Select_insp, " WHERE SI_srid='"+cf.selectedhomeid+"'");
			    	s.moveToFirst();
			    	if(s.getCount()>0)
			    	{
			    		String tmp=cf.decode(s.getString(s.getColumnIndex("SI_InspectionNames"))).trim();
			    		Cursor c =cf.SelectTablefunction(cf.inspnamelist, " Where  ARR_Custom_ID in ("+tmp+")  order by ARR_Custom_ID");
			    		
		    			if(c.getCount()>0)
		    			{
		    				c.moveToFirst();
		    				arr=new String[c.getCount()+1];
		    				//tmp=new String[c.getCount()];
		    				arr[0]="-Select-";
		    				for(int i=1;i<c.getCount()+1;i++,c.moveToNext())
		    				{
		    					
		    					arr[i]=cf.decode(c.getString(c.getColumnIndex("ARR_Insp_Name"))).trim();
		    					//selected_insp[i-1]=cf.decode(c.getString(c.getColumnIndex("ARR_Insp_Name"))).trim();
		    				}
		    				
		    			}
		    			else
	    				{
	    					arr=cf.All_insp_list;//getResources().getStringArray(R.array.Inspection_types);
	    				}
			    		if(tmp.length()>0)
			    		{
			    			tmp="0^"+tmp;
			    			tmp=tmp.replace("^","~");
			    		
			    			slect_insp_id=tmp.split("~");
			    		}
			    		else
			    		{
			    			//arr=getResources().getStringArray(R.array.Inspection_types);
			    		}
			    		
			    		
			    	}
			    	else
			    	{
			    		arr=cf.All_insp_list;
			    	}*/
				 
				 Cursor s=cf.SelectTablefunction(cf.Select_insp, " WHERE SI_srid='"+cf.selectedhomeid+"'");
			    	s.moveToFirst();
			    	if(s.getCount()>0)
			    	{
			    		String tmp=cf.decode(s.getString(s.getColumnIndex("SI_InspectionNames"))).trim();
			    		System.out.println("tmp="+tmp);
			    		if(tmp.length()>0)
			    		{
			    			try
			    			{
				    			Cursor c =cf.SelectTablefunction(cf.inspnamelist, " Where  ARR_Insp_ID in ("+tmp+")  order by ARR_Custom_ID");
				    			System.out.println("c=="+c.getCount());
				    			if(c.getCount()>0)
				    			{
				    				c.moveToFirst();
				    				arr=new String[c.getCount()+1];
				    				arr[0]="-Select-";
				    				for(int i=1;i<c.getCount()+1;i++,c.moveToNext())
				    				{System.out.println("arr=="+arr[i]);
				    					arr[i]=cf.decode(c.getString(c.getColumnIndex("ARR_Insp_Name"))).trim();
				    					System.out.println("arr=="+arr[i]);
				    				}
				    			}
			    			}
			    			catch (Exception e) {
								// TODO: handle exception
			    				System.out.println("second issues="+e.getMessage());
							}
			    		}
			    		else
			    		{
			    			arr=cf.All_insp_list;//getResources().getStringArray(R.array.Inspection_types);
			    		}
			    		
			    		
			    	}
			    	else
			    	{
			    		arr=getResources().getStringArray(R.array.Inspection_types);
			    	}
				 
			    }
			    catch (Exception e) {
					// TODO: handle exception
				}
			ArrayAdapter adapter2 = new ArrayAdapter(TakeCamera.this,android.R.layout.simple_spinner_item, arr);
			adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			sp1.setAdapter(adapter2);
			sp1.setOnItemSelectedListener(new OnItemSelectedListener() {

				

			

				@Override
				public void onItemSelected(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					String selected =sp1.getSelectedItem().toString();
					inspection="0";
					
					//String selected =insp_sp.getSelectedItem().toString();
					if(!selected.equals("-Select-"))
					{
						for(int i=0;i<cf.All_insp_list.length;i++)
						{
							if(cf.All_insp_list[i].equals(selected))
							{
								inspection=i+"";
								//getinspectionid(selected);
							}
						}
					}
					
					String[] arr=null;
					if(selected.trim().equals(cf.All_insp_list[3]))
					{
						arr=getResources().getStringArray(R.array.B1_1802_Inspection);					
					}
					else if(selected.trim().equals(cf.All_insp_list[1]))
					{
						arr=getResources().getStringArray(R.array.Four_Point_Inspection);					
					}
					else if(selected.trim().equals(cf.All_insp_list[4]))
					{
						arr=getResources().getStringArray(R.array.Commercial_Type_I);					
					}
					else if(selected.trim().equals(cf.All_insp_list[5]))
					{
						arr=getResources().getStringArray(R.array.Commercial_Type_II);					
					}
					else if(selected.trim().equals(cf.All_insp_list[6]))
					{
						arr=getResources().getStringArray(R.array.Commercial_Type_III);					
					}
					else if(selected.trim().equals(cf.All_insp_list[7]))
					{
						arr=getResources().getStringArray(R.array.General_Conditions_Hazards);					
					}
					else if(selected.trim().equals(cf.All_insp_list[8]))
					{
						arr=getResources().getStringArray(R.array.Sinkhole_Inspection);					
					}
					else if(selected.trim().equals(cf.All_insp_list[9]))
					{
						arr=getResources().getStringArray(R.array.Chinese_Drywall);					
					}
					else if(selected.trim().equals(cf.All_insp_list[2]))
					{
						arr=getResources().getStringArray(R.array.Roof_Survey);		
						max_allowed=2;
					}
					
					
					if(arr!=null)
					{
						inspections=selected;
						ArrayAdapter adapter3 = new ArrayAdapter(TakeCamera.this,android.R.layout.simple_spinner_item,arr);
						adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						sp2.setAdapter(adapter3);
						((View) findViewById(R.id.take_camera_rw2)).setVisibility(View.VISIBLE);
					}
					else
					{
						//((View) findViewById(R.id.PH_elev_lin)).setVisibility(View.GONE);
						//((View) findViewById(R.id.ph_lin2)).setVisibility(View.GONE);
					}
					
				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub
					
				}
			});
			
			sp2.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					int selected =sp2.getSelectedItemPosition();
					if(selected!=0)
					{
						if(inspection.equals("4"))
	 					{
	 						if(cf.Stories.equals("1") || cf.Stories.equals("2")  || cf.Stories.equals("3"))
	 						{
	 							inspection="4";		
	 						}
	 						else if(cf.Stories.equals("4") || cf.Stories.equals("5")  || cf.Stories.equals("6"))
	 						{
	 							inspection="5";		
	 						}
	 						else if(Integer.parseInt(cf.Stories)>7)
	 						{	
	 							inspection="6";
	 						}
	 					}
						
						elev=getelvationnumber(sp2.getSelectedItem().toString(), inspection);
						System.out.println("elev="+elev);
						if(elev==23)
						{
							max_allowed=1;							
						}
						else
						{
							max_allowed=8;
						}
						Cursor c=cf.SelectTablefunction(cf.ImageTable, " WHERE ARR_IM_SRID='"+cf.selectedhomeid+"' and ARR_IM_Insepctiontype='"+inspection+"' and ARR_IM_Elevation='"+elev+"'");
						if(c!=null)
						{
							System.out.println("max="+max_allowed+"c.get="+c.getCount());
							if(c.getCount()<max_allowed)
							{
								imageorder=c.getCount()+1;
								Cursor c1 = cf.SelectTablefunction(cf.Photo_caption,
									" WHERE ARR_IMP_Elevation='" + elev
											+ "' and ARR_IMP_Insepctiontype='"+inspection+"' and ARR_IMP_ImageOrder='" + (c.getCount()+1) + "'");
								String temp[]=null;
								if(c1.getCount()>0)
								{	
									temp = new String[c1.getCount() + 2];
								temp[0] = "Select";
								temp[1] = "ADD PHOTO CAPTION";
								int i = 2;
								c1.moveToFirst();
								do {
									temp[i] = cf.decode(c1.getString(c1
											.getColumnIndex("ARR_IMP_Caption")));
									i++;
								} while (c1.moveToNext());
							} else {
								temp = new String[2];
								temp[0] = "Select";
								temp[1] = "ADD PHOTO CAPTION";
							}
								ArrayAdapter adapter2 = new ArrayAdapter(TakeCamera.this,android.R.layout.simple_spinner_item, temp);
								adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
								sp3.setAdapter(adapter2);
								((View) findViewById(R.id.take_camera_rw3)).setVisibility(View.VISIBLE);
								
							}
							else
							{
								cf.ShowToast("Image limit exceeds! Maximum of "+max_allowed+" images added in the "+sp2.getSelectedItem().toString()+" under "+sp1.getSelectedItem().toString(), 0);
								sp2.setSelection(0);
							}
							
						}
					}
					else
					{
						//((View) findViewById(R.id.ph_lin2)).setVisibility(View.GONE);
					}
					
				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub
					
				}
			});
			sp3.setOnItemSelectedListener(new  OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					if (arg2 == 0) {
					} else {

						int maxLength = 99;
						try {

							
							if (sp3.getSelectedItem().equals("ADD PHOTO CAPTION")) {
								final AlertDialog.Builder alert = new AlertDialog.Builder(TakeCamera.this);
								alert.setTitle("ADD PHOTO CAPTION");
								final EditText input = new EditText(TakeCamera.this);
								alert.setView(input);
								InputFilter[] FilterArray = new InputFilter[1];
								FilterArray[0] = new InputFilter.LengthFilter(maxLength);
								input.setFilters(FilterArray);
								input.setTextSize(14);
								input.setWidth(800);

								alert.setPositiveButton("Add",new DialogInterface.OnClickListener() {

											public void onClick(DialogInterface dialog,
													int whichButton) {
												String commentsadd = input.getText()
														.toString();
												if (input.length() > 0
														&& !commentsadd.trim().equals(
																"")) {
													
													cf.arr_db.execSQL("INSERT INTO "
															+ cf.Photo_caption
															+ " (ARR_IMP_INSP_ID,ARR_IMP_Caption,ARR_IMP_Elevation,ARR_IMP_ImageOrder,ARR_IMP_Insepctiontype)"
															+ " VALUES ('" + cf.Insp_id	+ "','"+ cf.encode(commentsadd)+ "','" + elev + "','" + imageorder+ "','"+inspection+"')");
													System.out
															.println("INSERT INTO "
																	+ cf.Photo_caption
																	+ " (ARR_IMP_INSP_ID,ARR_IMP_Caption,ARR_IMP_Elevation,ARR_IMP_ImageOrder,ARR_IMP_Insepctiontype)"
																	+ " VALUES ('" + cf.Insp_id	+ "','"+ cf.encode(commentsadd)+ "','" + elev + "','" + imageorder+ "','"+inspection+"')");
													cf.ShowToast("Photo Caption added successfully.",0);
													
													showSavedcaption(commentsadd);
												cf.hidekeyboard(input);
													
													
													
												} else {
													cf.ShowToast("Please enter the caption.",0);
													cf.hidekeyboard(input);
													sp3.setSelection(0);  
												}
											}

											
										});

								alert.setNegativeButton("Cancel",
										new DialogInterface.OnClickListener() {

											public void onClick(DialogInterface dialog,
													int whichButton) {
												sp3.setSelection(0);
												cf.hidekeyboard(input);
												dialog.cancel();
												
											}
										});
								alert.setCancelable(false);
								alert.show();

							} else if (pos==0) {
								final Dialog dialog = new Dialog(TakeCamera.this);
								dialog.setContentView(R.layout.alertfront);

								dialog.setTitle("Choose Option");
								dialog.setCancelable(true);
								final EditText edit_desc = (EditText) dialog
										.findViewById(R.id.edittxtdesc);

								Button button_close = (Button) dialog
										.findViewById(R.id.Button01);
								final Button button_sel = (Button) dialog
										.findViewById(R.id.Button02);
								Button button_edit = (Button) dialog
										.findViewById(R.id.Button03);
								Button button_del = (Button) dialog
										.findViewById(R.id.Button04);
								final Button button_upd = (Button) dialog
										.findViewById(R.id.Button05);
   
								button_close.setOnClickListener(new OnClickListener() {
									public void onClick(View v) {
										sp3.setSelection(0);
										cf.hidekeyboard(edit_desc);
										dialog.setCancelable(true);
										dialog.cancel();
									}

									public void onNothingSelected(AdapterView<?> arg0) {
										// TODO Auto-generated method stub

									}
								});
								button_sel.setOnClickListener(new OnClickListener() {
									public void onClick(View v) {
										try {

											cf.arr_db.execSQL("UPDATE "
													+ cf.ImageTable
													+ " SET ARR_IM_Description='"
													+ cf.encode(sp3.getSelectedItem().toString())
													+ "',ARR_IM_Nameext='"
													+ cf.encode(path)
													+ "',ARR_IM_ModifiedOn='"
													+ cf.encode(cf.datewithtime
															.toString())
													+ "' WHERE ARR_IM_SRID ='"
													+ cf.selectedhomeid
													+ "' and ARR_IM_Elevation='" + elev
													+ "'  and  ARR_IM_Insepctiontype='"+inspections+"' and ARR_IM_ImageOrder='"
													+ imageorder + "'");
											
										} catch (Exception e) {
											cf.Error_LogFile_Creation("Problem in select the caption in the photos at the elevation "
													+ elev
													+ " error  ="
													+ e.getMessage());
										}

										
										dialog.setCancelable(true);
										dialog.cancel();
									}
								});

								button_edit.setOnClickListener(new OnClickListener() {

									public void onClick(View v) {

										button_sel.setVisibility(v.GONE);

										edit_desc.setVisibility(v.VISIBLE);
										edit_desc.setText(sp3.getSelectedItem().toString());

										button_upd.setVisibility(v.VISIBLE);
										
										button_upd
												.setOnClickListener(new OnClickListener() {

													//
													public void onClick(View v) {
														if (!edit_desc.getText()
																.toString().trim()
																.equals("")) {
															cf.arr_db.execSQL("UPDATE "
																	+ cf.Photo_caption
																	+ " set ARR_IMP_Caption = '"
																	+ cf.encode(edit_desc
																			.getText()
																			.toString())
																	+ "' WHERE ARR_IMP_Elevation ='"
																	+ elev
																	+ "' and ARR_IMP_ImageOrder='"
																	+ imageorder
																	+ "' and ARR_IMP_Insepctiontype='"+inspections+"' and ARR_IMP_Caption='"
																	+ cf.encode(sp3.getSelectedItem().toString())
																	+ "'");
															cf.hidekeyboard(edit_desc);
															showSavedcaption();
															cf.ShowToast("Photo Caption has been updated successfully.",0);
															dialog.setCancelable(true);
															dialog.cancel();
														} else {
															cf.ShowToast("Please enter the caption.",0);cf.hidekeyboard(edit_desc);
														}
													}
												});
									}
								});

								button_del.setOnClickListener(new OnClickListener() {
									public void onClick(View v) {

										AlertDialog.Builder builder = new AlertDialog.Builder(TakeCamera.this);
										builder.setMessage("Are you sure, Do you want to delete?")
												.setCancelable(false)
												.setPositiveButton("Yes",
														new DialogInterface.OnClickListener() {
															public void onClick(
																	DialogInterface dialog,
																	int id) {
																// SH_IMP_INSP_ID,SH_IMP_Caption,SH_IMP_Elevation,SH_IMP_ImageOrder
																cf.arr_db.execSQL("Delete From "
																		+ cf.Photo_caption
																		+ "  WHERE  ARR_IMP_Elevation ='"
																		+ elev
																		+ "' and ARR_IMP_ImageOrder='"
																		+ imageorder
																		+ "' and ARR_IMP_Insepctiontype='"+inspections+"' and ARR_IMP_Caption='"
																		+ cf.encode(sp3.getSelectedItem().toString())
																		+ "'");
																showSavedcaption();
																cf.ShowToast("Photo Caption has been deleted successfully.",0);

															}
														})
												.setNegativeButton(
														"No",
														new DialogInterface.OnClickListener() {
															public void onClick(
																	DialogInterface dialog,
																	int id) {
																sp3.setSelection(0);
																dialog.cancel();
															}
														});
										dialog.setCancelable(true);
										builder.show();
										dialog.cancel();
									}
								});
								dialog.setCancelable(false);
								dialog.show();
							}
							else
							{
								pos=0;
							}

						} catch (Exception e) {
							System.out.println("e + " + e.getMessage());
						}

					}

					// TODO Auto-generated method stub
					
				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub
					
				}
			} );
		
		}
		else
		{
			path = "";
			cf.ShowToast("Captured image not saved", 0);
			finish();
		}
	}
	private void showSavedcaption() {
		// TODO Auto-generated method stub
		
		Cursor c1 = cf.SelectTablefunction(cf.Photo_caption,
			" WHERE ARR_IMP_Elevation='" + elev
					+ "' and ARR_IMP_Insepctiontype='"+inspections+"' and ARR_IMP_ImageOrder='" + (imageorder) + "'");
		String temp[]=null;
		if(c1.getCount()>0)
		{	
			temp = new String[c1.getCount() + 2];
		temp[0] = "Select";
		temp[1] = "ADD PHOTO CAPTION";
		int i = 2;
		c1.moveToFirst();
		do {
			temp[i] = cf.decode(c1.getString(c1
					.getColumnIndex("ARR_IMP_Caption")));
			i++;
		} while (c1.moveToNext());
	} else {
		temp = new String[2];
		temp[0] = "Select";
		temp[1] = "ADD PHOTO CAPTION";
	}
		ArrayAdapter adapter2 = new ArrayAdapter(TakeCamera.this,android.R.layout.simple_spinner_item, temp);
		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp3.setAdapter(adapter2);
	}
	private void showSavedcaption(String val) {
		// TODO Auto-generated method stub
		
		Cursor c1 = cf.SelectTablefunction(cf.Photo_caption,
			" WHERE ARR_IMP_Elevation='" + elev
					+ "' and ARR_IMP_Insepctiontype='"+inspection+"' and ARR_IMP_ImageOrder='" + (imageorder) + "'");
		String temp[]=null;
		if(c1.getCount()>0)
		{	
			temp = new String[c1.getCount() + 2];
		temp[0] = "Select";
		temp[1] = "ADD PHOTO CAPTION";
		int i = 2;
		c1.moveToFirst();
		do {
			temp[i] = cf.decode(c1.getString(c1
					.getColumnIndex("ARR_IMP_Caption")));
			if(temp[i].equals(val))
			{
				pos=i;
			}
			i++;
		} while (c1.moveToNext());
	} else {
		temp = new String[2];
		temp[0] = "Select";
		temp[1] = "ADD PHOTO CAPTION";
	}
		ArrayAdapter adapter2 = new ArrayAdapter(TakeCamera.this,android.R.layout.simple_spinner_item, temp);
		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp3.setAdapter(adapter2);
		sp3.setSelection(pos);
		//pos=0;
	}
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.helpclose:
			finish();
			break;
		case R.id.cancel:
		cf.ShowToast("Image discarded!", 0);
		finish();
		break;
		case R.id.save:
			//cf.ShowToast("You have saved the image successfully ", 1);
			
			inspection=sp1.getSelectedItem().toString();
			if(sp2.getSelectedItemPosition()==-1)
			{
				elev=0;
			}
			else
			{
			elev=sp2.getSelectedItemPosition();
			}
			String Desc="";
			if(sp3.getSelectedItem()==null)
			{
				 Desc="Select";
			}
			else
			{
				Desc=sp3.getSelectedItem().toString();
			}
			
			String s="";
			
			File f= new File(path);
			if(!f.exists())
			{
				s="Captured image not saved";
				finish();
			}
			else if(inspections.equals("-Select-"))
			{
				s="Please select Inspection";
			}
			else if(elev==0)
				{
					s="Please select Type under "+sp1.getSelectedItem().toString();
				}
			else  if(Desc.equals("Select"))
			{
				s="Please select Caption";
			}
			
			if(s.equals(""))
			{
				
				
				Cursor SI_c=cf.SelectTablefunction(cf.Select_insp, " WHERE SI_srid ='"+cf.selectedhomeid+"'");
				if(SI_c.getCount()>0)
				{
					SI_c.moveToFirst();
					String  selinsptype = cf.decode(SI_c.getString(SI_c.getColumnIndex("SI_InspectionNames")));
					if(selinsptype.length()>0)
		    		{
						
						try
		    			{
			    			Cursor c =cf.SelectTablefunction(cf.inspnamelist, " Where  ARR_Insp_ID in ("+selinsptype+")  order by ARR_Custom_ID");
			    			System.out.println("c=="+c.getCount());
			    			if(c.getCount()>0)
			    			{
			    				c.moveToFirst();
			    				separated=new String[c.getCount()];
			    				for(int i=1;i<c.getCount()+1;i++,c.moveToNext())
			    				{
			    				
			    					separated[i-1]=cf.decode(c.getString(c.getColumnIndex("ARR_Insp_Name"))).trim();;
			    					System.out.println("arr=="+separated[i]);
			    				}
			    			}
		    			}
		    			catch (Exception e) {
							// TODO: handle exception
		    				System.out.println("second issues="+e.getMessage());
						}
						
		    			/*Cursor c =cf.SelectTablefunction(cf.inspnamelist, " Where  ARR_Custom_ID in ("+selinsptype+")  order by ARR_Custom_ID");
		    			if(c.getCount()>0)
		    			{
		    				c.moveToFirst();
		    				separated=new String[c.getCount()];

		    				for(int i=0;i<c.getCount();i++,c.moveToNext())
		    				{
		    					
		    					separated[i]=cf.decode(c.getString(c.getColumnIndex("ARR_Insp_Name"))).trim();
		    				}
		    			}*/
		    		}
					insert_photos(Desc);
				
				}
				/**Save the rotated value in to the external stroage place **//*
				System.out.println("current rotate"+currnet_rotated);*/
				if(currnet_rotated>0)
				{ 
					

					try
					{
						/**Create the new image with the rotation **/
						String current=MediaStore.Images.Media.insertImage(getContentResolver(), rotated_b, "My bitmap", "My rotated bitmap");
						  ContentValues values = new ContentValues();
						  values.put(MediaStore.Images.Media.ORIENTATION, 0);
						  TakeCamera.this.getContentResolver().update(Uri.parse(current), values, MediaStore.Images.Media.DATA+ "=?", new String[] { current } );
						
						
						if(current!=null)
						{
						String path=getPath(Uri.parse(current));
						File fout = new File(this.path);
						fout.delete();
						/** delete the selected image **/
						File fin = new File(path);
						/** move the newly created image in the slected image pathe ***/
						fin.renameTo(new File(this.path));
						
						
					}
					} catch(Exception e)
					{
						System.out.println("Error occure while rotate the image "+e.getMessage());
					}
					
				}
			 
			/**Save the rotated value in to the external stroage place ends **/
				cf.ShowToast(sp2.getSelectedItem().toString() +" Image Saved Successfully", 1);
				finish();
			}
			else
			{
				cf.ShowToast(s, 0);
			}
			
			break;
		default:
			break;
		}
	}
	
	private void insert_photos(String Desc) {
		// TODO Auto-generated method stub
		/*if(elev==23)
		{
			String[] value=	data.getExtras().getStringArray("Selected_array");
			getinspectionid(insp_sp.getSelectedItem().toString());
			for(int m=0;m<value.length;m++)
			{	
			
					cf.arr_db.execSQL("INSERT INTO "
							+ cf.ImageTable
							+ " (ARR_IM_SRID,ARR_IM_Insepctiontype,ARR_IM_Elevation,ARR_IM_ImageName,ARR_IM_Description,ARR_IM_Nameext,ARR_IM_CreatedOn,ARR_IM_ModifiedOn,ARR_IM_ImageOrder)"
							+ " VALUES ('" + cf.selectedhomeid + "','"+customid+"','"
							+ elev + "','"
							+ cf.encode(path) + "','"
							+ cf.encode(Desc)
							+ "','','"
							+ cf.datewithtime + "','" +cf.datewithtime + "','" + imageorder + "')");	
					
			//}
		}
		else
		{*/
			String name=sp2.getSelectedItem().toString();System.out.println("nae="+name);
			System.out.println("length="+separated.length);
			for(int i=0;i<separated.length;i++)
		    {
				for(int j=0;j<cf.All_insp_list.length;j++)
				{
					if(cf.All_insp_list[j].trim().equals(separated[i].trim()))
					{
						String arr[] = sf.getelevation(separated[i].trim());
						for(int k=0;k<arr.length;k++)
			 			{
			 				System.out.println("this comes wrong"+name+"check="+arr[i]);
			 				if(name.trim().equals(arr[k].trim()))
			 				{
			 					
			 					if(j==4)
			 					{
			 						if(cf.Stories.equals("1") || cf.Stories.equals("2")  || cf.Stories.equals("3"))
			 						{
			 							j=4;		
			 						}
			 						else if(cf.Stories.equals("4") || cf.Stories.equals("5")  || cf.Stories.equals("6"))
			 						{
			 							j=5;		
			 						}
			 						else if(Integer.parseInt(cf.Stories)>7)
			 						{	
			 							j=6;
			 						}
			 					}
			 					
			 					int elev= getelvationnumber(name, String.valueOf(j));		
			 					if(elev==23)
			 					{
			 						max_allowed=1;
			 					}
			 					Cursor c15 = cf.arr_db.rawQuery("SELECT * FROM "
			 	        				+ cf.ImageTable + " WHERE ARR_IM_SRID='"
			 	        				+ cf.selectedhomeid + "' and ARR_IM_Elevation='"
			 	        				+ elev + "'  and  ARR_IM_Insepctiontype='"+j+"' ORDER BY ARR_IM_ImageOrder DESC", null);
			 					//String[] value=	data.getExtras().getStringArray("Selected_array"); 
			 					if(c15.getCount()>0)
				 				{
				 					c15.moveToFirst();
				 					int elevdb = c15.getInt(c15.getColumnIndex("ARR_IM_ImageOrder"));
				 					
					 					if(c15.getCount()<max_allowed)
						 				{
						 					/*for(int m=0;m<value.length;m++)
											{*/	
					 						
					 						
						 						if((elevdb+1)<=max_allowed)
					 							{
						 							System.out
															.println("sddsd");
						 							Cursor c1 = cf.arr_db.rawQuery("SELECT * FROM "
								 	        				+ cf.ImageTable + " WHERE ARR_IM_SRID='"
								 	        				+ cf.selectedhomeid + "' and ARR_IM_Elevation='"
								 	        				+ elev + "'  and  ARR_IM_Insepctiontype='"+j+"' and ARR_IM_ImageName='"+cf.encode(path)+"'", null);
						 							if(c1.getCount()==0)
						 							{
						 								System.out
																.println("INSERT INTO "
									 							+ cf.ImageTable
									 							+ " (ARR_IM_SRID,ARR_IM_Insepctiontype,ARR_IM_Elevation,ARR_IM_ImageName,ARR_IM_Description,ARR_IM_Nameext,ARR_IM_CreatedOn,ARR_IM_ModifiedOn,ARR_IM_ImageOrder)"
									 							+ " VALUES ('" + cf.selectedhomeid + "','"+j+"','"
									 							+ elev + "','"
									 							+ cf.encode(path) + "','"
									 							+ cf.encode(Desc)
									 							+ "','','"
									 							+ cf.datewithtime + "','" +cf.datewithtime + "','" + (elevdb+1) + "')");
														cf.arr_db.execSQL("INSERT INTO "
									 							+ cf.ImageTable
									 							+ " (ARR_IM_SRID,ARR_IM_Insepctiontype,ARR_IM_Elevation,ARR_IM_ImageName,ARR_IM_Description,ARR_IM_Nameext,ARR_IM_CreatedOn,ARR_IM_ModifiedOn,ARR_IM_ImageOrder)"
									 							+ " VALUES ('" + cf.selectedhomeid + "','"+j+"','"
									 							+ elev + "','"
									 							+ cf.encode(path) + "','"
									 							+ cf.encode(Desc)
									 							+ "','','"
									 							+ cf.datewithtime + "','" +cf.datewithtime + "','" + (elevdb+1) + "')");
						 							}
												}
											//}
						 				}
				 				}
				 				else
				 				{
				 						if(c15.getCount()<max_allowed)
			 							{
					 						/*for(int m=0;m<value.length;m++ )
											{*/	
					 							Cursor c2 = cf.arr_db.rawQuery("SELECT * FROM "
							 	        				+ cf.ImageTable + " WHERE ARR_IM_SRID='"
							 	        				+ cf.selectedhomeid + "' and ARR_IM_Elevation='"
							 	        				+ elev + "'  and  ARR_IM_Insepctiontype='"+j+"' and ARR_IM_ImageName='"+cf.encode(path)+"'", null);
					 							if(c2.getCount()==0)
					 							{
					 								cf.arr_db.execSQL("INSERT INTO "
								 							+ cf.ImageTable
								 							+ " (ARR_IM_SRID,ARR_IM_Insepctiontype,ARR_IM_Elevation,ARR_IM_ImageName,ARR_IM_Description,ARR_IM_Nameext,ARR_IM_CreatedOn,ARR_IM_ModifiedOn,ARR_IM_ImageOrder)"
								 							+ " VALUES ('" + cf.selectedhomeid + "','"+j+"','"
								 							+ elev + "','"
								 							+ cf.encode(path) + "','"
								 							+ cf.encode(Desc)
								 							+ "','','"
								 							+ cf.datewithtime + "','" +cf.datewithtime + "','" + imageorder + "')");	
					 							}
					 						//}															
					 					}
				 					}
			 					}
			 			}
			 		}
				}
		    }

		//}
	}
	
	
	 public void getinspectionid(String inspname) 
	 {
			// TODO Auto-generated method stub
		 
	   	 Cursor c =cf.SelectTablefunction(cf.inspnamelist, " Where ARR_Insp_Name='"+cf.encode(inspname)+"'");
	   	 if(c.getCount()>0)
	   	 {
	   		 c.moveToFirst();
	   		 customid = cf.decode(c.getString(c.getColumnIndex("ARR_Custom_ID"))); 
	   	 }
	 }
	private String[] getelevation(String selected)
	{
		String[] arr=null;
		if(selected.trim().equals(cf.All_insp_list[3]))
		{
			arr=getResources().getStringArray(R.array.B1_1802_Inspection);					
		}
		else if(selected.trim().equals(cf.All_insp_list[1]))
		{
			arr=getResources().getStringArray(R.array.Four_Point_Inspection);					
		}
		else if(selected.trim().equals(cf.All_insp_list[4]))
		{
			arr=getResources().getStringArray(R.array.Commercial_Type_I);					
		}
		else if(selected.trim().equals(cf.All_insp_list[5]))
		{
			arr=getResources().getStringArray(R.array.Commercial_Type_II);					
		}
		else if(selected.trim().equals(cf.All_insp_list[6]))
		{
			arr=getResources().getStringArray(R.array.Commercial_Type_III);					
		}
		else if(selected.trim().equals(cf.All_insp_list[7]))
		{
			arr=getResources().getStringArray(R.array.General_Conditions_Hazards);					
		}
		else if(selected.trim().equals(cf.All_insp_list[8]))
		{
			arr=getResources().getStringArray(R.array.Sinkhole_Inspection);					
		}
		else if(selected.trim().equals(cf.All_insp_list[9]))
		{
			arr=getResources().getStringArray(R.array.Chinese_Drywall);					
		}
		else if(selected.trim().equals(cf.All_insp_list[2]))
		{
			arr=getResources().getStringArray(R.array.Roof_Survey);		
			//max_allowed=2;
		}
		return arr;
	}
public String getPath(Uri uri) {
	String[] projection = { MediaStore.Images.Media.DATA };
	Cursor cursor = managedQuery(uri, projection, null, null, null);
	int column_index = cursor
			.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
	cursor.moveToFirst();
	return cursor.getString(column_index);
}
public int getelvationnumber(String selected,String insp)
{
	
	int elev=0;
	if(insp.equals("1"))
	{
		if(selected.equals("Exterior Photographs"))elev=1;
		if(selected.equals("Roof Photographs"))elev=2;
		if(selected.equals("Plumbing Photographs"))elev=3;
		if(selected.equals("Electrical Photographs"))elev=4;
		if(selected.equals("HVAC Photographs"))elev=5;
		if(selected.equals("Other Photographs"))elev=7;
		if(selected.equals("Front Photo"))elev=23;
	}
	else if(insp.equals("2"))
	{
		if(selected.equals("Roof Photographs"))elev=2;
		if(selected.equals("Other Photographs"))elev=7;
		/**need to change ***/
		if(selected.equals("Exterior Photographs"))elev=1;
		if(selected.equals("Attic Photographs"))elev=6;
		if(selected.equals("Front Photo"))elev=23;
		if(selected.equals("Roof Cover Type Photos"))elev=24;
		
	}
	else if(insp.equals("3"))
	{
		//if(selected.equals("Owner signature"))elev=17;
		if(selected.equals("Front Elevation"))elev=17;
		if(selected.equals("Right Elevation"))elev=18;
		if(selected.equals("Left Elevation"))elev=19;
		if(selected.equals("Back Elevation"))elev=20;
		if(selected.equals("Attic Photographs"))elev=6;
		if(selected.equals("Additional Photographs"))elev=12;
		if(selected.equals("Other Photographs"))elev=7;
		if(selected.equals("Front Photo"))elev=23;
	}
	else if(insp.equals("4"))
	{
		//if(selected.equals("Owner signature"))elev=25;
		if(selected.equals("Front Elevation"))elev=17;
		if(selected.equals("Right Elevation"))elev=18;
		if(selected.equals("Left Elevation"))elev=19;
		if(selected.equals("Back Elevation"))elev=20;
		if(selected.equals("Attic Photographs"))elev=6;
		if(selected.equals("Additional Photographs"))elev=12;
		if(selected.equals("Other Photographs"))elev=7;
		if(selected.equals("Front Photo"))elev=23;
	}
	else if(insp.equals("5"))
	{
		//if(selected.equals("Owner signature"))elev=34;
		if(selected.equals("Front Elevation"))elev=17;
		if(selected.equals("Right Elevation"))elev=18;
		if(selected.equals("Left Elevation"))elev=19;
		if(selected.equals("Back Elevation"))elev=20;
		if(selected.equals("Attic Photographs"))elev=6;
		if(selected.equals("Additional Photographs"))elev=12;
		if(selected.equals("Other Photographs"))elev=7;
		if(selected.equals("Front Photo"))elev=23;
	}
	else if(insp.equals("6"))
	{
		//if(selected.equals("Owner signature"))elev=42;
		if(selected.equals("Front Elevation"))elev=17;
		if(selected.equals("Right Elevation"))elev=18;
		if(selected.equals("Left Elevation"))elev=19;
		if(selected.equals("Back Elevation"))elev=20;
		if(selected.equals("Attic Photographs"))elev=6;
		if(selected.equals("Additional Photographs"))elev=12;
		if(selected.equals("Other Photographs"))elev=7;
		if(selected.equals("Front Photo"))elev=23;
	}
	else if(insp.equals("7"))
	{
		if(selected.equals("Front Elevation"))elev=17;
		if(selected.equals("Right Elevation"))elev=18;
		if(selected.equals("Left Elevation"))elev=19;
		if(selected.equals("Back Elevation"))elev=20;
		if(selected.equals("Attic Photographs"))elev=6;
		if(selected.equals("Additional Photographs"))elev=12;
		if(selected.equals("Roof Photographs"))elev=2;
		if(selected.equals("Other Photographs"))elev=7;
		if(selected.equals("Front Photo"))elev=70;
	}
	else if(insp.equals("8"))
	{
		//if(selected.equals("Owner signature"))elev=50;
		if(selected.equals("External Photographs"))elev=8;
		if(selected.equals("Roof and attic Photographs"))elev=9;
		if(selected.equals("Ground and adjoining images"))elev=10;
		if(selected.equals("Internal Photographs"))elev=11;
		if(selected.equals("Additional Photographs"))elev=12;
		if(selected.equals("Other Photographs"))elev=7;
		if(selected.equals("Front Photo"))elev=23;
	}
	else if(insp.equals("9"))
	{
		/*if(selected.equals("Copper wiring"))elev=57;
		if(selected.equals("Copper wiring at outlets and switches"))elev=58;
		if(selected.equals("Metal Fixtures"))elev=59;
		if(selected.equals("Ceiling board stamps"))elev=60;*/
		if(selected.equals("Other Photographs"))elev=7;
		if(selected.equals("Front Photo"))elev=23;
		if(selected.equals("Interior Photographs"))elev=25;
		if(selected.equals("HVAC Photographs"))elev=5;
		if(selected.equals("Electrical Photographs"))elev=4;
		if(selected.equals("Plumbing Photographs"))elev=3;
		if(selected.equals("Exterior Photographs"))elev=1;
		if(selected.equals("Interior Fixture Photographs"))elev=26;
		if(selected.equals("Drywall Photographs"))elev=27;
		
	
	}
	return elev;
}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}
}