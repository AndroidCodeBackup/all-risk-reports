package idsoft.inspectiondepot.ARR;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;



public class pickSingleimage extends Activity {
String path="";
CommonFunctions cf;
boolean clear=false;
int id=0;
Bitmap b1=null;
int currnet_rotated=0;
Button rotateleft,rotateright;
private Uri CapturedImageURI;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.pick_single_image);
		cf=new CommonFunctions(this);
		Bundle b= getIntent().getExtras();
		if(b!=null)
		{
			path=b.getString("path");
			id=b.getInt("id");
			if(path!=null && !path.equals(""))
			{
				File f=new File(path);
				
				if(f.exists());
				{
					b1=cf.ShrinkBitmap(path, 400, 400);
					((ImageView) findViewById(R.id.Roof_img)).setImageBitmap(b1);
				}
			}
		}
		
		rotateleft=(Button) findViewById(R.id.rotateleft);
		rotateright=(Button) findViewById(R.id.rotateright);
		if(path==null || path.equals(""))
		{
			rotateleft.setVisibility(View.GONE);
			rotateright.setVisibility(View.GONE);
		}
		
		rotateright.setOnClickListener(new OnClickListener() {  
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				System.gc();
				currnet_rotated+=90;
				if(currnet_rotated>=360)
				{
					currnet_rotated=0;
				}
				
				Bitmap myImg;
				try {
					myImg = BitmapFactory.decodeStream(new FileInputStream(path));
					Matrix matrix =new Matrix();
					matrix.reset();
					//matrix.setRotate(currnet_rotated);
					System.out.println("Ther is no more issues top ");
					matrix.postRotate(currnet_rotated);
					
					 b1  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
					        matrix, true);
					 System.gc();
					 ((ImageView) findViewById(R.id.Roof_img)).setImageBitmap(b1);

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (Exception e) {
					
				}
				catch (OutOfMemoryError e) {
					System.out.println("comes in to out ot mem exception");
					System.gc();
					try {
						myImg=null;
						System.gc();
						Matrix matrix =new Matrix();
						matrix.reset();
						//matrix.setRotate(currnet_rotated);
						matrix.postRotate(currnet_rotated);
						myImg= cf.ShrinkBitmap(path, 800, 800);
						b1  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
						
						System.gc();
						((ImageView) findViewById(R.id.Roof_img)).setImageBitmap(b1); 

					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					 catch (OutOfMemoryError e1) {
							// TODO Auto-generated catch block
						 cf.ShowToast("You cannot rotate this image. Image size exceeds 2MB.",0);
					}
				}

			}
		});
		rotateleft.setOnClickListener(new OnClickListener() {  			
			public void onClick(View v) {

				// TODO Auto-generated method stub
			
				System.gc();
				currnet_rotated-=90;
				if(currnet_rotated<0)
				{
					currnet_rotated=270;
				}

				
				Bitmap myImg;
				try {
					myImg = BitmapFactory.decodeStream(new FileInputStream(path));
					Matrix matrix =new Matrix();
					matrix.reset();
					//matrix.setRotate(currnet_rotated);
					System.out.println("Ther is no more issues top ");
					matrix.postRotate(currnet_rotated);
					
					 b1  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
					        matrix, true);
					 
					 ((ImageView) findViewById(R.id.Roof_img)).setImageBitmap(b1);

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (Exception e) {
					
				}
				catch (OutOfMemoryError e) {
					System.out.println("comes in to out ot mem exception");
					System.gc();
					try {
						myImg=null;
						System.gc();
						Matrix matrix =new Matrix();
						matrix.reset();
						//matrix.setRotate(currnet_rotated);
						matrix.postRotate(currnet_rotated);
						myImg= cf.ShrinkBitmap(path, 800, 800);
						b1  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
						System.gc();
						((ImageView) findViewById(R.id.Roof_img)).setImageBitmap(b1); 

					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					 catch (OutOfMemoryError e1) {
							// TODO Auto-generated catch block
						 cf.ShowToast("You cannot rotate this image. Image size exceeds 2MB.",0);
					}
				}

			
			}
		});
	
		
		
	}
		
	
	public void clicker(View v)
	{
	switch(v.getId())
	 {
	case R.id.save:
		if(path!=null )
		{
			if(!path.trim().equals(""))
			{
				System.out.println("current rotate"+currnet_rotated);
				if(currnet_rotated>0)
				{ 
					

					try
					{
						/**Create the new image with the rotation **/
						String current=MediaStore.Images.Media.insertImage(getContentResolver(), b1, "My bitmap", "My rotated bitmap");
						  ContentValues values = new ContentValues();
						  values.put(MediaStore.Images.Media.ORIENTATION, 0);
						  pickSingleimage.this.getContentResolver().update(Uri.parse(current), values, MediaStore.Images.Media.DATA+ "=?", new String[] { current } );
						
						
						if(current!=null)
						{
						String path=getPath(Uri.parse(current));
						File fout = new File(this.path);
						fout.delete();
						/** delete the selected image **/
						File fin = new File(path);
						/** move the newly created image in the slected image pathe ***/
						fin.renameTo(new File(this.path));
						
						
					}
					} catch(Exception e)
					{
						System.out.println("Error occure while rotate the image "+e.getMessage());
					}
					
				}
				Intent n= new Intent();
				n.putExtra("path", path);
				n.putExtra("id", id);
				setResult(RESULT_OK,n);
				finish();
			}
			else
			{
				cf.ShowToast("Please select image", 0);
			}
		}
		else
		{
			cf.ShowToast("Please select image", 0);
		}
	break;
	case R.id.clear:
		path="";
		((ImageView) findViewById(R.id.Roof_img)).setImageDrawable(getResources().getDrawable(R.drawable.photonotavail));
		rotateleft.setVisibility(View.GONE);
		rotateright.setVisibility(View.GONE);
		clear=true;
	break;
	case R.id.cancel:
		
		/*if((!path.trim().equals("") || clear))
		{
			Intent n= new Intent();
			n.putExtra("path", path);
			n.putExtra("id", id);
			setResult(RESULT_OK,n);
			finish();
		}
		else
		{*/
			Intent n1= new Intent();
			n1.putExtra("id", id);
			setResult(RESULT_CANCELED,n1);
			finish();
		//}
	break;
	case R.id.browsecamera:
	
			String fileName = "temp.jpg";
			ContentValues values = new ContentValues();
			values.put(MediaStore.Images.Media.TITLE, fileName);
			CapturedImageURI = getContentResolver().insert(
					MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			intent.putExtra(MediaStore.EXTRA_OUTPUT, CapturedImageURI);
			startActivityForResult(intent, 0);

	break;
	case R.id.browsetxt:
		
				Intent reptoit1 = new Intent(this,Select_phots.class);
				Bundle b=new Bundle();
				reptoit1.putExtra("Selectedvalue", ""); /**Send the already selected image **/
				reptoit1.putExtra("Maximumcount", 0);/**Total count of image in the database **/
				reptoit1.putExtra("Total_Maximumcount", 1); /***Total count of image we need to accept**/
				reptoit1.putExtra("ok", "true"); /***Total count of image we need to accept**/
				//reptoit1.setClassName("com.idinspection","com.idinspection.Select_phots");
				startActivityForResult(reptoit1,121); /** Call the Select image page in the idma application  image ***/
		/*}
		else
		{
			cf.ShowToast("Please install IDMA new version", 0);
		}*/
				/***We call the centralized image selection part ends **/
	break;
	
	default:
		break;
	}

	
	
	}
	public String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		String selectedImagePath = "", picname = "";
		try
		{
			if (resultCode == RESULT_OK) {
				if (requestCode == 0) {
					String[] projection = { MediaStore.Images.Media.DATA };
					Cursor cursor = managedQuery(CapturedImageURI, projection,
							null, null, null);
					int column_index_data = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					cursor.moveToFirst();
					path  = cursor.getString(column_index_data);
				}
				else if(requestCode == 121)
				{
					/** we get the result from the Idma page for this elevation **/
				String[] value=	data.getExtras().getStringArray("Selected_array"); /**We pass the array of tje value from the IDAM Select page **/
					if(value.length>0)
					{
						if(!value[0].equals(""))
						path=value[0];
					}
						
				}
			} else {
				//path = "";
				// edbrowse.setText("");
			}
		
		}
		catch(Exception e)
		{
			path="";
		}
		File f=new File(path);
		if(f.exists())
		{
			b1=cf.ShrinkBitmap(path, 400, 400);
			((ImageView) findViewById(R.id.Roof_img)).setImageBitmap(b1);
				rotateleft.setVisibility(View.VISIBLE);
				rotateright.setVisibility(View.VISIBLE);
			
		}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if((!path.trim().equals("") || clear))
			{
				Intent n= new Intent();
				n.putExtra("path", path);
				n.putExtra("id", id);
				setResult(RESULT_OK,n);
				finish();
			}
			else
			{
				Intent n1= new Intent();
				n1.putExtra("id", id);
				setResult(RESULT_CANCELED,n1);
				finish();
			}
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}
}
