/*
   ************************************************************
   * Project: ALL RISK REPORT
   * Module : MyOnclickListener.java
   * Creation History:
      Created By : Gowri on 
   * Modification History:
      Last Modified By : Gowri on 1/16/2013
   ************************************************************  
*/
package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.text.InputFilter;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

public class MyOnclickListener  extends LinearLayout {
	private static final int visibility = 0;
	public Context context = null;
	Context activityname;
	private int app,roofiden;
	private int sub,innersub;
	private CommonFunctions cf;
	public Button btn_custom,btn_general,btn_four,btn_rfsurvey,btn_wind,btn_gch,btn_sinkhole,gen_btncloud,btn_signature,btn_elevation,btn_uploadedimg,
    btn_chinese,btn_photos,btn_feedback,btn_map,btn_submit,gen_btnschedule,gen_btnph,bntmain_ph,gen_btncall,gen_btnagnt,gen_btnbuild,four_btnroof,four_btnattic,four_btnplumb,
	four_btnelectrical,four_btnhvac,four_addendum,w1802_btnbcode,w1802_btnrfcvr,w1802_btnrfdeck,w1802_btnrfwall,w1802_btnrfgeo,w1802_btnswr,
	w1802_btnopen,w1802_btnwall,gen_btnddi,gen_btnweather,sink_btnsummary,sink_btnobs1,sink_btnobs2,sink_btnobs3,windaddendum,
	sink_btnobs4,gch_btnpp,gch_btnpf,gch_btnroof,gch_btnsum,gch_btnfire,gch_btnhaz,gch_btnhvac,gch_btnelectric,gch_btnrestsupp,
	gch_btncomments,w1802_btncommareas,w1802_btnroofsys,w1802_btnauxbuild,w1802_btnrestsupp,w1802_btncommunalareas,
	cd_btnsummary,cd_btnvar,cd_btnihsys,cd_btniasys,cd_btnattc,cd_btncomm,buil_gendata,buil_location,buil_rcv,
	buil_observation,buil_iso,buil_foundation,buil_wallstructure,buil_wallcladding,buil_seperation,
	buil_windowopenings,buil_dooropenings,buil_skylights,buil_comments,commwind_btnterrain,commwind_btnroofcover,
	commwind_btnroofdeck,commwind_btnswr,commwind_btnopenprot,commwind_btncertification,comm_wind,addendum,comm_rcv,
	w1802_windowopen,w1802_dooropen,w1802_skylights,gch_btnhoodcut,gch_btnfireprot,gch_btnmisc;
	public RelativeLayout relfour,rel1802,rcvsubmenu;
	public View v1;
	String str="",s1="",s2="",s3="",getinsptxtname="";
	Cursor chkatc_save = null;
	ImageView im,icon_policy,icon_insp,editbno_img,icon_abbreviation,btn_home;
	TextView w1802_dummytxtviewcomm;
	RadioButton b1802_chk1,comm1_chk1,comm2_chk1,comm3_chk1;
	Cursor c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11;
	public static int count =0;
	public MyOnclickListener(Context context, int appname,
			int sub,int innersub,CommonFunctions cf) {
		super(context);
		this.context = context; /** actiivty name **/
		this.app = appname;  /** class name **/
		this.sub = sub; /** in order to identify main/sub menu **/
		this.innersub = innersub;
		this.cf = cf;
		create();
	}

	public MyOnclickListener(Context context, AttributeSet attrs, int appname,
			 int sub,int innersub,CommonFunctions cf) {
		super(context, attrs);
		this.context = context;
		this.app = appname;
		this.sub = sub;
		this.innersub = innersub;
		this.cf = cf;
		create();
	}
	private void create() {
		LayoutInflater layoutInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		/** menu **/
		 
		if(this.sub==0){
			layoutInflater.inflate(R.layout.menuclick, this, true);
			
			btn_general = (Button) findViewById(R.id.generalinfo);
			btn_four = (Button) findViewById(R.id.four);
			btn_rfsurvey = (Button) findViewById(R.id.rfsurvey);
			btn_wind = (Button) findViewById(R.id.wind);
			btn_gch = (Button) findViewById(R.id.gch);
			btn_sinkhole = (Button) findViewById(R.id.sinkhole);
			btn_chinese = (Button) findViewById(R.id.chinese);
			btn_photos = (Button) findViewById(R.id.photos);
			btn_feedback = (Button) findViewById(R.id.feedback);
			btn_map = (Button) findViewById(R.id.map);
			btn_submit = (Button) findViewById(R.id.submit);
			btn_home = (ImageView) findViewById(R.id.home);
			btn_custom = (Button) findViewById(R.id.custom);
			/*if(cf.URL_ARR.contains("72.15.221.151:81"))
			{
	   			((TextView)findViewById(R.id.rcdate)).setText(cf.apkrc);
			}
	   		else
	   		{
	   			((TextView)findViewById(R.id.rcdate)).setText(cf.apkrc.replace("U", "L"));
	   		}*/
			
			
			/* Cursor IPT_retrive=cf.SelectTablefunction(cf.Select_insp, " where SI_srid='"+cf.selectedhomeid+"'");
			   if(IPT_retrive.getCount()>0)
			   {  
				 IPT_retrive.moveToFirst();
				 String  selinsptype = cf.decode(IPT_retrive.getString(IPT_retrive.getColumnIndex("SI_InspectionNames")));
				 cf.getinspectiotextname(selinsptype);
				 
				 if(cf.selinsptext.contains(cf.All_insp_list[1]))
				 {
					 btn_four.setBackgroundDrawable(cf.con.getResources().getDrawable(R.drawable.fourpointb));
				 }
				 else
				 {
					 btn_four.setBackgroundDrawable(cf.con.getResources().getDrawable(R.drawable.fourpointbdes));
				 }
				 if(cf.selinsptext.contains(cf.All_insp_list[2]))
				 {
					 btn_rfsurvey.setBackgroundDrawable(cf.con.getResources().getDrawable(R.drawable.roofb));
				 }
				 else
				 {
					 btn_rfsurvey.setBackgroundDrawable(cf.con.getResources().getDrawable(R.drawable.roofbdes));
				 }
				 if(cf.selinsptext.contains(cf.All_insp_list[3]) || cf.selinsptext.contains(cf.All_insp_list[4])
						 || cf.selinsptext.contains(cf.All_insp_list[5])||cf.selinsptext.contains(cf.All_insp_list[6]))
				 {
					 btn_wind.setBackgroundDrawable(cf.con.getResources().getDrawable(R.drawable.windmitb));
				 }
				 else
				 {
					 btn_wind.setBackgroundDrawable(cf.con.getResources().getDrawable(R.drawable.windmitbdes));
				 }
				 if(cf.selinsptext.contains(cf.All_insp_list[7]))
				 {
					 btn_gch.setBackgroundDrawable(cf.con.getResources().getDrawable(R.drawable.gchb));
				 }
				 else
				 {
					 btn_gch.setBackgroundDrawable(cf.con.getResources().getDrawable(R.drawable.gchbdes));
				 }
				 if(cf.selinsptext.contains(cf.All_insp_list[8]))
				 {
					 btn_sinkhole.setBackgroundDrawable(cf.con.getResources().getDrawable(R.drawable.sinkholeb));
				 }
				 else
				 {
					 btn_sinkhole.setBackgroundDrawable(cf.con.getResources().getDrawable(R.drawable.sinkholebdes));
				 }
				 if(cf.selinsptext.contains(cf.All_insp_list[9]))
				 {
					 btn_chinese.setBackgroundDrawable(cf.con.getResources().getDrawable(R.drawable.chinesedwb));
				 }
				 else
				 {
					 btn_chinese.setBackgroundDrawable(cf.con.getResources().getDrawable(R.drawable.chinesedwbdes));
				 }
			   }*/

			Cursor s=cf.SelectTablefunction(cf.Select_insp, " WHERE SI_srid='"+cf.selectedhomeid+"'");
	    	s.moveToFirst();
	    	System.out.println("its works"+s.getCount());
	    	if(s.getCount()>0)
	    	{
	    		String tmp=cf.decode(s.getString(s.getColumnIndex("SI_InspectionNames"))).trim();		    		
	    		if(tmp.length()>0)
	    		{		    			
	    			System.out.println("the quesry was "+"Where  ARR_Custom_ID in ('"+tmp+"')  order by ARR_Custom_ID");
	    			try
	    			{
		    			Cursor c =cf.SelectTablefunction(cf.inspnamelist, " Where  ARR_Insp_ID in ("+tmp+")  order by ARR_Custom_ID");
		    			System.out.println("some ieeuse"+c.getCount());
		    			if(c.getCount()>0)
		    			{
		    				 btn_photos.setBackgroundDrawable(cf.con.getResources().getDrawable(R.drawable.photosb));
		    			}
		    			else
		    			{
		    				btn_photos.setBackgroundDrawable(cf.con.getResources().getDrawable(R.drawable.photosdis));
		    			}
	    			}
	    			catch (Exception e) {
						// TODO: handle exception
	    				System.out.println("second issues="+e.getMessage());
					}
	    		}		    		
	    	}

	    	
			btn_general.setOnClickListener(new clicker());
			btn_four.setOnClickListener(new clicker());
			btn_rfsurvey.setOnClickListener(new clicker());
			btn_wind.setOnClickListener(new clicker());
			btn_gch.setOnClickListener(new clicker());
			btn_sinkhole.setOnClickListener(new clicker());
			btn_chinese.setOnClickListener(new clicker());
			btn_photos.setOnClickListener(new clicker());
			btn_feedback.setOnClickListener(new clicker());
			btn_map.setOnClickListener(new clicker());
			btn_submit.setOnClickListener(new clicker());
			btn_home.setOnClickListener(new clicker());
			btn_custom.setOnClickListener(new clicker());
			
			
		}
		else if(this.sub==1) /** SUBMENUS **/
		{
			if(this.app==450 || this.app==461)
			{
				 layoutInflater.inflate(R.layout.buildingsubmenu, this, true);
				 RelativeLayout relgen1 = (RelativeLayout) findViewById(R.id.building_submenu);
				 relgen1.setVisibility(visibility);
				 
				 buil_gendata = (Button) findViewById(R.id.bigendata);				
				 buil_comments = (Button) findViewById(R.id.bicomments);
				 
				 cf.Check_BI();
				 if(!cf.Gendata_val.equals(""))
				 {
					 buil_gendata.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				 }				
				 if(!cf.comments.equals(""))
				 {
					 buil_comments.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				 }				 
				 
				 buil_gendata.setOnClickListener(new clicker());				
				 buil_comments.setOnClickListener(new clicker());
			}

			 
			if(this.app==451 || this.app==452 || this.app==453 || this.app==454 || this.app==455 || this.app==456 || 
					this.app==457 	|| this.app==458 || this.app==459 || this.app==460)
	 		 {
				 
				 layoutInflater.inflate(R.layout.submenu, this, true);
				 RelativeLayout relgen = (RelativeLayout) findViewById(R.id.rcv_submenu);
				 relgen.setVisibility(visibility);				 
				 
				 LinearLayout lincompleted = (LinearLayout) findViewById(R.id.completedlayout);
				 lincompleted.setVisibility(cf.v1.GONE);
				 
				 buil_location = (Button) findViewById(R.id.bilocation);
				 buil_observation = (Button) findViewById(R.id.biobserv);				 
				 buil_iso = (Button) findViewById(R.id.biiso);
				 buil_foundation = (Button) findViewById(R.id.bifoundation);
				 buil_wallstructure = (Button) findViewById(R.id.biwallstructure);
				 buil_wallcladding = (Button) findViewById(R.id.biwallcladding);
				 buil_seperation = (Button) findViewById(R.id.biseperation);
				 
				 buil_location.setOnClickListener(new clicker());
				 buil_observation.setOnClickListener(new clicker());
				 buil_iso.setOnClickListener(new clicker());
				 buil_foundation.setOnClickListener(new clicker());
				 buil_wallstructure.setOnClickListener(new clicker());
				 buil_wallcladding.setOnClickListener(new clicker());
				 buil_seperation.setOnClickListener(new clicker());
				 
				 if(!cf.Loc_val.equals("") || cf.Loc_NA.equals("1"))
				 {
					 buil_location.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				 }				 
				 if((!cf.Observ1.equals("") && !cf.Observ2.equals("") && !cf.Observ3.equals("") && !cf.Observ4.equals("") && !cf.Observ5.equals("")) || cf.OBS_NA.equals("1"))
				 {
					 buil_observation.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				 }
				 if(!cf.Iso_val.equals("") || cf.ISO_NA.equals("1"))
				 {
					 buil_iso.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				 }
				 if(!cf.Foundation_val.equals("") || cf.Foun_NA.equals("1"))
				 {
					 buil_foundation.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				 }
				 if(!cf.Wallstru_val.equals("") || cf.WS_NA.equals("1"))
				 {
					 buil_wallstructure.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				 }
				 if(!cf.Wallclad_val.equals("") || cf.WC_NA.equals("1"))
				 {
					 buil_wallcladding.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				 }
				 if(!cf.Buildsep_val.equals("") || cf.BISEP_NA.equals("1"))
				 {
					 buil_seperation.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				 }
			 }
			if(this.app==56 || this.app==57 || this.app==58 || this.app==59 || this.app==60)
			{
				
				 layoutInflater.inflate(R.layout.submenu, this, true);
				 RelativeLayout relgen = (RelativeLayout) findViewById(R.id.policy_submenu);
				 relgen.setVisibility(visibility);				 
				 
				 LinearLayout lincompleted = (LinearLayout) findViewById(R.id.completedlayout);
				 lincompleted.setVisibility(cf.v1.GONE);
				 
				 gen_btnph = (Button) findViewById(R.id.phinformation);
				 gen_btncall = (Button) findViewById(R.id.call);
				 gen_btnagnt = (Button) findViewById(R.id.agent);
				 gen_btnschedule = (Button) findViewById(R.id.scheduleinspection);
				 gen_btnddi = (Button) findViewById(R.id.additional);
				 
				 gen_btnschedule.setOnClickListener(new clicker());
				 gen_btnph.setOnClickListener(new clicker());
				 gen_btncall.setOnClickListener(new clicker());
				 gen_btnagnt.setOnClickListener(new clicker());
				 gen_btnddi.setOnClickListener(new clicker());
			 } 
			
			
			 if(this.app==11 || this.app==12 || this.app==13 || this.app==14 || this.app==15 || this.app==16 || this.app==17 || this.app==19) /** SUBMENU - GENERAL **/
			 {
				 
				 layoutInflater.inflate(R.layout.submenu, this, true);
				 RelativeLayout relgen = (RelativeLayout) findViewById(R.id.generalInfo_submenu);
				 relgen.setVisibility(visibility);
				 //relgen.setVisibility(cf.v1.GONE);
				// gen_btnschedule = (Button) findViewById(R.id.scheduleinspection);
				 bntmain_ph = (Button) findViewById(R.id.phmain);
				// gen_btncall = (Button) findViewById(R.id.call);
				// gen_btnagnt = (Button) findViewById(R.id.agent);
				// gen_btnddi = (Button) findViewById(R.id.additional);
				 gen_btnbuild = (Button) findViewById(R.id.build);
				 buil_rcv= (Button) findViewById(R.id.bircv);
				 gen_btnweather = (Button) findViewById(R.id.weather);
				 cf.Check_BI();
				 
				 cf.getPolicyholderInformation(cf.selectedhomeid);
				 if((!cf.PH_Address1.equals("")) && (!cf.PH_Email.equals("") || cf.PH_EmailChkbx.equals("1")) && !cf.PH_FirstName.equals(""))
				 {
					 bntmain_ph.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				 }
				
				 
				 
				 if((!cf.Gendata_val.equals("")) && (!cf.Loc_val.equals("") || cf.Loc_NA.equals("1")) && ((!cf.Observ1.equals("") && !cf.Observ2.equals("") && !cf.Observ3.equals("") && !cf.Observ4.equals("") && !cf.Observ5.equals("")) || cf.OBS_NA.equals("1"))
						 && (!cf.Iso_val.equals("") || cf.ISO_NA.equals("1")) && (!cf.Foundation_val.equals("") || cf.Foun_NA.equals("1")) && (!cf.Wallstru_val.equals("") || cf.WS_NA.equals("1"))
						 && (!cf.Wallclad_val.equals("") || cf.WC_NA.equals("1")) && (!cf.Buildsep_val.equals("") || cf.BISEP_NA.equals("1")))
				 {
					 gen_btnbuild.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				 }
				 
				 
				 if((!cf.Loc_val.equals("") || cf.Loc_NA.equals("1")) && 
						 ((!cf.Observ1.equals("") && !cf.Observ2.equals("") && !cf.Observ3.equals("") && !cf.Observ4.equals("") && !cf.Observ5.equals("")) || cf.OBS_NA.equals("1")) 
						 && (!cf.Iso_val.equals("") || cf.ISO_NA.equals("1"))
						 && (!cf.Foundation_val.equals("") || cf.Foun_NA.equals("1"))
						 && (!cf.Wallstru_val.equals("") || cf.WS_NA.equals("1"))
						 && (!cf.Wallclad_val.equals("") || cf.WC_NA.equals("1"))
						 && (!cf.Buildsep_val.equals("") || cf.BISEP_NA.equals("1")))
				 {
					 buil_rcv.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				 }
				 
				 cf.CreateARRTable(8);
				 Cursor c= cf.SelectTablefunction(cf.WeatherCondition, " Where Wea_srid='"+cf.selectedhomeid+"'");
				 if(c.getCount()>0)
				 {
					 gen_btnweather.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				 }

				// gen_btnschedule.setOnClickListener(new clicker());
				 bntmain_ph.setOnClickListener(new clicker());
				// gen_btncall.setOnClickListener(new clicker());
				// gen_btnagnt.setOnClickListener(new clicker());
				// gen_btnddi.setOnClickListener(new clicker());
				 gen_btnbuild.setOnClickListener(new clicker());
				 gen_btnweather.setOnClickListener(new clicker());
				 buil_rcv.setOnClickListener(new clicker());
				 //gen_btncloud.setOnClickListener(new clicker());
				
			 }
			 
			 
			 
			 else if(this.app==21 || this.app==22 || this.app==23 || this.app==24 || this.app==25 || this.app==26)/** SUBMENU - FOURPOINT **/
			 {
				 layoutInflater.inflate(R.layout.submenu, this, true);
				 RelativeLayout relgen = (RelativeLayout) findViewById(R.id.generalInfo_submenu);
				 relgen.setVisibility(v1.GONE);
				 relfour = (RelativeLayout) findViewById(R.id.fourInfo_submenu);
				 relfour.setVisibility(visibility);
				 four_btnroof = (Button) findViewById(R.id.roof);
				 //four_btnattic = (Button) findViewById(R.id.attic);
				 four_btnplumb = (Button) findViewById(R.id.plumb);
				 four_btnelectrical = (Button) findViewById(R.id.electrical);
				 four_btnhvac = (Button) findViewById(R.id.hvac);
				 four_addendum= (Button) findViewById(R.id.addendum);
				 
				 /***For hide the attic if the External only selected menas **/
				/* boolean chk=cf.checkAttic();
				 if(chk==true){
					 four_btnattic.setVisibility(View.GONE);
				 }*/
				 /***For  hide the attic if the External only selected menas ends**/
				 int chkrf = cf.RoofValidation(1);
		    	 if(chkrf==2)
				 {
		    		 four_btnroof.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				 }
				 cf.CreateARRTable(22);
				 /*boolean chkattic = cf.FP_AtticValidation();
				 if(chkattic)
				 {
					four_btnattic.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				 }*/
				 boolean chkplumbing = cf.FP_PlumbingValidation();
				 if(chkplumbing)
				 {
					 four_btnplumb.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				 }
				 boolean chkgen = cf.FP_GeneralValidation();
				 boolean chkmpanel = cf.FP_MPanelValidation();
				 boolean chkspanel = cf.FP_SPanelValidation();
				 boolean chkpanel = cf.FP_PanelValidation();
				 boolean chkwir = cf.FP_WirValidation();
				 boolean chkrecp = cf.FP_RecpValidation();
				 boolean chkvbc = cf.FP_VBCValidation();
				 boolean chklfr = cf.FP_LFRValidation();
				 boolean chkgo = cf.FP_GoValidation();
				 boolean chkoes = cf.FP_OESValidation();
				 boolean chkcom = cf.FP_ComValidation();

				 cf.Addendumcomments(cf.selectedhomeid,1);
				 if(!cf.AddendumComm.equals(""))
				 {
					 four_addendum.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				 }
				 
				 
				 if(chkgen && chkmpanel && chkspanel && chkpanel && chkwir && chkrecp && chkvbc &&
						 chklfr && chkgo && chkoes && chkcom)
				 {
					 four_btnelectrical.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				 }
				 
				 boolean chkhvac = cf.FP_HVACValidation();
				 boolean chkhvaca = cf.FP_HVACAnValidation();
				 boolean chkhvacunt = cf.FP_HVACUnValidation();
				 if(chkhvac)
				 {
					 if(chkhvaca)
					 {
						 if(chkhvacunt)
						 {
							 four_btnhvac.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
						 }
					 }
					 else
					 {
						 four_btnhvac.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
					 }
				 }
				 
				 four_btnroof.setOnClickListener(new clicker());
				// four_btnattic.setOnClickListener(new clicker());
				 four_btnhvac.setOnClickListener(new clicker());
				 four_btnplumb.setOnClickListener(new clicker());
				 four_btnelectrical.setOnClickListener(new clicker());
				 four_addendum.setOnClickListener(new clicker());

			 }
			 else if(this.app==41 || this.app==42 || this.app==43 || this.app==44 || this.app==45 || this.app==46 || this.app==47 || this.app==48 || this.app==49 || this.app==416 || this.app==417 || this.app==418 || this.app==35)/** SUBMENU - GCH **/
			 {
				 layoutInflater.inflate(R.layout.submenu, this, true);
				 RelativeLayout relgch = (RelativeLayout) findViewById(R.id.gch_submenu);
				 relgch.setVisibility(visibility);
				  gch_btnpp = (Button) findViewById(R.id.poolpresent);
				  gch_btnpf = (Button) findViewById(R.id.poolfence);
				  gch_btnroof = (Button) findViewById(R.id.gchroof);
				  gch_btnsum = (Button) findViewById(R.id.sumary);
				  gch_btnfire = (Button) findViewById(R.id.fire);
				  gch_btnhaz = (Button) findViewById(R.id.hazards);
				  gch_btnhvac = (Button) findViewById(R.id.gchhvac);
				  gch_btnrestsupp = (Button) findViewById(R.id.gchrestsupp);				  
				  gch_btnelectric = (Button) findViewById(R.id.gchelectric);
				  gch_btnhoodcut = (Button) findViewById(R.id.hoodduct);
				  gch_btnfireprot = (Button) findViewById(R.id.fireprot);
				  gch_btnmisc = (Button) findViewById(R.id.misc);
				  gch_btncomments = (Button) findViewById(R.id.comments);
				  
				  boolean chkpp = cf.GCH_PPValidation();
				  if(chkpp)
				  {
					  gch_btnpp.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				  }
				  boolean chkpf = cf.GCH_PfValidation();
				  if(chkpf)
				  {
					  gch_btnpf.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				  }
				  int chkrf = cf.RoofValidation(7);
			      if(chkrf==2)
				  {
			    	  gch_btnroof.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				  }
			      boolean chksh = cf.GCH_SHValidation();
				  if(chksh)
				  {
					  gch_btnsum.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				  }
				  boolean chkfp = cf.GCH_FPValidation();
				  if(chkfp)
				  {
					  gch_btnfire.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				  }
				  boolean chksph = cf.GCH_SPHValidation();
				  if(chksph)
				  {
					  gch_btnhaz.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				  }
				  boolean chkhv = cf.GCH_HVValidation();
				  if(chkhv)
				  {
					  gch_btnhvac.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				  }
				  boolean chkele = cf.GCH_ELEValidation();
				  if(chkele)
				  {
					  gch_btnelectric.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				  }
				  boolean chkhd = cf.GCH_HDValidation();
				  if(chkhd)
				  {
					  gch_btnhoodcut.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				  }
				  boolean chkfpt = cf.GCH_FPTValidation();
				  if(chkfpt)
				  {
					  gch_btnfireprot.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				  }
				  boolean chkms = cf.GCH_MSValidation();
				  if(chkms)
				  {
					  gch_btnmisc.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				  }
				  boolean chkrestsupp = cf.GCH_RestValidation();
				  if(chkrestsupp)
				  {
					  gch_btnrestsupp.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				  }
				  boolean chkcom = cf.GCH_COMValidation();
				  if(chkcom)
				  {
					  gch_btncomments.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				  }
				  gch_btnpp.setOnClickListener(new clicker());
				  gch_btnpf.setOnClickListener(new clicker());
				  gch_btnroof.setOnClickListener(new clicker());
				  gch_btnsum.setOnClickListener(new clicker());
				  gch_btnfire.setOnClickListener(new clicker());
				  gch_btnhaz.setOnClickListener(new clicker());
				  gch_btnhvac.setOnClickListener(new clicker());
				  gch_btnelectric.setOnClickListener(new clicker());
				  gch_btnhoodcut.setOnClickListener(new clicker());
				  gch_btnfireprot.setOnClickListener(new clicker());
				  gch_btnmisc.setOnClickListener(new clicker());
				  gch_btncomments.setOnClickListener(new clicker());
				  gch_btnrestsupp.setOnClickListener(new clicker());
				 
			 }
			 else if(this.app==51 || this.app==52 || this.app==53 || this.app==54 || this.app==55)/** SUBMENU - SINKHOLE **/
			 {
				 layoutInflater.inflate(R.layout.submenu, this, true);
				 RelativeLayout relsink = (RelativeLayout) findViewById(R.id.sinkhole_submenu);
				 relsink.setVisibility(visibility);
				  sink_btnsummary = (Button) findViewById(R.id.summary);
				  sink_btnobs1 = (Button) findViewById(R.id.observation1);
				  sink_btnobs2 = (Button) findViewById(R.id.observation2);
				  sink_btnobs3 = (Button) findViewById(R.id.observation3);
				  sink_btnobs4 = (Button) findViewById(R.id.observation4);
				  
				  boolean chkss = cf.SH_SummaryValidation();
				  if(chkss)
				  {
					  sink_btnsummary.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				  }
				  boolean chkobs1 = cf.SH_OBS1Validation();
				  if(chkobs1)
				  {
					  if(cf.trval.equals("Yes"))
					  {
						  boolean chkobs1T = cf.SH_OBS1TValidation();
						  if(chkobs1T)
						  {
							  sink_btnobs1.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
						  }
					  }
					  else
					  {
						  sink_btnobs1.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
					  }
					  
				  }
				  boolean chkobs2 = cf.SH_OBS2Validation();
				  if(chkobs2)
				  {
					  sink_btnobs2.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				  }
				  boolean chkobs3 = cf.SH_OBS3Validation();
				  if(chkobs3)
				  {
					  sink_btnobs3.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				  }
				  boolean chkobs4 = cf.SH_OBS4Validation();
				  if(chkobs4)
				  {
					  sink_btnobs4.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				  }
				  sink_btnsummary.setOnClickListener(new clicker());
				  sink_btnobs1.setOnClickListener(new clicker());
				  sink_btnobs2.setOnClickListener(new clicker());
				  sink_btnobs3.setOnClickListener(new clicker());
				  sink_btnobs4.setOnClickListener(new clicker());
				 
			 }
			 else if(this.app==61 || this.app==62 || this.app==63 || this.app==64 || this.app==65 || this.app==66)/** SUBMENU - CHINESE DRYWALL **/
			 {
				 layoutInflater.inflate(R.layout.submenu, this, true);
				 RelativeLayout relcd = (RelativeLayout) findViewById(R.id.cd_submenu);
				 relcd.setVisibility(visibility);
				  cd_btnsummary = (Button) findViewById(R.id.sumcon);
				  cd_btnvar = (Button) findViewById(R.id.var);
				  cd_btnihsys = (Button) findViewById(R.id.ihsys);
				  cd_btniasys = (Button) findViewById(R.id.iasys);
				  cd_btnattc = (Button) findViewById(R.id.attc);
				  cd_btncomm = (Button) findViewById(R.id.drycomm);
				  
				  boolean chksum = cf.CD_sumValidation();
				  if(chksum)
				  {
					  cd_btnsummary.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				  }
				  boolean chkroom = cf.CD_roomValidation();
				  if(chkroom)
				  {
					  cd_btnvar.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				  }
				  boolean chkhvac = cf.CD_hvacValidation();
				  if(chkhvac)
				  {
					  cd_btnihsys.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				  }
				  boolean chkapp = cf.CD_appValidation();
				  if(chkapp)
				  {
					  cd_btniasys.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				  }
				  boolean chkattc = cf.CD_attcValidation();
				  if(chkattc)
				  {
					  cd_btnattc.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				  }
				  boolean chksum1 = cf.CD_sumValidation();
				  if(chksum1)
				  {
					  if(!cf.dcom.equals(""))
					  {
						  cd_btncomm.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
					  }
					  
				  }
				  cd_btnsummary.setOnClickListener(new clicker());
				  cd_btnvar.setOnClickListener(new clicker());
				  cd_btnihsys.setOnClickListener(new clicker());
				  cd_btniasys.setOnClickListener(new clicker());
				  cd_btnattc.setOnClickListener(new clicker());
				  cd_btncomm.setOnClickListener(new clicker());
				 
			 }
			 else if(this.app==67 || this.app==68)/** SUBMENU - PHOTOS **/
			 {
				 layoutInflater.inflate(R.layout.photossubmenu, this, true);
				 btn_signature = (Button) findViewById(R.id.signature);
				 btn_elevation = (Button) findViewById(R.id.elevation);
				
				 btn_signature.setOnClickListener(new clicker());
				 btn_elevation.setOnClickListener(new clicker());
			 }
			 else if(this.app==300)
			 {
				layoutInflater.inflate(R.layout.commercialsub, this, true);
				 RelativeLayout relsink = (RelativeLayout) findViewById(R.id.type_submenu);
				 relsink.setVisibility(visibility);
				 b1802_chk1 = (RadioButton) findViewById(R.id.b1802_chk);
				 comm1_chk1 = (RadioButton) findViewById(R.id.comm1_chk);
				 comm2_chk1 = (RadioButton) findViewById(R.id.comm2_chk);
				 comm3_chk1= (RadioButton) findViewById(R.id.comm3_chk);
				 editbno_img= (ImageView) findViewById(R.id.editbno);
				 
				 b1802_chk1.setOnClickListener(new clicker());
				 comm1_chk1.setOnClickListener(new clicker());
				 comm2_chk1.setOnClickListener(new clicker());
				 comm3_chk1.setOnClickListener(new clicker());
				 editbno_img.setOnClickListener(new clicker());
			 }

			 else if(this.app==31)/** SUBMENU - WIND MIT && COMMERCIAL I **/
			 {
            	 layoutInflater.inflate(R.layout.mitigationsubmenu, this, true);
				 RelativeLayout rel1802 = (RelativeLayout) findViewById(R.id.windmit_submenu);
				 rel1802.setVisibility(visibility);
				 w1802_btnbcode = (Button) rel1802.findViewById(R.id.buildingcode);
				 w1802_btnrfcvr = (Button) rel1802.findViewById(R.id.roofcover);
				 w1802_btnrfdeck = (Button) rel1802.findViewById(R.id.roofdeck);
				 w1802_btnrfwall = (Button) rel1802.findViewById(R.id.roofwall);
				 w1802_btnrfgeo = (Button) rel1802.findViewById(R.id.roofgeometry);
				 w1802_btnswr = (Button) rel1802.findViewById(R.id.swr);
				 w1802_windowopen = (Button) rel1802.findViewById(R.id.windowopen);	
				 w1802_dooropen = (Button) rel1802.findViewById(R.id.dooropen);	
				 w1802_skylights = (Button) rel1802.findViewById(R.id.skylights);	
				 w1802_btnopen = (Button) rel1802.findViewById(R.id.openingprotection);
				 windaddendum= (Button) rel1802.findViewById(R.id.windaddendum);
				 
				 cf.Addendumcomments(cf.selectedhomeid,31);
				 if(!cf.AddendumComm.equals(""))
				 {
					 windaddendum.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				 }
				 
				 
				 cf.Check_B1802(cf.selectedhomeid,31,2);
				 
				 
				 w1802_btnwall = (Button) rel1802.findViewById(R.id.wallconstruction);
				 w1802_btnwall.setVisibility(cf.v1.VISIBLE);
				 if(!cf.wccomments.equals("") && cf.wallcons.getCount()>0)
				  {
					  w1802_btnwall.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
				  }
				
				 w1802_dummytxtviewcomm = (TextView) rel1802.findViewById(R.id.txt1);
				 w1802_dummytxtviewcomm.setText(String.valueOf(this.app));		
				 w1802_btncommareas = (Button) rel1802.findViewById(R.id.commareas);
				 if(this.app==31)
				 {
					 w1802_btncommareas.setVisibility(cf.v1.GONE);
				 }
				 else  if(this.app==32)
				 {
					 w1802_btncommareas.setVisibility(cf.v1.VISIBLE);					
					 w1802_btncommareas.setOnClickListener(new clicker());
				 }
				
				 Tabcolour_B1802_Comm1();
				  
				 
				 w1802_btnbcode.setOnClickListener(new clicker());
				 w1802_btnrfcvr.setOnClickListener(new clicker());
				 w1802_btnrfdeck.setOnClickListener(new clicker());
				 w1802_btnrfwall.setOnClickListener(new clicker());
				 w1802_btnrfgeo.setOnClickListener(new clicker());
				 w1802_btnswr.setOnClickListener(new clicker());
				 w1802_btnopen.setOnClickListener(new clicker());
				 w1802_windowopen.setOnClickListener(new clicker());
				 w1802_dooropen.setOnClickListener(new clicker());
				 w1802_skylights.setOnClickListener(new clicker());
				 windaddendum.setOnClickListener(new clicker());
				 w1802_btnwall.setOnClickListener(new clicker());
			 }	
			 else if(this.app==32)
             {				
				 if(innersub==143 || innersub==144)
				 {				
						 layoutInflater.inflate(R.layout.mitigationsubmenu, this, true); 
						 rcvsubmenu = (RelativeLayout) findViewById(R.id.commrcv_submenu);
						 rcvsubmenu.setVisibility(visibility);
						 
						 LinearLayout dummytxt = (LinearLayout) findViewById(R.id.notcompleted);
						 dummytxt.setVisibility(cf.v1.GONE);
						 
						 w1802_btncommunalareas = (Button) rcvsubmenu.findViewById(R.id.communalareas);
						 w1802_btnroofsys = (Button) rcvsubmenu.findViewById(R.id.roofsystem);
						 w1802_btnauxbuild = (Button) rcvsubmenu.findViewById(R.id.auxilarybuild);
						 w1802_btnauxbuild.setVisibility(cf.v1.GONE);
						 w1802_btnrestsupp = (Button) rcvsubmenu.findViewById(R.id.restsupp);
						 w1802_btnrestsupp.setVisibility(cf.v1.GONE);
						 
						 w1802_dummytxtviewcomm = (TextView) rcvsubmenu.findViewById(R.id.txt3);
						 w1802_dummytxtviewcomm.setText(String.valueOf(this.app));
						 
						 boolean chkcomm = cf.Check_Comm1(cf.selectedhomeid,32,2);
						 if(chkcomm==true)
						 {
							 w1802_btncommunalareas.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));	
						 }
						 
						 int chkrf = cf.RoofValidation(4);
						 if(chkrf==2)
						 { 
							 w1802_btnroofsys.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));	
						 }
						 w1802_btncommunalareas.setOnClickListener(new clicker());
						 w1802_btnroofsys.setOnClickListener(new clicker());								 
				 }
				 else
				 {
						 layoutInflater.inflate(R.layout.mitigationsubmenu, this, true);
						 RelativeLayout rel1com1 = (RelativeLayout) findViewById(R.id.com1_submenu);
						 rel1com1.setVisibility(visibility);
						 
						 w1802_btnbcode = (Button) rel1com1.findViewById(R.id.buildingcode);
						 w1802_btnrfcvr = (Button) rel1com1.findViewById(R.id.roofcover);
						 w1802_btnrfdeck = (Button) rel1com1.findViewById(R.id.roofdeck);
						 w1802_btnrfwall = (Button) rel1com1.findViewById(R.id.roofwall);
						 w1802_btnrfgeo = (Button) rel1com1.findViewById(R.id.roofgeometry);
						 w1802_btnswr = (Button) rel1com1.findViewById(R.id.swr);
						 w1802_windowopen = (Button) rel1com1.findViewById(R.id.windowopen);	
						 w1802_dooropen = (Button) rel1com1.findViewById(R.id.dooropen);	
						 w1802_skylights = (Button) rel1com1.findViewById(R.id.skylights);
						 w1802_btnopen = (Button) rel1com1.findViewById(R.id.openingprotection);
						 comm_rcv = (Button) rel1com1.findViewById(R.id.commercialrcv);
						 windaddendum = (Button) rel1com1.findViewById(R.id.windaddendum);
						 
						 cf.Addendumcomments(cf.selectedhomeid, 32);
						 if(!cf.AddendumComm.equals(""))
						 {
							 windaddendum.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
						 }
						 
						 
						 cf.Check_B1802(cf.selectedhomeid,32,2);
						 if(cf.onlinspectionid.equals("12")){
							 w1802_btnwall = (Button) rel1com1.findViewById(R.id.wallconstruction);
							 w1802_btnwall.setVisibility(cf.v1.VISIBLE);
							 if(!cf.wccomments.equals("") && cf.wallcons.getCount()>0)
							  {
								  w1802_btnwall.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
							  }
							 w1802_btnwall.setOnClickListener(new clicker());
						 }
						 else
						 {
							
						 }
						 w1802_dummytxtviewcomm = (TextView) rel1com1.findViewById(R.id.txt1);
						 w1802_dummytxtviewcomm.setText(String.valueOf(this.app));		


						 w1802_btnbcode.setOnClickListener(new clicker());
						 w1802_btnrfcvr.setOnClickListener(new clicker());
						 w1802_btnrfdeck.setOnClickListener(new clicker());
						 w1802_btnrfwall.setOnClickListener(new clicker());
						 w1802_btnrfgeo.setOnClickListener(new clicker());
						 w1802_btnswr.setOnClickListener(new clicker());
						 w1802_btnopen.setOnClickListener(new clicker()); 
						 w1802_windowopen.setOnClickListener(new clicker());
						 w1802_dooropen.setOnClickListener(new clicker());
						 w1802_skylights.setOnClickListener(new clicker());
						 windaddendum.setOnClickListener(new clicker());
						 comm_rcv.setOnClickListener(new clicker());
						 
						 Tabcolour_B1802_Comm1();
						 boolean chkcomm = cf.Check_Comm1(cf.selectedhomeid,32,2);
						 int chkrf = cf.RoofValidation(4);
						 if(chkcomm==true && chkrf==2)
						 {
							 comm_rcv.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
						 }						 	 
					}
             }
			 else if(this.app==33 || this.app==34)
			 {
			
				 if(innersub==123 || innersub==124 || innersub==125 || innersub==126 || innersub==127 || innersub==128)
				 {
					 layoutInflater.inflate(R.layout.commercialwindsubmenu, this, true); 
					 RelativeLayout commwind = (RelativeLayout) findViewById(R.id.commwind_submenu);
					 commwind.setVisibility(visibility);
					 
					 commwind_btnterrain = (Button) commwind.findViewById(R.id.terrain);
					 commwind_btnroofcover = (Button) commwind.findViewById(R.id.roofcoverings);
					 commwind_btnroofdeck = (Button) commwind.findViewById(R.id.commwindroofdeck);
					 commwind_btnswr = (Button) commwind.findViewById(R.id.commwindswr);
					 commwind_btnopenprot = (Button) commwind.findViewById(R.id.commwindopeningprotect);
					 commwind_btncertification = (Button) commwind.findViewById(R.id.certification);
					
					 
					 cf.Check_Comm2(cf.selectedhomeid,this.app,2);
					 if(!cf.terrainval.equals(""))
					 {
						 commwind_btnterrain.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));	
					 }
					 if(!cf.rcvalue.equals(""))
					 {
						 commwind_btnroofcover.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));	
					 }
					 if(!cf.rdvalue.equals(""))
					 {
						 commwind_btnroofdeck.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));	
					 }
					 if(!cf.swrvalue.equals(""))
					 {
						 commwind_btnswr.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));	
					 }
					 if(!cf.opvalue.equals(""))
					 {
						 commwind_btnopenprot.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));	
					 }
					 if(!cf.certificationval.equals(""))
					 {
						 commwind_btncertification.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));	
					 }
					 
					 commwind_btnterrain.setOnClickListener(new clicker());
					 commwind_btnroofcover.setOnClickListener(new clicker());
					 commwind_btnroofdeck.setOnClickListener(new clicker());
					 commwind_btnswr.setOnClickListener(new clicker());
					 commwind_btnopenprot.setOnClickListener(new clicker());
					 commwind_btncertification.setOnClickListener(new clicker());
					 
					 
				 }
				 else if(innersub==133 || innersub==134 || innersub==135 || innersub==136)
				 {
					 layoutInflater.inflate(R.layout.commercialwindsubmenu, this, true); 
					 RelativeLayout commwind = (RelativeLayout) findViewById(R.id.commwind_submenu);
					 commwind.setVisibility(visibility);
					 
					 commwind_btnterrain = (Button) commwind.findViewById(R.id.terrain);
					 commwind_btnroofcover = (Button) commwind.findViewById(R.id.roofcoverings);
					 commwind_btnroofdeck = (Button) commwind.findViewById(R.id.commwindroofdeck);
					 commwind_btnswr = (Button) commwind.findViewById(R.id.commwindswr);
					 commwind_btnopenprot = (Button) commwind.findViewById(R.id.commwindopeningprotect);
					 commwind_btncertification = (Button) commwind.findViewById(R.id.certification);
					
					 
					 cf.Check_Comm2(cf.selectedhomeid,this.app,2);
					 if(!cf.terrainval.equals(""))
					 {
						 commwind_btnterrain.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));	
					 }
					 if(!cf.rcvalue.equals(""))
					 {
						 commwind_btnroofcover.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));	
					 }
					 if(!cf.rdvalue.equals(""))
					 {
						 commwind_btnroofdeck.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));	
					 }
					 if(!cf.swrvalue.equals(""))
					 {
						 commwind_btnswr.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));	
					 }
					 if(!cf.opvalue.equals(""))
					 {
						 commwind_btnopenprot.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));	
					 }
					 if(!cf.certificationval.equals(""))
					 {
						 commwind_btncertification.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));	
					 }
					 
					 commwind_btnterrain.setOnClickListener(new clicker());
					 commwind_btnroofcover.setOnClickListener(new clicker());
					 commwind_btnroofdeck.setOnClickListener(new clicker());
					 commwind_btnswr.setOnClickListener(new clicker());
					 commwind_btnopenprot.setOnClickListener(new clicker());
					 commwind_btncertification.setOnClickListener(new clicker());
				 }
				 else if(innersub==143 || innersub==144 || innersub==145 || innersub==146)
				 {
					 	 layoutInflater.inflate(R.layout.mitigationsubmenu, this, true); 
						 rcvsubmenu = (RelativeLayout) findViewById(R.id.commrcv_submenu);
						 rcvsubmenu.setVisibility(visibility);
						 
						 LinearLayout dummytxt = (LinearLayout) findViewById(R.id.notcompleted);
						 dummytxt.setVisibility(cf.v1.GONE);
						 
						 w1802_btncommunalareas = (Button) rcvsubmenu.findViewById(R.id.communalareas);
						 w1802_btnroofsys = (Button) rcvsubmenu.findViewById(R.id.roofsystem);
						 w1802_btnauxbuild = (Button) rcvsubmenu.findViewById(R.id.auxilarybuild);
						 w1802_btnrestsupp = (Button) rcvsubmenu.findViewById(R.id.restsupp);
						 
						 w1802_dummytxtviewcomm = (TextView) rcvsubmenu.findViewById(R.id.txt3);
						 w1802_dummytxtviewcomm.setText(String.valueOf(this.app));
						 
						 cf.Check_Comm2(cf.selectedhomeid,this.app,2); 
						 if(cf.curaux.getCount()!=0 || cf.Aux_NA.equals("1"))
						 {
							 w1802_btnauxbuild.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));	
						 }
						 if(cf.curapppl.getCount()!=0 || cf.Rest_NA.equals("1"))
						 {
							 w1802_btnrestsupp.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));	
						 }
						 boolean chkcomm = cf.Check_Comm1(cf.selectedhomeid,this.app,2);
						 if(chkcomm==true)
						 {
							 w1802_btncommunalareas.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));	
						 }						 
						 
						 if(this.app==33){roofiden=5;}
						 else if(this.app==34){roofiden=6;}
						 
						 int chkrf = cf.RoofValidation(roofiden);
						 if(chkrf==2)
						 { 
							 w1802_btnroofsys.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));	
						 }
						 						 
						 w1802_btncommunalareas.setOnClickListener(new clicker());
						 w1802_btnroofsys.setOnClickListener(new clicker());
						 w1802_btnauxbuild.setOnClickListener(new clicker());
						 w1802_btnrestsupp.setOnClickListener(new clicker());							 
				 }
				 else
				 {
					 layoutInflater.inflate(R.layout.mitigationsubmenu, this, true); 
					 
					 RelativeLayout relcomm2 = (RelativeLayout) findViewById(R.id.comm_submenu);
					 relcomm2.setVisibility(visibility);					 
					 comm_wind = (Button) relcomm2.findViewById(R.id.commercialwind);
					 comm_rcv = (Button) relcomm2.findViewById(R.id.commercialrcv);
					 windaddendum = (Button) relcomm2.findViewById(R.id.windaddendum);
					 
					 RelativeLayout rcvcom2 = (RelativeLayout) findViewById(R.id.commrcv_submenu);
					 w1802_btncommunalareas = (Button) rcvcom2.findViewById(R.id.communalareas);
					 w1802_btnroofsys = (Button) rcvcom2.findViewById(R.id.roofsystem);
					 w1802_btnauxbuild = (Button) rcvcom2.findViewById(R.id.auxilarybuild);
					 w1802_btnrestsupp = (Button) rcvcom2.findViewById(R.id.restsupp);			 
					 
					 cf.Check_Comm2(cf.selectedhomeid,this.app,2); 
					 if(cf.curaux.getCount()!=0 || cf.Aux_NA.equals("1"))
					 {
						 w1802_btnauxbuild.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));	
					 }
					 if(cf.curapppl.getCount()!=0 || cf.Rest_NA.equals("1"))
					 {
						 w1802_btnrestsupp.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));	
					 }
					 boolean chkcomm = cf.Check_Comm1(cf.selectedhomeid,this.app,2);
					 if(chkcomm==true)
					 {
						 w1802_btncommunalareas.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));	
					 }
					 
					 
					 if(this.app==33){roofiden=5;}
					 else if(this.app==34){roofiden=6;}
					 
					 int chkrf = cf.RoofValidation(roofiden);System.out.println("chl="+chkrf);
					 if(chkrf==2)
					 { 
						 w1802_btnroofsys.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));	
					 }
					 
					 cf.Addendumcomments(cf.selectedhomeid,this.app);
					 if(this.app==33)
					 {
						 if(!cf.AddendumComm.equals(""))
						 {
							 windaddendum.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
						 }
					 }
					 if(this.app==34)
					 {
						 if(!cf.AddendumComm.equals(""))
						 {
							 windaddendum.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
						 }
					 }
					 
					 
					 if((cf.curaux.getCount()!=0 || cf.Aux_NA.equals("1")) && (cf.curapppl.getCount()!=0 || cf.Rest_NA.equals("1")) && chkcomm==true && chkrf==2)
					 {
						 comm_rcv.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));	
					 }	
					 
					 cf.Check_Comm2(cf.selectedhomeid,this.app,2);
					 cf.CommercialComents(cf.selectedhomeid,this.app,2);
					 if(!cf.terrainval.equals("") && !cf.rcvalue.equals("") && !cf.rdvalue.equals("") && !cf.swrvalue.equals("") 
							 && !cf.opvalue.equals("") && !cf.certificationval.equals(""))
					 {
						 comm_wind.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));	
					 }				
					 if(this.app==33){roofiden=5;}
					 else if(this.app==34){roofiden=6;}					 
					
					 comm_wind.setOnClickListener(new clicker());
					 comm_rcv.setOnClickListener(new clicker());
					 windaddendum.setOnClickListener(new clicker());					 
					 
					 w1802_dummytxtviewcomm = (TextView) relcomm2.findViewById(R.id.txt2);
					 w1802_dummytxtviewcomm.setText(String.valueOf(this.app));
					  		
				 }

			 }


		}
		switch(this.app)
		{
		   case 1:/** GENERAL INFORMATION TAB TO BE SELECTED**/
			   btn_general.setBackgroundResource(R.drawable.generalb);
			   break;
		   case 2:/** FOUR POINT TAB TO BE SELECTED**/
			   cf.loadinsp_q=cf.getResourcesvalue(R.string.four_1);
			   cf.loadinsp_n=cf.All_insp_list[1];
			   cf.loadinsp_t=cf.onlinspectionid;
			   cf.isaccess= cf.chk_InspTypeQuery(cf.selectedhomeid,"1");
			   btn_four.setBackgroundResource(R.drawable.fourpointa);
			 break; 
		   case 3:/** WIND MIT TAB TO BE SELECTED**/
			   cf.isaccess= cf.chk_InspTypeQuery(cf.selectedhomeid,"3");
			   btn_wind.setBackgroundResource(R.drawable.windmita);
			  break;
		   case 4:/** GCH TAB TO BE SELECTED**/
			   cf.isaccess= cf.chk_InspTypeQuery(cf.selectedhomeid,"7");
			   btn_gch.setBackgroundResource(R.drawable.gcha);
			   
			   break;
		   case 5:/** SINKHOLE TAB TO BE SELECTED**/
			   cf.loadinsp_q=cf.getResourcesvalue(R.string.sink_1);
			   cf.loadinsp_n=cf.All_insp_list[8];
			   cf.loadinsp_t=cf.onlinspectionid;
			   cf.isaccess= cf.chk_InspTypeQuery(cf.selectedhomeid,"8");
			   btn_sinkhole.setBackgroundResource(R.drawable.sinkholea);
			   
			   break;
		   case 6:/** CHINESE DRYWALL TAB TO BE SELECTED**/
			   cf.loadinsp_q=cf.getResourcesvalue(R.string.cd_1);
			   cf.loadinsp_n=cf.All_insp_list[9];
			   cf.loadinsp_t=cf.onlinspectionid;
			   cf.isaccess= cf.chk_InspTypeQuery(cf.selectedhomeid,"9");
			   btn_chinese.setBackgroundResource(R.drawable.chinesedwa);
			   
			   break;
		   case 7:/** PHOTOS TAB TO BE SELECTED**/
			   btn_photos.setBackgroundResource(R.drawable.photosa);
			   break;
		   case 8:/** FEEDBACK TAB TO BE SELECTED**/
			   btn_feedback.setBackgroundResource(R.drawable.feedbacka);
			   break;
		   case 9:/** MAP TAB TO BE SELECTED**/
			   btn_map.setBackgroundResource(R.drawable.mapa);
			   break;
		   case 10:/** SUBMIT TAB TO BE SELECTED**/
			   btn_submit.setBackgroundResource(R.drawable.submita);
			   break;
		   case 11:/** GENERAL - SCHEDULE INFORMATION TAB TO BE SELECTED**/
			   gen_btnschedule.setBackgroundResource(R.drawable.subbackrepeatovr);			  
			   break;
		   case 12:/** GENERAL - PH INFORMATION TAB TO BE SELECTED**/
			   bntmain_ph.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 13:/** GENERAL - CALL ATTEMPT TAB TO BE SELECTED**/
			   gen_btncall.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		 
		   case 15:/** GENERAL - ADDITIONAL TAB TO BE SELECTED**/
			   gen_btnddi.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 16:/** GENERAL - BUILDING INFORMATION TAB TO BE SELECTED**/
			   buil_rcv.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 18:/** ROOF SURVEY TAB TO BE SELECTED**/
			   btn_rfsurvey.setBackgroundResource(R.drawable.roofa);
			   break;
		   case 17:/** GENERAL - WEATHER TAB TO BE SELECTED**/
			   gen_btnweather.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 19:/** GENERAL - CLOUD INFO TAB TO BE SELECTED**/
			  // gen_btncloud.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;  
		   case 20:/** SIGNATURE TAB TO BE SELECTED**/
			   btn_signature.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;  
		   case 21:/** FOURPOINT - ROOF INFORMATION TAB TO BE SELECTED**/
			   cf.loadinsp_q=cf.getResourcesvalue(R.string.four_1);
			   cf.loadinsp_n=cf.All_insp_list[1];
			   cf.loadinsp_t=cf.onlinspectionid;
			   four_btnroof.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 22:/** FOURPOINT - ATTIC INFORMATION TAB TO BE SELECTED**/
			   cf.loadinsp_q=cf.getResourcesvalue(R.string.four_2);
			   cf.loadinsp_n=cf.All_insp_list[1];
			   cf.loadinsp_t=cf.onlinspectionid;
			   four_btnattic.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 23:/** FOURPOINT - PLUMBING INFORMATION TAB TO BE SELECTED**/
			   cf.loadinsp_q=cf.getResourcesvalue(R.string.four_3);
			   cf.loadinsp_n=cf.All_insp_list[1];
			   cf.loadinsp_t=cf.onlinspectionid;
			   four_btnplumb.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 24:/** FOURPOINT - ELECTRICAL INFORMATION TAB TO BE SELECTED**/
			   cf.loadinsp_q=cf.getResourcesvalue(R.string.four_4);
			   cf.loadinsp_n=cf.All_insp_list[1];
			   cf.loadinsp_t=cf.onlinspectionid;
			   four_btnelectrical.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 25:/** FOURPOINT - HVAC INFORMATION TAB TO BE SELECTED**/
			   cf.loadinsp_q=cf.getResourcesvalue(R.string.four_5);
			   cf.loadinsp_n=cf.All_insp_list[1];
			   cf.loadinsp_t=cf.onlinspectionid;
			   four_btnhvac.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 29:/** ROOF SURVEY INFORMATION TAB TO BE SELECTED**/
			   btn_rfsurvey.setBackgroundResource(R.drawable.roofa);
			   break;
		 
		   case 35:/** GCH - REST SUPP TAB TO BE SELECTED**/
			   gch_btnrestsupp.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		  
		   case 41:/** GCH - SWIMMING POOL PRESENT TAB TO BE SELECTED**/
			   gch_btnpp.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 42:/** GCH - POOL FENCE TAB TO BE SELECTED**/
			   gch_btnpf.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 43:/** GCH - ROOF TAB TO BE SELECTED**/
			   gch_btnroof.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 44:/** GCH - SUMMARY OF HAZARDS TAB TO BE SELECTED**/
			   cf.loadinsp_q=cf.getResourcesvalue(R.string.gch_4);
			   cf.loadinsp_n=cf.All_insp_list[7];
			   cf.loadinsp_t=cf.onlinspectionid;
			   gch_btnsum.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 45:/** GCH - FIRE TAB TO BE SELECTED**/
			   gch_btnfire.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 46:/** GCH - SPECIAL HAZARDS TAB TO BE SELECTED**/
			   gch_btnhaz.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 47:/** GCH - HVAC TAB TO BE SELECTED**/
			   cf.loadinsp_q=cf.getResourcesvalue(R.string.gch_7);
			   cf.loadinsp_n=cf.All_insp_list[7];
			   cf.loadinsp_t=cf.onlinspectionid;
			   gch_btnhvac.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 48:/** GCH - ELECTRIC TAB TO BE SELECTED**/
			   cf.loadinsp_q=cf.getResourcesvalue(R.string.gch_8);
			   cf.loadinsp_n=cf.All_insp_list[7];
			   cf.loadinsp_t=cf.onlinspectionid;
			   gch_btnelectric.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 49:/** GCH - COMMENTS TAB TO BE SELECTED**/
			   cf.loadinsp_q=cf.getResourcesvalue(R.string.gch_9);
			   cf.loadinsp_n=cf.All_insp_list[7];
			   cf.loadinsp_t=cf.onlinspectionid;
			   gch_btncomments.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 51:/** SINKHOLE - SUMMARY  TAB TO BE SELECTED**/
			   cf.loadinsp_q=cf.getResourcesvalue(R.string.sink_1);
			   cf.loadinsp_n=cf.All_insp_list[8];
			   cf.loadinsp_t=cf.onlinspectionid;
			   sink_btnsummary.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 52:/** SINKHOLE - OBSERVATION1  TAB TO BE SELECTED**/
			   sink_btnobs1.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 53:/** SINKHOLE - OBSERVATION2  TAB TO BE SELECTED**/
			   sink_btnobs2.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 54:/** SINKHOLE - OBSERVATION3  TAB TO BE SELECTED**/
			   sink_btnobs3.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 55:/** SINKHOLE - OBSERVATION4  TAB TO BE SELECTED**/
			   sink_btnobs4.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		  
		   case 61:/** SUMMARY CONDITIONS - CHINESE DRYWALL TAB TO BE SELECTED**/
			   cf.loadinsp_q=cf.getResourcesvalue(R.string.cd_1);
			   cf.loadinsp_n=cf.All_insp_list[9];
			   cf.loadinsp_t=cf.onlinspectionid;
			   cd_btnsummary.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 62:/** ROOM - CHINESE DRYWALL TAB TO BE SELECTED**/
			   cf.loadinsp_q=cf.getResourcesvalue(R.string.cd_2);
			   cf.loadinsp_n=cf.All_insp_list[9];
			   cf.loadinsp_t=cf.onlinspectionid;
			   cd_btnvar.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 63:/** HVAC SYSTEMS - CHINESE DRYWALL TAB TO BE SELECTED**/
			   cf.loadinsp_q=cf.getResourcesvalue(R.string.cd_3);
			   cf.loadinsp_n=cf.All_insp_list[9];
			   cf.loadinsp_t=cf.onlinspectionid;
			   cd_btnihsys.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 64:/** APPLIANCE SYSTEM - CHINESE DRYWALL TAB TO BE SELECTED**/
			   cf.loadinsp_q=cf.getResourcesvalue(R.string.cd_4);
			   cf.loadinsp_n=cf.All_insp_list[9];
			   cf.loadinsp_t=cf.onlinspectionid;
			   cd_btniasys.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 65:/** ATTIC - CHINESE DRYWALL TAB TO BE SELECTED**/
			   cf.loadinsp_q=cf.getResourcesvalue(R.string.cd_5);
			   cf.loadinsp_n=cf.All_insp_list[9];
			   cf.loadinsp_t=cf.onlinspectionid;
			   cd_btnattc.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 66:/** OVERALL DRYWALL COMMENTS - CHINESE DRYWALL TAB TO BE SELECTED**/
			   cf.loadinsp_q=cf.getResourcesvalue(R.string.cd_6);
			   cf.loadinsp_n=cf.All_insp_list[9];
			   cf.loadinsp_t=cf.onlinspectionid;
			   cd_btncomm.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 67:/** PHOTOS - SIGNATURE TO BE SELECTED**/
			   btn_signature.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 68:/** PHOTOS - ELEVATION TAB TO BE SELECTED**/
			   btn_elevation.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 69:/** CUSTOM INSPECTION TAB TO BE SELECTED**/
			   btn_custom.setBackgroundResource(R.drawable.customwa);
			   break;
		   case 123:
		    	commwind_btnterrain.setBackgroundResource(R.drawable.subbackrepeatovr);
		    	break;
		    case 124:
		    	commwind_btnroofcover.setBackgroundResource(R.drawable.subbackrepeatovr);
		    	break;
		    case 125:
		    	commwind_btnroofdeck.setBackgroundResource(R.drawable.subbackrepeatovr);
		    	break;
		    case 126:
		    	commwind_btnswr.setBackgroundResource(R.drawable.subbackrepeatovr);
		    	break;
		    case 127:
		    	commwind_btnopenprot.setBackgroundResource(R.drawable.subbackrepeatovr);
		    	break;
		    case 128:
		    	commwind_btncertification.setBackgroundResource(R.drawable.subbackrepeatovr);
		    	break;
		   case 416:/** GCH - HOOD DUCT AND WORK TAB TO BE SELECTED**/
			   gch_btnhoodcut.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 417:/** GCH - FIRE PROTECTION TAB TO BE SELECTED**/
			   gch_btnfireprot.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 418:/** GCH - MISCELLANEOUS TAB TO BE SELECTED**/
			   gch_btnmisc.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 450:/** GENERAL DATA TAB TO BE SELECTED**/
			   buil_gendata.setBackgroundResource(R.drawable.subbackrepeatovr);
				   break;
		   case 451:/** BI - LOCATION TAB TO BE SELECTED**/
			   	  //buil_rcv.setBackgroundResource(R.drawable.subbackrepeatovr);
				  buil_location.setBackgroundResource(R.drawable.subbackrepeatovr);
				   break;
		   case 452:/** BI - OBSERVATIONS TAB TO BE SELECTED**/
			   buil_observation.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 453:/** BI - ISO TAB TO BE SELECTED**/
			   buil_iso.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 454:/** BI - FOUNDATION TAB TO BE SELECTED**/
			   buil_foundation.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 455:/** BI - WALL STRUCTURE TAB TO BE SELECTED**/
				  buil_wallstructure.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 456:/** BI - WALL CLADDING TAB TO BE SELECTED**/
			   buil_wallcladding.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 457:/** BI - SEPERATION TAB TO BE SELECTED**/
			   buil_seperation.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 458:/** BI - WINDOW OPENINGS TAB TO BE SELECTED**/
			  buil_windowopenings.setBackgroundResource(R.drawable.subbackrepeatovr);
			  break;
		   case 459:/** BI - DOOR OPENINGS TAB TO BE SELECTED**/
			   buil_dooropenings.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 460:/** BI - SKYLIGHTS TAB TO BE SELECTED**/
			   buil_skylights.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		   case 461:/** BI - COMMENTS TAB TO BE SELECTED**/
			   cf.loadinsp_q=cf.getResourcesvalue(R.string.gen_1);
			   cf.loadinsp_t=cf.onlinspectionid;
			   buil_comments.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
           case 314: /** ROOF SECTION - COMMERCIAL TYPE2 **/
		    	comm_wind.setBackgroundResource(R.drawable.subbackrepeatovr);
		       break;
           case 56:/** POLICY HOLDER TAB TO BE SELECTED**/
			   	  gen_btnph.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
           case 57:/** AGENT TAB TO BE SELECTED**/
        	   gen_btnagnt.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
           case 58:/** CALL ATTEMPT TAB TO BE SELECTED**/
        	   gen_btncall.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
           case 59:/**SCHEDULE TAB TO BE SELECTED**/
        	   gen_btnschedule.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
           case 60:/** AGDDITIONAL TAB TO BE SELECTED**/
        	   gen_btnddi.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
	   }
		switch(this.innersub)
		{
		
			case 301://** 1802 - BUILD CODE BE SELECTED**/
				System.out.println("inside 3013");
				w1802_btnbcode.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		    case 302://** 1802 - ROOF COVER CODE BE SELECTED**/
			   w1802_btnrfcvr.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		    case 303://** 1802 - ROOF DECK CODE BE SELECTED**/
			   w1802_btnrfdeck.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		    case 304://** 1802 - ROOF WALL CODE BE SELECTED**/
			   w1802_btnrfwall.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		    case 305://** 1802 - ROOF GEOMETRY CODE TO BE SELECTED**/
			   w1802_btnrfgeo.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		    case 306://** 1802 - SWR TO BE SELECTED**//*
			   w1802_btnswr.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		    case 307://** 1802 - OPEN PROTECTION CODE TO BE SELECTED**//*
			   w1802_btnopen.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		    case 308://** 1802 - WALL CONSTRUCTION TO BE SELECTED**//*
			   w1802_btnwall.setBackgroundResource(R.drawable.subbackrepeatovr);
			   break;
		    case 309://** 1802 - WALL CONSTRUCTION TO BE SELECTED**//*
		    	comm_rcv.setBackgroundResource(R.drawable.subbackrepeatovr);	
			   break;
		    case 310://** 1802 - WALL CONSTRUCTION TO BE SELECTED**//*
		    	comm_rcv.setBackgroundResource(R.drawable.subbackrepeatovr);
		    	break;
		    case 311://** 1802 - WALL CONSTRUCTION TO BE SELECTED**//*
		    	comm_rcv.setBackgroundResource(R.drawable.subbackrepeatovr);
		    	break;
		    case 312://** 1802 - WALL CONSTRUCTION TO BE SELECTED**//*
		    	comm_rcv.setBackgroundResource(R.drawable.subbackrepeatovr);
				   break;
		    case 313: /** ROOF SECTION - COMMERCIAL TYPE2 **/
		    	System.out.println("inside 3131313");
		    	comm_rcv.setBackgroundResource(R.drawable.subbackrepeatovr);
		    	System.out.println("comm_rcv");
		    	break;
		    case 314: /** ROOF SECTION - COMMERCIAL TYPE2 **/
		    	System.out.println("dfdfdfdfdd");
		    	comm_wind.setBackgroundResource(R.drawable.subbackrepeatovr);
		    	System.out.println("dfdcomeuinwd");
		    	break;
		    case 315: /** WINDOW OPENINGS **/
		    	w1802_windowopen.setBackgroundResource(R.drawable.subbackrepeatovr);
		    	break;
		    case 316: /** DOOR OPENINGS **/
		    	w1802_dooropen.setBackgroundResource(R.drawable.subbackrepeatovr);
		    	break;
		    case 317: /** SKYLIGHTS**/
		    	w1802_skylights.setBackgroundResource(R.drawable.subbackrepeatovr);
		    	break;
		    case 123:
		    	commwind_btnterrain.setBackgroundResource(R.drawable.subbackrepeatovr);
		    	break;
		    case 124:
		    	commwind_btnroofcover.setBackgroundResource(R.drawable.subbackrepeatovr);
		    	break;
		    case 125:
		    	commwind_btnroofdeck.setBackgroundResource(R.drawable.subbackrepeatovr);
		    	break;
		    case 126:
		    	commwind_btnswr.setBackgroundResource(R.drawable.subbackrepeatovr);
		    	break;
		    case 127:
		    	commwind_btnopenprot.setBackgroundResource(R.drawable.subbackrepeatovr);
		    	break;
		    case 128:
		    	commwind_btncertification.setBackgroundResource(R.drawable.subbackrepeatovr);
		    	break;
		    case 129:
		    	windaddendum.setBackgroundResource(R.drawable.subbackrepeatovr);
		    	break;
		    case 143:
		    	w1802_btncommunalareas.setBackgroundResource(R.drawable.subbackrepeatovr);
		    	break;
		    case 144:
		    	w1802_btnroofsys.setBackgroundResource(R.drawable.subbackrepeatovr);
		    	break;
		    case 145:
		    	w1802_btnauxbuild.setBackgroundResource(R.drawable.subbackrepeatovr);
		    	break;
		    case 146:
		    	w1802_btnrestsupp.setBackgroundResource(R.drawable.subbackrepeatovr);
		    	break;

		}

	}
	class clicker implements OnClickListener {
		public void onClick(View v)  {
			// TODO Auto-generated method stub
			switch (v.getId()) {			
			case R.id.generalinfo:			
				cf.goclass(11);
				break;
			case R.id.phmain:
				cf.goclass(12);
				break;
			case R.id.scheduleinspection:				
				cf.goclass(11);
				break;
			case R.id.phinformation:
				System.out.println("inside ph");
				cf.goclass(12);
				break;
			case R.id.call:				
				cf.goclass(13);
				break;
			case R.id.agent:				
				cf.goclass(14);
				break;
			case R.id.additional:				
				cf.goclass(15);
				break;	
			case R.id.addendum:
				cf.goclass(19);
				break;
			case R.id.windaddendum:				
				cf.gotoclass(cf.identityval, WindAddendumcomm.class);
				break;
			case R.id.build:				
			/*	if(cf.onlstatus.equals("Assign"))
				{
					cf.ShowToast("Editing information without scheduling is not allowed.", 0);
				}
				else
				{*/
					cf.getinspectioname(cf.selectedhomeid);
					if(cf.selinspname.equals("7"))
					{
						
					}
					else
					{
						try
						{
							Cursor c1 = cf.SelectTablefunction(cf.BI_General, " where fld_srid='"+cf.selectedhomeid+"'");
							
							if(c1.getCount()==0)
							{
								cf.arr_db.execSQL("INSERT INTO " + cf.BI_General + " (fld_srid,BI_LOC_NA,BI_LOC_DESC,BI_OBSERV_NA,BI_OSERV1,BI_OSERV2,BI_OSERV3,BI_OSERV4,BI_OSERV5,BI_OSERV6,BI_ISO_NA,BI_ISO,BI_FOUN_NA,BI_FOUN,BI_WS_NA,BI_WALLSTRU,BI_WALLSTRUPER,BI_WALLSTRUC_OTHER,BI_WC_NA,BI_WALLCLAD,BI_WALLCLAD_OTHER,BI_BUILDSEP_NA,BI_BUILD_SEP)"
										+ "VALUES ('"+cf.selectedhomeid+"','1','','1','','','','','','','1','','1','','1','','','','1','','','1','')");
							}
							else
							{
								c1.moveToFirst();
								
								String LOC_NA = c1.getString(c1.getColumnIndex("BI_LOC_NA"));
								String Obser_NA = c1.getString(c1.getColumnIndex("BI_OBSERV_NA"));
								String ISO_NA = c1.getString(c1.getColumnIndex("BI_ISO_NA"));
								String FOUN_NA = c1.getString(c1.getColumnIndex("BI_FOUN_NA"));
								String WS_NA = c1.getString(c1.getColumnIndex("BI_WS_NA"));
								String WC_NA = c1.getString(c1.getColumnIndex("BI_WC_NA"));
								String BUILDSEP_NA = c1.getString(c1.getColumnIndex("BI_BUILDSEP_NA"));
								
								
								if(LOC_NA.equals(""))
								{
									cf.arr_db.execSQL("UPDATE "
											+ cf.BI_General
											+ " SET BI_LOC_NA='1'"
											+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
								}
								else
								{
									cf.arr_db.execSQL("UPDATE "
											+ cf.BI_General
											+ " SET BI_LOC_NA='"+LOC_NA+"'"
											+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
								}
								
								if(Obser_NA.equals(""))
								{
									cf.arr_db.execSQL("UPDATE "
											+ cf.BI_General
											+ " SET BI_OBSERV_NA='1'"
											+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
								}
								else
								{
									cf.arr_db.execSQL("UPDATE "
											+ cf.BI_General
											+ " SET BI_OBSERV_NA='"+Obser_NA+"'"
											+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
								}
								
								if(ISO_NA.equals(""))
								{
									cf.arr_db.execSQL("UPDATE "
											+ cf.BI_General
											+ " SET BI_ISO_NA='1'"
											+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
								}
								else
								{
									cf.arr_db.execSQL("UPDATE "
											+ cf.BI_General
											+ " SET BI_ISO_NA='"+ISO_NA+"'"
											+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
								}
								
								if(FOUN_NA.equals(""))
								{
									cf.arr_db.execSQL("UPDATE "
											+ cf.BI_General
											+ " SET BI_FOUN_NA='1'"
											+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
								}
								else
								{
									cf.arr_db.execSQL("UPDATE "
											+ cf.BI_General
											+ " SET BI_FOUN_NA='"+FOUN_NA+"'"
											+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
								}
								
								if(WS_NA.equals(""))
								{
									cf.arr_db.execSQL("UPDATE "
											+ cf.BI_General
											+ " SET BI_WS_NA='1'"
											+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
								}
								else
								{
									cf.arr_db.execSQL("UPDATE "
											+ cf.BI_General
											+ " SET BI_WS_NA='"+WS_NA+"'"
											+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
								}
								
								if(WC_NA.equals(""))
								{
									cf.arr_db.execSQL("UPDATE "
											+ cf.BI_General
											+ " SET BI_WC_NA='1'"
											+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
								}
								else
								{
									cf.arr_db.execSQL("UPDATE "
											+ cf.BI_General
											+ " SET BI_WC_NA='"+WC_NA+"'"
											+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
								}
								
								if(BUILDSEP_NA.equals(""))
								{
									cf.arr_db.execSQL("UPDATE "
											+ cf.BI_General
											+ " SET BI_BUILDSEP_NA='1'"
											+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
								}
								else
								{
									cf.arr_db.execSQL("UPDATE "
											+ cf.BI_General
											+ " SET BI_BUILDSEP_NA='"+BUILDSEP_NA+"'"
											+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
								}
									
							}
						}catch (Exception e) {
							// TODO: handle exception
						}
				
					}
			    	cf.goclass(16);
				//}
				break;				
			case R.id.weather:			
				/*if(cf.onlstatus.equals("Assign"))
				{
					cf.ShowToast("Editing information without scheduling is not allowed.", 0);
				}
				else
				{*/
					cf.goclass(17);
				//}
				break;
			case R.id.four:				
				cf.isaccess= cf.chk_InspTypeQuery(cf.selectedhomeid,"13");
				if(cf.isaccess==true)
				{	/*if(cf.onlstatus.equals("Assign"))
					{
						cf.ShowToast("Editing information without scheduling is not allowed.", 0);
					}
					else
				    {*/
						
						cf.goclass(21);
						atticautosave();
				    //}
				}
				else
				{
					cf.showpopup("13");
					atticautosave();
					
				}
				break;
			case R.id.roof:				
				cf.goclass(21);
				break;
			case R.id.attic:
				/*cf.CreateARRTable(22);
				try
				{
					chkatc_save=cf.SelectTablefunction(cf.Four_Electrical_Attic, " where fld_srid='"+cf.selectedhomeid+"'");
				}
				catch (Exception e) {
					// TODO: handle exception
				}
				if(chkatc_save.getCount()==0){
				AlertDialog.Builder alt_bld = new AlertDialog.Builder(cf.con);
				alt_bld.setMessage("Are you sure, Do you want to include the  Attic Section?")
				.setCancelable(false)
				.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
				  // Action for 'Yes' Button
					str="Yes";
					insert();
					
				}
				})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
				//  Action for 'NO' Button
					str="No";
                	insert();	
				dialog.cancel();
				}
				});
				AlertDialog alert = alt_bld.create();
				alert.setTitle("Include Attic");
				alert.setIcon(R.drawable.alertmsg);
				alert.show();
				}
				else
				{*/
				
					cf.goclass(22);
				//}
				break;
			case R.id.commercialrcv:
				cf.gotoclass(Integer.parseInt(w1802_dummytxtviewcomm.getText().toString()), WindMitCommareas.class);
				break;
			case R.id.plumb:
				
				cf.goclass(23);
				break;
			case R.id.electrical:
				
				cf.goclass(24);
				break;
			case R.id.hvac:
				
				cf.goclass(25);
				break;
			case R.id.rfsurvey:
				
				cf.isaccess= cf.chk_InspTypeQuery(cf.selectedhomeid,"750");
				if(cf.isaccess==true)
				{	/*if(cf.onlstatus.equals("Assign"))
					{
						cf.ShowToast("Editing information without scheduling is not allowed.", 0);
					}
					else
				    {*/
				        cf.goclass(29);
				    //}
				}else
				{
					cf.showpopup("750");
					//cf.getsingleinspectiotextname("2");
				    //cf.ShowToast(cf.singleinspname+" inspection type was not ordered.", 0);
				}
				break;
			case R.id.wind:	
					boolean b1802access = cf.chk_InspTypeQuery(cf.selectedhomeid,"28");System.out.println("testaccess="+b1802access);
					boolean c1isaccess= cf.chk_InspTypeQuery(cf.selectedhomeid,"9");
					
					cf.getPolicyholderInformation(cf.selectedhomeid);
					if(b1802access==true || c1isaccess==true)
					{
						System.out.println("b1802"+b1802access);
						if(b1802access==true)
				         {
				        	 cf.gotoclass(31, WindMitBuildCode.class);
				         }
				         else
				         {
				        	   System.out.println("Storiestestest="+cf.Stories); 
				        	   //cf.showpopup("28");
				        	   cf.goclass(3);
				         }
					}
					else
					{
						//cf.ShowToast("Wind mit inspection type was not ordered.", 0);
						cf.goclass(3);
				}
			   
				break;
			case R.id.buildingcode:  
				
				cf.gotoclass(Integer.parseInt(w1802_dummytxtviewcomm.getText().toString()), WindMitBuildCode.class);
				break;
			case R.id.roofcover:
				
				cf.gotoclass(Integer.parseInt(w1802_dummytxtviewcomm.getText().toString()), WindMitRoofCover.class);				
				break;
			case R.id.roofdeck: 
				
				cf.gotoclass(Integer.parseInt(w1802_dummytxtviewcomm.getText().toString()), WindMitRoofDeck.class);	
				break;
			case R.id.roofwall: 
				
				cf.gotoclass(Integer.parseInt(w1802_dummytxtviewcomm.getText().toString()), WindMitRoofWall.class);	
				break;
			case R.id.roofgeometry: 
				
				cf.gotoclass(Integer.parseInt(w1802_dummytxtviewcomm.getText().toString()), WindMitRoofGeometry.class);	
				break;
			case R.id.swr: 
				
				cf.gotoclass(Integer.parseInt(w1802_dummytxtviewcomm.getText().toString()), WindMitSWR.class);	
				break;
			case R.id.windowopen:
				
				cf.gotoclass(Integer.parseInt(w1802_dummytxtviewcomm.getText().toString()), WindMitWindowopenings.class);
				break;
			case R.id.dooropen:
				
				cf.gotoclass(Integer.parseInt(w1802_dummytxtviewcomm.getText().toString()), WindMitDooropenings.class);
				break;
			case R.id.skylights:
				
				cf.gotoclass(Integer.parseInt(w1802_dummytxtviewcomm.getText().toString()), WindMitSkylights.class);
				break;
			case R.id.openingprotection: 
				
				cf.gotoclass(Integer.parseInt(w1802_dummytxtviewcomm.getText().toString()), WindMitOpenProt.class);	
				break;
			case R.id.wallconstruction:
				
				cf.gotoclass(Integer.parseInt(w1802_dummytxtviewcomm.getText().toString()), WindMitWallCons.class);	
				break;
			case R.id.commareas:
				
				cf.gotoclass(Integer.parseInt(w1802_dummytxtviewcomm.getText().toString()), WindMitCommareas.class);	
				break;
			case R.id.communalareas:	
				System.out.println("cmomm="+w1802_dummytxtviewcomm.getText().toString());
				cf.gotoclass(Integer.parseInt(w1802_dummytxtviewcomm.getText().toString()), WindMitCommareas.class);	
				break;
			case R.id.roofsystem:
				System.out.println("roofsystem");
				if(w1802_dummytxtviewcomm.getText().toString().equals("33"))
					cf.Roof=cf.getResourcesvalue(R.string.insp_5);/**we can get this from the String_value.xml*/
				else if(w1802_dummytxtviewcomm.getText().toString().equals("34"))
					cf.Roof=cf.getResourcesvalue(R.string.insp_6);/**we can get this from the String_value.xml*/
				else if(w1802_dummytxtviewcomm.getText().toString().equals("32")) 				
					cf.Roof=cf.getResourcesvalue(R.string.insp_4);/**we can get this from the String_value.xml*/
				System.out.println("dummy"+w1802_dummytxtviewcomm.getText().toString());
				if(w1802_dummytxtviewcomm.getText().toString().equals("32"))
				{
					cf.gotoclass(Integer.parseInt(w1802_dummytxtviewcomm.getText().toString()), Roof_section_common.class);
				}
				else
				{
					cf.gotoclass(Integer.parseInt(w1802_dummytxtviewcomm.getText().toString()), Roof_commercial_information.class);
				}
				break;
			case R.id.auxilarybuild:
				
				cf.gotoclass(Integer.parseInt(w1802_dummytxtviewcomm.getText().toString()), WindMitCommAuxbuildings.class);	
				break;
			case R.id.restsupp:
				
				cf.duplicaterestapp(Integer.parseInt(w1802_dummytxtviewcomm.getText().toString()));
				//cf.gotoclass(Integer.parseInt(w1802_dummytxtviewcomm.getText().toString()), WindMitCommRestsupp.class);	
				break;
			case R.id.commercialwind:
				
				cf.gotoclass(Integer.parseInt(w1802_dummytxtviewcomm.getText().toString()), CommercialWind.class);
				break;
	       case R.id.terrain:
	    	  
				cf.gotoclass(app, CommercialWind.class);
				break;
			case R.id.roofcoverings:
				
				cf.gotoclass(app, CommercialRC.class);
				break;
			case R.id.commwindroofdeck:
				
				cf.gotoclass(app, CommercialRD.class);
				break;
			case R.id.commwindswr:
				
				cf.gotoclass(app, CommercialSWR.class);
				break;
			case R.id.commwindopeningprotect:
				
				cf.gotoclass(app, CommercialOP.class);
				break;
			case R.id.certification:
				
				cf.gotoclass(app, Certification.class);
				break;
			case R.id.b1802_chk:
				System.out.println("b1802_chk");
							if(((RadioButton)findViewById(R.id.b1802_chk)).isChecked()==true)
							{
								cf.isaccess= cf.chk_InspTypeQuery(cf.selectedhomeid,"28");
								System.out.println("b1802_chk"+cf.isaccess);
								if(cf.isaccess==true)
								{
									
				                    hidesubmenu();
									if(cf.mainlayout!=null)	{cf.mainlayout.removeAllViews();}
									cf.submenu_layout = (LinearLayout) ((Activity) context).findViewById(R.id.submenu);
									cf.submenu_layout.addView(new MyOnclickListener(context, 31, 1,0,cf));
									cf.submenu_layout.removeAllViews();
									cf.gotoclass(31,WindMitBuildCode.class);
								}
								else
								{
									if(cf.submenu_layout!=null)
									{
										cf.submenu_layout.removeAllViews();
										if(cf.mainlayout!=null)	{cf.mainlayout.removeAllViews();}
										cf.save.setVisibility(cf.v1.GONE);				
									}
									cf.showpopup("28");
									//((RadioButton)findViewById(R.id.b1802_chk)).setChecked(false);
									//cf.getsingleinspectiotextname("3");
									//cf.ShowToast(cf.singleinspname+" inspection type was not ordered.", 0);
									//cf.goclass(3);
								}
								
							}
							else
							{
								cf.goclass(3);
							}

							break;
			case R.id.comm1_chk:
				
				if(((RadioButton)findViewById(R.id.comm1_chk)).isChecked()==true)
				{
					hidesubmenu();
					cf.isaccess= cf.chk_InspTypeQuery(cf.selectedhomeid,"9");
					if(cf.isaccess==true)
					{
						if(cf.mainlayout!=null)	{cf.mainlayout.removeAllViews();}
						cf.submenu_layout = (LinearLayout) ((Activity) context).findViewById(R.id.submenu);
						
						cf.submenu_layout.addView(new MyOnclickListener(context, 32, 1,0,cf));
						cf.submenu_layout.removeAllViews();
						
						cf.selecttbl(32);
						
						
						if(cf.comm1_buildno.equals(""))
						{
							cf.getcommercialbno(1,32,"Commercial Type I");	
						}
						else
						{
							cf.gotoclass(32,WindMitBuildCode.class);
						}
					}
					else
					{
						if(cf.submenu_layout!=null)
						{
							cf.submenu_layout.removeAllViews();
							cf.mainlayout.removeAllViews();	cf.save.setVisibility(cf.v1.GONE);
						}
						//cf.getsingleinspectiotextname("4");
						//cf.ShowToast(cf.singleinspname+" inspection type was not ordered.", 0);
						cf.showpopup("9");
						//cf.goclass(3);
					}
					
				}
				else
				{
					cf.goclass(3);
				}

				break;
				
			case R.id.comm2_chk:
				
				if(((RadioButton)findViewById(R.id.comm2_chk)).isChecked()==true)
				{
					hidesubmenu();
					cf.isaccess= cf.chk_InspTypeQuery(cf.selectedhomeid,"9");	
					
					if(cf.isaccess==true)
					{
						if(cf.mainlayout!=null)	{cf.mainlayout.removeAllViews();}
						cf.submenu_layout = (LinearLayout) ((Activity) context).findViewById(R.id.submenu);
						cf.submenu_layout.addView(new MyOnclickListener(context, 33, 1,0,cf));
						cf.submenu_layout.removeAllViews();
						cf.selecttbl(33);
						
						if(cf.comm1_buildno.equals(""))
						{
							cf.getcommercialbno(2,33,"Commercial Type II");	
						}	
						else
						{
							cf.gotoclass(33,CommercialWind.class);
						}
					}
					else
					{
						if(cf.submenu_layout!=null)
						{
							cf.submenu_layout.removeAllViews();
							cf.mainlayout.removeAllViews();	cf.save.setVisibility(cf.v1.GONE);
						}
						cf.showpopup("9");
						//cf.getsingleinspectiotextname("4");
						//cf.ShowToast(cf.singleinspname+" inspection type was not ordered.", 0);
						//cf.goclass(3);
					}
					
				}
				else
				{
					cf.goclass(3);
					
				}
				break;
			case R.id.comm3_chk:
				
				if(((RadioButton)findViewById(R.id.comm3_chk)).isChecked()==true)
				{
					hidesubmenu();	
					cf.isaccess= cf.chk_InspTypeQuery(cf.selectedhomeid,"9");
					if(cf.isaccess==true)
					{
						if(cf.mainlayout!=null) { cf.mainlayout.removeAllViews();}
						cf.submenu_layout = (LinearLayout) ((Activity) context).findViewById(R.id.submenu);
						cf.submenu_layout.addView(new MyOnclickListener(context, 34, 1,0,cf));
						cf.submenu_layout.removeAllViews();									
						cf.selecttbl(34);
						
						if(cf.comm1_buildno.equals(""))
						{
							cf.getcommercialbno(3,34,"Commercial Type III");	
						}
						else
						{
							cf.gotoclass(34,CommercialWind.class);
						}
					}
					else
					{
						if(cf.submenu_layout!=null)
						{
							cf.submenu_layout.removeAllViews();
							cf.mainlayout.removeAllViews();		cf.save.setVisibility(cf.v1.GONE);				
							
						}						
						cf.showpopup("9");
						//cf.getsingleinspectiotextname("4");
						//cf.ShowToast(cf.singleinspname+" inspection type was not ordered.", 0);
					}
					
				}
				else
				{	
					cf.goclass(3);
			
				}
				break;

						case R.id.editbno:
							
							try
							{
								cf.selecttbl(cf.identityval);
								
								if(((RadioButton)findViewById(R.id.comm1_chk)).isChecked()==true)
								{
									updatebuildingno(cf.comm1_buildno,1,32);
								}
								else if(((RadioButton)findViewById(R.id.comm2_chk)).isChecked()==true)
								{
									updatebuildingno(cf.comm1_buildno,2,33);
								}
								else if(((RadioButton)findViewById(R.id.comm3_chk)).isChecked()==true)
								{
									updatebuildingno(cf.comm1_buildno,3,34);
								}
							}
							catch(Exception e)
							{
								
							}						
							break;

						case R.id.bigendata:
							
							cf.goclass(450);
							break;
						case R.id.bircv:
							
							cf.goclass(451);
							break;
						case R.id.bilocation:
							
							cf.goclass(451);
							break;
						case R.id.biobserv:
							
							cf.goclass(452);
							break;
						case R.id.biiso:
							
							cf.goclass(453);
							break;
						case R.id.bifoundation:
							
							cf.goclass(454);
							break;
						case R.id.biwallstructure:
							
							cf.goclass(455);
							break;
						case R.id.biwallcladding:
							
							cf.goclass(456);
							break;
						case R.id.biseperation:
							
							cf.goclass(457);
							break;
						
						case R.id.bicomments:
							
							cf.goclass(461);
							break;

         	case R.id.gch:         		
         		cf.isaccess= cf.chk_InspTypeQuery(cf.selectedhomeid,"11");System.out.println("cf.is="+cf.isaccess);
				if(cf.isaccess==true)
				{
					/*if(cf.onlstatus.equals("Assign"))
					{
						cf.ShowToast("Editing information without scheduling is not allowed.", 0);
					}
					else
				    {*/
						gchautosave();
				        cf.goclass(41);
				    //}
				}else					
				{	
					System.out.println("cshowpopup");
					 cf.showpopup("11");
					gchautosave();
					 
					//cf.getsingleinspectiotextname("7");
					//cf.ShowToast(cf.singleinspname+" inspection type was not ordered.", 0);
				}
				break;
			case R.id.poolpresent:				
				cf.goclass(41);
				break;
			case R.id.gchroof:				
				cf.goclass(43);
			    break;
			case R.id.poolfence:				
				cf.goclass(42);
			    break;
			case R.id.sumary:				
				cf.goclass(44);
				break;
			case R.id.fire:				
				cf.goclass(45);
				break;
			case R.id.hazards:
				
				cf.goclass(46);
				break;
			case R.id.gchhvac:
				
				cf.goclass(47);
				break;
			case R.id.gchrestsupp:
				
				cf.duplicaterestapp(35);
				break;
			case R.id.gchelectric:
				
				cf.goclass(48);
				break;
			case R.id.comments:
							
				cf.goclass(49);
				break;
			case R.id.hoodduct:
				
				cf.goclass(416);
				break;
			case R.id.fireprot:
				
				cf.goclass(417);
				break;				
			case R.id.misc:
				
				cf.goclass(418);
				break;
			case R.id.sinkhole:
				
				cf.isaccess= cf.chk_InspTypeQuery(cf.selectedhomeid,"18");
				if(cf.isaccess==true)
				{	
					/*if(cf.onlstatus.equals("Assign"))
					{
						cf.ShowToast("Editing information without scheduling is not allowed.", 0);
					}
					else
				    {*/
				        cf.goclass(51);
				    //}
				}
				else
					
				{	
					cf.showpopup("18");
					//cf.getsingleinspectiotextname("8");
					//cf.ShowToast(cf.singleinspname+" inspection type was not ordered.", 0);
				}
				break;
			case R.id.summary:
				
				cf.goclass(51);
				break;
			case R.id.observation1:				
				cf.goclass(52);
				break;
			case R.id.observation2:				
				cf.goclass(53);
				break;
			case R.id.observation3:				
				cf.goclass(54);
				break;
			case R.id.observation4:				
				cf.goclass(55);
				break;
			case R.id.custom:
				   Cursor cur =cf.SelectTablefunction(cf.policyholder, " where ARR_PH_SRID='"+cf.selectedhomeid+"'");
				   if(cur.getCount()>0)
				   {  
					   cur.moveToFirst();
		               int iscustomincluded = cur.getInt(cur.getColumnIndex("iscustominspection"));
		               if(iscustomincluded==1)
		               {
		            	 	cf.goclass(56);    	   
		               }
		               else
		               {
		            	   cf.ShowToast("Custom Inspection Type was not ordered.",0);
		            	   
		               }
		               
				   }
				break;

			case R.id.chinese:
				
				cf.isaccess= cf.chk_InspTypeQuery(cf.selectedhomeid,"751");
				if(cf.isaccess==true)
				{	/*if(cf.onlstatus.equals("Assign"))
					{
						cf.ShowToast("Editing information without scheduling is not allowed.", 0);
					}
					else
				    {*/
				        cf.goclass(61);
				    //}
				}
				else				
				{
					cf.showpopup("751");
					//cf.getsingleinspectiotextname("9");
					//cf.ShowToast(cf.singleinspname+"inspection type was not ordered.", 0);
				}
				break;
			case R.id.sumcon:
				
				cf.goclass(61);
				break;
			case R.id.var:
				
				cf.goclass(62);
				break;
			case R.id.ihsys:
				
				cf.goclass(63);
				break;
			case R.id.iasys:
				
				cf.goclass(64);
				break;
			case R.id.attc:
				
				cf.goclass(65);
				break;
			case R.id.drycomm:
				
				cf.goclass(66);
				break;
			case R.id.photos:
				Cursor s=cf.SelectTablefunction(cf.Select_insp, " WHERE SI_srid='"+cf.selectedhomeid+"'");
		    	s.moveToFirst();
		    	if(s.getCount()>0)
		    	{
		    		String tmp=cf.decode(s.getString(s.getColumnIndex("SI_InspectionNames"))).trim();		    		
		    		if(tmp.length()>0)
		    		{		    			
		    			System.out.println("the quesry was "+"Where  ARR_Custom_ID in ('"+tmp+"')  order by ARR_Custom_ID");
		    			try
		    			{
			    			Cursor c =cf.SelectTablefunction(cf.allinspnamelist, " Where  ARR_Insp_ID in ("+tmp+")  order by ARR_Custom_ID");
			    			if(c.getCount()>0)
			    			{
			    				cf.goclass(6);
			    			}
		    			}
		    			catch (Exception e) {
							// TODO: handle exception
		    				System.out.println("second issues="+e.getMessage());
						}
		    		}		    		
		    	}
				
				break;			
			case R.id.signature:				
				cf.goclass(6);
				break;
			case R.id.elevation:
				
				cf.goclass(7);
				break;
			
			case R.id.feedback:
				
				/*if(cf.onlstatus.equals("Assign"))
				{
					cf.ShowToast("Editing information without scheduling is not allowed.", 0);
				}
				else
			    {*/
				    cf.goclass(8);
			    //}
				break;
			case R.id.map:
			
				cf.goclass(9);
				break;
			case R.id.submit:
				
				/*if(cf.onlstatus.equals("Assign"))
				{
					cf.ShowToast("Editing information without scheduling is not allowed.", 0);
				}
				else
			    {*/
				    cf.goclass(10);
			    //}
				break;
			case R.id.home:
				
				cf.gohome();
				
				break;
		
			}
		}

		protected void insert() {
			// TODO Auto-generated method stub
			try
			{
				cf.arr_db.execSQL("INSERT INTO "
						+ cf.Four_Electrical_Attic
						+ " (fld_srid,atcinc,atcoptions,atccomments,rfdcinc,rfdcmat,rfstmat,rfdcthi,rfdcatt,rftruss,"+
						"nailspac,missednails,rfwallinc,rtowopt,nail3,nail4,nail5,truss,truss1,gablewallinc,gablewallopt,"+
						"atccondinc,moisture,dsheath,droof,nsalt,othertxt,other)"
						+ "VALUES('"+cf.selectedhomeid+"','','','','','','','','','','','','',"+
						  "'','','','','','','','','','','','','','','')");

				cf.goclass(22);
		 	}
			catch (Exception E)
			{
				String strerrorlog="Inserting the Attic Details  - FourPoint";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
			}
		}
	}
	public void updatebuildingno(String buildingno,final int iden,final int typeid) {
		// TODO Auto-generated method stub
		final Dialog dialog1 = new Dialog(context,android.R.style.Theme_Translucent_NoTitleBar);
		dialog1.getWindow().setContentView(R.layout.alert);
		LinearLayout maintable= (LinearLayout) dialog1.findViewById(R.id.maintable);
		LinearLayout currenttable= (LinearLayout) dialog1.findViewById(R.id.Edit_number);
		maintable.setVisibility(View.GONE);
		currenttable.setVisibility(View.VISIBLE);
		Button btnsave=(Button) dialog1.findViewById(R.id.EN_save);
	    Button btncancel=(Button) dialog1.findViewById(R.id.EN_cancel);
		btncancel.setVisibility(v1.GONE);
		TextView txttitle=(TextView) dialog1.findViewById(R.id.RP_txthelp);txttitle.setText("Building Number");
		TextView txtcontent=(TextView) dialog1.findViewById(R.id.EN_txtid);txtcontent.setText("Update Building Number");
		
		InputFilter[] FilterArray = new InputFilter[1];
		FilterArray[0] = new InputFilter.LengthFilter(3);
		((EditText) dialog1.findViewById(R.id.EN_number)).setFilters(FilterArray);
		((EditText) dialog1.findViewById(R.id.EN_number)).setText(buildingno);
		
		 ImageView bt_close=(ImageView) dialog1.findViewById(R.id.EN_close);
		   bt_close.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//if(submenu_layout!=null){submenu_layout.removeAllViews();}
				dialog1.setCancelable(true);
				dialog1.dismiss();
			}
		});
		btnsave.setOnClickListener(new OnClickListener() {						
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					cf.getPolicyholderInformation(cf.selectedhomeid);
					
					String s =((EditText) dialog1.findViewById(R.id.EN_number)).getText().toString().trim();						
					if(s.equals(""))
					{
						cf.ShowToast("Please enter the Building Number.", 0);												
					}	
					else if(cf.noofbuildingvalidation(s) == false)
					{
						cf.ShowToast("Please enter the building number less than or equal to "+cf.noofbuildings, 0);
						((EditText) dialog1.findViewById(R.id.EN_number)).setText("");							
					}
					else if(cf.buildingnovalidation(s).equals("zero"))
					{
						cf.ShowToast("Invalid Entry! Please enter the valid building number.", 0);
						((EditText) dialog1.findViewById(R.id.EN_number)).setText("");							
					}					
					else if(cf.CheckDupBNo(s,cf.CommercialFlag)==false)
					{
						cf.ShowToast("Already Exists. Please enter a different Building Number.", 0);
						((EditText) dialog1.findViewById(R.id.EN_number)).setText("");							
					}
					else
					{
						try
						{
							Cursor c1 = cf.SelectTablefunction(cf.Comm_Building, " where Commercial_Flag='"+cf.CommercialFlag+"' and fld_srid='"+cf.selectedhomeid+"'");
							if(c1.getCount()>0)
							{				
								cf.arr_db.execSQL("UPDATE "+ cf.Comm_Building+ " SET BUILDING_NO='"+s+"' WHERE fld_srid ='"+ cf.selectedhomeid + "'");							
							}
						}				
						catch(Exception e)
						{
							System.out.println("dsdsfdsdsddfds="+e.getMessage());
						}
						dialog1.setCancelable(true);
						dialog1.dismiss();	
						cf.ShowToast("Building Number saved successfully.",0);
						cf.windmittypechk(cf.identityval,((RadioButton)findViewById(R.id.b1802_chk)),((RadioButton)findViewById(R.id.comm1_chk)),((RadioButton)findViewById(R.id.comm2_chk)),((RadioButton)findViewById(R.id.comm3_chk)),((TextView)findViewById(R.id.commtxt)),((ImageView)findViewById(R.id.editbno))); // To check the checkbox of choosen inspection type
				        
					}
					
				}
			});													    
			  dialog1.show();
	}
	

	public void atticautosave() {
		// TODO Auto-generated method stub
		
		try
		{
			Cursor c1=cf.SelectTablefunction(cf.Four_Electrical_Attic, " where fld_srid='"+cf.selectedhomeid+"'");
			if(c1.getCount()==0){
				cf.arr_db.execSQL("INSERT INTO "
						+ cf.Four_Electrical_Attic
						+ " (fld_srid,atcinc,atcoptions,atccomments,rfdcinc,rfdcmat,rfstmat,rfdcthi,rfdcatt,rftruss,"+
						"nailspac,missednails,rfwallinc,rtowopt,nail3,nail4,nail5,truss,truss1,gablewallinc,gablewallopt,"+
						"atccondinc,moisture,dsheath,droof,nsalt,othertxt,other)"
						+ "VALUES('"+cf.selectedhomeid+"','','','','','','','','','','','',"+
						  "'1','','','','','','','1','','','','','','','','')");

			}
			else
			{
				 c1.moveToFirst();
				 	String RTW_NA = c1.getString(c1.getColumnIndex("rfwallinc"));
				 	String GWS_NA = c1.getString(c1.getColumnIndex("gablewallinc"));
					if(RTW_NA.equals(""))
					{
						cf.arr_db.execSQL("UPDATE "
								+ cf.Four_Electrical_Attic
								+ " SET rfwallinc='1'"
								+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
					}
					else
					{
						cf.arr_db.execSQL("UPDATE "
								+ cf.Four_Electrical_Attic
								+ " SET rfwallinc='"+RTW_NA+"'"
								+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
					}		
					
					if(GWS_NA.equals(""))
					{
						cf.arr_db.execSQL("UPDATE "
								+ cf.Four_Electrical_Attic
								+ " SET gablewallinc='1'"
								+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
					}
					else
					{
						cf.arr_db.execSQL("UPDATE "
								+ cf.Four_Electrical_Attic
								+ " SET gablewallinc='"+GWS_NA+"'"
								+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
					}		
					
			}
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void gchautosave() {
		// TODO Auto-generated method stub
		 try
		 {
			 cf.CreateARRTable(417);
			 cf.CreateARRTable(418);
			 cf.CreateARRTable(416);
			 cf.CreateARRTable(48);
			 cf.CreateARRTable(47);
			 cf.CreateARRTable(46);
			 cf.CreateARRTable(42);
			 cf.CreateARRTable(44);
			 cf.CreateARRTable(45);
			 Cursor c1 = cf.arr_db.rawQuery("SELECT * FROM "+ cf.GCH_FireProtection + " WHERE fld_srid='"+ cf.selectedhomeid + "'", null);
			 if(c1.getCount()==0)
			 {
				 cf.arr_db.execSQL("INSERT INTO " + cf.GCH_FireProtection + " (fld_srid,fld_fireprot_na,fld_fireprotection)"
							+ "VALUES ('"+cf.selectedhomeid+"','1','')");		
			 }
			 else
			 {
				 c1.moveToFirst();
				 	String FIREPROT_NA = c1.getString(c1.getColumnIndex("fld_fireprot_na"));
					if(FIREPROT_NA.equals(""))
					{
						cf.arr_db.execSQL("UPDATE "
								+ cf.GCH_FireProtection
								+ " SET fld_fireprot_na='1'"
								+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
					}
					else
					{
						cf.arr_db.execSQL("UPDATE "
								+ cf.GCH_FireProtection
								+ " SET fld_fireprot_na='"+FIREPROT_NA+"'"
								+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
					}							 
			 }
			 
			
			 Cursor c2 = cf.arr_db.rawQuery("SELECT * FROM "+ cf.GCH_Misc + " WHERE fld_srid='"+ cf.selectedhomeid + "'", null);
			 if(c2.getCount()==0)
			 {
				 cf.arr_db.execSQL("INSERT INTO " + cf.GCH_Misc + " (fld_srid,fld_misc_na,fld_trash,fld_descroption,fld_desccomments,fld_soiled)"
							+ "VALUES ('"+cf.selectedhomeid+"','1','','','','')");		
			 }
			 else
			 {
				 c2.moveToFirst();
				 	String MISC_NA = c2.getString(c2.getColumnIndex("fld_misc_na"));
					if(MISC_NA.equals(""))
					{
						cf.arr_db.execSQL("UPDATE "
								+ cf.GCH_Misc
								+ " SET fld_misc_na='1'"
								+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
					}
					else
					{
						cf.arr_db.execSQL("UPDATE "
								+ cf.GCH_Misc
								+ " SET fld_misc_na='"+MISC_NA+"'"
								+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
					}							 
			 }
			 
			 
			 Cursor c3 = cf.arr_db.rawQuery("SELECT * FROM "+ cf.GCH_HoodDuct + " WHERE fld_srid='"+ cf.selectedhomeid + "'", null);
			 if(c3.getCount()==0)
			 {
				 cf.arr_db.execSQL("INSERT INTO " + cf.GCH_HoodDuct + " (fld_srid,fld_hoodduct_na,fld_hoodandductwork)"
							+ "VALUES ('"+cf.selectedhomeid+"','1','')");		
			 }
			 else
			 {
				 c3.moveToFirst();
				 	String HOOD_NA = c3.getString(c3.getColumnIndex("fld_hoodduct_na"));
					if(HOOD_NA.equals(""))
					{
						cf.arr_db.execSQL("UPDATE "
								+ cf.GCH_HoodDuct
								+ " SET fld_hoodduct_na='1'"
								+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
					}
					else
					{
						cf.arr_db.execSQL("UPDATE "
								+ cf.GCH_HoodDuct
								+ " SET fld_hoodduct_na='"+HOOD_NA+"'"
								+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
					}							 
			 }
			 
			 
			 Cursor c4 = cf.arr_db.rawQuery("SELECT * FROM "+ cf.GCH_Electrictbl + " WHERE fld_srid='"+ cf.selectedhomeid + "'", null);
			 if(c4.getCount()==0)
			 {
				 cf.arr_db.execSQL("INSERT INTO "
							+ cf.GCH_Electrictbl
							+ " (fld_srid,fld_elecchk,fld_mdisconnect,fld_mpamp,fld_mpampotr,fld_electype,fld_voprotect,fld_oehz,fld_csatis,"+
					                        "fld_updated,fld_age,fld_comments)"
							+ "VALUES ('"+cf.selectedhomeid+"','1','','','','','','','','','','')");
			 }
			 else
			 {
				 c4.moveToFirst();		
				 	String ELEC_NA = c4.getString(c4.getColumnIndex("fld_elecchk"));
					if(ELEC_NA.equals(""))
					{
						cf.arr_db.execSQL("UPDATE "
								+ cf.GCH_Electrictbl
								+ " SET fld_elecchk='1'"
								+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
					}
					else
					{
						cf.arr_db.execSQL("UPDATE "
								+ cf.GCH_Electrictbl
								+ " SET fld_elecchk='"+ELEC_NA+"'"
								+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
					}				
			 }
			 
			 Cursor c5 = cf.arr_db.rawQuery("SELECT * FROM "+ cf.GCH_Hvactbl + " WHERE fld_srid='"+ cf.selectedhomeid + "'", null);
			 if(c5.getCount()==0)
			 {
				 cf.arr_db.execSQL("INSERT INTO "
							+ cf.GCH_Hvactbl
							+ " (fld_srid,fld_hvchk,fld_hvacsystem,fld_type,fld_fuel,fld_air,fld_condsatis,fld_updated,fld_sdlupd,fld_sconf,fld_age,fld_appearsop,fld_other,fld_comments)"
							+ "VALUES ('"+cf.selectedhomeid+"','1','','','','','','','','','','','','')");
			 }
			 else
			 {
				 c5.moveToFirst();		
				 	String HVAC_NA = c5.getString(c5.getColumnIndex("fld_hvchk"));
					if(HVAC_NA.equals(""))
					{
						cf.arr_db.execSQL("UPDATE "
								+ cf.GCH_Hvactbl
								+ " SET fld_hvchk='1'"
								+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
					}
					else
					{
						cf.arr_db.execSQL("UPDATE "
								+ cf.GCH_Hvactbl
								+ " SET fld_hvchk='"+HVAC_NA+"'"
								+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
					}				
			 }
			 
			 
			 
			 Cursor c6 = cf.arr_db.rawQuery("SELECT * FROM "+ cf.GCH_SpecHazardstbl + " WHERE fld_srid='"+ cf.selectedhomeid + "'", null);
			 if(c6.getCount()==0)
			 {
				 cf.arr_db.execSQL("INSERT INTO "
							+ cf.GCH_SpecHazardstbl
							+ " (fld_srid,fld_shzchk,fld_fgases,fld_fliquids,fld_cliquids,fld_cmetals,fld_cchim,fld_welding,fld_spaint,fld_dip,fld_exp,fld_astorage,"+
					                                "fld_incin,fld_ifovens,fld_cgp,fld_agfstorage,fld_ufstorage,fld_soilyrages,fld_expdust,fld_wworking,"+
							                        "fld_hpiled,fld_pbaler,fld_ccook)"
							+ "VALUES ('"+cf.selectedhomeid+"','1','','','','','','','','','','','','','','','','','','','','','')");
			 }
			 else
			 {
				 c6.moveToFirst();		
				 	String SPECHZD_NA = c6.getString(c6.getColumnIndex("fld_shzchk"));
					if(SPECHZD_NA.equals(""))
					{
						cf.arr_db.execSQL("UPDATE "
								+ cf.GCH_SpecHazardstbl
								+ " SET fld_shzchk='1'"
								+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
					}
					else
					{
						cf.arr_db.execSQL("UPDATE "
								+ cf.GCH_SpecHazardstbl
								+ " SET fld_shzchk='"+SPECHZD_NA+"'"
								+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
					}				
			 }
			 
			 Cursor c7 = cf.SelectTablefunction(cf.GCH_FireProtecttbl, " where fld_srid='"+cf.selectedhomeid+"'");			 
			 if(c7.getCount()==0)
			 {
				 cf.arr_db.execSQL("INSERT INTO "
							+ cf.GCH_FireProtecttbl
							+ " (fld_srid,fld_fpschk,fld_fireext,fld_firedet,fld_size,fld_type,fld_wservice,fld_taged,fld_tagdate,fld_smokedet,fld_smokoptions,fld_ssys,fld_scover,fld_salarm,fld_sign)"
							+ "VALUES ('"+cf.selectedhomeid+"','1','','','','','','','','','','','','','')");
			 }
			 else
			 {
				 	c7.moveToFirst();		
				 	String FIRESURVEY_NA = c7.getString(c7.getColumnIndex("fld_fpschk"));
					if(FIRESURVEY_NA.equals(""))
					{
						cf.arr_db.execSQL("UPDATE "
								+ cf.GCH_FireProtecttbl
								+ " SET fld_fpschk='1'"
								+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
					}
					else
					{
						cf.arr_db.execSQL("UPDATE "
								+ cf.GCH_FireProtecttbl
								+ " SET fld_fpschk='"+FIRESURVEY_NA+"'"
								+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
					}	
			 }
			 
			 
			 Cursor c8 =cf.SelectTablefunction(cf.GCH_SummaryHazardstbl, " where fld_srid='"+cf.selectedhomeid+"'");
			 
		     if(c8.getCount()==0)
			 {
		    	 cf.arr_db.execSQL("INSERT INTO "
							+ cf.GCH_SummaryHazardstbl
							+ " (fld_srid,fld_shzchk,fld_vicious,fld_viciousytxt,fld_horses,fld_horseytxt,fld_overhanging,fld_trampoline,fld_skateboard,fld_bicycle,"+
							                   "fld_triphz,fld_triphztxt,fld_unsafe,fld_porchdeck,fld_nonconst,fld_odoor,fld_openfound,fld_wood,fld_excessdb,"+
							                   "fld_bop,fld_boptxt,fld_bgdisrepair,fld_visualpd,fld_structp,fld_inoper,fld_recentdw,fld_possiblecdw,"+
							                   "fld_ownerccdw,fld_nsssystem,fld_nwsdedic,fld_summaryhzcomments)"
							+ "VALUES ('"+cf.selectedhomeid+"','1','','','','','','','','','','','','','','','','','','','','','','','','','','','','','')");
		    	 
			 }
		     else
		     {
		    	 c8.moveToFirst();		
				 	String SUMMHZD_NA = c8.getString(c8.getColumnIndex("fld_shzchk"));
				 	
					if(SUMMHZD_NA.equals(""))
					{
						cf.arr_db.execSQL("UPDATE "
								+ cf.GCH_SummaryHazardstbl
								+ " SET fld_shzchk='1'"
								+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
					}
					else
					{
						cf.arr_db.execSQL("UPDATE "
								+ cf.GCH_SummaryHazardstbl
								+ " SET fld_shzchk='"+SUMMHZD_NA+"'"
								+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
					}	
		     }
		     
		     
		     Cursor c9 =cf.SelectTablefunction(cf.GCH_PoolFencetbl, " where fld_srid='"+cf.selectedhomeid+"'");
		     
		     if(c9.getCount()==0)
			 {
		    	 cf.arr_db.execSQL("INSERT INTO "
							+ cf.GCH_PoolFencetbl
							+ " (fld_srid,fld_ppfchk,fld_ppfence,fld_ppfenceother,fld_selflatches,fld_pinstalled,fld_pfdisrepair)"
							+ "VALUES ('"+cf.selectedhomeid+"','1','','','','','')");

			 }
		     else
		     {
		    	 c9.moveToFirst();		
				 	String PF_NA = c9.getString(c9.getColumnIndex("fld_ppfchk"));
				 	 
					if(PF_NA.equals(""))
					{
						cf.arr_db.execSQL("UPDATE "
								+ cf.GCH_PoolFencetbl
								+ " SET fld_ppfchk='1'"
								+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
					}
					else
					{
						cf.arr_db.execSQL("UPDATE "
								+ cf.GCH_PoolFencetbl
								+ " SET fld_ppfchk='"+PF_NA+"'"
								+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
					}	
		     }
		     
		     
		     
		     Cursor c10 =cf.SelectTablefunction(cf.GCH_PoolPrsenttbl, " where fld_srid='"+cf.selectedhomeid+"'");
		     
		     if(c10.getCount()==0)
			 {
		    		cf.arr_db.execSQL("INSERT INTO "
							+ cf.GCH_PoolPrsenttbl
							+ " (fld_srid,fld_ppchk,fld_ppground,fld_htpresent,fld_htcovered,fld_ppstructure,fld_egppresent,fld_pspresent,fld_dbpresent,fld_ppenclosure,fld_pedisrepair)"
							+ "VALUES ('"+cf.selectedhomeid+"','1','','','','','','','','','')");						 
		     }
		     else
		     {
		    	 c10.moveToFirst();		
				 	String PP_NA = c10.getString(c10.getColumnIndex("fld_ppchk"));
				 	
					if(PP_NA.equals(""))
					{
						cf.arr_db.execSQL("UPDATE "
								+ cf.GCH_PoolPrsenttbl
								+ " SET fld_ppchk='1'"
								+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
					}
					else
					{
						cf.arr_db.execSQL("UPDATE "
								+ cf.GCH_PoolPrsenttbl
								+ " SET fld_ppchk='"+PP_NA+"'"
								+ " WHERE fld_srid ='"+ cf.selectedhomeid + "'");
					}	
		     }
			 
		 }
		 catch (Exception e) {
			// TODO: handle exception
			 System.out.println("GCH="+e.getMessage());
		}
	}

	public void Windincludechk() {
		// TODO Auto-generated method stub
		((RadioButton)findViewById(R.id.b1802_chk)).setEnabled(true);((RadioButton)findViewById(R.id.b1802_chk)).setTextColor(Color.BLACK);
		((RadioButton)findViewById(R.id.comm1_chk)).setEnabled(true);((RadioButton)findViewById(R.id.comm1_chk)).setTextColor(Color.BLACK);
		((RadioButton)findViewById(R.id.comm2_chk)).setEnabled(true);((RadioButton)findViewById(R.id.comm2_chk)).setTextColor(Color.BLACK);
		((RadioButton)findViewById(R.id.comm3_chk)).setEnabled(true);((RadioButton)findViewById(R.id.comm3_chk)).setTextColor(Color.BLACK);
	}

	public void Roofselect(int i) {
		// TODO Auto-generated method stub
		int roofid=0;
		if(i==32)
		{
			roofid=4;
		}
		else if(i==33)
		{
			roofid=5;
		}
		else if(i==34)
		{
			roofid=6;
		}
		/*Cursor c1=	cf.arr_db.rawQuery("SELECT * FROM "+cf.Roof_cover_type+" WHERE (RCT_masterid= (Select RM_masterid from "+cf.Roof_master+" Where RM_inspectorid='"+cf.Insp_id+"' and RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+roofid+"' and RM_module_id='0'))",null);
		Cursor c2=  cf.arr_db.rawQuery("SELECT * From "+cf.General_Roof_information+" WHERE RC_masterid=(SELECT RM_masterid FROM "+cf.Roof_master+" Where RM_srid='"+cf.selectedhomeid+"' and RM_insp_id='"+roofid+"' and RM_module_id='1' )",null);
	*/
	}
	private void hidesubmenu() {
		// TODO Auto-generated method stub
		((RadioButton)findViewById(R.id.b1802_chk)).setChecked(false);			
		((RadioButton)findViewById(R.id.comm1_chk)).setChecked(false);
		((RadioButton)findViewById(R.id.comm2_chk)).setChecked(false);
		((RadioButton)findViewById(R.id.comm3_chk)).setChecked(false);
		((TextView)findViewById(R.id.commtxt)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.editbno)).setVisibility(cf.v1.GONE);
	}
	public void getTable(int typeid) {
		// TODO Auto-generated method stub
		try
		{
		selecttbl();
		c1 = cf.SelectTablefunction(cf.Wind_Questiontbl, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+typeid+"'");
		c2 = cf.SelectTablefunction(cf.quesroofcover, " where SRID='"+cf.selectedhomeid+"' and INSP_TYPEID='"+typeid+"'");
		c3 = cf.SelectTablefunction(cf.Wind_QuesCommtbl, " where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+typeid+"'");
		c4 = cf.SelectTablefunction(cf.WindMit_WallCons, "where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+typeid+"'");
		c5 = cf.SelectTablefunction(cf.Communal_Areas, "where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+typeid+"'");
		c6 = cf.SelectTablefunction(cf.Wind_Commercial_Comm, "where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+typeid+"'");
		c7 = cf.SelectTablefunction(cf.Communal_AreasElevation, "where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+typeid+"'");		
		c8 = cf.SelectTablefunction(cf.Comm_Aux_Master, "where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+typeid+"'");
		c9 = cf.SelectTablefunction(cf.Comm_AuxBuilding, "where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+typeid+"'");
		c10 = cf.SelectTablefunction(cf.Comm_RestSuppAppl, "where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+typeid+"'");
		c11 = cf.SelectTablefunction(cf.Comm_RestSupplement, "where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+typeid+"'");
		}
		catch(Exception e)
		{
			System.out.println("catch ="+e.getMessage());
		}
		
	}
	protected void deletecomm2(int id) {
			// TODO Auto-generated method stub
			selecttbl();
			getTable(id);
			roofdelete(id);
			if(c5.getCount()!=0){cf.arr_db.execSQL("Delete from "+cf.Communal_Areas+" Where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+id+"'");}
			if(c6.getCount()!=0){cf.arr_db.execSQL("Delete from "+cf.Wind_Commercial_Comm+" Where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+id+"'");}
			if(c7.getCount()!=0){cf.arr_db.execSQL("Delete from "+cf.Communal_AreasElevation+" Where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+id+"'");}
			if(c8.getCount()!=0){cf.arr_db.execSQL("Delete from "+cf.Comm_Aux_Master+" Where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+id+"'");}
			if(c9.getCount()!=0){cf.arr_db.execSQL("Delete from "+cf.Comm_AuxBuilding+" Where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+id+"'");}
			if(c10.getCount()!=0){cf.arr_db.execSQL("Delete from "+cf.Comm_RestSuppAppl+" Where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+id+"'");}
			if(c11.getCount()!=0){cf.arr_db.execSQL("Delete from "+cf.Comm_RestSupplement+" Where fld_srid='"+cf.selectedhomeid+"' and insp_typeid='"+id+"'");}
			hidesubmenu();	
	}
	private void roofdelete(int id)
	{
		int roofid = 0;
		cf.CreateARRTable(99);
		cf.CreateARRTable(431);
		cf.CreateARRTable(43);
		cf.CreateARRTable(432);
		cf.CreateARRTable(433);
		cf.CreateARRTable(434);
		
		if(id==32)
		{
			roofid=4;
		}
		else if(id==33)
		{
			roofid=5;
		}
		else if(id==34)
		{
			roofid=6;
		}
	}
	private void selecttbl() {
		// TODO Auto-generated method stub
		cf.CreateARRTable(31);cf.CreateARRTable(32);cf.CreateARRTable(33);cf.CreateARRTable(34);cf.CreateARRTable(35);
		cf.CreateARRTable(36);cf.CreateARRTable(37);cf.CreateARRTable(38);cf.CreateARRTable(39);cf.CreateARRTable(311);
		
	}

	private void Tabcolour_B1802_Comm1() {
		// TODO Auto-generated method stub
		 if(!cf.bccomments.equals(""))
		  {
			  w1802_btnbcode.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
		  }
		  if(!cf.rccomments.equals(""))
		  {
			  w1802_btnrfcvr.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
		  }
		  if(!cf.rdcomments.equals(""))
		  {
			  w1802_btnrfdeck.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
		  }
		  if(!cf.rwcomments.equals(""))
		  {
			  w1802_btnrfwall.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
		  }
		  if(!cf.rgcomments.equals(""))
		  {
			  w1802_btnrfgeo.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
		  }
		  if(!cf.swrcomments.equals(""))
		  {
			  w1802_btnswr.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
		  }
		  if(cf.WindOpen_NA.equals("1") || cf.windopen.getCount()!=0)
		  {
			  w1802_windowopen.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
		  }					 
		  if(cf.DoorOpen_NA.equals("1") || cf.ressupchk1>=2)
		  {
			  w1802_dooropen.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
		  }	

		  if(cf.Skylights_NA.equals("1") ||cf.skylightsopen.getCount()!=0)
		  {
			  w1802_skylights.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
		  }	
		  if(!cf.opcomments.equals(""))
		  {
			  w1802_btnopen.setBackgroundDrawable(getResources().getDrawable(R.drawable.changesubbackrepeatovr));
		  }
	}
}