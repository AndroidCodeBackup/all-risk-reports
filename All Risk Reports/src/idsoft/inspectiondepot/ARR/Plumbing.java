/*
************************************************************
* Project: ALL RISK REPORT
* Module : Plumbing.java
* Creation History:
Created By : Gowri on 
* Modification History:
Last Modified By : Gowri on 04/08/2013
************************************************************ 
*/
package idsoft.inspectiondepot.ARR;


import java.io.File;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Plumbing extends Activity{
	private static final int DATE_DIALOG_ID = 0;
	public int pick_img_code=24;
	
	public RadioButton sewercaprdio[] = new RadioButton[7];
	public int[] sewerrdioid = {R.id.sewercaprdio1,R.id.sewercaprdio2,R.id.sewercaprdio3,R.id.sewercaprdio4,R.id.sewercaprdio5,R.id.sewercaprdio7,R.id.sewercaprdio6};
	
	
	public int[] plumchkhdrs = {R.id.wss_chk,R.id.sss_chk,R.id.wh_chk,R.id.nob_chk,R.id.oc_chk,R.id.cop_chk};
	CheckBox[] plumchk = new CheckBox[6];
	public int[] wpt = {R.id.wpt_chk1,R.id.wpt_chk2,R.id.wpt_chk3,R.id.wpt_chk4,R.id.wpt_chk5,R.id.wpt_chk6,R.id.wpt_chk7};
    CheckBox[] wptchk = new CheckBox[7];
	public int[] pt = {R.id.pt_chk1,R.id.pt_chk2,R.id.pt_chk3,R.id.pt_chk4,R.id.pt_chk5,R.id.pt_chk6,R.id.pt_chk7,
			           R.id.pt_chk8,R.id.pt_chk9};
	CheckBox[] ptchk = new CheckBox[9];
	public int[] wru = {R.id.wru_chk1,R.id.wru_chk2,R.id.wru_chk3,R.id.wru_chk4};
    CheckBox[] wruchk = new CheckBox[4];
	RadioGroup pscrdgval,llerdgval,lrprdgval,dpuwrdgval,lsprdgval,lerdgval,wafcrdgval,alrdgval,iplrdgval,clcrdgval,
	           whlrdgval,otrrdgval,seprdgval,ptrnrdgval,yicrdgval,scfrdgval,rurdgval,pcrdgval,levrdgval,
	           sonrdgval,wyicrdgval,msoprdgval,wlocrdgval,wrurdgval,wpcrdgval,wstrdgval,strdgval;
	String pscrdtxt="",showstr="",retpscrdtxt="",plumcomt="",lrprdgtxt="",dpuwrdgtxt="",lsprdgtxt="",lerdgtxt="",
               wafcrdgtxt="",alrdgtxt="",iplrdgtxt="",clcrdgtxt="",llerdgtxt="",
	           whlrdgtxt="",otrrdgtxt="",retlrprdtxt="",nobstr="--Select--",nobfcstr="--Select--",seprdtxt="",ptrnrdtxt="",retnobstr="",agestr="",
	           yicrdtxt="Original",scfrdtxt="",rurdtxt="",pcrdtxt="",levrdtxt="",sonrdtxt="",flkrvstr="",ptchk_value="",
	           retsstypstr="",wagestr="",wyicrdtxt="Original",msoprdtxt="",wlocrdtxt="",wconfstr="--Select--",wrurdtxt="",wpcrdtxt="",
	           wptchk_value="",wyruchk_value="",retwstypstr="",whtrtypstr="",whtragestr="",
	           whtrlofcstr="",whtrdppstr,whtrdlstr="",whtrlestr="",htr="",wstrdgtxt="",wdlupdated="",strdgtxt="",
	           retaldtxt="",sdlupdated="",retodrdtxt="",whtrcapstr="",whtrlocstr="";
    int chkvl=2,rws,iden=0,
    	Current_select_id,spunt=0,wschk,ssck=0,ssso=0,wsso=0,odinc=0,whinc=0;
	String nobchkval,occhkval,ssschkval,wsschkval="0",odchkval,whtrchkval,copchkval,wetrup,ssage="",wsage="",ssageconfig="",wsageconfig="";
	String bval[] = { "--Select--","N/A","1","1.5","2","2.5","3","3.5","4","4.5","5","0"};
	String flr[] = {"--Select--","Yes","No","Not Applicable"};
	String whtr_in_DB[];
	String wconf[] = {"--Select--","Seller Disclosure","Visible Components","Owner Disclosure","Owner Representative Disclosure","Not Located","Other"};
	String whtrtype[] = {"--Select--","Electric","Oil","Gas","Solar","On Demand","Tankless Coil"};
	String whtrcapc[] = {"--Select--","18 Gallons","24 Gallons","32 Gallons","40 Gallons","50 Gallons","80 Gallons","Other"};
	String whtrloct[] = {"--Select--","Garage","Hall Closet","Master Bedroom Closet","Under Kitchen Sink","Closet","Laundry Room","Other"};
	ArrayAdapter nobadap,nobfcadap,ageadap,flkrvadap,wageadap,wconfadap,whtrtypadap,whtrageadap,
	             whtrlofc,whtrdpp,whtrdl,whtrle,whtrcap,whtrloc;
	Spinner nobspin,nobfcspin,agespin,flkrvspin,wagespin,wconfspin,whtrtypspin,whtragespin,
	        whtrlofcspin,whtrdppspin,whtrdlspin,whtrlespin,whtrcapspin,whtrlocspin;
	CommonFunctions cf;
	Cursor plumwh_save;
	int wc=0,sc=0,whval,wssval=0,ssval=0;
	TableLayout whtrtbl;
	LinearLayout systemrecupdcomlin;
	public boolean hack = false,wstck=false,wmsp=false,wloc=false,wruck=false,wpcck=false,
			stck=false,scfck=false,sruck=false,spcck=false,leck=false,sonck=false,wyicck=false,sschk=false,
			sepck=false,ptrnck=false,lrpck=false,lspck=false,dpuwck=false,wafcck=false,yicck=false,
			lleck=false,clcck=false,alck=false,ioplck=false,whlck=false,otrck=false,pscck=false,
			load_comment=true,wss_sve=false,sss_sve=false,wh_sve=false,nob_sve=false,lr_sve=false,
			od_sve=false,con_sve=false;
			
	@Override
    public void onCreate(Bundle savedInstanceState) {

	        super.onCreate(savedInstanceState);
	        cf=new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);
				
		 	}
	        setContentView(R.layout.plumbing);
	        cf.getDeviceDimensions();
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
			hdr_layout.addView(new HdrOnclickListener(this,1,"Four Point Inspection","Plumbing System",2,0,cf));
         	LinearLayout main_layout = (LinearLayout) findViewById(R.id.header);
         	main_layout.setMinimumWidth(cf.wd);
         	main_layout.addView(new MyOnclickListener(getApplicationContext(), 2, 0,0,cf));
	        LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 23, 1,0,cf));
			TableRow tblrw = (TableRow)findViewById(R.id.row2);
			tblrw.setMinimumHeight(cf.ht);
			cf.CreateARRTable(23);cf.CreateARRTable(231);
			try
			{
				Cursor chkplu_save=cf.SelectTablefunction(cf.Four_Electrical_Plumbing, " where fld_srid='"+cf.selectedhomeid+"'");
				if(chkplu_save.getCount()==0){
					cf.arr_db.execSQL("INSERT INTO "
							+ cf.Four_Electrical_Plumbing
							+ " (fld_srid,wsinc,wstyp,wptyp,wage,wyiconf,mainshut,wloc,wconf,wrupd,wrupdy,wdlupd,wpconf,"+
							  "ssinc,sstype,ptype,sapproxage,yiconf,scpfound,srupd,sdlupd,spconf,slevi,"+
						      "sonoted,sflrv,scomments,nobinc,tnob,nobfc,sencpre,ptrepned,ocinc,lrmpres,dpuwasher,lsnpre,levi,wafceil,aleak,inpleak,clkcrack,whleak,other,"+
							  "ovrplucondinc,ovrplucondopt,plucomments,odinc,whinc)"
							+ "VALUES('"+cf.selectedhomeid+"','','','','','','','','','','','','','','','','','','','','','','','','','','',"+
							  "'','','','',"+
							  "'','','','','','','','','','','','','','','','')");
					
				}
				else
				{
					
				}
			}
			catch (Exception e) {
				// TODO: handle exception
			}
			declarations();
			cf.getPolicyholderInformation(cf.selectedhomeid);
			Display_values();
	}
	private void Display_values() {
		// TODO Auto-generated method stub
		try{
			   chk_values();
			   Cursor plu_retrive=cf.SelectTablefunction(cf.Four_Electrical_Plumbing, " where fld_srid='"+cf.selectedhomeid+"'");
			   System.out.println("coun= "+plu_retrive.getCount());
			   if(plu_retrive.getCount()>0)
			   {  
				   plu_retrive.moveToFirst();
				   if(plu_retrive.getString(plu_retrive.getColumnIndex("wsinc")).equals("0"))
				   {
					    wschk=0;
					   ((LinearLayout)findViewById(R.id.wsslin)).setVisibility(cf.v1.VISIBLE);
					   ((TextView)findViewById(R.id.savetxtwss)).setVisibility(cf.v1.VISIBLE);
					   ((CheckBox)findViewById(R.id.wss_chk)).setChecked(false);
					   ((ImageView)findViewById(R.id.shwwss)).setVisibility(cf.v1.GONE);
					   ((ImageView)findViewById(R.id.hdwss)).setVisibility(cf.v1.VISIBLE);
					    String wtype=cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("wstyp")));
					    System.out.println("wtype"+wtype);
					    if(wtype.contains("&#94;"))
					    {
					        String[] wtemp = wtype.split("&#94;");
						    wstrdgtxt = wtemp[0];System.out.println("wtemp[0]"+wtemp[0]+wtemp.length);
						    if(wtemp.length>1)
						    {
							    if(!wtemp[1].equals(""))
							    {
							    	System.out.println("wtemp[1]"+wtemp[1]);
							    	((EditText)findViewById(R.id.watercomment)).setText(wtemp[1]);
							    }
						    }
					    }
					    else
					    {
					    	wstrdgtxt = wtype;
					    }						
					    System.out.println("wstrdgtxt"+wstrdgtxt);
						((RadioButton) wstrdgval.findViewWithTag(wstrdgtxt)).setChecked(true);
					    wptchk_value=cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("wptyp")));
					    System.out.println("wptchk_value"+wptchk_value);
					   cf.setvaluechk1(wptchk_value,wptchk,((EditText)findViewById(R.id.wptotr)));
					   
					   System.out.println("wptchk_value="+wptchk_value);
					   if(wptchk_value.contains("Galvanized Steel"))
					   {
						   ((LinearLayout)findViewById(R.id.waterflowrest)).setVisibility(cf.v1.VISIBLE);
						   String whleak = cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("whleak")));System.out.println("whleak"+whleak);
						   ((RadioButton) whlrdgval.findViewWithTag(whleak)).setChecked(true);						   
					   }
					   else
					   {
						   ((LinearLayout)findViewById(R.id.waterflowrest)).setVisibility(cf.v1.GONE);
					   }
					   
					    String wage=cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("wage")));
					    if(wage.contains("Other")|| wage.contains("Other&#94;")){  
						    String[] temp = wage.split("&#94;");wsso=1;
						    wagestr = temp[0];;
						    ((EditText)findViewById(R.id.wage_otr)).setText(temp[1]);
						    ((EditText)findViewById(R.id.wage_otr)).setVisibility(cf.v1.VISIBLE);
					    }else{
					    	wagestr = wage;((EditText)findViewById(R.id.wage_otr)).setVisibility(cf.v1.GONE);
						 }
					    wagespin.setSelection(wageadap.getPosition(wagestr)); 
					    wyicrdtxt=cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("wyiconf")));
					    ((RadioButton) wyicrdgval.findViewWithTag(wyicrdtxt)).setChecked(true);
					    msoprdtxt=cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("mainshut")));
					    ((RadioButton) msoprdgval.findViewWithTag(msoprdtxt)).setChecked(true);
					    wlocrdtxt=cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("wloc")));
					    ((RadioButton) wlocrdgval.findViewWithTag(wlocrdtxt)).setChecked(true);
					    String wcf=cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("wconf")));
					    if(wcf.contains("Other")){ 
						    String[] temp = wcf.split("&#94;");
						    wconfstr = temp[0];
						    ((EditText)findViewById(R.id.wconf_otr)).setText(temp[1]);
						    ((EditText)findViewById(R.id.wconf_otr)).setVisibility(cf.v1.VISIBLE);
					    }else{
					    	wconfstr = wcf;((EditText)findViewById(R.id.wconf_otr)).setVisibility(cf.v1.GONE);
						 }
					    wconfspin.setSelection(wconfadap.getPosition(wconfstr)); 
					    String wrupd=cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("wrupd")));
						if(wrupd.contains("Yes") || wrupd.contains("Yes~")){
							String[] temp1 = wrupd.split("~");
							 wrurdtxt = temp1[0];
							((LinearLayout)findViewById(R.id.perlin)).setVisibility(cf.v1.VISIBLE);
							((EditText)findViewById(R.id.wruotr)).setText(temp1[1]);
							((LinearLayout)findViewById(R.id.rushow)).setVisibility(cf.v1.VISIBLE);
							 wyruchk_value=cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("wrupdy")));
							 cf.setvaluechk1(wyruchk_value,wruchk,((EditText)findViewById(R.id.wptotr)));
							 String wretdl = cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("wdlupd")));
							 //if(wretdl.length()==4){
							
							 if(wretdl.equals("Not Determined")){
								  ((CheckBox)findViewById(R.id.wndchk)).setChecked(true); wc=1;
								  //((EditText)findViewById(R.id.wnd_otr)).setVisibility(cf.v1.VISIBLE);
								  //((EditText)findViewById(R.id.wnd_otr)).setText(cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("wdlupd"))));
							 }else{
								 ((CheckBox)findViewById(R.id.wndchk)).setChecked(false);wc=0; ((EditText)findViewById(R.id.wnd_otr)).setVisibility(cf.v1.GONE);
								
							 }
							 ((EditText)findViewById(R.id.wdlutxt)).setText(wretdl);
							 wpcrdtxt=cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("wpconf")));
							((RadioButton) wpcrdgval.findViewWithTag(wpcrdtxt)).setChecked(true);
						}else{
							wrurdtxt = wrupd;
							((LinearLayout)findViewById(R.id.perlin)).setVisibility(cf.v1.GONE);
							((LinearLayout)findViewById(R.id.rushow)).setVisibility(cf.v1.GONE);
						}
						((RadioButton) wrurdgval.findViewWithTag(wrurdtxt)).setChecked(true);
		
						
					    
				   }else if(plu_retrive.getString(plu_retrive.getColumnIndex("wsinc")).equals("1"))
				   {
					   ((LinearLayout)findViewById(R.id.wsslin)).setVisibility(cf.v1.GONE);
					   ((TextView)findViewById(R.id.savetxtwss)).setVisibility(cf.v1.VISIBLE);
					   ((ImageView)findViewById(R.id.shwwss)).setVisibility(cf.v1.VISIBLE);
					   ((ImageView)findViewById(R.id.hdwss)).setVisibility(cf.v1.GONE);
					   
					   if(plu_retrive.getString(plu_retrive.getColumnIndex("wsinc")).equals("1")||
							   plu_retrive.getString(plu_retrive.getColumnIndex("wsinc")).equals("2"))
					   {
						   ((CheckBox)findViewById(R.id.wss_chk)).setChecked(true);
					  }
				   }
				   else
				   {
					   defaultwage();					 
					   defaultwss();
				   }
				   System.out.println("SWRER="+plu_retrive.getString(plu_retrive.getColumnIndex("ssinc")));
				   if(plu_retrive.getString(plu_retrive.getColumnIndex("ssinc")).equals("0"))
				   {
					   ssck=0;
					   ((LinearLayout)findViewById(R.id.ssslin)).setVisibility(cf.v1.VISIBLE);
					   ((TextView)findViewById(R.id.savetxtsss)).setVisibility(cf.v1.VISIBLE);
					   ((CheckBox)findViewById(R.id.sss_chk)).setChecked(false);
					   ((ImageView)findViewById(R.id.shwsss)).setVisibility(cf.v1.GONE);
					   ((ImageView)findViewById(R.id.hdsss)).setVisibility(cf.v1.VISIBLE);
					   
					    strdgtxt=cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("sstype")));
					    System.out.println("the ss type value"+strdgtxt);
					    ((RadioButton) strdgval.findViewWithTag(strdgtxt)).setChecked(true);System.out.println("testfixed");
					    ptchk_value=cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("ptype")));System.out.println("ptchk_value"+ptchk_value);
					    cf.setvaluechk1(ptchk_value,ptchk,((EditText)findViewById(R.id.ptotr)));
					    String sage=cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("sapproxage")));System.out.println("sage"+sage);
					    if(sage.contains("Other")){ 
						    String[] temp = sage.split("&#94;");ssso=1;
						    agestr = temp[0];
						    ((EditText)findViewById(R.id.age_otr)).setText(temp[1]);
						    ((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.VISIBLE);
					    }else{
					    	agestr = sage;((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.GONE);
						 }System.out.println("agep"+agestr);
					    agespin.setSelection(ageadap.getPosition(agestr)); 
					    yicrdtxt=cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("yiconf")));System.out.println("yicrdtxt"+yicrdtxt);
						((RadioButton) yicrdgval.findViewWithTag(yicrdtxt)).setChecked(true);
						scfrdtxt=cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("scpfound")));System.out.println("scfrdtxt"+scfrdtxt);
						if(scfrdtxt.contains("Other"))
						{
							String[] sewertext = scfrdtxt.split("&#94;");
							cf.setRadioBtnValue(sewertext[0],sewercaprdio);
							((EditText)findViewById(R.id.sewerother)).setVisibility(cf.v1.VISIBLE);
					    	((EditText)findViewById(R.id.sewerother)).setText(sewertext[1]);
						}
						else
						{
							((EditText)findViewById(R.id.sewerother)).setVisibility(cf.v1.GONE);
					    	((EditText)findViewById(R.id.sewerother)).setText("");
							cf.setRadioBtnValue(scfrdtxt,sewercaprdio);
						}
						
						//((RadioButton) scfrdgval.findViewWithTag(scfrdtxt)).setChecked(true);
						String rupd=cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("srupd")));System.out.println("rupd"+rupd);
						if(rupd.contains(cf.getResourcesvalue(R.string.yesb)) || rupd.contains(cf.getResourcesvalue(R.string.yesb)+"~")){
							String[] temp1 = rupd.split("~");
							 rurdtxt = temp1[0];
							((LinearLayout)findViewById(R.id.sperlin)).setVisibility(cf.v1.VISIBLE);
							((EditText)findViewById(R.id.ruotr)).setText(temp1[1]);
							String sretdl = cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("sdlupd")));
							System.out.println("sretdl="+sretdl);
							//if(sretdl.length()==4){
							if(sretdl.equals("Not Determined")){
								  ((CheckBox)findViewById(R.id.sndchk)).setChecked(true); sc=1;
								  //((EditText)findViewById(R.id.snd_otr)).setVisibility(cf.v1.VISIBLE);
								  //((EditText)findViewById(R.id.snd_otr)).setText(cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("sdlupd"))));
							 }else{
								 ((CheckBox)findViewById(R.id.sndchk)).setChecked(false); sc=0;
								 ((EditText)findViewById(R.id.snd_otr)).setVisibility(cf.v1.GONE);
								 
							 }
							((EditText)findViewById(R.id.dlutxt)).setText(sretdl);
							 pcrdtxt=cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("spconf")));
	 						((RadioButton) pcrdgval.findViewWithTag(pcrdtxt)).setChecked(true);
	 						((LinearLayout)findViewById(R.id.ssrushow)
	 								).setVisibility(cf.v1.VISIBLE);
						}else{
							rurdtxt = rupd;
							((LinearLayout)findViewById(R.id.sperlin)).setVisibility(cf.v1.GONE);
							((LinearLayout)findViewById(R.id.ssrushow)).setVisibility(cf.v1.GONE);
						}
						((RadioButton) rurdgval.findViewWithTag(rurdtxt)).setChecked(true);
						
 						 levrdtxt=cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("slevi")));
  						((RadioButton) levrdgval.findViewWithTag(levrdtxt)).setChecked(true);
  						sonrdtxt=cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("sonoted")));
  						((RadioButton) sonrdgval.findViewWithTag(sonrdtxt)).setChecked(true);
  						
 					   ((EditText)findViewById(R.id.sewercomment)).setText(cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("scomments"))));
						
				   }else if(plu_retrive.getString(plu_retrive.getColumnIndex("ssinc")).equals("1")){
					   ((LinearLayout)findViewById(R.id.ssslin)).setVisibility(cf.v1.GONE);
					   ((TextView)findViewById(R.id.savetxtsss)).setVisibility(cf.v1.VISIBLE);
					   ((ImageView)findViewById(R.id.shwsss)).setVisibility(cf.v1.VISIBLE);
					   ((ImageView)findViewById(R.id.hdsss)).setVisibility(cf.v1.GONE);
					   
					   if(plu_retrive.getString(plu_retrive.getColumnIndex("ssinc")).equals("1")||
							   plu_retrive.getString(plu_retrive.getColumnIndex("ssinc")).equals("2"))
					   {
						   ((CheckBox)findViewById(R.id.sss_chk)).setChecked(true);
					 }
					 
				   }
				   else
				   {
					   defaultage();
					   defaultsewer();
				   }
				   /**Showing water heater**/
				   System.out.println("value="+plu_retrive.getString(plu_retrive.getColumnIndex("whinc")));
				   if(!plu_retrive.getString(plu_retrive.getColumnIndex("whinc")).equals(""))
				   {
					   whval = Integer.parseInt(plu_retrive.getString(plu_retrive.getColumnIndex("whinc")));   
				   }
				   
				   if(plu_retrive.getString(plu_retrive.getColumnIndex("whinc")).equals("0"))
				   {
					  ((CheckBox)findViewById(R.id.wh_chk)).setChecked(false);
					  ((TextView)findViewById(R.id.savetxtwh)).setVisibility(cf.v1.VISIBLE);
					  ((LinearLayout)findViewById(R.id.whlin)).setVisibility(cf.v1.VISIBLE);
					  ((ImageView)findViewById(R.id.shwwh)).setVisibility(cf.v1.GONE);
					  ((ImageView)findViewById(R.id.hdwh)).setVisibility(cf.v1.VISIBLE);
					  try
					  {
						  cf.arr_db.execSQL("UPDATE  "+cf.Plumbing_waterheater+" SET fld_flag='0' Where fld_srid='"+cf.selectedhomeid+"' ");

					  }catch (Exception e) {
						// TODO: handle exception
					}
					  ShowHeaterValue();
					   
				   }else if(plu_retrive.getString(plu_retrive.getColumnIndex("whinc")).equals("1")){
					    ((LinearLayout)findViewById(R.id.whlin)).setVisibility(cf.v1.GONE);
					    ((TextView)findViewById(R.id.savetxtwh)).setVisibility(cf.v1.VISIBLE);
					    ((ImageView)findViewById(R.id.shwwh)).setVisibility(cf.v1.VISIBLE);
						((ImageView)findViewById(R.id.hdwh)).setVisibility(cf.v1.GONE);
						  
					   if(plu_retrive.getString(plu_retrive.getColumnIndex("whinc")).equals("1") ||
							   plu_retrive.getString(plu_retrive.getColumnIndex("whinc")).equals("2"))
					   {
						   ((CheckBox)findViewById(R.id.wh_chk)).setChecked(true);
						   
					   }
				   
				   
		    	   }
				   else
				   {
					   System.out.println("Cem insde 111");
					   defaultwhage();
				   }
				   /***showing water heater ends*/
				   System.out.println("bathroo="+plu_retrive.getString(plu_retrive.getColumnIndex("nobinc")));
				   if(plu_retrive.getString(plu_retrive.getColumnIndex("nobinc")).equals("0"))
				   {
					    whtrchkval = "0";
					   ((CheckBox)findViewById(R.id.nob_chk)).setChecked(false);
				       ((TextView)findViewById(R.id.savetxtnob)).setVisibility(cf.v1.VISIBLE);
				       ((LinearLayout)findViewById(R.id.noblin)).setVisibility(cf.v1.VISIBLE);
				       ((ImageView)findViewById(R.id.shwnob)).setVisibility(cf.v1.GONE);
					   ((ImageView)findViewById(R.id.hdnob)).setVisibility(cf.v1.VISIBLE);
						  
					   nobstr=cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("tnob")));
					   nobspin.setSelection(nobadap.getPosition(nobstr));
					   nobfcstr=cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("nobfc")));
					   nobfcspin.setSelection(nobfcadap.getPosition(nobfcstr));
					   seprdtxt=cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("sencpre")));
					   ((RadioButton) seprdgval.findViewWithTag(seprdtxt)).setChecked(true);
					   ptrnrdtxt=cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("ptrepned")));
					   ((RadioButton) ptrnrdgval.findViewWithTag(ptrnrdtxt)).setChecked(true);
					   iplrdgtxt=cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("inpleak")));
					  try{ ((RadioButton) iplrdgval.findViewWithTag(iplrdgtxt)).setChecked(true);}catch(Exception e){System.out.println("EEEEE="+e.getMessage());}
					   
					   
				   }else if(plu_retrive.getString(plu_retrive.getColumnIndex("nobinc")).equals("1")){
					   ((LinearLayout)findViewById(R.id.noblin)).setVisibility(cf.v1.GONE);
					   ((TextView)findViewById(R.id.savetxtnob)).setVisibility(cf.v1.VISIBLE);
					   ((ImageView)findViewById(R.id.shwnob)).setVisibility(cf.v1.VISIBLE);
					   ((ImageView)findViewById(R.id.hdnob)).setVisibility(cf.v1.GONE);
					   
					   if(plu_retrive.getInt(plu_retrive.getColumnIndex("nobinc"))==1 ||
							   plu_retrive.getInt(plu_retrive.getColumnIndex("nobinc"))==2)
					   {
						   ((CheckBox)findViewById(R.id.nob_chk)).setChecked(true);
							  
					   }
					   
				   }
				   else
				   {
					   defaultbathroom();
				   }
			     
				   if(plu_retrive.getString(plu_retrive.getColumnIndex("ocinc")).equals("0"))
				   {
					   occhkval="0";System.out.println("ocinc is zero");
					   ((LinearLayout)findViewById(R.id.oclin)).setVisibility(cf.v1.VISIBLE);
					   ((TextView)findViewById(R.id.savetxtoc)).setVisibility(cf.v1.VISIBLE);
					   ((ImageView)findViewById(R.id.shwlr)).setVisibility(cf.v1.GONE);
					   ((ImageView)findViewById(R.id.hdlr)).setVisibility(cf.v1.VISIBLE);System.out.println("ocinc is zero");
					   ((CheckBox)findViewById(R.id.oc_chk)).setChecked(false);
					    lrprdgtxt=cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("lrmpres")));System.out.println("lrprdgtxt is zero"+lrprdgtxt);
					    ((RadioButton) lrprdgval.findViewWithTag(lrprdgtxt)).setChecked(true);
					    dpuwrdgtxt=cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("dpuwasher")));System.out.println("dpuwrdgtxt"+dpuwrdgtxt);
					    ((RadioButton) dpuwrdgval.findViewWithTag(dpuwrdgtxt)).setChecked(true);
					    lsprdgtxt=cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("lsnpre")));
					    ((RadioButton) lsprdgval.findViewWithTag(lsprdgtxt)).setChecked(true);
					    
					    wafcrdgtxt=cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("wafceil")));System.out.println("wafcrdgtxt"+wafcrdgtxt);
					    ((RadioButton) wafcrdgval.findViewWithTag(wafcrdgtxt)).setChecked(true);
					    clcrdgtxt=cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("clkcrack")));
					    ((RadioButton) clcrdgval.findViewWithTag(clcrdgtxt)).setChecked(true);
					    llerdgtxt=cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("levi")));
					    System.out.println("llerdgtxt"+llerdgtxt);
					    
					    
					    try{
					    ((RadioButton) llerdgval.findViewWithTag(llerdgtxt)).setChecked(true);
					    }catch (Exception e) {
							// TODO: handle exception
						}
					    
						
				   }else if(plu_retrive.getString(plu_retrive.getColumnIndex("ocinc")).equals("1")){
					   ((LinearLayout)findViewById(R.id.oclin)).setVisibility(cf.v1.GONE);
					   ((TextView)findViewById(R.id.savetxtoc)).setVisibility(cf.v1.VISIBLE);
					   ((ImageView)findViewById(R.id.shwlr)).setVisibility(cf.v1.VISIBLE);
					   ((ImageView)findViewById(R.id.hdlr)).setVisibility(cf.v1.GONE);
					   
					   System.out.println("int="+plu_retrive.getInt(plu_retrive.getColumnIndex("ocinc")));
					   if(plu_retrive.getInt(plu_retrive.getColumnIndex("ocinc"))==1||
							   plu_retrive.getInt(plu_retrive.getColumnIndex("ocinc"))==2)
					   {
						   System.out.println("inside");
						   ((CheckBox)findViewById(R.id.oc_chk)).setChecked(true);
					   }
					   else
					   {
                                System.out.println("end");
					   }
					   
				   }
				   else
				   {
					   defaultlaundry();
				   }
				   
				  /*if(plu_retrive.getString(plu_retrive.getColumnIndex("odinc")).equals("0")){
							   odinc = 0;
							   odchkval="0";
							   ((ImageView)findViewById(R.id.shwod)).setVisibility(cf.v1.GONE);
							   ((ImageView)findViewById(R.id.hdod)).setVisibility(cf.v1.VISIBLE);
								   ((LinearLayout)findViewById(R.id.odlin)).setVisibility(cf.v1.VISIBLE);
								   ((TextView)findViewById(R.id.savetxtod)).setVisibility(cf.v1.VISIBLE);
								   ((CheckBox)findViewById(R.id.od_chk)).setChecked(false);
								   ((RadioButton) alrdgval.findViewWithTag(cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("aleak"))))).setChecked(true);
								   iplrdgtxt=cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("inpleak")));
								   ((RadioButton) iplrdgval.findViewWithTag(iplrdgtxt)).setChecked(true);
								   whlrdgtxt=cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("whleak")));
								   ((RadioButton) whlrdgval.findViewWithTag(whlrdgtxt)).setChecked(true);
								   String st1=cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("other")));
								  
								   if(st1.contains("Yes")){ 
									    String[] temp = st1.split("&#94;");
									    otrrdgtxt = temp[0]; System.out.println("temp[1]="+temp[1]);
									    ((EditText)findViewById(R.id.otrtxt)).setText(temp[1]);
								    }else{
								    	otrrdgtxt = st1;
								    	((EditText)findViewById(R.id.otrtxt)).setVisibility(cf.v1.INVISIBLE);
								    }
								    ((RadioButton) otrrdgval.findViewWithTag(otrrdgtxt)).setChecked(true);
								   
							   }else if(plu_retrive.getString(plu_retrive.getColumnIndex("odinc")).equals("1")){ 
								   
								   ((LinearLayout)findViewById(R.id.odlin)).setVisibility(cf.v1.GONE);
								   ((TextView)findViewById(R.id.savetxtod)).setVisibility(cf.v1.VISIBLE);
								   ((ImageView)findViewById(R.id.shwod)).setVisibility(cf.v1.VISIBLE);
								   ((ImageView)findViewById(R.id.hdod)).setVisibility(cf.v1.GONE);
								   if(plu_retrive.getString(plu_retrive.getColumnIndex("odinc")).equals("1")||
										   plu_retrive.getString(plu_retrive.getColumnIndex("odinc")).equals("2"))
								   {
									   ((CheckBox)findViewById(R.id.od_chk)).setChecked(true);
									 }
								   //((CheckBox)findViewById(R.id.od_chk)).setChecked(true);
							   }
							   else
							   {
								   defaultodn();
							   }
				   */
				   
				   
				   if(plu_retrive.getString(plu_retrive.getColumnIndex("ovrplucondinc")).equals("0"))
				   {
					   copchkval="0";
					   ((LinearLayout)findViewById(R.id.coplin)).setVisibility(cf.v1.VISIBLE);
					   ((TextView)findViewById(R.id.savetxtcop)).setVisibility(cf.v1.VISIBLE);
					   ((ImageView)findViewById(R.id.shwcon)).setVisibility(cf.v1.GONE);
					   ((ImageView)findViewById(R.id.hdcon)).setVisibility(cf.v1.VISIBLE);
					   
					   ((CheckBox)findViewById(R.id.cop_chk)).setChecked(false);
					    String str1 =cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("ovrplucondopt")));
					    System.out.println("str1"+str1);
					    if(!(str1.contains("Yes, based on visual observations at the time of inspection")) && !(str1.contains("Not Determined")) && !(str1.contains("Beyond Scope of Inspection"))){ 
						    String[] temp = str1.split("&#94;");
						    pscrdtxt = temp[0];
						    ((RadioButton) pscrdgval.findViewWithTag(pscrdtxt)).setChecked(true);
						    ((LinearLayout)findViewById(R.id.nocommentsshow)).setVisibility(cf.v1.VISIBLE);
						    ((EditText)findViewById(R.id.nocomment)).setText(temp[1]);
					    }else{
					    	 pscrdtxt = str1;System.out.println("check");
					    	((RadioButton) pscrdgval.findViewWithTag(pscrdtxt)).setChecked(true);
						    ((LinearLayout)findViewById(R.id.nocommentsshow)).setVisibility(cf.v1.GONE);
					    }
					    flkrvstr=cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("sflrv")));
 					    flkrvspin.setSelection(flkrvadap.getPosition(flkrvstr));
				   }else if(plu_retrive.getString(plu_retrive.getColumnIndex("ovrplucondinc")).equals("1")){
					   ((LinearLayout)findViewById(R.id.coplin)).setVisibility(cf.v1.GONE);
					   ((TextView)findViewById(R.id.savetxtcop)).setVisibility(cf.v1.VISIBLE);
					   ((ImageView)findViewById(R.id.shwcon)).setVisibility(cf.v1.VISIBLE);
					   ((ImageView)findViewById(R.id.hdcon)).setVisibility(cf.v1.GONE);
					   
					   if(plu_retrive.getString(plu_retrive.getColumnIndex("ovrplucondinc")).equals("1") ||
							   plu_retrive.getString(plu_retrive.getColumnIndex("ovrplucondinc")).equals("2"))
					   {
						   
						   ((CheckBox)findViewById(R.id.cop_chk)).setChecked(true);
						}
					   //((CheckBox)findViewById(R.id.cop_chk)).setChecked(true);
				   }
				   else
				   {
					   defaultovrplu();
				   }
				   
				   plumcomt = cf.decode(plu_retrive.getString(plu_retrive.getColumnIndex("plucomments")));
				   if(!plumcomt.equals("")){
					   ((EditText)findViewById(R.id.plumbingcomment)).setText(plumcomt);

				      }
				   else
				   {
					   ((EditText)findViewById(R.id.plumbingcomment)).setText("No Comments.");

				   }
			   }
			   else
			   {
				   defaultwage();	
				   defaultage();
				   defaultwss();				   
				   defaultsewer();
				   defaultwhage();
				   ((EditText)findViewById(R.id.plumbingcomment)).setText("No Comments.");
			   }
				  
		}
	    catch (Exception E)
		{
	    	System.out.println("issues ="+E.getMessage());
			String strerrorlog="Retrieving Plumbing - FourPoint";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}

	private void defaultbathroom() {
		// TODO Auto-generated method stub
		cf.getPolicyholderInformation(cf.selectedhomeid);
		System.out.println("what is stories="+cf.Stories);
		if(cf.Stories.equals("1"))
		{
			nobfcspin.setSelection(nobfcadap.getPosition("0"));
		}
	}
	private void defaultwage()
	{
		  if(cf.YearPH.contains("Other"))
		  {
			   String otheryear[] = cf.YearPH.split("&#126;");
			   int spinnerPosition = wageadap.getPosition(otheryear[0]);
			   wagespin.setSelection(spinnerPosition);
			   ((EditText)findViewById(R.id.wage_otr)).setVisibility(cf.v1.VISIBLE);
			   ((EditText)findViewById(R.id.wage_otr)).setText(otheryear[1]);			   
		   }
		   else
		   {
			   int spinnerPosition = wageadap.getPosition(cf.YearPH);
			   wagespin.setSelection(spinnerPosition);					  
			   ((EditText)findViewById(R.id.wage_otr)).setVisibility(cf.v1.GONE);
		   }
	}
	private void defaultage()
	{
		if(cf.YearPH.contains("Other"))
		{
			  String otheryear[] = cf.YearPH.split("&#126;");
			  int spinnerPosition1 = ageadap.getPosition(otheryear[0]);
			  agespin.setSelection(spinnerPosition1);
			  ((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.VISIBLE);
			  ((EditText)findViewById(R.id.age_otr)).setText(otheryear[1]);			   
		}
		else
		{
		     int spinnerPosition1 = ageadap.getPosition(cf.YearPH);
			 agespin.setSelection(spinnerPosition1);			  
			 ((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.GONE);
		}		
	}
	private void defaultwhage()
	{
		if(cf.YearPH.contains("Other"))
		{
			  String otheryear[] = cf.YearPH.split("&#126;");
			  int spinnerPosition2 = whtrageadap.getPosition(otheryear[0]);
			  whtragespin.setSelection(spinnerPosition2);
			  ((EditText)findViewById(R.id.whtrage_otr)).setVisibility(cf.v1.VISIBLE);
			  ((EditText)findViewById(R.id.whtrage_otr)).setText(otheryear[1]);			   
		}
		else
		{
			System.out.println("Cem YearPH"+cf.YearPH);
		     int spinnerPosition2 = whtrageadap.getPosition(cf.YearPH);
		     whtragespin.setSelection(spinnerPosition2);			  
			 ((EditText)findViewById(R.id.whtrage_otr)).setVisibility(cf.v1.GONE);System.out.println("ce,p,et");
		}		
	}
	private void declarations() {
		// TODO Auto-generated method stub
		cf.setupUI((ScrollView) findViewById(R.id.scr));
		
		/** DEFINING SUB CHECKBOX OF PLUMBING STARTS **/
			for(int i=0;i<plumchk.length;i++){
				plumchk[i] = (CheckBox)findViewById(plumchkhdrs[i]);
			}
		/** DEFINING SUB CHECKBOX OF PLUMBING ENDS **/
        
		/** DEFINING WATER SERVICE TYPE STARTS **/
			wstrdgval = (RadioGroup)findViewById(R.id.wst_rdg);
			wstrdgval.setOnCheckedChangeListener(new checklistenetr(25));
			for(int i=0;i<wptchk.length;i++){
				wptchk[i] = (CheckBox)findViewById(wpt[i]);
			}
			for(int i=0;i<wruchk.length;i++){
				wruchk[i] = (CheckBox)findViewById(wru[i]);
			}
			wagespin=(Spinner) findViewById(R.id.wage_spin);
			wageadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, cf.yearbuilt);
			wageadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			wagespin.setAdapter(wageadap);//wagespin.setSelection(wageadap.getPosition("2012"));
			wagespin.setOnItemSelectedListener(new  Spin_Selectedlistener(5));
			wyicrdgval = (RadioGroup)findViewById(R.id.wyic_rdg);
			msoprdgval = (RadioGroup)findViewById(R.id.msop_rdg);
			msoprdgval.setOnCheckedChangeListener(new checklistenetr(21));
			wlocrdgval = (RadioGroup)findViewById(R.id.wlocation_rdg);
			wlocrdgval.setOnCheckedChangeListener(new checklistenetr(22));
			wconfspin=(Spinner) findViewById(R.id.wconf_spin);
			wconfadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, wconf);
			wconfadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			wconfspin.setAdapter(wconfadap);
			wconfspin.setOnItemSelectedListener(new  Spin_Selectedlistener(6));
			wrurdgval = (RadioGroup)findViewById(R.id.wru_rdg);
			wrurdgval.setOnCheckedChangeListener(new checklistenetr(23));
			wpcrdgval = (RadioGroup)findViewById(R.id.wpc_rdg);
			wpcrdgval.setOnCheckedChangeListener(new checklistenetr(24));
			((RadioButton)wyicrdgval.findViewWithTag("Original")).setChecked(true);
			wyicrdgval.setOnCheckedChangeListener(new checklistenetr(20));
			((EditText)findViewById(R.id.wruotr)).addTextChangedListener(new textwatcher(5));
			((EditText)findViewById(R.id.watercomment)).addTextChangedListener(new textwatcher(4));
			((EditText)findViewById(R.id.wage_otr)).addTextChangedListener(new textwatcher(7));
			systemrecupdcomlin = (LinearLayout)findViewById(R.id.systemrecupdcomments);
			
			
		/** DEFINING WATER SERVICE TYPE ENDS **/
		
		/** DEFINING SEWER SERVICE TYPE STARTS **/
			strdgval = (RadioGroup)findViewById(R.id.st_rdg);
			strdgval.setOnCheckedChangeListener(new checklistenetr(26));
			for(int i=0;i<ptchk.length;i++){
				ptchk[i] = (CheckBox)findViewById(pt[i]);
			}
			
			for(int i=0;i<sewercaprdio.length;i++)
		   	 {
				sewercaprdio[i] = (RadioButton)findViewById(sewerrdioid[i]);
				sewercaprdio[i].setOnClickListener(new RadioArrayclickerSewer());	   		
		   	 }
			
			 agespin=(Spinner) findViewById(R.id.age_spin);
			 ageadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, cf.yearbuilt);
			 ageadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			 agespin.setAdapter(ageadap);//agespin.setSelection(ageadap.getPosition("2012"));
			 agespin.setOnItemSelectedListener(new  Spin_Selectedlistener(3));
			 yicrdgval = (RadioGroup)findViewById(R.id.yic_rdg);
			 yicrdgval.setOnCheckedChangeListener(new checklistenetr(14));
			/* scfrdgval = (RadioGroup)findViewById(R.id.scf_rdg);
			 scfrdgval.setOnCheckedChangeListener(new checklistenetr(15));*/
			 rurdgval = (RadioGroup)findViewById(R.id.ru_rdg);
			 rurdgval.setOnCheckedChangeListener(new checklistenetr(16));
			 pcrdgval = (RadioGroup)findViewById(R.id.pc_rdg);
			 pcrdgval.setOnCheckedChangeListener(new checklistenetr(17));
			 levrdgval = (RadioGroup)findViewById(R.id.lev_rdg);
			 levrdgval.setOnCheckedChangeListener(new checklistenetr(18));
			 sonrdgval = (RadioGroup)findViewById(R.id.son_rdg);
			 sonrdgval.setOnCheckedChangeListener(new checklistenetr(19));
			 ((EditText)findViewById(R.id.ruotr)).addTextChangedListener(new textwatcher(6));
			 ((EditText)findViewById(R.id.sewercomment)).addTextChangedListener(new textwatcher(3));
			 ((EditText)findViewById(R.id.age_otr)).addTextChangedListener(new textwatcher(8));
			 ((RadioButton)yicrdgval.findViewWithTag("Original")).setChecked(true);
		/** DEFINING SEWER SERVICE TYPE ENDS **/
			
	    /** DEFINING WATER HEATER STARTS **/
			 whtrtypspin=(Spinner) findViewById(R.id.whtrtyp_spin);
			 whtrtypadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, whtrtype);
			 whtrtypadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			 whtrtypspin.setAdapter(whtrtypadap);
			 whtrtypspin.setOnItemSelectedListener(new  Spin_Selectedlistener(7));
			 
			 whtragespin=(Spinner) findViewById(R.id.whtrage_spin);
			 whtrageadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, cf.yearbuilt);
			 whtrageadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			 whtragespin.setAdapter(whtrageadap);
			 whtragespin.setOnItemSelectedListener(new  Spin_Selectedlistener(8));
			 ((EditText)findViewById(R.id.whtrage_otr)).addTextChangedListener(new textwatcher(9));
			 
			 whtrcapspin=(Spinner) findViewById(R.id.whtrcap_spin);
			 whtrcap= new ArrayAdapter(this,android.R.layout.simple_spinner_item, whtrcapc);
			 whtrcap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			 whtrcapspin.setAdapter(whtrcap);
			 whtrcapspin.setOnItemSelectedListener(new  Spin_Selectedlistener(13));
			 ((EditText)findViewById(R.id.whtrcap_otr)).addTextChangedListener(new textwatcher(10));
			 
			 whtrlocspin=(Spinner) findViewById(R.id.whtrloc_spin);
			 whtrloc = new ArrayAdapter(this,android.R.layout.simple_spinner_item, whtrloct);
			 whtrloc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			 whtrlocspin.setAdapter(whtrloc);
			 whtrlocspin.setOnItemSelectedListener(new  Spin_Selectedlistener(14));
			 
			 whtrlofcspin=(Spinner) findViewById(R.id.whtrfinceil_spin);
			 whtrlofc = new ArrayAdapter(this,android.R.layout.simple_spinner_item, flr);
			 whtrlofc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			 whtrlofcspin.setAdapter(whtrlofc);
			 whtrlofcspin.setOnItemSelectedListener(new  Spin_Selectedlistener(9));
			 
			 whtrdppspin=(Spinner) findViewById(R.id.whtrdpp_spin);
			 whtrdpp = new ArrayAdapter(this,android.R.layout.simple_spinner_item, flr);
			 whtrdpp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			 whtrdppspin.setAdapter(whtrdpp);
			 whtrdppspin.setOnItemSelectedListener(new  Spin_Selectedlistener(10));
			 
			 whtrdlspin=(Spinner) findViewById(R.id.whtrdl_spin);
			 whtrdl = new ArrayAdapter(this,android.R.layout.simple_spinner_item, flr);
			 whtrdl.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			 whtrdlspin.setAdapter(whtrdl);
			 whtrdlspin.setOnItemSelectedListener(new  Spin_Selectedlistener(11));
			 
			 whtrlespin=(Spinner) findViewById(R.id.whtrle_spin);
			 whtrle = new ArrayAdapter(this,android.R.layout.simple_spinner_item, flr);
			 whtrle.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			 whtrlespin.setAdapter(whtrle);
			 whtrlespin.setOnItemSelectedListener(new  Spin_Selectedlistener(12));
			 
			 whtrtbl = (TableLayout)findViewById(R.id.heatervalue);
			 
	    /** DEFINING WATER HEATER ENDS **/
			 
		/** DEFINING NUMBER OF BATHROOMS STARTS **/
			 nobspin=(Spinner) findViewById(R.id.nob_spin);
			 nobadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, bval);
			 nobadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			 nobspin.setAdapter(nobadap);
			 nobspin.setOnItemSelectedListener(new  Spin_Selectedlistener(1));
			 
			 nobfcspin=(Spinner) findViewById(R.id.nobfc_spin);
			 nobfcadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, bval);
			 nobfcadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			 nobfcspin.setAdapter(nobfcadap);
			 nobfcspin.setOnItemSelectedListener(new  Spin_Selectedlistener(2));
			 
			 seprdgval = (RadioGroup)findViewById(R.id.sep_rdg);
			 seprdgval.setOnCheckedChangeListener(new checklistenetr(12));
			 ptrnrdgval = (RadioGroup)findViewById(R.id.ptrn_rdg);
			 ptrnrdgval.setOnCheckedChangeListener(new checklistenetr(13));
	    /** DEFINING NUMBER OF BATHROOMS ENDS **/
			
		/** DEFINING OTHER CONDITIONS STARTS **/
			lrprdgval = (RadioGroup)findViewById(R.id.lrp_rdg);
			lrprdgval.setOnCheckedChangeListener(new checklistenetr(2));
			dpuwrdgval = (RadioGroup)findViewById(R.id.dpuw_rdg);
			dpuwrdgval.setOnCheckedChangeListener(new checklistenetr(3));
			lsprdgval = (RadioGroup)findViewById(R.id.lsp_rdg);
			lsprdgval.setOnCheckedChangeListener(new checklistenetr(4));
			llerdgval = (RadioGroup)findViewById(R.id.le_rdg);
			llerdgval.setOnCheckedChangeListener(new checklistenetr(5));
			wafcrdgval = (RadioGroup)findViewById(R.id.wafc_rdg);
			wafcrdgval.setOnCheckedChangeListener(new checklistenetr(6));
			/*alrdgval = (RadioGroup)findViewById(R.id.al_rdg);
			alrdgval.setOnCheckedChangeListener(new checklistenetr(7));*/
			iplrdgval = (RadioGroup)findViewById(R.id.iopl_rdg);
			iplrdgval.setOnCheckedChangeListener(new checklistenetr(8));
			clcrdgval = (RadioGroup)findViewById(R.id.clc_rdg);
			clcrdgval.setOnCheckedChangeListener(new checklistenetr(9));
			whlrdgval = (RadioGroup)findViewById(R.id.whl_rdg);
			whlrdgval.setOnCheckedChangeListener(new checklistenetr(10));
			/*otrrdgval = (RadioGroup)findViewById(R.id.otr_rdg);
			otrrdgval.setOnCheckedChangeListener(new checklistenetr(11));*/
		/** DEFINING OTHER CONDITIONS ends **/
			
		/** DEFINING OVERALL PLUMBING CONDT STARTS **/
		  pscrdgval = (RadioGroup)findViewById(R.id.psc_rdg);
		  pscrdgval.setOnCheckedChangeListener(new checklistenetr(1));
		  ((EditText)findViewById(R.id.nocomment)).addTextChangedListener(new textwatcher(2));
		  flkrvspin=(Spinner) findViewById(R.id.flr_spin);
		  flkrvadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, flr);
		  flkrvadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		  flkrvspin.setAdapter(flkrvadap);
		  flkrvspin.setOnItemSelectedListener(new  Spin_Selectedlistener(4));
		  flkrvspin.setSelection(2);
		  
		/** DEFINING OVERALL PLUMBING CONDT STARTS **/
		  
	    /** PLUMBING COMMENTS STARTS **/
	       ((EditText)findViewById(R.id.plumbingcomment)).addTextChangedListener(new textwatcher(1));
	    /** PLUMBING COMMENTS ENDS **/
	}
	class  RadioArrayclickerSewer implements OnClickListener
	{
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			for(int i=0;i<sewercaprdio.length;i++)
			{
				if(v.getId()==sewercaprdio[i].getId())
				{	
					 if(sewercaprdio[sewercaprdio.length-1].isChecked())
					{
						setvisible(((EditText)findViewById(R.id.sewerother)));
					}
					else
					{
						setInVisible(((EditText)findViewById(R.id.sewerother)));
					}
					 sewercaprdio[i].setChecked(true);
				}
				else
				{
					sewercaprdio[i].setChecked(false);
					setInVisible(((EditText)findViewById(R.id.sewerother)));
				}
			}
		}
	}
	private void setvisible(EditText editText) {
		// TODO Auto-generated method stub
		editText.setVisibility(cf.v1.VISIBLE);
		cf.setFocus(editText);			
	}
	private void setInVisible(EditText editText) {
		// TODO Auto-generated method stub
		editText.setVisibility(cf.v1.GONE);
		editText.setText("");
	}
	class Spin_Selectedlistener implements OnItemSelectedListener
	{
		int i;
		public Spin_Selectedlistener(int i) {
			// TODO Auto-generated constructor stub
			this.i=i;
		}
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			if(i==1){
			    nobstr = nobspin.getSelectedItem().toString();
			}else if(i==2){
			    nobfcstr = nobfcspin.getSelectedItem().toString();
			}else if(i==3){
				agestr = agespin.getSelectedItem().toString();
				if(agestr.equals("Other")){
					((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.VISIBLE);
                     if(ssso==0){cf.setFocus(((EditText)findViewById(R.id.age_otr)));}
				}else if(agestr.equals("Unknown")){
					((EditText)findViewById(R.id.age_otr)).setText("");
					((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.GONE);
					chk_values();
					if(ssageconfig.equals(""))
					{
						((RadioButton)yicrdgval.findViewWithTag("Unknown")).setChecked(true);
					}
				}
				else{
					System.out.println("ssval="+ssval);
					
					
					if(ssval==0)
					{
						if(ssageconfig.equals(""))
						{
							((RadioButton)yicrdgval.findViewWithTag("Original")).setChecked(true);
						}
						ssval=1;	
					}
					else if(ssval==1)
					{	
						System.out.println("inside ssval");
						sschk=true;
						
						System.out.println("ssck ssval"+ssck);
						if(ssck!=1){try{if(sschk){yicrdgval.clearCheck();}}catch(Exception e){}}
					}
					
					((EditText)findViewById(R.id.age_otr)).setText("");
					((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.GONE);
				   }
			}else if(i==4){
				flkrvstr = flkrvspin.getSelectedItem().toString();
			}else if(i==5){
				wagestr = wagespin.getSelectedItem().toString();chkvl=0;
				if(wagestr.equals("Other")){
					((EditText)findViewById(R.id.wage_otr)).setVisibility(cf.v1.VISIBLE);
                     if(wsso==0){cf.setFocus(((EditText)findViewById(R.id.wage_otr)));}
				}else if(wagestr.equals("Unknown")){
					((EditText)findViewById(R.id.wage_otr)).setText("");
					((EditText)findViewById(R.id.wage_otr)).setVisibility(cf.v1.GONE);
					chk_values();
					if(wsageconfig.equals(""))
					{
						((RadioButton)wyicrdgval.findViewWithTag("Unknown")).setChecked(true);
					}
				}
				else{		
					if(wssval==0)
					{
						if(wsageconfig.equals(""))
						{
							((RadioButton)wyicrdgval.findViewWithTag("Original")).setChecked(true);
						}
						
							wssval=1;	
					}
					else if(wssval==1)
					{	
						wyicck=true;
						if(wschk!=1){try{if(wyicck){wyicrdgval.clearCheck();}}catch(Exception e){}}
					}
				
					((EditText)findViewById(R.id.wage_otr)).setText("");
					((EditText)findViewById(R.id.wage_otr)).setVisibility(cf.v1.GONE);
				   }
				/*int fv = Character.getNumericValue(wagestr.charAt(0));
				if(wagestr.equals("Other")){
					((EditText)findViewById(R.id.wage_otr)).setVisibility(cf.v1.VISIBLE);
					// cf.setFocus(((EditText)findViewById(R.id.wage_otr)));
					 wyicck=true;
					 if(wschk!=1){
					 try{if(wyicck){wyicrdgval.clearCheck();}}catch(Exception e){}}
				}else if(wagestr.equals("Unknown")){
					((EditText)findViewById(R.id.wage_otr)).setVisibility(cf.v1.GONE);
					((EditText)findViewById(R.id.wage_otr)).setText("");
					((RadioButton)wyicrdgval.findViewWithTag("Unknown")).setChecked(true);
				}else if(fv>0 && fv<3){
					wyicck=true;
					 if(wschk!=1){try{if(wyicck){wyicrdgval.clearCheck();}}catch(Exception e){}}
				}*/
			
			}else if(i==6){
				wconfstr = wconfspin.getSelectedItem().toString();
				if(wconfstr.equals("Other")){
					((EditText)findViewById(R.id.wconf_otr)).setVisibility(cf.v1.VISIBLE);
					
			}else{((EditText)findViewById(R.id.wconf_otr)).setVisibility(cf.v1.GONE);
				((EditText)findViewById(R.id.wconf_otr)).setText("");}
			}else if(i==7){
			    whtrtypstr = whtrtypspin.getSelectedItem().toString();
			    if(spunt==0){defaultspin();}
			}else if(i==8){
				whtragestr = whtragespin.getSelectedItem().toString();
				if(whtragestr.equals("Other")){
					((EditText)findViewById(R.id.whtrage_otr)).setVisibility(cf.v1.VISIBLE);
				 	cf.setFocus(((EditText)findViewById(R.id.whtrage_otr)));
					
				}else{((EditText)findViewById(R.id.whtrage_otr)).setVisibility(cf.v1.GONE);
				((EditText)findViewById(R.id.whtrage_otr)).setText("");}
			}else if(i==9){
			    whtrlofcstr = whtrlofcspin.getSelectedItem().toString();
			}else if(i==10){
			    whtrdppstr = whtrdppspin.getSelectedItem().toString();
			}else if(i==11){
			    whtrdlstr = whtrdlspin.getSelectedItem().toString();
			}else if(i==12){
			    whtrlestr = whtrlespin.getSelectedItem().toString();
			}else if(i==13){
				whtrcapstr = whtrcapspin.getSelectedItem().toString();
				if(whtrcapstr.equals("Other")){
					((EditText)findViewById(R.id.whtrcap_otr)).setVisibility(cf.v1.VISIBLE);
					((TextView)findViewById(R.id.galid)).setVisibility(cf.v1.VISIBLE);
				 	 cf.setFocus(((EditText)findViewById(R.id.whtrcap_otr)));
					
				}else{((EditText)findViewById(R.id.whtrcap_otr)).setVisibility(cf.v1.INVISIBLE);
				((TextView)findViewById(R.id.galid)).setVisibility(cf.v1.INVISIBLE);
				((EditText)findViewById(R.id.whtrcap_otr)).setText("");}
			}else if(i==14){
				whtrlocstr = whtrlocspin.getSelectedItem().toString();
				if(whtrlocstr.equals("Other")){
					((EditText)findViewById(R.id.whtrloc_otr)).setVisibility(cf.v1.VISIBLE);
				 	cf.setFocus(((EditText)findViewById(R.id.whtrloc_otr)));
					
				}else{((EditText)findViewById(R.id.whtrloc_otr)).setVisibility(cf.v1.INVISIBLE);
				((EditText)findViewById(R.id.whtrloc_otr)).setText("");}
			}
		
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	class checklistenetr implements OnCheckedChangeListener, android.widget.RadioGroup.OnCheckedChangeListener
	{
    	int i;
		public checklistenetr(int i) {
			// TODO Auto-generated constructor stub
		   this.i=i;
		}

		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			   RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
		        boolean isChecked = checkedRadioButton.isChecked();
		        if (isChecked)
		        {
		          switch (i) {
					case 1:
						pscrdtxt= checkedRadioButton.getText().toString().trim();pscck=false;
						if(pscrdtxt.equals("No-Some deficiencies observed")){
							((LinearLayout)findViewById(R.id.nocommentsshow)).setVisibility(cf.v1.VISIBLE);
						    ((EditText)findViewById(R.id.nocomment)).setText("");
						   }
						else{
							((LinearLayout)findViewById(R.id.nocommentsshow)).setVisibility(cf.v1.GONE);}
					  break;
					case 2:
						lrprdgtxt= checkedRadioButton.getText().toString().trim();lrpck=false;
						break;
					case 3:
						dpuwrdgtxt= checkedRadioButton.getText().toString().trim();dpuwck=false;
						break;
					case 4:
						lsprdgtxt= checkedRadioButton.getText().toString().trim();lspck=false;
						break;
					case 5:
						llerdgtxt= checkedRadioButton.getText().toString().trim();lleck=false;
						break;
					case 6:
						wafcrdgtxt= checkedRadioButton.getText().toString().trim();wafcck=false;
						break;
					case 7:
						alrdgtxt= checkedRadioButton.getText().toString().trim();alck=false;
						break;
					case 8:
						iplrdgtxt= checkedRadioButton.getText().toString().trim();ioplck=false;
						break;
					case 9:
						clcrdgtxt= checkedRadioButton.getText().toString().trim();clcck=false;
						break;
					case 10:
						whlrdgtxt= checkedRadioButton.getText().toString().trim();whlck=false;
						break;
					/*case 11:
						otrrdgtxt= checkedRadioButton.getText().toString().trim();otrck=false;
						if(otrrdgtxt.equals("Yes")){
							((EditText)findViewById(R.id.otrtxt)).setVisibility(cf.v1.VISIBLE);
						}else{
							((EditText)findViewById(R.id.otrtxt)).setText("");
							((EditText)findViewById(R.id.otrtxt)).setVisibility(cf.v1.GONE);
						}
						break;*/
					case 12:
						seprdtxt= checkedRadioButton.getText().toString().trim();sepck=false;
						if(seprdtxt.equals("No"))
						{
							((RadioButton)ptrnrdgval.findViewWithTag("Not Applicable")).setChecked(true);
							ptrnck=true;
						}
						else
						{
							try{if(ptrnck){ptrnrdgval.clearCheck();}}catch(Exception e){}
						}
						break;
					case 13:
						ptrnrdtxt= checkedRadioButton.getText().toString().trim();ptrnck=false;
						break;
					case 14:
						yicrdtxt= checkedRadioButton.getText().toString().trim();
						if(yicrdtxt.equals("Unknown")){ssval=1;
							
							((RadioButton)yicrdgval.findViewWithTag("Unknown")).setChecked(true);
							chk_values();
							if(ssage.equals(""))
							{
								agespin.setSelection(ageadap.getPosition("Unknown"));
							}
							else
							{
								agespin.setSelection(ageadap.getPosition(ssage)); 
							}
						}
						else
						{
							chk_values();
							if(ssage.equals(""))
							{
								
								if(yicrdtxt.equals("Estimate"))
								{
									agespin.setSelection(0);
								}
								ssval=2;
							}
							else
							{
								agespin.setSelection(ageadap.getPosition(ssage)); 
							}	
						}
						
						/*else if(yicrdtxt.equals("Known")|| yicrdtxt.equals("Estimate")){
							ifagespin.setSelection(0);
						}*/
						break;
					/*case 15:
						scfrdtxt= checkedRadioButton.getText().toString().trim();scfck=false;
						break;*/
					case 16:
						rurdtxt= checkedRadioButton.getText().toString().trim();sruck=false;
						if(rurdtxt.equals(cf.getResourcesvalue(R.string.yesb))){
							((LinearLayout)findViewById(R.id.sperlin)).setVisibility(cf.v1.VISIBLE);
							((LinearLayout)findViewById(R.id.ssrushow)).setVisibility(cf.v1.VISIBLE);
						}else{
							((EditText)findViewById(R.id.ruotr)).setText("");
							((EditText)findViewById(R.id.dlutxt)).setText("");sc=0;
							((CheckBox)findViewById(R.id.sndchk)).setChecked(false);
							((Button)findViewById(R.id.getdlup)).setEnabled(true);
							 ((TextView)findViewById(R.id.sdatid)).setVisibility(cf.v1.INVISIBLE);
							 try{if(spcck){pcrdgval.clearCheck();}}catch(Exception e){}
							((LinearLayout)findViewById(R.id.sperlin)).setVisibility(cf.v1.GONE);
							((LinearLayout)findViewById(R.id.ssrushow)).setVisibility(cf.v1.GONE);
						}
						break;
					case 17:
						pcrdtxt= checkedRadioButton.getText().toString().trim();spcck=false;
						if(pcrdtxt.equals("Yes"))
						{
							((TextView)findViewById(R.id.sdatid)).setVisibility(cf.v1.VISIBLE);
						}
						else
						{
							((TextView)findViewById(R.id.sdatid)).setVisibility(cf.v1.INVISIBLE);
						}
						break;
					case 18:
						levrdtxt= checkedRadioButton.getText().toString().trim();leck=false;
						break;
					case 19:
						sonrdtxt= checkedRadioButton.getText().toString().trim();sonck=false;
						break;
					case 20:						
						wyicrdtxt= checkedRadioButton.getText().toString().trim();
						if(wyicrdtxt.equals("Unknown")){wssval=1;
							((RadioButton)wyicrdgval.findViewWithTag("Unknown")).setChecked(true);							
							chk_values();
							if(wsage.equals(""))
							{
								wagespin.setSelection(wageadap.getPosition("Unknown"));
							}
							else
							{
								wagespin.setSelection(wageadap.getPosition(wsage)); 
							}							
						}
						else
						{
							chk_values();
							if(wsage.equals(""))
							{
								
								if(wyicrdtxt.equals("Estimate"))
								{
									wagespin.setSelection(0);
								}
								wssval=2;
							}
							else
							{
								wagespin.setSelection(wageadap.getPosition(wsage)); 
							}	
						}
							/*else if(wyicrdtxt.equals("Known")|| wyicrdtxt.equals("Estimate")){
						}
							wagespin.setSelection(0);
						}else{
							wagespin.setSelection(0);
						}*/
					    break;
					case 21:
						msoprdtxt = checkedRadioButton.getText().toString().trim();wmsp=false;
					    break;
					case 22:
						wlocrdtxt = checkedRadioButton.getText().toString().trim();wloc=false;
					    break;
					case 23:
						wrurdtxt= checkedRadioButton.getText().toString().trim();wruck=false;
						if(wrurdtxt.equals("Yes")){
							((LinearLayout)findViewById(R.id.systemrecupdcomments)).setVisibility(cf.v1.VISIBLE);	
							((LinearLayout)findViewById(R.id.perlin)).setVisibility(cf.v1.VISIBLE);
							((LinearLayout)findViewById(R.id.rushow)).setVisibility(cf.v1.VISIBLE);
						}
						else{
							if(wrurdtxt.equals("No"))
							{
								((LinearLayout)findViewById(R.id.systemrecupdcomments)).setVisibility(cf.v1.GONE);	
							}
							wpcck=true;
							((EditText)findViewById(R.id.wruotr)).setText("");
							 cf.Set_UncheckBox(wruchk, ((EditText)findViewById(R.id.wptotr)));
							 ((EditText)findViewById(R.id.wdlutxt)).setText("");wc=0;
							 ((CheckBox)findViewById(R.id.wndchk)).setChecked(false);
							 ((Button)findViewById(R.id.wgetdlup)).setEnabled(true);
							 ((TextView)findViewById(R.id.datid)).setVisibility(cf.v1.INVISIBLE);
							 try{if(wpcck){wpcrdgval.clearCheck();}}catch(Exception e){}
							((LinearLayout)findViewById(R.id.perlin)).setVisibility(cf.v1.GONE);
							((LinearLayout)findViewById(R.id.rushow)).setVisibility(cf.v1.GONE);
						}
						break;
					case 24:
						wpcrdtxt = checkedRadioButton.getText().toString().trim();wpcck=false;
						if(wpcrdtxt.equals("Yes"))
						{
							((TextView)findViewById(R.id.datid)).setVisibility(cf.v1.VISIBLE);
						}
						else
						{
							((TextView)findViewById(R.id.datid)).setVisibility(cf.v1.INVISIBLE);
						}
					    break;
					case 25:
						wstrdgtxt = checkedRadioButton.getText().toString().trim();wstck=false;
					    break;
					case 26:
						strdgtxt = checkedRadioButton.getText().toString().trim();stck=false;
					    break;
		          }
		        }
		}

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			// TODO Auto-generated method stub
			
		}

		
    }
	class textwatcher implements TextWatcher
    {
	         public int type;
	         textwatcher(int type)
	    	{
	    		this.type=type;
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {


	    		// TODO Auto-generated method stub
	    		if(this.type==1)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.plumcom_tv_type1),500);
	    		}
	    		else if(this.type==2)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.no_tv_type1),250);
	    		}
	    		else if(this.type==3)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.sewercom_tv_type1),250);
	    		}
	    		else if(this.type==4)
	    		{
	    			cf.showing_limit(s.toString(),(TextView)findViewById(R.id.watercom_tv_type1),250);
	    		}
	    		else if(this.type==5)
	    		{
	    			 if(!s.toString().equals("")){
	    			     if(Character.toString(s.toString().charAt(0)).equals("0"))
		    			 {
		    				cf.ShowToast("Approximate Percentage Update should be Greater than 0 and Less than or Equal to 100 - Water Supply Service", 0);
		    				// cf.ShowError(((EditText)findViewById(R.id.wruotr)), "Approximate Percentage Update should be Greater than 0 and Less than or Equal to 100.");
		    				 ((EditText)findViewById(R.id.wruotr)).setText("");
		    				 cf.hidekeyboard();
		    			 }
	    			 }
	    		}
	    		else if(this.type==6)
	    		{
	    			 if(!s.toString().equals("")){
	    			     if(Character.toString(s.toString().charAt(0)).equals("0"))
		    			 {
		    				 cf.ShowToast("Approximate Percentage Update should be Greater than 0 and Less than or Equal to 100 - Sewer Service", 0);
		    				 ((EditText)findViewById(R.id.ruotr)).setText("");cf.hidekeyboard();
		    			 }
	    			 }
	    		}
	    		else if(this.type==7)
	    		{
	    			 if(!s.toString().equals("")){
	    			     if(Character.toString(s.toString().charAt(0)).equals("0"))
		    			 {
		    				 cf.ShowToast("Invalid Entry! Please enter valid year.", 0);
		    				 ((EditText)findViewById(R.id.wage_otr)).setText("");cf.hidekeyboard();
		    			 }
	    			 }
	    		}
	    		else if(this.type==8)
	    		{
	    			 if(!s.toString().equals("")){
	    			     if(Character.toString(s.toString().charAt(0)).equals("0"))
		    			 {
		    				 cf.ShowToast("Invalid Entry! Please enter valid year.", 0);
		    				 ((EditText)findViewById(R.id.age_otr)).setText("");cf.hidekeyboard();
		    			 }
	    			 }
	    		}
	    		else if(this.type==9)
	    		{
	    			 if(!s.toString().equals("")){
	    			     if(Character.toString(s.toString().charAt(0)).equals("0"))
		    			 {
		    				 cf.ShowToast("Invalid Entry! Please enter valid year.", 0);
		    				 ((EditText)findViewById(R.id.whtrage_otr)).setText("");cf.hidekeyboard();
		    			 }
	    			 }
	    		}
	    		else if(this.type==10)
	    		{
	    			 if(!s.toString().equals("")){
	    			     if(Character.toString(s.toString().charAt(0)).equals("0"))
		    			 {
		    				 cf.ShowToast("Invalid Entry! Please enter valid Gallons.", 0);
		    				 ((EditText)findViewById(R.id.whtrcap_otr)).setText("");cf.hidekeyboard();
		    			 }
	    			 }
	    		}
	    		
	     }

	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,

	    			int after) {
	    		// TODO Auto-generated method stub
	     	}

	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,

	    			int count) {
	    		// TODO Auto-generated method stub
	    	}
	   }	
	public void clicker(View v)
	{
		switch(v.getId())
		{
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.wimg_clr:
			cf.ShowToast("Image cleared successfully.", 0);
			((EditText)findViewById(R.id.whi_ed)).setText("");
			((Button)findViewById(R.id.wimg_clr)).setVisibility(v.INVISIBLE);
			break;
		case R.id.img_clr:
			cf.ShowToast("Image cleared successfully.", 0);
			((EditText)findViewById(R.id.msi_ed)).setText("");
			((Button)findViewById(R.id.img_clr)).setVisibility(v.INVISIBLE);
			break;
		case R.id.loadcomments:
			/***Call for the comments***/
			 int len=((EditText)findViewById(R.id.plumbingcomment)).getText().toString().length();
			if(load_comment )
				{
					load_comment=false;
					int loc[] = new int[2];
					v.getLocationOnScreen(loc);
					Intent in1 = cf.loadComments("Overall Plumbing Comments",loc);
					in1.putExtra("length", len);
					in1.putExtra("max_length", 500);
					startActivityForResult(in1, cf.loadcomment_code);
				}
			 break;
		case R.id.wndchk:
			if(((CheckBox)findViewById(R.id.wndchk)).isChecked())
			{
				wc=1;
				((Button)findViewById(R.id.wgetdlup)).setEnabled(false);
				((EditText)findViewById(R.id.wnd_otr)).setVisibility(v.INVISIBLE);
				((EditText)findViewById(R.id.wdlutxt)).setText("Not Determined");
				((EditText)findViewById(R.id.wdlutxt)).setFocusable(false);
				((EditText)findViewById(R.id.wruotr)).setFocusable(false);
				//cf.setFocus(((EditText)findViewById(R.id.wnd_otr)));
			}else{
				wc=0;
				((Button)findViewById(R.id.wgetdlup)).setEnabled(true);
				((EditText)findViewById(R.id.wnd_otr)).setVisibility(v.GONE);
				//((EditText)findViewById(R.id.wnd_otr)).setText("");
				((EditText)findViewById(R.id.wdlutxt)).setFocusable(true);
				((EditText)findViewById(R.id.wdlutxt)).setText("");
			}
			break;
		case R.id.sndchk:
			if(((CheckBox)findViewById(R.id.sndchk)).isChecked())
			{
				sc=1;
				((Button)findViewById(R.id.getdlup)).setEnabled(false);
				((EditText)findViewById(R.id.snd_otr)).setVisibility(v.INVISIBLE);
				((EditText)findViewById(R.id.dlutxt)).setFocusable(false);
				((EditText)findViewById(R.id.dlutxt)).setText("Not Determined");
				((EditText)findViewById(R.id.ruotr)).setFocusable(false);
				cf.setFocus(((EditText)findViewById(R.id.snd_otr)));
				
			}else{
				sc=0;
				((Button)findViewById(R.id.getdlup)).setEnabled(true);
				((EditText)findViewById(R.id.snd_otr)).setVisibility(v.GONE);
				((EditText)findViewById(R.id.dlutxt)).setFocusable(true);
				//((EditText)findViewById(R.id.snd_otr)).setText("");
				((EditText)findViewById(R.id.dlutxt)).setText("");
				
			}
			break;
		case R.id.whi_pck:
			try
			{
				String s =((EditText)findViewById(R.id.whi_ed).findViewWithTag("Path")).getText().toString();
				if(s==null)
				{
					s="";
				}
				Intent in=new Intent(Plumbing.this,pickSingleimage.class);
				in.putExtra("path", s);
				in.putExtra("id", 1);
				startActivityForResult(in, pick_img_code);
			}
			catch (Exception e) {
				// TODO: handle exception
			}
			break;
		case R.id.msi_pck:
			try
			{
				String s =((EditText)findViewById(R.id.msi_ed).findViewWithTag("ModPath")).getText().toString();
				if(s==null)
				{
					s="";
				}
				Intent in=new Intent(Plumbing.this,pickSingleimage.class);
				in.putExtra("path", s);
				in.putExtra("id", 2);
				startActivityForResult(in, pick_img_code);
			}
			catch (Exception e) {
				// TODO: handle exception
			}
			break;
		case R.id.addhtr:
			if(!((CheckBox)findViewById(R.id.wh_chk)).isChecked())
			{
				whtrchkval="0";
			
				spunt=2;
				
				if(whtrtypstr.equals("--Select--"))
				{
					cf.ShowToast("Please select Water Heater Type.",0);
				}
				else
				{
					if(whtragestr.equals("--Select--"))
					{
						cf.ShowToast("Please select Age.",0);
			        }
					else
					{
						if(whtragestr.contains("Other") || whtragestr.contains("Other~"))
						{
			        		if(((EditText)findViewById(R.id.whtrage_otr)).getText().toString().trim().equals(""))
			        		{
			        			cf.ShowToast("Please enter the Other text for Age.",0);
								cf.setFocus(((EditText)findViewById(R.id.whtrage_otr)));
							}
			        		else
			        		{
								if(((EditText)findViewById(R.id.whtrage_otr)).getText().toString().length()<4)
								{
									cf.ShowToast("Invalid Entry! Please enter valid year.",0);
									((EditText)findViewById(R.id.whtrage_otr)).setText("");
									 cf.setFocus(((EditText)findViewById(R.id.whtrage_otr)));
								}
								else
								{
									if(cf.yearValidation(((EditText)findViewById(R.id.whtrage_otr)).getText().toString()).equals("true"))
									{
										cf.ShowToast("Year should not be Greater than Current year.",0);
										((EditText)findViewById(R.id.whtrage_otr)).setText("");
										cf.setFocus(((EditText)findViewById(R.id.whtrage_otr)));
									}
									else if(cf.yearValidation(((EditText)findViewById(R.id.whtrage_otr)).getText().toString()).equals("zero"))
									{
										cf.ShowToast("Please enter the valid year under Age.",0);
										((EditText)findViewById(R.id.whtrage_otr)).setText("");
										cf.setFocus(((EditText)findViewById(R.id.whtrage_otr)));
									}
									else{
										whtrcapacity();
									}
									
								}
							}
			        	}else{
		        		whtragestr =whtragestr;whtrcapacity();
		        	}
				   }
			   }
		
			}
			else
			{
				whtrchkval="1";
			}
			break;
		case R.id.htrclear:
			System.out.println("htrclear");
			whtrtypspin.setSelection(0);((EditText)findViewById(R.id.whtrage_otr)).setText("");whtragespin.setSelection(0);
			((EditText)findViewById(R.id.whtrcap_otr)).setText("");whtrcapspin.setSelection(0);
			((EditText)findViewById(R.id.whtrcap_otr)).setVisibility(cf.v1.INVISIBLE);
			((TextView)findViewById(R.id.galid)).setVisibility(cf.v1.INVISIBLE);
			((EditText)findViewById(R.id.whtrloc_otr)).setVisibility(cf.v1.INVISIBLE);
			((EditText)findViewById(R.id.whtrloc_otr)).setText("");whtrlocspin.setSelection(0);
			whtrlofcspin.setSelection(0);whtrdppspin.setSelection(0);whtrtypspin.setEnabled(true);spunt=0;
			whtrdlspin.setSelection(0);whtrlespin.setSelection(0);((EditText)findViewById(R.id.whi_ed).findViewWithTag("Path")).setText("");
			((EditText)findViewById(R.id.msi_ed).findViewWithTag("ModPath")).setText("");
			((Button)findViewById(R.id.addhtr)).setText("Save/Add");
			((Button)findViewById(R.id.wimg_clr)).setVisibility(cf.v1.INVISIBLE);
			((Button)findViewById(R.id.img_clr)).setVisibility(cf.v1.INVISIBLE);
			break;
		case R.id.wgetdlup:
			if(wc==0)
			{  
				iden=1;
				cf.showDialogDate(((EditText)findViewById(R.id.wdlutxt)));
				
			}
			else
			{
				
			}
			
			break;
		case R.id.getdlup:
			if(sc==0)
			{  
				iden=2;
				cf.showDialogDate(((EditText)findViewById(R.id.dlutxt)));
			    
			}
			else
			{
				
			}
			break;
		/*case R.id.wsssave:
			if(((CheckBox)findViewById(R.id.wss_chk)).isChecked())
			{
				wsschkval="1";
			}
			else{

				   wsschkval="0";
				   if(wstrdgtxt.equals("")){
					  cf.ShowToast("Please select the option for Water Service Type.",0);
				   }
				   else
				   {
					   wptchk_value= cf.getselected_chk(wptchk);
					   String wptypchk_valueotr=(wptchk[wptchk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.wptotr)).getText().toString():""; // append the other text value in to the selected option
					   wptchk_value+=wptypchk_valueotr;
					   if(wptchk_value.equals(""))
					   {
						   cf.ShowToast("Please select the option for Piping Type.",0);
				       }
					   else
					   {
				    	   if(wptchk[wptchk.length-1].isChecked())
				    	   {
								 if(wptypchk_valueotr.trim().equals("&#40;")){
									 cf.ShowToast("Please enter the Other text for Piping Type.",0);
									 cf.setFocus(((EditText)findViewById(R.id.wptotr)));
								 }else {
									wss_age();
								 }
						   }
				    	   else
				    	   {
				    		   wss_age();
				    	   }
			    	    }
				   }
				
				
			}
			break;
		case R.id.ssssave:
			if(((CheckBox)findViewById(R.id.sss_chk)).isChecked())
			{
				ssschkval="1";
			}
			else
			{

				   ssschkval="0";
				   if(strdgtxt.equals(""))
				   {
					  cf.ShowToast("Please select the option for Sewer Service Type.",0);
				   }
				   else
				   {
					   ptchk_value= cf.getselected_chk(ptchk);
					   String ptypchk_valueotr=(ptchk[ptchk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.ptotr)).getText().toString():""; // append the other text value in to the selected option
					   ptchk_value+=ptypchk_valueotr;
					   if(ptchk_value.equals(""))
					   {
						   cf.ShowToast("Please select the option for Piping Type.",0);
					   }
				       else
				       {
				    	   if(ptchk[ptchk.length-1].isChecked()) {
								 if(ptypchk_valueotr.trim().equals("&#40;")){
									 cf.ShowToast("Please enter the Other text for Piping Type.",0);
									 cf.setFocus(((EditText)findViewById(R.id.ptotr)));
								 }else {
									sssage();
								 }
						   }
				    	   else
				    	   {
				    		   sssage();
				    	   }
			    	    }
				       
				   }

				
				
			}
			break;
		case R.id.nobsave:
			
			if(((CheckBox)findViewById(R.id.nob_chk)).isChecked())
			{
				nobchkval="1";
			}else{

				   nobchkval="0";
				     if(nobstr.equals("--Select--"))
				     {
						 cf.ShowToast("Please select Total Number of Bathrooms.",0);
					 }
				     else
				     {
				    	 if(nobfcstr.equals("--Select--"))
				    	 {
				    		 cf.ShowToast("Please select Number of Bathrooms above a Finished Ceiling.",0);
						 }
				    	 else
				    	 {
				    		 if(seprdtxt.equals(""))
				    		 {
				    			 cf.ShowToast("Please select the option for Shower/Tub Enclosures Present.",0);
							 }
				    		 else
				    		 {
				    			 if(ptrnrdtxt.equals(""))
				    			 {
				    				 cf.ShowToast("Please select the option for Possible Tile Repairs Needed.",0);
								 }
				    			 else
				    			 {
				    				 NOB_Insert();
				    			 }
				    		 }
				    	 }
				     }
				  
				
				
			}
			break;
		case R.id.ocsave:
			
			if(((CheckBox)findViewById(R.id.oc_chk)).isChecked())
			{
				occhkval="1";
			}else{

				 occhkval="0";
				 if(lrprdgtxt.equals(""))
				 {
					 cf.ShowToast("Please select the option for Laundry Room Present.",0);
				 }
				 else
				 {
					 if(lsprdgtxt.equals(""))
					 {
						 cf.ShowToast("Please select the option for Laundry Sink Present.",0);
					 }
					 else
					 {
						 if(dpuwrdgtxt.equals(""))
						 {
							 cf.ShowToast("Please select the option for Drain Pan Under Washing Machine.",0);
						 }
						 else
						 {
							 if(wafcrdgtxt.equals("")){
								 cf.ShowToast("Please select the option for Washing Machine above Finished Ceiling.",0);
							 }
							 else
							 {
								 if(llerdgtxt.equals("")){
									 cf.ShowToast("Please select the option for Leakage Evident in Laundry Room.",0);
								 }
								 else
								 {
									 if(clcrdgtxt.equals("")){
										 cf.ShowToast("Please select the option for Connections/Hoses Leaking or Cracked.",0);
									 }
									 else
									 {
										 OC_Insert();
									 }
								 }
							 }
						 }
					 }
				 }
			
		
				
			}
				break;
		case R.id.odsave:
			if(((CheckBox)findViewById(R.id.od_chk)).isChecked())
			{
				odchkval="1";
			}else{

				odchkval="0";
				if(alrdgtxt.equals("")){
					cf.ShowToast("Please select the option for Damaged Fixtures Noted.",0);
				}
				else
				{
				   // alrdgtxt += odchkval;
				    if(iplrdgtxt.equals("")){
				    	cf.ShowToast("Please select the option for Indication of Prior Leak(s) Observed.",0);
				    }
				    else
				    {
				    	if(whlrdgtxt.equals(""))
				    	{
				    		cf.ShowToast("Please select the option for Water Flow Restricted.",0);
				    	}
				    	else
				    	{
				    		 if(otrrdgtxt.equals("")){
				    			 cf.ShowToast("Please select the option for Other Leakage Noted.",0);
							 }
				    		 else
				    		 {
				    			 if(otrrdgtxt.equals("Yes")){
									 if(((EditText)findViewById(R.id.otrtxt)).getText().toString().trim().equals("")){
										 cf.ShowToast("Please enter the text for Other Leakage Noted.",0);											 
										 cf.setFocus(((EditText)findViewById(R.id.otrtxt)));
									 }
									 else
									 {
										 otrrdgtxt += "&#94;"+((EditText)findViewById(R.id.otrtxt)).getText().toString();
										 OD_Insert();
									 }
								 }
				    			 else
				    			 {
				    				otrrdgtxt=otrrdgtxt;
				    				OD_Insert();
				    			 }
					    	}
					    }
					 }
				}
			
				
			}
				break;
		case R.id.copsave:
			if(((CheckBox)findViewById(R.id.cop_chk)).isChecked())
			{
				copchkval="1";
			}
			 else
			 {

				 copchkval="0";
				 if(pscrdtxt.equals("")){
					 cf.ShowToast("Please select the option for Plumbing System to be appear in good working order.",0);
				 }
				 else
				 {
					 if(pscrdtxt.equals("No-Some deficiencies observed"))
					 {
						 if(((EditText)findViewById(R.id.nocomment)).getText().toString().trim().equals(""))
						 {
							 cf.ShowToast("Please enter the Comments.",0);
							 cf.setFocus(((EditText)findViewById(R.id.nocomment)));
						 }
						 else
						 {
							 pscrdtxt += "&#94;"+((EditText)findViewById(R.id.nocomment)).getText().toString();
							 if(flkrvstr.equals("--Select--"))
							 {
								 cf.ShowToast("Please select Full Leakage Review Part Of Inspection.",0);
						     }
							 else
							 {
								 COP_Insert();	
							 }
						 }
					 }
					 else
					 {
						 pscrdtxt=pscrdtxt;
						 if(flkrvstr.equals("--Select--"))
						 {
							 cf.ShowToast("Please select Full Leakage Review Part Of Inspection.",0);
					     }
						 else
						 {
							 COP_Insert();	
						 }
					 }
				 }
		 
				
			 }
			break;*/
		case R.id.save:
			showstr="";
			
				if(((CheckBox)findViewById(R.id.wss_chk)).isChecked())
				{
					wsschkval="1";ssschk();   
				}
				else{

					   wsschkval="0";
					   if(wstrdgtxt.equals("")){
						  cf.ShowToast("Please select the option for Water Service Type under Water Supply Service.",0);
					   }
					   else
					   {
						   wptchk_value= cf.getselected_chk(wptchk);
						   String wptypchk_valueotr=(wptchk[wptchk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.wptotr)).getText().toString():""; // append the other text value in to the selected option
						   wptchk_value+=wptypchk_valueotr;
						   if(wptchk_value.equals(""))
						   {
							   cf.ShowToast("Please select the option for Piping Type under Water Supply Service.",0);
					       }
						   else
						   {
					    	   if(wptchk[wptchk.length-1].isChecked())
					    	   {
									 if(wptypchk_valueotr.trim().equals("&#40;")){
										 cf.ShowToast("Please enter the Other text for Piping Type under Water Supply Service.",0);
										 cf.setFocus(((EditText)findViewById(R.id.wptotr)));
									 }else {
										wss_age();
									 }
							   }
					    	   else
					    	   {
					    		   wss_age();
					    	   }
				    	    }
					   }
					
					
				}								
			
		    break;
		/*case R.id.save:
			showstr="";
			if(((CheckBox)findViewById(R.id.wss_chk)).isChecked())
			{
				ssschk();   
			}else{
				 chk_values();
				    if(retwstypstr.equals(""))
				    {
					   cf.ShowToast("Please save the option under Water Supply Service.",0);
					   cf.setRowFocus(((LinearLayout)findViewById(R.id.rdwst)));
					}
				    else
				    {
					  ssschk();
					}
				
			}
		    break;*/
		 case R.id.wpt_chk7://** WATER PIPING TYPE CHECKBOX OTHER **//*
			  if(wptchk[wptchk.length-1].isChecked())
			  { 
				  ((EditText)findViewById(R.id.wptotr)).setVisibility(v.VISIBLE);
				  cf.setFocus(((EditText)findViewById(R.id.wptotr)));
			
			  }
			  else
			  {
				  cf.clearother(((EditText)findViewById(R.id.wptotr)));
			  }
			  break;
			  
		 case R.id.wpt_chk6://** WATER PIPING TYPE CHECKBOX OTHER **//*
			  if(((CheckBox)findViewById(R.id.wpt_chk6)).isChecked())
			  { 
				  ((LinearLayout)findViewById(R.id.waterflowrest)).setVisibility(v.VISIBLE);				  
			
			  }
			  else
			  {
				  ((LinearLayout)findViewById(R.id.waterflowrest)).setVisibility(v.GONE);
			  }
			  break;
		 case R.id.pt_chk9:/** PIPING TYPE CHECKBOX OTHER **/
			  if(ptchk[ptchk.length-1].isChecked())
			  { 
				  ((EditText)findViewById(R.id.ptotr)).setVisibility(v.VISIBLE);
				   cf.setFocus(((EditText)findViewById(R.id.ptotr)));
			  }
			  else
			  {
				  cf.clearother(((EditText)findViewById(R.id.ptotr))); 
				
			  }
			  break;
		case R.id.wss_chk: /** WATER SUPPLY SERVICE CHECKBOX **/
		//	System.out.println("check box checked test="+((CheckBox)findViewById(R.id.wss_chk)).isChecked());
			if(((CheckBox)findViewById(R.id.wss_chk)).isChecked()){
				wschk=1;wsschkval="1";
				if(wss_sve){chk_values();}
				 if(!retwstypstr.equals("")){
				     showunchkalert(1); 
				 }else{
					 clearwss();
					 wsschkval="1";
					 /*try
					 {
						 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set wsinc='"+wsschkval+"' where fld_srid='"+cf.selectedhomeid+"'");
					 }
					 catch (Exception E)
					 {
						 String strerrorlog="Updating the WATER SUPPLY SERVICE PLUMBING - FourPoint";
						 cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
					 }	*/
					 ((LinearLayout)findViewById(R.id.wsslin)).setVisibility(cf.v1.GONE);
					 //((TextView)findViewById(R.id.savetxtwss)).setVisibility(cf.v1.VISIBLE);
					 ((ImageView)findViewById(R.id.shwwss)).setVisibility(cf.v1.VISIBLE);
	 			     ((ImageView)findViewById(R.id.hdwss)).setVisibility(cf.v1.GONE);
	 			     ((ImageView)findViewById(R.id.shwwss)).setEnabled(false);
				}
			}else{
				 hideotherlayouts();wsschkval="0";clearwss();
				/* try
				 {
					 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set wsinc='"+2+"' where fld_srid='"+cf.selectedhomeid+"'");
				 }
				 catch (Exception E)
				 {
					 String strerrorlog="Updating the WATER SUPPLY SERVICE PLUMBING - FourPoint";
					 cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				 }*/
				 ((ImageView)findViewById(R.id.shwwss)).setVisibility(cf.v1.GONE);
			     ((ImageView)findViewById(R.id.hdwss)).setVisibility(cf.v1.VISIBLE);
			     ((ImageView)findViewById(R.id.shwwss)).setEnabled(true);
				 ((CheckBox)findViewById(R.id.wss_chk)).setChecked(false);
				 ((LinearLayout)findViewById(R.id.wsslin)).setVisibility(cf.v1.VISIBLE);
				 ((TextView)findViewById(R.id.savetxtwss)).setVisibility(cf.v1.INVISIBLE);
			}
			break;
		case R.id.sss_chk: /** SEWER SUPPLY SERVICE CHECKBOX **/
			if(((CheckBox)findViewById(R.id.sss_chk)).isChecked()){
				ssschkval="1";
				if(sss_sve){ chk_values();}
			    if(!retsstypstr.equals("")){
				    showunchkalert(2);
			    }else{
			    	clearsss();
			    	ssschkval="1";
					 /*try
					 {
						 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set ssinc='"+ssschkval+"' where fld_srid='"+cf.selectedhomeid+"'");
					 }
					 catch (Exception E)
					 {
						 String strerrorlog="Updating the SEWER SUPPLY SERVICE PLUMBING - FourPoint";
						 cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
					 }	*/
			    	((LinearLayout)findViewById(R.id.ssslin)).setVisibility(cf.v1.GONE);
			    	//((TextView)findViewById(R.id.savetxtsss)).setVisibility(cf.v1.VISIBLE);
			    	((ImageView)findViewById(R.id.shwsss)).setVisibility(cf.v1.VISIBLE);
				    ((ImageView)findViewById(R.id.hdsss)).setVisibility(cf.v1.GONE);
				    ((ImageView)findViewById(R.id.shwsss)).setEnabled(false);
			    }
			}else{
				hideotherlayouts();ssschkval="0";clearsss();
				/*try
				 {
					 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set ssinc='"+2+"' where fld_srid='"+cf.selectedhomeid+"'");
				 }
				 catch (Exception E)
				 {
					 String strerrorlog="Updating the SEWER SUPPLY SERVICE PLUMBING - FourPoint";
					 cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				 }*/
				 ((ImageView)findViewById(R.id.shwsss)).setVisibility(cf.v1.GONE);
			     ((ImageView)findViewById(R.id.hdsss)).setVisibility(cf.v1.VISIBLE);
			     ((ImageView)findViewById(R.id.shwsss)).setEnabled(true);
				 ((CheckBox)findViewById(R.id.sss_chk)).setChecked(false);
				 ((LinearLayout)findViewById(R.id.ssslin)).setVisibility(cf.v1.VISIBLE);
				 ((TextView)findViewById(R.id.savetxtsss)).setVisibility(cf.v1.INVISIBLE);
			}
			break;
		case R.id.wh_chk: /** WATER HEATERS CHECKBOX **/
			if(((CheckBox)findViewById(R.id.wh_chk)).isChecked()){
			     whtrchkval="1";
			     if(wh_sve){ chk_values();}
			     System.out.println("coun= "+plumwh_save.getCount());
			    if(plumwh_save.getCount()!=0){
			    	showunchkalert(3);
			    }else{
			    	clearWH();
			    	whtrchkval="1";
					 /*try
					 {
						 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set whinc='"+whtrchkval+"' where fld_srid='"+cf.selectedhomeid+"'");
					 }
					 catch (Exception E)
					 {
						 String strerrorlog="Updating the WATER HEATER PLUMBING - FourPoint";
						 cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
					 }	*/
			       ((LinearLayout)findViewById(R.id.whlin)).setVisibility(cf.v1.GONE);
			      // ((TextView)findViewById(R.id.savetxtwh)).setVisibility(cf.v1.VISIBLE);
			       ((ImageView)findViewById(R.id.shwwh)).setVisibility(cf.v1.VISIBLE);
			       ((ImageView)findViewById(R.id.hdwh)).setVisibility(cf.v1.GONE);
			       ((ImageView)findViewById(R.id.shwwh)).setEnabled(false);
			    }
			}else{
				hideotherlayouts();whtrchkval="0";clearWH();
				/*try
				 {
					 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set whinc='"+2+"' where fld_srid='"+cf.selectedhomeid+"'");
				 }
				 catch (Exception E)
				 {
					 String strerrorlog="Updating the WATER HEATER PLUMBING - FourPoint";
					 cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				 }	*/
				((ImageView)findViewById(R.id.shwwh)).setVisibility(cf.v1.GONE);
			    ((ImageView)findViewById(R.id.hdwh)).setVisibility(cf.v1.VISIBLE);
			    ((ImageView)findViewById(R.id.shwwh)).setEnabled(true);
				((CheckBox)findViewById(R.id.wh_chk)).setChecked(false);
				((LinearLayout)findViewById(R.id.whlin)).setVisibility(cf.v1.VISIBLE);
				((TextView)findViewById(R.id.savetxtwh)).setVisibility(cf.v1.INVISIBLE);
				
			}
			break;
		case R.id.nob_chk: /** NUMBER OF BATHROOMS CHECKBOX **/
			if(((CheckBox)findViewById(R.id.nob_chk)).isChecked()){
				nobchkval="1";
				if(nob_sve){chk_values();}
				if(!retnobstr.equals("")){
					 showunchkalert(4);
				}else{
					clearbrm();
					nobchkval="1";
					 /*try
					 {
						 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set nobinc='"+nobchkval+"' where fld_srid='"+cf.selectedhomeid+"'");
					 }
					 catch (Exception E)
					 {
						 String strerrorlog="Updating the BATHROOMS PLUMBING - FourPoint";
						 cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
					 }	*/
				    ((LinearLayout)findViewById(R.id.noblin)).setVisibility(cf.v1.GONE);
				    //((TextView)findViewById(R.id.savetxtnob)).setVisibility(cf.v1.VISIBLE);
				    ((ImageView)findViewById(R.id.shwnob)).setVisibility(cf.v1.VISIBLE);
 			    	((ImageView)findViewById(R.id.hdnob)).setVisibility(cf.v1.GONE);
 			    	((ImageView)findViewById(R.id.shwnob)).setEnabled(false);
				}
			}else{
				
				hideotherlayouts();nobchkval="0";clearbrm();
				/*try
				 {
					 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set nobinc='"+2+"' where fld_srid='"+cf.selectedhomeid+"'");
				 }
				 catch (Exception E)
				 {
					 String strerrorlog="Updating the BATHROOMS PLUMBING - FourPoint";
					 cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				 }	*/
				((ImageView)findViewById(R.id.shwnob)).setVisibility(cf.v1.GONE);
			    ((ImageView)findViewById(R.id.hdnob)).setVisibility(cf.v1.VISIBLE);
			    ((ImageView)findViewById(R.id.shwnob)).setEnabled(true);
				((CheckBox)findViewById(R.id.nob_chk)).setChecked(false);
				((LinearLayout)findViewById(R.id.noblin)).setVisibility(cf.v1.VISIBLE);
				((TextView)findViewById(R.id.savetxtnob)).setVisibility(cf.v1.INVISIBLE);
			}
			break;
		case R.id.oc_chk: /** LAUNDRY ROOM CHECKBOX **/
			if(((CheckBox)findViewById(R.id.oc_chk)).isChecked()){
				occhkval="1";
				if(lr_sve){chk_values();}
				if(!retlrprdtxt.equals("")){
					 showunchkalert(5);
				}else{
					clearlr();
					occhkval="1";
					/* try
					 {
						 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set ocinc='"+occhkval+"' where fld_srid='"+cf.selectedhomeid+"'");
					 }
					 catch (Exception E)
					 {
						 String strerrorlog="Updating the LAUNDRY PLUMBING - FourPoint";
						 cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
					 }	*/
				    ((LinearLayout)findViewById(R.id.oclin)).setVisibility(cf.v1.GONE);
				   // ((TextView)findViewById(R.id.savetxtoc)).setVisibility(cf.v1.VISIBLE);
				    ((ImageView)findViewById(R.id.shwlr)).setVisibility(cf.v1.VISIBLE);
 			    	((ImageView)findViewById(R.id.hdlr)).setVisibility(cf.v1.GONE);
 			    	((ImageView)findViewById(R.id.shwlr)).setEnabled(false);
				}

			}else{
				hideotherlayouts();occhkval="0";clearlr();
				/*try
				 {
					 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set ocinc='"+2+"' where fld_srid='"+cf.selectedhomeid+"'");
				 }
				 catch (Exception E)
				 {
					 String strerrorlog="Updating the LAUNDRY PLUMBING - FourPoint";
					 cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				 }	*/
				((ImageView)findViewById(R.id.shwlr)).setVisibility(cf.v1.GONE);
			    ((ImageView)findViewById(R.id.hdlr)).setVisibility(cf.v1.VISIBLE);
			    ((ImageView)findViewById(R.id.shwlr)).setEnabled(true);
				((CheckBox)findViewById(R.id.oc_chk)).setChecked(false);
				((LinearLayout)findViewById(R.id.oclin)).setVisibility(cf.v1.VISIBLE);
				((TextView)findViewById(R.id.savetxtoc)).setVisibility(cf.v1.INVISIBLE);
			}
			break;
		
/*		case R.id.od_chk: *//** OTHER CONDITIONS CHECKBOX **//*
			if(((CheckBox)findViewById(R.id.od_chk)).isChecked()){
				odinc=1;odchkval="1";
				if(od_sve){chk_values();}
				if(!retodrdtxt.equals("")){
					 showunchkalert(6);
				}else{
					cleardef();
					odchkval="1";
					 try
					 {
						 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set odinc='"+odchkval+"' where fld_srid='"+cf.selectedhomeid+"'");
					 }
					 catch (Exception E)
					 {
						 String strerrorlog="Updating the DEFICIENCUY PLUMBING - FourPoint";
						 cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
					 }	
				    ((LinearLayout)findViewById(R.id.odlin)).setVisibility(cf.v1.GONE);
				   // ((TextView)findViewById(R.id.savetxtod)).setVisibility(cf.v1.VISIBLE);
				    ((ImageView)findViewById(R.id.shwod)).setVisibility(cf.v1.VISIBLE);
 			    	((ImageView)findViewById(R.id.hdod)).setVisibility(cf.v1.GONE);
 			    	((ImageView)findViewById(R.id.shwod)).setEnabled(false);
				}
			}else{
				odinc=0;
				hideotherlayouts();odchkval="0";cleardef();
				 try
				 {
					 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set odinc='"+2+"' where fld_srid='"+cf.selectedhomeid+"'");
				 }
				 catch (Exception E)
				 {
					 String strerrorlog="Updating the DEFICIENCUY PLUMBING - FourPoint";
					 cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				 }	
				 ((ImageView)findViewById(R.id.shwod)).setVisibility(cf.v1.GONE);
			     ((ImageView)findViewById(R.id.hdod)).setVisibility(cf.v1.VISIBLE);
			     ((ImageView)findViewById(R.id.shwod)).setEnabled(true);
				((CheckBox)findViewById(R.id.od_chk)).setChecked(false);
				((LinearLayout)findViewById(R.id.odlin)).setVisibility(cf.v1.VISIBLE);
				((TextView)findViewById(R.id.savetxtod)).setVisibility(cf.v1.INVISIBLE);
			}
			break;*/
		case R.id.cop_chk: /** CONDITIONS OF PLUMBING SERVICES CHECKBOX **/
			
			if(((CheckBox)findViewById(R.id.cop_chk)).isChecked()){
				if(con_sve){chk_values();}
				copchkval="1";
				if(!retpscrdtxt.equals("")){
					
					showunchkalert(7);
					
				}else{
					
					clearcon();
					copchkval="1";
					/* try
					 {
						 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set ovrplucondinc='"+copchkval+"' where fld_srid='"+cf.selectedhomeid+"'");
					 }
					 catch (Exception E)
					 {
						 String strerrorlog="Updating the CONDIITON PLUMBING - FourPoint";
						 cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
					 }	*/
					((LinearLayout)findViewById(R.id.coplin)).setVisibility(cf.v1.GONE);
				    //((TextView)findViewById(R.id.savetxtcop)).setVisibility(cf.v1.VISIBLE);
				    ((ImageView)findViewById(R.id.shwcon)).setVisibility(cf.v1.VISIBLE);
 			    	((ImageView)findViewById(R.id.hdcon)).setVisibility(cf.v1.GONE);
 			    	((ImageView)findViewById(R.id.shwcon)).setEnabled(false);
				}
			}else{
				hideotherlayouts();copchkval="0";clearcon();
				/*try
				 {
					 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set ovrplucondinc='"+2+"' where fld_srid='"+cf.selectedhomeid+"'");
				 }
				 catch (Exception E)
				 {
					 String strerrorlog="Updating the CONDIITON PLUMBING - FourPoint";
					 cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				 }	*/
				((ImageView)findViewById(R.id.shwcon)).setVisibility(cf.v1.GONE);
			     ((ImageView)findViewById(R.id.hdcon)).setVisibility(cf.v1.VISIBLE);
			     ((ImageView)findViewById(R.id.shwcon)).setEnabled(true);
				((CheckBox)findViewById(R.id.cop_chk)).setChecked(false);
				((LinearLayout)findViewById(R.id.coplin)).setVisibility(cf.v1.VISIBLE);
				clearcon();
				((TextView)findViewById(R.id.savetxtcop)).setVisibility(cf.v1.INVISIBLE);
			}
			break;
		case R.id.shwwss:
			if(!((CheckBox)findViewById(R.id.wss_chk)).isChecked()){
				hideotherlayouts();
				((CheckBox)findViewById(R.id.wss_chk)).setChecked(false);
				((LinearLayout)findViewById(R.id.wsslin)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdwss)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.shwwss)).setVisibility(cf.v1.GONE);
			}
			break;
		case R.id.hdwss:
			((ImageView)findViewById(R.id.shwwss)).setVisibility(cf.v1.VISIBLE);
			((ImageView)findViewById(R.id.hdwss)).setVisibility(cf.v1.GONE);
			((LinearLayout)findViewById(R.id.wsslin)).setVisibility(cf.v1.GONE);
			break;
		case R.id.shwsss:
			chk_values();
			if(ssage.equals(""))
			{
				ssval=0;
			}
			if(!((CheckBox)findViewById(R.id.sss_chk)).isChecked()){
				hideotherlayouts();
				((CheckBox)findViewById(R.id.sss_chk)).setChecked(false);
				((LinearLayout)findViewById(R.id.ssslin)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdsss)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.shwsss)).setVisibility(cf.v1.GONE);
			}
			break;
		case R.id.hdsss:
			((ImageView)findViewById(R.id.shwsss)).setVisibility(cf.v1.VISIBLE);
			((ImageView)findViewById(R.id.hdsss)).setVisibility(cf.v1.GONE);
			((LinearLayout)findViewById(R.id.ssslin)).setVisibility(cf.v1.GONE);
			break;
		case R.id.shwwh:
			if(!((CheckBox)findViewById(R.id.wh_chk)).isChecked()){
				hideotherlayouts();
				((CheckBox)findViewById(R.id.wh_chk)).setChecked(false);
				((LinearLayout)findViewById(R.id.whlin)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdwh)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.shwwh)).setVisibility(cf.v1.GONE);
			}
			break;
		case R.id.hdwh:
			((ImageView)findViewById(R.id.shwwh)).setVisibility(cf.v1.VISIBLE);
			((ImageView)findViewById(R.id.hdwh)).setVisibility(cf.v1.GONE);
			((LinearLayout)findViewById(R.id.whlin)).setVisibility(cf.v1.GONE);
			break;
		case R.id.shwnob:
			if(!((CheckBox)findViewById(R.id.nob_chk)).isChecked()){
				hideotherlayouts();
				((CheckBox)findViewById(R.id.nob_chk)).setChecked(false);
				((LinearLayout)findViewById(R.id.noblin)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdnob)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.shwnob)).setVisibility(cf.v1.GONE);
			}
			break;
		case R.id.hdnob:
			((ImageView)findViewById(R.id.shwnob)).setVisibility(cf.v1.VISIBLE);
			((ImageView)findViewById(R.id.hdnob)).setVisibility(cf.v1.GONE);
			((LinearLayout)findViewById(R.id.noblin)).setVisibility(cf.v1.GONE);
			break;
		case R.id.shwlr:
			if(!((CheckBox)findViewById(R.id.oc_chk)).isChecked()){
				hideotherlayouts();
				((CheckBox)findViewById(R.id.oc_chk)).setChecked(false);
				((LinearLayout)findViewById(R.id.oclin)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdlr)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.shwlr)).setVisibility(cf.v1.GONE);
			}
			break;
		case R.id.hdlr:
			((ImageView)findViewById(R.id.shwlr)).setVisibility(cf.v1.VISIBLE);
			((ImageView)findViewById(R.id.hdlr)).setVisibility(cf.v1.GONE);
			((LinearLayout)findViewById(R.id.oclin)).setVisibility(cf.v1.GONE);
			break;
	/*	case R.id.shwod:
			if(!((CheckBox)findViewById(R.id.od_chk)).isChecked()){
				hideotherlayouts();
				((CheckBox)findViewById(R.id.od_chk)).setChecked(false);
				((LinearLayout)findViewById(R.id.odlin)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdod)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.shwod)).setVisibility(cf.v1.GONE);
			}
			break;
		case R.id.hdod:
			((ImageView)findViewById(R.id.shwod)).setVisibility(cf.v1.VISIBLE);
			((ImageView)findViewById(R.id.hdod)).setVisibility(cf.v1.GONE);
			((LinearLayout)findViewById(R.id.odlin)).setVisibility(cf.v1.GONE);
			break;*/
		case R.id.shwcon:
			if(!((CheckBox)findViewById(R.id.cop_chk)).isChecked()){
				hideotherlayouts();
				((CheckBox)findViewById(R.id.cop_chk)).setChecked(false);
				((LinearLayout)findViewById(R.id.coplin)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.hdcon)).setVisibility(cf.v1.VISIBLE);
				((ImageView)findViewById(R.id.shwcon)).setVisibility(cf.v1.GONE);
			}
			break;
		case R.id.hdcon:
			((ImageView)findViewById(R.id.shwcon)).setVisibility(cf.v1.VISIBLE);
			((ImageView)findViewById(R.id.hdcon)).setVisibility(cf.v1.GONE);
			((LinearLayout)findViewById(R.id.coplin)).setVisibility(cf.v1.GONE);
			break;
		}
	
	}
	
	private void whtrcapacity() {
		// TODO Auto-generated method stub
		  if(whtrcapstr.equals("--Select--"))
		  {
				cf.ShowToast("Please select Water Heater Capacity under Water Heaters.",0);
		  }
		  else
		  {
			    if(whtrcapstr.contains("Other") || whtrcapstr.contains("Other~"))
			    {
			    	if(((EditText)findViewById(R.id.whtrcap_otr)).getText().toString().trim().equals("")){
			    		cf.ShowToast("Please enter the Other text for Water Heater Capacity under Water Heaters.",0);
						cf.setFocus(((EditText)findViewById(R.id.whtrcap_otr)));
					}else{
						if(Integer.parseInt(((EditText)findViewById(R.id.whtrcap_otr)).getText().toString())<=0 || Integer.parseInt(((EditText)findViewById(R.id.whtrcap_otr)).getText().toString())>100)
						{
							cf.ShowToast("Gallons should be Greater than 0 and Less than or Equal to 100 under Water Heater Capacity under Water Heaters.",0);
							cf.setFocus(((EditText)findViewById(R.id.whtrcap_otr)));
							((EditText)findViewById(R.id.whtrcap_otr)).setText("");
						}
						else{
							whtrlocation();
						}
					}
				}
			    else{
	        		whtrcapstr =whtrcapstr;
	        		whtrlocation();
	        	}
			}
	}
	private void whtrlocation() {
		// TODO Auto-generated method stub

        if(whtrlocstr.equals("--Select--"))
        {
        	cf.ShowToast("Please select Water Heater Location under Water Heaters.",0);
		}
        else
        {
		    if(whtrlocstr.contains("Other") || whtrlocstr.contains("Other~"))
		    {
		    	if(((EditText)findViewById(R.id.whtrloc_otr)).getText().toString().trim().equals(""))
		    	{
		    		cf.ShowToast("Please enter the Other text for Water Heater Location under Water Heaters.",0);
					cf.setFocus(((EditText)findViewById(R.id.whtrcap_otr)));
				}else{
					loctnceiling();
			    }
			}
		    else{
        		whtrlocstr =whtrlocstr;
        		loctnceiling();
        	}
		}
	}
	private void loctnceiling() {
		// TODO Auto-generated method stub
		 
		if(whtrlofcstr.equals("--Select--"))
		{
			cf.ShowToast("Please select Location Over Finished Ceiling under Water Heaters.",0);
		}
		else
		{
			if(whtrdppstr.equals("--Select--"))
			{
				cf.ShowToast("Please select Drain Pan Present under Water Heaters.",0);
			}
			else
			{
				if(whtrdlstr.equals("--Select--"))
				{
					cf.ShowToast("Please select Drain Line Present to Exterior under Water Heaters.",0);
				}
				else
				{
					if(whtrlestr.equals("--Select--"))
					{
						cf.ShowToast("Please select Leakage Evident under Water Heaters.",0);
					}
					else
					{
						if(((EditText)findViewById(R.id.whi_ed)).getText().toString().trim().equals(""))
						{
							cf.ShowToast("Please upload Water Heater Image under Water Heaters.",0);
							cf.setFocus(((EditText)findViewById(R.id.whi_ed)));
						}
						else
						{
							if(((EditText)findViewById(R.id.msi_ed)).getText().toString().trim().equals(""))
							{
								cf.ShowToast("Please upload Water Heater Model and Serial Number Image under Water Heaters.",0);
								cf.setFocus(((EditText)findViewById(R.id.msi_ed)));
							}
							else
							{
								if(whtragestr.contains("Other"))
								{
									whtragestr = whtragestr + "~"+((EditText)findViewById(R.id.whtrage_otr)).getText().toString();
								}
								else
								{
									whtragestr=whtragestr;
								}
								if(whtrcapstr.contains("Other")){whtrcapstr = whtrcapstr+"~"+((EditText)findViewById(R.id.whtrcap_otr)).getText().toString();}
								else{whtrcapstr=whtrcapstr;}
								if(whtrlocstr.contains("Other")){whtrlocstr = whtrlocstr+"~"+((EditText)findViewById(R.id.whtrloc_otr)).getText().toString();}
								else{whtrlocstr=whtrlocstr;}
								try
								{
									if(((Button)findViewById(R.id.addhtr)).getText().toString().equals("Update")){
										cf.arr_db.execSQL("UPDATE  "+cf.Plumbing_waterheater+" SET whtrinc='"+whtrchkval+"',"+
										           "whtrtype='"+cf.encode(whtrtypstr)+"',whtrage='"+cf.encode(whtragestr)+"',"+
												   "whtrcap='"+cf.encode(whtrcapstr)+"',whtrloct=='"+cf.encode(whtrlocstr)+"',"+
												   "whtrloc='"+cf.encode(whtrlofcstr)+"',whtrdp='"+cf.encode(whtrdppstr)+"',"+
										           "whtrdl='"+cf.encode(whtrdlstr)+"',whtrle='"+cf.encode(whtrlestr)+"',"+
												   "whtrimg='"+cf.encode(((EditText)findViewById(R.id.whi_ed)).getText().toString())+"',"+
										           "whtrmodimg='"+cf.encode(((EditText)findViewById(R.id.msi_ed)).getText().toString())+"' where pwhidId='"+Current_select_id+"' ");
									whtrtypspin.setSelection(0);((EditText)findViewById(R.id.whtrage_otr)).setText("");whtragespin.setSelection(0);
									whtrlofcspin.setSelection(0);whtrdppspin.setSelection(0);whtrtypspin.setEnabled(true);
									whtrdlspin.setSelection(0);whtrlespin.setSelection(0);
									 ((EditText)findViewById(R.id.whtrcap_otr)).setText("");whtrcapspin.setSelection(0);
									 ((EditText)findViewById(R.id.whtrloc_otr)).setText("");whtrlocspin.setSelection(0);
									((EditText)findViewById(R.id.whi_ed).findViewWithTag("Path")).setText("");
									((EditText)findViewById(R.id.msi_ed).findViewWithTag("ModPath")).setText("");
									 cf.ShowToast("Water Heater saved successfully.", 0);spunt=0;((Button)findViewById(R.id.addhtr)).setText("Save/Add");
									 ((Button)findViewById(R.id.img_clr)).setVisibility(cf.v1.INVISIBLE);
									 ((Button)findViewById(R.id.wimg_clr)).setVisibility(cf.v1.INVISIBLE);
									 ShowHeaterValue();
									}else{
										chk_values();
										boolean[] b = {false};
										/*if(plumwh_save.getCount()==0){
											
										}else{
											if(whtr_in_DB!=null)
											{
												for(int i=0;i<whtr_in_DB.length;i++)
												{
													if((whtr_in_DB[i].equals(whtrtypstr)))
													{
														b[0]=true;
													}
												}
											}
										}*/
										if(plumwh_save.getCount()==12){
											whtrtypspin.setSelection(0);((EditText)findViewById(R.id.whtrage_otr)).setText("");
											whtragespin.setSelection(0);
											 ((EditText)findViewById(R.id.whtrcap_otr)).setText("");whtrcapspin.setSelection(0);
											 ((EditText)findViewById(R.id.whtrloc_otr)).setText("");whtrlocspin.setSelection(0);
											whtrlofcspin.setSelection(0);whtrdppspin.setSelection(0);
											whtrdlspin.setSelection(0);whtrlespin.setSelection(0);
											((EditText)findViewById(R.id.whi_ed).findViewWithTag("Path")).setText("");
											((EditText)findViewById(R.id.msi_ed).findViewWithTag("ModPath")).setText("");
											cf.ShowToast("Limit Exceeds! You have already added 12 Water Heaters.", 0);
											((Button)findViewById(R.id.img_clr)).setVisibility(cf.v1.INVISIBLE);
											((Button)findViewById(R.id.wimg_clr)).setVisibility(cf.v1.INVISIBLE);
										}else{
											
											if(!b[0]){cf.arr_db.execSQL("INSERT INTO "
													+ cf.Plumbing_waterheater
													+ " (fld_srid,fld_flag,whtrinc,whtrtype,whtrage,whtrcap,whtrloct,whtrloc,whtrdp,whtrdl,whtrle,whtrimg,whtrmodimg)"
													+ "VALUES('"+cf.selectedhomeid+"','0','"+whtrchkval+"','"+cf.encode(whtrtypstr)+"',"+
													"'"+cf.encode(whtragestr)+"','"+cf.encode(whtrcapstr)+"','"+cf.encode(whtrlocstr)+"','"+cf.encode(whtrlofcstr)+"',"+
													"'"+cf.encode(whtrdppstr)+"','"+cf.encode(whtrdlstr)+"','"+cf.encode(whtrlestr)+"',"+
													"'"+cf.encode(((EditText)findViewById(R.id.whi_ed)).getText().toString())+"',"+
													"'"+cf.encode(((EditText)findViewById(R.id.msi_ed)).getText().toString())+"')");
											 cf.ShowToast("Water Heater added successfully.", 0);
											 whtrtypspin.setSelection(0);((EditText)findViewById(R.id.whtrage_otr)).setText("");
											 whtragespin.setSelection(0);
											 ((EditText)findViewById(R.id.whtrcap_otr)).setText("");whtrcapspin.setSelection(0);
											 ((EditText)findViewById(R.id.whtrloc_otr)).setText("");whtrlocspin.setSelection(0);
											 whtrlofcspin.setSelection(0);whtrdppspin.setSelection(0);
											 whtrdlspin.setSelection(0);whtrlespin.setSelection(0);
											 ((EditText)findViewById(R.id.whi_ed).findViewWithTag("Path")).setText("");
											 ((EditText)findViewById(R.id.msi_ed).findViewWithTag("ModPath")).setText("");
											 ((Button)findViewById(R.id.img_clr)).setVisibility(cf.v1.INVISIBLE);
											 ((Button)findViewById(R.id.wimg_clr)).setVisibility(cf.v1.INVISIBLE);
											}else{cf.ShowToast("Already exists! Please select a different Water Heater Type.", 0);
											whtrtypspin.setSelection(0);
											}
											
											/**include whater heater updation**/
											cf.CreateARRTable(23);
											Cursor cs=cf.SelectTablefunction(cf.Four_Electrical_Plumbing, " Where fld_srid='"+cf.selectedhomeid+"'");
											if(cs.getCount()>0)
											{
												cf.arr_db.execSQL("UPDATE  "+ cf.Four_Electrical_Plumbing+ " SET whinc='0' Where fld_srid='"+cf.selectedhomeid+"' ");
												  ((CheckBox)findViewById(R.id.wh_chk)).setChecked(false);
												  ((TextView)findViewById(R.id.savetxtwh)).setVisibility(cf.v1.VISIBLE);
												  ((LinearLayout)findViewById(R.id.whlin)).setVisibility(cf.v1.VISIBLE);
										    	   
											}
											else
											{
												cf.arr_db.execSQL("INSERT INTO "
														+ cf.Four_Electrical_Plumbing
														+ " (fld_srid,wsinc,wstyp,wptyp,wage,wyiconf,mainshut,wloc,wconf,wrupd,wrupdy,wdlupd,wpconf,"+
														  "ssinc,sstype,ptype,sapproxage,yiconf,scpfound,srupd,sdlupd,spconf,slevi,"+
												          "sonoted,sflrv,scomments,nobinc,tnob,nobfc,sencpre,ptrepned,ocinc,lrmpres,dpuwasher,lsnpre,levi,wafceil,aleak,inpleak,clkcrack,whleak,other,ovrplucondinc,ovrplucondopt,plucomments,odinc,whinc)"
														+ "VALUES('"+cf.selectedhomeid+"','"+wsschkval+"','','','','','','','','','','','','"+ssschkval+"','','','','','','','','','','','','','"+nobchkval+"','','','','','"+occhkval+"','','','','','','','','','','','"+copchkval+"','',"+
														"'','"+odchkval+"','0')");
												  ((CheckBox)findViewById(R.id.wh_chk)).setChecked(false);
												  ((TextView)findViewById(R.id.savetxtwh)).setVisibility(cf.v1.VISIBLE);
												  ((LinearLayout)findViewById(R.id.whlin)).setVisibility(cf.v1.VISIBLE);
										    	   
												
											}
											wh_sve=true;
											
											/**include whater heater updation ends**/
											ShowHeaterValue();
										}
									}
								}
								catch(Exception e){}
							  
							}
						}
					}
				}
			}
		}
	}
	private void ssschk() {
		// TODO Auto-generated method stub
		/*if(!((CheckBox)findViewById(R.id.sss_chk)).isChecked())
		{*/
			if(((CheckBox)findViewById(R.id.sss_chk)).isChecked())
			{
				ssschkval="1";waterheatervalidation();
			}
			else
			{

				   ssschkval="0";
				   if(strdgtxt.equals(""))
				   {
					  cf.ShowToast("Please select the option for Sewer Service Type under Sewer Service.",0);
				   }
				   else
				   {
					   ptchk_value= cf.getselected_chk(ptchk);
					   String ptypchk_valueotr=(ptchk[ptchk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.ptotr)).getText().toString():""; // append the other text value in to the selected option
					   ptchk_value+=ptypchk_valueotr;
					   if(ptchk_value.equals(""))
					   {
						   cf.ShowToast("Please select the option for Piping Type under Sewer Service.",0);
					   }
				       else
				       {
				    	   if(ptchk[ptchk.length-1].isChecked()) {
								 if(ptypchk_valueotr.trim().equals("&#40;")){
									 cf.ShowToast("Please enter the Other text for Piping Type under Sewer Service.",0);
									 cf.setFocus(((EditText)findViewById(R.id.ptotr)));
								 }else {
									sssage();
								 }
						   }
				    	   else
				    	   {
				    		   sssage();
				    	   }
			    	    }
				       
				   }

				
				
			}
		/*}
		else
		{
			wh();
		}*/
	}
	private void wh() {
		// TODO Auto-generated method stub
		if(!((CheckBox)findViewById(R.id.wh_chk)).isChecked())
		{
			    chk_values();
			    if(plumwh_save.getCount()==0)
			    {
			    	cf.ShowToast("Please add atleast one Water Heater.",0);
				}
			    else
			    {
				 bathroomsvalidation();
				}
			    
		}else{
			bathroomsvalidation();
		}
		
	}
	private void broom() {
		// TODO Auto-generated method stub
		if(!((CheckBox)findViewById(R.id.nob_chk)).isChecked())
		{
			    chk_values();
			    if(retnobstr.equals(""))
			    {
			    	cf.ShowToast("Please save the options under Bathrooms.",0);
				} 
			    else
				{
				  lroom();
				}
		}
		else
		{
			lroom();
		}
	}
	private void lroom() {
		// TODO Auto-generated method stub
		if(!((CheckBox)findViewById(R.id.oc_chk)).isChecked())
		{
			    chk_values();
			    if(retlrprdtxt.equals(""))
			    {
			    	cf.ShowToast("Please save the options under Laundry Room.",0);
				}
			    else
			    {
				  odef();
				}
		}else{
			odef();
		}
	}
	private void odef() {
		// TODO Auto-generated method stub
		/*if(!((CheckBox)findViewById(R.id.od_chk)).isChecked())
		{
			    chk_values();
			    if(retodrdtxt.equals(""))
			    {
			    	cf.ShowToast("Please save the options under Other Deficiencies Noted.",0);
				}
			    else
			    {
					cond();
				}
		}else{*/
			cond();
		//}
	}
	private void cond() {
		// TODO Auto-generated method stub
		if(!((CheckBox)findViewById(R.id.cop_chk)).isChecked())
		{
			    chk_values();
			    if(retpscrdtxt.equals(""))
			    {
			    	cf.ShowToast("Please save the options under Conditional of Overall Plumbing Services.",0);
				}
			    else
			    {
				  comments();
				}
		}else{
			comments();
		}
	}
	private void comments() {
		// TODO Auto-generated method stub
		/*if(((EditText)findViewById(R.id.plumbingcomment)).getText().toString().trim().equals("")){
			cf.ShowToast("Please enter the Overall Plumbing Comments.",0);
			cf.setFocus(((EditText)findViewById(R.id.plumbingcomment)));
		}
		else
		{*/
			/*chk_values();
			if(rws==0){
				try
				{
					cf.arr_db.execSQL("INSERT INTO "
							+ cf.Four_Electrical_Plumbing
							+ " (fld_srid,wsinc,wstyp,wptyp,wage,wyiconf,mainshut,wloc,wconf,wrupd,wrupdy,wdlupd,wpconf,"+
							  "ssinc,sstype,ptype,sapproxage,yiconf,scpfound,srupd,sdlupd,spconf,slevi,"+
					          "sonoted,sflrv,scomments,nobinc,tnob,nobfc,sencpre,ptrepned,ocinc,lrmpres,dpuwasher,lsnpre,levi,wafceil,aleak,inpleak,clkcrack,whleak,other,ovrplucondinc,ovrplucondopt,plucomments,odinc,whinc)"
							+ "VALUES('"+cf.selectedhomeid+"','"+wsschkval+"','','','','','','','','','','','','"+ssschkval+"','','','','','','','','','','','','','"+nobchkval+"','','','','','"+occhkval+"','','','','','','','','','','','"+copchkval+"','',"+
							"'"+cf.encode(((EditText)findViewById(R.id.plumbingcomment)).getText().toString())+"','"+odchkval+"','"+whtrchkval+"')");
			 	}
				catch (Exception E)
				{
					String strerrorlog="Inserting the Plumbing Comments  - FourPoint";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
			}else{*/
		System.out.println("test ude");
				 if(((CheckBox)findViewById(R.id.wss_chk)).isChecked())
				 {
					 wsschkval="1";wstrdgtxt="";
					 System.out.println("UPDATE "+cf.Four_Electrical_Plumbing+ " set wsinc='"+wsschkval+"',"+
				               "wstyp='',wptyp='',whleak='"+cf.encode(whlrdgtxt)+"',"+
							   "wage='',wyiconf='',"+
				               "mainshut='',wloc='',"+
							   "wconf='',wrupd='',"+
				               "wrupdy='',"+
						       "wdlupd='',"+
				               "wpconf='' where fld_srid='"+cf.selectedhomeid+"'");
					 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set wsinc='"+wsschkval+"',"+
				               "wstyp='',wptyp='',whleak='"+cf.encode(whlrdgtxt)+"',"+
							   "wage='',wyiconf='',"+
				               "mainshut='',wloc='',"+
							   "wconf='',wrupd='',"+
				               "wrupdy='',"+
						       "wdlupd='',"+
				               "wpconf='' where fld_srid='"+cf.selectedhomeid+"'");
				 }
				 else
				 {
					 wsschkval="0";
					 System.out.println("UPDATE "+cf.Four_Electrical_Plumbing+ " set wsinc='"+wsschkval+"',"+
				               "wstyp='"+cf.encode(wstrdgtxt)+"',wptyp='"+cf.encode(wptchk_value)+"',whleak='"+cf.encode(whlrdgtxt)+"',"+
							   "wage='"+cf.encode(wagestr)+"',wyiconf='"+cf.encode(wyicrdtxt)+"',"+
				               "mainshut='"+cf.encode(msoprdtxt)+"',wloc='"+cf.encode(wlocrdtxt)+"',"+
							   "wconf='"+cf.encode(wconfstr)+"',wrupd='"+cf.encode(wrurdtxt)+"',"+
				               "wrupdy='"+cf.encode(wyruchk_value)+"',"+
						       "wdlupd='"+cf.encode(wdlupdated)+"',"+
				               "wpconf='"+cf.encode(wpcrdtxt)+"' where fld_srid='"+cf.selectedhomeid+"'");
					 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set wsinc='"+wsschkval+"',"+
				               "wstyp='"+cf.encode(wstrdgtxt)+"',wptyp='"+cf.encode(wptchk_value)+"',whleak='"+cf.encode(whlrdgtxt)+"',"+
							   "wage='"+cf.encode(wagestr)+"',wyiconf='"+cf.encode(wyicrdtxt)+"',"+
				               "mainshut='"+cf.encode(msoprdtxt)+"',wloc='"+cf.encode(wlocrdtxt)+"',"+
							   "wconf='"+cf.encode(wconfstr)+"',wrupd='"+cf.encode(wrurdtxt)+"',"+
				               "wrupdy='"+cf.encode(wyruchk_value)+"',"+
						       "wdlupd='"+cf.encode(wdlupdated)+"',"+
				               "wpconf='"+cf.encode(wpcrdtxt)+"' where fld_srid='"+cf.selectedhomeid+"'");

					 //cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set wsinc='"+wsschkval+"' where fld_srid='"+cf.selectedhomeid+"'");
				 }
				 if(((CheckBox)findViewById(R.id.sss_chk)).isChecked())
				 {
					 ssschkval="1";strdgtxt="";
					 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set ssinc='"+ssschkval+"',"+
				               "sstype='',ptype='',"+
							   "sapproxage='',yiconf='',"+
				               "scpfound='',srupd='',"+
							   "sdlupd='',"+
				               "spconf='',slevi='',"+
						       "sonoted='',"+
				               "scomments='' where fld_srid='"+cf.selectedhomeid+"'");

				 }
				 else
				 {
					 ssschkval="0";
					 if(scfrdtxt.contains("Other"))
						{
							scfrdtxt =scfrdtxt+"&#94;"+((EditText)findViewById(R.id.sewerother)).getText().toString().trim();
						}
						else
						{
							scfrdtxt =scfrdtxt;
						}

					 
					 
					 System.out.println("UPDATE "+cf.Four_Electrical_Plumbing+ " set ssinc='"+ssschkval+"',"+
				               "sstype='"+cf.encode(strdgtxt)+"',ptype='"+cf.encode(ptchk_value)+"',"+
							   "sapproxage='"+cf.encode(agestr)+"',yiconf='"+cf.encode(yicrdtxt)+"',"+
				               "scpfound='"+cf.encode(scfrdtxt)+"',srupd='"+cf.encode(rurdtxt)+"',"+
							   "sdlupd='"+cf.encode(sdlupdated)+"',"+
				               "spconf='"+cf.encode(pcrdtxt)+"',slevi='"+cf.encode(levrdtxt)+"',"+
						       "sonoted='"+cf.encode(sonrdtxt)+"',"+
				               "scomments='"+cf.encode(((EditText)findViewById(R.id.sewercomment)).getText().toString())+"' where fld_srid='"+cf.selectedhomeid+"'");
					 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set ssinc='"+ssschkval+"',"+
				               "sstype='"+cf.encode(strdgtxt)+"',ptype='"+cf.encode(ptchk_value)+"',"+
							   "sapproxage='"+cf.encode(agestr)+"',yiconf='"+cf.encode(yicrdtxt)+"',"+
				               "scpfound='"+cf.encode(scfrdtxt)+"',srupd='"+cf.encode(rurdtxt)+"',"+
							   "sdlupd='"+cf.encode(sdlupdated)+"',"+
				               "spconf='"+cf.encode(pcrdtxt)+"',slevi='"+cf.encode(levrdtxt)+"',"+
						       "sonoted='"+cf.encode(sonrdtxt)+"',"+
				               "scomments='"+cf.encode(((EditText)findViewById(R.id.sewercomment)).getText().toString())+"' where fld_srid='"+cf.selectedhomeid+"'");

					// cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set ssinc='"+ssschkval+"' where fld_srid='"+cf.selectedhomeid+"'");
				 }
				 if(((CheckBox)findViewById(R.id.wh_chk)).isChecked())
				 {
					 whtrchkval="1";
					 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set whinc='"+whtrchkval+"'  where fld_srid='"+cf.selectedhomeid+"'");
					 cf.arr_db.execSQL("Delete from "+cf.Plumbing_waterheater+" Where fld_srid='"+cf.selectedhomeid+"'");
				 }
				 else
				 {
					 whtrchkval="0";
					cf.arr_db.execSQL("UPDATE "+cf.Plumbing_waterheater+ "  SET fld_flag='0' ,whtrinc='"+whtrchkval+"' where fld_srid='"+cf.selectedhomeid+"'");
				 }
				 if(((CheckBox)findViewById(R.id.nob_chk)).isChecked())
				 {
					 nobchkval="1";nobstr="";
						cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set nobinc='"+nobchkval+"',"+
						        "tnob='',nobfc='',"+
								"sencpre='',ptrepned='' where fld_srid='"+cf.selectedhomeid+"'");
				 }
				 else
				 {
					 nobchkval="0";
					 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set nobinc='"+nobchkval+"',"+
						        "tnob='"+cf.encode(nobstr)+"',nobfc='"+cf.encode(nobfcstr)+"',"+
								"sencpre='"+cf.encode(seprdtxt)+"',ptrepned='"+cf.encode(ptrnrdtxt)+"' where fld_srid='"+cf.selectedhomeid+"'");
					 //cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set nobinc='"+nobchkval+"' where fld_srid='"+cf.selectedhomeid+"'");
				 }
				 if(((CheckBox)findViewById(R.id.oc_chk)).isChecked())
				 {
					 occhkval="1";lrprdgtxt="";
						cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set ocinc='"+occhkval+"',"+
						        "lrmpres='',dpuwasher='',"+
								"lsnpre='',levi='',wafceil='',"+
						        "clkcrack='' where fld_srid='"+cf.selectedhomeid+"'");
				 }
				 else
				 {
					 occhkval="0";
						cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set ocinc='"+occhkval+"',"+
						        "lrmpres='"+cf.encode(lrprdgtxt)+"',dpuwasher='"+cf.encode(dpuwrdgtxt)+"',"+
								"lsnpre='"+cf.encode(lsprdgtxt)+"',levi='"+cf.encode(llerdgtxt)+"',wafceil='"+cf.encode(wafcrdgtxt)+"',"+
						        "clkcrack='"+cf.encode(clcrdgtxt)+"' where fld_srid='"+cf.selectedhomeid+"'");
					 //cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set ocinc='"+occhkval+"' where fld_srid='"+cf.selectedhomeid+"'");
					 
				 }
				 
				 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set "+
					        "inpleak='"+cf.encode(iplrdgtxt)+"'"+
							" where fld_srid='"+cf.selectedhomeid+"'");
				/* if(((CheckBox)findViewById(R.id.od_chk)).isChecked())
				 {
					 odchkval="1";alrdgtxt="";odinc=1;
					 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set aleak='',"+
						        "inpleak='',"+
								"whleak='',other='',odinc='"+odinc+"' where fld_srid='"+cf.selectedhomeid+"'");
				 }
				 else
				 {
					 odchkval="0";odinc=0;
					 
						cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set aleak='"+cf.encode(alrdgtxt)+"',"+
						        "inpleak='"+cf.encode(iplrdgtxt)+"',"+
								"whleak='"+cf.encode(whlrdgtxt)+"',other='"+cf.encode(otrrdgtxt)+"',odinc='"+odinc+"' where fld_srid='"+cf.selectedhomeid+"'");
					 //cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set odinc='"+odinc+"' where fld_srid='"+cf.selectedhomeid+"'");
				 }*/
				 if(((CheckBox)findViewById(R.id.cop_chk)).isChecked())
				 {
					 copchkval="1";pscrdtxt="";
					 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set ovrplucondinc='"+copchkval+"',"+
						        "sflrv='',"+
						        "ovrplucondopt='' where fld_srid='"+cf.selectedhomeid+"'");
				 }
				 else
				 {
					 copchkval="0";
					 System.out.println("UPDATE "+cf.Four_Electrical_Plumbing+ " set ovrplucondinc='"+copchkval+"',"+
						        "sflrv='"+cf.encode(flkrvspin.getSelectedItem().toString())+"',"+
						        "ovrplucondopt='"+cf.encode(pscrdtxt)+"' where fld_srid='"+cf.selectedhomeid+"'");
					 cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set ovrplucondinc='"+copchkval+"',"+
						        "sflrv='"+cf.encode(flkrvspin.getSelectedItem().toString())+"',"+
						        "ovrplucondopt='"+cf.encode(pscrdtxt)+"' where fld_srid='"+cf.selectedhomeid+"'");
					 //cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set ovrplucondinc='"+copchkval+"' where fld_srid='"+cf.selectedhomeid+"'");
				 }					
				try
				{
				/*	Cursor c1 =cf.SelectTablefunction(cf.Plumbing_waterheater, " where fld_srid='"+cf.selectedhomeid+"'");
					
					 if(c1.getCount()>0)
					  {
						 cf.arr_db.execSQL("UPDATE  "+cf.Plumbing_waterheater+" SET fld_flag='0' Where fld_srid='"+cf.selectedhomeid+"' ");
						    
					  }
					  else
					  {
						  try
							{
								cf.arr_db.execSQL("INSERT INTO "
										+ cf.Plumbing_waterheater
										+ " (fld_srid,fld_electinc)"
										+ "VALUES('"+cf.selectedhomeid+"','"+elechkval+"')");
								
						 	}
							catch (Exception E)
							{
								String strerrorlog="Inserting the mainpanel - FourPoint";
								cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
							}
					  }*/

					//WSS_Insertval();SSS_Insertval();NOB_Insertval();OC_Insertval();COP_Insertval();OD_Insertval();
					
					cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set plucomments='"+cf.encode(((EditText)findViewById(R.id.plumbingcomment)).getText().toString())+"' where fld_srid='"+cf.selectedhomeid+"'");
				}
				catch (Exception E)
				{
					String strerrorlog="Updating the Plumbing Comments - FourPoint";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
			//}
			cf.ShowToast("Plumbing system saved successfully.", 0);cf.goclass(24);
	
		//}
	}
	private void sssage() {
		// TODO Auto-generated method stub
		   if(agestr.equals("--Select--"))
		   {
	        	cf.ShowToast("Please select the Age under Sewer Service.",0);
	       }
		   else
		   {
	        	String ageotr=((EditText)findViewById(R.id.age_otr)).getText().toString();
	        	if(agestr.contains("Other") || agestr.contains("Other&#94;")){
	        		if(ageotr.trim().equals("")){
	        			cf.ShowToast("Please enter the other text for Age under Sewer Service.",0);
						cf.setFocus(((EditText)findViewById(R.id.age_otr)));
					}
	        		else
	        		{
						if(ageotr.length()<4){
							cf.ShowToast("Invalid Year! Please enter the valid Age under Sewer Service.",0);
							cf.setFocus(((EditText)findViewById(R.id.age_otr)));
						}
						else
						{
							if(cf.yearValidation(((EditText)findViewById(R.id.age_otr)).getText().toString()).equals("true"))
							{
								cf.ShowToast("Year should not be Greater than the Current year under Sewer Service.",0);
								cf.setFocus(((EditText)findViewById(R.id.age_otr)));
							}
							else if(cf.yearValidation(((EditText)findViewById(R.id.age_otr)).getText().toString()).equals("zero"))
							{
								cf.ShowToast("Please enter the valid year under Sewer Service.",0);
								cf.setFocus(((EditText)findViewById(R.id.age_otr)));
							}
							else
							{
								agestr += "&#94;"+((EditText)findViewById(R.id.age_otr)).getText().toString();
								syic();
							}
							
						}
					}
	        	}
	        	else{
	        		agestr =agestr;
	        		syic();
	        	}
	        }
	}
	private void syic() {
		// TODO Auto-generated method stub
		 if(yicrdtxt.equals(""))
		 {
			 cf.ShowToast("Please select the option for Year Installed Confidence under Sewer Service.",0);
	     }
		 else
		 {
			 scfrdtxt=cf.getslected_radio(sewercaprdio);	
			 System.out.println("scfrdtxt="+scfrdtxt);
			 if(scfrdtxt.equals(""))
			 {
				 cf.ShowToast("Please select the option for Sewer Cap Located under Sewer Service.",0);
		     }
			 else
			 {
				 System.out.println("scfrdtxt11="+scfrdtxt);
				 if(scfrdtxt.equals("Other"))
				 {
					 System.out.println("othertext="+((EditText)findViewById(R.id.sewerother)).getText().toString());
					 if(((EditText)findViewById(R.id.sewerother)).getText().toString().trim().equals(""))
					 {
						 cf.ShowToast("Please enter the other text for Sewer Cap Located under Sewer Service.",0);
					 }					 
					 else
					 {
						 systemrecentlyupdated();	 
					 }
				 }
				 else
				 {
					 systemrecentlyupdated();
				 }
			 }
		 }
	}
	private void systemrecentlyupdated()
	{
		 if(rurdtxt.equals(""))
		 {
			 cf.ShowToast("Please select the option for System Recently Updated under Sewer Service.",0);
		 }
		 else
		 {
			 if(rurdtxt.contains("Yes")|| rurdtxt.contains("Yes~")){
				 String etrup = ((EditText)findViewById(R.id.ruotr)).getText().toString().trim();
				 if(etrup.equals(""))
				 {
					 cf.ShowToast("Please enter the Approximate Percentage Update under Sewer Service.",0);
					 cf.setFocus(((EditText)findViewById(R.id.ruotr)));
				 }
				 else
				 {
					 if(Integer.parseInt(etrup)<=0 || Integer.parseInt(etrup)>100)
					 {
						 cf.ShowToast("Approximate Percentage Update should be Greater than 0 and Less than or Equal to 100 under Sewer Service.",0);
						 ((EditText)findViewById(R.id.ruotr)).setText("");
						 cf.setFocus(((EditText)findViewById(R.id.ruotr)));
					  }
					   else
					   {
						  rurdtxt += "~"+etrup;
						  spconfirmed();
						}
						
				}
			 }
			 else
			 {
				 leakage();
			 }
		 }
	}
	private void preleakage() {
		// TODO Auto-generated method stub
		if(!((EditText)findViewById(R.id.dlutxt)).getText().toString().trim().equals("")&&!((EditText)findViewById(R.id.dlutxt)).getText().toString().equals("Not Determined"))
		{
			 if(cf.checkfortodaysdate(((EditText)findViewById(R.id.dlutxt)).getText().toString())=="false")
	         {
	        	 cf.ShowToast("Date Last Updated should not exceed current Year/Month/Date under Sewer Service.",0);
	        	 cf.setFocus(((EditText)findViewById(R.id.dlutxt)));
	       	 }
	       	 else
	       	 {
	             sdlupdated = ((EditText)findViewById(R.id.dlutxt)).getText().toString();
	             leakage();
	       	 }
		}
		else
		{
			sdlupdated = ((EditText)findViewById(R.id.dlutxt)).getText().toString();
			leakage();
		}
	}
	private void sdatelast() {
		// TODO Auto-generated method stub
		if(((CheckBox)findViewById(R.id.sndchk)).isChecked())
		{
			sdlupdated=((EditText)findViewById(R.id.dlutxt)).getText().toString();preleakage();
			/*if(((EditText)findViewById(R.id.snd_otr)).getText().toString().trim().equals("")){
				cf.ShowToast("Please enter the approximate year of Date Last Updated under Sewer Service.",0);
				cf.setFocus(((EditText)findViewById(R.id.snd_otr)));
			}
			else
			{
				if(((EditText)findViewById(R.id.snd_otr)).getText().toString().length()<4)
				{
					cf.ShowToast("Please enter the valid year of Age.",0);
					cf.setFocus(((EditText)findViewById(R.id.snd_otr)));
				}else{
					if(cf.yearValidation(((EditText)findViewById(R.id.snd_otr)).getText().toString()).equals("true"))
					{
						cf.ShowToast("The entered year should not be Greater than the Current year under Date Last Updated.",0);
						cf.setFocus(((EditText)findViewById(R.id.snd_otr)));
					}
					else if(cf.yearValidation(((EditText)findViewById(R.id.snd_otr)).getText().toString()).equals("zero"))
					{
						cf.ShowToast("Please enter the valid approximate year of Date Last Updated under Sewer Service.",0);
						cf.setFocus(((EditText)findViewById(R.id.snd_otr)));
					}
					else{
						sdlupdated = ((EditText)findViewById(R.id.snd_otr)).getText().toString();
						spconfirmed();
					}
				}
			}*/
		}
		else
		{
			 if(((EditText)findViewById(R.id.dlutxt)).getText().toString().trim().equals(""))
			 {
				 cf.ShowToast("Please select the Date Last Updated under Sewer Service.",0);
				 cf.setFocus(((EditText)findViewById(R.id.dlutxt)));
 		     }
			 else
			 {
				 if(cf.checkfortodaysdate(((EditText)findViewById(R.id.dlutxt)).getText().toString())=="false")
		         {
		        	 cf.ShowToast("Date Last Updated should not exceed current Year/Month/Date under Sewer Service.",0);
		        	 cf.setFocus(((EditText)findViewById(R.id.dlutxt)));
		       	 }
		       	 else
		       	 {
 		              sdlupdated = ((EditText)findViewById(R.id.dlutxt)).getText().toString();
 		             preleakage();
		       	 }
 		     }
		}
	}
	private void spconfirmed() {
		// TODO Auto-generated method stub
		 if(pcrdtxt.equals(""))
		 {
			 cf.ShowToast("Please select the option for Permit Confirmed under Sewer Service.",0);
	     }
		 else
		 {
			 if(pcrdtxt.equals("Yes"))
			 {
				 sdatelast();
			 }
			 else
			 {
				 preleakage();
			 }
			 
		 }
	}
	private void leakage() {
		// TODO Auto-generated method stub
		System.out.println("sdlupdated= "+sdlupdated);
		if(levrdtxt.equals(""))
		 {
			 cf.ShowToast("Please select the option for Leakage Evident under Sewer Service.",0);
	     }
		 else
		 {
			  if(sonrdtxt.equals(""))
			  {
				  cf.ShowToast("Please select the option for Sewer Odors Noted under Sewer Service.",0);
			  }
			   else
			   {
				  /* if(((EditText)findViewById(R.id.sewercomment)).getText().toString().trim().equals("")){
					   cf.ShowToast("Please enter the Comments under Sewer Service.",0);
			           cf.setFocus(((EditText)findViewById(R.id.sewercomment)));
			       }
				   else
				   {

					   waterheatervalidation();
				  }*/
				   waterheatervalidation();
			   }
		 }
	}
	private void waterheatervalidation() {
		// TODO Auto-generated method stub
		System.out.println("water heater valdi");
		if(((CheckBox)findViewById(R.id.wh_chk)).isChecked())
		{
			System.out.println("water heate checked");
			whtrchkval="1";bathroomsvalidation();
			
		}
		else
		{
			whtrchkval="0";
			
			spunt=2;
			
			Cursor whtr_retrieve= cf.SelectTablefunction(cf.Plumbing_waterheater, " Where fld_srid='"+cf.selectedhomeid+"' and fld_flag='0'");
			System.out.println("water he="+whtr_retrieve.getCount());
		    if(whtr_retrieve.getCount()>0)
			{
		    	bathroomsvalidation();
			}
		    else
		    {
		    	System.out.println("inside ettt");
		    	System.out.println("SSSS="+whtrtypspin.getSelectedItem().toString());
		    	cf.ShowToast("Please save atlease one Water Heater Type.",0);
							/*if(whtrtypspin.getSelectedItem().toString().equals("--Select--"))
							{
								cf.ShowToast("Please select Water Heater Type under Water Heaters.",0);
							}
							else
							{
								System.out.println("not sele");
								System.out.println("not whtragestr"+whtragestr);
								if(whtragestr.equals("--Select--"))
								{
									cf.ShowToast("Please select Age under Water Heaters.",0);
						        }
								else
								{
									if(whtragestr.contains("Other") || whtragestr.contains("Other~"))
									{
						        		if(((EditText)findViewById(R.id.whtrage_otr)).getText().toString().trim().equals(""))
						        		{
						        			cf.ShowToast("Please enter the Other text for Age under Water Heaters.",0);
											cf.setFocus(((EditText)findViewById(R.id.whtrage_otr)));
										}
						        		else
						        		{
											if(((EditText)findViewById(R.id.whtrage_otr)).getText().toString().length()<4)
											{
												cf.ShowToast("Invalid Entry! Please enter valid year under Water Heaters.",0);
												((EditText)findViewById(R.id.whtrage_otr)).setText("");
												 cf.setFocus(((EditText)findViewById(R.id.whtrage_otr)));
											}
											else
											{
												if(cf.yearValidation(((EditText)findViewById(R.id.whtrage_otr)).getText().toString()).equals("true"))
												{
													cf.ShowToast("Year should not be Greater than Current year under Water Heaters.",0);
													((EditText)findViewById(R.id.whtrage_otr)).setText("");
													cf.setFocus(((EditText)findViewById(R.id.whtrage_otr)));
												}
												else if(cf.yearValidation(((EditText)findViewById(R.id.whtrage_otr)).getText().toString()).equals("zero"))
												{
													cf.ShowToast("Please enter the valid year under Age under Water Heaters.",0);
													((EditText)findViewById(R.id.whtrage_otr)).setText("");
													cf.setFocus(((EditText)findViewById(R.id.whtrage_otr)));
												}
												else{
													whtrcapacity();
												}
												
											}
										}
						        	}else{
					        		whtragestr =whtragestr;whtrcapacity();
					        	}
							   }
						   }*/
		    }
		}
	}
	private void bathroomsvalidation() {
		// TODO Auto-generated method stub
		System.out.println("bathroom valdiatou");
		if(((CheckBox)findViewById(R.id.nob_chk)).isChecked())
		{
			nobchkval="1";	laundaryroomvalidation();
		}else{

			   nobchkval="0";
			     if(nobstr.equals("--Select--"))
			     {
					 cf.ShowToast("Please select Total Number of Bathrooms Under Bathrooms/Kitchen.",0);
				 }
			     else
			     {
			    	 if(nobfcstr.equals("--Select--"))
			    	 {
			    		 cf.ShowToast("Please select Number of Bathrooms above a Finished Ceiling Under Bathrooms/Kitchen.",0);
					 }
			    	 else
			    	 {
			    		 if(seprdtxt.equals(""))
			    		 {
			    			 cf.ShowToast("Please select the option for Bathroom/Kitchen Leakage Noted Under Bathrooms/Kitchen.",0);
						 }
			    		 else
			    		 {
			    			 if(ptrnrdtxt.equals(""))
			    			 {
			    				 cf.ShowToast("Please select the option for Possible Shower Enclosure/Tile Issues Noted Under Bathrooms/Kitchen.",0);
							 }
			    			 else
			    			 {
			    				 if(iplrdgtxt.equals(""))
			    				 {
			 				    	cf.ShowToast("Please select the option for Indication of Prior Leak(s) Observed Under Bathrooms/Kitchen.",0);
			 				     }
			 				     else
			 				     {
			 				    	laundaryroomvalidation();
			 				     }
			    			 }
			    		 }
			    	 }
			     }
			  
			
			
		}
	}
	private void laundaryroomvalidation() {
		// TODO Auto-generated method stub
		if(((CheckBox)findViewById(R.id.oc_chk)).isChecked())
		{
			occhkval="1";otherdefpresvaldiation();
		}else{

			 occhkval="0";
			 if(lrprdgtxt.equals(""))
			 {
				 cf.ShowToast("Please select the option for Laundry Room Present under Laundry Room.",0);
			 }
			 else
			 {
				 if(lsprdgtxt.equals(""))
				 {
					 cf.ShowToast("Please select the option for Laundry Sink Present under Laundry Room.",0);
				 }
				 else
				 {
					 if(dpuwrdgtxt.equals(""))
					 {
						 cf.ShowToast("Please select the option for Drain Pan Under Washing Machine under Laundry Room.",0);
					 }
					 else
					 {
						 if(wafcrdgtxt.equals("")){
							 cf.ShowToast("Please select the option for Washing Machine above Finished Ceiling under Laundry Room.",0);
						 }
						 else
						 {
							 if(llerdgtxt.equals("")){
								 cf.ShowToast("Please select the option for Leakage Evident in Laundry Room.",0);
							 }
							 else
							 {
								 if(clcrdgtxt.equals("")){
									 cf.ShowToast("Please select the option for Connections/Hoses Leaking or Cracked/Bulging under Laundry Room.",0);
								 }
								 else
								 {
									 otherdefpresvaldiation();
								 }
							 }
						 }
					 }
				 }
			 }
		
	
			
		}
		
	}
	private void otherdefpresvaldiation() {
		// TODO Auto-generated method stub
		Conditionalvalidation();
		/*if(((CheckBox)findViewById(R.id.od_chk)).isChecked())
		{
			odchkval="1"; Conditionalvalidation();
		}else{

			odchkval="0";
			if(alrdgtxt.equals("")){
				cf.ShowToast("Please select the option for Damaged Fixtures Noted under Other Deficiencies Noted.",0);
			}
			else
			{
			   // alrdgtxt += odchkval;
			    if(iplrdgtxt.equals("")){
			    	cf.ShowToast("Please select the option for Indication of Prior Leak(s) Observed under Other Deficiencies Noted.",0);
			    }
			    else
			    {
			    	if(whlrdgtxt.equals(""))
			    	{
			    		cf.ShowToast("Please select the option for Water Flow Restricted under Other Deficiencies Noted.",0);
			    	}
			    	else
			    	{
			    		 if(otrrdgtxt.equals("")){
			    			 cf.ShowToast("Please select the option for Other Leakage Noted under Other Deficiencies Noted.",0);
						 }
			    		 else
			    		 {
			    			 if(otrrdgtxt.equals("Yes")){
								 if(((EditText)findViewById(R.id.otrtxt)).getText().toString().trim().equals("")){
									 cf.ShowToast("Please enter the text for Other Leakage Noted.",0);											 
									 cf.setFocus(((EditText)findViewById(R.id.otrtxt)));
								 }
								 else
								 {
									 otrrdgtxt += "&#94;"+((EditText)findViewById(R.id.otrtxt)).getText().toString();
									 //OD_Insert();
									 Conditionalvalidation();
								 }
							 }
			    			 else
			    			 {
			    				otrrdgtxt=otrrdgtxt;
			    				Conditionalvalidation();
			    				//OD_Insert();
			    			 }
				    	}
				    }
				 }
			}
		
			
		}
		*/
	}
	private void Conditionalvalidation() {
		// TODO Auto-generated method stub
		if(((CheckBox)findViewById(R.id.cop_chk)).isChecked())
		{
			copchkval="1"; comments();
		}
		 else
		 {

			 copchkval="0";
			 if(pscrdtxt.equals("")){
				 cf.ShowToast("Please select the option for Plumbing System to be appear in good working order under Conditional of Overall Plumbing Services.",0);
			 }
			 else
			 {
				 if(pscrdtxt.equals("No-Some deficiencies observed"))
				 {
					 if(((EditText)findViewById(R.id.nocomment)).getText().toString().trim().equals(""))
					 {
						 cf.ShowToast("Please enter the Comments under Conditional of Overall Plumbing Services.",0);
						 cf.setFocus(((EditText)findViewById(R.id.nocomment)));
					 }
					 else
					 {
						 pscrdtxt += "&#94;"+((EditText)findViewById(R.id.nocomment)).getText().toString();
						 if(flkrvstr.equals("--Select--"))
						 {
							 cf.ShowToast("Please select Full Leakage Review Part Of Inspection under Conditional of Overall Plumbing Services.",0);
					     }
						 else
						 {
							 comments();
						 }
					 }
				 }
				 else
				 {
					 pscrdtxt=pscrdtxt;
					 if(flkrvstr.equals("--Select--"))
					 {
						 cf.ShowToast("Please select Full Leakage Review Part Of Inspection under Conditional of Overall Plumbing Services.",0);
				     }
					 else
					 {
						 comments();
					 }
				 }
			 }
	 
			
		 }
	}
	private void wss_age() {
		// TODO Auto-generated method stub
		if(wagestr.equals("--Select--"))
		{
        	cf.ShowToast("Please select the option for Age under Water Supply Service.",0);
        }
		else
		{
			if(wagestr.contains("Other") || wagestr.contains("Other&#94;"))
			{
				if(((EditText)findViewById(R.id.wage_otr)).getText().toString().trim().equals(""))
				{
					cf.ShowToast("Please enter the Other text for Age under Water Supply Service.",0);
					cf.setFocus(((EditText)findViewById(R.id.wage_otr)));
				}
				else
				{
					if(((EditText)findViewById(R.id.wage_otr)).getText().toString().length()<4){
						cf.ShowToast("Invalid Year! Please enter the valid year under Water Supply Service.", 0);
						 cf.setFocus(((EditText)findViewById(R.id.wage_otr)));
					}else{
						if(cf.yearValidation(((EditText)findViewById(R.id.wage_otr)).getText().toString()).equals("true"))
						{
							cf.ShowToast("Year should not exceed the current year under Water Supply Service.", 0);
							cf.setFocus(((EditText)findViewById(R.id.wage_otr)));
						}
						else if(cf.yearValidation(((EditText)findViewById(R.id.wage_otr)).getText().toString()).equals("zero"))
						{
							cf.ShowToast("Please enter the valid Age under Water Supply Service.", 0);
							cf.setFocus(((EditText)findViewById(R.id.wage_otr)));
						}
						else
						{
							wagestr += "&#94;"+((EditText)findViewById(R.id.wage_otr)).getText().toString();
							wyic();
						}
						
					}
				}

			}
			else
			{
				wyic();
			}

		}

	}
	private void wyic() {
		// TODO Auto-generated method stub
		System.out.println("wyicrdtxt= "+wyicrdtxt);
		 if(wyicrdtxt.equals(""))
		 {
			 cf.ShowToast("Please select the option for Year Installed Confidence under Water Supply Service.",0);
	     }
		 else
		 {
			 if(msoprdtxt.equals(""))
			 {
				 cf.ShowToast("Please select the option for Main Shut Off Present under Water Supply Service.",0);
		     }
			 else
			 {
				 System.out.println("wlocrdtxt"+wlocrdtxt);
				 if(wlocrdtxt.equals(""))
				 {
					 cf.ShowToast("Please select the option for Location under Water Supply Service.",0);
			     }
				 else
				 {
					 System.out.println("wconfstr"+wconfstr);
				        if(wconfstr.equals("--Select--"))
				        {
				        	cf.ShowToast("Please select the option for Confirmed By under Water Supply Service.",0);
				        }
				        else
				        {
				        	if(wconfstr.contains("Other") || wconfstr.contains("Other&#94;")){
				        		if(((EditText)findViewById(R.id.wconf_otr)).getText().toString().trim().equals("")){
				        			cf.ShowToast("Please enter the Other text for Confirmed By under Water Supply Service.",0);
									cf.setFocus(((EditText)findViewById(R.id.wconf_otr)));
								}
				        		else
				        		{
									wconfstr += "&#94;"+((EditText)findViewById(R.id.wconf_otr)).getText().toString();
									wsrup();
				        		}
						 	}else{
				        		wconfstr =wconfstr;
				        		wsrup();
				        	}
				        }
				 }
		     }
		 }
	}
	private void wsrup() {
		// TODO Auto-generated method stub
		  if(wrurdtxt.equals(""))
		  {
			  cf.ShowToast("Please select the option for System Recently Updated under Water Supply Service.",0);
	      }
		  else
		  {
			  if(wrurdtxt.contains("Yes")|| rurdtxt.contains("Yes~"))
			  {
					wetrup = ((EditText)findViewById(R.id.wruotr)).getText().toString().trim();
					if(wetrup.equals(""))
					{
						cf.ShowToast("Please enter the Approximate Percentage Update under Water Supply Service.",0);
						cf.setFocus(((EditText)findViewById(R.id.wruotr)));
					}
					else
					{
						if(Integer.parseInt(wetrup)<=0 || Integer.parseInt(wetrup)>100)
						{
							cf.ShowToast("Approximate Percentage Update should be Greater than 0 and Less than or Equal to 100 under Water Supply Service.",0);
							cf.setFocus(((EditText)findViewById(R.id.wruotr)));
							((EditText)findViewById(R.id.wruotr)).setText("");
						}else{
							wrurdtxt += "~"+wetrup;
							wyruchk_value = cf.getselected_chk(wruchk);
							if(wyruchk_value.equals(""))
							{
								cf.ShowToast("Please select the option for Type under Water Supply Service.",0);
			        		}else{
			        			wpconfirmed();
			        			
			        				/*if(((EditText)findViewById(R.id.wnd_otr)).getText().toString().trim().equals("")){
			        					cf.ShowToast("Please enter the approximate year of Date Last Updated under Water Supply Service.",0);
				        				cf.setFocus(((EditText)findViewById(R.id.wnd_otr)));
				        			}
			        				else
			        				{
			        					if(((EditText)findViewById(R.id.wnd_otr)).getText().toString().length()<4){
			        						cf.ShowToast("Please enter the valid year of Age.",0);
											cf.setFocus(((EditText)findViewById(R.id.wnd_otr)));
										}
			        					else
			        					{
			        						if(cf.yearValidation(((EditText)findViewById(R.id.wnd_otr)).getText().toString()).equals("true"))
											{
			        							cf.ShowToast("The entered year should not exceed the Current year under Date Last Updated.",0);
			        							cf.setFocus(((EditText)findViewById(R.id.wnd_otr)));
											}
											else if(cf.yearValidation(((EditText)findViewById(R.id.wnd_otr)).getText().toString()).equals("zero"))
											{
												cf.ShowToast("Please enter the valid approximate year of Date Last Updated under Water Supply Service.",0);
												cf.setFocus(((EditText)findViewById(R.id.wnd_otr)));
											}
											else{
												wdlupdated = ((EditText)findViewById(R.id.wnd_otr)).getText().toString();
												wpconfirmed();
											}

			        					}
			        				}
*/
			        		

			        		}
						}
						
					}
			  }
			  else
			  {
				  wsscomments();
			  }
		  }
	}
	private void wpconfirmed() {
		// TODO Auto-generated method stub
		System.out.println("wpcrdtxt"+wpcrdtxt);
		 if(wpcrdtxt.equals(""))
		 {
			 cf.ShowToast("Please select the option for Permit Confirmed under Water Supply Service.",0);
	     }
		 else
		 {
			 if(wpcrdtxt.equals("Yes"))
			 {
				 if(((CheckBox)findViewById(R.id.wndchk)).isChecked())
     			 {
     				wdlupdated=((EditText)findViewById(R.id.wdlutxt)).getText().toString();wsscomments();
     			 }
				 else
				 {
					 if(((EditText)findViewById(R.id.wdlutxt)).getText().toString().trim().equals(""))
    				 {
    					 cf.ShowToast("Please select the Date Last Updated under Water Supply Service.",0);
		 		         cf.setFocus(((EditText)findViewById(R.id.wdlutxt)));
		 		     }
    				 else
    				 {
    					 if(cf.checkfortodaysdate(((EditText)findViewById(R.id.wdlutxt)).getText().toString())=="false")
	 		        	 {
	 		        		 cf.ShowToast("Date Last Updated should not exceed current Year/Month/Date under Water Supply Service.",0);
	 		        		 cf.setFocus(((EditText)findViewById(R.id.wdlutxt)));
	 		        	 }
	 		        	 else
	 		        	 {
		 		            wdlupdated = ((EditText)findViewById(R.id.wdlutxt)).getText().toString();
		 		           prewsscomments();
	 		        	 }
    				 }
				 }
			 }
			 else
			 {
				 prewsscomments();
			 }
			 
		 }

	}
	private void prewsscomments() {
		// TODO Auto-generated method stub
		if(!((EditText)findViewById(R.id.wdlutxt)).getText().toString().trim().equals("")&&!((EditText)findViewById(R.id.wdlutxt)).getText().toString().equals("Not Determined"))
		{
			 if(cf.checkfortodaysdate(((EditText)findViewById(R.id.wdlutxt)).getText().toString())=="false")
	         {
	        	 cf.ShowToast("Date Last Updated should not exceed current Year/Month/Date under Water Supply Service.",0);
	        	 cf.setFocus(((EditText)findViewById(R.id.wdlutxt)));
	       	 }
	       	 else
	       	 {
	             wdlupdated = ((EditText)findViewById(R.id.wdlutxt)).getText().toString();
	             wsscomments();
	       	 }
		}
		else
		{
			wdlupdated = ((EditText)findViewById(R.id.wdlutxt)).getText().toString();
			wsscomments();
		}
	}
	private void wsscomments() {
		// TODO Auto-generated method stub
		if(((LinearLayout)findViewById(R.id.systemrecupdcomments)).getVisibility()==cf.v1.VISIBLE)
		{
			/*if(((EditText)findViewById(R.id.watercomment)).getText().toString().trim().equals("")){
				cf.ShowToast("Please enter the comments under Water Supply Service.",0);
	        	cf.setFocus(((EditText)findViewById(R.id.watercomment)));
	        }else{*/
	        	wstrdgtxt += "&#94;" + ((EditText)findViewById(R.id.watercomment)).getText().toString();
	        	//WSS_Insert();
	        	ssschk();
	        //}	
		}
		else
		{
			ssschk();
		}
		

	}
	
	public void defaultspin() {
		// TODO Auto-generated method stub
		System.out.println("defaulespin");
		whtragespin.setSelection(0);
		((EditText)findViewById(R.id.whtrage_otr)).setText("");
		((EditText)findViewById(R.id.whtrage_otr)).setVisibility(cf.v1.GONE);
		whtrlofcspin.setSelection(0);whtrdppspin.setSelection(0);
		whtrdlspin.setSelection(0);whtrlespin.setSelection(0);
		((EditText)findViewById(R.id.whi_ed)).setText("");
		 whtrcapspin.setSelection(0);
		 ((EditText)findViewById(R.id.whtrcap_otr)).setText("");
		 ((EditText)findViewById(R.id.whtrcap_otr)).setVisibility(cf.v1.INVISIBLE);
		 whtrlocspin.setSelection(0);
		 ((EditText)findViewById(R.id.whtrloc_otr)).setText("");
		 ((EditText)findViewById(R.id.whtrloc_otr)).setVisibility(cf.v1.INVISIBLE);
		 ((EditText)findViewById(R.id.msi_ed)).setText("");
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode==pick_img_code)
		{
			switch (resultCode) {
				case RESULT_CANCELED:
				//((EditText)findViewById(R.id.whi_ed).findViewWithTag("Path")).setText("");
				break;
				case RESULT_OK:
					cf.ShowToast("Image uploaded successfully.",0);
					if(data.getExtras().getInt("id")==1){
						((Button)findViewById(R.id.wimg_clr)).setVisibility(cf.v1.VISIBLE);
						((EditText)findViewById(R.id.whi_ed).findViewWithTag("Path")).setText(data.getExtras().getString("path").trim());
						
					}else if(data.getExtras().getInt("id")==2){
						System.out.println("insidedata");
						((Button)findViewById(R.id.img_clr)).setVisibility(cf.v1.VISIBLE);
						((EditText)findViewById(R.id.msi_ed).findViewWithTag("ModPath")).setText(data.getExtras().getString("path").trim());
					}
					
				break;
			}
		}
		else if(requestCode==cf.loadcomment_code)
		{
			load_comment=true;
			if(resultCode==RESULT_OK)
			{
				 cf.ShowToast("Comments added successfully.", 0);
				 String plumcomts = ((EditText)findViewById(R.id.plumbingcomment)).getText().toString();
				((EditText)findViewById(R.id.plumbingcomment)).setText((plumcomts+" "+data.getExtras().getString("Comments")).trim());
				
			}
	   }
	}
	/*private void OD_Insert() {
		// TODO Auto-generated method stub
		OD_Insertval();
		if(odchkval.equals("1")){ ((TextView)findViewById(R.id.savetxtod)).setVisibility(cf.v1.GONE);}
		else{ ((TextView)findViewById(R.id.savetxtod)).setVisibility(cf.v1.VISIBLE);
		cf.ShowToast("Other Deficiencies Noted saved successfully.", 0);}
		od_sve=true;
	}*/
	private void OD_Insertval() {
		// TODO Auto-generated method stub
		chk_values();
		 if(rws == 0){
			 try
				{
					cf.arr_db.execSQL("INSERT INTO "
							+ cf.Four_Electrical_Plumbing
							+ " (fld_srid,wsinc,wstyp,wptyp,wage,wyiconf,mainshut,wloc,wconf,wrupd,wrupdy,wdlupd,wpconf,"+
							  "ssinc,sstype,ptype,sapproxage,yiconf,scpfound,srupd,sdlupd,spconf,slevi,"+
						      "sonoted,sflrv,scomments,nobinc,tnob,nobfc,sencpre,ptrepned,ocinc,lrmpres,dpuwasher,lsnpre,levi,wafceil,aleak,inpleak,clkcrack,whleak,other,"+
							  "ovrplucondinc,ovrplucondopt,plucomments,odinc,whinc)"
							+ "VALUES('"+cf.selectedhomeid+"','"+wsschkval+"','','','','','','','','','','','','"+ssschkval+"','','','','','','','','','','','','','"+nobchkval+"','','','','','"+occhkval+"','','',"+
							  "'','','','"+cf.encode(alrdgtxt)+"',"+
							  "'"+cf.encode(iplrdgtxt)+"','','"+cf.encode(whlrdgtxt)+"','"+cf.encode(otrrdgtxt)+"',"+
							  "'"+copchkval+"','','','0','"+ whtrchkval+"')");
				 	  
			 	}
				catch (Exception E)
				{
					String strerrorlog="Inserting the Plumbing Other Conditions - FourPoint";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
		 }else{
			 try
				{
					cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set aleak='"+cf.encode(alrdgtxt)+"',"+
				        "inpleak='"+cf.encode(iplrdgtxt)+"',"+
						"whleak='"+cf.encode(whlrdgtxt)+"',other='"+cf.encode(otrrdgtxt)+"',odinc='"+odinc+"' where fld_srid='"+cf.selectedhomeid+"'");
				
				}
				catch (Exception E)
				{
					String strerrorlog="Updating the Plumbing Other Conditions - FourPoint";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
		}
	}
	private void ShowHeaterValue() {
		// TODO Auto-generated method stub
		   Cursor whtr_retrieve= cf.SelectTablefunction(cf.Plumbing_waterheater, " Where fld_srid='"+cf.selectedhomeid+"' and fld_flag='0'");
	       if(whtr_retrieve.getCount()>0)
			{
	    	   whtr_retrieve.moveToFirst();
			  
	    	   try
				{
					whtrtbl.removeAllViews();
				}
				catch (Exception e) {
					// TODO: handle exception
				}
				LinearLayout h1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvac_listview_header, null); 
				LinearLayout th = (LinearLayout)h1.findViewById(R.id.whtr);th.setVisibility(cf.v1.VISIBLE);
				TableLayout.LayoutParams lp = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
			 	lp.setMargins(2, 0, 2, 2); h1.removeAllViews(); 
			 	// th.setPadding(10, 0, 0, 0);
			 	
				
			 	whtrtbl.addView(th,lp);
			 	
			 	whtr_in_DB=new String[whtr_retrieve.getCount()];
				whtrtbl.setVisibility(View.VISIBLE); 
				
				for(int i=0;i<whtr_retrieve.getCount();i++)
				{
					
					TextView htrtyptxt,htragetxt,htrloctxt,htrdptxt,htrdltxt,htrletxt,htrcaptxt,htrlocttxt,htrnum;
					ImageView edit,delete,htrimgtxt;
					String htrtyp="",htrage="",htrloc="",htrdp="",htrdl="",htrle="",htrimg="",
							htrcap="",htrloctn="",htrmodimg="";
					htr=cf.decode(whtr_retrieve.getString(0));
					htrtyp=cf.decode(whtr_retrieve.getString(4));
					htrage=cf.decode(whtr_retrieve.getString(5));
					htrcap=cf.decode(whtr_retrieve.getString(6));
					htrloctn=cf.decode(whtr_retrieve.getString(7));
					htrloc=cf.decode(whtr_retrieve.getString(8));
					htrdp=cf.decode(whtr_retrieve.getString(9));
					htrdl=cf.decode(whtr_retrieve.getString(10));
					htrle=cf.decode(whtr_retrieve.getString(11));
					htrimg=cf.decode(whtr_retrieve.getString(12));
					htrmodimg=cf.decode(whtr_retrieve.getString(13));
					
					whtr_in_DB[i]=htrtyp;
					
					LinearLayout t1= (LinearLayout) getLayoutInflater().inflate(R.layout.hvac_listview, null);
					LinearLayout t = (LinearLayout)t1.findViewById(R.id.heater);t.setVisibility(cf.v1.VISIBLE);
					t.setId(44444+i);/// Set some id for further use
					((TextView) t.findViewWithTag("#")).setText(String.valueOf(i+1));
					htrtyptxt= (TextView) t.findViewWithTag("HType");
				 	htrtyptxt.setText(htrtyp);
				 	htragetxt= (TextView) t.findViewWithTag("HAge");
				 	
				 	if(htrage.contains("Other~")){htrage=htrage.replace("Other~", "");
				 	htragetxt.setText(Html.fromHtml(htrage));}
				 	else{htragetxt.setText(htrage);}
				 	
				 	htrcaptxt= (TextView) t.findViewWithTag("HCap");
				 	if(htrcap.contains("Other~")){htrcap=htrcap.replace("Other~", "");
				 	htrcaptxt.setText(Html.fromHtml(htrcap)+" Gallons");}
				 	else{htrcaptxt.setText(htrcap);}
				 	
				 	htrlocttxt= (TextView) t.findViewWithTag("HLoct");
				 	if(htrloctn.contains("Other~")){htrloctn=htrloctn.replace("Other~", "");
				 	htrlocttxt.setText(Html.fromHtml(htrloctn));}				 	
				 	else{htrlocttxt.setText(htrloctn);}
				 	
				 	htrloctxt= (TextView) t.findViewWithTag("HLoc");
				 	htrloctxt.setText(htrloc);
				 	htrdptxt= (TextView) t.findViewWithTag("HDp");
				 	htrdptxt.setText(htrdp);
				 	htrdltxt= (TextView) t.findViewWithTag("HDl");
				 	htrdltxt.setText(htrdl);
				 	htrletxt= (TextView) t.findViewWithTag("HLe");
				 	htrletxt.setText(htrle);
				 	ImageView  iv=(ImageView)t.findViewWithTag("HIMG");
					File f=new File(htrimg);
					if(f.exists())
					{
						
						Bitmap b1= cf.ShrinkBitmap(htrimg, 125, 125);
						if(b1!=null)
						{
							iv.setImageBitmap(b1);
							
						}
						else
						{
							iv.setImageDrawable(getResources().getDrawable(R.drawable.photonotavail));
						}
						
					}
					else
					{
						iv.setImageDrawable(getResources().getDrawable(R.drawable.photonotavail));
					}
				 	
					ImageView  iv1=(ImageView)t.findViewWithTag("HM");
					File f1=new File(htrmodimg);
					if(f1.exists())
					{
						
						Bitmap b1= cf.ShrinkBitmap(htrmodimg, 125, 125);
						if(b1!=null)
						{
							iv1.setImageBitmap(b1);
							
						}
						else
						{
							iv1.setImageDrawable(getResources().getDrawable(R.drawable.photonotavail));
						}
						
					}
					else
					{
						iv1.setImageDrawable(getResources().getDrawable(R.drawable.photonotavail));
					}
				 	
				 	edit= (ImageView) t.findViewWithTag("edit");
				 	edit.setId(789456+i);
				 	edit.setTag(whtr_retrieve.getString(0));
	                edit.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
						try
						{
						int i=Integer.parseInt(v.getTag().toString());
						  spunt=1;updateheater(i);
						}
						catch (Exception e) {
							// TODO: handle exception
						}
						}
	                });
						
				 	delete=(ImageView) t.findViewWithTag("del");
				 	delete.setTag(whtr_retrieve.getString(0));
	                delete.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(final View v) {
							// TODO Auto-generated method stub
							
							AlertDialog.Builder builder = new AlertDialog.Builder(Plumbing.this);
							builder.setMessage("Do you want to delete the selected Water Heater Type?")
									.setCancelable(false)
									.setPositiveButton("Yes",
											new DialogInterface.OnClickListener() {

												public void onClick(DialogInterface dialog,
														int id) {
													
													try
													{
													int i=Integer.parseInt(v.getTag().toString());
													if(i==Current_select_id)
													{
														Current_select_id=0;
													}
													cf.arr_db.execSQL("Delete from "+cf.Plumbing_waterheater+" Where pwhidId='"+i+"'");
													cf.ShowToast("Water Heater Type deleted successfully.", 0);
													whtrtypspin.setSelection(0);((EditText)findViewById(R.id.whtrage_otr)).setText("");whtragespin.setSelection(0);
													((EditText)findViewById(R.id.whtrcap_otr)).setText("");whtrcapspin.setSelection(0);
													((EditText)findViewById(R.id.whtrloc_otr)).setText("");whtrlocspin.setSelection(0);
													whtrlofcspin.setSelection(0);whtrdppspin.setSelection(0);whtrtypspin.setEnabled(true);
													whtrdlspin.setSelection(0);whtrlespin.setSelection(0);
													((EditText)findViewById(R.id.msi_ed)).setText("");
													((EditText)findViewById(R.id.whi_ed)).setText("");
													((Button)findViewById(R.id.addhtr)).setText("Save/Add");
													((Button)findViewById(R.id.img_clr)).setVisibility(cf.v1.INVISIBLE);
													((Button)findViewById(R.id.wimg_clr)).setVisibility(cf.v1.INVISIBLE);
													 ShowHeaterValue();
													}
													catch (Exception e) {
														// TODO: handle exception
													}
												}
											})
									.setNegativeButton("No",
											new DialogInterface.OnClickListener() {
												public void onClick(DialogInterface dialog,
														int id) {

													dialog.cancel();
												}
											});
							AlertDialog al=builder.create();
							al.setTitle("Confirmation");
							al.setIcon(R.drawable.alertmsg);
							al.setCancelable(false);
							al.show();
						
							
						}
					});
				 	t1.removeAllViews();
				//	t.setPadding(10, 0, 0, 0);
					whtrtbl.addView(t,lp);
					whtr_retrieve.moveToNext();
				}
			}
	       else
	       {
	    	   whtrtbl.setVisibility(View.GONE);//((TextView)findViewById(R.id.savetxtwh)).setVisibility(cf.v1.INVISIBLE);
	    	   System.out.println("whval="+whval);
	    	   /*if(whval==0 || whval==1)
	    	   {
	    		   ((TextView)findViewById(R.id.savetxtwh)).setVisibility(cf.v1.VISIBLE);
	    	   }
	    	   else
	    	   {*/
	    	    /* if(whval==1)
	    	     {
	    	    	 ((TextView)findViewById(R.id.savetxtwh)).setVisibility(cf.v1.VISIBLE);
	    	     }else{
	    		   ((TextView)findViewById(R.id.savetxtwh)).setVisibility(cf.v1.INVISIBLE);}*/
	    	  // }
	       }
	}
	protected void updateheater(int i) {
		// TODO Auto-generated method stub
		Current_select_id = i;whtrtypspin.setEnabled(false);
		try{Cursor whtrret=cf.SelectTablefunction(cf.Plumbing_waterheater, " Where pwhidId='"+i+"'");
		if(whtrret.getCount()>0)
		{
			whtrret.moveToFirst();
			String htr="",htrtyp="",hage="",htrloc="",htrdp="",htrdl="",htrle="",htrage="",htrimg="",
					hcap="",hloct="",htrmodimg="";
			htr=cf.decode(whtrret.getString(0));
			htrtyp=cf.decode(whtrret.getString(4));
			hage=cf.decode(whtrret.getString(5));
			hcap=cf.decode(whtrret.getString(6));
			hloct=cf.decode(whtrret.getString(7));
			htrloc=cf.decode(whtrret.getString(8));
			htrdp=cf.decode(whtrret.getString(9));
			htrdl=cf.decode(whtrret.getString(10));
			htrle=cf.decode(whtrret.getString(11));
			htrimg=cf.decode(whtrret.getString(12));
			htrmodimg=cf.decode(whtrret.getString(13));
			
			int typpos=whtrtypadap.getPosition(htrtyp);
			whtrtypspin.setSelection(typpos);
			
			if(hage.contains("Other")){ 
			    String[] temp = hage.split("~");
			    htrage = temp[0];
			    ((EditText)findViewById(R.id.whtrage_otr)).setText(temp[1]);
			    ((EditText)findViewById(R.id.whtrage_otr)).setVisibility(cf.v1.VISIBLE);
		    }else{
		    	htrage = hage;((EditText)findViewById(R.id.whtrage_otr)).setVisibility(cf.v1.GONE);
			 }		
			
			
			if(hcap.contains("Other")){ 
			    String[] temp = hcap.split("~");
			    hcap = temp[0];
			    ((EditText)findViewById(R.id.whtrcap_otr)).setText(temp[1]);
			    ((EditText)findViewById(R.id.whtrcap_otr)).setVisibility(cf.v1.VISIBLE);
		    }else{
		    	hcap = hcap;((EditText)findViewById(R.id.whtrcap_otr)).setVisibility(cf.v1.INVISIBLE);
			 }	
			
			
			if(hloct.contains("Other")){ 
			    String[] temp = hloct.split("~");
			    hloct = temp[0];
			    ((EditText)findViewById(R.id.whtrloc_otr)).setText(temp[1]);
			    ((EditText)findViewById(R.id.whtrloc_otr)).setVisibility(cf.v1.VISIBLE);
		    }else{
		    	hloct = hloct;((EditText)findViewById(R.id.whtrloc_otr)).setVisibility(cf.v1.INVISIBLE);
			 }	
			
			int agepos=whtrageadap.getPosition(htrage);
			whtragespin.setSelection(agepos);
			int cappos=whtrcap.getPosition(hcap);
			whtrcapspin.setSelection(cappos);
			int loctpos=whtrloc.getPosition(hloct);
			whtrlocspin.setSelection(loctpos);
			int locpos=whtrlofc.getPosition(htrloc);
			whtrlofcspin.setSelection(locpos);
			int dppos=whtrdpp.getPosition(htrdp);
			whtrdppspin.setSelection(dppos);
			int dlpos=whtrdl.getPosition(htrdl);
			whtrdlspin.setSelection(dlpos);
			int lepos=whtrle.getPosition(htrle);
			whtrlespin.setSelection(lepos);
			((EditText)findViewById(R.id.whi_ed)).setText(htrimg);
			((EditText)findViewById(R.id.msi_ed)).setText(htrmodimg);
			((Button)findViewById(R.id.addhtr)).setText("Update");
			
			if(((EditText)findViewById(R.id.msi_ed)).getText().toString().equals(""))
			{
				((Button)findViewById(R.id.img_clr)).setVisibility(cf.v1.GONE);
			}
			else
			{
				((Button)findViewById(R.id.img_clr)).setVisibility(cf.v1.VISIBLE);	
			}
			
			if(((EditText)findViewById(R.id.whi_ed)).getText().toString().equals(""))
			{
				((Button)findViewById(R.id.wimg_clr)).setVisibility(cf.v1.GONE);
			}
			else
			{
				((Button)findViewById(R.id.wimg_clr)).setVisibility(cf.v1.VISIBLE);	
			}
			
			
		}
		}catch(Exception e){}
	}
	private void WSS_Insert() {
		// TODO Auto-generated method stub
		WSS_Insertval();
		if(wsschkval.equals("1")){ ((TextView)findViewById(R.id.savetxtwss)).setVisibility(cf.v1.GONE);}
		else{ ((TextView)findViewById(R.id.savetxtwss)).setVisibility(cf.v1.VISIBLE);
		cf.ShowToast("Water Supply Service saved successfully.", 0);}
		wss_sve=true;
	}
	private void WSS_Insertval()
	{
		chk_values();
		 if(rws == 0){
			 try
				{
					cf.arr_db.execSQL("INSERT INTO "
							+ cf.Four_Electrical_Plumbing
							+ " (fld_srid,wsinc,wstyp,wptyp,wage,wyiconf,mainshut,wloc,wconf,wrupd,wrupdy,wdlupd,wpconf,"+
							  "ssinc,sstype,ptype,sapproxage,yiconf,scpfound,srupd,sdlupd,spconf,slevi,"+
						      "sonoted,sflrv,scomments,nobinc,tnob,nobfc,sencpre,ptrepned,ocinc,lrmpres,dpuwasher,lsnpre,levi,wafceil,aleak,inpleak,clkcrack,whleak,other,"+
							  "ovrplucondinc,ovrplucondopt,plucomments,odinc,whinc)"
							+ "VALUES('"+cf.selectedhomeid+"','"+wsschkval+"','"+cf.encode(wstrdgtxt)+"','"+cf.encode(wptchk_value)+"',"+
							  "'"+cf.encode(wagestr)+"','"+cf.encode(wyicrdtxt)+"','"+cf.encode(msoprdtxt)+"','"+cf.encode(wlocrdtxt)+"',"+
							  "'"+cf.encode(wconfstr)+"','"+cf.encode(wrurdtxt)+"','"+cf.encode(wyruchk_value)+"',"+
							  "'"+cf.encode(wdlupdated)+"','"+cf.encode(wpcrdtxt)+"',"+
							  "'"+ssschkval+"','','','','','','','','','','','','','"+nobchkval+"','','','','','"+occhkval+"','','','','','','','','','','','"+copchkval+"','','','"+odchkval+"','"+whtrchkval+"')");
				}
				catch (Exception E)
				{
					String strerrorlog="Inserting the Plumbing WATER SERVICE  - FourPoint";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
		 }else{
			 System.out.println("insertwdlupdated = "+wdlupdated);
			 try
				{
					cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set wsinc='"+wsschkval+"',"+
				               "wstyp='"+cf.encode(wstrdgtxt)+"',wptyp='"+cf.encode(wptchk_value)+"',"+
							   "wage='"+cf.encode(wagestr)+"',wyiconf='"+cf.encode(wyicrdtxt)+"',"+
				               "mainshut='"+cf.encode(msoprdtxt)+"',wloc='"+cf.encode(wlocrdtxt)+"',"+
							   "wconf='"+cf.encode(wconfstr)+"',wrupd='"+cf.encode(wrurdtxt)+"',"+
				               "wrupdy='"+cf.encode(wyruchk_value)+"',"+
						       "wdlupd='"+cf.encode(wdlupdated)+"',"+
				               "wpconf='"+cf.encode(wpcrdtxt)+"' where fld_srid='"+cf.selectedhomeid+"'");
				}
				catch (Exception E)
				{
					String strerrorlog="Updating the Plumbing SEWER SERVICE - FourPoint";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
		 }
	}
	private void SSS_Insert() {
		// TODO Auto-generated method stub
			SSS_Insertval();
			if(ssschkval.equals("1")){ ((TextView)findViewById(R.id.savetxtsss)).setVisibility(cf.v1.GONE);}
			else{ ((TextView)findViewById(R.id.savetxtsss)).setVisibility(cf.v1.VISIBLE);
			cf.ShowToast("Sewer Service saved successfully.", 0);}
			sss_sve=true;
	}
	private void SSS_Insertval()
	{
		if(scfrdtxt.contains("Other"))
		{
			scfrdtxt =scfrdtxt+"&#94;"+((EditText)findViewById(R.id.sewerother)).getText().toString().trim();
		}
		else
		{
			scfrdtxt =scfrdtxt;
		}
		
		
		chk_values();
		 if(rws == 0){
			 try
				{
					cf.arr_db.execSQL("INSERT INTO "
							+ cf.Four_Electrical_Plumbing
							+ " (fld_srid,wsinc,wstyp,wptyp,wage,wyiconf,mainshut,wloc,wconf,wrupd,wrupdy,wdlupd,wpconf,"+
							  "ssinc,sstype,ptype,sapproxage,yiconf,scpfound,srupd,sdlupd,spconf,slevi,"+
						      "sonoted,sflrv,scomments,nobinc,tnob,nobfc,sencpre,ptrepned,ocinc,lrmpres,dpuwasher,lsnpre,levi,wafceil,aleak,inpleak,clkcrack,whleak,other,"+
							  "ovrplucondinc,ovrplucondopt,plucomments,odinc,whinc)"
							+ "VALUES('"+cf.selectedhomeid+"','"+ssschkval+"','','','','','','','','','','','','"+ssschkval+"','"+cf.encode(strdgtxt)+"','"+cf.encode(ptchk_value)+"',"+
							  "'"+cf.encode(agestr)+"','"+cf.encode(yicrdtxt)+"','"+cf.encode(scfrdtxt)+"','"+cf.encode(rurdtxt)+"',"+
							  "'"+cf.encode(sdlupdated)+"','"+cf.encode(pcrdtxt)+"',"+
							  "'"+cf.encode(levrdtxt)+"','"+cf.encode(sonrdtxt)+"','',"+
							  "'"+cf.encode(((EditText)findViewById(R.id.sewercomment)).getText().toString())+"',"+
							  "'"+nobchkval+"','','','','',"+
							  "'"+occhkval+"','','','','','','','','','','','"+copchkval+"','','','"+odchkval+"','"+whtrchkval+"')");
				}
				catch (Exception E)
				{
					String strerrorlog="Inserting the Plumbing SEWER SERVICE  - FourPoint";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
		 }else{
			 try
				{
					cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set ssinc='"+ssschkval+"',"+
				               "sstype='"+cf.encode(strdgtxt)+"',ptype='"+cf.encode(ptchk_value)+"',"+
							   "sapproxage='"+cf.encode(agestr)+"',yiconf='"+cf.encode(yicrdtxt)+"',"+
				               "scpfound='"+cf.encode(scfrdtxt)+"',srupd='"+cf.encode(rurdtxt)+"',"+
							   "sdlupd='"+cf.encode(sdlupdated)+"',"+
				               "spconf='"+cf.encode(pcrdtxt)+"',slevi='"+cf.encode(levrdtxt)+"',"+
						       "sonoted='"+cf.encode(sonrdtxt)+"',"+
				               "scomments='"+cf.encode(((EditText)findViewById(R.id.sewercomment)).getText().toString())+"' where fld_srid='"+cf.selectedhomeid+"'");
				}
				catch (Exception E)
				{
					String strerrorlog="Updating the Plumbing SEWER SERVICE - FourPoint";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
		 }
	}
	private void NOB_Insert() {
		// TODO Auto-generated method stub
		NOB_Insertval();		
		 if(nobchkval.equals("1")){ ((TextView)findViewById(R.id.savetxtnob)).setVisibility(cf.v1.GONE);}
			else{ ((TextView)findViewById(R.id.savetxtnob)).setVisibility(cf.v1.VISIBLE);
			cf.ShowToast("Bathrooms saved successfully.", 0);}
	}
	private void NOB_Insertval() {
		// TODO Auto-generated method stub
		 chk_values();
		 if(rws == 0){
			 try
				{
					cf.arr_db.execSQL("INSERT INTO "
							+ cf.Four_Electrical_Plumbing
							+ " (fld_srid,wsinc,wstyp,wptyp,wage,wyiconf,mainshut,wloc,wconf,wrupd,wrupdy,wdlupd,wpconf,"+
							  "ssinc,sstype,ptype,sapproxage,yiconf,scpfound,srupd,sdlupd,spconf,slevi,"+
						      "sonoted,sflrv,scomments,nobinc,tnob,nobfc,sencpre,ptrepned,ocinc,lrmpres,dpuwasher,lsnpre,levi,wafceil,aleak,inpleak,clkcrack,whleak,other,"+
							  "ovrplucondinc,ovrplucondopt,plucomments,odinc,whinc)"
							+ "VALUES('"+cf.selectedhomeid+"','"+wsschkval+"','','','','','','','','','','','','"+ssschkval+"','','','','','','','','','','','','','"+nobchkval+"',"+
							  "'"+cf.encode(nobstr)+"','"+cf.encode(nobfcstr)+"','"+cf.encode(seprdtxt)+"','"+cf.encode(ptrnrdtxt)+"',"+
							  "'"+occhkval+"','','','','','','','','','','','"+copchkval+"','','','"+odchkval+"','"+whtrchkval+"')");
				}
				catch (Exception E)
				{
					String strerrorlog="Inserting the Plumbing NUMBER OF BATHROOMS  - FourPoint";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
		 }else{
			 try
				{
					cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set nobinc='"+nobchkval+"',"+
				        "tnob='"+cf.encode(nobstr)+"',nobfc='"+cf.encode(nobfcstr)+"',"+
						"sencpre='"+cf.encode(seprdtxt)+"',ptrepned='"+cf.encode(ptrnrdtxt)+"' where fld_srid='"+cf.selectedhomeid+"'");
				}
				catch (Exception E)
				{
					String strerrorlog="Updating the Plumbing NUMBER OF BATHROOMS - FourPoint";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
		 }
	}
	private void OC_Insert() {
		// TODO Auto-generated method stub
		OC_Insertval();		
		if(occhkval.equals("1")){ ((TextView)findViewById(R.id.savetxtoc)).setVisibility(cf.v1.GONE);}
		else{ ((TextView)findViewById(R.id.savetxtoc)).setVisibility(cf.v1.VISIBLE);
		cf.ShowToast("Laundry Room saved successfully.", 0);}
		lr_sve=true;		
	}
	private void OC_Insertval() {
		// TODO Auto-generated method stub
		chk_values();
		 if(rws == 0){
			 try
				{
					cf.arr_db.execSQL("INSERT INTO "
							+ cf.Four_Electrical_Plumbing
							+ " (fld_srid,wsinc,wstyp,wptyp,wage,wyiconf,mainshut,wloc,wconf,wrupd,wrupdy,wdlupd,wpconf,"+
							  "ssinc,sstype,ptype,sapproxage,yiconf,scpfound,srupd,sdlupd,spconf,slevi,"+
						      "sonoted,sflrv,scomments,nobinc,tnob,nobfc,sencpre,ptrepned,ocinc,lrmpres,dpuwasher,lsnpre,levi,wafceil,aleak,inpleak,clkcrack,whleak,other,"+
							  "ovrplucondinc,ovrplucondopt,plucomments,odinc,whinc)"
							+ "VALUES('"+cf.selectedhomeid+"','"+wsschkval+"','','','','','','','','','','','','"+ssschkval+"','','','','','','','','','','','','','"+nobchkval+"','','','','','"+occhkval+"','"+cf.encode(lrprdgtxt)+"','"+cf.encode(dpuwrdgtxt)+"',"+
							  "'"+cf.encode(lsprdgtxt)+"','"+cf.encode(llerdgtxt)+"','"+cf.encode(wafcrdgtxt)+"','',"+
							  "'','"+cf.encode(clcrdgtxt)+"','','',"+
							  "'"+copchkval+"','','','"+odchkval+"','"+whtrchkval+"')");
				}
				catch (Exception E)
				{
					String strerrorlog="Inserting the Plumbing Other Conditions - FourPoint";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
		 }else{
			 try
				{
					cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set ocinc='"+occhkval+"',"+
				        "lrmpres='"+cf.encode(lrprdgtxt)+"',dpuwasher='"+cf.encode(dpuwrdgtxt)+"',"+
						"lsnpre='"+cf.encode(lsprdgtxt)+"',levi='"+cf.encode(llerdgtxt)+"',wafceil='"+cf.encode(wafcrdgtxt)+"',"+
				        "clkcrack='"+cf.encode(clcrdgtxt)+"' where fld_srid='"+cf.selectedhomeid+"'");
				}
				catch (Exception E)
				{
					String strerrorlog="Updating the Plumbing Other Conditions - FourPoint";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
		 }
	}
	private void COP_Insert() {
		// TODO Auto-generated method stub
		COP_Insertval();
		if(copchkval.equals("1")){ ((TextView)findViewById(R.id.savetxtcop)).setVisibility(cf.v1.GONE);}
		else{ ((TextView)findViewById(R.id.savetxtcop)).setVisibility(cf.v1.VISIBLE);
		cf.ShowToast("Conditional of Overall Plumbing Services saved successfully.", 0);}
		con_sve=true;
		
	}
	private void COP_Insertval() {
		// TODO Auto-generated method stub
		 chk_values();
		 if(rws == 0){
			 try
				{
					cf.arr_db.execSQL("INSERT INTO "
							+ cf.Four_Electrical_Plumbing
							+ " (fld_srid,wsinc,wstyp,wptyp,wage,wyiconf,mainshut,wloc,wconf,wrupd,wrupdy,wdlupd,wpconf,"+
							  "ssinc,sstype,ptype,sapproxage,yiconf,scpfound,srupd,sdlupd,spconf,slevi,"+
						      "sonoted,sflrv,scomments,nobinc,tnob,nobfc,sencpre,ptrepned,ocinc,lrmpres,dpuwasher,lsnpre,levi,wafceil,aleak,inpleak,clkcrack,whleak,other,"+
							  "ovrplucondinc,ovrplucondopt,plucomments,odinc,whinc)"
							+ "VALUES('"+cf.selectedhomeid+"','"+ wsschkval+"','','','','','','','','','','','','"+ssschkval+"','','','','','','','','','','','"+cf.encode(flkrvstr)+"','','"+nobchkval+"','','','','','"+occhkval+"','','','','','','','','','','','"+copchkval+"','"+cf.encode(pscrdtxt)+"','','"+odchkval+"','"+whtrchkval+"')");
				
			 	}
				catch (Exception E)
				{
					String strerrorlog="Inserting the Plumbing  - FourPoint";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" in the stage of Table insert at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
		 }else{
			 try
				{
					cf.arr_db.execSQL("UPDATE "+cf.Four_Electrical_Plumbing+ " set ovrplucondinc='"+copchkval+"',"+
				        "sflrv='"+cf.encode(flkrvstr)+"',"+
				        "ovrplucondopt='"+cf.encode(pscrdtxt)+"' where fld_srid='"+cf.selectedhomeid+"'");
					
				}
				catch (Exception E)
				{
					String strerrorlog="Updating the Plumbing - FourPoint";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ cf.con+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
		 }
	}
	private void chk_values() {
		// TODO Auto-generated method stub
		try
		{
			Cursor chkplum_save=cf.SelectTablefunction(cf.Four_Electrical_Plumbing, " where fld_srid='"+cf.selectedhomeid+"'");
			rws = chkplum_save.getCount();
			if(rws>0){
				chkplum_save.moveToFirst();
				
				retpscrdtxt = chkplum_save.getString(chkplum_save.getColumnIndex("ovrplucondopt"));
				retodrdtxt = chkplum_save.getString(chkplum_save.getColumnIndex("whleak"));
				retlrprdtxt = chkplum_save.getString(chkplum_save.getColumnIndex("lrmpres"));
				retnobstr = chkplum_save.getString(chkplum_save.getColumnIndex("tnob"));
				retsstypstr = chkplum_save.getString(chkplum_save.getColumnIndex("sstype"));
				retwstypstr = chkplum_save.getString(chkplum_save.getColumnIndex("wstyp"));
				retaldtxt = chkplum_save.getString(chkplum_save.getColumnIndex("aleak"));
				ssage = chkplum_save.getString(chkplum_save.getColumnIndex("sapproxage"));
				wsage = chkplum_save.getString(chkplum_save.getColumnIndex("wage"));
				ssageconfig = chkplum_save.getString(chkplum_save.getColumnIndex("yiconf"));
				wsageconfig = chkplum_save.getString(chkplum_save.getColumnIndex("wyiconf"));
				//plumwh_save = chkplum_save.getInt(chkplum_save.getColumnIndex("aleak"));
				
			}
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		try{
			plumwh_save=cf.SelectTablefunction(cf.Plumbing_waterheater, " where fld_srid='"+cf.selectedhomeid+"'");
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}
	private void Uncheckhdrs(CheckBox[] plumchk2) {
		// TODO Auto-generated method stub
		for(int i=0;i<plumchk2.length;i++){
			plumchk2[i].setChecked(false);
		}
		chk_values();
		if(!retpscrdtxt.equals("")){
			plumchk[6].setChecked(true);((LinearLayout)findViewById(R.id.coplin)).setVisibility(cf.v1.VISIBLE);
		}
		else
		{
			clearcon();
		}
		/*if(!retaldtxt.equals("")){
			plumchk[5].setChecked(true);((LinearLayout)findViewById(R.id.odlin)).setVisibility(cf.v1.VISIBLE);
		}
		else
		{
			cleardef();
		}*/
		if(!retlrprdtxt.equals("")){
			plumchk[4].setChecked(true);((LinearLayout)findViewById(R.id.oclin)).setVisibility(cf.v1.VISIBLE);
		}
		else
		{
			clearlr();
		}
		if(!retnobstr.equals("")){
			plumchk[3].setChecked(true);((LinearLayout)findViewById(R.id.noblin)).setVisibility(cf.v1.VISIBLE);
		}
		else
		{
			clearbrm();
		}
		if(plumwh_save.getCount()>0){
			plumchk[2].setChecked(true);((LinearLayout)findViewById(R.id.whlin)).setVisibility(cf.v1.VISIBLE);
		}
		else
		{
			clearWH();
		}
		if(!retsstypstr.equals("")){
			plumchk[1].setChecked(true);((LinearLayout)findViewById(R.id.ssslin)).setVisibility(cf.v1.VISIBLE);
		}
		else
		{
			clearsss();
		}
		if(!retwstypstr.equals("")){
			plumchk[0].setChecked(true);((LinearLayout)findViewById(R.id.wsslin)).setVisibility(cf.v1.VISIBLE);
		}
		else
		{
			clearwss();
		}
	}
	private void hideotherlayouts() {
		// TODO Auto-generated method stub
		
		((LinearLayout)findViewById(R.id.coplin)).setVisibility(cf.v1.GONE);
		//((LinearLayout)findViewById(R.id.odlin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.oclin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.noblin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.whlin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.ssslin)).setVisibility(cf.v1.GONE);
		((LinearLayout)findViewById(R.id.wsslin)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.shwwss)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwsss)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwwh)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwnob)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwlr)).setVisibility(cf.v1.VISIBLE);
		//((ImageView)findViewById(R.id.shwod)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.shwcon)).setVisibility(cf.v1.VISIBLE);
		((ImageView)findViewById(R.id.hdwss)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdsss)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdwh)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdnob)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdlr)).setVisibility(cf.v1.GONE);
		//((ImageView)findViewById(R.id.hdod)).setVisibility(cf.v1.GONE);
		((ImageView)findViewById(R.id.hdcon)).setVisibility(cf.v1.GONE);
		/*chk_values();
		if(retpscrdtxt.equals("")){
			((LinearLayout)findViewById(R.id.coplin)).setVisibility(cf.v1.GONE);
		}
		if(retaldtxt.equals("")){
			((LinearLayout)findViewById(R.id.odlin)).setVisibility(cf.v1.GONE);
		}
		if(retlrprdtxt.equals("")){
			((LinearLayout)findViewById(R.id.oclin)).setVisibility(cf.v1.GONE);
		}
		if(retnobstr.equals("")){
			((LinearLayout)findViewById(R.id.noblin)).setVisibility(cf.v1.GONE);
		}
		if(plumwh_save.getCount()<=0){
			((LinearLayout)findViewById(R.id.whlin)).setVisibility(cf.v1.GONE);
		}
		if(retsstypstr.equals("")){
			((LinearLayout)findViewById(R.id.ssslin)).setVisibility(cf.v1.GONE);
		}
		if(retwstypstr.equals("")){
			((LinearLayout)findViewById(R.id.wsslin)).setVisibility(cf.v1.GONE);
		}*/
			
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
 		// replaces the default 'Back' button action
 		if (keyCode == KeyEvent.KEYCODE_BACK) {
 			 boolean chk=cf.checkAttic();
			 if(chk==true){
				 cf.Roof="4point";
		 		 cf.goback(1);
			 }else{
				 cf.goclass(21);
			 }
 		   return true;
 		}
 		
 		return super.onKeyDown(keyCode, event);
 	}
	public void showunchkalert(final int i) {
		// TODO Auto-generated method stub
		String titname="";
		switch(i)
		{
			case 1:
				titname=((TextView)findViewById(R.id.tit1)).getText().toString();
				break;
			case 2:
				titname=((TextView)findViewById(R.id.tit2)).getText().toString();
				break;
			case 3:
				titname=((TextView)findViewById(R.id.tit3)).getText().toString();
				break;
			case 4:
				titname=((TextView)findViewById(R.id.tit4)).getText().toString();
				break;
			case 5:
				titname=((TextView)findViewById(R.id.tit5)).getText().toString();
				break;
		/*	case 6:
				titname=((TextView)findViewById(R.id.tit6)).getText().toString();
				break;*/
			case 7:
				titname=((TextView)findViewById(R.id.tit7)).getText().toString();
				
				break;
			default:
				break;
		}
		AlertDialog.Builder bl = new Builder(Plumbing.this);
		bl.setTitle("Confirmation");
		bl.setMessage(Html.fromHtml("Do you want to clear the "+titname+" data?"));
		bl.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog,
											int id) {
										 switch(i){
										 case 1:
											 ((TextView)findViewById(R.id.savetxtwss)).setVisibility(cf.v1.INVISIBLE);
							    			  clearwss();//WSS_Insert();
							    			 ((LinearLayout)findViewById(R.id.wsslin)).setVisibility(cf.v1.GONE);
							    			// ((TextView)findViewById(R.id.savetxtwss)).setVisibility(cf.v1.VISIBLE);
							    			 ((ImageView)findViewById(R.id.shwwss)).setVisibility(cf.v1.VISIBLE);
							    			 ((ImageView)findViewById(R.id.hdwss)).setVisibility(cf.v1.GONE);
							    			 ((ImageView)findViewById(R.id.shwwss)).setEnabled(false);
							    			break;
										 case 2:
											 ((TextView)findViewById(R.id.savetxtsss)).setVisibility(cf.v1.INVISIBLE);
							    			 clearsss();defaultsewer();//SSS_Insert();
							    			 ((LinearLayout)findViewById(R.id.ssslin)).setVisibility(cf.v1.GONE);
							    			// ((TextView)findViewById(R.id.savetxtsss)).setVisibility(cf.v1.VISIBLE);
							    			 ((ImageView)findViewById(R.id.shwsss)).setVisibility(cf.v1.VISIBLE);
							    			 ((ImageView)findViewById(R.id.hdsss)).setVisibility(cf.v1.GONE);
							    			 ((ImageView)findViewById(R.id.shwsss)).setEnabled(false);
							    			 break;
										 case 3:
											 ((TextView)findViewById(R.id.savetxtwh)).setVisibility(cf.v1.INVISIBLE);
							    			  clearWH();
							    			 ((LinearLayout)findViewById(R.id.whlin)).setVisibility(cf.v1.GONE);
							    		//	 ((TextView)findViewById(R.id.savetxtwh)).setVisibility(cf.v1.VISIBLE);
							    			 ((ImageView)findViewById(R.id.shwwh)).setVisibility(cf.v1.VISIBLE);
							    			 ((ImageView)findViewById(R.id.hdwh)).setVisibility(cf.v1.GONE);
							    			 ((ImageView)findViewById(R.id.shwwh)).setEnabled(false);
							    			 break;
										 case 4:
											 ((TextView)findViewById(R.id.savetxtnob)).setVisibility(cf.v1.INVISIBLE);
							    			 clearbrm();
											// NOB_Insert();
							    			 ((LinearLayout)findViewById(R.id.noblin)).setVisibility(cf.v1.GONE);
							    			// ((TextView)findViewById(R.id.savetxtnob)).setVisibility(cf.v1.VISIBLE);
							    			 ((ImageView)findViewById(R.id.shwnob)).setVisibility(cf.v1.VISIBLE);
							    			 ((ImageView)findViewById(R.id.hdnob)).setVisibility(cf.v1.GONE);
							    			 ((ImageView)findViewById(R.id.shwnob)).setEnabled(false);
							    			 break;
										 case 5:
											 ((TextView)findViewById(R.id.savetxtoc)).setVisibility(cf.v1.INVISIBLE);
							    			 clearlr();//OC_Insert();
							    			 ((LinearLayout)findViewById(R.id.oclin)).setVisibility(cf.v1.GONE);
							    			// ((TextView)findViewById(R.id.savetxtoc)).setVisibility(cf.v1.VISIBLE);
							    			 ((ImageView)findViewById(R.id.shwlr)).setVisibility(cf.v1.VISIBLE);
							    			 ((ImageView)findViewById(R.id.hdlr)).setVisibility(cf.v1.GONE);
							    			 ((ImageView)findViewById(R.id.shwlr)).setEnabled(false);
							    			 break;
										/* case 6:
											 ((TextView)findViewById(R.id.savetxtod)).setVisibility(cf.v1.INVISIBLE);
							    			 cleardef();
							    			// OD_Insert();
							    			 ((LinearLayout)findViewById(R.id.odlin)).setVisibility(cf.v1.GONE);
							    			// ((TextView)findViewById(R.id.savetxtod)).setVisibility(cf.v1.VISIBLE);
							    			 ((ImageView)findViewById(R.id.shwod)).setVisibility(cf.v1.VISIBLE);
							    			 ((ImageView)findViewById(R.id.hdod)).setVisibility(cf.v1.GONE);
							    			 ((ImageView)findViewById(R.id.shwod)).setEnabled(false);
							    			 break;*/
										 case 7:
											 ((TextView)findViewById(R.id.savetxtcop)).setVisibility(cf.v1.INVISIBLE);
							    			 clearcon();
							    			 //COP_Insert();
							    			 ((LinearLayout)findViewById(R.id.coplin)).setVisibility(cf.v1.GONE);
							    			// ((TextView)findViewById(R.id.savetxtcop)).setVisibility(cf.v1.VISIBLE);
							    			 ((ImageView)findViewById(R.id.shwcon)).setVisibility(cf.v1.VISIBLE);
							    			 ((ImageView)findViewById(R.id.hdcon)).setVisibility(cf.v1.GONE);
							    			 ((ImageView)findViewById(R.id.shwcon)).setEnabled(false);
							    			 break;
										 }
										 	 
										 
									}
		});
		bl.setNegativeButton("No",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog,
							int id) {
						switch(i){
						 case 1:
							 ((CheckBox)findViewById(R.id.wss_chk)).setChecked(false);
							 ((ImageView)findViewById(R.id.shwwss)).setVisibility(cf.v1.GONE);
				    		 ((ImageView)findViewById(R.id.hdwss)).setVisibility(cf.v1.VISIBLE);
				    		 ((LinearLayout)findViewById(R.id.wsslin)).setVisibility(cf.v1.VISIBLE);
							 break;
						 case 2:
							 ((CheckBox)findViewById(R.id.sss_chk)).setChecked(false);
							 ((ImageView)findViewById(R.id.shwsss)).setVisibility(cf.v1.GONE);
				    		 ((ImageView)findViewById(R.id.hdsss)).setVisibility(cf.v1.VISIBLE);
				    		 ((LinearLayout)findViewById(R.id.ssslin)).setVisibility(cf.v1.VISIBLE);
							 break;
						 case 3:
							 ((CheckBox)findViewById(R.id.wh_chk)).setChecked(false);
							 ((ImageView)findViewById(R.id.shwwh)).setVisibility(cf.v1.GONE);
				    		 ((ImageView)findViewById(R.id.hdwh)).setVisibility(cf.v1.VISIBLE);
				    		 ((LinearLayout)findViewById(R.id.whlin)).setVisibility(cf.v1.VISIBLE);
							 break;
						 case 4:
							 ((CheckBox)findViewById(R.id.nob_chk)).setChecked(false);
							 ((ImageView)findViewById(R.id.shwnob)).setVisibility(cf.v1.GONE);
				    		 ((ImageView)findViewById(R.id.hdnob)).setVisibility(cf.v1.VISIBLE);
				    		 ((LinearLayout)findViewById(R.id.noblin)).setVisibility(cf.v1.VISIBLE);
							 break;
						 case 5:
							 ((CheckBox)findViewById(R.id.oc_chk)).setChecked(false);
							 ((ImageView)findViewById(R.id.shwlr)).setVisibility(cf.v1.GONE);
				    		 ((ImageView)findViewById(R.id.hdlr)).setVisibility(cf.v1.VISIBLE);
				    		 ((LinearLayout)findViewById(R.id.oclin)).setVisibility(cf.v1.VISIBLE);
							 break;
						/* case 6:
								 ((CheckBox)findViewById(R.id.od_chk)).setChecked(false);
								 ((ImageView)findViewById(R.id.shwod)).setVisibility(cf.v1.GONE);
					    		 ((ImageView)findViewById(R.id.hdod)).setVisibility(cf.v1.VISIBLE);
					    		 ((LinearLayout)findViewById(R.id.odlin)).setVisibility(cf.v1.VISIBLE);
							 break;*/
						 case 7:
							 ((CheckBox)findViewById(R.id.cop_chk)).setChecked(false);
							 ((ImageView)findViewById(R.id.shwcon)).setVisibility(cf.v1.GONE);
				    		 ((ImageView)findViewById(R.id.hdcon)).setVisibility(cf.v1.VISIBLE);
				    		 ((LinearLayout)findViewById(R.id.coplin)).setVisibility(cf.v1.VISIBLE);
							 break;
						 }
					}
        });
		AlertDialog al=bl.create();
		al.setIcon(R.drawable.alertmsg);
		al.setCancelable(false);
		al.show();
		
	}
	protected void clearcon() {
		// TODO Auto-generated method stub
		 copchkval="1";pscck=true;
		 try{if(pscck){pscrdgval.clearCheck();}}catch(Exception e){}
		 pscrdtxt="";flkrvspin.setSelection(2);
		 ((LinearLayout)findViewById(R.id.nocommentsshow)).setVisibility(cf.v1.GONE);
		 ((EditText)findViewById(R.id.nocomment)).setText("");
		 retpscrdtxt="";defaultovrplu();
	}
	protected void cleardef() {
		// TODO Auto-generated method stub
		 odchkval="1";alck=true;ioplck=true;whlck=true;otrck=true;
		 try{if(alck){ alrdgval.clearCheck();}}catch(Exception e){}
		 try{if(ioplck){iplrdgval.clearCheck();}}catch(Exception e){}
		 try{if(whlck){whlrdgval.clearCheck();}}catch(Exception e){}
		 try{if(otrck){otrrdgval.clearCheck();}}catch(Exception e){}
		 alrdgtxt="";iplrdgtxt="";whlrdgtxt="";otrrdgtxt="";
		// ((EditText)findViewById(R.id.otrtxt)).setText("");
		// ((EditText)findViewById(R.id.otrtxt)).setVisibility(cf.v1.GONE);
		 retodrdtxt="";defaultodn();
	}
	protected void clearbrm() {
		// TODO Auto-generated method stub
		 nobchkval="1";sepck=true;ptrnck=true;
		 try{if(sepck){seprdgval.clearCheck();}}catch (Exception e) {}
		 try{if(ptrnck){ptrnrdgval.clearCheck();}}catch(Exception e){}
		 nobspin.setSelection(0);nobstr="";
		 nobfcspin.setSelection(0);nobfcstr="";seprdtxt="";ptrnrdtxt="";
		 retnobstr="";
		
	}
	protected void clearlr() {
		// TODO Auto-generated method stub
		 lrprdgtxt="";lsprdgtxt="";dpuwrdgtxt="";wafcrdgtxt="";
		 llerdgtxt="";clcrdgtxt="";
		 occhkval="1";lrpck=true;lspck=true;dpuwck=true;wafcck=true;
		 lleck=true;clcck=true;
		 try{if(lrpck){lrprdgval.clearCheck();}}catch(Exception e){}
		 try{if(lspck){lsprdgval.clearCheck();}}catch(Exception e){}
		 try{if(dpuwck){dpuwrdgval.clearCheck();}}catch(Exception e){}
		 try{if(wafcck){wafcrdgval.clearCheck();}}catch(Exception e){}
		 try{if(lleck){llerdgval.clearCheck();}}catch(Exception e){}
		 try{if(clcck){clcrdgval.clearCheck();}}catch(Exception e){}
		 retlrprdtxt="";defaultlaundry();
	}
	protected void clearWH() {
		// TODO Auto-generated method stub
		System.out.println("celearwh");
		 whtrchkval="1";
		 whtrtypstr="";whtragestr="";whtrlofcstr="";
		 whtrdppstr="";whtrdlstr="";whtrlestr="";
		 whtrtypspin.setSelection(0); whtrtypspin.setEnabled(true);
		 whtragespin.setSelection(0);
		 ((EditText)findViewById(R.id.whtrage_otr)).setText("");
		 ((EditText)findViewById(R.id.whtrage_otr)).setVisibility(cf.v1.GONE);
		 whtrcapspin.setSelection(0);
		 ((EditText)findViewById(R.id.whtrcap_otr)).setText("");
		 ((EditText)findViewById(R.id.whtrcap_otr)).setVisibility(cf.v1.INVISIBLE);
		 whtrlocspin.setSelection(0);
		 ((EditText)findViewById(R.id.whtrloc_otr)).setText("");
		 ((EditText)findViewById(R.id.whtrloc_otr)).setVisibility(cf.v1.INVISIBLE);
		 whtrlofcspin.setSelection(0);whtrdppspin.setSelection(0);
		 whtrdlspin.setSelection(0);whtrlespin.setSelection(0);
		 ((EditText)findViewById(R.id.whi_ed)).setText("");
		 ((EditText)findViewById(R.id.msi_ed)).setText("");
		 ((Button)findViewById(R.id.addhtr)).setText("Save/Add");
		 try{
			 cf.arr_db.execSQL("UPDATE  "+cf.Plumbing_waterheater+" SET fld_flag='1' Where fld_srid='"+cf.selectedhomeid+"' ");
			  
		// cf.arr_db.execSQL("UPDATE  "+ cf.Four_Electrical_Plumbing+ " SET whinc='1' Where fld_srid='"+cf.selectedhomeid+"' ");
		 }catch(Exception e){}defaultwhage();
		spunt=0;ShowHeaterValue();
	}
	protected void clearsss() {
		// TODO Auto-generated method stub
		 ssschkval="1";stck=true;scfck=true;sruck=true;spcck=true;
		 leck=true;sonck=true;
		 try{if(stck){strdgval.clearCheck();}}catch(Exception e){}
		 for(int i=0;i<sewercaprdio.length;i++)
		 {
			 sewercaprdio[i].setChecked(false);
			 ((EditText)findViewById(R.id.sewerother)).setText("");
			 ((EditText)findViewById(R.id.sewerother)).setVisibility(cf.v1.GONE);
		 }
		 
		 try{if(scfck){scfrdgval.clearCheck();}}catch(Exception e){}
		 try{if(sruck){rurdgval.clearCheck();}}catch(Exception e){}
		 try{if(spcck){pcrdgval.clearCheck();}}catch(Exception e){}
		 try{if(leck){levrdgval.clearCheck();}}catch(Exception e){}
		 try{if(sonck){sonrdgval.clearCheck();}}catch(Exception e){}
		 strdgtxt="";ptchk_value="";agestr="";yicrdtxt="Original";scfrdtxt="";
		 rurdtxt="";sdlupdated="";pcrdtxt="";lerdgtxt="";sonrdtxt="";
		 cf.Set_UncheckBox(ptchk, ((EditText)findViewById(R.id.ptotr)));
		
		 defaultage();
		  ((RadioButton)yicrdgval.findViewWithTag("Original")).setChecked(true);
		 ((EditText)findViewById(R.id.ruotr)).setText("");
		 ((LinearLayout)findViewById(R.id.sperlin)).setVisibility(cf.v1.GONE);
		 ((EditText)findViewById(R.id.dlutxt)).setText("");
		 ((CheckBox)findViewById(R.id.sndchk)).setChecked(false);sc=0;
		 ((Button)findViewById(R.id.getdlup)).setEnabled(true);
		 ((EditText)findViewById(R.id.snd_otr)).setText("");
		 ((EditText)findViewById(R.id.snd_otr)).setVisibility(cf.v1.GONE);
		 ((LinearLayout)findViewById(R.id.ssrushow)).setVisibility(cf.v1.GONE);
		 ((EditText)findViewById(R.id.sewercomment)).setText("");
		 retsstypstr="";defaultsewer();
	}
	protected void clearwss() {
		// TODO Auto-generated method stub
		 wsschkval="1";wstck=true;wmsp=true;wloc=true;wruck=true;wpcck=true;
		 try{if(wstck){wstrdgval.clearCheck();}}catch(Exception e){}
		 try{if(wmsp){msoprdgval.clearCheck();}}catch(Exception e){}
		 try{if(wloc){wlocrdgval.clearCheck();}}catch(Exception e){}
		 try{if(wruck){wrurdgval.clearCheck();}}catch(Exception e){}
		 try{if(wpcck){wpcrdgval.clearCheck();}}catch(Exception e){}
		 cf.Set_UncheckBox(wptchk, ((EditText)findViewById(R.id.wptotr)));
		 defaultwage();
		 ((RadioButton)wyicrdgval.findViewWithTag("Original")).setChecked(true);
		 wconfspin.setSelection(0);
		 wstrdgtxt="";wptchk_value="";wagestr="";wyicrdtxt="Original";msoprdtxt="";
		 wlocrdtxt="";wconfstr="";wrurdtxt="";wyruchk_value="";
		 wdlupdated="";wdlupdated="";wpcrdtxt="";
		 ((EditText)findViewById(R.id.wage_otr)).setText("");
		 ((EditText)findViewById(R.id.wage_otr)).setVisibility(cf.v1.GONE);
		 ((EditText)findViewById(R.id.wconf_otr)).setText("");
		 ((EditText)findViewById(R.id.wconf_otr)).setVisibility(cf.v1.GONE);
		((EditText)findViewById(R.id.wruotr)).setText("");
		((LinearLayout)findViewById(R.id.perlin)).setVisibility(cf.v1.GONE);
		 cf.Set_UncheckBox(wruchk, ((EditText)findViewById(R.id.wptotr)));
		 ((EditText)findViewById(R.id.wdlutxt)).setText("");
		 ((CheckBox)findViewById(R.id.wndchk)).setChecked(false);
		 ((Button)findViewById(R.id.wgetdlup)).setEnabled(true);wc=0;
		 ((EditText)findViewById(R.id.wnd_otr)).setText("");
		 ((EditText)findViewById(R.id.wnd_otr)).setVisibility(cf.v1.GONE);
		 ((LinearLayout)findViewById(R.id.rushow)).setVisibility(cf.v1.GONE);
		 ((EditText)findViewById(R.id.watercomment)).setText("");
		 retwstypstr="";defaultwss();
	}
	private void defaultsewer() {
		// TODO Auto-generated method stub
		 ((RadioButton)levrdgval.findViewWithTag("None Visible From Areas Surveyed")).setChecked(true);
		 ((RadioButton)sonrdgval.findViewWithTag("None Noted at Time of Inspection")).setChecked(true);
		 ((RadioButton)strdgval.findViewWithTag("Municipality")).setChecked(true);
		 
		 
		 ((RadioButton) pcrdgval.findViewWithTag("Not Applicable")).setChecked(true);
		 ((RadioButton) rurdgval.findViewWithTag("Not Determined")).setChecked(true);
		 ((RadioButton)findViewById(R.id.sewercaprdio6)).setChecked(true);
		 ((EditText)findViewById(R.id.sewerother)).setVisibility(View.VISIBLE);
		 
		 
		 ((CheckBox)findViewById(R.id.sndchk)).setChecked(true);
		 ((Button)findViewById(R.id.getdlup)).setEnabled(false);
		 ((EditText)findViewById(R.id.dlutxt)).setText("Not Determined");
	}	
	private void defaultwss() {
		// TODO Auto-generated method stub
		//((RadioButton) wrurdgval.findViewWithTag("Yes")).setChecked(true);
		((RadioButton) wstrdgval.findViewWithTag("Municipality")).setChecked(true);
		((RadioButton) msoprdgval.findViewWithTag("Yes")).setChecked(true);
		((RadioButton) whlrdgval.findViewWithTag("Beyond Scope of Inspection")).setChecked(true);
		wconfspin.setSelection(2);
		
		//((LinearLayout)findViewById(R.id.perlin)).setVisibility(cf.v1.VISIBLE);
		
		 ((CheckBox)findViewById(R.id.wru_chk4)).setChecked(true);
		 
		 ((CheckBox)findViewById(R.id.wndchk)).setChecked(true);
		 ((Button)findViewById(R.id.wgetdlup)).setEnabled(false);
		 ((EditText)findViewById(R.id.wdlutxt)).setText("Not Determined");
		 
	}
	private void defaultlaundry() {
		// TODO Auto-generated method stub
		((RadioButton) lrprdgval.findViewWithTag("Yes")).setChecked(true);
		((RadioButton) lsprdgval.findViewWithTag("Yes")).setChecked(true);
		((RadioButton) llerdgval.findViewWithTag("None Visible at Time of Inspection")).setChecked(true);
		((RadioButton) wafcrdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) dpuwrdgval.findViewWithTag("No")).setChecked(true);
		((RadioButton) clcrdgval.findViewWithTag("None Visible at Time of Inspection")).setChecked(true);
	}	
	private void defaultodn() {
		// TODO Auto-generated method stub
		((RadioButton) alrdgval.findViewWithTag("None Visible From Areas Surveyed")).setChecked(true);
		((RadioButton) otrrdgval.findViewWithTag("None Visible From Areas Surveyed")).setChecked(true);
		((RadioButton) whlrdgval.findViewWithTag("Not Noted at Time of Inspection")).setChecked(true);
	}
	private void defaultovrplu()
	{
		((RadioButton) pscrdgval.findViewWithTag("Yes, based on visual observations at the time of inspection")).setChecked(true);
		
	}

}