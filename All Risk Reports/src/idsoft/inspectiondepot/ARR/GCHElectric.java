/*
   ************************************************************
   * Project: ALL RISK REPORT
   * Module : GCHElectric.java
   * Creation History:
      Created By : Gowri on 
   * Modification History:
      Last Modified By : Gowri on 04/04/2013
   ************************************************************  
*/
package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;
import idsoft.inspectiondepot.ARR.GCHHVAC.textwatcher;
import idsoft.inspectiondepot.ARR.Plumbing.Spin_Selectedlistener;
import idsoft.inspectiondepot.ARR.Plumbing.checklistenetr;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class GCHElectric extends Activity {
	CommonFunctions cf;
	Spinner mparspin,agespin;
	ArrayAdapter adapter,ageadap;
	int elecchkval=0;
	String amps[] = {"--Select--","60 AMPS","100 AMPS","150 AMPS","200 AMPS","300 AMPS","400 AMPS","Not Determined","Other"};
	String  mpaspinval="",electypechk_val="",vocpchk_val="",oehchk_val="",
			mdcrdgtxt="",acsrdgtxt="No",updrdgtxt="No",agestr="",retelecrdtxt="";
	public CheckBox[] electypchk = new CheckBox[9];
	public CheckBox[] vocpchk = new CheckBox[3];
	public CheckBox[] oehchk = new CheckBox[8];
    RadioGroup mdcrdgval,acsrdgval,updrdgval;
    boolean mdcck=false,acck=false,updck=false;	
	@Override
	   public void onCreate(Bundle savedInstanceState) {

	        super.onCreate(savedInstanceState);
	        cf=new CommonFunctions(this);
	        Bundle extras = getIntent().getExtras();
			if (extras != null) {
				cf.getExtras(extras);
				
		 	}
	        setContentView(R.layout.gchelectric);
	        cf.getDeviceDimensions();
	        
	        LinearLayout hdr_layout = (LinearLayout) findViewById(R.id.hedr);
			hdr_layout.addView(new HdrOnclickListener(this,1,"General Conditions & Hazards","Electric System",4,0,cf));
	        LinearLayout mainmenu_layout = (LinearLayout) findViewById(R.id.header);
	        mainmenu_layout.setMinimumWidth(cf.wd);
	        mainmenu_layout.addView(new MyOnclickListener(getApplicationContext(), 4, 0,0,cf));
	        LinearLayout submenu_layout = (LinearLayout) findViewById(R.id.submenu);
			submenu_layout.addView(new MyOnclickListener(getApplicationContext(), 48, 1,0,cf));
			TableRow tblrw = (TableRow)findViewById(R.id.row2);
			tblrw.setMinimumHeight(cf.ht);
			cf.CreateARRTable(48);
			declaration();
			GCHElectric_setvalue();
		
	}
	private void GCHElectric_setvalue() {
		// TODO Auto-generated method stub
		try
		{
			chk_values();
		   Cursor ELEC_retrive=cf.SelectTablefunction(cf.GCH_Electrictbl, " where fld_srid='"+cf.selectedhomeid+"' AND (fld_elecchk='1' OR (fld_elecchk='0' AND fld_mdisconnect<>''))");
		   if(ELEC_retrive.getCount()>0)
		   {  
			   ELEC_retrive.moveToFirst();
			   ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
			   if(ELEC_retrive.getInt(ELEC_retrive.getColumnIndex("fld_elecchk"))==1)
			   {
				   ((CheckBox)findViewById(R.id.ec_na)).setChecked(true);
				   ((LinearLayout)findViewById(R.id.elec_table)).setVisibility(cf.v1.GONE);
			   }
			   else
			   {
				   ((CheckBox)findViewById(R.id.ec_na)).setChecked(false);
				   ((LinearLayout)findViewById(R.id.elec_table)).setVisibility(cf.v1.VISIBLE);
				  
				     mdcrdgtxt=ELEC_retrive.getString(ELEC_retrive.getColumnIndex("fld_mdisconnect"));
				    ((RadioButton) mdcrdgval.findViewWithTag(mdcrdgtxt)).setChecked(true);
				  
				   mpaspinval=ELEC_retrive.getString(ELEC_retrive.getColumnIndex("fld_mpamp"));
				   if(!mpaspinval.trim().equals(""))
					{ 
						if(getFromArray(mpaspinval,amps))
						 {  
							mparspin.setSelection(adapter.getPosition("Other"));
						   
						 }
						 else
						 {
							
							    mparspin.setSelection(adapter.getPosition(mpaspinval)); 
							   ((EditText)findViewById(R.id.mpspinn_otr)).setVisibility(cf.v1.VISIBLE);
							   ((EditText)findViewById(R.id.mpspinn_otr)).setText(cf.decode(ELEC_retrive.getString(ELEC_retrive.getColumnIndex("fld_mpampotr")))); 
								
						 }
					}	 
				   
				   electypechk_val=cf.decode(ELEC_retrive.getString(ELEC_retrive.getColumnIndex("fld_electype")));
				   cf.setvaluechk1(electypechk_val,electypchk,((EditText)findViewById(R.id.electype_otr)));
				   
				   vocpchk_val=cf.decode(ELEC_retrive.getString(ELEC_retrive.getColumnIndex("fld_voprotect")));
				   cf.setvaluechk1(vocpchk_val,vocpchk,((EditText)findViewById(R.id.vop_other)));
				   
				   oehchk_val=cf.decode(ELEC_retrive.getString(ELEC_retrive.getColumnIndex("fld_oehz")));
				   cf.setvaluechk1(oehchk_val,oehchk,((EditText)findViewById(R.id.oeh_other)));
				   
				   acsrdgtxt=ELEC_retrive.getString(ELEC_retrive.getColumnIndex("fld_csatis"));
				   ((RadioButton) acsrdgval.findViewWithTag(acsrdgtxt)).setChecked(true);
				   
				   updrdgtxt=ELEC_retrive.getString(ELEC_retrive.getColumnIndex("fld_updated"));
				  ((RadioButton) updrdgval.findViewWithTag(updrdgtxt)).setChecked(true);
				   
				   String wage=cf.decode(ELEC_retrive.getString(ELEC_retrive.getColumnIndex("fld_age")));
				    if(wage.contains("Other")){ 
					    String[] temp = wage.split("~");
					    agestr = temp[0];
					    ((EditText)findViewById(R.id.age_otr)).setText(temp[1]);
					    ((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.VISIBLE);
				    }else{
				    	agestr = wage;((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.GONE);
					 }
				    agespin.setSelection(ageadap.getPosition(agestr)); 
				    
				    ((EditText)findViewById(R.id.elecriccomment)).setText(cf.decode(ELEC_retrive.getString(ELEC_retrive.getColumnIndex("fld_comments"))));
				  
			   }
			  
		   }
		   else
		   {
			   cf.getinspectioname(cf.selectedhomeid);
			    if(cf.selinspname.equals("4"))
				{
					((CheckBox)findViewById(R.id.ec_na)).setChecked(false);
					((LinearLayout)findViewById(R.id.elec_table)).setVisibility(cf.v1.VISIBLE);
				}
				else
				{
					((CheckBox)findViewById(R.id.ec_na)).setChecked(true);
					((LinearLayout)findViewById(R.id.elec_table)).setVisibility(cf.v1.GONE);
				}
			  
		   }
		   
		}
	    catch (Exception E)
		{
			String strerrorlog="Retrieving HVAC - GCH";
			cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
	public boolean getFromArray(String spinval, String[] ampsarr)
	 {
	 	for(int i=0;i < ampsarr.length;i++)
	 	 {
	 		 if(spinval.equals(ampsarr[i]))
	 		 {
	 			 return false;
	 		 }
	 	 }
	 	return true;
	 }
	private void declaration() {
		// TODO Auto-generated method stub
		cf.setupUI((ScrollView)findViewById(R.id.scr));
		((TextView)findViewById(R.id.md)).setText(Html.fromHtml(cf.redcolor+"Main Disconnect"));
		((TextView)findViewById(R.id.mpar)).setText(Html.fromHtml(cf.redcolor+"Main Panel Amp Rating"));
		((TextView)findViewById(R.id.rowtxt3)).setText(Html.fromHtml(cf.redcolor+"Electrical Type"));
		((TextView)findViewById(R.id.rowtxt4)).setText(Html.fromHtml(cf.redcolor+"Visible Overcurrent Protection"));
		((TextView)findViewById(R.id.rowtxt5)).setText(Html.fromHtml(cf.redcolor+"Observed Electrical Hazards"));
		
		mdcrdgval = (RadioGroup)findViewById(R.id.mdc_rdg);
		mdcrdgval.setOnCheckedChangeListener(new checklistenetr(1));
		
		mparspin = (Spinner) this.findViewById(R.id.mpspinn);
		adapter = new ArrayAdapter(GCHElectric.this,
		 			android.R.layout.simple_spinner_item, amps);
	 	adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	 	mparspin.setAdapter(adapter);
	  	mparspin.setOnItemSelectedListener(new spinnerlisner());
	    ((EditText)findViewById(R.id.mpspinn_otr)).addTextChangedListener(new textwatcher(1));
	  	
	  	electypchk[0] = ((CheckBox)findViewById(R.id.typ_chk1));
	  	electypchk[1] = ((CheckBox)findViewById(R.id.typ_chk2));
	  	electypchk[2] = ((CheckBox)findViewById(R.id.typ_chk3));
	  	electypchk[3] = ((CheckBox)findViewById(R.id.typ_chk4));
	  	electypchk[4] = ((CheckBox)findViewById(R.id.typ_chk5));
	  	electypchk[5] = ((CheckBox)findViewById(R.id.typ_chk6));
		electypchk[6] = ((CheckBox)findViewById(R.id.typ_chk7));
	  	electypchk[7] = ((CheckBox)findViewById(R.id.typ_chk8));
	  	electypchk[8] = ((CheckBox)findViewById(R.id.typ_chk9));
	  	
	  	vocpchk[0] = ((CheckBox)findViewById(R.id.voc_chk1));
	  	vocpchk[1] = ((CheckBox)findViewById(R.id.voc_chk2));
	  	vocpchk[2] = ((CheckBox)findViewById(R.id.voc_chk3));
	  	
	  	oehchk[0] = ((CheckBox)findViewById(R.id.oeh_chk1));
	  	oehchk[1] = ((CheckBox)findViewById(R.id.oeh_chk2));
	  	oehchk[2] = ((CheckBox)findViewById(R.id.oeh_chk3));
	  	oehchk[3] = ((CheckBox)findViewById(R.id.oeh_chk4));
	  	oehchk[4] = ((CheckBox)findViewById(R.id.oeh_chk5));
		oehchk[5] = ((CheckBox)findViewById(R.id.oeh_chk6));
	  	oehchk[6] = ((CheckBox)findViewById(R.id.oeh_chk7));
	  	oehchk[7] = ((CheckBox)findViewById(R.id.oeh_chk8));
	  	
	  	acsrdgval = (RadioGroup)findViewById(R.id.acs_rdg);
	  	acsrdgval.setOnCheckedChangeListener(new checklistenetr(2));
	  	updrdgval = (RadioGroup)findViewById(R.id.upd_rdg);
	  	updrdgval.setOnCheckedChangeListener(new checklistenetr(3));
	  	set_default();
		
	  	agespin = (Spinner) this.findViewById(R.id.age_spin);
	  	ageadap = new ArrayAdapter(this,android.R.layout.simple_spinner_item, cf.yearbuilt);
		ageadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		agespin.setAdapter(ageadap);
		defaultage(); 
		
		agespin.setOnItemSelectedListener(new  Spin_Selectedlistener());
		((EditText)findViewById(R.id.age_otr)).addTextChangedListener(new textwatcher(2));
		((EditText)findViewById(R.id.elecriccomment)).addTextChangedListener(new textwatcher(3));
			
	}
	private void defaultage()
	{
		cf.getPolicyholderInformation(cf.selectedhomeid);
		if(cf.YearPH.contains("Other"))
		{
			  String otheryear[] = cf.YearPH.split("&#126;");
			  int spinnerPosition1 = ageadap.getPosition(otheryear[0]);
			  agespin.setSelection(spinnerPosition1);
			  ((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.VISIBLE);
			  ((EditText)findViewById(R.id.age_otr)).setText(otheryear[1]);			   
		}
		else
		{
			  int spinnerPosition = ageadap.getPosition(cf.YearPH);
			   agespin.setSelection(spinnerPosition);		  
			 ((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.GONE);
		}		
	}
	class textwatcher implements TextWatcher
    {
	         public int type;
	         textwatcher(int type)
	    	{
	    		this.type=type;
	    	}
	    	@Override
	    	public void afterTextChanged(Editable s) {


	    		// TODO Auto-generated method stub
	    		if(this.type==1)
	    		{
	    			 if(!s.toString().equals("")){
	    			     if(Character.toString(s.toString().charAt(0)).equals("0"))
		    			 {
		    				 cf.ShowToast("Main Panel Amp Rating should be Greater than 0.", 0);
		    				 ((EditText)findViewById(R.id.mpspinn_otr)).setText("");
		    			 }
	    			 }
	    		}
	    		else if(this.type==2)
	    		{
	    			 if(!s.toString().equals("")){
	    			     if(Character.toString(s.toString().charAt(0)).equals("0"))
		    			 {
		    				 cf.ShowToast("Invalid Entry! Please enter valid Year of Age.", 0);
		    				 ((EditText)findViewById(R.id.age_otr)).setText("");
		    			 }
	    			 }
	    		}
	    		else if(this.type==3)
	    		{
	    			if(s.toString().equals(" ")){
	    		       ((EditText)findViewById(R.id.elecriccomment)).setText("");
		    		}
	    			else
	    			{
	    				cf.showing_limit(s.toString(),(TextView)findViewById(R.id.electric_tv_type1),500); 
	    			}
	    		}
	}

	    	@Override
	    	public void beforeTextChanged(CharSequence s, int start, int count,

	    			int after) {
	    		// TODO Auto-generated method stub
	     	}

	    	@Override
	    	public void onTextChanged(CharSequence s, int start, int before,

	    			int count) {
	    		// TODO Auto-generated method stub
	    	}
	   }	
	private void set_default() {
		((RadioButton)acsrdgval.findViewWithTag("No")).setChecked(true);
	  	((RadioButton)updrdgval.findViewWithTag("No")).setChecked(true);// TODO Auto-generated method stub
	  	mpaspinval="";electypechk_val="";vocpchk_val="";oehchk_val="";
				mdcrdgtxt="";acsrdgtxt="No";updrdgtxt="No";
				cf.getPolicyholderInformation(cf.selectedhomeid);agestr=cf.YearPH;
		
	}
	class Spin_Selectedlistener implements OnItemSelectedListener
	{
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			    agestr = agespin.getSelectedItem().toString();
				if(agestr.equals("Other")){
					((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.VISIBLE);
					//cf.setFocus(((EditText)findViewById(R.id.age_otr)));
					((EditText)findViewById(R.id.mpspinn_otr)).clearFocus();
				}else {
					((EditText)findViewById(R.id.age_otr)).setText("");
					((EditText)findViewById(R.id.age_otr)).setVisibility(cf.v1.GONE);
				
				}
		
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	class Touch_Listener implements OnTouchListener

    {
    	   public int type;
    	   Touch_Listener(int type)
    		{
    			this.type=type;
    			
    		}
    	    @Override
    		public boolean onTouch(View v, MotionEvent event) {

    			// TODO Auto-generated method stub
    	    	((EditText) findViewById(v.getId())).setFocusableInTouchMode(true);
		    	((EditText) findViewById(v.getId())).requestFocus();
    			return false;
    	    }
    }	
	class spinnerlisner implements OnItemSelectedListener
	{

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			String spintxt;
			switch (arg0.getId())
			{
			
			 case R.id.mpspinn:
				 spintxt=mparspin.getSelectedItem().toString();
				 mpaspinval = amps[arg2];
				 if(mpaspinval.equals("Other"))
				 {
					 ((EditText)findViewById(R.id.mpspinn_otr)).setVisibility(arg1.VISIBLE);
				     ((TextView)findViewById(R.id.mpttxt)).setVisibility(arg1.VISIBLE);
				 }
				 else
				 {
					 ((EditText)findViewById(R.id.mpspinn_otr)).setText("");
					 ((EditText)findViewById(R.id.mpspinn_otr)).setVisibility(arg1.GONE);
					 ((TextView)findViewById(R.id.mpttxt)).setVisibility(arg1.INVISIBLE);
				 
				 }
						
                break;
			}
			 
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	public void clicker(View v)
	{
		switch (v.getId()) {
		
		case R.id.ec_na:/** ELECTRIC NOT APPLICABLE CHECKBOX **/
			if(((CheckBox)findViewById(R.id.ec_na)).isChecked())
			{
				
				System.out.println("retelecrdtxt"+retelecrdtxt);
    			if(!retelecrdtxt.equals("")){
 				    showunchkalert(1);
 			    }
    			else
    			{
    				clearelc();
    				set_default();
    				((LinearLayout)findViewById(R.id.elec_table)).setVisibility(v.GONE);
 			    	((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.INVISIBLE);
 			    }
				
			}
			else
			{
				((LinearLayout)findViewById(R.id.elec_table)).setVisibility(v.VISIBLE);
				((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.INVISIBLE);
				((CheckBox)findViewById(R.id.ec_na)).setChecked(false);
			}
			
			break;
		case R.id.helpid:
			cf.ShowToast("Use for Commercial GCH report only.",0);
			break;
		case R.id.voc_chk3:/** TYPE CHECKBOX OTHER **/
			  if(vocpchk[vocpchk.length-1].isChecked())
			  { 
				  ((EditText)findViewById(R.id.vop_other)).setVisibility(v.VISIBLE);
			  }
			  else
			  {
				  ((EditText)findViewById(R.id.vop_other)).setText("");
				  ((EditText)findViewById(R.id.vop_other)).setVisibility(v.GONE);
			  }
			  break;
		case R.id.typ_chk9:/** TYPE CHECKBOX OTHER **/
			  if(electypchk[electypchk.length-1].isChecked())
			  { 
				  ((EditText)findViewById(R.id.electype_otr)).setVisibility(v.VISIBLE);
			  }
			  else
			  {
				  ((EditText)findViewById(R.id.electype_otr)).setText("");
				  ((EditText)findViewById(R.id.electype_otr)).setVisibility(v.GONE);
			  }
			  break;
			
		case R.id.oeh_chk8:
			  if(oehchk[oehchk.length-1].isChecked())
			  { 
				  ((EditText)findViewById(R.id.oeh_other)).setVisibility(v.VISIBLE);
			  }
			  else
			  {
				  ((EditText)findViewById(R.id.oeh_other)).setText("");
				  ((EditText)findViewById(R.id.oeh_other)).setVisibility(v.GONE);
			  }
			  break;
		case R.id.hme:
			cf.gohome();
			break;
		
		case R.id.save:
			if(((CheckBox)findViewById(R.id.ec_na)).isChecked())
			{
				elecchkval=1;E_Insert();
				((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
				cf.ShowToast("Electric System saved successfully.", 0);
				cf.goclass(417);
			}
			else
			{
				elecchkval=0;
				if(mdcrdgtxt.equals(""))
				{
					cf.ShowToast("Please select the option for Main Disconnect.", 0);
				}
				else
				{
					mpaspinval = mparspin.getSelectedItem().toString();
					if(mpaspinval.equals("--Select--"))
					{
						cf.ShowToast("Please select the option for Main Panel Amp Rating.", 0);
					}
					else
					{
						if(mpaspinval.equals("Other"))
						{
							if(((EditText)findViewById(R.id.mpspinn_otr)).getText().toString().equals(""))
							{
								cf.ShowToast("Please enter the Other text for Main Panel Amp Rating.", 0);
								cf.setFocus(((EditText)findViewById(R.id.mpspinn_otr)));
								
							}
							else
							{
								String mpttext=((EditText)findViewById(R.id.mpspinn_otr)).getText().toString();
								int fv = Character.getNumericValue(mpttext.charAt(0));
								if(fv==0)
								{
									cf.ShowToast("Please enter the valid Other Main Panel Amp Rating.", 0);
									cf.setFocus(((EditText)findViewById(R.id.mpspinn_otr)));
								}
								else
								{
									electricalvalidation();
								}
							}
						}
						else
						{
							electricalvalidation();
						}
					}
				}
			}
			
			
			break;

		}
	}
	private void electricalvalidation() {
		// TODO Auto-generated method stub
		electypechk_val= cf.getselected_chk(electypchk);
		String electypeotrval =(electypchk[electypchk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.electype_otr)).getText().toString():""; 
		electypechk_val+=electypeotrval;
		 if(electypechk_val.equals(""))
		 {
			 cf.ShowToast("Please select the option for Electrical Type.", 0);
		 }
		 else
		 {
			 if(electypchk[electypchk.length-1].isChecked())
			 {	 
				 if(electypeotrval.trim().equals("&#40;"))
				 {
					 cf.ShowToast("Please enter the other text for Electrical Type.", 0);
					 cf.setFocus(((EditText)findViewById(R.id.electype_otr)));
				 }
				 else
				 {
					visibleovercurrentvalidation();
				 }
			 
			 }
			 else
			 {
				 visibleovercurrentvalidation();				 
			 }
		 }
	}
	private void visibleovercurrentvalidation() {
		// TODO Auto-generated method stub
		vocpchk_val= cf.getselected_chk(vocpchk);
		String vocpchkotrval =(vocpchk[vocpchk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.vop_other)).getText().toString():""; 
		vocpchk_val+=vocpchkotrval;
		 if(vocpchk_val.equals(""))
		 {
			 cf.ShowToast("Please select the option for Visible Overcurrent Protection.", 0);
		 }
		 else
		 {
			 if(vocpchk[vocpchk.length-1].isChecked())
			 {	 
				 if(vocpchkotrval.trim().equals("&#40;"))
				 {
					 cf.ShowToast("Please enter the other text for Visible Overcurrent Protection.", 0);
					 cf.setFocus(((EditText)findViewById(R.id.vop_other)));
				 }
				 else
				 {
					 observedelecvalidation();
				 }
			 
			 }
			 else
			 {
				 observedelecvalidation();				 
			 }
		 }
	}
	private void observedelecvalidation() {
		// TODO Auto-generated method stub
		oehchk_val= cf.getselected_chk(oehchk);
		String oechkeotrval =(oehchk[oehchk.length-1].isChecked())? "&#40;"+((EditText)findViewById(R.id.oeh_other)).getText().toString():""; 
		oehchk_val+=oechkeotrval;
		 if(oehchk_val.equals(""))
		 {
			 cf.ShowToast("Please select the option for Observed Electrical Hazards.", 0);
		 }
		 else
		 {
			 if(oehchk[oehchk.length-1].isChecked())
			 {	 
				 if(oechkeotrval.trim().equals("&#40;"))
				 {
					 cf.ShowToast("Please enter the other text for Observed Electrical Hazards.", 0);
					 cf.setFocus(((EditText)findViewById(R.id.oeh_other)));
				 }
				 else
				 {
					 observedeleccondvalidation();
				 }
			 
			 }
			 else
			 {
				 observedeleccondvalidation();				 
			 }
		 }
	}
	private void observedeleccondvalidation() {
		// TODO Auto-generated method stub
		
			if(agestr.equals("--Select--")){
	        	cf.ShowToast("Please select Year of Age.",0);
	        }else{
	        	if(agestr.contains("Other") || agestr.contains("Other~")){
	        		if(((EditText)findViewById(R.id.age_otr)).getText().toString().trim().equals("")){
						cf.ShowToast("Please enter the Other text for Year of Age.",0);
						cf.setFocus(((EditText)findViewById(R.id.age_otr)));
					}else{
						if(((EditText)findViewById(R.id.age_otr)).getText().toString().length()<4){
							cf.ShowToast("Please enter the valid Year of Age.",0);
							((EditText)findViewById(R.id.age_otr)).setText("");
							cf.setFocus(((EditText)findViewById(R.id.age_otr)));
						}else{
							if(cf.yearValidation(((EditText)findViewById(R.id.age_otr)).getText().toString()).equals("true"))
							{
								cf.ShowToast("Year of Age should not exceed current year.", 0);
								cf.setFocus(((EditText)findViewById(R.id.age_otr)));
							}
							else if(cf.yearValidation(((EditText)findViewById(R.id.age_otr)).getText().toString()).equals("zero"))
							{
								cf.ShowToast("Please enter the valid Year of Age.", 0);
								cf.setFocus(((EditText)findViewById(R.id.age_otr)));
							}
							else
							{
							   agestr += "~"+((EditText)findViewById(R.id.age_otr)).getText().toString();
							   electric_comments();
							  
							}
						}
					}
	        	}else{
	        		agestr = agestr;
	        		electric_comments();	        		
	        	}
	        }			
		
	}
	private void electric_comments() {
		// TODO Auto-generated method stub
		/*if(((EditText)findViewById(R.id.elecriccomment)).getText().toString().trim().equals(""))
		{
			cf.ShowToast("Please enter the comments.", 0);
		}
		else
		{*/
			E_Insert();
		   ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
			cf.ShowToast("Electric System saved successfully.", 0);
			cf.goclass(417);
//		//}
		    
	}
	private void E_Insert() {
		// TODO Auto-generated method stub
		Cursor ELE_save=null;
		try
		{
			ELE_save=cf.SelectTablefunction(cf.GCH_Electrictbl, " where fld_srid='"+cf.selectedhomeid+"'");
			if(ELE_save.getCount()>0)
			{
				try
				{
					System.out.println("vocpchk_val"+vocpchk_val);
					cf.arr_db.execSQL("UPDATE "+cf.GCH_Electrictbl+ " set fld_elecchk='"+elecchkval+"',fld_mdisconnect='"+mdcrdgtxt+"',"+
				                        "fld_mpamp='"+mpaspinval+"',fld_mpampotr='"+cf.encode(((EditText)findViewById(R.id.mpspinn_otr)).getText().toString())+"',"+
							            "fld_electype='"+cf.encode(electypechk_val)+"',fld_voprotect='"+cf.encode(vocpchk_val)+"',fld_oehz='"+cf.encode(oehchk_val)+"',fld_csatis='"+acsrdgtxt+"',"+
				                        "fld_updated='"+updrdgtxt+"',fld_age='"+cf.encode(agestr)+"',fld_comments='"+cf.encode(((EditText)findViewById(R.id.elecriccomment)).getText().toString())+"' where fld_srid='"+cf.selectedhomeid+"'");
						/*((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
					cf.ShowToast("Electric System saved successfully.", 0);
					cf.goclass(416);*/
				}
				catch (Exception E)
				{
					String strerrorlog="Updating the ELECTRIC - GCH";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
			}
			else
			{
				try
				{
					System.out.println("INSERT INTO "
						+ cf.GCH_Electrictbl
						+ " (fld_srid,fld_elecchk,fld_mdisconnect,fld_mpamp,fld_mpampotr,fld_electype,fld_voprotect,fld_oehz,fld_csatis,"+
				                        "fld_updated,fld_age,fld_comments)"
						+ "VALUES ('"+cf.selectedhomeid+"','"+elecchkval+"','"+mdcrdgtxt+"','"+mpaspinval+"','"+cf.encode(((EditText)findViewById(R.id.mpspinn_otr)).getText().toString())+"',"+
							            "'"+cf.encode(electypechk_val)+"','"+vocpchk_val+"','"+cf.encode(oehchk_val)+"','"+acsrdgtxt+"','"+updrdgtxt+"',"+
						                "'"+cf.encode(agestr)+"','"+cf.encode(((EditText)findViewById(R.id.elecriccomment)).getText().toString())+"')");
					
					cf.arr_db.execSQL("INSERT INTO "
						+ cf.GCH_Electrictbl
						+ " (fld_srid,fld_elecchk,fld_mdisconnect,fld_mpamp,fld_mpampotr,fld_electype,fld_voprotect,fld_oehz,fld_csatis,"+
				                        "fld_updated,fld_age,fld_comments)"
						+ "VALUES ('"+cf.selectedhomeid+"','"+elecchkval+"','"+mdcrdgtxt+"','"+mpaspinval+"','"+cf.encode(((EditText)findViewById(R.id.mpspinn_otr)).getText().toString())+"',"+
							            "'"+cf.encode(electypechk_val)+"','"+vocpchk_val+"','"+cf.encode(oehchk_val)+"','"+acsrdgtxt+"','"+updrdgtxt+"',"+
						                "'"+cf.encode(agestr)+"','"+cf.encode(((EditText)findViewById(R.id.elecriccomment)).getText().toString())+"')");
					/*((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.VISIBLE);
				cf.ShowToast("Electric System saved successfully.", 0);
				 cf.goclass(416);*/
				}
				catch (Exception E)
				{
					String strerrorlog="Inserting the ELECTRIC- GCH";
					cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
				}
			}
		}
		catch (Exception E)
		{
				String strerrorlog="Checking the rows inserted in the GCH Electric table.";
				cf.Error_LogFile_Creation(strerrorlog+" "+" at "+ getApplicationContext()+" "+" at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
		}
	}
	class checklistenetr implements OnCheckedChangeListener, android.widget.RadioGroup.OnCheckedChangeListener
	{
    	int i;
		public checklistenetr(int i) {
			// TODO Auto-generated constructor stub
		   this.i=i;
		}

		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			   RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
		        boolean isChecked = checkedRadioButton.isChecked();
		        if (isChecked)
		        {
		          switch (i) {
					case 1:
						mdcrdgtxt= checkedRadioButton.getText().toString().trim();
						 break;
					case 2:
						acsrdgtxt= checkedRadioButton.getText().toString().trim();
						break;
					case 3:
						updrdgtxt= checkedRadioButton.getText().toString().trim();
						break;
				
		          }
		        }
		}

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			// TODO Auto-generated method stub
			
		}
    }
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			cf.goclass(47);
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}
	private void chk_values() {
		// TODO Auto-generated method stub
		try
		{
		   Cursor elec_retrive=cf.SelectTablefunction(cf.GCH_Electrictbl, " where fld_srid='"+cf.selectedhomeid+"'");
		   int rws = elec_retrive.getCount();
			if(rws>0){
				elec_retrive.moveToFirst();
				retelecrdtxt = elec_retrive.getString(elec_retrive.getColumnIndex("fld_mdisconnect"));
			}
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}
	public void showunchkalert(final int i) {
		// TODO Auto-generated method stub
		AlertDialog.Builder bl = new Builder(GCHElectric.this);
		bl.setTitle("Confirmation");
		bl.setMessage(Html.fromHtml("Do you want to clear the Electric System data?"));
		bl.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog,
											int id) {
										 switch(i){
										 case 1:
											 ((TextView)findViewById(R.id.savetxt1)).setVisibility(cf.v1.GONE);
							    			  clearelc();//E_Insert();
							    			  //cf.arr_db.execSQL("Delete from "+cf.GCH_Electrictbl+" Where fld_srid='"+cf.selectedhomeid+"'");
												 
							    			  set_default();cf.getPolicyholderInformation(cf.selectedhomeid);
							    			  agestr=cf.YearPH;
							    			 ((LinearLayout)findViewById(R.id.elec_table)).setVisibility(cf.v1.GONE);
							    			 break;
		                                 
										 }
										 	 
										 
									}
		});
		bl.setNegativeButton("No",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog,
							int id) {
						switch(i){
						 case 1:
							 ((CheckBox)findViewById(R.id.ec_na)).setChecked(false);
							 break;
					
						 }
					}
        });
		AlertDialog al=bl.create();
		al.setIcon(R.drawable.alertmsg);
		al.setCancelable(false);
		al.show();
		
		
	}
	protected void clearelc() {
		// TODO Auto-generated method stub
		  elecchkval=0;mdcck=true;acck=true;updck=true;
		  try{if(mdcck)mdcrdgval.clearCheck();}catch (Exception e) {}
		  try{if(acck)acsrdgval.clearCheck();}catch (Exception e) {}
		  try{if(updck)updrdgval.clearCheck();}catch (Exception e) {}
		  mdcrdgtxt="";mparspin.setSelection(0);mpaspinval="";
		  electypechk_val="";vocpchk_val="";oehchk_val="";
		  retelecrdtxt="";
		  updrdgtxt="";acsrdgtxt="";//retelecrdtxt="";
		  cf.Set_UncheckBox(electypchk, ((EditText)findViewById(R.id.typ_otr)));
		  cf.Set_UncheckBox(vocpchk, ((EditText)findViewById(R.id.typ_otr)));
		  cf.Set_UncheckBox(oehchk, ((EditText)findViewById(R.id.typ_otr)));
		  cf.getPolicyholderInformation(cf.selectedhomeid);
		   int spinnerPosition = ageadap.getPosition(cf.YearPH);
		   agespin.setSelection(spinnerPosition);
		  ((EditText)findViewById(R.id.age_otr)).setText("");
		  ((EditText)findViewById(R.id.elecriccomment)).setText("");
		  set_default();
		  
	}
}
