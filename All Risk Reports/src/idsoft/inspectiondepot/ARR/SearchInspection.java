package idsoft.inspectiondepot.ARR;

import idsoft.inspectiondepot.ARR.R;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Html;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class SearchInspection extends Activity {
	String fstname, lstname, pn;
	CommonFunctions cf;
	LinearLayout lst;
	String[] data;
	ScrollView sv;
	String dta = "";
	int Cnt;
	EditText edfirstname,edlastname,edploicy;
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		Bundle b = getIntent().getExtras();
		if (b != null) {
			fstname = b.getString("first");
			lstname = b.getString("last");
			pn = b.getString("policy");

		}
		cf = new CommonFunctions(this);
		setContentView(R.layout.searchinspection);
		
		lst = (LinearLayout) findViewById(R.id.linlayoutdyn);
		cf.getInspectorId();
		ImageView im =(ImageView) findViewById(R.id.head_insp_info);
		im.setVisibility(View.VISIBLE);
		im.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				    Intent s2= new Intent(SearchInspection.this,PolicyholdeInfoHead.class);
					Bundle b2 = new Bundle();
					s2.putExtra("homeid", cf.selectedhomeid);
					s2.putExtra("Type", "Inspector");
					s2.putExtra("insp_id", cf.Insp_id);
					((Activity) cf.con).startActivityForResult(s2, cf.info_requestcode);
			}
		});
		fetchdata();
	}

	private void fetchdata() {
		// TODO Auto-generated method stub
		SoapObject request = new SoapObject(cf.NAMESPACE, "SEARCHPOLICYHOLDERINFO");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("InspectorID", cf.Insp_id);
		request.addProperty("OwnerFirstName", fstname);
		request.addProperty("OwnerLastName", lstname);
		request.addProperty("OwnerPolicyNumber", pn);
		envelope.setOutputSoapObject(request);System.out.println("requestsearch"+request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL_ARR);
		try {
			androidHttpTransport.call(cf.NAMESPACE+"SEARCHPOLICYHOLDERINFO",envelope);
			SoapObject result = (SoapObject) envelope.getResponse();
			System.out.println("result "+result);
			if (result != null) {				
				if (result.toString().equals("null")
						|| result.toString().equals("")) {
					cf.ShowToast("Sorry, No records found.",0);
					((LinearLayout) findViewById(R.id.dynamiclayout)).setVisibility(cf.v1.GONE);
					((TextView) findViewById(R.id.norecordstxt)).setVisibility(cf.v1.VISIBLE);					
				} else {
					Cnt = result.getPropertyCount();
					((TextView) findViewById(R.id.noofrecords)).setText("No of Records : "+Cnt);
					((LinearLayout) findViewById(R.id.dynamiclayout)).setVisibility(cf.v1.VISIBLE);
					((TextView) findViewById(R.id.norecordstxt)).setVisibility(cf.v1.GONE);

					String[] data = new String[Cnt];
					
					for (int j = 0; j < Cnt; j++) {
						SoapObject result1 = (SoapObject) result.getProperty(j);
						try {
							dta += " "
									+ result1.getProperty("FirstName")
											.toString() + " ";
							dta += result1.getProperty("LastName").toString()
									+ " | ";
							dta += result1.getProperty("PolicyNumber")
									.toString() + " | ";
							dta += result1.getProperty("Address1").toString()
									+ " | ";
							dta += result1.getProperty("City").toString()
									+ " | ";
							dta += result1.getProperty("State").toString()
									+ " | ";
							dta += result1.getProperty("County").toString()
									+ " | ";
							dta += result1.getProperty("StatusName").toString()
									+ "|";
							dta += result1.getProperty("Inspectionname").toString()
									+ "~";
							
					} catch (Exception e) {

						}
					}
					display();
				}
			} else {
				cf.ShowToast("Sorry, No records found.",0);
				((LinearLayout) findViewById(R.id.dynamiclayout)).setVisibility(cf.v1.GONE);
				((TextView) findViewById(R.id.norecordstxt)).setVisibility(cf.v1.VISIBLE);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void display() {
		// TODO Auto-generated method stub
		lst.removeAllViews();
		sv = new ScrollView(this);
		lst.addView(sv);;

		final LinearLayout l1 = new LinearLayout(this);
		l1.setOrientation(LinearLayout.VERTICAL);
		sv.addView(l1);
		
		View v = new View(this);
		v.setBackgroundResource(R.color.black);
		l1.addView(v,LayoutParams.FILL_PARENT,0);
		
		this.data = dta.split("~");
			for (int i = 0; i < data.length; i++)
			{
				TextView[] tvstatus = new TextView[Cnt];
	
				LinearLayout l2 = new LinearLayout(this);
				l2.setOrientation(LinearLayout.HORIZONTAL);
				l1.addView(l2);
	
				LinearLayout lchkbox = new LinearLayout(this);
				LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(900, ViewGroup.LayoutParams.WRAP_CONTENT);
				paramschk.topMargin = 8;
				paramschk.leftMargin = 20;
				paramschk.bottomMargin = 10;
				l2.addView(lchkbox);
	
				tvstatus[i] = new TextView(this);
				tvstatus[i].setTag("textbtn" + i);
				tvstatus[i].setText(data[i]);
				tvstatus[i].setTextColor(Color.WHITE);
				tvstatus[i].setTextSize(TypedValue.COMPLEX_UNIT_PX,14);
				lchkbox.addView(tvstatus[i], paramschk);
	
				LinearLayout ldelbtn = new LinearLayout(this);
				LinearLayout.LayoutParams paramsdelbtn = new LinearLayout.LayoutParams(
						93, 37);
				paramsdelbtn.rightMargin = 10;
				paramsdelbtn.bottomMargin = 10;
				l2.addView(ldelbtn);
			
				 if (i % 2 == 0) {
					 l2.setBackgroundColor(Color.parseColor("#13456d"));
				    	} else {
				    	l2.setBackgroundColor(Color.parseColor("#386588"));
				    	}
					if(data.length!=(i+1))
					{
						 View v1 = new View(this);
						 v1.setBackgroundResource(R.color.white);
						 l1.addView(v1,LayoutParams.FILL_PARENT,0);	 
					}
			}
			 View v2 = new View(this);
			 v2.setBackgroundResource(R.color.black);
			 lst.addView(v2,LayoutParams.FILL_PARENT,0);
	}
	public void clicker(View v) {
		switch (v.getId()) {

		case R.id.home:
			cf.gohome();
			break;
			
		case R.id.srch:
			final Dialog dialog = new Dialog(SearchInspection.this,android.R.style.Theme_Translucent_NoTitleBar);
			dialog.getWindow().setContentView(R.layout.onlinesearch);
			edfirstname = (EditText) dialog.findViewById(R.id.txtfirstname);
			edlastname = (EditText) dialog.findViewById(R.id.txtlastname);
			edploicy = (EditText) dialog.findViewById(R.id.txtpolicy);
			Button btnsrch = (Button) dialog.findViewById(R.id.search);
			btnsrch.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					cf.hidekeyboard();
					Cnt = 0;
					lst.removeAllViews();
					dta = "";
					if (edfirstname.getText().toString().trim().equals("")
							&& edlastname.getText().toString().trim().equals("")
							&& edploicy.getText().toString().trim().equals("")) {
						cf.ShowToast("Please enter atleast one field to search.",0);
					} else {
						fstname = edfirstname.getText().toString();
						lstname = edlastname.getText().toString();
						pn = edploicy.getText().toString();
						ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
						NetworkInfo info = conMgr.getActiveNetworkInfo();
						if (info != null && info.isConnected()) {
							fetchdata();
							dialog.cancel();
						} else {
							cf.ShowToast("Internet connection not available.",0);
						}
					}
				}
			});
			ImageView btn_helpclose = (ImageView) dialog.findViewById(R.id.helpclose);
			btn_helpclose.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialog.cancel();
				}
				
			});		
			Button btncls = (Button) dialog.findViewById(R.id.cls);
			btncls.setOnClickListener(new OnClickListener() {
				public void onClick(View v)
				{
					dialog.dismiss();
				}
			});
			dialog.show();
			break;
		}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent loginpage = new Intent(SearchInspection.this,HomeScreen.class);
			loginpage.putExtra("back", "exit");
			startActivity(loginpage);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}